﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("NexCash AlBaraka")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Intelligenes")> 
<Assembly: AssemblyProduct("NexCash")> 
<Assembly: AssemblyCopyright("Copyright © 2015 by Intelligenes")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("8d060ae1-39d1-4671-9944-2f236ace796f")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version - Altogether a new version
'      Minor Version  - Major change to existing version. Always increment by 1 for each major change.
'      Build Number - Minor change to existing version. Always increment by 1 for each minor change / hotfix
'      Revision - Fixed at 0
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 


'The AssemblyFileVersion is intended to uniquely identify a build of the individual assembly



<Assembly: AssemblyFileVersion("1.0.0.0")> 

'The AssemblyInformationalVersion is intended to represent the version of your entire product


<Assembly: AssemblyInformationalVersion("NexCash AlBaraka 1.0")> 


'The AssemblyVersion is the only version the CLR cares about (but it cares about the entire AssemblyVersion)

'Do not change the AssemblyVersion for a servicing release which is intended to be backwards compatible.

'Do change the AssemblyVersion for a release that you know has breaking changes.


<Assembly: AssemblyVersion("1.0.0.0")> 