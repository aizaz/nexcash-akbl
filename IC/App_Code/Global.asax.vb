'
' DotNetNuke� - http://www.dotnetnuke.com
' Copyright (c) 2002-2010
' by DotNetNuke Corporation
'
' Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
' documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
' the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
' to permit persons to whom the Software is furnished to do so, subject to the following conditions:
'
' The above copyright notice and this permission notice shall be included in all copies or substantial portions 
' of the Software.
'
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
' DEALINGS IN THE SOFTWARE.
'




Imports System
Imports DotNetNuke.ComponentModel
Imports DotNetNuke.Common.Utilities
Imports System.Web
Imports ICBO
Imports ICBO.IC
Imports System.Net
Imports System.Web.Caching
Imports System.Runtime.Remoting.Messaging


Namespace DotNetNuke.Common

    ''' -----------------------------------------------------------------------------
    ''' Project	 : DotNetNuke
    ''' Class	 : Global
    ''' 
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[sun1]	1/18/2004	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    ''' 


    Public Class [Global]
        Inherits System.Web.HttpApplication

        Private Const DummyCacheItemKey As String = "DummyCacheItemKey"
        Private Const DummyCacheItemKey2 As String = "DummyCacheItemKey2"


        Private Const DummyPageUrl As String = "http://localhost:9004/dummypage.aspx"















        'Private Const DummyPageUrl As String = "http://uatesolution:9000/dummypage.aspx"


        Private Shared IsSending As Boolean = False

        Private Shared IsProcessing As Boolean = False

        Private Delegate Sub LongRun(ByVal name As String)












#Region "Application Event Handlers"

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        ''' Application_Start
        ''' Executes on the first web request into the portal application, 
        ''' when a new DLL is deployed, or when web.config is modified.
        ''' </summary>
        ''' <param name="Sender"></param>
        ''' <param name="E"></param>
        ''' <remarks>
        ''' - global variable initialization
        ''' </remarks>
        ''' <history>
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub Application_Start(ByVal Sender As Object, ByVal E As EventArgs)

            Dim objSwithc As New IntelligenesSwitch.SwitchClient
            Try
                'objSwithc.EuronetConnect()
            Catch ex As Exception

            End Try



            If Config.GetSetting("ServerName") = "" Then
                ServerName = Server.MachineName
            Else
                ServerName = Config.GetSetting("ServerName")
            End If

            ComponentFactory.Container = New SimpleContainer()

            'Install most Providers as Singleton LifeStyle
            ComponentFactory.InstallComponents(New ProviderInstaller("data", GetType(DotNetNuke.Data.DataProvider)))
            ComponentFactory.InstallComponents(New ProviderInstaller("caching", GetType(Services.Cache.CachingProvider)))
            ComponentFactory.InstallComponents(New ProviderInstaller("logging", GetType(Services.Log.EventLog.LoggingProvider)))
            ComponentFactory.InstallComponents(New ProviderInstaller("scheduling", GetType(Services.Scheduling.SchedulingProvider)))
            ComponentFactory.InstallComponents(New ProviderInstaller("searchIndex", GetType(Services.Search.IndexingProvider)))
            ComponentFactory.InstallComponents(New ProviderInstaller("searchDataStore", GetType(Services.Search.SearchDataStoreProvider)))
            ComponentFactory.InstallComponents(New ProviderInstaller("friendlyUrl", GetType(Services.Url.FriendlyUrl.FriendlyUrlProvider)))
            ComponentFactory.InstallComponents(New ProviderInstaller("members", GetType(DotNetNuke.Security.Membership.MembershipProvider)))
            ComponentFactory.InstallComponents(New ProviderInstaller("roles", GetType(DotNetNuke.Security.Roles.RoleProvider)))
            ComponentFactory.InstallComponents(New ProviderInstaller("profiles", GetType(DotNetNuke.Security.Profile.ProfileProvider)))
            ComponentFactory.InstallComponents(New ProviderInstaller("permissions", GetType(DotNetNuke.Security.Permissions.PermissionProvider)))
            ComponentFactory.InstallComponents(New ProviderInstaller("outputCaching", GetType(DotNetNuke.Services.OutputCache.OutputCachingProvider)))
            ComponentFactory.InstallComponents(New ProviderInstaller("moduleCaching", GetType(DotNetNuke.Services.ModuleCache.ModuleCachingProvider)))
            ComponentFactory.InstallComponents(New ProviderInstaller("sitemap", GetType(DotNetNuke.Services.Sitemap.SitemapProvider)))

            Dim provider As DotNetNuke.Security.Permissions.PermissionProvider = DotNetNuke.ComponentModel.ComponentFactory.GetComponent(Of DotNetNuke.Security.Permissions.PermissionProvider)()
            If provider Is Nothing Then
                ComponentFactory.RegisterComponentInstance(Of DotNetNuke.Security.Permissions.PermissionProvider)(New DotNetNuke.Security.Permissions.PermissionProvider())
            End If

            'Install Navigation and Html Providers as NewInstance Lifestyle (ie a new instance is generated each time the type is requested, as there are often multiple instances on the page)
            ComponentFactory.InstallComponents(New ProviderInstaller("htmlEditor", GetType(Modules.HTMLEditorProvider.HtmlEditorProvider), ComponentLifeStyleType.Transient))
            ComponentFactory.InstallComponents(New ProviderInstaller("navigationControl", GetType(Modules.NavigationProvider.NavigationProvider), ComponentLifeStyleType.Transient))




            EntitySpaces.Interfaces.esProviderFactory.Factory = New EntitySpaces.Loader.esDataProviderFactory()


            RegisterCacheEntry2() ' processing and disbursement
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        ''' Application_End
        ''' Executes when the Application times out
        ''' </summary>
        ''' <param name="Sender"></param>
        ''' <param name="E"></param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub Application_End(ByVal Sender As Object, ByVal E As EventArgs)

            ' log APPLICATION_END event
            Initialize.LogEnd()

            ' stop scheduled jobs
            Initialize.StopScheduler()

        End Sub

        Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
            Dim exc As Exception = Server.GetLastError

            'If (exc.GetType Is GetType(HttpException)) Then

            'Dim writer As New IO.StringWriter
            'Server.Execute("/Oops.aspx", writer)
            'Response.Write(writer.ToString())

            Context.Server.ClearError()
                Server.Transfer("/Oops.aspx")

            'End If
        End Sub

#End Region

        Private Sub Global_BeginRequest(ByVal sender As Object, ByVal e As EventArgs) Handles Me.BeginRequest



            Dim app As HttpApplication = CType(sender, HttpApplication)
            Dim Request As HttpRequest = app.Request

            checkUnwantedParams()

            If Request.Url.LocalPath.ToLower.EndsWith("scriptresource.axd") _
                    OrElse Request.Url.LocalPath.ToLower.EndsWith("webresource.axd") _
                    OrElse Request.Url.LocalPath.ToLower.EndsWith("gif") _
                    OrElse Request.Url.LocalPath.ToLower.EndsWith("jpg") _
                    OrElse Request.Url.LocalPath.ToLower.EndsWith("css") _
                    OrElse Request.Url.LocalPath.ToLower.EndsWith("js") Then
                Exit Sub
            End If

            ' all of the logic which was previously in Application_Start was moved to Init() in order to support IIS7 integrated pipeline mode ( which no longer provides access to HTTP context within Application_Start )
            Initialize.Init(app)

            'run schedule if in Request mode
            Initialize.RunSchedule(Request)
            Dim str As String = HttpContext.Current.Request.Url.ToString.ToLower
            If (HttpContext.Current.Request.Url.ToString.ToLower = DummyPageUrl.ToLower) Then

                'Add the item in cache and when succesful, do the work.

                'RegisterCacheEntry()

                RegisterCacheEntry2()

            End If
        End Sub




        Private Sub checkUnwantedParams()
            Try

                Dim paramInfo As String = ""

                Dim i, j As Integer

                ' Obtain a reference to the Request.Params
                ' collection.
                Dim pColl As NameValueCollection = Request.Params

                ' Iterate through the collection and add
                ' each key to the string variable.
                For i = 0 To pColl.Count - 1

                    paramInfo += "Key: " + pColl.GetKey(i) + "<br />"

                    If pColl.GetKey(i).Contains("dnn$") Then
                        Dim pValues As String = pColl.GetValues(i)(0)

                        If pValues.ToLower.StartsWith("invalid") Then
                            Response.Clear()
                            Response.Redirect("/Oops.aspx", True)
                        End If

                    End If

                Next i

            Catch ex As Exception
                'Dim execp As String = ex.Message
                'ProcessModuleLoadException(Nothing, ex)
            End Try
        End Sub





        Private Function RegisterCacheEntry2() As Boolean

            If Not HttpContext.Current.Cache(DummyCacheItemKey2) Is Nothing Then
                Return False
            Else

                HttpContext.Current.Cache.Add(DummyCacheItemKey2, "Test2", Nothing, DateTime.MaxValue, TimeSpan.FromSeconds(10), CacheItemPriority.Normal, New CacheItemRemovedCallback(AddressOf CacheItemRemovedCallback2))





                Return True

            End If

        End Function
















        Public Sub CacheItemRemovedCallback2(ByVal key As String, ByVal value As Object, ByVal reason As Caching.CacheItemRemovedReason)









            Try


                ICBO.ICUtilities.UpdateLog("Cache Callback2 Called." & IsProcessing)















                If IsProcessing = False Then





                    IsProcessing = True

                    ICSweepActionInstructionsController.AutoProcessAllSweepActionInstruction()
                    '' ''place your processing and disbursement method here
                    ICEODPendingInstructionController.EODAutoProcess()
                    ICCompanyController.DownloadAllMT940StatementsFromLocation()
                    '    ICInstructionProcessController.UpDateIBFTTransactionCount()
                    ' Auto Read File From Email
                    ICInstructionProcessController.AutoReadEmailsAndFileUpload()
                    ' Auto Read File From FTP
                    ICInstructionProcessController.AutoReadFTPAndFileUpload()
                    ' Process Email Uploaded Files
                    ICInstructionProcessController.AutoProcessFiles("Email File Upload")
                    ' Process FTP Uploaded Files
                    ICInstructionProcessController.AutoProcessFiles("FTP File Upload")

                    ' Stale Marking
                    ' Stale Marking
                    Try
                        ICInstructionProcessController.MarkInstructionsAsStaleAndMoveFundsToStaleAccount()
                        'Throw New Exception("Exception")
                    Catch ex As Exception
                        ICUtilities.SendErrorMessage(ex)
                    End Try
                    ' Standing Order AutoProcess

                    ICInstructionProcessController.ProcessStandingOrderInstructions()



                    '  ICInstructionProcessController.ReverseSameDayClearngApprovalTransactions()

                    IsProcessing = False





                End If



                HitPage2()



            Catch ex As Exception

                ICBO.ICUtilities.UpdateLog("ERROR: " & ex.ToString)

                IsProcessing = False

                HitPage2()






            End Try



        End Sub


        Private Sub HitPage2()

            Try


                Dim client As New WebClient




                client.DownloadData(DummyPageUrl)


            Catch ex As Exception


                ICBO.ICUtilities.UpdateLog("ERROR:" & ex.ToString)



            End Try


        End Sub




        'Private Function CallMethodAsync() As IAsyncResult



        '    Dim longm As LongRun = New LongRun(AddressOf IRISUtilities.ProcessIRISMessages) ' Delegate will call LongRunningMethod






        '    Dim ar As IAsyncResult = longm.BeginInvoke("", New AsyncCallback(AddressOf CallBackMethod), Nothing)

        '    '//�CHECK� will go as function parameter to LongRunningMethod
        '    Return ar     ' //Once LongRunningMethod get over CallBackMethod method will be invoked. 



        'End Function


        'Private Sub CallBackMethod(ByVal ar As IAsyncResult)


        '    Dim result As AsyncResult = CType(ar, AsyncResult)


        '    Dim longm As LongRun = CType(result.AsyncDelegate, LongRun)






        '    longm.EndInvoke(result)










        'End Sub










        Private Sub Global_PostAuthenticateRequest(sender As Object, e As System.EventArgs) Handles Me.PostAuthenticateRequest
            If Request.IsAuthenticated Then
                If Not HttpContext.Current.User Is Nothing Then
                    'update last activity date here
                    Dim ICUser As New ICBO.IC.ICUser
                    ICUser.es.Connection.CommandTimeout = 3600
                    ICUser.Query.Where(ICUser.Query.UserName = HttpContext.Current.User.Identity.Name)
                    If ICUser.Query.Load Then
                        ICUser.LastActivityDate = Date.Now
                        ICUser.Save()
                    End If
                End If
            End If
        End Sub
    End Class

End Namespace
