<%@ Control Language="vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Import Namespace="DotNetNuke.Services.Authentication" %>
<%@ Import Namespace="ICBO" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="NAV" Src="~/Admin/Skins/Nav.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<link href="<%= SkinPath %>css/styles.css" rel="stylesheet" type="text/css" />

<link href="<%= SkinPath %>menu.css" rel="stylesheet" type="text/css" />
<link href="<%= SkinPath %>jqueryui/jquery-ui-1.8.11.custom.css" rel="stylesheet"
    type="text/css" />
<script src="<%= SkinPath %>jqueryui/jquery-ui-1.8.11.custom.min.js" type="text/javascript"></script>
<script type="text/javascript">

    var autoclose = true;



    (function ($) {



        $(document).ready(function () {
            // This value can be true, false or a function to be used as a callback when the closer is clciked


            // Dialog			
            $('#dialog').dialog({
                autoOpen: false,
                width: 600,
                height: 120,
                modal: true,
                resizable: false



            });



            $(document).bind("idle.idleTimer", function () {

                showwindow();



            });

            $(document).bind("active.idleTimer", function () {





            });



            // correct the page







            function showwindow() {

                $.jGrowl("<span style='font-size:16px;'>Your session is about to expire in one minute. Please <a style='color:white;text-decoration:underline' onclick='closewindow();' href='javascript:void(0);'>Click Here</a> otherwise you will be logged out</span>", {
                    life: '60000',
                    speed: 'slow',
                    position: 'center',
                    closer: 'false',
                    closeTemplate: '',
                    beforeOpen: function (e, m, o) {

                    },
                    open: function (e, m, o) {

                    },
                    beforeClose: function (e, m, o) {

                    },
                    close: function (e, m, o) {

                        if (autoclose == true) {


                            window.location.href = 'https://cashlink.faysalbank.com:7780/ecustomer/Default.aspx?tabid=91&ctl=Logoff';




                        } else {


                            autoclose = true;



                        }




                    }
                });


            }




        });
    })(jQuery);


    function closewindow() {


        autoclose = false;



        $.jGrowl('close');





    }



</script>
<script runat="server">


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        If Request.IsAuthenticated Then

            Dim icUser2 As New ICBO.IC.ICUser
            icUser2.LoadByPrimaryKey(DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.UserID)



            If Not IsSecuredLogin(icUser2.UserName) Then
                Logoff()
                Response.Redirect(NavigateURL(PortalSettings.ActiveTab.TabID, "Login"))
            End If

            ' Me.lblLastLoginDateTime.Text = "Last Login : " & CDate(icUser2.LastLogInDate).ToString("dd-MMM-yyyy hh:mm:ss tt")
            Me.lblLastLoginDateTime.Text = "Last Login : " & cdate(GetUserLastLoginDateTime( icUser2.UserID.ToString,"Log On")).ToString("dd-MMM-yyyy hh:mm:ss tt")
            Me.lblwelcome.Text = "Welcome " & icUser2.DisplayName


            Me.linkpass.NavigateUrl = "~/ChangePassword.aspx"


            Me.linklogout.NavigateUrl = NavigateURL(PortalSettings.ActiveTab.TabID, "Logoff")


            Me.linkhome.NavigateUrl = NavigateURL(PortalSettings.HomeTabId)

            Me.hyperlinkHome.NavigateUrl = NavigateURL(PortalSettings.HomeTabId)



            If Page.IsPostBack = False Then



                Dim icUser As New ICBO.IC.ICUser
                icUser.LoadByPrimaryKey(DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.UserID)

                If Not icUser.AuthenticatedVia Is Nothing Then
                    If icUser.AuthenticatedVia.ToString() = "Active Directory" Then
                        linkpass.Enabled = False
                    End If
                End If
                'Dim ReplaceLogo As Boolean
                'Dim AgentType As String = ""
                'Dim AgentCode As String = ""
                'Dim frcUser As New Intelligenes.FRC.FrcUser
                'frcUser.es.Connection.CommandTimeout = 3600


                'If DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.IsSuperUser = True Then
                '    Exit Sub
                'End If

                'If DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.IsInRole("Acquisition Agent User") Then
                '    ReplaceLogo = True
                '    AgentType = "Acquisition Agent"
                '    If frcUser.LoadByPrimaryKey(DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.UserID) Then
                '        AgentCode = frcUser.AcqAgentCode
                '    End If
                'End If
                'If DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.IsInRole("Acquisition Agent Branch User") Then
                '    ReplaceLogo = True
                '    AgentType = "Acquisition Agent Branch"
                '    If frcUser.LoadByPrimaryKey(DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.UserID) Then
                '        AgentCode = frcUser.AcqAgentBranchID
                '    End If
                'End If
                'If DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.IsInRole("Acquisition Sub Agent User") Then
                '    ReplaceLogo = True
                '    AgentType = "Acquisition Sub Agent"
                '    If frcUser.LoadByPrimaryKey(DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.UserID) Then
                '        AgentCode = frcUser.AcqSubAgentID
                '    End If
                'End If
                'If DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.IsInRole("Acquisition Sub Agent Branch User") Then
                '    ReplaceLogo = True
                '    AgentType = "Acquisition Sub Agent Branch"
                '    If frcUser.LoadByPrimaryKey(DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.UserID) Then
                '        AgentCode = frcUser.AcqSubAgentBranchID
                '    End If
                'End If
                'If DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.IsInRole("Disbursement Agent User") Then
                '    ReplaceLogo = True
                '    AgentType = "Disbursement Agent"
                '    If frcUser.LoadByPrimaryKey(DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.UserID) Then
                '        AgentCode = frcUser.DisbAgentCode
                '    End If
                'End If
                'If DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.IsInRole("Disbursement Agent Branch User") Then
                '    ReplaceLogo = True
                '    AgentType = "Disbursement Agent Branch"
                '    If frcUser.LoadByPrimaryKey(DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.UserID) Then
                '        AgentCode = frcUser.DisbAgentBranchID
                '    End If
                'End If
                'If DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.IsInRole("Disbursement Sub Agent User") Then
                'ReplaceLogo = True
                '    AgentType = "Disbursement Sub Agent"
                '    If frcUser.LoadByPrimaryKey(DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.UserID) Then
                '        AgentCode = frcUser.DisbSubAgentID
                '    End If
                'End If
                'If DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.IsInRole("Disbursement Sub Agent Branch User") Then
                '    ReplaceLogo = True
                '    AgentType = "Disbursement Sub Agent Branch"
                '    If frcUser.LoadByPrimaryKey(DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.UserID) Then
                '        AgentCode = frcUser.DisbSubAgentBranchID
                '    End If
                'End If




                'If ReplaceLogo = True Then
                '    'get logo from agent code. if logo is defined then replace logo else make logo visible=false
                '    Me.hyperlinkHome.Visible = False
                '    Select Case AgentType.ToString()
                '        Case "Acquisition Agent"
                '            Dim acqAgent As New Intelligenes.FRC.FRCAcqAgent
                '            acqAgent.es.Connection.CommandTimeout = 3600

                '            acqAgent.LoadByPrimaryKey(AgentCode)
                '            If acqAgent.Logo = 0 Then
                '                Me.hyperlinkHome.Visible = True
                '                Me.tblFooter.Visible = True
                '            Else
                '                Me.hyperlinkHome.ImageUrl = ""
                '                Me.hyperlinkHome.Text = Intelligenes.FRCUtilities.ShowLogoFromDB(acqAgent.Logo, 235, 73)
                '                Me.hyperlinkHome.Visible = True
                '            End If
                '        Case "Acquisition Agent Branch"
                '            Dim acqAgent As New Intelligenes.FRC.FRCAcqAgent
                '            Dim acqAgentBr As New Intelligenes.FRC.FRCAcqAgentBranch

                '            acqAgent.es.Connection.CommandTimeout = 3600
                '            acqAgentbr.es.Connection.CommandTimeout = 3600

                '            acqAgentBr.LoadByPrimaryKey(AgentCode)
                '            acqAgent.LoadByPrimaryKey(acqAgentBr.AcqAgentID)
                '            If acqAgent.Logo = 0 Then
                '                Me.hyperlinkHome.Visible = True
                '                Me.tblFooter.Visible = True
                '            Else
                '                Me.hyperlinkHome.ImageUrl = ""
                '                Me.hyperlinkHome.Text = Intelligenes.FRCUtilities.ShowLogoFromDB(acqAgent.Logo, 235, 73)
                '                Me.hyperlinkHome.Visible = True
                '            End If
                '        Case "Acquisition Sub Agent"
                '            Dim acqAgent As New Intelligenes.FRC.FRCAcqAgent
                '            Dim acqSubAgent As New Intelligenes.FRC.FRCAcqSubAgent

                '            acqAgent.es.Connection.CommandTimeout = 3600
                '            acqSubAgent.es.Connection.CommandTimeout = 3600


                '            acqSubAgent.LoadByPrimaryKey(AgentCode)
                '            acqAgent.LoadByPrimaryKey(acqSubAgent.AcqAgentID)
                '            If acqAgent.Logo = 0 Then
                '                Me.hyperlinkHome.Visible = True
                '                Me.tblFooter.Visible = True
                '            Else
                '                Me.hyperlinkHome.ImageUrl = ""
                '                Me.hyperlinkHome.Text = Intelligenes.FRCUtilities.ShowLogoFromDB(acqAgent.Logo, 235, 73)
                '                Me.hyperlinkHome.Visible = True
                '            End If
                '        Case "Acquisition Sub Agent Branch"
                '            Dim acqAgent As New Intelligenes.FRC.FRCAcqAgent
                '            Dim acqSubAgent As New Intelligenes.FRC.FRCAcqSubAgent
                '            Dim acqSubAgentBr As New Intelligenes.FRC.FRCAcqSubAgentBranch


                '            acqAgent.es.Connection.CommandTimeout = 3600
                '            acqSubAgent.es.Connection.CommandTimeout = 3600
                '            acqSubAgentBr.es.Connection.CommandTimeout = 3600


                '            acqSubAgentBr.LoadByPrimaryKey(AgentCode)
                '            acqSubAgent.LoadByPrimaryKey(acqSubAgentBr.AcqSubAgentID)
                '            acqAgent.LoadByPrimaryKey(acqSubAgent.AcqAgentID)
                '            If acqAgent.Logo = 0 Then
                '                Me.hyperlinkHome.Visible = True
                '                Me.tblFooter.Visible = True
                '            Else
                '                Me.hyperlinkHome.ImageUrl = ""
                '                Me.hyperlinkHome.Text = Intelligenes.FRCUtilities.ShowLogoFromDB(acqAgent.Logo, 235, 73)
                '                Me.hyperlinkHome.Visible = True
                '            End If
                '        Case "Disbursement Agent"
                '            Dim disbAgent As New Intelligenes.FRC.FRCDisbAgent
                '            disbAgent.es.Connection.CommandTimeout = 3600
                '            disbAgent.LoadByPrimaryKey(AgentCode)
                '            disbAgent.LoadByPrimaryKey(AgentCode)
                '            If disbAgent.Logo = 0 Then
                '                Me.hyperlinkHome.Visible = True
                '                Me.tblFooter.Visible = True
                '            Else
                '                Me.hyperlinkHome.ImageUrl = ""
                '                Me.hyperlinkHome.Text = Intelligenes.FRCUtilities.ShowLogoFromDB(disbAgent.Logo, 235, 73)
                '                Me.hyperlinkHome.Visible = True
                '            End If
                '        Case "Disbursement Agent Branch"
                '            Dim disbAgent As New Intelligenes.FRC.FRCDisbAgent
                '            Dim disbAgentBr As New Intelligenes.FRC.FRCDisbAgentBranch


                '            disbAgent.es.Connection.CommandTimeout = 3600
                '            disbAgentBr.es.Connection.CommandTimeout = 3600

                '            disbAgentBr.LoadByPrimaryKey(AgentCode)
                '            disbAgent.LoadByPrimaryKey(disbAgentBr.DisbAgentID)
                '            If disbAgent.Logo = 0 Then
                '                Me.hyperlinkHome.Visible = True
                '                Me.tblFooter.Visible = True
                '            Else
                '                Me.hyperlinkHome.ImageUrl = ""
                '                Me.hyperlinkHome.Text = Intelligenes.FRCUtilities.ShowLogoFromDB(disbAgent.Logo, 235, 73)
                '                Me.hyperlinkHome.Visible = True
                '            End If
                '        Case "Disbursement Sub Agent"
                '            Dim disbAgent As New Intelligenes.FRC.FRCDisbAgent
                '            Dim disbSubAgent As New Intelligenes.FRC.FRCDisbSubAgent

                '            disbAgent.es.Connection.CommandTimeout = 3600
                '            disbSubAgent.es.Connection.CommandTimeout = 3600

                '            disbSubAgent.LoadByPrimaryKey(AgentCode)
                '            disbAgent.LoadByPrimaryKey(disbSubAgent.DisbAgentID)
                '            If disbAgent.Logo = 0 Then
                '                Me.hyperlinkHome.Visible = True
                '                Me.tblFooter.Visible = True
                '            Else
                '                Me.hyperlinkHome.ImageUrl = ""
                '                Me.hyperlinkHome.Text = Intelligenes.FRCUtilities.ShowLogoFromDB(disbAgent.Logo, 235, 73)
                '                Me.hyperlinkHome.Visible = True
                '            End If
                '        Case "Disbursement Sub Agent Branch"
                '            Dim disbAgent As New Intelligenes.FRC.FRCDisbAgent
                '            Dim disbSubAgent As New Intelligenes.FRC.FRCDisbSubAgent
                '            Dim disbSubAgentBr As New Intelligenes.FRC.FRCDisbSubAgentBranch

                '            disbAgent.es.Connection.CommandTimeout = 3600
                '            disbSubAgent.es.Connection.CommandTimeout = 3600
                '            disbsubAgentbr.es.Connection.CommandTimeout = 3600



                '            disbSubAgentBr.LoadByPrimaryKey(AgentCode)
                '            disbSubAgent.LoadByPrimaryKey(disbSubAgentBr.DisbSubAgentID)
                '            disbAgent.LoadByPrimaryKey(disbSubAgent.DisbAgentID)
                '            If disbAgent.Logo = 0 Then
                '                Me.hyperlinkHome.Visible = True
                '                Me.tblFooter.Visible = True
                '            Else
                '                Me.hyperlinkHome.ImageUrl = ""
                '                Me.hyperlinkHome.Text = Intelligenes.FRCUtilities.ShowLogoFromDB(disbAgent.Logo, 235, 73)
                '                Me.hyperlinkHome.Visible = True
                '            End If
                '        Case ""
                '            Me.hyperlinkHome.Visible = True
                '            Me.tblFooter.Visible = True
                '    End Select





                'End If





            End If





            '  Dim scr As String

            '   scr = "window.onload = function () {var timeout = 300000;$.idleTimer(timeout);};"


            '  Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "scr", scr, True)





            ' Me.litscr.Text = scr


        Else

            Me.tblwelcome.Visible = False
            Me.hyperlinkHome.NavigateUrl = NavigateURL(80)
            '  Me.hyperlinkHome.Visible = False
            Me.tblFooter.Visible = False
        End If
    End Sub

    Private Function IsSecuredLogin(ByRef UserName As String) As Boolean


        If Session("AuthToken") Is Nothing Then
            Return False
        End If

        If Request.Cookies("AuthToken") Is Nothing Then
            Return False
        End If

        If Not Session("AuthToken").ToString().Equals(Request.Cookies("AuthToken").Value) Then
            Return False
        End If

        Dim AuthToken As String = Session("AuthToken").ToString().DecryptString()

        If Not AuthToken.ToLower().Equals(UserName.ToLower()) Then
            Return False
        End If

        Return True
    End Function

    Private Function GetUserLastLoginDateTime(ByVal UserID As String, ByVal AuditType As String) As String
        Dim objICATrailQuery As New icbo.ic.ICAuditTrailQuery("objICATrailQuery")
        Dim objICATrailSubQuery As New icbo.ic.ICAuditTrailQuery("objICATrailSubQuery")
        Dim dt As New DataTable
        objICATrailQuery.Select(objICATrailQuery.AuditDate)
        objICATrailQuery.Where(objICATrailQuery.AuditID.In(objICATrailSubQuery.[Select](objICATrailSubQuery.AuditID).Where(objICATrailSubQuery.RelatedID = UserID And objICATrailSubQuery.AuditType = AuditType).OrderBy(objICATrailSubQuery.AuditID.Descending)))

        objICATrailQuery.OrderBy(objICATrailQuery.AuditID.Ascending)
        objICATrailSubQuery.es.Top = 2
        objICATrailQuery.es.Top = 1
        dt = objICATrailQuery.LoadDataTable
        If dt.Rows.Count > 0 Then
            Return dt(0)(0).ToString
        Else
            Return ""
        End If

    End Function




    Private Sub Redirect()
        ' Redirect browser back to portal 
        '  Response.Redirect(AuthenticationController.GetLogoffRedirectURL(PortalSettings, Request), True)

        Response.Redirect(NavigateURL(PortalSettings.LoginTabId))


    End Sub
    Private Sub Logoff()
        Try
            'Remove user from cache
            If DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo IsNot Nothing Then
                DataCache.ClearUserCache(Me.PortalSettings.PortalId, Context.User.Identity.Name)
            End If


            'add audit log function call here

            'DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.userid





            Dim objICUser As New ICBO.IC.ICUser
            If objICUser.LoadByPrimaryKey(DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.UserID.ToString()) Then
                objICUser.LastLogInDate = Now
                objICUser.LastActivityDate = Now
                objICUser.LastIPAddress = Request.UserHostAddress
                objICUser.IsLoggedIn = False
                objICUser.Save()
            End If

            ICBO.EmailUtilities.SendEmailOfLogOut( objICUser)

            ICBO.ICUtilities.AddAuditTrail("Log Off: " & DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.Username & " Log out.", "Log Off", DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.UserID.ToString(), DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.UserID.ToString(), DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.Username.ToString(), "LOG OFF")

            Dim objPortalSecurity As New PortalSecurity

            objPortalSecurity.SignOut()

            Session.Clear()
            Session.Abandon()
            If (Not Request.Cookies("ASP.NET_SessionId") Is Nothing) Then
                Dim myCookie As HttpCookie
                myCookie = New HttpCookie("ASP.NET_SessionId")
                myCookie.Expires = DateTime.Now.AddDays(-1)
                Response.Cookies.Add(myCookie)
            End If
            If (Not Request.Cookies(".DOTNETNUKE") Is Nothing) Then
                Dim myCookie As HttpCookie
                myCookie = New HttpCookie(".DOTNETNUKE")
                myCookie.Expires = DateTime.Now.AddDays(-1)
                Response.Cookies.Add(myCookie)
            End If

            If (Not Request.Cookies(".ASPXANONYMOUS") Is Nothing) Then
                Dim myCookie As HttpCookie
                myCookie = New HttpCookie(".ASPXANONYMOUS")
                myCookie.Expires = DateTime.Now.AddDays(-1)
                Response.Cookies.Add(myCookie)
            End If

            If (Not Request.Cookies("AuthToken") Is Nothing) Then
                Dim myCookie As HttpCookie
                myCookie = New HttpCookie("AuthToken")
                myCookie.Value = String.Empty
                myCookie.Expires = DateTime.Now.AddDays(-1)
                Response.Cookies.Add(myCookie)
            End If

        Catch exc As Exception    'Page failed to load
            ProcessPageLoadException(exc)
        End Try
    End Sub
    'Private Sub Logoff()
    '    Try
    '        'Remove user from cache
    '        If DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo IsNot Nothing Then
    '            DataCache.ClearUserCache(Me.PortalSettings.PortalId, Context.User.Identity.Name)
    '        End If


    '        'add audit log function call here

    '        'DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.userid


    '        ICBO.ICUtilities.AddAuditTrail("Log Off: " & DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.Username & " Log out.", "Log Off", DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.UserID.ToString(), DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.UserID.ToString(), DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo.Username.ToString(),"LOG OFF")



    '        Dim objPortalSecurity As New PortalSecurity

    '        objPortalSecurity.SignOut()

    '    Catch exc As Exception    'Page failed to load
    '        ProcessPageLoadException(exc)
    '    End Try
    'End Sub

    Protected Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Logoff()

        Redirect()


    End Sub
</script>

<div class="divmain" style="width:100%;min-width:1000px;">
    <table width="100%" cellpadding="0" cellspacing="0" class="bgheader">
        <tr>
            <td>
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td valign="bottom" height="90">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left" style="padding-left:5px;">
                                        <asp:HyperLink ID="hyperlinkHome" runat="server" ImageUrl="~/Portals/_default/Skins/IC/image/albrakalogo.png"
                                             ></asp:HyperLink>
                                    </td>
                                    <td align="right" height="83">
                                        
                                        

                                        <asp:Image id="imglogo" runat="server"  ImageUrl="~/Portals/_default/Skins/IC/image/logo.png" Visible="false"/>

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width:100%; margin-top: 0px;border-spacing:0px;">
                                <tr>
                                    <td style="height: 45px;background-color: #e31937;">
                                        <dnn:NAV ID="NAV1" runat="server" CSSControl="mainMenu" ControlOrientation="Horizontal"
                                            IndicateChildren="True" ProviderName="DNNMenuNavigationProvider"  />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="width: 100%;padding-top:3px;" runat="server" id='tblwelcome' >
                                            <tr>
                                                <td width="600" >
                                                    <asp:Label ID="lblwelcome" runat="server" Font-Size="12px" CssClass="headingblue2" ForeColor="White" ></asp:Label>
                                                    &nbsp;&nbsp;
                                                    <asp:Label ID="lblLastLoginDateTime" runat="server" ForeColor="White"  ></asp:Label>
                                                </td>
                                                <td align="right">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:HyperLink ID="linkhome" runat="server" CssClass="lblHomeLogOut" ForeColor="White">Home</asp:HyperLink>
                                                            </td>
                                                            <td class="headingblue">
                                                                <asp:Label ID="Label2" runat="server" Text="|" CssClass="lblHomeLogOut" ForeColor="White"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:HyperLink ID="linkpass" runat="server" CssClass="lblHomeLogOut" ForeColor="White">Change Password</asp:HyperLink>
                                                            </td>
                                                            <td class="headingblue">
                                                                <asp:Label ID="Label1" runat="server" Text="|" CssClass="lblHomeLogOut" ForeColor="White"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:HyperLink ID="linklogout" runat="server" CssClass="lblHomeLogOut" Visible="False" ForeColor="White">Logout</asp:HyperLink>
                                                                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" CausesValidation="False"
                                                                    CssClass="lblHomeLogOut" ForeColor="White">Logout</asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" height="400">
                            <table style="width: 100%;">
                                <tr>
                                    <td runat="server" id="LeftPane" visible="false" width="250" valign="top">
                                    </td>
                                    <td runat="server" id="ContentPane" visible="false" width="100%" valign="top">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" class="footermain">
        <tr>
            <td>
                <table id="tblFooter" runat="server" width="100%" border="0" align="center" cellpadding="0"
                    cellspacing="0">
                    <tr>
                        <td class="footer">
                          <%--  <a href="http://faysalbank.com/privacy.html" target="_blank">Privacy Statement</a>
                            | <a href="http://faysalbank.com/disclaimer.html" target="_blank">Disclaimer</a>--%>
                        </td>
                        <td class="copyright" align="right">
                            All Rights Reserved
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<asp:Literal ID="litscr" runat="server"></asp:Literal>
<!-- ui-dialog -->
<div id="dialog" title="Dialog Title" style="display:none;">
    <table style="width: 100%; border: 0px; margin-top: 5px;">
        <tr>
            <td width="60">
                <div id="divimg" style="width: 60px;">
                </div>
            </td>
            <td>
                <div id="divmsg">
                </div>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <a href="javascript:void(0);" onclick="$('#dialog').dialog('close');">
                    <img src='<%=SkinPath %>image/btn-ok.gif' /></a>
            </td>
        </tr>
    </table>
</div>
