﻿Imports ICBO
Imports ICBO.IC
Imports System.Web.UI
Partial Class ShowSiignatures
    'Inherits System.Web.UI.Page
    Inherits DotNetNuke.Framework.CDefault

    Private AccountNumber As String
    Private BranchCode As String
    Private Currency As String
    Private InstructionID As String

    Protected Sub ShowSiignatures_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Dim isValid As Boolean = True

        Dim objUserInfo As UserInfo = UserController.GetCurrentUserInfo

        If Session("AuthToken") Is Nothing Or Request.QueryString("an") Is Nothing Or objUserInfo.UserID = -1 Then
            isValid = False
        End If

        If isValid Then
            isValid = ICUtilities.AuthenticateRequest(Request, Session("AuthToken"))
        End If

        If Not isValid Then
            tblDetails.Visible = False
            Exit Sub
        End If



        InstructionID = Request.QueryString("an").ToString

        If InstructionID.IsValidBase64String() = False Then
            Exit Sub
        End If

        If isValidNumber(InstructionID) = True Then
            Exit Sub
        End If

        InstructionID = InstructionID.DecryptString()

        Try
            If Page.IsPostBack = False Then
                'lblPageHeader.Text = "Approvers of Account Number: [" & AccountNumber.ToString() & "] ; Branch Code: [" & BranchCode.ToString() & "] ; Currency: [" & Currency.ToString() & "]"
                lblPageHeader.Text = "Approvers of Instruction No. [ " & InstructionID & " ]"
                SetSetgvApprovers(InstructionID)
                SetImages()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Public Shared Function isValidNumber(number As String) As Boolean

        Try
            Convert.ToInt64(number)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    'Private Sub SetSetgvApprovers(ByVal InstructionID As String)
    '    Dim dt As New DataTable
    '    Dim objICInstruction As New ICInstruction
    '    Dim ArryListUserIDS As New ArrayList
    '    If objICInstruction.LoadByPrimaryKey(InstructionID) Then
    '        If objICInstruction.PaymentMode = "Cheque" Then
    '            ArryListUserIDS.Add(objICInstruction.ClientPrimaryApprovedBy)
    '            ArryListUserIDS.Add(objICInstruction.ClientSecondaryApprovedBy)
    '        ElseIf objICInstruction.PaymentMode = "PO" Or objICInstruction.PaymentMode = "DD" Then
    '            ArryListUserIDS.Add(objICInstruction.BankPrimaryApprovedBy)
    '            ArryListUserIDS.Add(objICInstruction.BankSecondaryApprovedBy)
    '        End If

    '        dt = ICUserLimitsController.GetSignaturesOfAccountByApproverIDs(ArryListUserIDS)
    '        gvAppprovers.DataSource = dt
    '        gvAppprovers.DataBind()
    '    End If





    'End Sub
    Private Sub SetSetgvApprovers(ByVal InstructionID As String)
        Dim dt As New DataTable
        Dim collObjICApprovalLog As New ICInstructionApprovalLogCollection
        Dim objICInstruction As New ICInstruction
        Dim ArryListUserIDS As New ArrayList
        objICInstruction.LoadByPrimaryKey(InstructionID)

       


        If objICInstruction.PaymentMode = "Cheque" Then
            collObjICApprovalLog.Query.Where(collObjICApprovalLog.Query.InstructionID = InstructionID And collObjICApprovalLog.Query.IsAprrovalVoid = False)
            collObjICApprovalLog.Query.OrderBy(collObjICApprovalLog.Query.UserApprovalLogID.Ascending)
            If collObjICApprovalLog.Query.Load Then
                For Each objICAppLog As ICInstructionApprovalLog In collObjICApprovalLog
                    ArryListUserIDS.Add(objICAppLog.UserID)
                Next
                dt = ICUserLimitsController.GetSignaturesOfAccountByApproverIDs(ArryListUserIDS)
                gvAppprovers.DataSource = dt
                gvAppprovers.DataBind()
            End If
        ElseIf objICInstruction.PaymentMode = "PO" Or objICInstruction.PaymentMode = "DD" Then
            ArryListUserIDS.Add(objICInstruction.BankPrimaryApprovedBy)
            ArryListUserIDS.Add(objICInstruction.BankSecondaryApprovedBy)
            dt = ICUserLimitsController.GetSignaturesOfAccountByApproverIDs(ArryListUserIDS)
            gvAppprovers.DataSource = dt
            gvAppprovers.DataBind()
        End If

        






    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Page.ClientScript.RegisterClientScriptBlock(Page.GetType, Page.ClientID, "window.onload = function () {parent.CloseIframe();    }", True)
    End Sub
    Private Sub SetImages()
        Dim row As Telerik.Web.UI.GridItem
        For Each row In gvAppprovers.Items
            Dim img As New WebControls.Image
            Dim AppPath As String = "" ' = DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
            img = DirectCast(row.FindControl("imgSignature"), WebControls.Image)
            img.ImageUrl = "/ShowimageFromDB.ashx?id=" & ICUserLimitsController.GetApprovalLimitIDByUserID(gvAppprovers.Items(row.ItemIndex).GetDataKeyValue("UserID").ToString()).ToString & "&UserType=Approval Limits User"
        Next
    End Sub
    
End Class
