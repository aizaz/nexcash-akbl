﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports Telerik.Web.UI
Partial Class DesktopModules_FixUserManagement_SaveUsersInBulk
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private UserCode As String


#Region "Page Load"
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If Not Me.UserInfo.IsSuperUser = True Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL())
                Exit Sub
            End If
            If Page.IsPostBack = False Then
                lblPageHeader.Text = "Add Bank Users In Bulk"
                gvUsers.DataSource = Nothing
                gvUserStatus.DataSource = Nothing
                gvUsers.Visible = False
                gvUserStatus.Visible = False
                btnSave.Text = "Save"
                btnSave.Visible = False
                btnProceed.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
#Region "Button Events"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim dtStatus As New DataTable
        Dim drStatus As DataRow
        Dim Response As String = ""
        Dim rowGVICRoles As GridDataItem
        dtStatus.Columns.Add(New DataColumn("Status", GetType(System.String)))
        dtStatus.Columns.Add(New DataColumn("UserName", GetType(System.String)))
        dtStatus.Columns.Add(New DataColumn("AuthenticatedVia", GetType(System.String)))
        dtStatus.Columns.Add(New DataColumn("DisplayName", GetType(System.String)))
        dtStatus.Columns.Add(New DataColumn("Email", GetType(System.String)))
        Page.Validate()
        If Page.IsValid Then


            Try
                If gvUsers.Items.Count = 0 Then
                    UIUtilities.ShowDialog(Me, "Error", "No users exists", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    Exit Sub
                End If
                If IsValidRecordExists() = False Then
                    UIUtilities.ShowDialog(Me, "Error", "No valid users exists", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVICRoles In gvUsers.Items
                    If rowGVICRoles.Item("ErrorMessage").Text.ToString = "OK" Then
                        drStatus = dtStatus.NewRow()
                        Try

                            Response = AddDNNUser(False, rowGVICRoles)
                            If Response = "OK" Then
                                drStatus(0) = "User added"
                                drStatus(1) = rowGVICRoles.Item("UserName").Text.ToString
                                drStatus(2) = rowGVICRoles.Item("AuthenticatedVia").Text.ToString
                                drStatus(3) = rowGVICRoles.Item("DisplayName").Text.ToString
                                drStatus(4) = rowGVICRoles.Item("Email").Text.ToString
                                dtStatus.Rows.Add(drStatus)
                            Else
                                drStatus(0) = Response
                                drStatus(1) = rowGVICRoles.Item("UserName").Text.ToString
                                drStatus(2) = rowGVICRoles.Item("AuthenticatedVia").Text.ToString
                                drStatus(3) = rowGVICRoles.Item("DisplayName").Text.ToString
                                drStatus(4) = rowGVICRoles.Item("Email").Text.ToString
                                dtStatus.Rows.Add(drStatus)
                            End If
                        Catch ex As Exception
                            drStatus(0) = ex.Message.ToString
                            drStatus(1) = rowGVICRoles.Item("UserName").Text.ToString
                            drStatus(2) = rowGVICRoles.Item("AuthenticatedVia").Text.ToString
                            drStatus(3) = rowGVICRoles.Item("DisplayName").Text.ToString
                            drStatus(4) = rowGVICRoles.Item("Email").Text.ToString
                            dtStatus.Rows.Add(drStatus)
                        End Try
                    End If
                Next
                btnSave.Visible = False
                gvUsers.DataSource = Nothing
                gvUsers.Visible = False
                gvUserStatus.Visible = True
                gvUserStatus.DataSource = Nothing
                gvUserStatus.DataSource = dtStatus
                gvUserStatus.DataBind()

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Function AddDNNUser(ByVal IsUpdated As Boolean, ByVal rowgvDataItem As GridDataItem) As String
        Dim Result As String = "OK"
        Try
            Dim Roles As String() = Nothing
            Dim RoleID As String = Nothing
            Dim HashedPassword As String = ""
            Dim objICUSer As ICUser
            Dim objFromICUser As ICUser

            Dim user As New UserInfo
            Dim objICUserRoles As ICUserRoles
            user.PortalID = Me.PortalId
            user.IsSuperUser = False
            user.FirstName = rowgvDataItem.Item("DisplayName").Text.ToString
            user.LastName = ""
            user.Email = rowgvDataItem.Item("DisplayName").Text.ToString
            user.Username = rowgvDataItem.Item("UserName").Text.ToString
            user.DisplayName = rowgvDataItem.Item("DisplayName").Text.ToString
            user.Profile.PreferredLocale = PortalSettings.DefaultLanguage
            user.Profile.TimeZone = PortalSettings.TimeZoneOffset
            user.Profile.FirstName = rowgvDataItem.Item("DisplayName").Text.ToString
            user.Profile.LastName = ""

            Dim member As New UserMembership
            member.Approved = False
            member.CreatedDate = Now
            member.Email = user.Email
            member.IsOnLine = False
            member.Username = user.Username
            member.Password = GetRandomPassword()
            user.Membership = member

            Dim status As DotNetNuke.Security.Membership.UserCreateStatus
            status = UserController.CreateUser(user)
            ' Add new password in password history table
            HashedPassword = ICUserPasswordHistoryController.GenerateMD5Hash(member.Password.ToString().Trim())
            HashedPassword = HashedPassword.Trim.ToLower.ToString()
            Dim objICUserPWDHistory As New ICUserPasswordHistory
            objICUserPWDHistory.UserID = user.UserID
            objICUserPWDHistory.Password = HashedPassword
            objICUserPWDHistory.CreatedDate = Date.Now
            objICUserPWDHistory.CreatedBy = Me.UserId
            ICUserPasswordHistoryController.AddICUserPasswordHistory(objICUserPWDHistory)
            If status = DotNetNuke.Security.Membership.UserCreateStatus.Success Then
                'user created
                Try
                    objICUSer = New ICUser
                    objICUSer.es.Connection.CommandTimeout = 3600
                    objICUSer.UserID = user.UserID
                    objICUSer.UserName = rowgvDataItem.Item("UserName").Text.ToString
                    objICUSer.DisplayName = rowgvDataItem.Item("DisplayName").Text.ToString
                    objICUSer.Password = member.Password.ToString()
                    objICUSer.Email = rowgvDataItem.Item("Email").Text.ToString
                    objICUSer.CellNo = rowgvDataItem.Item("CellNo").Text.ToString
                    objICUSer.PhoneNo1 = rowgvDataItem.Item("PhoneNo1").Text.ToString
                    objICUSer.PhoneNo2 = rowgvDataItem.Item("PhoneNo2").Text.ToString
                    objICUSer.IsActive = True
                    objICUSer.Location = rowgvDataItem.Item("Location").Text.ToString
                    objICUSer.Department = rowgvDataItem.Item("Department").Text.ToString
                    objICUSer.OfficeCode = CInt(rowgvDataItem.GetDataKeyValue("OfficeID"))
                    objICUSer.AuthenticatedVia = rowgvDataItem.Item("AuthenticatedVia").Text.ToString
                    objICUSer.EmployeeCode = rowgvDataItem.Item("EmployeeCode").Text.ToString
                    objICUSer.CreateBy = Me.UserId
                    objICUSer.CreateDate = Date.Now
                    objICUSer.Creater = Me.UserId
                    objICUSer.CreationDate = Date.Now
                    objICUSer.UserType = "Bank User"
                    ICUserController.AddICUser(objICUSer, False, Me.UserId.ToString, Me.UserInfo.Username.ToString, "", "Bank User")
                    ICUserController.ApproveUser(objICUSer.UserID.ToString, "2", True, "Admin", "Bank User")
                    AuthorizeUser(objICUSer.UserID.ToString)
                    If rowgvDataItem.Item("RoleName").Text.ToString <> "" Then
                        If rowgvDataItem.Item("RoleName").Text.ToString.Contains(",") Then
                            Roles = rowgvDataItem.Item("RoleName").Text.ToString.Split(",")
                            For j = 0 To Roles.Length - 1
                                RoleID = Nothing
                                RoleID = GetRoleIDByRoleName(Roles(j).ToString)
                                If Not RoleID = 0 Then
                                    If ICUserRolesController.CheckRoleAlReadyAssignToUser(objICUSer.UserID.ToString, RoleID) = False Then
                                        objICUserRoles = New ICUserRoles
                                        Dim objICUserForRole As New ICUser
                                        Dim i As Integer = 0
                                        Dim objUserInfo As New UserInfo
                                        Dim objRoleInfo As New RoleInfo
                                        Dim roleCtrl As New RoleController
                                        objICUserRoles.es.Connection.CommandTimeout = 3600
                                        objICUserForRole.es.Connection.CommandTimeout = 3600
                                        objICUserForRole.LoadByPrimaryKey(objICUSer.UserID.ToString)
                                        objICUserRoles.RoleID = RoleID
                                        objICUserRoles.UserID = objICUSer.UserID.ToString
                                        objICUserRoles.AssignedBy = Me.UserId
                                        objICUserRoles.AssignedOn = Date.Now
                                        ICUserRolesController.AddICUserRoles(objICUserRoles, False, Me.UserId.ToString, Me.UserInfo.Username.ToString, objICUserRoles.UpToICUserByUserID.UserType.ToString, objICUserRoles.UpToICRoleByRoleID.RoleType)
                                        objICUserRoles = New ICUserRoles
                                        objICUserRoles.LoadByPrimaryKey(objICUSer.UserID.ToString, RoleID)
                                        If objICUserRoles.IsApproved = False Then
                                            ICUserRolesController.ApproveICUserRoles(objICUserRoles.RoleID, objICUSer.UserID.ToString, True, "2", "Admin", objICUserRoles.UpToICUserByUserID.UserType.ToString, objICUserRoles.UpToICRoleByRoleID.RoleType.ToString)
                                            ICUserRolesController.AssignRoleToUser(RoleID.ToString(), Me.PortalId, objICUSer.UserID.ToString)
                                            objICUSer = New ICUser
                                            objFromICUser = New ICUser
                                            objICUSer.LoadByPrimaryKey(objICUserRoles.UserID.ToString)
                                            objFromICUser.LoadByPrimaryKey(objICUserRoles.UserID.ToString)
                                            If Not objICUSer.IsNotificationSent = True Or objICUSer.IsNotificationSent Is Nothing Then
                                                Dim password As String = ""
                                                Dim password2 As String = ""
                                                password2 = DotNetNuke.Entities.Users.UserController.GetPassword(user, password)
                                                EmailUtilities.UserCreated(objICUSer, password2)
                                                SMSUtilities.UserCreated(objICUSer, password2)
                                                objICUSer.IsNotificationSent = True
                                                objICUSer.Save()
                                                Dim Action As String = ""
                                                Action = "Bank User [ " & objICUSer.UserName & " ] [ " & objICUSer.DisplayName & " ] Updated."
                                                Action += "Notification sent from[" & objFromICUser.IsNotificationSent & "] to [" & objICUSer.IsNotificationSent & "] ; "
                                                ICUtilities.AddAuditTrail(Action, "Bank User", objICUSer.UserID.ToString, "2", "Admin", "UPDATE")
                                            End If
                                        End If
                                    End If
                                End If
                            Next
                        Else
                            RoleID = Nothing
                            RoleID = GetRoleIDByRoleName(rowgvDataItem.Item("RoleName").Text.ToString)
                            If Not RoleID = 0 Then
                                If ICUserRolesController.CheckRoleAlReadyAssignToUser(objICUSer.UserID.ToString, RoleID) = False Then
                                    objICUserRoles = New ICUserRoles
                                    Dim objICUserForRole As New ICUser
                                    Dim i As Integer = 0
                                    Dim objUserInfo As New UserInfo
                                    Dim objRoleInfo As New RoleInfo
                                    Dim roleCtrl As New RoleController
                                    objICUserRoles.es.Connection.CommandTimeout = 3600
                                    objICUserForRole.es.Connection.CommandTimeout = 3600
                                    objICUserForRole.LoadByPrimaryKey(objICUSer.UserID.ToString)
                                    objICUserRoles.RoleID = RoleID
                                    objICUserRoles.UserID = objICUSer.UserID.ToString
                                    objICUserRoles.AssignedBy = Me.UserId
                                    objICUserRoles.AssignedOn = Date.Now
                                    ICUserRolesController.AddICUserRoles(objICUserRoles, False, Me.UserId.ToString, Me.UserInfo.Username.ToString, objICUserRoles.UpToICUserByUserID.UserType.ToString, objICUserRoles.UpToICRoleByRoleID.RoleType)
                                    objICUserRoles = New ICUserRoles
                                    objICUserRoles.LoadByPrimaryKey(objICUSer.UserID.ToString, RoleID)
                                    If objICUserRoles.IsApproved = False Then
                                        ICUserRolesController.ApproveICUserRoles(objICUserRoles.RoleID, objICUSer.UserID.ToString, True, "2", "Admin", objICUserRoles.UpToICUserByUserID.UserType.ToString, objICUserRoles.UpToICRoleByRoleID.RoleType.ToString)
                                        ICUserRolesController.AssignRoleToUser(RoleID.ToString(), Me.PortalId, objICUSer.UserID.ToString)
                                        objICUSer = New ICUser
                                        objFromICUser = New ICUser
                                        objICUSer.LoadByPrimaryKey(objICUserRoles.UserID.ToString)
                                        objFromICUser.LoadByPrimaryKey(objICUserRoles.UserID.ToString)
                                        If Not objICUSer.IsNotificationSent = True Or objICUSer.IsNotificationSent Is Nothing Then
                                            Dim password As String = ""
                                            Dim password2 As String = ""
                                            password2 = DotNetNuke.Entities.Users.UserController.GetPassword(user, password)
                                            EmailUtilities.UserCreated(objICUSer, password2)
                                            SMSUtilities.UserCreated(objICUSer, password2)
                                            objICUSer.IsNotificationSent = True
                                            objICUSer.Save()
                                            Dim Action As String = ""
                                            Action = "Bank User [ " & objICUSer.UserName & " ] [ " & objICUSer.DisplayName & " ] Updated."
                                            Action += "Notification sent from[" & objFromICUser.IsNotificationSent & "] to [" & objICUSer.IsNotificationSent & "] ; "
                                            ICUtilities.AddAuditTrail(Action, "Bank User", objICUSer.UserID.ToString, "2", "Admin", "UPDATE")
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                    Return Result
                Catch ex As Exception
                    ProcessModuleLoadException(Me, ex, False)
                    Result = ""
                    Result = ex.Message.ToString
                    Return Result
                End Try
            Else
                Select Case status
                    Case DotNetNuke.Security.Membership.UserCreateStatus.UsernameAlreadyExists
                        Result = ""
                        Result = "User alreday exists."
                        'UIUtilities.ShowDialog(Me, "Registration", "User Name is already registered with us.", Dialogmessagetype.Failure, NavigateURL())
                        'UI.Skins.Skin.AddModuleMessage(Me, "Your card is already registered with us.", Skins.Controls.ModuleMessage.ModuleMessageType.RedError)
                        ' Me.lblmsg.Text = "Your email address is already registered with us."
                    Case DotNetNuke.Security.Membership.UserCreateStatus.DuplicateEmail
                        'Me.lblmsg.Text = "Your email address is already registered with us."
                        Result = ""
                        Result = "Your email address is already registered with us."
                        'UIUtilities.ShowDialog(Me, "Registration", "Your email address is already registered with us.", Dialogmessagetype.Failure, NavigateURL())
                        ' UI.Skins.Skin.AddModuleMessage(Me, "Your email address is already registered with us.", Skins.Controls.ModuleMessage.ModuleMessageType.RedError)
                    Case DotNetNuke.Security.Membership.UserCreateStatus.UserAlreadyRegistered
                        Result = ""
                        Result = "User already exists."
                        'Me.lblmsg.Text = "Your email address is already registered with us."
                        'UIUtilities.ShowDialog(Me, "Registration", "User already exists.", Dialogmessagetype.Failure, NavigateURL())
                        ' UI.Skins.Skin.AddModuleMessage(Me, "Your card is already registered with us.", Skins.Controls.ModuleMessage.ModuleMessageType.RedError)
                    Case Else
                        Result = ""
                        Result = "Your profile could not be created due to the following reason: " & status.ToString
                        'UIUtilities.ShowDialog(Me, "Registration", "Your profile could not be created due to the following reason: " & status.ToString, Dialogmessagetype.Failure, NavigateURL())
                        ' UI.Skins.Skin.AddModuleMessage(Me, "Your profile could not be created due to the following reason:" & status.ToString, Skins.Controls.ModuleMessage.ModuleMessageType.RedError)
                        ' Me.lblmsg.Text = "Your comment could not be saved due to the following reason:" & status.ToString
                End Select
                Return Result
            End If
        Catch ex As Exception
            Result = ""
            Result = ex.Message.ToString
            Return Result
        End Try
    End Function
    Public Shared Function GetRoleIDByRoleName(ByVal Name As String) As String
        Dim Result As Boolean = False
        Dim objICRoleqry As New ICRoleQuery("objICRoleqry")
        objICRoleqry.Select(objICRoleqry.RoleID)
        objICRoleqry.Where(objICRoleqry.RoleName = Name)
        If objICRoleqry.LoadDataTable.Rows.Count > 0 Then
            Return objICRoleqry.LoadDataTable(0)(0).ToString
        Else
            Return "0"
        End If
    End Function
    Private Function IsValidRecordExists() As Boolean
        Dim Result As Boolean = False
        Dim rowGVICRoles As GridDataItem
        For Each rowGVICRoles In gvUsers.Items
            If rowGVICRoles.Item("ErrorMessage").Text.ToString = "OK" Then
                Result = True
                Exit For
            End If
        Next
        Return Result
    End Function
    Private Sub ReadFileShowDetails()
        Try
            Dim FilePath As String = hfFilePath.Value.ToString()
            Dim connectionString As String = ""

            connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FilePath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""

            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = "Sheet1$"
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()
            FilterAndShowDetails(dtExcelRecords)
        Catch ex As Exception
            If ex.Message.Contains("'Sheet1$' is not a valid name") Then
                UIUtilities.ShowDialog(Me, "Bulk Bank Users", "Invalid sheet name", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            Else
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End If
        End Try
    End Sub
    Public Shared Function GetPrincipalBankBranchActiveAndApprove(ByVal OfficeCode As String) As String

        Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
        Dim qryObjICBank As New ICBankQuery("qryObjICBank")
        Dim dt As New DataTable
        Dim i As Integer

        qryObjICOffice.Select(qryObjICOffice.OfficeID, qryObjICOffice.OfficeName)
        qryObjICOffice.Where(qryObjICOffice.OffceType = "Branch Office" And qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True And qryObjICBank.IsPrincipal = True)
        qryObjICOffice.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)

        qryObjICOffice.Where(qryObjICOffice.OfficeCode = OfficeCode)

        qryObjICOffice.OrderBy(qryObjICOffice.OfficeID.Descending)
        dt = qryObjICOffice.LoadDataTable
        If dt.Rows.Count > 0 Then
            Return dt(0)(0).ToString & "-" & dt(0)(1).ToString
        Else
            Return ""
        End If
    End Function
    Public Shared Function IsRoleExistsByRoleName(ByVal Name As String) As Boolean
        Dim Result As Boolean = False
        Dim objICRoleqry As New ICRoleQuery("objICRoleqry")
        objICRoleqry.Where(objICRoleqry.RoleName = Name)
        If objICRoleqry.LoadDataTable.Rows.Count > 0 Then
            Result = True
            Return Result
        Else
            Return Result
        End If
    End Function
    Public Shared Function CheckDuplicateUserNameAndDisplayName(ByVal Name As String, ByVal Type As String) As Boolean
        Dim Result As Boolean = False
        Dim objICUserQry As New ICUserQuery("objICUserQry")
        If Type = "UserName" Then
            objICUserQry.Where(objICUserQry.UserName = Name)
        ElseIf Type = "DisplayName" Then
            objICUserQry.Where(objICUserQry.DisplayName = Name)
        ElseIf Type = "Email" Then
            objICUserQry.Where(objICUserQry.Email = Name)
        ElseIf Type = "CellNo" Then
            objICUserQry.Where(objICUserQry.CellNo = Name)
        Else
            objICUserQry.Where(objICUserQry.UserName = "")
        End If
        If objICUserQry.Load Then
            Result = True
            Return Result
        Else
            Return Result
        End If
    End Function
    Private Sub FilterAndShowDetails(ByVal dtExcel As DataTable)
        Try
            If dtExcel.Columns.Count <> 15 Then
                Dim aFileInfo As IO.FileInfo
                aFileInfo = New IO.FileInfo(hfFilePath.Value.ToString)
                'Delete file from HDD. 
                If aFileInfo.Exists Then
                    aFileInfo.Delete()
                End If
                UIUtilities.ShowDialog(Me, "Save Bulk Bank Users", "No of columns do not match.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            ElseIf dtExcel.Rows.Count > 1000 Then
                Dim aFileInfo As IO.FileInfo
                aFileInfo = New IO.FileInfo(hfFilePath.Value.ToString)
                'Delete file from HDD. 
                If aFileInfo.Exists Then
                    aFileInfo.Delete()
                End If
                UIUtilities.ShowDialog(Me, "Save Bulk Bank Users", "Invalid number of users.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ViewState("dtFiltered") = Nothing
            ViewState("dtUnVarified") = Nothing
            Dim dtFiltered As New DataTable
            Dim dtUnVarified As New DataTable
            Dim drExcel, drFiltered As DataRow
            Dim IsValid As Boolean = True
            Dim UserName, DisplayName, Phone1, Phone2, Email, Location, Department, OfficeCode, AuthenticatedVia, EmployeeCode As String
            Dim StrUserNameDuplicate, StrDisplayNameDuplicate As String
            Dim OfficeID As Integer = Nothing
            Dim strUserName As String()
            Dim strDisplayName As String()
            Dim RoleName As String()
            StrUserNameDuplicate = ""
            StrDisplayNameDuplicate = ""
            Dim RowNo As Integer = 0
            Dim ErrorMessage As String = ""

            dtFiltered.Columns.Add(New DataColumn("UserName", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("DisplayName", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("Email", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("PhoneNo1", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("PhoneNo2", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("Location", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("Department", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("OfficeCode", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("AuthenticatedVia", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("EmployeeCode", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("RoleName", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("ErrorMessage", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("DomainID", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("CellNo", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("OfficeID", GetType(System.String)))
            If dtExcel.Rows.Count > 0 Then

                For Each drExcel In dtExcel.Rows
                    RowNo = RowNo + 1
                    ErrorMessage = ""
                    IsValid = True
                    UserName = ""
                    DisplayName = ""
                    Phone1 = ""
                    Phone2 = ""
                    Location = ""
                    Email = ""
                    Department = ""
                    OfficeCode = ""
                    AuthenticatedVia = ""
                    OfficeID = Nothing
                    EmployeeCode = ""
                    RoleName = Nothing
                    If drExcel("BankBranchCode").ToString() = "" Then
                        IsValid = False
                        ErrorMessage += "Branch Code is empty (" + (drExcel("BankBranchCode").ToString()) + ") ; "
                    ElseIf drExcel(0).ToString().Length() > 7 Then
                        IsValid = False
                        ErrorMessage += "Invalid Branch Code Length (" + (drExcel("BankBranchCode").ToString()) + ") ; "

                    ElseIf Regex.IsMatch(drExcel("BankBranchCode").ToString(), "^[0-9]{1,9}$") = False Then
                        IsValid = False
                        ErrorMessage += "Invalid Branch Code (" + (drExcel("BankBranchCode").ToString()) + ") ; "
                    ElseIf GetPrincipalBankBranchActiveAndApprove(drExcel("BankBranchCode").ToString()) = "" Then
                        IsValid = False
                        ErrorMessage += "Invalid Branch Code (" + (drExcel("BankBranchCode").ToString()) + ") ; "
                    Else
                        OfficeCode = drExcel("BankBranchCode").ToString() & "-" & GetPrincipalBankBranchActiveAndApprove(drExcel("BankBranchCode").ToString()).Trim.ToString.Split("-")(1)
                        OfficeID = CInt(GetPrincipalBankBranchActiveAndApprove(drExcel("BankBranchCode").ToString()).Trim.ToString.Split("-")(0))
                    End If
                    If drExcel("AuthenticationMode").ToString() = "" Then
                        IsValid = False
                        ErrorMessage += "Invalid Authentication Mode (" + (drExcel("AuthenticationMode").ToString()) + ") ; "
                    ElseIf drExcel("AuthenticationMode").ToString().Length() > 20 Then
                        IsValid = False
                        ErrorMessage += "Invalid Authentication Mode Length (" + (drExcel("AuthenticationMode").ToString()) + ") ; "
                    ElseIf drExcel("AuthenticationMode").ToString() <> "" Then
                        If drExcel("AuthenticationMode").ToString.Trim <> "Active Directory" And drExcel("AuthenticationMode").ToString.Trim <> "Application" Then
                            IsValid = False
                            ErrorMessage += "Invalid Authentication Type (" + (drExcel("AuthenticationMode").ToString()) + ") ; "
                        End If
                    Else
                        AuthenticatedVia = drExcel("AuthenticationMode").ToString.Trim
                    End If
                    If drExcel("Usernamelogin").ToString() = "" Then
                        IsValid = False
                        ErrorMessage += "User Name is empty (" + (drExcel("Usernamelogin").ToString()) + ") ; "
                    ElseIf Regex.IsMatch(drExcel(3).ToString().Trim, "\b(\w*)\b(.)*\b(-)*\b(_)*\b") = False Then
                        IsValid = False
                        ErrorMessage += "User Name is invalid (" + (drExcel("Usernamelogin").ToString()) + ") ; "
                    ElseIf drExcel("Usernamelogin").ToString().Length() > 150 Then
                        IsValid = False
                        ErrorMessage += "Invalid User Name Length (" + (drExcel("Usernamelogin").ToString()) + ") ; "
                    ElseIf CheckDuplicateUserNameAndDisplayName(drExcel("Usernamelogin").ToString().Trim, "UserName") = True Then
                        IsValid = False
                        ErrorMessage += "Duplicate User Name (" + (drExcel("Usernamelogin").ToString()) + ") ; "
                    Else
                        If StrUserNameDuplicate.ToString() <> "" Then
                            strUserName = StrUserNameDuplicate.ToString.Split(",")
                            For cnt = 0 To strUserName.Length - 1
                                'If srtBranchCode(cnt) = drExcel("BranchCode").ToString().Trim() Then
                                If strUserName(cnt) = drExcel("Usernamelogin").ToString().Trim() Then
                                    IsValid = False
                                    ErrorMessage += "Duplicate User Name (" + (drExcel("Usernamelogin").ToString()) + ") ; "
                                    StrUserNameDuplicate = StrUserNameDuplicate & "," & drExcel("Usernamelogin").ToString().Trim()
                                    Exit For
                                End If
                            Next
                        End If
                        '  BranchCode = BranchCode & "," & drExcel("BranchCode").ToString().Trim()

                        StrUserNameDuplicate = StrUserNameDuplicate & "," & drExcel("Usernamelogin").ToString().Trim()
                    End If
                    If drExcel("DisplayName").ToString() = "" Then
                        IsValid = False
                        ErrorMessage += "Display Name is empty (" + (drExcel("DisplayName").ToString()) + ") ; "
                    ElseIf Regex.IsMatch(drExcel("DisplayName").ToString().Trim, "\b(\w*)\b(.)*\b(-)*\b(_)*\b") = False Then
                        IsValid = False
                        ErrorMessage += "Display Name is invalid (" + (drExcel("DisplayName").ToString()) + ") ; "
                    ElseIf drExcel("DisplayName").ToString().Length() > 150 Then
                        IsValid = False
                        ErrorMessage += "Invalid Display Name Length (" + (drExcel("DisplayName").ToString()) + ") ; "
                    ElseIf CheckDuplicateUserNameAndDisplayName(drExcel("DisplayName").ToString().Trim, "DisplayName") = True Then
                        IsValid = False
                        ErrorMessage += "Duplicate Display Name (" + (drExcel("DisplayName").ToString()) + ") ; "
                    Else
                        If StrDisplayNameDuplicate.ToString() <> "" Then
                            strDisplayName = StrDisplayNameDuplicate.ToString.Split(",")
                            For cnt = 0 To strDisplayName.Length - 1
                                'If srtBranchCode(cnt) = drExcel("BranchCode").ToString().Trim() Then
                                If strDisplayName(cnt) = drExcel("DisplayName").ToString().Trim() Then
                                    IsValid = False
                                    ErrorMessage += "Duplicate Display Name (" + (drExcel("DisplayName").ToString()) + ") ; "
                                    StrDisplayNameDuplicate = StrDisplayNameDuplicate & "," & drExcel("DisplayName").ToString().Trim()
                                    Exit For
                                End If
                            Next
                        End If
                        '  BranchCode = BranchCode & "," & drExcel("BranchCode").ToString().Trim()
                        StrDisplayNameDuplicate = StrDisplayNameDuplicate & "," & drExcel("DisplayName").ToString().Trim()
                    End If
                    If drExcel("PhonNo1").ToString() <> "" Then
                        If drExcel("PhonNo1").ToString().Length() > 15 Then
                            IsValid = False
                            ErrorMessage += "Invalid Phone1 Length (" + (drExcel("PhonNo1").ToString()) + ") ; "
                        ElseIf Regex.IsMatch(drExcel("PhonNo1").ToString(), "^\+?[\d- ]{2,15}$") = False Then
                            IsValid = False
                            ErrorMessage += "Invalid Phone1 Number (" + (drExcel("PhonNo1").ToString()) + ") ; "
                        End If
                    Else
                        IsValid = False
                        ErrorMessage += "Phone1 Number is empty (" + (drExcel("PhonNo1").ToString()) + ") ; "
                    End If
                    If drExcel("PhoneNo2").ToString().Length() > 15 Then
                        IsValid = False
                        ErrorMessage += "Invalid Phone2 Length (" + (drExcel("PhoneNo2").ToString()) + ") ; "
                    ElseIf drExcel("PhoneNo2").ToString() <> "" Then
                        If Regex.IsMatch(drExcel("PhoneNo2").ToString(), "^\+?[\d- ]{2,15}$") = False Then
                            IsValid = False
                            ErrorMessage += "Invalid Phone2 Number (" + (drExcel("PhoneNo2").ToString()) + ") ; "
                        End If
                    End If
                    If drExcel("Email").ToString() <> "" Then
                        If drExcel("Email").ToString().Length() > 100 Then
                            IsValid = False
                            ErrorMessage += "Invalid Email Length (" + (drExcel("Email").ToString()) + ") ; "
                        ElseIf Regex.IsMatch(drExcel("Email").ToString(), "^\w+([-+.']\w+)*@\w+([-.%]\w+)*\.\w+([-.]\w+)*$") = False Then
                            IsValid = False
                            ErrorMessage += "Invalid Email (" + (drExcel("Email").ToString()) + ") ; "
                        ElseIf CheckDuplicateUserNameAndDisplayName(drExcel("Email").ToString().Trim, "Email") = True Then
                            IsValid = False
                            ErrorMessage += "Duplicate Email Address (" + (drExcel("Email").ToString()) + ") ; "
                        End If
                    Else
                        IsValid = False
                        ErrorMessage += "Email is empty (" + (drExcel("Email").ToString()) + ") ; "
                    End If

                    If drExcel("CellNo").ToString() <> "" Then
                        If drExcel("CellNo").ToString().Length() > 15 Then
                            IsValid = False
                            ErrorMessage += "Invalid CellNo Length (" + (drExcel("CellNo").ToString()) + ") ; "
                        ElseIf Regex.IsMatch(drExcel("CellNo").ToString(), "^\+?[\d- ]{2,15}$") = False Then
                            IsValid = False
                            ErrorMessage += "Invalid CellNo (" + (drExcel("CellNo").ToString()) + ") ; "
                        ElseIf CheckDuplicateUserNameAndDisplayName(drExcel("CellNo").ToString().Trim, "CellNo") = True Then
                            IsValid = False
                            ErrorMessage += "Duplicate CellNo (" + (drExcel("CellNo").ToString()) + ") ; "
                        End If
                    Else
                        IsValid = False
                        ErrorMessage += "CellNo is empty (" + (drExcel("CellNo").ToString()) + ") ; "
                    End If

                    If drExcel("EmployeeCode").ToString = "" Then
                        IsValid = False
                        ErrorMessage += "Employee Code is empty (" + (drExcel("EmployeeCode").ToString()) + ") ; "
                    ElseIf drExcel("EmployeeCode").ToString().Length() > 50 Then
                        IsValid = False
                        ErrorMessage += "Invalid Employee Code Length (" + (drExcel("EmployeeCode").ToString()) + ") ; "
                    End If
                    If drExcel("Role").ToString <> "" Then
                        If drExcel("Role").ToString.Contains(",") Then
                            RoleName = drExcel("Role").ToString.Trim.Split(",")
                            For j = 0 To RoleName.Length - 1
                                If IsRoleExistsByRoleName(RoleName(j).ToString) = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Role Name (" + (RoleName(j).ToString) + ") ; "
                                End If
                            Next
                        Else
                            If IsRoleExistsByRoleName(drExcel("Role").ToString.Trim) = False Then
                                IsValid = False
                                ErrorMessage += "Invalid Role Name (" + (drExcel("Role").ToString.Trim) + ") ; "
                            End If
                        End If
                    End If
                    gvUsers.Visible = True
                    gvUserStatus.DataSource = Nothing
                    gvUserStatus.Visible = False
                    btnSave.Visible = True
                    btnProceed.Visible = False
                    
                    drFiltered = dtFiltered.NewRow()
                    'UserName
                    drFiltered(0) = drExcel("Usernamelogin").ToString()
                    'DisplayName
                    drFiltered(1) = drExcel("DisplayName").ToString()
                    'Email
                    drFiltered(2) = drExcel("Email").ToString()
                    'PhoneNo1
                    drFiltered(3) = drExcel("PhonNo1").ToString()
                    'PhoneNo2
                    drFiltered(4) = drExcel("PhoneNo2").ToString()
                    'Location
                    drFiltered(5) = drExcel("Location").ToString()
                    'Department
                    drFiltered(6) = drExcel("Department").ToString()
                    'OfficeCode
                    drFiltered(7) = OfficeCode
                    'AuthenticatedVia
                    drFiltered(8) = drExcel("AuthenticationMode").ToString()
                    'EmployeeCode
                    drFiltered(9) = drExcel("EmployeeCode").ToString()
                    'RoleName
                    drFiltered(10) = drExcel("Role").ToString()
                    'ErrorMessage
                    If ErrorMessage <> "" Then
                        drFiltered(11) = ErrorMessage
                    Else
                        drFiltered(11) = "OK"
                    End If
                    'DomainID
                    drFiltered(12) = drExcel(2).ToString()
                    'CellNo
                    drFiltered(13) = drExcel("CellNo").ToString()
                    drFiltered(14) = OfficeID
                    dtFiltered.Rows.Add(drFiltered)
                Next

                ViewState("dtFiltered") = dtFiltered
                ViewState("dtUnVarified") = dtUnVarified
                gvUsers.DataSource = Nothing
                gvUsers.DataSource = ViewState("dtFiltered")
                gvUsers.DataBind()
                'SetGVBankBranchesGVUnverified(True)
                Dim aFileInfo As IO.FileInfo
                aFileInfo = New IO.FileInfo(hfFilePath.Value.ToString)
                'Delete file from HDD. 
                If aFileInfo.Exists Then
                    aFileInfo.Delete()
                End If

            Else
                UIUtilities.ShowDialog(Me, "Save Bulk Bank Branch", "Empty File.", ICBO.IC.Dialogmessagetype.Failure)
                'Clear()
                Exit Sub
            End If
            'End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub
#End Region



#Region "Other Functions/Routines"
    Private Sub AuthorizeUser(ByVal UserID As String)
        Dim objICUser As New ICUser()
        objICUser.es.Connection.CommandTimeout = 3600
        Try
            If objICUser.LoadByPrimaryKey(UserID) Then
                Dim user As New UserInfo
                Dim member As New UserMembership
                user = UserController.GetUserById(Me.PortalId, UserID)
                If objICUser.IsApproved = True And objICUser.IsActive = True Then
                    member.Approved = True
                    user.Membership = member

                    UserController.UpdateUser(Me.PortalId, user)
                Else
                    member.Approved = False

                    user.Membership = member


                    UserController.UpdateUser(Me.PortalId, user)
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    
#End Region

    
    Private Function AuthenticationModeWiseSetting(ByVal AuthenticationType As String) As String
        If AuthenticationType = "Active Directory" Then
            If UserCode.ToString() = "0" Then
                Return GetRandomPassword().ToString()
            End If
        End If
    End Function
    Private Function GetRandomPassword() As String
        Return Membership.GeneratePassword(8, 1).ToString()
    End Function

    Protected Sub btnProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        Page.Validate()
        If Page.IsValid Then

            Try
                Dim str As String = System.IO.Path.GetExtension(fuplUsers.PostedFile.FileName)
                If fuplUsers.HasFile Then

                    If str = ".xlsx" Then

                        Dim aFileInfo As IO.FileInfo
                        aFileInfo = New IO.FileInfo(Server.MapPath("~/UploadedFiles/") & fuplUsers.FileName.ToString())
                        'Delete file from HDD. 
                        If aFileInfo.Exists Then
                            aFileInfo.Delete()
                        End If

                        fuplUsers.SaveAs(Server.MapPath("~/UploadedFiles/") & fuplUsers.FileName.ToString())
                        hfFilePath.Value = Server.MapPath("~/UploadedFiles/") & fuplUsers.FileName.ToString()
                        ReadFileShowDetails()
                    Else
                        UIUtilities.ShowDialog(Me, "Save Bulk Bank Users", "Please select the .xlsx file.", ICBO.IC.Dialogmessagetype.Failure)
                    End If
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try

        End If
    End Sub
End Class