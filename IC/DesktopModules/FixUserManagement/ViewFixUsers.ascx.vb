﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_FixUsers_ViewFixRolesUsers
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
   
#Region "Page Load"
    Protected Sub DesktopModules_FixUsers_ViewFixRolesUsers_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Me.UserInfo.IsSuperUser = True Then
                btnSaveUserInBulk.Visible = True
            Else
                btnSaveUserInBulk.Visible = False
            End If
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnSaveUser.Visible = CBool(htRights("Add"))
                LaodddlOffice()
                LoadgvFixUsers(True, 0, Me.gvBankUsers.PageSize, Me.gvBankUsers)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
#Region "Button Events"
    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Page.Validate()
        If Page.IsValid Then
            Try
                LoadgvFixUsers(True, Me.gvBankUsers.CurrentPageIndex + 1, Me.gvBankUsers.PageSize, Me.gvBankUsers)

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub
    
    Protected Sub btnSaveUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveUser.Click
        Page.Validate()
        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveFixUser", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If

    End Sub

#End Region
#Region "Grid View Events"
    Private Sub LoadgvFixUsers(ByVal DataBind As Boolean, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal gv As radgrid)
        Try
            Dim UserName, DisplayName, Email, OfficeCode, UserType As String

            If Not txtUserName.Text.ToString() = "" And Not txtUserName.Text = "Enter User Name" Then
                UserName = txtUserName.Text.ToString()
            Else
                UserName = ""
            End If
            If Not txtDisplayName.Text.ToString() = "" And Not txtDisplayName.Text = "Enter Display Name" Then
                DisplayName = txtDisplayName.Text.ToString()
            Else
                DisplayName = ""

            End If
            If Not txtEmailAddress.Text.ToString() = "" And Not txtEmailAddress.Text = "Enter Email Address" Then
                Email = txtEmailAddress.Text.ToString()
            Else
                Email = ""
            End If
            If ddlBankBranch.SelectedValue.ToString <> "0" Then
                OfficeCode = ddlBankBranch.SelectedValue.ToString()
            Else
                'UIUtilities.ShowDialog(Me, "Error", "Please select office/branch", ICBO.IC.Dialogmessagetype.Failure)
                'Exit Sub
                OfficeCode = "0"
            End If

            UserType = "Bank User"

            ICUserController.GetUserByOfficeCode(OfficeCode, UserType, UserName, DisplayName, Email, PageNumber, PageSize, gv, DataBind, "", "")

            If gvBankUsers.Items.Count > 0 Then
                lblError.Visible = False
                gvBankUsers.Visible = True
                btnDeleteUsers.Visible = CBool(htRights("Delete"))
                btnApproveUsers.Visible = CBool(htRights("Approve"))
                gvBankUsers.Columns(9).Visible = CBool(htRights("Delete"))
                'gvBankUsers.Columns(10).Visible = CBool(htRights("Tag Role"))
            Else
                gvBankUsers.Visible = False
                lblError.Visible = True
                btnDeleteUsers.Visible = False
                btnApproveUsers.Visible = False
            End If


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Bank Users Management")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub gvBankUsers_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvBankUsers.ItemCommand
    

        Try


            If e.CommandName = "del" Then
                If Page.IsValid Then
                    Dim objICUser As New ICUser
                    objICUser.es.Connection.CommandTimeout = 3600
                    Dim iUser As New UserInfo
                    objICUser.LoadByPrimaryKey(e.CommandArgument.ToString())

                    If objICUser.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                        If objICUser.IsApproved Then
                            UIUtilities.ShowDialog(Me, "Delete User", "User is approved and cannot be deleted.", ICBO.IC.Dialogmessagetype.Failure)
                            LoadgvFixUsers(False, Me.gvBankUsers.CurrentPageIndex + 1, Me.gvBankUsers.PageSize, Me.gvBankUsers)
                            Exit Sub
                        Else
                            iUser = UserController.GetUserById(Me.PortalId, e.CommandArgument.ToString())
                            ICUserController.DeleteICUser(e.CommandArgument.ToString(), Me.UserId.ToString, Me.UserInfo.Username.ToString, objICUser.UserType.ToString)
                            UserController.RemoveUser(iUser)
                            UIUtilities.ShowDialog(Me, "Delete User ", "User deleted successfully.", ICBO.IC.Dialogmessagetype.Success)

                            LoadgvFixUsers(True, 0, Me.gvBankUsers.PageSize, Me.gvBankUsers)
                        End If
                    End If
                End If
            End If
            If e.CommandName = "Unlock" Then
                Dim user As New UserInfo
                user = UserController.GetUserById(Me.PortalId, e.CommandArgument.ToString())
                UserController.UnLockUser(user)
                DataCache.RemoveCache("MembershipUser_" & user.Username.ToString())
                UIUtilities.ShowDialog(Me, "Save User", "User Unlocked Successfully.", ICBO.IC.Dialogmessagetype.Success)
                LoadgvFixUsers(True, Me.gvBankUsers.CurrentPageIndex + 1, Me.gvBankUsers.PageSize, Me.gvBankUsers)
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Delete User", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvBankUsers_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvBankUsers.ItemCreated
       Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvBankUsers.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub gvBankUsers_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvBankUsers.ItemDataBound
        Try
            Dim chkProcessAll As CheckBox

            If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
                chkProcessAll = New CheckBox
                chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAllUsers"), CheckBox)
                chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvBankUsers.MasterTableView.ClientID & "','0');"
            End If
            If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then

                Dim user As New UserInfo
                Dim IbtnUnlock As New ImageButton
                IbtnUnlock = DirectCast(e.Item.Cells(11).FindControl("IbtnUnlock"), ImageButton)
                user = UserController.GetUserById(Me.PortalId, IbtnUnlock.CommandArgument.ToString())
                IbtnUnlock.Visible = user.Membership.LockedOut
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
#End Region
#Region "Drop Down events"
    Private Sub LaodddlOffice()
        Try
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- ALL --"
            ddlBankBranch.Items.Clear()
            ddlBankBranch.Items.Add(lit)
            ddlBankBranch.AppendDataBoundItems = True
            ' dt = ICOfficeController.GetOfficeByOfficeTypeActiveAndApprove(UserType.ToString)
            ddlBankBranch.DataSource = ICOfficeController.GetPrincipalBankBranchActiveAndApproveWithCodeAndName()
            ddlBankBranch.DataTextField = "OfficeName"
            ddlBankBranch.DataValueField = "OfficeID"
            ddlBankBranch.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlOffice_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBankBranch.SelectedIndexChanged
        Try

            LoadgvFixUsers(True, Me.gvBankUsers.CurrentPageIndex + 1, Me.gvBankUsers.PageSize, Me.gvBankUsers)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
#Region "Other Functions/Routines"
  
    
#End Region

    Protected Sub gvBankUsers_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvBankUsers.NeedDataSource
        Try
            LoadgvFixUsers(False, Me.gvBankUsers.CurrentPageIndex + 1, Me.gvBankUsers.PageSize, Me.gvBankUsers)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnApproveUsers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveUsers.Click
        Page.Validate()
        If Page.IsValid Then

            Try
                Dim rowGVBankUser As GridDataItem
                Dim chkSelect As CheckBox
                Dim UserID As String = ""
                Dim PassToApproveCount As Integer = 0
                Dim FailToApproveCount As Integer = 0

                If CheckgvBankUserSelected() = False Then
                    UIUtilities.ShowDialog(Me, "Save User", "Please select atleast one user.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVBankUser In gvBankUsers.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVBankUser.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        UserID = rowGVBankUser.GetDataKeyValue("UserID").ToString
                        Dim objICBankUser As New ICUser
                        objICBankUser.es.Connection.CommandTimeout = 3600
                        objICBankUser.LoadByPrimaryKey(UserID)
                        If Me.UserInfo.IsSuperUser = False Then
                            If objICBankUser.IsApproved = True Or objICBankUser.CreateBy = Me.UserId Then
                                FailToApproveCount = FailToApproveCount + 1
                            Else
                                If objICBankUser.IsActive = True Then
                                    Try

                                        ICUserController.ApproveUser(objICBankUser.UserID, Me.UserId.ToString, True, Me.UserInfo.Username.ToString, objICBankUser.UserType.ToString)
                                        AuthorizeUser(objICBankUser.UserID.ToString)
                                        PassToApproveCount = PassToApproveCount + 1
                                    Catch ex As Exception

                                        FailToApproveCount = FailToApproveCount + 1
                                        Continue For

                                    End Try
                                Else
                                    FailToApproveCount = FailToApproveCount + 1
                                End If

                            End If

                        Else
                            If objICBankUser.IsApproved = True Then
                                FailToApproveCount = FailToApproveCount + 1
                            Else
                                If objICBankUser.IsActive = True Then
                                    Try
                                        ICUserController.ApproveUser(objICBankUser.UserID, Me.UserId.ToString, True, Me.UserInfo.Username.ToString, objICBankUser.UserType.ToString)
                                        AuthorizeUser(objICBankUser.UserID.ToString)
                                        PassToApproveCount = PassToApproveCount + 1
                                    Catch ex As Exception

                                        FailToApproveCount = FailToApproveCount + 1
                                        Continue For

                                    End Try
                                Else
                                    FailToApproveCount = FailToApproveCount + 1
                                    Continue For
                                End If

                            End If
                        End If

                    End If
                Next

                If Not PassToApproveCount = 0 And Not FailToApproveCount = 0 Then
                    LoadgvFixUsers(True, Me.gvBankUsers.CurrentPageIndex + 1, Me.gvBankUsers.PageSize, Me.gvBankUsers)
                    UIUtilities.ShowDialog(Me, "Save User", "[ " & PassToApproveCount.ToString & " ] User(s) approved successfully.<br /> [ " & FailToApproveCount.ToString & " ] User(s) can not be approved due to following reasons: <br /> 1. User is already approved<br /> 2. User must be approved by other than maker<br /> 3. User must be active", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToApproveCount = 0 And FailToApproveCount = 0 Then
                    LoadgvFixUsers(True, Me.gvBankUsers.CurrentPageIndex + 1, Me.gvBankUsers.PageSize, Me.gvBankUsers)
                    UIUtilities.ShowDialog(Me, "Save User", PassToApproveCount.ToString & " User(s) approved successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToApproveCount = 0 And PassToApproveCount = 0 Then
                    LoadgvFixUsers(True, Me.gvBankUsers.CurrentPageIndex + 1, Me.gvBankUsers.PageSize, Me.gvBankUsers)
                    UIUtilities.ShowDialog(Me, "Save User", "[ " & FailToApproveCount.ToString & " ] User(s) can not be approved due to following reasons: <br /> 1. User is already approved<br /> 2. User must be approved by other than maker<br /> 3. User must be active", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try

        End If
    End Sub
    Private Sub AuthorizeUser(ByVal UserCode As String)
        Dim objICUser As New ICUser()
        objICUser.es.Connection.CommandTimeout = 3600
        Try
            If objICUser.LoadByPrimaryKey(UserCode.ToString()) Then
                Dim user As New UserInfo
                Dim member As New UserMembership
                user = UserController.GetUserById(Me.PortalId, UserCode)
                If objICUser.IsApproved = True And objICUser.IsActive = True Then
                    member.Approved = True
                    user.Membership = member
                    UserController.UpdateUser(Me.PortalId, user)
                Else
                    member.Approved = False
                    user.Membership = member
                    UserController.UpdateUser(Me.PortalId, user)
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckgvBankUserSelected() As Boolean
        Try
            Dim rowGVICUsers As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICRoles In gvBankUsers.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub gvBankUsers_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvBankUsers.PageIndexChanged
        Try
            gvBankUsers.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnDeleteUsers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteUsers.Click
        Page.Validate()
        If Page.IsValid Then
            Dim rowGVBankUsers As GridDataItem
            Dim chkSelect As CheckBox
            Dim UserID As String = ""
            Dim PassToDeleteCount As Integer = 0
            Dim FailToDeleteCount As Integer = 0
            Dim userCtrl As New RoleController
            Dim arrLst As New ArrayList
            Dim iUser As UserInfo



            If CheckgvBankUserSelected() = False Then
                UIUtilities.ShowDialog(Me, "Bank User", "Please select atleast one user.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            Try
                For Each rowGVICRoles In gvBankUsers.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVICRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        UserID = rowGVICRoles.GetDataKeyValue("UserID").ToString
                        Dim objICBankUser As New ICUser
                        objICBankUser.es.Connection.CommandTimeout = 3600
                        If objICBankUser.LoadByPrimaryKey(UserID.ToString()) Then
                            If objICBankUser.IsApproved = True Then
                                FailToDeleteCount = FailToDeleteCount + 1
                            Else
                                Try
                                    iUser = New UserInfo
                                    iUser = UserController.GetUserById(Me.PortalId, UserID.ToString)
                                    ICUserController.DeleteICUser(objICBankUser.UserID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, objICBankUser.UserType.ToString)
                                    UserController.RemoveUser(iUser)
                                    PassToDeleteCount = PassToDeleteCount + 1
                                Catch ex As Exception
                                    'If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                    FailToDeleteCount = FailToDeleteCount + 1
                                    Continue For
                                    'End If
                                End Try
                            End If
                        End If
                    End If

                Next

                If Not PassToDeleteCount = 0 And Not FailToDeleteCount = 0 Then
                    LoadgvFixUsers(True, Me.gvBankUsers.CurrentPageIndex + 1, Me.gvBankUsers.PageSize, Me.gvBankUsers)
                    UIUtilities.ShowDialog(Me, "Save User", "[ " & PassToDeleteCount.ToString & " ] User(s) deleted successfuly.<br /> [ " & FailToDeleteCount.ToString & " ] User(s) can not be deleted due to following reason(s): <br /> 1. User is approved <br /> 2. Please delete associated record(s) first", ICBO.IC.Dialogmessagetype.Failure)

                    Exit Sub
                ElseIf Not PassToDeleteCount = 0 And FailToDeleteCount = 0 Then
                    LoadgvFixUsers(True, Me.gvBankUsers.CurrentPageIndex + 1, Me.gvBankUsers.PageSize, Me.gvBankUsers)
                    UIUtilities.ShowDialog(Me, "Save User", PassToDeleteCount.ToString & " User(s) deleted successfuly.", ICBO.IC.Dialogmessagetype.Success)

                    Exit Sub
                ElseIf Not FailToDeleteCount = 0 And PassToDeleteCount = 0 Then
                    LoadgvFixUsers(True, Me.gvBankUsers.CurrentPageIndex + 1, Me.gvBankUsers.PageSize, Me.gvBankUsers)
                    UIUtilities.ShowDialog(Me, "Save User", "[ " & FailToDeleteCount.ToString & " ] User(s) can not be deleted due to following reasons: <br />1. User is approved<br /> 2. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)

                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnSaveUserInBulk_Click(sender As Object, e As System.EventArgs) Handles btnSaveUserInBulk.Click
        Page.Validate()

        If Page.IsValid Then


            Response.Redirect(NavigateURL("SaveUsersInBulk", "&mid=" & Me.ModuleId), False)

        End If
    End Sub
End Class