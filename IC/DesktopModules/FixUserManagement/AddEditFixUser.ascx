﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddEditFixUser.ascx.vb"
    Inherits="DesktopModules_FixUsers_AddEditFixRolesUser" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }
    function checkboxActionLogin() {
        var chk1 = document.getElementById('<%=chk2FAViaSMSForLogin.ClientID%>');
        var chk2 = document.getElementById('<%=chk2FAViaEmailForLogin.ClientID%>');

        if (chk1.checked || chk2.checked) {
            document.getElementById('<%=chk2FAOnLogin.ClientID%>').checked = true;
          
        }
        else if (chk1.checked == false && chk2.checked == false)
  {
      document.getElementById('<%=chk2FAOnLogin.ClientID%>').checked = false;
  }
}
function checkboxActionApproval() {
    var chk1 = document.getElementById('<%=chk2FAViaSMSForApproval.ClientID%>');
    var chk2 = document.getElementById('<%=chk2FAViaEmailForApproval.ClientID%>');

    if (chk1.checked || chk2.checked) {
        document.getElementById('<%=chk2FAOnApproval.ClientID%>').checked = true;

    }
    else if (chk1.checked == false && chk2.checked == false) {
        document.getElementById('<%=chk2FAOnApproval.ClientID%>').checked = false;
    }
}
function checkboxAction2FAApproval() {
    var chk1 = document.getElementById('<%=chk2FAOnApproval.ClientID%>');

    if (chk1.checked) {
        document.getElementById('<%=chk2FAViaSMSForApproval.ClientID%>').checked = true;
        document.getElementById('<%=chk2FAViaEmailForApproval.ClientID%>').checked = true;

    }
    else if (chk1.checked == false) {
        document.getElementById('<%=chk2FAViaSMSForApproval.ClientID%>').checked = false;
        document.getElementById('<%=chk2FAViaEmailForApproval.ClientID%>').checked = false;
    }
}
function checkboxAction2FAOnLogin() {
    var chk1 = document.getElementById('<%=chk2FAOnLogin.ClientID%>');

    if (chk1.checked) {
        document.getElementById('<%=chk2FAViaSMSForLogin.ClientID%>').checked = true;
        document.getElementById('<%=chk2FAViaEmailForLogin.ClientID%>').checked = true;

    }
    else if (chk1.checked == false) {
        document.getElementById('<%=chk2FAViaSMSForLogin.ClientID%>').checked = false;
        document.getElementById('<%=chk2FAViaEmailForLogin.ClientID%>').checked = false;
    }
}
</script>

<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:RegExpTextBoxSetting BehaviorID="RECellNo" Validation-IsRequired="true"
        ValidationExpression="^\+?[\d- ]{2,15}$" ErrorMessage="Invalid Cell Number" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCellNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="REPhoneNo" Validation-IsRequired="true"
        ValidationExpression="^\+?[\d- ]{2,15}$" ErrorMessage="Invalid Phone Number" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPhoneNo1" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="REPhoneNo2" Validation-IsRequired="false"
        ValidationExpression="^\+?[\d- ]{2,15}$" ErrorMessage="Invalid Phone Number" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPhoneNo2" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="REUserName" Validation-IsRequired="true"
        ValidationExpression="\b(\w*)\b(.)*\b(-)*\b(_)*\b" ErrorMessage="Invalid Name"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtUserName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="REDisplayName" Validation-IsRequired="true"
        ValidationExpression="\b(\w*)\b(.)*\b(-)*\b(_)*\b" ErrorMessage="Invalid Display Name"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDisplayName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <%--<telerik:TextBoxSetting BehaviorID="REPassword" Validation-IsRequired="true" >
        <TargetControls>
            <telerik:TargetInput ControlID="txtPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="REConfirmPassword" Validation-IsRequired="true">
        <TargetControls>
            <telerik:TargetInput ControlID="txtConfirmPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>--%>
     <telerik:TextBoxSetting BehaviorID="REPassword" Validation-IsRequired="true">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="REConfirmPassword" Validation-IsRequired="true">
        <TargetControls>
            <telerik:TargetInput ControlID="txtConfirmPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="REEmailAddress" Validation-IsRequired="true"
        ValidationExpression="^([a-zA-Z0-9_\-\.]+)@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$"
        ErrorMessage="Invalid Email" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtEmailAddress" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RELocation" Validation-IsRequired="false" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtLocation" />
        </TargetControls>
    </telerik:TextBoxSetting>
     <telerik:TextBoxSetting BehaviorID="REDepartment" Validation-IsRequired="false" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDepartment" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reEmployeeCode" Validation-IsRequired="true" EmptyMessage="" ErrorMessage="Enter Employee Code">
        <TargetControls>
            <telerik:TargetInput ControlID="txtEmployeeCode" />
        </TargetControls>
    </telerik:TextBoxSetting>
     <telerik:TextBoxSetting BehaviorID="reClearingBranchCode" Validation-IsRequired="true" EmptyMessage="" ErrorMessage="Enter Clearing Branch Code">
        <TargetControls>
            <telerik:TargetInput ControlID="txtClearingBranchCode" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RADApprovalLimit" Validation-IsRequired="False"
        ValidationExpression="[0-9]{2,6}" ErrorMessage="Invalid Approval Limit" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtApprovalLimit" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
</telerik:RadInputManager>
<%--<telerik:TextBoxSetting BehaviorID="REPassword" Validation-IsRequired="true" >
        <TargetControls>
            <telerik:TargetInput ControlID="txtPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="REConfirmPassword" Validation-IsRequired="true">
        <TargetControls>
            <telerik:TargetInput ControlID="txtConfirmPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>--%><%--<telerik:TextBoxSetting BehaviorID="REPassword" Validation-IsRequired="true" >
        <TargetControls>
            <telerik:TargetInput ControlID="txtPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="REConfirmPassword" Validation-IsRequired="true">
        <TargetControls>
            <telerik:TargetInput ControlID="txtConfirmPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>--%>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
            <br />
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top">
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            &nbsp;
        </td>
        <td align="left" valign="top">
            &nbsp;
        </td>
    </tr>
    </table>
<%--<telerik:TextBoxSetting BehaviorID="REPassword" Validation-IsRequired="true" >
        <TargetControls>
            <telerik:TargetInput ControlID="txtPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="REConfirmPassword" Validation-IsRequired="true">
        <TargetControls>
            <telerik:TargetInput ControlID="txtConfirmPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>--%>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBankBranch" runat="server" Text="Select Bank Branch" 
                CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqBankBranch" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAuthenticationMode" runat="server" Text="Select Authentication Mode" 
                CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqBankBranch0" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlBankBranch" runat="server" AutoPostBack="True"
                CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlAuthenticationMode" runat="server"
                CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem>Active Directory</asp:ListItem>
                <asp:ListItem>Application</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvBankBranch" runat="server" ControlToValidate="ddlBankBranch"
                CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Bank Branch" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvAuthenticationMode" runat="server" ControlToValidate="ddlAuthenticationMode"
                CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Authentication Mode" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    </table>
<%--<telerik:TextBoxSetting BehaviorID="REPassword" Validation-IsRequired="true" >
        <TargetControls>
            <telerik:TargetInput ControlID="txtPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="REConfirmPassword" Validation-IsRequired="true">
        <TargetControls>
            <telerik:TargetInput ControlID="txtConfirmPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>--%>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblUserName" runat="server" Text="User Name" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqUserName" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblDisplayName" runat="server" Text="Display Name" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqDisplayName" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtUserName" runat="server" CssClass="txtbox" MaxLength="25"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtDisplayName" runat="server" CssClass="txtbox" 
                MaxLength="128"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
<%--    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblPassword" runat="server" Text="Password" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqPassword" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblConfirmPassword" runat="server" Text="Confirm Password" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqConfirmPasword" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            
            <asp:TextBox ID="txtPassword" runat="server" CssClass="txtbox" TextMode="Password"
                 MaxLength="8"></asp:TextBox>

            <asp:RegularExpressionValidator ID="revPassqord" runat="server" ControlToValidate="txtPassword"
                Display="Dynamic" ErrorMessage="Must have at least 1 number, 1 special character, 1 upper case letter, 1 lower case letter,
        and more than 6 characters." ValidationExpression="(?=^.{6,8}$)((?=.*\d)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
                ></asp:RegularExpressionValidator>

        </td>
        <td align="center" valign="top" style="width: 25%">
            <asp:LinkButton ID="lnkChangePassword" runat="server" CausesValidation="False">Change Password</asp:LinkButton>
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="txtbox" TextMode="Password"
                 MaxLength="8"></asp:TextBox>
           
            <br />
            <asp:CompareValidator ID="comparePasswords" runat="server" ControlToCompare="txtPassword"
                ControlToValidate="txtConfirmPassword" ErrorMessage="The passwords you typed do not match. Type the new password in both text boxes"
                Display="Dynamic" />
            <br />
            <asp:RegularExpressionValidator ID="revConfirmPassword" runat="server" ControlToValidate="txtConfirmPassword"
                Display="Dynamic" ErrorMessage="Must have at least 1 number, 1 special character, 1 upper case letter, 1 lower case letter,
        and more than 6 characters." ValidationExpression="(?=^.{6,8}$)((?=.*\d)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
               ></asp:RegularExpressionValidator>
            
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>--%>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCellNo" runat="server" Text="Cell No." CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqCellNo" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblEmailAddress" runat="server" Text="Email" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqEmailAddress" runat="server" ForeColor="Red" Text="*"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtCellNo" runat="server" CssClass="txtbox" MaxLength="15"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="txtbox" MaxLength="100"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblPhone1" runat="server" Text="Phone No 1" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label14" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblPhoneNo2" runat="server" Text="Phone No. 2" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtPhoneNo1" runat="server" CssClass="txtbox" MaxLength="15"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtPhoneNo2" runat="server" CssClass="txtbox" MaxLength="15"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblEmployeeCode" runat="server" Text="Employee Code" 
                CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqEmployeeCode" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblClearingBranchCode" runat="server" Text="Clearing Branch Code" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtEmployeeCode" runat="server" CssClass="txtbox" 
                MaxLength="50"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlClearingBranchCode" runat="server"
                CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblLocation" runat="server" CssClass="lbl" Text="Location"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblDepartment" runat="server" CssClass="lbl" Text="Department"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtLocation" runat="server" CssClass="txtbox" MaxLength="250" Height="70px"
                TextMode="MultiLine" onkeypress="return textboxMultilineMaxNumber(this,250)"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtDepartment" runat="server" CssClass="txtbox" 
                MaxLength="250" Height="70px"
                TextMode="MultiLine" 
                onkeypress="return textboxMultilineMaxNumber(this,250)"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lbl2FAOn" runat="server" CssClass="lbl" 
                Text="2FA Required On Login"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lbl2FAOnApproval" runat="server" CssClass="lbl" 
                Text="2FA Required On Approval"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <%--<telerik:RadInputManager ID="radPassword" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="REPassword" Validation-IsRequired="true" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="REConfirmPassword" Validation-IsRequired="true"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtConfirmPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>--%>
                 <asp:CheckBox ID="chk2FAOnLogin" runat="server" Text=" On Login" onclick="checkboxAction2FAOnLogin()"
                CssClass="chkBox" />
        <br /><br />
            <asp:Label ID="lbl2FAViaForLogin" runat="server" CssClass="lbl" 
                Text="2FA On Login Via"></asp:Label><br />
            <asp:CheckBox ID="chk2FAViaSMSForLogin" runat="server" Text=" SMS" onclick="checkboxActionLogin()"
                CssClass="chkBox" />
            <br />
            <asp:CheckBox ID="chk2FAViaEmailForLogin" runat="server" Text=" Email" onclick="checkboxActionLogin()"
                CssClass="chkBox" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <%--<telerik:RadInputManager ID="radPassword" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="REPassword" Validation-IsRequired="true">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="REConfirmPassword" Validation-IsRequired="true"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtConfirmPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>--%>
                <asp:CheckBox ID="chk2FAOnApproval" runat="server" CssClass="chkBox" onclick="checkboxAction2FAApproval()"
                Text=" On Approval" />
            <br />
            <br />
            <asp:Label ID="lbl2FAViaForApproval" runat="server" CssClass="lbl" 
                Text="2FA On Approval Via"></asp:Label>
            <br />
            <asp:CheckBox ID="chk2FAViaSMSForApproval" runat="server" CssClass="chkBox" onclick="checkboxActionApproval()"
                Text=" SMS" />
            <br />
            <asp:CheckBox ID="chk2FAViaEmailForApproval" runat="server" CssClass="chkBox" onclick="checkboxActionApproval()"
                Text=" Email" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
</table>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:LinkButton ID="lnkChangePassword" runat="server" CausesValidation="False">Change Password</asp:LinkButton>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIsActive" runat="server" Text="Is Active" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIsApproved" runat="server" Text="Is Approved" CssClass="lbl" Visible="False"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkActive" runat="server" Text=" " CssClass="chkBox" Checked="True" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkApproved" runat="server" Text=" " Visible="False" CssClass="chkBox" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnApproved" runat="server" Text="Approved" CssClass="btn" CausesValidation="False"
                Width="70px" />
            &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" 
                Width="70px"  />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False"
               />
        </td>
    </tr>
</table>
