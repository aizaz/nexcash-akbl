﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveUsersInBulk.ascx.vb" Inherits="DesktopModules_FixUserManagement_SaveUsersInBulk" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<style type="text/css">
.RadGrid_Default{font:12px/16px "segoe ui",arial,sans-serif}.RadGrid_Default{border:1px solid #828282;background:#fff;color:#333}.RadGrid_Default{font:12px/16px "segoe ui",arial,sans-serif}.RadGrid_Default{border:1px solid #828282;background:#fff;color:#333}.RadGrid_Default{font:12px/16px "segoe ui",arial,sans-serif}.RadGrid_Default{border:1px solid #828282;background:#fff;color:#333}.RadGrid_Default .rgHeaderDiv{background:#eee 0 -7550px repeat-x url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif')}.RadGrid_Default .rgHeaderDiv{background:#eee 0 -7550px repeat-x url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif')}.RadGrid_Default .rgHeaderDiv{background:#eee 0 -7550px repeat-x url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif')}.RadGrid_Default .rgMasterTable{font:12px/16px "segoe ui",arial,sans-serif}.RadGrid .rgMasterTable{border-collapse:separate;border-spacing:0}.RadGrid_Default .rgMasterTable{font:12px/16px "segoe ui",arial,sans-serif}.RadGrid .rgMasterTable{border-collapse:separate;border-spacing:0}.RadGrid_Default .rgMasterTable{font:12px/16px "segoe ui",arial,sans-serif}.RadGrid .rgMasterTable{border-collapse:separate;border-spacing:0}.RadGrid .rgClipCells .rgHeader{overflow:hidden}.RadGrid .rgClipCells .rgHeader{overflow:hidden}.RadGrid .rgClipCells .rgHeader{overflow:hidden}.RadGrid_Default .rgHeader{color:#333}.RadGrid_Default .rgHeader{border:0;border-bottom:1px solid #828282;background:#eaeaea 0 -2300px repeat-x url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif')}.RadGrid .rgHeader{padding-top:5px;padding-bottom:4px;text-align:left;font-weight:normal}.RadGrid .rgHeader{padding-left:7px;padding-right:7px}.RadGrid .rgHeader{cursor:default}.RadGrid_Default .rgHeader{color:#333}.RadGrid_Default .rgHeader{border:0;border-bottom:1px solid #828282;background:#eaeaea 0 -2300px repeat-x url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif')}.RadGrid .rgHeader{padding-top:5px;padding-bottom:4px;text-align:left;font-weight:normal}.RadGrid .rgHeader{padding-left:7px;padding-right:7px}.RadGrid .rgHeader{cursor:default}.RadGrid_Default .rgHeader{color:#333}.RadGrid_Default .rgHeader{border:0;border-bottom:1px solid #828282;background:#eaeaea 0 -2300px repeat-x url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif')}.RadGrid .rgHeader{padding-top:5px;padding-bottom:4px;text-align:left;font-weight:normal}.RadGrid .rgHeader{padding-left:7px;padding-right:7px}.RadGrid .rgHeader{cursor:default}.RadGrid_Default .rgHeader a{color:#333}.RadGrid .rgHeader a{text-decoration:none}.RadGrid_Default .rgHeader a{color:#333}.RadGrid .rgHeader a{text-decoration:none}.RadGrid_Default .rgHeader a{color:#333}.RadGrid .rgHeader a{text-decoration:none}.RadGrid_Default .rgFooterDiv{background:#eee}.RadGrid_Default .rgFooterDiv{background:#eee}.RadGrid_Default .rgFooterDiv{background:#eee}.RadGrid_Default .rgFooter{background:#eee}.RadGrid_Default .rgFooter{background:#eee}.RadGrid_Default .rgFooter{background:#eee}
    </style>--%>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
            <br />
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top">
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
           
        </td>
        <td align="left" valign="top">
           
        </td>
    </tr>
    </table>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblFileUpload" runat="server" Text="Select File" 
                CssClass="lbl"></asp:Label>           
        </td>
        <td align="left" valign="top" style="width: 25%">
           
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:HiddenField ID="hfFilePath" runat="server" />
           </td>
        <td align="left" valign="top" style="width: 25%">
           
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:FileUpload ID="fuplUsers" runat="server" />
        </td>
        <td align="left" valign="top" style="width: 25%">
           
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" style="width: 25%">
           
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvfuplUsers" runat="server" ErrorMessage="Please Select File."
                ControlToValidate="fuplUsers" ValidationGroup="User"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
           
        </td>
        <td align="left" valign="top" style="width: 25%">
           </td>
        <td align="left" valign="top" style="width: 25%">
           
        </td>
    </tr>
 </table>
<table id="tblUsersList" runat="server" width="100%" >
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%" colspan="4">

                        &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%" colspan="4">

        <br />
                        <telerik:RadGrid ID="gvUsers" runat="server" CssClass="RadGrid" AllowPaging="false"
                            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                            PageSize="1000">
                            <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="true" />
                            </ClientSettings>
                            <%-- <AlternatingItemStyle CssClass="rgAltRow" />--%>
                            <MasterTableView DataKeyNames="OfficeID" TableLayout="Fixed"
                                NoMasterRecordsText="">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                <Columns>
                                    <telerik:GridBoundColumn DataField="ErrorMessage" HeaderText="Error Message" SortExpression="ErrorMessage">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="UserName" HeaderText="User Name" SortExpression="UserName">
                            </telerik:GridBoundColumn>
                             <telerik:GridBoundColumn DataField="DomainID" HeaderText="Domain ID"
                                SortExpression="DomainID">
                            </telerik:GridBoundColumn>
                             <telerik:GridBoundColumn DataField="AuthenticatedVia" HeaderText="Authenticated Via" SortExpression="AuthenticatedVia">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DisplayName" HeaderText="Display Name" SortExpression="DisplayName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PhoneNo1" HeaderText="Phone No 1" SortExpression="PhoneNo1">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PhoneNo2" HeaderText="Phone No 2" SortExpression="PhoneNo2">
                            </telerik:GridBoundColumn>
                             <telerik:GridBoundColumn DataField="CellNo" HeaderText="Cell No"
                                SortExpression="CellNo"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Location" HeaderText="Location" SortExpression="Location">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Department" HeaderText="Department" SortExpression="Department">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="OfficeCode" HeaderText="OfficeCode" SortExpression="OfficeCode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="EmployeeCode" HeaderText="Employee Code" SortExpression="EmployeeCode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="RoleName" HeaderText="Role Name" SortExpression="RoleName">
                            </telerik:GridBoundColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <%--     <ItemStyle CssClass="rgRow" />--%>
                            <PagerStyle AlwaysVisible="True" />
                            <HeaderStyle CssClass="GridHeader" />
                            <ItemStyle CssClass="GridItem" />
                            <AlternatingItemStyle CssClass="GridItem" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                        <telerik:RadGrid ID="gvUserStatus" runat="server" 
                CssClass="RadGrid" AllowPaging="false"
                            AllowSorting="True" AutoGenerateColumns="False" 
                CellSpacing="0" ShowFooter="True"
                            PageSize="1000">
                            <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="true" />
                            </ClientSettings>
                            <%-- <AlternatingItemStyle CssClass="rgAltRow" />--%>
                            <MasterTableView TableLayout="Fixed"
                                NoMasterRecordsText="">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                <Columns>
                                    <telerik:GridBoundColumn DataField="Status" HeaderText="Status" SortExpression="Status">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="UserName" HeaderText="User Name" SortExpression="UserName">
                            </telerik:GridBoundColumn>
                           
                             <telerik:GridBoundColumn DataField="AuthenticatedVia" HeaderText="Authenticated Via" SortExpression="AuthenticatedVia">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="DisplayName" HeaderText="Display Name" SortExpression="DisplayName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email">
                            </telerik:GridBoundColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <%--     <ItemStyle CssClass="rgRow" />--%>
                            <PagerStyle AlwaysVisible="True" />
                            <HeaderStyle CssClass="GridHeader" />
                            <ItemStyle CssClass="GridItem" />
                            <AlternatingItemStyle CssClass="GridItem" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid><br />
                        
           </td>
    </tr>       
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnProceed" runat="server" Text="Proceed" CssClass="btn" ValidationGroup="User"
                Width="70px"  />
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" ValidationGroup="User"
                Width="70px" CausesValidation="False"  />
           <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False"
               />
        </td>
    </tr>
</table>
