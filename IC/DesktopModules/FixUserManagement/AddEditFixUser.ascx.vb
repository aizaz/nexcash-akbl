﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.DirectoryServices.AccountManagement
Imports System.Security.Cryptography

Partial Class DesktopModules_FixUsers_AddEditFixRolesUser
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private UserCode As String
    Private htRights As Hashtable


#Region "Page Load"
    Protected Sub DesktopModules_Users_SaveUser_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            UserCode = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                'SetAllValuesForBankUser()
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                If UserCode.ToString() = "0" Then
                    Clear()
                    LoadddlBankBranch()
                    LoadddlClearingBankBranch()
                    lblIsApproved.Visible = False
                    chkApproved.Visible = False
                    btnApproved.Visible = False
                    lblPageHeader.Text = "Add Bank User"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                    lnkChangePassword.Visible = False
                    RadInputManager3.GetSettingByBehaviorID("REPassword").Validation.IsRequired = True
                    RadInputManager3.GetSettingByBehaviorID("REConfirmPassword").Validation.IsRequired = True
                    AuthenticationModeWiseSetting()
                Else
                    Clear()
                    LoadddlBankBranch()
                    LoadddlClearingBankBranch()
                    lblIsApproved.Visible = True
                    chkApproved.Visible = True
                    btnApproved.Visible = True
                    lblPageHeader.Text = "Edit Bank User"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    btnApproved.Visible = CBool(htRights("Approve"))
                    txtUserName.ReadOnly = True
                    'ddlUserType.SelectedValue = RoleID
                    Dim cUser As New UserInfo
                    Dim objICUser As New ICUser
                    objICUser.es.Connection.CommandTimeout = 3600
                    cUser = UserController.GetUserById(Me.PortalId, UserCode)
                    objICUser.LoadByPrimaryKey(UserCode)
                    ddlBankBranch.SelectedValue = objICUser.OfficeCode.ToString
                    txtUserName.Text = cUser.Username
                    txtDisplayName.Text = cUser.DisplayName
                    txtEmailAddress.Text = objICUser.Email.ToString()
                    txtCellNo.Text = objICUser.CellNo.ToString()
                    txtPhoneNo2.Text = objICUser.PhoneNo2.ToString()
                    txtPhoneNo1.Text = objICUser.PhoneNo1.ToString()
                    txtCellNo.Text = objICUser.CellNo.ToString()
                    txtLocation.Text = objICUser.Location.ToString()
                    txtDepartment.Text = objICUser.Department.ToString
                    If objICUser.ClearingBranchCode IsNot Nothing Then
                        ddlClearingBranchCode.SelectedValue = objICUser.ClearingBranchCode
                    End If
                    'chk2FAOnApprovals.Checked = objICUser.Is2FARequiredOnApproval
                    chk2FAOnLogin.Checked = objICUser.Is2FARequiredOnLogin
                    chk2FAOnApproval.Checked = objICUser.Is2FARequiredOnApproval
                    chk2FAViaEmailForLogin.Checked = objICUser.IsTwoFAVIAEmailAllowForLogin
                    chk2FAViaSMSForLogin.Checked = objICUser.IsTwoFAVIASMSAllowForLogin
                    chk2FAViaEmailForApproval.Checked = objICUser.IsTwoFAVIAEmailAllowForApproval
                    chk2FAViaSMSForApproval.Checked = objICUser.IsTwoFAVIASMSAllowForApproval
                    If objICUser.IsApproved = True Then
                        chkApproved.Checked = True
                    Else
                        chkApproved.Checked = False
                    End If
                    If Not objICUser.EmployeeCode Is Nothing Then
                        txtEmployeeCode.Text = objICUser.EmployeeCode
                    End If
                    chkActive.Checked = objICUser.IsActive
                    'txtPassword.Text = objICUser.Password
                    'txtConfirmPassword.Text = objICUser.Password
                    'txtPassword.Enabled = False
                    'txtConfirmPassword.Enabled = False
                    RadInputManager3.GetSettingByBehaviorID("REPassword").Validation.IsRequired = False
                    RadInputManager3.GetSettingByBehaviorID("REConfirmPassword").Validation.IsRequired = False
                    If objICUser.AuthenticatedVia = "Application" Then
                        lnkChangePassword.Visible = CBool(htRights("Update"))
                    Else
                        lnkChangePassword.Visible = False
                    End If

                    If Not objICUser.AuthenticatedVia Is Nothing Then
                        ddlAuthenticationMode.SelectedValue = objICUser.AuthenticatedVia
                        AuthenticationModeWiseSetting()
                    End If
                    If objICUser.IsApproved = True Then
                        lblIsApproved.Visible = True
                        btnApproved.Visible = False
                        chkApproved.Enabled = False
                    End If
                End If

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Bank Users Management")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
#Region "Button Events"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then

            Try

                If chk2FAOnApproval.Checked = True Then
                    If chk2FAViaEmailForApproval.Checked = False And chk2FAViaSMSForApproval.Checked = False Then
                        UIUtilities.ShowDialog(Me, "Error", "Please select send 2FA notification for approval VIA SMS or Email.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                If chk2FAOnLogin.Checked = True Then
                    If chk2FAViaEmailForLogin.Checked = False And chk2FAViaSMSForLogin.Checked = False Then
                        UIUtilities.ShowDialog(Me, "Error", "Please select send 2FA notification for login VIA SMS or Email.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If


                If UserCode.ToString() = "0" Then

                    Dim Emailrslt As String = ""
                    Emailrslt = ICUserController.CheckDuplicateEmailAddress(txtEmailAddress.Text, UserCode)
                    If Emailrslt <> "OK" Then
                        UIUtilities.ShowDialog(Me, "Save User", Emailrslt, ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If

                    'Dim Mobilerslt As String = ""
                    'Mobilerslt = ICUserController.CheckDuplicateMobileNumber(txtCellNo.Text, UserCode)
                    'If Mobilerslt <> "OK" Then
                    '    UIUtilities.ShowDialog(Me, "Save User", Mobilerslt, ICBO.IC.Dialogmessagetype.Failure)
                    '    Exit Sub
                    'End If

                    AddDNNUser(False)
                    Clear()


                Else
                    Dim Emailrslt As String = ""
                    Emailrslt = ICUserController.CheckDuplicateEmailAddress(txtEmailAddress.Text, UserCode)
                    If Emailrslt <> "OK" Then
                        UIUtilities.ShowDialog(Me, "Save User", Emailrslt, ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If

                    'Dim Mobilerslt As String = ""
                    'Mobilerslt = ICUserController.CheckDuplicateMobileNumber(txtCellNo.Text, UserCode)
                    'If Mobilerslt <> "OK" Then
                    '    UIUtilities.ShowDialog(Me, "Save User", Mobilerslt, ICBO.IC.Dialogmessagetype.Failure)
                    '    Exit Sub
                    'End If

                    AddDNNUser(True)
                    AuthorizeUser()
                End If


            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'NavigateURL( "Edit","&mid="&Me.ModuleId & "&id=1")
        Response.Redirect(NavigateURL(), False)
    End Sub

    Protected Sub btnApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproved.Click

        Dim objICUser As New ICUser
        objICUser.es.Connection.CommandTimeout = 3600
        Dim user As New UserInfo
        Dim userCtrlChk As New RoleController
        Dim IDRoles As Boolean = False
        Dim member As New UserMembership

        Try
            objICUser.LoadByPrimaryKey(UserCode)
            user = UserController.GetUserById(Me.PortalId, UserCode)
            If objICUser.IsActive = False Then
                UIUtilities.ShowDialog(Me, "Save User", "User can not be approved due to following reason(s): <br>1. User must be active", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                Exit Sub
            End If



            If chkApproved.Checked Then

                If Me.UserInfo.IsSuperUser = False Then
                    If objICUser.CreateBy = Me.UserId Then
                        UIUtilities.ShowDialog(Me, "Save User", "User can not be approved due to following reason(s): <br>1. User must be approved other than maker.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    Else
                        ICUserController.ApproveUser(UserCode.ToString(), Me.UserId.ToString(), chkApproved.Checked, Me.UserInfo.Username.ToString(), objICUser.UserType.ToString)
                        AuthorizeUser()
                        UIUtilities.ShowDialog(Me, "Save User", "User Approved.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    End If
                Else
                    ICUserController.ApproveUser(UserCode.ToString(), Me.UserId.ToString(), chkApproved.Checked, Me.UserInfo.Username.ToString, objICUser.UserType.ToString)
                    AuthorizeUser()
                    UIUtilities.ShowDialog(Me, "Save User", "User Approved.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                End If
            Else
                If Me.UserInfo.IsSuperUser = False Then
                    If objICUser.CreateBy = Me.UserId Then
                        UIUtilities.ShowDialog(Me, "Save User", "User not be un-approved due to following reason(s): <br>1. User must be approved other than maker.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    Else
                        ICUserController.ApproveUser(UserCode.ToString(), Me.UserId.ToString(), chkApproved.Checked, Me.UserInfo.Username.ToString(), objICUser.UserType.ToString)
                        AuthorizeUser()
                        UIUtilities.ShowDialog(Me, "Save User", "User not Approved.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    End If
                Else
                    ICUserController.ApproveUser(UserCode.ToString(), Me.UserId.ToString(), chkApproved.Checked, Me.UserInfo.Username.ToString, objICUser.UserType.ToString)
                    AuthorizeUser()
                    UIUtilities.ShowDialog(Me, "Save User", "User not Approved.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                End If

            End If
            'Intelligenes.EmailUtilities.GenerateEmailsByEvent(0, 0, Intelligenes.EmailUtilities.EventType.User_Approved, FRCUtilities.GetSettingValue("Applicationurl"), cUser.UserID)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub AddDNNUser(ByVal IsUpdated As Boolean)
        Try
            Dim HashedPassword As String = ""
            If IsUpdated = False Then
                'If ddlAuthenticationMode.SelectedValue.ToString() = "Active Directory" Then
                '    txtPassword.Text = GetRandomPassword().ToString()
                '    txtConfirmPassword.Text = GetRandomPassword().ToString()
                'End If
                Dim user As New UserInfo
                user.PortalID = Me.PortalId
                user.IsSuperUser = False
                user.FirstName = txtDisplayName.Text.ToString()
                user.LastName = ""
                user.Email = txtEmailAddress.Text.ToString()
                user.Username = txtUserName.Text.ToString()
                user.DisplayName = txtDisplayName.Text.ToString()
                user.Profile.PreferredLocale = PortalSettings.DefaultLanguage
                user.Profile.TimeZone = PortalSettings.TimeZoneOffset
                user.Profile.FirstName = txtDisplayName.Text.ToString()
                user.Profile.LastName = ""

                Dim member As New UserMembership
                member.Approved = False
                member.CreatedDate = Now
                member.Email = user.Email
                member.IsOnLine = False
                member.Username = user.Username
                'member.Password = GetRandomPassword().ToString()
                member.Password = ICUserPasswordHistoryController.Generate(8, 8).ToString()
                user.Membership = member

                Dim status As DotNetNuke.Security.Membership.UserCreateStatus
                status = UserController.CreateUser(user)
                ' Add new password in password history table
                HashedPassword = ICUserPasswordHistoryController.GenerateMD5Hash(member.Password.ToString.Trim)
                HashedPassword = HashedPassword.Trim.ToLower.ToString()
                Dim objICUserPWDHistory As New ICUserPasswordHistory
                objICUserPWDHistory.UserID = user.UserID
                objICUserPWDHistory.Password = HashedPassword
                objICUserPWDHistory.CreatedDate = Date.Now
                objICUserPWDHistory.CreatedBy = Me.UserId
                ICUserPasswordHistoryController.AddICUserPasswordHistory(objICUserPWDHistory)
                If status = DotNetNuke.Security.Membership.UserCreateStatus.Success Then
                    'user created
                    Try
                        Dim objICUSer As New ICUser
                        objICUSer.es.Connection.CommandTimeout = 3600
                        objICUSer.UserID = user.UserID
                        objICUSer.UserName = txtUserName.Text.ToString()
                        objICUSer.DisplayName = txtDisplayName.Text.ToString()
                        objICUSer.Password = member.Password.ToString.Trim
                        objICUSer.Email = txtEmailAddress.Text.ToString()
                        objICUSer.CellNo = txtCellNo.Text.ToString()
                        objICUSer.PhoneNo1 = txtPhoneNo1.Text.ToString()
                        objICUSer.PhoneNo2 = txtPhoneNo2.Text.ToString()
                        objICUSer.IsActive = chkActive.Checked
                        objICUSer.Location = txtLocation.Text.ToString()
                        objICUSer.Department = txtDepartment.Text.ToString
                        objICUSer.OfficeCode = ddlBankBranch.SelectedValue.ToString
                        objICUSer.AuthenticatedVia = ddlAuthenticationMode.SelectedValue.ToString()
                        objICUSer.EmployeeCode = txtEmployeeCode.Text.ToString
                        If ddlClearingBranchCode.SelectedValue.ToString <> "0" Then
                            objICUSer.ClearingBranchCode = ddlClearingBranchCode.SelectedValue.ToString
                        End If

                        objICUSer.CreateBy = Me.UserId
                        objICUSer.CreateDate = Date.Now
                        objICUSer.Creater = Me.UserId
                        objICUSer.CreationDate = Date.Now
                        objICUSer.UserType = "Bank User"
                        If chk2FAOnApproval.Checked = True Then
                            objICUSer.Is2FARequiredOnApproval = True
                        Else
                            objICUSer.Is2FARequiredOnApproval = False
                        End If
                        If chk2FAOnLogin.Checked = True Then
                            objICUSer.Is2FARequiredOnLogin = True
                        Else
                            objICUSer.Is2FARequiredOnLogin = False
                        End If
                        If chk2FAViaEmailForLogin.Checked = True Then
                            objICUSer.IsTwoFAVIAEmailAllowForLogin = True
                        Else
                            objICUSer.IsTwoFAVIAEmailAllowForLogin = False
                        End If
                        If chk2FAViaSMSForLogin.Checked = True Then
                            objICUSer.IsTwoFAVIASMSAllowForLogin = True
                        Else
                            objICUSer.IsTwoFAVIASMSAllowForLogin = False
                        End If
                        If chk2FAViaSMSForApproval.Checked = True Then
                            objICUSer.IsTwoFAVIASMSAllowForApproval = True
                        Else
                            objICUSer.IsTwoFAVIASMSAllowForApproval = False
                        End If
                        If chk2FAViaEmailForApproval.Checked = True Then
                            objICUSer.IsTwoFAVIAEmailAllowForApproval = True
                        Else
                            objICUSer.IsTwoFAVIAEmailAllowForApproval = False
                        End If
                        ICUserController.AddICUser(objICUSer, False, Me.UserId.ToString, Me.UserInfo.Username.ToString, "", "Bank User")

                        'Intelligenes.EmailUtilities.GenerateEmailsByEvent(0, 0, Intelligenes.EmailUtilities.EventType.User_Creation, FRCUtilities.GetSettingValue("ApplicationUrl"), cUser.UserID)
                        'Intelligenes.EmailUtilities.GenerateEmailsByEvent(0, 0, Intelligenes.EmailUtilities.EventType.New_Password, FRCUtilities.GetSettingValue("ApplicationUrl"), cUser.User
                        UIUtilities.ShowDialog(Me, "Save User", "User added successfully.", ICBO.IC.Dialogmessagetype.Success)
                        Clear()
                        'EmailUtilities.UserCreated(objICUSer, txtPassword.Text.ToString)
                        'SMSUtilities.UserCreated(objICUSer, txtPassword.Text.ToString)
                    Catch ex As Exception
                        ProcessModuleLoadException(Me, ex, False)
                        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                    End Try
                Else
                    Select Case status
                        Case DotNetNuke.Security.Membership.UserCreateStatus.UsernameAlreadyExists
                            UIUtilities.ShowDialog(Me, "Registration", "User already exists.", Dialogmessagetype.Failure, NavigateURL())
                            'UIUtilities.ShowDialog(Me, "Registration", "User Name is already registered with us.", Dialogmessagetype.Failure, NavigateURL())
                            'UI.Skins.Skin.AddModuleMessage(Me, "Your card is already registered with us.", Skins.Controls.ModuleMessage.ModuleMessageType.RedError)
                            ' Me.lblmsg.Text = "Your email address is already registered with us."
                        Case DotNetNuke.Security.Membership.UserCreateStatus.DuplicateEmail
                            'Me.lblmsg.Text = "Your email address is already registered with us."
                            UIUtilities.ShowDialog(Me, "Registration", "Your email address is already registered with us.", Dialogmessagetype.Failure, NavigateURL())
                            ' UI.Skins.Skin.AddModuleMessage(Me, "Your email address is already registered with us.", Skins.Controls.ModuleMessage.ModuleMessageType.RedError)
                        Case DotNetNuke.Security.Membership.UserCreateStatus.UserAlreadyRegistered
                            'Me.lblmsg.Text = "Your email address is already registered with us."
                            UIUtilities.ShowDialog(Me, "Registration", "User already exists.", Dialogmessagetype.Failure, NavigateURL())
                            ' UI.Skins.Skin.AddModuleMessage(Me, "Your card is already registered with us.", Skins.Controls.ModuleMessage.ModuleMessageType.RedError)
                        Case Else
                            UIUtilities.ShowDialog(Me, "Registration", "Your profile could not be created due to the following reason: " & status.ToString, Dialogmessagetype.Failure, NavigateURL())
                            ' UI.Skins.Skin.AddModuleMessage(Me, "Your profile could not be created due to the following reason:" & status.ToString, Skins.Controls.ModuleMessage.ModuleMessageType.RedError)
                            ' Me.lblmsg.Text = "Your comment could not be saved due to the following reason:" & status.ToString
                    End Select
                End If

            Else
                Dim user As New UserInfo
                user = UserController.GetUserById(Me.PortalId, UserCode)
                user.PortalID = Me.PortalId
                user.IsSuperUser = False
                user.FirstName = txtDisplayName.Text.ToString()
                user.LastName = ""
                user.Email = txtEmailAddress.Text.ToString()
                '                user.Username = txtUserName.Text.ToString()
                user.DisplayName = txtDisplayName.Text.ToString()
                '               user.Profile.PreferredLocale = PortalSettings.DefaultLanguage
                '              user.Profile.TimeZone = PortalSettings.TimeZoneOffset

                user.Profile.FirstName = txtDisplayName.Text.ToString()
                user.Profile.LastName = ""

                Dim userCtrl As New UserController
                userCtrl.UpdateUser(Me.PortalId, user)
                'If txtPassword.Enabled = True Then
                '    ' Check Password History
                '    HashedPassword = ICUserPasswordHistoryController.GenerateMD5Hash(txtPassword.Text.ToString().Trim())
                '    HashedPassword = HashedPassword.Trim.ToLower.ToString()

                '    If ICUserPasswordHistoryController.CheckUserPasswordHistory(user.UserID, HashedPassword) = False Then
                '        UIUtilities.ShowDialog(Me, "Save User", "The new password should not be same as previous two passwords, please try again", Dialogmessagetype.Failure)
                '        Exit Sub
                '    End If
                '    ' Add new password in password history table
                '    Dim objICUserPWDHistory As New ICUserPasswordHistory
                '    objICUserPWDHistory.UserID = user.UserID
                '    objICUserPWDHistory.Password = HashedPassword
                '    objICUserPWDHistory.CreatedDate = Date.Now
                '    objICUserPWDHistory.CreatedBy = Me.UserId
                '    ICUserPasswordHistoryController.AddICUserPasswordHistory(objICUserPWDHistory)
                '    userCtrl.SetPassword(user, txtPassword.Text.ToString())

                'End If
                'user updated
                Try
                    Dim objICUSer As New ICUser
                    Dim objFromICUser As New ICUser
                    Dim Action As String = ""

                    objFromICUser.LoadByPrimaryKey(user.UserID)

                    objICUSer.UserID = user.UserID
                    objICUSer.UserName = txtUserName.Text.ToString()
                    objICUSer.DisplayName = txtDisplayName.Text.ToString()
                    'If txtPassword.Enabled = True Then
                    '    objICUSer.Password = txtPassword.Text.ToString()
                    'Else
                    '    objICUSer.Password = objFromICUser.Password

                    'End If
                    objICUSer.Email = txtEmailAddress.Text.ToString()
                    objICUSer.CellNo = txtCellNo.Text.ToString()
                    objICUSer.PhoneNo1 = txtPhoneNo1.Text.ToString()
                    objICUSer.PhoneNo2 = txtPhoneNo2.Text.ToString()
                    objICUSer.Location = txtLocation.Text.ToString()
                    objICUSer.UserType = "Bank User"
                    objICUSer.Department = txtDepartment.Text.ToString
                    objICUSer.IsActive = chkActive.Checked
                    objICUSer.OfficeCode = ddlBankBranch.SelectedValue.ToString
                    objICUSer.AuthenticatedVia = ddlAuthenticationMode.SelectedValue.ToString()
                    If ddlClearingBranchCode.SelectedValue.ToString <> "0" Then
                        objICUSer.ClearingBranchCode = ddlClearingBranchCode.SelectedValue.ToString
                    End If
                    objICUSer.CreateBy = Me.UserId
                    objICUSer.CreateDate = Date.Now
                    If chk2FAOnApproval.Checked = True Then
                        objICUSer.Is2FARequiredOnApproval = True
                    Else
                        objICUSer.Is2FARequiredOnApproval = False
                    End If
                    If chk2FAOnLogin.Checked = True Then
                        objICUSer.Is2FARequiredOnLogin = True
                    Else
                        objICUSer.Is2FARequiredOnLogin = False
                    End If
                    If chk2FAViaEmailForLogin.Checked = True Then
                        objICUSer.IsTwoFAVIAEmailAllowForLogin = True
                    Else
                        objICUSer.IsTwoFAVIAEmailAllowForLogin = False
                    End If
                    If chk2FAViaSMSForLogin.Checked = True Then
                        objICUSer.IsTwoFAVIASMSAllowForLogin = True
                    Else
                        objICUSer.IsTwoFAVIASMSAllowForLogin = False
                    End If
                    If chk2FAViaEmailForApproval.Checked = True Then
                        objICUSer.IsTwoFAVIAEmailAllowForApproval = True
                    Else
                        objICUSer.IsTwoFAVIAEmailAllowForApproval = False
                    End If
                    If chk2FAViaSMSForApproval.Checked = True Then
                        objICUSer.IsTwoFAVIASMSAllowForApproval = True
                    Else
                        objICUSer.IsTwoFAVIASMSAllowForApproval = False
                    End If
                    objICUSer.EmployeeCode = txtEmployeeCode.Text.ToString

                    Action = "Bank User " & objICUSer.UserName & " Updated : DisplayName from[" & objFromICUser.DisplayName & "] to [" & objICUSer.DisplayName & "] ; "
                    'If txtPassword.Enabled = True Then
                    '    Action += "Password Changed ; "
                    'End If
                    Action += "Email from[" & objFromICUser.Email & "] to [" & objICUSer.Email & "] ; "
                    Action += "CellNo from[" & objFromICUser.CellNo & "] to [" & objICUSer.CellNo & "] ; "
                    Action += "PhoneNo1 from[" & objFromICUser.PhoneNo1 & "] to [" & objICUSer.PhoneNo1 & "] ; "
                    Action += "Clearing Branch Code from[" & objFromICUser.ClearingBranchCode & "] to [" & objICUSer.ClearingBranchCode & "] ; "
                    Action += "PhoneNo2 from[" & objFromICUser.PhoneNo2 & "] to [" & objICUSer.PhoneNo2 & "] ; "
                    Action += "Location from[" & objFromICUser.Location & "] to [" & objICUSer.Location & "] ; "
                    Action += "Department from[" & objFromICUser.Department & "] to [" & objICUSer.Department & "] ; "
                    Action += "Employee Code from[" & objFromICUser.EmployeeCode & "] to [" & objICUSer.EmployeeCode & "] ; "
                    Action += "Notification sent from[" & objFromICUser.IsNotificationSent & "] to [" & objICUSer.IsNotificationSent & "] ; "
                    Action += "2FA on Login from[" & objFromICUser.Is2FARequiredOnLogin & "] to [" & objICUSer.Is2FARequiredOnLogin & "] ; "
                    Action += "2FA on Approval from[" & objFromICUser.Is2FARequiredOnApproval & "] to [" & objICUSer.Is2FARequiredOnApproval & "] ; "
                    Action += "2FA on Login VIA SMS from[" & objFromICUser.IsTwoFAVIASMSAllowForLogin & "] to [" & objICUSer.IsTwoFAVIASMSAllowForLogin & "] ; "
                    Action += "2FA on Login VIA Email from[" & objFromICUser.IsTwoFAVIAEmailAllowForLogin & "] to [" & objICUSer.IsTwoFAVIAEmailAllowForLogin & "] ; "
                    Action += "2FA on Approval VIA SMS from[" & objFromICUser.IsTwoFAVIASMSAllowForApproval & "] to [" & objICUSer.IsTwoFAVIASMSAllowForApproval & "] ; "
                    Action += "2FA on Approval VIA Email from[" & objFromICUser.IsTwoFAVIAEmailAllowForApproval & "] to [" & objICUSer.IsTwoFAVIAEmailAllowForApproval & "] ; "
                    Action += "Bank Branch from[" & objFromICUser.UpToICOfficeByOfficeCode.OfficeName.ToString & "] to [" & objICUSer.UpToICOfficeByOfficeCode.OfficeName.ToString & "] ; "
                    Action += "Authentication Mode from[" & objFromICUser.AuthenticatedVia & "] to [" & objICUSer.AuthenticatedVia & "] ; "

                    ICUserController.AddICUser(objICUSer, True, Me.UserId.ToString, Me.UserInfo.Username.ToString, Action.ToString, "Bank User")

                    AuthorizeUser()

                    UIUtilities.ShowDialog(Me, "Save User", "User Updated Successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())

                    If objICUSer.LoadByPrimaryKey(UserCode) Then
                        If objICUSer.IsNotificationSent = True Then
                            EmailUtilities.UserUpdated(objICUSer)
                            SMSUtilities.UserUpdated(objICUSer)
                            EmailUtilities.UserInactivated(objICUSer)
                            SMSUtilities.UserInactivated(objICUSer)

                            'If txtPassword.Enabled = True And txtPassword.Text <> "" Then
                            '    Dim password As String = ""
                            '    Dim password2 As String = ""
                            '    password2 = DotNetNuke.Entities.Users.UserController.GetPassword(user, password)
                            '    If password2 <> txtPassword.Text.ToString Then
                            '        If Me.UserId <> UserCode Then
                            '            EmailUtilities.PasswordChangeByAdministrator(objICUSer, password2)
                            '            SMSUtilities.PasswordChangeByAdministrator(objICUSer, password2)
                            '        Else
                            '            EmailUtilities.PasswordChange(objICUSer, password2)
                            '            SMSUtilities.PasswordChange(objICUSer, password2)
                            '        End If
                            '    End If
                            'End If
                        End If
                    End If
                    Clear()


                    'Intelligenes.EmailUtilities.GenerateEmailsByEvent(0, 0, Intelligenes.EmailUtilities.EventType.User_UpDated_Email, FRCUtilities.GetSettingValue("ApplicationUrl"), cUser.UserID)
                    'Intelligenes.EmailUtilities.GenerateEmailsByEvent(0, 0, Intelligenes.EmailUtilities.EventType.User_UpDated_Password_Email, FRCUtilities.GetSettingValue("ApplicationUrl"), cUser.UserID)




                Catch ex As Exception
                    ProcessModuleLoadException(Me, ex, False)
                    UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                End Try
            End If
        Catch ex As Exception
            UIUtilities.ShowDialog(Me, "Registration", ex.Message, Dialogmessagetype.Failure)
        End Try
    End Sub


   





#End Region


#Region "Other Functions/Routines"
    Private Sub LoadddlBankBranch()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBankBranch.AutoPostBack = False
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBankBranch.Items.Clear()
            ddlBankBranch.Items.Add(lit)
            ddlBankBranch.AppendDataBoundItems = True
            ddlBankBranch.DataSource = ICOfficeController.GetPrincipalBankBranchActiveAndApproveForAddUser
            ddlBankBranch.DataTextField = "OfficeName"
            ddlBankBranch.DataValueField = "OfficeID"
            ddlBankBranch.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlClearingBankBranch()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBankBranch.AutoPostBack = False
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlClearingBranchCode.Items.Clear()
            ddlClearingBranchCode.Items.Add(lit)
            ddlClearingBranchCode.AppendDataBoundItems = True
            ddlClearingBranchCode.DataSource = ICOfficeController.GetPrincipalBankBranchActiveAndApproveForAddUser
            ddlClearingBranchCode.DataTextField = "OfficeName"
            ddlClearingBranchCode.DataValueField = "OfficeCode"
            ddlClearingBranchCode.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            ' str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function
    Private Sub Clear()
        txtUserName.Text = ""
        txtDisplayName.Text = ""
        'txtPassword.Text = ""
        'txtConfirmPassword.Text = ""
        txtEmailAddress.Text = ""
        txtCellNo.Text = ""
        txtPhoneNo2.Text = ""
        txtPhoneNo1.Text = ""
        txtLocation.Text = ""
        txtDepartment.Text = ""
        txtEmployeeCode.Text = ""
        'chk2FAOnApprovals.Checked = False
        chk2FAOnLogin.Checked = False
        chk2FAViaEmailForLogin.Checked = False
        chk2FAViaSMSForLogin.Checked = False
        chk2FAOnApproval.Checked = False
        chk2FAViaEmailForApproval.Checked = False
        chk2FAViaSMSForApproval.Checked = False
        ddlBankBranch.ClearSelection()
        ddlAuthenticationMode.ClearSelection()
        chkActive.Checked = True

    End Sub
    Private Sub AuthorizeUser()
        Dim objICUser As New ICUser()

        Try
            If objICUser.LoadByPrimaryKey(UserCode.ToString()) Then
                Dim user As New UserInfo
                Dim member As New UserMembership
                user = UserController.GetUserById(Me.PortalId, UserCode)
                If objICUser.IsApproved = True And objICUser.IsActive = True Then
                    member.Approved = True
                    user.Membership = member

                    UserController.UpdateUser(Me.PortalId, user)
                Else
                    member.Approved = False

                    user.Membership = member


                    UserController.UpdateUser(Me.PortalId, user)
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    'Private Sub AssignFixRoleToUser(ByVal RoleID As String, ByVal UserCode As String)
    '    Dim userCtrl As New RoleController
    '    Dim strChk As String()
    '    Dim IDRoles As Boolean = False
    '    If CheckIsAdminOrSuperUser() = False Then
    '        strChk = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
    '        For i = 0 To strChk.Length - 1
    '            If strChk(i).ToString().Trim() = "ID Management User" Then
    '                IDRoles = True
    '            End If
    '            If strChk(i).ToString().Trim() = "ID Approval User" Then
    '                IDRoles = True
    '            End If
    '            If strChk(i).ToString().Trim() = "Administrators" Then
    '                IDRoles = True
    '            End If
    '        Next
    '    End If

    '    FRCRoleController.AssignRoleToUser(RoleID, Me.PortalId, UserCode)
    '    If IDRoles = True Then
    '        FRCUtilities.AddAuditTrail("UserRoles : " & txtUserName.Text.ToString() & " Assign " & ddlRole.SelectedItem.Text.ToString() & " Role.", "UserRole", UserCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
    '    Else
    '        FRCUtilities.AddAuditTrail("UserRoles : " & txtUserName.Text.ToString() & " Assign " & ddlRole.SelectedItem.Text.ToString() & " Role.", "FixUserRole", UserCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
    '    End If


    'End Sub
    Protected Sub lnkChangePassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkChangePassword.Click
        Try
            Dim objICUser As New ICUser
            Dim userCtrl As New UserController
            Dim user As New UserInfo
            Dim Password As String
            Dim Password2, NewPassWord As String
            Dim HashedPassword As String = ""
            Password = Nothing
            Password2 = Nothing
            NewPassWord = Nothing

            objICUser.LoadByPrimaryKey(UserCode)

            user = UserController.GetUserById(Me.PortalId, UserCode.ToString)
            NewPassWord = GetRandomPassword.ToString.Trim()
            userCtrl.SetPassword(user, NewPassWord)
            Password2 = DotNetNuke.Entities.Users.UserController.GetPassword(user, Password)

            '' Check Password History
            'HashedPassword = ICUserPasswordHistoryController.GenerateMD5Hash(member.Password.ToString().Trim())
            'HashedPassword = HashedPassword.Trim.ToLower.ToString()

            'If ICUserPasswordHistoryController.CheckUserPasswordHistory(user.UserID, HashedPassword) = False Then
            '    UIUtilities.ShowDialog(Me, "Save User", "The new password should not be same as previous two passwords, please try again", Dialogmessagetype.Failure)
            '    Exit Sub
            'End If
            '' Add new password in password history table
            'Dim objICUserPWDHistory As New ICUserPasswordHistory
            'objICUserPWDHistory.UserID = user.UserID
            'objICUserPWDHistory.Password = HashedPassword
            'objICUserPWDHistory.CreatedDate = Date.Now
            'objICUserPWDHistory.CreatedBy = Me.UserId
            'ICUserPasswordHistoryController.AddICUserPasswordHistory(objICUserPWDHistory)
            If objICUser.IsNotificationSent = True Then
                If Me.UserInfo.UserID = UserCode Then
                    EmailUtilities.PasswordChange(objICUser, Password2)
                    SMSUtilities.PasswordChange(objICUser, Password2)
                Else
                    EmailUtilities.PasswordChangeByAdministrator(objICUser, Password2)
                    SMSUtilities.PasswordChangeByAdministrator(objICUser, Password2)
                End If
            End If
            ICUtilities.AddAuditTrail("Password for user [ " & UserCode & " ] [ " & objICUser.UserName & " ] is updated. Action was taken by user [ " & Me.UserInfo.UserID & " ] [ " & Me.UserInfo.Username & " ].", "Bank User", UserCode, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "UPDATE")
            UIUtilities.ShowDialog(Me, "Bank User", "Password updated sucessfully", Dialogmessagetype.Success, NavigateURL())
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

    Protected Sub ddlAuthenticationMode_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlAuthenticationMode.SelectedIndexChanged
        AuthenticationModeWiseSetting()
    End Sub
    Private Sub AuthenticationModeWiseSetting()
        Dim objICUser As New ICUser
        If ddlAuthenticationMode.SelectedValue.ToString() = "Active Directory" Then
            lnkChangePassword.Visible = False
        Else
            If UserCode.ToString() = "0" Then
                lnkChangePassword.Visible = False
            Else
                objICUser.LoadByPrimaryKey(UserCode)
                If objICUser.IsNotificationSent = True Then
                    lnkChangePassword.Visible = CBool(htRights("Update"))
                Else
                    lnkChangePassword.Visible = False
                End If
            End If
        End If
    End Sub
    Private Function GetRandomPassword() As String
        Return Membership.GeneratePassword(7, 1).ToString()

    End Function
End Class