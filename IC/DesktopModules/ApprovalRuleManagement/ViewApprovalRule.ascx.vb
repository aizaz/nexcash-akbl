﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports System.Web
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Partial Class DesktopModules_ApprovalRuleManagement_ViewApprovalRule
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private htRights As Hashtable
#Region "Page Load"

   
    Protected Sub DesktopModules_AccountsManagement_ViewAccounts_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlCompany()
                LoadddlPaymentNature()
                LoadAllApprovalRule(True)
                btnAddAppRule.Visible = CBool(htRights("Add"))

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
#End Region
#Region "Grid View Events"

    Protected Sub gvAPNFUTemplate_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvAPPRuleList.ItemCommand

        Try
            Dim objICApprovalRule As New ICApprovalRule


            If e.CommandName.ToString = "del" Then
                If Page.IsValid Then
                    If objICApprovalRule.LoadByPrimaryKey(e.CommandArgument.ToString) Then
                        ICApprovalRuleManagementController.DeleteApprovalRule(e.CommandArgument.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                        LoadAllApprovalRule(True)
                        UIUtilities.ShowDialog(Me, "Aproval Rule Management", "Aproval rule deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Else
                        Throw New Exception("Invalid aproval rule id")
                    End If
                End If
            ElseIf e.CommandName.ToString = "Copy" Then
                ICApprovalRuleManagementController.CopyApprovalRule(e.CommandArgument.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                LoadAllApprovalRule(True)
                UIUtilities.ShowDialog(Me, "Aproval Rule Management", "Aproval rule copied successfully.", ICBO.IC.Dialogmessagetype.Success)
            End If

        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Error ", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                LoadAllApprovalRule(False)
                Exit Sub
            Else
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End If
           
        End Try
    End Sub

    Protected Sub gvAccountsDetails_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAPPRuleList.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                ' Dim arrPageSizes() As String = {"1", "2", "5", "10", "50"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAPPRuleList.MasterTableView.ClientID)
                Next
                'If gvAccountsDetails.Items.Count > 0 Then
                '    myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
                'End If
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


#End Region
#Region "Button Events"
    Protected Sub btnSaveAccounts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddAppRule.Click
        Page.Validate()
        If Page.IsValid Then

            Response.Redirect(NavigateURL("SaveApprovalRule", "&mid=" & Me.ModuleId & "&AppruleID=0"), False)
        End If
    End Sub
#End Region
#Region "Other Functions/Routines"
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Approval Rule Management")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    

    Private Sub LoadAllApprovalRule(ByVal DoDataBind As Boolean)
        Try
            ICApprovalRuleManagementController.GetAllApprovalRuleForRadGrid(ddlPaymentNature.SelectedValue.ToString, Me.gvAPPRuleList.CurrentPageIndex + 1, Me.gvAPPRuleList.PageSize, DoDataBind, Me.gvAPPRuleList, ddlGroup.SelectedValue.ToString, ddlcompany.SelectedValue.ToString)

            If gvAPPRuleList.Items.Count > 0 Then
                gvAPPRuleList.Visible = True
                lblRNF.Visible = False
                gvAPPRuleList.Columns(4).Visible = CBool(htRights("Update"))
                gvAPPRuleList.Columns(5).Visible = CBool(htRights("Delete"))
                gvAPPRuleList.Columns(6).Visible = CBool(htRights("Copy"))
                gvAPPRuleList.Columns(7).Visible = CBool(htRights("Manage Conditions"))

            Else
                gvAPPRuleList.Visible = False
                lblRNF.Visible = True

            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try




    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlcompany.Items.Clear()
            ddlcompany.Items.Add(lit)
            ddlcompany.AppendDataBoundItems = True
            ddlcompany.DataSource = ICCompanyController.GetCompanyActiveAndApproveByGroupCodeAfterInitialTagging(ddlGroup.SelectedValue.ToString)
            ddlcompany.DataValueField = "CompanyCode"
            ddlcompany.DataTextField = "CompanyName"
            ddlcompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlPaymentNature()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlPaymentNature.Items.Clear()
            ddlPaymentNature.Items.Add(lit)
            ddlPaymentNature.AppendDataBoundItems = True
            ddlPaymentNature.DataSource = ICAccountPaymentNatureController.GetPaymentNatureByCompanyCode(ddlcompany.SelectedValue.ToString)
            ddlPaymentNature.DataValueField = "PaymentNatureCode"
            ddlPaymentNature.DataTextField = "PaymentNature"
            ddlPaymentNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
            LoadddlPaymentNature()
            LoadAllApprovalRule(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAPNFUTemplate_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAPPRuleList.NeedDataSource
        Try
            LoadAllApprovalRule(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAPNFUTemplate_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvAPPRuleList.PageIndexChanged
        Try
            gvAPPRuleList.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlcompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcompany.SelectedIndexChanged
        Try
            LoadddlPaymentNature()
            LoadAllApprovalRule(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub gvAPNFUTemplate_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAPPRuleList.ItemDataBound
        'Dim chkProcessAll As CheckBox

        'If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
        '    chkProcessAll = New CheckBox
        '    chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
        '    chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAPPRuleList.MasterTableView.ClientID & "','0');"
        'End If
    End Sub
    Private Function CheckGVAPNFUTemplateSelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvAPNFUTemplateRow As GridDataItem In gvAPPRuleList.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvAPNFUTemplateRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub ddlPaymentNature_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPaymentNature.SelectedIndexChanged
        Try
            LoadAllApprovalRule(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
