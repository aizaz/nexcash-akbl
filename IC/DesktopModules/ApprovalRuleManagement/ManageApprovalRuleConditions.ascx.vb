﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports Telerik.Web.UI

Partial Class DesktopModules_ApprovalRuleManagement_ManageApprovalRuleConditions
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ApprovalRuleID As String
    Private CompanyCode As String
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_AccountsManagement_SaveAccount_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            ApprovalRuleID = Request.QueryString("AppruleID")
            CompanyCode = Request.QueryString("CompanyCode")
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ClearAndControls()
                ClearOrControls()
                btnSaveAnd.Visible = CBool(htRights("Add"))
                btnSaveOR.Visible = CBool(htRights("Add"))
                LoadddlApprovalAndGroup()
                LoadddlApprovalORGroup()

                LoadAndApprovalConditions(True)
                LoadOrApprovalConditions(True)

            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Approval Rule Management")
            ViewState("htRights") = htRights

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ClearAndControls()
        tblChkListUserCount.Style.Add("display", "none")
        tblCount.Style.Add("display", "none")
        ddlSelAndCriteria.ClearSelection()
        ddlAppAndGroup.ClearSelection()
        chklstClientUser.DataSource = Nothing
        chklstClientUser.DataBind()
        txtAndCount.Text = ""
        RadInputManager1.GetSettingByBehaviorID("radAndCount").Validation.IsRequired = False
        cvAndAppUser.Enabled = False
    End Sub
    Private Sub ClearOrControls()
        tblChkListUserCountOr.Style.Add("display", "none")
        tblCount0.Style.Add("display", "none")
        ddlSelOrCriteria.ClearSelection()
        ddlAppOrGroup.ClearSelection()
        chklstClientUserOR.DataSource = Nothing
        chklstClientUserOR.DataBind()
        txtOrCount.Text = ""
        'chklstClientUserOR.Text = ""
        RadInputManager1.GetSettingByBehaviorID("radOrCount").Validation.IsRequired = False
        cvORAppUser.Enabled = False
    End Sub


#Region "Drop Down Events"
    Private Sub LoadddlApprovalAndGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlAppAndGroup.Items.Clear()
            ddlAppAndGroup.Items.Add(lit)
            ddlAppAndGroup.AppendDataBoundItems = True
            ddlAppAndGroup.DataSource = ICApprovalGroupController.GetAllActiveApprovalGroupsByCompanyCode(CompanyCode)
            ddlAppAndGroup.DataTextField = "GroupName"
            ddlAppAndGroup.DataValueField = "ApprovalGroupID"
            ddlAppAndGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlApprovalORGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlAppOrGroup.Items.Clear()
            ddlAppOrGroup.Items.Add(lit)
            ddlAppOrGroup.AppendDataBoundItems = True
            ddlAppOrGroup.DataSource = ICApprovalGroupController.GetAllActiveApprovalGroupsByCompanyCode(CompanyCode)
            ddlAppOrGroup.DataTextField = "GroupName"
            ddlAppOrGroup.DataValueField = "ApprovalGroupID"
            ddlAppOrGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadAndApprovalConditions(ByVal DoDataBind As Boolean)
        Try
            ICApprovalRuleConditionsController.GetAllApprovalRuleConditionsByApprovalRuleIDAndType(Me.gvAndConditions.CurrentPageIndex + 1, Me.gvAndConditions.PageSize, DoDataBind, Me.gvAndConditions, ApprovalRuleID, "And")
            If gvAndConditions.Items.Count > 0 Then
                gvAndConditions.Visible = True
                gvAndConditions.Columns(5).Visible = CBool(htRights("Delete"))
                lblRNF.Visible = False
            Else
                gvAndConditions.Visible = False
                lblRNF.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadOrApprovalConditions(ByVal DoDataBind As Boolean)
        Try
            ICApprovalRuleConditionsController.GetAllApprovalRuleConditionsByApprovalRuleIDAndType(Me.gvOrConditions.CurrentPageIndex + 1, Me.gvOrConditions.PageSize, DoDataBind, Me.gvOrConditions, ApprovalRuleID, "Or")
            If gvOrConditions.Items.Count > 0 Then
                gvOrConditions.Visible = True
                gvOrConditions.Columns(5).Visible = CBool(htRights("Delete"))
                lblRNFOr.Visible = False
            Else
                gvOrConditions.Visible = False
                lblRNFOr.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAndConditions_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles gvAndConditions.ItemCommand
        Try
            If e.CommandName = "del" Then
                
                Dim dt As New DataTable
                Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
                dt = ICApprovalRuleConditionUserController.GetApprovalRuleCondUSersByApprovalRuleCondID(e.CommandArgument.ToString, "And", item.GetDataKeyValue("SelectionCriteria"))
                For Each dr As DataRow In dt.Rows
                    ICApprovalRuleConditionUserController.DeleteApprovalRuleCondition(dr("ApprovalRuleConditionUserID"), Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                Next



                Dim ActionString As String = ""
                Dim objICApprovalCondition As New ICApprovalRuleConditions
                Dim objICApprovalRule As New ICApprovalRule


                objICApprovalCondition.LoadByPrimaryKey(e.CommandArgument.ToString)
                objICApprovalRule.LoadByPrimaryKey(objICApprovalCondition.ApprovalRuleID)

                ActionString = "Aproval Rule [ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.ApprovalRuleName.ToString() & " ], For Company "
                ActionString += "[ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.CompanyCode.ToString() & " ][ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.UpToICCompanyByCompanyCode.CompanyName.ToString() & " ],"
                ActionString += " Payment Nature [ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.PaymentNatureCode & " ][ "
                ActionString += "" & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ], From Amount [ "
                ActionString += "" & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.FromAmount & " ] , To Amount [" & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.ToAmount & " ], IsApproved [ "
                ActionString += "updated"


                objICApprovalRule.CreatedBy = Me.UserInfo.UserID
                objICApprovalRule.CreatedDate = Date.Now


                ICApprovalRuleManagementController.AddApprovalRule(objICApprovalRule, True, Me.UserInfo.UserID, Me.UserInfo.Username, ActionString)
                ICApprovalRuleConditionsController.DeleteApprovalRuleCondition(e.CommandArgument.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)


               
                LoadAndApprovalConditions(True)
                UIUtilities.ShowDialog(Me, "Aproval Rule Condition", "Aproval rule condition deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub gvAndConditions_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAndConditions.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"7"}
                ' Dim arrPageSizes() As String = {"1", "2", "5", "10", "50"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAndConditions.MasterTableView.ClientID)
                Next
                'If gvAccountsDetails.Items.Count > 0 Then
                '    myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
                'End If
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvOrConditions_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles gvOrConditions.ItemCommand
        Try
            If e.CommandName = "del" Then
                Dim dt As New DataTable
                Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
                dt = ICApprovalRuleConditionUserController.GetApprovalRuleCondUSersByApprovalRuleCondID(e.CommandArgument.ToString, "Or", item.GetDataKeyValue("SelectionCriteria"))
                For Each dr As DataRow In dt.Rows
                    ICApprovalRuleConditionUserController.DeleteApprovalRuleCondition(dr("ApprovalRuleConditionUserID"), Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                Next



                Dim ActionString As String = ""
                Dim objICApprovalCondition As New ICApprovalRuleConditions
                Dim objICApprovalRule As New ICApprovalRule
                objICApprovalCondition.LoadByPrimaryKey(e.CommandArgument.ToString)
                objICApprovalRule.LoadByPrimaryKey(objICApprovalCondition.ApprovalRuleID)
                ActionString = "Aproval Rule [ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.ApprovalRuleName.ToString() & " ], For Company "
                ActionString += "[ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.CompanyCode.ToString() & " ][ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.UpToICCompanyByCompanyCode.CompanyName.ToString() & " ],"
                ActionString += " Payment Nature [ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.PaymentNatureCode & " ][ "
                ActionString += "" & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ], From Amount [ "
                ActionString += "" & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.FromAmount & " ] , To Amount [" & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.ToAmount & " ], IsApproved [ "
                ActionString += "updated"


                objICApprovalRule.CreatedBy = Me.UserInfo.UserID
                objICApprovalRule.CreatedDate = Date.Now


                ICApprovalRuleManagementController.AddApprovalRule(objICApprovalRule, True, Me.UserInfo.UserID, Me.UserInfo.Username, ActionString)


                ICApprovalRuleConditionsController.DeleteApprovalRuleCondition(e.CommandArgument.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)

                



                LoadOrApprovalConditions(True)
                UIUtilities.ShowDialog(Me, "Aproval Rule Condition", "Aproval rule condition deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub gvOrConditions_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvOrConditions.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"7"}
                ' Dim arrPageSizes() As String = {"1", "2", "5", "10", "50"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvOrConditions.MasterTableView.ClientID)
                Next
                'If gvAccountsDetails.Items.Count > 0 Then
                '    myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
                'End If
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAndConditions_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles gvAndConditions.ItemDataBound
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            'Dim chkBoxList As New CheckBoxList
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            'Dim ApprovalRuleCondID As String
            'chkBoxList = DirectCast(e.Item.FindControl("chkLstAndUser"), CheckBoxList)
            'ApprovalRuleCondID = item.GetDataKeyValue("ApprovalRuleConditionID")
            'BindConditionUsers(ApprovalRuleCondID, chkBoxList)
            item.Cells(5).Text = "From"
        End If
    End Sub
    Protected Sub gvOrConditions_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles gvOrConditions.ItemDataBound
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            'Dim chkBoxList As New CheckBoxList
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            'Dim ApprovalRuleCondID As String
            'chkBoxList = DirectCast(e.Item.FindControl("chkLstAndUser"), CheckBoxList)
            'ApprovalRuleCondID = item.GetDataKeyValue("ApprovalRuleConditionID")
            'BindConditionUsers(ApprovalRuleCondID, chkBoxList)
            item.Cells(5).Text = "From"
        End If
    End Sub
    Private Sub BindConditionUsers(ByVal ApprovalRuleConditionID As String, ByVal chkBoxList As CheckBoxList)
        Try
            chkBoxList.DataSource = Nothing
            chkBoxList.DataBind()


            chkBoxList.DataSource = ICApprovalRuleConditionUserController.GetApprovalRuleConditionsByApprovalRuleIDAsDataTable(ApprovalRuleConditionID)
            chkBoxList.DataTextField = "UserName"
            chkBoxList.DataValueField = "UserID"
            chkBoxList.DataBind()
            If chkBoxList.Items.Count > 0 Then
                chkBoxList.Visible = True
            Else
                chkBoxList.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub gvAndConditions_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAndConditions.NeedDataSource
        Try
            LoadAndApprovalConditions(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub gvOrConditions_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvOrConditions.NeedDataSource
        Try
            LoadOrApprovalConditions(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub gvAndConditions_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvAndConditions.PageIndexChanged
        Try
            gvAndConditions.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub gvOrConditions_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvOrConditions.PageIndexChanged
        Try
            gvOrConditions.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

    Protected Sub ddlAppAndGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAppAndGroup.SelectedIndexChanged
        Try
            If ddlSelAndCriteria.SelectedValue.ToString = "All" Then
                chklstClientUser.DataSource = Nothing
                chklstClientUser.DataBind()
                tblChkListUserCount.Style.Add("display", "none")
                tblCount.Style.Add("display", "none")
                RadInputManager1.GetSettingByBehaviorID("radAndCount").Validation.IsRequired = False
                cvAndAppUser.Enabled = False
            ElseIf ddlSelAndCriteria.SelectedValue.ToString = "Any" Then
                chklstClientUser.DataSource = Nothing
                chklstClientUser.DataBind()
                tblChkListUserCount.Style.Add("display", "none")
                tblCount.Style.Remove("display")
                RadInputManager1.GetSettingByBehaviorID("radAndCount").Validation.IsRequired = True
                cvAndAppUser.Enabled = False
            ElseIf ddlSelAndCriteria.SelectedValue.ToString = "Specific" Then
                chklstClientUser.DataSource = Nothing
                chklstClientUser.DataBind()
                tblChkListUserCount.Style.Remove("display")
                tblCount.Style.Add("display", "none")
                RadInputManager1.GetSettingByBehaviorID("radAndCount").Validation.IsRequired = False
                BindCheckBoxUserList(chklstClientUser, ddlAppAndGroup.SelectedValue.ToString, "And", ddlSelAndCriteria.SelectedValue.ToString, ApprovalRuleID)
                cvAndAppUser.Enabled = True
            Else
                ClearAndControls()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub BindCheckBoxUserList(ByVal CheckBoxList As CheckBoxList, ByVal ApprovalGroupID As String, ByVal ConditionType As String, ByVal SelectionCriteria As String, ByVal ApprovalRuleID As String)
        Try
            CheckBoxList.DataSource = Nothing
            CheckBoxList.DataBind()
            CheckBoxList.DataSource = GetApprovalGroupUserNotInCondition(ApprovalGroupID, ConditionType, SelectionCriteria, ApprovalRuleID)
            CheckBoxList.DataTextField = "UserName"
            CheckBoxList.DataValueField = "UserID"
            CheckBoxList.DataBind()
            If CheckBoxList.Items.Count > 0 Then
                CheckBoxList.Visible = True
            Else
                CheckBoxList.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ddlSelAndCriteria_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSelAndCriteria.SelectedIndexChanged
        Try
            LoadddlApprovalAndGroup()
            chklstClientUser.DataSource = Nothing
            chklstClientUser.DataBind()
            tblChkListUserCount.Style.Add("display", "none")
            tblCount.Style.Add("display", "none")
            RadInputManager1.GetSettingByBehaviorID("radAndCount").Validation.IsRequired = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ddlSelOrCriteria_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSelOrCriteria.SelectedIndexChanged
        Try
            LoadddlApprovalORGroup()
            chklstClientUserOR.DataSource = Nothing
            chklstClientUserOR.DataBind()
            tblChkListUserCountOr.Style.Add("display", "none")
            tblCount0.Style.Add("display", "none")
            RadInputManager1.GetSettingByBehaviorID("radOrCount").Validation.IsRequired = False
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ddlAppOrGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAppOrGroup.SelectedIndexChanged
        Try
            If ddlSelOrCriteria.SelectedValue.ToString = "All" Then
                chklstClientUserOR.DataSource = Nothing
                chklstClientUserOR.DataBind()
                tblChkListUserCountOr.Style.Add("display", "none")
                tblCount0.Style.Add("display", "none")
                RadInputManager1.GetSettingByBehaviorID("radOrCount").Validation.IsRequired = False
                cvORAppUser.Enabled = False
            ElseIf ddlSelOrCriteria.SelectedValue.ToString = "Any" Then
                chklstClientUserOR.DataSource = Nothing
                chklstClientUserOR.DataBind()
                tblChkListUserCountOr.Style.Add("display", "none")
                tblCount0.Style.Remove("display")
                RadInputManager1.GetSettingByBehaviorID("radOrCount").Validation.IsRequired = True
                cvORAppUser.Enabled = False
            ElseIf ddlSelOrCriteria.SelectedValue.ToString = "Specific" Then
                chklstClientUserOR.DataSource = Nothing
                chklstClientUserOR.DataBind()
                tblChkListUserCountOr.Style.Remove("display")
                tblCount0.Style.Add("display", "none")
                RadInputManager1.GetSettingByBehaviorID("radOrCount").Validation.IsRequired = False
                BindCheckBoxUserList(chklstClientUserOR, ddlAppOrGroup.SelectedValue.ToString, "Or", ddlSelOrCriteria.SelectedValue.ToString, ApprovalRuleID)
                cvORAppUser.Enabled = True
            Else
                ClearOrControls()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSaveAnd_Click(sender As Object, e As EventArgs) Handles btnSaveAnd.Click
        If Page.IsValid Then
            Try
                Dim objICApprovalCondition As New ICApprovalRuleConditions
                Dim objICApprovalRule As New ICApprovalRule
                Dim ApprovalRuleConditionID As String
                Dim ActionString As String = ""
                objICApprovalCondition.ApprovalRuleID = ApprovalRuleID
                objICApprovalCondition.ApprovalGroupID = ddlAppAndGroup.SelectedValue.ToString
                objICApprovalCondition.SelectionCriteria = ddlSelAndCriteria.SelectedValue.ToString
                objICApprovalCondition.ConditionType = "And"
                objICApprovalCondition.Creator = Me.UserInfo.UserID.ToString
                objICApprovalCondition.CreatedDate = Date.Now
                objICApprovalCondition.CreatedBy = Me.UserInfo.UserID.ToString
                objICApprovalCondition.CreationDate = Date.Now
                If ddlSelAndCriteria.SelectedValue.ToString = "Specific" And chklstClientUser.Items.Count = 0 Then
                    UIUtilities.ShowDialog(Me, "Aproval Rule Condition", "Please select atleast one user", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                ''Check duplicate condition
                If CheckDuplicateCondition("And", ddlSelAndCriteria.SelectedValue.ToString, ddlAppAndGroup.SelectedValue.ToString) = False Then
                    ''Check valid user exists against condition type
                    If ValidateUserSelection(ddlSelAndCriteria.SelectedValue.ToString, ddlAppAndGroup.SelectedValue.ToString, txtAndCount.Text, "And", ApprovalRuleID) = True Then
                        ''Add condition
                        ApprovalRuleConditionID = ICApprovalRuleConditionsController.AddApprovalRuleCondtions(objICApprovalCondition, False, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                        ''Add condition users
                        AddApprovalGroupConditionUsers(ddlAppAndGroup.SelectedValue.ToString, ddlSelAndCriteria.SelectedValue.ToString, txtAndCount.Text, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "And", ApprovalRuleConditionID, chklstClientUser, ApprovalRuleID)


                        ActionString = "Aproval Rule [ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.ApprovalRuleName.ToString() & " ], For Company "
                        ActionString += "[ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.CompanyCode.ToString() & " ][ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.UpToICCompanyByCompanyCode.CompanyName.ToString() & " ],"
                        ActionString += " Payment Nature [ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.PaymentNatureCode & " ][ "
                        ActionString += "" & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ], From Amount [ "
                        ActionString += "" & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.FromAmount & " ] , To Amount [" & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.ToAmount & " ], IsApproved [ "
                        ActionString += "updated"

                        objICApprovalRule.LoadByPrimaryKey(objICApprovalCondition.ApprovalRuleID)


                        objICApprovalRule.CreatedBy = Me.UserInfo.UserID
                        objICApprovalRule.CreatedDate = Date.Now
                        ICApprovalRuleManagementController.AddApprovalRule(objICApprovalRule, True, Me.UserInfo.UserID, Me.UserInfo.Username, ActionString)

                        ClearAndControls()
                        LoadAndApprovalConditions(True)
                        UIUtilities.ShowDialog(Me, "Aproval Rule Condition", "Aproval rule condition added successfully.", ICBO.IC.Dialogmessagetype.Success)
                        Exit Sub
                    Else
                        UIUtilities.ShowDialog(Me, "Aproval Rule Condition", "Invalid User Selection", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If

                Else
                    UIUtilities.ShowDialog(Me, "Aproval Rule Condition", "Dupplicate aproval rule condition is not allowed", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub
    Private Function CheckDuplicateCondition(ByVal ConditionType As String, ByVal SelectionCriteria As String, ByVal ApprovalGroupID As String) As Boolean
        If SelectionCriteria <> "Specific" Then
            Dim objICAppGroupUserColl As New ICApprovalRuleConditionsCollection

            objICAppGroupUserColl.Query.Where(objICAppGroupUserColl.Query.ConditionType = ConditionType And objICAppGroupUserColl.Query.SelectionCriteria = SelectionCriteria)
            objICAppGroupUserColl.Query.Where(objICAppGroupUserColl.Query.ApprovalGroupID = ApprovalGroupID)
            objICAppGroupUserColl.Query.Where(objICAppGroupUserColl.Query.ApprovalRuleID = ApprovalRuleID)
            objICAppGroupUserColl.Query.Load()
            If objICAppGroupUserColl.Query.Load Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If

    End Function
    Private Function GetApprovalGroupTaggedUserByNotInCondition(ByVal ApprovalGroupID As String, ByVal CondtionType As String, ByVal ApprovalRuleID As String) As ICApprovalGroupUsersCollection
        Dim collObjICApprovalGroupUser As New ICApprovalGroupUsersCollection
        Dim qryObjICAppGroupCondUSer As New ICApprovalRuleConditionUsersQuery("qryObjICAppGroupCondUSer")
        Dim qryObjICAppRuleCond As New ICApprovalRuleConditionsQuery("qryObjICAppRule")


        qryObjICAppGroupCondUSer.Select(qryObjICAppGroupCondUSer.UserID)
        qryObjICAppGroupCondUSer.InnerJoin(qryObjICAppRuleCond).On(qryObjICAppGroupCondUSer.ApprovaRuleCondID = qryObjICAppRuleCond.ApprovalRuleConditionID)
        qryObjICAppGroupCondUSer.Where(qryObjICAppRuleCond.ConditionType = CondtionType And qryObjICAppRuleCond.ApprovalGroupID = ApprovalGroupID)
        qryObjICAppGroupCondUSer.Where(qryObjICAppRuleCond.ApprovalRuleID = ApprovalRuleID)


        collObjICApprovalGroupUser.Query.Where(collObjICApprovalGroupUser.Query.ApprovalGroupID = ApprovalGroupID)
        collObjICApprovalGroupUser.Query.Where(collObjICApprovalGroupUser.Query.UserID.NotIn(qryObjICAppGroupCondUSer))
        collObjICApprovalGroupUser.Query.OrderBy(collObjICApprovalGroupUser.Query.UserApprovalGroupID.Ascending)

        collObjICApprovalGroupUser.Query.Load()
        Return collObjICApprovalGroupUser

    End Function
    Private Function AddApprovalGroupConditionUsers(ByVal ApprovalGroupID As String, ByVal SelectionCriteria As String, ByVal UserCount As String, ByVal UserID As String, ByVal UserName As String, ByVal ConditionType As String, ByVal ApprovalRuleCondionID As String, ByVal CheckBoxListToADD As CheckBoxList, ByVal ApprovalRuleID As String) As Boolean
        Dim Result As Boolean = False
        Dim objICApprovlGroupUserColl As New ICApprovalGroupUsersCollection
        Dim objICAppRuleCondUser As ICApprovalRuleConditionUsers
        If SelectionCriteria = "All" Or SelectionCriteria = "Any" Then
            objICApprovlGroupUserColl = GetApprovalGroupTaggedUserByNotInCondition(ApprovalGroupID, ConditionType, ApprovalRuleID)
            For Each objICApprovalRuleCondUSer As ICApprovalGroupUsers In objICApprovlGroupUserColl
                objICAppRuleCondUser = New ICApprovalRuleConditionUsers
                objICAppRuleCondUser.UserID = objICApprovalRuleCondUSer.UserID
                objICAppRuleCondUser.ApprovaRuleCondID = ApprovalRuleCondionID
                objICAppRuleCondUser.CreatedBy = UserID
                objICAppRuleCondUser.CreatedDate = Date.Now
                If UserCount <> "" And UserCount IsNot Nothing Then
                    objICAppRuleCondUser.UserCount = UserCount
                Else
                    objICAppRuleCondUser.UserCount = Nothing
                End If
                ICApprovalRuleConditionUserController.AddApprovalRuleCondtionsUser(objICAppRuleCondUser, False, UserID, UserName)
            Next
        ElseIf SelectionCriteria = "Specific" Then
            For Each lit As ListItem In CheckBoxListToADD.Items
                If lit.Selected = True Then
                    objICAppRuleCondUser = New ICApprovalRuleConditionUsers
                    objICAppRuleCondUser.UserID = lit.Value
                    objICAppRuleCondUser.ApprovaRuleCondID = ApprovalRuleCondionID
                    objICAppRuleCondUser.CreatedBy = UserID
                    objICAppRuleCondUser.CreatedDate = Date.Now
                    objICAppRuleCondUser.UserCount = Nothing

                    ICApprovalRuleConditionUserController.AddApprovalRuleCondtionsUser(objICAppRuleCondUser, False, UserID, UserName)
                End If
            Next
        End If
        Return Result
    End Function


    Private Function ValidateUserSelection(ByVal SelectionCriteria As String, ByVal ApprovalGroupID As String, ByVal UserCount As String, ConditionType As String, ByVal ApprovalRuleID As String) As Boolean
        Dim Result As Boolean = False
        Dim GroupUserCount As String
        If SelectionCriteria = "Any" Then
            'GroupUserCount = GetApprovalGroupUserCount(ApprovalGroupID, ConditionType, SelectionCriteria, ApprovalRuleID)
            GroupUserCount = GetApprovalGroupUserCount(ApprovalGroupID, ConditionType, ApprovalRuleID)
            If GroupUserCount >= UserCount Then
                Result = True
            End If
        ElseIf SelectionCriteria = "All" Then
            'GroupUserCount = GetApprovalGroupUserCount(ApprovalGroupID, ConditionType, SelectionCriteria, ApprovalRuleID)
            GroupUserCount = GetApprovalGroupUserCount(ApprovalGroupID, ConditionType, ApprovalRuleID)
            If GroupUserCount > 0 Then
                Result = True
            End If
        ElseIf SelectionCriteria = "Specific" Then

            Result = True
        End If
        Return Result
    End Function
    'Private Function GetApprovalGroupUserCount(ByVal ApprovalGroupID As String, ByVal CondtionType As String, ByVal SelectionCriteria As String, ByVal ApprovalRuleID As String) As Integer
    Private Function GetApprovalGroupUserCount(ByVal ApprovalGroupID As String, ByVal CondtionType As String, ByVal ApprovalRuleID As String) As Integer
        Dim qryObjICAppGroupUser As New ICApprovalGroupUsersQuery("qryObjICAppGroupUser")
        Dim qryObjICAppGroupCondUSer As New ICApprovalRuleConditionUsersQuery("qryObjICAppGroupCondUSer")
        Dim qryObjICAppRuleCond As New ICApprovalRuleConditionsQuery("qryObjICAppRule")
        Dim dt As New DataTable
        Dim dt2 As New DataTable
        Dim ArrayListUsrID As New ArrayList
        Dim usercount As Integer = 0
        ArrayListUsrID.Add("-1")
        qryObjICAppGroupCondUSer.Select(qryObjICAppGroupCondUSer.UserID)
        qryObjICAppGroupCondUSer.InnerJoin(qryObjICAppRuleCond).On(qryObjICAppGroupCondUSer.ApprovaRuleCondID = qryObjICAppRuleCond.ApprovalRuleConditionID)
        qryObjICAppGroupCondUSer.Where(qryObjICAppRuleCond.ConditionType = CondtionType And qryObjICAppRuleCond.ApprovalGroupID = ApprovalGroupID)
        'qryObjICAppGroupCondUSer.Where(qryObjICAppRuleCond.ApprovalRuleID = ApprovalRuleID And qryObjICAppRuleCond.SelectionCriteria = SelectionCriteria)
        qryObjICAppGroupCondUSer.Where(qryObjICAppRuleCond.ApprovalRuleID = ApprovalRuleID)
        For Each dr As DataRow In qryObjICAppGroupCondUSer.LoadDataTable.Rows
            ArrayListUsrID.Add(dr("UserID"))
        Next


        qryObjICAppGroupUser.Select(qryObjICAppGroupUser.UserID.Count)

        qryObjICAppGroupUser.Where(qryObjICAppGroupUser.ApprovalGroupID = ApprovalGroupID)
        qryObjICAppGroupUser.Where(qryObjICAppGroupUser.UserID.NotIn(ArrayListUsrID))
        qryObjICAppGroupUser.GroupBy(qryObjICAppGroupUser.ApprovalGroupID)
        dt = qryObjICAppGroupUser.LoadDataTable

        If dt.Rows.Count > 0 Then
            usercount = dt.Rows(0)(0)
        End If


        Return usercount


        'Dim objICAppGroupUserColl As New ICApprovalGroupUsersCollection

        'objICAppGroupUserColl.Query.Select(objICAppGroupUserColl.Query.UserApprovalGroupID.Count)
        'objICAppGroupUserColl.Query.Where(objICAppGroupUserColl.Query.ApprovalGroupID = ApprovalGroupID)
        'objICAppGroupUserColl.Query.GroupBy(objICAppGroupUserColl.Query.ApprovalGroupID)
        'Return CInt(objICAppGroupUserColl.Query.LoadDataTable.Rows(0)(0))
    End Function
    Private Function GetApprovalGroupUserNotInCondition(ByVal ApprovalGroupID As String, ByVal CondtionType As String, ByVal SelectionCriteria As String, ByVal ApprovalRuleID As String) As DataTable

        Dim collObjICApprovalGroupUser As New ICApprovalGroupUsersQuery("collObjICApprovalGroupUser")
        Dim qryObjICUser As New ICUserQuery("qryObjICUser")
        Dim qryObjICAppGroupCondUSer As New ICApprovalRuleConditionUsersQuery("qryObjICAppGroupCondUSer")
        Dim qryObjICAppRuleCond As New ICApprovalRuleConditionsQuery("qryObjICAppRule")
        Dim UserIDArrayList As New ArrayList
        UserIDArrayList.Add("-1")
        qryObjICAppGroupCondUSer.Select(qryObjICAppGroupCondUSer.UserID)
        qryObjICAppGroupCondUSer.InnerJoin(qryObjICAppRuleCond).On(qryObjICAppGroupCondUSer.ApprovaRuleCondID = qryObjICAppRuleCond.ApprovalRuleConditionID)
        qryObjICAppGroupCondUSer.Where(qryObjICAppRuleCond.ConditionType = CondtionType And qryObjICAppRuleCond.ApprovalGroupID = ApprovalGroupID And qryObjICAppRuleCond.SelectionCriteria = SelectionCriteria)
        qryObjICAppGroupCondUSer.Where(qryObjICAppRuleCond.ApprovalRuleID = ApprovalRuleID)

        For Each dr As DataRow In qryObjICAppGroupCondUSer.LoadDataTable().Rows
            UserIDArrayList.Add(dr("UserID"))
        Next

        collObjICApprovalGroupUser.Select(qryObjICUser.UserID, (qryObjICUser.UserName + "-" + qryObjICUser.DisplayName).As("UserName"))
        collObjICApprovalGroupUser.InnerJoin(qryObjICUser).On(collObjICApprovalGroupUser.UserID = qryObjICUser.UserID)
        collObjICApprovalGroupUser.Where(collObjICApprovalGroupUser.ApprovalGroupID = ApprovalGroupID)
        collObjICApprovalGroupUser.Where(collObjICApprovalGroupUser.UserID.NotIn(UserIDArrayList))
        collObjICApprovalGroupUser.OrderBy(collObjICApprovalGroupUser.UserApprovalGroupID.Ascending)


        Return collObjICApprovalGroupUser.LoadDataTable


        'Dim objICAppGroupUserColl As New ICApprovalGroupUsersCollection

        'objICAppGroupUserColl.Query.Select(objICAppGroupUserColl.Query.UserApprovalGroupID.Count)
        'objICAppGroupUserColl.Query.Where(objICAppGroupUserColl.Query.ApprovalGroupID = ApprovalGroupID)
        'objICAppGroupUserColl.Query.GroupBy(objICAppGroupUserColl.Query.ApprovalGroupID)
        'Return CInt(objICAppGroupUserColl.Query.LoadDataTable.Rows(0)(0))
    End Function

    Protected Sub btnSaveOR_Click(sender As Object, e As EventArgs) Handles btnSaveOR.Click
        If Page.IsValid Then
            Try
                Dim objICApprovalCondition As New ICApprovalRuleConditions
                Dim objICApprovalRule As New ICApprovalRule
                Dim ApprovalRuleConditionID As String
                Dim ActionString As String = ""
                objICApprovalCondition.ApprovalRuleID = ApprovalRuleID
                objICApprovalCondition.ApprovalGroupID = ddlAppOrGroup.SelectedValue.ToString
                objICApprovalCondition.SelectionCriteria = ddlSelOrCriteria.SelectedValue.ToString
                objICApprovalCondition.ConditionType = "Or"
                objICApprovalCondition.Creator = Me.UserInfo.UserID.ToString
                objICApprovalCondition.CreatedDate = Date.Now
                objICApprovalCondition.CreatedBy = Me.UserInfo.UserID.ToString
                objICApprovalCondition.CreationDate = Date.Now


                If ddlSelOrCriteria.SelectedValue.ToString = "Specific" And chklstClientUserOR.Items.Count = 0 Then
                    UIUtilities.ShowDialog(Me, "Aproval Rule Condition", "Please select atleast one user", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If


                If CheckDuplicateCondition("Or", ddlSelOrCriteria.SelectedValue.ToString, ddlAppOrGroup.SelectedValue.ToString) = False Then
                    If ValidateUserSelection(ddlSelOrCriteria.SelectedValue.ToString, ddlAppOrGroup.SelectedValue.ToString, txtOrCount.Text, "Or", ApprovalRuleID) = True Then
                        ApprovalRuleConditionID = ICApprovalRuleConditionsController.AddApprovalRuleCondtions(objICApprovalCondition, False, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                        AddApprovalGroupConditionUsers(ddlAppOrGroup.SelectedValue.ToString, ddlSelOrCriteria.SelectedValue.ToString, txtOrCount.Text, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Or", ApprovalRuleConditionID, chklstClientUserOR, ApprovalRuleID)
                        ClearOrControls()




                        ActionString = "Aproval Rule [ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.ApprovalRuleName.ToString() & " ], For Company "
                        ActionString += "[ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.CompanyCode.ToString() & " ][ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.UpToICCompanyByCompanyCode.CompanyName.ToString() & " ],"
                        ActionString += " Payment Nature [ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.PaymentNatureCode & " ][ "
                        ActionString += "" & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ], From Amount [ "
                        ActionString += "" & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.FromAmount & " ] , To Amount [" & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.ToAmount & " ], IsApproved [ "
                        ActionString += "updated"

                        objICApprovalRule.LoadByPrimaryKey(objICApprovalCondition.ApprovalRuleID)


                        objICApprovalRule.CreatedBy = Me.UserInfo.UserID
                        objICApprovalRule.CreatedDate = Date.Now
                        ICApprovalRuleManagementController.AddApprovalRule(objICApprovalRule, True, Me.UserInfo.UserID, Me.UserInfo.Username, ActionString)
                        LoadOrApprovalConditions(True)
                        UIUtilities.ShowDialog(Me, "Aproval Rule Condition", "Aproval rule condition added successfully.", ICBO.IC.Dialogmessagetype.Success)
                        Exit Sub
                    Else
                        UIUtilities.ShowDialog(Me, "Aproval Rule Condition", "Invalid User Selection", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else
                    UIUtilities.ShowDialog(Me, "Aproval Rule Condition", "Dupplicate aproval rule condition is not allowed", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Protected Sub btnCancel0_Click(sender As Object, e As EventArgs) Handles btnCancel0.Click

        Try
                Response.Redirect(NavigateURL)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try


    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click

        Try
                Response.Redirect(NavigateURL)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try


    End Sub






    

End Class
