﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_ApprovalRuleManagement_ViewConditionTaggedUsers
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private htRights As Hashtable
    Private AppRuleCondID As String
    Private ApprovalRuleID As String
    Private CompanyCode As String
#Region "Page Load"
    Protected Sub DesktopModules_AccountsManagement_ViewAccounts_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            AppRuleCondID = Request.QueryString("AppRuleCondID")
           
            ApprovalRuleID = Request.QueryString("ApprovalRuleID")
            CompanyCode = Request.QueryString("CompanyCode")
            If Page.IsPostBack = False Then
                
                LoadAppRovalRuleCondUsers(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
#End Region
#Region "Grid View Events"

    Protected Sub gvAccountsDetails_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvUserDetails.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                ' Dim arrPageSizes() As String = {"1", "2", "5", "10", "50"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvUserDetails.MasterTableView.ClientID)
                Next
                'If gvAccountsDetails.Items.Count > 0 Then
                '    myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
                'End If
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Other Functions/Routines"
    
    Public Shared Sub GetAllAccountsForRadGridByGroupAndCompanyCode(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid, ByVal ApprovalRuleCondID As String)


        Dim qryObjICAppRuleCondUsers As New ICApprovalRuleConditionUsersQuery("qryObjICAppRuleCondUsers")
        Dim qryObjICUsers As New ICUserQuery("qryObjICUsers")

        Dim dt As New DataTable
        qryObjICAppRuleCondUsers.Select(qryObjICUsers.UserName, qryObjICUsers.DisplayName, qryObjICAppRuleCondUsers.UserCount.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'"))


        qryObjICAppRuleCondUsers.InnerJoin(qryObjICUsers).On(qryObjICAppRuleCondUsers.UserID = qryObjICUsers.UserID)

        qryObjICAppRuleCondUsers.Where(qryObjICAppRuleCondUsers.ApprovaRuleCondID = ApprovalRuleCondID)

        dt = qryObjICAppRuleCondUsers.LoadDataTable


        If Not PageNumber = 0 Then
            qryObjICAppRuleCondUsers.es.PageNumber = PageNumber
            qryObjICAppRuleCondUsers.es.PageSize = PageSize
            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        Else
            qryObjICAppRuleCondUsers.es.PageNumber = 1
            qryObjICAppRuleCondUsers.es.PageSize = PageSize
            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        End If
    End Sub
    Private Sub LoadAppRovalRuleCondUsers(ByVal DoDataBind As Boolean)
        Try


            GetAllAccountsForRadGridByGroupAndCompanyCode(Me.gvUserDetails.CurrentPageIndex + 1, Me.gvUserDetails.PageSize, DoDataBind, Me.gvUserDetails, AppRuleCondID)
            If gvUserDetails.Items.Count > 0 Then
                gvUserDetails.Visible = True
                lblRNF.Visible = False
               
            Else
                gvUserDetails.Visible = False
               
                lblRNF.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
    

#End Region
    

    Protected Sub gvAccountsDetails_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvUserDetails.NeedDataSource
        Try
            LoadAppRovalRuleCondUsers(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAccountsDetails_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvUserDetails.PageIndexChanged
        Try
            gvUserDetails.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    
    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL("AddConditions", "&mid=" & Me.ModuleId & "&AppRuleID=" & ApprovalRuleID & "&CompanyCode=" & CompanyCode))
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
