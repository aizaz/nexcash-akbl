﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports Telerik.Web.UI

Partial Class DesktopModules_ApprovalRuleManagement_SaveApprovalRule
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ApprovalRuleID As String
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_AccountsManagement_SaveAccount_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            ApprovalRuleID = Request.QueryString("AppruleID")
            If Page.IsPostBack = False Then
            ViewState("htRights") = Nothing
            GetPageAccessRights()
            

            'pnlFlexiFields.Style.Add("display", "none")

            If ApprovalRuleID.ToString() = "0" Then
                    Clear()
                    LoadddlGroup()
                LoadddlCompany()
                LoadddlPaymentNature()
                    lblPageHeader.Text = "Add Aproval Rule"
                btnSave.Text = "Save"
                    lblIsActive.Visible = False
                    chkActive.Visible = False
                    btnApprove.Visible = False
                'lblRNFFlexiFields.Visible = True
                btnSave.Visible = CBool(htRights("Add"))

                Else
                    lblIsActive.Visible = CBool(htRights("Approve"))
                    chkActive.Visible = CBool(htRights("Approve"))
                    btnApprove.Visible = CBool(htRights("Approve"))
                    lblPageHeader.Text = "Edit Aproval Rule"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    'txtAccountNumber.ReadOnly = True
                    Dim objICApprovalRule As New ICApprovalRule


                    objICApprovalRule.LoadByPrimaryKey(ApprovalRuleID)



                    txtRuleName.Text = objICApprovalRule.ApprovalRuleName
                    txtFromAmount.Text = objICApprovalRule.FromAmount
                    txtToAmount.Text = objICApprovalRule.ToAmount
                    chkActive.Checked = objICApprovalRule.IsActive
                    LoadddlGroup()
                    ddlGroup.SelectedValue = objICApprovalRule.UpToICCompanyByCompanyCode.GroupCode
                    LoadddlCompany()
                    ddlCompany.SelectedValue = objICApprovalRule.CompanyCode
                    LoadddlPaymentNature()
                    ddlPaymentNature.SelectedValue = objICApprovalRule.PaymentNatureCode


                    ddlGroup.Enabled = False
                    ddlCompany.Enabled = False
                    ddlPaymentNature.Enabled = False



                    If objICApprovalRule.IsActive = True Then
                        chkActive.Enabled = False
                        chkActive.Checked = True
                        btnApprove.Visible = False
                    End If
            End If
            'btnSave.Visible = ArrRights(1)
             End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Approval Rule Management")
            ViewState("htRights") = htRights

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#Region "Button Events"
    Private Sub Clear()
        ddlGroup.ClearSelection()
        ddlCompany.ClearSelection()
        ddlPaymentNature.ClearSelection()
        chkActive.Checked = False
        txtRuleName.Text = ""
        txtFromAmount.Text = ""
        txtToAmount.Text = ""

    End Sub
    Private Function CheckDuplicateRuleName(ByVal objICApprovalRule As ICApprovalRule, ByVal IsUpdate As Boolean) As Boolean
        Dim Result As Boolean = False
        Dim collObjICAppRule As New ICApprovalRuleCollection


        If IsUpdate = False Then
            collObjICAppRule.Query.Where(collObjICAppRule.Query.ApprovalRuleName = objICApprovalRule.ApprovalRuleName)

        Else
            collObjICAppRule.Query.Where(collObjICAppRule.Query.ApprovalRuleName = objICApprovalRule.ApprovalRuleName And collObjICAppRule.Query.ApprovalRuleID <> objICApprovalRule.ApprovalRuleID)

        End If

        If collObjICAppRule.Query.Load Then
            Result = True
        End If
        Return Result
    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim objICApprovalRule As New ICApprovalRule
                Dim ObjICApprovalRulePrev As New ICApprovalRule
                Dim ActionString As String = ""

                If Not ApprovalRuleID.ToString() = "0" Then
                    ObjICApprovalRulePrev.LoadByPrimaryKey(ApprovalRuleID)
                End If


                objICApprovalRule.ApprovalRuleName = txtRuleName.Text
                objICApprovalRule.FromAmount = txtFromAmount.Text
                objICApprovalRule.ToAmount = txtToAmount.Text
                objICApprovalRule.PaymentNatureCode = ddlPaymentNature.SelectedValue.ToString
                objICApprovalRule.CompanyCode = ddlCompany.SelectedValue.ToString

                objICApprovalRule.IsActive = chkActive.Checked

                If ApprovalRuleID.ToString() = "0" Then
                    If CheckDuplicateRuleName(objICApprovalRule, False) = False Then

                        'If chkActive.Checked = True Then
                        '    UIUtilities.ShowDialog(Me, "Error", "Please define approval rule conditions to mark an approval rule as approved", ICBO.IC.Dialogmessagetype.Failure)
                        '    Exit Sub
                        'End If

                        objICApprovalRule.CreatedBy = Me.UserId
                        objICApprovalRule.CreatedDate = Date.Now
                        objICApprovalRule.Creator = Me.UserId
                        objICApprovalRule.CreationDate = Date.Now
                        ActionString = "Aproval Rule [ " & objICApprovalRule.ApprovalRuleName.ToString() & " ], For Company "
                        ActionString += "[ " & objICApprovalRule.CompanyCode.ToString() & " ][ " & objICApprovalRule.UpToICCompanyByCompanyCode.CompanyName.ToString() & " ],"
                        ActionString += " Payment Nature [ " & objICApprovalRule.PaymentNatureCode & " ][ "
                        ActionString += "" & objICApprovalRule.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ], From Amount [ "
                        ActionString += "" & objICApprovalRule.FromAmount & " ] , To Amount [" & objICApprovalRule.ToAmount & " ], IsApproved [ "
                        ActionString += "" & objICApprovalRule.IsActive & " Added"
                        ICApprovalRuleManagementController.AddApprovalRule(objICApprovalRule, False, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, ActionString)
                        UIUtilities.ShowDialog(Me, "Aproval Rule", "Aproval Rule added successfully.", ICBO.IC.Dialogmessagetype.Success)
                        Clear()
                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Duplicate Aproval Rule is not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If

                Else


                    'If chkActive.Checked = True Then
                    '    If IsApprovalRuleCondiotnsDefined(ApprovalRuleID) = False Then
                    '        UIUtilities.ShowDialog(Me, "Error", "Please define approval rule conditions to mark an approval rule as approved", ICBO.IC.Dialogmessagetype.Failure)
                    '        Exit Sub
                    '    End If
                    '    If Me.UserInfo.IsSuperUser = False Then
                    '        If ObjICApprovalRulePrev.CreatedBy = Me.UserId Then

                    '            UIUtilities.ShowDialog(Me, "Error", "Approval rule must be approved by user other than maker", ICBO.IC.Dialogmessagetype.Failure)
                    '            Exit Sub

                    '        End If
                    '    End If
                    'End If
                    objICApprovalRule.CreatedBy = Me.UserId
                    objICApprovalRule.CreatedDate = Date.Now
                    objICApprovalRule.ApprovalRuleID = ApprovalRuleID
                    If CheckDuplicateRuleName(objICApprovalRule, True) = True Then
                        UIUtilities.ShowDialog(Me, "Error", "Duplicate Aproval Rule is not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If


                    ActionString = "Aproval Rule [ " & objICApprovalRule.ApprovalRuleName.ToString() & " ], For Company "
                    ActionString += "[ " & objICApprovalRule.CompanyCode.ToString() & " ][ " & objICApprovalRule.UpToICCompanyByCompanyCode.CompanyName.ToString() & " ],"
                    ActionString += " Payment Nature [ " & objICApprovalRule.PaymentNatureCode & " ][ "
                    ActionString += "" & objICApprovalRule.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ], From Amount [ "
                    ActionString += "" & objICApprovalRule.FromAmount & " ] , To Amount [" & objICApprovalRule.ToAmount & " ], IsApproved [ "
                    ActionString += "" & objICApprovalRule.IsActive & " updated"
                    ICApprovalRuleManagementController.AddApprovalRule(objICApprovalRule, True, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, ActionString)
                    UIUtilities.ShowDialog(Me, "Aproval Rule", "Aproval Rule updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL)
                    Clear()
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

#End Region
#Region "Drop Down Events"
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetCompanyActiveAndApproveByGroupCodeAfterInitialTagging(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
  
    Private Sub LoadddlPaymentNature()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlPaymentNature.Items.Clear()
            ddlPaymentNature.Items.Add(lit)
            ddlPaymentNature.AppendDataBoundItems = True
            'ddlPaymentNature.DataSource = ICAccountPaymentNatureController.GetPaymentNatureByCompanyCode(ddlCompany.SelectedValue.ToString)
            ddlPaymentNature.DataSource = ICAccountPaymentNatureController.GetPaymentNatureByCompanyCode(ddlCompany.SelectedValue.ToString)
            ddlPaymentNature.DataTextField = "PaymentNature"
            ddlPaymentNature.DataValueField = "PaymentNatureCode"
            ddlPaymentNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        LoadddlCompany()
        LoadddlPaymentNature()
    End Sub
    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadddlPaymentNature()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function IsApprovalRuleCondiotnsDefined(ByVal ApprovalRuleID As String) As Boolean
        Dim qryObjICApprovalRuleCond As New ICApprovalRuleConditionsQuery("qryObjICApprovalRuleCond")
        Dim qryObjICApprovalRuleCondUser As New ICApprovalRuleConditionsQuery("qryObjICApprovalRuleCondUser")


        qryObjICApprovalRuleCond.InnerJoin(qryObjICApprovalRuleCondUser).On(qryObjICApprovalRuleCond.ApprovalRuleConditionID = qryObjICApprovalRuleCondUser.ApprovalRuleConditionID)
        qryObjICApprovalRuleCond.Where(qryObjICApprovalRuleCond.ApprovalRuleID = ApprovalRuleID)


        If qryObjICApprovalRuleCond.LoadDataTable.Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If


    End Function

    Protected Sub btnApprove_Click(sender As Object, e As System.EventArgs) Handles btnApprove.Click
        If Page.IsValid Then
            Try
                Dim objICApprovalRule As New ICApprovalRule


                Try

                    objICApprovalRule.LoadByPrimaryKey(ApprovalRuleID)
                    If Me.UserInfo.IsSuperUser = False Then
                        If objICApprovalRule.CreatedBy = Me.UserId Then
                            UIUtilities.ShowDialog(Me, "Aproval Rule", "Aproval rule must be approved by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                            Exit Sub
                        End If
                    End If
                    If chkActive.Checked = True Then
                        If IsApprovalRuleCondiotnsDefined(ApprovalRuleID) = False Then
                            UIUtilities.ShowDialog(Me, "Error", "Please define aproval rule conditions to mark an aproval rule as approved", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                        ICApprovalRuleManagementController.ApproveApprovalRule(ApprovalRuleID, Me.UserId, Me.UserInfo.Username, chkActive.Checked)
                        UIUtilities.ShowDialog(Me, "Aproval Rule", "Aproval Rule approved succesfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    Else
                        UIUtilities.ShowDialog(Me, "Aproval Rule", "Please checked the Approved Check Box.", ICBO.IC.Dialogmessagetype.Warning)
                    End If

                Catch ex As Exception
                    ProcessModuleLoadException(Me, ex, False)
                    UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                End Try
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub
End Class
