﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ManageApprovalRuleConditions.ascx.vb"
    Inherits="DesktopModules_ApprovalRuleManagement_ManageApprovalRuleConditions" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }



    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;

        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }

</script>
<script language="javascript" type="text/javascript">
    function AdjustWidth(ddl) {
        var maxWidth = 0;
        var Counter = 0;
        for (var i = 0; i < ddl.length; i++) {
            if (ddl.options[i].text.length > maxWidth) {
                Counter = Counter + 1;
                maxWidth = ddl.options[i].text.length;
            }
        }
        if (Counter = 1) { ddl.style.width = "250px"; }

        else {
            alert(Counter);
            ddl.style.width = maxWidth + "px";
        }
        //-->
    }


    function ValidateCheckClientUserList(source, args) {
        var chkListModules = document.getElementById('<%= chklstClientUser.ClientID%>');
        var chkListinputs = chkListModules.getElementsByTagName("input");
        for (var i = 0; i < chkListinputs.length; i++) {
            if (chkListinputs[i].checked) {
                args.IsValid = true;
                return;
            }
        }
        args.IsValid = false;
    }
    function ValidateCheckClientUserList2(source, args) {
        var chkListModules = document.getElementById('<%= chklstClientUserOR.ClientID%>');
          var chkListinputs = chkListModules.getElementsByTagName("input");
          for (var i = 0; i < chkListinputs.length; i++) {
              if (chkListinputs[i].checked) {
                  args.IsValid = true;
                  return;
              }
          }
          args.IsValid = false;
      }
</script>

<style type="text/css">
.ctrDropDown{
    width:250px;
   
}
.ctrDropDownClick{
    
    width:600px;
 
}
.plainDropDown{
    width:250px;
   
}
    .auto-style1 {
        width: 25%;
        height: 26px;
    }
    </style>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:RegExpTextBoxSetting BehaviorID="radAndCount" ValidationExpression="[0-9]{1,8}" Validation-IsRequired="true"
        ErrorMessage="Invalid Count" EmptyMessage="" Validation-ValidationGroup="AND">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAndCount" />
        </TargetControls>
        </telerik:RegExpTextBoxSetting>
         <telerik:RegExpTextBoxSetting BehaviorID="radOrCount" ValidationExpression="[0-9]{1,8}" Validation-IsRequired="true"
        ErrorMessage="Invalid Count" EmptyMessage="" Validation-ValidationGroup="OR">
        <TargetControls>
            <telerik:TargetInput ControlID="txtOrCount" />
        </TargetControls>
        </telerik:RegExpTextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue">Approval Rule Condition(s) List</asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCriteria" runat="server" Text="Criteria" CssClass="lbl"></asp:Label>
            </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCriteria0" runat="server" Text="Match All of these Conditions" CssClass="lbl"></asp:Label>
            </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <telerik:RadGrid ID="gvAndConditions" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="7" CssClass="RadGrid">
                           <AlternatingItemStyle CssClass="rgAltRow" />
                           <ClientSettings>
                           <Scrolling  UseStaticHeaders="true" />
                           </ClientSettings>
                            <MasterTableView DataKeyNames="ApprovalRuleConditionID,SelectionCriteria" TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="SelectionCriteria" HeaderText="Criteria" SortExpression="SelectionCriteria">
                                          <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridBoundColumn>
                                     <telerik:GridTemplateColumn HeaderText="View Users">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" Text="View Users" NavigateUrl='<%#NavigateURL("ViewUsers", "&mid=" & Me.ModuleId & "&AppRuleCondID=" & Eval("ApprovalRuleConditionID") & "&ApprovalRuleID=" & Request.QueryString("AppruleID") & "&CompanyCode=" & Request.QueryString("CompanyCode"))%>'
                                                ToolTip="View Users" ForeColor="Blue">Users</asp:HyperLink>
                                        </ItemTemplate>
                                      <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridTemplateColumn>
                                   <telerik:GridBoundColumn DataField="UserCount" HeaderText="Count" SortExpression="UserCount">
                                          <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="From" HeaderText="From" SortExpression="From">
                                          <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="GroupName" HeaderText="GroupName" SortExpression="GroupName">
                                          <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server"
          CommandArgument='<%#Eval("ApprovalRuleConditionID") %>'                                    
                                                CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                          <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                 <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                        <%--<telerik:RadGrid ID="gvAndConditions" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                           <AlternatingItemStyle CssClass="rgAltRow" />
                           <ClientSettings>
                           <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                           </ClientSettings>
                            <MasterTableView DataKeyNames="ApprovalRuleConditionID,SelectionCriteria" TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="SelectionCriteria" HeaderText="Criteria" SortExpression="SelectionCriteria">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Users">
                                        <ItemTemplate>
                                            <asp:CheckBoxList ID="chkLstAndUser" runat="server"></asp:CheckBoxList>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="From" HeaderText="From" SortExpression="From">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="GroupName" HeaderText="GroupName" SortExpression="GroupName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server"
          CommandArgument='<%#Eval("ApprovalRuleConditionID") %>'                                    
                                                CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                 <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>--%>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblRNF" runat="server" Font-Bold="False" Text="No Record Found" Visible="False"
                            CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 28%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 22%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblSelectionAndCriteria" runat="server" Text="Selection Criteria" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqSelAndCriteria" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 28%">
            <asp:Label ID="lblApprovalGroup" runat="server" Text="Select Approval Group" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqAppAndGroup" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 22%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlSelAndCriteria" runat="server" CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem>All</asp:ListItem>
                <asp:ListItem>Any</asp:ListItem>
                <asp:ListItem>Specific</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlAppAndGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlSelAndCriteria" runat="server" ControlToValidate="ddlSelAndCriteria"
                Display="Dynamic" ErrorMessage="Please select Criteria" InitialValue="0" SetFocusOnError="True" ValidationGroup="AND"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 33%">
            <asp:RequiredFieldValidator ID="rfvddlAndAppGroup" runat="server" ControlToValidate="ddlAppAndGroup"
                Display="Dynamic" ErrorMessage="Please select Approval Group" InitialValue="0" SetFocusOnError="True" ValidationGroup="AND"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 17%">
            &nbsp;
        </td>
    </tr>
  
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <table id="tblCount" runat="server" style="width:100%">
                  <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCount" runat="server" Text="Count"
                CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqAndCount" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        
    </tr>
                <tr>
                    <td ><asp:TextBox ID="txtAndCount" runat="server" CssClass="txtbox" MaxLength="8"></asp:TextBox></td>
                </tr>
            </table>
            <table id="tblChkListUserCount" runat="server" style="width:100%">
                  <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblUser" runat="server" Text="Select User"
                CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqUser" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        
    </tr>
                <tr>
                    <td > <asp:Panel ID="pnlClientUser" runat="server" Width="100%" ScrollBars="Vertical"
                Height="68px" >
                <asp:CheckBoxList ID="chklstClientUser" runat="server" CssClass="chkBox" Width="100%" >
                </asp:CheckBoxList>
            </asp:Panel></td>
                </tr>
            </table>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">

            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
  
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
                                        <asp:CustomValidator ID="cvAndAppUser" runat="server" ClientValidationFunction="ValidateCheckClientUserList" ErrorMessage="Please Select Atleast one User" Enabled="False" ValidationGroup="AND" Display="Dynamic"></asp:CustomValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">

            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;<asp:Button ID="btnSaveAnd" runat="server" Text="Save" CssClass="btn" Width="71px" ValidationGroup="AND" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False"
               />
        </td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCriteriaOr" runat="server" Text="Match Any of these Conditions" CssClass="lbl"></asp:Label>
            </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <telerik:RadGrid ID="gvOrConditions" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="7" CssClass="RadGrid">
                           <AlternatingItemStyle CssClass="rgAltRow" />
                           <ClientSettings>
                           <Scrolling UseStaticHeaders="true" />
                           </ClientSettings>
                            <MasterTableView TableLayout="Fixed" DataKeyNames="ApprovalRuleConditionID,SelectionCriteria">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="SelectionCriteria" HeaderText="Criteria" SortExpression="SelectionCriteria">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="View Users">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" Text="View Users" NavigateUrl='<%#NavigateURL("ViewUsers", "&mid=" & Me.ModuleId & "&AppRuleCondID=" & Eval("ApprovalRuleConditionID") & "&ApprovalRuleID=" & Request.QueryString("AppruleID") & "&CompanyCode=" & Request.QueryString("CompanyCode"))%>'
                                                ToolTip="View Users" ForeColor="Blue">Users</asp:HyperLink>
                                        </ItemTemplate>
                                      <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="UserCount" HeaderText="Count" SortExpression="UserCount">
                                          <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="From" HeaderText="From" SortExpression="From">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="GroupName" HeaderText="GroupName" SortExpression="GroupName">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server"
          CommandArgument='<%#Eval("ApprovalRuleConditionID")%>'                                    
                                                CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                 <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                        <%--<telerik:RadGrid ID="gvOrConditions" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                           <AlternatingItemStyle CssClass="rgAltRow" />
                           <ClientSettings>
                           <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                           </ClientSettings>
                            <MasterTableView TableLayout="Fixed" DataKeyNames="ApprovalRuleConditionID,SelectionCriteria">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="SelectionCriteria" HeaderText="Criteria" SortExpression="SelectionCriteria">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Users">
                                        <ItemTemplate>
                                            <asp:CheckBoxList ID="chkLstOrUser" runat="server"></asp:CheckBoxList>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="From" HeaderText="From" SortExpression="From">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="GroupName" HeaderText="GroupName" SortExpression="GroupName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server"
          CommandArgument='<%#Eval("ApprovalRuleConditionID")%>'                                    
                                                CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                 <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>--%>
         </td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblRNFOr" runat="server" Font-Bold="False" Text="No Record Found" Visible="False"
                            CssClass="headingblue"></asp:Label>
         </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblSelectionORCriteria" runat="server" Text="Selection Criteria" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqSelOrCriteria" runat="server" Text="*" ForeColor="Red"></asp:Label>
         </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblApprovalOrGroup" runat="server" Text="Select Approval Group" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqAppAndGroup0" runat="server" Text="*" ForeColor="Red"></asp:Label>
         </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="auto-style1">
            <asp:DropDownList ID="ddlSelOrCriteria" runat="server" CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem>All</asp:ListItem>
                <asp:ListItem>Any</asp:ListItem>
                <asp:ListItem>Specific</asp:ListItem>
            </asp:DropDownList>
         </td>
        <td align="left" valign="top" class="auto-style1">
            </td>
        <td align="left" valign="top" class="auto-style1">
            <asp:DropDownList ID="ddlAppOrGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
         </td>
        <td align="left" valign="top" class="auto-style1">
            </td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlSelOrCriteria" runat="server" ControlToValidate="ddlSelOrCriteria"
                Display="Dynamic" ErrorMessage="Please select Criteria" InitialValue="0" SetFocusOnError="True" ValidationGroup="OR"></asp:RequiredFieldValidator>
         </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlOrAppGroup" runat="server" ControlToValidate="ddlAppOrGroup"
                Display="Dynamic" ErrorMessage="Please select Approval Group" InitialValue="0" SetFocusOnError="True" ValidationGroup="OR"></asp:RequiredFieldValidator>
         </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <table id="tblCount0" runat="server" style="width:100%">
                <tr align="left" style="width: 100%" valign="top">
                    <td align="left" style="width: 25%" valign="top">
                        <asp:Label ID="lblOrCount" runat="server" CssClass="lbl" Text="Count"></asp:Label>
                        <asp:Label ID="lblReqOrCount" runat="server" ForeColor="Red" Text="*"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtOrCount" runat="server" CssClass="txtbox" MaxLength="8"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <table id="tblChkListUserCountOr" runat="server" style="width:100%">
                <tr align="left" style="width: 100%" valign="top">
                    <td align="left" style="width: 25%" valign="top">
                        <asp:Label ID="lblUserOr" runat="server" CssClass="lbl" Text="Select User"></asp:Label>
                        <asp:Label ID="lblReqUserOr" runat="server" ForeColor="Red" Text="*"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Panel ID="pnlClientUserOR" runat="server" Height="68px" ScrollBars="Vertical" Width="100%">
                            <asp:CheckBoxList ID="chklstClientUserOR" runat="server" CssClass="chkBox" Width="100%">
                            </asp:CheckBoxList>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
         </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
                                        <asp:CustomValidator ID="cvORAppUser" runat="server" ClientValidationFunction="ValidateCheckClientUserList2" ErrorMessage="Please Select Atleast one User" ValidationGroup="OR" Enabled="False" Display="Dynamic"></asp:CustomValidator>
         </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
     <tr align="center" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnSaveOR" runat="server" Text="Save" CssClass="btn" Width="71px" ValidationGroup="OR" />
            <asp:Button ID="btnCancel0" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False"
               />
            </td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
</table>
