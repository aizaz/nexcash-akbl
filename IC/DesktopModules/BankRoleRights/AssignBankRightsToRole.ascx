﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AssignBankRightsToRole.ascx.vb"
    Inherits="DesktopModules_BankRoleRights_AssignBankRightsToRole" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<script type="text/javascript" language="javascript">
        function clientNodeChecked(sender, eventArgs) {
            var node = eventArgs.get_node();

            if (node.get_parent().get_nodes().get_count() > 0) {

                for (i = 0; i < node.get_parent().get_nodes().get_count(); i++) {
                var nodesecond = new Telerik.Web.UI.RadTreeNode();
                 nodesecond  = node.get_parent().get_nodes(i);
                    if ((nodesecond.get_text()) = "View") { 

                    if (nodesecond.get_checked()=true){
                    else
                    
                    
                    }


                    }
                    else{
                    
                    
                    
                    }
                    
                    }

                
                }
            
            
            }
            else{
            
            
            
            }
        }
</script>--%>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="false">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRoleType" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" colspan="2">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Assign Rights To Bank Roles" CssClass="headingblue"></asp:Label>
                    </td>
                    <td align="left" valign="top" colspan="2">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 50%">
                        <asp:Label ID="lblRole" runat="server" Text="Select Role" CssClass="lbl"></asp:Label>
                        <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 50%">
                    
                    
                    
                        <asp:Label ID="lblIsApproved" runat="server" CssClass="lbl" Text="Is Approve" 
                            Visible="False"></asp:Label>
                    
                    
                    
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 50%">
                        <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always" EnableViewState="true">
                            <ContentTemplate>--%>
                        <asp:DropDownList ID="ddlRole" runat="server" AutoPostBack="True" CssClass="dropdown">
                        </asp:DropDownList>
                        <br />
                        <asp:RequiredFieldValidator ID="rfvRole" runat="server" ControlToValidate="ddlRole"
                            CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Role" SetFocusOnError="True"
                            ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" valign="top" style="width: 50%">
                                <asp:CheckBox ID="chkApproved" runat="server" CssClass="chkBox" 
                            Text=" " Visible="False" />
                            </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 100%" colspan="2">
                        <asp:Label ID="lblRightType" runat="server" CssClass="headingblue" Text="Rights List"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 100%" colspan="2">
                        <asp:Panel ID="pnlRightList" runat="server" Width="100%" Height="400px" ScrollBars="Vertical">
                            <asp:UpdatePanel ID="pnlCompanyRights" runat="server" UpdateMode="Always" EnableViewState="true">
                                <ContentTemplate>
                                    <telerik:RadTreeView ID="radtvRights" runat="server" CheckChildNodes="True" TriStateCheckBoxes="true">
                                        <Nodes >
                                            <telerik:RadTreeNode id="radMainTreeNode" runat="server" Checkable="true" Value="top" Text="(Select All)">
                                            </telerik:RadTreeNode>
                                        </Nodes>
                                    </telerik:RadTreeView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" colspan="2">
                        <asp:Button ID="btnApproved" runat="server" Text="Approve" CssClass="btn" 
                            Width="71px" />
                        &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="79px" />
                        &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False"
                             />
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" colspan="2">
                        &nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" colspan="4">
                        &nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" colspan="2">
                        &nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
