﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveBulkBankBranch.ascx.vb"
    Inherits="DesktopModules_BankBranchManagement_SaveBulkBankBranch" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="left" valign="top">
                        <asp:Label ID="lblBulkBranchList" runat="server" Text="Add Bank Branch List" CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <table width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:FileUpload ID="FileUploadBranchesinBulk" runat="server" />
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                        <asp:HiddenField ID="hfFilePath" runat="server" />
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:RequiredFieldValidator ID="rfvFile" runat="server" ControlToValidate="FileUploadBranchesinBulk"
                            CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select File" SetFocusOnError="True"
                            ToolTip="Required Field"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                        </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top">
            
                <telerik:RadGrid ID="gvBankBranch" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                    <%--<AlternatingItemStyle CssClass="rgAltRow" />--%>
                    <ClientSettings>
                    <Scrolling AllowScroll="true" UseStaticHeaders="true"/>
                    </ClientSettings>
                    <MasterTableView UseAllDataFields="true" TableLayout="Fixed">
                        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                        <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn DataField="Code" HeaderText="Code" SortExpression="Code">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Name" HeaderText="Name" SortExpression="Name">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="BankCode" HeaderText="Bank Code" SortExpression="BankCode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="BankName" HeaderText="Bank Name" SortExpression="BankName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CityID" HeaderText="City ID" SortExpression="CityID">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CityName" HeaderText="City Name" SortExpression="CityName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Phone1" HeaderText="Phone1" SortExpression="Phone1">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Phone2" HeaderText="Phone2" SortExpression="Phone2">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Address" HeaderText="Address" SortExpression="Address">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Description" HeaderText="Description" SortExpression="Description">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="IsBranch" HeaderText="IsBranch" SortExpression="IsBranch">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="IsPrintLocation" HeaderText="IsPrint Location"
                                SortExpression="IsPrintLocation">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ClearingNormalAccountNumber" HeaderText="Clearing Normal Account Number"
                                SortExpression="ClearingNormalAccountNumber">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ClearingNormalBranchCode" HeaderText="Clearing Normal Branch Code"
                                SortExpression="ClearingNormalBranchCode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ClearingNormalCurrency" HeaderText="Clearing Normal Currency"
                                SortExpression="ClearingNormalCurrency">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="InterCityNormalAccountNumber" HeaderText="InterCity Account Number"
                                SortExpression="InterCityNormalAccountNumber">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="InterCityNormalBranchCode" HeaderText="InterCity Branch Code"
                                SortExpression="InterCityNormalBranchCode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="InterCityNormalCurrency" HeaderText="InterCity Currency"
                                SortExpression="InterCityNormalCurrency">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="SameDayNormalAccountNumber" HeaderText="SameDay Account Number"
                                SortExpression="SameDayNormalAccountNumber">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="SameDayNormalBranchCode" HeaderText="SameDay Branch Code"
                                SortExpression="SameDayNormalBranchCode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="SameDayNormalCurrency" HeaderText="SameDay Currency"
                                SortExpression="SameDayNormalCurrency">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                        </EditFormSettings>
                        <PagerStyle AlwaysVisible="True" />
                    </MasterTableView>
                    <ItemStyle CssClass="rgRow" />
                    <PagerStyle AlwaysVisible="True" />
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                </telerik:RadGrid>
            
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <asp:Button ID="btnAddBranchesinBulk" runat="server" Text="Add Branches in Bulk"
                CssClass="btn" CausesValidation="False" />
                        <asp:Button ID="btnProceed" runat="server" Text="Proceed" CssClass="btn" OnClick="btnProceed_Click" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px"
                CausesValidation="False" />
        </td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <telerik:RadGrid ID="gvUnVarified" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                CellSpacing="0" ShowFooter="True">
                <AlternatingItemStyle CssClass="rgAltRow" />
                <MasterTableView>
                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn DataField="RowNo" HeaderText="Line No." SortExpression="RowNo">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ErrorMessage" HeaderText="ErrorMessage" SortExpression="ErrorMessage">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                    <PagerStyle AlwaysVisible="True" />
                </MasterTableView>
                <ItemStyle CssClass="rgRow" />
                <PagerStyle AlwaysVisible="True" />
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
        </td>
    </tr>
</table>
