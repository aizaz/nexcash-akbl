﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals

Partial Class DesktopModules_CompanyOfficeManagement_SaveCompanyOffice
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private OfficeID As String
    Private htRights As Hashtable

#Region "Page Load"

    Protected Sub DesktopModules_CompanyOfficeManagement_SaveCompanyOffice_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            OfficeID = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing

                lblPrintLocation.Visible = False
                chkPrntLocation.Visible = False
                chkPrntLocation.Checked = False


                lblIsAllowedForTagging.Visible = False
                chkIsAllowedForTagging.Visible = False
                chkIsAllowedForTagging.Checked = False


                lblNormalClearingAcc.Visible = False
                lblIntercityClearingAcc.Visible = False
                lblSameDayClearingAcc.Visible = False
                lblBranchCashTillAccount.Visible = False
                ddlNormalClearingAcc.Visible = False
                ddlIntercityClearingAcc.Visible = False
                ddlSameDayClearingAcc.Visible = False
                ddlBranchCashTillAccount.Visible = False
                LoadddlCountry()
                LoadddlProvince()
                LoadddlCity()
                LoadddlBank("")
                LoadddlNormalClearingAccount()
                LoadddlIntercityClearingAccount()
                LoadddlSameDayClearingAccount()
                LoadddlBranchCashTillAccount()
              
                If OfficeID.ToString() = "0" Then
                    Clear()
                    lblIsApproved.Visible = False
                    chkApproved.Visible = False
                    btnApproved.Visible = False
                    lblPageHeader.Text = "Add Bank Branch"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))

                Else
                    lblIsApproved.Visible = CBool(htRights("Approve"))
                    chkApproved.Visible = CBool(htRights("Approve"))
                    btnApproved.Visible = CBool(htRights("Approve"))
                    LoadddlSameDayClearingAccount()
                    LoadddlNormalClearingAccount()
                    LoadddlIntercityClearingAccount()
                    LoadddlBranchCashTillAccount()
                    'txtBranchCode.Enabled = False
                    'txtBranchName.Enabled = False
                    lblIsApproved.Visible = True
                    chkApproved.Visible = True
                    btnApproved.Visible = True
                    ddlBankType.Enabled = False
                    ddlBank.Enabled = False
                    lblPageHeader.Text = "Edit Bank Branch"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                   
                    Dim icOffice As New ICOffice
                    icOffice.es.Connection.CommandTimeout = 3600
                    icOffice.LoadByPrimaryKey(OfficeID)
                    If icOffice.LoadByPrimaryKey(OfficeID) Then
                        chkActive.Checked = icOffice.IsActive
                        chkApproved.Checked = icOffice.IsApprove
                        If icOffice.IsApprove = True Then
                            chkApproved.Enabled = False
                            btnApproved.Visible = False
                        End If
                    End If

                    Dim objICBankBranch As New ICOffice
                    Dim objBank As New ICBank
                    Dim objBankAccounts As New ICBankAccounts
                    objICBankBranch.es.Connection.CommandTimeout = 3600

                    If objICBankBranch.LoadByPrimaryKey(OfficeID) Then
                        txtBranchCode.Text = objICBankBranch.OfficeCode
                        txtBranchPhone1.Text = objICBankBranch.Phone1
                        txtBranchPhone2.Text = objICBankBranch.Phone2
                        txtBranchAddress.Text = objICBankBranch.Address
                        txtBranchDescrption.Text = objICBankBranch.Description
                        txtBranchName.Text = objICBankBranch.OfficeName
                        txtBranchEmail.Text = objICBankBranch.Email
                        chkIsAllowedForTagging.Checked = objICBankBranch.IsAllowedForTagging
                        If Not objICBankBranch.IsActive Is Nothing Then
                            chkActive.Checked = objICBankBranch.IsActive
                        Else
                            chkActive.Checked = False
                        End If
                        If Not objICBankBranch.IsApprove Is Nothing Then
                            chkApproved.Checked = objICBankBranch.IsApprove
                        Else
                            chkApproved.Checked = False
                        End If
                        If Not objICBankBranch.IsPrintingLocation Is Nothing Then
                            chkPrntLocation.Checked = objICBankBranch.IsPrintingLocation
                        Else
                            chkPrntLocation.Checked = False
                        End If
                       

                        If objBank.LoadByPrimaryKey(objICBankBranch.BankCode.ToString()) Then
                            If objBank.IsPrincipal = True Then
                                Dim AccNo As String = Nothing
                                ddlBankType.SelectedValue = "Principal Bank"
                                LoadddlBank("Principal Bank")

                                If objICBankBranch.ClearingNormalAccountType = "GL" Then
                                    ddlNormalClearingAcc.SelectedValue = objICBankBranch.ClearingNormalAccountType & "-" & objICBankBranch.ClearingNormalAccountNumber & "-" & objICBankBranch.ClearingNormalAccountBranchCode & "-" & objICBankBranch.ClearingNormalAccountCurrency & "-" & "-" & "-" & "-" & "-" & "-"
                                Else
                                    AccNo = Nothing
                                    AccNo = objICBankBranch.ClearingNormalAccountType & "-" & objICBankBranch.ClearingNormalAccountNumber & "-" & objICBankBranch.ClearingNormalAccountBranchCode & "-" & objICBankBranch.ClearingNormalAccountCurrency & "-" & "-" & "-" & "-" & "-" & "-"
                                    ddlNormalClearingAcc.SelectedValue = AccNo
                                End If
                                If objICBankBranch.BranchCashTillAccountType = "GL" Then
                                    ddlBranchCashTillAccount.SelectedValue = objICBankBranch.BranchCashTillAccountType & "-" & objICBankBranch.BranchCashTillAccountNumber & "-" & objICBankBranch.BranchCashTillAccountBranchCode & "-" & objICBankBranch.BranchCashTillAccountCurrency & "-" & "-" & "-" & "-" & "-" & "-"
                                Else
                                    AccNo = Nothing
                                    AccNo = objICBankBranch.BranchCashTillAccountType & "-" & objICBankBranch.BranchCashTillAccountNumber & "-" & objICBankBranch.BranchCashTillAccountBranchCode & "-" & objICBankBranch.BranchCashTillAccountCurrency & "-" & "-" & "-" & "-" & "-" & "-"
                                    ddlBranchCashTillAccount.SelectedValue = AccNo
                                End If
                                If objICBankBranch.InterCityAccountType = "GL" Then
                                    ddlIntercityClearingAcc.SelectedValue = objICBankBranch.InterCityAccountType & "-" & objICBankBranch.InterCityAccountNumber & "-" & objICBankBranch.InterCityAccountBranchCode & "-" & objICBankBranch.InterCityAccountCurrency & "-" & "-" & "-" & "-" & "-" & "-"
                                Else
                                    AccNo = Nothing
                                    AccNo = objICBankBranch.InterCityAccountType & "-" & objICBankBranch.InterCityAccountNumber & "-" & objICBankBranch.InterCityAccountBranchCode & "-" & objICBankBranch.InterCityAccountCurrency & "-" & "-" & "-" & "-" & "-" & "-"
                                    ddlIntercityClearingAcc.SelectedValue = AccNo
                                End If
                                If objICBankBranch.SameDayAccountType = "GL" Then
                                    ddlSameDayClearingAcc.SelectedValue = objICBankBranch.SameDayAccountType & "-" & objICBankBranch.SameDayAccountNumber & "-" & objICBankBranch.SameDayAccountBranchCode & "-" & objICBankBranch.SameDayAccountCurrency & "-" & "-" & "-" & "-" & "-" & "-"
                                Else
                                    AccNo = Nothing
                                    AccNo = objICBankBranch.SameDayAccountType & "-" & objICBankBranch.SameDayAccountNumber & "-" & objICBankBranch.SameDayAccountBranchCode & "-" & objICBankBranch.SameDayAccountCurrency & "-" & "-" & "-" & "-" & "-" & "-"
                                    ddlSameDayClearingAcc.SelectedValue = AccNo
                                End If
                                'If objICBankBranch.ClearingNormalAccountType = "GL" Then
                                '    ddlNormalClearingAcc.SelectedValue = objICBankBranch.ClearingNormalAccountType & "-" & objICBankBranch.ClearingNormalAccountNumber & "-" & objICBankBranch.ClearingNormalAccountBranchCode & "-" & objICBankBranch.ClearingNormalAccountCurrency & "-" & objICBankBranch.ClearingNormalSeqNo & "-" & objICBankBranch.ClearingNormalClientNo & "-" & objICBankBranch.ClearingNormalProfitCenter
                                'Else
                                '    AccNo = Nothing
                                '    AccNo = objICBankBranch.ClearingNormalAccountType & "-" & objICBankBranch.ClearingNormalAccountNumber & "-" & objICBankBranch.ClearingNormalAccountBranchCode & "-" & objICBankBranch.ClearingNormalAccountCurrency & "-" & "-" & "-" & "-" & "-" & "-"
                                '    ddlNormalClearingAcc.SelectedValue = AccNo
                                'End If
                                'If objICBankBranch.InterCityAccountType = "GL" Then
                                '    ddlIntercityClearingAcc.SelectedValue = objICBankBranch.InterCityAccountType & "-" & objICBankBranch.InterCityAccountNumber & "-" & objICBankBranch.InterCityAccountBranchCode & "-" & objICBankBranch.InterCityAccountCurrency & "-" & objICBankBranch.InterCityAccountSeqNo & "-" & objICBankBranch.InterCityAccountClientNo & "-" & objICBankBranch.InterCityAccountProfitCentre
                                'Else
                                '    AccNo = Nothing
                                '    AccNo = objICBankBranch.InterCityAccountType & "-" & objICBankBranch.InterCityAccountNumber & "-" & objICBankBranch.InterCityAccountBranchCode & "-" & objICBankBranch.InterCityAccountCurrency & "-" & "-" & "-" & "-" & "-" & "-"
                                '    ddlIntercityClearingAcc.SelectedValue = AccNo
                                'End If
                                'If objICBankBranch.SameDayAccountType = "GL" Then
                                '    ddlSameDayClearingAcc.SelectedValue = objICBankBranch.SameDayAccountType & "-" & objICBankBranch.SameDayAccountNumber & "-" & objICBankBranch.SameDayAccountBranchCode & "-" & objICBankBranch.SameDayAccountCurrency & "-" & objICBankBranch.SameDayAccountSeqNo & "-" & objICBankBranch.SameDayAccountClientNo & "-" & objICBankBranch.SameDayAccountProfitCentre
                                'Else
                                '    AccNo = Nothing
                                '    AccNo = objICBankBranch.SameDayAccountType & "-" & objICBankBranch.SameDayAccountNumber & "-" & objICBankBranch.SameDayAccountBranchCode & "-" & objICBankBranch.SameDayAccountCurrency & "-" & "-" & "-" & "-" & "-" & "-"
                                '    ddlSameDayClearingAcc.SelectedValue = AccNo
                                'End If



                                lblNormalClearingAcc.Visible = True
                                lblIntercityClearingAcc.Visible = True
                                lblSameDayClearingAcc.Visible = True
                                lblBranchCashTillAccount.Visible = True
                                ddlBranchCashTillAccount.Visible = True
                                ddlNormalClearingAcc.Visible = True
                                ddlIntercityClearingAcc.Visible = True
                                ddlSameDayClearingAcc.Visible = True
                                lblPrintLocation.Visible = True
                                chkPrntLocation.Visible = True
                                chkPrntLocation.Checked = objICBankBranch.IsPrintingLocation
                                lblIsAllowedForTagging.Visible = True
                                chkIsAllowedForTagging.Visible = True
                            Else
                                ddlBankType.SelectedValue = "Non-Principal Bank"
                                LoadddlBank("Non-Principal Bank")
                            End If
                                ddlBank.SelectedValue = objICBankBranch.BankCode.ToString()
                            End If

                        Dim ICcty As New ICCity
                        Dim ICProvince As New ICProvince
                        Dim ICCountry As New ICCountry
                        ICcty.es.Connection.CommandTimeout = 3600
                        ICProvince.es.Connection.CommandTimeout = 3600
                        ICCountry.es.Connection.CommandTimeout = 3600

                        ICcty.LoadByPrimaryKey(objICBankBranch.CityID.ToString())
                        ICProvince = ICcty.UpToICProvinceByProvinceCode()
                        ICCountry = ICProvince.UpToICCountryByCountryCode()

                        LoadddlCountry()
                        ddlCountry.SelectedValue = ICCountry.CountryCode
                        LoadddlProvince()
                        ddlProvince.SelectedValue = ICProvince.ProvinceCode()
                        LoadddlCity()
                        ddlCity.SelectedValue = ICcty.CityCode
                    End If

                End If

                End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Bank Branch Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        txtBranchCode.Text = ""
        txtBranchName.Text = ""
        txtBranchPhone1.Text = ""
        txtBranchPhone2.Text = ""
        txtBranchAddress.Text = ""
        txtBranchDescrption.Text = ""
        txtBranchName.Text = ""
        txtBranchEmail.Text = ""
        ddlBankType.ClearSelection()
        LoadddlBank(ddlBankType.SelectedValue.ToString)
        LoadddlCountry()
        LoadddlProvince()
        LoadddlCity()
        LoadddlNormalClearingAccount()
        LoadddlIntercityClearingAccount()
        LoadddlSameDayClearingAccount()
        chkActive.Checked = True
        chkApproved.Checked = False
        chkPrntLocation.Checked = False
        chkIsAllowedForTagging.Checked = False
    End Sub

#End Region

#Region "Button Events"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim objICBankBranch As New ICOffice
                Dim prevobjICOffce As New ICOffice
                Dim prevCity As New ICCity
                Dim ActionStr As String
                Dim CurrentAt As String
                Dim PrevAt As String

                objICBankBranch.es.Connection.CommandTimeout = 3600
                prevobjICOffce.es.Connection.CommandTimeout = 3600
                prevCity.es.Connection.CommandTimeout = 3600


                If OfficeID.ToString() <> "0" Then
                    objICBankBranch.LoadByPrimaryKey(OfficeID)
                    prevobjICOffce.LoadByPrimaryKey(objICBankBranch.OfficeID())
                    prevCity.LoadByPrimaryKey(prevobjICOffce.CityID.ToString())
                End If

                objICBankBranch.CityID = ddlCity.SelectedValue.ToString()
                objICBankBranch.OfficeCode = txtBranchCode.Text.ToString()
                objICBankBranch.Email = txtBranchEmail.Text.ToString()
                objICBankBranch.Phone1 = txtBranchPhone1.Text
                objICBankBranch.Phone2 = txtBranchPhone2.Text
                objICBankBranch.Address = txtBranchAddress.Text.ToString()
                objICBankBranch.Description = txtBranchDescrption.Text
                objICBankBranch.BankCode = ddlBank.SelectedValue.ToString()
                objICBankBranch.OfficeName = txtBranchName.Text.ToString()
                objICBankBranch.CompanyCode = Nothing
                objICBankBranch.IsActive = chkActive.Checked
                objICBankBranch.OffceType = "Branch Office"
                objICBankBranch.IsPrintingLocation = chkPrntLocation.Checked
                objICBankBranch.IsBranch = True
                objICBankBranch.IsApprove = False
                objICBankBranch.IsAllowedForTagging = chkIsAllowedForTagging.Checked
                If ddlBankType.SelectedValue = "Principal Bank" Then

                    If ddlNormalClearingAcc.SelectedValue.ToString() = "0" Then
                        objICBankBranch.ClearingNormalAccountBranchCode = Nothing
                        objICBankBranch.ClearingNormalAccountCurrency = Nothing
                        objICBankBranch.ClearingNormalAccountNumber = Nothing
                        objICBankBranch.ClearingNormalSeqNo = Nothing
                        objICBankBranch.ClearingNormalClientNo = Nothing
                        objICBankBranch.ClearingNormalProfitCenter = Nothing
                        objICBankBranch.ClearingNormalAccountType = Nothing
                    Else
                        If ddlNormalClearingAcc.SelectedValue.Split("-")(0).ToString() = "GL" Then
                            objICBankBranch.ClearingNormalAccountType = ddlNormalClearingAcc.SelectedValue.Split("-")(0).ToString()
                            objICBankBranch.ClearingNormalAccountNumber = ddlNormalClearingAcc.SelectedValue.Split("-")(1).ToString()
                            objICBankBranch.ClearingNormalAccountBranchCode = ddlNormalClearingAcc.SelectedValue.Split("-")(2).ToString()
                            objICBankBranch.ClearingNormalAccountCurrency = ddlNormalClearingAcc.SelectedValue.Split("-")(3).ToString()
                            objICBankBranch.ClearingNormalSeqNo = Nothing
                            objICBankBranch.ClearingNormalClientNo = Nothing
                            objICBankBranch.ClearingNormalProfitCenter = Nothing
                        Else
                            objICBankBranch.ClearingNormalAccountType = ddlNormalClearingAcc.SelectedValue.Split("-")(0).ToString()
                            objICBankBranch.ClearingNormalAccountNumber = ddlNormalClearingAcc.SelectedValue.Split("-")(1).ToString()
                            objICBankBranch.ClearingNormalAccountBranchCode = ddlNormalClearingAcc.SelectedValue.Split("-")(2).ToString()
                            objICBankBranch.ClearingNormalAccountCurrency = ddlNormalClearingAcc.SelectedValue.Split("-")(3).ToString()
                            objICBankBranch.ClearingNormalSeqNo = Nothing
                            objICBankBranch.ClearingNormalClientNo = Nothing
                            objICBankBranch.ClearingNormalProfitCenter = Nothing
                        End If


                    End If
                    If ddlBranchCashTillAccount.SelectedValue.ToString() = "0" Then
                        objICBankBranch.BranchCashTillAccountBranchCode = Nothing
                        objICBankBranch.BranchCashTillAccountCurrency = Nothing
                        objICBankBranch.BranchCashTillAccountNumber = Nothing
                        objICBankBranch.BranchCashTillAccountSeqNo = Nothing
                        objICBankBranch.BranchCashTillAccountClientNo = Nothing
                        objICBankBranch.BranchCashTillAccountProfitCentre = Nothing
                        objICBankBranch.BranchCashTillAccountType = Nothing
                    Else
                        If ddlBranchCashTillAccount.SelectedValue.Split("-")(0).ToString() = "GL" Then
                            objICBankBranch.BranchCashTillAccountType = ddlBranchCashTillAccount.SelectedValue.Split("-")(0).ToString()
                            objICBankBranch.BranchCashTillAccountNumber = ddlBranchCashTillAccount.SelectedValue.Split("-")(1).ToString()
                            objICBankBranch.BranchCashTillAccountBranchCode = ddlBranchCashTillAccount.SelectedValue.Split("-")(2).ToString()
                            objICBankBranch.BranchCashTillAccountCurrency = ddlBranchCashTillAccount.SelectedValue.Split("-")(3).ToString()
                            objICBankBranch.BranchCashTillAccountSeqNo = Nothing
                            objICBankBranch.BranchCashTillAccountClientNo = Nothing
                            objICBankBranch.BranchCashTillAccountProfitCentre = Nothing
                        Else
                            objICBankBranch.BranchCashTillAccountType = ddlBranchCashTillAccount.SelectedValue.Split("-")(0).ToString()
                            objICBankBranch.BranchCashTillAccountNumber = ddlBranchCashTillAccount.SelectedValue.Split("-")(1).ToString()
                            objICBankBranch.BranchCashTillAccountBranchCode = ddlBranchCashTillAccount.SelectedValue.Split("-")(2).ToString()
                            objICBankBranch.BranchCashTillAccountCurrency = ddlBranchCashTillAccount.SelectedValue.Split("-")(3).ToString()
                            objICBankBranch.BranchCashTillAccountSeqNo = Nothing
                            objICBankBranch.BranchCashTillAccountClientNo = Nothing
                            objICBankBranch.BranchCashTillAccountProfitCentre = Nothing
                        End If


                    End If
                    If ddlIntercityClearingAcc.SelectedValue.ToString() = "0" Then
                        objICBankBranch.InterCityAccountBranchCode = Nothing
                        objICBankBranch.InterCityAccountCurrency = Nothing
                        objICBankBranch.InterCityAccountNumber = Nothing
                        objICBankBranch.InterCityAccountType = Nothing
                        objICBankBranch.InterCityAccountSeqNo = Nothing
                        objICBankBranch.InterCityAccountClientNo = Nothing
                        objICBankBranch.InterCityAccountProfitCentre = Nothing
                    Else
                        If ddlIntercityClearingAcc.SelectedValue.Split("-")(0).ToString() = "GL" Then
                            objICBankBranch.InterCityAccountType = ddlIntercityClearingAcc.SelectedValue.Split("-")(0).ToString()
                            objICBankBranch.InterCityAccountNumber = ddlIntercityClearingAcc.SelectedValue.Split("-")(1).ToString()
                            objICBankBranch.InterCityAccountBranchCode = ddlIntercityClearingAcc.SelectedValue.Split("-")(2).ToString()
                            objICBankBranch.InterCityAccountCurrency = ddlIntercityClearingAcc.SelectedValue.Split("-")(3).ToString()
                            objICBankBranch.InterCityAccountSeqNo = Nothing
                            objICBankBranch.InterCityAccountClientNo = Nothing
                            objICBankBranch.InterCityAccountProfitCentre = Nothing
                        Else
                            objICBankBranch.InterCityAccountType = ddlIntercityClearingAcc.SelectedValue.Split("-")(0).ToString()
                            objICBankBranch.InterCityAccountNumber = ddlIntercityClearingAcc.SelectedValue.Split("-")(1).ToString()
                            objICBankBranch.InterCityAccountBranchCode = ddlIntercityClearingAcc.SelectedValue.Split("-")(2).ToString()
                            objICBankBranch.InterCityAccountCurrency = ddlIntercityClearingAcc.SelectedValue.Split("-")(3).ToString()
                            objICBankBranch.InterCityAccountSeqNo = Nothing
                            objICBankBranch.InterCityAccountClientNo = Nothing
                            objICBankBranch.InterCityAccountProfitCentre = Nothing
                        End If

                    End If

                    If ddlSameDayClearingAcc.SelectedValue.ToString() = "0" Then
                        objICBankBranch.SameDayAccountBranchCode = Nothing
                        objICBankBranch.SameDayAccountCurrency = Nothing
                        objICBankBranch.SameDayAccountNumber = Nothing
                        objICBankBranch.SameDayAccountType = Nothing
                        objICBankBranch.SameDayAccountSeqNo = Nothing
                        objICBankBranch.SameDayAccountClientNo = Nothing
                        objICBankBranch.SameDayAccountProfitCentre = Nothing
                    Else
                        If ddlSameDayClearingAcc.SelectedValue.Split("-")(0).ToString() = "GL" Then
                            objICBankBranch.SameDayAccountType = ddlSameDayClearingAcc.SelectedValue.Split("-")(0).ToString()
                            objICBankBranch.SameDayAccountNumber = ddlSameDayClearingAcc.SelectedValue.Split("-")(1).ToString()
                            objICBankBranch.SameDayAccountBranchCode = ddlSameDayClearingAcc.SelectedValue.Split("-")(2).ToString()
                            objICBankBranch.SameDayAccountCurrency = ddlSameDayClearingAcc.SelectedValue.Split("-")(3).ToString()
                            objICBankBranch.SameDayAccountSeqNo = Nothing
                            objICBankBranch.SameDayAccountClientNo = Nothing
                            objICBankBranch.SameDayAccountProfitCentre = Nothing
                        Else
                            objICBankBranch.SameDayAccountType = ddlSameDayClearingAcc.SelectedValue.Split("-")(0).ToString()
                            objICBankBranch.SameDayAccountNumber = ddlSameDayClearingAcc.SelectedValue.Split("-")(1).ToString()
                            objICBankBranch.SameDayAccountBranchCode = ddlSameDayClearingAcc.SelectedValue.Split("-")(2).ToString()
                            objICBankBranch.SameDayAccountCurrency = ddlSameDayClearingAcc.SelectedValue.Split("-")(3).ToString()
                            objICBankBranch.SameDayAccountSeqNo = Nothing
                            objICBankBranch.SameDayAccountClientNo = Nothing
                            objICBankBranch.SameDayAccountProfitCentre = Nothing
                        End If


                    End If

                End If

                If OfficeID = "0" Then

                    objICBankBranch.CreatedBy = Me.UserId
                    objICBankBranch.CreateDate = Date.Now
                    objICBankBranch.Creater = Me.UserId
                    objICBankBranch.CreationDate = Date.Now
                    If CheckDuplicateCodeName(objICBankBranch) = False Then

                        If ddlBankType.SelectedValue.ToString() = "Principal Bank" Then
                            CurrentAt = "Bank Branch : [Code:  " & objICBankBranch.OfficeCode.ToString() & " ; Name:  " & objICBankBranch.OfficeName.ToString() & " ; Phone1:  " & objICBankBranch.Phone1 & " ; Phone2:  " & objICBankBranch.Phone2 & " ; Address:  " & objICBankBranch.Address.ToString() & " ; Email:  " & objICBankBranch.Email.ToString() & " ; Description:  " & objICBankBranch.Description & " ; City:  " & ddlCity.SelectedItem.ToString() & " ; IsActive:  " & chkActive.Checked.ToString() & " ; IsApproved:  " & chkApproved.Checked.ToString() & " ; Is Printing Location:  " & chkPrntLocation.Checked.ToString() & " ; ClearingNormalAccountNumber: " & objICBankBranch.ClearingNormalAccountNumber & " ; ClearingNormalAccountBranchCode: " & objICBankBranch.ClearingNormalAccountBranchCode & " ; ClearingNormalAccountCurrency: " & objICBankBranch.ClearingNormalAccountCurrency & " ; InterCityAccountNumber: " & objICBankBranch.InterCityAccountNumber & " ; InterCityAccountBranchCode: " & objICBankBranch.InterCityAccountBranchCode & " ; InterCityAccountCurrency: " & objICBankBranch.InterCityAccountCurrency & " ; SameDayAccountNumber: " & objICBankBranch.SameDayAccountNumber & " ; SameDayAccountBranchCode: " & objICBankBranch.SameDayAccountBranchCode & " ; SameDayAccountCurrency: " & objICBankBranch.SameDayAccountCurrency & "]"
                            ActionStr = CurrentAt
                        ElseIf ddlBankType.SelectedValue.ToString() = "Non-Principal Bank" Then
                            CurrentAt = "Bank Branch : [Code:  " & objICBankBranch.OfficeCode.ToString() & " ; Name:  " & objICBankBranch.OfficeName.ToString() & " ; Phone1:  " & objICBankBranch.Phone1 & " ; Phone2:  " & objICBankBranch.Phone2 & " ; Address:  " & objICBankBranch.Address.ToString() & " ; Email:  " & objICBankBranch.Email.ToString() & " ; Description:  " & objICBankBranch.Description & " ; City:  " & ddlCity.SelectedValue.ToString() & " ; IsActive:  " & chkActive.Checked.ToString() & " ; IsApproved:  " & chkApproved.Checked.ToString() & " ; Is Printing Location:  " & chkPrntLocation.Checked.ToString() & "]"
                            ActionStr = CurrentAt
                        End If

                        ICOfficeController.AddOffice(objICBankBranch, False, Me.UserId.ToString(), Me.UserInfo.Username.ToString(), ActionStr)
                        UIUtilities.ShowDialog(Me, "Save Bank Branch", "Bank Branch added successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Else
                        UIUtilities.ShowDialog(Me, "Save Bank Branch", "Can not add duplicate Bank Branch.", Dialogmessagetype.Failure)
                        Exit Sub
                    End If

                Else

                    If CheckDuplicateOnUpdate(objICBankBranch) = True Then
                        UIUtilities.ShowDialog(Me, "Save Bank Branch", "Can not add duplicate Bank Branch.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    If ddlBankType.SelectedValue.ToString() = "Principal Bank" Then
                        CurrentAt = "Bank Branch : Current Values [Code:  " & objICBankBranch.OfficeCode & " ; Name:  " & objICBankBranch.OfficeName & " ; Phone1:  " & objICBankBranch.Phone1 & " ; Phone2:  " & objICBankBranch.Phone2 & " ; Address:  " & objICBankBranch.Address & " ; Email:  " & objICBankBranch.Email & " ; Description:  " & objICBankBranch.Description & " ; City:  " & ddlCity.SelectedItem.ToString & " ; IsActive:  " & chkActive.Checked & " ; IsApproved:  " & chkApproved.Checked & " ; Is Printing Location:  " & chkPrntLocation.Checked & " ; ClearingNormalAccountNumber: " & objICBankBranch.ClearingNormalAccountNumber & " ; ClearingNormalAccountBranchCode: " & objICBankBranch.ClearingNormalAccountBranchCode & " ; ClearingNormalAccountCurrency: " & objICBankBranch.ClearingNormalAccountCurrency & " ; InterCityAccountNumber: " & objICBankBranch.InterCityAccountNumber & " ; InterCityAccountBranchCode: " & objICBankBranch.InterCityAccountBranchCode & " ; InterCityAccountCurrency: " & objICBankBranch.InterCityAccountCurrency & " ; SameDayAccountNumber: " & objICBankBranch.SameDayAccountNumber & " ; SameDayAccountBranchCode: " & objICBankBranch.SameDayAccountBranchCode & " ; SameDayAccountCurrency: " & objICBankBranch.SameDayAccountCurrency & "]"
                        PrevAt = "<br />Bank Branch : Previous Values [Code: " & prevobjICOffce.OfficeCode.ToString() & " ; Name: " & prevobjICOffce.OfficeName.ToString() & " ; Phone1:  " & prevobjICOffce.Phone1 & " ; Phone2:  " & prevobjICOffce.Phone2 & " ; Address:  " & prevobjICOffce.Address & " ; Email:  " & prevobjICOffce.Email & " ; Description:  " & prevobjICOffce.Description & " ; City:  " & prevCity.CityName & " ; IsActive:  " & prevobjICOffce.IsActive & " ; IsApproved:  " & prevobjICOffce.IsApprove & " ; IsBranch:  " & prevobjICOffce.IsBranch & " ; Is Printing Location:  " & prevobjICOffce.IsPrintingLocation & " ; ClearingNormalAccountNumber: " & prevobjICOffce.ClearingNormalAccountNumber & " ; ClearingNormalAccountBranchCode: " & prevobjICOffce.ClearingNormalAccountBranchCode & " ; ClearingNormalAccountCurrency: " & prevobjICOffce.ClearingNormalAccountCurrency & " ; InterCityAccountNumber: " & prevobjICOffce.InterCityAccountNumber & " ; InterCityAccountBranchCode: " & prevobjICOffce.InterCityAccountBranchCode & " ; InterCityAccountCurrency: " & prevobjICOffce.InterCityAccountCurrency & " ; SameDayAccountNumber: " & prevobjICOffce.SameDayAccountNumber & " ; SameDayAccountBranchCode: " & prevobjICOffce.SameDayAccountBranchCode & " ; SameDayAccountCurrency: " & prevobjICOffce.SameDayAccountCurrency & "] "
                        ActionStr = CurrentAt & PrevAt

                    ElseIf ddlBankType.SelectedValue.ToString() = "Non-Principal Bank" Then
                        CurrentAt = "Bank Branch : Current Values [Code:  " & objICBankBranch.OfficeCode & " ; Name:  " & objICBankBranch.OfficeName & " ; Phone1:  " & objICBankBranch.Phone1 & " ; Phone2:  " & objICBankBranch.Phone2 & " ; Address:  " & objICBankBranch.Address & " ; Email:  " & objICBankBranch.Email & " ; Description:  " & objICBankBranch.Description & " ; City:  " & ddlCity.SelectedItem.ToString & " ; IsActive:  " & chkActive.Checked & " ; IsApproved:  " & chkApproved.Checked & " ;  Is Printing Location:  " & chkPrntLocation.Checked & "]"
                        PrevAt = "<br />Bank Branch : Previous Values [Code: " & prevobjICOffce.OfficeCode & " ; Name: " & prevobjICOffce.OfficeName & " ; Phone1:  " & prevobjICOffce.Phone1 & " ; Phone2:  " & prevobjICOffce.Phone2 & " ; Address:  " & prevobjICOffce.Address & " ; Email:  " & prevobjICOffce.Email & " ; Description:  " & prevobjICOffce.Description & " ; City:  " & prevCity.CityName & " ; IsActive:  " & prevobjICOffce.IsActive & " ; IsApproved:  " & prevobjICOffce.IsApprove & " ; IsBranch:  " & prevobjICOffce.IsBranch & " ; Is Printing Location:  " & prevobjICOffce.IsPrintingLocation & "] "
                        ActionStr = CurrentAt & PrevAt

                    End If

                    objICBankBranch.CreatedBy = Me.UserId
                    objICBankBranch.CreateDate = Date.Now
                    ICOfficeController.AddOffice(objICBankBranch, True, Me.UserId.ToString(), Me.UserInfo.Username.ToString(), ActionStr)
                    UIUtilities.ShowDialog(Me, "Save Bank Branch", "Bank Branch updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                End If

                Clear()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Function CheckDuplicateOnUpdate(ByVal ICOffice As ICOffice) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim objICOfficeColl As ICOfficeCollection
            'Dim collCompany As New ICCompanyCollection
            objICOfficeColl = New ICOfficeCollection
            objICOfficeColl.es.Connection.CommandTimeout = 3600
            objICOfficeColl.Query.Where(objICOfficeColl.Query.OfficeName = ICOffice.OfficeName.ToString And objICOfficeColl.Query.OffceType = "Branch Office" And objICOfficeColl.Query.OfficeID <> ICOffice.OfficeID)
            objICOfficeColl.Query.Load()
            If objICOfficeColl.Query.Load Then
                rslt = True
            End If
            If rslt = False Then
                objICOfficeColl = New ICOfficeCollection
                objICOfficeColl.es.Connection.CommandTimeout = 3600
                objICOfficeColl.Query.Where(objICOfficeColl.Query.OfficeCode = ICOffice.OfficeCode.ToString And objICOfficeColl.Query.OffceType = "Branch Office" And objICOfficeColl.Query.OfficeID <> ICOffice.OfficeID)
                objICOfficeColl.Query.Load()
                If objICOfficeColl.Query.Load Then
                    rslt = True
                End If
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function CheckDuplicateCodeName(ByVal ICOFfice As ICOffice) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim Office As New ICOffice
            Dim colloffice As New ICOfficeCollection

            Office.es.Connection.CommandTimeout = 3600
            colloffice.es.Connection.CommandTimeout = 3600

            colloffice.Query.Where(colloffice.Query.OfficeCode.ToLower.Trim = ICOFfice.OfficeCode.ToLower.Trim)
            If colloffice.Query.Load() Then
                rslt = True
            End If

            colloffice = New ICOfficeCollection
            colloffice.Query.Where(colloffice.Query.OfficeName.ToLower.Trim = ICOFfice.OfficeName.ToLower.Trim)
            If colloffice.Query.Load() Then
                rslt = True
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    Protected Sub btnApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproved.Click
        If Page.IsValid Then
            Dim objICBankBranch As New ICOffice

            objICBankBranch.es.Connection.CommandTimeout = 3600

            Try
                If objICBankBranch.LoadByPrimaryKey(OfficeID.ToString()) Then
                    If Me.UserInfo.IsSuperUser = False Then
                        If objICBankBranch.CreatedBy = Me.UserId Then
                            UIUtilities.ShowDialog(Me, "Save Bank Branch", "Bank Branch must be approve by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If

                    End If
                    If chkApproved.Checked = True Then
                        ICOfficeController.ApproveOffice(OfficeID.ToString(), Me.UserId.ToString(), chkApproved.Checked, Me.UserInfo.Username.ToString())
                        UIUtilities.ShowDialog(Me, "Save Bank Branch", "Bank Branch approve.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    Else
                        UIUtilities.ShowDialog(Me, "Save Bank Branch", "Please select approve check box.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub
#End Region

#Region "Load Drop Down"

    Private Sub LoadddlCountry()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCountry.Items.Clear()
            ddlCountry.Items.Add(lit)
            ddlCountry.AppendDataBoundItems = True
            ddlCountry.DataSource = ICCountryController.GetCountryActiveAndApproved()
            ddlCountry.DataTextField = "CountryName"
            ddlCountry.DataValueField = "CountryCode"
            ddlCountry.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlProvince()
        Try
            ddlProvince.Items.Clear()
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlProvince.Items.Add(lit)
            ddlProvince.AppendDataBoundItems = True
            ddlProvince.DataSource = ICProvinceController.GetProvinceActiveAndApprove(ddlCountry.SelectedValue.ToString())
            ddlProvince.DataTextField = "ProvinceName"
            ddlProvince.DataValueField = "ProvinceCode"
            ddlProvince.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlCity()
        Try
            ddlCity.Items.Clear()
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCity.Items.Add(lit)
            ddlCity.AppendDataBoundItems = True
            ddlCity.DataSource = ICCityController.GetCityActiveAndApprovedByProvinceCode(ddlProvince.SelectedValue.ToString())
            ddlCity.DataTextField = "CityName"
            ddlCity.DataValueField = "CityCode"
            ddlCity.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlBank(ByVal BankType As String)

        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBank.Items.Clear()
            ddlBank.Items.Add(lit)
            ddlBank.AppendDataBoundItems = True
            If BankType = "Non-Principal Bank" Then
                ddlBank.DataSource = ICBankController.GetAllApprovedActiveNonPrincipalBanks()
                ddlBank.DataValueField = "BankCode"
                ddlBank.DataTextField = "BankName"
                ddlBank.DataBind()
            ElseIf BankType = "Principal Bank" Then
                ddlBank.DataSource = ICBankController.GetAllApprovedActivePrincipalBanks()
                ddlBank.DataValueField = "BankCode"
                ddlBank.DataTextField = "BankName"
                ddlBank.DataBind()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub LoadddlNormalClearingAccount()
        Try
            ddlNormalClearingAcc.Items.Clear()
            Dim lit As New ListItem
            Dim dt As New DataTable
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlNormalClearingAcc.Items.Add(lit)
            ddlNormalClearingAcc.AppendDataBoundItems = True
            ddlNormalClearingAcc.DataSource = ICOfficeController.GetNormalClearingAccountsByAccountTypeNew
            ddlNormalClearingAcc.DataTextField = "AccountTitle"
            ddlNormalClearingAcc.DataValueField = "AccountNumber"
            ddlNormalClearingAcc.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlIntercityClearingAccount()
        Try
            ddlIntercityClearingAcc.Items.Clear()
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlIntercityClearingAcc.Items.Add(lit)
            ddlIntercityClearingAcc.AppendDataBoundItems = True
            ddlIntercityClearingAcc.DataSource = ICOfficeController.GetIntercityClearingAccountsByAccountTypeNew
            ddlIntercityClearingAcc.DataTextField = "AccountTitle"
            ddlIntercityClearingAcc.DataValueField = "AccountNumber"
            ddlIntercityClearingAcc.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlSameDayClearingAccount()
        Try
            ddlSameDayClearingAcc.Items.Clear()
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlSameDayClearingAcc.Items.Add(lit)
            ddlSameDayClearingAcc.AppendDataBoundItems = True
            ddlSameDayClearingAcc.DataSource = ICOfficeController.GetSameDayClearingAccountsByAccountTypeNew
            ddlSameDayClearingAcc.DataTextField = "AccountTitle"
            ddlSameDayClearingAcc.DataValueField = "AccountNumber"
            ddlSameDayClearingAcc.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlBranchCashTillAccount()
        Try
            ddlBranchCashTillAccount.Items.Clear()
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBranchCashTillAccount.Items.Add(lit)
            ddlBranchCashTillAccount.AppendDataBoundItems = True
            ddlBranchCashTillAccount.DataSource = ICBankAccountController.GetAllBankAccountsByAccountTypes("Branch Cash Till Account")
            ddlBranchCashTillAccount.DataTextField = "AccountTitle"
            ddlBranchCashTillAccount.DataValueField = "AccountNumber"
            ddlBranchCashTillAccount.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

#Region "Drop Down Selected Index Chnaged"

    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        If ddlCountry.SelectedValue.ToString = "0" Then
            LoadddlProvince()
            LoadddlCity()
        Else
            LoadddlProvince()
            LoadddlCity()
        End If
        
    End Sub

    Protected Sub ddlProvince_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProvince.SelectedIndexChanged
        LoadddlCity()

    End Sub

    Protected Sub ddlBankType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBankType.SelectedIndexChanged
        If ddlBankType.SelectedValue.ToString() = "0" Then
            LoadddlBank(0)
            lblPrintLocation.Visible = False
            chkPrntLocation.Visible = False
            lblIsAllowedForTagging.Visible = False
            chkIsAllowedForTagging.Visible = False
        ElseIf ddlBankType.SelectedValue.ToString() = "Principal Bank" Then
            LoadddlBank("Principal Bank")
            lblNormalClearingAcc.Visible = True
            lblIntercityClearingAcc.Visible = True
            lblSameDayClearingAcc.Visible = True
            lblBranchCashTillAccount.Visible = True
            ddlNormalClearingAcc.Visible = True
            ddlIntercityClearingAcc.Visible = True
            ddlSameDayClearingAcc.Visible = True
            ddlBranchCashTillAccount.Visible = True
            lblPrintLocation.Visible = True
            chkPrntLocation.Visible = True
            lblIsAllowedForTagging.Visible = True
            chkIsAllowedForTagging.Visible = True
            LoadddlSameDayClearingAccount()
            LoadddlNormalClearingAccount()
            LoadddlIntercityClearingAccount()
            LoadddlBranchCashTillAccount()

        ElseIf ddlBankType.SelectedValue.ToString() = "Non-Principal Bank" Then
            LoadddlBank("Non-Principal Bank")
            lblNormalClearingAcc.Visible = False
            lblIntercityClearingAcc.Visible = False
            lblSameDayClearingAcc.Visible = False
            lblBranchCashTillAccount.Visible = False
            ddlBranchCashTillAccount.Visible = False
            ddlNormalClearingAcc.Visible = False
            ddlIntercityClearingAcc.Visible = False
            ddlSameDayClearingAcc.Visible = False

            lblPrintLocation.Visible = False
            chkPrntLocation.Visible = False
            lblIsAllowedForTagging.Visible = False
            chkIsAllowedForTagging.Visible = False

        End If
    End Sub

#End Region

End Class
