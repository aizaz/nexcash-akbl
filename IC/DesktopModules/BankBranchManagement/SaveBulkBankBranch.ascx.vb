﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Partial Class DesktopModules_BankBranchManagement_SaveBulkBankBranch
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private OfficeID As String
    Dim dtDa As DataTable
    Private htRights As Hashtable



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            OfficeID = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing
                btnAddBranchesinBulk.Visible = CBool(htRights("Add"))

                If OfficeID.ToString() = "1" Then
                    lblBulkBranchList.Visible = True
                    gvBankBranch.Visible = False
                    gvUnVarified.Visible = False
                    FileUploadBranchesinBulk.Visible = True
                    btnCancel.Visible = True
                    btnProceed.Visible = True
                    btnAddBranchesinBulk.Visible = False

                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Bank Branch Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        gvBankBranch.Visible = False
        gvUnVarified.Visible = False
        btnAddBranchesinBulk.Visible = False
        hfFilePath.Value = Nothing
    End Sub



    Protected Sub btnProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        If Page.IsValid Then
            Try
                Dim str As String = System.IO.Path.GetExtension(FileUploadBranchesinBulk.PostedFile.FileName)
                btnProceed.Visible = False
                btnCancel.Visible = True
                If FileUploadBranchesinBulk.HasFile Then

                    If str = ".xls" Or str = ".xlsx" Then

                        Dim aFileInfo As IO.FileInfo
                        aFileInfo = New IO.FileInfo(Server.MapPath("~/UploadedFiles/") & FileUploadBranchesinBulk.FileName.ToString())
                        'Delete file from HDD. 
                        If aFileInfo.Exists Then
                            aFileInfo.Delete()
                        End If

                        FileUploadBranchesinBulk.SaveAs(Server.MapPath("~/UploadedFiles/") & FileUploadBranchesinBulk.FileName.ToString())
                        hfFilePath.Value = Server.MapPath("~/UploadedFiles/") & FileUploadBranchesinBulk.FileName.ToString()
                        ReadFileShowDetails()
                    Else
                        UIUtilities.ShowDialog(Me, "Save Bulk Bank Branch", "Please select the .xls or .xlsx file.", ICBO.IC.Dialogmessagetype.Failure)
                    End If
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If


    End Sub

    Private Sub ReadFileShowDetails()
        Try
            Dim FilePath As String = hfFilePath.Value.ToString()
            Dim connectionString As String = ""

            connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FilePath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""

            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = "Sheet1$"
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()

            FilterAndShowDetails(dtExcelRecords)

        Catch ex As Exception
            If ex.Message.Contains("'Sheet1$' is not a valid name") Then
                UIUtilities.ShowDialog(Me, "Bulk Bank Branch", "Invalid sheet name", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            Else
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End If
        End Try
    End Sub

    Private Sub FilterAndShowDetails(ByVal dtExcel As DataTable)
        Try
            If dtExcel.Columns.Count <> 28 Then
                UIUtilities.ShowDialog(Me, "Save Bulk Bank Branch", "No of columns do not match.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                Clear()
                Exit Sub
            End If

            ViewState("dtFiltered") = Nothing
            ViewState("dtUnVarified") = Nothing
            Dim dtFiltered As New DataTable
            Dim dtUnVarified As New DataTable
            Dim drExcel, drFiltered, drUnVarified As DataRow
            Dim IsValid As Boolean = True
            Dim BankName, CityName, Phone1, Phone2, Address, Email, Description As String
            Dim ClearingNormalAccountNumber, ClearingNormalCurrency, ClearingNormalBranchCode, SameDayCurrency, SameDayAccountNumber, SameDayBranchCode, InterCityBranchCode, InterCityCurrency, InterCityAccountNumber As String
            Dim ClearingAccountType, ClearingAccountClientNo, ClearingAccountSeqNo, ClearingAccountProfitCentre As String
            Dim SameDayAccountType, SameDayAccountClientNo, SameDaySeqNo, SameSayProfitCentre As String
            Dim InterCityAccountType, InterCityClientNo, InterCityProfitCenter, InterCitySeqNo As String
            Dim BranchCashTillAccountType, BranchCashTillAccountNumber, BranchCashTillAccountBranchCode, BranchCashTillAccountCurrency As String

            Dim IsPrintLocation As Boolean = False
            Dim IsAllowedForTagging As Boolean = False
            Dim IsBranch As Boolean = False
            Dim IsPrincipal As Boolean
            Dim ValidateNormalClearing As Boolean
            Dim ValidateIntercityClearing As Boolean
            Dim ValidateSameDayClearing As Boolean
            Dim srtBranchCode As String()
            Dim srtBranchName As String()
            Dim BranchCode As String = ""
            Dim BranchName As String = ""
            Dim RowNo As Integer = 0
            Dim ErrorMessage As String = ""

            dtFiltered.Columns.Add(New DataColumn("Code", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("Name", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BankCode", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BankName", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("CityID", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("CityName", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("Phone1", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("Phone2", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("Address", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("Email", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("Description", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("IsBranch", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("IsPrintLocation", GetType(System.Boolean)))

            dtFiltered.Columns.Add(New DataColumn("ClearingNormalAccountNumber", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("ClearingNormalBranchCode", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("ClearingNormalCurrency", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("InterCityNormalAccountNumber", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("InterCityNormalBranchCode", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("InterCityNormalCurrency", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("SameDayNormalAccountNumber", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("SameDayNormalBranchCode", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("SameDayNormalCurrency", GetType(System.String)))
            dtUnVarified.Columns.Add(New DataColumn("RowNo", GetType(System.String)))
            dtUnVarified.Columns.Add(New DataColumn("ErrorMessage", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("ClearingAccountType", GetType(System.String)))
            'dtFiltered.Columns.Add(New DataColumn("ClearingAccountClientNo", GetType(System.String)))
            'dtFiltered.Columns.Add(New DataColumn("ClearingAccountSeqNo", GetType(System.String)))
            'dtFiltered.Columns.Add(New DataColumn("ClearingAccountProfitCentre", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("InterCityAccountType", GetType(System.String)))
            'dtFiltered.Columns.Add(New DataColumn("InterCityClientNo", GetType(System.String)))
            'dtFiltered.Columns.Add(New DataColumn("InterCityProfitCenter", GetType(System.String)))
            'dtFiltered.Columns.Add(New DataColumn("InterCitySeqNo", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("SameDayAccountType", GetType(System.String)))
            'dtFiltered.Columns.Add(New DataColumn("SameDayAccountClientNo", GetType(System.String)))
            'dtFiltered.Columns.Add(New DataColumn("SameDaySeqNo", GetType(System.String)))
            'dtFiltered.Columns.Add(New DataColumn("SameSayProfitCentre", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BranchCashTillAccountType", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BranchCashTillAccountNumber", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BranchCashTillAccountBranchCode", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BranchCashTillAccountCurrency", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("IsAllowedForTagging", GetType(System.Boolean)))

            If dtExcel.Rows.Count > 0 Then

                For Each drExcel In dtExcel.Rows

                    RowNo = RowNo + 1
                    ErrorMessage = ""
                    IsValid = True
                    IsPrintLocation = False
                    IsBranch = False
                    BankName = ""
                    CityName = ""
                    Phone1 = ""
                    Phone2 = ""
                    Address = ""
                    Email = ""
                    Description = ""

                    For Each dtCol As DataColumn In dtExcel.Columns

                        Dim FieldContent As String = drExcel(dtCol.ColumnName)
                        Dim reg As Regex = New Regex("<[^>]+>")
                        Dim check As Boolean = reg.IsMatch(FieldContent)

                        If check Then
                            ErrorMessage = dtCol.ColumnName & " is containing potentially dangerous value "
                            IsValid = False
                            drExcel(dtCol.ColumnName) = Server.HtmlEncode(drExcel(dtCol.ColumnName))
                        End If
                    Next

                    If IsValid = False Then
                        gvUnVarified.Visible = True
                        drUnVarified = dtUnVarified.NewRow()
                        drUnVarified(0) = RowNo.ToString()
                        drUnVarified(1) = ErrorMessage.ToString()
                        dtUnVarified.Rows.Add(drUnVarified)
                        Continue For
                    End If

                    If drExcel(0).ToString() = "" Then
                        IsValid = False
                        ErrorMessage += "Branch Code is empty (" + (drExcel(0).ToString()) + ") ; "
                    ElseIf drExcel(0).ToString().Length() > 10 Then
                        IsValid = False
                        ErrorMessage += "Invalid Branch Code Length (" + (drExcel(0).ToString()) + ") ; "

                    ElseIf Regex.IsMatch(drExcel(0).ToString(), "[ -_]{2,10}$") = False Then
                        IsValid = False
                        ErrorMessage += "Invalid Branch Code (" + (drExcel(0).ToString()) + ") ; "
                    ElseIf CheckDuplicateCode(DirectCast(drExcel(0).ToString(), String)) = True Then
                        IsValid = False
                        ErrorMessage += "Duplicate Branch Code (" + (drExcel(0).ToString()) + ") ; "

                    Else


                        If BranchCode.ToString() <> "" Then
                            srtBranchCode = BranchCode.ToString.Split(",")
                            For cnt = 0 To srtBranchCode.Length - 1
                                'If srtBranchCode(cnt) = drExcel("BranchCode").ToString().Trim() Then
                                If srtBranchCode(cnt) = drExcel(0).ToString().Trim() Then
                                    IsValid = False
                                    ErrorMessage += "Duplicate Branch Code (" + (drExcel(0).ToString()) + ") ; "
                                    BranchCode = BranchCode & "," & drExcel(0).ToString().Trim()
                                    '  BranchCode = BranchCode & "," & drExcel("BranchCode").ToString().Trim()
                                    Exit For
                                End If
                            Next
                        End If
                        '  BranchCode = BranchCode & "," & drExcel("BranchCode").ToString().Trim()

                        BranchCode = BranchCode & "," & drExcel(0).ToString().Trim()
                    End If


                    If drExcel(1).ToString() = "" Then
                        IsValid = False
                        ErrorMessage += "Branch Name is empty (" + (drExcel(1).ToString()) + ") ; "
                    ElseIf drExcel(1).ToString().Length() > 150 Then
                        IsValid = False
                        ErrorMessage += "Invalid Branch Name Length (" + (drExcel(1).ToString()) + ") ; "
                    ElseIf Regex.IsMatch(drExcel(1).ToString(), "^[a-zA-Z0-9.-_/ ]{1,100}$") = False Then
                        IsValid = False
                        ErrorMessage += "Invalid Branch Name (" + (drExcel(1).ToString()) + ") ; "
                    ElseIf CheckDuplicateName(DirectCast(drExcel(1).ToString(), String)) = True Then
                        IsValid = False
                        ErrorMessage += "Duplicate Branch Name (" + (drExcel(1).ToString()) + ") ; "

                    Else


                        If BranchName.ToString() <> "" Then
                            srtBranchName = BranchName.ToString.Split(",")
                            For cnt = 0 To srtBranchName.Length - 1
                                'If srtBranchName(cnt) = drExcel("BranchName").ToString().Trim() Then
                                If srtBranchName(cnt) = drExcel(1).ToString().Trim() Then
                                    IsValid = False
                                    ErrorMessage += "Duplicate Branch Name (" + (drExcel(1).ToString()) + ") ; "
                                    ' BranchName = BranchName & "," & drExcel("BranchName").ToString().Trim()
                                    BranchName = BranchName & "," & drExcel(1).ToString().Trim()
                                    Exit For
                                End If
                            Next
                        End If
                        'BranchName = BranchName & "," & drExcel("BranchName").ToString().Trim()
                        BranchName = BranchName & "," & drExcel(1).ToString().Trim()

                    End If



                    If drExcel(2).ToString() = "" Then
                        IsValid = False
                        ErrorMessage += "Bank Code is empty (" + (drExcel(2).ToString()) + ") ; "
                    ElseIf Regex.IsMatch(drExcel(2).ToString().Trim, "^[0-9]{1,9}$") = False Then
                        IsValid = False
                        ErrorMessage += "Bank Code must be in numbers only (" + (drExcel(2).ToString()) + ") ; "
                    ElseIf drExcel(2).ToString().Length() > 9 Then
                        IsValid = False
                        ErrorMessage += "Invalid Bank Code Length (" + (drExcel(2).ToString()) + ") ; "

                    Else
                        BankName = ICBankController.GetBankNameByBankCode(drExcel(2).ToString()).ToString()
                        If BankName.ToString() = "" Then
                            IsValid = False
                            ErrorMessage += "Invalid Bank Code (" + (drExcel(2).ToString()) + ") ; "
                        End If

                        IsPrincipal = ICBankController.IsPrincipalBank(drExcel(2).ToString(), BankName.ToString()).ToString()
                    End If
                    If IsPrincipal = True Then

                        'If drExcel(20).ToString = "" And drExcel(11).ToString() = "" And drExcel(12).ToString = "" And drExcel(13).ToString() = "" And drExcel(21).ToString = "" And drExcel(22).ToString = "" And drExcel(23).ToString = "" Then
                        If drExcel(20).ToString = "" And drExcel(11).ToString() = "" And drExcel(12).ToString = "" And drExcel(13).ToString() = "" Then
                            ClearingNormalAccountNumber = False
                            ClearingNormalBranchCode = False
                            ClearingNormalCurrency = False
                            ClearingAccountType = False
                            ClearingAccountClientNo = False
                            ClearingAccountSeqNo = False
                            ClearingAccountProfitCentre = False
                        ElseIf drExcel(20).ToString = "" Then
                            'If drExcel(11).ToString() <> "" Or drExcel(12).ToString <> "" Or drExcel(13).ToString() <> "" Or drExcel(21).ToString <> "" Or drExcel(22).ToString <> "" Or drExcel(23).ToString <> "" Then
                            If drExcel(11).ToString() <> "" Or drExcel(12).ToString <> "" Or drExcel(13).ToString() <> "" Then
                                IsValid = False
                                ErrorMessage += "Invalid Clearing account type ( Clearing Normal Account Type is empty ) ; "

                            End If
                        ElseIf drExcel(20).ToString <> "" Then
                            If drExcel(20).ToString.Length > 2 Then
                                IsValid = False
                                ErrorMessage += "Invalid Clearing account type length ( " + (drExcel(20).ToString) + " ) ; "

                            ElseIf drExcel(20).ToString.Trim.ToLower <> "rb" And drExcel(20).ToString.Trim.ToLower <> "gl" Then
                                IsValid = False
                                ErrorMessage += "Invalid Clearing account type ( " + (drExcel(20).ToString) + " ) ; "
                            ElseIf drExcel(20).ToString.ToLower = "rb" Then
                                If drExcel(11).ToString = "" Or drExcel(12).ToString = "" Or drExcel(13).ToString = "" Then
                                    IsValid = False
                                    ErrorMessage += "RB Clearing account must have values in"
                                    If drExcel(11).ToString = "" Then
                                        ErrorMessage += " ( Clearing Normal Account Number ) ; "
                                    End If
                                    If drExcel(12).ToString = "" Then
                                        ErrorMessage += " ( Clearing Normal Currency ) ; "
                                    End If
                                    If drExcel(13).ToString = "" Then
                                        ErrorMessage += " ( Clearing Normal Branch Code ) ; "
                                    End If
                                ElseIf drExcel(11).ToString() <> "" And drExcel(12).ToString <> "" And drExcel(13).ToString() <> "" Then
                                    ValidateNormalClearing = ICBankAccountController.ValidateRBAccountNoByAccountType(drExcel(11).ToString().Trim, drExcel(13).ToString.Trim, drExcel(12).ToString().Trim, drExcel(20).ToString.Trim.ToUpper, "Normal Clearing Account").ToString()
                                    If ValidateNormalClearing = False Then
                                        IsValid = False
                                        ErrorMessage += "Invalid RB Clearing Normal (Account No.: " + (drExcel(11).ToString()) + " ; Branch Code: " + (drExcel(13).ToString()) + " ; Currency: " + (drExcel(12).ToString()) + ") ; "
                                    End If
                                ElseIf Regex.IsMatch(drExcel(11).ToString().Trim, "^[a-zA-Z0-9.-_/]{2,100}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Clearing Normal Account Number (" + (drExcel(11).ToString()) + ") ; "
                                ElseIf Regex.IsMatch(drExcel(13).ToString().Trim, "^[a-zA-Z0-9.-_/]{2,20}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Clearing Normal Branch Code (" + (drExcel(13).ToString()) + ") ; "
                                ElseIf Regex.IsMatch(drExcel(12).ToString().Trim, "^[a-zA-Z0-9.-_/]{2,50}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Clearing Normal Currency (" + (drExcel(12).ToString()) + ") ; "
                                End If
                            ElseIf drExcel(20).ToString.Trim.ToLower = "gl" Then
                                'If drExcel(21).ToString = "" Or drExcel(22).ToString = "" Or drExcel(23).ToString = "" Or drExcel(11).ToString = "" Or drExcel(12).ToString = "" Or drExcel(13).ToString = "" Then
                                If drExcel(11).ToString = "" Or drExcel(12).ToString = "" Or drExcel(13).ToString = "" Then
                                    IsValid = False
                                    ErrorMessage += "GL Clearing account must have values in"
                                    If drExcel(11).ToString = "" Then
                                        ErrorMessage += " ( Clearing Normal Account Number ) ; "
                                    End If
                                    If drExcel(12).ToString = "" Then
                                        ErrorMessage += " ( Clearing Normal Currency ) ; "
                                    End If
                                    If drExcel(13).ToString = "" Then
                                        ErrorMessage += " ( Clearing Normal Branch Code ) ; "
                                    End If
                                    'If drExcel(21).ToString = "" Then
                                    '    ErrorMessage += " ( Clearing Normal Client No ) ; "
                                    'End If
                                    'If drExcel(22).ToString = "" Then
                                    '    ErrorMessage += " ( Clearing Normal Seq No ) ; "
                                    'End If
                                    'If drExcel(23).ToString = "" Then
                                    '    ErrorMessage += " ( Clearing Normal Profit Center ) ; "
                                    'End If

                                    'ElseIf drExcel(11).ToString() <> "" And drExcel(12).ToString <> "" And drExcel(13).ToString() <> "" And drExcel(21).ToString <> "" And drExcel(22).ToString <> "" And drExcel(23).ToString <> "" Then
                                    '    ValidateNormalClearing = ICBankAccountController.ValidateGLAccountNoByAccountType(drExcel(11).ToString().Trim, drExcel(13).ToString.Trim, drExcel(12).ToString().Trim, drExcel(21).ToString.Trim, drExcel(22).ToString.Trim, drExcel(23).ToString.Trim, drExcel(20).ToString.Trim.ToUpper, "Normal Clearing Account").ToString()
                                ElseIf drExcel(11).ToString() <> "" And drExcel(12).ToString <> "" And drExcel(13).ToString() <> "" Then
                                    ValidateNormalClearing = ICBankAccountController.ValidateGLAccountNoByAccountType(drExcel(11).ToString().Trim, drExcel(13).ToString.Trim, drExcel(12).ToString().Trim, drExcel(20).ToString.Trim.ToUpper, "Normal Clearing Account").ToString()
                                    If ValidateNormalClearing = False Then
                                        IsValid = False
                                        'ErrorMessage += "Invalid GL Clearing Normal (Account No.: " + (drExcel(11).ToString()) + " ; Branch Code: " + (drExcel(13).ToString()) + " ; Currency: " + (drExcel(12).ToString()) + " ; Client No: " + (drExcel(21).ToString) + " ; Sequence No: " + (drExcel(22).ToString) + " ; Profit Centre: " + (drExcel(23).ToString) + ") ; "
                                        ErrorMessage += "Invalid GL Clearing Normal (Account No.: " + (drExcel(11).ToString()) + " ; Branch Code: " + (drExcel(13).ToString()) + " ; Currency: " + (drExcel(12).ToString()) + ") ; "
                                    End If
                                ElseIf Regex.IsMatch(drExcel(11).ToString(), "^[a-zA-Z0-9.-_/]{2,100}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Clearing Normal Account Number (" + (drExcel(11).ToString()) + ") ; "
                                ElseIf Regex.IsMatch(drExcel(13).ToString().Trim, "^[a-zA-Z0-9.-_/]{2,20}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Clearing Normal Branch Code (" + (drExcel(13).ToString()) + ") ; "
                                ElseIf Regex.IsMatch(drExcel(12).ToString().Trim, "^[a-zA-Z0-9.-_/]{2,50}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Clearing Normal Currency (" + (drExcel(12).ToString()) + ") ; "
                                    'ElseIf drExcel(22).ToString().Trim.Length > 9 Then
                                    '    IsValid = False
                                    '    ErrorMessage += "Invalid Clearing Normal Sequence no length (" + (drExcel(22).ToString()) + ") ; "
                                    'ElseIf Regex.IsMatch(drExcel(22).ToString().Trim, "^[0-9]{1,9}$") = False Then
                                    '    IsValid = False
                                    '    ErrorMessage += "Invalid Clearing Normal Sequence no (" + (drExcel(22).ToString()) + ") ; "
                                    'ElseIf drExcel(21).ToString().Trim.Length > 100 Then
                                    '    IsValid = False
                                    '    ErrorMessage += "Invalid Clearing Normal  Client no length (" + (drExcel(21).ToString()) + ") ; "
                                    'ElseIf drExcel(23).ToString().Trim.Length > 100 Then
                                    '    IsValid = False
                                    '    ErrorMessage += "Invalid Clearing Normal profit centre (" + (drExcel(23).ToString()) + ") ; "
                                End If
                            End If
                        End If


                        'If drExcel(24).ToString() = "" And drExcel(14).ToString() = "" And drExcel(15).ToString() = "" And drExcel(16).ToString() = "" And drExcel(25).ToString() = "" And drExcel(26).ToString() = "" And drExcel(27).ToString() = "" Then
                        If drExcel(21).ToString() = "" And drExcel(14).ToString() = "" And drExcel(15).ToString() = "" And drExcel(16).ToString() = "" Then
                            InterCityAccountNumber = False
                            InterCityBranchCode = False
                            InterCityCurrency = False
                            InterCityAccountType = False
                            'InterCityClientNo = False
                            'InterCityProfitCenter = False
                            'InterCitySeqNo = False
                        ElseIf drExcel(21).ToString = "" Then
                            'If drExcel(14).ToString() <> "" Or drExcel(15).ToString() <> "" Or drExcel(16).ToString() <> "" Or drExcel(25).ToString() <> "" Or drExcel(26).ToString() <> "" Or drExcel(27).ToString() <> "" Then
                            If drExcel(14).ToString() <> "" Or drExcel(15).ToString() <> "" Or drExcel(16).ToString() <> "" Then
                                IsValid = False
                                ErrorMessage += "Invalid Intercity Normal Account type ( Inter City Account Type is empty ) ; "
                            End If
                        ElseIf drExcel(21).ToString <> "" Then
                            If drExcel(21).ToString.Length > 2 Then
                                IsValid = False
                                ErrorMessage += "Invalid Intercity Normal account type length ( " + (drExcel(21).ToString) + " ) ; "

                            ElseIf drExcel(21).ToString.Trim.ToLower <> "rb" And drExcel(21).ToString.Trim.ToLower <> "gl" Then
                                IsValid = False
                                ErrorMessage += "Invalid Intercity Normal account type ( " + (drExcel(21).ToString) + " ) ; "
                            ElseIf drExcel(21).ToString.ToLower = "rb" Then
                                If drExcel(14).ToString() = "" Or drExcel(15).ToString = "" Or drExcel(16).ToString() = "" Then
                                    IsValid = False
                                    ErrorMessage += "RB Intercity Normal account must have values in "
                                    If drExcel(14).ToString = "" Then
                                        ErrorMessage += "( Inter City Account Number ) ; "
                                    End If
                                    If drExcel(15).ToString = "" Then
                                        ErrorMessage += " ( Inter City Branch Code ) ; "
                                    End If
                                    If drExcel(16).ToString = "" Then
                                        ErrorMessage += " ( Inter City Currency ) ; "
                                    End If
                                ElseIf drExcel(14).ToString() <> "" And drExcel(15).ToString <> "" And drExcel(16).ToString() <> "" Then
                                    ValidateIntercityClearing = ICBankAccountController.ValidateRBAccountNoByAccountType(drExcel(14).ToString().Trim, drExcel(15).ToString().Trim, drExcel(16).ToString.Trim, drExcel(21).ToString.ToUpper.Trim, "Intercity Clearing Account").ToString()
                                    If ValidateIntercityClearing = False Then
                                        IsValid = False
                                        ErrorMessage += "Invalid RB Intercity Normal (Account No.: " + (drExcel(14).ToString()) + " ; Branch Code: " + (drExcel(15).ToString()) + " ; Currency: " + (drExcel(16).ToString()) + ") ; "
                                    End If
                                ElseIf Regex.IsMatch(drExcel(14).ToString().Trim, "^[a-zA-Z0-9.-_/]{2,100}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Intercity Normal Account Number (" + (drExcel(14).ToString()) + ") ; "
                                ElseIf Regex.IsMatch(drExcel(15).ToString().Trim, "^[a-zA-Z0-9.-_/]{2,20}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Intercity Normal Account Branch Code (" + (drExcel(15).ToString()) + ") ; "
                                ElseIf Regex.IsMatch(drExcel(16).ToString().Trim, "^[a-zA-Z0-9.-_/]{2,50}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Intercity Normal Account Currency (" + (drExcel(16).ToString()) + ") ; "
                                End If
                            ElseIf drExcel(21).ToString.ToLower = "gl" Then
                                'If drExcel(14).ToString = "" Or drExcel(15).ToString = "" Or drExcel(16).ToString = "" Or drExcel(25).ToString = "" Or drExcel(26).ToString = "" Or drExcel(27).ToString = "" Then
                                If drExcel(14).ToString = "" Or drExcel(15).ToString = "" Or drExcel(16).ToString = "" Then
                                    IsValid = False
                                    ErrorMessage += "GL Intercity Normal Account must have values in"
                                    If drExcel(14).ToString = "" Then
                                        ErrorMessage += " ( Inter City Account Number ) ; "
                                    End If
                                    If drExcel(15).ToString = "" Then
                                        ErrorMessage += " ( Inter City Branch Code ) ; "
                                    End If
                                    If drExcel(16).ToString = "" Then
                                        ErrorMessage += " ( Inter City Currency ) ; "
                                    End If
                                    'If drExcel(25).ToString = "" Then
                                    '    ErrorMessage += " ( Inter City Account Client No ) ; "
                                    'End If
                                    'If drExcel(26).ToString = "" Then
                                    '    ErrorMessage += " ( Inter City Account Profit Centre ) ; "
                                    'End If
                                    'If drExcel(27).ToString = "" Then
                                    '    ErrorMessage += " ( Inter City Account Seq No ) ; "
                                    'End If
                                    'ElseIf drExcel(14).ToString() <> "" And drExcel(15).ToString <> "" And drExcel(16).ToString() <> "" And drExcel(25).ToString <> "" And drExcel(26).ToString <> "" And drExcel(27).ToString <> "" Then
                                    '    ValidateIntercityClearing = ICBankAccountController.ValidateGLAccountNoByAccountType(drExcel(14).ToString().Trim, drExcel(15).ToString().Trim, drExcel(16).ToString.Trim, drExcel(25).ToString.Trim, drExcel(27).ToString.Trim, drExcel(26).ToString.Trim, drExcel(24).ToString.Trim.ToUpper, "Intercity Clearing Account").ToString()
                                ElseIf drExcel(14).ToString() <> "" And drExcel(15).ToString <> "" And drExcel(16).ToString() <> "" Then
                                    ValidateIntercityClearing = ICBankAccountController.ValidateGLAccountNoByAccountType(drExcel(14).ToString().Trim, drExcel(15).ToString().Trim, drExcel(16).ToString.Trim, drExcel(21).ToString.Trim.ToUpper, "Intercity Clearing Account").ToString()
                                    If ValidateIntercityClearing = False Then
                                        IsValid = False
                                        'ErrorMessage += "Invalid GL Intercity Normal Account (Account No.: " + (drExcel(14).ToString()) + " ; Branch Code: " + (drExcel(15).ToString()) + " ; Currency: " + (drExcel(16).ToString()) + " ; Client No: " + (drExcel(25).ToString) + " ; Sequence No: " + (drExcel(27).ToString) + " ; Profit Centre: " + (drExcel(26).ToString) + ") ; "
                                        ErrorMessage += "Invalid GL Intercity Normal Account (Account No.: " + (drExcel(14).ToString()) + " ; Branch Code: " + (drExcel(15).ToString()) + " ; Currency: " + (drExcel(16).ToString()) + ") ; "
                                    End If
                                ElseIf Regex.IsMatch(drExcel(14).ToString().Trim, "^[a-zA-Z0-9.-_/]{2,100}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Intercity Normal Account Number (" + (drExcel(14).ToString()) + ") ; "
                                ElseIf Regex.IsMatch(drExcel(15).ToString().Trim, "^[a-zA-Z0-9.-_/]{2,20}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Intercity Normal Account Branch Code (" + (drExcel(15).ToString()) + ") ; "
                                ElseIf Regex.IsMatch(drExcel(16).ToString().Trim, "^[a-zA-Z0-9.-_/]{2,50}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Intercity Normal Account Currency (" + (drExcel(16).ToString()) + ") ; "
                                    'ElseIf drExcel(27).ToString().Trim.Length > 9 Then
                                    '    IsValid = False
                                    '    ErrorMessage += "Invalid Intercity Normal Account Sequence no length (" + (drExcel(27).ToString()) + ") ; "
                                    'ElseIf Regex.IsMatch(drExcel(27).ToString().Trim, "^[0-9]{1,9}$") = False Then
                                    '    IsValid = False
                                    '    ErrorMessage += "Invalid Intercity Normal Account Sequence no (" + (drExcel(27).ToString()) + ") ; "
                                    'ElseIf drExcel(25).ToString().Trim.Length > 100 Then
                                    '    IsValid = False
                                    '    ErrorMessage += "Invalid Intercity Normal Account Client No. (" + (drExcel(25).ToString()) + ") ; "
                                    'ElseIf drExcel(26).ToString().Trim.Length > 100 Then
                                    '    IsValid = False
                                    '    ErrorMessage += "Invalid Intercity Normal Account profit centre (" + (drExcel(26).ToString()) + ") ; "
                                End If

                            End If
                        End If


                        'If drExcel(28).ToString() = "" And drExcel(17).ToString() = "" And drExcel(18).ToString() = "" And drExcel(19).ToString() = "" And drExcel(29).ToString() = "" And drExcel(30).ToString() = "" And drExcel(31).ToString() = "" Then
                        If drExcel(22).ToString() = "" And drExcel(17).ToString() = "" And drExcel(18).ToString() = "" And drExcel(19).ToString() = "" Then
                            SameDayAccountNumber = False
                            SameDayBranchCode = False
                            SameDayCurrency = False
                            SameDayAccountType = False
                            SameDaySeqNo = False
                            SameDayAccountClientNo = False
                            SameSayProfitCentre = False
                        ElseIf drExcel(22).ToString = "" Then
                            'If drExcel(17).ToString() <> "" Or drExcel(18).ToString() <> "" Or drExcel(19).ToString() <> "" Or drExcel(29).ToString() <> "" Or drExcel(30).ToString() <> "" Or drExcel(31).ToString() <> "" Then
                            If drExcel(17).ToString() <> "" Or drExcel(18).ToString() <> "" Or drExcel(19).ToString() <> "" Then
                                IsValid = False
                                ErrorMessage += "Invalid SameDay Normal Account type ( Same Day Account Type is empty ) ; "
                            End If
                        ElseIf drExcel(22).ToString <> "" Then
                            If drExcel(22).ToString.Length > 2 Then
                                IsValid = False
                                ErrorMessage += "Invalid SameDay Normal account type length ( " + (drExcel(22).ToString) + " ) ; "

                            ElseIf drExcel(22).ToString.Trim.ToLower <> "rb" And drExcel(22).ToString.Trim.ToLower <> "gl" Then
                                IsValid = False
                                ErrorMessage += "Invalid SameDay Normal account type ( " + (drExcel(22).ToString) + " ) ; "
                            ElseIf drExcel(22).ToString.Trim.ToLower = "rb" Then
                                If drExcel(17).ToString() = "" Or drExcel(18).ToString = "" Or drExcel(19).ToString() = "" Then
                                    IsValid = False
                                    ErrorMessage += "RB SameDay Normal account must have values in"
                                    If drExcel(17).ToString = "" Then
                                        ErrorMessage += " ( Same Day AccountNumber  ) ;"
                                    End If
                                    If drExcel(18).ToString = "" Then
                                        ErrorMessage += " ( Same Day Branch Code ) ; "
                                    End If
                                    If drExcel(19).ToString = "" Then
                                        ErrorMessage += " ( Same Day Currency ) ; "
                                    End If
                                ElseIf drExcel(17).ToString() <> "" And drExcel(18).ToString <> "" And drExcel(19).ToString() <> "" Then
                                    ValidateSameDayClearing = ICBankAccountController.ValidateRBAccountNoByAccountType(drExcel(17).ToString().Trim, drExcel(18).ToString().Trim, drExcel(19).ToString.Trim, drExcel(22).ToString.ToUpper.Trim, "Same Day Clearing Account").ToString()
                                    If ValidateSameDayClearing = False Then
                                        IsValid = False
                                        ErrorMessage += "Invalid RB SameDay Normal (Account No.: " + (drExcel(17).ToString()) + " ; Branch Code: " + (drExcel(18).ToString()) + " ; Currency: " + (drExcel(19).ToString()) + ") ; "
                                    End If
                                ElseIf Regex.IsMatch(drExcel(17).ToString(), "^[a-zA-Z0-9.-_/]{2,100}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid SameDay Normal Account Number (" + (drExcel(17).ToString()) + ") ; "
                                ElseIf Regex.IsMatch(drExcel(18).ToString(), "^[a-zA-Z0-9.-_/]{2,20}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid SameDay Normal Account Branch Code (" + (drExcel(18).ToString()) + ") ; "
                                ElseIf Regex.IsMatch(drExcel(19).ToString(), "^[a-zA-Z0-9.-_/]{2,50}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid SameDay Normal Account Currency (" + (drExcel(19).ToString()) + ") ; "
                                End If
                            ElseIf drExcel(22).ToString.ToLower = "gl" Then
                                'If drExcel(17).ToString = "" Or drExcel(18).ToString = "" Or drExcel(19).ToString = "" Or drExcel(29).ToString = "" Or drExcel(30).ToString = "" Or drExcel(31).ToString = "" Then
                                If drExcel(17).ToString = "" Or drExcel(18).ToString = "" Or drExcel(19).ToString = "" Then
                                    IsValid = False
                                    ErrorMessage += "GL SameDay Normal Account must have values in "
                                    If drExcel(17).ToString = "" Then
                                        ErrorMessage += " ( Same Day AccountNumber ) ; "
                                    End If
                                    If drExcel(18).ToString = "" Then
                                        ErrorMessage += " ( Same Day Branch Code ) ; "
                                    End If
                                    If drExcel(19).ToString = "" Then
                                        ErrorMessage += " ( Same Day Currency ) ; "
                                    End If
                                    'If drExcel(29).ToString = "" Then
                                    '    ErrorMessage += " ( Same Day Account Client No ) ; "
                                    'End If
                                    'If drExcel(30).ToString = "" Then
                                    '    ErrorMessage += " ( Same Day Account Seq No ) ; "
                                    'End If
                                    'If drExcel(31).ToString = "" Then
                                    '    ErrorMessage += " ( Same Day Account ProfitCentre ) ; "
                                    'End If
                                    'ElseIf drExcel(17).ToString() <> "" And drExcel(18).ToString <> "" And drExcel(19).ToString() <> "" And drExcel(29).ToString <> "" And drExcel(30).ToString <> "" And drExcel(31).ToString <> "" Then
                                    '    ValidateSameDayClearing = ICBankAccountController.ValidateGLAccountNoByAccountType(drExcel(17).ToString().Trim, drExcel(18).ToString().Trim, drExcel(19).ToString.Trim, drExcel(29).ToString.Trim, drExcel(30).ToString.Trim, drExcel(31).ToString.Trim, drExcel(28).ToString.Trim.ToUpper, "Same Day Clearing Account").ToString()
                                ElseIf drExcel(17).ToString() <> "" And drExcel(18).ToString <> "" And drExcel(19).ToString() <> "" Then
                                    ValidateSameDayClearing = ICBankAccountController.ValidateGLAccountNoByAccountType(drExcel(17).ToString().Trim, drExcel(18).ToString().Trim, drExcel(19).ToString.Trim, drExcel(22).ToString.Trim.ToUpper, "Same Day Clearing Account").ToString()
                                    If ValidateSameDayClearing = False Then
                                        IsValid = False
                                        'ErrorMessage += "Invalid GL SameDay Normal Account (Account No.: " + (drExcel(17).ToString()) + " ; Branch Code: " + (drExcel(18).ToString()) + " ; Currency: " + (drExcel(19).ToString()) + " ; Client No: " + (drExcel(29).ToString) + " ; Sequence No: " + (drExcel(30).ToString) + " ; Profit Centre: " + (drExcel(31).ToString) + ") ; "
                                        ErrorMessage += "Invalid GL SameDay Normal Account (Account No.: " + (drExcel(17).ToString()) + " ; Branch Code: " + (drExcel(18).ToString()) + " ; Currency: " + (drExcel(19).ToString()) + " ) ; "
                                    End If
                                ElseIf Regex.IsMatch(drExcel(17).ToString().Trim, "^[a-zA-Z0-9.-_/]{2,100}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid SameDay Normal Account Number (" + (drExcel(17).ToString()) + ") ; "
                                ElseIf Regex.IsMatch(drExcel(18).ToString().Trim, "^[a-zA-Z0-9.-_/]{2,20}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid SameDay Normal Account Branch Code (" + (drExcel(18).ToString()) + ") ; "
                                ElseIf Regex.IsMatch(drExcel(19).ToString().Trim, "^[a-zA-Z0-9.-_/]{2,50}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid SameDay Normal Account Currency (" + (drExcel(19).ToString()) + ") ; "
                                    'ElseIf drExcel(30).ToString().Trim.Length > 9 Then
                                    '    IsValid = False
                                    '    ErrorMessage += "Invalid SameDay Normal Account Sequence no length (" + (drExcel(30).ToString()) + ") ; "
                                    'ElseIf Regex.IsMatch(drExcel(30).ToString().Trim, "^[0-9]{1,9}$") = False Then
                                    '    IsValid = False
                                    '    ErrorMessage += "Invalid SameDay Normal Account Sequence no (" + (drExcel(30).ToString()) + ") ; "
                                    'ElseIf drExcel(29).ToString().Trim.Length > 100 Then
                                    '    IsValid = False
                                    '    ErrorMessage += "Invalid SameDay Normal Account Client No. length (" + (drExcel(29).ToString()) + ") ; "
                                    'ElseIf drExcel(31).ToString().Trim.Length > 100 Then
                                    '    IsValid = False
                                    '    ErrorMessage += "Invalid SameDay Normal Account profit centre length (" + (drExcel(31).ToString()) + ") ; "
                                End If

                            End If
                        End If

                        'If drExcel(28).ToString() = "" And drExcel(17).ToString() = "" And drExcel(18).ToString() = "" And drExcel(19).ToString() = "" And drExcel(29).ToString() = "" And drExcel(30).ToString() = "" And drExcel(31).ToString() = "" Then
                        If drExcel(23).ToString() = "" And drExcel(24).ToString() = "" And drExcel(25).ToString() = "" And drExcel(26).ToString() = "" Then
                            BranchCashTillAccountType = False
                            BranchCashTillAccountNumber = False
                            BranchCashTillAccountBranchCode = False
                            BranchCashTillAccountCurrency = False
                            'SameDaySeqNo = False
                            'SameDayAccountClientNo = False
                            'SameSayProfitCentre = False
                        ElseIf drExcel(23).ToString = "" Then
                            'If drExcel(17).ToString() <> "" Or drExcel(18).ToString() <> "" Or drExcel(19).ToString() <> "" Or drExcel(29).ToString() <> "" Or drExcel(30).ToString() <> "" Or drExcel(31).ToString() <> "" Then
                            If drExcel(24).ToString() <> "" Or drExcel(25).ToString() <> "" Or drExcel(26).ToString() <> "" Then
                                IsValid = False
                                ErrorMessage += "Invalid Branch Cash Till Account type ( Branch Cash Till Account Type is empty ) ; "
                            End If
                        ElseIf drExcel(23).ToString <> "" Then
                            If drExcel(23).ToString.Length > 2 Then
                                IsValid = False
                                ErrorMessage += "Invalid Branch Cash Till account type length ( " + (drExcel(23).ToString) + " ) ; "

                            ElseIf drExcel(23).ToString.Trim.ToLower <> "rb" And drExcel(23).ToString.Trim.ToLower <> "gl" Then
                                IsValid = False
                                ErrorMessage += "Invalid Branch Cash Till account type ( " + (drExcel(23).ToString) + " ) ; "
                            ElseIf drExcel(23).ToString.Trim.ToLower = "rb" Then
                                If drExcel(24).ToString() = "" Or drExcel(25).ToString = "" Or drExcel(26).ToString() = "" Then
                                    IsValid = False
                                    ErrorMessage += "RB Branch Cash Till account must have values in"
                                    If drExcel(24).ToString = "" Then
                                        ErrorMessage += " ( Branch Cash Till AccountNumber  ) ;"
                                    End If
                                    If drExcel(25).ToString = "" Then
                                        ErrorMessage += " ( Branch Cash Till Branch Code ) ; "
                                    End If
                                    If drExcel(26).ToString = "" Then
                                        ErrorMessage += " ( Branch Cash Till Currency ) ; "
                                    End If
                                ElseIf drExcel(24).ToString() <> "" And drExcel(25).ToString <> "" And drExcel(26).ToString() <> "" Then
                                    ValidateSameDayClearing = ICBankAccountController.ValidateRBAccountNoByAccountType(drExcel(24).ToString().Trim, drExcel(25).ToString().Trim, drExcel(26).ToString.Trim, drExcel(23).ToString.ToUpper.Trim, "Branch Cash Till Account").ToString()
                                    If ValidateSameDayClearing = False Then
                                        IsValid = False
                                        ErrorMessage += "Invalid RB Branch Cash Till (Account No.: " + (drExcel(24).ToString()) + " ; Branch Code: " + (drExcel(25).ToString()) + " ; Currency: " + (drExcel(26).ToString()) + ") ; "
                                    End If
                                ElseIf Regex.IsMatch(drExcel(24).ToString(), "^[a-zA-Z0-9.-_/]{2,100}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Branch Cash Till Account Number (" + (drExcel(24).ToString()) + ") ; "
                                ElseIf Regex.IsMatch(drExcel(25).ToString(), "^[a-zA-Z0-9.-_/]{2,20}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Branch Cash Till Account Branch Code (" + (drExcel(25).ToString()) + ") ; "
                                ElseIf Regex.IsMatch(drExcel(26).ToString(), "^[a-zA-Z0-9.-_/]{2,50}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Branch Cash Till Account Currency (" + (drExcel(26).ToString()) + ") ; "
                                End If
                            ElseIf drExcel(23).ToString.ToLower = "gl" Then
                                'If drExcel(17).ToString = "" Or drExcel(18).ToString = "" Or drExcel(19).ToString = "" Or drExcel(29).ToString = "" Or drExcel(30).ToString = "" Or drExcel(31).ToString = "" Then
                                If drExcel(24).ToString = "" Or drExcel(25).ToString = "" Or drExcel(26).ToString = "" Then
                                    IsValid = False
                                    ErrorMessage += "GL Branch Cash Till Account must have values in "
                                    If drExcel(24).ToString = "" Then
                                        ErrorMessage += " ( Branch Cash Till AccountNumber ) ; "
                                    End If
                                    If drExcel(25).ToString = "" Then
                                        ErrorMessage += " ( Branch Cash Till Branch Code ) ; "
                                    End If
                                    If drExcel(26).ToString = "" Then
                                        ErrorMessage += " ( Branch Cash Till Currency ) ; "
                                    End If
                                    'If drExcel(29).ToString = "" Then
                                    '    ErrorMessage += " ( Same Day Account Client No ) ; "
                                    'End If
                                    'If drExcel(30).ToString = "" Then
                                    '    ErrorMessage += " ( Same Day Account Seq No ) ; "
                                    'End If
                                    'If drExcel(31).ToString = "" Then
                                    '    ErrorMessage += " ( Same Day Account ProfitCentre ) ; "
                                    'End If
                                    'ElseIf drExcel(17).ToString() <> "" And drExcel(18).ToString <> "" And drExcel(19).ToString() <> "" And drExcel(29).ToString <> "" And drExcel(30).ToString <> "" And drExcel(31).ToString <> "" Then
                                    '    ValidateSameDayClearing = ICBankAccountController.ValidateGLAccountNoByAccountType(drExcel(17).ToString().Trim, drExcel(18).ToString().Trim, drExcel(19).ToString.Trim, drExcel(29).ToString.Trim, drExcel(30).ToString.Trim, drExcel(31).ToString.Trim, drExcel(28).ToString.Trim.ToUpper, "Same Day Clearing Account").ToString()
                                ElseIf drExcel(24).ToString() <> "" And drExcel(25).ToString <> "" And drExcel(26).ToString() <> "" Then
                                    ValidateSameDayClearing = ICBankAccountController.ValidateGLAccountNoByAccountType(drExcel(24).ToString().Trim, drExcel(25).ToString().Trim, drExcel(26).ToString.Trim, drExcel(22).ToString.Trim.ToUpper, "Branch Cash Till Account").ToString()
                                    If ValidateSameDayClearing = False Then
                                        IsValid = False
                                        'ErrorMessage += "Invalid GL SameDay Normal Account (Account No.: " + (drExcel(17).ToString()) + " ; Branch Code: " + (drExcel(18).ToString()) + " ; Currency: " + (drExcel(19).ToString()) + " ; Client No: " + (drExcel(29).ToString) + " ; Sequence No: " + (drExcel(30).ToString) + " ; Profit Centre: " + (drExcel(31).ToString) + ") ; "
                                        ErrorMessage += "Invalid GL Branch Cash Till Account (Account No.: " + (drExcel(24).ToString()) + " ; Branch Code: " + (drExcel(25).ToString()) + " ; Currency: " + (drExcel(26).ToString()) + " ) ; "
                                    End If
                                ElseIf Regex.IsMatch(drExcel(24).ToString().Trim, "^[a-zA-Z0-9.-_/]{2,100}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Branch Cash Till Account Number (" + (drExcel(24).ToString()) + ") ; "
                                ElseIf Regex.IsMatch(drExcel(25).ToString().Trim, "^[a-zA-Z0-9.-_/]{2,20}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Branch Cash Till Account Branch Code (" + (drExcel(25).ToString()) + ") ; "
                                ElseIf Regex.IsMatch(drExcel(26).ToString().Trim, "^[a-zA-Z0-9.-_/]{2,50}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Branch Cash Till Account Currency (" + (drExcel(26).ToString()) + ") ; "
                                    'ElseIf drExcel(30).ToString().Trim.Length > 9 Then
                                    '    IsValid = False
                                    '    ErrorMessage += "Invalid SameDay Normal Account Sequence no length (" + (drExcel(30).ToString()) + ") ; "
                                    'ElseIf Regex.IsMatch(drExcel(30).ToString().Trim, "^[0-9]{1,9}$") = False Then
                                    '    IsValid = False
                                    '    ErrorMessage += "Invalid SameDay Normal Account Sequence no (" + (drExcel(30).ToString()) + ") ; "
                                    'ElseIf drExcel(29).ToString().Trim.Length > 100 Then
                                    '    IsValid = False
                                    '    ErrorMessage += "Invalid SameDay Normal Account Client No. length (" + (drExcel(29).ToString()) + ") ; "
                                    'ElseIf drExcel(31).ToString().Trim.Length > 100 Then
                                    '    IsValid = False
                                    '    ErrorMessage += "Invalid SameDay Normal Account profit centre length (" + (drExcel(31).ToString()) + ") ; "
                                End If

                            End If
                        End If



                    End If


                    If drExcel(3).ToString() = "" Then
                        IsValid = False
                        ErrorMessage += "City ID is empty (" + (drExcel(3).ToString()) + ") ; "
                    ElseIf drExcel(3).ToString().Length() > 9 Then
                        IsValid = False
                        ErrorMessage += "City ID length exceeded (" + (drExcel(3).ToString()) + ") ; "
                    ElseIf Regex.IsMatch(drExcel(3).ToString().Trim, "^[0-9]{1,9}$") = False Then
                        IsValid = False
                        ErrorMessage += "City ID must be in numbers only (" + (drExcel(3).ToString()) + ") ; "
                    Else
                        CityName = ICCityController.GetCityName(drExcel(3).ToString()).ToString()
                        If CityName.ToString() = "" Then
                            IsValid = False
                            ErrorMessage += "Invalid City ID (" + (drExcel(3).ToString()) + ") ; "
                        End If
                    End If

                    If drExcel(4).ToString() <> "" Then
                        If drExcel(4).ToString().Length() > 15 Then
                            IsValid = False
                            ErrorMessage += "Invalid Phone1 Length (" + (drExcel(4).ToString()) + ") ; "
                        ElseIf Regex.IsMatch(drExcel(4).ToString(), "^\+?[\d- ]{2,15}$") = False Then
                            IsValid = False
                            ErrorMessage += "Invalid Phone1 Number (" + (drExcel(4).ToString()) + ") ; "
                        End If
                    End If
                 If drExcel(5).ToString().Length() > 15 Then
                        IsValid = False
                        ErrorMessage += "Invalid Phone2 Length (" + (drExcel(5).ToString()) + ") ; "
                    ElseIf drExcel(5).ToString() <> "" Then
                        If Regex.IsMatch(drExcel(5).ToString(), "^\+?[\d- ]{2,15}$") = False Then
                            IsValid = False
                            ErrorMessage += "Invalid Phone2 Number (" + (drExcel(5).ToString()) + ") ; "
                        End If
                    End If
                    If drExcel(6).ToString() <> "" Then
                        If drExcel(6).ToString().Length() > 250 Then
                            IsValid = False
                            ErrorMessage += "Invalid Address Length (" + (drExcel(6).ToString()) + ") ; "
                        End If
                    End If


                    If drExcel(7).ToString() <> "" Then
                        If drExcel(7).ToString().Length() > 100 Then
                            IsValid = False
                            ErrorMessage += "Invalid Email Length (" + (drExcel(7).ToString()) + ") ; "
                        ElseIf Regex.IsMatch(drExcel(7).ToString(), "^\w+([-+.']\w+)*@\w+([-.%]\w+)*\.\w+([-.]\w+)*$") = False Then
                            IsValid = False
                            ErrorMessage += "Invalid Email (" + (drExcel(7).ToString()) + ") ; "
                        End If
                    End If

                    If drExcel(8).ToString() = "" Then
                        Description = False
                    ElseIf drExcel(8).ToString().Length() > 250 Then
                        IsValid = False
                        ErrorMessage += "Invalid Description Length (" + (drExcel(8).ToString()) + ") ; "
                    End If

                    If drExcel(9).ToString() = "" Then
                        IsBranch = False
                    Else
                        If Boolean.TryParse(drExcel(9).ToString(), False) Then
                            IsBranch = CBool(drExcel(9).ToString())
                        Else
                            IsValid = False
                            ErrorMessage += "Invalid Is Branch value (" + (drExcel(9).ToString()) + ") ; "
                        End If
                    End If
                    If drExcel(27).ToString() = "" Then
                        IsAllowedForTagging = False
                    Else
                        If Boolean.TryParse(drExcel(27).ToString(), False) Then
                            IsAllowedForTagging = CBool(drExcel(27).ToString())
                        Else
                            IsValid = False
                            ErrorMessage += "Invalid Is Allowed for tagging (" + (drExcel(27).ToString()) + ") ; "
                        End If
                    End If
                    If drExcel(10).ToString() = "" Then
                        IsPrintLocation = False
                    Else
                        If Boolean.TryParse(drExcel(10).ToString(), False) Then
                            IsPrintLocation = CBool(drExcel(10).ToString())
                        Else
                            IsValid = False
                            ErrorMessage += "Invalid Is Print Location value (" + (drExcel(10).ToString()) + ") ; "
                        End If
                    End If

                    If IsValid = True Then
                        gvBankBranch.Visible = True
                        btnAddBranchesinBulk.Visible = True

                        drFiltered = dtFiltered.NewRow()
                        drFiltered(0) = drExcel(0).ToString()
                        drFiltered(1) = drExcel(1).ToString()
                        drFiltered(2) = drExcel(2).ToString()
                        drFiltered(3) = BankName.ToString()
                        drFiltered(4) = drExcel(3).ToString()
                        drFiltered(5) = CityName.ToString()
                        drFiltered(6) = drExcel(4).ToString()
                        drFiltered(7) = drExcel(5).ToString()
                        drFiltered(8) = drExcel(6).ToString()
                        drFiltered(9) = drExcel(7).ToString()
                        drFiltered(10) = drExcel(8).ToString()
                        drFiltered(11) = IsBranch
                        drFiltered(12) = IsPrintLocation
                        drFiltered(29) = IsAllowedForTagging
                        If IsPrincipal = True Then
                            drFiltered(13) = drExcel(11).ToString().Trim
                            drFiltered(14) = drExcel(12).ToString().Trim
                            drFiltered(15) = drExcel(13).ToString().Trim
                            drFiltered(16) = drExcel(14).ToString().Trim
                            drFiltered(17) = drExcel(15).ToString().Trim
                            drFiltered(18) = drExcel(16).ToString().Trim
                            drFiltered(19) = drExcel(17).ToString().Trim
                            drFiltered(20) = drExcel(18).ToString().Trim
                            drFiltered(21) = drExcel(19).ToString().Trim
                            drFiltered(22) = drExcel(20).ToString().Trim
                            drFiltered(23) = drExcel(21).ToString().Trim
                            drFiltered(24) = drExcel(22).ToString().Trim
                            drFiltered(25) = drExcel(23).ToString().Trim
                            drFiltered(26) = drExcel(24).ToString().Trim
                            drFiltered(27) = drExcel(25).ToString().Trim
                            drFiltered(28) = drExcel(26).ToString().Trim

                            'drFiltered(29) = drExcel(27).ToString().Trim
                            'drFiltered(30) = drExcel(28).ToString().Trim
                            'drFiltered(31) = drExcel(29).ToString().Trim
                            'drFiltered(32) = drExcel(30).ToString().Trim
                            'drFiltered(33) = drExcel(31).ToString().Trim
                        End If
                        dtFiltered.Rows.Add(drFiltered)
                    Else
                        ' gvBankBranch.Visible = False
                        gvUnVarified.Visible = True

                        drUnVarified = dtUnVarified.NewRow()
                        drUnVarified(0) = RowNo.ToString()
                        drUnVarified(1) = ErrorMessage.ToString()

                        dtUnVarified.Rows.Add(drUnVarified)
                    End If
                Next

                ViewState("dtFiltered") = dtFiltered
                ViewState("dtUnVarified") = dtUnVarified

                SetGVBankBranchesGVUnverified(True)


            Else
                UIUtilities.ShowDialog(Me, "Save Bulk Bank Branch", "Empty File.", ICBO.IC.Dialogmessagetype.Failure)
                Clear()
                Exit Sub
            End If
            'End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub SetGVBankBranchesGVUnverified(ByVal IsBind As Boolean)
        Try
            Dim dtFiltered As New DataTable
            Dim dtUnVarified As New DataTable

            dtFiltered = DirectCast(ViewState("dtFiltered"), DataTable)
            dtUnVarified = DirectCast(ViewState("dtUnVarified"), DataTable)

            gvBankBranch.DataSource = dtFiltered
            gvUnVarified.DataSource = dtUnVarified

            If IsBind = True Then
                gvBankBranch.DataBind()
                gvUnVarified.DataBind()
            End If


            If dtFiltered.Rows.Count > 0 Then
                gvBankBranch.Visible = True
            Else
                gvBankBranch.Visible = False
                btnAddBranchesinBulk.Visible = False
            End If
            If dtUnVarified.Rows.Count > 0 Then
                gvUnVarified.Visible = True
            Else
                gvUnVarified.Visible = False
            End If


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click




        If Not hfFilePath.Value Is Nothing And hfFilePath.Value <> "" Then
                Dim aFileInfo As IO.FileInfo
                aFileInfo = New IO.FileInfo(hfFilePath.Value.ToString())
                'Delete file from HDD. 
                If aFileInfo.Exists Then
                    aFileInfo.Delete()
                End If
            End If
            Response.Redirect(NavigateURL(), False)

    End Sub

    Protected Sub btnAddBranchesinBulk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddBranchesinBulk.Click
        If Page.IsValid Then
            Try
                Dim dtFiltered As New DataTable
                Dim drFiltered As DataRow

                Dim objICOffice As New ICOffice
                objICOffice.es.Connection.CommandTimeout = 3600

                If ViewState("dtFiltered") Is Nothing Then
                    UIUtilities.ShowDialog(Me, "File Upload", "Record not Found.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                Else
                    dtFiltered = DirectCast(ViewState("dtFiltered"), DataTable)


                    For Each drFiltered In dtFiltered.Rows

                        Try
                            objICOffice = New ICOffice
                            objICOffice.OfficeCode = drFiltered(0).ToString()
                            objICOffice.OfficeName = drFiltered(1).ToString()
                            objICOffice.BankCode = drFiltered(2).ToString()
                            objICOffice.CityID = drFiltered(4).ToString()
                            objICOffice.Phone1 = drFiltered(6).ToString()
                            objICOffice.Phone2 = drFiltered(7).ToString()
                            objICOffice.Address = drFiltered(8).ToString()
                            objICOffice.Email = drFiltered(9).ToString()
                            objICOffice.Description = drFiltered(10).ToString()
                            objICOffice.IsBranch = drFiltered(11).ToString()
                            objICOffice.IsPrintingLocation = drFiltered(12).ToString()
                            objICOffice.IsAllowedForTagging = drFiltered(29).ToString()
                            objICOffice.OffceType = "Branch Office"
                            objICOffice.IsActive = True
                            objICOffice.CreatedBy = Me.UserId
                            objICOffice.CreateDate = Date.Now
                            objICOffice.Creater = Me.UserId
                            objICOffice.CreationDate = Date.Now


                            If Not drFiltered(22).ToString = "" Then
                                If drFiltered(22).ToString.ToUpper = "RB" Then
                                    objICOffice.ClearingNormalAccountType = drFiltered(22).ToString.ToUpper
                                    objICOffice.ClearingNormalAccountNumber = drFiltered(13).ToString()
                                    objICOffice.ClearingNormalAccountBranchCode = drFiltered(15).ToString()
                                    objICOffice.ClearingNormalAccountCurrency = drFiltered(14).ToString
                                    objICOffice.ClearingNormalClientNo = Nothing
                                    objICOffice.ClearingNormalSeqNo = Nothing
                                    objICOffice.ClearingNormalProfitCenter = Nothing
                                ElseIf drFiltered(22).ToString.ToUpper = "GL" Then
                                    objICOffice.ClearingNormalAccountType = drFiltered(22).ToString.ToUpper
                                    objICOffice.ClearingNormalAccountNumber = drFiltered(13).ToString()
                                    objICOffice.ClearingNormalAccountBranchCode = drFiltered(15).ToString()
                                    objICOffice.ClearingNormalAccountCurrency = drFiltered(14).ToString
                                    objICOffice.ClearingNormalClientNo = Nothing
                                    objICOffice.ClearingNormalSeqNo = Nothing
                                    objICOffice.ClearingNormalProfitCenter = Nothing
                                End If
                            Else
                                objICOffice.ClearingNormalAccountType = Nothing
                                objICOffice.ClearingNormalAccountNumber = Nothing
                                objICOffice.ClearingNormalAccountBranchCode = Nothing
                                objICOffice.ClearingNormalAccountCurrency = Nothing
                                objICOffice.ClearingNormalClientNo = Nothing
                                objICOffice.ClearingNormalSeqNo = Nothing
                                objICOffice.ClearingNormalProfitCenter = Nothing
                            End If

                            If Not drFiltered(23).ToString = "" Then
                                If drFiltered(23).ToString.ToUpper = "RB" Then
                                    objICOffice.InterCityAccountType = drFiltered(23).ToString.ToUpper
                                    objICOffice.InterCityAccountNumber = drFiltered(16).ToString()
                                    objICOffice.InterCityAccountBranchCode = drFiltered(17).ToString()
                                    objICOffice.InterCityAccountCurrency = drFiltered(18).ToString()
                                    objICOffice.InterCityAccountClientNo = Nothing
                                    objICOffice.InterCityAccountSeqNo = Nothing
                                    objICOffice.InterCityAccountProfitCentre = Nothing

                                ElseIf drFiltered(23).ToString.ToUpper = "GL" Then
                                    objICOffice.InterCityAccountType = drFiltered(23).ToString.ToUpper
                                    objICOffice.InterCityAccountNumber = drFiltered(16).ToString()
                                    objICOffice.InterCityAccountBranchCode = drFiltered(17).ToString()
                                    objICOffice.InterCityAccountCurrency = drFiltered(18).ToString()
                                    objICOffice.InterCityAccountClientNo = Nothing
                                    objICOffice.InterCityAccountSeqNo = Nothing
                                    objICOffice.InterCityAccountProfitCentre = Nothing
                                End If

                            Else
                                objICOffice.InterCityAccountType = Nothing
                                objICOffice.InterCityAccountNumber = Nothing
                                objICOffice.InterCityAccountBranchCode = Nothing
                                objICOffice.InterCityAccountCurrency = Nothing
                                objICOffice.InterCityAccountClientNo = Nothing
                                objICOffice.InterCityAccountSeqNo = Nothing
                                objICOffice.InterCityAccountProfitCentre = Nothing
                            End If

                            If Not drFiltered(24).ToString = "" Then
                                If drFiltered(24).ToString.ToUpper = "RB" Then
                                    objICOffice.SameDayAccountType = drFiltered(24).ToString.ToUpper
                                    objICOffice.SameDayAccountNumber = drFiltered(19).ToString().Trim
                                    objICOffice.SameDayAccountBranchCode = drFiltered(20).ToString()
                                    objICOffice.SameDayAccountCurrency = drFiltered(21).ToString().Trim
                                    objICOffice.SameDayAccountClientNo = Nothing
                                    objICOffice.SameDayAccountSeqNo = Nothing
                                    objICOffice.SameDayAccountProfitCentre = Nothing
                                ElseIf drFiltered(24).ToString.ToUpper = "GL" Then
                                    objICOffice.SameDayAccountType = drFiltered(24).ToString.ToUpper
                                    objICOffice.SameDayAccountNumber = drFiltered(19).ToString().Trim
                                    objICOffice.SameDayAccountBranchCode = drFiltered(20).ToString()
                                    objICOffice.SameDayAccountCurrency = drFiltered(21).ToString().Trim
                                    objICOffice.SameDayAccountClientNo = Nothing
                                    objICOffice.SameDayAccountSeqNo = Nothing
                                    objICOffice.SameDayAccountProfitCentre = Nothing
                                End If
                            Else
                                objICOffice.SameDayAccountType = Nothing
                                objICOffice.SameDayAccountNumber = Nothing
                                objICOffice.SameDayAccountBranchCode = Nothing
                                objICOffice.SameDayAccountCurrency = Nothing
                                objICOffice.SameDayAccountClientNo = Nothing
                                objICOffice.SameDayAccountSeqNo = Nothing
                                objICOffice.SameDayAccountProfitCentre = Nothing
                            End If

                            If Not drFiltered(25).ToString = "" Then
                                If drFiltered(25).ToString.ToUpper = "RB" Then
                                    objICOffice.BranchCashTillAccountType = drFiltered(25).ToString.ToUpper
                                    objICOffice.BranchCashTillAccountNumber = drFiltered(26).ToString().Trim
                                    objICOffice.BranchCashTillAccountBranchCode = drFiltered(27).ToString()
                                    objICOffice.BranchCashTillAccountCurrency = drFiltered(28).ToString().Trim
                                    objICOffice.BranchCashTillAccountClientNo = Nothing
                                    objICOffice.BranchCashTillAccountSeqNo = Nothing
                                    objICOffice.BranchCashTillAccountProfitCentre = Nothing
                                ElseIf drFiltered(24).ToString.ToUpper = "GL" Then
                                    objICOffice.BranchCashTillAccountType = drFiltered(25).ToString.ToUpper
                                    objICOffice.BranchCashTillAccountNumber = drFiltered(26).ToString().Trim
                                    objICOffice.BranchCashTillAccountBranchCode = drFiltered(27).ToString()
                                    objICOffice.BranchCashTillAccountCurrency = drFiltered(28).ToString().Trim
                                    objICOffice.BranchCashTillAccountClientNo = Nothing
                                    objICOffice.BranchCashTillAccountSeqNo = Nothing
                                    objICOffice.BranchCashTillAccountProfitCentre = Nothing
                                End If
                            Else
                                objICOffice.BranchCashTillAccountType = Nothing
                                objICOffice.BranchCashTillAccountNumber = Nothing
                                objICOffice.BranchCashTillAccountBranchCode = Nothing
                                objICOffice.BranchCashTillAccountCurrency = Nothing
                                objICOffice.BranchCashTillAccountClientNo = Nothing
                                objICOffice.BranchCashTillAccountSeqNo = Nothing
                                objICOffice.BranchCashTillAccountProfitCentre = Nothing
                            End If



                            ICOfficeController.AddOffice(objICOffice, False, Me.UserId, Me.UserInfo.Username, "")
                        Catch ex As Exception
                            ProcessModuleLoadException(Me, ex, False)
                            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                        End Try
                    Next
                End If
                UIUtilities.ShowDialog(Me, " Save Bulk Bank Branch", "File saved successfully.", ICBO.IC.Dialogmessagetype.Success)
                Dim aFileInfo As IO.FileInfo
                aFileInfo = New IO.FileInfo(hfFilePath.Value.ToString())
                'Delete file from HDD. 
                If aFileInfo.Exists Then
                    aFileInfo.Delete()
                End If

                Clear()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub


    Private Function CheckDuplicateCode(ByVal OfficeCode As String) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim colloffice As New ICOfficeCollection

            colloffice.es.Connection.CommandTimeout = 3600

            colloffice.Query.Where(colloffice.Query.OfficeCode.ToLower.Trim = OfficeCode.ToLower.Trim)
            If colloffice.Query.Load() Then
                rslt = True
            End If

            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
        End Try
    End Function

    Private Function CheckDuplicateName(ByVal OfficeName As String) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim colloffice As New ICOfficeCollection

            colloffice.es.Connection.CommandTimeout = 3600

            colloffice.Query.Where(colloffice.Query.OfficeName.ToLower.Trim = OfficeName.ToLower.Trim)
            If colloffice.Query.Load() Then
                rslt = True
            End If

            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub gvUnVarified_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvUnVarified.NeedDataSource
        SetGVBankBranchesGVUnverified(False)
    End Sub

End Class