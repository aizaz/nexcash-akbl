﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_BankBranchManagement_ViewBankBranch
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Dim dtDa As DataTable
    Private htRights As Hashtable

#Region "Page Load"
    Protected Sub DesktopModules_CompanyOfficeManagement_ViewCompanyOffice_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
          
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing
                btnAddBulkBranch.Visible = CBool(htRights("Add"))
                btnSaveBranch.Visible = CBool(htRights("Add"))
                btnDeleteBankBranches0.Visible = CBool(htRights("Delete"))
                btnApproveBankBranches.Visible = CBool(htRights("Approve"))

                LoadddlBank()
                LoadBankBranches(True)
                ViewState("SortExp") = Nothing
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Bank Branch Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Grid View Events"

    Protected Sub gvBankBranch_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvBankBranch.ItemCommand
        Try
            If e.CommandName = "del" Then
                Dim objBankBranch As New ICOffice

                objBankBranch.es.Connection.CommandTimeout = 3600

                objBankBranch.LoadByPrimaryKey(e.CommandArgument.ToString())
                If objBankBranch.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                    If objBankBranch.IsApprove Then
                        UIUtilities.ShowDialog(Me, "Warning", "Branch is approved and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
                    Else
                        ICOfficeController.DeleteOffice(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString(), "Bank Branch")
                        UIUtilities.ShowDialog(Me, "Delete Bank Branch", "Bank Branch deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                        LoadBankBranches(True)
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Delete Bank Branch", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvBankBranch_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvBankBranch.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
               For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvBankBranch.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvBankBranch_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvBankBranch.NeedDataSource
        LoadBankBranches(False)
    End Sub

    Protected Sub gvBankBranch_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvBankBranch.PageIndexChanged
        gvBankBranch.CurrentPageIndex = e.NewPageIndex

    End Sub

    Protected Sub gvBankBranch_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvBankBranch.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvBankBranch.MasterTableView.ClientID & "','0');"
        End If
    End Sub

#End Region

#Region "Drop Down"

    Private Sub LoadddlBank()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlBank.Items.Clear()
            ddlBank.Items.Add(lit)
            ddlBank.AppendDataBoundItems = True
            ddlBank.DataSource = ICBankController.GetBank()
            ddlBank.DataTextField = "BankName"
            ddlBank.DataValueField = "BankCode"
            ddlBank.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

   
    Private Sub LoadBankBranches(ByVal IsBind As Boolean)

        Try
            ICOfficeController.GetBankBranchgv(ddlBank.SelectedValue.ToString(), Me.gvBankBranch.CurrentPageIndex + 1, Me.gvBankBranch.PageSize, Me.gvBankBranch, IsBind)
            If gvBankBranch.Items.Count > 0 Then
                lblBranchList.Visible = True
                lblBankBranchesRNF.Visible = False
                gvBankBranch.Visible = True
             
                btnDeleteBankBranches0.Visible = CBool(htRights("Delete"))
                btnApproveBankBranches.Visible = CBool(htRights("Approve"))
                gvBankBranch.Columns(8).Visible = CBool(htRights("Delete"))
            Else
                lblBranchList.Visible = True
                gvBankBranch.Visible = False
                btnApproveBankBranches.Visible = False
                btnDeleteBankBranches0.Visible = False
                lblBankBranchesRNF.Visible = True
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub



    Protected Sub ddlBank_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBank.SelectedIndexChanged
        Try
            LoadBankBranches(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

#End Region

#Region "Button Events"

    Protected Sub btnSaveBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveBranch.Click
        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveBranch", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If
    End Sub

    Protected Sub btnApproveBankBranches_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveBankBranches.Click
        If Page.IsValid Then
            Try
                Dim rowgvBankBranch As GridDataItem
                Dim chkSelect As CheckBox
                Dim officeID As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0
                Dim icOffice As New ICOffice

                If CheckgvBankBranchForProcessAll() = True Then
                    For Each rowgvBankBranch In gvBankBranch.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvBankBranch.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            officeID = rowgvBankBranch("officeID").Text.ToString()
                            icOffice.es.Connection.CommandTimeout = 3600
                            If icOffice.LoadByPrimaryKey(officeID.ToString()) Then
                                If icOffice.IsApprove Then
                                    FailCount = FailCount + 1
                                Else
                                    If Me.UserInfo.IsSuperUser = False Then
                                        If icOffice.CreatedBy = Me.UserId Then
                                            FailCount = FailCount + 1
                                        Else
                                            ICOfficeController.ApproveOffice(officeID.ToString(), chkSelect.Checked, Me.UserId, Me.UserInfo.Username)
                                            PassCount = PassCount + 1
                                        End If
                                    Else
                                        ICOfficeController.ApproveOffice(officeID.ToString(), chkSelect.Checked, Me.UserId, Me.UserInfo.Username)
                                        PassCount = PassCount + 1
                                    End If
                                End If
                            End If
                        End If

                    Next
                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Approve Bank Branches", "[" & FailCount.ToString() & "]   Bank Branches can not be approve due to following reasons : <br /> 1.  Bank Branch is approve.<br /> 2.  Bank Branch must be approve by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Approve Bank Branches", "[" & PassCount.ToString() & "]  Bank Branches approve successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If

                    UIUtilities.ShowDialog(Me, "Approve Bank Branches", "[" & PassCount.ToString() & "]  Bank Branches approve successfully. [" & FailCount.ToString() & "]  Bank Branches can not be approve due to following reasons : <br /> 1.  Bank Branch is approve.<br /> 2.  Bank Branch must be approve by the user other than the maker.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())

                Else
                    UIUtilities.ShowDialog(Me, "Approve Bank Branches", "Please select atleast one(1) Bank Branch.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Private Function CheckgvBankBranchForProcessAll() As Boolean
        Try
            Dim rowgvBankBranch As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvBankBranch In gvBankBranch.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvBankBranch.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnDeleteBankBranches0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteBankBranches0.Click
        If Page.IsValid Then
            Try
                Dim rowgvBankBranch As GridDataItem
                Dim chkSelect As CheckBox
                Dim officeID As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0

                If CheckgvBankBranchForProcessAll() = True Then
                    For Each rowgvBankBranch In gvBankBranch.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvBankBranch.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            officeID = rowgvBankBranch("officeID").Text.ToString()
                            Dim icoffice As New ICOffice
                            icoffice.es.Connection.CommandTimeout = 3600
                            If icoffice.LoadByPrimaryKey(officeID.ToString()) Then
                                If icoffice.IsApprove Then
                                    FailCount = FailCount + 1
                                Else
                                    Try
                                        ICOfficeController.DeleteOffice(officeID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString(), "Bank Branch")
                                        PassCount = PassCount + 1
                                    Catch ex As Exception
                                        If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                            FailCount = FailCount + 1
                                        End If
                                    End Try
                                End If
                            End If
                        End If

                    Next

                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Bank Branches", "[" & FailCount.ToString() & "] Bank Branches can not be deleted due to following reasons : <br /> 1. Bank Branche is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Bank Branches", "[" & PassCount.ToString() & "]  Bank Branches deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Delete Bank Branches", "[" & PassCount.ToString() & "] Bank Branches deleted successfully. [" & FailCount.ToString() & "] Bank Branches can not be deleted due to following reasons : <br /> 1. Bank Branch is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Delete Bank Branches", "Please select atleast one(1) Bank Branch.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub
   
    Protected Sub btnAddBulkBranch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddBulkBranch.Click

        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveBulkBranch", "&mid=" & Me.ModuleId & "&id=1"), False)
        End If
    End Sub

#End Region

End Class
