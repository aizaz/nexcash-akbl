﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveBankBranch.ascx.vb" Inherits="DesktopModules_CompanyOfficeManagement_SaveCompanyOffice" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .style1
    {
        width: 25%;
        height: 23px;
    }
</style>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }

</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior2" EmptyMessage=""
        Type="Number">
    </telerik:NumericTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior2" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9 -_]{1,10}$" ErrorMessage="Invalid Branch Code"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBranchCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="true"
        EmptyMessage="" ErrorMessage="Invalid Branch Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBranchName" />
        </TargetControls>
    </telerik:TextBoxSetting>
     <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehaviorPhone1" ValidationExpression="^\+?[\d- ]{2,15}$"
        Validation-IsRequired="false" ErrorMessage="Invalid Phone Number" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBranchPhone1" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior3" ValidationExpression="^\+?[\d- ]{2,15}$"
        ErrorMessage="Invalid Phone Number" EmptyMessage="" Validation-IsRequired="false">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBranchPhone2" />
        </TargetControls>
        </telerik:RegExpTextBoxSetting>
         <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehaviorEmail" Validation-IsRequired="false"
        EmptyMessage="" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.%]\w+)*\.\w+([-.]\w+)*$"
        ErrorMessage="Invalid Email">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBranchEmail" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
     <telerik:TextBoxSetting BehaviorID="RagExpBehaviorAddress" Validation-IsRequired="false"
        ErrorMessage="Enter Address" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBranchAddress" />
        </TargetControls>
    </telerik:TextBoxSetting>
         <telerik:TextBoxSetting BehaviorID="RagExpBehaviorOffName" Validation-IsRequired="True"
        ErrorMessage="Invalid Office Name" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtOfficeName" />
        </TargetControls>
    </telerik:TextBoxSetting>
     <telerik:TextBoxSetting BehaviorID="RagExpBehaviorDescription"
         EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBranchDescrption" />
        </TargetControls>
    </telerik:TextBoxSetting>
        <telerik:RegExpTextBoxSetting BehaviorID="ddClearingAccntNo" Validation-IsRequired="false"
        ValidationExpression="^[a-zA-Z0-9.-_/]{1,15}$" ErrorMessage="Invalid Account No"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDDClearingAccNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
        <telerik:RegExpTextBoxSetting BehaviorID="poClearingAccntNo" Validation-IsRequired="false"
        ValidationExpression="^[a-zA-Z0-9.-_/]{1,15}$" ErrorMessage="Invalid Account No"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPOClearingAccNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
      <telerik:TextBoxSetting BehaviorID="ddaccnttitle" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Invalid Account Title">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDDAcntTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>

          <telerik:TextBoxSetting BehaviorID="poaccnttitle" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Invalid Account Title">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPOAccntTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>
 
</telerik:RadInputManager>
<telerik:RadInputManager ID="radForAccounts" runat="server" Enabled="false">
    <telerik:RegExpTextBoxSetting BehaviorID="ddClearingAccntNo" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9.-_/]{1,15}$" ErrorMessage="Invalid Account No"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDDClearingAccNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
        <telerik:RegExpTextBoxSetting BehaviorID="poClearingAccntNo" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9.-_/]{1,15}$" ErrorMessage="Invalid Account No"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPOClearingAccNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
      <telerik:TextBoxSetting BehaviorID="ddaccnttitle" Validation-IsRequired="true"
        EmptyMessage="" ErrorMessage="Invalid Account Title">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDDAcntTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>

          <telerik:TextBoxSetting BehaviorID="poaccnttitle" Validation-IsRequired="true"
        EmptyMessage="" ErrorMessage="Invalid Account Title">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPOAccntTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>

</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBankType" runat="server" Text="Select Bank Type" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label16" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBankCode" runat="server" Text="Select Bank" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlBankType" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem Value="0">-- Please Select Bank Type --</asp:ListItem>
                <asp:ListItem>Principal Bank</asp:ListItem>
                <asp:ListItem>Non-Principal Bank</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">

            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlBank" runat="server" 
                CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvBankType" runat="server" 
                ControlToValidate="ddlBankType" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Bank Type" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvBank" runat="server" 
                ControlToValidate="ddlBank" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Bank" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBranchCode" runat="server" Text="Branch Code" 
                CssClass="lbl"></asp:Label><asp:Label
                ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBranchName" runat="server" Text="Branch Name" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtBranchCode" runat="server" CssClass="txtbox" 
                MaxLength="10"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtBranchName" runat="server" CssClass="txtbox" 
                MaxLength="100"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            </td>
        <td align="left" valign="top" class="style1">
            </td>
        <td align="left" valign="top" class="style1">
            </td>
        <td align="left" valign="top" class="style1">
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBranchPhone1" runat="server" Text="Branch Phone 1" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBranchPhone2" runat="server" Text="Branch Phone 2" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtBranchPhone1" runat="server" CssClass="txtbox" 
                MaxLength="15"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtBranchPhone2" runat="server" CssClass="txtbox" 
                MaxLength="15"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBranchAddress" runat="server" Text="Branch Address" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyEmail" runat="server" Text="Branch Email" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtBranchAddress" runat="server" CssClass="txtbox" 
                MaxLength="250" TextMode="MultiLine" Height="70px"  
                onkeypress="return textboxMultilineMaxNumber(this,250)"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtBranchEmail" runat="server" CssClass="txtbox" 
                MaxLength="100"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyCountry" runat="server" Text="Select Country" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label9" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyProvince" runat="server" Text="Select Province" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label10" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCountry" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlProvince" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvGroup0" runat="server" 
                ControlToValidate="ddlCountry" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Country" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvGroup1" runat="server" 
                ControlToValidate="ddlProvince" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Province" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyCity" runat="server" Text="Select City" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label11" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblDescrption" runat="server" Text="Description" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCity" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
            <br />
            <asp:RequiredFieldValidator ID="rfvcity" runat="server" 
                ControlToValidate="ddlCity" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select City" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtBranchDescrption" runat="server" CssClass="txtbox" 
                MaxLength="250" TextMode="MultiLine" Height="70px"  
                onkeypress="return textboxMultilineMaxNumber(this,250)"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblNormalClearingAcc" runat="server" Text="Select Normal Clearing Account" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIntercityClearingAcc" runat="server" Text="Select Intercity Clearing Account" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlNormalClearingAcc" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem>--Please Select--</asp:ListItem>
            </asp:DropDownList>
         </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlIntercityClearingAcc" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem>--Please Select--</asp:ListItem>
            </asp:DropDownList>
         </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblSameDayClearingAcc" runat="server" Text="Select Same Day Clearing Account" 
                CssClass="lbl"></asp:Label>
         </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBranchCashTillAccount" runat="server" Text="Branch Cash Till Account" 
                CssClass="lbl"></asp:Label>
         </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlSameDayClearingAcc" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem>--Please Select--</asp:ListItem>
            </asp:DropDownList>
         </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlBranchCashTillAccount" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
         </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>

    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblPrintLocation" runat="server" Text="Is Printing Location" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
             <asp:Label ID="lblIsAllowedForTagging" runat="server" Text="Allowed For Tagging" 
                CssClass="lbl"></asp:Label></td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkPrntLocation" runat="server" Text=" " CssClass="chkBox" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
             <asp:CheckBox ID="chkIsAllowedForTagging" runat="server" Text=" " CssClass="chkBox" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
             &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
             &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIsActive" runat="server" Text="Is Active" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIsApproved" runat="server" Text="Is Approved" CssClass="lbl" Visible="False"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkActive" runat="server" Text=" " CssClass="chkBox" Checked="True" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkApproved" runat="server" Text=" " Visible="False" CssClass="chkBox" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnApproved" runat="server" Text="Approved" CssClass="btn" Visible="False"
                Width="71px" />
            &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False"
               />
            &nbsp;
        </td>
    </tr>
</table>
