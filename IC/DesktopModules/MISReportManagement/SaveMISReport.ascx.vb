﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports Telerik.Web.UI

Partial Class DesktopModules_CompanyOfficeManagement_SaveCompanyOffice
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ReportID As String
    Private htRights As Hashtable

#Region "Page Load"
    Protected Sub DesktopModules_CompanyOfficeManagement_SaveCompanyOffice_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            ReportID = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing
               

                If ReportID.ToString() = "0" Then
                    Clear()
                    lblPageHeader.Text = "Add MIS Report"
                    gvAssignedUsersToReport.Visible = False
                    btnSave.Visible = CBool(htRights("Add"))
                    btnApprove.Visible = False
                    btnDelete.Visible = False
                    hlDownloadReportFile.Visible = False
                    tblGroup.Style.Add("display", "none")
                    tblUsers.Style.Add("display", "none")
                    LoadddlGroup()
                    LoadddlCompany()
                Else
                    btnDelete.Visible = True
                    btnApprove.Visible = True
                    LoadgvAssignedUsersToReport(True)
                    lblPageHeader.Text = "Edit MIS Report"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    '   btnApprove.Visible = CBool(htRights("Approve"))


                    rfvReportFile.Enabled = False

                    Dim objUser As New ICUser
                    Dim objReport As New ICMISReports
                    objReport.es.Connection.CommandTimeout = 3600
                    objUser.es.Connection.CommandTimeout = 3600

                    objReport.LoadByPrimaryKey(ReportID)
                    txtReportName.Text = objReport.ReportName.ToString()
                    rbtnReportTypeList.SelectedValue = objReport.ReportType

                    LoadddlGroup()
                    LoadddlCompany()

                    Dim AppPath As String = DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
                    hlDownloadReportFile.Visible = True
                    'hlDownloadReportFile.NavigateUrl = AppPath & "/DownloadFile.aspx?fileid=" & objReport.ReportID.ToString & "&FileType=0"
                    hlDownloadReportFile.NavigateUrl = "/DownloadFile.aspx?fileid=" & objReport.ReportID.ToString().EncryptString() & "&FileType=0"



                End If

            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "MIS Reports Management")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        txtReportName.Text = ""
        rbtnReportTypeList.ClearSelection()
        ddlUserType.ClearSelection()
        ChkBoxListforUsers.Items.Clear()
        ChkBoxListforUsers.DataSource = Nothing
        LoadddlGroup()
        LoadddlCompany()
        tblGroup.Style.Add("display", "none")
        tblUsers.Style.Add("display", "none")
    End Sub

#End Region

    Private Function SaveReport() As Boolean
        Dim Result As Boolean = True
        Dim AppPath As String = ICUtilities.GetSettingValue("PhysicalApplicationPath").ToString & "MISReports\"
        If Not File.Exists(fupReportFile.FileName) Then
            fupReportFile.SaveAs(AppPath + fupReportFile.FileName)
        End If
        Dim settings As New System.Xml.XmlReaderSettings()

        settings.IgnoreWhitespace = True
        Dim xmlReader As System.Xml.XmlReader = System.Xml.XmlReader.Create(AppPath & fupReportFile.FileName, settings)
        Dim xmlSerializer As New Telerik.Reporting.XmlSerialization.ReportXmlSerializer()
        Dim report As Telerik.Reporting.Report = DirectCast(xmlSerializer.Deserialize(xmlReader), Telerik.Reporting.Report)
        xmlReader.Close()

        If report.ReportParameters.Count = 14 Then



            'Dim _ClientName As String = report.ReportParameters.Item(0).Name
            Dim _CompanyCode As String = report.ReportParameters.Item(0).Name
            Dim _ProductTypeCode As String = report.ReportParameters.Item(1).Name
            Dim _FileBatchNo As String = report.ReportParameters.Item(2).Name
            Dim _GroupCode As String = report.ReportParameters.Item(3).Name
            Dim _ClientAccountPaymentNature As String = report.ReportParameters.Item(4).Name
            Dim _StatusCode As String = report.ReportParameters.Item(5).Name
            Dim _ValueDateTo As String = report.ReportParameters.Item(6).Name
            Dim _ValueDateFrom As String = report.ReportParameters.Item(7).Name
            Dim _PrintDateTo As String = report.ReportParameters(8).Name
            Dim _PrintDateFrom As String = report.ReportParameters(9).Name
            Dim _CreateDateTo As String = report.ReportParameters(10).Name
            Dim _CreateDateFrom As String = report.ReportParameters(11).Name
            Dim _ClearingDateTo As String = report.ReportParameters(12).Name
            Dim _ClearingDateFrom As String = report.ReportParameters(13).Name
            '  Dim _AccountNo As String = report.ReportParameters(14).Name


            Dim _ParametersCount As Integer = 0
            Dim MissingParameters As String = ""
            'If _ClientName = "ClientName" Then
            '    _ParametersCount = _ParametersCount + 1
            'Else
            '    MissingParameters += "ClientName ,"
            'End If

            If _ProductTypeCode = "ProductTypeCode" Then
                _ParametersCount = _ParametersCount + 1
            Else
                MissingParameters += "ProductTypeCode ,"
            End If

            If _FileBatchNo = "FileBatchNo" Then
                _ParametersCount = _ParametersCount + 1
            Else
                MissingParameters += "FileBatchNo ,"
            End If

            If _CompanyCode = "CompanyCode" Then
                _ParametersCount = _ParametersCount + 1
            Else
                MissingParameters += "CompanyCode ,"
            End If

            If _GroupCode = "GroupCode" Then
                _ParametersCount = _ParametersCount + 1
            Else
                MissingParameters += "GroupCode ,"
            End If

            If _ClientAccountPaymentNature = "PaymentNatureCode" Then
                _ParametersCount = _ParametersCount + 1
            Else
                MissingParameters += "PaymentNatureCode ,"
            End If

            If _StatusCode = "Status" Then
                _ParametersCount = _ParametersCount + 1
            Else
                MissingParameters += "Status ,"
            End If

            If _ValueDateTo = "ValueDateTo" Then
                _ParametersCount = _ParametersCount + 1
            Else
                MissingParameters += "ValueDateTo ,"
            End If

            If _ValueDateFrom = "ValueDateFrom" Then
                _ParametersCount = _ParametersCount + 1
            Else
                MissingParameters += "ValueDateFrom ,"
            End If

            If _PrintDateTo = "PrintDateTo" Then
                _ParametersCount = _ParametersCount + 1
            Else
                MissingParameters += "PrintDateTo ,"
            End If

            If _PrintDateFrom = "PrintDateFrom" Then
                _ParametersCount = _ParametersCount + 1
            Else
                MissingParameters += "PrintDateFrom ,"
            End If


            If _CreateDateTo = "CreateDateTo" Then
                _ParametersCount = _ParametersCount + 1
            Else
                MissingParameters += "CreateDateTo ,"
            End If

            If _CreateDateFrom = "CreateDateFrom" Then
                _ParametersCount = _ParametersCount + 1
            Else
                MissingParameters += "CreateDateFrom ,"
            End If

            If _ClearingDateTo = "ClearingDateTo" Then
                _ParametersCount = _ParametersCount + 1
            Else
                MissingParameters += "ClearingDateTo ,"
            End If

            If _ClearingDateFrom = "ClearingDateFrom" Then
                _ParametersCount = _ParametersCount + 1
            Else
                MissingParameters += "ClearingDateFrom ,"
            End If
         
            If MissingParameters <> "" Then
                MissingParameters = MissingParameters.Remove(MissingParameters.Length - 1, 1)
            End If


            If _ParametersCount <> 14 Then
                UIUtilities.ShowDialog(Me, "Save MIS Report", "Report could not save because " + MissingParameters + "Parameter(s) is missing !", ICBO.IC.Dialogmessagetype.Failure)
                File.Delete(AppPath + fupReportFile.FileName)
                Result = False
                Return Result
                Exit Function
            End If

        Else
            Result = False
            UIUtilities.ShowDialog(Me, "Save MIS Report", "Report could not save please fill the necessary Parameters.", ICBO.IC.Dialogmessagetype.Failure)
            File.Delete(AppPath + fupReportFile.PostedFile.FileName)
            Return Result
            Exit Function
        End If

        File.Delete(AppPath + fupReportFile.FileName)
        Return Result
    End Function
#Region "Button Events"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim objReport As New ICMISReports
                Dim objReportUsers As New ICMISReportUsers
                Dim CurrentAt As String = ""
                Dim lit As New ListItem
                Dim str As String = System.IO.Path.GetExtension(fupReportFile.PostedFile.FileName)
                Dim ALCurrentUsers As New ArrayList
                objReport.es.Connection.CommandTimeout = 3600
                objReportUsers.es.Connection.CommandTimeout = 3600
                'Transaction Report Reader By James 

                If rbtnReportTypeList.SelectedIndex = 1 Then

                    If ReportID = "0" Then
                        If SaveReport() = False Then
                            Exit Sub
                        End If

                    Else
                        If fupReportFile.HasFile = True Then
                            If SaveReport() = False Then
                                Exit Sub
                            End If
                        End If




                    End If
                End If

                '==================================================

                If ReportID = "0" Then

                    If str = ".trdx" Then
                        Dim imageBytes(fupReportFile.PostedFile.InputStream.Length) As Byte
                        fupReportFile.PostedFile.InputStream.Read(imageBytes, 0, imageBytes.Length)
                        objReport.ReportFileData = imageBytes


                        objReport.ReportName = txtReportName.Text.ToString()
                        objReport.ReportFileSize = fupReportFile.PostedFile.ContentLength
                        objReport.ReportFileType = fupReportFile.PostedFile.ContentType
                        objReport.ReportType = rbtnReportTypeList.SelectedItem.ToString()


                        objReport.CreatedBy = Me.UserId
                        objReport.CreatedDate = Date.Now
                        objReport.Creater = Me.UserId
                        objReport.CreationDate = Date.Now
                        If CheckDuplicateName(objReport) = False Then
                            ReportID = ICMISReportController.AddMISReport(objReport, CurrentAt, False, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                            For Each lit In ChkBoxListforUsers.Items
                                If lit.Selected = True Then
                                    objReportUsers = New ICMISReportUsers
                                    objReportUsers.ReportID = ReportID
                                    objReportUsers.CreatedBy = Me.UserId
                                    objReportUsers.CreatedDate = Date.Now
                                    objReportUsers.Creater = Me.UserId
                                    objReportUsers.CreationDate = Date.Now
                                    objReportUsers.UserID = lit.Value
                                    CurrentAt = "MIS Report Users" & "[ReportID:  " & objReportUsers.ReportID.ToString() & " ; ReportName:  " & txtReportName.Text.ToString() & "] is tagged with User [" & " UserID:  " & lit.Value.ToString() & " ; UserName:  " & lit.Text.ToString() & "]"
                                    ICMISReportUsersController.AddMISReportUsers(objReportUsers, CurrentAt, False, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                End If
                            Next
                            UIUtilities.ShowDialog(Me, "Save MIS Report", "MIS Report added successfully.", ICBO.IC.Dialogmessagetype.Success)
                        Else
                            UIUtilities.ShowDialog(Me, "Save MIS Report", "Can not add duplicate Report Name.", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If

                    Else
                        UIUtilities.ShowDialog(Me, "Save MIS Report", "Please select the .trdx file.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If

                Else
                    Dim objReportUpdate As New ICMISReports
                    objReportUpdate.es.Connection.CommandTimeout = 3600
                    If objReportUpdate.LoadByPrimaryKey(ReportID.ToString()) Then

                        objReportUpdate.ReportName = txtReportName.Text.ToString()
                        objReportUpdate.ReportType = rbtnReportTypeList.SelectedItem.ToString()
                        objReportUpdate.CreatedBy = Me.UserId
                        objReportUpdate.CreatedDate = Date.Now
                        If fupReportFile.HasFile = True Then

                            If Not str = ".trdx" Then
                                UIUtilities.ShowDialog(Me, "Save MIS Report", "Please select the .trdx file.", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If



                            Dim imageBytes(fupReportFile.PostedFile.InputStream.Length) As Byte
                            fupReportFile.PostedFile.InputStream.Read(imageBytes, 0, imageBytes.Length)
                            objReportUpdate.ReportFileSize = fupReportFile.PostedFile.ContentLength
                            objReportUpdate.ReportFileType = fupReportFile.PostedFile.ContentType
                            objReportUpdate.ReportFileData = imageBytes
                        End If

                        CurrentAt = "MISReport : Cuurent Values [Code:  " & objReportUpdate.ReportID.ToString() & " ; Name:  " & objReportUpdate.ReportName.ToString() & " ; ReportFileSize:  " & objReportUpdate.ReportFileSize.ToString() & " ; ReportFileType:  " & objReportUpdate.ReportFileType.ToString() & " ; ReportType:  " & objReportUpdate.ReportType.ToString() & "]"
                        ICMISReportController.AddMISReport(objReportUpdate, CurrentAt, True, Me.UserId.ToString(), Me.UserInfo.Username.ToString())

                        For Each lit In ChkBoxListforUsers.Items
                            If lit.Selected = True Then
                                ALCurrentUsers.Add(lit.Value.ToString)
                                objReportUsers = New ICMISReportUsers
                                objReportUsers.ReportID = ReportID
                                objReportUsers.CreatedBy = Me.UserId
                                objReportUsers.CreatedDate = Date.Now
                                objReportUsers.Creater = Me.UserId
                                objReportUsers.CreationDate = Date.Now
                                objReportUsers.UserID = lit.Value
                                CurrentAt = "MIS Report Users" & "[ReportID:  " & objReportUsers.ReportID.ToString() & " ; ReportName:  " & txtReportName.Text.ToString() & "] is tagged with User [" & " UserID:  " & lit.Value.ToString() & " ; UserName:  " & lit.Text.ToString() & "]"
                                ICMISReportUsersController.AddMISReportUsers(objReportUsers, CurrentAt, False, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                            End If
                        Next
                        ICMISReportUsersController.UpdateTaggesReportUsers(ReportID, Me.UserId.ToString, Me.UserInfo.Username.ToString, ALCurrentUsers)
                        UIUtilities.ShowDialog(Me, "Save MIS Report", "MIS Report updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Clear()
                    End If

                End If

                Clear()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Private Function CheckDuplicateName(ByVal ReportName As ICMISReports) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim objReport As New ICMISReports
            Dim collobjReport As New ICMISReportsCollection

            objReport.es.Connection.CommandTimeout = 3600
            collobjReport.es.Connection.CommandTimeout = 3600

            collobjReport.Query.Where(collobjReport.Query.ReportName.ToLower.Trim = ReportName.ReportName.ToLower.Trim)
            If collobjReport.Query.Load() Then
                rslt = True
            End If

            Return rslt

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click

        Try
                Dim rowgvAssignedUsersToReport As GridDataItem
                Dim chkSelect As CheckBox
                Dim ReportUserID As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0
                Dim objReports As New ICMISReports
                Dim objReportUsers As New ICMISReportUsers

                If CheckgvAssignedUsersToReportForProcessAll() = True Then
                    For Each rowgvAssignedUsersToReport In gvAssignedUsersToReport.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvAssignedUsersToReport.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            ReportUserID = Nothing
                            ReportUserID = rowgvAssignedUsersToReport("ReportUserID").Text.ToString()
                            objReportUsers.es.Connection.CommandTimeout = 3600
                            If objReportUsers.LoadByPrimaryKey(ReportUserID.ToString()) Then
                                If objReportUsers.IsApproved Then
                                    FailCount = FailCount + 1
                                    Continue For
                                Else
                                    If Me.UserInfo.IsSuperUser = False Then
                                        If objReportUsers.CreatedBy = Me.UserId Then
                                            FailCount = FailCount + 1
                                            Continue For
                                        Else
                                            Try
                                                ICMISReportUsersController.ApproveReportUsers(ReportUserID.ToString(), chkSelect.Checked, Me.UserId, Me.UserInfo.Username)
                                                PassCount = PassCount + 1
                                                Continue For
                                            Catch ex As Exception
                                                FailCount = FailCount + 1
                                                Continue For
                                            End Try
                                        End If
                                    Else
                                        Try
                                            ICMISReportUsersController.ApproveReportUsers(ReportUserID.ToString(), chkSelect.Checked, Me.UserId, Me.UserInfo.Username)
                                            PassCount = PassCount + 1
                                            Continue For
                                        Catch ex As Exception
                                            FailCount = FailCount + 1
                                            Continue For
                                        End Try
                                    End If
                                End If
                            End If
                        End If

                    Next
                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Approve Report Users", "[" & FailCount.ToString() & "] Report Users can not be approved due to following reasons : <br /> 1. Report Users is approved.<br /> 2. Report Users must be approved by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Approve Report Users", "[" & PassCount.ToString() & "] Report Users approved successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Approve Report Users", "[" & PassCount.ToString() & "] Report Users approved successfully. [" & FailCount.ToString() & "] Report Users can not be approved due to following reasons : <br /> 1. Report Users is approved.<br /> 2. Report Users must be approved by the user other than the maker.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Approve Report Users", "Please select atleast one(1) Report User.", ICBO.IC.Dialogmessagetype.Warning)
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try


    End Sub
    
    Private Function CheckgvAssignedUsersToReportForProcessAll() As Boolean
        Try
            Dim rowgvAssignedUsersToReport As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvAssignedUsersToReport In gvAssignedUsersToReport.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvAssignedUsersToReport.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim rowgvAssignedUsersToReport As GridDataItem
            Dim chkSelect As CheckBox
            Dim ReportUserID As String = ""
            Dim PassCount As Integer = 0
            Dim FailCount As Integer = 0

            If CheckgvAssignedUsersToReportForProcessAll() = True Then
                For Each rowgvAssignedUsersToReport In gvAssignedUsersToReport.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowgvAssignedUsersToReport.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        ReportUserID = rowgvAssignedUsersToReport("ReportUserID").Text.ToString()
                        Dim objReportUserID As New ICMISReportUsers
                        objReportUserID.es.Connection.CommandTimeout = 3600
                        If objReportUserID.LoadByPrimaryKey(ReportUserID.ToString()) Then
                            If objReportUserID.IsApproved Then
                                FailCount = FailCount + 1
                            Else
                                Try
                                    ICMISReportUsersController.DeleteReportUsers(ReportUserID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                    PassCount = PassCount + 1
                                Catch ex As Exception
                                    If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                        FailCount = FailCount + 1
                                    End If
                                End Try
                            End If
                        End If
                    End If

                Next

                If PassCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Delete MIS Report Users", "[" & FailCount.ToString() & "] MIS Report Users can not be deleted due to following reasons : <br /> 1. MIS Report Users is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    Exit Sub
                End If
                If FailCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Delete MIS Report Users", "[" & PassCount.ToString() & "]  MIS Report Users deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    Exit Sub
                End If
                UIUtilities.ShowDialog(Me, "Delete MIS Report Users", "[" & PassCount.ToString() & "] MIS Report Users deleted successfully. [" & FailCount.ToString() & "] MIS Report Users can not be deleted due to following reasons : <br /> 1. MIS Report Users is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
            Else
                UIUtilities.ShowDialog(Me, "Delete MIS Report Users", "Please select atleast one(1) MIS Report Users.", ICBO.IC.Dialogmessagetype.Warning)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub

#End Region

#Region "Drop Down"

    Private Sub LoadChkBoxListforUsers()
        Try
            If ReportID = "0" Then
                ChkBoxListforUsers.Items.Clear()
                ChkBoxListforUsers.DataSource = ICUserController.GetAllUsersByUserTypeAndReportID(ddlUserType.SelectedValue.ToString(), 0, ddlCompany.SelectedValue.ToString)
            Else
                ChkBoxListforUsers.Items.Clear()
                ChkBoxListforUsers.DataSource = ICUserController.GetAllUsersByUserTypeAndReportID(ddlUserType.SelectedValue.ToString(), ReportID, ddlCompany.SelectedValue.ToString)
            End If
            ChkBoxListforUsers.DataTextField = "UserName"
            ChkBoxListforUsers.DataValueField = "UserID"
            ChkBoxListforUsers.DataBind()
            If ChkBoxListforUsers.Items.Count > 0 Then

                tblUsers.Style.Remove("display")
            Else
                ChkBoxListforUsers.Items.Clear()
                ChkBoxListforUsers.DataSource = Nothing
                tblUsers.Style.Add("display", "none")
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICUserController.GetClient()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetCompanyActiveAndApproveByGroupCode(ddlGroup.SelectedValue.ToString)
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub ddlUserType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUserType.SelectedIndexChanged
        If ddlUserType.SelectedValue = "Bank User" Then
            LoadChkBoxListforUsers()
            tblGroup.Style.Add("display", "none")
        ElseIf ddlUserType.SelectedValue = "Client User" Then
            LoadddlGroup()
            LoadddlCompany()
            ChkBoxListforUsers.DataSource = Nothing
            tblGroup.Style.Remove("display")
            tblUsers.Style.Add("display", "none")
        Else
            LoadddlGroup()
            LoadddlCompany()
            ChkBoxListforUsers.DataSource = Nothing
            tblGroup.Style.Add("display", "none")
            tblUsers.Style.Add("display", "none")
        End If

    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        ChkBoxListforUsers.DataSource = Nothing
        tblUsers.Style.Add("display", "none")
        LoadddlCompany()
    End Sub

#End Region


#Region "Grid View"

    Private Sub LoadgvAssignedUsersToReport(ByVal IsBind As Boolean)
        Try
            ICMISReportUsersController.GetgvAssignedUsersToReport(ReportID.ToString(), gvAssignedUsersToReport.CurrentPageIndex + 1, gvAssignedUsersToReport.PageSize, gvAssignedUsersToReport, IsBind)

            If gvAssignedUsersToReport.Items.Count > 0 Then
                gvAssignedUsersToReport.Visible = True
                btnApprove.Visible = CBool(htRights("Approve"))
                btnDelete.Visible = CBool(htRights("Delete"))
                gvAssignedUsersToReport.Columns(9).Visible = CBool(htRights("Delete"))
              
            Else
                gvAssignedUsersToReport.Visible = False
                btnApprove.Visible = False
                btnDelete.Visible = False
                '    rfvUserType.Enabled = True

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Protected Sub gvAssignedUsersToReport_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAssignedUsersToReport.NeedDataSource
        LoadgvAssignedUsersToReport(False)
    End Sub

    Protected Sub gvAssignedUsersToReport_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvAssignedUsersToReport.ItemCommand
        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    Dim objReportUser As New ICMISReportUsers

                    objReportUser.es.Connection.CommandTimeout = 3600

                    objReportUser.LoadByPrimaryKey(e.CommandArgument.ToString())
                    If objReportUser.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                        If objReportUser.IsApproved Then
                            UIUtilities.ShowDialog(Me, "Warning", "MIS Report is approved and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
                        Else
                            ICMISReportUsersController.DeleteReportUsers(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                            UIUtilities.ShowDialog(Me, "Delete MIS Report", "MIS Report deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                            LoadChkBoxListforUsers()
                            LoadgvAssignedUsersToReport(True)

                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Deleted MIS Report", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAssignedUsersToReport_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAssignedUsersToReport.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelect"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAssignedUsersToReport.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub gvAssignedUsersToReport_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAssignedUsersToReport.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAssignedUsersToReport.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

   
    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            If ddlCompany.SelectedValue.ToString <> "0" Then
                LoadChkBoxListforUsers()
            Else
                ChkBoxListforUsers.DataSource = Nothing
                tblUsers.Style.Add("display", "none")

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
