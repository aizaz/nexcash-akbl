﻿Imports ICBO
Imports ICBO.IC
'Imports Telerik.Reporting.Processing
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports System.Messaging
'Imports Microsoft.VisualBasic
'Imports System.Net
'Imports System.Data
'Imports System.Security.Cryptography.X509Certificates
'Imports System.Net.Security



Partial Class DesktopModules_PDC_ViewPdc
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Private ChequeId As String
    Dim dtDa As DataTable
    Private htRights As Hashtable

    'CREATED NEW MODULE FOR POST DATED CHEQUES MANAGEMENT BY JAWWAD 

#Region "Page Load"

    Protected Sub DesktopModules_PDC_ViewPdc_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlCheque()
                'LoadddlGroup()
                'LoadddlCompany()
                LoadgvPdc(True)
                btnSavePdc.Visible = CBool(htRights("Add"))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Post Dated Cheques")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Buttons"

    'link to add page
    Protected Sub btnSavePdc_Click(sender As Object, e As EventArgs) Handles btnSavePdc.Click

        'link for add page
        If Page.IsValid Then
            Response.Redirect(NavigateURL("Add", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If

        'creata a dummy function to check notification 


        'ICPdcController.AutoSendEmailForPDCClearingUser()





    End Sub

    ' created to delete cheque from grid view(single or bulk)
    Protected Sub btnDeletePdc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeletePdc.Click
        If Page.IsValid Then
            Try
                Dim rowgvPdc As GridDataItem
                Dim chkSelect As CheckBox
                Dim ChequeId As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0

                If CheckgvPdcForProcessAll() = True Then
                    For Each rowgvPdc In gvPdc.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvPdc.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            ChequeId = rowgvPdc("ChequeId").Text.ToString()
                            Dim icPdc As New ICPdc
                            icPdc.es.Connection.CommandTimeout = 3600
                            If icPdc.LoadByPrimaryKey(ChequeId.ToString()) Then
                                'If icPdc.IsApproved Then
                                '    FailCount = FailCount + 1
                                'Else
                                Try
                                    ICPdcController.DeletePdc(ChequeId.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                    PassCount = PassCount + 1
                                Catch ex As Exception
                                    If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                        FailCount = FailCount + 1
                                    End If
                                End Try
                                'End If
                            End If


                        End If

                    Next

                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete PDC", "[" & FailCount.ToString() & "] Cheques can not be deleted due to following reasones : <br /> 1. Cheques is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete PDC", "[" & PassCount.ToString() & "] Cheques deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        LoadddlCheque()
                        Exit Sub
                    End If

                    UIUtilities.ShowDialog(Me, "Delete PDC", "[" & PassCount.ToString() & "] Cheques deleted successfully. [" & FailCount.ToString() & "] Cheques can not be deleted due to following reasons : <br /> 1. Cheques is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    LoadddlCheque()
                Else
                    UIUtilities.ShowDialog(Me, "Delete PDC", "Please select atleast one(1) Cheque.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If


    End Sub

    'created function for delete pdc
    Private Function CheckgvPdcForProcessAll() As Boolean
        Try
            Dim rowgvPdc As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvPdc In gvPdc.Items
                chkSelect = New CheckBox

                chkSelect = DirectCast(rowgvPdc.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
#End Region

#Region "Load"


    'created to load grid view
    Private Sub LoadgvPdc(ByVal IsBind As Boolean)
        Try
            ICPdcController.GetPdcgv(ddlCheque.SelectedValue.ToString(), gvPdc.CurrentPageIndex + 1, gvPdc.PageSize, gvPdc, IsBind)
            If gvPdc.Items.Count > 0 Then
                lblPdcRNF.Visible = False
                gvPdc.Visible = True

                gvPdc.Columns(6).Visible = CBool(htRights("Delete"))
                btnDeletePdc.Visible = CBool(htRights("Delete"))
            Else
                lblPdcRNF.Visible = True
                gvPdc.Visible = False
                btnDeletePdc.Visible = False

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    'created to drop down chq select index change
    Private Sub LoadCheque(ByVal IsBind As Boolean)
        Try
            ICPdcController.GetgvPdc(ddlCheque.SelectedValue.ToString(), Me.gvPdc.CurrentPageIndex + 1, Me.gvPdc.PageSize, Me.gvPdc, IsBind)


            If gvPdc.Items.Count > 0 Then
                gvPdc.Visible = True
                btnDeletePdc.Visible = CBool(htRights("Delete"))
                'btnApproveCountries.Visible = CBool(htRights("Approve"))
                gvPdc.Columns(6).Visible = CBool(htRights("Delete"))

            Else
                gvPdc.Visible = False
                'btnApproveCountries.Visible = False
                btnDeletePdc.Visible = False

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    'created to load cheque in drop down
    Private Sub LoadddlCheque()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- ALL --"
            ddlCheque.Items.Clear()
            ddlCheque.Items.Add(lit)
            ddlCheque.AppendDataBoundItems = True
            ddlCheque.DataSource = ICPdcController.GetPdc()
            ddlCheque.DataTextField = "ChequeNo"
            ddlCheque.DataValueField = "ChequeNo"
            ddlCheque.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

    'created for drop down select index change
    Protected Sub ddlCheque_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCheque.SelectedIndexChanged

        Try
            LoadCheque(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub


#Region "GVPDC"

    'created for grid view functionality

    Protected Sub gvPdc_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles gvPdc.ItemCommand

        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    Dim icPdc As New ICPdc
                    icPdc.es.Connection.CommandTimeout = 3600
                    If icPdc.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                        'If icPdc.IsApproved Then
                        '    UIUtilities.ShowDialog(Me, "Warning", "Country is approved and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
                        'Else
                        ICPdcController.DeletePdc(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        UIUtilities.ShowDialog(Me, "Delete PDC", "Cheque deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                        LoadCheque(True)
                        LoadddlCheque()
                        ' End If
                    End If
                    'ElseIf e.CommandName = "ShowProvince" Then
                    '    hfCountryCode.Value = e.CommandArgument.ToString()
                    '    Dim frcCountry As New ICPdc
                    '    frcCountry.es.Connection.CommandTimeout = 3600
                    '    frcCountry.LoadByPrimaryKey(e.CommandArgument.ToString())
                    '    If frcCountry.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                    '        lblProvinceListHeader.Text = "Province List of " & frcCountry.CountryName.ToString()
                    '    Else
                    '        lblProvinceListHeader.Text = "Province List"
                    '    End If
                    '    LoadProvinces(hfCountryCode.Value.ToString(), True)
                    '    LoadCity(0, True)
                    '    lblCityListHeader.Visible = False
                    '    lblCityRNF.Visible = False
                End If
            End If

        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Delete PDC", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Protected Sub gvPdc_ItemCreated(sender As Object, e As GridItemEventArgs) Handles gvPdc.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvPdc.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvPdc_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles gvPdc.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvPdc.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub gvPdc_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles gvPdc.NeedDataSource
        LoadgvPdc(False)
    End Sub

    Protected Sub gvPdc_PageIndexChanged(sender As Object, e As GridPageChangedEventArgs) Handles gvPdc.PageIndexChanged
        gvPdc.CurrentPageIndex = e.NewPageIndex
    End Sub

#End Region


  


End Class

