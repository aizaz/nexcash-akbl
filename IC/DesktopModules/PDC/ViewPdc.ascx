﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewPdc.ascx.vb"
    Inherits="DesktopModules_PDC_ViewPdc" %>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
            }
        }
    }
</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnSavePdc" runat="server" Text="Add Cheque" CssClass="btn" />
                         <%--<asp:Button ID="btnAddBulkPdc" runat="server" Text="Add Bulk Cheque" CssClass="btn" />--%>
                        <asp:HiddenField ID="hfBankCode" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <asp:Label ID="lblPdcList" runat="server" Text="Cheque List" CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                 <tr>
                    <td align="left" valign="top">
                    <table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblPdc1" runat="server" Text="Select Cheque No" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCheque" runat="server"  
                CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem Value="0">-- All --</asp:ListItem>
            </asp:DropDownList>
        </td>
       <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="center" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
            <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
                        <asp:Label ID="lblPdcRNF" runat="server" Text="Record Not Found." CssClass="headingblue"
                            Visible="false"></asp:Label>
                </td>
    </tr>
    </table> 
                    </td>
                </tr>   
                 <tr>
                    <td align="left" valign="top">
                        <telerik:RadGrid ID="gvPdc" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                            <ClientSettings>
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Select" DataField="IsApprove">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>    
                                    <telerik:GridBoundColumn DataField="ChequeId" HeaderText="ID" SortExpression="ChequeId">
                                    </telerik:GridBoundColumn>                               
                                    <telerik:GridBoundColumn DataField="ChequeNo" HeaderText="Cheque No" SortExpression="ChequeNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PostingDate" HeaderText="Posting Date" SortExpression="PostingDate" 
                                        DataFormatString="{0:dd-MMM-yyyy}"  HtmlEncode="False">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TransactionType" HeaderText="Transaction Type" SortExpression="TransactionType">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ClearingType" HeaderText="Clearing Type" SortExpression="ClearingType">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn>
                                       <HeaderTemplate>
                                            <asp:Label ID="Edit" runat="server" Text="Edit"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                           <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("Add", "&mid=" & Me.ModuleId & "&id=" & Eval("ChequeId"))%>'
                                            ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>                                   
                                    <telerik:GridTemplateColumn>
                                         <HeaderTemplate>
                                            <asp:Label ID="Delete" runat="server" Text="Delete"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                           <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("ChequeId")%>'
                                            CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                            ToolTip="Delete" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />

<FilterMenu EnableImageSprites="False"></FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <%--<asp:Button ID="btnApprovePdc" runat="server" Text="Approve Cheque" CssClass="btn" />--%>
                    &nbsp;<asp:Button ID="btnDeletePdc" runat="server"  Text="Delete Cheque"  
                        OnClientClick="javascript: return con();"  CssClass="btn" />
                    </td>
                </tr>  
                <tr>
                <td align="center" valign="top">

                    </td>
                </tr>             
            </table>
        </td>
    </tr>
</table>
