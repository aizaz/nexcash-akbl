﻿Imports ICBO
Imports ICBO.IC
Imports Telerik.Web.UI
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data
Imports Telerik.Reporting.Processing
Imports Microsoft.VisualBasic
Imports System.Net
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security

Partial Class DesktopModules_PDC_AddPdc
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private Add As String
    Private ChequeId As String
    Private htRights As Hashtable
    Dim FundTransfer As String = Nothing
    Dim Clearing As String = Nothing

#Region "Page Load"

    Protected Sub DesktopModules_PDC_AddPdc_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            ChequeId = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                If ChequeId.ToString() = "0" Then
                    Clear()
                    lblPageHeader.Text = "Add Post Dated Cheque"
                    btnSave.Text = "Save"
                    RadioButtonList2.Visible = False
                    lblClearingType.Visible = False
                    Label17.Visible = False
                    RequiredFieldValidator3.Enabled = False
                    btnSave.Visible = CBool(htRights("Add"))
                    LoadddlGroup()
                    LoadddlCompany()
                    LoadddlAccounts()
                    LoadddlBank()
                    LoadddlCity()
                    radDPPresentmentDate.MinDate = Date.Now.AddDays(1)
                Else
                    lblPageHeader.Text = "Edit Post Dated Cheque"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    Dim cPdc As New ICPdc
                    Dim objICgrp As New ICGroup
                    Dim objICCompany As New ICCompany
                    Dim objICAccounts As New ICAccounts
                    Dim objICBank As New ICBank
                    Dim objICCity As New ICCity
                    cPdc.es.Connection.CommandTimeout = 3600
                    radDPPresentmentDate.MinDate = Date.Now.AddDays(1)

                    If cPdc.LoadByPrimaryKey(ChequeId) Then
                        If objICAccounts.LoadByPrimaryKey(cPdc.AccountNumber, cPdc.BranchCode, cPdc.Currency) Then
                            If objICCompany.LoadByPrimaryKey(objICAccounts.CompanyCode) Then
                                If objICgrp.LoadByPrimaryKey(objICCompany.GroupCode) Then
                                    LoadddlGroup()
                                    ddlGroup.SelectedValue = objICgrp.GroupCode.ToString()
                                    LoadddlCompany()
                                    ddlCompany.SelectedValue = objICCompany.CompanyCode.ToString()
                                    LoadddlAccounts()
                                    ddlAccount.SelectedValue = cPdc.AccountNumber.ToString() & "-" & cPdc.BranchCode.ToString() & "-" & cPdc.Currency.ToString()
                                End If
                            End If
                        End If
                        LoadddlBank()
                        ddlBank.SelectedValue = cPdc.BankCode
                        LoadddlCity()
                        ddlCity.SelectedValue = cPdc.CityCode
                        txtChqNo.Text = cPdc.ChequeNo.ToString()
                        txtTransactionDetails.Text = cPdc.TransactionDetails.ToString()
                        txtAdditionalDetails.Text = cPdc.AddtionalDetails.ToString()
                        radDPPresentmentDate.SelectedDate = cPdc.Presentment
                        RadioButtonList1.SelectedValue = cPdc.TransactionType
                        RadioButtonList2.SelectedValue = cPdc.ClearingType

                    End If

                    If RadioButtonList1.SelectedValue = "Clearing" Then
                        RadioButtonList2.Visible = True
                        lblClearingType.Visible = True
                        Label17.Visible = True
                        RequiredFieldValidator3.Enabled = True
                    Else
                        RadioButtonList2.Visible = False
                        lblClearingType.Visible = False
                        Label17.Visible = False
                        RequiredFieldValidator3.Enabled = False
                        RadioButtonList2.ClearSelection()



                    End If

                End If
            End If


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try



    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Post Dated Cheques")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub



#End Region

#Region "Load Drop Down"

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccounts()
        Try
            Dim lit As New ListItem
            Dim objAccount As New ICAccounts
            Dim collAccounts As New ICAccountsCollection

            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlAccount.Items.Clear()
            ddlAccount.Items.Add(lit)
            collAccounts = ICAccountsController.GetAllAccountsByCompanyCode(ddlCompany.SelectedValue.ToString())
            For Each objAccount In collAccounts
                lit = New ListItem
                lit.Text = objAccount.AccountTitle.ToString() & " - " & objAccount.AccountNumber.ToString()
                lit.Value = objAccount.AccountNumber.ToString() & "-" & objAccount.BranchCode.ToString() & "-" & objAccount.Currency.ToString()
                ddlAccount.Items.Add(lit)
            Next
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlBank()

        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBank.Items.Clear()
            ddlBank.Items.Add(lit)
            ddlBank.AppendDataBoundItems = True


            ddlBank.DataSource = ICBankController.GetBank()
            ddlBank.DataValueField = "BankCode"
            ddlBank.DataTextField = "BankName"
            ddlBank.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlCity()
        Try
            ddlCity.Items.Clear()
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCity.Items.Add(lit)
            ddlCity.AppendDataBoundItems = True
            ddlCity.DataSource = ICCityController.GetCityActiveAndApprovedCityByCityCode(ddlCity.SelectedValue.ToString())
            ddlCity.DataTextField = "CityName"
            ddlCity.DataValueField = "CityCode"
            ddlCity.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


#End Region

#Region "Select Index Changed"

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged
        LoadddlCompany()
        LoadddlAccounts()
        'LoadddlBank()
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompany.SelectedIndexChanged
        LoadddlAccounts()
        LoadddlCity()
        'LoadddlBank()
    End Sub

    Protected Sub RadioButtonList1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RadioButtonList1.SelectedIndexChanged

        If RadioButtonList1.SelectedValue = "Clearing" Then
            RadioButtonList2.Visible = True
            lblClearingType.Visible = True
            Label17.Visible = True
            RequiredFieldValidator3.Enabled = True
        Else
            RadioButtonList2.Visible = False
            lblClearingType.Visible = False
            Label17.Visible = False
            RequiredFieldValidator3.Enabled = False
            RadioButtonList2.ClearSelection()



        End If



    End Sub

#End Region

#Region "Buttons"

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim cPdc As New ICPdc()
                Dim chkBank As New ICPdc()

                cPdc.es.Connection.CommandTimeout = 3600
                chkBank.es.Connection.CommandTimeout = 3600

                If ChequeId.ToString() <> "0" Then
                    cPdc.LoadByPrimaryKey(ChequeId)
                End If

                cPdc.AccountNumber = ddlAccount.SelectedValue.ToString().Split("-")(0)
                cPdc.BranchCode = ddlAccount.SelectedValue.ToString().Split("-")(1)
                cPdc.Currency = ddlAccount.SelectedValue.ToString().Split("-")(2)
                cPdc.BankCode = ddlBank.SelectedValue
                cPdc.CityCode = ddlCity.SelectedValue
                cPdc.ChequeNo = txtChqNo.Text
                cPdc.Presentment = radDPPresentmentDate.SelectedDate
                cPdc.PostingDate = Date.Now
                cPdc.TransactionDetails = txtTransactionDetails.Text.ToString()
                cPdc.AddtionalDetails = txtAdditionalDetails.Text.ToString()


                If RadioButtonList1.SelectedValue = "Clearing" Then
                    cPdc.TransactionType = "Clearing"
                End If
                If RadioButtonList2.SelectedValue = "Intercity" Then
                    cPdc.ClearingType = "Intercity"
                End If
                If RadioButtonList2.SelectedValue = "Outstation" Then
                    cPdc.ClearingType = "Outstation"
                End If
                If RadioButtonList2.SelectedValue = "Local" Then
                    cPdc.ClearingType = "Local"
                End If

                If RadioButtonList1.SelectedValue = "Fund Transfer" Then
                    cPdc.TransactionType = "Fund Transfer"
                    cPdc.ClearingType = Nothing

                End If

                If ChequeId = "0" Then

                    cPdc.CreateBy = Me.UserId
                    cPdc.CreateDate = Date.Now
                    cPdc.Creater = Me.UserId
                    cPdc.CreationDate = Date.Now
                    If CheckDuplicate(cPdc, False) = False Then

                        ICPdcController.AddPdc(cPdc, False, Me.UserInfo.UserID.ToString, Me.UserInfo.Username)
                        UIUtilities.ShowDialog(Me, "Save PDC", "Cheque Information saved successfully", Dialogmessagetype.Success)
                        Clear()
                    Else
                        UIUtilities.ShowDialog(Me, "Save PDC", "Can not add duplicate Cheque.", ICBO.IC.Dialogmessagetype.Failure)
                    End If
                Else

                    ' cPdc.ChequeId = ChequeId.ToString()
                    'cPdc.LoadByPrimaryKey(ChequeId)
                    cPdc.ChequeId = ChequeId
                    cPdc.CreateBy = Me.UserId
                    cPdc.CreateDate = Date.Now
                    If CheckDuplicate(cPdc, True) = False Then

                        ICPdcController.AddPdc(cPdc, True, Me.UserInfo.UserID.ToString, Me.UserInfo.Username)
                        UIUtilities.ShowDialog(Me, "Update PDC", "Cheque Information updated successfully", Dialogmessagetype.Success, NavigateURL())
                    Else
                        UIUtilities.ShowDialog(Me, "Update PDC", "Can not update duplicate Cheque", ICBO.IC.Dialogmessagetype.Failure)

                    End If

                End If


            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)

            End Try
        End If
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL())
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)

        End Try
    End Sub

#End Region

#Region "Other Function"

    'created a function for checking duplicate chq entry in addition and updation
    Private Function CheckDuplicate(ByVal cPdc As ICPdc, ByVal IsUpdatee As Boolean) As Boolean
        Dim rslt As Boolean = False
        Dim icPdc As New ICPdc
        icPdc.es.Connection.CommandTimeout = 3600
        Dim collPdc As New ICPdcCollection
        collPdc.es.Connection.CommandTimeout = 3600

        Try
            If IsUpdatee = False Then
                collPdc.Query.Where(collPdc.Query.ChequeNo.Like(cPdc.ChequeNo))
            Else
                collPdc.Query.Where(collPdc.Query.ChequeNo.Like(cPdc.ChequeNo) And collPdc.Query.ChequeId <> cPdc.ChequeId)
            End If
            If collPdc.Query.Load() Then
                rslt = True
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Private Sub Clear()

        ddlGroup.ClearSelection()
        ddlCompany.ClearSelection()
        ddlAccount.ClearSelection()
        ddlBank.ClearSelection()
        ddlCity.ClearSelection()
        txtChqNo.Text = ""
        radDPPresentmentDate.Clear()
        RadioButtonList1.ClearSelection()
        RadioButtonList2.ClearSelection()
        txtTransactionDetails.Text = ""
        txtAdditionalDetails.Text = ""




    End Sub

#End Region

End Class
