﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddPdc.ascx.vb"
    Inherits="DesktopModules_PDC_AddPdc" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .style1
    {
        width: 25%;
        height: 23px;
    }
.ctrDropDown{
    }
    .txtbox
    {}
</style>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }

</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server">


<%--    <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior2" EmptyMessage=""
        Type="Number">
    </telerik:NumericTextBoxSetting>--%>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior2" Validation-IsRequired="true"
        ValidationExpression="^[0-9]{1,50}$"  ErrorMessage="Invalid Cheque No"
        EmptyMessage="Enter Cheque No" Validation-ValidationGroup ="PDC">
        <TargetControls>
            <telerik:TargetInput ControlID="txtChqNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>

    <telerik:TextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="false"
        EmptyMessage="Enter Details" ErrorMessage="" Validation-ValidationGroup ="PDC">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTransactionDetails" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehaviorAddress" Validation-IsRequired="false"
        ErrorMessage="" EmptyMessage="Enter Details" Validation-ValidationGroup ="PDC">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAdditionalDetails" />
        </TargetControls>
    </telerik:TextBoxSetting>
<%--     <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehaviorPhone1" ValidationExpression="^\+?[\d- ]{2,15}$"
        Validation-IsRequired="false" ErrorMessage="Invalid Phone Number" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBranchPhone1" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>--%>
    <%--<telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior3" ValidationExpression="^\+?[\d- ]{2,15}$"
        ErrorMessage="Invalid Date" EmptyMessage="" Validation-IsRequired="true">
        <TargetControls>
            <telerik:TargetInput ControlID="" />
        </TargetControls>
        </telerik:RegExpTextBoxSetting>
         <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehaviorEmail" Validation-IsRequired="True"
        EmptyMessage="" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.%]\w+)*\.\w+([-.]\w+)*$"
        ErrorMessage="Invalid Email">
        <TargetControls>
            <telerik:TargetInput ControlID="RadioButtonList1" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
     
         <telerik:TextBoxSetting BehaviorID="RagExpBehaviorOffName" Validation-IsRequired="True"
        ErrorMessage="Invalid Office Name" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtOfficeName" />
        </TargetControls>
    </telerik:TextBoxSetting>--%>
    <%-- <telerik:TextBoxSetting BehaviorID="RagExpBehaviorDescription"
         EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBranchDescrption" />
        </TargetControls>
    </telerik:TextBoxSetting>--%>
     <%--   <telerik:RegExpTextBoxSetting BehaviorID="ddClearingAccntNo" Validation-IsRequired="false"
        ValidationExpression="^[a-zA-Z0-9.-_/]{1,15}$" ErrorMessage="Invalid Account No"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDDClearingAccNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
        <telerik:RegExpTextBoxSetting BehaviorID="poClearingAccntNo" Validation-IsRequired="false"
        ValidationExpression="^[a-zA-Z0-9.-_/]{1,15}$" ErrorMessage="Invalid Account No"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPOClearingAccNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
      <telerik:TextBoxSetting BehaviorID="ddaccnttitle" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Invalid Account Title">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDDAcntTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>--%>

<%--          <telerik:TextBoxSetting BehaviorID="poaccnttitle" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Invalid Account Title">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPOAccntTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>--%>
 
</telerik:RadInputManager>
<%--    <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior2" EmptyMessage=""
        Type="Number">
    </telerik:NumericTextBoxSetting>--%>


<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label8" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label18" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlGroup" runat="server" ControlToValidate="ddlGroup"
                Display="Dynamic" ErrorMessage="Please select Group" InitialValue="0" SetFocusOnError="True" ValidationGroup ="PDC"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;<asp:RequiredFieldValidator ID="rfvddlCompany0" runat="server" ControlToValidate="ddlCompany"
                Display="Dynamic" ErrorMessage="Please select Company" InitialValue="0" SetFocusOnError="True" ValidationGroup ="PDC"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 33%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAccount" runat="server" Text="Select Account" CssClass="lbl"></asp:Label><asp:Label
                ID="Label19" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBankCode" runat="server" Text="Select Bank" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlAccount" runat="server" CssClass="dropdown"  AutoPostBack="true" >
            </asp:DropDownList>
            </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlBank" runat="server" 
                CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlAccount" runat="server" ControlToValidate="ddlAccount"
                Display="Dynamic" ErrorMessage="Please select Account" InitialValue="0" SetFocusOnError="True" ValidationGroup ="PDC"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvBank" runat="server" 
                ControlToValidate="ddlBank" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Bank" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0" ValidationGroup ="PDC"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyCity" runat="server" Text="Select City" 
                CssClass="lbl"></asp:Label><asp:Label
                ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlCity" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ControlToValidate="ddlCity" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select City" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0" ValidationGroup ="PDC"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            </td>
        <td align="left" valign="top" class="style1">
            </td>
        <td align="left" valign="top" class="style1">
            </td>
        <td align="left" valign="top" class="style1">
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblChqNo" runat="server" Text="Cheque No" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label11" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblRadDatePicker" runat="server" Text="Presentment Date" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label6" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        <%--     <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehaviorPhone1" ValidationExpression="^\+?[\d- ]{2,15}$"
        Validation-IsRequired="false" ErrorMessage="Invalid Phone Number" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBranchPhone1" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>--%>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtChqNo" runat="server" CssClass="txtbox" Height ="20px">
            </asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
             <telerik:RadDatePicker ID="radDPPresentmentDate" runat="server" MinDate="1900-01-01" AutoPostBack="true" Height="25px" Width="213px" DateInput-EmptyMessage="Please select date">
                            <Calendar ID="Calendar1" RangeMinDate="1900-01-01" runat="server">
                            </Calendar>

<DateInput DisplayDateFormat="M/d/yyyy" DateFormat="M/d/yyyy" DisplayText="" AutoPostBack="True" LabelWidth="40%" type="text" value="" Height="25px"></DateInput>

<DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton></telerik:RadDatePicker> 
            </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="radDPPresentmentDate"
                        ErrorMessage="Please Select Date!" ValidationGroup ="PDC" ></asp:RequiredFieldValidator></td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblTransactionType" runat="server" Text="Select Transaction Type" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label5" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblClearingType" runat="server" Text="Select Clearing Type" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label17" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <%--<telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior3" ValidationExpression="^\+?[\d- ]{2,15}$"
        ErrorMessage="Invalid Date" EmptyMessage="" Validation-IsRequired="true">
        <TargetControls>
            <telerik:TargetInput ControlID="" />
        </TargetControls>
        </telerik:RegExpTextBoxSetting>
         <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehaviorEmail" Validation-IsRequired="True"
        EmptyMessage="" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.%]\w+)*\.\w+([-.]\w+)*$"
        ErrorMessage="Invalid Email">
        <TargetControls>
            <telerik:TargetInput ControlID="RadioButtonList1" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
     
         <telerik:TextBoxSetting BehaviorID="RagExpBehaviorOffName" Validation-IsRequired="True"
        ErrorMessage="Invalid Office Name" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtOfficeName" />
        </TargetControls>
    </telerik:TextBoxSetting>--%>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True">
                <asp:ListItem Value ="Clearing" Text="Clearing">Clearing</asp:ListItem>
                <asp:ListItem Value ="Fund Transfer" Text ="Fund Transfer"> Fund Transfer</asp:ListItem>
            </asp:RadioButtonList> <asp:RequiredFieldValidator   
            ID="ReqiredFieldValidator1"  
            runat="server"  
            ControlToValidate="RadioButtonList1"  
            ErrorMessage="Select Type" ValidationGroup ="PDC">  
        </asp:RequiredFieldValidator>  


            <%-- <telerik:TextBoxSetting BehaviorID="RagExpBehaviorDescription"
         EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBranchDescrption" />
        </TargetControls>
    </telerik:TextBoxSetting>--%>
         </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RadioButtonList ID="RadioButtonList2" runat="server" AutoPostBack="True">
                <asp:ListItem Value="Local" Text="Local">Local</asp:ListItem>
                <asp:ListItem Value="Intercity" Text="Intercity">Intercity</asp:ListItem>
                <asp:ListItem Value="Outstation" Text="Outstation">Outstation</asp:ListItem>
            </asp:RadioButtonList><asp:RequiredFieldValidator   
            ID="RequiredFieldValidator3"  
            runat="server"  
            ControlToValidate="RadioButtonList2"  
            ErrorMessage="Select Type" ValidationGroup ="PDC">  
        </asp:RequiredFieldValidator>


          <%--   <telerik:RegExpTextBoxSetting BehaviorID="ddClearingAccntNo" Validation-IsRequired="false"
        ValidationExpression="^[a-zA-Z0-9.-_/]{1,15}$" ErrorMessage="Invalid Account No"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDDClearingAccNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
        <telerik:RegExpTextBoxSetting BehaviorID="poClearingAccntNo" Validation-IsRequired="false"
        ValidationExpression="^[a-zA-Z0-9.-_/]{1,15}$" ErrorMessage="Invalid Account No"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPOClearingAccNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
      <telerik:TextBoxSetting BehaviorID="ddaccnttitle" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Invalid Account Title">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDDAcntTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>--%>
         </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblTransactionDetail" runat="server" Text="Add Transaction Details" 
                CssClass="lbl"></asp:Label>
         </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAdditionalDetail" runat="server" Text="Add Additional Details" 
                CssClass="lbl"></asp:Label>
         </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>

    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtTransactionDetails" runat="server" CssClass="txtbox" 
                MaxLength="100" TextMode="MultiLine" Height="70px"  
                onkeypress="return textboxMultilineMaxNumber(this,100)"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtAdditionalDetails" runat="server" CssClass="txtbox" 
                MaxLength="100" TextMode="MultiLine" Height="70px"  
                onkeypress="return textboxMultilineMaxNumber(this,100)"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
             &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
             &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
             &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <%--          <telerik:TextBoxSetting BehaviorID="poaccnttitle" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Invalid Account Title">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPOAccntTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>--%>&nbsp;<asp:Button ID="btnSave" runat="server" ValidationGroup ="PDC" CausesValidation ="true"  Text="Save" CssClass="btn" Width="71px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False"
               />
            &nbsp;
        </td>
    </tr>
</table>


