﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewSubmitComplains.ascx.vb"
    Inherits="DesktopModules_ComplainSubmission_ViewSubmitComplains" %>
     <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
    <telerik:RadInputManager ID="RadInputManager1" runat="server">   
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior4" Validation-IsRequired="false"
        EmptyMessage="Enter Complain">
        <TargetControls>
            <telerik:TargetInput ControlID="txtComplain" />
        </TargetControls>
    </telerik:TextBoxSetting>    
</telerik:RadInputManager>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnSave" runat="server" Text="Add Complain" CssClass="btn" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <asp:Label ID="lblBankListHeader" runat="server" Text="Complains List" CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                 <tr>
                    <td align="left" valign="top">
                        <table width="100%" id="tblDesc" runat="server" style="display:none">
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="Label1" runat="server" Text="Complain ID" CssClass="lbl"></asp:Label>
                                </td>
                               <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="Label2" runat="server" Text="Description" CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblComplainID" runat="server" CssClass="lbl"></asp:Label>
                                </td>
                                 <td align="left" valign="top" style="width: 25%">
                                    <asp:TextBox ID="txtComplain" runat="server" CssClass="txtbox" TextMode="MultiLine"
                                        Height="70px" ReadOnly="True" Enabled ="false" ></asp:TextBox>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <%--<tr>
                    <td align="left" valign="top">
                        <asp:Label ID="lblNRF" runat="server" Font-Bold="False" Text="No Record Found" Visible="False"
                            CssClass="headingblue"></asp:Label>
                        <asp:GridView ID="gvComplains" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                            Width="100%" AllowPaging="True" EnableModelValidation="True" 
                            AllowSorting="True">
                            <AlternatingRowStyle CssClass="GridAltItem" />
                            <Columns>
                                <asp:BoundField DataField="ComplainID" HeaderText="ID" SortExpression="ComplainID">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="CreateDate" HeaderText="Dated" SortExpression="CreateDate"
                                    DataFormatString="{0:dd-MMM-yyyy HH:mm:ss}" HtmlEncode="False">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Subject" HeaderText="Subject" SortExpression="Subject">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="IbtnView" runat="server" CommandArgument='<%#Eval("ComplainID") %>'
                                            CommandName="ViewDesc" CausesValidation="false" ImageUrl="~/images/action_source.gif"
                                            ToolTip="Description" />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlDetail" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveComplain", "&mid=" & Me.ModuleId & "&id="& Eval("ComplainID"))%>'
                                            ToolTip="Details">Details</asp:HyperLink>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="GridHeader" />
                            <PagerStyle CssClass="GridPager" />
                            <RowStyle CssClass="GridItem" />
                        </asp:GridView>
                    </td>
                </tr>--%>
                                 <tr>
                                                         <td align="left" valign="top">
                        <asp:Label ID="lblNRF" runat="server" Font-Bold="False" Text="No Record Found" Visible="False"
                            CssClass="headingblue"></asp:Label>
                                                             </tr>
                <tr>
                    <td align="left" valign="top">
                        <telerik:RadGrid ID="gvComplains" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                            <ClientSettings>
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
<%--                                    <telerik:GridTemplateColumn HeaderText="Select" DataField="IsApprove">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>   --%> 
                                    <telerik:GridBoundColumn DataField="ComplainID" HeaderText="ID" SortExpression="ComplainID">
                                    </telerik:GridBoundColumn>                               
                                    <telerik:GridBoundColumn DataField="CreateDate" HeaderText="Dated" SortExpression="CreateDate">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Subject" HeaderText="Subject" SortExpression="Subject">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Status" HeaderText="Status" SortExpression="Status">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn>
                                       <HeaderTemplate>
                                            <asp:Label ID="Edit" runat="server" Text="Reply"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                           <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveComplain", "&mid=" & Me.ModuleId & "&id=" & Eval("ComplainID"))%>'
                                            ToolTip="Reply">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                      <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Label ID="ShowProvince" runat="server" Text="View"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnView" runat="server" CommandArgument='<%#Eval("ComplainID")%>'
                                                CommandName="ViewDesc" ImageUrl="~/images/view.gif" ToolTip="View Description" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>                                   
                                </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />

<FilterMenu EnableImageSprites="False"></FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
