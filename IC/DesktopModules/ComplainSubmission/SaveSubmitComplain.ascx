﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveSubmitComplain.ascx.vb"
    Inherits="DesktopModules_ComplainSubmission_SaveSubmitComplain" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }
</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior3" Validation-IsRequired="true"
        ErrorMessage ="Invalid Subject">
        <TargetControls>
            <telerik:TargetInput ControlID="txtSubject" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior4" Validation-IsRequired="true"
         ErrorMessage ="Invalid Description" >
        <TargetControls>
            <telerik:TargetInput ControlID="txtComplain" />
        </TargetControls>
    </telerik:TextBoxSetting>
<%--    <telerik:TextBoxSetting BehaviorID="RagExpBehavior6" Validation-IsRequired="false"
        EmptyMessage="Enter Response" >
        <TargetControls>
            <telerik:TargetInput ControlID="txtResponse" />
        </TargetControls>
    </telerik:TextBoxSetting>--%>
    <telerik:RegExpTextBoxSetting BehaviorID="REPhoneNo" Validation-IsRequired="true"
        ValidationExpression="^\+?[\d- ]{2,15}$" ErrorMessage="Invalid Phone Number" >
        <TargetControls>
            <telerik:TargetInput ControlID="txtPhoneNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="REDisplayName" Validation-IsRequired="true"
        ValidationExpression="\b(\w*)\b(.)*\b(-)*\b(_)*\b" ErrorMessage="Invalid Name" >
        <TargetControls>
            <telerik:TargetInput ControlID="txtDisplayName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="REEmailAddress" Validation-IsRequired="true"
        ValidationExpression="^([a-zA-Z0-9_\-\.]+)@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$"
        ErrorMessage="Invalid Email"  >
        <TargetControls>
            <telerik:TargetInput ControlID="txtEmailAddress" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
</telerik:RadInputManager>
<telerik:RadInputManager ID="radResponse" runat="server">
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior6" Validation-IsRequired="true"
         ErrorMessage ="Invalid Response" >
        <TargetControls>
            <telerik:TargetInput ControlID="txtResponse" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblName" runat="server" Text="Name" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblPhoneNo" runat="server" Text="Contact Number" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label5" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtDisplayName" runat="server" CssClass="txtbox"  MaxLength="15"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtPhoneNo" runat="server" CssClass="txtbox" MaxLength="15" ></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblEmail" runat="server" Text="Contact Email" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label6" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblSubject" runat="server" Text="Subject" CssClass="lbl"></asp:Label><asp:Label
                ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="txtbox" MaxLength="100"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtSubject" runat="server" CssClass="txtbox" MaxLength="145"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblComplain" runat="server" Text="Description" CssClass="lbl"></asp:Label><asp:Label
                ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblResponse" runat="server" Text="Response" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblResponceRq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtComplain" runat="server" CssClass="txtbox" TextMode="MultiLine"
                Height="70px" onkeypress="return textboxMultilineMaxNumber(this,1500)"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtResponse" runat="server" CssClass="txtbox" TextMode="MultiLine"
                Height="70px" onkeypress="return textboxMultilineMaxNumber(this,1500)"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnSave" runat="server" Text="Submit" CssClass="btn"  />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" CausesValidation="False" />
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <%--<asp:GridView ID="gvComplains" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                CssClass="Grid" EnableModelValidation="True" Width="100%" Visible="False">
                <AlternatingRowStyle CssClass="GridAltItem" />
                <Columns>
                    <asp:BoundField DataField="ComplainID" HeaderText="ID" SortExpression="ComplainID">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CreateDate" HeaderText="Dated" SortExpression="CreateDate"
                        DataFormatString="{0:dd-MMM-yyyy HH:mm:ss}" HtmlEncode="False">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Subject" HeaderText="Subject" SortExpression="Subject">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="IbtnView" runat="server" CommandArgument='<%#Eval("ComplainID") %>'
                                CommandName="ViewDesc" CausesValidation="false" ImageUrl="~/images/action_source.gif"
                                ToolTip="Response" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                        <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="GridHeader" />
                <PagerStyle CssClass="GridPager" />
                <RowStyle CssClass="GridItem" />
            </asp:GridView>--%>
            <telerik:RadGrid ID="gvComplains" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize ="100" CssClass="RadGrid">
                            <ClientSettings>
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView TableLayout="Fixed" DataKeyNames="Sender,Receiver">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
<%--                                    <telerik:GridTemplateColumn HeaderText="Select" DataField="IsApprove">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>  --%>  
                                    <telerik:GridBoundColumn DataField="ComplainID" HeaderText="ID" SortExpression="ComplainID">
                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                        <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>                               
                                    <telerik:GridBoundColumn DataField="CreateDate" HeaderText="Dated" SortExpression="CreateDate">
                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                        <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Sender" HeaderText="Sender" SortExpression="Sender">
                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                        <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="Receiver" HeaderText="Reply By" SortExpression="Receiver">
                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                        <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Subject" HeaderText="Subject" SortExpression="Subject">
                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                        <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
<%--                                    <telerik:GridTemplateColumn>
                                       <HeaderTemplate>
                                            <asp:Label ID="Edit" runat="server" Text="Edit"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                           <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveComplain", "&mid=" & Me.ModuleId & "&id=" & Eval("ComplainID"))%>'
                                            ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>
                                      <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Label ID="ShowProvince" runat="server" Text="View"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnView" runat="server" CommandArgument='<%#Eval("ComplainID")%>'
                                                CommandName="ViewDesc" CausesValidation="false" ImageUrl="~/images/view.gif" ToolTip="View Response" />
                                        </ItemTemplate>
                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                        <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" />
                                    </telerik:GridTemplateColumn>                                   
                                </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />

<FilterMenu EnableImageSprites="False"></FilterMenu>
                        </telerik:RadGrid>
        </td>
    </tr>
</table>
