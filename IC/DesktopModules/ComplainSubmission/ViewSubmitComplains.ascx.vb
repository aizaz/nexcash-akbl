﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_ComplainSubmission_ViewSubmitComplains
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase

    Private BankCode As String
    Private ArrRights As ArrayList
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Private htRights As Hashtable
    Dim dtDa As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                LoadComplains(True)
                GetPageAccessRights()
                ViewState("SortExp") = Nothing
            End If
            tblDesc.Style.Add("display", "none")
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Complain")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    'Private Sub LoadComplains()
    '    Dim ComplainColl As New ICComplainsCollection
    '    ComplainColl.es.Connection.CommandTimeout = 3600

    '    ComplainColl = ICComplainsController.GetAllCopmlainsBySender(Me.UserId)

    '    dtDa = ComplainColl.Query.LoadDataTable()
    '    gvComplains.DataSource = dtDa
    '    ViewState("DataTable") = dtDa
    '    gvComplains.DataBind()

    '    If gvComplains.Rows.Count > 0 Then
    '        lblNRF.Visible = False
    '    Else
    '        lblNRF.Visible = True
    '    End If
    'End Sub
    Private Sub LoadComplains(ByVal IsBind As Boolean)

        Try
            Dim ComplainColl As New ICComplainsCollection
            ComplainColl.es.Connection.CommandTimeout = 3600

            ICComplainsController.GetAllCopmlainsBySender(Me.UserId, gvComplains.CurrentPageIndex + 1, gvComplains.PageSize, gvComplains, IsBind)

            'dtDa = ComplainColl.Query.LoadDataTable()
            'gvComplains.DataSource = dtDa
            'ViewState("DataTable") = dtDa
            'gvComplains.DataBind()

            If gvComplains.Items.Count > 0 Then
                lblNRF.Visible = False
            Else
                lblNRF.Visible = True
                gvComplains.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Response.Redirect(NavigateURL("SaveComplain", "&mid=" & Me.ModuleId & "&id=0"), False)

    End Sub



#Region "Load GV OLd/ without Telerik"

    ' comit this section because i  have change the gv to telerik gv

    'Protected Sub gvComplains_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvComplains.PageIndexChanged
    '    If (Not ViewState("sortDirection") Is Nothing And Not ViewState("SortExp") Is Nothing) Then
    '        Dim sortExpression As String = CStr(ViewState("SortExp")).ToString()
    '        If GridViewSortDirection() = SortDirection.Ascending Then
    '            SortGridView(sortExpression, ASCENDING)
    '        Else
    '            SortGridView(sortExpression, DESCENDING)
    '        End If
    '    Else
    '        LoadComplains()
    '    End If
    'End Sub
    'Private Sub SortGridView(ByVal sortExpression As String, ByVal direction As String)
    '    '  You can cache the DataTable for improving performance
    '    Try
    '        Dim dt As DataTable = ViewState("DataTable")
    '        Dim sortedDt As New DataTable
    '        dt.DefaultView.Sort = sortExpression + direction

    '        gvComplains.DataSource = dt.DefaultView
    '        gvComplains.DataBind()


    '        ViewState("dtGrid") = dt
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try


    'End Sub
    'Public Property GridViewSortDirection() As SortDirection
    '    Get
    '        If ViewState("sortDirection") Is Nothing Then
    '            ViewState("sortDirection") = SortDirection.Ascending
    '        ElseIf ViewState("sortDirection") = SortDirection.Ascending Then
    '            ViewState("sortDirection") = SortDirection.Descending
    '        ElseIf ViewState("sortDirection") = SortDirection.Descending Then
    '            ViewState("sortDirection") = SortDirection.Ascending
    '        End If

    '        Return DirectCast(ViewState("sortDirection"), SortDirection)
    '    End Get
    '    Set(ByVal value As SortDirection)
    '        ViewState("sortDirection") = value
    '    End Set
    'End Property
    'Protected Sub gvAuthorization_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvComplains.Sorting
    '    ViewState("SortExp") = e.SortExpression
    '    Dim sortExpression As String = CStr(ViewState("SortExp")).ToString()
    '    If GridViewSortDirection() = SortDirection.Ascending Then
    '        SortGridView(sortExpression, ASCENDING)
    '    Else
    '        SortGridView(sortExpression, DESCENDING)
    '    End If
    'End Sub

    'Protected Sub gvComplains_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvComplains.PageIndexChanging
    '    gvComplains.PageIndex = e.NewPageIndex
    '    If (Not ViewState("sortDirection") Is Nothing And Not ViewState("SortExp") Is Nothing) Then
    '        Dim sortExpression As String = CStr(ViewState("SortExp")).ToString()
    '        If GridViewSortDirection() = SortDirection.Ascending Then
    '            SortGridView(sortExpression, ASCENDING)
    '        Else
    '            SortGridView(sortExpression, DESCENDING)
    '        End If
    '    Else
    '        LoadComplains()
    '    End If

    'End Sub

    'Protected Sub gvComplains_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvComplains.RowCommand
    '    Try
    '        If e.CommandName = "ViewDesc" Then
    '            lblComplainID.Text = e.CommandArgument.ToString()
    '            txtComplain.Text = ICComplainsController.GetCopmlainDescriptionByComplainID(e.CommandArgument.ToString())
    '            tblDesc.Style.Remove("display")
    '        End If
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

#End Region

    Protected Sub gvComplains_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles gvComplains.ItemCommand
        Try
            If e.CommandName = "ViewDesc" Then
                lblComplainID.Text = e.CommandArgument.ToString()
                txtComplain.Text = ICComplainsController.GetCopmlainDescriptionByComplainID(e.CommandArgument.ToString())
                tblDesc.Style.Remove("display")
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvComplains_ItemCreated(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles gvComplains.ItemCreated

        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvComplains.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub



    Protected Sub gvComplains_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvComplains.NeedDataSource
        LoadComplains(False)
    End Sub

    Protected Sub gvComplains_PageIndexChanged(sender As Object, e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvComplains.PageIndexChanged
        gvComplains.CurrentPageIndex = e.NewPageIndex
    End Sub
End Class
