﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_PrintingQueue_PrintingQueue
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrICAssignUserRolsID As New ArrayList
    Private ArrICAssignedOfficeID As New ArrayList
    Private ArrICAssignedStatus As New ArrayList
    Private ArrICAssignedPaymentModes As New ArrayList
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()
            SetArraListAssignedOfficeIDs()
            lblMessageForQueue.Style.Add("display", "none")
            If Page.IsPostBack = False Then
                lblMessageForQueue.Text = ICUtilities.GetSettingValue("ErrorMessage")
                btnPrintSingle.Attributes.Item("onclick") = "if (Page_ClientValidate('Remarks')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnPrintSingle, Nothing).ToString() & " } "
                btnPrintBulk.Attributes.Item("onclick") = "if (Page_ClientValidate('Remarks')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnPrintBulk, Nothing).ToString() & " } "
                'btnCancelPrint.Attributes.Item("onclick") = "if (Page_ClientValidate('Remarks')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnCancelPrint, Nothing).ToString() & " } "


                DesignDts()
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                SetArraListAssignedPaymentModes()
                'SetArraListAssignedOfficeIDs()
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
                rbtnlstDateSelection.SelectedValue = "Creation Date"
                LoadddlCompany()

                LoadddlAccountPaymentNatureByUserID()
                LoadddlProductTypeByAccountPaymentNature()

                FillDesignedDtByAPNatureByAssignedRole()
                Dim dtPageLoad As New DataTable
                dtPageLoad = DirectCast(ViewState("AccountNumber"), DataTable)
                If dtPageLoad.Rows.Count > 0 Then
                    RefreshPage()
                Else
                    UIUtilities.ShowDialog(Me, "Error", "You do not have access to any transactional data. Please contact Al Baraka Administrator.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                    btnPrintBulk.Visible = False
                    btnPrintSingle.Visible = False
                    btnCancelPrint.Visible = False
                    gvAuthorization.Visible = False
                    lblNRF.Visible = True
                End If

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub RefreshPage()
        ClearAllTextBoxes()
        DesignDtForSummary()
        LoadddlBatcNumber()
        LoadgvAccountPaymentNatureProductType(True)
       
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Printing Queue")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Printing Queue") = True Then
                    ArrICAssignUserRolsID.Add(RoleID.ToString)
                End If
            Next
        End If
    End Sub
    Private Sub DesignDts()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim dtOfficeID As New DataTable
        dtAccountNumber.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("BranchCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("Currency", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("PaymentNatureCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("OfficeID", GetType(System.String)))
        ViewState("AccountNumber") = dtAccountNumber
    End Sub
    Private Sub FillDesignedDtByAPNatureByAssignedRole()
         Dim dtAssignedAPNatureOfficeID As New DataTable
        dtAssignedAPNatureOfficeID.Rows.Clear()
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserSecond(Me.UserId.ToString, ArrICAssignUserRolsID)
        dtAssignedAPNatureOfficeID.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAssignedAPNatureOfficeID.Rows
            dr("DropDown") = "False"
        Next
        ViewState("AccountNumber") = Nothing
        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID
       
    End Sub
    Private Sub SetArraListAssignedOfficeIDs()
        Dim dt As New DataTable
        Dim objICUser As New ICUser
        ArrICAssignedOfficeID.Clear()
        dt = ICUserRolesPaymentNatureController.GetAllTaggedLocationsWithUserByUSerIDAndRoleID(Me.UserId.ToString, ArrICAssignUserRolsID)
        If dt.Rows.Count > 0 Then
            objICUser.LoadByPrimaryKey(Me.UserId)
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("OfficeID").ToString
                ArrICAssignedOfficeID.Add(RoleID.ToString)
            Next
            ArrICAssignedOfficeID.Add(objICUser.OfficeCode)
        End If
    End Sub
    Private Sub SetArraListAssignedStatus()
        ArrICAssignedStatus.Clear()
        ArrICAssignedStatus.Add(12)

    End Sub
    Private Sub SetArraListAssignedPaymentModes()
        ArrICAssignedPaymentModes.Clear()
        If CBool(htRights("Cheque Instructions")) = True Then
            ArrICAssignedPaymentModes.Add("Cheque")
        End If
        If CBool(htRights("DD Instructions")) = True Then
            ArrICAssignedPaymentModes.Add("DD")
        End If
        If CBool(htRights("PO Instructions")) = True Then
            ArrICAssignedPaymentModes.Add("PO")
        End If
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, "", ArrICAssignUserRolsID)
            ddlCompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlCompany.Items.Clear()
                    ddlCompany.Items.Add(lit)
                    lit.Selected = True
                    ddlCompany.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
                ddlCompany.DataSource = dt
                ddlCompany.DataTextField = "CompanyName"
                ddlCompany.DataValueField = "CompanyCode"
                ddlCompany.DataBind()
                ddlCompany.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccountPaymentNatureByUserID()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlAcccountPaymentNature.Items.Clear()
            ddlAcccountPaymentNature.Items.Add(lit)
            lit.Selected = True
            ddlAcccountPaymentNature.AppendDataBoundItems = True
            ddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForDropDown(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
            ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
            ddlAcccountPaymentNature.DataBind()
           
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlBatcNumber()
        Try
            SetArraListAssignedStatus()
            Dim dtAccountNumber As New DataTable


            dtAccountNumber = ViewState("AccountNumber")

            SetArraListAssignedStatus()
            SetArraListAssignedPaymentModes()
            Dim lit As New ListItem
            lit.Text = "-- All --"
            lit.Value = "0"
            ddlBatch.Items.Clear()
            ddlBatch.Items.Add(lit)
            lit.Selected = True
            ddlBatch.AppendDataBoundItems = True
            ddlBatch.DataSource = ICInstructionController.GetAllTaggedBatchNumbersByCompanyCodeWithStatusForPrinting(ArrICAssignedPaymentModes, dtAccountNumber, ddlCompany.SelectedValue.ToString, ArrICAssignedStatus, Me.UserId.ToString)
            ddlBatch.DataTextField = "FileBatchNoName"
            ddlBatch.DataValueField = "FileBatchNo"
            ddlBatch.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlProductTypeByAccountPaymentNature()
        Try
            Dim lit As New ListItem
            Dim objICAPNature As New ICAccountsPaymentNature
            objICAPNature.es.Connection.CommandTimeout = 3600
            ddlProductType.Items.Clear()
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlProductType.Items.Add(lit)
            ddlProductType.AppendDataBoundItems = True
            If ddlAcccountPaymentNature.SelectedValue.ToString <> "0" Then
                If objICAPNature.LoadByPrimaryKey(ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)) Then
                    ddlProductType.DataSource = ICAccountPaymentNatureProductTypeController.GetAllProductTypesByAccountAndPaymentNature(objICAPNature)
                    ddlProductType.DataTextField = "ProductTypeName"
                    ddlProductType.DataValueField = "ProductTypeCode"
                    ddlProductType.DataBind()
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadgvAccountPaymentNatureProductType(ByVal DoDataBind As Boolean)
        Try
          
            Dim DateType, AccountNumber, BranchCode, Currency, PaymentNatureCode, CompanyCode, ProductTypeCode, InstrumentNo, InstructionNo, BatchCode, ReferenceNo As String
            Dim FromDate, ToDate As String
            Dim Amount As Double = 0
            Dim dtAccountNumber As New DataTable
            dtAccountNumber = ViewState("AccountNumber")
            DateType = ""
            AccountNumber = ""
            BranchCode = ""
            Currency = ""
            Currency = ""
            PaymentNatureCode = ""
            CompanyCode = "0"
            ProductTypeCode = ""
            InstructionNo = ""
            InstrumentNo = ""
            BatchCode = ""
            ReferenceNo = ""
            FromDate = Nothing
            ToDate = Nothing
            If txtAmount.Text <> "" And txtAmount.Text <> "Enter Amount" Then
                Amount = CDbl(txtAmount.Text)
            End If

            If rbtnlstDateSelection.SelectedValue = "Creation Date" Then
                DateType = "Creation Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Value Date" Then
                DateType = "Value Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Approval Date" Then
                DateType = "Approval Date"
            End If

            If txtReferenceNo.Text.ToString <> "" Then
                ReferenceNo = txtReferenceNo.Text.ToString
            End If

            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            End If
            If ddlProductType.SelectedValue.ToString <> "0" Then
                ProductTypeCode = ddlProductType.SelectedValue.ToString
            End If
            If txtInstrumentNo.Text.ToString <> "" Then
                InstrumentNo = txtInstrumentNo.Text.ToString
            End If
            If txtInstructionNo.Text.ToString <> "" Then
                InstructionNo = txtInstructionNo.Text.ToString
            End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                FromDate = raddpCreationFromDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If Not raddpCreationToDate.SelectedDate Is Nothing Then
                ToDate = raddpCreationToDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If ddlBatch.SelectedValue <> "0" Then
                BatchCode = ddlBatch.SelectedValue.ToString
            End If
            SetArraListAssignedStatus()
            SetArraListAssignedOfficeIDs()
            SetArraListAssignedPaymentModes()
            ICInstructionController.GetAllInstructionsForRadGridForPrintingWithUserLocation(DateType, FromDate, ToDate, dtAccountNumber, BatchCode, ProductTypeCode, InstructionNo, InstrumentNo, ReferenceNo, Me.gvAuthorization.CurrentPageIndex + 1, Me.gvAuthorization.PageSize, Me.gvAuthorization, DoDataBind, ArrICAssignedStatus, Me.UserId.ToString, Amount, ddlAmountOp.SelectedValue.ToString, ArrICAssignedPaymentModes, CompanyCode)
            If gvAuthorization.Items.Count > 0 Then
                gvAuthorization.Visible = True
                lblNRF.Visible = False

                btnPrintBulk.Visible = CBool(htRights("Bulk Print"))
                btnPrintSingle.Visible = CBool(htRights("Single Print"))
                btnCancelPrint.Visible = CBool(htRights("Cancel Print"))
                'SetJavaScript()
            Else
                lblNRF.Visible = True
                gvAuthorization.Visible = False
                btnPrintBulk.Visible = False
                btnPrintSingle.Visible = False
                btnCancelPrint.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
   

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadddlBatcNumber()
            LoadddlAccountPaymentNatureByUserID()
            LoadddlProductTypeByAccountPaymentNature()
            LoadgvAccountPaymentNatureProductType(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlAcccountPaymentNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcccountPaymentNature.SelectedIndexChanged
        Try
            If ddlAcccountPaymentNature.SelectedValue.ToString = "0" Then
                SetArraListRoleID()

                FillDesignedDtByAPNatureByAssignedRole()
                LoadddlBatcNumber()
                LoadddlProductTypeByAccountPaymentNature()

                
                LoadgvAccountPaymentNatureProductType(True)
            Else
FillDesignedDtByAPNatureByAssignedRole()
                SetViewStateDtOnAPNatureIndexChnage()
                LoadddlBatcNumber()
                LoadddlProductTypeByAccountPaymentNature()

               
                LoadgvAccountPaymentNatureProductType(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetViewStateDtOnAPNatureIndexChnage()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim AccountNumber, BranchCode, Currency, PaymentNatureCode As String
        Dim ArrayListOfficeID As New ArrayList
        Dim drAccountNo As DataRow
        AccountNumber = Nothing
        BranchCode = Nothing
        Currency = Nothing
        PaymentNatureCode = Nothing
        dtAccountNumber = ViewState("AccountNumber")
        ViewState("AccountNumber") = Nothing
        AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0)
        BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1)
        Currency = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2)
        PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)
        For Each dr As DataRow In dtAccountNumber.Rows
            If dr("AccountNumber").ToString.Split("-")(0) = AccountNumber And dr("AccountNumber").ToString.Split("-")(1) = BranchCode And dr("AccountNumber").ToString.Split("-")(2) = Currency And dr("PaymentNature").ToString = PaymentNatureCode Then
                ArrayListOfficeID.Add(dr("OfficeID").ToString)
            End If
        Next
        dtAccountNumber.Rows.Clear()
        If ArrayListOfficeID.Count > 0 Then
            For Each OfficeID In ArrayListOfficeID
                drAccountNo = dtAccountNumber.NewRow()
                drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
                drAccountNo("PaymentNature") = PaymentNatureCode
                drAccountNo("OfficeID") = OfficeID
                dtAccountNumber.Rows.Add(drAccountNo)
            Next
        Else
            drAccountNo = dtAccountNumber.NewRow()
            drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
            drAccountNo("PaymentNature") = PaymentNatureCode
            drAccountNo("OfficeID") = "0"
            dtAccountNumber.Rows.Add(drAccountNo)
        End If
        'dtAccountNumber.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAccountNumber.Rows
            dr("DropDown") = "True"
        Next
        ViewState("AccountNumber") = dtAccountNumber



    End Sub
    Protected Sub ddlBatch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBatch.SelectedIndexChanged
        Try
            SetArraListRoleID()
            LoadddlProductTypeByAccountPaymentNature()

            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged
        Try
            SetArraListRoleID()



            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub ClearAllTextBoxes()
        txtRemarks.Text = ""
        txtInstructionNo.Text = ""
        txtInstrumentNo.Text = ""
        txtReferenceNo.Text = ""
        txtAmount.Text = ""
        ddlAmountOp.SelectedValue = "="
    End Sub
    Private Function CheckgvInstructionAuthentication() As Boolean
        Try

            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVInstruction In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVInstruction.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub gvAuthorization_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAuthorization.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAuthorization_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','0');"
        End If
        Dim imgBtn As LinkButton
        Dim AppPath As String = "" ' DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            imgBtn = New LinkButton
            imgBtn = DirectCast(item.Cells(1).FindControl("lbInstructionID"), LinkButton)
            imgBtn.OnClientClick = "showurldialog('Instruction Details',' " & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & item.GetDataKeyValue("InstructionID").ToString().EncryptString() & "' ,true,700,650);return false;"
            If CBool(htRights("View Instructions Detail")) = True Then
                imgBtn.Enabled = True

            End If
            If item.GetDataKeyValue("IsAmendmentComplete") = True Then

                gvAuthorization.AlternatingItemStyle.CssClass = "GridItemComplete"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItemComplete"


            Else
                gvAuthorization.AlternatingItemStyle.CssClass = "GridItem"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItem"


            End If

        End If
    End Sub

    Protected Sub gvAuthorization_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAuthorization.NeedDataSource
        Try
            ClearAllTextBoxes()
            SetArraListRoleID()
            LoadgvAccountPaymentNatureProductType(False)
            'SetJavaScript()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Try
                Dim dtPageLoad As New DataTable
                dtPageLoad = DirectCast(ViewState("AccountNumber"), DataTable)
                If dtPageLoad.Rows.Count > 0 Then
                    LoadgvAccountPaymentNatureProductType(True)
                Else
                    btnPrintBulk.Visible = False
                    btnPrintSingle.Visible = False
                    btnCancelPrint.Visible = False
                    gvAuthorization.Visible = False
                    lblNRF.Visible = True
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub
    Private Function CheckgvClientRoleCheckedForProcessAll() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function CheckgvMultiRepoprtSelected() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            Dim ReportCount As Integer = 0
            For Each rowGVICClientRoles In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    ReportCount = ReportCount + 1
                End If
            Next
            If ReportCount > 1 Then
                Result = True
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Sub DesignDtForSummary()
        ViewState("Summary") = Nothing
        Dim dtSummary As New DataTable

        dtSummary.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
        dtSummary.Columns.Add(New DataColumn("Status", GetType(System.String)))
        dtSummary.Columns.Add(New DataColumn("Message", GetType(System.String)))
        ViewState("Summary") = dtSummary
    End Sub
    Protected Sub btnPrintSingle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrintSingle.Click
        Page.Validate()

        If Page.IsValid Then
            Try

                Dim chkSelect As CheckBox
                Dim ArryListInstructionId As New ArrayList
                Dim InstructionCount As Integer = 0
                Dim ReportBook As New Telerik.Reporting.ReportBook
                Dim StrInstCount As String = Nothing
                Dim StrArr As String() = Nothing
                Dim dtInstructionsForCheque As New DataTable
                Dim objICUser As New ICUser
                Dim dtSummary As DataTable
                dtSummary = New DataTable
                objICUser.LoadByPrimaryKey(Me.UserId)
                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Printing Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If


                If CheckgvMultiRepoprtSelected() = True Then
                    UIUtilities.ShowDialog(Me, "Printing Queue", "Please select only one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        InstructionCount = InstructionCount + 1
                        ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                    End If
                Next
                If ArryListInstructionId.Count > 0 Then

                    If CInt(GetRequiredInstrumentNosForPrint()) > 0 Then
                        If ICInstrumentController.GetTotalUnUSedInstrumentCountByPrintLocationCode(objICUser.OfficeCode) >= CInt(GetRequiredInstrumentNosForPrint()) Then
                            dtInstructionsForCheque = ICInstructionController.GetInstructionsByArrayInstructionIDGroupByAccountNo(ArryListInstructionId, objICUser.OfficeCode.ToString, "Cheque")
                            For Each dr As DataRow In dtInstructionsForCheque.Rows
                                If ICInstrumentController.GetRequiredUnAssignedAndUnUsedInstrumentNoByAccNoAndPrintLocation(dr("ClientAccountNo").ToString, objICUser.OfficeCode.ToString, CInt(dr("Count"))) = False Then
                                    UIUtilities.ShowDialog(Me, "Error", "Selected instruction can not be printed due to insufficient available stationary on printing location against client account(s).", ICBO.IC.Dialogmessagetype.Failure)
                                    Exit Sub
                                End If

                            Next

                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Selected instruction can not be printed due to insufficient available stationary on printing location", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    End If


                    dtSummary = DirectCast(ViewState("Summary"), DataTable)
                    dtSummary.Rows.Clear()

                    Dim Remarks As String = ""
                    If txtRemarks.Text <> "" And txtRemarks.Text <> "Enter Remarks" Then
                        Remarks = txtRemarks.Text.ToString
                    End If
                    ReportBook = ICInstructionProcessController.PerformPrintitngOnInstructions(ArryListInstructionId, Me.UserInfo.Username.ToString, Me.UserId.ToString, Remarks, "Print")

                End If
                RefreshPage()
                If ReportBook.Reports.Count > 0 Then
                    rptCheque.Report = ReportBook
                    rptCheque.RefreshReport()
                    Dim scr As String = "window.onload = function() { " & Me.rptCheque.ClientID & ".PrintReport() } "
                    Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "scr", scr, True)

                Else
                    UIUtilities.ShowDialog(Me, "Printing Queue", "There is an issue in printing. Please contact Al Baraka administrator.", ICBO.IC.Dialogmessagetype.Failure)
                    RefreshPage()
                End If
                'If dtSummary.Rows.Count > 0 Then

                '    gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dtSummary)
                '    gvShowSummary.DataBind()
                '    pnlShowSummary.Style.Remove("display")
                '    Dim scr As String
                '    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                '    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)

                'Else
                '    Dim drPrintSummary As DataRow
                '    drPrintSummary = dtSummary.NewRow
                '    drPrintSummary("InstructionID") = "-"
                '    drPrintSummary("Status") = "-"
                '    drPrintSummary("Message") = "There is an issue in printing. Please contact Al Baraka administrator."
                '    dtSummary.Rows.Add(drPrintSummary)
                '    gvShowSummary.DataSource = dtSummary
                '    gvShowSummary.DataBind()
                '    pnlShowSummary.Style.Remove("display")
                '    Dim scr As String
                '    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                '    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                'End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnCancelPrint_Click(sender As Object, e As System.EventArgs) Handles btnCancelPrint.Click
        Page.Validate()

        If Page.IsValid Then
            Try
                Dim objICInstruction As ICInstruction
                Dim chkSelect As CheckBox
                Dim ArryListInstructionId As New ArrayList
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0

                Dim StrInstCount As String = Nothing
                Dim StrArr As String() = Nothing
                ArryListInstructionId.Clear()
                Dim dt As New DataTable
                dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Message", GetType(System.String)))

                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Printing Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Dim Remarks As String = ""
                If txtRemarks.Text <> "" And txtRemarks.Text <> "Enter Remarks" Then
                    Remarks = txtRemarks.Text.ToString
                End If

                For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        objICInstruction = New ICInstruction
                        objICInstruction.LoadByPrimaryKey(gvVerificationRow.GetDataKeyValue("InstructionID"))
                        'ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                        If objICInstruction.Status.ToString = "12" And (objICInstruction.PaymentMode = "PO" Or objICInstruction.PaymentMode = "DD") Then
                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "52", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                            ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                        ElseIf objICInstruction.Status.ToString = "12" And objICInstruction.PaymentMode = "Cheque" Then
                            ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                        End If
                    End If
                Next
                If ArryListInstructionId.Count > 0 Then
                    StrInstCount = ICInstructionProcessController.CancelPrintOfInstruction(ArryListInstructionId, Me.UserId, Me.UserInfo.Username.ToString, Remarks, "20", dt, "CANCEL PRINT")
                Else
                    UIUtilities.ShowDialog(Me, "Error", "Invalid selected instruction(s) status", Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If

                RefreshPage()
                If dt.Rows.Count > 0 Then
                    gvShowSummary.DataSource = Nothing
                    gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                    gvShowSummary.DataBind()
                    pnlShowSummary.Style.Remove("display")
                    Dim scr As String
                    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                Else
                    gvShowSummary.DataSource = Nothing
                    pnlShowSummary.Style.Add("display", "none")
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnPrintBulk_Click(sender As Object, e As System.EventArgs) Handles btnPrintBulk.Click
        Page.Validate()

        If Page.IsValid Then
            Try

                Dim chkSelect As CheckBox

                Dim ArryListInstructionId As New ArrayList
                Dim InstructionCount As Integer = 0
                Dim ChkInstCount As Integer = 0
                Dim ReportBook As New Telerik.Reporting.ReportBook
                Dim StrInstCount As String = Nothing
                Dim StrArr As String() = Nothing
                Dim dtSummary As New DataTable
                Dim objICUser As New ICUser
                Dim dtInstructionsForCheque As New DataTable
                objICUser.LoadByPrimaryKey(Me.UserId.ToString)
                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Printing Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If


                For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        InstructionCount = InstructionCount + 1
                        ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                    End If
                Next
                If ArryListInstructionId.Count > 0 Then
                    If CInt(GetRequiredInstrumentNosForPrint()) > 0 Then
                        If ICInstrumentController.GetTotalUnUSedInstrumentCountByPrintLocationCode(objICUser.OfficeCode) >= CInt(GetRequiredInstrumentNosForPrint()) Then
                            dtInstructionsForCheque = ICInstructionController.GetInstructionsByArrayInstructionIDGroupByAccountNo(ArryListInstructionId, objICUser.OfficeCode.ToString, "Cheque")
                            For Each dr As DataRow In dtInstructionsForCheque.Rows
                                If ICInstrumentController.GetRequiredUnAssignedAndUnUsedInstrumentNoByAccNoAndPrintLocation(dr("ClientAccountNo").ToString, objICUser.OfficeCode.ToString, CInt(dr("Count"))) = False Then
                                    UIUtilities.ShowDialog(Me, "Error", "Selected instruction can not be printed due to insufficient available stationary on printing location against client account(s).", ICBO.IC.Dialogmessagetype.Failure)
                                    Exit Sub
                                End If

                            Next

                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Selected instruction can not be printed due to insufficient available stationary on printing location", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    End If
                    dtSummary = DirectCast(ViewState("Summary"), DataTable)
                    dtSummary.Rows.Clear()
                    Dim Remarks As String = ""
                    If txtRemarks.Text <> "" And txtRemarks.Text <> "Enter Remarks" Then
                        Remarks = txtRemarks.Text.ToString
                    End If
                    ArryListInstructionId.Sort()
                    ReportBook = ICInstructionProcessController.PerformPrintitngOnInstructions(ArryListInstructionId, Me.UserInfo.Username.ToString, Me.UserId.ToString, Remarks, "Print")
                End If
                If ReportBook.Reports.Count > 0 Then
                    rptCheque.Report = ReportBook
                    rptCheque.RefreshReport()
                    Dim scr As String = "window.onload = function() { " & Me.rptCheque.ClientID & ".PrintReport() } "
                    Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "scr", scr, True)
                    RefreshPage()
                Else
                    UIUtilities.ShowDialog(Me, "Error", "There is an issue in printing. Please contact Al Baraka administrator.", ICBO.IC.Dialogmessagetype.Failure)
                    RefreshPage()
                End If
                'If dtSummary.Rows.Count > 0 Then

                '    gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dtSummary)
                '    gvShowSummary.DataBind()
                '    pnlShowSummary.Style.Remove("display")
                '    Dim scr As String
                '    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                '    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)

                'Else
                '    Dim drPrintSummary As DataRow
                '    drPrintSummary = dtSummary.NewRow
                '    drPrintSummary("InstructionID") = "-"
                '    drPrintSummary("Status") = "-"
                '    drPrintSummary("Message") = "There is an issue in printing. Please contact Al Baraka administrator."
                '    dtSummary.Rows.Add(drPrintSummary)
                '    gvShowSummary.DataSource = dtSummary
                '    gvShowSummary.DataBind()
                '    pnlShowSummary.Style.Remove("display")
                '    Dim scr As String
                '    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                '    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                'End If
                'RefreshPage()

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Function GetRequiredInstrumentNosForPrint() As Integer
        Dim InstrumentNoCount As Integer = 0

        Dim chkSelect As CheckBox

        For Each rowGVICClientRoles As GridDataItem In gvAuthorization.Items
            chkSelect = New CheckBox
            chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
            If chkSelect.Checked = True Then
                If rowGVICClientRoles.GetDataKeyValue("PaymentMode").ToString = "Cheque" Then
                    If rowGVICClientRoles.GetDataKeyValue("InstrumentNo") Is Nothing Or rowGVICClientRoles.GetDataKeyValue("InstrumentNo") = "-" Or rowGVICClientRoles.GetDataKeyValue("InstrumentNo") = "" Then
                        InstrumentNoCount = InstrumentNoCount + 1
                    End If
                End If
            End If
        Next
        Return InstrumentNoCount
    End Function
    Protected Sub raddpCreationFromDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationFromDate.SelectedDateChanged
        Try

            'If raddpCreationFromDate.SelectedDate Is Nothing Then
            '    UIUtilities.ShowDialog(Me, "Error", "Please select creation from date.", ICBO.IC.Dialogmessagetype.Failure)
            '    Exit Sub
            'End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Approval Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Approval from date should be less than approval to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub raddpCreationToDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationToDate.SelectedDateChanged
        Try
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Approval Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Approval from date should be less than approval to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub rbtnlstDateSelection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnlstDateSelection.SelectedIndexChanged
        Try
            If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now.AddYears(2)
            Else
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
            End If

            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Approval Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Approval from date should be less than approval to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class

