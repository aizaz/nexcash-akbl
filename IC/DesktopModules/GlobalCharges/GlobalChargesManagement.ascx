﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GlobalChargesManagement.ascx.vb"
    Inherits="DesktopModules_User_Limits_ViewUserLimits" %>
    <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>



<telerik:RadInputManager ID="RadInputManager2" runat="server">
    <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior2" EmptyMessage="type.."
        Type="Number">
    </telerik:NumericTextBoxSetting>   
   <%-- <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior4" Validation-IsRequired="true"
        ValidationExpression="^[ a-zA-Z][ a-zA-Z\\s]+$" ErrorMessage="Invalid Name" EmptyMessage="Enter Province Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtProvinceName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>    --%>
      <telerik:RegExpTextBoxSetting BehaviorID="regTitle" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z ]{1,150}$" ErrorMessage="Invalid Title Name"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTitle" />
        </TargetControls>

<Validation IsRequired="True"></Validation>
        </telerik:RegExpTextBoxSetting> 


        <telerik:NumericTextBoxSetting DecimalDigits="2" BehaviorID="regChargeAmount" Validation-IsRequired="true"
         ErrorMessage="Please enter numeric values"
        EmptyMessage="" AllowRounding="False" MaxValue="1000" MinValue="0.001">
        <TargetControls>
            <telerik:TargetInput ControlID="txtChargeAmount" />
        </TargetControls>

<Validation IsRequired="True"></Validation>
        </telerik:NumericTextBoxSetting> 

          <telerik:RegExpTextBoxSetting  ValidationExpression="^[a-zA-Z0-9 ]+$" BehaviorID="regChargeAccount" Validation-IsRequired="true"
         ErrorMessage="Invalid Account No" 
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtChargeAccount" />
        </TargetControls>

<Validation IsRequired="True"></Validation>
        </telerik:RegExpTextBoxSetting> 

           


</telerik:RadInputManager>





 


<style type="text/css">
    
    .style1
    {
        width: 25%;
        
    }
    
</style>
<p>
    &nbsp;</p>
<table width="100%">

 
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
       
    </tr>
   
 
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
       
    </tr>
   
 
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
       
    </tr>
   
 
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblTitle" runat="server" Text="Title" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqTitle" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblChargeAmount" runat="server" Text="Charge Type"
                CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqChargeAmount" runat="server" Text="*" ForeColor="Red"></asp:Label>
         
        
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
       
    </tr>
   
        <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
             <asp:TextBox ID="txtTitle" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
        </td>
        <td align="left" valign="top" class="style1">
            &nbsp;</td>
        <td align="left" valign="top" class="style1">
          <asp:RadioButtonList  runat="server" ID="rdListChareAmountType" 
                RepeatDirection="Horizontal" AutoPostBack="True">
              <asp:ListItem Value="Fixed" Selected="True">Fixed</asp:ListItem>
              <asp:ListItem Value="Percentage of Amount">Percentage of Amount</asp:ListItem>
            </asp:RadioButtonList>
        
        </td>
        <td align="left" valign="top" class="style1">
           
        </td>
    </tr>

        <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
             &nbsp;&nbsp;</td>
        <td align="left" valign="top" class="style1">
            &nbsp;</td>
        <td align="left" valign="top" class="style1">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="rdListChareAmountType" ErrorMessage="Please select charge type"></asp:RequiredFieldValidator>
        
        </td>
        <td align="left" valign="top" class="style1">
           
            &nbsp;</td>
    </tr>

        <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
          
            <asp:Label ID="lblChargeAmountValue" runat="server" Text="Charge Amount"
                CssClass="lbl"></asp:Label>
        
            <asp:Label ID="lblReqTitle0" runat="server" Text="*" ForeColor="Red"></asp:Label>
        
         </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
          
          <asp:Label ID="lblisActive" runat="server" Text="Active"
                CssClass="lbl"></asp:Label>
           
               </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>

        <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
          
               <asp:TextBox ID="txtChargeAmount" runat="server" CssClass="txtbox" MaxLength="4"></asp:TextBox>
        
            </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
          
               <asp:Checkbox ID="chkActive" runat="server" CssClass="txtbox" MaxLength="50"></asp:Checkbox>
        
               </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>

        <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
          
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>

      <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="savebtn" runat="server" Text="Save" CssClass="btn" CausesValidation ="true" 
                Width="74px" />
            <asp:Button ID="cancelbtn" runat="server" Text="Cancel" CssClass="btnCancel"
                Width="75px" CausesValidation="False" />
    </tr>

</table>
