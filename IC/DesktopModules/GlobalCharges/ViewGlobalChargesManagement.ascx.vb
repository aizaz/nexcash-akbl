﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Security.Permissions
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports Telerik.Web.UI

Partial Class DesktopModules_User_Limits_ViewUserLimits
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase

    Private htRights As Hashtable
    Dim ObjICGlobalCharges As New ICBO.IC.ICGlobalCharges

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Not Page.IsPostBack Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnAdd.Visible = htRights("Add")
                LoadAllGlobalCharges(True)


            End If



        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub LoadAllGlobalCharges(ByVal DoBind As Boolean)
        Try
            ICGlobalChargesController.GetAllGlobalCharges(0, Me.gvGlobalCharges.PageSize, Me.gvGlobalCharges, DoBind)
            If gvGlobalCharges.Items.Count > 0 Then
                btnDeleteGlobalID.Visible = htRights("Delete")
                gvGlobalCharges.Columns(6).Visible = htRights("Delete")
                gvGlobalCharges.Columns(5).Visible = htRights("Update")
                gvGlobalCharges.Visible = True
            Else
                btnDeleteGlobalID.Visible = False
                gvGlobalCharges.Visible = False

            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "GlobalChargesManagement")
            ViewState("htRights") = htRights



            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub



#End Region







    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Response.Redirect(NavigateURL("SaveGlobalCharges", "&mid=" & Me.ModuleId & "&id=0"), False)
    End Sub

    Protected Sub gvGlobalCharges_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvGlobalCharges.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvGlobalCharges.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvGlobalCharges_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvGlobalCharges.ItemDataBound
        Dim chkActive As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkActive = New CheckBox
            chkActive = DirectCast(e.Item.Cells(0).FindControl("chkSelectAllGlobalID"), CheckBox)
            chkActive.Attributes("onclick") = "checkAllCheckboxes('" & chkActive.ClientID & "','" & gvGlobalCharges.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Private Sub LoadGlobalChages(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal RadGrid As RadGrid, ByVal DoDataBind As Boolean)
        Try

            LoadAllGlobalCharges(DoDataBind)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Protected Sub gvGlobalCharges_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvGlobalCharges.ItemCommand
        Try
            Dim objICGlobalCharges As New ICGlobalCharges
            objICGlobalCharges.es.Connection.CommandTimeout = 3600
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    If objICGlobalCharges.LoadByPrimaryKey(e.CommandArgument.ToString) Then

                        ICGlobalChargesController.DeleteGlobalChargesByGlobalID(objICGlobalCharges.GlobalChargeID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        UIUtilities.ShowDialog(Me, "Global Charges ", "Global Charges deleted successfully", ICBO.IC.Dialogmessagetype.Success)
                        LoadGlobalChages(Me.gvGlobalCharges.CurrentPageIndex + 1, Me.gvGlobalCharges.PageSize, Me.gvGlobalCharges, True)

                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Error ", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                LoadGlobalChages(Me.gvGlobalCharges.CurrentPageIndex + 1, Me.gvGlobalCharges.PageSize, Me.gvGlobalCharges, False)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnDeleteGlobalID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteGlobalID.Click
        If Page.IsValid Then
            Try
                Dim chkBox As CheckBox
                Dim objICGlobal As ICGlobalCharges
                Dim PassToDeleteCount As Integer = 0
                Dim FailToDeleteCount As Integer = 0
                Dim GlobalID As String

                If CheckGVCompanySelected() = False Then
                    UIUtilities.ShowDialog(Me, "Error", "Please select at least one record", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each gvGlobalCharges1 As GridDataItem In gvGlobalCharges.Items
                    chkBox = New CheckBox
                    chkBox = DirectCast(gvGlobalCharges1.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkBox.Checked = True Then
                        objICGlobal = New ICGlobalCharges
                        GlobalID = gvGlobalCharges1.GetDataKeyValue("GlobalChargeID")
                        If objICGlobal.LoadByPrimaryKey(GlobalID.ToString) Then
                            'If objICGlobal.Active = True Then
                            '    FailToDeleteCount = FailToDeleteCount + 1
                            'Else
                            ICGlobalChargesController.DeleteGlobalChargesByGlobalID(objICGlobal.GlobalChargeID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            PassToDeleteCount = PassToDeleteCount + 1

                            ' End If
                        End If
                    End If
                Next
                If Not PassToDeleteCount = 0 And Not FailToDeleteCount = 0 Then
                    LoadGlobalChages(Me.gvGlobalCharges.CurrentPageIndex + 1, Me.gvGlobalCharges.PageSize, Me.gvGlobalCharges, True)
                    UIUtilities.ShowDialog(Me, "Delete Global Charges", "[ " & PassToDeleteCount.ToString & " ] Global Charge(s) deleted successfully.<br /> [ " & FailToDeleteCount.ToString & " ] Global Charge(s) can not be deleted due to following reason: <br /> 1. Company is approved<br /> 2. Delete associated records", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                ElseIf Not PassToDeleteCount = 0 And FailToDeleteCount = 0 Then
                    LoadGlobalChages(Me.gvGlobalCharges.CurrentPageIndex + 1, Me.gvGlobalCharges.PageSize, Me.gvGlobalCharges, True)
                    UIUtilities.ShowDialog(Me, "Delete Global Charges", PassToDeleteCount.ToString & " Global Charge(s) deleted successfully.", ICBO.IC.Dialogmessagetype.Success)

                    Exit Sub
                ElseIf Not FailToDeleteCount = 0 And PassToDeleteCount = 0 Then
                    LoadGlobalChages(Me.gvGlobalCharges.CurrentPageIndex + 1, Me.gvGlobalCharges.PageSize, Me.gvGlobalCharges, True)
                    UIUtilities.ShowDialog(Me, "Delete Global Charges", "[ " & FailToDeleteCount.ToString & " ] Global Charge(s) can not be deleted due to following reasons: <br /> 1. Global Charge(s) is approved<br /> 2. Delete associated records", ICBO.IC.Dialogmessagetype.Failure)

                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Private Function CheckGVCompanySelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvGlobalCharges1 As GridDataItem In gvGlobalCharges.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvGlobalCharges1.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub gvGlobalCharges_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles gvGlobalCharges.NeedDataSource
        Try
            LoadAllGlobalCharges(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
