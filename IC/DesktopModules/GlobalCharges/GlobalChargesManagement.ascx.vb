﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Security.Permissions
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Partial Class DesktopModules_User_Limits_ViewUserLimits
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Dim GlobalID As String = Nothing
    Private htRights As Hashtable
    Dim ObjICGlobalCharges As New ICBO.IC.ICGlobalCharges

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then

                GlobalID = IIf(Request.QueryString("GlobalID") Is Nothing, Nothing, Request.QueryString("GlobalID"))
                ViewState("globalID") = GlobalID
                '   GetPageAccessRights()
                'LoadDllAccounts()
                If GlobalID is Nothing  Then
                    lblPageHeader.Text = "Add Global Charges"
                Else
                    Clear()
                    Dim objICGlobalCharges As New ICGlobalCharges
                    objICGlobalCharges.LoadByPrimaryKey(GlobalID)
                    lblPageHeader.Text = "Update Global Charges"
                    savebtn.Text = "Update"
                    txtTitle.Text = objICGlobalCharges.Title.ToString
                    rdListChareAmountType.SelectedValue = objICGlobalCharges.ChargeType


                    SetChargesType()


                    'Me.ddlAccounts.SelectedValue = objICGlobalCharges.PrincipalBankAccountID




                    txtChargeAmount.Text = objICGlobalCharges.ChargeAmount.ToString
                    chkActive.Checked = objICGlobalCharges.Active.ToString


                End If


            End If



        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub


    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Global Charges")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

  

#End Region



   
  

    Protected Sub savebtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles savebtn.Click
        If Page.IsValid Then
            Try
                GlobalID = ViewState("globalID")
                ObjICGlobalCharges.Title = txtTitle.Text



                ObjICGlobalCharges.ChargeAmount = txtChargeAmount.Text
                ObjICGlobalCharges.ChargeType = rdListChareAmountType.SelectedItem.Text


                If Me.rdListChareAmountType.SelectedIndex = 0 Then

                    ObjICGlobalCharges.ChargeAmount = txtChargeAmount.Text

                Else

                    ObjICGlobalCharges.ChargePercentage = txtChargeAmount.Text

                End If

                ObjICGlobalCharges.Active = chkActive.Checked

                ObjICGlobalCharges.CreatedBy = Me.UserId
                ObjICGlobalCharges.CreatedOn = DateTime.Now
                'If ddlAccounts.SelectedValue <> 0 Then
                '    ObjICGlobalCharges.PrincipalBankAccountID = Convert.ToInt32(ddlAccounts.SelectedValue)
                'End If
                If GlobalID Is Nothing Then

                    If CheckDuplicate(ObjICGlobalCharges, False) = False Then
                        ICGlobalChargesController.AddGlobalCharges(ObjICGlobalCharges, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        UIUtilities.ShowDialog(Me, "Save Global Charges", "Global Charges Added successfully.", ICBO.IC.Dialogmessagetype.Success)
                        Clear()
                    Else
                        UIUtilities.ShowDialog(Me, "Save Global Charges", "Can not add duplicate Title.", ICBO.IC.Dialogmessagetype.Failure)
                    End If
                Else
                    ObjICGlobalCharges.GlobalChargeID = GlobalID
                    If CheckDuplicate(ObjICGlobalCharges, True) = False Then

                        ICGlobalChargesController.AddGlobalCharges(ObjICGlobalCharges, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        UIUtilities.ShowDialog(Me, "Update GlobalCharges", "Global Charges Updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())

                    Else
                        UIUtilities.ShowDialog(Me, "Save Global Charges", "Can not add duplicate Title.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    End If

                End If


            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)

            End Try
        End If
    End Sub
    Private Sub Clear()
        txtTitle.Text = ""
        txtChargeAmount.Text = ""
        rdListChareAmountType.ClearSelection()
        chkActive.Checked = False
    End Sub

    Private Function CheckDuplicate(ByVal objICGlobalID As ICGlobalCharges, ByVal IsUpdate As Boolean) As Boolean
        Try
            Dim rslt As Boolean = False

            Dim objICGlobalChargesColl As New ICGlobalChargesCollection


            objICGlobalChargesColl.es.Connection.CommandTimeout = 3600
            objICGlobalChargesColl.LoadAll()
            objICGlobalChargesColl.Query.Where(objICGlobalChargesColl.Query.Title = objICGlobalID.Title)
            If IsUpdate = True Then
                objICGlobalChargesColl.Query.Where(objICGlobalChargesColl.Query.GlobalChargeID <> objICGlobalID.GlobalChargeID)
            End If
            If objICGlobalChargesColl.Query.Load() Then
                rslt = True
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub cancelbtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelbtn.Click
        Response.Redirect(NavigateURL(), False)
    End Sub


    'Private Sub LoadDllAccounts()
    '    Try
    '        Dim lit As New ListItem
    '        lit.Value = "0"
    '        lit.Text = "-- Please Select --"
    '        ddlAccounts.Items.Clear()
    '        ddlAccounts.Items.Add(lit)
    '        ddlAccounts.AppendDataBoundItems = True
    '        ddlAccounts.DataSource = ICGlobalChargesController.GetallAccounttypeWithChargesTypeAccount()
    '        ddlAccounts.DataTextField = "AccountTitleAndNumber"
    '        ddlAccounts.DataValueField = "PrincipalBankAccountID"
    '        ddlAccounts.DataBind()

    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub


   


   

    Protected Sub rdListChareAmountType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rdListChareAmountType.SelectedIndexChanged

     
        SetChargesType()



    End Sub


    Private Sub SetChargesType()


        If Me.rdListChareAmountType.SelectedIndex = 0 Then

            Me.lblChargeAmountValue.Text = "Charge Amount"

            Dim setting As Telerik.Web.UI.NumericTextBoxSetting

            setting = Me.RadInputManager2.GetSettingByBehaviorID("regChargeAmount")


            setting.MinValue = 0.001

            setting.MaxValue = 1000

            setting.ErrorMessage = "Please enter numeric values"


            setting.Type = Telerik.Web.UI.NumericType.Number



        Else


            Me.lblChargeAmountValue.Text = "Charge Percentage"


            Dim setting As Telerik.Web.UI.NumericTextBoxSetting

            setting = Me.RadInputManager2.GetSettingByBehaviorID("regChargeAmount")


            setting.MinValue = 0.001

            setting.MaxValue = 100

            setting.ErrorMessage = "Percentage must be less than 100"


            setting.Type = Telerik.Web.UI.NumericType.Percent






        End If

    End Sub


End Class
