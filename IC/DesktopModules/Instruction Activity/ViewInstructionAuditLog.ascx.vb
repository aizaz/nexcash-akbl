﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_InstructionAuditLog_InstructionAuditLog
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then

                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlCompany()
                ClearAllTextBoxes()
                btnExport.Visible = False
                'LoadgvInstructionActivity(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Instruction Audit Log")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetCompanyActiveAndApproveByGroupCode(ddlGroup.SelectedValue.ToString)
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlGroup()
         Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveAndApproveGroups
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadgvInstructionActivity(ByVal DoDataBind As Boolean)
        Try
            Dim GroupCode, CompanyCode, ToInstructionID, FromInstructionID As String
            Dim FromDate, ToDate As String

            FromInstructionID = ""
            ToInstructionID = ""
            GroupCode = ""
            CompanyCode = ""
            If txtToInstructionID.Text <> "" And txtFromInstructionID.Text <> "" Then
                If CInt(txtToInstructionID.Text) < CInt(txtFromInstructionID.Text) Then
                    UIUtilities.ShowDialog(Me, "Error", "To Instruction ID should be greater from Instruction ID", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                Else
                    ToInstructionID = txtToInstructionID.Text
                    FromInstructionID = txtFromInstructionID.Text
                End If
            ElseIf (txtToInstructionID.Text = "" Or txtToInstructionID.Text Is Nothing) And (txtFromInstructionID.Text <> "" Or Not txtFromInstructionID.Text Is Nothing) Then
                FromInstructionID = txtFromInstructionID.Text
            ElseIf (txtToInstructionID.Text <> "" Or Not txtToInstructionID.Text Is Nothing) And (txtFromInstructionID.Text = "" Or txtFromInstructionID.Text Is Nothing) Then
                ToInstructionID = txtToInstructionID.Text
            End If
         
            If ddlGroup.SelectedValue.ToString <> "0" Then
                GroupCode = ddlGroup.SelectedValue.ToString
            End If
            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            End If
          
            ICInstructionAuditLogController.GetAllInstructionsAuditLogForGV(FromInstructionID, ToInstructionID, CompanyCode, GroupCode, Me.gvInstructionActivity.CurrentPageIndex + 1, Me.gvInstructionActivity.PageSize, Me.gvInstructionActivity, DoDataBind)
            If gvInstructionActivity.Items.Count > 0 Then
                gvInstructionActivity.Visible = True
                lblNRF.Visible = False
                btnExport.Visible = CBool(htRights("Export"))
            Else
                lblNRF.Visible = True
                gvInstructionActivity.Visible = False
                btnExport.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadgvInstructionActivity(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub ClearAllTextBoxes()
        txtFromInstructionID.Text = ""
        txtToInstructionID.Text = ""

    End Sub

    Protected Sub gvInstructionActivity_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvInstructionActivity.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvInstructionActivity.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Function CheckgvClientRoleCheckedForProcessAll() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvInstructionActivity.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub gvInstructionActivity_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvInstructionActivity.NeedDataSource
        Try
            LoadgvInstructionActivity(False)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        If Page.IsValid Then
            Try
                LoadgvInstructionActivity(True)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
            LoadgvInstructionActivity(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnExport_Click(sender As Object, e As System.EventArgs) Handles btnExport.Click



        gvInstructionActivity.ExportSettings.ExportOnlyData = True
            gvInstructionActivity.MasterTableView.AllowPaging = False
            gvInstructionActivity.ExportSettings.IgnorePaging = True
            gvInstructionActivity.PageSize = 500

            gvInstructionActivity.Rebind()

            gvInstructionActivity.ExportSettings.OpenInNewWindow = True
            gvInstructionActivity.ExportSettings.FileName = "Al Baraka Instruction Audit Trail " & Date.Now.ToString("dd-MMM-yyyy")
            gvInstructionActivity.MasterTableView.ExportToExcel()

            LoadgvInstructionActivity(True)


    End Sub

   
End Class

