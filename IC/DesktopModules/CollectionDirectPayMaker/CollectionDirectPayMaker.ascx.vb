﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_CollectionDirectPayMaker_CollectionDirectPayMaker
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
    Private AssignUserRolsID As New ArrayList


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()
            If Page.IsPostBack = False Then
                If Me.UserInfo.IsSuperUser = False Then
                    'btnUpDate.Attributes.Item("onclick") = "if (Page_ClientValidate('Search')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnUpDate, Nothing).ToString() & " } "

                    ViewState("htRights") = Nothing
                    GetPageAccessRights()
                    LoadddlCompany()
                    LoadddlCollectionNature()
                    tblMain.Style.Remove("display")
                    tblReferenceField.Style.Add("display", "none")
                    tblResult.Style.Add("display", "none")
                    tblAmountAndCompanyDetails.Style.Add("display", "none")
                    btnUpDate.Visible = False
                    btnBack2.Visible = False
                Else
                    UIUtilities.ShowDialog(Me, "Direct Pay Maker", "Sorry! You are not authorized to login.", Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        AssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Direct Pay Maker") = True Then
                    If Not AssignUserRolsID.Contains(RoleID.ToString) Then
                        AssignUserRolsID.Add(RoleID.ToString)
                    End If
                End If
            Next
        End If
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Direct Pay Maker")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllCompanyActiveApproveAndCollectionAllowed()
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlPaymentCompany()
        Try
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, "", AssignUserRolsID)
            ddlCompanyPayment.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlCompanyPayment.Items.Clear()
                    ddlCompanyPayment.Items.Add(lit)
                    lit.Selected = True
                    ddlCompanyPayment.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then
                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompanyPayment.Items.Clear()
                ddlCompanyPayment.Items.Add(lit)
                ddlCompanyPayment.AppendDataBoundItems = True
                ddlCompanyPayment.DataSource = dt
                ddlCompanyPayment.DataTextField = "CompanyName"
                ddlCompanyPayment.DataValueField = "CompanyCode"
                ddlCompanyPayment.DataBind()
                ddlCompanyPayment.Enabled = True
            Else
                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompanyPayment.Items.Clear()
                ddlCompanyPayment.Items.Add(lit)
                ddlCompanyPayment.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccountPayment()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlPayAccount.Items.Clear()
            ddlPayAccount.Items.Add(lit)
            lit.Selected = True
            ddlPayAccount.AppendDataBoundItems = True
            ddlPayAccount.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForDropDown(Me.UserId.ToString, ddlCompanyPayment.SelectedValue.ToString, AssignUserRolsID)
            ddlPayAccount.DataTextField = "AccountAndPaymentNature"
            ddlPayAccount.DataValueField = "AccountPaymentNature"
            ddlPayAccount.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCollectionNature()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCollNature.Items.Clear()
            ddlCollNature.Items.Add(lit)
            ddlCollNature.AppendDataBoundItems = True
            ddlCollNature.DataSource = ICCollectionNatureController.GetAllActiveCollectionNatures(ddlCompany.SelectedValue.ToString)
            ddlCollNature.DataTextField = "CollectionNatureName"
            ddlCollNature.DataValueField = "CollectionNatureCode"
            ddlCollNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        LoadddlCollectionNature()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(41))
    End Sub
    Private Sub Clear()

        LoadddlCompany()
        LoadddlCollectionNature()

    End Sub
    Protected Sub ddlCollNature_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCollNature.SelectedIndexChanged
        Try
            If ddlCollNature.SelectedValue.ToString <> "0" Then
                Dim objICCollectionNature As New ICCollectionNature
                objICCollectionNature.LoadByPrimaryKey(ddlCollNature.SelectedValue.ToString)
                ViewState("CollectionAccounts") = Nothing
                ViewState("CollectionAccounts") = ICCollectionAccountsCollectionNatureController.GetCollectionAccountsByCollectionNature(objICCollectionNature.CollectionNatureCode.ToString)
                If DirectCast(ViewState("CollectionAccounts"), DataTable).Rows.Count = 0 Then
                    UIUtilities.ShowDialog(Me, "Direct Pay Maker", "No collection account is tagged with selected collection nature", Dialogmessagetype.Failure, NavigateURL(186))
                    Exit Sub
                End If
                If objICCollectionNature.IsInvoiceDBAvailable = True Then
                    DisableReferenceFieldValidation()
                    tblMain.Style.Add("display", "none")
                    tblResult.Style.Add("display", "none")
                    tblReferenceField.Style.Remove("display")
                    SetReferenceFieldVisibilityByCollectionNature(objICCollectionNature)
                    tblAmountAndCompanyDetails.Style.Add("display", "none")
                    txtAmount.Text = Nothing
                    RadAmountComment.Enabled = False
                    btnSearch.Visible = CBool(htRights("Search"))
                    btnUpDate.Visible = False
                    btnBack.Visible = True
                    btnBack2.Visible = False
                Else
                    tblMain.Style.Add("display", "none")
                    DisableReferenceFieldValidation()
                    tblReferenceField.Style.Remove("display")
                    SetReferenceFieldVisibilityByCollectionNature(objICCollectionNature)
                    LoadddlPaymentCompany()
                    LoadddlAccountPayment()
                    tblResult.Style.Add("display", "none")
                    gvInvoiceRecords.DataSource = Nothing
                    gvInvoiceRecords.DataBind()
                    gvInvoiceRecords.Visible = False
                    tblAmountAndCompanyDetails.Style.Remove("display")
                    txtAmount.Text = Nothing
                    RadAmountComment.Enabled = True
                    btnBack.Visible = False
                    btnBack2.Visible = True
                    btnSearch.Visible = False
                    btnUpDate.Visible = CBool(htRights("Make"))
                End If
            Else
                DisableReferenceFieldValidation()
                tblReferenceField.Style.Add("display", "none")
                tblResult.Style.Add("display", "none")
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub DisableReferenceFieldValidation()
        DirectCast(Me.Control.FindControl("trReferenceField1"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField2"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField3"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField4"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField5"), HtmlTableRow).Style.Add("display", "none")
        rfvReferenceField1.Enabled = False
        rfvReferenceField2.Enabled = False
        rfvReferenceField3.Enabled = False
        rfvReferenceField4.Enabled = False
        rfvReferenceField5.Enabled = False
        RadInputReferenceField1.Enabled = False
        RadInputReferenceField2.Enabled = False
        RadInputReferenceField3.Enabled = False
        RadInputReferenceField4.Enabled = False
        RadInputReferenceField5.Enabled = False
        txtReferenceField1.Text = ""
        txtReferenceField2.Text = ""
        txtReferenceField3.Text = ""
        txtReferenceField4.Text = ""
        txtReferenceField5.Text = ""
    End Sub
    Private Sub SetReferenceFieldVisibilityByCollectionNature(ByVal objICCollectionNature As ICCollectionNature)
        Dim objICReferencFieldsCollection As New ICCollectionInvoiceDataTemplateFieldsCollection
        objICReferencFieldsCollection = ICCollectionInvoiceDataTemplateFieldsListController.GetReferenceFieldsCollectionInvoiceTemplateOfDualFile(objICCollectionNature.InvoiceTemplateID.ToString)
        If objICCollectionNature.IsInvoiceDBAvailable = True Then
            If objICCollectionNature.RefField1Caption IsNot Nothing Then
                If objICCollectionNature.RefField1RequiredforSearching = True Then
                    lblReferenceField1.Text = objICCollectionNature.RefField1Caption
                    DirectCast(Me.Control.FindControl("trReferenceField1"), HtmlTableRow).Style.Remove("display")
                    RadInputReferenceField1.Enabled = True
                    rfvReferenceField1.Enabled = True
                    rfvReferenceField1.ErrorMessage = ""
                    rfvReferenceField1.ValidationGroup = "Search"
                    txtReferenceField1.MaxLength = objICReferencFieldsCollection(0).FieldLength
                    If objICReferencFieldsCollection(0).FieldDBType = "Integer" Then
                        rfvReferenceField1.ValidationExpression = "^[0-9]*$"
                        rfvReferenceField1.ErrorMessage = "Only numbers are allowed."
                    ElseIf objICReferencFieldsCollection(0).FieldDBType = "Decimal" Then
                        rfvReferenceField1.ValidationExpression = "^\d+\.\d{0,2}$"
                        rfvReferenceField1.ErrorMessage = "Only decimal values are allowed."
                    ElseIf objICReferencFieldsCollection(0).FieldDBType = "String" Then
                        rfvReferenceField1.Enabled = False
                    End If
                    If objICCollectionNature.RefField1RequiredforInput = True Then
                        RadInputReferenceField1.GetSettingByBehaviorID("reReferenceFieldOne").Validation.IsRequired = True
                    End If
                    RadInputReferenceField1.GetSettingByBehaviorID("reReferenceFieldOne").Validation.ValidationGroup = "Search"
                End If
            End If
            If objICCollectionNature.RefField2Caption IsNot Nothing Then
                If objICCollectionNature.RefField2RequiredforSearching = True Then
                    lblReferenceField2.Text = objICCollectionNature.RefField2Caption
                    DirectCast(Me.Control.FindControl("trReferenceField2"), HtmlTableRow).Style.Remove("display")
                    RadInputReferenceField2.Enabled = True
                    rfvReferenceField2.Enabled = True
                    rfvReferenceField2.ErrorMessage = ""
                    rfvReferenceField2.ValidationGroup = "Search"
                    txtReferenceField2.MaxLength = objICReferencFieldsCollection(1).FieldLength
                    If objICReferencFieldsCollection(1).FieldDBType = "Integer" Then
                        rfvReferenceField2.ValidationExpression = "^[0-9]*$"
                        rfvReferenceField2.ErrorMessage = "Only numbers are allowed."
                    ElseIf objICReferencFieldsCollection(1).FieldDBType = "Decimal" Then
                        rfvReferenceField2.ValidationExpression = "^\d+\.\d{0,2}$"
                        rfvReferenceField2.ErrorMessage = "Only decimal values are allowed."
                    ElseIf objICReferencFieldsCollection(1).FieldDBType = "String" Then
                        rfvReferenceField2.Enabled = False
                    End If
                    If objICCollectionNature.RefField2RequiredforInput = True Then
                        RadInputReferenceField2.GetSettingByBehaviorID("reReferenceFieldTwo").Validation.IsRequired = True
                    End If
                    RadInputReferenceField2.GetSettingByBehaviorID("reReferenceFieldTwo").Validation.ValidationGroup = "Search"
                End If
            End If
            If objICCollectionNature.RefField3Caption IsNot Nothing Then
                If objICCollectionNature.RefField3RequiredforSearching = True Then
                    lblReferenceField3.Text = objICCollectionNature.RefField3Caption
                    DirectCast(Me.Control.FindControl("trReferenceField3"), HtmlTableRow).Style.Remove("display")
                    RadInputReferenceField3.Enabled = True
                    rfvReferenceField3.Enabled = True
                    rfvReferenceField3.ErrorMessage = ""
                    rfvReferenceField3.ValidationGroup = "Search"
                    txtReferenceField3.MaxLength = objICReferencFieldsCollection(2).FieldLength
                    If objICReferencFieldsCollection(2).FieldDBType = "Integer" Then
                        rfvReferenceField3.ValidationExpression = "^[0-9]*$"
                        rfvReferenceField3.ErrorMessage = "Only numbers are allowed."
                    ElseIf objICReferencFieldsCollection(2).FieldDBType = "Decimal" Then
                        rfvReferenceField3.ValidationExpression = "^\d+\.\d{0,2}$"
                        rfvReferenceField3.ErrorMessage = "Only decimal values are allowed."
                    ElseIf objICReferencFieldsCollection(2).FieldDBType = "String" Then
                        rfvReferenceField3.Enabled = False
                    End If
                    If objICCollectionNature.RefField3RequiredforInput = True Then
                        RadInputReferenceField3.GetSettingByBehaviorID("reReferenceFieldThree").Validation.IsRequired = True
                    End If
                    RadInputReferenceField3.GetSettingByBehaviorID("reReferenceFieldThree").Validation.ValidationGroup = "Search"
                End If
            End If
            If objICCollectionNature.RefField4Caption IsNot Nothing Then
                If objICCollectionNature.RefField4RequiredforSearching = True Then
                    lblReferenceField4.Text = objICCollectionNature.RefField4Caption
                    DirectCast(Me.Control.FindControl("trReferenceField4"), HtmlTableRow).Style.Remove("display")
                    RadInputReferenceField4.Enabled = True
                    rfvReferenceField4.Enabled = True
                    rfvReferenceField4.ErrorMessage = ""
                    rfvReferenceField4.ValidationGroup = "Search"
                    txtReferenceField4.MaxLength = objICReferencFieldsCollection(3).FieldLength
                    If objICReferencFieldsCollection(3).FieldDBType = "Integer" Then
                        rfvReferenceField4.ValidationExpression = "^[0-9]*$"
                        rfvReferenceField4.ErrorMessage = "Only numbers are allowed."
                    ElseIf objICReferencFieldsCollection(3).FieldDBType = "Decimal" Then
                        rfvReferenceField4.ValidationExpression = "^\d+\.\d{0,2}$"
                        rfvReferenceField4.ErrorMessage = "Only decimal values are allowed."
                    ElseIf objICReferencFieldsCollection(3).FieldDBType = "String" Then
                        rfvReferenceField4.Enabled = False
                    End If
                    If objICCollectionNature.RefField4RequiredforInput = True Then
                        RadInputReferenceField4.GetSettingByBehaviorID("reReferenceFieldFour").Validation.IsRequired = True
                    End If
                    RadInputReferenceField4.GetSettingByBehaviorID("reReferenceFieldFour").Validation.ValidationGroup = "Search"
                End If
            End If
            If objICCollectionNature.RefField5Caption IsNot Nothing Then
                If objICCollectionNature.RefField5RequiredforSearching = True Then
                    lblReferenceField5.Text = objICCollectionNature.RefField5Caption
                    DirectCast(Me.Control.FindControl("trReferenceField5"), HtmlTableRow).Style.Remove("display")
                    RadInputReferenceField5.Enabled = True
                    rfvReferenceField5.ErrorMessage = ""
                    rfvReferenceField5.Enabled = True
                    rfvReferenceField5.ValidationGroup = "Search"
                    txtReferenceField5.MaxLength = objICReferencFieldsCollection(4).FieldLength
                    If objICReferencFieldsCollection(4).FieldDBType = "Integer" Then
                        rfvReferenceField5.ValidationExpression = "^[0-9]*$"
                        rfvReferenceField5.ErrorMessage = "Only numbers are allowed."
                    ElseIf objICReferencFieldsCollection(4).FieldDBType = "Decimal" Then
                        rfvReferenceField5.ValidationExpression = "^\d+\.\d{0,2}$"
                        rfvReferenceField5.ErrorMessage = "Only decimal values are allowed."
                    ElseIf objICReferencFieldsCollection(4).FieldDBType = "String" Then
                        rfvReferenceField5.Enabled = False
                    End If
                    If objICCollectionNature.RefField5RequiredforInput = True Then
                        RadInputReferenceField5.GetSettingByBehaviorID("reReferenceFieldFive").Validation.IsRequired = True
                    End If
                    RadInputReferenceField5.GetSettingByBehaviorID("reReferenceFieldFive").Validation.ValidationGroup = "Search"
                End If
            End If
        Else

            For i = 0 To objICReferencFieldsCollection.Count - 1
                Select Case i.ToString
                    Case "0"
                        lblReferenceField1.Text = objICReferencFieldsCollection(i).FieldName
                        DirectCast(Me.Control.FindControl("trReferenceField1"), HtmlTableRow).Style.Remove("display")
                        RadInputReferenceField1.Enabled = True
                        rfvReferenceField1.Enabled = True
                        'rfvReferenceField1.ValidationGroup = "Pay"
                        txtReferenceField1.MaxLength = objICReferencFieldsCollection(i).FieldLength
                        If objICReferencFieldsCollection(i).FieldDBType = "Integer" Then
                            rfvReferenceField1.ValidationExpression = "^[0-9]*$"
                            rfvReferenceField1.ErrorMessage = "Only numbers are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "Decimal" Then
                            rfvReferenceField1.ValidationExpression = "^\d+\.\d{0,2}$"
                            rfvReferenceField1.ErrorMessage = "Only decimal values are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "String" Then
                            rfvReferenceField1.Enabled = False
                        End If
                        RadInputReferenceField1.GetSettingByBehaviorID("reReferenceFieldOne").Validation.IsRequired = True
                        'RadInputReferenceField1.GetSettingByBehaviorID("reReferenceFieldOne").Validation.ValidationGroup = "Pay"
                    Case "1"
                        lblReferenceField2.Text = objICReferencFieldsCollection(i).FieldName
                        DirectCast(Me.Control.FindControl("trReferenceField2"), HtmlTableRow).Style.Remove("display")
                        RadInputReferenceField2.Enabled = True
                        rfvReferenceField2.Enabled = True
                        rfvReferenceField2.ValidationGroup = "Pay"
                        txtReferenceField2.MaxLength = objICReferencFieldsCollection(i).FieldLength
                        If objICReferencFieldsCollection(i).FieldDBType = "Integer" Then
                            rfvReferenceField2.ValidationExpression = "^[0-9]*$"
                            rfvReferenceField2.ErrorMessage = "Only numbers are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "Decimal" Then
                            rfvReferenceField2.ValidationExpression = "^\d+\.\d{0,2}$"
                            rfvReferenceField2.ErrorMessage = "Only decimal values are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "String" Then
                            rfvReferenceField2.Enabled = False
                        End If
                        RadInputReferenceField2.GetSettingByBehaviorID("reReferenceFieldTwo").Validation.IsRequired = True
                        'RadInputReferenceField2.GetSettingByBehaviorID("reReferenceFieldTwo").Validation.ValidationGroup = "Pay"
                    Case "2"
                        lblReferenceField3.Text = objICReferencFieldsCollection(i).FieldName
                        DirectCast(Me.Control.FindControl("trReferenceField3"), HtmlTableRow).Style.Remove("display")
                        RadInputReferenceField3.Enabled = True
                        rfvReferenceField3.Enabled = True
                        'rfvReferenceField3.ValidationGroup = "Pay"
                        txtReferenceField3.MaxLength = objICReferencFieldsCollection(i).FieldLength
                        If objICReferencFieldsCollection(i).FieldDBType = "Integer" Then
                            rfvReferenceField3.ValidationExpression = "^[0-9]*$"
                            rfvReferenceField3.ErrorMessage = "Only numbers are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "Decimal" Then
                            rfvReferenceField3.ValidationExpression = "^\d+\.\d{0,2}$"
                            rfvReferenceField3.ErrorMessage = "Only decimal values are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "String" Then
                            rfvReferenceField3.Enabled = False
                        End If
                        RadInputReferenceField3.GetSettingByBehaviorID("reReferenceFieldThree").Validation.IsRequired = True
                        'RadInputReferenceField3.GetSettingByBehaviorID("reReferenceFieldThree").Validation.ValidationGroup = "Pay"
                    Case "3"
                        lblReferenceField4.Text = objICReferencFieldsCollection(i).FieldName
                        DirectCast(Me.Control.FindControl("trReferenceField4"), HtmlTableRow).Style.Remove("display")
                        RadInputReferenceField4.Enabled = True
                        rfvReferenceField4.Enabled = True
                        'rfvReferenceField4.ValidationGroup = "Pay"
                        txtReferenceField4.MaxLength = objICReferencFieldsCollection(i).FieldLength
                        If objICReferencFieldsCollection(i).FieldDBType = "Integer" Then
                            rfvReferenceField4.ValidationExpression = "^[0-9]*$"
                            rfvReferenceField4.ErrorMessage = "Only numbers are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "Decimal" Then
                            rfvReferenceField4.ValidationExpression = "^\d+\.\d{0,2}$"
                            rfvReferenceField4.ErrorMessage = "Only decimal values are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "String" Then
                            rfvReferenceField4.Enabled = False
                        End If
                        RadInputReferenceField4.GetSettingByBehaviorID("reReferenceFieldFour").Validation.IsRequired = True
                        'RadInputReferenceField4.GetSettingByBehaviorID("reReferenceFieldFour").Validation.ValidationGroup = "Pay"
                    Case "4"
                        lblReferenceField5.Text = objICReferencFieldsCollection(i).FieldName
                        DirectCast(Me.Control.FindControl("trReferenceField5"), HtmlTableRow).Style.Remove("display")
                        RadInputReferenceField5.Enabled = True
                        rfvReferenceField5.Enabled = True
                        'rfvReferenceField5.ValidationGroup = "Pay"
                        txtReferenceField5.MaxLength = objICReferencFieldsCollection(i).FieldLength
                        If objICReferencFieldsCollection(i).FieldDBType = "Integer" Then
                            rfvReferenceField5.ValidationExpression = "^[0-9]*$"
                            rfvReferenceField5.ErrorMessage = "Only numbers are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "Decimal" Then
                            rfvReferenceField5.ValidationExpression = "^\d+\.\d{0,2}$"
                            rfvReferenceField5.ErrorMessage = "Only decimal values are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "String" Then
                            rfvReferenceField5.Enabled = False
                        End If
                        RadInputReferenceField5.GetSettingByBehaviorID("reReferenceFieldFive").Validation.IsRequired = True
                        'RadInputReferenceField5.GetSettingByBehaviorID("reReferenceFieldFive").Validation.ValidationGroup = "Pay"
                End Select
            Next
        End If
    End Sub
    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Try
            Response.Redirect(NavigateURL(186))
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If Page.IsValid Then
            Try

                tblReferenceField.Style.Add("display", "none")
                tblResult.Style.Remove("display")
                LoadddlPaymentCompany()
                LoadddlAccountPayment()
                Dim objICCollectionInvoiceDBTemplate As New ICCollectionInvoiceDataTemplate
                Dim objICCollectionNature As New ICCollectionNature
                objICCollectionNature.LoadByPrimaryKey(ddlCollNature.SelectedValue.ToString)
                objICCollectionInvoiceDBTemplate.LoadByPrimaryKey(objICCollectionNature.InvoiceTemplateID.ToString)
                Dim ReferenceField1, ReferenceField2, ReferenceField3, ReferenceField4, ReferenceField5 As String
                Dim dt As New DataTable
                Dim dtFinal As New DataTable
                Dim drFinal As DataRow
                If txtReferenceField1.Text <> "" Then
                    ReferenceField1 = txtReferenceField1.Text
                Else
                    ReferenceField1 = Nothing
                End If
                If txtReferenceField2.Text <> "" Then
                    ReferenceField2 = txtReferenceField2.Text
                Else
                    ReferenceField2 = Nothing
                End If
                If txtReferenceField3.Text <> "" Then
                    ReferenceField3 = txtReferenceField3.Text
                Else
                    ReferenceField3 = Nothing
                End If
                If txtReferenceField4.Text <> "" Then
                    ReferenceField4 = txtReferenceField4.Text
                Else
                    ReferenceField4 = Nothing
                End If
                If txtReferenceField5.Text <> "" Then
                    ReferenceField5 = txtReferenceField5.Text
                Else
                    ReferenceField5 = Nothing
                End If
                dt = ICCollectionSQLController.GetInvoiceRecordsByReferenceFields(objICCollectionInvoiceDBTemplate, ReferenceField1, ReferenceField2, ReferenceField3, ReferenceField4, ReferenceField5)


                If dt.Rows.Count > 0 Then
                    If dt.Rows.Count = 1 Then
                        If dt.Rows(0)("Status").ToString = "Paid" Then
                            UIUtilities.ShowDialog(Me, "Direct Pay Maker", "Invoice already paid", Dialogmessagetype.Failure, NavigateURL())
                            Exit Sub
                        Else
                            dtFinal = dt.Clone

                            For Each dr As DataRow In dt.Rows
                                If dr("Status") = "Pending" Then
                                    drFinal = dtFinal.NewRow
                                    For i = 0 To dr.Table.Columns.Count - 1
                                        drFinal(i) = dr(i).ToString
                                    Next
                                    dtFinal.Rows.Add(drFinal)
                                End If
                            Next
                        End If
                    Else

                        dtFinal = dt.Clone

                        For Each dr As DataRow In dt.Rows
                            If dr("Status") = "Pending" Then
                                drFinal = dtFinal.NewRow
                                For i = 0 To dr.Table.Columns.Count - 1
                                    drFinal(i) = dr(i).ToString
                                Next
                                dtFinal.Rows.Add(drFinal)
                            End If
                        Next
                    End If
                    If dtFinal.Rows.Count > 0 Then
                        tblAmountAndCompanyDetails.Style.Remove("display")
                        txtAmount.Text = Nothing
                        RadAmountComment.Enabled = True
                        gvInvoiceRecords.DataSource = Nothing
                        gvInvoiceRecords.DataSource = dtFinal
                        gvInvoiceRecords.DataBind()
                        SetAmountTextBoxValue(gvInvoiceRecords.Items(0), objICCollectionNature)
                        btnUpDate.Visible = CBool(htRights("Make"))
                    Else
                        gvInvoiceRecords.Visible = False
                        btnUpDate.Visible = False
                        UIUtilities.ShowDialog(Me, "Direct Pay Maker", "No Record Found<br />1. Invoice Record is already paid <br />2. Invoice record do not exists <br />3. Invoice record is not in Pending status", Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                        'Throw New Exception("No Record Found<br />1. Invoice Record is already paid <br />2. Invoice record do not exists <br />3. Invoice record is not in Pending status")
                    End If
                Else

                    gvInvoiceRecords.Visible = False
                    btnUpDate.Visible = False
                    UIUtilities.ShowDialog(Me, "Direct Pay Maker", "No Record Found", Dialogmessagetype.Failure, NavigateURL)
                    Exit Sub

                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure, NavigateURL(186))
            End Try
        End If
    End Sub
    Private Sub SetAmountTextBoxValue(ByVal gvRow As GridDataItem, ByVal objICCollectionNature As ICCollectionNature)
        If CDate(gvRow.Item(objICCollectionNature.UpToICCollectionInvoiceDataTemplateFieldsByDueDateFieldID.FieldName).Text) >= Now.Date Then
            txtAmount.Text = gvRow.Item(objICCollectionNature.UpToICCollectionInvoiceDataTemplateFieldsByAmountBeforeDueDateFieldID.FieldName).Text
            If objICCollectionNature.IsPartialCollectionAllowed = True Then
                txtAmount.ReadOnly = False
            Else
                txtAmount.ReadOnly = True
            End If
        Else
            txtAmount.Text = gvRow.Item(objICCollectionNature.UpToICCollectionInvoiceDataTemplateFieldsByAmountAfterDueDateFieldID.FieldName).Text
            If objICCollectionNature.IsPartialCollectionAllowed = True Then
                txtAmount.ReadOnly = False
            Else
                txtAmount.ReadOnly = True
            End If
        End If
    End Sub
    Protected Sub gvInvoiceRecords_ItemCreated(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles gvInvoiceRecords.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvInvoiceRecords.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub gvInvoiceRecords_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles gvInvoiceRecords.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvInvoiceRecords.MasterTableView.ClientID & "','0');"
        End If
    End Sub
    Protected Sub btnBack2_Click(sender As Object, e As EventArgs) Handles btnBack2.Click
        Try
            Response.Redirect(NavigateURL(186))
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckgvBankBranchForProcessAll() As Boolean
        Try
            Dim rowgvBankBranch As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvBankBranch In gvInvoiceRecords.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvBankBranch.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function CheckValidStatus(ByVal ToStatus As String) As String
        Try
            Dim rowgvBankBranch As GridDataItem
            Dim RowCount As Integer = 0
            Dim Result As Boolean = False
            Dim Msg As String = "OK"
            For Each rowgvBankBranch In gvInvoiceRecords.Items
                RowCount = RowCount + 1
                If rowgvBankBranch.GetDataKeyValue("Status") = ToStatus Then
                    Msg += "From and To Satus are same in row " & RowCount & "<br />"
                    Return Msg
                    Exit Function
                End If
            Next
            Return Msg
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnUpDate_Click(sender As Object, e As EventArgs) Handles btnUpDate.Click
        If Page.IsValid Then
            Try
                Dim objICCollectionNature As New ICCollectionNature
                Dim objICCollections As ICCollections
                Dim TemplateType As String = Nothing
                Dim Ref1, Ref2, Ref3, Ref4, Ref5 As String
                Dim MasterSql As String = Nothing
                Dim ActionString As String = ""
                Dim CollectionID As Integer
                Dim ClientAccountStatus As String = Nothing
                Dim CollectionAccountStatus As String = Nothing
                Dim AccountBalance As Double = 0
                Dim CollectionAccount As String = ""
                Dim RowCount As Integer = 0
                Dim CheckSelect As CheckBox
                Dim dtAmountAndDueDate As New DataTable
                Dim FailCount As Integer = 0
                Dim PassCount As Integer = 0
                Dim FailErrorMessage As String = Nothing
                Dim FailErrorMessage2 As String = Nothing
                Dim dr As DataRow
                Ref1 = Nothing
                Ref2 = Nothing
                Ref3 = Nothing
                Ref4 = Nothing
                Ref5 = Nothing
                objICCollectionNature.LoadByPrimaryKey(ddlCollNature.SelectedValue.ToString)
                If ddlCompanyPayment.SelectedValue.ToString = "0" Then
                    UIUtilities.ShowDialog(Me, "Direct Pay Maker", "Please select Company", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If ddlPayAccount.SelectedValue.ToString = "0" Then
                    UIUtilities.ShowDialog(Me, "Direct Pay Maker", "Please select debit account", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If txtAmount.Text = "" Then
                    UIUtilities.ShowDialog(Me, "Direct Pay Maker", "Please enter amount", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                ''Validate


                If objICCollectionNature.IsInvoiceDBAvailable = True Then
                    RowCount = 0
                    If CheckgvBankBranchForProcessAll() = False Then
                        UIUtilities.ShowDialog(Me, "Direct Pay Maker", "Please select atleast one(1) record", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    For Each gvInvoiceRecordRow As GridDataItem In gvInvoiceRecords.Items
                        RowCount = RowCount + 1
                        CheckSelect = New CheckBox
                        CheckSelect = DirectCast(gvInvoiceRecordRow.FindControl("chkSelect"), CheckBox)
                        If CheckSelect.Checked = True Then
                            Ref1 = Nothing
                            Ref2 = Nothing
                            Ref3 = Nothing
                            Ref4 = Nothing
                            Ref5 = Nothing
                            SetReferenceFieldValuesByTemplateFieldsByGridRow(Ref1, Ref2, Ref3, Ref4, Ref5, gvInvoiceRecordRow)
                            FailErrorMessage2 = Nothing
                            FailErrorMessage2 = ICCollectionsController.VerifyEnteredAmount(objICCollectionNature, Ref1, Ref2, Ref3, Ref4, Ref5, CDbl(txtAmount.Text), RowCount)
                            If FailErrorMessage2 = "OK" Then
                                PassCount = PassCount + 1
                            Else
                                FailCount = FailCount + 1
                                FailErrorMessage += FailErrorMessage2
                            End If
                        End If
                    Next
                End If
                If Not PassCount = 0 And Not FailCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Direct Pay Maker", "[ " & PassCount.ToString & " ] Invoice(s) are valid.<br /> [ " & FailCount.ToString & " ] Following Invoices are not valid due to following reason(s):<br />" & FailErrorMessage, ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                ElseIf PassCount = 0 And Not FailCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Direct Pay Maker", "[ " & FailCount.ToString & " ] Following Invoice(s) are not valid due to following reason(s): <br />" & FailErrorMessage, ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Try
                    ClientAccountStatus = CBUtilities.AccountStatus(ddlPayAccount.SelectedValue.ToString.Split("-")(0), CBUtilities.AccountType.RB, "Collection Debit Account", ddlPayAccount.SelectedValue.ToString.Split("-")(0), "Debit", "Direct Pay Maker")
                    If ClientAccountStatus <> "Active" Then
                        Throw New Exception("Invalid Payable account's [ " & ddlPayAccount.SelectedValue.ToString.Split("-")(0) & " ] status <br />" & ClientAccountStatus)
                    End If
                Catch ex As Exception
                    Throw ex
                End Try
                RowCount = 0
                For Each drCollAccount As DataRow In DirectCast(ViewState("CollectionAccounts"), DataTable).Rows
                    Try

                        RowCount = RowCount + 1
                        CollectionAccount = Nothing
                        CollectionAccount = drCollAccount("Value").ToString.Split("-")(0)
                        CollectionAccountStatus = CBUtilities.AccountStatus(drCollAccount("Value").ToString.Split("-")(0), CBUtilities.AccountType.RB, "Collection Credit Account", drCollAccount("Value").ToString.Split("-")(0), "Credit", "Direct Pay Maker")
                        If CollectionAccountStatus = "Active" Then
                            Exit For
                        End If
                    Catch ex As Exception
                        Continue For
                    End Try
                Next
                If CollectionAccountStatus <> "Active" Then
                    UIUtilities.ShowDialog(Me, "Direct Pay Maker", "Invalid tagged Collection account status", Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Try
                    AccountBalance = CBUtilities.BalanceInquiry(ddlPayAccount.SelectedValue.ToString.Split("-")(0), CBUtilities.AccountType.RB, "Collection Debit Account", ddlPayAccount.SelectedValue.ToString.Split("-")(0))
                    If AccountBalance <> -1 Then
                        If AccountBalance < CDbl(txtAmount.Text) Then
                            Throw New Exception("Insufficient Collection dedit Account's [ " & ddlPayAccount.SelectedValue.ToString.Split("-")(0) & " ] balance [ " & AccountBalance.ToString("N2") & " ]")
                        End If
                    Else
                        Throw New Exception("Failed to Collection dedit Account's [ " & ddlPayAccount.SelectedValue.ToString.Split("-")(0) & " ] balance inquiry")
                    End If
                Catch ex As Exception
                    Throw ex
                End Try


                If objICCollectionNature.IsInvoiceDBAvailable = False Then
                    objICCollections = New ICCollections
                    SetReferenceFieldValuesByTemplateFieldsByTextBoxes(Ref1, Ref2, Ref3, Ref4, Ref5, objICCollectionNature.InvoiceTemplateID.ToString)
                    objICCollections = GetCollectionObject(Ref1, Ref2, Ref3, Ref4, Ref5, DirectCast(ViewState("CollectionAccounts"), DataTable).Rows(RowCount - 1), Nothing, Nothing, Nothing)
                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionNature.InvoiceTemplateID.ToString, MasterSql, Me.UserId.ToString, Nothing, "Select", "Master", Ref1, Ref2, Ref3, Ref4, Ref5, Nothing, Nothing)
                    If ICCollectionSQLController.IsInvoiceRecordAlreadyExists(MasterSql) = False Then
                        ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionNature.InvoiceTemplateID.ToString, MasterSql, Me.UserId.ToString, Nothing, "Insert", "Master", Ref1, Ref2, Ref3, Ref4, Ref5, "In Process", "In Process")
                        If ICCollectionSQLController.ExecuteSqlStatementForCreateTableVIATemplate(MasterSql) = True Then
                            CollectionID = ICCollectionsController.AddICCollections(objICCollections, False, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                            UIUtilities.ShowDialog(Me, "Direct Pay Maker", "Collection [ " & CollectionID & " ] added successfully", Dialogmessagetype.Success, NavigateURL(186))
                            Exit Sub
                        End If
                    Else
                        'ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollectionNature.InvoiceTemplateID.ToString, MasterSql, Me.UserId.ToString, Nothing, "Update", "Master", Ref1, Ref2, Ref3, Ref4, Ref5, "In Process", "In Process")
                        'If ICCollectionSQLController.ExecuteSqlStatementForCreateTableVIATemplate(MasterSql) = True Then
                        '    CollectionID = ICCollectionsController.AddICCollections(objICCollections, False, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                        '    UIUtilities.ShowDialog(Me, "Direct Pay Maker", "Collection [ " & CollectionID & " ] added successfully", Dialogmessagetype.Success, NavigateURL(186))
                        '    Exit Sub
                        'End If
                        UIUtilities.ShowDialog(Me, "Direct Pay Maker", "Record already exists.", Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else
                    PassCount = 0
                    FailCount = 0

                    For Each gvInvoiceRecordRow As GridDataItem In gvInvoiceRecords.Items
                        CheckSelect = New CheckBox
                        CheckSelect = DirectCast(gvInvoiceRecordRow.FindControl("chkSelect"), CheckBox)
                        If CheckSelect.Checked = True Then
                            Ref1 = Nothing
                            Ref2 = Nothing
                            Ref3 = Nothing
                            Ref4 = Nothing
                            Ref5 = Nothing
                            SetReferenceFieldValuesByTemplateFieldsByGridRow(Ref1, Ref2, Ref3, Ref4, Ref5, gvInvoiceRecordRow)
                            dtAmountAndDueDate = New DataTable
                            dtAmountAndDueDate = ICCollectionSQLController.GetAmountAndDueDateDataByCollectionNature(objICCollectionNature, Ref1, Ref2, Ref3, Ref4, Ref5)
                            Try
                                objICCollections = GetCollectionObject(Ref1, Ref2, Ref3, Ref4, Ref5, DirectCast(ViewState("CollectionAccounts"), DataTable).Rows(RowCount - 1), CDbl(dtAmountAndDueDate.Rows(0)("AmountAfterDueDate")), CDate(dtAmountAndDueDate.Rows(0)("DueDate")), gvInvoiceRecordRow.Item("FileID").Text.ToString)
                                CollectionID = ICCollectionsController.AddICCollections(objICCollections, False, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                                ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollectionNature.InvoiceTemplateID.ToString, Ref1, Ref2, Ref3, Ref4, Ref5, "Pending", "In Process", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                                PassCount = PassCount + 1
                            Catch ex As Exception
                                FailCount = FailCount + 1
                                'ProcessModuleLoadException(Me, ex, False)
                                Continue For
                            End Try
                        End If
                    Next
                End If
                If Not PassCount = 0 And Not FailCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Direct Pay Maker", "[ " & PassCount.ToString & " ] Invoice(s) added successfully.<br /> [ " & FailCount.ToString & " ] Invoices not added successfully", ICBO.IC.Dialogmessagetype.Success, NavigateURL(186))
                    Exit Sub
                ElseIf Not PassCount = 0 And FailCount = 0 Then

                    UIUtilities.ShowDialog(Me, "Direct Pay Maker", "[ " & PassCount.ToString & " ] Invoice(s) added successfully", ICBO.IC.Dialogmessagetype.Success, NavigateURL(186))
                    Exit Sub
                ElseIf Not PassCount = 0 And FailCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Direct Pay Maker", "[ " & PassCount.ToString & " ] Invoice(s) not added successfully", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(186))
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Sub SetReferenceFieldValuesByTemplateFieldsByTextBoxes(ByRef Ref1 As String, ByRef Ref2 As String, ByRef Ref3 As String, ByRef Ref4 As String, ByRef Ref5 As String, ByVal CollectionNatureCode As String)
        Dim objICInvoiceDBTemplateFields As New ICCollectionInvoiceDataTemplateFieldsCollection

        objICInvoiceDBTemplateFields = ICCollectionInvoiceDataTemplateFieldsListController.GetReferenceFieldsCollectionInvoiceTemplateOfDualFile(CollectionNatureCode)
        For i = 0 To objICInvoiceDBTemplateFields.Count - 1
            Select Case i.ToString
                Case "0"
                    If txtReferenceField1.Text <> "" Then
                        Ref1 = txtReferenceField1.Text.ToString.Trim
                    End If
                Case "1"
                    If txtReferenceField2.Text <> "" Then
                        Ref2 = txtReferenceField2.Text.ToString.Trim
                    End If
                Case "2"
                    If txtReferenceField3.Text <> "" Then
                        Ref3 = txtReferenceField3.Text.ToString.Trim
                    End If
                Case "3"
                    If txtReferenceField4.Text <> "" Then
                        Ref4 = txtReferenceField4.Text.ToString.Trim
                    End If
                Case "4"
                    If txtReferenceField5.Text <> "" Then
                        Ref5 = txtReferenceField5.Text.ToString.Trim
                    End If
            End Select
        Next
    End Sub
    Private Sub SetReferenceFieldValuesByTemplateFieldsByGridRow(ByRef Ref1 As String, ByRef Ref2 As String, ByRef Ref3 As String, ByRef Ref4 As String, ByRef Ref5 As String, ByVal gvRow As GridDataItem)
        Dim objICInvoiceDBTemplateFields As New ICCollectionInvoiceDataTemplateFieldsCollection

        objICInvoiceDBTemplateFields = ICCollectionInvoiceDataTemplateFieldsListController.GetReferenceFieldsCollectionInvoiceTemplateOfDualFile(gvRow.Item("TemplateID").Text.ToString)
        For i = 0 To objICInvoiceDBTemplateFields.Count - 1
            Select Case i.ToString
                Case "0"

                    Ref1 = gvRow.Item(objICInvoiceDBTemplateFields(i).FieldName.ToString).Text.ToString

                Case "1"

                    Ref2 = gvRow.Item(objICInvoiceDBTemplateFields(i).FieldName.ToString).Text.ToString

                Case "2"

                    Ref3 = gvRow.Item(objICInvoiceDBTemplateFields(i).FieldName.ToString).Text.ToString

                Case "3"

                    Ref4 = gvRow.Item(objICInvoiceDBTemplateFields(i).FieldName.ToString).Text.ToString

                Case "4"

                    Ref5 = gvRow.Item(objICInvoiceDBTemplateFields(i).FieldName.ToString).Text.ToString

            End Select
        Next
    End Sub
    Private Function GetCollectionObject(ByVal RefField1 As String, ByVal RefField2 As String, ByVal RefField3 As String, ByVal RefField4 As String, ByVal RefField5 As String, ByVal dr As DataRow, ByVal AmountAfterDueDate As Double, ByVal DueDate As Nullable(Of DateTime), ByVal FileID As String) As ICCollections
        Dim objICCollections As New ICCollections
        Dim objICCompany As New ICCompany
        Dim objICUser As New ICUser

        objICCompany.LoadByPrimaryKey(ddlCompanyPayment.SelectedValue.ToString)
        objICUser.LoadByPrimaryKey(Me.UserId.ToString)
        objICCollections.PayersName = objICCompany.CompanyName
        objICCollections.CollectionDate = Date.Now
        objICCollections.PayersAccountNo = ddlPayAccount.SelectedValue.ToString.Split("-")(0)
        objICCollections.PayersAccountBranchCode = ddlPayAccount.SelectedValue.ToString.Split("-")(1)
        objICCollections.PayersAccountCurrency = ddlPayAccount.SelectedValue.ToString.Split("-")(2)
        objICCollections.PayersAccountType = "RB"
        objICCollections.CollectionAccountNo = dr("Value").ToString.Split("-")(0)
        objICCollections.CollectionAccountBranchCode = dr("Value").ToString.Split("-")(1)
        objICCollections.CollectionAccountCurrency = dr("Value").ToString.Split("-")(2)
        objICCollections.CollectionAccountType = "RB"
        objICCollections.Status = 2
        objICCollections.LastStatus = 2
        objICCollections.CollectionAmount = CDbl(txtAmount.Text)
        objICCollections.CollectionRemarks = txtRemarks.Text
        objICCollections.CollectionLocationCode = objICUser.OfficeCode
        objICCollections.CollectionCreatedBy = Me.UserId.ToString
        objICCollections.CollectionMode = "Direct Pay"
        objICCollections.CollectionClientCode = ddlCompany.SelectedValue.ToString
        objICCollections.CollectionNatureCode = ddlCollNature.SelectedValue.ToString
        objICCollections.CollectionDirectPayCreatedBy = Me.UserId.ToString
        objICCollections.CollectionDirectPayCreatedDate = Date.Now
        objICCollections.ReferenceField1 = RefField1
        objICCollections.ReferenceField2 = RefField2
        objICCollections.ReferenceField3 = RefField3
        objICCollections.ReferenceField4 = RefField4
        objICCollections.ReferenceField5 = RefField5
        objICCollections.PaymentNatureCode = ddlPayAccount.SelectedValue.ToString.Split("-")(3)
        If DueDate IsNot Nothing Then
            objICCollections.DueDate = DueDate
        Else
            objICCollections.DueDate = Nothing
        End If
        objICCollections.AmountAfterDueDate = AmountAfterDueDate
        objICCollections.FileID = FileID
        objICCollections.CreatedOfficeCode = objICUser.OfficeCode
        Return objICCollections
    End Function
    Protected Sub txtAmount_TextChanged(sender As Object, e As EventArgs) Handles txtAmount.TextChanged
        Dim result As Double
        If txtAmount.Text.ToString() <> "" Then


            If txtAmount.Text.Contains(".") Then
                Dim str As String()
                Dim strBDeci, strADeci As String
                Dim TotAmt As String = ""
                str = txtAmount.Text.Split(".")
                strBDeci = str(0).ToString()
                strADeci = str(1).ToString()
                TotAmt = strBDeci & strADeci
                If TotAmt.Length > 13 Then
                    txtAmount.Text = Nothing
                    UIUtilities.ShowDialog(Me, "Direct Pay - Maker", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If strBDeci.Length > 11 Then
                    txtAmount.Text = Nothing
                    UIUtilities.ShowDialog(Me, "Direct Pay - Maker", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Else
                If txtAmount.Text.Length > 11 Then
                    txtAmount.Text = Nothing
                    UIUtilities.ShowDialog(Me, "Direct Pay - Maker", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            End If

            Try
                If Double.TryParse(txtAmount.Text.Trim(), result) = False Then
                    txtAmount.Text = Nothing
                    UIUtilities.ShowDialog(Me, "Direct Pay - Maker", "Invalid Amount - Amount cannot be egreatr than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                txtAmount.Text = Nothing
                UIUtilities.ShowDialog(Me, "Direct Pay - Maker", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End Try


            If CDbl(txtAmount.Text.ToString()) < 1 Then
                UIUtilities.ShowDialog(Me, "Direct Pay - Maker", "Amount should be greater than or equal to 1.", ICBO.IC.Dialogmessagetype.Failure)
                txtAmount.Text = ""
                Exit Sub
            End If
            'Standard Format
            'txtAmountWords.Text = NumToWord.AmtInWord(CDbl(txtAmountPKR.Text.ToString())).TrimStart().TrimEnd().Trim().ToString()
            'txtAmountInWords.Text = NumToWord.SpellNumber(CDbl(txtAmountPKR.Text.ToString())).TrimStart().TrimEnd().Trim().ToString()
        End If
    End Sub
    Protected Sub ddlCompanyPayment_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompanyPayment.SelectedIndexChanged
        Try
            LoadddlAccountPayment()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
