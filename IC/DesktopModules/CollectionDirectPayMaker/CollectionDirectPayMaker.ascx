﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CollectionDirectPayMaker.ascx.vb"
    Inherits="DesktopModules_CollectionDirectPayMaker_CollectionDirectPayMaker" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }


    function ValidateChkList(source, arguments) {

        arguments.IsValid = IsCheckBoxChecked() ? true : false;



    }

    function conVerify() {
        if (window.confirm("Are you sure you wish to procceed?") == true) {
            var btnVerify = document.getElementById('<%=btnUpDate.ClientID%>')
            btnVerify.value = "Processing...";
            btnVerify.disabled = true;

            var a = btnVerify.name;
            __doPostBack(a, '');
            return true;
        }
        else {
            return false;
        }
    }


    function AdjustWidth(ddl) {
        var maxWidth = 0;
        var Counter = 0;
        for (var i = 0; i < ddl.length; i++) {
            if (ddl.options[i].text.length > maxWidth) {
                Counter = Counter + 1;
                maxWidth = ddl.options[i].text.length;
            }
        }
        if (Counter = 1) { ddl.style.width = "250px"; }
        
        else {
            alert(Counter);
            ddl.style.width = maxWidth + "px";
        }
        //-->
    }
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
            }
        }
    }
</script>
<%--<style type="text/css">
.ctrDropDown{
    width:250px;
   
}
.ctrDropDownClick{
    
    width:600px;
 
}
.plainDropDown{
    width:250px;
   
}
    .auto-style1 {
        width: 25%;
        height: 23px;
    }
    </style>--%>
<telerik:RadInputManager ID="RadAmountComment" runat="server" Enabled="false">
    
    <telerik:TextBoxSetting BehaviorID="reComments" Validation-IsRequired="false" Validation-ValidationGroup="Search"
        ErrorMessage="Invalid Remarks" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRemarks" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:NumericTextBoxSetting BehaviorID="reAmountPKR" EmptyMessage="" AllowRounding="false"
                                                            Validation-IsRequired="true" Type="Number" DecimalDigits="2" ErrorMessage="Invalid amount"
                                                            Validation-ValidationGroup="Search">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtAmount" />
                                                            </TargetControls>
                                                        </telerik:NumericTextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
             <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue">Direct Pay - Maker</asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
          <table id="tblMain" runat="server" style="width:100%;">
         
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Collection Company" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 28%">
            <asp:Label ID="lblCollNature" runat="server" Text="Select Collection Nature" CssClass="lbl"></asp:Label><asp:Label
                ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 22%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
       
            <asp:DropDownList ID="ddlCollNature" runat="server" AutoPostBack="true" CssClass="dropdown">
               <%-- class="ctrDropDown"  onblur="this.className='ctrDropDown';" onmousedown="this.className='ctrDropDownClick';" onchange="this.className='ctrDropDown';" --%>
            </asp:DropDownList>
           
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" ControlToValidate="ddlCompany"
                Display="Dynamic" ErrorMessage="Please select Collection Company" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 33%">
            <asp:RequiredFieldValidator ID="rfvddlCollNature" runat="server" ControlToValidate="ddlCollNature"
                Display="Dynamic" ErrorMessage="Please select Collection Nature" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 17%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;&nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" CausesValidation="False"
                Width="75px" />
            &nbsp;
        </td>
    </tr>
          </table></td>
    </tr>
      <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
           <table id="tblReferenceField" runat="server" style="width:100%;">
               <tr  align="left">
                   <td align="left" style="width:25%;">
            <asp:Label ID="lblPageHeader0" runat="server" CssClass="headingblue">Reference Field Detail</asp:Label>
                   </td>
                   <td align="left" style="width:25%;"></td>
                   <td align="left" style="width:25%;"></td>
                   <td align="left" style="width:25%;"></td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr id="trReferenceField1" runat="server" align="left">
                   <td align="center" style="width:25%;">
            <asp:Label ID="lblReferenceField1" runat="server" CssClass="lbl"></asp:Label></td>
                   <td align="left" style="width:25%;">
            <asp:TextBox ID="txtReferenceField1" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
                   </td>
                   <td align="left" style="width:25%;">
                       <asp:RegularExpressionValidator ID="rfvReferenceField1" runat="server" Display="Dynamic" SetFocusOnError="True" ControlToValidate="txtReferenceField1" ValidationGroup="Search"></asp:RegularExpressionValidator>
                   </td>
                   <td align="left" style="width:25%;">
<telerik:RadInputManager ID="RadInputReferenceField1" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="reReferenceFieldOne" Validation-IsRequired="false" ErrorMessage="Invalid Reference Field One" Validation-ValidationGroup="Search"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceField1" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
                   </td>
               </tr>
               <tr id="trReferenceField2" runat="server" align="left">
                   <td align="center"  style="width:25%;">
            <asp:Label ID="lblReferenceField2" runat="server" CssClass="lbl"></asp:Label></td>
                   <td align="left" style="width:25%;">
            <asp:TextBox ID="txtReferenceField2" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
                   </td>
                   <td align="left" style="width:25%;">
                       <asp:RegularExpressionValidator ID="rfvReferenceField2" runat="server" Display="Dynamic" SetFocusOnError="True" ControlToValidate="txtReferenceField2" ValidationGroup="Search"></asp:RegularExpressionValidator>
                   </td>
                   <td align="left" style="width:25%;">
<telerik:RadInputManager ID="RadInputReferenceField2" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="reReferenceFieldTwo" Validation-IsRequired="false" ErrorMessage="Invalid Reference Field Two" Validation-ValidationGroup="Search"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceField2" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
                   </td>
               </tr>
               <tr id="trReferenceField3" runat="server" align="left">
                   <td align="center"  style="width:25%;">
            <asp:Label ID="lblReferenceField3" runat="server" CssClass="lbl"></asp:Label></td>
                   <td align="left" style="width:25%;">
            <asp:TextBox ID="txtReferenceField3" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
                   </td>
                   <td align="left" style="width:25%;">
                       <asp:RegularExpressionValidator ID="rfvReferenceField3" runat="server" Display="Dynamic" SetFocusOnError="True" ControlToValidate="txtReferenceField3" ValidationGroup="Search"></asp:RegularExpressionValidator>
                   </td>
                   <td align="left" style="width:25%;">
<telerik:RadInputManager ID="RadInputReferenceField3" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="reReferenceFieldThree" Validation-IsRequired="false" ErrorMessage="Invalid Reference Field Three" Validation-ValidationGroup="Search"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceField3" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
                   </td>
               </tr>
               <tr id="trReferenceField4" runat="server" align="left">
                   <td align="center" style="width:25%;">
            <asp:Label ID="lblReferenceField4" runat="server" CssClass="lbl"></asp:Label></td>
                   <td align="left" style="width:25%;">
            <asp:TextBox ID="txtReferenceField4" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
                   </td>
                   <td align="left" style="width:25%;">
                       <asp:RegularExpressionValidator ID="rfvReferenceField4" runat="server" Display="Dynamic" SetFocusOnError="True" ControlToValidate="txtReferenceField4" ValidationGroup="Search"></asp:RegularExpressionValidator>
                   </td>
                   <td align="left" style="width:25%;">
<telerik:RadInputManager ID="RadInputReferenceField4" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="reReferenceFieldFour" Validation-IsRequired="false" ErrorMessage="Invalid Reference Field Four" Validation-ValidationGroup="Search"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceField4" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
                   </td>
               </tr>
               <tr  id="trReferenceField5" runat="server" align="left">
                   <td align="center"  style="width:25%;">
            <asp:Label ID="lblReferenceField5" runat="server" CssClass="lbl"></asp:Label></td>
                   <td align="left" style="width:25%;">
            <asp:TextBox ID="txtReferenceField5" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
                   </td>
                   <td align="left" style="width:25%;">
                       <asp:RegularExpressionValidator ID="rfvReferenceField5" runat="server" Display="Dynamic" SetFocusOnError="True" ControlToValidate="txtReferenceField5" ValidationGroup="Search"></asp:RegularExpressionValidator>
                   </td>
                   <td align="left" style="width:25%;">
<telerik:RadInputManager ID="RadInputReferenceField5" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="reReferenceFieldFive" Validation-IsRequired="false" ErrorMessage="Invalid Reference Field Five" Validation-ValidationGroup="Search"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceField" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
                   </td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr  align="left">
                   <td align="center" colspan="4"><asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnCancel"
                Width="75px" ValidationGroup="Search" />
                       <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btnCancel" CausesValidation="False"
                Width="75px" />
                   </td>
               </tr>
           </table>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
           <table id="tblAmountAndCompanyDetails" runat="server" style="width:100%;">
               <tr  align="left">
                   <td align="left" style="width:25%;">
            <asp:Label ID="Label3" runat="server" CssClass="headingblue">Invoice Details</asp:Label>
                   </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">
                       &nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">
            <asp:Label ID="lblCompanyPayment" runat="server" Text="Select Payment Company" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqPayCompany" runat="server" Text="*" ForeColor="Red"></asp:Label>
                   </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">
            <asp:Label ID="lblPayAccount" runat="server" Text="Select Payment Account Nature" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqPayCompany0" runat="server" Text="*" ForeColor="Red"></asp:Label>
                   </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">
            <asp:DropDownList ID="ddlCompanyPayment" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
                   </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">
            <asp:DropDownList ID="ddlPayAccount" runat="server" CssClass="dropdown">
            </asp:DropDownList>
                   </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">
            <asp:RequiredFieldValidator ID="rfvddlCompanyPayment" runat="server" ControlToValidate="ddlCompanyPayment"
                Display="Dynamic" ErrorMessage="Please select Payment Company" InitialValue="0" SetFocusOnError="True" ValidationGroup="Search"></asp:RequiredFieldValidator>
                   </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">
            <asp:RequiredFieldValidator ID="rfvddlPayAccount" runat="server" ControlToValidate="ddlPayAccount"
                Display="Dynamic" ErrorMessage="Please select Payment Account" InitialValue="0" SetFocusOnError="True" ValidationGroup="Search"></asp:RequiredFieldValidator>
                   </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">
            <asp:Label ID="lblAmount" runat="server" Text="Amount" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqAmount" runat="server" Text="*" ForeColor="Red"></asp:Label>
                   </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">
            <asp:Label ID="lblRemarks" runat="server" Text="Remarks" CssClass="lbl"></asp:Label>
                   </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr  align="left">
                   <td align="left" valign="top" style="width:25%;">
            <asp:TextBox ID="txtAmount" runat="server" CssClass="txtbox" MaxLength="14" AutoPostBack="True"></asp:TextBox>
                   </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">
                                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="txtbox" Height="60px"
                                                        MaxLength="150" TextMode="MultiLine" ></asp:TextBox></td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr  align="left">
                   <td align="center" colspan="4">&nbsp;</td>
               </tr>
           </table>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
           <table id="tblResult" runat="server" style="width:100%;">
               <tr  align="left">
                   <td align="left" style="width:25%;">
                       &nbsp;</td>
                   <td align="left" style="width:25%;"></td>
                   <td align="left" style="width:25%;"></td>
                   <td align="left" style="width:25%;"></td>
               </tr>
               <tr id="tr1" runat="server" align="left">
                   <td align="left" colspan="4">
                        <telerik:RadGrid ID="gvInvoiceRecords" runat="server" AllowPaging="false" AllowSorting="false"
                            AutoGenerateColumns="true" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                            <ClientSettings>
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView TableLayout="Fixed" DataKeyNames="Status">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                   <telerik:GridTemplateColumn HeaderText="Select" DataField="IsApprove">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>    
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">
                       &nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               </table>
        </td>
    </tr>
    <tr style="width:100%;">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnUpDate" runat="server" Text="Pay" CssClass="btnCancel"
                Width="75px" ValidationGroup="Search"   CausesValidation="true"/>
            <%--<asp:Button ID="btnUpDate" runat="server" Text="Pay" CssClass="btnCancel"
                Width="75px" ValidationGroup="Search"  OnClientClick ="javascript: return conVerify();" CausesValidation="true"/>--%>
                       <asp:Button ID="btnBack2" runat="server" Text="Back" CssClass="btnCancel" CausesValidation="False"
                Width="75px" />
                   </td>
    </tr>
</table>
