﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_WorkingDayManagement_ManageWorkingDays
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                BindListBoxes()
                btnAddWorkingDay.Visible = CBool(htRights("Manage"))
                btnDeleteWorkingDay.Visible = CBool(htRights("Manage"))
            End If
        Catch exc As Exception
            Exceptions.ProcessModuleLoadException(Me, exc)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Working Days Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub BindListBoxes()
        Try
            Dim dtWorkingDays As New DataTable
            Dim dr As DataRow
            Dim i As Integer = 0
            Dim lit As ListItem
            Dim litOrder As ListItem
            Dim chklit As ListItem
            Dim IsWorkingDays As Boolean = False

            lstboxNonWorkingDays.Items.Clear()
            lstboxWorkingDays.Items.Clear()

            dtWorkingDays = ICWorkingDaysController.GetWorkingDays()
            If dtWorkingDays.Rows.Count > 0 Then
                For Each dr In dtWorkingDays.Rows
                    lit = New ListItem
                    litOrder = New ListItem

                    lit.Value = dr("DayName").ToString()
                    lit.Text = dr("DayName").ToString()
                    litOrder.Value = dr("OrderDay").ToString()
                    litOrder.Text = dr("OrderDay").ToString()
                    lstboxWorkingDays.Items.Add(lit)
                Next
            End If
            For i = 1 To 7
                lit = New ListItem
                litOrder = New ListItem
                lit.Value = i.ToString()
                lit.Text = GetDayName(i).ToString()

                If lstboxWorkingDays.Items.Count > 0 Then
                    For Each chklit In lstboxWorkingDays.Items
                        If chklit.Text.ToString() = GetDayName(i).ToString() Then
                            IsWorkingDays = True
                        End If
                    Next
                End If
                If IsWorkingDays = False Then
                    lstboxNonWorkingDays.Items.Add(lit)
                End If
                IsWorkingDays = False
            Next i
        Catch ex As Exception
            UIUtilities.ShowDialog(Me, "Working Day Management", ex.Message.ToString(), Dialogmessagetype.Failure)
            Exceptions.ProcessModuleLoadException(Me, ex)
        End Try
    End Sub

    Private Function GetDayName(ByVal DayOfWeek As Integer) As String
        Select Case DayOfWeek.ToString()
            Case "1"
                Return "Monday"
            Case "2"
                Return "Tuesday"
            Case "3"
                Return "Wednesday"
            Case "4"
                Return "Thursday"
            Case "5"
                Return "Friday"
            Case "6"
                Return "Saturday"
            Case "7"
                Return "Sunday"
        End Select
    End Function

    Private Function GetDayNameIndex(ByVal DayName As String) As Integer
        Select Case DayName.ToString()
            Case "Monday"
                Return 1
            Case "Tuesday"
                Return 2
            Case "Wednesday"
                Return 3
            Case "Thursday"
                Return 4
            Case "Friday"
                Return 5
            Case "Saturday"
                Return 6
            Case "Sunday"
                Return 7
        End Select
    End Function

    Protected Sub BtnCan_Click(sender As Object, e As EventArgs) Handles BtnCan.Click
        Response.Redirect(NavigateURL(41), False)
    End Sub

    Protected Sub btnAddWorkingDay_Click(sender As Object, e As EventArgs) Handles btnAddWorkingDay.Click
        Try
            If lstboxNonWorkingDays.SelectedValue.ToString() = "" Then
                UIUtilities.ShowDialog(Me, "Working Day Management", "Please select Non Working Day to add", Dialogmessagetype.Failure)
                Exit Sub
            End If
            Dim objWorkingDay As New ICWorkingDays
            objWorkingDay.OrderDay = lstboxNonWorkingDays.SelectedValue.ToString()
            objWorkingDay.DayName = lstboxNonWorkingDays.SelectedItem.Text.ToString()
            ICWorkingDaysController.AddWorkingDay(objWorkingDay, Me.UserId, Me.UserInfo.Username)
            BindListBoxes()
            UIUtilities.ShowDialog(Me, "Working Day Management", "Working Day added successfully.", Dialogmessagetype.Success)
        Catch ex As Exception
            UIUtilities.ShowDialog(Me, "Working Day Management", ex.Message.ToString(), Dialogmessagetype.Failure)
            Exceptions.ProcessModuleLoadException(Me, ex)
        End Try
    End Sub

    Protected Sub btnDeleteWorkingDay_Click(sender As Object, e As EventArgs) Handles btnDeleteWorkingDay.Click
        Try
            If lstboxWorkingDays.SelectedValue.ToString() = "" Then
                UIUtilities.ShowDialog(Me, "Working Day Management", "Please select Working Day to delete", Dialogmessagetype.Failure)
                Exit Sub
            End If
            If ICWorkingDaysController.DeleteWorkingDay(lstboxWorkingDays.SelectedValue.ToString(), Me.UserId, Me.UserInfo.Username) = True Then
                BindListBoxes()
                UIUtilities.ShowDialog(Me, "Working Day Management", "Working Day deleted successfully.", Dialogmessagetype.Success)
            Else
                UIUtilities.ShowDialog(Me, "Working Day Management", "Unable to delete selected Working Day.", Dialogmessagetype.Failure)
            End If

        Catch ex As Exception
            UIUtilities.ShowDialog(Me, "Working Day Management", ex.Message.ToString(), Dialogmessagetype.Failure)
            Exceptions.ProcessModuleLoadException(Me, ex)
        End Try
    End Sub
End Class