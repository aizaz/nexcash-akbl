﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ManageWorkingDays.ascx.vb" Inherits="DesktopModules_WorkingDayManagement_ManageWorkingDays" %>

<table style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%;">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue" Text="Working Days Management"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%;"></td>
        <td align="left" valign="top" style="width: 25%;"></td>
        <td align="left" valign="top" style="width: 25%;"></td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%;"></td>
        <td align="left" valign="top" style="width: 25%;"></td>
        <td align="left" valign="top" style="width: 25%;"></td>
        <td align="left" valign="top" style="width: 25%;"></td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="right" style="width: 25%;">
            <asp:Label ID="lblNonWorkingDays" runat="server" Text="List of Non Working Days" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" style="width: 25%;"></td>
        <td align="left" style="width: 25%;">
            <asp:Label ID="lblWorkingDays" runat="server" Text="List of Working Days" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%;"></td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="right" style="width: 25%;">
            <asp:ListBox ID="lstboxNonWorkingDays" runat="server" Height="200px" Width="50%" SelectionMode="Single"></asp:ListBox>
        </td>
        <td align="Center" style="width: 25%;" valign="center">
            <asp:Button ID="btnAddWorkingDay" runat="server" Text="&gt;&gt;" Width="45px" />
            <br />
            <asp:Button ID="btnDeleteWorkingDay" runat="server" Text="&lt;&lt;" Width="45px" />
        </td>
        <td align="left" style="width: 25%;">
            <asp:ListBox ID="lstboxWorkingDays" runat="server" Height="200px" Width="50%" SelectionMode="Single" align="Right"></asp:ListBox>
        </td>
        <td align="left" style="width: 25%;"></td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" style="width: 25%;"></td>
        <td align="left" style="width: 25%;"></td>
        <td align="left" style="width: 25%;"></td>
        <td align="left" style="width: 25%;"></td>
    </tr>
    <tr align="Center" valign="top" style="width: 100%">
        <td align="left" style="width: 25%;"></td>
        <td align="Center" style="width: 25%;">&nbsp;
            <asp:Button ID="BtnCan" runat="server" Text="Cancel" CssClass="btnCancel" Width="71px" />
        </td>
        <td align="left" style="width: 25%;"></td>
        <td align="left" style="width: 25%;"></td>
    </tr>
</table>

