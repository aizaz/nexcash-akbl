﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Partial Class DesktopModules_AccountPaymentNatureProductTypeManagement_AddAccountPaymentNatureProductType
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private AccountPaymentNatureProductTypeCode As String
    Private htRights As Hashtable


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            AccountPaymentNatureProductTypeCode = Request.QueryString("APNPTCode").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                If AccountPaymentNatureProductTypeCode.ToString() = "0" Then
                    lblPageHeader.Text = "Add Account, Payment Nature"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    lblPageHeader.Text = "Edit Account, Payment Nature"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                End If
                LoadddlGroup()
                LoadddlCompany()
                LoadddlAccounts()
                LoadchklstPaymentNature()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client Account & Payment Nature Tagging")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccounts()
        Try
            Dim lit As New ListItem
            Dim objAccount As New ICAccounts
            Dim collAccounts As New ICAccountsCollection

            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlAccount.Items.Clear()
            ddlAccount.Items.Add(lit)
            collAccounts = ICAccountsController.GetAllAccountsByCompanyCode(ddlCompany.SelectedValue.ToString())
            For Each objAccount In collAccounts
                lit = New ListItem
                lit.Text = objAccount.AccountTitle.ToString() & " - " & objAccount.AccountNumber.ToString()
                lit.Value = objAccount.AccountNumber.ToString() & ";" & objAccount.BranchCode.ToString() & ";" & objAccount.Currency.ToString()
                ddlAccount.Items.Add(lit)
            Next
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadchklstPaymentNature()
        Try
            chklstPaymentNature.DataSource = ICPaymentNatureController.GetAllActivePaymentNatures()
            chklstPaymentNature.DataTextField = "PaymentNatureName"
            chklstPaymentNature.DataValueField = "PaymentNatureCode"
            chklstPaymentNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        LoadddlCompany()
        LoadddlAccounts()
        LoadchklstPaymentNature()
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        LoadddlAccounts()
        LoadchklstPaymentNature()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If Page.IsValid Then
            Try
                Dim lit As New ListItem
                Dim objAccountPaymentNature As ICAccountsPaymentNature
                Dim objchkAccountPaymentNature As ICAccountsPaymentNature
                Dim objICCompany As ICCompany
                Dim StrActionForCompany As String = Nothing

                Dim ActionText As String = ""
                Dim PassTaggingCount As Integer = 0
                Dim FailTaggingCount As Integer = 0
                For Each lit In chklstPaymentNature.Items
                    If lit.Selected = True Then
                        objAccountPaymentNature = New ICAccountsPaymentNature
                        objchkAccountPaymentNature = New ICAccountsPaymentNature
                        objAccountPaymentNature.AccountNumber = ddlAccount.SelectedValue.ToString().Split(";")(0).ToString()
                        objAccountPaymentNature.BranchCode = ddlAccount.SelectedValue.ToString().Split(";")(1).ToString()
                        objAccountPaymentNature.Currency = ddlAccount.SelectedValue.ToString().Split(";")(2).ToString()
                        objAccountPaymentNature.PaymentNatureCode = lit.Value.ToString()
                        objAccountPaymentNature.CreateBy = Me.UserId
                        objAccountPaymentNature.CreateDate = Date.Now
                        objAccountPaymentNature.Creater = Me.UserId
                        objAccountPaymentNature.CreationDate = Date.Now
                        ActionText = "Account [Account Number : " & ddlAccount.SelectedValue.ToString().Split(";")(0).ToString() & " ; Branch Code : " & ddlAccount.SelectedValue.ToString().Split(";")(1).ToString() & " ; Currency : " & ddlAccount.SelectedValue.ToString().Split(";")(2).ToString() & "] ; Payment Nature [Code : " & lit.Value.ToString() & " ; Name : " & lit.Text.ToString() & "] ; Client [ " & ddlCompany.SelectedItem.ToString & " ] and group [ " & ddlGroup.SelectedItem.ToString & " ] tagged successfully."
                        If objchkAccountPaymentNature.LoadByPrimaryKey(ddlAccount.SelectedValue.ToString().Split(";")(0).ToString(), ddlAccount.SelectedValue.ToString().Split(";")(1).ToString(), ddlAccount.SelectedValue.ToString().Split(";")(2).ToString(), lit.Value.ToString()) = False Then
                            ICAccountPaymentNatureController.AddAccountPaymentNature(objAccountPaymentNature, ActionText.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                            PassTaggingCount = PassTaggingCount + 1
                        Else
                            FailTaggingCount = FailTaggingCount + 1
                        End If
                    End If
                Next
                'If AccountPaymentNatureProductTypeCode = 0 Then
                '    If Not PassTaggingCount = 0 Then
                '        objICCompany = New ICCompany
                '        objICCompany.LoadByPrimaryKey(ddlCompany.SelectedValue.ToString)
                '        objICCompany.IsApprove = False
                '        objICCompany.CreatedBy = Me.UserId
                '        objICCompany.CreatedDate = Date.Now
                '        StrActionForCompany = Nothing
                '        StrActionForCompany += "Compnay [ " & objICCompany.CompanyName & " ] is unapproved on tagging account, payment nature. Action was taken by user [ " & Me.UserId.ToString & " ] [ " & Me.UserInfo.Username.ToString & " ]"
                '        ICCompanyController.AddCompany(objICCompany, True, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrActionForCompany)
                '    End If
                'End If
                'If Not AccountPaymentNatureProductTypeCode.ToString() = "0" Then
                '    objICCompany = New ICCompany
                '    objICCompany.LoadByPrimaryKey(ddlCompany.SelectedValue.ToString)
                '    objICCompany.IsApprove = False
                '    objICCompany.CreatedBy = Me.UserId
                '    objICCompany.CreatedDate = Date.Now
                '    StrActionForCompany = Nothing
                '    StrActionForCompany += "Compnay [ " & objICCompany.CompanyName & " ] is unapproved on updating account, payment nature. Action was taken by user [ " & Me.UserId.ToString & " ] [ " & Me.UserInfo.Username.ToString & " ]"
                '    ICCompanyController.AddCompany(objICCompany, True, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrActionForCompany)
                'End If
                If PassTaggingCount <> 0 And FailTaggingCount <> 0 Then
                    UIUtilities.ShowDialog(Me, "Save Account Payment Nature", PassTaggingCount & "Account and Payment Natures are tagged successfully.<br /> " & FailTaggingCount & " Account and Payment Natures are not tagged successfully due to following reason <br /> 1. Duplicate account payment nature are not allowed", ICBO.IC.Dialogmessagetype.Success)
                ElseIf PassTaggingCount <> 0 And FailTaggingCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Save Account Payment Nature", PassTaggingCount & "Account and Payment Natures are tagged successfully.", ICBO.IC.Dialogmessagetype.Success)
                ElseIf PassTaggingCount = 0 And FailTaggingCount <> 0 Then

                    UIUtilities.ShowDialog(Me, "Save Account Payment Nature", FailTaggingCount & " Account and Payment Natures are not tagged successfully due to following reason <br /> 1. Duplicate account payment nature are not allowed", ICBO.IC.Dialogmessagetype.Failure)

                End If

                LoadchklstPaymentNature()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try

        End If
    End Sub
    Private Sub Clear()
        LoadddlGroup()
        LoadddlCompany()
        LoadddlAccounts()
        LoadchklstPaymentNature()
    End Sub
End Class
