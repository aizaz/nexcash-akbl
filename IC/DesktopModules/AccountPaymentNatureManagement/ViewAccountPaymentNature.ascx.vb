﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_AccountPaymentNatureProductTypeManagement_ViewAccountPaymentNatureProductType
     Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_AccountPaymentNatureProductTypeManagement_ViewAccountPaymentNatureProductType_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlCompany()
                LoadgvAccountPaymentNature(True)
                btnSaveAccPayNatProTyp.Visible = CBool(htRights("Add"))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client Account & Payment Nature Tagging")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
#Region "Grid View Events"

#End Region
#Region "Button Events"
    Protected Sub btnSaveAccPayNatProTyp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAccPayNatProTyp.Click
        If Page.IsValid Then

            Response.Redirect(NavigateURL("AddAccountPaymentNatureProductType", "&mid=" & Me.ModuleId & "&APNPTCode=0"), False)

        End If
    End Sub
#End Region
#Region "DropDown Events"
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        LoadddlCompany()
        LoadgvAccountPaymentNature(True)
    End Sub
    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        LoadgvAccountPaymentNature(True)
    End Sub
#End Region
#Region "Other Functions/Routines"
  
    Private Sub LoadgvAccountPaymentNature(ByVal IsBind As Boolean)
        Try
            ICAccountPaymentNatureController.SetgvAccountPaymentNatures(ddlGroup.SelectedValue.ToString(), ddlCompany.SelectedValue.ToString(), gvAccountPaymentNature.CurrentPageIndex + 1, gvAccountPaymentNature.PageSize, gvAccountPaymentNature, IsBind)
            If gvAccountPaymentNature.Items.Count > 0 Then
                lblBankRNF.Visible = False
                gvAccountPaymentNature.Visible = True
                btnBulkDeleteAccountPNature.Visible = CBool(htRights("Delete"))
                gvAccountPaymentNature.Columns(6).Visible = CBool(htRights("Delete"))
            Else
                lblBankRNF.Visible = True
                gvAccountPaymentNature.Visible = False
                btnBulkDeleteAccountPNature.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
   
#End Region
    Protected Sub gvAccountPaymentNature_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAccountPaymentNature.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAccountPaymentNature.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAccountPaymentNature_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAccountPaymentNature.ItemDataBound
        Dim chkProcessAll As CheckBox
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAccountPaymentNature.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub gvAccountPaymentNature_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvAccountPaymentNature.PageIndexChanged
        gvAccountPaymentNature.CurrentPageIndex = e.NewPageIndex
    End Sub

    Protected Sub gvAccountPaymentNature_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAccountPaymentNature.NeedDataSource
        Try
            LoadgvAccountPaymentNature(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub DeleteAccountPaymentNature(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String)
        Dim objPaymentNature As New ICPaymentNature
        Dim objAccountPaymentNature As New ICAccountsPaymentNature
        Dim ActionText As String = ""
        objPaymentNature.LoadByPrimaryKey(PaymentNatureCode.ToString())
        objAccountPaymentNature.LoadByPrimaryKey(AccountNumber.ToString(), BranchCode.ToString(), Currency.ToString(), PaymentNatureCode.ToString())
        ActionText = "Account [Account Number : " & AccountNumber.ToString() & " ; Branch Code : " & BranchCode.ToString() & " ; Currency : " & Currency.ToString() & "] ; Payment Nature [Code : " & objPaymentNature.PaymentNatureCode.ToString() & " ; Name : " & objPaymentNature.PaymentNatureName.ToString() & "] ; Client [ " & objAccountPaymentNature.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.CompanyName & " ] and group [ " & objAccountPaymentNature.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupName & " ] untagged successfully."
        ICAccountPaymentNatureController.DeleteAccountPaymentNature(objAccountPaymentNature, ActionText.ToString(), Me.UserId, Me.UserInfo.Username)
    End Sub
    Protected Sub gvAccountPaymentNature_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvAccountPaymentNature.ItemCommand
        Try
            If e.CommandName = "del" Then
                Dim AccountNumber, BranchCode, Currency, PaymentNatureCode As String
                AccountNumber = e.CommandArgument.ToString().Split(";")(0).ToString()
                BranchCode = e.CommandArgument.ToString().Split(";")(1).ToString()
                Currency = e.CommandArgument.ToString().Split(";")(2).ToString()
                PaymentNatureCode = e.CommandArgument.ToString().Split(";")(3).ToString()
                DeleteAccountPaymentNature(AccountNumber.ToString(), BranchCode.ToString(), Currency.ToString(), PaymentNatureCode.ToString())
                UIUtilities.ShowDialog(Me, "Delete Account Payment Nature", "Account and Payment Natures are untagged successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Delete Account Payment Nature", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckgvAccountPaymentNatureForProcessAll() As Boolean
        Try
            Dim rowgvAccountPaymentNature As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvAccountPaymentNature In gvAccountPaymentNature.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvAccountPaymentNature.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnBulkDeleteAccountPNature_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBulkDeleteAccountPNature.Click
        If Page.IsValid Then
            Try
                Dim rowgvAccountPaymentNature As GridDataItem
                Dim chkSelect As CheckBox
                Dim AccountNumber, BranchCode, Currency, PaymentNatureCode As String
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0

                If CheckgvAccountPaymentNatureForProcessAll() = True Then
                    For Each rowgvAccountPaymentNature In gvAccountPaymentNature.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvAccountPaymentNature.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            AccountNumber = rowgvAccountPaymentNature.GetDataKeyValue("AccountNumber").ToString()
                            BranchCode = rowgvAccountPaymentNature.GetDataKeyValue("BranchCode").ToString()
                            Currency = rowgvAccountPaymentNature.GetDataKeyValue("Currency").ToString()
                            PaymentNatureCode = rowgvAccountPaymentNature.GetDataKeyValue("PaymentNatureCode").ToString()
                            Try
                                DeleteAccountPaymentNature(AccountNumber.ToString(), BranchCode.ToString(), Currency.ToString(), PaymentNatureCode.ToString())
                                PassCount = PassCount + 1
                            Catch ex As Exception
                                If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                    FailCount = FailCount + 1
                                End If
                            End Try
                        End If
                    Next
                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Account Payment Nature", "[" & FailCount.ToString() & "] Account Payment Natures can not be untagged due to following reasons : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Account Payment Nature", "[" & PassCount.ToString() & "] Account and Payment Natures untagged successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Delete Account Payment Nature", "[" & PassCount.ToString() & "] Account and Payment Natures untagged successfully. [" & FailCount.ToString() & "] Account Payment Natures can not be untagged due to following reasons : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Delete Account Payment Nature", "Please select atleast one(1) Account Payment Nature.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub
End Class
