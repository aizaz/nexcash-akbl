﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InstructionPendingForPrimaryApproval.ascx.vb"
    Inherits="DesktopModules_InstructionStatus_InstructionStatus" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


    <telerik:RadInputManager ID="RadInputManager3" runat="server">
      <telerik:TextBoxSetting BehaviorID="rvRIN" Validation-IsRequired="false" EmptyMessage="Enter RIN">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRIN" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reTracking" Validation-IsRequired="false" ValidationExpression="^[0-9]*$"
        ErrorMessage="Invalid Tracking No" EmptyMessage="Enter Tracking No">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTrackingNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
</telerik:RadInputManager>


<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Primary Approval Queue" 
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        &nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">                        
                        <table width="100%">
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblAcqAgent" runat="server" Text="Account Payment Nature" 
                                        CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                                <td align="left" valign="top" style="width: 25%">
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:DropDownList ID="ddlAcqAgent" runat="server" Width="100%"
                                        CssClass="dropdown">
                                        <asp:ListItem>Vendor Payments</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                            </tr>
                            </table>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                    
                            <asp:Label ID="lblNRF" runat="server" Text="No Record Found." CssClass="headingblue"
                                Visible="false"></asp:Label>
                    
                                              
                     </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                    
                        <telerik:RadGrid ID="gvAuthorization" runat="server" AllowPaging="True"
                            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                            PageSize="4">
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView>
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Approve" DataField="IsApproved">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAllGroups" runat="server" CssClass="chkBox" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="RemittID" HeaderText="Tracking No" SortExpression="RemittID">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneName" HeaderText="Beneficiary Name" SortExpression="BeneName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="AmountPKR" HeaderText="Amount (PKR)"
                                        SortExpression="AmountPKR">
                                    </telerik:GridBoundColumn>
                                 
                                    <telerik:GridBoundColumn DataField="CellNo" HeaderText="Cell No"
                                        SortExpression="CellNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CreateDate" HeaderText="Create Date"
                                        SortExpression="CreateDate">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Status" HeaderText="Status"
                                        SortExpression="Status">
                                    </telerik:GridBoundColumn>
                                           <telerik:GridTemplateColumn HeaderText="Comments">
                                        <ItemTemplate>
                                           <asp:TextBox ID="txtCommnets" runat="server" TextMode="MultiLine" Height="60px"></asp:TextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn>
                                        <ItemTemplate>
                                           <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/view.gif"
                                            ToolTip="Instruction Details">Detail</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                        </telerik:RadGrid>
                    
                            <%--<asp:GridView ID="gvAuthorization" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                                Width="100%" AllowPaging="True" EnableModelValidation="True" 
                                AllowSorting="true"
                                 >
                                <AlternatingRowStyle CssClass="GridAltItem" />
                                <Columns>
                                <asp:BoundField DataField="RemittID" HeaderText="Tracking No" 
                                        SortExpression="RemittID">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="RIN" HeaderText="RIN" SortExpression="RIN">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="BeneName" HeaderText="Beneficiary Name" 
                                        SortExpression="BeneName">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="RemitterName" HeaderText="Remitter Name" 
                                        SortExpression="RemitterName">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AmountPKR" HeaderText="Amount (PKR)" 
                                        SortExpression="AmountPKR" DataFormatString="{0:N2}" HtmlEncode="False">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                     <asp:BoundField DataField="CreateDate" HeaderText="Acq Date" SortExpression="CreateDate"
                                    DataFormatString="{0:dd-MMM-yyyy HH:mm:ss}" HtmlEncode="False">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                 <asp:BoundField DataField="DisbursedDate" HeaderText="Disb Date" SortExpression="DisbursedDate"
                                    DataFormatString="{0:dd-MMM-yyyy HH:mm:ss}" HtmlEncode="False">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                    <asp:BoundField DataField="DisbType" HeaderText="Disbursement Mode" 
                                        SortExpression="DisbType">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>                                    
                                    <asp:BoundField DataField="TransferType" HeaderText="Transfer Type" 
                                        SortExpression="TransferType">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>                                    
                                    <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status">
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:BoundField>    
                                    <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/view.gif" NavigateUrl='<%#NavigateURL("InstructionDetail", "&mid=" & Me.ModuleId & "&rid="& Eval("RemittID"))%>'
                                            ToolTip="Instruction Details">Detail</asp:HyperLink>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="IbtnPrint" runat="server" CommandArgument='<%#Eval("RemittID") %>'
                                            CommandName="printInstruction" ImageUrl="~/images/print.gif" 
                                            ToolTip="Print" />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                </asp:TemplateField>                                      
                                </Columns>
                                <HeaderStyle CssClass="GridHeader" />
                                <PagerStyle CssClass="GridPager" />
                                <RowStyle CssClass="GridItem" />
                            </asp:GridView>--%>
                     </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                    
                            &nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%">
                    
                            <asp:Button ID="Button2" runat="server" Text="Approve" Width="71px" 
                                CssClass="btn" />
                            <asp:Button ID="Button3" runat="server" Text="Amend" Width="70px" 
                                CssClass="btn" />
                            <asp:Button ID="Button1" runat="server" Text="Cancel" Width="69px" 
                                CssClass="btn" />
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%">
                    
                            &nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                    
                        <asp:Label ID="lblPageHeader0" runat="server" Text="Secondary Approval Queue" 
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                    
                        &nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                    
                                    <asp:Label ID="lblAcqAgent0" runat="server" Text="Account Payment Nature" 
                                        CssClass="lbl"></asp:Label>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                    
                                    <asp:DropDownList ID="ddlAcqAgent0" runat="server"
                                        CssClass="dropdown">
                                        <asp:ListItem>Vendor Payments</asp:ListItem>
                                    </asp:DropDownList>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%">
                    
                            &nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%">
                    
                        <telerik:RadGrid ID="gvAuthorization0" runat="server" AllowPaging="True"
                            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                            PageSize="4">
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView>
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Approve" DataField="IsApproved">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAllGroups0" runat="server" CssClass="chkBox" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect0" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="RemittID" HeaderText="Tracking No" SortExpression="RemittID">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneName" HeaderText="Beneficiary Name" SortExpression="BeneName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="AmountPKR" HeaderText="Amount (PKR)"
                                        SortExpression="AmountPKR">
                                    </telerik:GridBoundColumn>
                                 
                                    <telerik:GridBoundColumn DataField="CellNo" HeaderText="Cell No"
                                        SortExpression="CellNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CreateDate" HeaderText="Create Date"
                                        SortExpression="CreateDate">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Status" HeaderText="Status"
                                        SortExpression="Status">
                                    </telerik:GridBoundColumn>
                                           <telerik:GridTemplateColumn HeaderText="Comments">
                                        <ItemTemplate>
                                           <asp:TextBox ID="txtCommnets0" runat="server" TextMode="MultiLine" Height="60px"></asp:TextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn>
                                        <ItemTemplate>
                                           <asp:HyperLink ID="hlEdit0" runat="server" ImageUrl="~/images/view.gif"
                                            ToolTip="Instruction Details">Detail</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                        </telerik:RadGrid>
                    
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%">
                    
                            &nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%">
                    
                            <asp:Button ID="Button4" runat="server" Text="Approve" Width="71px" 
                                CssClass="btn" />
                            <asp:Button ID="Button5" runat="server" Text="Cancel" Width="69px" 
                                CssClass="btn" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
