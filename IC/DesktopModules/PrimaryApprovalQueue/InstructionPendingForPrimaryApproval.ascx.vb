﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data
Imports System.Data.OleDb

Partial Class DesktopModules_InstructionStatus_InstructionStatus
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Dim dtDa As DataTable



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                LoadgvAccountPaymentNatureProductType()
                LoadgvAccountPaymentNatureProductType2()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadgvAccountPaymentNatureProductType()
        Try

            'dtDa = ICAccountPaymentNatureProductTypeController.GetAllAccountPaymentNatureProductType(ddlCompany.SelectedValue.ToString())
            'ViewState("DataTable") = dtDa

            Dim connectionString As String = ""
            Dim fileLocation As String = Server.MapPath("~/Excel Sheets For Grids/instructions.xlsx")

            'Check whether file extension is xls or xslx

            'If fileExtension = ".xls" Then
            '    connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
            'ElseIf fileExtension = ".xlsx" Then
            connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
            'End If

            'Create OleDB Connection and OleDb Command

            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = "Sheet1$"
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()



            gvAuthorization.DataSource = dtExcelRecords
            gvAuthorization.DataBind()



            'If CheckIsAdminOrSuperUser() = False Then
            '    If ArrRights.Count > 0 Then
            '        gvAccountPaymentNatureProductTypeDetails.Columns(5).Visible = ArrRights(2)
            '    Else
            '        UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            '    End If
            'End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadgvAccountPaymentNatureProductType2()
        Try

            'dtDa = ICAccountPaymentNatureProductTypeController.GetAllAccountPaymentNatureProductType(ddlCompany.SelectedValue.ToString())
            'ViewState("DataTable") = dtDa

            Dim connectionString As String = ""
            Dim fileLocation As String = Server.MapPath("~/Excel Sheets For Grids/instructions2.xlsx")

            'Check whether file extension is xls or xslx

            'If fileExtension = ".xls" Then
            '    connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
            'ElseIf fileExtension = ".xlsx" Then
            connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
            'End If

            'Create OleDB Connection and OleDb Command

            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = "Sheet1$"
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()



            gvAuthorization0.DataSource = dtExcelRecords
            gvAuthorization0.DataBind()



            'If CheckIsAdminOrSuperUser() = False Then
            '    If ArrRights.Count > 0 Then
            '        gvAccountPaymentNatureProductTypeDetails.Columns(5).Visible = ArrRights(2)
            '    Else
            '        UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            '    End If
            'End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

End Class

