﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_ReleaseToBankQueue_ReleaseToBankQueue
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrICAssignUserRolsID As New ArrayList
    Private ArrICAssignedOfficeID As New ArrayList
    Private ArrICAssignedStatus As New ArrayList
    Dim ArrICAssignedPaymentModes As New ArrayList
    Private htRights As Hashtable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()
            lblMessageForQueue.Style.Add("display", "none")
            If Page.IsPostBack = False Then
                lblMessageForQueue.Text = ICUtilities.GetSettingValue("ErrorMessage")

                btnPayAll.Attributes.Item("onclick") = "if (Page_ClientValidate('Remarks')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";document.getElementById('<%=lblMessageForQueue.ClientID%>').style.display = 'block';this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnPayAll, Nothing).ToString() & " } "
                btnClubPosting.Attributes.Item("onclick") = "if (Page_ClientValidate('Remarks')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";document.getElementById('<%=lblMessageForQueue.ClientID%>').style.display = 'block';this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnClubPosting, Nothing).ToString() & " } "
                btnSendForPrinting.Attributes.Item("onclick") = "if (Page_ClientValidate('Remarks')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnSendForPrinting, Nothing).ToString() & " } "


                DesignDts()

                ViewState("htRights") = Nothing
                GetPageAccessRights()

                'SetArraListAssignedOfficeIDs()
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
                rbtnlstDateSelection.SelectedValue = "Creation Date"
                RefreshPage()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub RefreshPage()
        LoadddlCompany()
        LoadddlAccountNumbers()
        LoadddlAccountPaymentNatureByUserID()
        LoadddlProductTypeByAccountPaymentNature()
        ClearAllTextBoxes()
        FillDesignedDtByAPNatureByAssignedRole()
        Dim dtPageLoad As New DataTable
        dtPageLoad = DirectCast(ViewState("AccountNumber"), DataTable)
        If dtPageLoad.Rows.Count > 0 Then
            LoadddlBatcNumber()
            LoadgvAccountPaymentNatureProductType(True)
            ' Set by default All Buttons Visible False
            btnPayAll.Visible = False
            btnClubPosting.Visible = False
            btnSendForPrinting.Visible = False
        Else
            UIUtilities.ShowDialog(Me, "Error", "You do not have access to any transactional data. Please contact Al Baraka Administrator.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
            Exit Sub
        End If
    End Sub
    Private Sub DesignDts()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim dtOfficeID As New DataTable
        dtAccountNumber.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("BranchCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("Currency", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("PaymentNatureCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("OfficeID", GetType(System.String)))
        ViewState("AccountNumber") = dtAccountNumber
    End Sub
    Private Sub FillDesignedDtByAPNatureByAssignedRole()
        Dim dtAssignedAPNatureOfficeID As New DataTable
        dtAssignedAPNatureOfficeID.Rows.Clear()
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserSecond(Me.UserId.ToString, ArrICAssignUserRolsID)
        dtAssignedAPNatureOfficeID.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAssignedAPNatureOfficeID.Rows
            dr("DropDown") = "False"
        Next
        ViewState("AccountNumber") = Nothing
        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Pay Queue")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Pay Queue") = True Then
                    ArrICAssignUserRolsID.Add(RoleID.ToString)
                End If
            Next
        End If
    End Sub
    Private Sub SetArraListAssignedOfficeIDs()
        Dim dt As New DataTable
        ArrICAssignedOfficeID.Clear()
        dt = ICUserRolesPaymentNatureController.GetAllTaggedLocationsWithUserByUSerIDAndRoleID(Me.UserId.ToString, ArrICAssignUserRolsID)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("OfficeID").ToString
                ArrICAssignedOfficeID.Add(RoleID.ToString)
            Next
        End If
    End Sub
    Private Sub SetArraListAssignedStatus()
        Dim dt As New DataTable
        ArrICAssignedStatus.Clear()
        ArrICAssignedStatus.Add(14)
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, "", ArrICAssignUserRolsID)
            ddlCompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlCompany.Items.Clear()
                    ddlCompany.Items.Add(lit)
                    lit.Selected = True
                    ddlCompany.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
                ddlCompany.DataSource = dt
                ddlCompany.DataTextField = "CompanyName"
                ddlCompany.DataValueField = "CompanyCode"
                ddlCompany.DataBind()
                ddlCompany.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccountPaymentNatureByUserID()
        'Try
        '    Dim lit As New ListItem
        '    lit.Value = "0"
        '    lit.Text = "-- All --"
        '    ddlAcccountPaymentNature.Items.Clear()
        '    ddlAcccountPaymentNature.Items.Add(lit)
        '    lit.Selected = True
        '    ddlAcccountPaymentNature.AppendDataBoundItems = True
        '    ddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUser(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
        '    ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
        '    ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
        '    ddlAcccountPaymentNature.DataBind()
        'Catch ex As Exception
        '    ProcessModuleLoadException(Me, ex, False)
        '    UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        'End Try
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlAcccountPaymentNature.Items.Clear()
            ddlAcccountPaymentNature.Items.Add(lit)
            lit.Selected = True
            ddlAcccountPaymentNature.AppendDataBoundItems = True
            ddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForDropDown(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
            ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
            ddlAcccountPaymentNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccountNumbers()
        Try
            Dim lit As New ListItem
            lit.Text = "-- Please Select --"
            lit.Value = "0"
            ddlAccount.Items.Clear()
            ddlAccount.Items.Add(lit)
            lit.Selected = True
            ddlAccount.AppendDataBoundItems = True
            ddlAccount.DataSource = ICInstructionController.GetAccountNumbersTaggedWithUserSecond(Me.UserId.ToString, ArrICAssignUserRolsID, ddlCompany.SelectedValue.ToString)
            ddlAccount.DataTextField = "MainAccountNo"
            ddlAccount.DataValueField = "AccountNumber"
            ddlAccount.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlBatcNumber()
        Try
           
            Dim dtAccountNumber As New DataTable


            dtAccountNumber = ViewState("AccountNumber")

            SetArraListAssignedStatus()
            Dim lit As New ListItem
            lit.Text = "-- All --"
            lit.Value = "0"
            ddlBatch.Items.Clear()
            ddlBatch.Items.Add(lit)
            lit.Selected = True
            ddlBatch.AppendDataBoundItems = True
            ddlBatch.DataSource = ICInstructionController.GetAllTaggedBatchNumbersByCompanyCodeWithStatus(ArrICAssignedPaymentModes, dtAccountNumber, ddlCompany.SelectedValue.ToString, ArrICAssignedStatus, Me.UserId.ToString)
            ddlBatch.DataTextField = "FileBatchNoName"
            ddlBatch.DataValueField = "FileBatchNo"
            ddlBatch.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlProductTypeByAccountPaymentNature()
        Try
            Dim lit As New ListItem
            Dim objICAPNature As New ICAccountsPaymentNature
            objICAPNature.es.Connection.CommandTimeout = 3600
            ddlProductType.Items.Clear()
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlProductType.Items.Add(lit)
            ddlProductType.AppendDataBoundItems = True
            If ddlAcccountPaymentNature.SelectedValue.ToString <> "0" Then
                If objICAPNature.LoadByPrimaryKey(ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)) Then
                    ddlProductType.DataSource = ICAccountPaymentNatureProductTypeController.GetAllProductTypesByAccountAndPaymentNature(objICAPNature)
                    ddlProductType.DataTextField = "ProductTypeName"
                    ddlProductType.DataValueField = "ProductTypeCode"
                    ddlProductType.DataBind()
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadgvAccountPaymentNatureProductType(ByVal DoDataBind As Boolean)
        Try
          
            Dim DateType, AccountNumber, BranchCode, Currency, PaymentNatureCode, CompanyCode, ProductTypeCode, InstrumentNo, InstructionNo, BatchCode, ReferenceNo As String
            Dim FromDate, ToDate As String
            DateType = ""
            AccountNumber = ""
            BranchCode = ""
            Currency = ""
            Currency = ""
            PaymentNatureCode = ""
            CompanyCode = "0"
            ProductTypeCode = ""
            InstructionNo = ""
            InstrumentNo = ""
            BatchCode = ""
            ReferenceNo = ""
            FromDate = Nothing
            ToDate = Nothing
            If rbtnlstDateSelection.SelectedValue = "Creation Date" Then
                DateType = "Creation Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Value Date" Then
                DateType = "Value Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Approval Date" Then
                DateType = "Approval Date"
            End If

            Dim dtAccountNumber As New DataTable
            dtAccountNumber = ViewState("AccountNumber")
            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            End If
            If ddlProductType.SelectedValue.ToString <> "0" Then
                ProductTypeCode = ddlProductType.SelectedValue.ToString
            End If
            If txtInstrumentNo.Text.ToString <> "" And txtInstrumentNo.Text.ToString <> "Enter Instrument No." Then
                InstrumentNo = txtInstrumentNo.Text.ToString
            End If
            If txtInstructionNo.Text.ToString <> "" And txtInstructionNo.Text.ToString <> "Enter Instruction No." Then
                InstructionNo = txtInstructionNo.Text.ToString
            End If
            If txtReferenceNo.Text.ToString <> "" And txtReferenceNo.Text.ToString <> "Enter Reference No." Then
                ReferenceNo = txtReferenceNo.Text.ToString
            End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                FromDate = raddpCreationFromDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If Not raddpCreationToDate.SelectedDate Is Nothing Then
                ToDate = raddpCreationToDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If ddlBatch.SelectedValue <> "0" Then
                BatchCode = ddlBatch.SelectedValue.ToString
            End If
            ' Set Status Array List 
            SetArraListAssignedStatus()
            ' Set Assigned Offices Array List
            SetArraListAssignedOfficeIDs()
            ICInstructionController.GetAllInstructionsForRadGridSecond(DateType, FromDate, ToDate, dtAccountNumber, BatchCode, ProductTypeCode, InstructionNo, InstrumentNo, ReferenceNo, Me.gvAuthorization.CurrentPageIndex + 1, Me.gvAuthorization.PageSize, Me.gvAuthorization, DoDataBind, ArrICAssignedStatus, Me.UserId.ToString, 0, "", ArrICAssignedPaymentModes, CompanyCode)
            ClearSummaryAndAccountBalance()
            If gvAuthorization.Items.Count > 0 Then
                gvAuthorization.Visible = True
                lblNRF.Visible = False

                'SetJavaScript()
                btnPayAll.Visible = CBool(htRights("Pay All"))
                btnClubPosting.Visible = CBool(htRights("Club Posting"))
                btnSendForPrinting.Visible = CBool(htRights("Send For Printing"))
                btnCancelInstructions.Visible = CBool(htRights("Cancel"))
                btnSummary.Visible = True
            Else
                lblNRF.Visible = True
                gvAuthorization.Visible = False
                btnSummary.Visible = False
                btnPayAll.Visible = False
                btnClubPosting.Visible = False
                btnSendForPrinting.Visible = False
                btnCancelInstructions.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ClearSummaryAndAccountBalance()
        txtTnxCount.Text = ""
        txtTotalAmount.Text = ""
        txtAvailableBalance.Text = ""
        LoadddlAccountNumbers()
    End Sub
    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged

        Try
            'SetArraListRoleID()

            LoadddlAccountNumbers()
            LoadddlAccountPaymentNatureByUserID()
            LoadddlBatcNumber()
            LoadddlProductTypeByAccountPaymentNature()
            LoadgvAccountPaymentNatureProductType(True)
            SetProductTypeWiseProcessingButtonsVisibility()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub ddlAcccountPaymentNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcccountPaymentNature.SelectedIndexChanged
        Try
            If ddlAcccountPaymentNature.SelectedValue.ToString = "0" Then
                SetArraListRoleID()
                FillDesignedDtByAPNatureByAssignedRole()
                LoadddlBatcNumber()
                LoadddlAccountNumbers()
                LoadddlProductTypeByAccountPaymentNature()

               
                LoadgvAccountPaymentNatureProductType(True)
                SetProductTypeWiseProcessingButtonsVisibility()
            Else
FillDesignedDtByAPNatureByAssignedRole()
                SetViewStateDtOnAPNatureIndexChnage()
                LoadddlBatcNumber()
                LoadddlAccountNumbers()
                LoadddlProductTypeByAccountPaymentNature()


                LoadgvAccountPaymentNatureProductType(True)
                SetProductTypeWiseProcessingButtonsVisibility()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetViewStateDtOnAPNatureIndexChnage()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim AccountNumber, BranchCode, Currency, PaymentNatureCode As String
        Dim ArrayListOfficeID As New ArrayList
        Dim drAccountNo As DataRow
        AccountNumber = Nothing
        BranchCode = Nothing
        Currency = Nothing
        PaymentNatureCode = Nothing
        dtAccountNumber = ViewState("AccountNumber")
        ViewState("AccountNumber") = Nothing
        AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0)
        BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1)
        Currency = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2)
        PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)
        For Each dr As DataRow In dtAccountNumber.Rows
            If dr("AccountNumber").ToString.Split("-")(0) = AccountNumber And dr("AccountNumber").ToString.Split("-")(1) = BranchCode And dr("AccountNumber").ToString.Split("-")(2) = Currency And dr("PaymentNature").ToString = PaymentNatureCode Then
                ArrayListOfficeID.Add(dr("OfficeID").ToString)
            End If
        Next
        dtAccountNumber.Rows.Clear()
        If ArrayListOfficeID.Count > 0 Then
            For Each OfficeID In ArrayListOfficeID
                drAccountNo = dtAccountNumber.NewRow()
                drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
                drAccountNo("PaymentNature") = PaymentNatureCode
                drAccountNo("OfficeID") = OfficeID
                dtAccountNumber.Rows.Add(drAccountNo)
            Next
        Else
            drAccountNo = dtAccountNumber.NewRow()
            drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
            drAccountNo("PaymentNature") = PaymentNatureCode
            drAccountNo("OfficeID") = "0"
            dtAccountNumber.Rows.Add(drAccountNo)
        End If
        'dtAccountNumber.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAccountNumber.Rows
            dr("DropDown") = "True"
        Next
        ViewState("AccountNumber") = dtAccountNumber



    End Sub
    Protected Sub ddlBatch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBatch.SelectedIndexChanged
        Try
            SetArraListRoleID()
            LoadddlAccountNumbers()
            LoadddlProductTypeByAccountPaymentNature()
            LoadgvAccountPaymentNatureProductType(True)
            SetProductTypeWiseProcessingButtonsVisibility()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged
        Try
            SetArraListRoleID()
            LoadgvAccountPaymentNatureProductType(True)
            SetProductTypeWiseProcessingButtonsVisibility()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetProductTypeWiseProcessingButtonsVisibility()
        Try

            btnPayAll.Visible = False
            btnClubPosting.Visible = False
            btnSendForPrinting.Visible = False
            If gvAuthorization.Items.Count > 0 Then
                If ddlProductType.SelectedValue.ToString() <> "0" Then
                    Dim objProductType As New ICProductType
                    If objProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString()) Then
                        If objProductType.DisbursementMode.ToString() = "Cheque" Then
                            btnPayAll.Visible = False
                            btnClubPosting.Visible = False
                            btnSendForPrinting.Visible = CBool(htRights("Send For Printing"))

                        ElseIf objProductType.DisbursementMode.ToString() = "PO" Or objProductType.DisbursementMode.ToString() = "DD" Then
                            btnSendForPrinting.Visible = False
                            btnClubPosting.Visible = CBool(htRights("Club Posting"))
                            btnPayAll.Visible = CBool(htRights("Pay All"))
                        ElseIf objProductType.DisbursementMode.ToString() = "Direct Credit" Then
                            If objProductType.IsSundaryAllow = True Then
                                btnClubPosting.Visible = CBool(htRights("Club Posting"))
                                btnPayAll.Visible = False
                                btnSendForPrinting.Visible = False
                            Else
                                btnPayAll.Visible = CBool(htRights("Pay All"))
                                btnClubPosting.Visible = False
                                btnSendForPrinting.Visible = False
                            End If
                        ElseIf objProductType.DisbursementMode.ToString() = "Other Credit" Then
                            If objProductType.IsSundaryAllow = True Then
                                btnClubPosting.Visible = CBool(htRights("Club Posting"))
                                btnPayAll.Visible = False
                                btnSendForPrinting.Visible = False
                            Else
                                btnPayAll.Visible = CBool(htRights("Pay All"))
                                btnClubPosting.Visible = False
                                btnSendForPrinting.Visible = False
                            End If
                        ElseIf objProductType.DisbursementMode.ToString() = "COTC" Then
                            If objProductType.IsSundaryAllow = True Then

                                btnClubPosting.Visible = CBool(htRights("Club Posting"))
                                btnPayAll.Visible = False
                                btnSendForPrinting.Visible = False

                            Else
                                btnPayAll.Visible = CBool(htRights("Pay All"))
                                btnClubPosting.Visible = False
                                btnSendForPrinting.Visible = False
                            End If
                        ElseIf objProductType.DisbursementMode.ToString() = "Bill Payment" Then
                            
                                btnPayAll.Visible = CBool(htRights("Pay All"))
                                btnClubPosting.Visible = False
                                btnSendForPrinting.Visible = False

                        End If
                    End If


                End If
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ClearAllTextBoxes()
        txtRemarks.Text = ""

    End Sub
    Private Function CheckgvInstructionAuthentication() As Boolean
        Try

            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVInstruction In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVInstruction.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSummary.Click

        Try

                Dim InstructionCount As Double = 0
                Dim TotalAmount As Double = 0
                Dim TotalAmountInInstruction As Double
                Dim TotalCurrencyUsed, PassesCurrencyName, CurrencyInRow As String
                Dim TotalPaymentModeUsed, PassesPaymentModeName, PaymentModeInRow As String
                Dim chkSelect As CheckBox

                If CheckgvInstructionAuthentication() = False Then
                    UIUtilities.ShowDialog(Me, "Release to Bank Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                TotalCurrencyUsed = ""
                TotalPaymentModeUsed = ""
                PassesCurrencyName = ""
                PassesPaymentModeName = ""
                For Each gvInsctructionAuthenticationRow As GridDataItem In gvAuthorization.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvInsctructionAuthenticationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        TotalAmountInInstruction = 0
                        CurrencyInRow = ""
                        PaymentModeInRow = ""
                        InstructionCount = InstructionCount + 1
                        TotalAmountInInstruction = CDbl(gvInsctructionAuthenticationRow.Item("Amount").Text)
                        TotalAmount = TotalAmount + TotalAmountInInstruction


                        If TotalCurrencyUsed.ToString.Contains(CurrencyInRow) = False Then
                            TotalCurrencyUsed += CurrencyInRow.ToString & ","
                        End If
                        If TotalPaymentModeUsed.ToString.Contains(PaymentModeInRow) = False Then
                            TotalPaymentModeUsed += PaymentModeInRow.ToString & ","
                        End If
                    End If
                Next

                If Not TotalAmount = 0 Then
                    txtTotalAmount.Text = CDbl(TotalAmount).ToString("N2")
                End If
                If Not InstructionCount = 0 Then
                    txtTnxCount.Text = CInt(InstructionCount)
                End If

                InstructionCount = 0
                TotalAmount = 0
                TotalCurrencyUsed = ""
                TotalPaymentModeUsed = ""
                PassesPaymentModeName = ""
                PassesCurrencyName = ""
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub

    Protected Sub gvAuthorization_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAuthorization.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAuthorization_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','0');"
        End If

        Dim imgBtn As LinkButton
        Dim AppPath As String = "" ' DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            imgBtn = New LinkButton
            imgBtn = DirectCast(item.Cells(1).FindControl("lbInstructionID"), LinkButton)
            imgBtn.OnClientClick = "showurldialog('Instruction Details',' " & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & item.GetDataKeyValue("InstructionID").ToString().EncryptString() & "' ,true,700,650);return false;"
            If CBool(htRights("View Instruction Details")) = True Then
                imgBtn.Enabled = True

            End If
            If item.GetDataKeyValue("IsAmendmentComplete") = True Then

                gvAuthorization.AlternatingItemStyle.CssClass = "GridItemComplete"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItemComplete"


            Else
                gvAuthorization.AlternatingItemStyle.CssClass = "GridItem"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItem"


            End If
        End If



    End Sub
    Protected Sub gvAuthorization_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAuthorization.NeedDataSource
        Try
            ClearAllTextBoxes()
            SetArraListRoleID()
            LoadgvAccountPaymentNatureProductType(False)
            SetProductTypeWiseProcessingButtonsVisibility()
            'SetJavaScript()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Try
            LoadgvAccountPaymentNatureProductType(True)
            SetProductTypeWiseProcessingButtonsVisibility()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Function CheckgvClientRoleCheckedForProcessAll() As Boolean
        Try
            Dim gvRow As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each gvRow In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnPayAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPayAll.Click
        Page.Validate()

        If Page.IsValid Then

            Dim chkSelect As CheckBox
            Dim InstructionID As String = ""
            Dim objICInstruction As ICInstruction
            Dim objICInstructionForStatus As ICInstruction
            Dim dtInstructionInfo As New DataTable
            Dim ArrListStatus As New ArrayList
            Dim PassCount As Integer = 0
            Dim FailCount As Integer = 0
            Dim ArryListInstID As New ArrayList
            Dim ArryListPassStatus As ArrayList
            Dim ArryListFailStatus As ArrayList
            Dim dr As DataRow
            Dim dt As New DataTable
            Dim FinalStatus As String = Nothing
            dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
            dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
            dt.Columns.Add(New DataColumn("Message", GetType(System.String)))

            If CheckgvClientRoleCheckedForProcessAll() = False Then
                UIUtilities.ShowDialog(Me, "Pay Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    InstructionID = Nothing
                    InstructionID = gvVerificationRow.GetDataKeyValue("InstructionID").ToString()
                    objICInstruction = New ICInstruction
                    If objICInstruction.LoadByPrimaryKey(InstructionID) Then
                        If objICInstruction.Status.ToString() = "14" Then
                            ICInstructionController.UpdateInstructionStatus(InstructionID, objICInstruction.Status.ToString, "52", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE")
                            ArryListInstID.Add(InstructionID)
                        End If
                    End If
                End If
            Next
            If ArryListInstID.Count = 0 Then
                UIUtilities.ShowDialog(Me, "Pay Queue", "Invalid selected instruction(s) status", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                Exit Sub
            End If
            For Each Instruction In ArryListInstID
                objICInstruction = New ICInstruction
                Dim ClearingType As String = Nothing
                If objICInstruction.LoadByPrimaryKey(Instruction) Then
                    FinalStatus = Nothing
                    If objICInstruction.PaymentMode = "PO" Or objICInstruction.PaymentMode = "DD" Then
                        ClearingType = "Issuance"
                        FinalStatus = "16"
                    ElseIf objICInstruction.PaymentMode = "Direct Credit" Or objICInstruction.PaymentMode = "Other Credit" Or objICInstruction.PaymentMode = "Bill Payment" Then
                        FinalStatus = "15"
                    ElseIf objICInstruction.PaymentMode = "Cheque" Then
                        FinalStatus = "12"
                    ElseIf objICInstruction.PaymentMode = "COTC" Then
                        ClearingType = "Issuance"
                        FinalStatus = "48"
                    End If
                End If
                dr = dt.NewRow
                Try

                    If ICInstructionProcessController.MoveIndividualFunds(Instruction, Me.UserId, Me.UserInfo.Username, txtRemarks.Text.ToString(), FinalStatus, "14", ClearingType) Then

                        objICInstructionForStatus = New ICInstruction
                        objICInstructionForStatus.LoadByPrimaryKey(objICInstruction.InstructionID.ToString)
                        dr("InstructionID") = objICInstruction.InstructionID.ToString
                        If objICInstruction.PaymentMode = "COTC" And objICInstruction.UpToICProductTypeByProductTypeCode.IsSundaryAllow = False Then

                            dr("Message") = "Transaction sent for disbursement successfully"

                        Else
                            dr("Message") = "Funds transferred successfully"

                        End If

                        dr("Status") = objICInstructionForStatus.Status.ToString
                        dt.Rows.Add(dr)
                        PassCount = PassCount + 1
                        Continue For
                    Else
                        objICInstructionForStatus = New ICInstruction
                        objICInstructionForStatus.LoadByPrimaryKey(objICInstruction.InstructionID.ToString)
                        '    ICUtilities.AddAuditTrail("Instruction Skip:Funds transfer fails on pay queue for instruction with ID [ " & objICInstruction.InstructionID & " ]", "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        dr("InstructionID") = objICInstruction.InstructionID.ToString
                        dr("Message") = "Instruction(s) not processed successfully."
                        dr("Status") = objICInstructionForStatus.Status.ToString
                        dt.Rows.Add(dr)
                        FailCount = FailCount + 1
                        Continue For
                    End If
                Catch ex As Exception
                    objICInstruction.SkipReason = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " skipped.Funds transfer fails on pay queue due to " & ex.Message.ToString
                    ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    objICInstructionForStatus = New ICInstruction
                    objICInstructionForStatus.LoadByPrimaryKey(objICInstruction.InstructionID.ToString)
                    'ICUtilities.AddAuditTrail("Instruction Skip:Funds transfer fails on pay queue for instruction with ID [ " & objICInstruction.InstructionID & " due to " & ex.Message.ToString, "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "SKIP")
                    dr("InstructionID") = objICInstruction.InstructionID.ToString
                    'dr("Message") = "Instruction Skipped:Funds transfer fails on pay queue due to " & ex.Message.ToString
                    dr("Message") = "Instruction Skipped"
                    dr("Status") = objICInstructionForStatus.Status.ToString
                    dt.Rows.Add(dr)
                    FailCount = FailCount + 1
                    Continue For
                End Try
            Next

            RefreshPage()
            If dt.Rows.Count > 0 Then
                gvShowSummary.DataSource = Nothing
                gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                gvShowSummary.DataBind()
                pnlShowSummary.Style.Remove("display")
                Dim scr As String
                scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
            Else
                gvShowSummary.DataSource = Nothing
                pnlShowSummary.Style.Add("display", "none")
            End If

            '' For Email & SMS

            If Not PassCount = 0 And FailCount = 0 Then
                ArrListStatus.Clear()
                ArrListStatus.Add(12)
                ArrListStatus.Add(15)
                ArrListStatus.Add(16)
                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArryListInstID, ArrListStatus, False)
                EmailUtilities.TransactionsDisbursedSuccessfully(dtInstructionInfo, ArryListInstID, ArrListStatus, Me.UserInfo.Username.ToString)
                SMSUtilities.Transactionsdisbursedsuccessfully(dtInstructionInfo, ArryListInstID, ArrListStatus)
                ArrListStatus.Clear()
                ArrListStatus.Add(12)
                dtInstructionInfo = New DataTable
                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocationForPrinting(ArryListInstID, ArrListStatus, False)
                If dtInstructionInfo.Rows.Count > 0 Then
                    EmailUtilities.Printing(dtInstructionInfo)
                    SMSUtilities.Printing(dtInstructionInfo, ArryListInstID, ArrListStatus)
                End If

                ArrListStatus.Clear()

                dtInstructionInfo = New DataTable

                ArrListStatus.Add(16)
                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocationForPrinting(ArryListInstID, ArrListStatus, True)
                If dtInstructionInfo.Rows.Count > 0 Then
                    EmailUtilities.TxnAvailableForBankApproval(dtInstructionInfo)
                    SMSUtilities.TxnAvailableForBankApproval(dtInstructionInfo, ArryListInstID, ArrListStatus)
                End If



            ElseIf Not PassCount = 0 And Not FailCount = 0 Then
                ArryListFailStatus = New ArrayList
                ArryListPassStatus = New ArrayList
                ArrListStatus.Clear()
                ArrListStatus.Add(12)
                ArrListStatus.Add(15)
                ArrListStatus.Add(16)
                ArrListStatus.Add(37)
                ArryListFailStatus.Add(37)
                ArryListPassStatus.Add(12)
                ArryListPassStatus.Add(15)
                ArryListPassStatus.Add(16)
                dtInstructionInfo = New DataTable
                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArryListInstID, ArrListStatus, False)
                EmailUtilities.SomeTransactionsWereNotDisbursed(dtInstructionInfo, ArryListInstID, ArryListPassStatus, ArryListFailStatus)
                SMSUtilities.Sometransactionswerenotdisbursed(dtInstructionInfo, ArryListInstID, ArryListPassStatus, ArryListFailStatus)
                ArrListStatus.Clear()
                ArrListStatus.Add(12)
                ArryListPassStatus = New ArrayList
                ArryListPassStatus.Add(12)
                dtInstructionInfo = New DataTable
                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocationForPrinting(ArryListInstID, ArrListStatus, False)
                If dtInstructionInfo.Rows.Count > 0 Then

                    EmailUtilities.Printing(dtInstructionInfo)
                    SMSUtilities.Printing(dtInstructionInfo, ArryListInstID, ArryListPassStatus)
                End If

                ArrListStatus.Clear()

                dtInstructionInfo = New DataTable

                ArrListStatus.Add(16)
                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocationForPrinting(ArryListInstID, ArrListStatus, True)
                If dtInstructionInfo.Rows.Count > 0 Then
                    EmailUtilities.TxnAvailableForBankApproval(dtInstructionInfo)
                    SMSUtilities.TxnAvailableForBankApproval(dtInstructionInfo, ArryListInstID, ArrListStatus)
                End If



            ElseIf Not FailCount = 0 And PassCount = 0 Then
                ArrListStatus.Clear()
                ArrListStatus.Add(37)
                ArrListStatus.Add(14)
                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArryListInstID, ArrListStatus, False)
                EmailUtilities.NoTransactionsWereDisbursed(dtInstructionInfo)
                SMSUtilities.Notransactionsweredisbursed(dtInstructionInfo)
            End If
        End If
    End Sub
    Protected Sub btnClubPosting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClubPosting.Click
        Page.Validate()

        If Page.IsValid Then
            Try

                Try
                    If CBUtilities.GetEODStatus() = "01" Then
                        UIUtilities.ShowDialog(Me, "Error", "EOD Status : " & CBUtilities.GetErrorMessageForTransactionFromResponseCode("01", "EOD"), Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Catch ex As Exception
                    UIUtilities.ShowDialog(Me, "Error", "EOD Status : " & ex.Message.ToString, Dialogmessagetype.Failure)
                    Exit Sub
                End Try
                Dim chkSelect As CheckBox
                Dim ArryListInstructionId As New ArrayList
                Dim InstructionCount As Integer = 0
                Dim Remarks As String = ""
                Dim dt As New DataTable
                Dim FinalStatus As String = Nothing
                Dim objICProductType As New ICProductType
                Dim objICInstruction As ICInstruction

                Dim dtInstructionInfo As New DataTable
                Dim ArrListStatus As New ArrayList
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0
                Dim ArryListInstID As New ArrayList
                Dim ArryListPassStatus As ArrayList
                Dim ArryListFailtatus As ArrayList

                dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Message", GetType(System.String)))
                ArryListInstructionId.Clear()

                If txtRemarks.Text <> "" And txtRemarks.Text <> "Enter Remarks" Then
                    Remarks = txtRemarks.Text.ToString
                End If
                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Pay Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        InstructionCount = InstructionCount + 1
                        objICInstruction = New ICInstruction
                        objICInstruction.LoadByPrimaryKey(gvVerificationRow.GetDataKeyValue("InstructionID"))
                        If objICInstruction.Status.ToString = "14" Then
                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID, objICInstruction.Status.ToString, "52", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE")
                            ArryListInstructionId.Add(objICInstruction.InstructionID)
                        End If
                    End If
                Next



                If ArryListInstructionId.Count > 0 Then
                    objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)
                    Dim ClearingType As String = Nothing
                    FinalStatus = Nothing
                    If objICProductType.DisbursementMode = "PO" Or objICProductType.DisbursementMode = "DD" Then
                        ClearingType = "Issuance"
                        FinalStatus = "16"
                    ElseIf objICProductType.DisbursementMode = "Direct Credit" Or objICProductType.DisbursementMode = "Other Credit" Then
                        FinalStatus = "15"
                    ElseIf objICProductType.DisbursementMode = "Cheque" Then
                        FinalStatus = "12"
                    ElseIf objICProductType.DisbursementMode = "COTC" Then
                        ClearingType = "Issuance"
                        FinalStatus = "48"
                    End If
                    ICInstructionProcessController.MoveClubedFunds(ArryListInstructionId, Me.UserId.ToString, Me.UserInfo.Username.ToString, Remarks, dt, PassCount, FailCount, FinalStatus, "14", ClearingType)
                Else
                    UIUtilities.ShowDialog(Me, "Error", "Invalid selected instruction(s) status", Dialogmessagetype.Failure, NavigateURL())
                    Exit Sub
                End If
                RefreshPage()
                If dt.Rows.Count > 0 Then
                    gvShowSummary.DataSource = Nothing
                    gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                    gvShowSummary.DataBind()
                    pnlShowSummary.Style.Remove("display")
                    Dim scr As String
                    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                Else
                    gvShowSummary.DataSource = Nothing
                    pnlShowSummary.Style.Add("display", "none")
                End If

                ' SMS Email

                If Not PassCount = 0 And FailCount = 0 Then
                    ArrListStatus.Clear()
                    ArryListPassStatus = New ArrayList
                    ArryListPassStatus.Add(15)
                    ArryListPassStatus.Add(16)
                    ArrListStatus.Add(15)
                    ArrListStatus.Add(16)
                    dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArryListInstructionId, ArrListStatus, False)
                    EmailUtilities.TransactionsDisbursedSuccessfully(dtInstructionInfo, ArryListInstructionId, ArrListStatus, Me.UserInfo.Username.ToString)
                    SMSUtilities.Transactionsdisbursedsuccessfully(dtInstructionInfo, ArryListInstructionId, ArryListPassStatus)


                    ArrListStatus.Clear()

                    dtInstructionInfo = New DataTable

                    ArrListStatus.Add(16)
                    dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocationForPrinting(ArryListInstructionId, ArrListStatus, True)
                    If dtInstructionInfo.Rows.Count > 0 Then
                        EmailUtilities.TxnAvailableForBankApproval(dtInstructionInfo)
                        SMSUtilities.TxnAvailableForBankApproval(dtInstructionInfo, ArryListInstructionId, ArrListStatus)
                    End If



                ElseIf Not PassCount = 0 And Not FailCount = 0 Then
                    ArrListStatus.Clear()
                    ArryListPassStatus = New ArrayList
                    ArryListFailtatus = New ArrayList
                    ArryListPassStatus.Add(15)
                    ArryListPassStatus.Add(16)
                    ArryListFailtatus.Add(37)
                    ArrListStatus.Add(37)
                    ArrListStatus.Add(15)
                    ArrListStatus.Add(16)
                    ArrListStatus.Add(14)
                    dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArryListInstructionId, ArrListStatus, False)
                    EmailUtilities.SomeTransactionsWereNotDisbursed(dtInstructionInfo, ArryListInstructionId, ArryListPassStatus, ArryListFailtatus)
                    SMSUtilities.Sometransactionswerenotdisbursed(dtInstructionInfo, ArryListInstructionId, ArryListPassStatus, ArryListFailtatus)


                    ArrListStatus.Clear()

                    dtInstructionInfo = New DataTable

                    ArrListStatus.Add(16)
                    dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocationForPrinting(ArryListInstructionId, ArrListStatus, True)
                    If dtInstructionInfo.Rows.Count > 0 Then
                        EmailUtilities.TxnAvailableForBankApproval(dtInstructionInfo)
                        SMSUtilities.TxnAvailableForBankApproval(dtInstructionInfo, ArryListInstructionId, ArrListStatus)
                    End If


                ElseIf Not FailCount = 0 And PassCount = 0 Then
                    ArrListStatus.Clear()
                    ArrListStatus.Add(37)
                    ArrListStatus.Add(14)
                    dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArryListInstructionId, ArrListStatus, False)
                    EmailUtilities.NoTransactionsWereDisbursed(dtInstructionInfo)
                    SMSUtilities.Notransactionsweredisbursed(dtInstructionInfo)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Protected Sub btnSendForPrinting_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendForPrinting.Click
        Page.Validate()

        If Page.IsValid Then
            Dim chkSelect As CheckBox
            Dim InstructionID As String = ""
            Dim objICInstruction As ICInstruction
            Dim objICInstructionForStatus As ICInstruction
            Dim PassCount As Integer = 0
            Dim FailCount As Integer = 0
            Dim dtInstructionInfo As New DataTable
            Dim ArrListStatus As New ArrayList
            Dim ArryListInstID As New ArrayList
            Dim SkipMsg As String = Nothing
            Dim dr As DataRow
            Dim dt As New DataTable
            dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
            dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
            dt.Columns.Add(New DataColumn("Message", GetType(System.String)))

            If CheckgvClientRoleCheckedForProcessAll() = False Then
                UIUtilities.ShowDialog(Me, "Pay Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    InstructionID = gvVerificationRow.GetDataKeyValue("InstructionID").ToString()
                    objICInstruction = New ICInstruction
                    If objICInstruction.LoadByPrimaryKey(InstructionID) Then
                        dr = dt.NewRow
                        If objICInstruction.Status.ToString() = "14" Then
                            ArryListInstID.Add(objICInstruction.InstructionID.ToString)
                        End If
                    End If
                End If
            Next
            If ArryListInstID.Count = 0 Then
                UIUtilities.ShowDialog(Me, "Pay Queue", "Invalid selected instruction(s) status", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                Exit Sub
            End If

            For Each Instruction In ArryListInstID
                objICInstruction = New ICInstruction
                objICInstruction.LoadByPrimaryKey(Instruction)
                dr = dt.NewRow
                Try
                    If ICInstructionProcessController.MoveIndividualFunds(objICInstruction.InstructionID, Me.UserId, Me.UserInfo.Username, txtRemarks.Text.ToString(), "12", "14") Then

                        dr("InstructionID") = objICInstruction.InstructionID.ToString
                        dr("Message") = "Instruction(s) sent for printing successfully"
                        objICInstructionForStatus = New ICInstruction
                        objICInstructionForStatus.LoadByPrimaryKey(objICInstruction.InstructionID.ToString)
                        dr("Status") = objICInstructionForStatus.Status.ToString
                        dt.Rows.Add(dr)
                        PassCount = PassCount + 1
                    Else
                        SkipMsg = Nothing
                        SkipMsg = "Instruction Skip:Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped.Send for printing fails on pay queue."
                        objICInstruction.SkipReason = SkipMsg
                        ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        'ICUtilities.AddAuditTrail("Instruction Skip:Send for printing fails on pay queue for instruction with ID [ " & objICInstruction.InstructionID & " ]", "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "SKIP")
                        dr("InstructionID") = objICInstruction.InstructionID.ToString
                        dr("Message") = "Instruction(s) not sent for printing successfully"
                        objICInstructionForStatus = New ICInstruction
                        objICInstructionForStatus.LoadByPrimaryKey(objICInstruction.InstructionID.ToString)
                        dr("Status") = objICInstructionForStatus.Status.ToString
                        dt.Rows.Add(dr)
                        FailCount = FailCount + 1
                        Continue For
                    End If
                Catch ex As Exception
                    SkipMsg = Nothing
                    SkipMsg = "Instruction Skip:Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped.Send for printing fails on pay queue due to " & ex.Message.ToString
                    objICInstruction.SkipReason = SkipMsg
                    ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    'ICUtilities.AddAuditTrail("Instruction Skip:Send for printing fails on pay queue for instruction with ID [ " & objICInstruction.InstructionID & " ] due to " & ex.Message.ToString, "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "SKIP")
                    dr("InstructionID") = objICInstruction.InstructionID.ToString
                    dr("Message") = "Instruction(s) not sent for printing"
                    objICInstructionForStatus = New ICInstruction
                    objICInstructionForStatus.LoadByPrimaryKey(objICInstruction.InstructionID.ToString)
                    dr("Status") = objICInstructionForStatus.Status.ToString
                    dt.Rows.Add(dr)
                    FailCount = FailCount + 1
                    Continue For
                End Try
            Next
            RefreshPage()
            If dt.Rows.Count > 0 Then
                gvShowSummary.DataSource = Nothing
                gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                gvShowSummary.DataBind()
                pnlShowSummary.Style.Remove("display")
                Dim scr As String
                scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
            Else
                gvShowSummary.DataSource = Nothing
                pnlShowSummary.Style.Add("display", "none")
            End If
            If Not PassCount = 0 Then
                ArrListStatus.Clear()
                ArrListStatus.Add(12)

                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocationForPrinting(ArryListInstID, ArrListStatus, False)
                EmailUtilities.Printing(dtInstructionInfo)
                SMSUtilities.Printing(dtInstructionInfo, ArryListInstID, ArrListStatus)
            End If

        End If
    End Sub
    Protected Sub btnCancelInstructions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelInstructions.Click
        Page.Validate()

        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim ArryListInstructionId As New ArrayList
                Dim objICInstruction As ICInstruction
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim StrAuditTrailAction As String
                Dim dr As DataRow
                ArryListInstructionId.Clear()


                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Pay Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                Dim dt As New DataTable
                dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Message", GetType(System.String)))

                For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                    End If
                Next
                If ArryListInstructionId.Count > 0 Then
                    Dim Remarks As String = ""
                    If txtRemarks.Text <> "" And txtRemarks.Text <> "Enter Remarks" Then
                        Remarks = txtRemarks.Text.ToString
                    End If
                    For Each InstructionID In ArryListInstructionId
                        objICInstruction = New ICInstruction
                        objICInstruction.es.Connection.CommandTimeout = 3600
                        objICInstruction.LoadByPrimaryKey(InstructionID)
                        dr = dt.NewRow
                        Try
                            StrAuditTrailAction = Nothing
                            StrAuditTrailAction += "Instruction with ID [ " & InstructionID & " ]  updated."
                            StrAuditTrailAction += "Status updated from  [ " & objICInstruction.Status & " ][ " & objICInstruction.UpToICInstructionStatusByStatus.StatusName & " ], to status [ " & "5" & "] "
                            StrAuditTrailAction += "by user [ " & Me.UserInfo.UserID & " ] [ " & Me.UserInfo.Username & " ]"
                            objICInstruction.CancellationDate = Date.Now

                            ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId, Me.UserInfo.Username, StrAuditTrailAction, "UPDATE")
                            ICInstructionController.UpdateInstructionStatus(InstructionID, objICInstruction.Status, "5", Me.UserId.ToString, Me.UserInfo.Username.ToString, "Remarks", "CANCEL", Remarks)

                            If objICInstruction.PaymentMode = "Cheque" And objICInstruction.InstrumentNo IsNot Nothing And objICInstruction.InstrumentNo <> "" And (objICInstruction.PrintLocationCode IsNot Nothing And objICInstruction.PrintLocationCode.ToString <> "") Then

                                ICInstrumentController.MarkInstrumentNumberIsCancelledByInstructionIDAndInstrumentNumberAndOfficeCode(objICInstruction.InstrumentNo.ToString, objICInstruction.PrintLocationCode.ToString, objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)

                            End If
                            dr("InstructionID") = InstructionID.ToString
                            dr("Message") = "Instruction(s) cancelled successfully"
                            dr("Status") = "5"
                            dt.Rows.Add(dr)
                            PassToCancelCount = PassToCancelCount + 1

                            Continue For
                        Catch ex As Exception
                            'ICUtilities.AddAuditTrail("Instruction Skip: Cancel instruction fails on Verification queue due to " & ex.Message.ToString, "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            objICInstruction.SkipReason = "Instruction Skip: Cancel instruction fails on Pay queue due to " & ex.Message.ToString & ". Action was taken by [ " & Me.UserId & " ] [ " & Me.UserInfo.Username & " ]."
                            ICInstructionController.UpdateInstructionSkipReason(InstructionID, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            'dr("Message") = "Instruction(s) not cancelled successfully due to " & ex.Message.ToString
                            dr("Message") = "Instruction(s) skipped"
                            dr("Status") = objICInstruction.Status.ToString
                            dr("InstructionID") = objICInstruction.InstructionID.ToString
                            dt.Rows.Add(dr)
                            FailToCancelCount = FailToCancelCount + 1

                            Continue For
                        End Try

                    Next
                    RefreshPage()
                    If dt.Rows.Count > 0 Then
                        gvShowSummary.DataSource = Nothing
                        gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                        gvShowSummary.DataBind()
                        pnlShowSummary.Style.Remove("display")
                        Dim scr As String
                        scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                        Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                    Else
                        gvShowSummary.DataSource = Nothing
                        pnlShowSummary.Style.Add("display", "none")
                    End If

                    If Not PassToCancelCount = 0 Then
                        Dim dtInstructionInfo As New DataTable
                        Dim ArrayListStatus As New ArrayList
                        ArrayListStatus.Clear()
                        ArrayListStatus.Add(5)
                        dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArryListInstructionId, ArrayListStatus, False)
                        EmailUtilities.Cancelledtransactions(dtInstructionInfo, Me.UserInfo.Username.ToString, "APNatureAndLocation")
                        SMSUtilities.Cancelledtransactions(Me.UserInfo.Username.ToString, dtInstructionInfo, ArrayListStatus, ArryListInstructionId, "APNatureAndLocation")
                    End If

                End If


            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub ddlAccount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAccount.SelectedIndexChanged
        Try
            If ddlAccount.SelectedValue.ToString = "0" Then
                txtAvailableBalance.Text = ""
            Else
                txtAvailableBalance.Text = ""
                txtAvailableBalance.Text = CDbl(CBUtilities.BalanceInquiry(ddlAccount.SelectedValue.Split("-")(0).ToString, ICBO.CBUtilities.AccountType.RB, "Client", ddlAccount.SelectedValue.Split("-")(0).ToString, ddlAccount.SelectedValue.Split("-")(1).ToString)).ToString("N2")
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAuthorization_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvAuthorization.PageIndexChanged
        Try
            gvAuthorization.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub raddpCreationToDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationToDate.SelectedDateChanged
        Try
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Approval Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Approval from date should be less than approval to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
            SetProductTypeWiseProcessingButtonsVisibility()
        Catch ex As Exception
            LoadgvAccountPaymentNatureProductType(True)
            SetProductTypeWiseProcessingButtonsVisibility()
        End Try
    End Sub

    Protected Sub raddpCreationFromDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationFromDate.SelectedDateChanged
        Try

            'If raddpCreationFromDate.SelectedDate Is Nothing Then
            '    UIUtilities.ShowDialog(Me, "Error", "Please select creation from date.", ICBO.IC.Dialogmessagetype.Failure)
            '    Exit Sub
            'End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Approval Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Approval from date should be less than approval to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
            SetProductTypeWiseProcessingButtonsVisibility()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub rbtnlstDateSelection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnlstDateSelection.SelectedIndexChanged
        Try
            If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now.AddYears(2)
            Else
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
            End If

            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Approval Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Approval from date should be less than approval to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
            SetProductTypeWiseProcessingButtonsVisibility()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class

