﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ReleaseToBankQueue.ascx.vb"
    Inherits="DesktopModules_ReleaseToBankQueue_ReleaseToBankQueue" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">


    function conPayAll() {
        if (window.confirm("Are you sure you wish to fund transfer?\nFinancial transaction(s) is in process, please do not refresh the page.") == true) {
            var btnPayAll = document.getElementById('<%=btnPayAll.ClientID%>')
            var ErrorMessage = document.getElementById('<%=lblMessageForQueue.ClientID%>')
            ErrorMessage.value = "Financial transaction(s) is in process, please do not refresh the page";
            ErrorMessage.style.display = 'inherit';
            btnPayAll.value = "Processing...";
            btnPayAll.disabled = true;

            var a = btnPayAll.name;
            __doPostBack(a, '');
            return true;
        }
        else {
            var ErrorMessage2 = document.getElementById('<%=lblMessageForQueue.ClientID%>')
            ErrorMessage2.value = "";
            ErrorMessage2.style.display = 'none';
            return false;
        }
    }


    function conClubPosting() {
        if (window.confirm("Are you sure you wish to fund transfer?\nFinancial transaction(s) is in process, please do not refresh the page.") == true) {
            var btnClubPosting = document.getElementById('<%=btnClubPosting.ClientID%>')
            var ErrorMessage = document.getElementById('<%=lblMessageForQueue.ClientID%>')
            ErrorMessage.value = "Financial transaction(s) is in process, please do not refresh the page";
            ErrorMessage.style.display = 'inherit';
            btnClubPosting.value = "Processing...";
            btnClubPosting.disabled = true;

            var a = btnClubPosting.name;
            __doPostBack(a, '');
            return true;
        }
        else {
            var ErrorMessage2 = document.getElementById('<%=lblMessageForQueue.ClientID%>')
            ErrorMessage2.value = "";
            ErrorMessage2.style.display = 'none';
          
            return false;
        }
    }


    function conSendForPrinting() {
        if (window.confirm("Are you sure you wish to send for printing selected instruction(s)?") == true) {
            var btnSendForPrinting = document.getElementById('<%=btnSendForPrinting.ClientID%>')
            btnSendForPrinting.value = "Processing...";
            btnSendForPrinting.disabled = true;

            var a = btnSendForPrinting.name;
            __doPostBack(a, '');
            return true;
        }
        else {
            return false;
        }
    }

    </script>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:TextBoxSetting BehaviorID="reTnxCount" Validation-IsRequired="false" EmptyMessage=""
        ErrorMessage="Transaction Count">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTnxCount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reTotalAmount" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Total Amount">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTotalAmount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reCurrency" Validation-IsRequired="false" EmptyMessage=""
        ErrorMessage="Currency">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCurrency" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rePaymentMode" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Payment Mode">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPaymentMode" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reAvailableBalance" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Available Balance">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAvailableBalance" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reInstructionNo" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Enter Instruction No." ValidationExpression="[0-9]{0,8}">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstructionNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reInstrumentNo" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Enter Instrument No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstrumentNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reReferenceNo" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Enter Reference No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reRemarks" Validation-IsRequired="false" EmptyMessage=""
        ErrorMessage="Enter Remarks" Validation-ValidationGroup="Remarks">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRemarks" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<script type="text/javascript">
    //    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
    //        var grid = document.getElementById(gridViewClientID);
    //        var cell;
    //        if (grid.rows.length > 0) {
    //            for (i = 0; i < grid.rows.length; i++) {
    //                cell = grid.rows[i].cells[CellNo];
    //                for (j = 0; j < cell.childNodes.length; j++) {
    //                    if (cell.childNodes[j].type == "checkbox") {
    //                        cell.childNodes[j].checked = document.getElementById(idOfControllingCheckbox).checked;
    //                    }
    //                }
    //            }
    //        }
    //    }
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        //      alert(grid)
        //        if ((grid.rows.length -1) != 10 ) {
        //        if ((grid.rows.length) >= 12) {
        //            //            alert('More than 10 rows')
        //            for (i = 0; i < grid.rows.length - 1; i++) {
        //                cell = grid.rows[i].cells[CellNo];
        //                for (j = 0; j < cell.childNodes.length - 1; j++) {
        //                    if (cell.childNodes[j].type == "checkbox") {
        //                        cell.childNodes[j].checked = document.getElementById(idOfControllingCheckbox).checked;
        //                    }
        //                }
        //            }
        //        }
        //        else {
        //                     alert(grid.rows.length)
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to cancel selected instruction(s)?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
    $(document).ready(function () {

        // Dialog
        $('#urldialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#loading').hide();



            },
            resizable: false
        });
        // Summary Dialog
        $('#sumdialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#sumdialog').hide();
                //                window.location.reload();
                //                window.location.reload();sa
            },
            resizable: false
        });

        
    });
    function showsumdialog(title, modal, width, height) {
        //  alert(modal);
        $('#sumdialog').dialog("option", "title", title);
        $('#sumdialog').dialog("option", "modal", modal);
        $('#sumdialog').dialog("option", "width", width);
        $('#sumdialog').dialog("option", "height", height);
        $('#sumdialog').dialog('open');
        $('#sumdialog').dialog();
        return false;

    };
    function CloseSummaryDialogBox() {
        $('#sumdialog').dialog('close');
    }
    function showurldialog(title, url, modal, width, height) {
        //  alert(modal);
        $('#urldialog').dialog("option", "title", title);
        $('#urldialog').dialog("option", "modal", modal);
        $('#urldialog').dialog("option", "width", width);
        $('#urldialog').dialog("option", "height", height);
        $('#urldialog').dialog('open');
        $('#dialogiframe').hide();
        $('#loading').show();
        $('#dialogiframe').attr("src", url);
        $('#dialogiframe').load(function () {
            $('#loading').hide();
            $('#dialogiframe').show();
        });
        return false;

    };

    function CloseIframe() {
        $('#urldialog').dialog('close');
    }
</script>
<script language="javascript" type="text/javascript">
    function AdjustWidth(ddl) {
        var maxWidth = 0;
        var Counter = 0;
        for (var i = 0; i < ddl.length; i++) {
            if (ddl.options[i].text.length > maxWidth) {
                Counter = Counter + 1;
                maxWidth = ddl.options[i].text.length;
            }
        }
        if (Counter = 1) { ddl.style.width = "250px"; }

        else {
            alert(Counter);
            ddl.style.width = maxWidth + "px";
        }
        //-->
    }
</script>
<style type="text/css">
    .ctrDropDown
    {
        width: 290px;
        font-size: 11px;
        height: 19px;
    }
    .ctrDropDownClick
    {
        width: 400px;
        font-size: 11px;
        height: 19px;
    }
    .plainDropDown
    {
        width: 290px;
        font-size: 11px;
        height: 19px;
    }
</style>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Pay Queue" CssClass="lblHeadingForQueue"></asp:Label>
                        <br />
                        <br />
                        <asp:Label ID="lblMessageForQueue" runat="server" Text="" CssClass="" ForeColor="Red" >
                        </asp:Label>
                        <br />
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Panel ID="pnlHeader" runat="server" DefaultButton="btnSearch">
                            <table align="left" valign="top" style="width: 100%">
                                <tr align="left" valign="top" style="width: 100%">
                                    <td align="left" valign="top" style="width: 43%">
                                        <table align="left" style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    &nbsp;
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:RadioButtonList ID="rbtnlstDateSelection" runat="server" RepeatDirection="Horizontal"
                                                        Font-Size="10px" AutoPostBack="True">
                                                        <asp:ListItem Value="Creation Date"> Creation Date</asp:ListItem>
                                                        <asp:ListItem Value="Value Date"> Value Date</asp:ListItem>
                                                        <asp:ListItem Value="Approval Date">Approval Date</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblDateFrom" runat="server" Text="Date From" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <telerik:RadDatePicker ID="raddpCreationFromDate" runat="server" Width="180px" AutoPostBack="true">
                                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                        </Calendar>
                                                        <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                            Font-Size="10.5px" LabelWidth="40%" type="text" value="">
                                                        </DateInput>
                                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                    </telerik:RadDatePicker>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%; height: 25%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblDateTo" runat="server" Text="Date To" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <telerik:RadDatePicker ID="raddpCreationToDate" runat="server" Width="180px" AutoPostBack="true">
                                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                        </Calendar>
                                                        <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                            LabelWidth="40%" type="text" value="" Font-Size="10.5px">
                                                        </DateInput>
                                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                    </telerik:RadDatePicker>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 45%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 25%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblCompany" runat="server" Text="Company" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdownForQueue" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblAccountPaymentNature" runat="server" Text="Account Payment Nature"
                                                        CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <%--<asp:DropDownList ID="ddlAcccountPaymentNature" runat="server" CssClass="dropdownForQueue"
                                                        AutoPostBack="True">
                                                    </asp:DropDownList>--%>
                                                     <div style="width: 290px; overflow: hidden;">                                                      
                                                        <asp:DropDownList ID="ddlAcccountPaymentNature" runat="server" class="ctrDropDown"
                                                            onblur="this.className='ctrDropDown';" onmousedown="this.className='ctrDropDownClick';"
                                                            onchange="this.className='ctrDropDown';" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </div>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblBatch" runat="server" Text="Batch" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:DropDownList ID="ddlBatch" runat="server" CssClass="dropdownForQueue" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblProductType" runat="server" Text="Product Type" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:DropDownList ID="ddlProductType" runat="server" CssClass="dropdownForQueue"
                                                        AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblRemarks" runat="server" Text="Remarks" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:TextBox ID="txtRemarks" runat="server" Width="288px" CssClass="txtboxForQueue"
                                                        TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    &nbsp;
                                                </td>
                                                <td align="left" valign="top" style="width: 70%">
                                                    <asp:Button ID="btnPayAll" runat="server" Text="Pay All" CssClass="btn2" ValidationGroup="Remarks"
                                                          OnClientClick ="javascript: return conPayAll();" />
                                                    <asp:Button ID="btnClubPosting" runat="server" CssClass="btn2" Text="Club Posting"
                                                        ValidationGroup="Remarks" OnClientClick ="javascript: return conClubPosting();" />
                                                    <asp:Button ID="btnSendForPrinting" runat="server" Text="Send For Printing" CssClass="btn2"
                                                        ValidationGroup="Remarks" OnClientClick ="javascript: return conSendForPrinting();" />
                                                    <asp:Button ID="btnCancelInstructions" runat="server" CssClass="btn2" OnClientClick="javascript: return conBukDelete();" Text="Cancel Instructions" ValidationGroup="Remarks" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="right" valign="top" style="width: 57%">
                                        <table align="right" style="width: 100%">
                                            <tr>
                                                <td align="right" valign="top" style="width: 30%">
                                                    <fieldset class="fieldsetForIE7LeftTD">
                                                        <legend class="legendForIE7LeftTD">Transaction Summary</legend>
                                                        <table align="right" style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td align="center" valign="top" style="width: 100%">
                                                                    &nbsp;
                                                                    <asp:Label ID="lblTnxCount" runat="server" Text="Transaction Count" CssClass="lblForFieldSetLabels"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr align="left" valign="top" style="width: 100%">
                                                                <td align="center" valign="top" style="width: 100%">
                                                                    <asp:TextBox ID="txtTnxCount" runat="server" MaxLength="150" Width="120px" Font-Size="10px"
                                                                        ReadOnly="True"></asp:TextBox>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr valign="top" style="width: 100%">
                                                                <td align="center" valign="top" style="width: 100%">
                                                                    <asp:Label ID="lblTotalAmount" runat="server" Text="Total Amount" CssClass="lblForFieldSetLabels"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr align="center" valign="top" style="width: 100%">
                                                                <td align="center" valign="top" style="width: 100%">
                                                                    <asp:TextBox ID="txtTotalAmount" runat="server" MaxLength="150" Width="120px" Font-Size="10px"
                                                                        HtmlEncode="false" DataFormatString="{0:N2}" ReadOnly="true"></asp:TextBox><br />
                                                                </td>
                                                            </tr>
                                                            <tr align="center" valign="top" style="width: 100%">
                                                                <td align="center" valign="top">
                                                                    <asp:Button ID="btnSummary" runat="server" Text="Summary" CssClass="btn2" CausesValidation="False" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                                <td align="left" valign="top" style="width: 70%">
                                                    <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="left" valign="top" style="width: 100%">
                                                                <fieldset class="fieldsetForIE7RightTopTD">
                                                                    <legend class="legendForIE7RightTopTD">Search</legend>
                                                                    <table align="left" style="width: 100%">
                                                                        <tr align="left" valign="top" style="width: 100%">
                                                                            <td align="left" valign="top" style="width: 25%">
                                                                                <asp:Label ID="lblInstructionNo" runat="server" Text="Instruction No." CssClass="lblForFieldSetLabels"></asp:Label>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 25%">
                                                                                <asp:Label ID="lblInstrumentNo" runat="server" Text="Instrument No." CssClass="lblForFieldSetLabels"></asp:Label>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 25%">
                                                                                <asp:Label ID="Label2" runat="server" Text="Reference No." CssClass="lblForFieldSetLabels"></asp:Label>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 25%">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr align="left" valign="top" style="width: 100%">
                                                                            <td align="left" valign="top" style="width: 28%">
                                                                                <asp:TextBox ID="txtInstructionNo" runat="server" Width="120px" Font-Size="10px"
                                                                                    MaxLength="8"></asp:TextBox>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 28%">
                                                                                <asp:TextBox ID="txtInstrumentNo" runat="server" Width="120px" Font-Size="10px" MaxLength="18"></asp:TextBox>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 28%">
                                                                                <asp:TextBox ID="txtReferenceNo" runat="server" Width="120px" Font-Size="10px" MaxLength="18"></asp:TextBox>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 16%">
                                                                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn2" CausesValidation="False" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <%--                <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 45%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 25%; height: 1px">
                                                </td>
                                            </tr>--%>
                                                        <tr>
                                                            <td align="left" valign="top" style="width: 100%">
                                                                <fieldset class="fieldsetForIE7RightBottomTD">
                                                                    <legend class="legendForIE7RightBottomTD">Check Balance</legend>
                                                                    <table align="left" style="width: 100%">
                                                                        <tr align="left" valign="top" style="width: 100%">
                                                                            <td align="left" valign="top" style="width: 60%">
                                                                                <asp:Label ID="Label4" runat="server" Text="Select Account" CssClass="lblForFieldSetLabels"></asp:Label>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 40%">
                                                                                <asp:Label ID="lblAvailableBalance" runat="server" Text="Available Balance" CssClass="lblForFieldSetLabels"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr align="left" valign="top" style="width: 100%">
                                                                            <td align="left" valign="top" style="width: 60%">
                                                                                <asp:DropDownList ID="ddlAccount" runat="server" CssClass="dropdownForQueue2" AutoPostBack="True">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 40%">
                                                                                <asp:TextBox ID="txtAvailableBalance" runat="server" MaxLength="150" Width="120px"
                                                                                    Font-Size="10px" ReadOnly="true" HtmlEncode="false" DataFormatString="{0:N2}"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                    <td align="left" valign="top" style="width: 50%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 50%">
                        &nbsp;
                        <telerik:RadGrid ID="gvAuthorization" runat="server" AllowPaging="True" Width="100%"
                            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                            PageSize="100">
                            <ClientSettings>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <MasterTableView DataKeyNames="ClientAccountCurrency,InstructionID,IsAmendmentComplete"
                                TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn >
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text="Select" Checked="false"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox2" Text="" Checked="false" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Instruction No." >
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbInstructionID" runat="server" Text='<%#Eval("InstructionID") %>'
                                                ToolTip="Instruction No." ForeColor="Blue" Enabled="false"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridTemplateColumn>
                                     <telerik:GridBoundColumn DataField="FileBatchNo" HeaderText="Batch No" SortExpression="FileBatchNo">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CreateDate" HeaderText="Creation Date" SortExpression="CreateDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}" >
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ValueDate" HeaderText="Value Date" SortExpression="ValueDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}" >
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneficiaryName" HeaderText="Bene Name" SortExpression="BeneficiaryName">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Amount" HeaderText="Amount" SortExpression="Amount"
                                        HtmlEncode="false" DataFormatString="{0:N2}">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProductTypeCode" HeaderText="Payment Mode" SortExpression="ProductTypeCode">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InstrumentNo" HeaderText="Instrument No." SortExpression="InstrumentNo">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneficiaryAccountNo" HeaderText="Account No."
                                        SortExpression="BeneficiaryAccountNo">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneAccountBranchCode" HeaderText="Branch Code"
                                        SortExpression="BeneAccountBranchCode">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridBoundColumn DataField="BeneAccountCurrency" HeaderText="Currency" SortExpression="BeneAccountCurrency">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="6%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="6%" />
                                    </telerik:GridBoundColumn>--%>
                                    <telerik:GridBoundColumn DataField="BankName" HeaderText="Bank" SortExpression="BankName">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CompanyName" HeaderText="UBPS Company Name"
                                        SortExpression="CompanyName">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="UBPReferenceField" HeaderText="Consumer ID"
                                        SortExpression="UBPReferenceField">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <HeaderStyle CssClass="GridHeader" />
                            <ItemStyle CssClass="GridItem" />
                            <AlternatingItemStyle CssClass="GridItem" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid><br />
                        <asp:Label ID="lblNRF" runat="server" Text="No Record Found." CssClass="headingblue"
                            Visible="false"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 50%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="center" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%" colspan="2">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</td> </tr>
<tr align="left" valign="top" style="width: 100%">
    <td align="left" valign="top" style="width: 100%">
    </td>
</tr>
<tr align="left" valign="top" style="width: 100%">
    <td align="left" valign="top" style="width: 100%">
    </td>
</tr>
</table> </td> </tr> </table>
<div id="urldialog" title="">
    <iframe id='dialogiframe' frameborder="0" allowtransparency="true" src="" style="border-style: none;
        width: 100%; height: 100%; overflow-x: hidden; background-color: transparent;">
    </iframe>
    <div id='loading' style="display: none; margin: 0px auto; text-align: center; width: 100%;
        height: 100%; position: relative">
        <img src='../../../images/progressbar.gif' style="left: 45%; top: 50%; position: absolute;" />
    </div>
</div>
<div id="sumdialog" title="" style="text-align: center">
    <asp:Panel ID="pnlShowSummary" runat="server" Style="display: none; width: 650px"
        ScrollBars="Vertical" Height="600px">
        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnCancel" OnClientClick="CloseSummaryDialogBox();return false;" /><br />
        <telerik:RadGrid ID="gvShowSummary" runat="server" AllowPaging="false" Width="620px"
            CssClass="RadGrid" AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0"
            ShowFooter="True" PageSize="10">
            <AlternatingItemStyle CssClass="rgAltRow" />
            <MasterTableView TableLayout="Auto">
                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                </RowIndicatorColumn>
                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="InstructionID" HeaderText="Instruction No." SortExpression="InstructionID">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                    <%-- <telerik:GridBoundColumn DataField="ReferenceNo" HeaderText="Reference No." SortExpression="ReferenceNo">
                                    </telerik:GridBoundColumn>--%>
                    <telerik:GridBoundColumn DataField="Message" HeaderText="Message" SortExpression="Message">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
                <PagerStyle AlwaysVisible="True" />
            </MasterTableView>
            <ItemStyle CssClass="rgRow" />
            <PagerStyle AlwaysVisible="True" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
    </asp:Panel>
</div>
