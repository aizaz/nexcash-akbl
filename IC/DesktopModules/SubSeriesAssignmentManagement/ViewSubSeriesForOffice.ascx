﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewSubSeriesForOffice.ascx.vb"
    Inherits="DesktopModules_FixUsers_AddEditFixRolesUser" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to remove Record(s)?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }    
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
   
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    //    }
</script>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:RegExpTextBoxSetting BehaviorID="RECellNo" Validation-IsRequired="true"
        ValidationExpression="[0-9 -]{2,15}" ErrorMessage="Invalid Cell Number" EmptyMessage="Enter Cell Number">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCellNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="REPhoneNo" Validation-IsRequired="true"
        ValidationExpression="[0-9 -]{2,15}" ErrorMessage="Invalid Phone Number" EmptyMessage="Enter Phone Number">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPhoneNo1" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="REPhoneNo2" Validation-IsRequired="false"
        ValidationExpression="[0-9 -]{2,15}" ErrorMessage="Invalid Phone Number" EmptyMessage="Enter Phone Number">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPhoneNo2" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="REUserName" Validation-IsRequired="true"
        ValidationExpression="\b(\w*)\b(.)*\b(-)*\b(_)*\b" ErrorMessage="Invalid Name"
        EmptyMessage="Enter User Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtUserName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="REDisplayName" Validation-IsRequired="true"
        ValidationExpression="\b(\w*)\b(.)*\b(-)*\b(_)*\b" ErrorMessage="Invalid Display Name"
        EmptyMessage="Enter Display Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDisplayName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="REPassword" Validation-IsRequired="false" EmptyMessage="Enter Password">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="REConfirmPassword" Validation-IsRequired="false"
        EmptyMessage="Enter Password">
        <TargetControls>
            <telerik:TargetInput ControlID="txtConfirmPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="REEmailAddress" Validation-IsRequired="true"
        ValidationExpression="^([a-zA-Z0-9_\-\.]+)@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$"
        ErrorMessage="Invalid Email" EmptyMessage="Enter Email Address">
        <TargetControls>
            <telerik:TargetInput ControlID="txtEmailAddress" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RELocation" Validation-IsRequired="false" EmptyMessage="Enter Location">
        <TargetControls>
            <telerik:TargetInput ControlID="txtLocation" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<telerik:RadInputManager ID="radPassword" runat="server" Enabled="true">
    <telerik:TextBoxSetting BehaviorID="REPassword" Validation-IsRequired="true" EmptyMessage="Enter Password">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="REConfirmPassword" Validation-IsRequired="true"
        EmptyMessage="Enter Password">
        <TargetControls>
            <telerik:TargetInput ControlID="txtConfirmPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnSaveSubSeries" runat="server" Text="Add Sub Set" CssClass="btn"
                CausesValidation="False" />
            <asp:HiddenField ID="hfBankCode" runat="server" />
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
</table>
<table width="100%">
    <tr>
        <td colspan="4" style="width: 100%">
            <table id="tblSlctBank" runat="server" style="width:100%">
                <tr>
                    <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblOfficeType" runat="server" Text="Select Office Type" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblBankBranch" runat="server" Text="Select Location" 
                            CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlOfficeType" runat="server" AutoPostBack="True" CssClass="dropdown">
                <asp:ListItem Value="0">-- All --</asp:ListItem>
                <asp:ListItem>Company Office</asp:ListItem>
                <asp:ListItem>Branch Office</asp:ListItem>
            </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlBankBranch" runat="server" AutoPostBack="True"
                            CssClass="dropdown">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                </table>
        </td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%">
            <table id="tblSlctCompanyOffice" runat="server" style="width: 100%">
                <tr>
                    <td style="width: 25%">
                        <asp:Label ID="lblGroup" runat="server" CssClass="lbl" Text="Select Group"></asp:Label>
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                    <td style="width: 25%">
                        <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%">
                        <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True" CssClass="dropdown">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                    <td style="width: 25%">
                        <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True"
                            CssClass="dropdown">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%">
                        &nbsp;</td>
                    <td style="width: 25%">
                        &nbsp;</td>
                    <td style="width: 25%">
                        &nbsp;</td>
                    <td style="width: 25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 25%">
                        <asp:Label ID="lblAccountNo" runat="server" Text="Select Account" CssClass="lbl"></asp:Label>
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                    <td style="width: 25%">
                        <asp:Label ID="lblMasterSeries" runat="server" Text="Select Master Series" 
                            CssClass="lbl"></asp:Label>
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%">
                        <asp:DropDownList ID="ddlAccountNo" runat="server" CssClass="dropdown" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                    <td style="width: 25%">
                        <asp:DropDownList ID="ddlMasterSeries" runat="server" CssClass="dropdown"
                            AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                </table>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <table id="tblCompanyOffice" runat="server" style="display:none ; width:100%">
                <tr>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblSelectOffice" runat="server" Text="Select Office" 
                            CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlCompanyOffice" runat="server" AutoPostBack="True"
                            CssClass="dropdown">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                </table></td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblMasterSeriesList" runat="server" Text="Sub Sets List" 
                CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <asp:Label ID="lblRNF" runat="server" Font-Bold="False" Text="No Record Found"
                Visible="False" CssClass="headingblue"></asp:Label>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
        <telerik:RadGrid ID="gvViewSubSeries" runat="server" AllowPaging="True"
                            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                            PageSize="100" CssClass="RadGrid">


                            <ClientSettings>
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            
                            </ClientSettings>
                             <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView NoMasterRecordsText="" DataKeyNames="SubSetID" TableLayout="Auto">
                         
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Approve" DataField="IsApproved" HeaderStyle-Width="3%">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=""
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="3%" />
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="3%" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="SubSetID" HeaderText="ID" SortExpression="SubSetID" HeaderStyle-Width="3%">
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CompanyName" HeaderText="Company" SortExpression="CompanyName" HeaderStyle-Width="10%">
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                                    </telerik:GridBoundColumn>
                                
                                     <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="Account No." SortExpression="AccountNumber" HeaderStyle-Width="9%" >
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"  Width="9%"/>
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="9%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="MasterSeriesID" HeaderText="Master Series" SortExpression="MasterSeriesID" HeaderStyle-Width="6%">
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                                    </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="OffceType" HeaderText="Location Type" SortExpression="OffceType" HeaderStyle-Width="7%">
                                      <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="OfficeName" HeaderText="Location" SortExpression="OfficeName" HeaderStyle-Width="10%">
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                                    </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="SubSetFrom" HeaderText="Sub Series From" SortExpression="SubSetFrom">
                                      <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="SubSetTo" HeaderText="Sub Series To" SortExpression="SubSetTo">
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="Series" HeaderText="Series" SortExpression="Series">
                                      <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="UnUsed" HeaderText="UnUsed" SortExpression="UnUsed">
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                                    </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="Used" HeaderText="Used" SortExpression="Used">
                                      <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                                    </telerik:GridBoundColumn>
                                     
                                      <telerik:GridTemplateColumn HeaderText="Edit">
                                      
                                        <ItemTemplate>
                                     
                                            <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("AssignSubSeriesToOffice", "&mid=" & Me.ModuleId & "&SubSetID="& Eval("SubSetID"))%>'
                                                ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                         <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Delete">
                                   
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("SubSetID") %>'
                                CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                ToolTip="Delete" />
                                        </ItemTemplate>
                                          <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnDeleteSubSeries" runat="server" Text="Delete Sub Sets" CssClass="btn" OnClientClick="javascript: return conBukDelete();"
                CausesValidation="False" />
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            
        </td>
    </tr>
</table>
