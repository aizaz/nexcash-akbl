﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Partial Class DesktopModules_FixUsers_AddEditFixRolesUser
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase

    Private SubSetID As String
    Private htRights As Hashtable
    Protected Sub DesktopModules_Users_SaveUser_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SubSetID = Request.QueryString("SubSetID").ToString
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlBankBranch()
                LoadddlCompanyByGroupCode()
                LoadddlaccountNumberByCompanyCode()
                LoadddlMasterSeries()

                If SubSetID.ToString = "0" Then
                    lblPageHeader.Text = "Add Sub Set"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    btnSave.Visible = CBool(htRights("Update"))
                    lblPageHeader.Text = "Update Sub Set"
                    btnSave.Text = "Update"
                    Dim objICSubSet As New ICSubSet
                    Dim objICOffice As New ICOffice
                    Dim objICGroup As New ICGroup
                    Dim objICCompany As New ICCompany
                    Dim objICAccounts As New ICAccounts
                    Dim objICMasterSeries As New ICMasterSeries


                    objICSubSet.es.Connection.CommandTimeout = 3600
                    objICOffice.es.Connection.CommandTimeout = 3600
                    objICGroup.es.Connection.CommandTimeout = 3600
                    objICAccounts.es.Connection.CommandTimeout = 3600
                    objICCompany.es.Connection.CommandTimeout = 3600
                    objICMasterSeries.es.Connection.CommandTimeout = 3600



                    ddlOfficeType.Enabled = False
                    ddlOffice.Enabled = False
                    ddlGroup.Enabled = False
                    ddlCompany.Enabled = False
                    ddlAccountNo.Enabled = False
                    ddlMasterSeries.Enabled = False
                    txtSubSeriesFrom.ReadOnly = True

                    objICSubSet.LoadByPrimaryKey(SubSetID)
                    objICOffice = objICSubSet.UpToICOfficeByOfficeID
                    objICAccounts.LoadByPrimaryKey(objICSubSet.AccountNumber, objICSubSet.BranchCode, objICSubSet.Currency)

                    If objICOffice.OffceType = "Branch Office" Then
                        ddlOfficeType.SelectedValue = "Branch Office"
                        LoadddlBankBranch()
                        ddlOffice.SelectedValue = objICOffice.OfficeID
                        objICCompany.LoadByPrimaryKey(objICAccounts.CompanyCode)
                        objICGroup = objICCompany.UpToICGroupByGroupCode
                        LoadddlGroup()
                        ddlGroup.SelectedValue = objICGroup.GroupCode
                        LoadddlCompanyByGroupCode()
                        ddlCompany.SelectedValue = objICCompany.CompanyCode
                        LoadddlaccountNumberByCompanyCode()
                        ddlAccountNo.SelectedValue = objICAccounts.AccountNumber + "," + objICAccounts.BranchCode + "," + objICAccounts.Currency
                        LoadddlMasterSeries()
                        ddlMasterSeries.SelectedValue = objICSubSet.MasterSeriesID
                        objICMasterSeries.LoadByPrimaryKey(ddlMasterSeries.SelectedValue.ToString)
                        RangeValidator1.MinimumValue = CLng(objICMasterSeries.StartFrom)
                        RangeValidator1.MaximumValue = CLng(objICMasterSeries.EndsAt - 1)
                        rvSubSetTo.MinimumValue = CLng(objICMasterSeries.StartFrom + 1)
                        rvSubSetTo.MaximumValue = CLng(objICMasterSeries.EndsAt)

                    ElseIf objICOffice.OffceType = "Company Office" Then
                        ddlOfficeType.SelectedValue = "Company Office"
                        objICCompany.LoadByPrimaryKey(objICOffice.CompanyCode)
                        objICGroup.LoadByPrimaryKey(objICCompany.GroupCode)
                        LoadddlGroup()
                        ddlGroup.SelectedValue = objICGroup.GroupCode
                        LoadddlCompanyByGroupCode()
                        ddlCompany.SelectedValue = objICOffice.CompanyCode.ToString
                        LoadddlBankBranch()
                        ddlOffice.SelectedValue = objICOffice.OfficeID
                        LoadddlaccountNumberByCompanyCode()
                        ddlAccountNo.SelectedValue = objICAccounts.AccountNumber + "," + objICAccounts.BranchCode + "," + objICAccounts.Currency
                        LoadddlMasterSeries()
                        ddlMasterSeries.SelectedValue = objICSubSet.MasterSeriesID
                        objICMasterSeries.LoadByPrimaryKey(ddlMasterSeries.SelectedValue.ToString)
                        RangeValidator1.MinimumValue = CLng(objICMasterSeries.StartFrom)
                        RangeValidator1.MaximumValue = CLng(objICMasterSeries.EndsAt - 1)
                        rvSubSetTo.MinimumValue = CLng(objICMasterSeries.StartFrom + 1)
                        rvSubSetTo.MaximumValue = CLng(objICMasterSeries.EndsAt)

                    End If
                    txtSubSeriesFrom.Text = objICSubSet.SubSetFrom
                    txtSubSeriesTo.Text = objICSubSet.SubSetTo
                    txtRemarks.Text = objICSubSet.Remarks
                End If


            End If


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client Stationery Management")
            ViewState("htRights") = htRights

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
Private Sub LoadddlBankBranch()
        Try
            Dim lit As New ListItem

            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlOffice.Items.Clear()
            ddlOffice.Items.Add(lit)
            ddlOffice.AppendDataBoundItems = True
            If ddlOfficeType.SelectedValue.ToString = "Company Office" Then
                ddlOffice.DataSource = ICOfficeController.GetOfficeByCompanyCodeActiveAndApprovePrintingLocatoionSecond(ddlCompany.SelectedValue.ToString)
                ddlOffice.DataTextField = "OfficeName"
                ddlOffice.DataValueField = "OfficeID"
                ddlOffice.DataBind()
            ElseIf ddlOfficeType.SelectedValue.ToString = "Branch Office" Then
                ddlOffice.DataSource = ICOfficeController.GetPrincipalBankBranchActiveAndApprovePrintingLocations()
                ddlOffice.DataTextField = "OfficeName"
                ddlOffice.DataValueField = "OfficeID"
                ddlOffice.DataBind()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCompanyByGroupCode()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"

            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlaccountNumberByCompanyCode()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"

            ddlAccountNo.Items.Clear()
            ddlAccountNo.Items.Add(lit)
            ddlAccountNo.AppendDataBoundItems = True
            ddlAccountNo.DataSource = ICAccountsController.GetAllAccountsByCompanyCodeForSubSeriesDropDown(ddlCompany.SelectedValue.ToString)
            ddlAccountNo.DataTextField = "AccountTitle"
            ddlAccountNo.DataValueField = "AccountNumber"
            ddlAccountNo.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlMasterSeries()
        Try

            Dim lit As New ListItem
            Dim dt As New DataTable
            Dim ArrStr As String()
            ArrStr = Nothing
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlMasterSeries.Items.Clear()
            ddlMasterSeries.Items.Add(lit)
            ddlMasterSeries.AppendDataBoundItems = True
            If ddlAccountNo.SelectedValue.ToString <> "0" Then
                ArrStr = ddlAccountNo.SelectedValue.ToString.Split(",")
                For Each MSItem In ICMasterSeriesController.GetAllMasterSeriesByAccnNmberBranchCodeCurrency(ArrStr(0).ToString, ArrStr(1).ToString, ArrStr(2).ToString)
                    ddlMasterSeries.Items.Add(New ListItem(MSItem.StartFrom & "-" & MSItem.EndsAt, MSItem.MasterSeriesID))
                Next
            End If
            ddlMasterSeries.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    'Private Sub LoadddlBank()
    '    Try
    '        Dim lit As New ListItem

    '        lit.Value = "0"
    '        lit.Text = "-- Please Select --"
    '        ddlBank.Items.Clear()
    '        ddlBank.Items.Add(lit)
    '        ddlBank.AppendDataBoundItems = True
    '        ddlBank.DataSource = ICBankController.GetAllApprovedActivePrincipalBanks()
    '        ddlBank.DataTextField = "BankName"
    '        ddlBank.DataValueField = "BankCode"

    '        ddlBank.DataBind()

    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    Protected Sub ddlOfficeType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOfficeType.SelectedIndexChanged
       
        LoadddlBankBranch()

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL(), False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
   
    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            If ddlOfficeType.SelectedValue.ToString = "Company Office" Then
                LoadddlBankBranch()
            End If
            LoadddlaccountNumberByCompanyCode()
            LoadddlMasterSeries()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompanyByGroupCode()
            If ddlOfficeType.SelectedValue.ToString = "Company Office" Then
                LoadddlBankBranch()
            End If
            LoadddlaccountNumberByCompanyCode()
            LoadddlMasterSeries()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub ddlAccountNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAccountNo.SelectedIndexChanged
        Try
            LoadddlMasterSeries()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim objICSubSet As New ICSubSet
                Dim objICOfficeFrom As New ICOffice
                Dim objICOFficeTo As New ICOffice
                Dim objICSubSetFrom As New ICSubSet
                Dim ArrStrForAccount As String()
                Dim SavedSubSetID As Integer = 0
                Dim AssignedLeaves As Long = 0
                Dim UpDatedLeaves As Integer = 0
                Dim TopOrder As Integer = 0
                ArrStrForAccount = Nothing

                'If IsSubSetExistsInMasterSeries(ddlMasterSeries.SelectedValue.ToString, txtSubSeriesFrom.Text.ToString, txtSubSeriesTo.Text.ToString) = False Then
                '    UIUtilities.ShowDialog(Me, "Error", "Sub set not exists", ICBO.IC.Dialogmessagetype.Failure)
                '    Exit Sub
                'End If
                objICSubSetFrom.es.Connection.CommandTimeout = 3600
                objICSubSet.es.Connection.CommandTimeout = 3600


                objICSubSet.SubSetFrom = CLng(txtSubSeriesFrom.Text)
                objICSubSet.SubSetTo = CLng(txtSubSeriesTo.Text)
                objICSubSet.OfficeID = ddlOffice.SelectedValue.ToString
                objICSubSet.MasterSeriesID = ddlMasterSeries.SelectedValue.ToString
                objICSubSet.CompanyCode = ddlCompany.SelectedValue.ToString
                objICSubSet.Remarks = txtRemarks.Text
                If ddlAccountNo.SelectedValue.ToString <> "0" Then
                    ArrStrForAccount = ddlAccountNo.SelectedValue.ToString.Split(",")
                    objICSubSet.AccountNumber = ArrStrForAccount(0).ToString
                    objICSubSet.BranchCode = ArrStrForAccount(1).ToString
                    objICSubSet.Currency = ArrStrForAccount(2).ToString
                End If
                If SubSetID = "0" Then
                    If ICInstrumentController.IsInstrumentSeriesIsAssignedOrPrintedForSubSet(objICSubSet.SubSetFrom, objICSubSet.SubSetTo, objICSubSet.MasterSeriesID) = False Then
                        objICSubSet.CreatedDate = Date.Now
                        objICSubSet.CreatedBy = Me.UserId
                        objICSubSet.CreationDate = Date.Now
                        objICSubSet.Creater = Me.UserId
                        SavedSubSetID = ICSubSetController.AddSubSetForPrintLocations(objICSubSet, False, Me.UserId.ToString, Me.UserInfo.Username.ToString, "")
                        If Not SavedSubSetID = 0 Then
                            objICSubSet.SubSetID = SavedSubSetID
                            ICInstrumentController.AssignedSubSetToPrintLocationFromSubSet(objICSubSet, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            UIUtilities.ShowDialog(Me, "Save Sub Set", "Sub set assigned to office successfully.", ICBO.IC.Dialogmessagetype.Success)
                            clear()
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Part of subset is already assigned to another location", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else
                    objICSubSetFrom.LoadByPrimaryKey(SubSetID)
                    objICSubSet.SubSetID = SubSetID
                    objICSubSet.CreatedBy = Me.UserId
                    objICSubSet.CreationDate = Date.Now

                    AssignedLeaves = CLng(CLng(objICSubSetFrom.SubSetTo) - CLng(objICSubSetFrom.SubSetFrom))
                    UpDatedLeaves = CLng(CLng(objICSubSet.SubSetTo) - CLng(objICSubSet.SubSetFrom))
                    If UpDatedLeaves < AssignedLeaves Then
                        TopOrder = AssignedLeaves - UpDatedLeaves
                        If ICInstrumentController.IsInstrumentsExistForUpdateOfSubSet(TopOrder, objICSubSet, objICSubSetFrom.SubSetTo, False) = True Then
                            ICInstrumentController.UpDateInstrumentsForUpdateOfSubSet(TopOrder, objICSubSet, objICSubSetFrom.SubSetTo, Me.UserId.ToString, Me.UserInfo.Username.ToString, False)
                            ICSubSetController.AddSubSetForPrintLocations(objICSubSet, True, Me.UserId, Me.UserInfo.Username.ToString, "")
                            UIUtilities.ShowDialog(Me, "Save Sub Set", "Sub set updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Part of subset is already assigned to another location or sub set is already used.", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    ElseIf UpDatedLeaves > AssignedLeaves Then
                        TopOrder = ((AssignedLeaves - UpDatedLeaves) * (-1))
                        If ICInstrumentController.IsInstrumentsExistForUpdateOfSubSet(TopOrder, objICSubSet, objICSubSetFrom.SubSetTo, True) = True Then
                            ICInstrumentController.UpDateInstrumentsForUpdateOfSubSet(TopOrder, objICSubSet, objICSubSetFrom.SubSetTo, Me.UserId.ToString, Me.UserInfo.Username.ToString, True)
                            ICSubSetController.AddSubSetForPrintLocations(objICSubSet, True, Me.UserId, Me.UserInfo.Username.ToString, "")
                            UIUtilities.ShowDialog(Me, "Save Sub Set", "Sub set updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Part of subset is already assigned to another location", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                            Exit Sub
                        End If
                    ElseIf UpDatedLeaves = AssignedLeaves And Not objICSubSetFrom.Remarks = objICSubSet.Remarks Then
                        ICSubSetController.AddSubSetForPrintLocations(objICSubSet, True, Me.UserId, Me.UserInfo.Username.ToString, "")
                        UIUtilities.ShowDialog(Me, "Save Sub Set", "Sub set updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    ElseIf UpDatedLeaves = AssignedLeaves Then
                        UIUtilities.ShowDialog(Me, "Save Sub Set", "No changes made in sub set", ICBO.IC.Dialogmessagetype.Failure, NavigateURL)
                        Exit Sub

                    End If
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Sub clear()
        ddlOfficeType.SelectedValue = "0"
        LoadddlGroup()
        LoadddlCompanyByGroupCode()
        LoadddlaccountNumberByCompanyCode()
        LoadddlMasterSeries()
        LoadddlBankBranch()
        txtSubSeriesFrom.Text = ""
        txtSubSeriesTo.Text = ""
        txtRemarks.Text = ""
    End Sub
    Private Function IsInstrumentsNotAssignedOrUsedAgainstPreviousLocation(ByVal OfficeCode As String, ByVal SubsetFrom As String, ByVal SubSetTo As String, ByVal SubSetID As String) As Boolean
        Dim qryObjICInstrument As New ICInstrumentsQuery("qryObjICInstrument")
        Dim qryObjICSubSet As New ICSubSetQuery("qryObjICSubSet")
        Dim dt As New DataTable
        Dim Result As Boolean = False

        qryObjICInstrument.InnerJoin(qryObjICSubSet).On(qryObjICInstrument.SubSetID = qryObjICSubSet.SubSetID)
        qryObjICInstrument.Where(qryObjICInstrument.OfficeID = OfficeCode And qryObjICInstrument.InstrumentNumber.Between(SubsetFrom, SubSetTo))
        qryObjICInstrument.Where(qryObjICInstrument.IsAssigned.IsNull And qryObjICInstrument.IsUSed.IsNull And qryObjICInstrument.SubSetID = SubSetID.ToString)
        dt = qryObjICInstrument.LoadDataTable
        If qryObjICInstrument.Load() Then
            Result = True
        End If
        Return Result
    End Function

    Public Shared Function IsSubSetExistsInMasterSeries(ByVal MasterSeriesID As String, ByVal SubSetFrom As String, ByVal SubSetTo As String) As Boolean
        Dim Result As Boolean = False
        Dim objICInstrumentsColl As New ICInstrumentsCollection
        objICInstrumentsColl.es.Connection.CommandTimeout = 3600

        objICInstrumentsColl.Query.Where(objICInstrumentsColl.Query.InstrumentNumber >= SubSetFrom And objICInstrumentsColl.Query.InstrumentNumber <= SubSetTo)
        objICInstrumentsColl.Query.Where(objICInstrumentsColl.Query.MasterSeriesID = MasterSeriesID)
        If objICInstrumentsColl.Query.Load Then
            Result = True
        End If
        Return Result
    End Function

    Protected Sub ddlMasterSeries_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMasterSeries.SelectedIndexChanged
        If ddlMasterSeries.SelectedValue.ToString <> "0" Then
            Dim objICMasterSeries As New ICMasterSeries

            objICMasterSeries.LoadByPrimaryKey(ddlMasterSeries.SelectedValue.ToString)

            RangeValidator1.MinimumValue = CLng(objICMasterSeries.StartFrom)
            RangeValidator1.MaximumValue = CLng(objICMasterSeries.EndsAt - 1)
            rvSubSetTo.MinimumValue = CLng(objICMasterSeries.StartFrom + 1)
            rvSubSetTo.MaximumValue = CLng(objICMasterSeries.EndsAt)
        ElseIf ddlMasterSeries.SelectedValue.ToString = "0" Then
            RangeValidator1.MinimumValue = 0
            RangeValidator1.MaximumValue = 0
            rvSubSetTo.MinimumValue = 0
            rvSubSetTo.MaximumValue = 0
        End If
    End Sub
End Class
