﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_StandingOrderQueue_StandingOrderQueue
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrICAssignUserRolsID As New ArrayList
    Private ArrICAssignedOfficeID As New ArrayList
    Private ArrICAssignedApprovalStatus As New ArrayList
    Private ArrICAssignedStatus As New ArrayList
    Private ArrICAssignedPaymentModes As New ArrayList
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()

            If Page.IsPostBack = False Then
                'btnCancelInstructions.Attributes.Item("onclick") = "if (Page_ClientValidate('Verify')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnCancelInstructions, Nothing).ToString() & " } "

                DesignDts()

                ViewState("htRights") = Nothing
                GetPageAccessRights()
                SetArraListAssignedPaymentModes()
                'SetArraListAssignedOfficeIDs()
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
                rbtnlstDateSelection.SelectedValue = "Creation Date"


                RefreshPage()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub RefreshPage()
        LoadddlCompany()
        LoadddlAccountPaymentNatureByUserID()
        LoadddlProductTypeByAccountPaymentNature()
        ClearAllTextBoxes()
        FillDesignedDtByAPNatureByAssignedRole()
        Dim dtPageLoad As New DataTable
        dtPageLoad = DirectCast(ViewState("AccountNumber"), DataTable)
        If dtPageLoad.Rows.Count > 0 Then
            LoadddlBatcNumber()
            LoadgvAccountPaymentNatureProductType(True)
            LoadgvAuthorizationAssigned(True)
        Else
            UIUtilities.ShowDialog(Me, "Error", "You do not have access to any transactional data. Please contact Al Baraka Administrator.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
            Exit Sub
        End If
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Standing Order Queue")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Standing Order Queue") = True Then
                    ArrICAssignUserRolsID.Add(RoleID.ToString)
                End If
            Next
        End If
    End Sub
    Private Sub DesignDts()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim dtOfficeID As New DataTable
        dtAccountNumber.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("BranchCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("Currency", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("PaymentNatureCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("OfficeID", GetType(System.String)))
        ViewState("AccountNumber") = dtAccountNumber
    End Sub
    Private Sub FillDesignedDtByAPNatureByAssignedRole()
       Dim dtAssignedAPNatureOfficeID As New DataTable
        dtAssignedAPNatureOfficeID.Rows.Clear()
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserSecond(Me.UserId.ToString, ArrICAssignUserRolsID)
        dtAssignedAPNatureOfficeID.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAssignedAPNatureOfficeID.Rows
            dr("DropDown") = "False"
        Next
        ViewState("AccountNumber") = Nothing
        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID
       
    End Sub
    Private Sub SetArraListAssignedOfficeIDs()
        Dim dt As New DataTable
        Dim objICUser As New ICUser
        ArrICAssignedOfficeID.Clear()
        dt = ICUserRolesPaymentNatureController.GetAllTaggedLocationsWithUserByUSerIDAndRoleID(Me.UserId.ToString, ArrICAssignUserRolsID)
        If dt.Rows.Count > 0 Then
            objICUser.LoadByPrimaryKey(Me.UserId)
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("OfficeID").ToString
                ArrICAssignedOfficeID.Add(RoleID.ToString)
            Next
            ArrICAssignedOfficeID.Add(objICUser.OfficeCode)
        End If
    End Sub
    Private Sub SetArraListAssignedStatus()
        ArrICAssignedStatus.Clear()
        ArrICAssignedStatus.Add(11)
    End Sub
    Private Sub SetArraListAssignedPaymentModes()
        ArrICAssignedPaymentModes.Clear()
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, "", ArrICAssignUserRolsID)
            ddlCompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlCompany.Items.Clear()
                    ddlCompany.Items.Add(lit)
                    lit.Selected = True
                    ddlCompany.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
                ddlCompany.DataSource = dt
                ddlCompany.DataTextField = "CompanyName"
                ddlCompany.DataValueField = "CompanyCode"
                ddlCompany.DataBind()
                ddlCompany.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccountPaymentNatureByUserID()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlAcccountPaymentNature.Items.Clear()
            ddlAcccountPaymentNature.Items.Add(lit)
            lit.Selected = True
            ddlAcccountPaymentNature.AppendDataBoundItems = True
            ddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForDropDown(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
            ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
            ddlAcccountPaymentNature.DataBind()
          
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub LoadddlBatcNumber()
        Try

            Dim dtAccountNumber As New DataTable

            dtAccountNumber = ViewState("AccountNumber")

            SetArraListAssignedStatus()
            Dim lit As New ListItem
            lit.Text = "-- All --"
            lit.Value = "0"
            ddlBatch.Items.Clear()
            ddlBatch.Items.Add(lit)
            lit.Selected = True
            ddlBatch.AppendDataBoundItems = True
            ddlBatch.DataSource = ICInstructionController.GetAllTaggedBatchNumbersByCompanyCodeWithStatus(ArrICAssignedPaymentModes, dtAccountNumber, ddlCompany.SelectedValue.ToString, ArrICAssignedStatus, Me.UserId.ToString)
            ddlBatch.DataTextField = "FileBatchNoName"
            ddlBatch.DataValueField = "FileBatchNo"
            ddlBatch.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlProductTypeByAccountPaymentNature()
        Try
            Dim lit As New ListItem
            Dim objICAPNature As New ICAccountsPaymentNature
            objICAPNature.es.Connection.CommandTimeout = 3600
            ddlProductType.Items.Clear()
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlProductType.Items.Add(lit)
            ddlProductType.AppendDataBoundItems = True
            If ddlAcccountPaymentNature.SelectedValue.ToString <> "0" Then
                If objICAPNature.LoadByPrimaryKey(ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)) Then
                    ddlProductType.DataSource = ICAccountPaymentNatureProductTypeController.GetAllProductTypesByAccountAndPaymentNature(objICAPNature)
                    ddlProductType.DataTextField = "ProductTypeName"
                    ddlProductType.DataValueField = "ProductTypeCode"
                    ddlProductType.DataBind()
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadgvAccountPaymentNatureProductType(ByVal DoDataBind As Boolean)
        Try
           
            Dim DateType, AccountNumber, BranchCode, Currency, PaymentNatureCode, CompanyCode, ProductTypeCode, InstrumentNo, InstructionNo, BatchCode, ReferenceNo As String
            Dim FromDate, ToDate As String
            Dim Amount As Double = 0
            Dim dtAccountNumber As New DataTable
            dtAccountNumber = ViewState("AccountNumber")
            DateType = ""
            AccountNumber = ""
            BranchCode = ""
            Currency = ""
            Currency = ""
            PaymentNatureCode = ""
            CompanyCode = "0"
            ProductTypeCode = ""
            InstructionNo = ""
            InstrumentNo = ""
            BatchCode = ""
            ReferenceNo = ""
            FromDate = Nothing
            ToDate = Nothing
            If txtAmount.Text <> "" And txtAmount.Text <> "Enter Amount" Then
                Amount = CDbl(txtAmount.Text)
            End If

            If rbtnlstDateSelection.SelectedValue = "Creation Date" Then
                DateType = "Creation Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Value Date" Then
                DateType = "Value Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Approval Date" Then
                DateType = "Approval Date"
            End If

            If txtReferenceNo.Text.ToString <> "" And txtReferenceNo.Text.ToString <> "Enter Reference No." Then
                ReferenceNo = txtReferenceNo.Text.ToString
            End If

            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            End If
            If ddlProductType.SelectedValue.ToString <> "0" Then
                ProductTypeCode = ddlProductType.SelectedValue.ToString
            End If
            If txtInstrumentNo.Text.ToString <> "" And txtInstrumentNo.Text.ToString <> "Enter Instrument No." Then
                InstrumentNo = txtInstrumentNo.Text.ToString
            End If
            If txtInstructionNo.Text.ToString <> "" And txtInstructionNo.Text.ToString <> "Enter Instruction No." Then
                InstructionNo = txtInstructionNo.Text.ToString
            End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                FromDate = raddpCreationFromDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If Not raddpCreationToDate.SelectedDate Is Nothing Then
                ToDate = raddpCreationToDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If ddlBatch.SelectedValue <> "0" Then
                BatchCode = ddlBatch.SelectedValue.ToString
            End If

            If CBool(htRights("Cancel")) = True Or CBool(htRights("View")) = True Then
                SetArraListAssignedStatus()
                SetArraListAssignedOfficeIDs()
                SetArraListAssignedPaymentModes()
                ICInstructionController.GetAllInstructionsForRadGridSecond(DateType, FromDate, ToDate, dtAccountNumber, BatchCode, ProductTypeCode, InstructionNo, InstrumentNo, ReferenceNo, Me.gvAuthorization.CurrentPageIndex + 1, Me.gvAuthorization.PageSize, Me.gvAuthorization, DoDataBind, ArrICAssignedStatus, Me.UserId.ToString, Amount, ddlAmountOp.SelectedValue.ToString, ArrICAssignedPaymentModes, CompanyCode)
                If gvAuthorization.Items.Count > 0 Then
                    gvAuthorization.Visible = True
                    lblNRF.Visible = False

                    btnCancelInstructions.Visible = CBool(htRights("Cancel"))

                Else
                    lblNRF.Visible = True
                    gvAuthorization.Visible = False
                    btnCancelInstructions.Visible = False

                End If
            Else

                'tblStatusChangeQueue.Style.Add("display", "none")
                gvAuthorization.DataSource = Nothing
                gvAuthorization.Visible = False
                lblNRF.Visible = False
                btnCancelInstructions.Visible = False



            End If


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    'Created this Method to load instructions in approval gv(new created gv) by Jawwad 11-04-2014
    Private Sub LoadgvAuthorizationAssigned(ByVal DoDataBind As Boolean)
        Try

            Dim DateType, AccountNumber, BranchCode, Currency, PaymentNatureCode, CompanyCode, ProductTypeCode, InstrumentNo, InstructionNo, BatchCode, ReferenceNo As String
            Dim FromDate, ToDate As String
            Dim Amount As Double = 0
            Dim dtAccountNumber As New DataTable
            dtAccountNumber = ViewState("AccountNumber")
            DateType = ""
            AccountNumber = ""
            BranchCode = ""
            Currency = ""
            Currency = ""
            PaymentNatureCode = ""
            CompanyCode = "0"
            ProductTypeCode = ""
            InstructionNo = ""
            InstrumentNo = ""
            BatchCode = ""
            ReferenceNo = ""
            FromDate = Nothing
            ToDate = Nothing

            'If ddlStatus.SelectedValue.ToString <> "0" Then
            '    Status = ddlStatus.SelectedValue.ToString
            'End If
            If txtAmount.Text <> "" And txtAmount.Text <> "Enter Amount" Then
                Amount = CDbl(txtAmount.Text)
            End If

            If rbtnlstDateSelection.SelectedValue = "Creation Date" Then
                DateType = "Creation Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Value Date" Then
                DateType = "Value Date"
            End If
            If ddlProductType.SelectedValue.ToString <> "0" Then
                ProductTypeCode = ddlProductType.SelectedValue.ToString
            End If
            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            End If
            If txtInstrumentNo.Text.ToString <> "" And txtInstrumentNo.Text.ToString <> "Enter Instrument No." Then
                InstrumentNo = txtInstrumentNo.Text.ToString
            End If
            If txtInstructionNo.Text.ToString <> "" And txtInstructionNo.Text.ToString <> "Enter Instruction No." Then
                InstructionNo = txtInstructionNo.Text.ToString
            End If
            If txtReferenceNo.Text.ToString <> "" And txtReferenceNo.Text.ToString <> "Enter Reference No." Then
                ReferenceNo = txtReferenceNo.Text.ToString
            End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                FromDate = raddpCreationFromDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If Not raddpCreationToDate.SelectedDate Is Nothing Then
                ToDate = raddpCreationToDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If ddlBatch.SelectedValue <> "0" Then
                BatchCode = ddlBatch.SelectedValue.ToString
            End If


            If CBool(htRights("Approve Cancellation")) = True Or CBool(htRights("View")) = True Or CBool(htRights("Cancel Approve")) = True Then
                'If IsApprovalRihtsAssigned() = True Then
                SetArraListAssignedApprovalStatus()
                ICInstructionController.GetAllInstructionsForRadGridSecond(DateType, FromDate, ToDate, dtAccountNumber, BatchCode, ProductTypeCode, InstructionNo, InstrumentNo, ReferenceNo, Me.gvAuthorizationAssigned.CurrentPageIndex + 1, Me.gvAuthorizationAssigned.PageSize, Me.gvAuthorizationAssigned, DoDataBind, ArrICAssignedApprovalStatus, Me.UserId.ToString, Amount, ddlAmountOp.SelectedValue.ToString, ArrICAssignedPaymentModes, CompanyCode)

                'ClearSummaryAndAccountBalance()

                'tblApprovalQueue.Style.Remove("display")
                If gvAuthorizationAssigned.Items.Count > 0 Then
                    gvAuthorizationAssigned.Visible = True
                    lblNRFApprovalGrid.Visible = False
                    btnApproveCancelInstructions.Visible = CBool(htRights("Approve Cancellation"))
                    btnCancelApprove.Visible = CBool(htRights("Cancel Approve"))

                Else
                    lblNRFApprovalGrid.Visible = True
                    gvAuthorizationAssigned.Visible = False
                    btnCancelApprove.Visible = False
                    btnApproveCancelInstructions.Visible = False

                End If
            Else

                'tblStatusChangeQueue.Style.Add("display", "none")
                gvAuthorizationAssigned.DataSource = Nothing
                gvAuthorizationAssigned.Visible = False
                lblNRFApprovalGrid.Visible = False
                btnCancelApprove.Visible = False
                btnApproveCancelInstructions.Visible = False
                Label3.Visible = False
            End If

                'Else
                '    tblApprovalQueue.Style.Add("display", "none")
                '    gvAuthorizationAssigned.DataSource = Nothing
                'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Function IsApprovalRihtsAssigned() As Boolean
        Dim Result As Boolean = False
        If CBool(htRights("Approve Cancellation")) = True Then
            Result = True
        End If
        Return Result
    End Function

    'Created this Method by Jawwad 11-04-2014
    Private Sub SetArraListAssignedApprovalStatus()
        ArrICAssignedApprovalStatus.Clear()
        If CBool(htRights("Approve Cancellation")) = True Or CBool(htRights("View")) = True Or CBool(htRights("Cancel Approve")) = True Then
            ArrICAssignedApprovalStatus.Add(50)
        End If

    End Sub


   
    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            'SetArraListRoleID()
            LoadddlBatcNumber()
            LoadddlAccountPaymentNatureByUserID()
            LoadddlProductTypeByAccountPaymentNature()

            LoadgvAccountPaymentNatureProductType(True)
            LoadgvAuthorizationAssigned(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlAcccountPaymentNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcccountPaymentNature.SelectedIndexChanged
         Try
            If ddlAcccountPaymentNature.SelectedValue.ToString = "0" Then
                SetArraListRoleID()

                FillDesignedDtByAPNatureByAssignedRole()
                LoadddlBatcNumber()
                LoadddlProductTypeByAccountPaymentNature()


                LoadgvAccountPaymentNatureProductType(True)
                LoadgvAuthorizationAssigned(True)
            Else
  FillDesignedDtByAPNatureByAssignedRole()
                SetViewStateDtOnAPNatureIndexChnage()
                LoadddlBatcNumber()
                LoadddlProductTypeByAccountPaymentNature()

             
                LoadgvAccountPaymentNatureProductType(True)
                LoadgvAuthorizationAssigned(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetViewStateDtOnAPNatureIndexChnage()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim AccountNumber, BranchCode, Currency, PaymentNatureCode As String
        Dim ArrayListOfficeID As New ArrayList
        Dim drAccountNo As DataRow
        AccountNumber = Nothing
        BranchCode = Nothing
        Currency = Nothing
        PaymentNatureCode = Nothing
        dtAccountNumber = ViewState("AccountNumber")
        ViewState("AccountNumber") = Nothing
        AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0)
        BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1)
        Currency = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2)
        PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)
        For Each dr As DataRow In dtAccountNumber.Rows
            If dr("AccountNumber").ToString.Split("-")(0) = AccountNumber And dr("AccountNumber").ToString.Split("-")(1) = BranchCode And dr("AccountNumber").ToString.Split("-")(2) = Currency And dr("PaymentNature").ToString = PaymentNatureCode Then
                ArrayListOfficeID.Add(dr("OfficeID").ToString)
            End If
        Next
        dtAccountNumber.Rows.Clear()
        If ArrayListOfficeID.Count > 0 Then
            For Each OfficeID In ArrayListOfficeID
                drAccountNo = dtAccountNumber.NewRow()
                drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
                drAccountNo("PaymentNature") = PaymentNatureCode
                drAccountNo("OfficeID") = OfficeID
                dtAccountNumber.Rows.Add(drAccountNo)
            Next
        Else
            drAccountNo = dtAccountNumber.NewRow()
            drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
            drAccountNo("PaymentNature") = PaymentNatureCode
            drAccountNo("OfficeID") = "0"
            dtAccountNumber.Rows.Add(drAccountNo)
        End If
        'dtAccountNumber.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAccountNumber.Rows
            dr("DropDown") = "True"
        Next
        ViewState("AccountNumber") = dtAccountNumber



    End Sub

    Protected Sub ddlBatch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBatch.SelectedIndexChanged
        Try
            SetArraListRoleID()
            LoadddlProductTypeByAccountPaymentNature()

            LoadgvAccountPaymentNatureProductType(True)
            LoadgvAuthorizationAssigned(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged
        Try
            SetArraListRoleID()



            LoadgvAccountPaymentNatureProductType(True)
            LoadgvAuthorizationAssigned(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ClearAllTextBoxes()
        txtRemarks.Text = ""
        txtInstructionNo.Text = ""
        txtInstrumentNo.Text = ""
        txtReferenceNo.Text = ""
        txtAmount.Text = ""
        ddlAmountOp.SelectedValue = "="
    End Sub
    Private Function CheckgvInstructionAuthentication() As Boolean
        Try

            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVInstruction In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVInstruction.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub gvAuthorization_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAuthorization.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAuthorization_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','0');"
        End If
        Dim imgBtn As LinkButton
        Dim AppPath As String = "" ' DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            imgBtn = New LinkButton
            imgBtn = DirectCast(item.Cells(1).FindControl("lbInstructionID"), LinkButton)
            If CBool(htRights("View Instruction Details")) = True Then
                imgBtn.Enabled = True
                imgBtn.OnClientClick = "showurldialog('Instruction Details',' " & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & item.GetDataKeyValue("InstructionID").ToString().EncryptString() & "' ,true,700,650);return false;"

            End If
            If item.GetDataKeyValue("IsAmendmentComplete") = True Then

                gvAuthorization.AlternatingItemStyle.CssClass = "GridItemComplete"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItemComplete"


            Else
                gvAuthorization.AlternatingItemStyle.CssClass = "GridItem"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItem"


            End If
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Try
                LoadgvAccountPaymentNatureProductType(True)
                LoadgvAuthorizationAssigned(True)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub
    Private Function CheckgvClientRoleCheckedForProcessAll() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Private Function CheckgvClientRoleCheckedForProcessAllApproveCancel() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvAuthorizationAssigned.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelectApproval"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnCancelInstructions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelInstructions.Click
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim ArryListInstructionId As New ArrayList
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim objICInstruction As ICInstruction
                Dim StrInstCount As String = Nothing
                Dim StrArr As String() = Nothing
                Dim StrAction As String = Nothing
                Dim Remarks As String = Nothing
                ArryListInstructionId.Clear()


                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Standing Order Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                If txtRemarks.Text <> "" And Not txtRemarks.Text Is Nothing Then
                    Remarks = txtRemarks.Text.ToString
                Else
                    Remarks = ""
                End If
                Dim dr As DataRow
                Dim dt As New DataTable
                dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Message", GetType(System.String)))
                For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                    End If
                Next
                For Each InstructionID In ArryListInstructionId
                    objICInstruction = New ICInstruction
                    objICInstruction.LoadByPrimaryKey(InstructionID)
                    If objICInstruction.Status.ToString = "11" Then
                        dr = dt.NewRow
                        Try
                            StrAction = Nothing
                            StrAction = "Instruction with ID:[" & objICInstruction.InstructionID.ToString & "]. "
                            StrAction += " Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                            objICInstruction.CancelledBy = Me.UserId
                            objICInstruction.CancelledApprovalDate = Date.Now
                            objICInstruction.Remarks = Remarks
                            ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                            ICInstructionController.UpdateInstructionStatus(InstructionID, objICInstruction.Status.ToString, "50", Me.UserId.ToString, Me.UserInfo.Username.ToString, "Remarks", txtRemarks.Text.ToString)
                            PassToCancelCount = PassToCancelCount + 1
                            dr("InstructionID") = objICInstruction.InstructionID.ToString()
                            dr("Message") = "Instruction(s) cancelled successfully"
                            dr("Status") = "50"
                            dt.Rows.Add(dr)
                            Continue For
                        Catch ex As Exception
                            objICInstruction.SkipReason = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped. Due to " & ex.Message.ToString & ". Action was taken by [ " & Me.UserId.ToString & " ][ " & Me.UserInfo.Username.ToString & " ]"
                            ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            'ICInstructionController.UpdateInstructionStatus(InstructionID, objICInstruction.Status.ToString, "37", Me.UserId.ToString, Me.UserInfo.Username.ToString, "Error", ex.Message.ToString)
                            dr("InstructionID") = objICInstruction.InstructionID.ToString()
                            'dr("Message") = "Instruction(s) not cancelled successfully due to " & ex.Message.ToString
                            dr("Message") = "Instruction(s) skipped."
                            dr("Status") = objICInstruction.Status.ToString
                            dt.Rows.Add(dr)
                            ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For
                        End Try
                    End If
                Next
                RefreshPage()
                If dt.Rows.Count > 0 Then
                    gvShowSummary.DataSource = Nothing
                    gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                    gvShowSummary.DataBind()
                    pnlShowSummary.Style.Remove("display")
                    Dim scr As String
                    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                Else
                    gvShowSummary.DataSource = Nothing
                    pnlShowSummary.Style.Add("display", "none")
                End If
                'If Not PassToCancelCount = 0 Then
                '    Dim dtInstructionInfo As New DataTable
                '    Dim ArrayListStatus As New ArrayList
                '    ArrayListStatus.Clear()
                '    ArrayListStatus.Add(5)
                '    dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArryListInstructionId, ArrayListStatus, False)
                '    EmailUtilities.Cancelledtransactions(dtInstructionInfo, Me.UserInfo.Username.ToString, "APNatureAndLocation")
                '    SMSUtilities.Cancelledtransactions(Me.UserInfo.Username.ToString, dtInstructionInfo, ArrayListStatus, ArryListInstructionId, "APNatureAndLocation")
                'End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    'Protected Sub btnCancelInstructions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelInstructions.Click
    '    Try
    '        Dim chkSelect As CheckBox
    '        Dim ArryListRequiredStatus As New ArrayList
    '        Dim ArrayListInstructionID As New ArrayList
    '        Dim ArryListInstructionId As New ArrayList
    '        Dim objICInstructionColl As New ICInstructionCollection
    '        Dim PassToCancelCount As Integer = 0
    '        Dim FailToCancelCount As Integer = 0
    '        'Dim objICInstruction As ICInstruction
    '        Dim StrInstCount As String = Nothing
    '        Dim StrArr As String() = Nothing
    '        Dim StrAction As String = Nothing
    '        Dim Remarks As String = Nothing
    '        Dim dtInstructionInfo As New DataTable
    '        Dim ArrayListStatus As New ArrayList
    '        Dim ArrylistInstID As New ArrayList
    '        Dim dtVerifiedInstruction As New DataTable
    '        ArryListInstructionId.Clear()
    '        ArryListRequiredStatus.Add(5)

    '        If CheckgvClientRoleCheckedForProcessAll() = False Then
    '            UIUtilities.ShowDialog(Me, "Standing Order Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
    '            Exit Sub
    '        End If

    '        Dim ResultStr As String = ""
    '        ResultStr = CheckInstructionRequiredStatusSecond(ArryListRequiredStatus)
    '        If ResultStr <> "OK" Then
    '            ClearAllTextBoxes()
    '            UIUtilities.ShowDialog(Me, "Standing Order Queue", "Following selected Instruction(s) are invalid :<br />" & ResultStr, ICBO.IC.Dialogmessagetype.Failure)
    '            Exit Sub
    '        End If
    '        objICInstructionColl = GetInstructionCollWithRequiredStatus(ArryListRequiredStatus)
    '        If objICInstructionColl.Count = 0 Then
    '            UIUtilities.ShowDialog(Me, "Standing Order Queue", "Invalid selected Instruction(s) status.", ICBO.IC.Dialogmessagetype.Failure)
    '            Exit Sub
    '        End If
    '        If txtRemarks.Text <> "" And Not txtRemarks.Text Is Nothing Then
    '            Remarks = txtRemarks.Text.ToString
    '        Else
    '            Remarks = ""
    '        End If


    '        For Each objICInstruction As ICInstruction In objICInstructionColl
    '            ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
    '            objICInstruction.StatusCancelledBy = Me.UserId
    '            objICInstruction.StatusCancelledDate = Date.Now
    '            objICInstruction.StatusChangeRemarks = Remarks
    '            StrAction = Nothing
    '            StrAction = "Instruction with ID [ " & objICInstruction.InstructionID & " ] mark as cancelled and pending for cancellation approval from standing order queue."
    '            StrAction += " Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
    '            Try
    '                ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
    '                ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "52", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
    '                PassToCancelCount = PassToCancelCount + 1
    '            Catch ex As Exception
    '                StrAction = Nothing
    '                StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as cancel from standing order queue due to " & ex.Message.ToString
    '                StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
    '                ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
    '                FailToCancelCount = FailToCancelCount + 1
    '                Continue For
    '            End Try
    '        Next
    '        ArrayListStatus.Add(52)
    '        dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArrayListInstructionID, ArrayListStatus, False)
    '        'If dtInstructionInfo.Rows.Count > 0 Then
    '        '    EmailUtilities.TxnMarkAsCancelledFromStatusChnagequeue(dtInstructionInfo, ArrayListInstructionID, ArrayListStatus, Me.UserInfo.Username.ToString)
    '        '    SMSUtilities.TxnavailableForCancellationFromStatusChnageQueue(dtInstructionInfo, ArrayListStatus, ArrayListInstructionID, "APNatureAndLocation")
    '        'End If
    '        If Not PassToCancelCount = 0 And Not FailToCancelCount = 0 Then
    '            RefreshPage()
    '            UIUtilities.ShowDialog(Me, "Standing Order Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) sent for cancellation approval successfully.<br /> [ " & FailToCancelCount.ToString & " ] Instruction(s) can not be sent to cancellation approval successfully.", ICBO.IC.Dialogmessagetype.Success)
    '            Exit Sub
    '        ElseIf Not PassToCancelCount = 0 And FailToCancelCount = 0 Then
    '            RefreshPage()
    '            UIUtilities.ShowDialog(Me, "Standing Order Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) sent for cancellation approval successfully.", ICBO.IC.Dialogmessagetype.Success)
    '            Exit Sub
    '        ElseIf Not FailToCancelCount = 0 And PassToCancelCount = 0 Then
    '            RefreshPage()
    '            UIUtilities.ShowDialog(Me, "Standing Order Queue", "[ " & FailToCancelCount.ToString & " ] Instruction(s) can not be sent to cancellation approval successfully.", ICBO.IC.Dialogmessagetype.Failure)
    '            Exit Sub
    '        End If

    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    Protected Sub gvAuthorization_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAuthorization.NeedDataSource
        Try
            LoadgvAccountPaymentNatureProductType(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub raddpCreationFromDate_SelectedDateChanged(sender As Object, e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationFromDate.SelectedDateChanged
        Try

           
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Approval Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Approval from date should be less than approval to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
            LoadgvAuthorizationAssigned(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub raddpCreationToDate_SelectedDateChanged(sender As Object, e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationToDate.SelectedDateChanged
        Try

            'If raddpCreationFromDate.SelectedDate Is Nothing Then
            '    UIUtilities.ShowDialog(Me, "Error", "Please select creation from date.", ICBO.IC.Dialogmessagetype.Failure)
            '    Exit Sub
            'End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Approval Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Approval from date should be less than approval to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
            LoadgvAuthorizationAssigned(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub rbtnlstDateSelection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnlstDateSelection.SelectedIndexChanged
        Try
            If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now.AddYears(2)
            Else
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
            End If

            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Approval Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Approval from date should be less than approval to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
            LoadgvAuthorizationAssigned(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#Region " Approval/Cancel Grid(New Added) By Jawwad 11-04-2014"

    Protected Sub gvAuthorizationAssigned_ItemCreated(sender As Object, e As GridItemEventArgs) Handles gvAuthorizationAssigned.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAuthorizationAssigned.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAuthorizationAssigned_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles gvAuthorizationAssigned.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAllApproval"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAuthorizationAssigned.MasterTableView.ClientID & "','0');"
        End If
        Dim imgBtn As LinkButton
        Dim AppPath As String = "" ' DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            imgBtn = New LinkButton
            imgBtn = DirectCast(item.Cells(1).FindControl("lbInstructionIDApproval"), LinkButton)

            If CBool(htRights("View Instruction Details")) = True Then
                imgBtn.OnClientClick = "showurldialog('Instruction Details',' " & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & item.GetDataKeyValue("InstructionID").ToString().EncryptString() & "' ,true,700,650);return false;"
                imgBtn.Enabled = True

            End If
            If item.GetDataKeyValue("IsAmendmentComplete") = True Then

                gvAuthorizationAssigned.AlternatingItemStyle.CssClass = "GridItemComplete"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItemComplete"


            Else
                gvAuthorizationAssigned.AlternatingItemStyle.CssClass = "GridItem"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItem"


            End If
        End If
        'If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
        '    chkProcessAll = New CheckBox
        '    chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAllApproval"), CheckBox)
        '    chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAuthorizationAssigned.MasterTableView.ClientID & "','0');"
        'End If
        'Dim imgBtn As LinkButton
        'Dim AppPath As String = DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()

        'If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
        '    Dim objICUser As New ICUser
        '    objICUser.LoadByPrimaryKey(Me.UserId.ToString)
        '    Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        '    imgBtn = New LinkButton
        '    imgBtn = DirectCast(item.Cells(1).FindControl("lbInstructionIDApproval"), LinkButton)

        '    If CBool(htRights("View Instruction Details")) = True Then
        '        imgBtn.Enabled = True
        '        imgBtn.OnClientClick = "showurldialog('Instruction Details',' " & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & item.GetDataKeyValue("InstructionID").ToString() & "' ,true,700,650);return false;"
        '    End If
        '    If item.GetDataKeyValue("Status") = "45" And (item.GetDataKeyValue("PaymentMode") = "PO" Or item.GetDataKeyValue("PaymentMode") = "DD" Or item.GetDataKeyValue("PaymentMode") = "Cheque") Then
        '        If Not item.GetDataKeyValue("ClearingOfficeCode") Is Nothing Then
        '            If (item.GetDataKeyValue("ClearingOfficeCode") = objICUser.OfficeCode.ToString) Then
        '                If item.GetDataKeyValue("IsAmendmentComplete") = True Then
        '                    gvAuthorization.AlternatingItemStyle.CssClass = ""
        '                    e.Item.CssClass = ""
        '                    gvAuthorization.AlternatingItemStyle.CssClass = "GridItemAmendedAndClearingReversal"
        '                    'gvAmendment.ItemStyle.BackColor = Color.Red
        '                    e.Item.CssClass = "GridItemAmendedAndClearingReversal"
        '                Else
        '                    gvAuthorization.AlternatingItemStyle.CssClass = ""
        '                    e.Item.CssClass = ""
        '                    gvAuthorization.AlternatingItemStyle.CssClass = "GridItemClearingReversal"
        '                    'gvAmendment.ItemStyle.BackColor = Color.Red
        '                    e.Item.CssClass = "GridItemClearingReversal"
        '                End If
        '            Else
        '                If item.GetDataKeyValue("IsAmendmentComplete") = True Then
        '                    gvAuthorization.AlternatingItemStyle.CssClass = ""
        '                    e.Item.CssClass = ""

        '                    gvAuthorization.AlternatingItemStyle.CssClass = "GridItemComplete"
        '                    'gvAmendment.ItemStyle.BackColor = Color.Red
        '                    e.Item.CssClass = "GridItemComplete"
        '                Else
        '                    gvAuthorization.AlternatingItemStyle.CssClass = ""
        '                    e.Item.CssClass = ""
        '                    gvAuthorization.AlternatingItemStyle.CssClass = "GridItem"
        '                    'gvAmendment.ItemStyle.BackColor = Color.Red
        '                    e.Item.CssClass = "GridItem"
        '                End If
        '            End If
        '        Else
        '            If item.GetDataKeyValue("IsAmendmentComplete") = True Then
        '                gvAuthorization.AlternatingItemStyle.CssClass = ""
        '                e.Item.CssClass = ""

        '                gvAuthorization.AlternatingItemStyle.CssClass = "GridItemComplete"
        '                'gvAmendment.ItemStyle.BackColor = Color.Red
        '                e.Item.CssClass = "GridItemComplete"
        '            Else
        '                gvAuthorization.AlternatingItemStyle.CssClass = ""
        '                e.Item.CssClass = ""
        '                gvAuthorization.AlternatingItemStyle.CssClass = "GridItem"
        '                'gvAmendment.ItemStyle.BackColor = Color.Red
        '                e.Item.CssClass = "GridItem"
        '            End If
        '        End If
        '    Else
        '        If item.GetDataKeyValue("IsAmendmentComplete") = True Then
        '            gvAuthorization.AlternatingItemStyle.CssClass = ""
        '            e.Item.CssClass = ""
        '            gvAuthorization.AlternatingItemStyle.CssClass = "GridItemComplete"
        '            'gvAmendment.ItemStyle.BackColor = Color.Red
        '            e.Item.CssClass = "GridItemComplete"
        '        Else
        '            gvAuthorization.AlternatingItemStyle.CssClass = Nothing
        '            e.Item.CssClass = Nothing
        '            gvAuthorization.AlternatingItemStyle.CssClass = "GridItem"
        '            'gvAmendment.ItemStyle.BackColor = Color.Red
        '            e.Item.CssClass = "GridItem"
        '        End If
        '    End If
        'End If
    End Sub

    Protected Sub gvAuthorizationAssigned_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles gvAuthorizationAssigned.NeedDataSource
        Try
            'ClearAllTextBoxes()
            SetArraListRoleID()
            LoadgvAuthorizationAssigned(False)
            'SetJavaScript()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    'Created this Method to approve cancel instruction
    Protected Sub btnApproveCancelInstructions_Click(sender As Object, e As EventArgs) Handles btnApproveCancelInstructions.Click
        If Page.IsValid Then
            Try
                Dim chkSelectApproval As CheckBox
                Dim ArryListInstructionId As New ArrayList
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim objICInstruction As ICInstruction
                Dim StrInstCount As String = Nothing
                Dim StrArr As String() = Nothing
                Dim StrAction As String = Nothing
                Dim Remarks As String = Nothing
                ArryListInstructionId.Clear()


                If CheckgvClientRoleCheckedForProcessAllApproveCancel() = False Then
                    UIUtilities.ShowDialog(Me, "Standing Order Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                If txtRemarks.Text <> "" And Not txtRemarks.Text Is Nothing Then
                    Remarks = txtRemarks.Text.ToString
                Else
                    Remarks = ""
                End If

                Dim dr As DataRow
                Dim dt As New DataTable

                dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Message", GetType(System.String)))
                For Each gvVerificationRow As GridDataItem In gvAuthorizationAssigned.Items
                    chkSelectApproval = New CheckBox
                    chkSelectApproval = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelectApproval"), CheckBox)
                    If chkSelectApproval.Checked = True Then
                        ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                    End If
                Next
                For Each InstructionID In ArryListInstructionId
                    objICInstruction = New ICInstruction
                    objICInstruction.LoadByPrimaryKey(InstructionID)
                    dr = dt.NewRow
                    If objICInstruction.CancelledBy.ToString <> Me.UserId.ToString Then
                        If objICInstruction.Status.ToString = "50" Then

                            Try
                                StrAction = Nothing
                                StrAction = "Instruction with ID:[" & objICInstruction.InstructionID.ToString & "]. "
                                StrAction += " Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                objICInstruction.CancelledApproveBy = Me.UserId
                                objICInstruction.CancellationDate = Date.Now
                                objICInstruction.Remarks = Remarks
                                ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                                ICInstructionController.UpdateInstructionStatus(InstructionID, objICInstruction.Status.ToString, "5", Me.UserId.ToString, Me.UserInfo.Username.ToString, "Remarks", txtRemarks.Text.ToString)


                                If objICInstruction.PaymentMode = "Cheque" And objICInstruction.InstrumentNo IsNot Nothing And objICInstruction.InstrumentNo <> "" And (objICInstruction.PrintLocationCode IsNot Nothing And objICInstruction.PrintLocationCode.ToString <> "") Then

                                    ICInstrumentController.MarkInstrumentNumberIsCancelledByInstructionIDAndInstrumentNumberAndOfficeCode(objICInstruction.InstrumentNo.ToString, objICInstruction.PrintLocationCode.ToString, objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)

                                End If
                                PassToCancelCount = PassToCancelCount + 1
                                dr("InstructionID") = objICInstruction.InstructionID.ToString()
                                dr("Message") = "Instruction(s) cancelled successfully"
                                dr("Status") = "5"
                                dt.Rows.Add(dr)
                                Continue For
                            Catch ex As Exception
                                objICInstruction.SkipReason = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped. Due to " & ex.Message.ToString & ". Action was taken by [ " & Me.UserId.ToString & " ][ " & Me.UserInfo.Username.ToString & " ]"
                                ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                'ICInstructionController.UpdateInstructionStatus(InstructionID, objICInstruction.Status.ToString, "37", Me.UserId.ToString, Me.UserInfo.Username.ToString, "Error", ex.Message.ToString)
                                dr("InstructionID") = objICInstruction.InstructionID.ToString()
                                'dr("Message") = "Instruction(s) not cancelled successfully due to " & ex.Message.ToString
                                dr("Message") = "Instruction(s) skipped."
                                dr("Status") = objICInstruction.Status.ToString
                                dt.Rows.Add(dr)
                                ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                FailToCancelCount = FailToCancelCount + 1
                                Continue For
                            End Try
                        End If
                    Else
                        dr("InstructionID") = objICInstruction.InstructionID.ToString()
                        dr("Message") = "Approve Cancelled. Instruction(s) must be approved by other than maker."
                        dr("Status") = "50"
                        dt.Rows.Add(dr)
                        'StrAction = Nothing
                        'StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as cancelled on approval from standing Order Queue due to instruction must be approved by other than maker."
                        'StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                        'UIUtilities.ShowDialog(Me, "Standing Order Queue", "fail to mark as cancelled on approval from standing Order Queue due to instruction must be approved by other than maker.", ICBO.IC.Dialogmessagetype.Failure)
                    End If

                Next
                RefreshPage()
                If dt.Rows.Count > 0 Then
                    gvShowSummary.DataSource = Nothing
                    gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                    gvShowSummary.DataBind()
                    pnlShowSummary.Style.Remove("display")
                    Dim scr As String
                    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                Else
                    gvShowSummary.DataSource = Nothing
                    pnlShowSummary.Style.Add("display", "none")
                End If
                'If Not PassToCancelCount = 0 Then
                '    Dim dtInstructionInfo As New DataTable
                '    Dim ArrayListStatus As New ArrayList
                '    ArrayListStatus.Clear()
                '    ArrayListStatus.Add(5)
                '    dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArryListInstructionId, ArrayListStatus, False)
                '    EmailUtilities.Cancelledtransactions(dtInstructionInfo, Me.UserInfo.Username.ToString, "APNatureAndLocation")
                '    SMSUtilities.Cancelledtransactions(Me.UserInfo.Username.ToString, dtInstructionInfo, ArrayListStatus, ArryListInstructionId, "APNatureAndLocation")
                'End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Private Function CheckgvInstructionApprovalStandingOrderQueue() As Boolean
        Try

            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVInstruction In gvAuthorizationAssigned.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVInstruction.Cells(0).FindControl("chkSelectApproval"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Private Function GetInstructionCollWithRequiredStatusForApproval(ByVal ArrayListRequiredStatus As ArrayList) As ICInstructionCollection
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim objICInstructionColl As New ICInstructionCollection
            Dim ArraListInstID As New ArrayList
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvAuthorizationAssigned.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelectApproval"), CheckBox)
                If chkSelect.Checked = True Then
                    ArraListInstID.Add(rowGVICClientRoles.GetDataKeyValue("InstructionID").ToString)
                End If
            Next
            If ArraListInstID.Count > 0 Then
                objICInstructionColl.Query.Where(objICInstructionColl.Query.InstructionID.In(ArraListInstID) And objICInstructionColl.Query.Status.In(ArrayListRequiredStatus))
                objICInstructionColl.Query.Load()
            End If

            Return objICInstructionColl
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Public Shared Function PerformCancelApprovalFormStatusChnageQueue(ByVal InstructionID As String, ByVal FinalStatus As String, ByVal FailureStatus As String, ByVal UsersID As String) As Boolean
        Try
            Dim Result As Boolean = False
            Dim CurrentStatus, LastStatus As String
            CurrentStatus = Nothing
            LastStatus = Nothing
            Dim objICInstruction As ICInstruction
            objICInstruction = New ICInstruction
            objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
            CurrentStatus = objICInstruction.Status.ToString
            LastStatus = objICInstruction.LastStatus.ToString
            If objICInstruction.PaymentMode.ToLower = "po" Then
                If objICInstruction.Status = "52" Then
                    If objICInstruction.ClearingAccountType.ToString.ToLower.Trim = "rb" Then
                        If CBUtilities.Reversal(objICInstruction.ClearingAccountNo.ToString, objICInstruction.ClearingAccountBranchCode.ToString, objICInstruction.ClearingAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.ClientAccountNo.ToString, objICInstruction.ClientAccountBranchCode.ToString, objICInstruction.ClientAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailureStatus, UsersID) = True Then
                            Result = True
                            Return Result
                            Exit Function
                        Else
                            objICInstruction = New ICInstruction
                            objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                            objICInstruction.Status = CurrentStatus
                            objICInstruction.LastStatus = LastStatus
                            objICInstruction.Save()
                            Result = False
                            Return Result
                            Exit Function
                        End If
                    ElseIf objICInstruction.ClearingAccountType.ToString.ToLower.Trim = "gl" Then
                        If CBUtilities.Reversal(objICInstruction.ClearingAccountNo.ToString, objICInstruction.ClearingAccountBranchCode.ToString, objICInstruction.ClearingAccountCurrency.ToString, objICInstruction.ClearingClientNo.ToString, objICInstruction.ClearingSeqNo.ToString, objICInstruction.ClearingProfitCentre.ToString, ICBO.CBUtilities.AccountType.GL, objICInstruction.ClientAccountNo.ToString, objICInstruction.ClientAccountBranchCode.ToString, objICInstruction.ClientAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailureStatus, UsersID) = True Then
                            Result = True
                            Return Result
                            Exit Function
                        Else
                            objICInstruction = New ICInstruction
                            objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                            objICInstruction.Status = CurrentStatus
                            objICInstruction.LastStatus = LastStatus
                            objICInstruction.Save()
                            Result = False
                            Return Result
                            Exit Function
                        End If
                    End If
                Else
                    If objICInstruction.PayableAccountType.ToString.ToLower.Trim = "rb" Then
                        If CBUtilities.Reversal(objICInstruction.PayableAccountNumber.ToString, objICInstruction.PayableAccountBranchCode.ToString, objICInstruction.PayableAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.ClientAccountNo.ToString, objICInstruction.ClientAccountBranchCode.ToString, objICInstruction.ClientAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailureStatus, UsersID) = True Then
                            Result = True
                            Return Result
                            Exit Function
                        Else
                            objICInstruction = New ICInstruction
                            objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                            objICInstruction.Status = CurrentStatus
                            objICInstruction.LastStatus = LastStatus
                            objICInstruction.Save()
                            Result = False
                            Return Result
                            Exit Function
                        End If
                    ElseIf objICInstruction.PayableAccountType.ToString.ToLower.Trim = "gl" Then
                        If CBUtilities.Reversal(objICInstruction.PayableAccountNumber.ToString, objICInstruction.PayableAccountBranchCode.ToString, objICInstruction.PayableAccountCurrency.ToString, objICInstruction.PayableClientNo.ToString, objICInstruction.PayableSeqNo.ToString, objICInstruction.PayableProfitCentre.ToString, ICBO.CBUtilities.AccountType.GL, objICInstruction.ClientAccountNo.ToString, objICInstruction.ClientAccountBranchCode.ToString, objICInstruction.ClientAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailureStatus, UsersID) = True Then
                            Result = True
                            Return Result
                            Exit Function
                        Else
                            objICInstruction = New ICInstruction
                            objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                            objICInstruction.Status = CurrentStatus
                            objICInstruction.LastStatus = LastStatus
                            objICInstruction.Save()
                            Result = False
                            Return Result
                            Exit Function
                        End If
                    End If
                End If
            Else
                Result = True
            End If
            Return Result
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Created this Method to cancel the instruction to approve and remove from approval grid to normal instruction grid
    Protected Sub btnCancelApprove_Click(sender As Object, e As EventArgs) Handles btnCancelApprove.Click
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim objICInstruction As ICInstruction
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0
                Dim ActionStr As String = Nothing
                Dim objICInstructionLastStatus As ICInstructionStatus
                Dim Remarks As String = ""
                If CheckgvClientRoleCheckedForProcessAllApproveCancel() = False Then
                    ClearAllTextBoxes()
                    UIUtilities.ShowDialog(Me, "Standing Order Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                'If txtApprovalRemarks.Text <> "" And Not txtApprovalRemarks.Text Is Nothing Then
                '    Remarks = txtApprovalRemarks.Text.ToString
                'End If
                For Each gvChnageApprovalRow As GridDataItem In gvAuthorizationAssigned.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvChnageApprovalRow.Cells(0).FindControl("chkSelectApproval"), CheckBox)
                    If chkSelect.Checked = True Then
                        objICInstruction = New ICInstruction
                        objICInstructionLastStatus = New ICInstructionStatus
                        objICInstruction.LoadByPrimaryKey(gvChnageApprovalRow.GetDataKeyValue("InstructionID"))
                        objICInstructionLastStatus.LoadByPrimaryKey(objICInstruction.LastStatus.ToString)
                        objICInstruction.CancelledApproveBy = Me.UserId
                        objICInstruction.CancellationDate = Date.Now
                        'objICInstruction.StatusApproveRemarks = Remarks
                        ActionStr = Nothing
                        ActionStr = "Instruction with ID [ " & objICInstruction.InstructionID & " ] approval cancelled from "
                        If objICInstruction.Status.ToString = "50" Then
                            ActionStr += "[50] [ Approve Cancellation ] "
                        End If
                        ActionStr += " to [ " & objICInstructionLastStatus.StatusID & " ] [ " & objICInstructionLastStatus.StatusName & " ]. Action was taken by [ " & Me.UserId & " ] [ " & Me.UserInfo.Username & " ]."
                        Try
                            'ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, ActionStr, "UPDATE")
                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, objICInstruction.LastStatus.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                            PassCount = PassCount + 1
                        Catch ex As Exception
                            FailCount = FailCount + 1
                            Continue For
                        End Try
                    End If
                Next
                If Not PassCount = 0 And Not FailCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Standing Order Queue", "[ " & PassCount.ToString & " ] Instruction(s) approval cancelled successfully.<br /> [ " & FailCount.ToString & " ] Instruction(s) approval not cancelled successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassCount = 0 And FailCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Standing Order Queue", "[ " & PassCount.ToString & " ] Instruction(s) approval cancelled successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailCount = 0 And PassCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Standing Order Queue", "[ " & FailCount.ToString & " ] Instruction(s) approval not cancelled successfully.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

#End Region

    Private Function CheckInstructionRequiredStatusSecond(ByVal ArrayListRequiredStatus As ArrayList) As String
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim objICInstructionColl As New ICInstructionCollection
            Dim ArraListInstID As New ArrayList
            Dim chkSelect As CheckBox
            Dim ResultString As String = ""
            For Each rowGVICClientRoles In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    If ArrayListRequiredStatus.Contains(rowGVICClientRoles.GetDataKeyValue("Status")) = True Then
                        ArraListInstID.Add(rowGVICClientRoles.GetDataKeyValue("InstructionID").ToString)
                    Else
                        ResultString += rowGVICClientRoles.GetDataKeyValue("InstructionID").ToString & ","
                    End If
                End If
            Next
            If ResultString = "" Then
                objICInstructionColl.Query.Where(objICInstructionColl.Query.InstructionID.In(ArraListInstID) And objICInstructionColl.Query.Status.In(ArrayListRequiredStatus))
                objICInstructionColl.Query.Load()
                If objICInstructionColl.Count > 0 Then
                    ResultString = "OK"
                End If
            Else
                ResultString = ResultString.Remove(ResultString.Length - 1, 1)
            End If
            Return ResultString
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Private Function GetInstructionCollWithRequiredStatus(ByVal ArrayListRequiredStatus As ArrayList) As ICInstructionCollection
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim objICInstructionColl As New ICInstructionCollection
            Dim ArraListInstID As New ArrayList
            Dim chkSelect As CheckBox
            Dim objICUser As New ICUser
            objICUser.LoadByPrimaryKey(Me.UserId.ToString)
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    ArraListInstID.Add(rowGVICClientRoles.GetDataKeyValue("InstructionID").ToString)
                End If
            Next
            If ArraListInstID.Count > 0 Then
                objICInstructionColl.Query.Where(objICInstructionColl.Query.InstructionID.In(ArraListInstID) And objICInstructionColl.Query.Status.In(ArrayListRequiredStatus))
                objICInstructionColl.Query.Load()
            End If
            Return objICInstructionColl
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click

    End Sub
End Class

