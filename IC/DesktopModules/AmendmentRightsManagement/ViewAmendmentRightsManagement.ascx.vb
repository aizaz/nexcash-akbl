﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Security.Permissions
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_AmendmentRightsManagement_ViewAmendmentRightsManagement
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable


    Protected Sub DesktopModules_AmendmentRightsManagement_ViewAmendmentRightsManagement_Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing

                GetPageAccessRights()
                btnAddAmendmentRights.Visible = CBool(htRights("Add"))
                LoadddlGroup()
                LoadddlCompany()
                LoadddlProductType()
                LoadgvAmendRightsManagement(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Amendment Rights Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString)
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlProductType()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlProductType.Items.Clear()
            ddlProductType.Items.Add(lit)
            ddlProductType.AppendDataBoundItems = True
            ddlProductType.DataSource = ICAccountPaymentNatureProductTypeController.GetProductTypesTaggedWithCompanies(ddlCompany.SelectedValue.ToString)
            ddlProductType.DataTextField = "ProductTypeName"
            ddlProductType.DataValueField = "ProductTypeCode"
            ddlProductType.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
            LoadddlProductType()
            LoadgvAmendRightsManagement(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub btnAddEmailSettings_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddAmendmentRights.Click
        Page.Validate()
        If Page.IsValid Then
            Try
                Response.Redirect(NavigateURL("SaveAmendmentRights", "&mid=" & Me.ModuleId & "&CompanyCode=0" & "&GroupCode=0" & "&ProductTypeCode=0"), False)

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadddlProductType()
            LoadgvAmendRightsManagement(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadgvAmendRightsManagement(ByVal DoDataBind As Boolean)
        Dim GroupCode, CompanyCode, ProductTypeCode As String
        If ddlGroup.SelectedValue.ToString <> "0" Then
            GroupCode = ddlGroup.SelectedValue.ToString
        Else
            GroupCode = ""
        End If
        If ddlCompany.SelectedValue.ToString <> "0" Then
            CompanyCode = ddlCompany.SelectedValue.ToString
        Else
            CompanyCode = ""
        End If
        If ddlProductType.SelectedValue.ToString <> "0" Then
            ProductTypeCode = ddlProductType.SelectedValue.ToString
        Else
            ProductTypeCode = ""
        End If


        ICAmendmentRightsController.GetAllAmendmentRightsForRadGrid(GroupCode, CompanyCode, ProductTypeCode, Me.gvAmendRightsManagement.CurrentPageIndex + 1, Me.gvAmendRightsManagement.PageSize, DoDataBind, Me.gvAmendRightsManagement)
        If gvAmendRightsManagement.Items.Count > 0 Then
            gvAmendRightsManagement.Visible = True
            gvAmendRightsManagement.Columns(4).Visible = CBool(htRights("Delete"))
            lblRNF.Visible = False
        Else
            gvAmendRightsManagement.Visible = False
            lblRNF.Visible = True
        End If
    End Sub

    Protected Sub gvAmendRightsManagement_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvAmendRightsManagement.ItemCommand
        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    Dim objICAmendmentRightsColl As New ICAmendmentsRightsManagementCollection
                    objICAmendmentRightsColl.es.Connection.CommandTimeout = 3600
                    Dim ArrCommandArgument As String() = Nothing
                    ArrCommandArgument = e.CommandArgument.ToString.Split(";")
                    objICAmendmentRightsColl = ICAmendmentRightsController.GetAllAmendRightsCollectionByCompanyCodeAndProductTypeCode(ArrCommandArgument(1).ToString, ArrCommandArgument(0).ToString)
                    If objICAmendmentRightsColl.Query.Load Then
                        For Each objICAmendmentRights As ICAmendmentsRightsManagement In objICAmendmentRightsColl
                            ICAmendmentRightsController.DeleteamendRightsManagement(objICAmendmentRights, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        Next
                        LoadgvAmendRightsManagement(True)
                        UIUtilities.ShowDialog(Me, "Amendment Rights Management", "Amendment rights deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                    End If
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub gvAmendRightsManagement_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAmendRightsManagement.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                ' Dim arrPageSizes() As String = {"1", "2", "5", "10", "50"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAmendRightsManagement.MasterTableView.ClientID)
                Next
                'If gvAccountsDetails.Items.Count > 0 Then
                '    myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
                'End If
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged
        Try
            LoadgvAmendRightsManagement(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAmendRightsManagement_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAmendRightsManagement.NeedDataSource
        LoadgvAmendRightsManagement(False)
    End Sub
End Class
