﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AmendmentRightsManagement.ascx.vb"
    Inherits="DesktopModules_BankRoleAccountPaymentNature_BankRoleAccountPaymentNatureTagging" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:RegExpTextBoxSetting BehaviorID="REUserName" Validation-IsRequired="true"
        ValidationExpression="\b(\w*)\b(.)*\b(-)*\b(_)*\b" ErrorMessage="Invalid Name"
        EmptyMessage="Enter User Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtUserName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior7" Validation-IsRequired="true"
        ValidationExpression="\b(\w*)\b(.)*\b(-)*\b(_)*\b" ErrorMessage="Invalid Display  Name"
        EmptyMessage="Enter Display Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDisplayName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="false"
        EmptyMessage="Enter Role Type">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRoleType" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<script type="text/javascript">
    function CheckInGridAll(chkView, chkAmend, chkAmendBulk) {

        if (document.getElementById(chkAmend).checked == true) {
            document.getElementById(chkView).checked = true;
            document.getElementById(chkAmend).checked = true;
        }

        if (document.getElementById(chkAmendBulk).checked == true) {
            document.getElementById(chkView).checked = true;
            document.getElementById(chkAmendBulk).checked = true;
        }
    }
</script>
<script type="text/javascript">
    function CheckInGridView(chkView, chkAmend, chkAmendBulk) {

        if (document.getElementById(chkView).checked == true) {
          
        }
        else {
            document.getElementById(chkView).checked = false;
            document.getElementById(chkAmend).checked = false;
            document.getElementById(chkAmendBulk).checked = false;
        }
    }
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];
      
            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {

                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
                else {
                  
                    if (cell.childNodes[j].type == "checkbox") {
                        cell.childNodes[j].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                
                }
            }
        }
    }
</script>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
    function ValidateChkList(source, arguments) {

        arguments.IsValid = IsCheckBoxChecked() ? true : false;



    }

</script>
<table style="width: 100%">
    <tr>
        <td style="width: 100%" colspan="4">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
            <br />
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqGroup" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqCompany" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlGroup" runat="server" ControlToValidate="ddlGroup"
                Display="Dynamic" ErrorMessage="Please select Group" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" ControlToValidate="ddlCompany"
                Display="Dynamic" ErrorMessage="Please select Company" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:Label ID="lblSelectProduct" runat="server" Text="Select Product" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqProduct" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:DropDownList ID="ddlProduct" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvDdlProduct" runat="server" ControlToValidate="ddlProduct"
                Display="Dynamic" ErrorMessage="Please select Product" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%">
            <%--<asp:Panel ID="pnlAmendmentFieldList" runat="server" Width="100%" Style="display: none"
                Height="250px" ScrollBars="Vertical">--%>
                <table id="tblAmendmentFieldList"  runat="server" style="width:100%; display:none">
                <tr>
                <td colspan="4" style="width: 100%">
                 <asp:Label ID="lblFieldsList" runat="server" CssClass="headingblue" Text="Amendment Fields List"></asp:Label>
                <br />
                <telerik:RadGrid ID="gvAmendmentFieldList" runat="server" AllowPaging="false" AllowSorting="false"
                    AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100">
                    <AlternatingItemStyle CssClass="rgAltRow" />
                    <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="true" />
                            </ClientSettings>
                    <MasterTableView DataKeyNames="FieldID" TableLayout="Fixed">
                        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                        <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn DataField="FieldName" HeaderText="Name" SortExpression="FieldName">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="canViewAll" runat="server" Text="Can View" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="canViewField" runat="server" Text="" />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="isAmendAllowAll" runat="server" Text="Can Amend" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="IsAmend" runat="server" Text="" />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="isAmendAllowInBulkAll" runat="server" Text="Can Amend in Bulk" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="IsBulkAmendAllow" runat="server" Text="" />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                        </EditFormSettings>
                        <PagerStyle AlwaysVisible="True" />
                    </MasterTableView>
                    <ItemStyle CssClass="rgRow" />
                    <PagerStyle AlwaysVisible="True" />
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                </telerik:RadGrid>
                </td>
                
                </tr>
                
                </table>
               
            <%--</asp:Panel>--%>
        </td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%" align="center">
            <asp:Button ID="btnSave" runat="server" Text="Save" Width="77px" CssClass="btn"  />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="75px" CssClass="btnCancel"
                 CausesValidation="False" />
        </td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%" align="center">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%" align="left">
         <%--   <asp:Panel ID="pnlAmendAllowedFieldList" runat="server" Height="250px" Style="display: none"
                ScrollBars="Vertical" Width="100%">--%>
                   <table id="tblAmendmentAlloedFieldList"  runat="server" style="width:100%; display:none">
                   <tr>
                   <td colspan="4" style="width: 100%" >
                    <asp:Label ID="lblAmenmentAllowedFieldList" runat="server" CssClass="headingblue"
                    Text="Amendment Allowed Fields List"></asp:Label><br />
                <br />
                <telerik:RadGrid ID="gvAmendedAllowedFields" runat="server" AllowPaging="False" AllowSorting="false"
                    AutoGenerateColumns="False" CellSpacing="0" PageSize="100" ShowFooter="True">
                    <AlternatingItemStyle CssClass="rgAltRow" />
                    <MasterTableView DataKeyNames="FieldID">
                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn DataField="FieldName" HeaderText="Name" SortExpression="FieldName">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CanView" HeaderText="Can View" SortExpression="CanView">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="IsAmendmentAllow" HeaderText="Can Amend" SortExpression="IsAmendmentAllow">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="IsBulkAmendmentAllow" HeaderText="Can Amend in Bulk"
                                SortExpression="IsBulkAmendmentAllow">
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ItemStyle CssClass="rgRow" />
                </telerik:RadGrid>
                   </td>
                   
                   </tr>
                   
                   </table>
               
            
        </td>
    </tr>
</table>
