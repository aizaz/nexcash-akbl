﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_BankRoleAccountPaymentNature_BankRoleAccountPaymentNatureTagging
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private CompanyCode As String
    Private GroupCode As String
    Private ProductTypeCode As String
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            CompanyCode = Request.QueryString("CompanyCode").ToString()
            ProductTypeCode = Request.QueryString("ProductTypeCode").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                clear()
                LoadddlGroup()
                LoadddlCompany()
                LoadddlProductType()
                tblAmendmentFieldList.Style.Add("display", "none")
                tblAmendmentAlloedFieldList.Style.Add("display", "none")
                If CompanyCode.ToString() = "0" And ProductTypeCode.ToString() = "0" Then
                    lblPageHeader.Text = "Add Amendment Rights Management"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    clear()
                    lblPageHeader.Text = "Edit Amendment Rights Management"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    Dim objICamendRightsManagement As New ICAmendmentsRightsManagement
                    Dim objICCompany As New ICCompany
                    Dim objICGroup As New ICGroup
                    Dim objICProductType As New ICProductType
                    objICamendRightsManagement.es.Connection.CommandTimeout = 3600

                    objICamendRightsManagement = ICAmendmentRightsController.GetAllAmendRightssingleObjectByCompanyCodeAndProductTypeCode(ProductTypeCode, CompanyCode)
                    objICCompany = objICamendRightsManagement.UpToICCompanyByCompanyCode
                    objICGroup = objICCompany.UpToICGroupByGroupCode
                    objICProductType = objICamendRightsManagement.UpToICProductTypeByProductTypeCode
                    LoadddlGroup()
                    ddlGroup.SelectedValue = objICGroup.GroupCode
                    LoadddlCompany()
                    ddlCompany.SelectedValue = objICCompany.CompanyCode
                    LoadddlProductType()
                    ddlProduct.SelectedValue = objICProductType.ProductTypeCode
                    LoadTemplateFieldsList(True)
                    ApplyJavaScript()
                    LoadAssignedAmendmentFieldsFieldsList()
                    CheckMarkAssignedAmendmentFields()
                    ddlGroup.Enabled = False
                    ddlCompany.Enabled = False
                    ddlProduct.Enabled = False
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Amendment Rights Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub clear()
        ddlGroup.ClearSelection()
        ddlCompany.ClearSelection()
        ddlProduct.ClearSelection()

        tblAmendmentAlloedFieldList.Style.Add("display", "none")
        tblAmendmentFieldList.Style.Add("display", "none")
        btnSave.Visible = False
    End Sub

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub LoadTemplateFieldsList(ByVal DoDataBind As Boolean)
        Try
            Dim objICProductType As New ICProductType
            objICProductType.LoadByPrimaryKey(ddlProduct.SelectedValue.ToString)


            ICTemplateFieldListController.GetAllTemplateFieldsForSingleInstructionFormSettingsRadGrid(objICProductType.DisbursementMode, False, Me.gvAmendmentFieldList.CurrentPageIndex + 1, Me.gvAmendmentFieldList.PageSize, Me.gvAmendmentFieldList, DoDataBind)
            If gvAmendmentFieldList.Items.Count > 0 Then
                tblAmendmentFieldList.Style.Remove("display")
                If ProductTypeCode.ToString = "0" And CompanyCode.ToString = "0" Then
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    btnSave.Visible = CBool(htRights("Update"))
                End If

            Else
                tblAmendmentFieldList.Style.Add("display", "none")
                btnSave.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadAssignedAmendmentFieldsFieldsList()
        Try

            ICAmendmentRightsController.GetAllAmendRightAssignedByCompanyCodeAndProductTypeCodeForRadGrid(ddlProduct.SelectedValue.ToString, ddlCompany.SelectedValue.ToString, Me.gvAmendedAllowedFields.CurrentPageIndex + 1, Me.gvAmendedAllowedFields.PageSize, True, Me.gvAmendedAllowedFields)
            If gvAmendedAllowedFields.Items.Count > 0 Then
                tblAmendmentAlloedFieldList.Style.Remove("display")

            Else
                tblAmendmentAlloedFieldList.Style.Add("display", "none")

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        If ddlGroup.SelectedValue.ToString <> "0" Then
            LoadddlCompany()
            LoadddlProductType()
        Else
            LoadddlCompany()
            LoadddlProductType()
            LoadTemplateFieldsList(True)
            ApplyJavaScript()
            tblAmendmentFieldList.Style.Add("display", "none")
            tblAmendmentAlloedFieldList.Style.Add("display", "none")
        End If

    End Sub
    Private Sub LoadddlProductType()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlProduct.Items.Clear()
            ddlProduct.Items.Add(lit)
            ddlProduct.AppendDataBoundItems = True
            ddlProduct.DataSource = ICAccountPaymentNatureProductTypeController.GetProductTypesTaggedWithCompanies(ddlCompany.SelectedValue.ToString)
            ddlProduct.DataTextField = "ProductTypeName"
            ddlProduct.DataValueField = "ProductTypeCode"
            ddlProduct.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            If ddlCompany.SelectedValue.ToString <> "0" Then
                LoadddlProductType()

            Else
                LoadddlProductType()
                LoadTemplateFieldsList(True)
                ApplyJavaScript()
                tblAmendmentFieldList.Style.Add("display", "none")
                tblAmendmentAlloedFieldList.Style.Add("display", "none")
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProduct.SelectedIndexChanged
        Try
            If ddlProduct.SelectedValue.ToString <> "0" Then
                LoadTemplateFieldsList(True)
                ApplyJavaScript()
                LoadAssignedAmendmentFieldsFieldsList()
                CheckMarkAssignedAmendmentFields()
            Else
                LoadTemplateFieldsList(True)
                ApplyJavaScript()
                tblAmendmentFieldList.Style.Add("display", "none")
                tblAmendmentAlloedFieldList.Style.Add("display", "none")
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub CheckMarkAssignedAmendmentFields()
        Dim objICAmendRightsColl As New ICAmendmentsRightsManagementCollection
        Dim chkCanView, chkCanAmend, chkCanBulkAmendAllow As CheckBox
        objICAmendRightsColl.es.Connection.CommandTimeout = 3600



        objICAmendRightsColl = ICAmendmentRightsController.GetAllAmendRightsCollectionByCompanyCodeAndProductTypeCode(ddlProduct.SelectedValue.ToString, ddlCompany.SelectedValue.ToString)

        If objICAmendRightsColl.Count > 0 Then
            For Each objICAmendmentRights As ICAmendmentsRightsManagement In objICAmendRightsColl
                For Each gvAmendFieldListRow As GridDataItem In gvAmendmentFieldList.Items
                    If objICAmendmentRights.FieldID = gvAmendFieldListRow.GetDataKeyValue("FieldID") Then
                        chkCanView = New CheckBox
                        chkCanAmend = New CheckBox
                        chkCanBulkAmendAllow = New CheckBox

                        chkCanView = DirectCast(gvAmendFieldListRow.Cells(1).FindControl("canViewField"), CheckBox)
                        chkCanAmend = DirectCast(gvAmendFieldListRow.Cells(2).FindControl("IsAmend"), CheckBox)
                        chkCanBulkAmendAllow = DirectCast(gvAmendFieldListRow.Cells(3).FindControl("IsBulkAmendAllow"), CheckBox)

                        chkCanView.Checked = objICAmendmentRights.CanView
                        chkCanAmend.Checked = objICAmendmentRights.IsAmendmentAllow
                        chkCanBulkAmendAllow.Checked = objICAmendmentRights.IsBulkAmendmentAllow
                    End If
                Next
            Next
        End If
    End Sub

   
    Protected Sub gvAmendmentFieldList_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAmendmentFieldList.ItemDataBound
        Dim chkViewAll As CheckBox
        Dim chkCanAmend As CheckBox
        Dim chkBulkAmend As CheckBox
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkViewAll = New CheckBox
            chkCanAmend = New CheckBox
            chkBulkAmend = New CheckBox

            chkViewAll = DirectCast(e.Item.Cells(1).FindControl("canViewAll"), CheckBox)
            chkCanAmend = DirectCast(e.Item.Cells(2).FindControl("isAmendAllowAll"), CheckBox)
            chkBulkAmend = DirectCast(e.Item.Cells(3).FindControl("isAmendAllowInBulkAll"), CheckBox)

            chkViewAll.Attributes("onclick") = "checkAllCheckboxes('" & chkViewAll.ClientID & "','" & gvAmendmentFieldList.MasterTableView.ClientID & "','1');"

            chkBulkAmend.Attributes("onclick") = "checkAllCheckboxes('" & chkBulkAmend.ClientID & "','" & gvAmendmentFieldList.MasterTableView.ClientID & "','3');"

            chkCanAmend.Attributes("onclick") = "checkAllCheckboxes('" & chkCanAmend.ClientID & "','" & gvAmendmentFieldList.MasterTableView.ClientID & "','2');"
        End If
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Dim objICProductType As New ICProductType
            objICProductType.LoadByPrimaryKey(ddlProduct.SelectedValue.ToString)
            Dim chkBulkInstrument As CheckBox
            chkBulkInstrument = New CheckBox
            chkBulkInstrument = DirectCast(item.Cells(3).FindControl("IsBulkAmendAllow"), CheckBox)

            If item.GetDataKeyValue("FieldID").ToString = "62" Then
                chkBulkInstrument.Checked = False
                chkBulkInstrument.Enabled = False
            End If
       


        End If
    End Sub
    Private Sub ApplyJavaScript()
        If gvAmendmentFieldList.Items.Count > 0 Then
            For Each gvAmendmentFieldListRow As GridDataItem In gvAmendmentFieldList.Items
                Dim chkView, chkAmend, chkAmendBulk As CheckBox
                chkView = New CheckBox
                chkAmend = New CheckBox
                chkAmendBulk = New CheckBox
                chkView = DirectCast(gvAmendmentFieldListRow.Cells(1).FindControl("canViewField"), CheckBox)
                chkAmend = DirectCast(gvAmendmentFieldListRow.Cells(2).FindControl("IsAmend"), CheckBox)
                chkAmendBulk = DirectCast(gvAmendmentFieldListRow.Cells(3).FindControl("IsBulkAmendAllow"), CheckBox)
                chkView.Attributes("onclick") = "CheckInGridView('" & chkView.ClientID & "','" & chkAmend.ClientID & "','" & chkAmendBulk.ClientID & "');"
                chkAmend.Attributes("onclick") = "CheckInGridAll('" & chkView.ClientID & "','" & chkAmend.ClientID & "','" & chkAmendBulk.ClientID & "');"
                chkAmendBulk.Attributes("onclick") = "CheckInGridAll('" & chkView.ClientID & "','" & chkAmend.ClientID & "','" & chkAmendBulk.ClientID & "');"
            Next
        End If
    End Sub
    Private Function CheckAmendFieldSelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvAmendFieldRow As GridDataItem In gvAmendmentFieldList.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvAmendFieldRow.Cells(1).FindControl("canViewField"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim objICAmendmentRightsManagement As ICAmendmentsRightsManagement
                Dim chkCanView, chkCanAmend, chkCanBulkAmend As CheckBox
                Dim RecordCount As Integer = 0
                If CheckAmendFieldSelected() = False Then
                    UIUtilities.ShowDialog(Me, "Error", "Please select at least one can view field.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each gvAmendmentFieldListRow As GridDataItem In gvAmendmentFieldList.Items
                    chkCanView = New CheckBox
                    chkCanView = DirectCast(gvAmendmentFieldListRow.Cells(1).FindControl("canViewField"), CheckBox)
                    If chkCanView.Checked = True Then
                        If RecordCount = 0 Then
                            DeleteAmendRightsFieldByCompanycodeAndProductType()
                            RecordCount = 1
                        End If
                        objICAmendmentRightsManagement = New ICAmendmentsRightsManagement
                        objICAmendmentRightsManagement.es.Connection.CommandTimeout = 3600
                        chkCanAmend = DirectCast(gvAmendmentFieldListRow.Cells(2).FindControl("IsAmend"), CheckBox)
                        chkCanBulkAmend = DirectCast(gvAmendmentFieldListRow.Cells(3).FindControl("IsBulkAmendAllow"), CheckBox)
                        objICAmendmentRightsManagement.FieldID = gvAmendmentFieldListRow.GetDataKeyValue("FieldID")
                        objICAmendmentRightsManagement.IsAmendmentAllow = chkCanAmend.Checked
                        If chkCanBulkAmend.Checked = True And chkCanBulkAmend.Enabled = True Then
                            objICAmendmentRightsManagement.IsBulkAmendmentAllow = chkCanBulkAmend.Checked
                        Else
                            objICAmendmentRightsManagement.IsBulkAmendmentAllow = False
                        End If
                        objICAmendmentRightsManagement.ProductTypeCode = ddlProduct.SelectedValue.ToString
                        objICAmendmentRightsManagement.CompanyCode = ddlCompany.SelectedValue.ToString
                        objICAmendmentRightsManagement.CreatedBy = Me.UserId
                        objICAmendmentRightsManagement.CreatedDate = Date.Now
                        objICAmendmentRightsManagement.CanView = chkCanView.Checked
                        ICAmendmentRightsController.AddAmendRightsManagement(objICAmendmentRightsManagement, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    End If

                Next
                RecordCount = 0
                If ProductTypeCode.ToString = "0" And CompanyCode.ToString = "0" Then
                    UIUtilities.ShowDialog(Me, "Amendment Rights Management", "Amendment rights added successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL("SaveAmendmentRights", "&mid=" & Me.ModuleId & "&CompanyCode=0" & "&GroupCode=0" & "&ProductTypeCode=0"))
                Else
                    UIUtilities.ShowDialog(Me, "Amendment Rights Management", "Amendment rights updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                End If
                clear()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub
    Private Sub DeleteAmendRightsFieldByCompanycodeAndProductType()
        Try
            Dim qryObjICAmendRightsColl As New ICAmendmentsRightsManagementCollection
            qryObjICAmendRightsColl.es.Connection.CommandTimeout = 3600
            qryObjICAmendRightsColl = ICAmendmentRightsController.GetAllAmendRightsCollectionByCompanyCodeAndProductTypeCode(ddlProduct.SelectedValue.ToString, ddlCompany.SelectedValue.ToString)
            If qryObjICAmendRightsColl.Count > 0 Then
                For Each objICAmendRights As ICAmendmentsRightsManagement In qryObjICAmendRightsColl
                    ICAmendmentRightsController.DeleteamendRightsManagement(objICAmendRights, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                Next
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL())
    End Sub

    Protected Sub gvAmendmentFieldList_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAmendmentFieldList.NeedDataSource
        LoadTemplateFieldsList(False)

    End Sub
End Class
