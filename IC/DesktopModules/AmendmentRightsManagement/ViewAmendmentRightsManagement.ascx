﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewAmendmentRightsManagement.ascx.vb"
    Inherits="DesktopModules_AmendmentRightsManagement_ViewAmendmentRightsManagement" %>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure to remove this record ?") == true) {

            return true;
        }
        else {
            return false;
        }
    }
    function delcon(item) {


        if (window.confirm("Are you sure to remove this " + item + "?") == true) {

            return true;

        }
        else {

            return false;

        }
    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to remove Record(s)?") == true) {
            return true;
        }
        else {
            return false;
        }
    }  
</script>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2" style="width: 50%">
            <asp:Label ID="lblPageHeader" runat="server" Text="Amendment Rights List" CssClass="headingblue"></asp:Label>
            <br />
           <%-- Note:&nbsp; Fields marked as
            <asp:Label ID="lblReqMark" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.--%>
        </td>
        <td align="left" valign="top" colspan="2" style="width: 50%">
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2" style="width: 50%">
            &nbsp;
        </td>
        <td align="left" valign="top" colspan="2" style="width: 50%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnAddAmendmentRights" runat="server" CausesValidation="False" CssClass="btn"
                Text="Add Amendment Rights" />
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
        &nbsp;</tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True" CssClass="dropdown">
            </asp:DropDownList>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" CssClass="dropdown">
                </asp:DropDownList>
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;<td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblProductType" runat="server" Text="Select Product Type" CssClass="lbl"></asp:Label>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlProductType" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;<td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblRNF" runat="server" Font-Bold="False" Text="No Record Found" Visible="False"
                            CssClass="headingblue"></asp:Label>
                    <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <telerik:RadGrid ID="gvAmendRightsManagement" runat="server" AllowPaging="True" AllowSorting="True"
                AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                <ClientSettings>
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                </ClientSettings>
                <AlternatingItemStyle CssClass="rgAltRow" />
                <MasterTableView TableLayout="Fixed">
                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    </ExpandCollapseColumn>
                    <Columns>
                        <%-- <telerik:GridTemplateColumn HeaderText="Approve" DataField="IsApproved">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelectAllUsers" runat="server" CssClass="chkBox" Text=" Select"
                                    TextAlign="Right" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>--%>
                        <%--    <telerik:GridBoundColumn DataField="DisplayName" HeaderText="User Name" SortExpression="DisplayName">
                        </telerik:GridBoundColumn>--%>
                        <telerik:GridBoundColumn DataField="GroupName" HeaderText="Group Name" SortExpression="GroupName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CompanyName" HeaderText="Company Name" SortExpression="CompanyName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ProductTypeName" HeaderText="Product Type" SortExpression="ProductTypeName">
                        </telerik:GridBoundColumn>
                        <%--  <telerik:GridBoundColumn DataField="TemplateCode" HeaderText="Template Code" SortExpression="TemplateCode">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FTPLocation" HeaderText="FTP Location" SortExpression="FTPLocation">
                        </telerik:GridBoundColumn>--%>
                        <telerik:GridTemplateColumn HeaderText="Edit">
                            <ItemTemplate>
                                <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveAmendmentRights","&mid=" & Me.ModuleId & "&CompanyCode="& Eval("CompanyCode") & "&ProductTypeCode="& Eval("ProductTypeCode"))%>'
                                    ToolTip="Edit">Edit</asp:HyperLink>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Delete">
                            <ItemTemplate>
                                <asp:ImageButton ID="hlDelete" runat="server" ImageUrl="~/images/delete.gif" CommandName="del" CommandArgument='<%# Eval("CompanyCode").tostring + ";" +Eval("ProductTypeCode") %>'
                                ToolTip="Delete" OnClientClick="javascript: return con();" ></asp:ImageButton>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                    <PagerStyle AlwaysVisible="True" />
                </MasterTableView>
                <ItemStyle CssClass="rgRow" />
                <PagerStyle AlwaysVisible="True" />
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;<td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
        &nbsp;</tr>
</table>
