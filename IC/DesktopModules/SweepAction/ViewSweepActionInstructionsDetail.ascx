﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewSweepActionInstructionsDetail.ascx.vb"
    Inherits="DesktopModules_SweepActions_ViewSweepActions" %>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure to remove this record ?") == true) {

            return true;
        }
        else {
            return false;
        }
    }
    function delcon(item) {


        if (window.confirm("Are you sure to remove this " + item + "?") == true) {

            return true;

        }
        else {

            return false;

        }
    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to remove Record(s)?") == true) {
            return true;
        }
        else {
            return false;
        }
    }  
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        //      alert(grid)
        //        if ((grid.rows.length -1) != 10 ) {
        //        if ((grid.rows.length) >= 12) {
        //            //            alert('More than 10 rows')
        //            for (i = 0; i < grid.rows.length - 1; i++) {
        //                cell = grid.rows[i].cells[CellNo];
        //                for (j = 0; j < cell.childNodes.length - 1; j++) {
        //                    if (cell.childNodes[j].type == "checkbox") {
        //                        cell.childNodes[j].checked = document.getElementById(idOfControllingCheckbox).checked;
        //                    }
        //                }
        //            }
        //        }
        //        else {
        //                     alert(grid.rows.length)
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    //    }
</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table width="100%">
                <tr>
                    <td align="center" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="middle">
                        <asp:Label ID="lblSweepActionsList" runat="server" Text="Sweep Actions List" CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle">
                        <asp:Label ID="lblRNF" runat="server" Font-Bold="False" Text="No Record Found" Visible="False"
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle">
                        <telerik:RadGrid ID="gvSweepActionsDetails" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                           <AlternatingItemStyle CssClass="rgAltRow" />
                           <ClientSettings>
                           <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                           </ClientSettings>
                            <MasterTableView DataKeyNames="SweepActionInstructionID" TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Approve" DataField="IsApproved">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                         <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="SweepActionInstructionID" HeaderText="Sweep Instruction ID" SortExpression="SweepActionInstructionID">
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="11%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="11%" />
                                    </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="SweepActionID" HeaderText="Action ID" SortExpression="SweepActionID">
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="9%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="9%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FromAccountNo" HeaderText="From Account" SortExpression="FromAccountNo">
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="16%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="16%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ToAccountNo" HeaderText="To Account" SortExpression="ToAccountNo">
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="16%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="16%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InstructionAmount" HeaderText="Amount" SortExpression="InstructionAmount" HtmlEncode="false" DataFormatString="{0:N2}">
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ValueDate" HeaderText="Value Date" SortExpression="ValueDate" HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>
                                     
                                     <telerik:GridBoundColumn DataField="SweepActionFrequency" HeaderText="Frequency" SortExpression="SweepActionFrequency">
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                       <telerik:GridCheckBoxColumn HeaderText="Processed" DataField="IsPaid">
                                   <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="6%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="6%" />
                                    </telerik:GridCheckBoxColumn>
                                       <telerik:GridCheckBoxColumn HeaderText="Active" DataField="IsActive">
                                   <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                    </telerik:GridCheckBoxColumn>
                                     <telerik:GridCheckBoxColumn HeaderText="Approve" DataField="IsApproved">
                                   <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                    </telerik:GridCheckBoxColumn>
                    
                                    <telerik:GridTemplateColumn HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("SweepActionInstructionID") %>'
                                                CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                      <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                 <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="center">
                        <asp:Button ID="btnActivateSweepActionInstruction" runat="server" Text="Activate Instruction" CssClass="btn"
                            CausesValidation="False" />
                        <asp:Button ID="btnApproveSweepActionInstruction" runat="server" Text="Approve Instruction" CssClass="btn"
                            CausesValidation="False" />
                        <asp:Button ID="btnDeleteSweepActionInstruction" runat="server" Text="Delete Instruction" CssClass="btn" OnClientClick="javascript: return conBukDelete();"
                            CausesValidation="False" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn"
                            CausesValidation="False" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
