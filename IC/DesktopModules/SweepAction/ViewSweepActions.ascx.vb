﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports EntitySpaces.Core
Partial Class DesktopModules_SweepActions_ViewSweepActions
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private htRights As Hashtable
    Private ArrICAssignUserRolsID As New ArrayList
#Region "Page Load"
    Protected Sub DesktopModules_AccountsManagement_ViewAccounts_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnAddSweepAction.Visible = CBool(htRights("Add"))
                LoadddlGroup()
                LoadddlCompany()
                FillDesignedDtFromAccountNoByAssignedRole()
                FillDesignedDtToAccountNoByAssignedRole()
                LoadddlFromAccountNo()
                LoadddlToAccountNo()
                LoadSweepActions(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Sweep Action") = True Then
                    If Not ArrICAssignUserRolsID.Contains(RoleID.ToString) Then
                        ArrICAssignUserRolsID.Add(RoleID.ToString)
                    End If
                End If
            Next
        End If
    End Sub
    Private Sub DesignDts()
        Dim dtAccountNumber As New DataTable
        dtAccountNumber.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("BranchCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("Currency", GetType(System.String)))
        ViewState("AccountNumber") = dtAccountNumber
    End Sub
#End Region
#Region "Grid View Events"

    Protected Sub gvAccountsDetails_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvSweepActionsDetails.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                ' Dim arrPageSizes() As String = {"1", "2", "5", "10", "50"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvSweepActionsDetails.MasterTableView.ClientID)
                Next
                'If gvAccountsDetails.Items.Count > 0 Then
                '    myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
                'End If
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    'Protected Sub gvAccountsDetails_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAccountsDetails.RowCommand
    '    Try
    '        If e.CommandName = "del" Then
    '            Dim ICAccounts As New ICAccounts
    '            Dim cmdArg1 As String = Nothing
    '            Dim cmdArg2 As String = Nothing
    '            Dim cmdArg3 As String = Nothing
    '            Dim s As String()
    '            s = e.CommandArgument.ToString().Split(";")
    '            cmdArg1 = s.GetValue(0)
    '            cmdArg2 = s.GetValue(1)
    '            cmdArg3 = s.GetValue(2)
    '            ICAccounts.es.Connection.CommandTimeout = 3600

    '            ICAccounts.LoadByPrimaryKey(cmdArg1, cmdArg2, cmdArg3)
    '            If ICAccounts.LoadByPrimaryKey(cmdArg1, cmdArg2, cmdArg3) Then
    '                If ICAccounts.IsApproved Then
    '                    UIUtilities.ShowDialog(Me, "Warning", "Account is approved and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
    '                Else
    '                    ICAccountsController.DeleteAccount(cmdArg1, cmdArg2, cmdArg3)

    '                    ICUtilities.AddAuditTrail("Account : " & ICAccounts.AccountNumber.ToString() & " Deleted", "Account", ICAccounts.BranchCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
    '                    UIUtilities.ShowDialog(Me, "Delete Account", "Account deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
    '                    LoadAccounts()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
    '            UIUtilities.ShowDialog(Me, "Deleted Account", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
    '            Exit Sub
    '        End If
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
#End Region
#Region "Button Events"
    Protected Sub btnSaveAccounts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddSweepAction.Click
        Page.Validate()
        If Page.IsValid Then
            Response.Redirect(NavigateURL("AddSweepAction", "&mid=" & Me.ModuleId & "&SweepActionID=0"), False)
        End If
    End Sub
#End Region
#Region "Other Functions/Routines"
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Sweep Action")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    
    Public Shared Sub GetAllAccountsForRadGridByGroupAndCompanyCode(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid, ByVal GroupCode As String, ByVal CompanyCode As String, ByVal dtAccountNo As DataTable, ByVal dtFromAccountNo As DataTable)
        Dim StrQuery As String = Nothing
        Dim WhereClause As String = " Where"
        Dim dt As New DataTable
        Dim params As New EntitySpaces.Interfaces.esParameters

        StrQuery = "Select SweepActionID,SweepActionTitle,FromAccountNo,ToAccountNo,SweepActionFrequency,TotalInstructions,"
        StrQuery += "SweepActionFromDate as FromDate,SweepActionToDate as ToDate,SweepActionTime as ScheduleTime,"
        StrQuery += " case when (select COUNT(SweepActionInstructionID) from IC_SweepActionInstructions where IC_SweepActionInstructions.SweepActionID=SA.SweepActionID and IsProcessed=1)= TotalInstructions then 'Complete' else 'In Process' end as Status"
        StrQuery += ",AmountPerInstruction As InstructionAmount,convert(date,SweepActionCreationDate) as SweepActionCreationDate  from IC_SweepAction as SA "
        StrQuery += " INNER JOIN IC_Accounts as FA on SA.FromAccountNo=FA.AccountNumber and SA.FromAccountBrCode=FA.BranchCode and SA.FromAccountCurrency=FA.Currency"
        StrQuery += " INNER JOIN IC_Accounts as TA on SA.ToAccountNo=TA.AccountNumber and SA.ToAccountBrCode=TA.BranchCode and SA.ToAccountCurrency=TA.Currency"
        WhereClause += " AND TA.IsApproved=1 AND TA.isActive=1 AND FA.IsApproved=1 AND FA.isActive=1"
        If GroupCode <> "" Then
            WhereClause += " AND SA.GroupCode=@GroupCode"
            params.Add("GroupCode", GroupCode)
        End If
        If CompanyCode <> "" Then
            WhereClause += " AND SA.CompanyCode=@CompanyCode"
            params.Add("CompanyCode", CompanyCode)
        End If

        If dtFromAccountNo.Rows.Count > 0 Then
            WhereClause += " AND SA.FromAccountNo + '-' + SA.FromAccountBrCode + '-' + SA.FromAccountCurrency in ("
            For Each drFromAccountNo As DataRow In dtFromAccountNo.Rows
                WhereClause += "'" & drFromAccountNo("AccountNumber").ToString.Split("-")(0) & "-" & drFromAccountNo("AccountNumber").ToString.Split("-")(1) & "-" & drFromAccountNo("AccountNumber").ToString.Split("-")(2) & "',"
            Next
            WhereClause = WhereClause.Remove(WhereClause.Length - 1, 1)
            WhereClause += ")"
        End If
        If dtAccountNo.Rows.Count > 0 Then
            WhereClause += " AND SA.ToAccountNo + '-' + SA.ToAccountBrCode + '-' + SA.ToAccountCurrency in ("
            For Each drFromAccountNo As DataRow In dtAccountNo.Rows
                WhereClause += "'" & drFromAccountNo("AccountNumber").ToString.Split("-")(0) & "-" & drFromAccountNo("AccountNumber").ToString.Split("-")(1) & "-" & drFromAccountNo("AccountNumber").ToString.Split("-")(2) & "',"
            Next
            WhereClause = WhereClause.Remove(WhereClause.Length - 1, 1)
            WhereClause += ")"
        End If
        If WhereClause.Contains(" Where AND ") = True Then
            WhereClause = WhereClause.Replace(" Where AND ", " Where  ")
        End If

        StrQuery = StrQuery & WhereClause
        Dim utill As New esUtility()
        dt = New DataTable
        dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)
        If Not PageNumber = 0 Then
     
            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        Else
           
            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        End If
    End Sub

    Private Sub LoadSweepActions(ByVal DoDataBind As Boolean)
        Try

            Dim GroupCode, CompanyCode As String
            Dim dtFromAccountNo As New DataTable
            Dim dtToAccountNo As New DataTable
            GroupCode = ""
            CompanyCode = ""
            dtFromAccountNo = ViewState("FromAccountNo")
            dtToAccountNo = ViewState("ToAccountNo")
            If ddlGroup.SelectedValue.ToString <> "0" Then
                GroupCode = ddlGroup.SelectedValue.ToString
            End If
            If ddlcompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlcompany.SelectedValue.ToString
            End If

            GetAllAccountsForRadGridByGroupAndCompanyCode(Me.gvSweepActionsDetails.CurrentPageIndex + 1, Me.gvSweepActionsDetails.PageSize, DoDataBind, Me.gvSweepActionsDetails, GroupCode, CompanyCode, dtToAccountNo, dtFromAccountNo)
            If gvSweepActionsDetails.Items.Count > 0 Then
                gvSweepActionsDetails.Visible = True
                lblRNF.Visible = False
                btnDeleteAccounts.Visible = CBool(htRights("Delete"))

                gvSweepActionsDetails.Columns(14).Visible = CBool(htRights("Delete"))
            Else
                gvSweepActionsDetails.Visible = False
                btnDeleteAccounts.Visible = False
                lblRNF.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            objICUser.es.Connection.CommandTimeout = 3600
            ddlGroup.Enabled = True
            Dim lit As New ListItem
            lit.Text = Nothing
            lit.Value = Nothing
            If objICUser.LoadByPrimaryKey(Me.UserId) Then
                If objICUser.UserType = "Client User" Then

                    lit.Value = "0"
                    lit.Text = "-- Please Select --"
                    ddlGroup.Items.Clear()
                    ddlGroup.Items.Add(lit)
                    ddlGroup.AppendDataBoundItems = True
                    ddlGroup.DataSource = ICGroupController.GetAllActiveAndApproveGroups
                    ddlGroup.DataTextField = "GroupName"
                    ddlGroup.DataValueField = "GroupCode"
                    ddlGroup.DataBind()
                ElseIf objICUser.UserType = "Bank User" Then

                    ddlGroup.Items.Clear()
                    dt = ICUserRolesPaymentNatureController.GetAllTaggedGroupsByUserID(objICUser.UserID.ToString, ArrICAssignUserRolsID)
                    If dt.Rows.Count = 1 Then
                        For Each dr As DataRow In dt.Rows
                            lit.Value = dr("GroupCode")
                            lit.Text = dr("GroupName")
                            ddlGroup.Items.Add(lit)
                            lit.Selected = True
                            ddlGroup.Enabled = False
                        Next
                    ElseIf dt.Rows.Count > 1 Then
                        lit.Value = "0"
                        lit.Text = "-- Please Select --"
                        ddlGroup.Items.Clear()
                        ddlGroup.Items.Add(lit)
                        ddlGroup.AppendDataBoundItems = True
                        ddlGroup.DataSource = dt
                        ddlGroup.DataTextField = "GroupName"
                        ddlGroup.DataValueField = "GroupCode"
                        ddlGroup.DataBind()
                    Else

                        lit.Value = "0"
                        lit.Text = "-- Please Select --"
                        ddlGroup.Items.Clear()
                        ddlGroup.Items.Add(lit)
                        ddlGroup.AppendDataBoundItems = True

                    End If
                Else

                    lit.Value = "0"
                    lit.Text = "-- Please Select --"
                    ddlGroup.Items.Clear()
                    ddlGroup.Items.Add(lit)
                    ddlGroup.AppendDataBoundItems = True
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, ddlGroup.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlcompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlcompany.Items.Clear()
                    ddlcompany.Items.Add(lit)
                    lit.Selected = True
                    ddlcompany.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- Please Select --"
                ddlcompany.Items.Clear()
                ddlcompany.Items.Add(lit)
                ddlcompany.AppendDataBoundItems = True
                ddlcompany.DataSource = dt
                ddlcompany.DataTextField = "CompanyName"
                ddlcompany.DataValueField = "CompanyCode"
                ddlcompany.DataBind()
                ddlcompany.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- Please Select --"
                ddlcompany.Items.Clear()
                ddlcompany.Items.Add(lit)
                ddlcompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlFromAccountNo()
        Try
            Dim GroupCode, CompanyCode As String
            GroupCode = "-1"
            CompanyCode = "-1"
            If ddlGroup.SelectedValue.ToString <> "0" Then
                GroupCode = ddlGroup.SelectedValue.ToString
            End If
            If ddlcompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlcompany.SelectedValue.ToString
            End If
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlFromAccount.Items.Clear()
            ddlFromAccount.Items.Add(lit)
            lit.Selected = True
            ddlFromAccount.AppendDataBoundItems = True
            ddlFromAccount.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForSweepAction(Me.UserInfo.UserID.ToString, ArrICAssignUserRolsID, GroupCode, CompanyCode)
            ddlFromAccount.DataTextField = "Value"
            ddlFromAccount.DataValueField = "AccountNumber"
            ddlFromAccount.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlToAccountNo()
        Try
            Dim GroupCode, CompanyCode As String
            GroupCode = "-1"
            CompanyCode = "-1"
            If ddlGroup.SelectedValue.ToString <> "0" Then
                GroupCode = ddlGroup.SelectedValue.ToString
            End If
            If ddlcompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlcompany.SelectedValue.ToString
            End If
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlToAccountNo.Items.Clear()
            ddlToAccountNo.Items.Add(lit)
            lit.Selected = True
            ddlToAccountNo.AppendDataBoundItems = True
            ddlToAccountNo.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForSweepAction(Me.UserInfo.UserID.ToString, ArrICAssignUserRolsID, GroupCode, CompanyCode)
            ddlToAccountNo.DataTextField = "Value"
            ddlToAccountNo.DataValueField = "AccountNumber"
            ddlToAccountNo.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub FillDesignedDtFromAccountNoByAssignedRole()
        Dim dtAssignedAPNatureOfficeID As New DataTable
        dtAssignedAPNatureOfficeID.Rows.Clear()
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForSweepAction(Me.UserId.ToString, ArrICAssignUserRolsID,ddlGroup.SelectedValue.ToString,ddlcompany.SelectedValue.ToString)

        If dtAssignedAPNatureOfficeID.Rows.Count = 0 Then
            UIUtilities.ShowDialog(Me, "Error", "You have no access to any transactional data.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
            Exit Sub
        End If
        ViewState("FromAccountNo") = Nothing
        ViewState("FromAccountNo") = dtAssignedAPNatureOfficeID
    End Sub
    Private Sub FillDesignedDtToAccountNoByAssignedRole()
        Dim dtAssignedAPNatureOfficeID As New DataTable
        dtAssignedAPNatureOfficeID.Rows.Clear()
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForSweepAction(Me.UserId.ToString, ArrICAssignUserRolsID, ddlGroup.SelectedValue.ToString, ddlcompany.SelectedValue.ToString)
        ViewState("ToAccountNo") = Nothing
        ViewState("ToAccountNo") = dtAssignedAPNatureOfficeID
    End Sub
#End Region
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
            LoadddlFromAccountNo()
            LoadddlToAccountNo()
            LoadSweepActions(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAccountsDetails_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvSweepActionsDetails.NeedDataSource
        Try
            LoadSweepActions(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAccountsDetails_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvSweepActionsDetails.PageIndexChanged
        Try
            gvSweepActionsDetails.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlcompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcompany.SelectedIndexChanged
        Try

            LoadddlFromAccountNo()
            LoadddlToAccountNo()

            LoadSweepActions(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAccountsDetails_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvSweepActionsDetails.ItemCommand
        Try
            Dim objICSweepAction As New ICSweepAction

            objICSweepAction.es.Connection.CommandTimeout = 3600
            If e.CommandName.ToString = "del" Then
                If Page.IsValid Then

                    If objICSweepAction.LoadByPrimaryKey(e.CommandArgument) Then
                        If ICSweepActionsController.IsInstructionPaidAgainstSweepAction(e.CommandArgument) = False Then
                            ICSweepActionInstructionsController.DeleteSweepActionInstructionBySweepActionID(e.CommandArgument.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                            ICSweepActionsController.DeleteSweepActionsbySweepActionID(e.CommandArgument.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "DELETE")
                            UIUtilities.ShowDialog(Me, "Delete Sweep Action", "Sweep Action deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                            LoadSweepActions(True)
                        Else
                            UIUtilities.ShowDialog(Me, "Delete Sweep Action", "Instructions paid against sweep action and can not be deleted", ICBO.IC.Dialogmessagetype.Failure)
                            LoadSweepActions(True)
                        End If

                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Error ", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                LoadSweepActions(True)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckGVCompanySelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvCompanyRow As GridDataItem In gvSweepActionsDetails.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvCompanyRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnDeleteAccounts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteAccounts.Click
        Page.Validate()
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim PassToApproveCount As Integer = 0
                Dim FailToApproveCount As Integer = 0
                Dim objICSweepActions As ICSweepAction
                Dim SweepActionID As String

                If CheckGVCompanySelected() = False Then
                    UIUtilities.ShowDialog(Me, "Delete Sweep Action", "Please select atleast one sweep action.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVAccounts In gvSweepActionsDetails.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVAccounts.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        SweepActionID = ""

                        SweepActionID = rowGVAccounts.GetDataKeyValue("SweepActionID").ToString

                        objICSweepActions = New ICSweepAction
                        objICSweepActions.es.Connection.CommandTimeout = 3600
                        objICSweepActions.LoadByPrimaryKey(SweepActionID)

                        Try
                            If ICSweepActionsController.IsInstructionPaidAgainstSweepAction(SweepActionID) = False Then
                                If ICSweepActionInstructionsController.DeleteSweepActionInstructionBySweepActionID(SweepActionID, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString) = True Then
                                    If ICSweepActionsController.DeleteSweepActionsbySweepActionID(SweepActionID, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "DELETE") = False Then
                                        PassToApproveCount = PassToApproveCount + 1
                                    Else
                                        FailToApproveCount = FailToApproveCount + 1
                                    End If
                                Else
                                    FailToApproveCount = FailToApproveCount + 1
                                End If

                            Else
                                FailToApproveCount = FailToApproveCount + 1
                            End If

                        Catch ex As Exception
                            FailToApproveCount = FailToApproveCount + 1
                            Continue For
                        End Try


                    End If
                Next

                If Not PassToApproveCount = 0 And Not FailToApproveCount = 0 Then
                    LoadSweepActions(True)
                    UIUtilities.ShowDialog(Me, "Delete Sweep Action", "[ " & PassToApproveCount.ToString & " ] Sweep Actions(s) deleted successfully.<br /> [ " & FailToApproveCount.ToString & " ] Sweep Actions(s) can not be deleted due to following reasons:<br /> 1. Instructions are paid against sweep actions", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToApproveCount = 0 And FailToApproveCount = 0 Then
                    LoadSweepActions(True)
                    UIUtilities.ShowDialog(Me, "Delete Sweep Action", PassToApproveCount.ToString & " Sweep Actions(s) deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToApproveCount = 0 And PassToApproveCount = 0 Then
                    LoadSweepActions(True)
                    UIUtilities.ShowDialog(Me, "Delete Sweep Action", "[ " & FailToApproveCount.ToString & " ] Sweep Actions(s) can not be deleted due to following reasons: <br /> 1. Instructions are paid against sweep actions.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub gvAccountsDetails_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvSweepActionsDetails.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvSweepActionsDetails.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub ddlFromAccount_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFromAccount.SelectedIndexChanged
        Try


            If ddlFromAccount.SelectedValue.ToString <> "0" Then
                FillDesignedDtToAccountNoByAssignedRole()
                SetViewStateDtFromAccountNoOnIndexChange()
                LoadddlToAccountNo()
                LoadSweepActions(True)
            Else
                FillDesignedDtFromAccountNoByAssignedRole()
                FillDesignedDtToAccountNoByAssignedRole()
                LoadddlFromAccountNo()
                LoadddlToAccountNo()
                LoadSweepActions(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlToAccountNo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlToAccountNo.SelectedIndexChanged
        Try
            If ddlToAccountNo.SelectedValue.ToString <> "0" Then
                SetViewStateDtToAccountNoOnIndexChange()
                LoadSweepActions(True)
            Else
                FillDesignedDtToAccountNoByAssignedRole()
                LoadddlToAccountNo()
                LoadSweepActions(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetViewStateDtFromAccountNoOnIndexChange()
        Dim dtAccountNumber As New DataTable
        Dim AccountNumber, BranchCode, Currency As String
        Dim drAccountNo As DataRow
        AccountNumber = Nothing
        BranchCode = Nothing
        Currency = Nothing
        dtAccountNumber = ViewState("FromAccountNo")
        ViewState("FromAccountNo") = Nothing
        AccountNumber = ddlFromAccount.SelectedValue.ToString.Split("-")(0)
        BranchCode = ddlFromAccount.SelectedValue.ToString.Split("-")(1)
        Currency = ddlFromAccount.SelectedValue.ToString.Split("-")(2)
        dtAccountNumber.Rows.Clear()
        drAccountNo = dtAccountNumber.NewRow()
        drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
        dtAccountNumber.Rows.Add(drAccountNo)
        ViewState("FromAccountNo") = dtAccountNumber
    End Sub
    Private Sub SetViewStateDtToAccountNoOnIndexChange()
        Dim dtAccountNumber As New DataTable
        Dim AccountNumber, BranchCode, Currency As String
        Dim drAccountNo As DataRow
        AccountNumber = Nothing
        BranchCode = Nothing
        Currency = Nothing
        dtAccountNumber = ViewState("ToAccountNo")
        ViewState("ToAccountNo") = Nothing
        AccountNumber = ddlToAccountNo.SelectedValue.ToString.Split("-")(0)
        BranchCode = ddlToAccountNo.SelectedValue.ToString.Split("-")(1)
        Currency = ddlToAccountNo.SelectedValue.ToString.Split("-")(2)
        dtAccountNumber.Rows.Clear()
        drAccountNo = dtAccountNumber.NewRow()
        drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
        dtAccountNumber.Rows.Add(drAccountNo)
        ViewState("ToAccountNo") = dtAccountNumber
    End Sub
End Class
