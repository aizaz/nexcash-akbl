﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_SweepActions_ViewSweepActions
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private htRights As Hashtable
    Private ArrICAssignUserRolsID As New ArrayList
#Region "Page Load"
    Protected Sub DesktopModules_AccountsManagement_ViewAccounts_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()
            If Page.IsPostBack = False Then
                btnSave.Attributes.Item("onclick") = "if (Page_ClientValidate()) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnSave, Nothing).ToString() & " } "
                sharedCalenderSceduleStartDate.RangeMinDate = Date.Now.AddDays(1)
                sharedCalenderEndScheduleDate.RangeMinDate = Date.Now.AddDays(1)
                sharedCalenderEndScheduleDate.SelectedDate = Date.Now.AddDays(1)
                sharedCalenderSceduleStartDate.SelectedDate = Date.Now.AddDays(1)
                txtStartDate.SelectedDate = Now.Date.AddDays(1)
                txtEndDate.SelectedDate = Now.Date.AddDays(1)
                Clear()

                tblDayType.Style.Add("display", "none")
                tblNoOfDays.Style.Add("display", "none")
                tblSpecificDay.Style.Add("display", "none")
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnSave.Visible = CBool(htRights("Add"))
                LoadddlGroup()
                LoadddlCompany()

                LoadddlFromAccountNo()
                LoadddlToAccountNo()

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Sweep Action") = True Then
                    If Not ArrICAssignUserRolsID.Contains(RoleID.ToString) Then
                        ArrICAssignUserRolsID.Add(RoleID.ToString)
                    End If
                End If
            Next
        End If
    End Sub

#End Region


#Region "Other Functions/Routines"
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Sweep Action")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    
    Private Sub LoadddlGroup()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            objICUser.es.Connection.CommandTimeout = 3600
            ddlGroup.Enabled = True
            Dim lit As New ListItem
            lit.Text = Nothing
            lit.Value = Nothing
            If objICUser.LoadByPrimaryKey(Me.UserId) Then
                If objICUser.UserType = "Client User" Then

                    lit.Value = "0"
                    lit.Text = "-- Please Select --"
                    ddlGroup.Items.Clear()
                    ddlGroup.Items.Add(lit)
                    ddlGroup.AppendDataBoundItems = True
                    ddlGroup.DataSource = ICGroupController.GetAllActiveAndApproveGroups
                    ddlGroup.DataTextField = "GroupName"
                    ddlGroup.DataValueField = "GroupCode"
                    ddlGroup.DataBind()
                ElseIf objICUser.UserType = "Bank User" Then

                    ddlGroup.Items.Clear()
                    dt = ICUserRolesPaymentNatureController.GetAllTaggedGroupsByUserID(objICUser.UserID.ToString, ArrICAssignUserRolsID)
                    If dt.Rows.Count = 1 Then
                        For Each dr As DataRow In dt.Rows
                            lit.Value = dr("GroupCode")
                            lit.Text = dr("GroupName")
                            ddlGroup.Items.Add(lit)
                            lit.Selected = True
                            ddlGroup.Enabled = False
                        Next
                    ElseIf dt.Rows.Count > 1 Then
                        lit.Value = "0"
                        lit.Text = "-- Please Select --"
                        ddlGroup.Items.Clear()
                        ddlGroup.Items.Add(lit)
                        ddlGroup.AppendDataBoundItems = True
                        ddlGroup.DataSource = dt
                        ddlGroup.DataTextField = "GroupName"
                        ddlGroup.DataValueField = "GroupCode"
                        ddlGroup.DataBind()
                    Else

                        lit.Value = "0"
                        lit.Text = "-- Please Select --"
                        ddlGroup.Items.Clear()
                        ddlGroup.Items.Add(lit)
                        ddlGroup.AppendDataBoundItems = True

                    End If
                Else

                    lit.Value = "0"
                    lit.Text = "-- Please Select --"
                    ddlGroup.Items.Clear()
                    ddlGroup.Items.Add(lit)
                    ddlGroup.AppendDataBoundItems = True
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, ddlGroup.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlcompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlcompany.Items.Clear()
                    ddlcompany.Items.Add(lit)
                    lit.Selected = True
                    ddlcompany.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- Please Select --"
                ddlcompany.Items.Clear()
                ddlcompany.Items.Add(lit)
                ddlcompany.AppendDataBoundItems = True
                ddlcompany.DataSource = dt
                ddlcompany.DataTextField = "CompanyName"
                ddlcompany.DataValueField = "CompanyCode"
                ddlcompany.DataBind()
                ddlcompany.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- Please Select --"
                ddlcompany.Items.Clear()
                ddlcompany.Items.Add(lit)
                ddlcompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlFromAccountNo()
        Dim GroupCode, CompanyCode As String
        GroupCode = "-1"
        CompanyCode = "-1"
        If ddlGroup.SelectedValue.ToString <> "0" Then
            GroupCode = ddlGroup.SelectedValue.ToString
        End If
        If ddlcompany.SelectedValue.ToString <> "0" Then
            CompanyCode = ddlcompany.SelectedValue.ToString
        End If
        Dim lit As New ListItem
        lit.Value = "0"
        lit.Text = "-- All --"
        ddlFromAccount.Items.Clear()
        ddlFromAccount.Items.Add(lit)
        lit.Selected = True
        ddlFromAccount.AppendDataBoundItems = True
        ddlFromAccount.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForSweepAction(Me.UserInfo.UserID.ToString, ArrICAssignUserRolsID, GroupCode, CompanyCode)
        ddlFromAccount.DataTextField = "Value"
        ddlFromAccount.DataValueField = "AccountNumber"
        ddlFromAccount.DataBind()

    End Sub
    Private Sub LoadddlToAccountNo()
        Try
            Dim GroupCode, CompanyCode As String
            GroupCode = "-1"
            CompanyCode = "-1"
            If ddlGroup.SelectedValue.ToString <> "0" Then
                GroupCode = ddlGroup.SelectedValue.ToString
            End If
            If ddlcompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlcompany.SelectedValue.ToString
            End If
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlToAccountNo.Items.Clear()
            ddlToAccountNo.Items.Add(lit)
            lit.Selected = True
            ddlToAccountNo.AppendDataBoundItems = True
            ddlToAccountNo.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForSweepAction(Me.UserInfo.UserID.ToString, ArrICAssignUserRolsID, GroupCode, CompanyCode)
            ddlToAccountNo.DataTextField = "Value"
            ddlToAccountNo.DataValueField = "AccountNumber"
            ddlToAccountNo.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

#End Region
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
            LoadddlFromAccountNo()
            LoadddlToAccountNo()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub ddlcompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcompany.SelectedIndexChanged
        Try

            LoadddlFromAccountNo()
            LoadddlToAccountNo()


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click



        Dim AccountBalance As Double = 0
        Dim SweepActionsCount As Integer = 0
        Dim SweepActionID As Integer
        Dim objICSweepAction As New ICSweepAction
        Dim objICSweepActionInstruction As ICSweepActionInstructions
        Dim StrBestPossible As String
        Dim dtBestPossibleDate As New DataTable
        If Page.IsValid Then
            Try

                If ddlFromAccount.SelectedValue = ddlToAccountNo.SelectedValue Then
                    UIUtilities.ShowDialog(Me, "Error", "Destination account must be other than source account", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                If Not txtAmountPKR.Text Is Nothing And txtAmountPKR.Text IsNot "" Then

                    If CDbl(txtAmountPKR.Text) < 1 Then
                        UIUtilities.ShowDialog(Me, "Error", "Invalid amount", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    AccountBalance = CBUtilities.BalanceInquiry(ddlFromAccount.SelectedValue.Split("-")(0).ToString, CBUtilities.AccountType.RB, "Sweep Action Source Account Balance", ddlFromAccount.SelectedValue.Split("-")(0).ToString, ddlFromAccount.SelectedValue.Split("-")(1).ToString)
                    If CDbl(txtAmountPKR.Text) > AccountBalance Then
                        UIUtilities.ShowDialog(Me, "Error", "Entered amount is greater than available balance.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    If CDbl(txtAmountPKR.Text) > AccountBalance Then
                        UIUtilities.ShowDialog(Me, "Error", "Entered amount is greater than available balance.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    'Else
                    '    UIUtilities.ShowDialog(Me, "Error", "Invalid amount", ICBO.IC.Dialogmessagetype.Failure)
                    '    Exit Sub
                End If
                If txtStartDate.SelectedDate Is Nothing Then
                    UIUtilities.ShowDialog(Me, "Error", "Please select Schedule Start Date", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If txtEndDate.SelectedDate Is Nothing Then
                    UIUtilities.ShowDialog(Me, "Error", "Please select Schedule End Date", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If txtStartDate.SelectedDate > txtEndDate.SelectedDate Then
                    UIUtilities.ShowDialog(Me, "Error", "Schedule start must be greater than or equal to Schedule To Date", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If ddlScheduleFrequency.SelectedValue.ToString = "Monthly" Then
                    Dim MonthInterval As Integer = DateDiff(DateInterval.Month, CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate))
                    If MonthInterval <= 0 Then
                        UIUtilities.ShowDialog(Me, "Error", "Schedule End Date must be greater than or equal to one month from Schedule Start Date", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    If rblDaysOfMonth.SelectedValue.ToString = "Specific Day" Then
                        Dim DayInterval As Integer = DateDiff(DateInterval.Day, CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate))
                        If DayInterval < CInt(ddlSpecificDay.SelectedValue) Then
                            UIUtilities.ShowDialog(Me, "Error", "Schedule End Date must be greater than or equal to Specific Day", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    End If
                ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Number Of Days" Then
                    Dim MonthInterval As Integer = DateDiff(DateInterval.Day, CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate))
                    If MonthInterval < CInt(txtNoOfDays.Text) Then
                        UIUtilities.ShowDialog(Me, "Error", "Schedule End Date must be greater than or equal to No of Days", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                If ddlScheduleFrequency.SelectedValue.ToString = "Monthly" Then
                    If rblDaysOfMonth.SelectedValue.ToString = "First Day" Then
                        StrBestPossible = ICScheduleInstructionController.GetBestPossibleDay(CDate(txtStartDate.SelectedDate), "First Day", "")
                        dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, StrBestPossible, "First Day")
                    ElseIf rblDaysOfMonth.SelectedValue.ToString = "Last Day" Then
                        Dim MonthInterval As Integer = DateDiff(DateInterval.Month, CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate))
                        StrBestPossible = MonthInterval.ToString
                        dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, StrBestPossible, "Last Day")
                    ElseIf rblDaysOfMonth.SelectedValue.ToString = "Specific Day" Then
                        StrBestPossible = ICScheduleInstructionController.GetBestPossibleDay(CDate(txtStartDate.SelectedDate), "Specific Day", ddlSpecificDay.SelectedValue.ToString)
                        dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, StrBestPossible, "Specific Day")
                    End If
                ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Daily" Then
                    dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, "", "")
                ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Number Of Days" Then
                    StrBestPossible = ICScheduleInstructionController.GetBestPossibleDay(CDate(txtStartDate.SelectedDate), "Number Of Days", txtNoOfDays.Text.ToString)
                    dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, StrBestPossible, "Number Of Days")
                End If
                If dtBestPossibleDate.Rows.Count > 0 Then
                    objICSweepAction = GetSweepActionMainObject(dtBestPossibleDate.Rows.Count)
                    SweepActionID = ICSweepActionsController.AddSweepAction(objICSweepAction, False, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "ADD")
                    For Each dr As DataRow In dtBestPossibleDate.Rows
                        SweepActionsCount = SweepActionsCount + 1
                        objICSweepActionInstruction = New ICSweepActionInstructions
                        objICSweepActionInstruction = GetSweepInstructionsMainObject(SweepActionID, CDate(dr("ValueDates")))
                        ICSweepActionInstructionsController.AddSweepActionsInstructions(objICSweepActionInstruction, False, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "ADD")
                    Next
                    Clear()
                    UIUtilities.ShowDialog(Me, "Save Sweep Actions", "[ " & SweepActionsCount & " ] Sweep Action(s) added successfully.", ICBO.IC.Dialogmessagetype.Success)
                Else
                    UIUtilities.ShowDialog(Me, "Error", "Invalid sweep action date", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Sub Clear()
        LoadddlGroup()
        LoadddlCompany()
        LoadddlFromAccountNo()
        LoadddlToAccountNo()
        txtAmountPKR.Text = ""
        txtScheduleTitle.Text = ""
        ddlScheduleFrequency.ClearSelection()
        ddlSpecificDay.ClearSelection()
        txtNoOfDays.Text = ""
        tblDayType.Style.Add("display", "none")
        tblSpecificDay.Style.Add("display", "none")
        tblNoOfDays.Style.Add("display", "none")
        txtStartDate.SelectedDate = Now.Date.AddDays(1)
        txtEndDate.SelectedDate = Now.Date.AddDays(1)
        radTimePicker.SelectedDate = Nothing
        rblDaysOfMonth.ClearSelection()
    End Sub
    Private Function GetSweepActionMainObject(ByVal TotalInstructions As Integer) As ICSweepAction
        Dim objICSweepAction As New ICSweepAction
        objICSweepAction.GroupCode = ddlGroup.SelectedValue
        objICSweepAction.CompanyCode = ddlcompany.SelectedValue
        objICSweepAction.FromAccountNo = ddlFromAccount.SelectedValue.Split("-")(0)
        objICSweepAction.FromAccountBrCode = ddlFromAccount.SelectedValue.Split("-")(1)
        objICSweepAction.FromAccountCurrency = ddlFromAccount.SelectedValue.Split("-")(2)
        objICSweepAction.FromAccountType = "RB"
        objICSweepAction.ToAccountNo = ddlToAccountNo.SelectedValue.Split("-")(0)
        objICSweepAction.ToAccountBrCode = ddlToAccountNo.SelectedValue.Split("-")(1)
        objICSweepAction.ToAccountCurrency = ddlToAccountNo.SelectedValue.Split("-")(2)
        objICSweepAction.ToAccountType = "RB"
        objICSweepAction.SweepActionTitle = txtScheduleTitle.Text
        objICSweepAction.SweepActionFrequency = ddlScheduleFrequency.SelectedValue.ToString
        objICSweepAction.SweepActionTime = CDate(radTimePicker.SelectedDate)
        If txtAmountPKR.Text IsNot Nothing And txtAmountPKR.Text IsNot "" Then
            objICSweepAction.AmountPerInstruction = CDbl(txtAmountPKR.Text)
        Else
            objICSweepAction.AmountPerInstruction = Nothing
        End If
        objICSweepAction.SweepActionFromDate = txtStartDate.SelectedDate
        objICSweepAction.SweepActionToDate = txtEndDate.SelectedDate
        objICSweepAction.TotalInstructions = TotalInstructions
        objICSweepAction.SweepActionCreatedBy = Me.UserInfo.UserID
        objICSweepAction.SweepActionCreationDate = Date.Now
        objICSweepAction.IsProcessed = False
        objICSweepAction.SweepActionDayType = rblDaysOfMonth.SelectedValue

        Return objICSweepAction
    End Function
    Private Function GetSweepInstructionsMainObject(ByVal SweepActionID As String, ByVal ValueDate As String) As ICSweepActionInstructions
        Dim objICSweepActionInstruction As New ICSweepActionInstructions

        objICSweepActionInstruction.SweepActionID = SweepActionID
        objICSweepActionInstruction.FromAccountNo = ddlFromAccount.SelectedValue.ToString.Split("-")(0)
        objICSweepActionInstruction.FromAccountBrCode = ddlFromAccount.SelectedValue.ToString.Split("-")(1)
        objICSweepActionInstruction.FromAccountCurrency = ddlFromAccount.SelectedValue.ToString.Split("-")(2)
        objICSweepActionInstruction.FromAccountType = "RB"
        objICSweepActionInstruction.ToAccountNo = ddlToAccountNo.SelectedValue.ToString.Split("-")(0)
        objICSweepActionInstruction.ToAccountBrCode = ddlToAccountNo.SelectedValue.ToString.Split("-")(1)
        objICSweepActionInstruction.ToAccountCurrency = ddlToAccountNo.SelectedValue.ToString.Split("-")(2)
        objICSweepActionInstruction.ToAccountType = "RB"
        If txtAmountPKR.Text IsNot Nothing And txtAmountPKR.Text IsNot "" Then
            objICSweepActionInstruction.InstructionAmount = CDbl(txtAmountPKR.Text)
        Else
            objICSweepActionInstruction.InstructionAmount = Nothing
        End If
        objICSweepActionInstruction.SweepActionCreationDate = Date.Now
        objICSweepActionInstruction.SweepActionCreatedBy = Me.UserInfo.UserID.ToString
        objICSweepActionInstruction.SweepActionFromDate = txtStartDate.SelectedDate
        objICSweepActionInstruction.SweepActionToDate = txtEndDate.SelectedDate
        objICSweepActionInstruction.SweepActionDayType = rblDaysOfMonth.SelectedValue
        objICSweepActionInstruction.CurrentStatus = "Pending For Approval"
        objICSweepActionInstruction.LastStatus = "Pending For Approval"
        objICSweepActionInstruction.IsActive = False
        objICSweepActionInstruction.IsApproved = False
        objICSweepActionInstruction.IsProcessed = False
        objICSweepActionInstruction.IsPaid = False
        objICSweepActionInstruction.GroupCode = ddlGroup.SelectedValue
        objICSweepActionInstruction.CompanyCode = ddlcompany.SelectedValue
        objICSweepActionInstruction.ValueDate = ValueDate
        objICSweepActionInstruction.SweepActionFrequency = ddlScheduleFrequency.SelectedValue
        objICSweepActionInstruction.SweepActionTime = CDate(radTimePicker.SelectedDate)
        Return objICSweepActionInstruction
    End Function
    Protected Sub ddlFromAccount_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFromAccount.SelectedIndexChanged
        Try


            If ddlFromAccount.SelectedValue.ToString <> "0" Then
                txtAmountPKR.Text = CBUtilities.BalanceInquiry(ddlFromAccount.SelectedValue.Split("-")(0).ToString, CBUtilities.AccountType.RB, "Sweep Action Source Account Balance", ddlFromAccount.SelectedValue.Split("-")(0).ToString, ddlFromAccount.SelectedValue.Split("-")(1).ToString)

            Else
                txtAmountPKR.Text = ""
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL())
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlScheduleFrequency_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlScheduleFrequency.SelectedIndexChanged
        Try
            tblDayType.Style.Add("display", "none")
            tblNoOfDays.Style.Add("display", "none")
            tblSpecificDay.Style.Add("display", "none")
            rfvddlSpecificDay.Enabled = False
            radNoOfDay.Enabled = False
            txtNoOfDays.Text = ""
            ddlSpecificDay.ClearSelection()
            rfvrblrblDaysOfMonth.Enabled = False
            radScheduleTitle.GetSettingByBehaviorID("reScheduleTitle").Validation.IsRequired = True
            radNoOfDay.GetSettingByBehaviorID("reNoOfDays").Validation.IsRequired = False
            rblDaysOfMonth.ClearSelection()

            'sharedCalenderSceduleStartDate.RangeMinDate t= Date.Now
            'sharedCalenderEndScheduleDate.RangeMinDate = Date.Now
            'sharedCalenderEndScheduleDate.SelectedDate = Date.Now
            'sharedCalenderSceduleStartDate.SelectedDate = Date.Now
            txtStartDate.SelectedDate = Date.Now.AddDays(1)
            txtEndDate.SelectedDate = Date.Now.AddDays(1)

            If ddlScheduleFrequency.SelectedValue.ToString = "Daily" Then
                txtEndDate.SelectedDate = Nothing
                txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).Date
                sharedCalenderEndScheduleDate.RangeMinDate = CDate(txtStartDate.SelectedDate).Date
            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Monthly" Then
                sharedCalenderEndScheduleDate.RangeMinDate = Date.Now.AddDays(1).AddMonths(1)
                sharedCalenderEndScheduleDate.SelectedDate = Date.Now.AddDays(1).AddMonths(1)
                txtEndDate.SelectedDate = Nothing
                txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddMonths(1).Date
                tblDayType.Style.Remove("display")
                rfvrblrblDaysOfMonth.Enabled = True
            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Number Of Days" Then
                tblNoOfDays.Style.Remove("display")
                radNoOfDay.Enabled = True
                radNoOfDay.GetSettingByBehaviorID("reNoOfDays").Validation.IsRequired = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub rblDaysOfMonth_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblDaysOfMonth.SelectedIndexChanged
        Try
            tblSpecificDay.Style.Add("display", "none")
            rfvddlSpecificDay.Enabled = False
            If rblDaysOfMonth.SelectedValue.ToString = "Specific Day" Then
                tblSpecificDay.Style.Remove("display")
                rfvddlSpecificDay.Enabled = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub txtNoOfDays_TextChanged(sender As Object, e As EventArgs) Handles txtNoOfDays.TextChanged
        Try
            sharedCalenderEndScheduleDate.RangeMinDate = Date.Now.AddDays(1)
            sharedCalenderEndScheduleDate.SelectedDate = Date.Now.AddDays(1)

            If txtNoOfDays.Text <> "" Then
                sharedCalenderEndScheduleDate.RangeMinDate = Date.Now.AddDays(CInt(txtNoOfDays.Text)).Date
                sharedCalenderEndScheduleDate.SelectedDate = Date.Now.AddDays(CInt(txtNoOfDays.Text)).Date
                txtEndDate.SelectedDate = Nothing
                txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddDays(CInt(txtNoOfDays.Text))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub txtStartDate_SelectedDateChanged(sender As Object, e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles txtStartDate.SelectedDateChanged
        Try


            If ddlScheduleFrequency.SelectedValue.ToString = "Daily" Then
              
                sharedCalenderEndScheduleDate.RangeMinDate = CDate(Now.Date.AddDays(1))
            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Monthly" Then
                sharedCalenderEndScheduleDate.RangeMinDate = CDate(txtStartDate.SelectedDate).AddMonths(1).Date
                sharedCalenderEndScheduleDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddMonths(1).Date
            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Number Of Days" Then
                sharedCalenderEndScheduleDate.RangeMinDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date
                sharedCalenderEndScheduleDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date

                If Not txtStartDate.SelectedDate Is Nothing Then
                    txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date
                End If
            Else
                sharedCalenderEndScheduleDate.RangeMinDate = Date.Now.AddDays(1)
                sharedCalenderEndScheduleDate.SelectedDate = Date.Now.AddDays(1)
                txtStartDate.SelectedDate = Date.Now.AddDays(1)
                txtEndDate.SelectedDate = Date.Now.AddDays(1)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub txtEndDate_SelectedDateChanged(sender As Object, e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles txtEndDate.SelectedDateChanged
        Try


            If ddlScheduleFrequency.SelectedValue.ToString = "Daily" Then
                 sharedCalenderEndScheduleDate.RangeMinDate = CDate(Now.Date.AddDays(1))
            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Monthly" Then
                sharedCalenderEndScheduleDate.RangeMinDate = CDate(txtStartDate.SelectedDate).AddMonths(1).Date
                sharedCalenderEndScheduleDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddMonths(1).Date
            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Number Of Days" Then
                sharedCalenderEndScheduleDate.RangeMinDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date
                If CDate(txtEndDate.SelectedDate) >= CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text) Then

                Else
                    sharedCalenderEndScheduleDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date

                    If Not txtStartDate.SelectedDate Is Nothing Then
                        txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date
                    End If

                End If

               
            Else
                sharedCalenderEndScheduleDate.RangeMinDate = Date.Now.AddDays(1)
                sharedCalenderEndScheduleDate.SelectedDate = Date.Now.AddDays(1)
                txtStartDate.SelectedDate = Date.Now.AddDays(1)
                txtEndDate.SelectedDate = Date.Now.AddDays(1)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
