﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_SweepActions_ViewSweepActions
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private htRights As Hashtable
    Private ArrICAssignUserRolsID As New ArrayList
    Private SweepActionID As String
#Region "Page Load"
    Protected Sub DesktopModules_AccountsManagement_ViewAccounts_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SweepActionID = Request.QueryString("SweepActionID")
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadSweepActionsInstructions(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Sweep Action") = True Then
                    If Not ArrICAssignUserRolsID.Contains(RoleID.ToString) Then
                        ArrICAssignUserRolsID.Add(RoleID.ToString)
                    End If
                End If
            Next
        End If
    End Sub
    Private Sub DesignDts()
        Dim dtAccountNumber As New DataTable
        dtAccountNumber.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("BranchCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("Currency", GetType(System.String)))
        ViewState("AccountNumber") = dtAccountNumber
    End Sub
#End Region
#Region "Grid View Events"

    Protected Sub gvAccountsDetails_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvSweepActionsDetails.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                ' Dim arrPageSizes() As String = {"1", "2", "5", "10", "50"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvSweepActionsDetails.MasterTableView.ClientID)
                Next
                'If gvAccountsDetails.Items.Count > 0 Then
                '    myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
                'End If
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Sweep Action")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Private Sub LoadSweepActionsInstructions(ByVal DoDataBind As Boolean)
        Try

            Dim GroupCode, CompanyCode As String
            Dim dtFromAccountNo As New DataTable
            Dim dtToAccountNo As New DataTable
            GroupCode = "0"
            CompanyCode = "0"
            dtFromAccountNo = ViewState("FromAccountNo")
            dtToAccountNo = ViewState("ToAccountNo")
            ICSweepActionInstructionsController.GetAllSweepActionInstructionBySweepActionID(Me.gvSweepActionsDetails.CurrentPageIndex + 1, Me.gvSweepActionsDetails.PageSize, DoDataBind, Me.gvSweepActionsDetails, SweepActionID)
            If gvSweepActionsDetails.Items.Count > 0 Then
                gvSweepActionsDetails.Visible = True
                lblRNF.Visible = False
                btnDeleteSweepActionInstruction.Visible = CBool(htRights("Delete"))
                btnActivateSweepActionInstruction.Visible = CBool(htRights("Activate"))
                btnApproveSweepActionInstruction.Visible = CBool(htRights("Approve"))
                gvSweepActionsDetails.Columns(9).Visible = CBool(htRights("Delete"))
            Else
                gvSweepActionsDetails.Visible = False
                btnDeleteSweepActionInstruction.Visible = False
                btnActivateSweepActionInstruction.Visible = False
                btnApproveSweepActionInstruction.Visible = False
                lblRNF.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
    Protected Sub gvAccountsDetails_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvSweepActionsDetails.NeedDataSource
        Try
            LoadSweepActionsInstructions(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAccountsDetails_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvSweepActionsDetails.PageIndexChanged
        Try
            gvSweepActionsDetails.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub gvAccountsDetails_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvSweepActionsDetails.ItemCommand
        Try
            Dim objICSweepActionInstruction As New ICSweepActionInstructions
            Dim IsMasterDeleted As Boolean = False

            If e.CommandName.ToString = "del" Then
                If Page.IsValid Then

                    If objICSweepActionInstruction.LoadByPrimaryKey(e.CommandArgument) Then

                        If objICSweepActionInstruction.IsPaid = True Then
                            UIUtilities.ShowDialog(Me, "Delete Sweep Action", "Instructions paid and can not be deleted", ICBO.IC.Dialogmessagetype.Failure)
                            LoadSweepActionsInstructions(True)
                            Exit Sub
                        End If
                        ICSweepActionInstructionsController.DeleteSweepActionInstruction(e.CommandArgument.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                        If ICSweepActionInstructionsController.GetSweepInstructionsCountAgainstSweepAction(e.CommandArgument.ToString) = 0 Then
                            ICSweepActionsController.DeleteSweepActionsbySweepActionID(e.CommandArgument.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "DELETE")
                            IsMasterDeleted = True
                        End If
                        If IsMasterDeleted = False Then
                            UIUtilities.ShowDialog(Me, "Delete Sweep Action", "Sweep Action Instructions deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                            LoadSweepActionsInstructions(True)
                        Else
                            UIUtilities.ShowDialog(Me, "Delete Sweep Action", "Sweep Action Instructions deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())

                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Error ", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                LoadSweepActionsInstructions(True)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckGVCompanySelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvCompanyRow As GridDataItem In gvSweepActionsDetails.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvCompanyRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnDeleteAccounts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteSweepActionInstruction.Click
        Page.Validate()
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim PassToApproveCount As Integer = 0
                Dim FailToApproveCount As Integer = 0
                Dim objICSweepActionsInstruction As ICSweepActionInstructions
                Dim SweepActionInstructionID As String
                Dim IsMasterDeleted As Boolean = False
                If CheckGVCompanySelected() = False Then
                    UIUtilities.ShowDialog(Me, "Delete Sweep Action Instruction", "Please select atleast one sweep action instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVAccounts In gvSweepActionsDetails.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVAccounts.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        SweepActionInstructionID = ""

                        SweepActionInstructionID = rowGVAccounts.GetDataKeyValue("SweepActionInstructionID").ToString

                        objICSweepActionsInstruction = New ICSweepActionInstructions

                        objICSweepActionsInstruction.LoadByPrimaryKey(SweepActionInstructionID)

                        Try
                            If objICSweepActionsInstruction.IsPaid = True Then
                                FailToApproveCount = FailToApproveCount + 1
                                Continue For
                            Else
                                ICSweepActionInstructionsController.DeleteSweepActionInstruction(SweepActionInstructionID, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                                PassToApproveCount = PassToApproveCount + 1
                                Continue For
                            End If
                        Catch ex As Exception
                            FailToApproveCount = FailToApproveCount + 1
                            Continue For
                        End Try


                    End If
                Next
                If ICSweepActionInstructionsController.GetSweepInstructionsCountAgainstSweepAction(SweepActionID) = 0 Then
                    ICSweepActionsController.DeleteSweepActionsbySweepActionID(SweepActionID, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "DELETE")
                    IsMasterDeleted = True
                End If
                If Not PassToApproveCount = 0 And Not FailToApproveCount = 0 Then
                    If IsMasterDeleted = False Then
                        LoadSweepActionsInstructions(True)
                        UIUtilities.ShowDialog(Me, "Delete Sweep Action Instructions", "[ " & PassToApproveCount.ToString & " ] Sweep Actions Instruction(s) deleted successfully.<br /> [ " & FailToApproveCount.ToString & " ] Sweep Actions(s) can not be deleted due to following reasons:<br /> 1. Instructions are paid", ICBO.IC.Dialogmessagetype.Success)
                        Exit Sub
                    Else
                        LoadSweepActionsInstructions(True)
                        UIUtilities.ShowDialog(Me, "Delete Sweep Action Instructions", "[ " & PassToApproveCount.ToString & " ] Sweep Actions Instruction(s) deleted successfully.<br /> [ " & FailToApproveCount.ToString & " ] Sweep Actions(s) can not be deleted due to following reasons:<br /> 1. Instructions are paid", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If

                ElseIf Not PassToApproveCount = 0 And FailToApproveCount = 0 Then
                    If IsMasterDeleted = False Then
                        LoadSweepActionsInstructions(True)
                        UIUtilities.ShowDialog(Me, "Delete Sweep Action Instructions", "[ " & PassToApproveCount.ToString & " ] Sweep Actions Instruction(s) deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                        Exit Sub
                    Else
                        LoadSweepActionsInstructions(True)
                        UIUtilities.ShowDialog(Me, "Delete Sweep Action Instructions", "[ " & PassToApproveCount.ToString & " ] Sweep Actions Instruction(s) deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                ElseIf Not FailToApproveCount = 0 And PassToApproveCount = 0 Then
                    If IsMasterDeleted = False Then
                        LoadSweepActionsInstructions(True)
                        UIUtilities.ShowDialog(Me, "Delete Sweep Action Instructions", "[ " & FailToApproveCount.ToString & " ] Sweep Actions Instruction(s) can not be deleted due to following reasons: <br /> 1. Instructions are paid", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    Else
                        LoadSweepActionsInstructions(True)
                        UIUtilities.ShowDialog(Me, "Delete Sweep Action Instructions", "[ " & FailToApproveCount.ToString & " ] Sweep Actions Instruction(s) can not be deleted due to following reasons: <br /> 1. Instructions are paid", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Protected Sub btnActivateSweepActionInstruction_Click(sender As Object, e As EventArgs) Handles btnActivateSweepActionInstruction.Click
        Page.Validate()
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim PassToApproveCount As Integer = 0
                Dim FailToApproveCount As Integer = 0
                Dim objICSweepActionsInstruction As ICSweepActionInstructions
                Dim SweepActionInstructionID As String

                If CheckGVCompanySelected() = False Then
                    UIUtilities.ShowDialog(Me, "Activate Sweep Action Instruction", "Please select atleast one sweep action instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVAccounts In gvSweepActionsDetails.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVAccounts.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        SweepActionInstructionID = ""

                        SweepActionInstructionID = rowGVAccounts.GetDataKeyValue("SweepActionInstructionID").ToString

                        objICSweepActionsInstruction = New ICSweepActionInstructions

                        objICSweepActionsInstruction.LoadByPrimaryKey(SweepActionInstructionID)

                        Try
                            If objICSweepActionsInstruction.IsActive = True Then
                                FailToApproveCount = FailToApproveCount + 1
                                Continue For
                            Else
                                ICSweepActionInstructionsController.ActivateSweepActionInstruction(SweepActionInstructionID, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                                PassToApproveCount = PassToApproveCount + 1
                                Continue For
                            End If
                        Catch ex As Exception
                            FailToApproveCount = FailToApproveCount + 1
                            Continue For
                        End Try


                    End If
                Next

                If Not PassToApproveCount = 0 And Not FailToApproveCount = 0 Then
                    LoadSweepActionsInstructions(True)
                    UIUtilities.ShowDialog(Me, "Delete Sweep Action Instructions", "[ " & PassToApproveCount.ToString & " ] Sweep Actions Instruction(s) activated successfully.<br /> [ " & FailToApproveCount.ToString & " ] Sweep Actions Instructions(s) can not be activated due to following reasons:<br /> 1. Instructions are already activated", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToApproveCount = 0 And FailToApproveCount = 0 Then
                    LoadSweepActionsInstructions(True)
                    UIUtilities.ShowDialog(Me, "Delete Sweep Action Instructions", PassToApproveCount.ToString & " Sweep Actions Instruction(s) activated successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToApproveCount = 0 And PassToApproveCount = 0 Then
                    LoadSweepActionsInstructions(True)
                    UIUtilities.ShowDialog(Me, "Delete Sweep Action Instructions", "[ " & FailToApproveCount.ToString & " ] Sweep Actions Instruction(s) can not be activated due to following reasons: <br /> 1. Instructions are already activated", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnApproveSweepActionInstruction_Click(sender As Object, e As EventArgs) Handles btnApproveSweepActionInstruction.Click
        Page.Validate()
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim PassToApproveCount As Integer = 0
                Dim FailToApproveCount As Integer = 0
                Dim objICSweepActionsInstruction As ICSweepActionInstructions
                Dim SweepActionInstructionID As String

                If CheckGVCompanySelected() = False Then
                    UIUtilities.ShowDialog(Me, "Delete Sweep Action Instruction", "Please select atleast one sweep action instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVAccounts In gvSweepActionsDetails.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVAccounts.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        SweepActionInstructionID = ""

                        SweepActionInstructionID = rowGVAccounts.GetDataKeyValue("SweepActionInstructionID").ToString

                        objICSweepActionsInstruction = New ICSweepActionInstructions

                        objICSweepActionsInstruction.LoadByPrimaryKey(SweepActionInstructionID)

                        Try
                            If Not objICSweepActionsInstruction.IsActive = True Or objICSweepActionsInstruction.SweepInstructionActivatedBy = Me.UserInfo.UserID Then
                                FailToApproveCount = FailToApproveCount + 1
                                Continue For
                            Else
                                If objICSweepActionsInstruction.IsApproved = True Then
                                    FailToApproveCount = FailToApproveCount + 1
                                    Continue For
                                Else
                                    ICSweepActionInstructionsController.ApprovedSweepActionInstruction(SweepActionInstructionID, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                                    PassToApproveCount = PassToApproveCount + 1
                                    Continue For
                                End If

                            End If
                        Catch ex As Exception
                            FailToApproveCount = FailToApproveCount + 1
                            Continue For
                        End Try


                    End If
                Next

                If Not PassToApproveCount = 0 And Not FailToApproveCount = 0 Then
                    LoadSweepActionsInstructions(True)
                    UIUtilities.ShowDialog(Me, "Approve Sweep Action Instructions", "[ " & PassToApproveCount.ToString & " ] Sweep Actions Instruction(s) approved successfully.<br /> [ " & FailToApproveCount.ToString & " ] Sweep Actions Instructions(s) can not be approved due to following reasons:<br /> 1. Instructions are already approved<br /> 2. Checker must be other than maker<br /> 3. Instructions are not activated.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToApproveCount = 0 And FailToApproveCount = 0 Then
                    LoadSweepActionsInstructions(True)
                    UIUtilities.ShowDialog(Me, "Approve Sweep Action Instructions", PassToApproveCount.ToString & " Sweep Actions Instruction(s) approved successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToApproveCount = 0 And PassToApproveCount = 0 Then
                    LoadSweepActionsInstructions(True)
                    UIUtilities.ShowDialog(Me, "Approve Sweep Action Instructions", "[ " & FailToApproveCount.ToString & " ] Sweep Actions Instruction(s) can not be approved due to following reasons: <br /> 1. Instructions are already approved<br /> 2. Checker must be other than maker<br /> 3. Instructions are not activated.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub gvSweepActionsDetails_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles gvSweepActionsDetails.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvSweepActionsDetails.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL(), False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    
End Class
