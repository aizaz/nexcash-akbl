﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewPackageManagement.ascx.vb" Inherits="DesktopModules_PackageManagement_ViewPackageManagement" %>

    <script type="text/javascript">
        function con() {
            if (window.confirm("Are you sure you wish to remove this Record?") == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function delcon(item) {
            if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
                return true;
            }
            else {
                return false;
            }
        }
        function conBukDelete() {
            if (window.confirm("Are you sure you wish to remove Record(s)?") == true) {
                return true;
            }
            else {
                return false;
            }
        }  
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        //      alert(grid)
        //        if ((grid.rows.length -1) != 10 ) {
        //        if ((grid.rows.length) >= 12) {
        //            //            alert('More than 10 rows')
        //            for (i = 0; i < grid.rows.length - 1; i++) {
        //                cell = grid.rows[i].cells[CellNo];
        //                for (j = 0; j < cell.childNodes.length - 1; j++) {
        //                    if (cell.childNodes[j].type == "checkbox") {
        //                        cell.childNodes[j].checked = document.getElementById(idOfControllingCheckbox).checked;
        //                    }
        //                }
        //            }
        //        }
        //        else {
        //                     alert(grid.rows.length)
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    //    }
</script>

  <table width="100%">

     <tr align="Center" valign="top" style="width: 100%">
    <td align="Center" valign="top">
            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn" CausesValidation="false" 
                Width="74px" />
    </td>
   </tr>
       
     <tr align="Center" valign="top" style="width: 100%">
    <td align="left" valign="top">
                        <asp:Label ID="lblPackageList" runat="server" Text="Package List" CssClass="headingblue"></asp:Label>
    </td>
   </tr>
       
     <tr align="Center" valign="top" style="width: 100%">
    <td align="left" valign="top">
                        <asp:Label ID="lblRNF" runat="server" Font-Bold="False" Text="No Record Found" Visible="False"
                            CssClass="headingblue"></asp:Label>
    </td>
   </tr>
       
        <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
        <telerik:RadGrid ID="gvPackage" runat="server" AllowPaging="True" AllowSorting="True"
                AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" 
                PageSize="100" Width="100%" GridLines="None">
                <ClientSettings>
                
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                </ClientSettings>
                <AlternatingItemStyle CssClass="rgAltRow" />
                 <MasterTableView NoMasterRecordsText="" DataKeyNames="PackageID" TableLayout="Fixed">
               
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    </ExpandCollapseColumn>
                    <Columns>
                       <telerik:GridTemplateColumn HeaderText="Approve" DataField="Active">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAllGlobalID" runat="server" CssClass="chkBox" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="Title" HeaderText="Title" SortExpression="Title" FilterControlAltText="Filter Title column" UniqueName="Title">
                        </telerik:GridBoundColumn>

                         <telerik:GridBoundColumn DataField="DisbursementMode" HeaderText="Payment Mode" SortExpression="DisbursementMode" FilterControlAltText="Filter DisbursementMode column" UniqueName="DisbursementMode">
                        </telerik:GridBoundColumn>


                           <telerik:GridBoundColumn DataField="PackageType" HeaderText="Package Type" SortExpression="PackageType" FilterControlAltText="Filter PackageType column" UniqueName="PackageType">
                        </telerik:GridBoundColumn>


                             <telerik:GridTemplateColumn>
                            <HeaderTemplate>
                                <asp:Label ID="isActive" runat="server" Text="Active"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                              <%--  <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveFTPSettings", "&mid=" & Me.ModuleId & "&FTPSettingsID="& Eval("FTPSettingsID"))%>'
                                    ToolTip="Edit">Edit</asp:HyperLink>--%>
                                    <asp:CheckBox runat="server" ID="chkActive" Enabled="false" Checked ='<%#Eval("IsActive") %>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>


                  
                        <telerik:GridTemplateColumn>
                            <HeaderTemplate>
                                <asp:Label ID="Edit" runat="server" Text="Edit"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SavePackage", "&mid=" & Me.ModuleId & "&PackageID="& Eval("PackageID"))%>'
                                    ToolTip="Edit">Edit</asp:HyperLink>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn>
                            <HeaderTemplate>
                                <asp:Label ID="Delete" runat="server" Text="Delete"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("PackageID").ToString%>'
                                    CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                    ToolTip="Delete" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                    <PagerStyle AlwaysVisible="True" />
                </MasterTableView>
                <ItemStyle CssClass="rgRow" />
                <PagerStyle AlwaysVisible="True" />
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
     
        </td>
    </tr>
    <tr  align="center" valign="top" style="width: 100%">
      <td>
        <asp:Button ID="btnDeleteGlobalID" runat="server" Text="Delete Package(s)" OnClientClick="javascript: return conBukDelete();"
                            CssClass="btn" />
     
      </td>
    </tr>
    <tr  align="center" valign="top" style="width: 100%">
      <td>
          &nbsp;</td>
    </tr>
    </table>
