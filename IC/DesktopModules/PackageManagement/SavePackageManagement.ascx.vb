﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Security.Permissions
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports Telerik.Web.UI

Partial Class DesktopModules_User_Limits_ViewUserLimits
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Dim PackageID As String = Nothing
    Private htRights As Hashtable


#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Not Page.IsPostBack Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()

                LoadddlAccountsGlobalCharges()

                Clear()
                If Not Request.QueryString("packageid") = 0 Then
                    lblPageHeader.Text = "Update Package"
                    savebtn.Text = "Update"

                    LoadPackage(Request.QueryString("packageid"))


                    savebtn.Visible = CBool(htRights("Add"))
                    btnSavePercVolume.Visible = CBool(htRights("Add"))

                Else
                    lblPageHeader.Text = "Add Package"
                    savebtn.Text = "Save"
                    savebtn.Visible = CBool(htRights("Add"))
                    btnSavePercVolume.Visible = CBool(htRights("Add"))
                End If


            End If


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub


    Private Sub LoadPackage(ByVal packageid As Integer)


        Dim package As IC.ICPackage


        package = ICPackageManagementController.GetPackage(packageid)


        If Not package Is Nothing Then


            Me.ddlPaymentMode.SelectedValue = package.DisbursementMode

            Me.txtTitle.Text = package.Title

            Me.rdFixed.SelectedValue = package.PackageType

            setpackagetype()

            If package.PackageType = "Fixed" Then

                Me.txtChargeAmount.Text = package.PackageAmount



            ElseIf package.PackageType = "Percentage of Instruction Amount" Then

                Me.txtChargeAmount.Text = package.PackagePercentage



            Else


                Dim packageslabs As IC.ICPackageSlabsCollection

                packageslabs = ICPackageManagementController.GetPackageSlabs(packageid)





                If Not packageslabs Is Nothing Then


                    Dim dt As DataTable = GetSlabsDt()



                    For Each slab As ICPackageSlabs In packageslabs


                        Dim row As DataRow = dt.NewRow


                        row("Sno") = dt.Rows.Count + 1
                        row("MinValue") = slab.Min
                        row("MaxValue") = slab.Max
                        row("Percentage") = slab.Percentage


                        dt.Rows.Add(row)








                    Next



                    Me.gvVolume.DataSource = dt
                    Me.gvVolume.DataBind()






                End If




            End If



            If Not package.ChargeFrequency = "" Then


                Me.ddlChargeFrequency.SelectedValue = package.ChargeFrequency





            End If



            Dim pcharges As IC.ICPackagesGlobalChargesCollection


            pcharges = ICPackageManagementController.GetPackageCharges(packageid)

            If Not pcharges Is Nothing Then


                For Each charge As IC.ICPackagesGlobalCharges In pcharges


                    Dim item As ListItem = Me.chkglobalcharges.Items.FindByValue(charge.GlobalChargeID)

                    If Not item Is Nothing Then



                        item.Selected = True





                    End If






                Next


            End If



            Me.chkActive.Checked = package.IsActive




        End If




    End Sub




    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "PackageManagement")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub



#End Region


  


    Protected Sub savebtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles savebtn.Click
        Try

            Dim dtslabs As DataTable



            dtslabs = ViewState("dtslabs")


            Dim ObjICPackage As New ICBO.IC.ICPackage

            ObjICPackage.DisbursementMode = Me.ddlPaymentMode.SelectedValue
            ObjICPackage.Title = Me.txtTitle.Text
            ObjICPackage.PackageType = Me.rdFixed.SelectedValue

            If ObjICPackage.PackageType = "Fixed" Then


                ObjICPackage.PackageAmount = Me.txtChargeAmount.Text

                ObjICPackage.PackagePercentage = Nothing

                ObjICPackage.ChargeFrequency = Me.ddlChargeFrequency.SelectedValue



            ElseIf ObjICPackage.PackageType = "Percentage of Instruction Amount" Then

                ObjICPackage.PackagePercentage = Me.txtChargeAmount.Text

                ObjICPackage.PackageAmount = Nothing

                ObjICPackage.ChargeFrequency = Nothing


            ElseIf ObjICPackage.PackageType = "Percentage of Volume" Then

                ObjICPackage.PackageAmount = Nothing
                ObjICPackage.PackagePercentage = Nothing

                ObjICPackage.ChargeFrequency = Me.ddlChargeFrequency.SelectedValue


                If dtslabs Is Nothing Then


                    UIUtilities.ShowDialog(Me, "Error", "Please enter at least one slab", ICBO.IC.Dialogmessagetype.Failure)

                    Exit Sub

                ElseIf dtslabs.Rows.Count = 0 Then


                    UIUtilities.ShowDialog(Me, "Error", "Please enter at least one slab", ICBO.IC.Dialogmessagetype.Failure)

                    Exit Sub





                End If



            End If


            ObjICPackage.IsActive = Me.chkActive.Checked

            ObjICPackage.CreatedBy = Me.UserId
            ObjICPackage.CreatedOn = Now



            Dim globalcharges As New ArrayList


            For Each item As ListItem In Me.chkglobalcharges.Items


                If item.Selected Then

                    globalcharges.Add(item.Value)




                End If



            Next


          




            If Request.QueryString("packageid") Is Nothing Then


                If CheckDupliCateTitle(ObjICPackage, False) = False Then
                    ICPackageManagementController.AddPackage(ObjICPackage, False, Me.UserId, Me.UserInfo.Username, globalcharges, dtslabs)
                    UIUtilities.ShowDialog(Me, "Save Package", "Package added successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Clear()
                Else
                    UIUtilities.ShowDialog(Me, "Save Package", "Duplicate package not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If


            Else

                ObjICPackage.PackageID = Request.QueryString("packageid")
                If CheckDupliCateTitle(ObjICPackage, True) = False Then
                    ICPackageManagementController.AddPackage(ObjICPackage, True, Me.UserId, Me.UserInfo.Username, globalcharges, dtslabs)
                    UIUtilities.ShowDialog(Me, "Save Package", "Package updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Save Package", "Duplicate package not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            End If





        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)

        End Try
    End Sub
    Private Sub Clear()
        ddlPaymentMode.ClearSelection()
        txtTitle.Text = ""
        rdFixed.ClearSelection()
        rdFixed.SelectedValue = "Fixed"
        ddlChargeFrequency.ClearSelection()
        txtChargeAmount.Text = ""
        LoadddlAccountsGlobalCharges()
        chkActive.Checked = False
    End Sub
    Private Function CheckDupliCateTitle(ByVal objICPackages As ICPackage, ByVal IsUpdate As Boolean) As Boolean
        Dim Result As Boolean = False

        Dim objICPackageColl As New ICPackageCollection

        objICPackageColl.Query.Where(objICPackageColl.Query.Title = objICPackages.Title)
        If IsUpdate = True Then
            objICPackageColl.Query.Where(objICPackageColl.Query.PackageID <> objICPackages.PackageID)
        End If
        If objICPackageColl.Query.Load Then
            Result = True
        End If
        Return Result
    End Function
    Private Function CheckDuplicate(ByVal objICGlobalID As ICGlobalCharges) As Boolean
        Try
            Dim rslt As Boolean = False

            Dim ObjICPackageColl As New ICPackageCollection

            ObjICPackageColl.es.Connection.CommandTimeout = 3600
            ObjICPackageColl.LoadAll()
            ObjICPackageColl.Query.Where(ObjICPackageColl.Query.IsActive)
            If ObjICPackageColl.Query.Load() Then
                rslt = True
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function





    Protected Sub cancelbtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelbtn.Click
        Response.Redirect(NavigateURL("", "&mid=" & Me.ModuleId & "&id=0"), False)
    End Sub

    Protected Sub rdFixed_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdFixed.SelectedIndexChanged

        setpackagetype()


      
    End Sub


    Private Sub setpackagetype()


        If rdFixed.SelectedIndex = 0 Then

            ddlChargeFrequency.Enabled = True
            reqChargeFrequency.Enabled = True
            txtChargeAmount.Visible = True
            lblReqChargeAmount0.Visible = True
            ddlChargeFrequency.Visible = True
            lblReqChargeAmount1.Visible = True
            lblChargeFrequency.Visible = True
            pnlPercentagaOfVolume.Visible = False
            gvVolume.Visible = False
            Me.lblChargeAmountTitle.Visible = True

            Me.lblChargeAmountTitle.Text = "Charge Amount"

            Dim setting As Telerik.Web.UI.NumericTextBoxSetting

            setting = Me.RadInputManager2.GetSettingByBehaviorID("regChargeAmount")


            setting.MinValue = 0.001

            setting.MaxValue = 1000

            setting.ErrorMessage = "Please enter numeric values"


            setting.Type = Telerik.Web.UI.NumericType.Number

            ' Me.lblChargeAmountTitle.Visible = False


        ElseIf rdFixed.SelectedIndex = 1 Then
            txtChargeAmount.Visible = True
            lblReqChargeAmount0.Visible = True
            reqChargeFrequency.Enabled = False
            Me.lblChargeAmountTitle.Visible = True
            ' lblChargeAmount.Text = "Percentage Of Instruction Amount"
            ddlChargeFrequency.Enabled = True
            ddlChargeFrequency.ClearSelection()
            ddlChargeFrequency.Visible = False
            lblChargeFrequency.Visible = False
            lblReqChargeAmount1.Visible = False

            pnlPercentagaOfVolume.Visible = False
            '/   txtPercentageOfInstructionAmount.Text =Nothing 
            gvVolume.Visible = False


            Me.lblChargeAmountTitle.Text = "Charge Percentage"


            Dim setting As Telerik.Web.UI.NumericTextBoxSetting

            setting = Me.RadInputManager2.GetSettingByBehaviorID("regChargeAmount")


            setting.MinValue = 0.001

            setting.MaxValue = 100

            setting.ErrorMessage = "Percentage must be less than 100"


            setting.Type = Telerik.Web.UI.NumericType.Percent

            '  Me.lblChargeAmountTitle.Visible = False

        ElseIf rdFixed.SelectedIndex = 2 Then

            reqChargeFrequency.Enabled = True
            txtChargeAmount.Visible = False


            pnlPercentagaOfVolume.Visible = True
            ddlChargeFrequency.Visible = True
            lblReqChargeAmount1.Visible = True
            gvVolume.Visible = True
            lblReqChargeAmount0.Visible = False
            lblChargeFrequency.Visible = True
            Me.lblChargeAmountTitle.Visible = False

        End If




    End Sub


    Private Sub LoadddlDisbModes()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlPaymentMode.Items.Clear()
            ddlPaymentMode.Items.Add(lit)
            ddlPaymentMode.AppendDataBoundItems = True
            ddlPaymentMode.DataSource = ICProductTypeController.GetAllProductTypesActiveAndApprove()
            ddlPaymentMode.DataTextField = "ProductTypeName"
            ddlPaymentMode.DataValueField = "ProductTypeCode"
            ddlPaymentMode.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub



    Private Sub LoadddlAccountsGlobalCharges()
        Try
           
            Dim dt As New IC.ICGlobalChargesCollection
            
            Dim dtp As New DataTable
           
            dt = ICGlobalChargesController.GetGlobalChargesForPackages()

            If Not dt Is Nothing Then

                Me.chkglobalcharges.DataTextField = "Title"
                Me.chkglobalcharges.DataValueField = "GlobalChargeID"

                Me.chkglobalcharges.DataSource = dt


                Me.chkglobalcharges.DataBind()




            End If



        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub btnSavePercVolume_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSavePercVolume.Click
        If Page.IsValid Then
            Try
                If CheckIsValid(Me.txtmin.Text, Me.txtmax.Text) = False Then
                    UIUtilities.ShowDialog(Me, "Error", "Min and max value overlap with other values", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Dim dt As DataTable
                dt = ViewState("dtslabs")
                If dt Is Nothing Then
                    dt = GetSlabsDt()
                End If
                Dim dr As DataRow
                'Assign Data Row of datatable

                dr = dt.NewRow()

                'Add values in data row columns

                dr("Sno") = dt.Rows.Count + 1
                dr("MinValue") = txtmin.Text
                dr("MaxValue") = txtmax.Text
                dr("Percentage") = txtPercentageOfAmount.Text

                dt.Rows.Add(dr)

                ViewState("dtslabs") = dt
                gvVolume.Visible = True
                gvVolume.DataSource = dt
                gvVolume.DataBind()


                Me.txtmin.Text = ""
                Me.txtmax.Text = ""
                Me.txtPercentageOfAmount.Text = ""


            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Private Function GetSlabsDt() As DataTable


        Dim dt As DataTable


        dt = ViewState("dtslabs")

        If dt Is Nothing Then


            dt = New DataTable("dtslabs")

            dt.Columns.Add("SNo", GetType(Integer))
            dt.Columns.Add("MinValue", GetType(Long))
            dt.Columns.Add("MaxValue", GetType(Long))
            dt.Columns.Add("Percentage", GetType(Decimal))


            Dim pk(0) As DataColumn

            pk(0) = dt.Columns(0)

            dt.PrimaryKey = pk


            ViewState("dtslabs") = dt





        End If


        Return dt




    End Function
    'Private Function CheckIsValid(ByVal min As Double, ByVal max As Double) As Boolean

    '    Dim dt As DataTable = ViewState("dtslabs")

    '    If Not dt Is Nothing Then


    '        For Each row As DataRow In dt.Rows
    '            If min >= row("minvalue") And min <= row("maxvalue") Then
    '                Return False
    '                Exit For
    '                Exit Function
    '            ElseIf max >= row("minvalue") And max <= row("maxvalue") Then
    '                Return False
    '                Exit For
    '                Exit Function
    '            ElseIf max >= row("minvalue") And row("maxvalue") <= max Then
    '                Return False
    '                Exit For
    '                Exit Function
    '            End If



    '        Next

    '        Return True


    '    Else

    '        Return True






    '    End If




    'End Function
    Private Function CheckIsValid(ByVal min As Double, ByVal max As Double) As Boolean

        Dim dt As DataTable = ViewState("dtslabs")
        Dim Result As Boolean = True
        If Not dt Is Nothing Then


            For Each row As DataRow In dt.Rows





                If min < row("minvalue") And max < row("minvalue") Then



                    'Return True
                    Result = True
                ElseIf min > row("maxvalue") And max > row("maxvalue") Then

                    Result = True
                    'Return True


                Else
                    Result = False
                    Return Result
                    Exit Function
                    'Return False



                End If



            Next
            Return Result
            'Return True


        Else

            Return True






        End If




    End Function
    Private Sub GetPerVolumeByPackageID(ByVal packagedID As String)

    End Sub

    Protected Sub gvVolume_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvVolume.ItemCommand
        Try
            If Not ViewState("dtslabs") Is Nothing Then
                Dim dt As DataTable
                dt = ViewState("dtslabs")
                If e.CommandName = "del" Then
                    dt.Rows.Remove(dt.Rows.Find(e.CommandArgument))
                    ViewState("dtslabs") = dt
                    gvVolume.DataSource = dt
                    gvVolume.DataBind()
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub


End Class
