﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SavePackageManagement.ascx.vb"
    Inherits="DesktopModules_User_Limits_ViewUserLimits" %>
    <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadInputManager ID="RadInputManager2" runat="server">
   <%-- <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior4" Validation-IsRequired="true"
        ValidationExpression="^[ a-zA-Z][ a-zA-Z\\s]+$" ErrorMessage="Invalid Name" EmptyMessage="Enter Province Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtProvinceName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>    --%>


        <telerik:NumericTextBoxSetting DecimalDigits="3" BehaviorID="regChargeAmount" Validation-IsRequired="true" AllowRounding="false"
         ErrorMessage="Invalid Amount"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtChargeAmount" />
        </TargetControls>

<Validation IsRequired="True"></Validation>
        </telerik:NumericTextBoxSetting> 

            <telerik:NumericTextBoxSetting MaxValue="100" DecimalDigits="0" BehaviorID="regPercAmount" Validation-IsRequired="true"
         ErrorMessage="Invalid Percentage Of Instruction Amount"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPercentageOfInstructionAmount" />
        </TargetControls>

<Validation IsRequired="True"></Validation>
        </telerik:NumericTextBoxSetting> 

              <telerik:NumericTextBoxSetting DecimalDigits="2" BehaviorID="regAddCharges" Validation-IsRequired="true"
         ErrorMessage="Invalid Additional Charges"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAddtionalCharges" />
        </TargetControls>

<Validation IsRequired="True"></Validation>
        </telerik:NumericTextBoxSetting> 



          <telerik:RegExpTextBoxSetting  ValidationExpression="^[a-zA-Z0-9 ]+$" BehaviorID="regChargeAccount" Validation-IsRequired="true"
         ErrorMessage="Invalid Account No" 
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtChargeFrequency" />
        </TargetControls>

<Validation IsRequired="True"></Validation>
        </telerik:RegExpTextBoxSetting> 

           <telerik:RegExpTextBoxSetting  ValidationExpression="^[0-9 ]+$" BehaviorID="regPercMin" Validation-IsRequired="true"
         ErrorMessage="Invalid Min Amount" 
        EmptyMessage="" Validation-ValidationGroup="pv">
        <TargetControls>
            <telerik:TargetInput ControlID="txtmin" />
        </TargetControls>

<Validation IsRequired="True"></Validation>
        </telerik:RegExpTextBoxSetting> 

           <telerik:RegExpTextBoxSetting  ValidationExpression="^[0-9 ]+$" BehaviorID="reqPercMax" Validation-IsRequired="true"
         ErrorMessage="Invalid Max Amount" 
        EmptyMessage="" Validation-ValidationGroup="pv">
        <TargetControls>
            <telerik:TargetInput ControlID="txtmax" />
        </TargetControls>

<Validation IsRequired="True"></Validation>
        </telerik:RegExpTextBoxSetting> 


         
    <telerik:NumericTextBoxSetting
        Type="Percent" AllowRounding="False" DecimalDigits="3" ErrorMessage="Percentage must be less than 100" MaxValue="100" MinValue="0.001" NegativePattern="-n %" PositivePattern="n %" ZeroPattern="n %" Validation-IsRequired="True" Validation-ValidationGroup="pv">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPercentageOfAmount" />
        </TargetControls>

<Validation IsRequired="True" ValidationGroup="pv"></Validation>
    </telerik:NumericTextBoxSetting>   

        
         
    <telerik:TextBoxSetting ErrorMessage="Please enter title" Validation-IsRequired="True">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>

        
         
</telerik:RadInputManager>





 


<style type="text/css">
    
    .style1
    {
        width: 11%;
    }
    
    .style2
    {
        width: 42%;
    }
    
</style>

    <%--<div><span><h3><span style="font-family:Verdana">Package(s) Management</h3></span> </div>--%>
<table width="100%">

 
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style2">
         
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" class="style1">
           
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">

          
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            
            &nbsp;</td>
       
    </tr>
   
 
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style2">
         
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" class="style1">
           
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">

          
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            
            &nbsp;</td>
       
    </tr>
   
 
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style2">
         
               &nbsp;</td>
        <td align="left" valign="top" class="style1">
           
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">

          
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            
            &nbsp;</td>
       
    </tr>
   
 
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style2">
         
               <asp:Label ID="lblPaymentMode" runat="server" Text="Payment Mode" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqPaymentMode" runat="server" Text="*" ForeColor="Red"></asp:Label>

        </td>
        <td align="left" valign="top" class="style1">
           
        </td>
        <td align="left" valign="top" style="width: 25%">

          
           <asp:Label ID="lblPR" runat="server" Text="Title" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
           


              </td>
        <td align="left" valign="top" style="width: 25%">
            
        </td>
       
    </tr>
   
 
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style2">
         
               <span>
            <asp:DropDownList runat="server" id="ddlPaymentMode" CssClass="dropdown">
               <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem>Cheque</asp:ListItem>
                <asp:ListItem Value="DD">Demand Draft</asp:ListItem>
                <asp:ListItem Value="PO">Pay Order</asp:ListItem>
                <asp:ListItem Value="Direct Credit">Fund Transfer</asp:ListItem>
                <asp:ListItem Value="Other Credit">Inter Bank Fund Transfer</asp:ListItem>
                <asp:ListItem>COTC</asp:ListItem>
                <asp:ListItem>Bill Payment</asp:ListItem>
               </asp:DropDownList>

                  </td>
        <td align="left" valign="top" class="style1">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">

          
            <span>
             <asp:TextBox ID="txtTitle" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>


              </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
       
    </tr>
   
 
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style2">
         
               <span>

                  <asp:RequiredFieldValidator ID="rfddlPaymentMode" runat="server" 
                ControlToValidate="ddlPaymentMode" Display="Dynamic" 
                ErrorMessage="Please select Payment Mode" InitialValue="0" 
                SetFocusOnError="True"></asp:RequiredFieldValidator>

        </td>
        <td align="left" valign="top" class="style1">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">

          
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
       
    </tr>
   
 
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style2">
         
               <span>

         <asp:Label ID="lblChargeAmount" runat="server" Text="Package Type"
                CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqChargeAmount" runat="server" Text="*" ForeColor="Red"></asp:Label>
            
        </td>
        <td align="left" valign="top" class="style1">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">

          
            <span>
        
            <asp:Label ID="lblChargeFrequency" runat="server" Text="Charge Frequency" CssClass="lbl"></asp:Label>
            

            <asp:Label ID="lblReqChargeAmount1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            

                 </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
       
    </tr>
   
 
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style2">
         
               <span>
               
                 <asp:RadioButtonList RepeatDirection="Horizontal" ID="rdFixed" 
                runat="server" AutoPostBack="True">
               <asp:ListItem Selected="True">Fixed</asp:ListItem>
               <asp:ListItem>Percentage of Instruction Amount</asp:ListItem>
              
                <asp:ListItem>Percentage of Volume</asp:ListItem>
            </asp:RadioButtonList>
                 </span>

        </td>
        <td align="left" valign="top" class="style1">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">

          
            <span>
             <asp:DropDownList runat="server" ID="ddlChargeFrequency" CssClass="dropdown">
                 <asp:ListItem Value="-1">--SELECT--</asp:ListItem>
                 <asp:ListItem>Weekly</asp:ListItem>
                 <asp:ListItem>Monthly</asp:ListItem>
                 <asp:ListItem>Quarterly</asp:ListItem>
            </asp:DropDownList>

            
              </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
       
    </tr>
   
        <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style2">

            <span>

            <asp:RequiredFieldValidator ID="reqChargeFrequency0" runat="server" 
                Display="Dynamic"  ControlToValidate ="rdFixed"
                ErrorMessage="Please select package type" 
                SetFocusOnError="True"></asp:RequiredFieldValidator>


                 </td>

        <td align="left" valign="top" class="style1">
         
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
        
            <span>

            <asp:RequiredFieldValidator ID="reqChargeFrequency" runat="server" 
                Display="Dynamic"  ControlToValidate ="ddlChargeFrequency"
                ErrorMessage="Please select frequency" InitialValue="-1" 
                SetFocusOnError="True"></asp:RequiredFieldValidator>


        </td>
        <td align="left" valign="top" style="width: 25%">
   
            &nbsp;</td>
    </tr>

        <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style2">

            <span>

         <asp:Label ID="lblChargeAmountTitle" runat="server" Text="Charge Amount"
                CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqChargeAmount0" runat="server" Text="*" ForeColor="Red"></asp:Label>
            

                 </td>

        <td align="left" valign="top" class="style1">
         
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
        
            <span>
          <asp:Label runat="server" ID="lblChargesAccount" Text="Select Global Charges" CssClass="lbl"></asp:Label>
              

        </td>
        <td align="left" valign="top" style="width: 25%">
   
            &nbsp;</td>
    </tr>

        <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style2">

            <span>
               <asp:TextBox ID="txtChargeAmount" runat="server" CssClass="txtbox" MaxLength="3"></asp:TextBox>
 
                 <asp:panel runat="server" ID="pnlPercentagaOfVolume" Visible="False">
                   <table style="width: 100%">
                    <tr>
                    <td>
                     Minimum Value:
                    </td>
                     <td>
                    <asp:TextBox runat="server" ID="txtmin" ValidationGroup="pv" MaxLength="9"></asp:TextBox><br />    
                     </td>
                    </tr>
                    <tr>
                      <td>
                          &nbsp;</td>
                    <td>
                  
                        &nbsp;</td>  
                    </tr>
                    <tr>

                    <td>
                         Maximum Value:  
                    </td>

                     <td>
                <asp:TextBox runat="server" ID="txtmax" ValidationGroup="pv" MaxLength="9"></asp:TextBox> 
                         <br />
      
                     </td>
                    </tr>

                       <tr>
                           <td>&nbsp;</td>
                           <td>
                               <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtmin" ControlToValidate="txtmax" ErrorMessage="Maximum value should be greater than minimum value" Operator="GreaterThan" ValidationGroup="pv" Type="Double"></asp:CompareValidator>
                           </td>
                       </tr>

                    <tr>
                    <td>Percentage: </td>
                    <td>
                        <asp:TextBox ID="txtPercentageOfAmount" runat="server" ValidationGroup="pv" MaxLength="3"></asp:TextBox>
                    </td>

                    </tr>
                       <tr>
                           <td>&nbsp;</td>
                           <td>&nbsp;</td>
                       </tr>
                       <tr>
                           <td></td>
                           <td>
                               <asp:Button ID="btnSavePercVolume" runat="server" Text="Save Volume" ValidationGroup="pv" Width="114px" />
                           </td>
                       </tr>
                   </table>     
                   
                    
                     
                 
                 </asp:panel>


                 </td>

        <td align="left" valign="top" class="style1">
         
            <span>
            <br />
         
        </td>
        <td align="left" valign="top" style="width: 25%">
        
            <%-- <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior4" Validation-IsRequired="true"
        ValidationExpression="^[ a-zA-Z][ a-zA-Z\\s]+$" ErrorMessage="Invalid Name" EmptyMessage="Enter Province Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtProvinceName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>    --%>
            <asp:CheckBoxList ID="chkglobalcharges" runat="server">
            </asp:CheckBoxList>


        </td>
        <td align="left" valign="top" style="width: 25%">
   
        </td>
    </tr>

        <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style2">

            &nbsp;</td>

        <td align="left" valign="top" class="style1">
         
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
        
            <span>
              <asp:Label ID="lblisActive" runat="server" Text="Active"
                CssClass="lbl"></asp:Label>
           
               
        </td>
        <td align="left" valign="top" style="width: 25%">
   
            &nbsp;</td>
    </tr>

        <tr>
        <td>

              <telerik:RadGrid ID="gvVolume" runat="server" AllowSorting="false"
                AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" 
                PageSize="100" Width="100%" GridLines="None" Height="178px" Visible="False">
                <ClientSettings>
                
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                </ClientSettings>
                <AlternatingItemStyle CssClass="rgAltRow" />
                 <MasterTableView NoMasterRecordsText="" TableLayout="Fixed">
               
                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    </ExpandCollapseColumn>
                    <Columns>
                        
                        <telerik:GridBoundColumn DataField="MinValue" HeaderText="Minimum Value" SortExpression="MinValue">
                        </telerik:GridBoundColumn>

                         <telerik:GridBoundColumn DataField="MaxValue" HeaderText="Maximum Value" SortExpression="MaxValue">
                        </telerik:GridBoundColumn>


                           <telerik:GridBoundColumn DataField="Percentage" HeaderText="Percentage" SortExpression="Percentage">
                        </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandName="del" ImageUrl="~/images/delete.gif"
                                                OnClientClick="javascript: return confirm('Are your sure want to delete!');" CommandArgument='<%#Eval("SNo")%>'
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                         <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridTemplateColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                    <PagerStyle AlwaysVisible="True" />
                </MasterTableView>
                <ItemStyle CssClass="rgRow" />
                <PagerStyle AlwaysVisible="True" />
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
            </td>

        <td align="left" valign="top" class="style1">
         
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <span>
           
               <asp:Checkbox ID="chkActive" runat="server" CssClass="txtbox" MaxLength="50"></asp:Checkbox>
           
            </td>
        <td align="left" valign="top" style="width: 25%">
   
            &nbsp;</td>
    </tr>

        <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style2">
           
            <br />
               </td>
        <td align="left" valign="top" class="style1">
             </td>
        <td align="left" valign="top" style="width: 25%">
            <span>
           
               <br />
           
            <br />
               </td>
        <td align="left" valign="top" style="width: 25%">
            <br />
            <br />
            <br />
            <br />
            <br />
            </td>
    </tr>

      <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="savebtn" runat="server" Text="Save" CssClass="btn" CausesValidation ="true" 
                Width="74px" />
            <asp:Button ID="cancelbtn" runat="server" Text="Cancel" CssClass="btnCancel"
                Width="75px" CausesValidation="False" />
    </tr>

</table>
