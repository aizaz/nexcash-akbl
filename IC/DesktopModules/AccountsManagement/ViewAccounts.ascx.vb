﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_AccountsManagement_ViewAccounts
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_AccountsManagement_ViewAccounts_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlCompany()
                btnSaveAccounts.Visible = CBool(htRights("Add"))
                btnDeleteAccounts.Visible = CBool(htRights("Delete"))
                LoadAccounts(0, True, Me.gvAccountsDetails.PageSize)



            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
#End Region
#Region "Grid View Events"

    Protected Sub gvAccountsDetails_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAccountsDetails.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                ' Dim arrPageSizes() As String = {"1", "2", "5", "10", "50"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAccountsDetails.MasterTableView.ClientID)
                Next
                'If gvAccountsDetails.Items.Count > 0 Then
                '    myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
                'End If
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    'Protected Sub gvAccountsDetails_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAccountsDetails.RowCommand
    '    Try
    '        If e.CommandName = "del" Then
    '            Dim ICAccounts As New ICAccounts
    '            Dim cmdArg1 As String = Nothing
    '            Dim cmdArg2 As String = Nothing
    '            Dim cmdArg3 As String = Nothing
    '            Dim s As String()
    '            s = e.CommandArgument.ToString().Split(";")
    '            cmdArg1 = s.GetValue(0)
    '            cmdArg2 = s.GetValue(1)
    '            cmdArg3 = s.GetValue(2)
    '            ICAccounts.es.Connection.CommandTimeout = 3600

    '            ICAccounts.LoadByPrimaryKey(cmdArg1, cmdArg2, cmdArg3)
    '            If ICAccounts.LoadByPrimaryKey(cmdArg1, cmdArg2, cmdArg3) Then
    '                If ICAccounts.IsApproved Then
    '                    UIUtilities.ShowDialog(Me, "Warning", "Account is approved and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
    '                Else
    '                    ICAccountsController.DeleteAccount(cmdArg1, cmdArg2, cmdArg3)

    '                    ICUtilities.AddAuditTrail("Account : " & ICAccounts.AccountNumber.ToString() & " Deleted", "Account", ICAccounts.BranchCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
    '                    UIUtilities.ShowDialog(Me, "Delete Account", "Account deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
    '                    LoadAccounts()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
    '            UIUtilities.ShowDialog(Me, "Deleted Account", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
    '            Exit Sub
    '        End If
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
#End Region
#Region "Button Events"
    Protected Sub btnSaveAccounts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAccounts.Click
        'If Page.IsValid Then
        Response.Redirect(NavigateURL("SaveAccount", "&mid=" & Me.ModuleId & "&AccountNumber=0" & "&BranchCode=0" & "&Currency=0"), False)
        'Dim obj As New RoleController
        'Dim arr As New ArrayList
        'arr = obj.GetUsersInRole(Me.PortalId, "All rights")
        'UIUtilities.ShowDialog(Me, "Roles", arr.Count.ToString, ICBO.IC.Dialogmessagetype.Success)
        'End If

    End Sub
#End Region
#Region "Other Functions/Routines"
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client Accounts Management")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            ' str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function

    Private Sub LoadAccounts(ByVal PageNumber As Integer, ByVal DoDataBind As Boolean, ByVal PageSize As Integer)
        Try

            Dim GroupCode, CompanyCode As String
            GroupCode = ""
            CompanyCode = ""

            If ddlGroup.SelectedValue.ToString <> "0" Then
                GroupCode = ddlGroup.SelectedValue.ToString
            End If
            If ddlcompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlcompany.SelectedValue.ToString
            End If

            ICAccountsController.GetAllAccountsForRadGridByGroupAndCompanyCode(PageNumber, PageSize, DoDataBind, Me.gvAccountsDetails, GroupCode, CompanyCode)
            If gvAccountsDetails.Items.Count > 0 Then
                gvAccountsDetails.Visible = True
                lblRNF.Visible = False
                btnSaveAccounts.Visible = CBool(htRights("Add"))
                btnApproveAccount.Visible = CBool(htRights("Approve"))
                btnDeleteAccounts.Visible = CBool(htRights("Delete"))
                gvAccountsDetails.Columns(7).Visible = CBool(htRights("Update"))
                gvAccountsDetails.Columns(8).Visible = CBool(htRights("Delete"))
            Else
                gvAccountsDetails.Visible = False
                btnDeleteAccounts.Visible = False
                btnApproveAccount.Visible = False
                lblRNF.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try




    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlcompany.Items.Clear()
            ddlcompany.Items.Add(lit)
            ddlcompany.AppendDataBoundItems = True
            ddlcompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString)
            ddlcompany.DataValueField = "CompanyCode"
            ddlcompany.DataTextField = "CompanyName"
            ddlcompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try




    End Sub
#End Region
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
            LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, True, Me.gvAccountsDetails.PageSize)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAccountsDetails_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAccountsDetails.NeedDataSource
        Try
            LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, False, Me.gvAccountsDetails.PageSize)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAccountsDetails_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvAccountsDetails.PageIndexChanged
        Try
            gvAccountsDetails.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlcompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcompany.SelectedIndexChanged
        Try
            LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, True, Me.gvAccountsDetails.PageSize)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAccountsDetails_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvAccountsDetails.ItemCommand


        Try
            Dim objICAccounts As New ICAccounts
            Dim StrArray As String()
            Dim chkSelect As CheckBox
            Dim PassToApproveCount As Integer = 0
            Dim FailToApproveCount As Integer = 0



            If e.CommandName.ToString = "del" Then
                Page.Validate()
                If Page.IsValid Then
                    StrArray = e.CommandArgument.ToString.Split(";")
                    Dim AccountNumber, BranchCode, Currency As String
                    AccountNumber = StrArray(0).ToString
                    BranchCode = StrArray(1).ToString
                    Currency = StrArray(2).ToString

                    objICAccounts.LoadByPrimaryKey(AccountNumber, BranchCode, Currency)

                    '' Checking IsApprove

                    'If objICAccounts.IsApproved = True Then
                    If objICAccounts.IsApproved = CBool(htRights("Approve")) Then
                        UIUtilities.ShowDialog(Me, "Approved Warning", "Account is already approved and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    Else
                        Try
                            ICAccountsController.DeleteAccount(AccountNumber, BranchCode, Currency, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            PassToApproveCount = PassToApproveCount + 1
                        Catch ex As Exception
                            FailToApproveCount = FailToApproveCount + 1

                        End Try
                    End If
                    'UIUtilities.ShowDialog(Me, "Delete Account", "Account deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                    'LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, True, Me.gvAccountsDetails.PageSize)
                End If
            End If

                If Not PassToApproveCount = 0 And Not FailToApproveCount = 0 Then
                LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, True, Me.gvAccountsDetails.PageSize)
                UIUtilities.ShowDialog(Me, "Delete Account", "[ " & PassToApproveCount.ToString & " ] Account(s) deleted successfully.<br /> [ " & FailToApproveCount.ToString & " ] Account(s) can not be deleted due to following reasons:<br /> 1. Deleted associated record(s) first.", ICBO.IC.Dialogmessagetype.Success)
                Exit Sub
            ElseIf Not PassToApproveCount = 0 And FailToApproveCount = 0 Then
                LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, True, Me.gvAccountsDetails.PageSize)
                UIUtilities.ShowDialog(Me, "Delete Account", PassToApproveCount.ToString & " Account(s) deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                Exit Sub
            ElseIf Not FailToApproveCount = 0 And PassToApproveCount = 0 Then
                LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, True, Me.gvAccountsDetails.PageSize)
                UIUtilities.ShowDialog(Me, "Delete Account", "[ " & FailToApproveCount.ToString & " ] Account(s) can not be deleted due to following reasons: <br /> 1. Deleted associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckGVCompanySelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvCompanyRow As GridDataItem In gvAccountsDetails.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvCompanyRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnDeleteAccounts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteAccounts.Click
        Page.Validate()
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim PassToApproveCount As Integer = 0
                Dim FailToApproveCount As Integer = 0
                Dim objICAccounts As ICAccounts
                Dim AccountNumber, BranchCode, Currency As String

                If CheckGVCompanySelected() = False Then
                    UIUtilities.ShowDialog(Me, "Delete Accounts", "Please select atleast one account.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVAccounts In gvAccountsDetails.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVAccounts.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        AccountNumber = ""
                        BranchCode = ""
                        Currency = ""
                        AccountNumber = rowGVAccounts.GetDataKeyValue("AccountNumber").ToString
                        BranchCode = rowGVAccounts.GetDataKeyValue("BranchCode").ToString
                        Currency = rowGVAccounts.GetDataKeyValue("Currency").ToString
                        objICAccounts = New ICAccounts
                        objICAccounts.LoadByPrimaryKey(AccountNumber.ToString, BranchCode.ToString, Currency.ToString)
                        Try
                            If objICAccounts.IsApproved = False Then
                                ICAccountsController.DeleteAccount(AccountNumber, BranchCode, Currency, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                PassToApproveCount = PassToApproveCount + 1
                            Else
                                FailToApproveCount = FailToApproveCount + 1
                                Continue For
                            End If
                        Catch ex As Exception
                            FailToApproveCount = FailToApproveCount + 1
                            Continue For
                        End Try
                    End If
                Next

                If Not PassToApproveCount = 0 And Not FailToApproveCount = 0 Then
                    LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, True, Me.gvAccountsDetails.PageSize)
                    UIUtilities.ShowDialog(Me, "Delete Account", "[ " & PassToApproveCount.ToString & " ] Account(s) deleted successfully.<br /> [ " & FailToApproveCount.ToString & " ] Account(s) can not be deleted due to following reasons:<br /> 1. Deleted associated record(s) first<br /> 2. Account is approved", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToApproveCount = 0 And FailToApproveCount = 0 Then
                    LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, True, Me.gvAccountsDetails.PageSize)
                    UIUtilities.ShowDialog(Me, "Delete Account", PassToApproveCount.ToString & " Account(s) deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToApproveCount = 0 And PassToApproveCount = 0 Then
                    LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, True, Me.gvAccountsDetails.PageSize)
                    UIUtilities.ShowDialog(Me, "Delete Account", "[ " & FailToApproveCount.ToString & " ] Account(s) can not be deleted due to following reasons: <br /> 1. Deleted associated record(s) first<br /> 2. Account is approved", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub gvAccountsDetails_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAccountsDetails.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAccountsDetails.MasterTableView.ClientID & "','0');"
        End If

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim Item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Dim hlAddSeries As New HyperLink
            Dim objICAccounts As New ICAccounts
            Dim AccountNo, BranchCode, Currency As String

            AccountNo = Nothing
            BranchCode = Nothing
            Currency = Nothing


            AccountNo = Item.GetDataKeyValue("AccountNumber")
            BranchCode = Item.GetDataKeyValue("BranchCode")
            Currency = Item.GetDataKeyValue("Currency")

            objICAccounts.LoadByPrimaryKey(AccountNo, BranchCode, Currency)
            hlAddSeries = DirectCast(e.Item.Cells(4).FindControl("hlAddMasterSeries"), HyperLink)


            ''If objICAccounts.IsApproved = True Then
           
            If objICAccounts.IsApproved = True Then
                hlAddSeries.Visible = True
            Else
                hlAddSeries.Visible = False
            End If


        End If
    End Sub

    Protected Sub btnApproveAccount_Click(sender As Object, e As EventArgs) Handles btnApproveAccount.Click
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim PassToApproveCount As Integer = 0
                Dim FailToApproveCount As Integer = 0
                Dim objICAccounts As ICAccounts
                Dim AccountNumber, BranchCode, Currency As String

                If CheckGVCompanySelected() = False Then
                    UIUtilities.ShowDialog(Me, "Approve Accounts", "Please select atleast one account.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVAccounts In gvAccountsDetails.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVAccounts.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        AccountNumber = ""
                        BranchCode = ""
                        Currency = ""
                        AccountNumber = rowGVAccounts.GetDataKeyValue("AccountNumber").ToString
                        BranchCode = rowGVAccounts.GetDataKeyValue("BranchCode").ToString
                        Currency = rowGVAccounts.GetDataKeyValue("Currency").ToString
                        objICAccounts = New ICAccounts

                        objICAccounts.LoadByPrimaryKey(AccountNumber.ToString, BranchCode.ToString, Currency.ToString)
                        If Me.UserInfo.IsSuperUser = False Then
                            If objICAccounts.CreateBy.ToString = Me.UserInfo.UserID.ToString Then
                                FailToApproveCount = FailToApproveCount + 1
                                Continue For
                            Else
                                Try
                                    ICAccountsController.ApproveAccount(AccountNumber, BranchCode, Currency, Me.UserId.ToString, Me.UserInfo.Username.ToString, True)
                                    PassToApproveCount = PassToApproveCount + 1
                                Catch ex As Exception
                                    FailToApproveCount = FailToApproveCount + 1
                                    Continue For
                                End Try
                            End If
                        Else
                            Try
                                ICAccountsController.ApproveAccount(AccountNumber, BranchCode, Currency, Me.UserId.ToString, Me.UserInfo.Username.ToString, True)
                                PassToApproveCount = PassToApproveCount + 1
                            Catch ex As Exception
                                FailToApproveCount = FailToApproveCount + 1
                                Continue For
                            End Try
                        End If
                    End If
                Next
                If Not PassToApproveCount = 0 And Not FailToApproveCount = 0 Then
                    LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, True, Me.gvAccountsDetails.PageSize)
                    UIUtilities.ShowDialog(Me, "Approve Account", "[ " & PassToApproveCount.ToString & " ] Account(s) Approved successfully.<br /> [ " & FailToApproveCount.ToString & " ] Account(s) can not be approved due to following reasons:<br /> 1.Account is already approved<br />2. Account must be approved by user other than maker", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToApproveCount = 0 And FailToApproveCount = 0 Then
                    LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, True, Me.gvAccountsDetails.PageSize)
                    UIUtilities.ShowDialog(Me, "Approve Account", PassToApproveCount.ToString & " Account(s) Approved successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToApproveCount = 0 And PassToApproveCount = 0 Then
                    LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, True, Me.gvAccountsDetails.PageSize)
                    UIUtilities.ShowDialog(Me, "Approve Account", "[ " & FailToApproveCount.ToString & " ] Account(s) can not be Approved due to following reasons:<br /> 1.Account is already approved<br />2. Account must be approved by user other than maker", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub
End Class
