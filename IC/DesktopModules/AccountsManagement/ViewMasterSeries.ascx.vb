﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_CompanyOfficeManagement_ViewCompanyOffice
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private AccntNumber As String
    Private BranchCode As String
    Private Currency As String
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_CompanyOfficeManagement_ViewCompanyOffice_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            AccntNumber = Request.QueryString("AccountNumber").ToString
            BranchCode = Request.QueryString("BranchCode").ToString
            Currency = Request.QueryString("Currency").ToString
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                SetHeaderValues()
                btnAddMasterSeries.Visible = CBool(htRights("Add"))
                btnBulkDeleteMSeries.Visible = CBool(htRights("Delete"))
                LoadgvMasterSeries(0, Me.gvViewMasterSeries.PageSize, True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client Accounts Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#Region "Button Events"
    Protected Sub btnSaveBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddMasterSeries.Click
        If Page.IsValid Then
            Response.Redirect(NavigateURL("saveMasterSeries", "&mid=" & Me.ModuleId & "&AccountNumber=" & AccntNumber & "&BranchCode=" & BranchCode & "&Currency=" & Currency & "&MasterSeriesID=0"), False)
        End If

    End Sub
#End Region

    Private Sub LoadgvMasterSeries(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean)
        Try
            ICMasterSeriesController.GetAllMasterSeriesForRadGrid(PageNumber, PageSize, Me.gvViewMasterSeries, DoDataBind, AccntNumber, BranchCode, Currency)

            If gvViewMasterSeries.Items.Count > 0 Then
                lblRNF.Visible = False
                gvViewMasterSeries.Visible = True
                btnBulkDeleteMSeries.Visible = CBool(htRights("Delete"))
                gvViewMasterSeries.Columns(5).Visible = CBool(htRights("Delete"))
            Else
                lblRNF.Visible = True
                gvViewMasterSeries.Visible = False
                btnBulkDeleteMSeries.Visible = False
            End If


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetHeaderValues()
        Try
            Dim objICAccounts As New ICAccounts

            If objICAccounts.LoadByPrimaryKey(AccntNumber, BranchCode, Currency) Then
                txtGroup.Text = objICAccounts.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupName.ToString()
                txtCompany.Text = objICAccounts.UpToICCompanyByCompanyCode.CompanyName.ToString()
                txtAccountNo.Text = objICAccounts.AccountNumber.ToString()
                txtAccountTitle.Text = objICAccounts.AccountTitle.ToString
                txtGroup.ReadOnly = True
                txtCompany.ReadOnly = True
                txtAccountNo.ReadOnly = True
                txtAccountTitle.ReadOnly = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    '#End Region

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    Protected Sub gvViewMasterSeries_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvViewMasterSeries.ItemCommand


        Dim objICMasterSeries As New ICMasterSeries
        objICMasterSeries.es.Connection.CommandTimeout = 3600

        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    If objICMasterSeries.LoadByPrimaryKey(e.CommandArgument.ToString) Then
                        If ICMasterSeriesController.GetIsMasterSeriesAssignedToLocation(objICMasterSeries.MasterSeriesID) = False Then
                            ICInstrumentController.DeleteAddedInstrumentSeriesByMasterSeriesID(objICMasterSeries.MasterSeriesID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            ICMasterSeriesController.DeleteMasterSeries(objICMasterSeries.MasterSeriesID, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            UIUtilities.ShowDialog(Me, "Delete Master Series", "Master series deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                            LoadgvMasterSeries(Me.gvViewMasterSeries.CurrentPageIndex + 1, Me.gvViewMasterSeries.PageSize, True)
                        Else
                            UIUtilities.ShowDialog(Me, "Delete Master Series", "Master series is assigned to office and can not be deleted.", ICBO.IC.Dialogmessagetype.Success)
                            LoadgvMasterSeries(Me.gvViewMasterSeries.CurrentPageIndex + 1, Me.gvViewMasterSeries.PageSize, False)
                            Exit Sub
                        End If

                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Error ", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                LoadgvMasterSeries(Me.gvViewMasterSeries.CurrentPageIndex + 1, Me.gvViewMasterSeries.PageSize, False)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvViewMasterSeries_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvViewMasterSeries.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                ' Dim arrPageSizes() As String = {"1", "2", "5", "10", "50"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvViewMasterSeries.MasterTableView.ClientID)
                Next
                'If gvAccountsDetails.Items.Count > 0 Then
                '    myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
                'End If
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub gvViewMasterSeries_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvViewMasterSeries.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvViewMasterSeries.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub gvViewMasterSeries_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvViewMasterSeries.NeedDataSource
        Try
            LoadgvMasterSeries(Me.gvViewMasterSeries.CurrentPageIndex + 1, Me.gvViewMasterSeries.PageSize, False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvViewMasterSeries_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvViewMasterSeries.PageIndexChanged
        Try
            gvViewMasterSeries.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckGVCompanySelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvMSRow As GridDataItem In gvViewMasterSeries.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvMSRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnBulkDeleteMSeries_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBulkDeleteMSeries.Click
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim PassToDeleteCount As Integer = 0
                Dim FailToDeleteCount As Integer = 0
                Dim objICMasterSeries As ICMasterSeries
                Dim MasterSeriesID As String

                If CheckGVCompanySelected() = False Then
                    UIUtilities.ShowDialog(Me, "Delete Master Series", "Please select atleast one master series.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVMasterSeries In gvViewMasterSeries.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVMasterSeries.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        MasterSeriesID = ""
                        MasterSeriesID = rowGVMasterSeries.GetDataKeyValue("MasterSeriesID").ToString
                        objICMasterSeries = New ICMasterSeries
                        objICMasterSeries.es.Connection.CommandTimeout = 3600
                        objICMasterSeries.LoadByPrimaryKey(MasterSeriesID.ToString)
                        Try
                            If ICMasterSeriesController.GetIsMasterSeriesAssignedToLocation(objICMasterSeries.MasterSeriesID.ToString) = False Then
                                ICInstrumentController.DeleteAddedInstrumentSeriesByMasterSeriesID(objICMasterSeries.MasterSeriesID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                ICMasterSeriesController.DeleteMasterSeries(MasterSeriesID, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                PassToDeleteCount = PassToDeleteCount + 1
                            Else
                                FailToDeleteCount = FailToDeleteCount + 1
                                Continue For
                            End If
                        Catch ex As Exception
                            FailToDeleteCount = FailToDeleteCount + 1
                            Continue For
                        End Try
                    End If
                Next

                If Not PassToDeleteCount = 0 And Not FailToDeleteCount = 0 Then
                    LoadgvMasterSeries(Me.gvViewMasterSeries.CurrentPageIndex + 1, Me.gvViewMasterSeries.PageSize, True)
                    UIUtilities.ShowDialog(Me, "Delete Master Series", "[ " & PassToDeleteCount.ToString & " ] Master series deleted successfully.<br /> [ " & FailToDeleteCount.ToString & " ] Master series can not be deleted due to following reasons:<br /> 1. Deleted associated record(s) first.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToDeleteCount = 0 And FailToDeleteCount = 0 Then
                    LoadgvMasterSeries(Me.gvViewMasterSeries.CurrentPageIndex + 1, Me.gvViewMasterSeries.PageSize, True)
                    UIUtilities.ShowDialog(Me, "Delete Master Series", PassToDeleteCount.ToString & " Master series deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToDeleteCount = 0 And PassToDeleteCount = 0 Then
                    LoadgvMasterSeries(Me.gvViewMasterSeries.CurrentPageIndex + 1, Me.gvViewMasterSeries.PageSize, True)
                    UIUtilities.ShowDialog(Me, "Delete Master Series", "[ " & FailToDeleteCount.ToString & " ] Master series can not be deleted due to following reasons: <br /> 1. Deleted associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
End Class
