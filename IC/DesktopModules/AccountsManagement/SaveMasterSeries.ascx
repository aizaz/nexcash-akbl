﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveMasterSeries.ascx.vb" Inherits="DesktopModules_CompanyOfficeManagement_ViewCompanyOffice" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
    <script type="text/javascript">
        function con() {
            if (window.confirm("Are you sure you wish to remove this Record?") == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function delcon(item) {
            if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
                return true;
            }
            else {
                return false;
            }
        }    
</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server">

    <telerik:TextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="false"
         >
        <TargetControls>
            <telerik:TargetInput ControlID="txtBranchCode" />
        </TargetControls>
    </telerik:TextBoxSetting>

     <telerik:TextBoxSetting BehaviorID="RagExpBehaviorAddress" Validation-IsRequired="false"
         >
        <TargetControls>
            <telerik:TargetInput ControlID="txtCurrency" />
        </TargetControls>
    </telerik:TextBoxSetting>
     <telerik:TextBoxSetting BehaviorID="RagExpBehaviorAccountNo" Validation-IsRequired="false"
         >
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
         <telerik:TextBoxSetting BehaviorID="RagExpBehaviorOffName" Validation-IsRequired="false">
        <TargetControls>
            <telerik:TargetInput ControlID="txtProductType" />
        </TargetControls>
    </telerik:TextBoxSetting>

         <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehaviorPhone1" ValidationExpression="[0-9]{1,23}"
        Validation-IsRequired="true" ErrorMessage="Invalid End Series">
        <TargetControls>
            <telerik:TargetInput ControlID="txtEndsAt" />
        </TargetControls>
       
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior3" ValidationExpression="[0-9]{1,17}" Validation-IsRequired="true"
        ErrorMessage="Invalid Start Series" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtStartsFrom" />
        </TargetControls>
        </telerik:RegExpTextBoxSetting>
         <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehanoOfLeaves" ValidationExpression="[0-9]{1,4}" Validation-IsRequired="true"
        ErrorMessage="Invalid No of Leaves" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtNoOfLeaves" />
        </TargetControls>
        </telerik:RegExpTextBoxSetting>
 
</telerik:RadInputManager>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="center" valign="top" colspan="4">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="lblReqHeader" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
            <asp:Label ID="lblBranchCode" runat="server" Text="Branch Code" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
            <asp:Label ID="lblCurrency" runat="server" Text="Currency" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top">
            <asp:TextBox ID="txtBranchCode" runat="server" CssClass="txtbox" 
                MaxLength="20"></asp:TextBox>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
            <asp:TextBox ID="txtCurrency" runat="server" CssClass="txtbox" 
                MaxLength="50"></asp:TextBox>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top">
            <asp:Label ID="lblAccountNo" runat="server" Text="Account Number" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
            <asp:Label ID="lblStartsFrom" runat="server" Text="Master Series From" CssClass="lbl"></asp:Label>
                        <asp:Label
                ID="lblReqStartFrom" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top">
            <asp:TextBox ID="txtAccountNo" runat="server" CssClass="txtbox" 
                MaxLength="100"></asp:TextBox>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
            <asp:TextBox ID="txtStartsFrom" runat="server" CssClass="txtbox" 
                MaxLength="17" AutoPostBack="True"></asp:TextBox>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top">
            <asp:Label ID="lblEndsAt0" runat="server" Text="No. of Leaves" CssClass="lbl"></asp:Label>
                        <asp:Label
                ID="lblReqnoOfLeaves" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
            <asp:Label ID="lblEndsAt" runat="server" Text="Master Series To" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top">
            <asp:TextBox ID="txtNoOfLeaves" runat="server" CssClass="txtbox" 
                MaxLength="4" AutoPostBack="True"></asp:TextBox>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
            <asp:TextBox ID="txtEndsAt" runat="server" CssClass="txtbox" 
                MaxLength="23" ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        <asp:RequiredFieldValidator ID="rfvPreferenceOrder" runat="server" 
                            ControlToValidate="txtEndsAt" Display="Dynamic" Enabled="true" 
                            ErrorMessage="Please enter master series start from and ends at values." InitialValue="" 
                            SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                </tr>
                <%--<tr>
                    <td align="left" valign="top">
            <asp:Label ID="lblIsPrePrinted" runat="server" Text="Is Pre Printed" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top">
            <asp:CheckBox ID="chkIsPrePrinted" runat="server" Text=" " CssClass="chkBox" />
                    </td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                </tr>--%>
                <tr>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="center" valign="top">
                        <asp:Button ID="btnSaveMasterSeries" runat="server" Text="Save" 
                            CssClass="btn" Width="67px" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                            CssClass="btnCancel" CausesValidation="False" Width="75px" />
                        </td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="center" valign="top" colspan="4">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        &nbsp;</td>
                </tr>
               </table>
</td>
</tr></table>