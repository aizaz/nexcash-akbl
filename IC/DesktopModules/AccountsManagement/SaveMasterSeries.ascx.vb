﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Partial Class DesktopModules_CompanyOfficeManagement_ViewCompanyOffice
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private AccntNumber As String
    Private BranchCode As String
    Private Currency As String
    Private MasterSeriesID As String
    Private TextSeriesStartFromChange As Boolean = False
    Private TextNoOfLeavesChange As Boolean = False
    Private htRights As Hashtable

    Protected Sub DesktopModules_CompanyOfficeManagement_ViewCompanyOffice_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            AccntNumber = Request.QueryString("AccountNumber").ToString()
            BranchCode = Request.QueryString("BranchCode").ToString()
            Currency = Request.QueryString("Currency").ToString()
            MasterSeriesID = Request.QueryString("MasterSeriesID").ToString()
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                If MasterSeriesID.ToString = "0" Then
                    lblPageHeader.Text = "Add Master Series"
                    btnSaveMasterSeries.Text = "Save"
                    SetHeaderValues()
                    btnSaveMasterSeries.Visible = CBool(htRights("Add"))
                Else
                    Dim objICMasterSeries As New ICMasterSeries
                    objICMasterSeries.es.Connection.CommandTimeout = 3600
                    objICMasterSeries.LoadByPrimaryKey(MasterSeriesID)
                    btnSaveMasterSeries.Visible = CBool(htRights("Update"))
                    lblPageHeader.Text = "Edit Master Series"
                    btnSaveMasterSeries.Text = "Update"
                    txtStartsFrom.ReadOnly = True
                    'chkIsPrePrinted.Checked = objICMasterSeries.IsPrePrinted
                    SetHeaderValues()
                    txtStartsFrom.Text = CLng(objICMasterSeries.StartFrom)
                    txtEndsAt.Text = CLng(objICMasterSeries.EndsAt)
                    txtNoOfLeaves.Text = CInt(txtEndsAt.Text - txtStartsFrom.Text) + 1
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client Accounts Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    'Private Sub ManagePageAccessRights()
    '    Try
    '        ArrRights = FRCRoleRightController.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Manage Area")
    '        If ArrRights.Count > 0 Then
    '            btnSaveBank.Visible = ArrRights(0)
    '        Else
    '            UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
    '        End If
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            ' str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function

    Private Sub SetHeaderValues()
        Try
            Dim objICAccount As New ICAccounts

            If objICAccount.LoadByPrimaryKey(AccntNumber, BranchCode, Currency) Then
                txtBranchCode.Text = objICAccount.BranchCode.ToString()
                txtCurrency.Text = objICAccount.Currency.ToString()
                txtAccountNo.Text = objICAccount.AccountNumber.ToString()
                txtCurrency.ReadOnly = True
                txtAccountNo.ReadOnly = True
                txtBranchCode.ReadOnly = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

 
    Protected Sub btnSaveMasterSeries_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveMasterSeries.Click
        If Page.IsValid Then
            Try
                Dim objICMasterSeries As New ICMasterSeries
                Dim objICMasterSeriesUpdate As New ICMasterSeries
                Dim objICInstrument As New ICInstruments
                Dim SavedMasterSeriesID As Integer = 0
                Dim StrAction As String = Nothing
                objICMasterSeries.es.Connection.CommandTimeout = 3600
                objICInstrument.es.Connection.CommandTimeout = 3600
                objICMasterSeries.StartFrom = CLng(txtStartsFrom.Text)
                objICMasterSeries.EndsAt = CLng(txtEndsAt.Text)
                objICMasterSeries.AccountNumber = txtAccountNo.Text.ToString()
                objICMasterSeries.BranchCode = txtBranchCode.Text.ToString()
                objICMasterSeries.Currency = txtCurrency.Text.ToString()
                'objICMasterSeries.IsPrePrinted = chkIsPrePrinted.Checked
                If MasterSeriesID.ToString = "0" Then
                    If CheckDuplicateMasterSeries(objICMasterSeries) = False Then
                        objICMasterSeries.CreateBy = Me.UserId
                        objICMasterSeries.CreateDate = Date.Now
                        objICMasterSeries.Creater = Me.UserId
                        objICMasterSeries.CreationDate = Date.Now
                        SavedMasterSeriesID = ICMasterSeriesController.AddMasterSeries(objICMasterSeries, False, Me.UserId.ToString, Me.UserInfo.Username.ToString, "")
                        If Not SavedMasterSeriesID = 0 Then
                            Dim i As Long = Nothing
                            Dim SeriesStarFrom As Long = CLng(txtStartsFrom.Text)
                            Dim NoOfLeaves As Long = CLng(txtEndsAt.Text)
                            Dim MasterSeriesEndsAt As Long = CLng(txtEndsAt.Text)
                            Dim k As Long = SeriesStarFrom
                            'Dim h As Long = CLng((txtStartsFrom.Text) + (txtEndsAt.Text))
                            For i = SeriesStarFrom To MasterSeriesEndsAt
                                objICInstrument.MasterSeriesID = SavedMasterSeriesID
                                objICInstrument.InstrumentNumber = CLng(i)
                                objICInstrument.SubSetID = Nothing
                                objICInstrument.OfficeID = Nothing
                                objICInstrument.IsUSed = Nothing
                                objICInstrument.CreatedOn = Date.Now
                                objICInstrument.CreatedBy = Me.UserId
                                objICInstrument.CreationDate = Date.Now
                                objICInstrument.Creater = Me.UserId
                                ICInstrumentController.AddInstrumentSeriesFromMasterSeries(objICInstrument, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            Next
                        End If
                        UIUtilities.ShowDialog(Me, "Add Master Series", "Master series added successfully", ICBO.IC.Dialogmessagetype.Success)
                        clear()
                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Duplicate Master Series is not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                        clear()
                        Exit Sub
                    End If
                Else
                    objICMasterSeries.MasterSeriesID = MasterSeriesID
                    If CheckDuplicateMasterSeriesOnUpDate(objICMasterSeries) = True Then
                        UIUtilities.ShowDialog(Me, "Error", "Duplicate Master Series is not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    If objICMasterSeriesUpdate.LoadByPrimaryKey(MasterSeriesID) Then
                        objICMasterSeries.MasterSeriesID = objICMasterSeriesUpdate.MasterSeriesID
                        objICMasterSeries.CreateDate = Date.Now
                        objICMasterSeries.CreateBy = Me.UserId
                        Dim TopOrder As Long = Nothing
                        Dim AvailableLeaves As Integer = Nothing
                        Dim LastInstrumentNo As Integer = Nothing
                        Dim i As Integer = 0
                        TopOrder = CLng(CLng(objICMasterSeriesUpdate.EndsAt - objICMasterSeriesUpdate.StartFrom + 1) - CLng(txtNoOfLeaves.Text))
                        If CLng(txtNoOfLeaves.Text) < (CLng(CLng(objICMasterSeriesUpdate.EndsAt) - CLng(objICMasterSeriesUpdate.StartFrom) + 1)) Then
                            If CheckDuplicateMasterSeriesOnAddingLeaves(objICMasterSeries) = False Then
                                If GetTopAssignedInstrumentNumbersToPrintLocationsByMSID(MasterSeriesID, TopOrder) = True Then
                                    StrAction = Nothing
                                    StrAction = "Master series with ID [ " & objICMasterSeries.MasterSeriesID & " ] for account number [ " & objICMasterSeries.AccountNumber & " ] updated."
                                    StrAction += "Master series start from [ " & objICMasterSeriesUpdate.StartFrom & " ] to [ " & objICMasterSeries.StartFrom & " ] ."
                                    StrAction += "Master series ends at from [ " & objICMasterSeriesUpdate.EndsAt & " ] to [ " & objICMasterSeries.EndsAt & " ]."

                                    ICMasterSeriesController.AddMasterSeries(objICMasterSeries, True, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction)
                                    ICInstrumentController.DeleteUpdatedInstrumentSeriesByMasterSeriesID(objICMasterSeries.MasterSeriesID, Me.UserId.ToString, Me.UserInfo.Username.ToString, TopOrder)
                                    UIUtilities.ShowDialog(Me, "Add Master Series", "Master series updated successfully", ICBO.IC.Dialogmessagetype.Success)
                                    clear()
                                Else
                                    UIUtilities.ShowDialog(Me, "Error", "Required number of leaves are not available.", ICBO.IC.Dialogmessagetype.Failure)
                                    Exit Sub
                                End If
                            Else
                                UIUtilities.ShowDialog(Me, "Error", "Duplicate Master Series is not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If

                        ElseIf CLng(txtNoOfLeaves.Text) > (CLng(CLng(objICMasterSeriesUpdate.EndsAt) - CLng(objICMasterSeriesUpdate.StartFrom) + 1)) Then
                            If CheckDuplicateMasterSeriesOnAddingLeaves(objICMasterSeries) = False Then
                                StrAction = Nothing
                                StrAction = "Master series with ID [ " & objICMasterSeries.MasterSeriesID & " ] for account number [ " & objICMasterSeries.AccountNumber & " ] updated."
                                StrAction += "Master series start from [ " & objICMasterSeriesUpdate.StartFrom & " ] to [ " & objICMasterSeries.StartFrom & " ] ."
                                StrAction += "Master series ends at from [ " & objICMasterSeriesUpdate.EndsAt & " ] to [ " & objICMasterSeries.EndsAt & " ]."
                                ICMasterSeriesController.AddMasterSeries(objICMasterSeries, True, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction)
                                LastInstrumentNo = (ICInstrumentController.GetLastInstrumentOFMasterSeries(objICMasterSeriesUpdate.MasterSeriesID) + 1)
                                For i = LastInstrumentNo To txtEndsAt.Text
                                    objICInstrument.MasterSeriesID = objICMasterSeries.MasterSeriesID
                                    objICInstrument.InstrumentNumber = CLng(i)
                                    objICInstrument.SubSetID = Nothing
                                    objICInstrument.OfficeID = Nothing
                                    objICInstrument.IsUSed = Nothing
                                    objICInstrument.CreatedOn = Date.Now
                                    objICInstrument.CreatedBy = Me.UserId
                                    ICInstrumentController.AddInstrumentSeriesFromMasterSeries(objICInstrument, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                Next
                                UIUtilities.ShowDialog(Me, "Add Master Series", "Master series updated successfully", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                                clear()
                            Else

                                UIUtilities.ShowDialog(Me, "Error", "Duplicate Master Series is not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Equal no of leaves", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    End If

                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub
    Public Shared Function GetTopAssignedInstrumentNumbersToPrintLocationsByMSID(ByVal MasterSeriesID As String, ByVal TopOrder As Integer) As Boolean
        Dim qryObjICInstruments As New ICInstrumentsQuery("qryObjICInstruments")
        Dim Result As Boolean = False
        Dim dt As New DataTable
        Dim dtFinal As New DataTable
        Dim drFinal As DataRow()
        qryObjICInstruments.Select(qryObjICInstruments.InstrumentNumber, qryObjICInstruments.OfficeID)
        qryObjICInstruments.Where(qryObjICInstruments.MasterSeriesID = MasterSeriesID)
        qryObjICInstruments.OrderBy(qryObjICInstruments.InstrumentNumber.Descending)
        qryObjICInstruments.es.Top = TopOrder
        dt = qryObjICInstruments.LoadDataTable()
        If dt.Rows.Count > 0 Then
            drFinal = dt.Select("OfficeID IS NULL")
            dtFinal = dt.Clone
            For Each drFinal2 As DataRow In drFinal
                dtFinal.ImportRow(drFinal2)
            Next
            If dtFinal.Rows.Count >= TopOrder Then
                Result = True
            End If

        End If
        Return Result
    End Function
    Private Sub clear()
        txtStartsFrom.Text = ""
        txtEndsAt.Text = ""
        'chkIsPrePrinted.Checked = False
        txtNoOfLeaves.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Response.Redirect(NavigateURL(), False)
        Response.Redirect(NavigateURL("viewMasterSeries", "&mid=" & Me.ModuleId & "&AccountNumber=" & AccntNumber & "&BranchCode=" & BranchCode & "&Currency=" & Currency), False)

    End Sub
    Private Function CheckDuplicateMasterSeries(ByVal objICMasterSeries As ICMasterSeries) As Boolean
        Dim Result As Boolean = False

        Dim objICMSeriesColl As New ICMasterSeriesCollection
        Dim objICInstrumentColl As New ICInstrumentsCollection
        objICInstrumentColl.es.Connection.CommandTimeout = 3600
        objICMSeriesColl.es.Connection.CommandTimeout = 3600

        objICMSeriesColl.LoadAll()
        objICMSeriesColl.Query.Where(objICMSeriesColl.Query.StartFrom = objICMasterSeries.StartFrom Or objICMSeriesColl.Query.EndsAt = objICMasterSeries.EndsAt)
        objICMSeriesColl.Query.Load()
        If objICMSeriesColl.Query.Load Then
            Result = True
        End If
        If Result = False Then

            objICInstrumentColl.Query.Where(objICInstrumentColl.Query.InstrumentNumber.Between(objICMasterSeries.StartFrom, objICMasterSeries.EndsAt))
            If objICInstrumentColl.Query.Load Then
                Result = True
            End If
            'objICMSeriesColl.LoadAll()
            'objICMSeriesColl.Query.Where(objICMSeriesColl.Query.StartFrom.Between(objICMasterSeries.StartFrom, objICMasterSeries.EndsAt) Or objICMSeriesColl.Query.EndsAt.Between(objICMasterSeries.StartFrom, objICMasterSeries.EndsAt))
            'If objICMSeriesColl.Query.Load Then
            '    Result = True
            'End If
        End If
        'If Result = False Then
        '    objICInstrumentColl.LoadAll()
        '    objICInstrumentColl.Query.Where(objICInstrumentColl.Query.InstrumentNumber.Between(objICMasterSeries.StartFrom, objICMasterSeries.EndsAt))
        '    If objICInstrumentColl.Query.Load Then
        '        Result = True
        '    End If
        'End If
        Return Result
    End Function
    Private Function CheckDuplicateMasterSeriesOnUpDate(ByVal objICMasterSeries As ICMasterSeries) As Boolean
        Dim Result As Boolean = False

        Dim objICInstrumentsColl As New ICInstrumentsCollection
        objICInstrumentsColl.es.Connection.CommandTimeout = 3600

        objICInstrumentsColl.LoadAll()
        objICInstrumentsColl.Query.Where(objICInstrumentsColl.Query.InstrumentNumber.Between(objICMasterSeries.StartFrom, objICMasterSeries.EndsAt))
        objICInstrumentsColl.Query.Where(objICInstrumentsColl.Query.MasterSeriesID <> objICMasterSeries.MasterSeriesID)
        objICInstrumentsColl.Query.Load()
        If objICInstrumentsColl.Query.Load Then
            Result = True
        End If

        Return Result
    End Function
    Private Function CheckDuplicateMasterSeriesOnAddingLeaves(ByVal objICMasterSeries As ICMasterSeries) As Boolean
        Dim Result As Boolean = False

        Dim objICMSeriesColl As New ICMasterSeriesCollection
        Dim objICMSeriesCollSecond As New ICMasterSeriesCollection
        objICMSeriesCollSecond.es.Connection.CommandTimeout = 3600
        objICMSeriesColl.es.Connection.CommandTimeout = 3600

        objICMSeriesColl.LoadAll()
        objICMSeriesColl.Query.Where(objICMSeriesColl.Query.StartFrom.Between(objICMasterSeries.StartFrom, objICMasterSeries.EndsAt) Or objICMSeriesColl.Query.EndsAt.Between(objICMasterSeries.StartFrom, objICMasterSeries.EndsAt))
        objICMSeriesColl.Query.Where(objICMSeriesColl.Query.MasterSeriesID <> objICMasterSeries.MasterSeriesID)
        objICMSeriesColl.Query.Load()
        If objICMSeriesColl.Query.Load Then
            Result = True
        End If

        Return Result
    End Function

    Protected Sub txtNoOfLeaves_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNoOfLeaves.TextChanged
        Try
            'TextNoOfLeavesChange = True
            'If TextSeriesStartFromChange = False Then
            If txtNoOfLeaves.Text.ToString() <> "" And txtNoOfLeaves.Text.ToString() <> "Enter No Of Leaves" Then
                If txtStartsFrom.Text.ToString() <> "" And Not txtStartsFrom.Text.ToString() = "Enter Start Series" Then
                    Dim SeriesStartfrom As Long = Nothing
                    Dim LeavesOfSeries As Integer = Nothing
                    Dim SeriesEndAt As Long = Nothing
                    SeriesStartfrom = CLng(txtStartsFrom.Text)
                    LeavesOfSeries = CInt(txtNoOfLeaves.Text)
                    SeriesEndAt = (SeriesStartfrom + LeavesOfSeries) - 1
                    txtEndsAt.Text = SeriesEndAt
                Else
                    txtEndsAt.Text = Nothing
                End If
              

                'Else
                '    txtEndsAt.Text = ""
                '    txtStartsFrom.Text = ""
                '    txtNoOfLeaves.Text = ""
            Else
                txtEndsAt.Text = Nothing
            End If
            'End If
            'TextSeriesStartFromChange = False
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub txtStartsFrom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStartsFrom.TextChanged
        Try
            TextSeriesStartFromChange = True
            'If TextNoOfLeavesChange = False Then
            If txtStartsFrom.Text.ToString() <> "" And txtStartsFrom.Text.ToString() <> "Enter Start Series" Then
                If txtNoOfLeaves.Text.ToString() <> "" And txtNoOfLeaves.Text.ToString() <> "Enter No Of Leaves" Then
                    Dim SeriesStartfrom As Long = Nothing
                    Dim LeavesOfSeries As Integer = Nothing
                    Dim SeriesEndAt As Long = Nothing
                    SeriesStartfrom = CLng(txtStartsFrom.Text)
                    LeavesOfSeries = CInt(txtNoOfLeaves.Text)
                    SeriesEndAt = (SeriesStartfrom + LeavesOfSeries) - 1
                    txtEndsAt.Text = SeriesEndAt
                    'Else
                    '    UIUtilities.ShowDialog(Me, "Error", "Please enter No of leaves in double figure.", ICBO.IC.Dialogmessagetype.Failure)
                    '    Exit Sub
                Else
                    txtEndsAt.Text = Nothing
                End If
                'Else
                '    txtEndsAt.Text = ""
                '    'txtStartsFrom.Text = ""
                '    txtNoOfLeaves.Text = ""
            Else
                txtEndsAt.Text = Nothing
            End If
            'End If
            'TextNoOfLeavesChange = False
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
