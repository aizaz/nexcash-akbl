﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_AccountsManagement_SaveAccount
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private AccntNumber As String
    Private AccountNumber As String
    Private BranchCode As String
    Private Currency As String
    Private TaggesUserID As String
    Private CompanyCode As String
    Private htRights As Hashtable
#Region "Page Load"

    'Add code for taggedclientuserforaccount by JAWWAD 13-03-2014

    ' Addition code for [4 eye principle] by Farhan Hassan 07-04-2015
    Protected Sub DesktopModules_AccountsManagement_SaveAccount_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            AccntNumber = Request.QueryString("AccountNumber").ToString()
            BranchCode = Request.QueryString("BranchCode").ToString()
            Currency = Request.QueryString("Currency").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlCompany()
                LoadClientUser(False)



                If AccntNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" Then

                    Clear()
                    '' Checking IsApproved


                    lblIsApproved.Visible = False
                    chkApproved.Visible = False
                    btnApprove.Visible = False


                    lblPageHeader.Text = "Add Account"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                    lblFetchedAccountTitle.Visible = False
                    txtFetchedAccountTitle.Visible = False
                    chkUpDateFetchedAccountTitle.Visible = False
                    gvClientUser.Visible = False
                    btnDeletePdc.Visible = False
                    lblUpdateFetchTitle.Visible = False
                    'chklstClientUser.Visible = False
                    lblClientUser.Visible = False
                    Label3.Visible = False
                    pnlClientUser.Visible = False
                Else
                    'lblIsApproved.Visible = CBool(htRights("Approve"))
                    'chkApproved.Visible = CBool(htRights("Approve"))
                    'btnApprove.Visible = CBool(htRights("Approve"))

                    lblIsApproved.Visible = CBool(htRights("Approve"))
                    chkApproved.Visible = CBool(htRights("Approve"))
                    btnApprove.Visible = CBool(htRights("Approve"))

                    ''=======================================================

                    lblPageHeader.Text = "Edit Account"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    txtAccountNumber.ReadOnly = True
                    txtAccountTitle.ReadOnly = True
                    Dim objICAccounts As New ICAccounts
                    objICAccounts.es.Connection.CommandTimeout = 3600
                    objICAccounts.LoadByPrimaryKey(AccntNumber, BranchCode, Currency)

                    If objICAccounts.LoadByPrimaryKey(AccntNumber, BranchCode, Currency) Then
                        txtAccountTitle.Text = objICAccounts.AccountTitle.ToString()
                        txtAccountNumber.Text = objICAccounts.AccountNumber.ToString()
                        txtAccountCurrency.Text = objICAccounts.Currency
                        txtCustomerSWFTCode.Text = objICAccounts.CustomerSwiftCode

                        If objICAccounts.IsApproved = CBool(htRights("Approve")) Then
                            chkApproved.Enabled = False
                            chkApproved.Checked = CBool(htRights("Approve"))
                            btnApprove.Visible = False
                        End If





                        Dim objICCompany As New ICCompany
                        Dim objICGroup As New ICGroup





                        objICCompany.es.Connection.CommandTimeout = 3600
                        objICGroup.es.Connection.CommandTimeout = 3600


                        objICCompany = objICAccounts.UpToICCompanyByCompanyCode()
                        objICGroup = objICCompany.UpToICGroupByGroupCode()
                        LoadddlGroup()
                        ddlGroup.SelectedValue = objICGroup.GroupCode.ToString()
                        LoadddlCompany()
                        'ddlGroup.Enabled = False
                        'ddlCompany.Enabled = False
                        ddlCompany.SelectedValue = objICCompany.CompanyCode.ToString()
                        txtBranchCode.Text = objICAccounts.BranchCode
                        chkActive.Checked = objICAccounts.IsActive
                        'chkApproved.Checked = ICAccount.IsApproved
                        chkUpDateFetchedAccountTitle.Visible = True
                        txtFetchedAccountTitle.Visible = True
                        lblFetchedAccountTitle.Visible = True
                        lblUpdateFetchTitle.Visible = True

                        txtFetchedAccountTitle.Text = (CBUtilities.TitleFetch(objICAccounts.AccountNumber.ToString, Nothing, "Client Account Title", objICAccounts.AccountNumber.ToString)).ToString.Split(";")(0)



                        'Code for taggedclientuser for account
                        If Not objICAccounts.DailyBalanceAllow Is Nothing Then
                            If objICAccounts.DailyBalanceAllow = True Then
                                chkselectclientuser.Checked = True
                                LoadClientUser(True)
                                LoadgvTaggedAccountUser(True)
                                'chkselectclientuser.Checked = True
                                ''function to load grid
                                ''user already tagged will not be loaded in user check box list
                            End If

                        End If


                        If chklstClientUser.Items.Count = 0 Then
                            lblClientUser.Visible = False
                            Label3.Visible = False
                        End If

                        'If ddlGroup.SelectedIndex <> -1 Then
                        '    ClearonUpdate()

                        'End If


                        'If objICTaggedClientUserForAccount.LoadByPrimaryKey(TaggesUserID) Then
                        'End If

                    End If
                    'btnSave.Visible = ArrRights(1)

                End If
            End If


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client Accounts Management")
            ViewState("htRights") = htRights

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#Region "Button Events"

    'Add code for taggedclientuserforaccount by JAWWAD 13-03-2014
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then

            Try
                Dim objICAccounts As New ICAccounts
                Dim objICCompany As New ICCompany
                Dim objICPreviousCompany As New ICCompany
                Dim objICTaggedClientUserForAccount As New ICTaggedClientUserForAccount
                Dim objICAccountsFrom As New ICAccounts
                Dim StrAction As String = ""
                Dim ICTaggedClientUserAccount As New ICTaggedClientUserForAccount()
                Dim objICgrp As New ICGroup
                Dim StrActionForCompany As String = Nothing
                objICAccounts.es.Connection.CommandTimeout = 3600
                objICAccountsFrom.es.Connection.CommandTimeout = 3600
                objICCompany.es.Connection.CommandTimeout = 3600
                objICPreviousCompany.es.Connection.CommandTimeout = 3600
                objICAccounts.AccountTitle = txtAccountTitle.Text.ToString()
                objICAccounts.AccountNumber = txtAccountNumber.Text.Trim.ToString()
                objICAccounts.Currency = txtAccountCurrency.Text.ToString()
                objICAccounts.BranchCode = txtBranchCode.Text.ToString()
                objICAccounts.CompanyCode = ddlCompany.SelectedValue.ToString()



                objICAccounts.CustomerSwiftCode = txtCustomerSWFTCode.Text
                objICAccounts.IsActive = chkActive.Checked
                If chkselectclientuser.Checked = True Then
                    objICAccounts.DailyBalanceAllow = True
                Else
                    objICAccounts.DailyBalanceAllow = False
                End If

                ''' Changes made by Farhan Hassan 10-04-15

                If chkselectclientuser.Checked = True Then
                    If chklstClientUser.Items.Count = 0 Then
                        UIUtilities.ShowDialog(Me, "Save Account", "User Not Tagged.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                If AccntNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" Then
                    objICAccounts.CreateBy = Me.UserId
                    objICAccounts.CreateDate = Date.Now
                    objICAccounts.Creater = Me.UserId
                    objICAccounts.CreationDate = Date.Now

                    If CheckDuplicate(objICAccounts) = False Then
                        ICAccountsController.AddAccounts(objICAccounts, objICAccountsFrom, False, Me.UserId.ToString, Me.UserInfo.Username.ToString, "")
                        'Addition code for taggedclientuserforaccount table.
                        'its another table for client user selection and sending sms and email.

                        For Each lit As ListItem In chklstClientUser.Items
                            If lit.Selected = True Then
                                objICTaggedClientUserForAccount = New ICTaggedClientUserForAccount
                                objICTaggedClientUserForAccount.AccountNumber = objICAccounts.AccountNumber
                                objICTaggedClientUserForAccount.BranchCode = objICAccounts.BranchCode
                                objICTaggedClientUserForAccount.Currency = objICAccounts.Currency
                                objICTaggedClientUserForAccount.IsActive = True
                                objICTaggedClientUserForAccount.UserID = lit.Value
                                objICTaggedClientUserForAccount.CreateBy = Me.UserId
                                objICTaggedClientUserForAccount.CreateDate = Date.Now
                                objICTaggedClientUserForAccount.Creater = Me.UserId
                                objICTaggedClientUserForAccount.CreationDate = Date.Now



                                ICTaggedClientUserForAccountController.AddTaggedClientUserForAccount(objICTaggedClientUserForAccount, False, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)


                            End If

                        Next



                        UIUtilities.ShowDialog(Me, "Save Account", "Account added successfully.", ICBO.IC.Dialogmessagetype.Success)
                        Clear()
                    Else
                        UIUtilities.ShowDialog(Me, "Save Account", "Can not add duplicate Account.", ICBO.IC.Dialogmessagetype.Failure)
                        'Exit Sub
                    End If


                    ''  If Updated / Edit
                Else
                    objICAccounts.CreateBy = Me.UserId
                    objICAccounts.CreateDate = Date.Now
                    objICAccountsFrom.LoadByPrimaryKey(AccntNumber, BranchCode, Currency)
                    objICCompany.LoadByPrimaryKey(ddlCompany.SelectedValue.ToString)
                    objICPreviousCompany.LoadByPrimaryKey(objICAccountsFrom.CompanyCode.ToString)

                    objICCompany.IsApprove = False

                    objICCompany.CreatedBy = Me.UserId
                    objICCompany.CreatedDate = Date.Now
                    StrAction += "Account updated from " & objICAccountsFrom.AccountNumber & " to " & objICAccounts.AccountNumber & " ; "
                    StrAction += "Branch Code from " & objICAccountsFrom.BranchCode & " to " & objICAccounts.BranchCode & " ; "
                    StrAction += "Currency from " & objICAccountsFrom.Currency & " to " & objICAccounts.Currency & " ; "
                    StrAction += "Account Title from " & objICAccountsFrom.AccountTitle & " to " & objICAccounts.AccountTitle & " ; "
                    StrAction += "Customer Swift Code from " & objICAccountsFrom.CustomerSwiftCode & " to " & objICAccounts.CustomerSwiftCode & " ; "
                    StrAction += "Company from " & objICAccountsFrom.UpToICCompanyByCompanyCode.CompanyName & " to " & objICAccounts.UpToICCompanyByCompanyCode.CompanyName & " ; "
                    StrAction += "Status from " & objICAccountsFrom.IsActive & " to " & objICAccounts.IsActive & " ; "
                    If chkUpDateFetchedAccountTitle.Checked = True Then
                        objICAccounts.AccountTitle = txtFetchedAccountTitle.Text.ToString
                    End If

                    If objICPreviousCompany.CompanyCode <> objICAccounts.CompanyCode Then
                        If objICAccountsFrom.DailyBalanceAllow = True Then
                            ''delete user
                            ICTaggedClientUserForAccountController.DeleteAlreadyTaggedClientUserinGVonddselectindexchange(AccntNumber, BranchCode, Currency, objICPreviousCompany.CompanyCode, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        End If
                    Else

                        If objICAccounts.DailyBalanceAllow = False Then
                            ICTaggedClientUserForAccountController.DeleteAlreadyTaggedClientUserinGVonddselectindexchange(AccntNumber, BranchCode, Currency, objICPreviousCompany.CompanyCode, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        End If
                    End If


                    ICAccountsController.AddAccounts(objICAccounts, objICAccountsFrom, True, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction)

                    For Each lit As ListItem In chklstClientUser.Items
                        If lit.Selected = True Then
                            objICTaggedClientUserForAccount = New ICTaggedClientUserForAccount
                            objICTaggedClientUserForAccount.AccountNumber = objICAccounts.AccountNumber
                            objICTaggedClientUserForAccount.BranchCode = objICAccounts.BranchCode
                            objICTaggedClientUserForAccount.Currency = objICAccounts.Currency
                            objICTaggedClientUserForAccount.IsActive = True
                            objICTaggedClientUserForAccount.UserID = lit.Value
                            objICTaggedClientUserForAccount.CreateBy = Me.UserId
                            objICTaggedClientUserForAccount.CreateDate = Date.Now
                            ICTaggedClientUserForAccountController.AddTaggedClientUserForAccount(objICTaggedClientUserForAccount, False, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                        End If


                    Next


                    UIUtilities.ShowDialog(Me, "Save Account", "Account updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                End If
                txtAccountNumber.Text = ""
                LoadClientUser(True)
            Catch ex As Exception
                If ex.Message.Contains("Incorrect syntax near the keyword") Then
                    UIUtilities.ShowDialog(Me, "Save Account", "Can not add duplicate Account.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)


        'ICTaggedClientUserForAccountController.SendDailyAccountBalanceToTaggedClientUser()
    End Sub

#End Region

#Region "Drop Down Events"
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub



    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        LoadddlCompany()
        If ddlGroup.SelectedValue.ToString = "0" Then
            If AccntNumber = "0" Then
                LoadClientUser(False)
            Else
                LoadClientUser(True)
            End If
            LoadgvTaggedAccountUser(True)
        Else
            chklstClientUser.DataSource = Nothing
            chklstClientUser.DataBind()
            chklstClientUser.Visible = False
            gvClientUser.DataSource = Nothing
            gvClientUser.DataBind()
            gvClientUser.Visible = False
            ClearonUpdate(True)
        End If


        'txtAccountNumber.Text = ""
        'txtAccountTitle.Text = ""
        'txtBranchCode.Text = ""
        'txtAccountCurrency.Text = ""
    End Sub
#End Region

#Region "Other Functions/Routines"

    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return Result
        End Try
    End Function
    Private Sub Clear()
        txtAccountTitle.Text = ""
        'txtAccountNumber.Text = ""
        txtAccountCurrency.Text = ""
        ddlGroup.ClearSelection()
        ddlCompany.ClearSelection()
        txtBranchCode.Text = ""
        txtFetchedAccountTitle.Text = ""
        chkUpDateFetchedAccountTitle.Checked = False
        chkActive.Checked = True
        chkselectclientuser.Checked = False
        chklstClientUser.Items.Clear()
        lblClientUser.Visible = False
        Label3.Visible = False
        txtCustomerSWFTCode.Text = ""

    End Sub

    Private Function CheckDuplicate(ByVal ICAccount As ICAccounts) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim objICAccounts As New ICAccounts
            Dim objICAccountsColl As New ICAccountsCollection

            objICAccounts.es.Connection.CommandTimeout = 3600
            objICAccountsColl.es.Connection.CommandTimeout = 3600

            If objICAccounts.LoadByPrimaryKey(ICAccount.AccountNumber, ICAccount.BranchCode, ICAccount.Currency) = True Then
                rslt = True
            End If

            objICAccountsColl.LoadAll()
            objICAccountsColl.Query.Where(objICAccountsColl.Query.AccountNumber = ICAccount.AccountNumber.ToString())
            If objICAccountsColl.Query.Load() Then
                rslt = True
            End If

            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

#End Region

    Protected Sub txtAccountNumber_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAccountNumber.TextChanged
        Try
            Dim StrAccountDetails As String()
            Dim AccountStatus As String = ""
            Dim RestraintsDetails As String = ""
            Dim BBAccNo As String = Nothing
            If Not txtAccountNumber.Text.ToString.Trim = "" And Not txtAccountNumber.Text.ToString = "Enter Account Number" Then
                If CBUtilities.IsNormalAccountNoOrIBAN(txtAccountNumber.Text.ToString) = True Then
                    If CBUtilities.ValidateIBAN(txtAccountNumber.Text.ToString) = True Then
                        BBAccNo = CBUtilities.GetBBANFromIBAN(txtAccountNumber.Text.ToString)
                        Try
                            AccountStatus = CBUtilities.AccountStatus(BBAccNo, ICBO.CBUtilities.AccountType.RB, "Client Account Status", txtAccountNumber.ToString, "Status", "Add", txtBranchCode.Text).ToString
                        Catch ex As Exception
                            txtAccountTitle.Text = ""
                            txtAccountCurrency.Text = ""
                            txtBranchCode.Text = ""
                            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End Try
                        Try
                            StrAccountDetails = (CBUtilities.TitleFetch(BBAccNo, ICBO.CBUtilities.AccountType.RB, "Client Account Title", txtAccountNumber.Text.ToString).ToString.Split(";"))
                            If AccountStatus = "Active" Then
                                txtAccountTitle.Text = StrAccountDetails(0).ToString
                                txtAccountCurrency.Text = StrAccountDetails(2).ToString
                                txtBranchCode.Text = StrAccountDetails(1).ToString
                            Else
                                If AccountStatus.Contains("-") = True Then
                                    RestraintsDetails = AccountStatus.Split("-")(1)
                                    AccountStatus = AccountStatus.Split("-")(0)
                                    UIUtilities.ShowDialog(Me, "Warning", AccountStatus.ToString & "<br />" & RestraintsDetails, ICBO.IC.Dialogmessagetype.Warning)
                                Else
                                    UIUtilities.ShowDialog(Me, "Warning", AccountStatus.ToString, ICBO.IC.Dialogmessagetype.Warning)
                                End If
                                txtAccountTitle.Text = StrAccountDetails(0).ToString
                                txtAccountCurrency.Text = StrAccountDetails(2).ToString
                                txtBranchCode.Text = StrAccountDetails(1).ToString
                            End If
                        Catch ex As Exception
                            txtAccountTitle.Text = ""
                            txtAccountCurrency.Text = ""
                            txtBranchCode.Text = ""
                            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End Try
                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Invalid IBAN", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else
                    Try
                        AccountStatus = CBUtilities.AccountStatus(txtAccountNumber.Text.ToString, ICBO.CBUtilities.AccountType.RB, "Client Account Status", txtAccountNumber.ToString, "Status", "Add", txtBranchCode.Text).ToString
                    Catch ex As Exception
                        txtAccountTitle.Text = ""
                        txtAccountCurrency.Text = ""
                        txtBranchCode.Text = ""
                        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End Try
                    Try
                        StrAccountDetails = (CBUtilities.TitleFetch(txtAccountNumber.Text.ToString, ICBO.CBUtilities.AccountType.RB, "Client Account Title", txtAccountNumber.Text.ToString).ToString.Split(";"))
                        If AccountStatus = "Active" Then
                            txtAccountTitle.Text = StrAccountDetails(0).ToString
                            txtAccountCurrency.Text = StrAccountDetails(2).ToString
                            txtBranchCode.Text = StrAccountDetails(1).ToString
                        Else
                            If AccountStatus.Contains("-") = True Then
                                RestraintsDetails = AccountStatus.Split("-")(1)
                                AccountStatus = AccountStatus.Split("-")(0)

                                UIUtilities.ShowDialog(Me, "Warning", AccountStatus.ToString & "<br />" & RestraintsDetails, ICBO.IC.Dialogmessagetype.Warning)
                            Else
                                UIUtilities.ShowDialog(Me, "Warning", AccountStatus.ToString, ICBO.IC.Dialogmessagetype.Warning)
                            End If
                            txtAccountTitle.Text = StrAccountDetails(0).ToString
                            txtAccountCurrency.Text = StrAccountDetails(2).ToString
                            txtBranchCode.Text = StrAccountDetails(1).ToString
                        End If
                    Catch ex As Exception
                        txtAccountTitle.Text = ""
                        txtAccountCurrency.Text = ""
                        txtBranchCode.Text = ""
                        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End Try
                End If


            Else

                'UIUtilities.ShowDialog(Me, "Error", "Please enter valid account number.", ICBO.IC.Dialogmessagetype.Failure)
                txtAccountNumber.Text = ""
                txtAccountCurrency.Text = ""
                txtAccountTitle.Text = ""
                txtBranchCode.Text = ""
                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged

        'If AccntNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" Then
        '    ClearonUpdate(False)
        '    LoadClientUser(False)
        'Else
        '    ClearonUpdate(True)
        '    LoadClientUser(True)

        'End If
        If ddlCompany.SelectedValue.ToString = "0" Then
            If AccntNumber = "0" Then
                LoadClientUser(False)
            Else
                LoadClientUser(True)
            End If
            LoadgvTaggedAccountUser(True)
        Else
            chklstClientUser.DataSource = Nothing
            chklstClientUser.DataBind()
            chklstClientUser.Visible = False
            gvClientUser.DataSource = Nothing
            gvClientUser.DataBind()
            gvClientUser.Visible = False
            ClearonUpdate(True)
        End If





        'txtAccountNumber.Text = ""
        'txtAccountTitle.Text = ""
        'txtBranchCode.Text = ""
        'txtAccountCurrency.Text = ""
    End Sub

    Private Sub ClearonUpdate(ByVal IsUpdate As Boolean)
        chkselectclientuser.Checked = False
        chklstClientUser.Visible = False
        pnlClientUser.Visible = False
        lblClientUser.Visible = False
        Label3.Visible = False
        'Me.gvClientUser.SelectedIndexes.Clear()
        'gvClientUser.Enabled = False
        gvClientUser.Visible = False
        btnDeletePdc.Visible = False
    End Sub


    'created for USER TAGGING SECTION WITH ACCOUNTS By JAWWAD 12-03-2014
#Region "USER TAGGING SECTION"

    'Created to load client user in Checklist
    Private Sub LoadClientUser(ByVal IsUpdate As Boolean)
        Try
            Dim AccountNo, BranchCode, Currency As String
            AccountNo = Nothing
            BranchCode = Nothing
            Currency = Nothing
            If txtAccountNumber.Text <> "" Then
                AccountNo = txtAccountNumber.Text
            End If
            If txtBranchCode.Text <> "" Then
                BranchCode = txtBranchCode.Text
            End If
            If txtAccountCurrency.Text <> "" Then
                Currency = txtAccountCurrency.Text
            End If
            chklstClientUser.DataSource = ICUserController.GetAllActiveAndApproveUsersByCompanyCodeForTaggingAccountBalance(ddlCompany.SelectedValue.ToString, IsUpdate, AccountNo, BranchCode, Currency)
            chklstClientUser.DataTextField = "UserName"
            chklstClientUser.DataValueField = "UserID"
            chklstClientUser.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub chkselectclientuser_CheckedChanged(sender As Object, e As EventArgs) Handles chkselectclientuser.CheckedChanged
        Dim objICAccounts As New ICAccounts
        'If AccntNumber <> "0" Then
        '    If gvClientUser.Items.Count > 0 Then
        '        btnDeletePdc.Visible = True
        '        If chkselectclientuser.Checked = True Then
        '            btnDeletePdc.Visible = True
        '        Else
        '            btnDeletePdc.Visible = False
        '        End If
        '    Else
        '        btnDeletePdc.Visible = False
        '    End If
        'End If


        If chkselectclientuser.Checked = True Then
            chklstClientUser.Visible = True
            lblClientUser.Visible = True
            Label3.Visible = True
            pnlClientUser.Visible = True

            If AccntNumber = "0" Then
                LoadClientUser(False)
            Else
                LoadClientUser(True)
            End If
            LoadgvTaggedAccountUser(True)
        Else
            chklstClientUser.Visible = False
            lblClientUser.Visible = False
            Label3.Visible = False
            pnlClientUser.Visible = False
            gvClientUser.DataSource = Nothing
            gvClientUser.DataBind()
            gvClientUser.Visible = False
            chklstClientUser.DataSource = Nothing
            chklstClientUser.Visible = False
            btnDeletePdc.Visible = False
        End If




    End Sub

    'Created to load gv in add page( to load selected user)
    Private Sub LoadgvTaggedAccountUser(ByVal DoDataBind As Boolean)
        Try
            Dim AccountNo, BranchCode, Currency, CompanyCode As String
            AccountNo = ""
            BranchCode = ""
            Currency = ""
            CompanyCode = ""

            If txtAccountNumber.Text <> "" Then
                AccountNo = txtAccountNumber.Text
            End If
            If txtBranchCode.Text <> "" Then
                BranchCode = txtBranchCode.Text
            End If
            If txtAccountCurrency.Text <> "" Then
                Currency = txtAccountCurrency.Text
            End If



            ICTaggedClientUserForAccountController.GetTaggedAccountUser(Me.gvClientUser.CurrentPageIndex + 1, Me.gvClientUser.PageSize, Me.gvClientUser, DoDataBind, AccountNo, BranchCode, Currency, ddlCompany.SelectedValue.ToString())
            If gvClientUser.Items.Count > 0 Then
                gvClientUser.Visible = True
                'lblRNF.Visible = False
                btnDeletePdc.Visible = CBool(htRights("Delete"))
                'btnApproveCompany.Visible = CBool(htRights("Approve"))
                gvClientUser.Columns(3).Visible = CBool(htRights("Delete"))
                CustomValidator2.Enabled = False
            Else
                gvClientUser.Visible = False
                'lblRNF.Visible = True
                btnDeletePdc.Visible = False
                If chkselectclientuser.Checked = True Then
                    CustomValidator2.Enabled = True
                Else
                    CustomValidator2.Enabled = False
                End If
                'btnApproveCompany.Visible = False
            End If
            'If CheckIsAdminOrSuperUser() = False Then
            '    If ArrRights.Count > 0 Then
            '        gvCompany.Columns(5).Visible = ArrRights(2)
            '    Else
            '        UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL())
            '        Exit Sub
            '    End If
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    'created for add page gv functionality
#Region "GVCLIENTUSER"

    Protected Sub gvClientUser_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles gvClientUser.ItemCommand

        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    Dim icTaggesClientUser As New ICTaggedClientUserForAccount
                    icTaggesClientUser.es.Connection.CommandTimeout = 3600
                    If icTaggesClientUser.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                        'If icPdc.IsApproved Then
                        '    UIUtilities.ShowDialog(Me, "Warning", "Country is approved and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
                        'Else
                        ICTaggedClientUserForAccountController.DeletegvTaggedClientUser(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        UIUtilities.ShowDialog(Me, "Delete User", "Client User deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                        LoadgvTaggedAccountUser(True)
                        ' End If
                    End If
                    'ElseIf e.CommandName = "ShowProvince" Then
                    '    hfCountryCode.Value = e.CommandArgument.ToString()
                    '    Dim frcCountry As New ICPdc
                    '    frcCountry.es.Connection.CommandTimeout = 3600
                    '    frcCountry.LoadByPrimaryKey(e.CommandArgument.ToString())
                    '    If frcCountry.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                    '        lblProvinceListHeader.Text = "Province List of " & frcCountry.CountryName.ToString()
                    '    Else
                    '        lblProvinceListHeader.Text = "Province List"
                    '    End If
                    '    LoadProvinces(hfCountryCode.Value.ToString(), True)
                    '    LoadCity(0, True)
                    '    lblCityListHeader.Visible = False
                    '    lblCityRNF.Visible = False
                End If
            End If
            LoadClientUser(True)
            If gvClientUser.Items.Count = 0 Then
                btnDeletePdc.Visible = False
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Delete User", "Client User delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvClientUser_ItemCreated(sender As Object, e As GridItemEventArgs) Handles gvClientUser.ItemCreated

        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvClientUser.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvClientUser_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles gvClientUser.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvClientUser.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub gvClientUser_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles gvClientUser.NeedDataSource
        LoadgvTaggedAccountUser(False)
    End Sub

    Protected Sub gvClientUser_PageIndexChanged(sender As Object, e As GridPageChangedEventArgs) Handles gvClientUser.PageIndexChanged
        gvClientUser.CurrentPageIndex = e.NewPageIndex
    End Sub

#End Region

    'created from button delete pdc below
    Private Function CheckgvPdcForProcessAll() As Boolean
        Try
            Dim rowgvPdc As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvPdc In gvClientUser.Items
                chkSelect = New CheckBox

                chkSelect = DirectCast(rowgvPdc.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    'Created to delete selected user(from button/bulk delete) from the gv in add page
    Protected Sub btnDeletePdc_Click(sender As Object, e As EventArgs) Handles btnDeletePdc.Click
        If Page.IsValid Then
            Try
                Dim rowgvPdc As GridDataItem
                Dim chkSelect As CheckBox
                Dim TaggesUserID As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0

                If CheckgvPdcForProcessAll() = True Then
                    For Each rowgvPdc In gvClientUser.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvPdc.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            TaggesUserID = rowgvPdc("TaggesUserID").Text.ToString()
                            Dim icTaggesClientUser As New ICTaggedClientUserForAccount
                            icTaggesClientUser.es.Connection.CommandTimeout = 3600
                            If icTaggesClientUser.LoadByPrimaryKey(TaggesUserID.ToString()) Then
                                'If icPdc.IsApproved Then
                                '    FailCount = FailCount + 1
                                'Else
                                Try
                                    ICTaggedClientUserForAccountController.DeletegvTaggedClientUser(TaggesUserID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                    PassCount = PassCount + 1
                                Catch ex As Exception
                                    If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                        FailCount = FailCount + 1
                                    End If
                                End Try
                                'End If
                            End If


                        End If

                    Next

                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete User", "[" & FailCount.ToString() & "] Cheques can not be deleted due to following reasones : <br /> 1. Country is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete User", "[" & PassCount.ToString() & "] Client User deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        'LoadgvTaggedAccountUser(True)
                        'LoadClientUser(True)
                        Exit Sub
                    End If

                    UIUtilities.ShowDialog(Me, "Delete User", "[" & PassCount.ToString() & "] Client User deleted successfully. [" & FailCount.ToString() & "] Countries can not be deleted due to following reasons : <br /> 1. Country is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Delete User", "Please select atleast one(1) Client User.", ICBO.IC.Dialogmessagetype.Warning)
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If



    End Sub

#End Region

    'created this function for daily send notification to tagged client user with account( checks the time of the day)
    'Public Sub SendDailyAccountBalanceToTaggedClientUser()

    '    'If (System.DateTime.Now = #12:35:00 PM#) Then

    '    '    Label4.Text = "True"
    '    'Else
    '    '    Label4.Text = "False"

    '    'End If

    '    'If TimeOfDay >= "3:13:00 PM" Then

    '    '    Label4.Text = "True"
    '    'Else
    '    '    Label4.Text = "False"

    '    'End If


    '    'Dim compareTime As String = "12:41:00 PM"

    '    'If compareTime = DateTime.Now.ToString("12:41:00 PM") Then

    '    '    Label4.Text = "True"
    '    'Else
    '    '    Label4.Text = "False"

    '    'End If



    '    'Dim myCompareTime As DateTime = DateTime.Parse("12:43:21 PM")

    '    'If myCompareTime.TimeOfDay = DateTime.Now.TimeOfDay Then

    '    '    Label4.Text = "True"
    '    'Else
    '    '    Label4.Text = "False"

    '    'End If


    '    'Dim collpdc As New IC.ICTaggedClientUserForAccountController
    '    'collpdc = ICTaggedClientUserForAccountController.SendDailyAccountBalanceToTaggedClientUser()
    '    'If collpdc Is True Then
    '    If TimeOfDay >= #4:32:00 PM# Then


    '        EmailUtilities.TaggedClientUserForAccountBalance()
    '        SMSUtilities.TaggedClientUserForAccountBalance()
    '    Else
    '        Label4.Text = " Data Not Matches"

    '    End If




    '    'If collPdc Is Nothing Then

    '    '    'MsgBox("Data Not Matches", MsgBoxStyle.Information, "Error")
    '    '    Label1.Text = "Data Not Matches"
    '    'Else
    '    '    'MsgBox("Data Matches", MsgBoxStyle.Information, "Error")
    '    '    Label1.Text = "Data Matches"

    '    'End If

    'End Sub


    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click

        Dim ObjICAccounts As New ICAccounts
        If Page.IsValid Then
            Try

                ObjICAccounts.LoadByPrimaryKey(AccntNumber, BranchCode, Currency)
                If Me.UserInfo.IsSuperUser = False Then
                    If ObjICAccounts.CreateBy = Me.UserId Then
                        UIUtilities.ShowDialog(Me, "Account Approve", "Account must be approved by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                End If
                If chkApproved.Checked Then
                    ICAccountsController.ApproveAccount(AccntNumber.ToString(), BranchCode.ToString(), Currency.ToString(), Me.UserId, Me.UserInfo.Username, chkApproved.Checked)
                    UIUtilities.ShowDialog(Me, "Approve Account", "Account approved succesfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Approve Account", "Please checked the Approved Check Box.", ICBO.IC.Dialogmessagetype.Warning)
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If


    End Sub


End Class
