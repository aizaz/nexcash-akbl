﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Partial Class DesktopModules_IBFT_RTGS_Decision_Rule_ViewIBFT_RTGSDecisionRule
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If CheckIsAdminOrSuperUser() = False Then
                ManagePageAccessRights()
            End If
            If Page.IsPostBack = False Then
                LoadddlGroup()
                LoadddlCompany()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ManagePageAccessRights()
        Try
            ArrRights = ICBankRoleRightsController.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "IBFT/RTGS Rule Management")
            If ArrRights.Count > 0 Then
                btnAddIBFTRTGSRule.Visible = ArrRights(0)
            Else
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            ' str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function
    Private Sub LoadddlCompany()
        Try
            ddlCompany.Items.Clear()
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True

            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"

            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlGroup()
        Try
            ddlCompany.Items.Clear()
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True

            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"

            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetgvIBFTRTGSRule(ByVal ComPanyCode As String, ByVal TransferType As String)
        gvIBFTRTGSRule.DataSource = ICIBFTRTGSRuleController.GetIBFTRTGSByCompanyCodeAndTransferType(ddlCompany.SelectedValue.ToString(), TransferType.ToString())
        gvIBFTRTGSRule.DataBind()
        If gvIBFTRTGSRule.Rows.Count > 0 Then
            lblNRF.Visible = False
        Else
            lblNRF.Visible = True
        End If

        If CheckIsAdminOrSuperUser() = False Then
            If ArrRights.Count > 0 Then
                gvIBFTRTGSRule.Columns(4).Visible = ArrRights(2)
            Else
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
        End If
    End Sub
    Protected Sub btnAddDisbRule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddIBFTRTGSRule.Click
        Response.Redirect(NavigateURL("SaveTransferRule", "&mid=" & Me.ModuleId & "&id=0"), False)
    End Sub
    Protected Sub gvDisbRule_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvIBFTRTGSRule.RowCommand
        Try
            If e.CommandName = "del" Then
                Dim ActionStr As String = ""
                Dim objICDisbRule As New ICIBFTRTGSRules
                objICDisbRule.es.Connection.CommandTimeout = 3600
                If objICDisbRule.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                    ICIBFTRTGSRuleController.DeleteIBFTRTGSRules(e.CommandArgument.ToString())
                    ActionStr = "IBFT/RTGS Rule Deleted :"
                    ActionStr += " Company [Code: " & ddlCompany.SelectedValue.ToString() & " ; Name: " & ddlCompany.SelectedItem.Text.ToString & "] ;"
                    ActionStr += " Transfer Type  [" & objICDisbRule.DisbType & "] ;"
                    ActionStr += " Field Name [" & objICDisbRule.FieldName & "] ;"
                    If objICDisbRule.FieldValue Is Nothing Or objICDisbRule.FieldValue = "" Then
                        ActionStr += " Condition / Operator [" & objICDisbRule.FieldCondition & "]"
                    Else
                        ActionStr += " Condition / Operator [" & objICDisbRule.FieldCondition & "] ;"
                        ActionStr += " Value [" & objICDisbRule.FieldValue & "]"
                    End If
                    'ICUtilities.AddAuditTrail(ActionStr.ToString(), "IBFT/RTGS Rule Management", objICDisbRule.IBFTRTGSRuleID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    UIUtilities.ShowDialog(Me, "IBFT/RTGS Rule Management", "IBFT/RTGS Rule deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                    SetgvIBFTRTGSRule(ddlCompany.SelectedValue.ToString(), ddlTransferType.SelectedValue.ToString())
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Error", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        ddlTransferType.SelectedValue = "0"
        SetgvIBFTRTGSRule(ddlCompany.SelectedValue.ToString(), ddlTransferType.SelectedValue.ToString())
    End Sub

    Protected Sub ddlTransferType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTransferType.SelectedIndexChanged
        SetgvIBFTRTGSRule(ddlCompany.SelectedValue.ToString(), ddlTransferType.SelectedValue.ToString())
    End Sub

    Protected Sub gvIBFTRTGSRule_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvIBFTRTGSRule.PageIndexChanging
        gvIBFTRTGSRule.PageIndex = e.NewPageIndex
        SetgvIBFTRTGSRule(ddlCompany.SelectedValue.ToString(), ddlTransferType.SelectedValue.ToString())
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        LoadddlCompany()
    End Sub
End Class
