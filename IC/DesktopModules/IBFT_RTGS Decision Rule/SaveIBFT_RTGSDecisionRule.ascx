﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveIBFT_RTGSDecisionRule.ascx.vb" Inherits="DesktopModules_IBFT_RTGS_Decision_Rule_SaveIBFT_RTGSDecisionRule" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadInputManager ID="rimValue" runat="server" Enabled="true" >
    <telerik:TextBoxSetting BehaviorID="REValue" Validation-IsRequired="true" EmptyMessage="Enter Value" ErrorMessage="Invalid Value">
        <TargetControls>
            <telerik:TargetInput ControlID="txtValue" />
        </TargetControls>
    </telerik:TextBoxSetting>
    </telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" Text="Add IBFT / RTGS Rule" 
                CssClass="headingblue"></asp:Label>
            <br />
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" colspan="2">
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblreqAgent0" runat="server" Text="*" ForeColor="Red" 
                Visible="True"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" Visible="True" 
                AutoPostBack="True" CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvAgent0" runat="server" ControlToValidate="ddlGroup"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please Select Group" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblreqAgent" runat="server" Text="*" ForeColor="Red" Visible="True"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAgentBranch" runat="server" Text="Transfer Mode" 
                CssClass="lbl"></asp:Label>
            <asp:Label ID="lblreqAgentBranch" runat="server" Text="*" ForeColor="Red" Visible="True"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" Visible="True" 
                AutoPostBack="True" CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlTransferMode" runat="server" Visible="True" 
                CssClass="dropdown">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem>IBFT</asp:ListItem>
                <asp:ListItem>RTGS</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvAgent" runat="server" ControlToValidate="ddlCompany"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please Select Company" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvAgentBranch" runat="server" ControlToValidate="ddlTransferMode"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please Select Transfer Mode"
                SetFocusOnError="True" ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblSubAgent" runat="server" Text="Field Name" CssClass="lbl" Visible="True"></asp:Label>
            <asp:Label ID="lblreqSubAgent" runat="server" Text="*" ForeColor="Red" Visible="True"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblSubAgentBranch" runat="server" Text="Operator" CssClass="lbl" Visible="True"></asp:Label>
            <asp:Label ID="lblreqSubAgentBranch" runat="server" Text="*" ForeColor="Red" Visible="True"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlFieldName" runat="server" Visible="True" 
                CssClass="dropdown">
                <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>                
                <asp:ListItem Selected="False" Value="AmountPKR">AmountPKR</asp:ListItem>                
                <asp:ListItem Selected="False" Value="BankCode">BankCode</asp:ListItem>
                <asp:ListItem Selected="False" Value="BankName">BankName</asp:ListItem>
                <asp:ListItem Selected="False" Value="BankAccountNumber">BankAccountNumber</asp:ListItem>                
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlOperator" runat="server" Visible="True" 
                CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem Selected="False" Value="Contains">Contains</asp:ListItem>
                <asp:ListItem Selected="False" Value="Equal">Equal</asp:ListItem>
                <asp:ListItem Selected="False" Value="GreaterThan">GreaterThan</asp:ListItem>
                <asp:ListItem Selected="False" Value="GreaterThanOrEqual">GreaterThanOrEqual</asp:ListItem>
                <asp:ListItem Selected="False" Value="In">In</asp:ListItem>
                <asp:ListItem Selected="False" Value="IsNotNull">IsNotNull</asp:ListItem>
                <asp:ListItem Selected="False" Value="IsNull">IsNull</asp:ListItem>
                <asp:ListItem Selected="False" Value="LessThan">LessThan</asp:ListItem>
                <asp:ListItem Selected="False" Value="LessThanOrEqual">LessThanOrEqual</asp:ListItem>
                <asp:ListItem Selected="False" Value="Like">Like</asp:ListItem>
                <asp:ListItem Selected="False" Value="NotEqual">NotEqual</asp:ListItem>
                <asp:ListItem Selected="False" Value="NotIn">NotIn</asp:ListItem>
                <asp:ListItem Selected="False" Value="NotLike">NotLike</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;<asp:RequiredFieldValidator ID="rfvSubAgent" runat="server" ControlToValidate="ddlFieldName"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please Select Field Name"
                SetFocusOnError="True" ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvSubAgentBranch" runat="server" ControlToValidate="ddlOperator"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please Select Operator" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblValue" runat="server" Text="Value" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblValueIsRequired" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGuidLine" runat="server"
                Font-Size="Small" ForeColor="#999999" Visible="False"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtValue" runat="server" CssClass="txtbox" MaxLength="25" 
                style="margin-bottom: 0px"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" 
                Width="60px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False" />
        </td>
    </tr>
</table>