﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewPOMasterSeries.ascx.vb"
    Inherits="DesktopModules_POStationaryManagement_ViewPOMasterSeries" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure to remove this record ?") == true) {

            return true;
        }
        else {
            return false;
        }
    }
    function delcon(item) {


        if (window.confirm("Are you sure to remove this " + item + "?") == true) {

            return true;

        }
        else {

            return false;

        }
    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to remove Record(s)?") == true) {
            return true;
        }
        else {
            return false;
        }
    }  
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
   
</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="false"
        ErrorMessage="Invalid Company Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtGroup" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehaviorAddress" Validation-IsRequired="false">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCompany" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehaviorAccountNo" Validation-IsRequired="false">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehaviorOffName" Validation-IsRequired="false">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehaviorDescription">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPaymentNature" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehaviorProductType">
        <TargetControls>
            <telerik:TargetInput ControlID="txtProductType" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="center" valign="top" colspan="4">
                        <asp:Button ID="btnAddMasterSeries" runat="server" Text="Add PO Master Series" CssClass="btn" />
                        <asp:HiddenField ID="hfBankCode" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <asp:Label ID="lblMasterSeriesList" runat="server" Text="PO Master Series List" CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <asp:Label ID="lblRNF" runat="server" Font-Bold="False" Text="No Record Found" Visible="False"
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" colspan="4">
                        <telerik:RadGrid ID="gvViewMasterSeries" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100">
                            <ClientSettings>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Approve" DataField="IsApproved">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select" TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="POMasterSeriesID" HeaderText="PO Master Series ID"
                                        SortExpression="POMasterSeriesID">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StartFrom" HeaderText="StartFrom" SortExpression="StartFrom">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="EndsAt" HeaderText="EndsAt" SortExpression="EndsAt">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PreFix" HeaderText="Prefix" SortExpression="Prefix">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TotalCount" HeaderText="Total Count" SortExpression="TotalCount">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="IsUsed" HeaderText="Used" SortExpression="IsUsed">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="UsedOn" HeaderText="UnUsed" SortExpression="UsedOn">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SavePOMasterSeries", "&mid=" & Me.ModuleId & "&id="& Eval("POMasterSeriesID"))%>'
                                                ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                         <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandName="del" ImageUrl="~/images/delete.gif"
                                                OnClientClick="javascript: return con();" CommandArgument='<%#Eval("POMasterSeriesID")%>'
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                         <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" colspan="4">
                        <asp:Button ID="btnBulkDeleteMSeries" runat="server" Text="Delete Series" CssClass="btn"
                            OnClientClick="javascript: return conBukDelete();" />
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
