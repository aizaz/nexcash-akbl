﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports Telerik.Web.UI
Partial Class DesktopModules_POStationaryManagement_ViewPOMasterSeries
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnAddMasterSeries.Visible = CBool(htRights("Add"))
                btnBulkDeleteMSeries.Visible = CBool(htRights("Delete"))
                LoadgvMasterSeries(0, Me.gvViewMasterSeries.PageSize, True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region


    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "PO Stationary Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#Region "Button Events"
    Protected Sub btnSaveBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddMasterSeries.Click
        Page.Validate()
        If Page.IsValid Then
            Response.Redirect(NavigateURL("SavePOMasterSeries", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If
    End Sub
#End Region

    Private Sub LoadgvMasterSeries(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean)

        Try
            ICPOMasterSeriesController.GetAllPOMasterSeriesForRadGrid(PageNumber, PageSize, Me.gvViewMasterSeries, DoDataBind)

            If gvViewMasterSeries.Items.Count > 0 Then
                lblRNF.Visible = False
                gvViewMasterSeries.Visible = True
                btnBulkDeleteMSeries.Visible = CBool(htRights("Delete"))
                gvViewMasterSeries.Columns(9).Visible = CBool(htRights("Delete"))
            Else
                lblRNF.Visible = True
                gvViewMasterSeries.Visible = False

                btnBulkDeleteMSeries.Visible = False
            End If


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub


    Protected Sub gvViewMasterSeries_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvViewMasterSeries.ItemCommand

        Dim objICPOMasterSeries As New ICPOMasterSeries
        objICPOMasterSeries.es.Connection.CommandTimeout = 3600

        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    If objICPOMasterSeries.LoadByPrimaryKey(e.CommandArgument.ToString) Then
                        If CheckIsPOInstrumentsAssignedToLocation(e.CommandArgument.ToString) = False Then
                            If CheckIsInstrumentsAssignedOrUsedAgainstDDSeries(objICPOMasterSeries.POMasterSeriesID.ToString) = False Then
                                ICPOInstrumentsController.DeleteAddedPOInstrumentSeriesByPOMasterSeriesID(objICPOMasterSeries.POMasterSeriesID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                ICPOMasterSeriesController.DeletePOMasterSeries(objICPOMasterSeries.POMasterSeriesID, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                UIUtilities.ShowDialog(Me, "Delete PO Master Series", "PO Master series deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                                LoadgvMasterSeries(Me.gvViewMasterSeries.CurrentPageIndex + 1, Me.gvViewMasterSeries.PageSize, True)
                            Else
                                UIUtilities.ShowDialog(Me, "Delete PO Master Series", "PO Master series is assigned to office or instruction and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
                                LoadgvMasterSeries(Me.gvViewMasterSeries.CurrentPageIndex + 1, Me.gvViewMasterSeries.PageSize, False)
                                Exit Sub
                            End If
                        Else
                            UIUtilities.ShowDialog(Me, "Delete PO Master Series", "PO Master series is assigned to office", ICBO.IC.Dialogmessagetype.Failure)
                            LoadgvMasterSeries(Me.gvViewMasterSeries.CurrentPageIndex + 1, Me.gvViewMasterSeries.PageSize, False)
                            Exit Sub
                        End If
                    End If

                End If
            End If
            '    End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Error ", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                LoadgvMasterSeries(Me.gvViewMasterSeries.CurrentPageIndex + 1, Me.gvViewMasterSeries.PageSize, False)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Public Shared Function CheckIsPOInstrumentsAssignedToLocation(ByVal DDMasterSeriesID As String) As Boolean
        Dim Result As Boolean = False
        Dim objICInstrumentsColl As New ICPOInstrumentsCollection
        objICInstrumentsColl.es.Connection.CommandTimeout = 3600
        objICInstrumentsColl.Query.Where(objICInstrumentsColl.Query.POMasterSeriesID = DDMasterSeriesID And objICInstrumentsColl.Query.OfficeCode.IsNotNull)
        If objICInstrumentsColl.Query.Load Then
            Result = True
        End If
        Return Result
    End Function
    Public Shared Function CheckIsInstrumentsAssignedOrUsedAgainstDDSeries(ByVal DDMasterSeriesID As String) As Boolean
        Dim Result As Boolean = False
        Dim objICInstrumentsColl As New ICPOInstrumentsCollection
        objICInstrumentsColl.es.Connection.CommandTimeout = 3600
        objICInstrumentsColl.Query.Where(objICInstrumentsColl.Query.IsAssigned = True Or objICInstrumentsColl.Query.IsUsed = True)
        objICInstrumentsColl.Query.Where(objICInstrumentsColl.Query.POMasterSeriesID = DDMasterSeriesID)
        If objICInstrumentsColl.Query.Load Then
            Result = True
        End If
        Return Result
    End Function

    Protected Sub gvViewMasterSeries_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvViewMasterSeries.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvViewMasterSeries.MasterTableView.ClientID)
                Next
               myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub gvViewMasterSeries_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvViewMasterSeries.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvViewMasterSeries.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub gvViewMasterSeries_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvViewMasterSeries.NeedDataSource
        Try
            LoadgvMasterSeries(Me.gvViewMasterSeries.CurrentPageIndex + 1, Me.gvViewMasterSeries.PageSize, False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvViewMasterSeries_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvViewMasterSeries.PageIndexChanged
        Try
            gvViewMasterSeries.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckGVForProcessAll() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvMSRow As GridDataItem In gvViewMasterSeries.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvMSRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnBulkDeleteMSeries_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBulkDeleteMSeries.Click
        If Page.IsValid Then
            Try
                Dim rowgvMasterSeries As GridDataItem
                Dim chkSelect As CheckBox
                Dim POMasterSeriesID As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0

                If CheckGridViewForProcessAll() = True Then
                    For Each rowgvMasterSeries In gvViewMasterSeries.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvMasterSeries.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            POMasterSeriesID = rowgvMasterSeries("POMasterSeriesID").Text.ToString()
                            Dim objPOMasterSeries As New ICPOMasterSeries
                            objPOMasterSeries.es.Connection.CommandTimeout = 3600
                            If objPOMasterSeries.LoadByPrimaryKey(POMasterSeriesID.ToString()) Then
                                If CheckIsPOInstrumentsAssignedToLocation(POMasterSeriesID.ToString) = False Then
                                    Try
                                        ICPOInstrumentsController.DeleteAddedPOInstrumentSeriesByPOMasterSeriesID(objPOMasterSeries.POMasterSeriesID.ToString(), Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                        ICPOMasterSeriesController.DeletePOMasterSeries(POMasterSeriesID, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                        PassCount = PassCount + 1
                                    Catch ex As Exception
                                        If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                            FailCount = FailCount + 1
                                            Continue For
                                        End If
                                    End Try
                                Else
                                    FailCount = FailCount + 1
                                    Continue For
                                End If

                            End If
                        End If

                    Next

                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete PO Master Series", "[" & FailCount.ToString() & "] PO Master Series can not be deleted due to following reasons : <br /> 1. Associated records are present.<br /> 2. Assigned to location", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete PO Master Series", "[" & PassCount.ToString() & "] PO Master Series deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Delete PO Master Series", "[" & PassCount.ToString() & "] PO Master Series deleted successfully. [" & FailCount.ToString() & "] PO Master Series can not be deleted due to following reasons : <br /> 1. Associated records are present.<br /> 2. Assigned to location", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Delete PO Master Series", "Please select atleast one(1) PO Master Series.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try

        End If



    End Sub

    Private Function CheckGridViewForProcessAll() As Boolean
        Try
            Dim rowgvMasterSeries As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvMasterSeries In gvViewMasterSeries.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvMasterSeries.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

End Class
