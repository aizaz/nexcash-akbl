﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SavePOMasterSeries.ascx.vb"
    Inherits="DesktopModules_POStationaryManagement_SavePOMasterSeries" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }    
</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:TextBoxSetting BehaviorID="RagExpBehaviorAccountNo" Validation-IsRequired="true" ErrorMessage="Invalid Serires Prefix" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtSeriesPrefix" />
        </TargetControls>
    </telerik:TextBoxSetting>  
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehaviorPhone1" ValidationExpression="[0-9]{1,23}"
        Validation-IsRequired="true" ErrorMessage="Invalid End Series" >
        <TargetControls>
            <telerik:TargetInput ControlID="txtEndsAt" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior3" ValidationExpression="[0-9]{1,17}"
        Validation-IsRequired="true" ErrorMessage="Invalid Start Series" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtStartsFrom" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehanoOfLeaves" ValidationExpression="[0-9]{1,4}"
        Validation-IsRequired="true" ErrorMessage="Invalid No of Leaves" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtNoOfLeaves" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
</telerik:RadInputManager>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="2">
                        Note:&nbsp; Fields marked as
                        <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        &nbsp;are required.
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <asp:Label ID="lblStartsFrom" runat="server" Text="Master Series From" CssClass="lbl"></asp:Label>
                        <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                    <td align="left" valign="top">
                        <asp:Label ID="lblEndsAt0" runat="server" Text="No. of Leaves" CssClass="lbl"></asp:Label>
                        <asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <asp:TextBox ID="txtStartsFrom" runat="server" CssClass="txtbox" MaxLength="17" AutoPostBack="True"></asp:TextBox>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                    <td align="left" valign="top">
                        <asp:TextBox ID="txtNoOfLeaves" runat="server" CssClass="txtbox" MaxLength="4" AutoPostBack="True"></asp:TextBox>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <asp:Label ID="lblEndsAt" runat="server" Text="Master Series To" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <asp:TextBox ID="txtEndsAt" runat="server" CssClass="txtbox" MaxLength="23" 
                            ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                    <td align="left" valign="top">
                        &nbsp;</td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                         <br />
                         
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="middle" colspan="4">
                        <asp:Button ID="btnSaveMasterSeries" runat="server" Text="Save" CssClass="btn" Width="67px" />
                        &nbsp;
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False"
                            />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
