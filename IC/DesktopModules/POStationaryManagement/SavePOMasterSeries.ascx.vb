﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Partial Class DesktopModules_POStationaryManagement_SavePOMasterSeries
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private POMasterSeriesID As String
    Private TextSeriesStartFromChange As Boolean = False
    Private TextNoOfLeavesChange As Boolean = False
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            POMasterSeriesID = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                If POMasterSeriesID.ToString = "0" Then
                    lblPageHeader.Text = "Add PO Master Series"
                    btnSaveMasterSeries.Text = "Save"
                    btnSaveMasterSeries.Visible = CBool(htRights("Add"))
                Else
                    Dim objICPOMasterSeries As New ICPOMasterSeries
                    objICPOMasterSeries.es.Connection.CommandTimeout = 3600
                    objICPOMasterSeries.LoadByPrimaryKey(POMasterSeriesID)
                    btnSaveMasterSeries.Visible = CBool(htRights("Update"))

                    lblPageHeader.Text = "Edit PO Master Series"
                    btnSaveMasterSeries.Text = "Update"
                    txtStartsFrom.ReadOnly = True
                    txtEndsAt.ReadOnly = True
                    txtStartsFrom.Text = objICPOMasterSeries.StartFrom
                    txtEndsAt.Text = objICPOMasterSeries.EndsAt
                    txtNoOfLeaves.Text = CInt(txtEndsAt.Text - txtStartsFrom.Text) + 1

                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "PO Stationary Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub btnSaveMasterSeries_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveMasterSeries.Click
        If Page.IsValid Then
            Try
                Dim objICPOMasterSeries As New ICPOMasterSeries
                Dim objICPOMasterSeriesUpdate As New ICPOMasterSeries
                Dim objICPOInstrument As New ICPOInstruments
                Dim objICPOInstrumentUpdate As New ICPOInstruments
                Dim SavedPOMasterSeriesID As Integer = 0
                objICPOMasterSeries.es.Connection.CommandTimeout = 3600
                objICPOInstrument.es.Connection.CommandTimeout = 3600

                objICPOMasterSeries.StartFrom = CLng(txtStartsFrom.Text)
                objICPOMasterSeries.EndsAt = CLng(txtEndsAt.Text)
                objICPOMasterSeries.PreFix = "PO"

                If POMasterSeriesID.ToString = "0" Then
                    If CheckDuplicatePOMasterSeries(objICPOMasterSeries) = False Then
                        objICPOMasterSeries.CreatedBy = Me.UserId
                        objICPOMasterSeries.CreatedDate = Date.Now
                        objICPOMasterSeries.Creater = Me.UserId
                        objICPOMasterSeries.CreationDate = Date.Now
                        SavedPOMasterSeriesID = ICPOMasterSeriesController.AddPOMasterSeries(objICPOMasterSeries, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        If Not SavedPOMasterSeriesID = 0 Then
                            Dim i As Long = Nothing
                            Dim SeriesStarFrom As Long = CLng(txtStartsFrom.Text)
                            Dim NoOfLeaves As Long = CLng(txtEndsAt.Text)
                            Dim POMasterSeriesEndsAt As Long = CLng(txtEndsAt.Text)
                            Dim k As Long = SeriesStarFrom
                            For i = SeriesStarFrom To POMasterSeriesEndsAt

                                objICPOInstrument.POMasterSeriesID = SavedPOMasterSeriesID
                                objICPOInstrument.POInstrumentNumber = CLng(i)
                                objICPOInstrument.PreFix = objICPOMasterSeries.PreFix
                                objICPOInstrument.IsUsed = Nothing
                                objICPOInstrument.CreatedDate = Date.Now
                                objICPOInstrument.CreatedBy = Me.UserId
                                objICPOInstrument.CreationDate = Date.Now
                                objICPOInstrument.Creater = Me.UserId
                                ICPOInstrumentsController.AddPOInstrumentSeriesFromPOMasterSeries(objICPOInstrument, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            Next
                        End If
                        UIUtilities.ShowDialog(Me, "Add PO Master Series", "PO Master series added successfully", ICBO.IC.Dialogmessagetype.Success)
                        clear()
                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Sorry! Duplicate PO Master Series is not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                        clear()
                        Exit Sub
                    End If
                Else

                    If objICPOMasterSeriesUpdate.LoadByPrimaryKey(POMasterSeriesID) Then
                        objICPOMasterSeries.POMasterSeriesID = objICPOMasterSeriesUpdate.POMasterSeriesID
                        objICPOMasterSeries.CreatedBy = Me.UserId
                        objICPOMasterSeries.CreatedDate = Date.Now
                        Dim TopOrder As Long = Nothing
                        Dim AvailableLeaves As Integer = Nothing
                        Dim i As Integer = 0

                        TopOrder = CLng(CLng(objICPOMasterSeriesUpdate.EndsAt - objICPOMasterSeriesUpdate.StartFrom + 1) - CLng(txtNoOfLeaves.Text))

                        If CLng(txtNoOfLeaves.Text) < (CLng(CLng(objICPOMasterSeriesUpdate.EndsAt) - CLng(objICPOMasterSeriesUpdate.StartFrom) + 1)) Then
                            If CheckDuplicateDDMasterSeriesOnAddingLeaves(objICPOMasterSeries, TopOrder, False) = True Then
                                If GetTopAssignedPOInstrumentNumbersToPrintLocationsByPOMSID(POMasterSeriesID, TopOrder) = True Then
                                    ICPOMasterSeriesController.AddPOMasterSeries(objICPOMasterSeries, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                    ICPOInstrumentsController.DeleteUpdatedPOInstrumentSeriesByPOMasterSeriesID(objICPOMasterSeries.POMasterSeriesID, Me.UserId.ToString, Me.UserInfo.Username.ToString, TopOrder)
                                    UIUtilities.ShowDialog(Me, "Updated PO Master Series", "PO Master series updated successfully", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                                    clear()
                                Else
                                    UIUtilities.ShowDialog(Me, "Error", "Required number series is assigned to printed location.", ICBO.IC.Dialogmessagetype.Failure)
                                    Exit Sub
                                End If
                            Else
                                UIUtilities.ShowDialog(Me, "Error", "Required number of series is printed or assigned to instruction.", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If

                        ElseIf CLng(txtNoOfLeaves.Text) > (CLng(CLng(objICPOMasterSeriesUpdate.EndsAt) - CLng(objICPOMasterSeriesUpdate.StartFrom) + 1)) Then

                            If CheckDuplicateDDMasterSeriesOnUpdate(objICPOMasterSeries) = False Then
                                Dim LastPOInstrumentNo As Integer = Nothing
                                ICPOMasterSeriesController.AddPOMasterSeries(objICPOMasterSeries, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                LastPOInstrumentNo = (ICPOInstrumentsController.GetLastPOInstrumentOfPOMasterSeries(objICPOMasterSeriesUpdate.POMasterSeriesID) + 1)
                                For i = LastPOInstrumentNo To CInt(txtEndsAt.Text)

                                    objICPOInstrument.POMasterSeriesID = objICPOMasterSeries.POMasterSeriesID
                                    objICPOInstrument.POInstrumentNumber = CLng(i)
                                    objICPOInstrument.PreFix = objICPOMasterSeries.PreFix
                                    objICPOInstrument.IsUsed = Nothing
                                    objICPOInstrument.CreatedDate = Date.Now
                                    objICPOInstrument.CreatedBy = Me.UserId
                                    objICPOInstrument.CreationDate = Date.Now
                                    objICPOInstrument.Creater = Me.UserId
                                    ICPOInstrumentsController.AddPOInstrumentSeriesFromPOMasterSeries(objICPOInstrument, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                Next
                                UIUtilities.ShowDialog(Me, "Updated PO Master Series", "PO Master series updated successfully", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                                clear()
                            Else
                                UIUtilities.ShowDialog(Me, "Error", "Duplicate PO Master Series is not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Equal no of leaves", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    End If

                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub
    Public Shared Function GetTopAssignedPOInstrumentNumbersToPrintLocationsByPOMSID(ByVal POMasterSeriesID As String, ByVal TopOrder As Integer) As Boolean
        Dim qryObjICPOInstruments As New ICPOInstrumentsQuery("qryObjICPOInstruments")
        Dim Result As Boolean = False
        Dim dt As New DataTable
        Dim dtFinal As New DataTable
        Dim drFinal As DataRow()
        qryObjICPOInstruments.Select(qryObjICPOInstruments.POInstrumentNumber, qryObjICPOInstruments.OfficeCode)
        qryObjICPOInstruments.Where(qryObjICPOInstruments.POMasterSeriesID = POMasterSeriesID)
        qryObjICPOInstruments.OrderBy(qryObjICPOInstruments.POInstrumentNumber.Descending)
        qryObjICPOInstruments.es.Top = TopOrder
        dt = qryObjICPOInstruments.LoadDataTable()
        If dt.Rows.Count > 0 Then
            drFinal = dt.Select("OfficeCode IS NULL")
            dtFinal = dt.Clone
            For Each drFinal2 As DataRow In drFinal
                dtFinal.ImportRow(drFinal2)
            Next
            If dtFinal.Rows.Count >= TopOrder Then
                Result = True
            End If

        End If
        Return Result
    End Function
    Public Shared Function IsUpDatedPOMasterSeriesInstrumentsAssignedToLocation(ByVal POMasterSeriesID As String, ByVal POSeriesStartFrom As Long, ByVal POSeriesEndsAt As Long) As Boolean
        Dim objICPOInstrumentColl As New ICPOInstrumentsCollection
        Dim Result As Boolean = False
        objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.POInstrumentNumber.Between(POSeriesStartFrom, POSeriesEndsAt) And objICPOInstrumentColl.Query.OfficeCode.IsNull)
        objICPOInstrumentColl.Query.OrderBy(objICPOInstrumentColl.Query.POInstrumentNumber.Ascending)
        objICPOInstrumentColl.Query.Load()
        If objICPOInstrumentColl.Query.Load Then
            Result = True
        End If
        Return Result
    End Function
    Private Function CheckDuplicateDDMasterSeriesOnUpdate(ByVal objICDDMasterSeries As ICPOMasterSeries) As Boolean
        Dim Result As Boolean = False

        Dim objICDDMSeriesColl As New ICPOMasterSeriesCollection
        Dim objICDDInstrumentColl As New ICPOInstrumentsCollection
        objICDDInstrumentColl.es.Connection.CommandTimeout = 3600
        objICDDMSeriesColl.es.Connection.CommandTimeout = 3600

        'objICDDMSeriesColl.LoadAll()
        objICDDMSeriesColl.Query.Where(objICDDMSeriesColl.Query.StartFrom = objICDDMasterSeries.StartFrom And objICDDMSeriesColl.Query.EndsAt = objICDDMasterSeries.EndsAt And objICDDMSeriesColl.Query.PreFix = objICDDMasterSeries.PreFix And objICDDMSeriesColl.Query.POMasterSeriesID <> objICDDMasterSeries.POMasterSeriesID)
        objICDDMSeriesColl.Query.Load()
        If objICDDMSeriesColl.Query.Load Then
            Result = True
        End If
        If Result = False Then
            objICDDInstrumentColl.LoadAll()

            objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.POInstrumentNumber >= objICDDMasterSeries.StartFrom And objICDDInstrumentColl.Query.POInstrumentNumber <= objICDDMasterSeries.EndsAt And objICDDInstrumentColl.Query.PreFix = objICDDMasterSeries.PreFix And objICDDMSeriesColl.Query.POMasterSeriesID <> objICDDMasterSeries.POMasterSeriesID)
            objICDDInstrumentColl.Query.Load()
            If objICDDInstrumentColl.Query.Load Then
                Result = True
            End If
        End If
        Return Result
    End Function
    Private Sub clear()
        txtStartsFrom.Text = ""
        txtEndsAt.Text = ""
        txtNoOfLeaves.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
     
    End Sub
  

    Private Function CheckDuplicatePOMasterSeries(ByVal objICPOMasterSeries As ICPOMasterSeries) As Boolean
        Dim Result As Boolean = False

        Dim objICPOMSeriesColl As New ICPOMasterSeriesCollection
        Dim objICPOInstrumentColl As New ICPOInstrumentsCollection
       
        objICPOInstrumentColl.es.Connection.CommandTimeout = 3600
        objICPOMSeriesColl.es.Connection.CommandTimeout = 3600


       objICPOMSeriesColl.LoadAll()
        objICPOMSeriesColl.Query.Where(objICPOMSeriesColl.Query.StartFrom = objICPOMasterSeries.StartFrom And objICPOMSeriesColl.Query.EndsAt = objICPOMasterSeries.EndsAt And objICPOMSeriesColl.Query.PreFix = objICPOMasterSeries.PreFix)
        objICPOMSeriesColl.Query.Load()
        If objICPOMSeriesColl.Query.Load Then
            Result = True
        End If
        objICPOInstrumentColl.LoadAll()

        If Result = False Then
            objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.POInstrumentNumber >= objICPOMasterSeries.StartFrom And objICPOInstrumentColl.Query.POInstrumentNumber <= objICPOMasterSeries.EndsAt And objICPOInstrumentColl.Query.PreFix = objICPOMasterSeries.PreFix)
            objICPOInstrumentColl.Query.Load()
            If objICPOInstrumentColl.Query.Load Then
                Result = True
            End If
        End If

        Return Result

   End Function
    Private Function CheckDuplicateDDMasterSeriesOnAddingLeaves(ByVal objICMasterSeries As ICPOMasterSeries, ByVal TopOrder As Double, ByVal IsExtends As Boolean) As Boolean
        Dim Result As Boolean = False
        Dim qryObjICDDInstrument As New ICPOInstrumentsQuery
        Dim dt As New DataTable
        If IsExtends = False Then
            qryObjICDDInstrument.Select(qryObjICDDInstrument.POInstrumentNumber.Count)
            qryObjICDDInstrument.Where(qryObjICDDInstrument.POMasterSeriesID = objICMasterSeries.POMasterSeriesID And qryObjICDDInstrument.IsUsed = False And qryObjICDDInstrument.IsAssigned.IsNull)
            qryObjICDDInstrument.OrderBy(qryObjICDDInstrument.POInstrumentNumber.Descending)
            qryObjICDDInstrument.es.Top = TopOrder
            dt = qryObjICDDInstrument.LoadDataTable

            If CLng(dt.Rows(0)(0)) >= TopOrder Then
                Result = True
            End If
        End If
        Return Result
    End Function

    'Private Function CheckDuplicateMasterSeriesOnAddingLeaves(ByVal objICMasterSeries As ICPOMasterSeries) As Boolean
    '    Dim Result As Boolean = False

    '    Dim objICMSeriesColl As New ICPOMasterSeriesCollection
    '    Dim objICPOInstrumentColl As New ICPOInstrumentsCollection

    '    objICPOInstrumentColl.es.Connection.CommandTimeout = 3600
    '    objICMSeriesColl.es.Connection.CommandTimeout = 3600



    '    objICPOInstrumentColl.LoadAll()

    '    objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.POInstrumentNumber.In(objICMasterSeries.StartFrom, objICMasterSeries.EndsAt) And objICPOInstrumentColl.Query.PreFix = objICMasterSeries.PreFix)
    '    objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.POMasterSeriesID <> objICMasterSeries.POMasterSeriesID)
    '    objICPOInstrumentColl.Query.Load()
    '    If objICPOInstrumentColl.Query.Load Then
    '        Result = True
    '    End If

    '    Return Result
    'End Function


    Protected Sub txtNoOfLeaves_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNoOfLeaves.TextChanged

        Try
            'TextNoOfLeavesChange = True
            'If TextSeriesStartFromChange = False Then
            If txtNoOfLeaves.Text.ToString() <> "" And txtNoOfLeaves.Text.ToString() <> "Enter No Of Leaves" Then
                If txtStartsFrom.Text.ToString() <> "" And txtStartsFrom.Text.ToString() <> "Enter Start Series" Then
                    Dim SeriesStartfrom As Long = Nothing
                    Dim LeavesOfSeries As Integer = Nothing
                    Dim SeriesEndAt As Long = Nothing
                    SeriesStartfrom = CLng(txtStartsFrom.Text)
                    LeavesOfSeries = CInt(txtNoOfLeaves.Text)
                    SeriesEndAt = (SeriesStartfrom + LeavesOfSeries) - 1
                    txtEndsAt.Text = SeriesEndAt
                Else
                    txtEndsAt.Text = ""
                End If

            Else
                txtEndsAt.Text = ""
              
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub

    Protected Sub txtStartsFrom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStartsFrom.TextChanged
      
        Try
            ' TextSeriesStartFromChange = True
            '  If TextNoOfLeavesChange = False Then
            If txtStartsFrom.Text.ToString() <> "" And txtStartsFrom.Text.ToString() <> "Enter Start Series" Then
                If txtNoOfLeaves.Text.ToString() <> "" And txtNoOfLeaves.Text.ToString() <> "Enter No Of Leaves" Then
                    Dim SeriesStartfrom As Long = Nothing
                    Dim LeavesOfSeries As Integer = Nothing
                    Dim SeriesEndAt As Long = Nothing
                    SeriesStartfrom = CLng(txtStartsFrom.Text)
                    LeavesOfSeries = CInt(txtNoOfLeaves.Text)
                    SeriesEndAt = (SeriesStartfrom + LeavesOfSeries) - 1
                    txtEndsAt.Text = SeriesEndAt
                Else
                    txtEndsAt.Text = ""
                    'UIUtilities.ShowDialog(Me, "Error", "Please enter No of leaves in double figure.", ICBO.IC.Dialogmessagetype.Failure)
                    'Exit Sub
                End If
            Else
                txtEndsAt.Text = ""
                '  txtNoOfLeaves.Text = ""
            End If
            ' End If
            TextNoOfLeavesChange = False
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
End Class
