﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveClearingLimits.ascx.vb" Inherits="DesktopModules_UserClearingLimits_SaveClearingLimits" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior2" EmptyMessage="type.."
        Type="Number">
    </telerik:NumericTextBoxSetting>
    <telerik:NumericTextBoxSetting BehaviorID="RagExpBehavior2" Validation-IsRequired="false"
        AllowRounding="false" DecimalDigits="2" ErrorMessage="Invalid Clearing Limit"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtClearingLimit" />
        </TargetControls>
    </telerik:NumericTextBoxSetting>
    <telerik:NumericTextBoxSetting BehaviorID="RagExpBehaviorPayment1" AllowRounding="false" DecimalDigits="2"
        Validation-IsRequired="false" ErrorMessage="Invalid Clearing Approval Limit" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtClearingApprovalLimit" />
        </TargetControls>
    </telerik:NumericTextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:label id="lblPageHeader" runat="server" cssclass="headingblue"></asp:label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:label id="Label1" runat="server" text="*" forecolor="Red"></asp:label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:label id="lblBankBranch" runat="server" text="Select Bank Branch" cssclass="lbl"></asp:label>
            <asp:label id="lblReqBankBranch" runat="server" text="*" forecolor="Red"></asp:label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:label id="lblUser" runat="server" text="Select User" cssclass="lbl"></asp:label>
            <asp:label id="lblReqUser" runat="server" text="*" forecolor="Red"></asp:label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:dropdownlist id="ddlBankBranch" runat="server" cssclass="dropdown" autopostback="True">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
            </asp:dropdownlist>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:dropdownlist id="ddlUsers" runat="server" cssclass="dropdown" autopostback="True">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
            </asp:dropdownlist>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlBankBranch" runat="server" 
                ControlToValidate="ddlBankBranch" Display="Dynamic" 
                ErrorMessage="Please select Bank Branch" InitialValue="0" 
                SetFocusOnError="True"></asp:RequiredFieldValidator>
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlUsers" runat="server" 
                ControlToValidate="ddlUsers" Display="Dynamic" 
                ErrorMessage="Please select Users" InitialValue="0" 
                SetFocusOnError="True"></asp:RequiredFieldValidator>
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:label id="lblClearingLimit" runat="server" text="Clearing Limit" cssclass="lbl"></asp:label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:label id="lblClearingApprovalLimit" runat="server" text="Clearing Approval Limit" cssclass="lbl"></asp:label>
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:textbox id="txtClearingLimit" runat="server" cssclass="txtbox" maxlength="14"></asp:textbox>
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:textbox id="txtClearingApprovalLimit" runat="server" cssclass="txtbox" 
                maxlength="14"></asp:textbox>
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
   
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;<asp:button id="btnSave" runat="server" text="Save" cssclass="btn" width="71px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" causesvalidation="False"
                />
            &nbsp;
        </td>
    </tr>
</table>

