﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewClearingLimits.ascx.vb"
    Inherits="DesktopModules_UserClearingLimits_ViewClearingLimits" %>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
            }
        }
    }
</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table  width="100%">
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnAddUserClearingLimits" runat="server" Text="Add User Clearing Limits"
                            CssClass="btn" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <asp:Label ID="lblClearingLimitsListHeader" runat="server" Text="Clearing Limits List"
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" >
                        <asp:Label ID="lblBankBranch" runat="server" Text="Select Bank Branch" CssClass="lbl"></asp:Label>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" >
                        <asp:DropDownList ID="ddlBankBranch" runat="server" CssClass="dropdown" AutoPostBack="True">
                            <asp:ListItem Value="0">-- All --</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" >
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" >
                        <asp:Label ID="lblClearingLimitsRNF" runat="server" Text="Record Not Found." CssClass="headingblue"
                            Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <telerik:RadGrid ID="gvUserClearingLimits" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <ClientSettings>
                            
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <MasterTableView TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Approve" DataField="IsApproved">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select" TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="LimitsForClearingID" HeaderText="Clearing LimitsID" SortExpression="LimitsForClearingID">
                                    </telerik:GridBoundColumn>
                                   <telerik:GridBoundColumn DataField="OfficeName" HeaderText="Office Name" SortExpression="OfficeName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="UserName" HeaderText="User Name" SortExpression="UserName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisplayName" HeaderText="Display Name" SortExpression="DisplayName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="MakerLimits" HeaderText="Maker Limits" SortExpression="MakerLimits" HtmlEncode="false" DataFormatString="{0:N2}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ApprovalLimits" HeaderText="ApprovalLimits" SortExpression="ApprovalLimits" HtmlEncode="false" DataFormatString="{0:N2}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Label ID="Edit" runat="server" Text="Edit"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveClearingLimits", "&mid=" & Me.ModuleId & "&ClearingLmt="& Eval("LimitsForClearingID"))%>'
                                                ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Label ID="Delete" runat="server" Text="Delete"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("LimitsForClearingID") %>'
                                                CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr align="center" valign="top" style="width: 100%">
        <td align="center" valign="top" style="width: 100%">
            <asp:Button ID="btnDeleteLimits" runat="server" Text="Delete Limits" CssClass="btn" />
        </td>
    </tr>
</table>
