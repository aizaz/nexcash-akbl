﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_UserClearingLimits_ViewClearingLimits
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing
                btnAddUserClearingLimits.Visible = CBool(htRights("Add"))
                btnDeleteLimits.Visible = CBool(htRights("Delete"))
                LoadddlBankBranch()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "UserClearingLimits")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Drop Down"

    Private Sub LoadddlBankBranch()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlBankBranch.Items.Clear()
            ddlBankBranch.Items.Add(lit)
            ddlBankBranch.AppendDataBoundItems = True
            ddlBankBranch.DataSource = ICOfficeController.GetPrincipalBankBranchActiveAndApproveWithCodeAndName()
            ddlBankBranch.DataTextField = "OfficeName"
            ddlBankBranch.DataValueField = "OfficeID"
            ddlBankBranch.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlBankBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBankBranch.SelectedIndexChanged
        Try
            LoadUserClearingLimits(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Grid View"

    Protected Sub gvUserClearingLimits_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvUserClearingLimits.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvUserClearingLimits.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub gvUserClearingLimits_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvUserClearingLimits.NeedDataSource
        LoadUserClearingLimits(False)
    End Sub

    Protected Sub gvUserClearingLimits_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvUserClearingLimits.ItemCommand
        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    ICLimitsForClearingController.DeleteClearingLimits(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    UIUtilities.ShowDialog(Me, "Delete Clearing Limits", "Clearing Limits deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                    LoadUserClearingLimits(True)
                End If
            End If

        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Deleted Clearing Limits", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvUserClearingLimits_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvUserClearingLimits.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvUserClearingLimits.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvUserClearingLimits_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvUserClearingLimits.PageIndexChanged
        gvUserClearingLimits.CurrentPageIndex = e.NewPageIndex
    End Sub

    Private Sub LoadUserClearingLimits(ByVal IsBind As Boolean)
        Try
            ICLimitsForClearingController.GetClearingLmtgv(ddlBankBranch.SelectedValue.ToString(), gvUserClearingLimits.CurrentPageIndex + 1, gvUserClearingLimits.PageSize, gvUserClearingLimits, IsBind)

            If gvUserClearingLimits.Items.Count > 0 Then
                gvUserClearingLimits.Visible = True

                lblClearingLimitsRNF.Visible = False
                btnDeleteLimits.Visible = CBool(htRights("Delete"))
                gvUserClearingLimits.Columns(8).Visible = CBool(htRights("Delete"))
            Else
                gvUserClearingLimits.Visible = False
                btnDeleteLimits.Visible = False
                lblClearingLimitsRNF.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Button"

    Protected Sub btnAddUserClearingLimits_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddUserClearingLimits.Click
        Page.Validate()

        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveClearingLimits", "&mid=" & Me.ModuleId & "&ClearingLmt=0"), False)
        End If
    End Sub

    Protected Sub btnDeleteLimits_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteLimits.Click
        Try
            Dim rowgvUserClearingLimits As GridDataItem
            Dim chkSelect As CheckBox
            Dim LimitsForClearingID As String = ""
            Dim PassCount As Integer = 0
            Dim FailCount As Integer = 0

            If CheckgvUserClearingLimitsForProcessAll() = True Then
                For Each rowgvUserClearingLimits In gvUserClearingLimits.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowgvUserClearingLimits.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        LimitsForClearingID = rowgvUserClearingLimits("LimitsForClearingID").Text.ToString()
                        Dim objClearingLmts As New ICLimitsForClearing
                        objClearingLmts.es.Connection.CommandTimeout = 3600
                        If objClearingLmts.LoadByPrimaryKey(LimitsForClearingID.ToString()) Then
                            Try
                                ICLimitsForClearingController.DeleteClearingLimits(LimitsForClearingID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                PassCount = PassCount + 1
                            Catch ex As Exception
                                If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                    FailCount = FailCount + 1
                                End If
                            End Try
                            ' End If
                        End If
                    End If

                Next

                If PassCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Delete Clearing Limits", "[" & FailCount.ToString() & "] Clearing Limits can not be deleted due to following reasones : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    Exit Sub
                End If
                If FailCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Delete Clearing Limits", "[" & PassCount.ToString() & "] Clearing Limits deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    Exit Sub
                End If
                UIUtilities.ShowDialog(Me, "Delete Clearing Limits", "[" & PassCount.ToString() & "] Clearing Limits deleted successfully. [" & FailCount.ToString() & "] Clearing Limits can not be deleted due to following reasons : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
            Else
                UIUtilities.ShowDialog(Me, "Delete Clearing Limits", "Please select atleast one(1) Clearing Limit.", ICBO.IC.Dialogmessagetype.Warning)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Function CheckgvUserClearingLimitsForProcessAll() As Boolean
        Try
            Dim rowgvUserClearingLimits As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvUserClearingLimits In gvUserClearingLimits.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvUserClearingLimits.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

#End Region


End Class

