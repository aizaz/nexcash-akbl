﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_UserClearingLimits_SaveClearingLimits
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
    Private ClearingLmt As String

#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            ClearingLmt = Request.QueryString("ClearingLmt").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing



                If ClearingLmt.ToString() = "0" Then
                    Clear()
                    lblPageHeader.Text = "Add User Clearing Limits"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                    LoadddlBankBranch()
                Else
                    lblPageHeader.Text = "Edit User Clearing Limits"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    ddlBankBranch.Enabled = False
                    ddlUsers.Enabled = False

                  
                    Dim objClearingLmt As New ICLimitsForClearing
                    Dim objUser As New ICUser
                    objUser.es.Connection.CommandTimeout = 3600
                    objClearingLmt.es.Connection.CommandTimeout = 3600
                    objClearingLmt.LoadByPrimaryKey(ClearingLmt)
                    If objClearingLmt.LoadByPrimaryKey(ClearingLmt) Then
                        objUser.LoadByPrimaryKey(objClearingLmt.UserID)
                        txtClearingApprovalLimit.Text = objClearingLmt.ApprovalLimits.ToString()
                        txtClearingLimit.Text = objClearingLmt.MakerLimits.ToString()
                        LoadddlBankBranch()
                        ddlBankBranch.SelectedValue = objUser.OfficeCode
                        LoadddlBankBranchUser()
                        ddlUsers.SelectedValue = objClearingLmt.UserID
                    End If

                End If

            End If
         
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        txtClearingLimit.Text = ""
        txtClearingApprovalLimit.Text = ""
        ddlBankBranch.ClearSelection()
        ddlUsers.ClearSelection()
        LoadddlBankBranchUser()
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "UserClearingLimits")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Drop Down"

    Private Sub LoadddlBankBranch()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBankBranch.Items.Clear()
            ddlBankBranch.Items.Add(lit)
            ddlBankBranch.AppendDataBoundItems = True
            ddlBankBranch.DataSource = ICOfficeController.GetPrincipalBankBranchActiveAndApproveWithCodeAndName()
            ddlBankBranch.DataTextField = "OfficeName"
            ddlBankBranch.DataValueField = "OfficeID"
            ddlBankBranch.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlBankBranchUser()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlUsers.Items.Clear()
            ddlUsers.Items.Add(lit)
            ddlUsers.AppendDataBoundItems = True
            ddlUsers.DataSource = ICUserController.GetAllActiveAndApproveUsersOfPrincipalBankBranchforClearingLmts(ddlBankBranch.SelectedValue.ToString, ClearingLmt.ToString())
            ddlUsers.DataTextField = "UserName"
            ddlUsers.DataValueField = "UserID"
            ddlUsers.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlBankBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBankBranch.SelectedIndexChanged
        Try
            If ddlBankBranch.SelectedValue = "0" Then
                LoadddlBankBranchUser()
            Else
                LoadddlBankBranchUser()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlUsers_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUsers.SelectedIndexChanged
        If ddlBankBranch.SelectedValue = "0" Then
            LoadddlBankBranchUser()
        End If
    End Sub

#End Region

#Region "Button"

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub
    Private Function CheckUserApprovalLimitIsValid(ByVal TextBoxForVerify As TextBox) As String
        Dim ResultString As String = "OK"
        Dim Result As Double
        If TextBoxForVerify.Text.ToString() <> "" Then

            If TextBoxForVerify.Text.Contains(".") Then
                Dim str As String()
                Dim strBDeci, strADeci As String
                Dim TotAmt As String = ""
                str = TextBoxForVerify.Text.Split(".")
                strBDeci = str(0).ToString()
                strADeci = str(1).ToString()
                TotAmt = strBDeci & strADeci
                If TotAmt.Length > 13 Then
                    ResultString = ""
                    ResultString = "Invalid clearing limit."
                    Return Result
                    Exit Function
                End If
                If strBDeci.Length > 11 Then
                    ResultString = ""
                    ResultString = "Invalid clearing limit."
                    Return Result
                    Exit Function
                End If
            Else
                If TextBoxForVerify.Text.Length > 11 Then
                    ResultString = ""
                    ResultString = "Invalid clearing limit."
                    Return Result
                    Exit Function
                End If
            End If

            Try
                If Double.TryParse(TextBoxForVerify.Text.Trim(), Result) = False Then
                    ResultString = ""
                    ResultString = "Invalid clearing limit."
                    Return Result
                    Exit Function
                End If
            Catch ex As Exception
                ResultString = ""
                ResultString = "Invalid clearing limit."
                Return Result
                Exit Function
            End Try


            If CDbl(TextBoxForVerify.Text.ToString()) <= 0 Then
                ResultString = ""
                ResultString = "Invalid clearing limit."
                Return Result
                Exit Function
            End If
        End If
        Return ResultString
    End Function
    Private Function ValidateApprovalLimits() As Boolean
        Dim Result As Boolean = True
        If txtClearingLimit.Text.ToString <> "" And Not txtClearingLimit.Text Is Nothing Then
            If CheckUserApprovalLimitIsValid(txtClearingLimit) <> "OK" Then
                Result = False
                Return Result
                Exit Function
            End If
        End If
        If txtClearingApprovalLimit.Text.ToString <> "" And Not txtClearingApprovalLimit.Text Is Nothing Then
            If CheckUserApprovalLimitIsValid(txtClearingApprovalLimit) <> "OK" Then
                Result = False
                Return Result
                Exit Function
            End If
        End If
        Return Result
    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim objClearingLmts As New ICLimitsForClearing
                objClearingLmts.es.Connection.CommandTimeout = 3600

                If ClearingLmt <> "0" Then
                    objClearingLmts.LoadByPrimaryKey(ClearingLmt)
                End If

                If txtClearingLimit.Text.ToString() = "" And txtClearingApprovalLimit.Text.ToString() = "" Then
                    UIUtilities.ShowDialog(Me, "Save Clearing Limits", "Please enter atleast one Limit.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                If txtClearingLimit.Text = "0" Or txtClearingApprovalLimit.Text = "0" Then
                    UIUtilities.ShowDialog(Me, "Save Clearing Limits", "Please enter Limit greater than zero.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If ValidateApprovalLimits() = False Then
                    UIUtilities.ShowDialog(Me, "Save Clearing Limits", "Invalid clearing limits entered", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If txtClearingLimit.Text.ToString() = "" Then
                    objClearingLmts.MakerLimits = 0
                Else
                    objClearingLmts.MakerLimits = CDbl(txtClearingLimit.Text)
                End If

                If txtClearingApprovalLimit.Text.ToString() = "" Then
                    objClearingLmts.ApprovalLimits = 0
                Else
                    objClearingLmts.ApprovalLimits = CDbl(txtClearingApprovalLimit.Text)
                End If

                objClearingLmts.UserID = ddlUsers.SelectedValue.ToString()


                If ClearingLmt = "0" Then
                    objClearingLmts.CreatedBy = Me.UserId
                    objClearingLmts.CreatedDate = Date.Now
                    objClearingLmts.Creater = Me.UserId
                    objClearingLmts.CreationDate = Date.Now
                    ICLimitsForClearingController.DeleteAllRecordsByUserID(ddlUsers.SelectedValue.ToString())
                    ICLimitsForClearingController.AddClearingLimits(objClearingLmts, False, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    UIUtilities.ShowDialog(Me, "Save Clearing Limits", "Clearing Limits added successfully.", ICBO.IC.Dialogmessagetype.Success)

                Else
                    objClearingLmts.CreatedBy = Me.UserId
                    objClearingLmts.CreatedDate = Date.Now
                    objClearingLmts.LimitsForClearingID = ClearingLmt
                    ICLimitsForClearingController.AddClearingLimits(objClearingLmts, True, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    UIUtilities.ShowDialog(Me, "Save Clearing Limits", "Clearing Limits updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                End If
                Clear()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

#End Region

End Class
