﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_CollectionAccountsManagement_ViewCollectionAccounts
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private htRights As Hashtable

#Region "Page Load"

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlCompany()
                btnSaveAccounts.Visible = CBool(htRights("Add"))
                btnDeleteAccounts.Visible = CBool(htRights("Delete"))
                LoadAccounts(0, True, Me.gvAccountsDetails.PageSize)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Collection Accounts Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            ' str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function

#End Region

#Region "Buttons"

    Protected Sub btnSaveAccounts_Click(sender As Object, e As System.EventArgs) Handles btnSaveAccounts.Click
        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveCollectionAccounts", "&mid=" & Me.ModuleId & "&AccountNumber=0" & "&BranchCode=0" & "&Currency=0"), False)
        End If
    End Sub

    Protected Sub btnDeleteAccounts_Click(sender As Object, e As System.EventArgs) Handles btnDeleteAccounts.Click
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim PassToApproveCount As Integer = 0
                Dim FailToApproveCount As Integer = 0
                Dim objICCollectionAccounts As ICCollectionAccounts
                Dim AccountNumber, BranchCode, Currency As String

                If CheckGVCompanySelected() = False Then
                    UIUtilities.ShowDialog(Me, "Delete Collection Accounts", "Please select atleast one account.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVAccounts In gvAccountsDetails.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVAccounts.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        AccountNumber = ""
                        BranchCode = ""
                        Currency = ""
                        AccountNumber = rowGVAccounts.GetDataKeyValue("AccountNumber").ToString
                        BranchCode = rowGVAccounts.GetDataKeyValue("BranchCode").ToString
                        Currency = rowGVAccounts.GetDataKeyValue("Currency").ToString
                        objICCollectionAccounts = New ICCollectionAccounts
                        objICCollectionAccounts.es.Connection.CommandTimeout = 3600
                        objICCollectionAccounts.LoadByPrimaryKey(AccountNumber.ToString, BranchCode.ToString, Currency.ToString)
                        If CheckIsAdminOrSuperUser() = False Then
                            Try
                                ICCollectionAccountsController.DeleteCollectionAccount(AccountNumber, BranchCode, Currency, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                PassToApproveCount = PassToApproveCount + 1
                            Catch ex As Exception
                                FailToApproveCount = FailToApproveCount + 1
                                Continue For
                            End Try


                        Else
                            Try
                                ICCollectionAccountsController.DeleteCollectionAccount(AccountNumber, BranchCode, Currency, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                PassToApproveCount = PassToApproveCount + 1
                            Catch ex As Exception
                                FailToApproveCount = FailToApproveCount + 1
                                Continue For

                            End Try

                        End If

                    End If
                Next

                If Not PassToApproveCount = 0 And Not FailToApproveCount = 0 Then
                    LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, True, Me.gvAccountsDetails.PageSize)
                    UIUtilities.ShowDialog(Me, "Delete Collection Account", "[ " & PassToApproveCount.ToString & " ] Collection Account(s) deleted successfully.<br /> [ " & FailToApproveCount.ToString & " ] Collection Account(s) can not be deleted due to following reasons:<br /> 1. Deleted associated record(s) first.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToApproveCount = 0 And FailToApproveCount = 0 Then
                    LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, True, Me.gvAccountsDetails.PageSize)
                    UIUtilities.ShowDialog(Me, "Delete Collection Account", PassToApproveCount.ToString & "Collection Account(s) deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToApproveCount = 0 And PassToApproveCount = 0 Then
                    LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, True, Me.gvAccountsDetails.PageSize)
                    UIUtilities.ShowDialog(Me, "Delete Collection Account", "[ " & FailToApproveCount.ToString & " ] Collection Account(s) can not be deleted due to following reasons: <br /> 1. Deleted associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

#End Region

#Region "Grid View"

    Private Sub LoadAccounts(ByVal PageNumber As Integer, ByVal DoDataBind As Boolean, ByVal PageSize As Integer)
        Try

            Dim GroupCode, CompanyCode As String
            GroupCode = ""
            CompanyCode = ""

            If ddlGroup.SelectedValue.ToString <> "0" Then
                GroupCode = ddlGroup.SelectedValue.ToString
            End If
            If ddlcompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlcompany.SelectedValue.ToString
            End If

            ICCollectionAccountsController.GetAllAccountsForRadGridByGroupAndCompanyCode(PageNumber, PageSize, DoDataBind, Me.gvAccountsDetails, GroupCode, CompanyCode)
            If gvAccountsDetails.Items.Count > 0 Then
                gvAccountsDetails.Visible = True
                lblRNF.Visible = False
                btnDeleteAccounts.Visible = CBool(htRights("Delete"))

                gvAccountsDetails.Columns(5).Visible = CBool(htRights("Delete"))
            Else
                gvAccountsDetails.Visible = False
                btnDeleteAccounts.Visible = False
                lblRNF.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub

    Protected Sub gvAccountsDetails_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAccountsDetails.NeedDataSource
        Try
            LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, False, Me.gvAccountsDetails.PageSize)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAccountsDetails_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles gvAccountsDetails.ItemCommand
        Try
            Dim objICCollectionAccounts As New ICCollectionAccounts
            Dim StrArray As String()
            objICCollectionAccounts.es.Connection.CommandTimeout = 3600
            If e.CommandName.ToString = "del" Then
                StrArray = e.CommandArgument.ToString.Split(";")
                If objICCollectionAccounts.LoadByPrimaryKey(StrArray(0).ToString, StrArray(1).ToString, StrArray(2).ToString()) Then
                    ICCollectionAccountsController.DeleteCollectionAccount(StrArray(0).ToString, StrArray(1).ToString, StrArray(2).ToString(), Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    UIUtilities.ShowDialog(Me, "Delete Collection Account", "Collection Account deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                    LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, True, Me.gvAccountsDetails.PageSize)
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Error ", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, False, Me.gvAccountsDetails.PageSize)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAccountsDetails_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles gvAccountsDetails.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAccountsDetails.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub gvAccountsDetails_PageIndexChanged(sender As Object, e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvAccountsDetails.PageIndexChanged
        Try
            gvAccountsDetails.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAccountsDetails_ItemCreated(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles gvAccountsDetails.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAccountsDetails.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Function CheckGVCompanySelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvCompanyRow As GridDataItem In gvAccountsDetails.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvCompanyRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

#End Region

#Region "Drop Down"

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
            LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, True, Me.gvAccountsDetails.PageSize)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlcompany_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlcompany.SelectedIndexChanged
        Try
            LoadAccounts(Me.gvAccountsDetails.CurrentPageIndex + 1, True, Me.gvAccountsDetails.PageSize)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            Dim dt As DataTable
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            dt = ICGroupController.GetAllActiveGroupsIsCollectionAllowonCompany()
            ddlGroup.DataSource = dt
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlcompany.Items.Clear()
            ddlcompany.Items.Add(lit)
            ddlcompany.AppendDataBoundItems = True
            ddlcompany.DataSource = ICCompanyController.GetAllActiveandApproveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString)
            ddlcompany.DataTextField = "CompanyName"
            ddlcompany.DataValueField = "CompanyCode"
            ddlcompany.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

End Class
