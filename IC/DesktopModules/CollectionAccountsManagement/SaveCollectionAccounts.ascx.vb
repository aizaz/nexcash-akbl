﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals

Partial Class DesktopModules_CollectionAccountsManagement_SaveCollectionAccounts
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private AccntNumber As String
    Private BranchCode As String
    Private Currency As String
    Private htRights As Hashtable

#Region "Page Load"

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            AccntNumber = Request.QueryString("AccountNumber").ToString()
            BranchCode = Request.QueryString("BranchCode").ToString()
            Currency = Request.QueryString("Currency").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlCompany()


                If AccntNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" Then
                    Clear()
                    lblPageHeader.Text = "Add Collection Account"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    lblPageHeader.Text = "Edit Collection Account"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    txtAccountNumber.ReadOnly = True
                    txtAccountTitle.ReadOnly = False
                    Dim objICCollectionAccounts As New ICCollectionAccounts
                    objICCollectionAccounts.es.Connection.CommandTimeout = 3600
                    objICCollectionAccounts.LoadByPrimaryKey(AccntNumber, BranchCode, Currency)
                    If objICCollectionAccounts.LoadByPrimaryKey(AccntNumber, BranchCode, Currency) Then
                        txtAccountTitle.Text = objICCollectionAccounts.AccountTitle.ToString()
                        txtAccountNumber.Text = objICCollectionAccounts.AccountNumber.ToString()
                        txtAccountCurrency.Text = objICCollectionAccounts.Currency

                        Dim objICCompany As New ICCompany
                        Dim objICGroup As New ICGroup


                        objICCompany.es.Connection.CommandTimeout = 3600
                        objICGroup.es.Connection.CommandTimeout = 3600


                        objICCompany = objICCollectionAccounts.UpToICCompanyByCompanyCode()
                        objICGroup = objICCompany.UpToICGroupByGroupCode()
                        LoadddlGroup()
                        ddlGroup.SelectedValue = objICGroup.GroupCode.ToString()
                        LoadddlCompany()
                        ddlGroup.Enabled = False
                        ddlCompany.Enabled = False
                        ddlCompany.SelectedValue = objICCompany.CompanyCode.ToString()
                        txtBranchCode.Text = objICCollectionAccounts.BranchCode
                        chkActive.Checked = objICCollectionAccounts.IsActive
                        'chkApproved.Checked = ICAccount.IsApproved
                    End If
                End If
                'btnSave.Visible = ArrRights(1)
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Collection Accounts Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        txtAccountTitle.Text = ""
        'txtAccountNumber.Text = ""
        txtAccountCurrency.Text = ""
        LoadddlGroup()
        LoadddlCompany()
        txtBranchCode.Text = ""

        chkActive.Checked = True
    End Sub

#End Region


#Region "Buttons"

    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim objICCollectionAccounts As New ICCollectionAccounts
                Dim objICCompany As New ICCompany
                Dim objICCollectionAccountsFrom As New ICCollectionAccounts
                Dim StrAction As String = ""
                Dim StrActionForCompany As String = Nothing
                objICCollectionAccounts.es.Connection.CommandTimeout = 3600
                objICCollectionAccountsFrom.es.Connection.CommandTimeout = 3600
                objICCompany.es.Connection.CommandTimeout = 3600

                objICCollectionAccounts.AccountTitle = txtAccountTitle.Text.ToString()
                objICCollectionAccounts.AccountNumber = txtAccountNumber.Text.ToString()
                objICCollectionAccounts.Currency = txtAccountCurrency.Text.ToString()
                objICCollectionAccounts.BranchCode = txtBranchCode.Text.ToString()
                objICCollectionAccounts.CompanyCode = ddlCompany.SelectedValue.ToString()
                objICCollectionAccounts.IsActive = chkActive.Checked


                If AccntNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" Then
                    objICCollectionAccounts.CreateBy = Me.UserId
                    objICCollectionAccounts.CreateDate = Date.Now
                    If CheckDuplicate(objICCollectionAccounts) = False Then
                        ICCollectionAccountsController.AddCollectionAccounts(objICCollectionAccounts, objICCollectionAccountsFrom, False, Me.UserId.ToString, Me.UserInfo.Username.ToString, "")
                        UIUtilities.ShowDialog(Me, "Save Collection Account", "Collection Account added successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Else
                        UIUtilities.ShowDialog(Me, "Save Collection Account", "Can not add duplicate Collection Account.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                        Clear()
                    End If
                Else
                    objICCollectionAccountsFrom.LoadByPrimaryKey(AccntNumber, BranchCode, Currency)
                    objICCompany.LoadByPrimaryKey(ddlCompany.SelectedValue.ToString)
                    objICCompany.IsApprove = False
                    StrAction += "Account updated from " & objICCollectionAccountsFrom.AccountNumber & " to " & objICCollectionAccounts.AccountNumber & " ; "
                    StrAction += "Branch Code from " & objICCollectionAccountsFrom.BranchCode & " to " & objICCollectionAccounts.BranchCode & " ; "
                    StrAction += "Currency from " & objICCollectionAccountsFrom.Currency & " to " & objICCollectionAccounts.Currency & " ; "
                    StrAction += "Company from " & objICCollectionAccountsFrom.UpToICCompanyByCompanyCode.CompanyName & " to " & objICCollectionAccounts.UpToICCompanyByCompanyCode.CompanyName & " ; "
                    StrAction += "Status from " & objICCollectionAccountsFrom.IsActive & " to " & objICCollectionAccounts.IsActive & " ; "

                    ICCollectionAccountsController.AddCollectionAccounts(objICCollectionAccounts, objICCollectionAccountsFrom, True, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction)

                    StrActionForCompany = Nothing
                    StrActionForCompany += "Compnay [ " & objICCompany.CompanyName & " ] is unapproved on updating collection account. Action was taken by user [ " & Me.UserId.ToString & " ] [ " & Me.UserInfo.Username.ToString & " ]"
                    ICCompanyController.AddCompany(objICCompany, True, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrActionForCompany)
                    UIUtilities.ShowDialog(Me, "Save Collection Account", "Collection Account updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                End If
                Clear()
                txtAccountNumber.Text = ""
            Catch ex As Exception
                If ex.Message.Contains("Incorrect syntax near the keyword") Then
                    UIUtilities.ShowDialog(Me, "Save Collection Account", "Can not add duplicate Collection Account.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Private Function CheckDuplicate(ByVal ICAccount As ICCollectionAccounts) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim objICCollectionAccounts As New ICCollectionAccounts
            Dim objICCollectionAccountsColl As New ICCollectionAccountsCollection

            objICCollectionAccounts.es.Connection.CommandTimeout = 3600
            objICCollectionAccountsColl.es.Connection.CommandTimeout = 3600

            If objICCollectionAccounts.LoadByPrimaryKey(ICAccount.AccountNumber, ICAccount.BranchCode, ICAccount.Currency) = True Then
                rslt = True
            End If

            objICCollectionAccountsColl.LoadAll()
            objICCollectionAccountsColl.Query.Where(objICCollectionAccountsColl.Query.AccountNumber = ICAccount.AccountNumber.ToString())
            If objICCollectionAccountsColl.Query.Load() Then
                rslt = True
            End If

            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

#End Region

#Region "Drop Downs"

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        LoadddlCompany()
        txtAccountNumber.Text = ""
        txtAccountTitle.Text = ""
        txtBranchCode.Text = ""
        txtAccountCurrency.Text = ""
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        txtAccountNumber.Text = ""
        txtAccountTitle.Text = ""
        txtBranchCode.Text = ""
        txtAccountCurrency.Text = ""
    End Sub

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            Dim dt As DataTable
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            dt = ICGroupController.GetAllActiveGroupsIsCollectionAllowonCompany()
            ddlGroup.DataSource = dt
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveandApproveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString)
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "TextBox"

    Protected Sub txtAccountNumber_TextChanged(sender As Object, e As System.EventArgs) Handles txtAccountNumber.TextChanged
        Try
            Dim StrAccountDetails As String()
            Dim AccountStatus As String = ""
            If Not txtAccountNumber.Text.ToString = "" And Not txtAccountNumber.Text.ToString = "Enter Account Number" Then
                Try
                    AccountStatus = CBUtilities.AccountStatus(txtAccountNumber.Text.ToString, ICBO.CBUtilities.AccountType.RB, "IC Client Account", txtAccountNumber.ToString, "Status", "Add", "").ToString
                Catch ex As Exception
                    UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End Try
                If AccountStatus = "Active" Then
                    Try
                        StrAccountDetails = (CBUtilities.TitleFetch(txtAccountNumber.Text.ToString, ICBO.CBUtilities.AccountType.RB, "IC Client Type", txtAccountNumber.Text.ToString).ToString.Split(";"))
                        txtAccountTitle.Text = StrAccountDetails(0).ToString
                        txtAccountCurrency.Text = StrAccountDetails(2).ToString
                        txtBranchCode.Text = StrAccountDetails(1).ToString
                    Catch ex As Exception
                        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End Try
                Else
                    UIUtilities.ShowDialog(Me, "Error", AccountStatus.ToString, ICBO.IC.Dialogmessagetype.Failure)
                    txtAccountNumber.Text = ""
                    txtAccountCurrency.Text = ""
                    txtAccountTitle.Text = ""
                    txtBranchCode.Text = ""
                    Exit Sub

                End If

            Else

                'UIUtilities.ShowDialog(Me, "Error", "Please enter valid account number.", ICBO.IC.Dialogmessagetype.Failure)
                txtAccountNumber.Text = ""
                txtAccountCurrency.Text = ""
                txtAccountTitle.Text = ""
                txtBranchCode.Text = ""
                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

End Class
