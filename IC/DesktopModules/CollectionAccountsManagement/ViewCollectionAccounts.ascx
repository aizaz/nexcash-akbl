﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewCollectionAccounts.ascx.vb"
    Inherits="DesktopModules_CollectionAccountsManagement_ViewCollectionAccounts" %>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure to remove this record ?") == true) {

            return true;
        }
        else {
            return false;
        }
    }
    function delcon(item) {


        if (window.confirm("Are you sure to remove this " + item + "?") == true) {

            return true;

        }
        else {

            return false;

        }
    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to remove Record(s)?") == true) {
            return true;
        }
        else {
            return false;
        }
    }  
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;

        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    //    }
</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table width="100%">
                <tr>
                    <td align="center" valign="top" colspan="4">
                        <asp:Button ID="btnSaveAccounts" runat="server" Text="Add Collection Account" CssClass="btn"
                            CausesValidation="False" />
                        <asp:HiddenField ID="hfAccountCode" runat="server" Visible="true" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" colspan="4">
                        <asp:Label ID="lblAccountList" runat="server" Text="Collection Accounts List" CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr style="width: 100%">
                    <td align="left" valign="middle" style="width: 25%">
                        <asp:Label ID="lblGroup" runat="server" Text="Group" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="middle" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="middle" style="width: 25%">
                        <asp:Label ID="lblCompany" runat="server" Text="Company" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="middle" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr style="width: 100%">
                    <td align="left" valign="middle" style="width: 25%">
                        <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="middle" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="middle" style="width: 25%">
                        <asp:DropDownList ID="ddlcompany" runat="server" CssClass="dropdown" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="middle" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr style="width: 100%">
                    <td align="left" valign="middle" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="middle" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="middle" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="middle" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" colspan="4">
                        <asp:Label ID="lblRNF" runat="server" Font-Bold="False" Text="No Record Found" Visible="False"
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" colspan="4">
                        <telerik:RadGrid ID="gvAccountsDetails" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="10">
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView DataKeyNames="AccountNumber,BranchCode,Currency">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Approve" DataField="IsApproved">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select" TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="Account Number" SortExpression="AccountNumber">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="AccountTitle" HeaderText="Account Title" SortExpression="AccountTitle">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Active" DataField="isActive">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridTemplateColumn HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveCollectionAccounts", "&mid=" & Me.ModuleId & "&AccountNumber="& Eval("AccountNumber") & "&BranchCode="& Eval("BranchCode") & "&Currency="& Eval("Currency") )%>'
                                                ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("AccountNumber") + ";" +Eval("BranchCode") + ";" +Eval("Currency") %>'
                                                CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="center" colspan="4">
                        <asp:Button ID="btnDeleteAccounts" runat="server" Text="Delete Collection Accounts"
                            CssClass="btn" OnClientClick="javascript: return conBukDelete();" CausesValidation="False" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
