﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveCollectionAccounts.ascx.vb" 
Inherits="DesktopModules_CollectionAccountsManagement_SaveCollectionAccounts" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    } 
</script>
<telerik:RadInputManager ID="radSaveAccounts" runat="server">
           <telerik:TextBoxSetting BehaviorID="radAccountTitle" Validation-IsRequired="true" ErrorMessage="Invalid Account Title" >
            <TargetControls>
                <telerik:TargetInput ControlID="txtAccountTitle" />
            </TargetControls>
        </telerik:TextBoxSetting>            
         <%--<telerik:TextBoxSetting BehaviorID="radAccountNo" Validation-IsRequired="true" ErrorMessage="Invalid Account No." EmptyMessage="Enter Account Number">
            <TargetControls>
                <telerik:TargetInput ControlID="txtAccountNumber" />
            </TargetControls>
        </telerik:TextBoxSetting>--%>
        <telerik:RegExpTextBoxSetting BehaviorID="radAccountNo" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9 ]+$" ErrorMessage="Invalid Account No."
        EmptyMessage="Enter Account Number">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountNumber" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
        <telerik:RegExpTextBoxSetting BehaviorID="radCurrency" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9 ]+$" ErrorMessage="Invalid Currency"
       >
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountCurrency" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
         <telerik:RegExpTextBoxSetting BehaviorID="radBranchCode" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9 ]+$" ErrorMessage="Invalid Branch Code"
      >
        <TargetControls>
            <telerik:TargetInput ControlID="txtBranchCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
 
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqGroup" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 28%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqCompany" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 22%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlGroup" runat="server" 
                ControlToValidate="ddlGroup" Display="Dynamic" 
                ErrorMessage="Please select Group" InitialValue="0" 
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 33%">
            <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" 
                ControlToValidate="ddlCompany" Display="Dynamic" 
                ErrorMessage="Please select Company" InitialValue="0" 
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 17%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAccountNumber" runat="server" Text="Account Number" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqAccountNo" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAccountTitle" runat="server" Text="Account Title" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtAccountNumber" runat="server" CssClass="txtbox" 
                MaxLength="50" AutoPostBack="True"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtAccountTitle" runat="server" CssClass="txtbox" 
                MaxLength="30" ReadOnly="True"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCurrency" runat="server" Text="Account Currency" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBranchCode" runat="server" Text="Branch Code" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtAccountCurrency" runat="server" CssClass="txtbox" 
                MaxLength="50" ReadOnly="True"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtBranchCode" runat="server" CssClass="txtbox" 
                MaxLength="10" ReadOnly="True"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIsActive" runat="server" Text="Is Active" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkActive" runat="server" Text=" " CssClass="chkBox" Checked="True" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
            </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False"
               />
            &nbsp;
        </td>
    </tr>
</table>
