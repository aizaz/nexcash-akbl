﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddViewSettings.ascx.vb" Inherits="DesktopModules_PaymentsSettings_AddViewSettings" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<style type="text/css">
    .auto-style1 {
        width: 25%;
        height: 19px;
    }
</style>

<telerik:RadInputManager ID="rad" runat="server">

    <telerik:TextBoxSetting BehaviorID="radDomain" Validation-IsRequired="true" EmptyMessage="" ErrorMessage="Invalid Domain">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDomain" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radPassword" Validation-IsRequired="false" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radServer" Validation-IsRequired="true" EmptyMessage="" ErrorMessage="Invalid Server">
        <TargetControls>
            <telerik:TargetInput ControlID="txtServer" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radUserName" Validation-IsRequired="true" EmptyMessage="" ErrorMessage="Invalid User Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtUserName" />
        </TargetControls>
    </telerik:TextBoxSetting>

    <telerik:TextBoxSetting BehaviorID="radNIFTCMSCode" Validation-IsRequired="true" EmptyMessage="" ErrorMessage="Invalid Code">
        <TargetControls>
            <telerik:TargetInput ControlID="txtNIFTCMSCode" />
        </TargetControls>
    </telerik:TextBoxSetting>


    <telerik:RegExpTextBoxSetting BehaviorID="rad2FAFailAttempt" Validation-IsRequired="False"
        ValidationExpression="[0-9]{1,2}" ErrorMessage="Invalid 2FA Fail attempt count" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txt2FAFailAttempts" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="rad2FAExpire" Validation-IsRequired="False"
        ValidationExpression="[0-9]{1,3}" ErrorMessage="Invalid 2FA Expire Limit" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txt2FATimeOut" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>

    <telerik:TextBoxSetting BehaviorID="radAppURL" Validation-IsRequired="true" EmptyMessage="" ErrorMessage="Invalid User Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAppURL" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radSign" Validation-IsRequired="true" EmptyMessage="" ErrorMessage="Invalid User Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtEmailSign" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radPhysicalPath" Validation-IsRequired="true" EmptyMessage="" ErrorMessage="Invalid User Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPhysicalPath" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:NumericTextBoxSetting BehaviorID="reIBFTLimit"
        Validation-IsRequired="true" DecimalDigits="2" AllowRounding="false" MinValue="1"
        ErrorMessage="Invalid IBFT Limit">
        <TargetControls>
            <telerik:TargetInput ControlID="txtIBFTLimit" />
        </TargetControls>
    </telerik:NumericTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="REUserIDforFailure" Validation-IsRequired="true" Validation-ValidationGroup ="UserIDforFailure"
        ValidationExpression="^([a-zA-Z0-9_\-\.]+)@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$"
        ErrorMessage="Invalid Email" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtUserIDforFailure" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
</telerik:RadInputManager>

<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblHeading" runat="server" Text="Basic Settings" CssClass="headingblue"></asp:Label>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;          
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;     
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblEmailAddressSetupforInvoiceDBImport" runat="server" Text="Exchange Server Settings" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%"></td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;          
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;     
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="auto-style1">
            <asp:Label ID="lblServer" runat="server" Text="Server" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqServer" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" class="auto-style1">&nbsp;
        </td>
        <td align="left" valign="top" class="auto-style1">
            <asp:Label ID="lblDomain" runat="server" Text="Domain" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqDomain" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" class="auto-style1">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtServer" runat="server" CssClass="txtbox" MaxLength="100"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtDomain" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;          
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;     
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblUserName" runat="server" Text="User Name" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqUserName" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblPassword" runat="server" Text="Password" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqPassword" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtUserName" runat="server" CssClass="txtbox"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="txtbox" MaxLength="15"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:LinkButton ID="lnkPassword" runat="server" CausesValidation="False" Enabled="False">Change Password</asp:LinkButton>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="auto-style1">
            <asp:Label ID="lbl2FASetting" runat="server" Text="2 FA Settings" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" class="auto-style1"></td>
        <td align="left" valign="top" class="auto-style1"></td>
        <td align="left" valign="top" class="auto-style1"></td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblLPSDays" runat="server" Text="2FA Fail Attempts"
                CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqUserName0" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lbl2FATimeOut" runat="server" Text="2FA Time Out"
                CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqUserName1" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txt2FAFailAttempts" runat="server" CssClass="txtbox"
                MaxLength="2"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txt2FATimeOut" runat="server" CssClass="txtbox"
                MaxLength="3"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAppLogo" runat="server" Text="Application URL &amp; Signature" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAppLogo0" runat="server" Text="Application URL" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqUserName2" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAppLogo1" runat="server" Text="Email Signature" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqUserName3" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtAppURL" runat="server" CssClass="txtbox"
                MaxLength="100"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtEmailSign" runat="server" CssClass="txtbox"
                MaxLength="150"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAppPath" runat="server" Text="Application Path" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqUserName4" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%"></td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;<asp:TextBox ID="txtPhysicalPath" runat="server" CssClass="txtbox"
            MaxLength="500"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;     
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <%--  <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior10" ValidationExpression="[0-9]{3}$"
        ErrorMessage="Enter LPS Days" EmptyMessage="Please Enter LPS Days" Validation-IsRequired="true">
        <TargetControls>
            <telerik:TargetInput ControlID="txtLPSDays" />
        </TargetControls>
        </telerik:RegExpTextBoxSetting--%>
    </tr>
    <%-- ================--%>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <%--  <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior10" ValidationExpression="[0-9]{3}$"
        ErrorMessage="Enter LPS Days" EmptyMessage="Please Enter LPS Days" Validation-IsRequired="true">
        <TargetControls>
            <telerik:TargetInput ControlID="txtLPSDays" />
        </TargetControls>
        </telerik:RegExpTextBoxSetting--%>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;     
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAppLogo2" runat="server" Text="Other Settings" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIBFTLimit" runat="server" Text="IBFT Limit" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqIBFTLimit" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblNIFTCMSCode" runat="server" Text="NIFT CMS Code" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqNIFTCMSCode" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <%-- ================--%><asp:TextBox ID="txtIBFTLimit" runat="server" CssClass="txtbox"
                MaxLength="14"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;<asp:TextBox ID="txtNIFTCMSCode" runat="server" CssClass="txtbox"
            MaxLength="14"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>


    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>


    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblStaleFundTransfer" runat="server" Text="Stale Funds Transfer" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqStaleTransfer" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblUserIDforFailure" runat="server" Text="User ID(s) for Failure Events" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>


    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">

            <span>
                <asp:DropDownList runat="server" ID="ddlStaleTransfer" CssClass="dropdown">
                    <asp:ListItem Value="Allow">Allow</asp:ListItem>
                    <asp:ListItem Value="No">Not Allow</asp:ListItem>
                </asp:DropDownList>

        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtUserIDforFailure" runat="server" CssClass="txtbox" MaxLength="50" ValidationGroup="UserIDforFailure"></asp:TextBox>
            <asp:ImageButton ID="btnAddUserIDforFailure" runat="server" CssClass="button" ImageUrl="~/images/add.gif"
                Text="Add" CausesValidation="true" ValidationGroup="UserIDforFailure"/>


        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>

    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:ListBox ID="lstboxUserIDforFailure" runat="server" Width="250px" Height="70px"></asp:ListBox>
             <br />
            <asp:CheckBox ID="ChkRemoveAll" runat="server" Text=" Remove All" />
            <asp:ImageButton ID="btnClear" runat="server" CssClass="button" ImageUrl="~/images/delete.gif"
                Text="Clear" Width="16px" CausesValidation="false" />
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>

     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>

    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="70px" CausesValidation="true" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" Width="70px" CausesValidation="False" />
        </td>
    </tr>
</table>
