﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_PaymentsSettings_AddViewSettings
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase

    Private htRights As Hashtable

#Region "Page Load"
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then

                If ICUtilities.CheckIsAdminOrSuperUser(Me.PortalId, Me.UserId) Then
                    CheckFieldsExists()
                Else
                    If Not ViewState("htRights") Is Nothing Then
                        htRights = DirectCast(ViewState("htRights"), Hashtable)
                    End If

                    GetPageAccessRights()

                    If CBool(htRights("Manage")) Then
                        CheckFieldsExists()
                    Else
                        UIUtilities.ShowDialog(Me, "Error", "You do not have access.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                        Exit Sub
                    End If
                End If



            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Basic Settings")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Exit Sub
        End Try
    End Sub
#End Region

#Region "Other Functions"
    Private Sub CheckFieldsExists()
        Try
            If Not ICUtilities.GetSettingValue("Server") Is Nothing Then
                txtServer.Text = ICUtilities.GetSettingValue("Server")
            Else
                txtServer.Text = Nothing
            End If

            If Not ICUtilities.GetSettingValue("Domain") Is Nothing Then
                txtDomain.Text = ICUtilities.GetSettingValue("Domain")
            Else
                txtDomain.Text = Nothing
            End If
            If Not ICUtilities.GetSettingValue("UserName") Is Nothing Then
                txtUserName.Text = ICUtilities.GetSettingValue("UserName")
            Else
                txtUserName.Text = Nothing
            End If
            If Not ICUtilities.GetSettingValue("NIFTCMSCODE") Is Nothing Then
                txtNIFTCMSCode.Text = ICUtilities.GetSettingValue("NIFTCMSCODE")
            Else
                txtNIFTCMSCode.Text = Nothing
            End If
            If Not ICUtilities.GetSettingValue("StaleTransfer") Is Nothing Then
                ddlStaleTransfer.SelectedValue = ICUtilities.GetSettingValue("StaleTransfer")
            Else
                ddlStaleTransfer.SelectedValue = "No"
            End If
            If Not ICUtilities.GetSettingValue("2FAMaxFailAttempts") Is Nothing Then
                txt2FAFailAttempts.Text = ICUtilities.GetSettingValue("2FAMaxFailAttempts")
            Else
                txt2FAFailAttempts.Text = Nothing
            End If
            If Not ICUtilities.GetSettingValue("2FATimeOut") Is Nothing Then
                txt2FATimeOut.Text = ICUtilities.GetSettingValue("2FATimeOut")
            Else
                txt2FATimeOut.Text = Nothing
            End If
            If Not ICUtilities.GetSettingValue("ApplicationURL") Is Nothing Then
                txtAppURL.Text = ICUtilities.GetSettingValue("ApplicationURL")
            Else
                txtAppURL.Text = Nothing
            End If
            If Not ICUtilities.GetSettingValue("EmailSignature") Is Nothing Then
                txtEmailSign.Text = ICUtilities.GetSettingValue("EmailSignature")
            Else
                txtEmailSign.Text = Nothing
            End If
            If Not ICUtilities.GetSettingValue("PhysicalApplicationPath") Is Nothing Then
                txtPhysicalPath.Text = ICUtilities.GetSettingValue("PhysicalApplicationPath")
            Else
                txtPhysicalPath.Text = Nothing
            End If
            If ICUtilities.GetSettingValue("IBFTLimit") <> "" Then
                txtIBFTLimit.Text = CDbl(ICUtilities.GetSettingValue("IBFTLimit"))
            Else
                txtIBFTLimit.Text = CDbl(0.0)
            End If
            If Not ICUtilities.GetSettingValue("Password") Is Nothing Then
                txtPassword.Text = EncryptDecryptUtilities.Decrypt(ICUtilities.GetSettingValue("Password"))
                If txtPassword.Text.Trim = "" Then
                    rad.GetSettingByBehaviorID("radPassword").Validation.IsRequired = True
                    txtPassword.Enabled = True
                    lnkPassword.Enabled = False
                Else
                    rad.GetSettingByBehaviorID("radPassword").Validation.IsRequired = False
                    txtPassword.Enabled = False
                    lnkPassword.Enabled = True
                End If
            Else
                txtPassword.Text = Nothing
                rad.GetSettingByBehaviorID("radPassword").Validation.IsRequired = True
                txtPassword.Enabled = True
                lnkPassword.Enabled = False
            End If
             
            If Not ICUtilities.GetSettingValue("UserIDForFailureEvent") Is Nothing Then
                If Not ICUtilities.GetSettingValue("UserIDForFailureEvent").Trim = "" Then
                    Dim list As String()
                    Dim listOffields As String = String.Empty
                    listOffields = ICUtilities.GetSettingValue("UserIDForFailureEvent")

                    If listOffields.Contains(";") Then
                        list = listOffields.Split(";")
                        lstboxUserIDforFailure.DataSource = list
                        lstboxUserIDforFailure.DataBind()
                    Else
                        lstboxUserIDforFailure.Items.Add(ICUtilities.GetSettingValue("UserIDForFailureEvent"))
                    End If
                End If
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

#Region "Button Events"
    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL(41), False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim ActionStr As String = ""
                ActionStr = "Settings"
                ICUtilities.SetSettingValue("Server", txtServer.Text)
                ICUtilities.SetSettingValue("Domain", txtDomain.Text)
                ICUtilities.SetSettingValue("UserName", txtUserName.Text)
                If txtPassword.Enabled = True Then
                    ICUtilities.SetSettingValue("Password", EncryptDecryptUtilities.Encrypt(txtPassword.Text))
                End If
                ICUtilities.SetSettingValue("2FAMaxFailAttempts", txt2FAFailAttempts.Text)
                ICUtilities.SetSettingValue("2FATimeOut", txt2FATimeOut.Text)
                ICUtilities.SetSettingValue("ApplicationURL", txtAppURL.Text)
                ICUtilities.SetSettingValue("EmailSignature", txtEmailSign.Text)
                ICUtilities.SetSettingValue("PhysicalApplicationPath", txtPhysicalPath.Text)
                ICUtilities.SetSettingValue("NIFTCMSCODE", txtNIFTCMSCode.Text)
                ICUtilities.SetSettingValue("StaleTransfer", ddlStaleTransfer.SelectedValue.ToString)
                ICUtilities.SetSettingValue("IBFTLimit", CDbl(txtIBFTLimit.Text).ToString)

                Dim str As New StringBuilder
                If lstboxUserIDforFailure.Items.Count > 0 Then
                    For i As Integer = 0 To lstboxUserIDforFailure.Items.Count - 1
                        If i = lstboxUserIDforFailure.Items.Count - 1 Then
                            str.Append(lstboxUserIDforFailure.Items(i).Value)
                        Else
                            str.Append(lstboxUserIDforFailure.Items(i).Value & ";")
                        End If
                    Next
                End If

                ICUtilities.SetSettingValue("UserIDForFailureEvent", str.ToString())


                UIUtilities.ShowDialog(Me, "Basic Settings", "Settings saved successfully.", ICBO.IC.Dialogmessagetype.Success)
                ICUtilities.AddAuditTrail(ActionStr & " Added ", "Payment Settings", Nothing, Me.UserId, Me.UserInfo.Username.ToString(), "ADD")

                txtPassword.Enabled = False
                lnkPassword.Enabled = True
                rad.GetSettingByBehaviorID("radPassword").Validation.IsRequired = False

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Protected Sub lnkPassword_Click(sender As Object, e As EventArgs) Handles lnkPassword.Click
        txtPassword.Enabled = True
        rad.GetSettingByBehaviorID("radPassword").Validation.IsRequired = True
    End Sub

    Private Sub btnClear_Click(sender As Object, e As ImageClickEventArgs) Handles btnClear.Click
        Try
            If lstboxUserIDforFailure.Items.Count > 0 Then
                If ChkRemoveAll.Checked = False Then
                    If lstboxUserIDforFailure.SelectedItem Is Nothing Then
                        UIUtilities.ShowDialog(Me, "Basic Settings", "Please select item to remove.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    lstboxUserIDforFailure.Items.Remove(lstboxUserIDforFailure.SelectedItem)
                Else
                    lstboxUserIDforFailure.Items.Clear()
                    ChkRemoveAll.Checked = False
                End If
            Else
                UIUtilities.ShowDialog(Me, "Basic Settings", "No item to remove.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub btnAddUserIDforFailure_Click(sender As Object, e As ImageClickEventArgs) Handles btnAddUserIDforFailure.Click
        If Page.IsValid Then
            Try
                lstboxUserIDforFailure.Items.Add(txtUserIDforFailure.Text)
                txtUserIDforFailure.Text = ""

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub


#End Region

    
End Class
