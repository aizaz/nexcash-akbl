﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_BankRoleAccountPaymentNature_BankRoleAccountPaymentNatureTagging
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private UserCode As String
    Private AccountNumber As String
    Private BranchCode As String
    Private Currency As String
    Private PaymentNatureCode As String
    Private ProductTypeCode As String
    Private CompanyCode As String
    Private htRights As Hashtable
    Public DisburstmentMode As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            AccountNumber = Request.QueryString("AccountNumber").ToString()
            BranchCode = Request.QueryString("BranchCode").ToString()
            Currency = Request.QueryString("Currency").ToString()
            PaymentNatureCode = Request.QueryString("PaymentNatureCode").ToString()
            ProductTypeCode = Request.QueryString("ProductTypeCode").ToString()
            CompanyCode = Request.QueryString("CompanyCode").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlCompany()
                LoadddlAccountPaymentNature()
                LoadddlProductType()
                gvOnlineFormSettings.Style.Add("display", "none")
                gvAssignedFileds.Style.Add("display", "none")

                If AccountNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" And PaymentNatureCode.ToString() = "0" And ProductTypeCode.ToString() = "0" And CompanyCode.ToString() = "0" Then
                    lblPageHeader.Text = "Add Single Instruction Form Settings"
                    btnSave.Text = "Save"
                    lblIsApproved.Visible = False
                    chkApprove.Visible = False
                    btnApprove.Visible = False
                    lblAssignedFieldList.Visible = False
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    lblPageHeader.Text = "Edit Single Instruction Form Settings"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    Dim objICCompany As New ICCompany
                    Dim objICGroup As New ICGroup
                    Dim objICAccountPaymentNature As New ICAccountsPaymentNature
                    Dim objICProductType As New ICProductType
                    Dim objICOnlineFormSettin As New ICOnlineFormSettings
                    objICCompany.es.Connection.CommandTimeout = 3600
                    objICGroup.es.Connection.CommandTimeout = 3600
                    objICAccountPaymentNature.es.Connection.CommandTimeout = 3600
                    objICProductType.es.Connection.CommandTimeout = 3600
                    objICOnlineFormSettin.es.Connection.CommandTimeout = 3600
                    objICOnlineFormSettin = ICSingleInstructionFormSettingController.GetOnlineFormSettingSingleObjectByAPNPTypeAndCompanyCode(AccountNumber, BranchCode, Currency, PaymentNatureCode, ProductTypeCode, CompanyCode)
                    objICCompany = objICOnlineFormSettin.UpToICCompanyByCompanyCode
                    LoadddlGroup()
                    ddlGroup.SelectedValue = objICCompany.GroupCode
                    LoadddlCompany()
                    ddlCompany.SelectedValue = objICCompany.CompanyCode
                    LoadddlAccountPaymentNature()
                    ddlAccPNature.SelectedValue = objICOnlineFormSettin.AccountNumber + "-" + objICOnlineFormSettin.BranchCode + "-" + objICOnlineFormSettin.Currency + "-" + objICOnlineFormSettin.PaymentNatureCode
                    LoadddlProductType()
                    ddlProductType.SelectedValue = objICOnlineFormSettin.ProductTypeCode
                    objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)
                    DisburstmentMode = objICProductType.DisbursementMode
                    LoadTemplateFieldsList(True)
                    LoadAssignedFieldsList(True)

                    MarkCheckedGVFieldsListOnDisbMode(objICProductType.DisbursementMode)
                    CheckMarkMainGridByAssignedFields()
                    ddlGroup.Enabled = False
                    ddlCompany.Enabled = False
                    ddlAccPNature.Enabled = False
                    ddlProductType.Enabled = False
                End If
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub CheckMarkMainGridByAssignedFields()
        Dim objICOnlineFormSettingColl As New ICOnlineFormSettingsCollection
        Dim FieldID As Integer = Nothing
        objICOnlineFormSettingColl.es.Connection.CommandTimeout = 3600
        Dim chkSelect, chkVisible, chkIsRequired, chkIsApproved As CheckBox

        objICOnlineFormSettingColl = ICSingleInstructionFormSettingController.GetOnlineFormSettingsByAPNPTypeAndCompanyCode(ddlAccPNature.SelectedValue.Split("-")(0), ddlAccPNature.SelectedValue.Split("-")(1), ddlAccPNature.SelectedValue.Split("-")(2), ddlAccPNature.SelectedValue.Split("-")(3), ddlProductType.SelectedValue.ToString, ddlCompany.SelectedValue.ToString)
        If objICOnlineFormSettingColl.Count > 0 Then
            For Each objICSingleInstructionFormSetting As ICOnlineFormSettings In objICOnlineFormSettingColl
                For Each gvOnlineFormSettingsRow As GridDataItem In gvOnlineFormSettings.Items
                    FieldID = Nothing
                    FieldID = gvOnlineFormSettingsRow.GetDataKeyValue("FieldID")
                    If objICSingleInstructionFormSetting.FieldID = FieldID Then
                        chkSelect = New CheckBox
                        chkVisible = New CheckBox
                        chkIsRequired = New CheckBox
                        chkIsApproved = New CheckBox

                        chkSelect = DirectCast(gvOnlineFormSettingsRow.Cells(0).FindControl("chkSelect"), CheckBox)
                        chkVisible = DirectCast(gvOnlineFormSettingsRow.Cells(2).FindControl("isVisible"), CheckBox)
                        chkIsRequired = DirectCast(gvOnlineFormSettingsRow.Cells(3).FindControl("isRequired"), CheckBox)
                        chkIsApproved = DirectCast(gvOnlineFormSettingsRow.Cells(4).FindControl("isApproved"), CheckBox)
                        chkIsRequired.Checked = objICSingleInstructionFormSetting.IsRequired
                        chkVisible.Checked = objICSingleInstructionFormSetting.IsVisible
                        If objICSingleInstructionFormSetting.IsVisible = True Or objICSingleInstructionFormSetting.IsRequired = True Then
                            chkSelect.Checked = True
                        End If
                        If Not objICSingleInstructionFormSetting.IsApproved Is Nothing Then
                            chkIsApproved.Checked = objICSingleInstructionFormSetting.IsApproved
                        Else
                            chkIsApproved.Checked = False
                        End If
                    End If
                Next
            Next
        End If
    End Sub


    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client Single Instruction Form Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub MarkCheckedGVFieldsListOnDisbMode(ByVal DisbMode As String)
        Dim objICFieldsListColl As New ICFieldsListCollection
        Dim FieldID As Integer = Nothing
        objICFieldsListColl.es.Connection.CommandTimeout = 3600
        Dim chkSelect, chkVisible, chkIsRequired As CheckBox

        objICFieldsListColl = ICTemplateFieldListController.GetFieldsListByDisbursementMode(DisbMode)
        If objICFieldsListColl.Count > 0 Then
            For Each objICFieldsList As ICFieldsList In objICFieldsListColl
                For Each gvOnlineFormSettingsRow As GridDataItem In gvOnlineFormSettings.Items
                    FieldID = Nothing
                    FieldID = gvOnlineFormSettingsRow.GetDataKeyValue("FieldID")
                    If objICFieldsList.FieldID = FieldID Then
                        chkSelect = DirectCast(gvOnlineFormSettingsRow.Cells(0).FindControl("chkSelect"), CheckBox)
                        chkVisible = DirectCast(gvOnlineFormSettingsRow.Cells(2).FindControl("isVisible"), CheckBox)
                        chkIsRequired = DirectCast(gvOnlineFormSettingsRow.Cells(3).FindControl("isRequired"), CheckBox)
                        chkIsRequired.Checked = objICFieldsList.MustRequired
                        chkVisible.Checked = objICFieldsList.MustRequired
                        chkSelect.Checked = objICFieldsList.MustRequired
                        If chkIsRequired.Checked = True Then
                            chkIsRequired.Enabled = False
                        End If
                        If chkVisible.Checked = True Then
                            chkVisible.Enabled = False
                        End If
                        If chkSelect.Checked = True Then
                            chkSelect.Enabled = False
                        End If

                    End If
                Next
            Next
        End If
    End Sub
    Private Sub LoadddlAccountPaymentNature()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlAccPNature.Items.Clear()
            ddlAccPNature.Items.Add(lit)
            ddlAccPNature.AppendDataBoundItems = True
            ddlAccPNature.DataSource = ICAccountPaymentNatureController.GetAccountPaymentNatureByCompanyCode(ddlCompany.SelectedValue.ToString)
            ddlAccPNature.DataTextField = "AccountAndPaymentNature"
            ddlAccPNature.DataValueField = "AccountPaymentNature"
            ddlAccPNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlProductType()
        Try
            Dim objICAPNature As New ICAccountsPaymentNature
            Dim StrArray As String() = Nothing
            objICAPNature.es.Connection.CommandTimeout = 3600
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlProductType.Items.Clear()
            ddlProductType.Items.Add(lit)
            ddlProductType.AppendDataBoundItems = True
            If ddlAccPNature.SelectedValue.ToString <> "0" Then
                StrArray = ddlAccPNature.SelectedValue.Split("-")
                If objICAPNature.LoadByPrimaryKey(StrArray(0).ToString, StrArray(1).ToString, StrArray(2).ToString, StrArray(3).ToString) Then
                    ddlProductType.DataSource = ICAccountPaymentNatureProductTypeController.GetAllProductTypesByAccountAndPaymentNature(objICAPNature)
                    ddlProductType.DataTextField = "ProductTypeName"
                    ddlProductType.DataValueField = "ProductTypeCode"
                    ddlProductType.DataBind()
                End If
            End If


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    'Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged
    '    Try
    '        Dim objICProductType As New ICProductType
    '        objICProductType.es.Connection.CommandTimeout = 3600
    '        LoadTemplateFieldsList(True)
    '        If ddlProductType.SelectedValue.ToString <> "0" Then
    '            If objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString) Then
    '                MarkCheckedGVFieldsListOnDisbMode(objICProductType.DisbursementMode)
    '                DisburstmentMode = objICProductType.DisbursementMode.ToString
    '            End If
    '            If Not AccountNumber.ToString() = "0" Then
    '                LoadAssignedFieldsList(True)
    '            End If

    '        Else
    '            lblRNF.Visible = False
    '            lblAssignedFieldList.Visible = False
    '        End If
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged
        Try
            Dim objICProductType As New ICProductType
            objICProductType.es.Connection.CommandTimeout = 3600
            objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)
            DisburstmentMode = objICProductType.DisbursementMode.ToString
            LoadTemplateFieldsList(True)
            If ddlProductType.SelectedValue.ToString <> "0" Then
               
                If Not AccountNumber.ToString() = "0" Then
                    LoadAssignedFieldsList(True)
                    If objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString) Then
                        'MarkCheckedGVFieldsListOnDisbMode(objICProductType.DisbursementMode)
                        ' DisburstmentMode = objICProductType.DisbursementMode.ToString
                    End If
                End If

            Else
                lblRNF.Visible = False
                lblAssignedFieldList.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Public Shared Sub GetAllTemplateFieldsForSingleInstructionFormSettingsRadGrid(ByVal DisbursementMode As String, ByVal IsOnlineForm As Boolean, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
        Dim objICFieldsListColl As New ICFieldsListCollection

        Dim dt As New DataTable
        objICFieldsListColl.Query.Select(objICFieldsListColl.Query.FieldID, objICFieldsListColl.Query.FieldName)

        If DisbursementMode = "Direct Credit" Then
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredFT.Case.When(objICFieldsListColl.Query.IsRequiredFT = True).Then("True").Else("False").End.As("IsRequired"))
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredFT.Case.When(objICFieldsListColl.Query.IsRequiredFT = True).Then("False").Else("True").End.As("IsMustRequired"))

            'objICFieldsListColl.Query.Select(objICFieldsListColl.Query.MustRequired.Case.When(objICFieldsListColl.Query.MustRequired = True).Then("True").Else("False").End.As("ChkRequired"))

        ElseIf DisbursementMode = "PO" Then
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredPO.Case.When(objICFieldsListColl.Query.IsRequiredPO = True).Then("True").Else("False").End.As("IsRequired"))
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredPO.Case.When(objICFieldsListColl.Query.IsRequiredPO = True).Then("False").Else("True").End.As("IsMustRequired"))

        ElseIf DisbursementMode = "Cheque" Then
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredCheque.Case.When(objICFieldsListColl.Query.IsRequiredCheque = True).Then("True").Else("False").End.As("IsRequired"))
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredCheque.Case.When(objICFieldsListColl.Query.IsRequiredCheque = True).Then("False").Else("True").End.As("IsMustRequired"))

        ElseIf DisbursementMode = "DD" Then
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredDD.Case.When(objICFieldsListColl.Query.IsRequiredDD = True).Then("True").Else("False").End.As("IsRequired"))
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredDD.Case.When(objICFieldsListColl.Query.IsRequiredDD = True).Then("False").Else("True").End.As("IsMustRequired"))

        ElseIf DisbursementMode = "Other Credit" Then
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredIBFT.Case.When(objICFieldsListColl.Query.IsRequiredIBFT = True).Then("True").Else("False").End.As("IsRequired"))
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredIBFT.Case.When(objICFieldsListColl.Query.IsRequiredIBFT = True).Then("False").Else("True").End.As("IsMustRequired"))

        ElseIf DisbursementMode = "COTC" Then
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredCOTC.Case.When(objICFieldsListColl.Query.IsRequiredCOTC = True).Then("True").Else("False").End.As("IsRequired"))
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredCOTC.Case.When(objICFieldsListColl.Query.IsRequiredCOTC = True).Then("False").Else("True").End.As("IsMustRequired"))
        ElseIf DisbursementMode = "Bill Payment" Then
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredBillPayment.Case.When(objICFieldsListColl.Query.IsRequiredBillPayment = True).Then("True").Else("False").End.As("IsRequired"))
            objICFieldsListColl.Query.Select(objICFieldsListColl.Query.IsRequiredBillPayment.Case.When(objICFieldsListColl.Query.IsRequiredBillPayment = True).Then("False").Else("True").End.As("IsMustRequired"))

        End If


        objICFieldsListColl.Query.Where(objICFieldsListColl.Query.IsRequiredOnlineForm = True)

        If IsOnlineForm = False Then
            If DisbursementMode <> "Cheque" Then
                objICFieldsListColl.Query.Where(objICFieldsListColl.Query.FieldID <> 62)
            End If
        End If

        objICFieldsListColl.Query.OrderBy(objICFieldsListColl.Query.FieldOrder, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
        dt = objICFieldsListColl.Query.LoadDataTable
        If Not PageNumber = 0 Then
            objICFieldsListColl.Query.es.PageNumber = PageNumber
            objICFieldsListColl.Query.es.PageSize = PageSize
            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        Else
            objICFieldsListColl.Query.es.PageNumber = 1
            objICFieldsListColl.Query.es.PageSize = PageSize
            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        End If



    End Sub
    Private Sub LoadTemplateFieldsList(ByVal DoDataBind As Boolean)
        Try

            GetAllTemplateFieldsForSingleInstructionFormSettingsRadGrid(DisburstmentMode, True, Me.gvOnlineFormSettings.CurrentPageIndex + 1, Me.gvOnlineFormSettings.PageSize, Me.gvOnlineFormSettings, True)

            If gvOnlineFormSettings.Items.Count > 0 Then
                gvOnlineFormSettings.Style.Remove("display")

                If AccountNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" And PaymentNatureCode.ToString() = "0" And ProductTypeCode.ToString() = "0" And CompanyCode.ToString() = "0" Then
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    btnSave.Visible = CBool(htRights("Update"))
                End If
                ApplyJavaScript()
            Else

                btnSave.Visible = False
                gvOnlineFormSettings.Style.Add("display", "none")
            End If


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub LoadAssignedFieldsList(ByVal DoDataBind As Boolean)
        Try

            ICSingleInstructionFormSettingController.GetOnlineFormSettingsAssignedFieldsForRadGrid(AccountNumber, BranchCode, Currency, PaymentNatureCode, ProductTypeCode, CompanyCode, Me.gvAssignedFileds.CurrentPageIndex + 1, Me.gvAssignedFileds.PageSize.ToString, Me.gvAssignedFileds, DoDataBind)

            If gvAssignedFileds.Items.Count > 0 Then
                gvAssignedFileds.Style.Remove("display")
                lblRNF.Visible = False
                lblIsApproved.Visible = CBool(htRights("Approve"))
                chkApprove.Visible = CBool(htRights("Approve"))
                btnApprove.Visible = CBool(htRights("Approve"))
                lblAssignedFieldList.Visible = True
            Else
                lblAssignedFieldList.Visible = True
                gvAssignedFileds.Style.Add("display", "none")
                lblRNF.Visible = True
                lblIsApproved.Visible = False
                chkApprove.Visible = False
                btnApprove.Visible = False
            End If


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        LoadddlCompany()
        LoadddlAccountPaymentNature()
        LoadddlProductType()
        gvAssignedFileds.Style.Add("display", "none")
        gvOnlineFormSettings.Style.Add("display", "none")
    End Sub


    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        LoadddlAccountPaymentNature()
        LoadddlProductType()
        gvAssignedFileds.Style.Add("display", "none")
        gvOnlineFormSettings.Style.Add("display", "none")
    End Sub

    Protected Sub ddlAccPNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAccPNature.SelectedIndexChanged

        Try

            LoadddlProductType()
            gvAssignedFileds.Style.Add("display", "none")
            gvOnlineFormSettings.Style.Add("display", "none")
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

   
    Private Function CheckGVSingleInstructionFormSelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvSingleInstructionFormRow As GridDataItem In gvOnlineFormSettings.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvSingleInstructionFormRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Sub DeleteOnlineFormSettingsOnUpDate()


        Dim objOnlineFormSettingColl As New ICOnlineFormSettingsCollection
        objOnlineFormSettingColl.es.Connection.CommandTimeout = 3600
        objOnlineFormSettingColl = ICSingleInstructionFormSettingController.GetOnlineFormSettingsByAPNPTypeAndCompanyCode(AccountNumber.ToString, BranchCode.ToString, Currency.ToString, PaymentNatureCode.ToString, ProductTypeCode.ToString, CompanyCode.ToString)
        Try
            If objOnlineFormSettingColl.Count > 0 Then
                For Each objOnlineFormSetting As ICOnlineFormSettings In objOnlineFormSettingColl
                    ICSingleInstructionFormSettingController.DeleteOnlineFormSettings(objOnlineFormSetting.OnlineFormSettingsID, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                Next
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                'Dim gvOnlineFormSettingRow As GridDataItem
                Dim objICOnlineFormSetting As ICOnlineFormSettings
                Dim chkSelect, chkVisible, chkRequired As CheckBox
                Dim StrArray As String()
                Dim CountRecord As Integer = 0

                If CheckGVSingleInstructionFormSelected() = False Then
                    UIUtilities.ShowDialog(Me, "Single Instruction Form Settings", "Please select atleast one single instruction form setting.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If AccountNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" And PaymentNatureCode.ToString() = "0" And ProductTypeCode.ToString() = "0" And CompanyCode.ToString() = "0" Then


                    For Each gvOnlineFormSettingRow As GridDataItem In gvOnlineFormSettings.Items
                        chkSelect = DirectCast(gvOnlineFormSettingRow.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            objICOnlineFormSetting = New ICOnlineFormSettings
                            StrArray = Nothing
                            objICOnlineFormSetting.es.Connection.CommandTimeout = 3600
                            StrArray = ddlAccPNature.SelectedValue.Split("-")
                            chkVisible = DirectCast(gvOnlineFormSettingRow.Cells(2).FindControl("isVisible"), CheckBox)
                            chkRequired = DirectCast(gvOnlineFormSettingRow.Cells(3).FindControl("isRequired"), CheckBox)
                            objICOnlineFormSetting.AccountNumber = StrArray(0).ToString
                            objICOnlineFormSetting.BranchCode = StrArray(1).ToString
                            objICOnlineFormSetting.Currency = StrArray(2).ToString
                            objICOnlineFormSetting.PaymentNatureCode = StrArray(3).ToString
                            objICOnlineFormSetting.ProductTypeCode = ddlProductType.SelectedValue.ToString
                            objICOnlineFormSetting.CompanyCode = ddlCompany.SelectedValue.ToString
                            objICOnlineFormSetting.FieldID = gvOnlineFormSettingRow.GetDataKeyValue("FieldID")
                            objICOnlineFormSetting.IsVisible = chkVisible.Checked
                            objICOnlineFormSetting.IsRequired = chkRequired.Checked
                            objICOnlineFormSetting.CreatedBy = Me.UserId
                            objICOnlineFormSetting.CreatedDate = Date.Now
                            objICOnlineFormSetting.Creater = Me.UserId
                            objICOnlineFormSetting.CreationDate = Date.Now
                            If CountRecord = 0 Then
                                If CheckDuplicateOnlineFormSettings(objICOnlineFormSetting) = True Then
                                    UIUtilities.ShowDialog(Me, "Single Instruction Form Settings", "Duplicate single instruction form settings are not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                                    Exit Sub
                                End If
                            End If
                            ICSingleInstructionFormSettingController.AddSingleInstructionFormSettings(objICOnlineFormSetting, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        End If
                        CountRecord = 1
                    Next
                    UIUtilities.ShowDialog(Me, "Single Instruction Form Settings", "Single instruction form settings added successfully", ICBO.IC.Dialogmessagetype.Success)
                    clear()
                    CountRecord = 0
                    Exit Sub
                Else
                    objICOnlineFormSetting = ICSingleInstructionFormSettingController.GetOnlineFormSettingSingleObjectByAPNPTypeAndCompanyCode(AccountNumber, BranchCode, Currency, PaymentNatureCode, ProductTypeCode, CompanyCode)
                    DeleteOnlineFormSettingsOnUpDate()
                    'If CheckDuplicateOnlineFormSettings(objICOnlineFormSetting) = True Then
                    '    UIUtilities.ShowDialog(Me, "Single Instruction Form Settings", "Duplicate single instruction form settings are not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                    '    Exit Sub
                    'Else

                    For Each gvOnlineFormSettingRow As GridDataItem In gvOnlineFormSettings.Items
                        chkSelect = DirectCast(gvOnlineFormSettingRow.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            objICOnlineFormSetting = New ICOnlineFormSettings
                            StrArray = Nothing
                            objICOnlineFormSetting.es.Connection.CommandTimeout = 3600
                            StrArray = ddlAccPNature.SelectedValue.Split("-")
                            chkVisible = DirectCast(gvOnlineFormSettingRow.Cells(2).FindControl("isVisible"), CheckBox)
                            chkRequired = DirectCast(gvOnlineFormSettingRow.Cells(3).FindControl("isRequired"), CheckBox)
                            objICOnlineFormSetting.AccountNumber = StrArray(0).ToString
                            objICOnlineFormSetting.BranchCode = StrArray(1).ToString
                            objICOnlineFormSetting.Currency = StrArray(2).ToString
                            objICOnlineFormSetting.PaymentNatureCode = StrArray(3).ToString
                            objICOnlineFormSetting.ProductTypeCode = ddlProductType.SelectedValue.ToString
                            objICOnlineFormSetting.CompanyCode = ddlCompany.SelectedValue.ToString
                            objICOnlineFormSetting.FieldID = gvOnlineFormSettingRow.GetDataKeyValue("FieldID")
                            objICOnlineFormSetting.IsVisible = chkVisible.Checked
                            objICOnlineFormSetting.IsRequired = chkRequired.Checked
                            objICOnlineFormSetting.CreatedBy = Me.UserId
                            objICOnlineFormSetting.CreatedDate = Date.Now
                            objICOnlineFormSetting.Creater = Me.UserId
                            objICOnlineFormSetting.CreationDate = Date.Now
                            If CountRecord = 0 Then
                                If CheckDuplicateOnlineFormSettings(objICOnlineFormSetting) = True Then
                                    UIUtilities.ShowDialog(Me, "Single Instruction Form Settings", "Duplicate single instruction form settings are not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                                    Exit Sub
                                End If
                            End If

                            ICSingleInstructionFormSettingController.AddSingleInstructionFormSettings(objICOnlineFormSetting, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)

                        End If
                        CountRecord = 1
                    Next
                    UIUtilities.ShowDialog(Me, "Single Instruction Form Settings", "Single instruction form settings updated successfully", ICBO.IC.Dialogmessagetype.Success, NavigateURL)
                    clear()
                    CountRecord = 0
                End If

                'Exit Sub
                'End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Sub clear()
        LoadddlGroup()
        LoadddlCompany()
        LoadddlAccountPaymentNature()
        LoadddlProductType()
        gvAssignedFileds.Style.Add("display", "none")
        gvOnlineFormSettings.Style.Add("display", "none")
    End Sub
    Private Function CheckDuplicateOnlineFormSettings(ByVal objICOnlineFormSettings As ICOnlineFormSettings) As Boolean
        Dim objICOnlineFormSettingColl As New ICOnlineFormSettingsCollection
        Dim Result As Boolean = False
        objICOnlineFormSettingColl.es.Connection.CommandTimeout = 3600

        objICOnlineFormSettingColl.Query.Where(objICOnlineFormSettingColl.Query.AccountNumber = objICOnlineFormSettings.AccountNumber And objICOnlineFormSettingColl.Query.BranchCode = objICOnlineFormSettings.BranchCode And objICOnlineFormSettingColl.Query.Currency = objICOnlineFormSettings.Currency And objICOnlineFormSettingColl.Query.PaymentNatureCode = objICOnlineFormSettings.PaymentNatureCode And objICOnlineFormSettingColl.Query.ProductTypeCode = objICOnlineFormSettings.ProductTypeCode And objICOnlineFormSettingColl.Query.CompanyCode = objICOnlineFormSettings.CompanyCode)
       
       

        If objICOnlineFormSettingColl.Query.Load Then
            Result = True
        End If

        Return Result
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL)
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Page.Validate()
        If Page.IsValid Then
            Try
                Dim objICOnlineFormSetting As ICOnlineFormSettings
                'Dim chkSelect As CheckBox
                Dim FieldID As Integer = 0
                If chkApprove.Checked = False Then
                    UIUtilities.ShowDialog(Me, "Single Instruction Form Settings", "Please select approve check box to approve single instruction form settings.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                objICOnlineFormSetting = ICSingleInstructionFormSettingController.GetOnlineFormSettingSingleObjectByAPNPTypeAndCompanyCode(AccountNumber, BranchCode, Currency, PaymentNatureCode, ProductTypeCode, CompanyCode)
                If Me.UserInfo.IsSuperUser = False Then
                    If objICOnlineFormSetting.CreatedBy = Me.UserId Then
                        UIUtilities.ShowDialog(Me, "Single Instruction Form Settings", "Single instruction form settings must be approved by user other than maker.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                For Each gvOnlineFormSettingRow As GridDataItem In gvAssignedFileds.Items

                    FieldID = 0
                    FieldID = gvOnlineFormSettingRow.GetDataKeyValue("FieldID")
                    ICSingleInstructionFormSettingController.AproveOnlineFormSettings(FieldID.ToString, AccountNumber, BranchCode, Currency, PaymentNatureCode, ProductTypeCode, CompanyCode, chkApprove.Checked, Me.UserId.ToString, Me.UserInfo.Username.ToString)

                Next
                UIUtilities.ShowDialog(Me, "Single Instruction Form Settings", "Single instruction form settings approved successfully", ICBO.IC.Dialogmessagetype.Success, NavigateURL)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
            'Try
            '    Dim objICOnlineFormSetting As ICOnlineFormSettings
            '    'Dim chkSelect As CheckBox
            '    Dim FieldID As Integer = 0
            '    If chkApprove.Checked = False Then
            '        UIUtilities.ShowDialog(Me, "Single Instruction Form Settings", "Please select approve check box to approve single instruction form settings.", ICBO.IC.Dialogmessagetype.Failure)
            '        Exit Sub
            '    End If
            '    objICOnlineFormSetting = ICSingleInstructionFormSettingController.GetOnlineFormSettingSingleObjectByAPNPTypeAndCompanyCode(AccountNumber, BranchCode, Currency, PaymentNatureCode, ProductTypeCode, CompanyCode)
            '    If Me.UserInfo.IsSuperUser = False Then
            '        If objICOnlineFormSetting.CreatedBy = Me.UserId Then
            '            UIUtilities.ShowDialog(Me, "Single Instruction Form Settings", "Single instruction form settings must be approved by other than maker.", ICBO.IC.Dialogmessagetype.Failure)
            '            Exit Sub
            '        End If
            '    End If
            '    For Each gvOnlineFormSettingRow As GridDataItem In gvAssignedFileds.Items

            '        FieldID = 0
            '        FieldID = gvOnlineFormSettingRow.GetDataKeyValue("FieldID")
            '        ICSingleInstructionFormSettingController.AproveOnlineFormSettings(FieldID.ToString, chkApprove.Checked, Me.UserId.ToString, Me.UserInfo.Username.ToString)

            '    Next
            '    UIUtilities.ShowDialog(Me, "Single Instruction Form Settings", "Single instruction form settings approved successfully", ICBO.IC.Dialogmessagetype.Success, NavigateURL)
            'Catch ex As Exception
            '    ProcessModuleLoadException(Me, ex, False)
            '    UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            'End Try
        End If
    End Sub

    Protected Sub gvAssignedFileds_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles gvAssignedFileds.ItemDataBound
        Try
          
            If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
                Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)

                If item.GetDataKeyValue("IsApproved") = True Then
                    chkApprove.Checked = True
                Else
                    chkApprove.Checked = False
                End If


            End If

           
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ApplyJavaScript()
        If gvOnlineFormSettings.Items.Count > 0 Then
            For Each gvAmendmentFieldListRow As GridDataItem In gvOnlineFormSettings.Items
                Dim chkSelect, chkVisibile, chkRequired As CheckBox
                chkSelect = New CheckBox
                chkVisibile = New CheckBox
                chkRequired = New CheckBox
                chkSelect = DirectCast(gvAmendmentFieldListRow.Cells(0).FindControl("chkSelect"), CheckBox)
                chkVisibile = DirectCast(gvAmendmentFieldListRow.Cells(2).FindControl("isVisible"), CheckBox)
                chkRequired = DirectCast(gvAmendmentFieldListRow.Cells(3).FindControl("isRequired"), CheckBox)
                chkSelect.Attributes("onclick") = "CheckInGridView('" & chkSelect.ClientID & "','" & chkVisibile.ClientID & "','" & chkRequired.ClientID & "');"
                chkVisibile.Attributes("onclick") = "CheckInGridAll('" & chkSelect.ClientID & "','" & chkVisibile.ClientID & "','" & chkRequired.ClientID & "');"
                chkRequired.Attributes("onclick") = "CheckInGridAll('" & chkSelect.ClientID & "','" & chkVisibile.ClientID & "','" & chkRequired.ClientID & "');"
            Next
        End If
    End Sub
End Class
