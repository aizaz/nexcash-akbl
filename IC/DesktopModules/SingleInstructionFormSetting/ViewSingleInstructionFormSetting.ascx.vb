﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.Configuration
Imports System.Web.Configuration
Imports Telerik.Web.UI

Partial Class DesktopModules_SingleInstructionformSetting_ViewSingleInstructionFormSetting
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_DesktopModules_SingleInstructionformSetting_ViewSingleInstructionFormSetting_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlCompany()
                LoadgvOnlineFormSettingsList(True)
                btnAddSingleFormSettings.Visible = CBool(htRights("Add"))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client Single Instruction Form Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


#Region "Button Events"
    Protected Sub btnSaveAccPayNatProTyp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddSingleFormSettings.Click
        If Page.IsValid Then
            'Dim str As String = NavigateURL("SaveSingleInstructionFormSettings", "&mid=" & Me.ModuleId & "&AccountNumber=0" & "&BranchCode=0" & "&Currency=0" & "&PaymentNatureCode=0" & "&ProductTypeCode=0" & "&CompanyCode=0")
            Response.Redirect(NavigateURL("SaveSingleInstructionFormSettings", "&mid=" & Me.ModuleId & "&AccountNumber=0" & "&BranchCode=0" & "&Currency=0" & "&PaymentNatureCode=0" & "&ProductTypeCode=0" & "&CompanyCode=0"), False)
        End If
    End Sub
#End Region

    Private Sub LoadgvOnlineFormSettingsList(ByVal DoDataBind As Boolean)
        Try
            Dim GroupCode, CompanyCode As String

            If ddlGroup.SelectedValue.ToString <> "0" Then
                GroupCode = ddlGroup.SelectedValue.ToString
            Else
                GroupCode = ""
            End If
            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            Else
                CompanyCode = ""
            End If
            ICSingleInstructionFormSettingController.GetAllSingleInstructionFormSettingsForRadGrid(GroupCode, CompanyCode, Me.gvSingleInstructionForm.CurrentPageIndex + 1, Me.gvSingleInstructionForm.PageSize, Me.gvSingleInstructionForm, DoDataBind)

            If gvSingleInstructionForm.Items.Count > 0 Then
                gvSingleInstructionForm.Visible = True
                lblRNF.Visible = False
                gvSingleInstructionForm.Columns(7).Visible = CBool(htRights("Delete"))
                btnBulkDeleteSingleInstructionForm.Visible = CBool(htRights("Delete"))
                btnBulkApproveSingleInstructionForm0.Visible = CBool(htRights("Approve"))
            Else
                gvSingleInstructionForm.Visible = False
                btnBulkDeleteSingleInstructionForm.Visible = False
                btnBulkApproveSingleInstructionForm0.Visible = False
                lblRNF.Visible = True

            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString)
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#Region "DropDown Events"
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        LoadddlCompany()
        LoadgvOnlineFormSettingsList(True)
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        LoadgvOnlineFormSettingsList(True)
    End Sub
#End Region

    Protected Sub gvSingleInstructionForm_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvSingleInstructionForm.ItemCommand
        Try

            Dim objICOnlineFormSettingsColl As New ICOnlineFormSettingsCollection

            objICOnlineFormSettingsColl.es.Connection.CommandTimeout = 3600
            Dim StrArray As String() = Nothing
            If e.CommandName.ToString = "del" Then
                If Page.IsValid Then
                    StrArray = e.CommandArgument.ToString.Split(";")
                    objICOnlineFormSettingsColl = ICSingleInstructionFormSettingController.GetOnlineFormSettingsByAPNPTypeAndCompanyCode(StrArray(0).ToString, StrArray(1).ToString, StrArray(2).ToString, StrArray(3).ToString, StrArray(4).ToString, StrArray(5).ToString)
                    For Each objICOnlineFormSetting As ICOnlineFormSettings In objICOnlineFormSettingsColl
                        ICSingleInstructionFormSettingController.DeleteOnlineFormSettings(objICOnlineFormSetting.OnlineFormSettingsID, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    Next
                    UIUtilities.ShowDialog(Me, "Delete Single Instruction Form Settings", "Single instruction form settings deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                    LoadgvOnlineFormSettingsList(True)
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Error ", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                LoadgvOnlineFormSettingsList(False)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub gvAPNPType_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvSingleInstructionForm.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                ' Dim arrPageSizes() As String = {"1", "2", "5", "10", "50"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvSingleInstructionForm.MasterTableView.ClientID)
                Next
                'If gvAccountsDetails.Items.Count > 0 Then
                '    myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
                'End If
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAPNPType_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvSingleInstructionForm.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvSingleInstructionForm.MasterTableView.ClientID & "','0');"
        End If
    End Sub
    Private Function CheckGVSingleInstructionFormSelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvSingleInstructionFormRow As GridDataItem In gvSingleInstructionForm.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvSingleInstructionFormRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnDeleteAPNPType_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBulkDeleteSingleInstructionForm.Click
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim PassToDeleteCount As Integer = 0
                Dim FailToDeleteCount As Integer = 0
                Dim objOnlineFormSettingColl As ICOnlineFormSettingsCollection
                Dim AccountNumber, BranchCode, Currency, ProductTypeCode, PaymentNatureCode, CompanyCode As String

                If CheckGVSingleInstructionFormSelected() = False Then
                    UIUtilities.ShowDialog(Me, "Delete Single Instruction Form Settings", "Please select atleast one single instruction form setting.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVAccounts In gvSingleInstructionForm.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVAccounts.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        AccountNumber = ""
                        BranchCode = ""
                        Currency = ""
                        ProductTypeCode = ""
                        PaymentNatureCode = ""
                        CompanyCode = ""
                        AccountNumber = rowGVAccounts.GetDataKeyValue("AccountNumber").ToString
                        BranchCode = rowGVAccounts.GetDataKeyValue("BranchCode").ToString
                        Currency = rowGVAccounts.GetDataKeyValue("Currency").ToString
                        ProductTypeCode = rowGVAccounts.GetDataKeyValue("ProductTypeCode").ToString
                        PaymentNatureCode = rowGVAccounts.GetDataKeyValue("PaymentNatureCode").ToString
                        CompanyCode = rowGVAccounts.GetDataKeyValue("CompanyCode").ToString
                        objOnlineFormSettingColl = New ICOnlineFormSettingsCollection
                        objOnlineFormSettingColl.es.Connection.CommandTimeout = 3600
                        objOnlineFormSettingColl = ICSingleInstructionFormSettingController.GetOnlineFormSettingsByAPNPTypeAndCompanyCode(AccountNumber.ToString, BranchCode.ToString, Currency.ToString, PaymentNatureCode.ToString, ProductTypeCode.ToString, CompanyCode.ToString)
                        Try
                            If objOnlineFormSettingColl.Count > 0 Then
                                For Each objOnlineFormSetting As ICOnlineFormSettings In objOnlineFormSettingColl

                                    ICSingleInstructionFormSettingController.DeleteOnlineFormSettings(objOnlineFormSetting.OnlineFormSettingsID, Me.UserId.ToString, Me.UserInfo.Username.ToString)

                                Next
                            End If
                            PassToDeleteCount = PassToDeleteCount + 1
                        Catch ex As Exception
                            FailToDeleteCount = FailToDeleteCount + 1
                        End Try
                    End If
                Next

                If Not PassToDeleteCount = 0 And Not FailToDeleteCount = 0 Then
                    LoadgvOnlineFormSettingsList(True)
                    UIUtilities.ShowDialog(Me, "Delete Single Instruction Form Settings", "[ " & PassToDeleteCount.ToString & " ] Single instruction form settings deleted successfuly.<br /> [ " & FailToDeleteCount.ToString & " ] Single instruction form settings can not be deleted due to following reasons:<br /> 1. Deleted associated record(s) first.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToDeleteCount = 0 And FailToDeleteCount = 0 Then
                    LoadgvOnlineFormSettingsList(True)
                    UIUtilities.ShowDialog(Me, "Delete Single Instruction Form Settings", PassToDeleteCount.ToString & "Single instruction form settings deleted successfuly.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToDeleteCount = 0 And PassToDeleteCount = 0 Then
                    LoadgvOnlineFormSettingsList(True)
                    UIUtilities.ShowDialog(Me, "Delete Single Instruction Form Settings", "[ " & FailToDeleteCount.ToString & " ] Single instruction form settings can not be deleted due to following reasons: <br /> 1. Deleted associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub gvSingleInstructionForm_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvSingleInstructionForm.NeedDataSource
        Try
            LoadgvOnlineFormSettingsList(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnBulkApproveSingleInstructionForm0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBulkApproveSingleInstructionForm0.Click
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim PassToApproveCount As Integer = 0
                Dim FailToApproveCount As Integer = 0
                Dim objOnlineFormSettingColl As ICOnlineFormSettingsCollection
                Dim AccountNumber, BranchCode, Currency, ProductTypeCode, PaymentNatureCode, CompanyCode As String
                Dim FieldID As Integer = 0
                If CheckGVSingleInstructionFormSelected() = False Then
                    UIUtilities.ShowDialog(Me, "Approve Single Instruction Form Settings", "Please select atleast one single instruction form setting.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVAccounts In gvSingleInstructionForm.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVAccounts.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        AccountNumber = ""
                        BranchCode = ""
                        Currency = ""
                        ProductTypeCode = ""
                        PaymentNatureCode = ""
                        CompanyCode = ""
                        AccountNumber = rowGVAccounts.GetDataKeyValue("AccountNumber").ToString
                        BranchCode = rowGVAccounts.GetDataKeyValue("BranchCode").ToString
                        Currency = rowGVAccounts.GetDataKeyValue("Currency").ToString
                        ProductTypeCode = rowGVAccounts.GetDataKeyValue("ProductTypeCode").ToString
                        PaymentNatureCode = rowGVAccounts.GetDataKeyValue("PaymentNatureCode").ToString
                        CompanyCode = rowGVAccounts.GetDataKeyValue("CompanyCode").ToString

                        Dim objICOnlineFormSetting As ICOnlineFormSettings
                        objICOnlineFormSetting = ICSingleInstructionFormSettingController.GetOnlineFormSettingSingleObjectByAPNPTypeAndCompanyCode(AccountNumber, BranchCode, Currency, PaymentNatureCode, ProductTypeCode, CompanyCode)
                        If Me.UserInfo.IsSuperUser = False Then
                            If objICOnlineFormSetting.CreatedBy = Me.UserId Then
                                FailToApproveCount = FailToApproveCount + 1
                                Continue For
                            Else
                                If objICOnlineFormSetting.IsApproved = True Then
                                    FailToApproveCount = FailToApproveCount + 1
                                    Continue For
                                Else
                                    objOnlineFormSettingColl = New ICOnlineFormSettingsCollection
                                    objOnlineFormSettingColl.es.Connection.CommandTimeout = 3600
                                    objOnlineFormSettingColl = ICSingleInstructionFormSettingController.GetOnlineFormSettingsByAPNPTypeAndCompanyCode(AccountNumber.ToString, BranchCode.ToString, Currency.ToString, PaymentNatureCode.ToString, ProductTypeCode.ToString, CompanyCode.ToString)

                                    If objOnlineFormSettingColl.Count > 0 Then
                                        For Each objOnlineFormSetting As ICOnlineFormSettings In objOnlineFormSettingColl

                                            FieldID = 0
                                            FieldID = objOnlineFormSetting.FieldID
                                            ICSingleInstructionFormSettingController.AproveOnlineFormSettings(FieldID.ToString, AccountNumber, BranchCode, Currency, PaymentNatureCode, ProductTypeCode, CompanyCode, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)

                                        Next
                                    End If
                                    PassToApproveCount = PassToApproveCount + 1
                                    Continue For
                                End If


                            End If
                        Else

                            If objICOnlineFormSetting.IsApproved = True Then
                                FailToApproveCount = FailToApproveCount + 1
                                Continue For
                            Else
                                objOnlineFormSettingColl = New ICOnlineFormSettingsCollection
                                objOnlineFormSettingColl.es.Connection.CommandTimeout = 3600
                                objOnlineFormSettingColl = ICSingleInstructionFormSettingController.GetOnlineFormSettingsByAPNPTypeAndCompanyCode(AccountNumber.ToString, BranchCode.ToString, Currency.ToString, PaymentNatureCode.ToString, ProductTypeCode.ToString, CompanyCode.ToString)

                                If objOnlineFormSettingColl.Count > 0 Then
                                    For Each objOnlineFormSetting As ICOnlineFormSettings In objOnlineFormSettingColl

                                        FieldID = 0
                                        FieldID = objOnlineFormSetting.FieldID
                                        ICSingleInstructionFormSettingController.AproveOnlineFormSettings(FieldID.ToString, AccountNumber, BranchCode, Currency, PaymentNatureCode, ProductTypeCode, CompanyCode, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)


                                    Next
                                End If
                                PassToApproveCount = PassToApproveCount + 1
                                Continue For
                            End If
                        End If



                    End If
                Next

                If Not PassToApproveCount = 0 And Not FailToApproveCount = 0 Then
                    LoadgvOnlineFormSettingsList(True)
                    UIUtilities.ShowDialog(Me, "Approve Single Instruction Form Settings", PassToApproveCount.ToString & "  Single instruction form settings approved successfuly.<br />  " & FailToApproveCount.ToString & "  Single instruction form settings can not be approved due to following reasons:<br /> 1. Single instruction form settings are already approved <br /> 2. Single instruction form settings must be approved by other than maker", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToApproveCount = 0 And FailToApproveCount = 0 Then
                    LoadgvOnlineFormSettingsList(True)
                    UIUtilities.ShowDialog(Me, "Approve Single Instruction Form Settings", PassToApproveCount.ToString & " Single instruction form settings approved successfuly.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToApproveCount = 0 And PassToApproveCount = 0 Then
                    LoadgvOnlineFormSettingsList(True)
                    UIUtilities.ShowDialog(Me, "Approve Single Instruction Form Settings", FailToApproveCount.ToString & "  Single instruction form settings can not be approved due to following reasons: <br /> 1. Single instruction form settings are already approved <br /> 2. Single instruction form settings must be approved by other than maker", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
End Class
