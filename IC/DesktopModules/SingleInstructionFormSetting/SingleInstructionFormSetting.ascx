﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SingleInstructionFormSetting.ascx.vb"
    Inherits="DesktopModules_BankRoleAccountPaymentNature_BankRoleAccountPaymentNatureTagging" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:RegExpTextBoxSetting BehaviorID="REUserName" Validation-IsRequired="true"
        ValidationExpression="\b(\w*)\b(.)*\b(-)*\b(_)*\b" ErrorMessage="Invalid Name"
        EmptyMessage="Enter User Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtUserName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior7" Validation-IsRequired="true"
        ValidationExpression="\b(\w*)\b(.)*\b(-)*\b(_)*\b" ErrorMessage="Invalid Display  Name"
        EmptyMessage="Enter Display Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDisplayName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="false"
        EmptyMessage="Enter Role Type">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRoleType" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<script type="text/javascript">
    function CheckInGridAll(chkView, chkAmend, chkAmendBulk) {

        if (document.getElementById(chkAmend).checked == true && document.getElementById(chkAmendBulk).checked == false) {
            document.getElementById(chkView).checked = true;
            document.getElementById(chkAmend).checked = true;
        }

        if (document.getElementById(chkAmendBulk).checked == true && document.getElementById(chkAmend).checked == false) {
            document.getElementById(chkView).checked = true;
            document.getElementById(chkAmendBulk).checked = true;
            document.getElementById(chkAmend).checked = true;
        }
        if (document.getElementById(chkAmend).checked == false && document.getElementById(chkAmendBulk).checked == false) {
            document.getElementById(chkView).checked = false;
            document.getElementById(chkAmendBulk).checked = false;
            document.getElementById(chkAmend).checked = false;
        }
    }
</script>
<script type="text/javascript">
    function CheckInGridView(chkView, chkAmend, chkAmendBulk) {

        if (document.getElementById(chkView).checked == true) {
            document.getElementById(chkAmend).checked = true;
        }
        else {
            document.getElementById(chkView).checked = false;
            document.getElementById(chkAmend).checked = false;
            document.getElementById(chkAmendBulk).checked = false;
        }
    }
</script>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
    function ValidateChkList(source, arguments) {

        arguments.IsValid = IsCheckBoxChecked() ? true : false;



    }

</script>



<script language="javascript" type="text/javascript">
    function AdjustWidth(ddl) {
        var maxWidth = 0;
        var Counter = 0;
        for (var i = 0; i < ddl.length; i++) {
            if (ddl.options[i].text.length > maxWidth) {
                Counter = Counter + 1;
                maxWidth = ddl.options[i].text.length;
            }
        }
        if (Counter = 1) { ddl.style.width = "250px"; }

        else {
            alert(Counter);
            ddl.style.width = maxWidth + "px";
        }
        //-->
    }
</script>
<style type="text/css">
.ctrDropDown{
    width:250px;
   
}
.ctrDropDownClick{
    
    width:600px;
 
}
.plainDropDown{
    width:250px;
   
}
</style>


<table style="width: 100%">
    <tr>
        <td style="width: 100%" colspan="4">
            <asp:Label ID="lblPageHeader" runat="server" Text="Single Input Form Setting"
                CssClass="headingblue"></asp:Label>
            <br />
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqGroup" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqCompany" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlGroup" runat="server" ControlToValidate="ddlGroup"
                Display="Dynamic" ErrorMessage="Please select Group" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" ControlToValidate="ddlCompany"
                Display="Dynamic" ErrorMessage="Please select Company" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:Label ID="lblSelectAccountPNature" runat="server" Text="Select Account Payment Nature"
                CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqAccPNature" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:Label ID="lblProductType" runat="server" Text="Select Product Type" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqProductType" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <%--<asp:DropDownList ID="ddlAccPNature" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>--%>
              <div style= "width:250px; overflow:hidden;">
            <asp:DropDownList ID="ddlAccPNature" runat="server" class="ctrDropDown"  onblur="this.className='ctrDropDown';" onmousedown="this.className='ctrDropDownClick';" onchange="this.className='ctrDropDown';" AutoPostBack="true">
            </asp:DropDownList>
            </div>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:DropDownList ID="ddlProductType" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlAccPNature" runat="server" ControlToValidate="ddlAccPNature"
                Display="Dynamic" ErrorMessage="Please select Account Payment Nature" InitialValue="0"
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlProductType" runat="server" ControlToValidate="ddlProductType"
                Display="Dynamic" ErrorMessage="Please select Product Type" InitialValue="0"
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            &nbsp;</td>
        <td style="width: 25%">
            &nbsp;</td>
        <td style="width: 25%">
            &nbsp;</td>
        <td style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr>
        <td style="width: 25%">
            &nbsp;</td>
        <td style="width: 25%">
            &nbsp;</td>
        <td style="width: 25%">
            &nbsp;</td>
        <td style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%">
        </td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%">
           <%-- <asp:Panel ID="pnlOnlineFormFields" runat="server" Width="100%" Height="250px" ScrollBars="Vertical" style="display:none">--%>
            
            <telerik:RadGrid ID="gvOnlineFormSettings" runat="server" AllowPaging="True" AllowSorting="false"
                AutoGenerateColumns="False" CellSpacing="0" PageSize="100" ShowFooter="True">
                <ClientSettings>
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                </ClientSettings>
                <AlternatingItemStyle CssClass="rgAltRow" />
                <MasterTableView DataKeyNames="FieldID" TableLayout="Fixed">
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                    </ExpandCollapseColumn>
                    <Columns>
                    <telerik:GridTemplateColumn headerText="Select">
        
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" Text="" Checked='<%# Bind("IsRequired")%>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="FieldName" HeaderText="Name" SortExpression="FieldName">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn headerText="Is Visible">
                            <%--<HeaderTemplate>
                                <asp:CheckBox ID="isVisibleAll" runat="server" Text="Is Visible" />
                            </HeaderTemplate>--%>
                            <ItemTemplate>
                                <asp:CheckBox ID="isVisible" runat="server" Text="" Checked='<%# Bind("IsRequired")%>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn headerText="Is Required">
                           <%-- <HeaderTemplate>
                                <asp:CheckBox ID="isRequiredAll" runat="server" Text="Is Required" />
                            </HeaderTemplate>--%>
                            <ItemTemplate>
                                <asp:CheckBox ID="isRequired" runat="server" Text="" Checked='<%# Bind("IsRequired")%>' />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                         <telerik:GridTemplateColumn headerText="Is Approved">
                           <%-- <HeaderTemplate>
                                <asp:CheckBox ID="isRequiredAll" runat="server" Text="Is Required" />
                            </HeaderTemplate>--%>
                            <ItemTemplate>
                                <asp:CheckBox ID="isApproved" runat="server" Text="" Enabled="false"/>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
                <ItemStyle CssClass="rgRow" />
            </telerik:RadGrid>
            
          <%--  </asp:Panel>--%>
        </td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%">
            &nbsp;</td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%" align="center">
            <asp:Button ID="btnSave" runat="server" Text="Save" Width="77px" CssClass="btn" 
                />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="75px" CssClass="btnCancel" 
                 CausesValidation="False" />
        </td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%">
            &nbsp;</td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%">
          <%--  <asp:Panel ID="pnlAssignedFields" runat="server" Height="250px" style="display:none"
                ScrollBars="Vertical" Width="100%">--%>
                <asp:Label ID="lblAssignedFieldList" runat="server" CssClass="headingblue" 
                    Text="Already Assigned Fields List"></asp:Label>
                <br /><br /><asp:Label ID="lblRNF" runat="server" Font-Bold="False" 
                            Text="No Record Found" Visible="False" CssClass="headingblue"></asp:Label><br />
                <telerik:RadGrid ID="gvAssignedFileds" runat="server" AllowPaging="True" 
                    AllowSorting="false" AutoGenerateColumns="False" CellSpacing="0" PageSize="100" 
                    ShowFooter="True">
                    <ClientSettings>
                    <Scrolling  UseStaticHeaders="true" />
                    </ClientSettings>
                    <AlternatingItemStyle CssClass="rgAltRow" />
                    <MasterTableView DataKeyNames="FieldID,IsApproved" TableLayout="Fixed">
                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                            Visible="True">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                            Visible="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn DataField="FieldName" HeaderText="Name" 
                                SortExpression="FieldName">
                            </telerik:GridBoundColumn>
                            
                             <telerik:GridBoundColumn DataField="IsVisible" HeaderText="Is Visible" 
                                SortExpression="IsVisible">
                            </telerik:GridBoundColumn>
                             <telerik:GridBoundColumn DataField="IsRequired" HeaderText="Is Required" 
                                SortExpression="IsRequired">
                            </telerik:GridBoundColumn>
                            <telerik:GridCheckBoxColumn DataField="IsApproved" HeaderText="Is Approved"></telerik:GridCheckBoxColumn>
                        </Columns>
                    </MasterTableView>
                    <ItemStyle CssClass="rgRow" />
                </telerik:RadGrid>
           <%-- </asp:Panel>--%>
        </td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%">
            &nbsp;</td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%" align="left">
            <asp:Label ID="lblIsApproved" runat="server" Text="Is Approved" 
                CssClass="lbl" Visible="False"></asp:Label>
        </td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%" align="left">
            <asp:CheckBox ID="chkApprove" runat="server" Visible="False" />
        </td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%" align="center">
            <asp:Button ID="btnApprove" runat="server" Text="Approve" Width="77px" CssClass="btn" 
                Height="25px" Visible="False" CausesValidation="False" />
        </td>
    </tr>
</table>
