﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewInstructionDetails.ascx.vb"
    Inherits="DesktopModules_InstructionView_ViewInstructionDetails" %>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 50%">
            <asp:Label ID="lblViewInstructionDetails" runat="server" Text="Instruction Details"
                CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 50%">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False" />
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%" colspan="2">
            <asp:Label ID="lblError" runat="server" Font-Bold="False" Text="No Record Found"
                Visible="False" CssClass="headingblue"></asp:Label>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%" colspan="2">
            <asp:DetailsView ID="dvViewInsDetails" runat="server" AutoGenerateColumns="true"
                CssClass="Grid" Width="100%" AllowPaging="True"
                AutoGenerateRows="true">
                <AlternatingRowStyle CssClass="GridAltItem" />
                <FieldHeaderStyle Width="180px" HorizontalAlign="Left" VerticalAlign="Top" />
                
                <HeaderStyle CssClass="GridHeader" />
                <PagerStyle CssClass="GridPager" />
                <RowStyle CssClass="GridItem" />
            </asp:DetailsView>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%" colspan="2">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%" colspan="2">

            &nbsp;</td>
    </tr>
</table>
