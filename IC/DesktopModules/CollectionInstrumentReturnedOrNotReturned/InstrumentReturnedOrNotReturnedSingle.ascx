﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InstrumentReturnedOrNotReturnedSingle.ascx.vb"
    Inherits="DesktopModules_CollectionInstrumentReturnedOrNotReturned_InstrumentReturnedOrNotReturnedSingle" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
            }
        }
    }
</script>
<telerik:RadInputManager ID="rad" runat="server">
    <telerik:TextBoxSetting BehaviorID="regInstrumentNumber" Validation-IsRequired="true"
        ErrorMessage="Invalid Instrument Number" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstrumentNumber" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="regtxtReturnReason" Validation-IsRequired="false"
        ErrorMessage="Invalid Return Reason" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReturnReason" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue" Text="Instrument Status Update - Single"></asp:Label>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBank" runat="server" Text="Bank" CssClass="lbl"></asp:Label><asp:Label
                ID="lblReqBank" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblInstrumentNumber" runat="server" Text="Instrument Number" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqInstrumentNumber" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlBank" runat="server" CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtInstrumentNumber" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlBank" runat="server" ControlToValidate="ddlBank"
                Display="Dynamic" ErrorMessage="Please select Bank" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;     </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblStatus" runat="server" Text="Select Status" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqStatus" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblReturnReason" runat="server" Text="Return Reason" Visible="false" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqReturnReason" runat="server" Text="*" ForeColor="Red" Visible="false"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlInstrumentStatus" runat="server" CssClass="dropdown" AutoPostBack="true">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem Value="R">Returned</asp:ListItem>
                <asp:ListItem Value="NR">Not Returned</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtReturnReason" runat="server" CssClass="txtbox" MaxLength="250" Visible="false"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlInstrumentStatus" runat="server" ControlToValidate="ddlInstrumentStatus"
                Display="Dynamic" ErrorMessage="Please select Status" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn" Width="71px" CausesValidation="true" />
        </td>
    </tr>
    <asp:Panel ID="pnlInstrumentDetails" runat="server" Visible="false">
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" colspan="4">
                <telerik:RadGrid ID="gvInstrumentDetails" runat="server" AllowPaging="false" AllowSorting="false"
                    AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" Width="1250px">
                    <ClientSettings>
                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    </ClientSettings>
                    <MasterTableView UseAllDataFields="true" TableLayout="Auto" DataKeyNames="StatusID,CollectionID">
                        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                        <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridTemplateColumn>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select"
                                        TextAlign="Right" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="InstrumentNo" HeaderText="Instrument No."
                                SortExpression="InstrumentNo">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="BankName" HeaderText="Bank Name"
                                SortExpression="BankName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="LastStatus" HeaderText="Last Status"
                                SortExpression="LastStatus">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Status" HeaderText="Status"
                                SortExpression="Status">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CollectionAccountNo" HeaderText="Collection Account No"
                                SortExpression="CollectionAccountNo">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CollectionAccountBranchCode" HeaderText="Collection Account Branch Code"
                                SortExpression="CollectionAccountBranchCode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CollectionAccountCurrency" HeaderText="Collection Account Currency"
                                SortExpression="CollectionAccountCurrency">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CollectionAccountType" HeaderText="Collection Account Type"
                                SortExpression="CollectionAccountType">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ClearingAccountNo" HeaderText="Clearing Account No"
                                SortExpression="ClearingAccountNo">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ClearingAccountBranchCode" HeaderText="Clearing Account Branch Code"
                                SortExpression="ClearingAccountBranchCode">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ClearingAccountCurrency" HeaderText="Clearing Account Currency"
                                SortExpression="ClearingAccountCurrency">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ClearingCBAccountType" HeaderText="Clearing CB AccountType"
                                SortExpression="ClearingCBAccountType">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CollectionAmount" HeaderText="Collection Amount"
                                SortExpression="CollectionAmount">
                            </telerik:GridBoundColumn>
                            <%--  <telerik:GridTemplateColumn>
                                <HeaderTemplate>
                                    <asp:Label runat="server" Text="Returned/Not Returned"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlInstrumentStatus" runat="server" CssClass="dropdown">
                                        <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                                        <asp:ListItem Value="R">Returned</asp:ListItem>
                                        <asp:ListItem Value="NR">Not Returned</asp:ListItem>
                                    </asp:DropDownList>
                                    <br />
                                    <asp:RequiredFieldValidator ID="rfvddlInstrumentStatus" runat="server" ControlToValidate="ddlInstrumentStatus"
                                        Display="Dynamic" ErrorMessage="Please select Instrument Status" InitialValue="0" SetFocusOnError="True" Enabled="false"></asp:RequiredFieldValidator>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn>
                                <HeaderTemplate>
                                    <asp:Label ID="lblReturnReason" runat="server" Text="Return Reason"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtReturnReason" runat="server" CssClass="txtbox" MaxLength="250"></asp:TextBox>
                                    <telerik:RadInputManager ID="radgv" runat="server">
                                        <telerik:TextBoxSetting BehaviorID="regtxtReturnReason" Validation-IsRequired="false"
                                            ErrorMessage="Invalid Return Reason" EmptyMessage="">
                                            <TargetControls>
                                                <telerik:TargetInput ControlID="txtReturnReason" />
                                            </TargetControls>
                                        </telerik:TextBoxSetting>
                                    </telerik:RadInputManager>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>--%>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                        </EditFormSettings>
                        <PagerStyle AlwaysVisible="True" />
                    </MasterTableView>
                    <ItemStyle CssClass="rgRow" />
                    <PagerStyle AlwaysVisible="True" />
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                </telerik:RadGrid>
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" colspan="4">
                <asp:Label ID="lblInstrumentDetailsRNF" runat="server" Text="Record Not Found." CssClass="headingblue"
                    Visible="false"></asp:Label>
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="center" valign="top" colspan="4">
                <asp:Button ID="btnProceed" runat="server" Text="Proceed" CssClass="btn" Visible="False"
                    Width="75px" CausesValidation="true" Height="26px" />
                &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" Visible="False"
                    CssClass="btnCancel" Width="75px" CausesValidation="False" Height="26px" />
                &nbsp;
            </td>
        </tr>
    </asp:Panel>
</table>
