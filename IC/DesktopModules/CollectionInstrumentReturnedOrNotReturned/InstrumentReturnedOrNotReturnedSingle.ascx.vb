﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_CollectionInstrumentReturnedOrNotReturned_InstrumentReturnedOrNotReturnedSingle
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlBank()
                btnProceed.Visible = CBool(htRights("Update"))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Single Instrument Status Update")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#Region "Buttons"

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If Page.IsValid Then
            Try
                Dim qryCollectionInst As New ICCollectionsQuery("qryCollectionInst")
                Dim qryCollectionLastStatus As New ICCollectionsStatusQuery("qryCollectionLastStatus")
                Dim qryCollectionStatus As New ICCollectionsStatusQuery("qryCollectionStatus")
                Dim qryBank As New ICBankQuery("qryBank")

                If txtInstrumentNumber.Text.Trim <> "" Then
                    qryCollectionInst.Select(qryBank.BankName, qryCollectionLastStatus.CollectionStatusName.As("LastStatus"), qryCollectionStatus.CollectionStatusName.As("Status"))
                    qryCollectionInst.Select(qryCollectionInst.LastStatus.As("LastStatusID"), qryCollectionInst.Status.As("StatusID"))
                    qryCollectionInst.SelectAll()
                    qryCollectionInst.InnerJoin(qryCollectionLastStatus).On(qryCollectionInst.LastStatus = qryCollectionLastStatus.CollectionStatusID)
                    qryCollectionInst.InnerJoin(qryCollectionStatus).On(qryCollectionInst.Status = qryCollectionStatus.CollectionStatusID)
                    qryCollectionInst.InnerJoin(qryBank).On(qryCollectionInst.BankCode = qryBank.BankCode)
                    qryCollectionInst.Where(qryCollectionInst.InstrumentNo = txtInstrumentNumber.Text)
                    qryCollectionInst.Where(qryCollectionInst.BankCode = ddlBank.SelectedValue.ToString())
                    qryCollectionInst.Where(qryCollectionInst.CollectionClearingType <> "Funds Transfer")
                    qryCollectionInst.Where(qryCollectionInst.Status <> "3") '' Paid
                    Dim dt As New DataTable
                    dt = qryCollectionInst.LoadDataTable()
                    If dt.Rows.Count > 0 Then
                        LoadgvInstrumentDetails(True, dt)
                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Record not exist", ICBO.IC.Dialogmessagetype.Failure)
                    End If
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL(), False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnProceed_Click(sender As Object, e As EventArgs) Handles btnProceed.Click

        If Page.IsValid Then
            Try
                'Dim txtReturnReason As New TextBox
                'Dim ddlInstrumentStatus As DropDownList

                'If gvInstrumentDetails.Items.Count > 0 Then
                '    For Each gvRow As GridDataItem In gvInstrumentDetails.Items
                '        ddlInstrumentStatus = DirectCast(gvRow.Cells(13).FindControl("ddlInstrumentStatus"), DropDownList)
                '        txtReturnReason = DirectCast(gvRow.Cells(14).FindControl("txtReturnReason"), TextBox)
                '        If ddlInstrumentStatus.SelectedValue = "R" And txtReturnReason.Text.Trim = "" Then
                '            UIUtilities.ShowDialog(Me, "Instrument Status Update - Single", "Please enter Return Reason.", ICBO.IC.Dialogmessagetype.Warning)
                '            Exit Sub
                '        End If
                '    Next
                'End If

                Dim rowgvInstrumentDetails As GridDataItem
                Dim chkSelect As CheckBox
                Dim objCollections As New ICCollections

                If CheckgvCollectionNatureForProcessAll() = True Then
                    For Each rowgvInstrumentDetails In gvInstrumentDetails.Items
                        chkSelect = New CheckBox
                        'ddlInstrumentStatus = New DropDownList
                        chkSelect = DirectCast(rowgvInstrumentDetails.Cells(0).FindControl("chkSelect"), CheckBox)
                        'ddlInstrumentStatus = DirectCast(rowgvInstrumentDetails.Cells(13).FindControl("ddlInstrumentStatus"), DropDownList)
                        'txtReturnReason = DirectCast(rowgvInstrumentDetails.Cells(14).FindControl("txtReturnReason"), TextBox)

                        If chkSelect.Checked = True Then
                            If objCollections.LoadByPrimaryKey(rowgvInstrumentDetails.GetDataKeyValue("CollectionID").ToString()) Then
                                InstrumentMarkedReturnedOrNotReturned(rowgvInstrumentDetails.GetDataKeyValue("StatusID"), ddlInstrumentStatus.SelectedValue.ToString(), objCollections, txtReturnReason.Text, Me.UserId, Me.UserInfo.Username)
                            End If
                        End If
                    Next
                Else
                    UIUtilities.ShowDialog(Me, "Instrument Status Update - Single", "Please select atleast one(1) Instrument.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub ddlInstrumentStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlInstrumentStatus.SelectedIndexChanged
        Try
            If ddlInstrumentStatus.SelectedValue = "R" Then
                lblReturnReason.Visible = True
                lblReqReturnReason.Visible = True
                txtReturnReason.Visible = True
                rad.GetSettingByBehaviorID("regtxtReturnReason").Validation.IsRequired = True
            Else
                lblReturnReason.Visible = False
                lblReqReturnReason.Visible = False
                txtReturnReason.Visible = False
                txtReturnReason.Text = Nothing
                rad.GetSettingByBehaviorID("regtxtReturnReason").Validation.IsRequired = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

    Private Sub InstrumentMarkedReturnedOrNotReturned(ByVal Status As String, ByVal MarkAs As String, ByVal objCollections As ICCollections, ByVal ReturnReason As String, ByVal UserID As String, ByVal UserName As String)
        Try
            If MarkAs = "R" Then
                If Status = "6" Then       '' Returned
                    UIUtilities.ShowDialog(Me, "Error", "Instrument status is already Returned.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                ElseIf Status = "8" Then             '' Pending Clearing
                    Dim FromAccType As CBUtilities.AccountType
                    Dim ToAccType As CBUtilities.AccountType
                    If objCollections.ClearingCBAccountType = "RB" Then
                        FromAccType = CBUtilities.AccountType.RB
                    Else
                        FromAccType = CBUtilities.AccountType.GL
                    End If

                    If objCollections.CollectionAccountType = "RB" Then
                        ToAccType = CBUtilities.AccountType.RB
                    Else
                        ToAccType = CBUtilities.AccountType.GL
                    End If

                    Try
                        If CBUtilities.OpenEndedFundsTransfer(objCollections.ClearingAccountNo, objCollections.ClearingAccountBranchCode, objCollections.ClearingAccountCurrency, Nothing, Nothing, Nothing, FromAccType, objCollections.CollectionAccountNo, objCollections.CollectionAccountBranchCode, objCollections.CollectionAccountCurrency, Nothing, Nothing, Nothing, ToAccType, objCollections.CollectionAmount, "Collections", objCollections.CollectionID) = CBUtilities.TransactionStatus.Successfull Then
                            Dim FrmAccType As CBUtilities.AccountType
                            Dim ToAcType As CBUtilities.AccountType
                            If objCollections.CollectionAccountType = "RB" Then
                                FrmAccType = CBUtilities.AccountType.RB
                            Else
                                FrmAccType = CBUtilities.AccountType.GL
                            End If
                            If objCollections.ClearingCBAccountType = "RB" Then
                                ToAcType = CBUtilities.AccountType.RB
                            Else
                                ToAcType = CBUtilities.AccountType.GL
                            End If

                            Try
                                CBUtilities.OpenEndedFundsTransfer(objCollections.CollectionAccountNo, objCollections.CollectionAccountBranchCode, objCollections.CollectionAccountCurrency, Nothing, Nothing, Nothing, FrmAccType, objCollections.ClearingAccountNo, objCollections.ClearingAccountBranchCode, objCollections.ClearingAccountCurrency, Nothing, Nothing, Nothing, ToAcType, objCollections.CollectionAmount, "Collections", objCollections.CollectionID)
                                'objCollections.LastStatus = "6"    ''Returned
                                'objCollections.Status = "6"    ''Returned

                                ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objCollections.ReferenceField1, objCollections.ReferenceField2, objCollections.ReferenceField3, objCollections.ReferenceField4, objCollections.ReferenceField5, objCollections.UpToICCollectionsStatusByStatus.CollectionStatusName, "Returned", UserID, UserName)
                                ICCollectionsController.UpdateICCollectionsStatus(objCollections.CollectionID, objCollections.Status, "6", UserID, UserName)

                                objCollections.ReturnReason = ReturnReason
                                objCollections.Save()
                                Dim ActionStr As String = ""
                                ActionStr = "Instrument No: " & objCollections.InstrumentNo & " ; Bank Code: " & objCollections.BankCode & " successfully marked as Returned"
                                ICUtilities.AddAuditTrail(ActionStr, "Collections", objCollections.CollectionID, Me.UserId, Me.UserInfo.Username, "UPDATE")
                                UIUtilities.ShowDialog(Me, "Instrument Status Update - Single", "Instrument successfully marked as Returned.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())

                            Catch ex As Exception
                                'objCollections.LastStatus = "10"    ''Clearing Error
                                'objCollections.Status = "10"    ''Clearing Error

                                ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objCollections.ReferenceField1, objCollections.ReferenceField2, objCollections.ReferenceField3, objCollections.ReferenceField4, objCollections.ReferenceField5, objCollections.UpToICCollectionsStatusByStatus.CollectionStatusName, "Clearing Error", UserID, UserName)
                                ICCollectionsController.UpdateICCollectionsStatus(objCollections.CollectionID, objCollections.Status, "10", UserID, UserName)

                                'objCollections.Save()
                                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                            End Try

                        End If
                    Catch ex As Exception
                        UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    End Try

                ElseIf Status = "9" Then     '' Cleared
                    Dim FrmAccType As CBUtilities.AccountType
                    Dim ToAcType As CBUtilities.AccountType
                    If objCollections.CollectionAccountType = "RB" Then
                        FrmAccType = CBUtilities.AccountType.RB
                    Else
                        FrmAccType = CBUtilities.AccountType.GL
                    End If
                    If objCollections.ClearingCBAccountType = "RB" Then
                        ToAcType = CBUtilities.AccountType.RB
                    Else
                        ToAcType = CBUtilities.AccountType.GL
                    End If

                    Try
                        CBUtilities.OpenEndedFundsTransfer(objCollections.CollectionAccountNo, objCollections.CollectionAccountBranchCode, objCollections.CollectionAccountCurrency, Nothing, Nothing, Nothing, FrmAccType, objCollections.ClearingAccountNo, objCollections.ClearingAccountBranchCode, objCollections.ClearingAccountCurrency, Nothing, Nothing, Nothing, ToAcType, objCollections.CollectionAmount, "Collections", objCollections.CollectionID)
                        'objCollections.LastStatus = "6"    ''Returned
                        'objCollections.Status = "6"    ''Returned

                        ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objCollections.ReferenceField1, objCollections.ReferenceField2, objCollections.ReferenceField3, objCollections.ReferenceField4, objCollections.ReferenceField5, objCollections.UpToICCollectionsStatusByStatus.CollectionStatusName, "Returned", UserID, UserName)
                        ICCollectionsController.UpdateICCollectionsStatus(objCollections.CollectionID, objCollections.Status, "6", UserID, UserName)


                        objCollections.ReturnReason = ReturnReason
                        objCollections.Save()
                        Dim ActionStr As String = ""
                        ActionStr = "Instrument No: " & objCollections.InstrumentNo & " ; Bank Code: " & objCollections.BankCode & " successfully marked as Returned"
                        ICUtilities.AddAuditTrail(ActionStr, "Collections", objCollections.CollectionID, Me.UserId, Me.UserInfo.Username, "UPDATE")
                        UIUtilities.ShowDialog(Me, "Instrument Status Update - Single", "Instrument successfully marked as Returned.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())

                    Catch ex As Exception
                        'objCollections.LastStatus = "10"    ''Clearing Error
                        'objCollections.Status = "10"    ''Clearing Error

                        ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objCollections.ReferenceField1, objCollections.ReferenceField2, objCollections.ReferenceField3, objCollections.ReferenceField4, objCollections.ReferenceField5, objCollections.UpToICCollectionsStatusByStatus.CollectionStatusName, "Clearing Error", UserID, UserName)
                        ICCollectionsController.UpdateICCollectionsStatus(objCollections.CollectionID, objCollections.Status, "10", UserID, UserName)


                        'objCollections.Save()
                        UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    End Try

                ElseIf Status = "10" Then     '' Clearing Error
                    'objCollections.LastStatus = "6"    ''Returned
                    'objCollections.Status = "6"    ''Returned

                    ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objCollections.ReferenceField1, objCollections.ReferenceField2, objCollections.ReferenceField3, objCollections.ReferenceField4, objCollections.ReferenceField5, objCollections.UpToICCollectionsStatusByStatus.CollectionStatusName, "Returned", UserID, UserName)
                    ICCollectionsController.UpdateICCollectionsStatus(objCollections.CollectionID, objCollections.Status, "6", UserID, UserName)


                    objCollections.ReturnReason = ReturnReason
                    objCollections.Save()
                    Dim ActionStr As String = ""
                    ActionStr = "Instrument No: " & objCollections.InstrumentNo & " ; Bank Code: " & objCollections.BankCode & " successfully marked as Returned"
                    ICUtilities.AddAuditTrail(ActionStr, "Collections", objCollections.CollectionID, Me.UserId, Me.UserInfo.Username, "UPDATE")
                    UIUtilities.ShowDialog(Me, "Instrument Status Update - Single", "Instrument successfully marked as Returned.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                End If


            ElseIf MarkAs = "NR" Then
                If Status = "8" Then             '' Pending Clearing
                    UIUtilities.ShowDialog(Me, "Error", "Instrument is Pending Clearing state and can not be marked as Not Returned.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                ElseIf Status = "9" Then      '' Cleared
                    UIUtilities.ShowDialog(Me, "Error", "Instrument is Cleared state and can not be marked as Not Returned.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                ElseIf Status = "6" Then       '' Returned
                    'objCollections.LastStatus = "8"              '' Pending Clearing
                    'objCollections.Status = "8"              '' Pending Clearing

                    ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objCollections.ReferenceField1, objCollections.ReferenceField2, objCollections.ReferenceField3, objCollections.ReferenceField4, objCollections.ReferenceField5, objCollections.UpToICCollectionsStatusByStatus.CollectionStatusName, "Pending Clearing", UserID, UserName)
                    ICCollectionsController.UpdateICCollectionsStatus(objCollections.CollectionID, objCollections.Status, "8", UserID, UserName)


                    objCollections.ReturnReason = Nothing
                    objCollections.Save()
                    Dim ActionStr As String = ""
                    ActionStr = "Instrument No: " & objCollections.InstrumentNo & " ; Bank Code: " & objCollections.BankCode & " successfully marked as Not Returned"
                    ICUtilities.AddAuditTrail(ActionStr, "Collections", objCollections.CollectionID, Me.UserId, Me.UserInfo.Username, "UPDATE")
                    UIUtilities.ShowDialog(Me, "Instrument Status Update - Single", "Instrument successfully marked as Not Returned.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                ElseIf Status = "10" Then     '' Clearing Error
                    'objCollections.LastStatus = "8"             '' Pending Clearing
                    'objCollections.Status = "8"             '' Pending Clearing

                    ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objCollections.ReferenceField1, objCollections.ReferenceField2, objCollections.ReferenceField3, objCollections.ReferenceField4, objCollections.ReferenceField5, objCollections.UpToICCollectionsStatusByStatus.CollectionStatusName, "Pending Clearing", UserID, UserName)
                    ICCollectionsController.UpdateICCollectionsStatus(objCollections.CollectionID, objCollections.Status, "8", UserID, UserName)


                    objCollections.ReturnReason = Nothing
                    objCollections.Save()
                    Dim ActionStr As String = ""
                    ActionStr = "Instrument No: " & objCollections.InstrumentNo & " ; Bank Code: " & objCollections.BankCode & " successfully marked as Not Returned"
                    ICUtilities.AddAuditTrail(ActionStr, "Collections", objCollections.CollectionID, Me.UserId, Me.UserInfo.Username, "UPDATE")
                    UIUtilities.ShowDialog(Me, "Instrument Status Update - Single", "Instrument successfully marked as Not Returned.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                End If

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlBank()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBank.Items.Clear()
            ddlBank.Items.Add(lit)
            ddlBank.AppendDataBoundItems = True
            ddlBank.DataSource = ICBankController.GetAllApprovedAndActiveBanks()
            ddlBank.DataTextField = "BankName"
            ddlBank.DataValueField = "BankCode"
            ddlBank.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#Region "Grid View"
    Private Sub LoadgvInstrumentDetails(ByVal Bind As Boolean, ByVal dt As DataTable)
        Try
            If Bind = True Then
                pnlInstrumentDetails.Visible = True
                lblInstrumentDetailsRNF.Visible = False
                btnProceed.Visible = True
                btnCancel.Visible = True
                gvInstrumentDetails.DataSource = dt
                gvInstrumentDetails.DataBind()
            Else
                pnlInstrumentDetails.Visible = True
                lblInstrumentDetailsRNF.Visible = True
                gvInstrumentDetails.Visible = False
                btnProceed.Visible = False
                btnCancel.Visible = False
                gvInstrumentDetails.DataSource = dt
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvEmailUsers_ItemCreated(sender As Object, e As GridItemEventArgs) Handles gvInstrumentDetails.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvInstrumentDetails.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvInstrumentDetails_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles gvInstrumentDetails.ItemDataBound
        Dim chkProcessAll As CheckBox
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvInstrumentDetails.MasterTableView.ClientID & "','0');"
        End If

        'If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
        '    Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        '    Dim rfvddlInstrumentStatus As New RequiredFieldValidator
        '    rfvddlInstrumentStatus = DirectCast(e.Item.Cells(13).FindControl("rfvddlInstrumentStatus"), RequiredFieldValidator)
        '    rfvddlInstrumentStatus.Enabled = True
        'End If
    End Sub

    Protected Sub gvInstrumentDetails_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles gvInstrumentDetails.NeedDataSource
        Try
            LoadgvInstrumentDetails(False, Nothing)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Function CheckgvCollectionNatureForProcessAll() As Boolean
        Try
            Dim rowgvInstrumentDetails As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvInstrumentDetails In gvInstrumentDetails.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvInstrumentDetails.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function


#End Region

   
End Class
