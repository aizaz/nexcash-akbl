﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_RePrintingQueue_ReprintingQueue
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrICAssignUserRolsID As New ArrayList
    Private ArrICAssignedOfficeID As New ArrayList
    Private ArrICAssignedStatus As New ArrayList
    Private ArrICAssignedPaymentModes As New ArrayList
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()
            SetArraListAssignedOfficeIDs()
            lblMessageForQueue.Style.Add("display", "none")
            If Page.IsPostBack = False Then
                lblMessageForQueue.Text = ICUtilities.GetSettingValue("ErrorMessage")
                btnPrintMultiple.Attributes.Item("onclick") = "if (Page_ClientValidate('Verify')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnPrintMultiple, Nothing).ToString() & " } "
                btnSinglePrint.Attributes.Item("onclick") = "if (Page_ClientValidate('Verify')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnSinglePrint, Nothing).ToString() & " } "
                'btnCancelPrint.Attributes.Item("onclick") = "if (Page_ClientValidate('Verify')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnCancelPrint, Nothing).ToString() & " } "

                'SetArraListAssignedOfficeIDs()


                RefreshPage()


            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub RefreshPage()
        DesignDts()
        rbtnlstDateSelection.SelectedValue = "Creation Date"
        raddpCreationToDate.SelectedDate = Date.Now
        raddpCreationToDate.MaxDate = Date.Now
        ViewState("htRights") = Nothing
        GetPageAccessRights()
        SetArraListAssignedPaymentModes()
        LoadddlCompany()
        LoadddlAccountPaymentNatureByUserID()
        LoadddlProductTypeByAccountPaymentNature()
        FillDesignedDtByAPNatureByAssignedRole()
        Dim dtPageLoad As New DataTable
        dtPageLoad = DirectCast(ViewState("AccountNumber"), DataTable)
        If dtPageLoad.Rows.Count > 0 Then
            ClearAllTextBoxes()
            LoadddlBatcNumber()
            LoadgvAccountPaymentNatureProductType(True)
            DesignDtForSummary()
        Else
            UIUtilities.ShowDialog(Me, "Error", "You do not have access to any transactional data. Please contact Al Baraka Administrator.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
            Exit Sub
            btnSinglePrint.Visible = False
            btnPrintMultiple.Visible = False
            btnCancelPrint.Visible = False
            gvAllowRePrintReIssue.Visible = False
            lblNRF.Visible = True
        End If


    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Re-Printing Queue")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Re-Printing Queue") = True Then
                    ArrICAssignUserRolsID.Add(RoleID.ToString)
                End If
            Next
        End If
    End Sub
    Private Sub DesignDts()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim dtOfficeID As New DataTable
        dtAccountNumber.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("BranchCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("Currency", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("PaymentNatureCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("OfficeID", GetType(System.String)))
        ViewState("AccountNumber") = dtAccountNumber
    End Sub
    Private Sub FillDesignedDtByAPNatureByAssignedRole()
        Dim dtAssignedAPNatureOfficeID As New DataTable
        dtAssignedAPNatureOfficeID.Rows.Clear()
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserSecond(Me.UserId.ToString, ArrICAssignUserRolsID)
        dtAssignedAPNatureOfficeID.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAssignedAPNatureOfficeID.Rows
            dr("DropDown") = "False"
        Next
        ViewState("AccountNumber") = Nothing
        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID
    End Sub
    Private Sub SetArraListAssignedOfficeIDs()
        Dim dt As New DataTable
        Dim objICUser As New ICUser
        ArrICAssignedOfficeID.Clear()
        dt = ICUserRolesPaymentNatureController.GetAllTaggedLocationsWithUserByUSerIDAndRoleID(Me.UserId.ToString, ArrICAssignUserRolsID)
        If dt.Rows.Count > 0 Then
            objICUser.LoadByPrimaryKey(Me.UserId)
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("OfficeID").ToString
                ArrICAssignedOfficeID.Add(RoleID.ToString)
            Next
            ArrICAssignedOfficeID.Add(objICUser.OfficeCode)
        End If
    End Sub
    Private Sub SetArraListAssignedStatus()
        ArrICAssignedStatus.Clear()
        ArrICAssignedStatus.Add(23)
    End Sub
    Private Sub SetArraListAssignedPaymentModes()
        ArrICAssignedPaymentModes.Clear()
        If CBool(htRights("Cheque Instructions")) = True Then
            ArrICAssignedPaymentModes.Add("Cheque")
        End If
        If CBool(htRights("DD Instructions")) = True Then
            ArrICAssignedPaymentModes.Add("DD")
        End If
        If CBool(htRights("PO Instructions")) = True Then
            ArrICAssignedPaymentModes.Add("PO")
        End If
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)



            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, "", ArrICAssignUserRolsID)
            ddlCompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlCompany.Items.Clear()
                    ddlCompany.Items.Add(lit)
                    lit.Selected = True
                    ddlCompany.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
                ddlCompany.DataSource = dt
                ddlCompany.DataTextField = "CompanyName"
                ddlCompany.DataValueField = "CompanyCode"
                ddlCompany.DataBind()
                ddlCompany.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccountPaymentNatureByUserID()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlAcccountPaymentNature.Items.Clear()
            ddlAcccountPaymentNature.Items.Add(lit)
            lit.Selected = True
            ddlAcccountPaymentNature.AppendDataBoundItems = True
            ddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForDropDown(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
            ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
            ddlAcccountPaymentNature.DataBind()
          
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlBatcNumber()
        Try
            
            Dim dtAccountNumber As New DataTable


            dtAccountNumber = ViewState("AccountNumber")

            SetArraListAssignedStatus()
            SetArraListAssignedPaymentModes()
            Dim lit As New ListItem
            lit.Text = "-- All --"
            lit.Value = "0"
            ddlBatch.Items.Clear()
            ddlBatch.Items.Add(lit)
            lit.Selected = True
            ddlBatch.AppendDataBoundItems = True
            ddlBatch.DataSource = ICInstructionController.GetAllTaggedBatchNumbersByCompanyCodeWithStatusForPrinting(ArrICAssignedPaymentModes, dtAccountNumber, ddlCompany.SelectedValue.ToString, ArrICAssignedStatus, Me.UserId.ToString)
            ddlBatch.DataTextField = "FileBatchNoName"
            ddlBatch.DataValueField = "FileBatchNo"
            ddlBatch.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlProductTypeByAccountPaymentNature()
        Try
            Dim lit As New ListItem
            Dim objICAPNature As New ICAccountsPaymentNature
            objICAPNature.es.Connection.CommandTimeout = 3600
            ddlProductType.Items.Clear()
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlProductType.Items.Add(lit)
            ddlProductType.AppendDataBoundItems = True
            If ddlAcccountPaymentNature.SelectedValue.ToString <> "0" Then
                If objICAPNature.LoadByPrimaryKey(ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)) Then
                    ddlProductType.DataSource = ICAccountPaymentNatureProductTypeController.GetAllProductTypesByAccountAndPaymentNature(objICAPNature)
                    ddlProductType.DataTextField = "ProductTypeName"
                    ddlProductType.DataValueField = "ProductTypeCode"
                    ddlProductType.DataBind()
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadgvAccountPaymentNatureProductType(ByVal DoDataBind As Boolean)
        Try
            Dim objICUser As New ICUser
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId.ToString)
            Dim DateType, AccountNumber, BranchCode, Currency, PaymentNatureCode, CompanyCode, ProductTypeCode, InstrumentNo, InstructionNo, BatchCode, ReferenceNo As String
            Dim FromDate, ToDate As String
            Dim Amount As Double = 0
            Dim dtAccountNumber As New DataTable
            dtAccountNumber = ViewState("AccountNumber")
            DateType = ""
            AccountNumber = ""
            BranchCode = ""
            Currency = ""
            Currency = ""
            PaymentNatureCode = ""
            CompanyCode = "0"
            ProductTypeCode = ""
            InstructionNo = ""
            InstrumentNo = ""
            BatchCode = ""
            ReferenceNo = ""
            FromDate = Nothing
            ToDate = Nothing
            If txtAmount.Text <> "" And txtAmount.Text <> "Enter Amount" Then
                Amount = CDbl(txtAmount.Text)
            End If

            If rbtnlstDateSelection.SelectedValue = "Creation Date" Then
                DateType = "Creation Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Value Date" Then
                DateType = "Value Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Approval Date" Then
                DateType = "Approval Date"
            End If
            If txtReferenceNo.Text.ToString <> "" And txtReferenceNo.Text.ToString <> "Enter Reference No." Then
                ReferenceNo = txtReferenceNo.Text.ToString
            End If

            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            End If
            If ddlProductType.SelectedValue.ToString <> "0" Then
                ProductTypeCode = ddlProductType.SelectedValue.ToString
            End If
            If txtInstrumentNo.Text.ToString <> "" And txtInstrumentNo.Text.ToString <> "Enter Instrument No." Then
                InstrumentNo = txtInstrumentNo.Text.ToString
            End If
            If txtInstructionNo.Text.ToString <> "" And txtInstructionNo.Text.ToString <> "Enter Instruction No." Then
                InstructionNo = txtInstructionNo.Text.ToString
            End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                FromDate = raddpCreationFromDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If Not raddpCreationToDate.SelectedDate Is Nothing Then
                ToDate = raddpCreationToDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If ddlBatch.SelectedValue <> "0" Then
                BatchCode = ddlBatch.SelectedValue.ToString
            End If
            SetArraListAssignedStatus()
            SetArraListAssignedOfficeIDs()
            SetArraListAssignedPaymentModes()
            ICInstructionController.GetAllInstructionsForRadGridForPrintingWithUserLocation(DateType, FromDate, ToDate, dtAccountNumber, BatchCode, ProductTypeCode, InstructionNo, InstrumentNo, ReferenceNo, Me.gvAllowRePrintReIssue.CurrentPageIndex + 1, Me.gvAllowRePrintReIssue.PageSize, Me.gvAllowRePrintReIssue, DoDataBind, ArrICAssignedStatus, Me.UserId.ToString, Amount, ddlAmountOp.SelectedValue.ToString, ArrICAssignedPaymentModes, CompanyCode)
            If gvAllowRePrintReIssue.Items.Count > 0 Then
                gvAllowRePrintReIssue.Visible = True
                lblNRF.Visible = False

                gvAllowRePrintReIssue.Columns(9).Visible = CBool(htRights("Change Instrument No"))
                btnCancelPrint.Visible = CBool(htRights("Cancel Print"))
                btnPrintMultiple.Visible = CBool(htRights("Bulk Print"))
                btnSinglePrint.Visible = CBool(htRights("Single Print"))
                'SetJavaScript()
            Else
                lblNRF.Visible = True
                gvAllowRePrintReIssue.Visible = False
                btnCancelPrint.Visible = False
                btnPrintMultiple.Visible = False
                btnSinglePrint.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    'Private Sub SetJavaScript()
    '    Dim imgBtn As ImageButton
    '    'If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Then
    '    Dim AppPath As String = DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
    '    For Each gvAuthorizationRow As GridDataItem In gvAllowRePrintReIssue.Items
    '        imgBtn = New ImageButton
    '        imgBtn = DirectCast(gvAuthorizationRow.Cells(11).FindControl("ibButton0"), ImageButton)
    '        imgBtn.OnClientClick = "showurldialog('Instruction Details','" & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & imgBtn.CommandArgument.ToString() & "' ,true,700,650);return false;"
    '    Next
    '    'End If
    'End Sub


    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
      Try
            'SetArraListRoleID()
            LoadddlAccountPaymentNatureByUserID()
            LoadddlBatcNumber()

            LoadddlProductTypeByAccountPaymentNature()

            LoadgvAccountPaymentNatureProductType(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlAcccountPaymentNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcccountPaymentNature.SelectedIndexChanged
        Try
            If ddlAcccountPaymentNature.SelectedValue.ToString = "0" Then
                SetArraListRoleID()
                FillDesignedDtByAPNatureByAssignedRole()
                LoadddlBatcNumber()
                LoadddlProductTypeByAccountPaymentNature()


                LoadgvAccountPaymentNatureProductType(True)
            Else
FillDesignedDtByAPNatureByAssignedRole()
                SetViewStateDtOnAPNatureIndexChnage()
                LoadddlBatcNumber()
                LoadddlProductTypeByAccountPaymentNature()


                LoadgvAccountPaymentNatureProductType(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetViewStateDtOnAPNatureIndexChnage()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim AccountNumber, BranchCode, Currency, PaymentNatureCode As String
        Dim ArrayListOfficeID As New ArrayList
        Dim drAccountNo As DataRow
        AccountNumber = Nothing
        BranchCode = Nothing
        Currency = Nothing
        PaymentNatureCode = Nothing
        dtAccountNumber = ViewState("AccountNumber")
        ViewState("AccountNumber") = Nothing
        AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0)
        BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1)
        Currency = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2)
        PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)
        For Each dr As DataRow In dtAccountNumber.Rows
            If dr("AccountNumber").ToString.Split("-")(0) = AccountNumber And dr("AccountNumber").ToString.Split("-")(1) = BranchCode And dr("AccountNumber").ToString.Split("-")(2) = Currency And dr("PaymentNature").ToString = PaymentNatureCode Then
                ArrayListOfficeID.Add(dr("OfficeID").ToString)
            End If
        Next
        dtAccountNumber.Rows.Clear()
        If ArrayListOfficeID.Count > 0 Then
            For Each OfficeID In ArrayListOfficeID
                drAccountNo = dtAccountNumber.NewRow()
                drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
                drAccountNo("PaymentNature") = PaymentNatureCode
                drAccountNo("OfficeID") = OfficeID
                dtAccountNumber.Rows.Add(drAccountNo)
            Next
        Else
            drAccountNo = dtAccountNumber.NewRow()
            drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
            drAccountNo("PaymentNature") = PaymentNatureCode
            drAccountNo("OfficeID") = "0"
            dtAccountNumber.Rows.Add(drAccountNo)
        End If
        'dtAccountNumber.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAccountNumber.Rows
            dr("DropDown") = "True"
        Next
        ViewState("AccountNumber") = dtAccountNumber



    End Sub
    Protected Sub ddlBatch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBatch.SelectedIndexChanged
        Try
            SetArraListRoleID()
            LoadddlProductTypeByAccountPaymentNature()

            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged
        Try
            SetArraListRoleID()



            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ClearAllTextBoxes()
        txtRemarks.Text = ""
        txtInstructionNo.Text = ""
        txtInstrumentNo.Text = ""
        txtReferenceNo.Text = ""
        txtAmount.Text = ""
        ddlAmountOp.SelectedValue = "="
        pnlUpdateInstrumentNo.Style.Add("display", "none")
        txtInsNo.Text = ""
        txtNewInstrumentNo.Text = ""
    End Sub
    Private Function CheckgvInstructionAuthentication() As Boolean
        Try

            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVInstruction In gvAllowRePrintReIssue.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVInstruction.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub gvAllowRePrintReIssue_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles gvAllowRePrintReIssue.ItemCommand
        Try
            If e.CommandName.ToString = "UpdateInstrumentNo" Then
                ClearAllTextBoxes()
                pnlUpdateInstrumentNo.Style.Remove("display")
                txtInsNo.Text = e.CommandArgument.ToString

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAllowRePrintReIssue_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAllowRePrintReIssue.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAllowRePrintReIssue.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAllowRePrintReIssue_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAllowRePrintReIssue.ItemDataBound
        Dim chkProcessAll As CheckBox
        Dim imgEdit As ImageButton


        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAllowRePrintReIssue.MasterTableView.ClientID & "','0');"
        End If
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            imgEdit = New ImageButton
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            imgEdit = DirectCast(item.Cells(10).FindControl("ibtnUpdateInstrumentNo"), ImageButton)
            imgEdit.Visible = False
            If item.GetDataKeyValue("PaymentMode").ToString = "Cheque" Or item.GetDataKeyValue("PaymentMode").ToString = "PO" Or item.GetDataKeyValue("PaymentMode").ToString = "DD" Then
                imgEdit.Visible = True
            End If
        End If
        Dim imgBtn As LinkButton
        Dim AppPath As String = "" ' DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            imgBtn = New LinkButton
            imgBtn = DirectCast(item.Cells(1).FindControl("lbInstructionID"), LinkButton)
            imgBtn.OnClientClick = "showurldialog('Instruction Details',' " & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & item.GetDataKeyValue("InstructionID").ToString().EncryptString() & "' ,true,700,650);return false;"
            If CBool(htRights("View Instruction Details")) = True Then
                imgBtn.Enabled = True

            End If
            If item.GetDataKeyValue("IsAmendmentComplete") = True Then

                gvAllowRePrintReIssue.AlternatingItemStyle.CssClass = "GridItemComplete"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItemComplete"


            Else
                gvAllowRePrintReIssue.AlternatingItemStyle.CssClass = "GridItem"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItem"


            End If
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Dim dtPageLoad As New DataTable
            dtPageLoad = DirectCast(ViewState("AccountNumber"), DataTable)
            If dtPageLoad.Rows.Count > 0 Then
                LoadgvAccountPaymentNatureProductType(True)
            Else
                btnSinglePrint.Visible = False
                btnPrintMultiple.Visible = False
                btnCancelPrint.Visible = False
                gvAllowRePrintReIssue.Visible = False
                lblNRF.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnCancelInstrumentNo_Click(sender As Object, e As System.EventArgs) Handles btnCancelInstrumentNo.Click
        Try
            ClearAllTextBoxes()
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim objICInstruction As New ICInstruction
                Dim StrAuditTrail As String = Nothing
                objICInstruction.es.Connection.CommandTimeout = 3600
                objICInstruction.LoadByPrimaryKey(txtInsNo.Text.ToString)
                If objICInstruction.PaymentMode = "Cheque" Then
                    If ICInstrumentController.IsInstrumentNoExistsByInstrumentNo(txtNewInstrumentNo.Text.ToString) = True Then
                        If ICInstrumentController.IsInstrumentNoIsUsedByInstrumentNo(txtNewInstrumentNo.Text.ToString, objICInstruction.InstructionID.ToString, False) = False Then
                            If ICInstrumentController.IsInstrumentNoIsAssignedToSpecificOfficeByInstrumentNoAndOfficeCode(txtNewInstrumentNo.Text.ToString, objICInstruction.PrintLocationCode.ToString) = True Then
                                StrAuditTrail = Nothing
                                StrAuditTrail += "Instrument number [ " & txtNewInstrumentNo.Text.ToString & " ] assigned to print location [ " & objICInstruction.PrintLocationCode & " ][ " & objICInstruction.UpToICOfficeByPrintLocationCode.OfficeName & " ]."
                                StrAuditTrail += "for instruction with ID [ " & objICInstruction.InstructionID & " ]. Previous instrument number was [ " & objICInstruction.InstrumentNo & " ]."
                                StrAuditTrail += "Action was taken by user [ " & Me.UserId.ToString & " ][ " & Me.UserInfo.Username.ToString & " ]."
                                objICInstruction.InstrumentNo = txtNewInstrumentNo.Text.ToString
                                ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAuditTrail, "UPDATE")
                                ICInstrumentController.MarkInstrumentNumberIsAssignedByInstructionIDAndInstrumentNumberAndOfficeCode(objICInstruction.InstrumentNo.ToString, objICInstruction.PrintLocationCode.ToString, objICInstruction.InstructionID, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                UIUtilities.ShowDialog(Me, "Re-Printing Queue", "Instrument number updated successfully.", ICBO.IC.Dialogmessagetype.Success)
                                ClearAllTextBoxes()
                                LoadgvAccountPaymentNatureProductType(True)
                                Exit Sub
                            Else
                                UIUtilities.ShowDialog(Me, "Re-Printing Queue", "Specified instrument number is not assigned to current print location.", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        Else
                            UIUtilities.ShowDialog(Me, "Re-Printing Queue", "Specified instrument number is used or assigned", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Re-Printing Queue", "Specified instrument number is invalid", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                ElseIf objICInstruction.PaymentMode = "DD" Then
                    If ICDDInstrumentsController.IsDDInstrumentNumberExistsByPrefixAndInstrumentNo(txtNewInstrumentNo.Text.ToString, "DD") = True Then
                        If ICDDInstrumentsController.IsDDInstrumentNoIsAssignedToSpecificPrintLocationByInstrumentNoAndOfficeCode(txtNewInstrumentNo.Text.ToString, "DD", objICInstruction.PrintLocationCode.ToString) = True Then
                            If ICDDInstrumentsController.IsDDInstrumentNumberAssignedOrPrintedByPrefixAndInstrumentNoAndInstructionID(txtNewInstrumentNo.Text.ToString, "DD", objICInstruction.PrintLocationCode.ToString) = False Then
                                StrAuditTrail = Nothing
                                StrAuditTrail += "Instrument number [ DD ] [ " & txtNewInstrumentNo.Text.ToString & " ] "
                                StrAuditTrail += "for instruction with ID [ " & objICInstruction.InstructionID & " ]. Previous instrument number was [ " & objICInstruction.InstrumentNo.ToString.Split("-")(0) & " ] [ " & objICInstruction.InstrumentNo.ToString.Split("-")(1) & " ]."
                                StrAuditTrail += "Action was taken by user [ " & Me.UserId.ToString & " ][ " & Me.UserInfo.Username.ToString & " ]."
                                objICInstruction.InstrumentNo = "DD-" & txtNewInstrumentNo.Text.ToString
                                ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAuditTrail, "UPDATE")
                                ICDDInstrumentsController.MarkInstrumentNumberAssignedByInstructionIDAndDDInstrumentNumber(txtNewInstrumentNo.Text.ToString, "DD", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, objICInstruction.PrintLocationCode.ToString)
                                UIUtilities.ShowDialog(Me, "Re-Printing Queue", "Instrument number updated successfully.", ICBO.IC.Dialogmessagetype.Success)
                                ClearAllTextBoxes()
                                LoadgvAccountPaymentNatureProductType(True)
                                Exit Sub

                            Else
                                UIUtilities.ShowDialog(Me, "Re-Printing Queue", "Specified instrument number is used or assigned", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        Else
                            UIUtilities.ShowDialog(Me, "Re-Printing Queue", "Specified instrument number is not assigned to print location", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If

                    Else
                        UIUtilities.ShowDialog(Me, "Re-Printing Queue", "Specified instrument number is invalid", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                ElseIf objICInstruction.PaymentMode = "PO" Then
                    If ICPOInstrumentsController.IsPOInstrumentNumberExistsByPrefixAndInstrumentNo(txtNewInstrumentNo.Text.ToString, "PO") = True Then
                        If ICPOInstrumentsController.IsPOInstrumentNoIsAssignedToSpecificPrintLocationByInstrumentNoAndOfficeCode(txtNewInstrumentNo.Text.ToString, "PO", objICInstruction.PrintLocationCode.ToString) = True Then
                            If ICPOInstrumentsController.IsPOInstrumentNumberAssignedOrPrintedByPrefixAndInstrumentNoAndInstructionID(txtNewInstrumentNo.Text.ToString, "PO", objICInstruction.PrintLocationCode.ToString) = False Then
                                StrAuditTrail = Nothing
                                StrAuditTrail += "Instrument number [ PO ] [ " & txtNewInstrumentNo.Text.ToString & " ] "
                                StrAuditTrail += "for instruction with ID [ " & objICInstruction.InstructionID & " ]. Previous instrument number was [ " & objICInstruction.InstrumentNo.ToString.Split("-")(0) & " ] [ " & objICInstruction.InstrumentNo.ToString.Split("-")(1) & " ]."
                                StrAuditTrail += "Action was taken by user [ " & Me.UserId.ToString & " ][ " & Me.UserInfo.Username.ToString & " ]."
                                objICInstruction.InstrumentNo = "PO-" & txtNewInstrumentNo.Text.ToString
                                ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAuditTrail, "UPDATE")
                                ICPOInstrumentsController.MarkInstrumentNumberAssignedByInstructionIDAndPOInstrumentNumber(txtNewInstrumentNo.Text.ToString, "PO", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, objICInstruction.PrintLocationCode.ToString)
                                UIUtilities.ShowDialog(Me, "Re-Printing Queue", "Instrument number updated successfully.", ICBO.IC.Dialogmessagetype.Success)
                                ClearAllTextBoxes()
                                LoadgvAccountPaymentNatureProductType(True)
                                Exit Sub

                            Else
                                UIUtilities.ShowDialog(Me, "Re-Printing Queue", "Specified instrument number is used or assigned", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        Else
                            UIUtilities.ShowDialog(Me, "Re-Printing Queue", "Specified instrument number is not assigned to print location", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Re-Printing Queue", "Specified instrument number is invalid", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else
                    UIUtilities.ShowDialog(Me, "Re-Printing Queue", "Invalid payment mode", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If




            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Function CheckgvClientRoleCheckedForProcessAll() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvAllowRePrintReIssue.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnCancelPrint_Click(sender As Object, e As System.EventArgs) Handles btnCancelPrint.Click
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim ArryListInstructionId As New ArrayList
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim objICInstruction As ICInstruction
                Dim StrInstCount As String = Nothing
                Dim StrArr As String() = Nothing
                ArryListInstructionId.Clear()


                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Printing Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                Dim dt As New DataTable
                dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Message", GetType(System.String)))
                Dim Remarks As String = ""
                If txtRemarks.Text <> "" And txtRemarks.Text <> "Enter Remarks" Then
                    Remarks = txtRemarks.Text.ToString
                End If
                For Each gvVerificationRow As GridDataItem In gvAllowRePrintReIssue.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        objICInstruction = New ICInstruction
                        objICInstruction.LoadByPrimaryKey(gvVerificationRow.GetDataKeyValue("InstructionID"))
                        If (objICInstruction.PaymentMode = "PO" Or objICInstruction.PaymentMode = "DD") And objICInstruction.Status = "23" Then
                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "52", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                            ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                        ElseIf objICInstruction.PaymentMode = "Cheque" And objICInstruction.Status = "23" Then
                            ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                        End If
                    End If
                Next

                If ArryListInstructionId.Count > 0 Then
                    StrInstCount = ICInstructionProcessController.CancelPrintOfInstructionFromPrintedQueue(ArryListInstructionId, Me.UserId, Me.UserInfo.Username.ToString, Remarks, "24", dt, "CANCEL REPRINT")
                Else
                    UIUtilities.ShowDialog(Me, "Error", "Invalid selected instruction(s) status", Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If
                RefreshPage()
                If dt.Rows.Count > 0 Then
                    gvShowSummary.DataSource = Nothing
                    gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                    gvShowSummary.DataBind()
                    pnlShowSummary.Style.Remove("display")
                    Dim scr As String
                    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                Else
                    gvShowSummary.DataSource = Nothing
                    pnlShowSummary.Style.Add("display", "none")
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Function CheckgvMultiRepoprtSelected() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            Dim ReportCount As Integer = 0
            For Each rowGVICClientRoles In gvAllowRePrintReIssue.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    ReportCount = ReportCount + 1
                End If
            Next
            If ReportCount > 1 Then
                Result = True
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Sub DesignDtForSummary()
        ViewState("Summary") = Nothing
        Dim dtSummary As New DataTable

        dtSummary.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
        dtSummary.Columns.Add(New DataColumn("Status", GetType(System.String)))
        dtSummary.Columns.Add(New DataColumn("Message", GetType(System.String)))
        ViewState("Summary") = dtSummary
    End Sub
    Protected Sub btnSinglePrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSinglePrint.Click
        If Page.IsValid Then
            Try

                Dim chkSelect As CheckBox
                Dim ArryListInstructionId As New ArrayList
                Dim InstructionCount As Integer = 0
                Dim ReportBook As New Telerik.Reporting.ReportBook
                Dim StrInstCount As String = Nothing
                Dim StrArr As String() = Nothing
                Dim objICUser As New ICUser
                Dim dtInstructionsForCheque As New DataTable
                Dim objICInstruction As ICInstruction
                Dim dtSummary As New DataTable
                objICUser.LoadByPrimaryKey(Me.UserId.ToString)


                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Re-Printing Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                If CheckgvMultiRepoprtSelected() = True Then
                    UIUtilities.ShowDialog(Me, "Re-Printing Queue", "Please select only one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each gvVerificationRow As GridDataItem In gvAllowRePrintReIssue.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        objICInstruction = New ICInstruction
                        objICInstruction.LoadByPrimaryKey(gvVerificationRow.GetDataKeyValue("InstructionID").ToString)
                        If objICInstruction.Status.ToString = "23" Then
                            InstructionCount = InstructionCount + 1
                            ArryListInstructionId.Add(objICInstruction.InstructionID.ToString)
                        End If
                    End If
                Next
                Dim Remarks As String = ""
                If txtRemarks.Text <> "" And txtRemarks.Text <> "Enter Remarks" Then
                    Remarks = txtRemarks.Text.ToString
                End If
                If ArryListInstructionId.Count > 0 Then
                    If CInt(GetRequiredInstrumentNosForPrint()) > 0 Then
                        If ICInstrumentController.GetTotalUnUSedInstrumentCountByPrintLocationCode(objICUser.OfficeCode) >= CInt(GetRequiredInstrumentNosForPrint()) Then
                            dtInstructionsForCheque = ICInstructionController.GetInstructionsByArrayInstructionIDGroupByAccountNo(ArryListInstructionId, objICUser.OfficeCode.ToString, "Cheque")
                            For Each dr As DataRow In dtInstructionsForCheque.Rows
                                If ICInstrumentController.GetRequiredUnAssignedAndUnUsedInstrumentNoByAccNoAndPrintLocation(dr("ClientAccountNo").ToString, objICUser.OfficeCode.ToString, CInt(dr("Count"))) = False Then
                                    UIUtilities.ShowDialog(Me, "Error", "Selected instruction can not be printed due to insufficient available stationary on printing location against client account(s).", ICBO.IC.Dialogmessagetype.Failure)
                                    Exit Sub
                                End If

                            Next

                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Selected instruction can not be printed due to insufficient available stationary on printing location", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    End If
                    dtSummary = DirectCast(ViewState("Summary"), DataTable)
                    dtSummary.Rows.Clear()
                    ReportBook = ICInstructionProcessController.PerformRePrintitngOnInstructions(ArryListInstructionId, Me.UserInfo.Username.ToString, Me.UserId.ToString, Remarks, "Print")
                Else
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Printing Queue", "There is an issue in printing. Please contact Al Baraka administrator.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                RefreshPage()
                If ReportBook.Reports.Count > 0 Then
                    rptCheque.Report = Nothing
                    rptCheque.Report = ReportBook
                    rptCheque.RefreshReport()
                    Dim scr As String = "window.onload = function() { " & Me.rptCheque.ClientID & ".PrintReport() }"
                    Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "scr", scr, True)
                Else
                    UIUtilities.ShowDialog(Me, "Re-Printing Queue", "There is an issue in re-printing. Please contact Al Baraka administrator.", ICBO.IC.Dialogmessagetype.Failure)
                End If
                'If dtSummary.Rows.Count > 0 Then

                '    gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dtSummary)
                '    gvShowSummary.DataBind()
                '    pnlShowSummary.Style.Remove("display")
                '    Dim scr As String
                '    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                '    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)

                'Else
                '    Dim drPrintSummary As DataRow
                '    drPrintSummary = dtSummary.NewRow
                '    drPrintSummary("InstructionID") = "-"
                '    drPrintSummary("Status") = "-"
                '    drPrintSummary("Message") = "There is an issue in re-printing. Please contact Al Baraka administrator."
                '    dtSummary.Rows.Add(drPrintSummary)
                '    gvShowSummary.DataSource = dtSummary
                '    gvShowSummary.DataBind()
                '    pnlShowSummary.Style.Remove("display")
                '    Dim scr As String
                '    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                '    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                'End If
                'RefreshPage()



            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Protected Sub btnPrintMultiple_Click(sender As Object, e As System.EventArgs) Handles btnPrintMultiple.Click
        If Page.IsValid Then
            Try

                Dim chkSelect As CheckBox
                Dim ArryListInstructionId As New ArrayList
                Dim InstructionCount As Integer = 0
                Dim ReportBook As New Telerik.Reporting.ReportBook
                Dim StrInstCount As String = Nothing
                Dim StrArr As String() = Nothing
                Dim objICUser As New ICUser
                Dim dtInstructionsForCheque As New DataTable
                Dim objICInstruction As ICInstruction
                objICUser.LoadByPrimaryKey(Me.UserId.ToString)
                Dim dtSummary As New DataTable
                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Re-Printing Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each gvVerificationRow As GridDataItem In gvAllowRePrintReIssue.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        objICInstruction = New ICInstruction
                        objICInstruction.LoadByPrimaryKey(gvVerificationRow.GetDataKeyValue("InstructionID"))
                        If objICInstruction.Status.ToString = "23" Then
                            InstructionCount = InstructionCount + 1
                            ArryListInstructionId.Add(objICInstruction.InstructionID)
                        End If
                    End If
                Next
                Dim Remarks As String = ""
                If txtRemarks.Text <> "" And txtRemarks.Text <> "Enter Remarks" Then
                    Remarks = txtRemarks.Text.ToString
                End If
                If ArryListInstructionId.Count > 0 Then

                    If CInt(GetRequiredInstrumentNosForPrint()) > 0 Then
                        If ICInstrumentController.GetTotalUnUSedInstrumentCountByPrintLocationCode(objICUser.OfficeCode) >= CInt(GetRequiredInstrumentNosForPrint()) Then
                            dtInstructionsForCheque = ICInstructionController.GetInstructionsByArrayInstructionIDGroupByAccountNo(ArryListInstructionId, objICUser.OfficeCode.ToString, "Cheque")
                            For Each dr As DataRow In dtInstructionsForCheque.Rows
                                If ICInstrumentController.GetRequiredUnAssignedAndUnUsedInstrumentNoByAccNoAndPrintLocation(dr("ClientAccountNo").ToString, objICUser.OfficeCode.ToString, CInt(dr("Count"))) = False Then
                                    UIUtilities.ShowDialog(Me, "Error", "Selected instruction can not be printed due to insufficient available stationary on printing location against client account(s).", ICBO.IC.Dialogmessagetype.Failure)
                                    Exit Sub
                                End If

                            Next

                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Selected instruction can not be printed due to insufficient available stationary on printing location", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    End If
                    dtSummary = DirectCast(ViewState("Summary"), DataTable)
                    dtSummary.Rows.Clear()
                    ArryListInstructionId.Sort()
                    ReportBook = ICInstructionProcessController.PerformRePrintitngOnInstructions(ArryListInstructionId, Me.UserInfo.Username.ToString, Me.UserId.ToString, Remarks, "Print")
                Else
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Error", "There is an issue in printing. Please contact Al Baraka administrator.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                RefreshPage()
                If ReportBook.Reports.Count > 0 Then
                    rptCheque.Report = Nothing
                    rptCheque.Report = ReportBook
                    rptCheque.RefreshReport()
                    Dim scr As String = "window.onload = function() { " & Me.rptCheque.ClientID & ".PrintReport() }"
                    Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "scr", scr, True)
                Else
                    UIUtilities.ShowDialog(Me, "Re-Printing Queue", "There is an issue in re-printing. Please contact Al Baraka administrator.", ICBO.IC.Dialogmessagetype.Failure)
                End If
                'If dtSummary.Rows.Count > 0 Then

                '    gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dtSummary)
                '    gvShowSummary.DataBind()
                '    pnlShowSummary.Style.Remove("display")
                '    Dim scr As String
                '    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                '    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)

                'Else
                '    Dim drPrintSummary As DataRow
                '    drPrintSummary = dtSummary.NewRow
                '    drPrintSummary("InstructionID") = "-"
                '    drPrintSummary("Status") = "-"
                '    drPrintSummary("Message") = "There is an issue in re-issuance. Please contact Al Baraka administrator."
                '    dtSummary.Rows.Add(drPrintSummary)
                '    gvShowSummary.DataSource = dtSummary
                '    gvShowSummary.DataBind()
                '    pnlShowSummary.Style.Remove("display")
                '    Dim scr As String
                '    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                '    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                'End If




            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Function GetRequiredInstrumentNosForPrint() As Integer
        Dim InstrumentNoCount As Integer = 0

        Dim chkSelect As CheckBox

        For Each rowGVICClientRoles As GridDataItem In gvAllowRePrintReIssue.Items
            chkSelect = New CheckBox
            chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
            If chkSelect.Checked = True Then
                If rowGVICClientRoles.GetDataKeyValue("PaymentMode").ToString = "Cheque" Then
                    If rowGVICClientRoles.GetDataKeyValue("InstrumentNo") Is Nothing Or rowGVICClientRoles.GetDataKeyValue("InstrumentNo") = "-" Or rowGVICClientRoles.GetDataKeyValue("InstrumentNo") = "" Then
                        InstrumentNoCount = InstrumentNoCount + 1
                    End If
                End If
            End If
        Next
        Return InstrumentNoCount
    End Function
    Protected Sub gvAllowRePrintReIssue_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAllowRePrintReIssue.NeedDataSource
        Try
            LoadgvAccountPaymentNatureProductType(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub raddpCreationFromDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationFromDate.SelectedDateChanged
        Try


            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Approval Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Approval from date should be less than approval to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub raddpCreationToDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationToDate.SelectedDateChanged
        Try


            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Approval Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Approval from date should be less than approval to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub rbtnlstDateSelection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnlstDateSelection.SelectedIndexChanged
        Try
            If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now.AddYears(2)
            Else
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
            End If

            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Approval Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Approval from date should be less than approval to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click

    End Sub
End Class


