﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_InvoiceDataTemplate_ViewInvoiceDataTemplate
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable

    Protected Sub DesktopModules_Bank_ViewBank_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnSaveInvoiceDataTemplate.Visible = CBool(htRights("Add"))
                LoadgvInvoiceDataTemplate(gvInvoiceDataTemplate.CurrentPageIndex + 1, Me.gvInvoiceDataTemplate.PageSize, True)

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Invoice Data Template")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    

    Private Sub LoadgvInvoiceDataTemplate(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean)
        Try

            ICCollectionInvoiceDataTemplateController.GetInvoiceDataTemplateForRadGrid(PageNumber, PageSize, Me.gvInvoiceDataTemplate, DoDataBind)

            If gvInvoiceDataTemplate.Items.Count > 0 Then
                gvInvoiceDataTemplate.Visible = True
                lblRNF.Visible = False
                btnBulkDeleteInvoiceDataTemplate.Visible = CBool(htRights("Delete"))
                gvInvoiceDataTemplate.Columns(10).Visible = CBool(htRights("Delete"))
                gvInvoiceDataTemplate.Columns(7).Visible = CBool(htRights("Manage Fields"))
            Else
                gvInvoiceDataTemplate.Visible = False
                btnBulkDeleteInvoiceDataTemplate.Visible = False
                lblRNF.Visible = True

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Protected Sub btnSaveBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveInvoiceDataTemplate.Click
        Response.Redirect(NavigateURL("AddInvoiceDataTemplate", "&mid=" & Me.ModuleId & "&id=0"), False)
    End Sub

    Protected Sub gvInvoiceDataTemplate_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvInvoiceDataTemplate.ItemCommand
        Try
            Dim objICTemplate As New ICCollectionInvoiceDataTemplate
            objICTemplate.es.Connection.CommandTimeout = 3600
            If e.CommandName = "del" Then
                If ICCollectionInvoiceDataTemplateFieldsListController.CheckInvoiceStatusAgainstTemplate(e.CommandArgument.ToString) = False Then
                    If objICTemplate.LoadByPrimaryKey(e.CommandArgument.ToString) Then
                        If objICTemplate.TemplateType = "Dual" Then
                            ICCollectionInvoiceDataTemplateController.DeleteInvoiceDataBase(objICTemplate.CollectionInvoiceTemplateID, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Dual")
                            ICCollectionInvoiceDataTemplateController.DeleteInvoiceDataBase(objICTemplate.CollectionInvoiceTemplateID, Me.UserId.ToString, Me.UserInfo.Username.ToString, "NonFlexi")
                            ICCollectionInvoiceDataTemplateFieldsListController.DeleteTemplateFieldForUpdate(objICTemplate.CollectionInvoiceTemplateID.ToString, objICTemplate.TemplateName.ToString(), Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            ICCollectionInvoiceDataTemplateController.DeleteTemplate(objICTemplate.CollectionInvoiceTemplateID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            UIUtilities.ShowDialog(Me, "Delete Invoice Data Template", "Invoice Data Template deleted successfully", ICBO.IC.Dialogmessagetype.Success)
                            LoadgvInvoiceDataTemplate(Me.gvInvoiceDataTemplate.CurrentPageIndex + 1, Me.gvInvoiceDataTemplate.PageSize, True)
                        Else
                            ICCollectionInvoiceDataTemplateController.DeleteInvoiceDataBase(objICTemplate.CollectionInvoiceTemplateID, Me.UserId.ToString, Me.UserInfo.Username.ToString, "NonFlexi")
                            ICCollectionInvoiceDataTemplateFieldsListController.DeleteTemplateFieldForUpdate(objICTemplate.CollectionInvoiceTemplateID.ToString, objICTemplate.TemplateName.ToString(), Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            ICCollectionInvoiceDataTemplateController.DeleteTemplate(objICTemplate.CollectionInvoiceTemplateID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            UIUtilities.ShowDialog(Me, "Delete Invoice Data Template", "Invoice Data Template deleted successfully", ICBO.IC.Dialogmessagetype.Success)
                            LoadgvInvoiceDataTemplate(Me.gvInvoiceDataTemplate.CurrentPageIndex + 1, Me.gvInvoiceDataTemplate.PageSize, True)
                        End If
                    End If
                Else
                    UIUtilities.ShowDialog(Me, "Error", "Invoice Data Template can not deleted due to following reason: <br /> 1. Files exists in pending read status", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    LoadgvInvoiceDataTemplate(Me.gvInvoiceDataTemplate.CurrentPageIndex + 1, Me.gvInvoiceDataTemplate.PageSize, False)
                    Exit Sub
                End If

            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Error ", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                LoadgvInvoiceDataTemplate(Me.gvInvoiceDataTemplate.CurrentPageIndex + 1, Me.gvInvoiceDataTemplate.PageSize, False)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvInvoiceDataTemplate_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvInvoiceDataTemplate.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvInvoiceDataTemplate.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvInvoiceDataTemplate_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvInvoiceDataTemplate.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvInvoiceDataTemplate.MasterTableView.ClientID & "','0');"
        End If
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Dim hlEdit As HyperLink
            hlEdit = New HyperLink
            hlEdit = DirectCast(item.Cells(7).FindControl("hlEdit"), HyperLink)
            If item.GetDataKeyValue("IsPublished") = True Then
                hlEdit.Visible = False
            Else
                hlEdit.Visible = True
            End If

        End If
    End Sub

    Private Function CheckgvInvoiceDataTemplateSelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvInvoiceDataTemplateRow As GridDataItem In gvInvoiceDataTemplate.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvInvoiceDataTemplateRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnBulkDeleteInvoiceDataTemplate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBulkDeleteInvoiceDataTemplate.Click
        Try
            Dim chkBox As CheckBox
            Dim objICTemplate As ICCollectionInvoiceDataTemplate
            Dim PassToDeleteCount As Integer = 0
            Dim FailToDeleteCount As Integer = 0
            Dim InvoiceDataTemplateID As String

            If CheckgvInvoiceDataTemplateSelected() = False Then
                UIUtilities.ShowDialog(Me, "Error", "Please select at least one invoice data template", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            For Each gvFUTemplateRow As GridDataItem In gvInvoiceDataTemplate.Items
                chkBox = New CheckBox
                chkBox = DirectCast(gvFUTemplateRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkBox.Checked = True Then
                    objICTemplate = New ICCollectionInvoiceDataTemplate
                    objICTemplate.es.Connection.CommandTimeout = 3600
                    InvoiceDataTemplateID = gvFUTemplateRow.GetDataKeyValue("CollectionInvoiceTemplateID")
                    objICTemplate.LoadByPrimaryKey(InvoiceDataTemplateID)
                    If ICCollectionInvoiceDataTemplateFieldsListController.CheckInvoiceStatusAgainstTemplate(InvoiceDataTemplateID) = False Then
                        Try
                            If objICTemplate.TemplateType = "Dual" Then
                                ICCollectionInvoiceDataTemplateController.DeleteInvoiceDataBase(InvoiceDataTemplateID, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Detail")
                                ICCollectionInvoiceDataTemplateController.DeleteInvoiceDataBase(InvoiceDataTemplateID, Me.UserId.ToString, Me.UserInfo.Username.ToString, "NonFlexi")
                                ICCollectionInvoiceDataTemplateFieldsListController.DeleteTemplateFieldForUpdate(InvoiceDataTemplateID, objICTemplate.TemplateName.ToString(), Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                ICCollectionInvoiceDataTemplateController.DeleteTemplate(InvoiceDataTemplateID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                PassToDeleteCount = PassToDeleteCount + 1
                            Else
                                ICCollectionInvoiceDataTemplateController.DeleteInvoiceDataBase(InvoiceDataTemplateID, Me.UserId.ToString, Me.UserInfo.Username.ToString, "NonFlexi")
                                ICCollectionInvoiceDataTemplateFieldsListController.DeleteTemplateFieldForUpdate(InvoiceDataTemplateID, objICTemplate.TemplateName.ToString(), Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                ICCollectionInvoiceDataTemplateController.DeleteTemplate(InvoiceDataTemplateID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                PassToDeleteCount = PassToDeleteCount + 1
                            End If
                        Catch ex As Exception
                            FailToDeleteCount = FailToDeleteCount + 1
                            Continue For
                        End Try
                    Else
                        FailToDeleteCount = FailToDeleteCount + 1
                    End If
                End If
            Next
            If Not PassToDeleteCount = 0 And Not FailToDeleteCount = 0 Then
                LoadgvInvoiceDataTemplate(Me.gvInvoiceDataTemplate.CurrentPageIndex + 1, Me.gvInvoiceDataTemplate.PageSize, True)
                UIUtilities.ShowDialog(Me, "Delete Invoice Data Template", "[ " & PassToDeleteCount.ToString & " ] Invoice Data Template(s) deleted successfully.<br /> [ " & FailToDeleteCount.ToString & " ] Invoice Data Template(s) can not be deleted due to following reason: <br /> 1. File exist with pending read status<br /> 2. Delete associated records", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            ElseIf Not PassToDeleteCount = 0 And FailToDeleteCount = 0 Then
                LoadgvInvoiceDataTemplate(Me.gvInvoiceDataTemplate.CurrentPageIndex + 1, Me.gvInvoiceDataTemplate.PageSize, True)
                UIUtilities.ShowDialog(Me, "Delete Invoice Data Template", PassToDeleteCount.ToString & " Invoice Data Template(s) deleted successfully.", ICBO.IC.Dialogmessagetype.Success)

                Exit Sub
            ElseIf Not FailToDeleteCount = 0 And PassToDeleteCount = 0 Then
                LoadgvInvoiceDataTemplate(Me.gvInvoiceDataTemplate.CurrentPageIndex + 1, Me.gvInvoiceDataTemplate.PageSize, True)
                UIUtilities.ShowDialog(Me, "Delete Invoice Data Template", "[ " & FailToDeleteCount.ToString & " ] Invoice Data Template(s) can not be deleted due to following reasons: <br /> 1. File exist with pending read status<br /> 2. Delete associated records", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvInvoiceDataTemplate_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvInvoiceDataTemplate.NeedDataSource
        Try
            LoadgvInvoiceDataTemplate(Me.gvInvoiceDataTemplate.CurrentPageIndex + 1, Me.gvInvoiceDataTemplate.PageSize, False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


End Class
