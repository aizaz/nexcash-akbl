﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_InvoiceDataTemplateManagement_ManageFields
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private FileUploadTemplateCode As String
    Private htRights As Hashtable


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            FileUploadTemplateCode = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                Dim objICInvoiceDBTemplate As New ICCollectionInvoiceDataTemplate
                objICInvoiceDBTemplate.LoadByPrimaryKey(FileUploadTemplateCode)
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                If objICInvoiceDBTemplate.IsPublished = False Or objICInvoiceDBTemplate.IsPublished Is Nothing Then
                    btnSave.Visible = CBool(htRights("Add"))
                    tblAddFields.Style.Remove("display")
                    btnCancel2.Visible = False
                Else
                    btnSave.Visible = False
                    btnPublish.Visible = False
                    tblAddFields.Style.Add("display", "none")
                    btnCancel2.Visible = True
                End If
                LoadGVInvoiceDBTemplateFields(True)
                Clear()
                'HideFieldLengthTable("")
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Invoice Data Template")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadGVInvoiceDBTemplateFields(ByVal IsBind As Boolean)

        Try
            Dim objICInvoiceDBTemplate As New ICCollectionInvoiceDataTemplate
            objICInvoiceDBTemplate.LoadByPrimaryKey(FileUploadTemplateCode)
            ICCollectionInvoiceDataTemplateFieldsListController.GetAllTemplateFieldByTemplateIDForRadGrid(FileUploadTemplateCode, Me.gvInvoiceDBTemplate.CurrentPageIndex + 1, Me.gvInvoiceDBTemplate.PageSize, Me.gvInvoiceDBTemplate, IsBind)

            If gvInvoiceDBTemplate.Items.Count > 0 Then
                gvInvoiceDBTemplate.Visible = True

                If objICInvoiceDBTemplate.IsPublished = False Or objICInvoiceDBTemplate.IsPublished Is Nothing Then
                    btnPublish.Visible = CBool(htRights("Publish"))
                    gvInvoiceDBTemplate.Columns(6).Visible = CBool(htRights("Delete"))
                    btnSave.Visible = CBool(htRights("Add"))
                    tblAddFields.Style.Remove("display")
                    btnCancel2.Visible = False
                Else
                    btnPublish.Visible = False
                    gvInvoiceDBTemplate.Columns(6).Visible = False
                    btnSave.Visible = False
                    tblAddFields.Style.Add("display", "none")
                    btnCancel2.Visible = True
                End If

            Else
                gvInvoiceDBTemplate.Visible = False
                btnPublish.Visible = False
                btnPublish.Visible = False
                tblAddFields.Style.Remove("display")
                btnCancel2.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
       





    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objICInvoiceDBTemplateField As New ICCollectionInvoiceDataTemplateFields
            Dim objICCollInvoiceDBTemplate As New ICCollectionInvoiceDataTemplate

            objICCollInvoiceDBTemplate.LoadByPrimaryKey(FileUploadTemplateCode)

            If ddlFieldType.SelectedValue.ToString = "Date" And chkReference.Checked = True Then
                UIUtilities.ShowDialog(Me, "Invoice DB Template Field", "Date type field can not be set as reference", Dialogmessagetype.Failure)
                Exit Sub
            End If
            objICInvoiceDBTemplateField.FieldName = txtFieldName.Text
            objICInvoiceDBTemplateField.FieldDBType = ddlFieldType.SelectedValue.ToString
            If txtFieldLength.Text <> "" Then
                objICInvoiceDBTemplateField.FieldLength = txtFieldLength.Text
            Else
                objICInvoiceDBTemplateField.FieldLength = Nothing
            End If

            objICInvoiceDBTemplateField.CreateBy = Me.UserId
            objICInvoiceDBTemplateField.CreateDate = Date.Now
            objICInvoiceDBTemplateField.IsReference = chkReference.Checked
            objICInvoiceDBTemplateField.CollectionInvoiceTemplateID = FileUploadTemplateCode
            If rblFieldType.SelectedValue <> "" Then
                objICInvoiceDBTemplateField.FieldType = rblFieldType.SelectedValue.ToString
            Else
                objICInvoiceDBTemplateField.FieldType = "NonFlexi"
            End If
            objICInvoiceDBTemplateField.FieldOrder = GetMaxFieldOrder(FileUploadTemplateCode, objICInvoiceDBTemplateField.FieldType) + 1
            If CheckDuplicateFieldName(objICInvoiceDBTemplateField.FieldName, FileUploadTemplateCode) = False Then
                ICCollectionInvoiceDataTemplateFieldsListController.AddTemplateField(objICInvoiceDBTemplateField, objICInvoiceDBTemplateField.UpToICCollectionInvoiceDataTemplateByCollectionInvoiceTemplateID.TemplateName, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                UIUtilities.ShowDialog(Me, "Invoice DB Template Field", "Invoice DB template field added successfully.", Dialogmessagetype.Success)
                LoadGVInvoiceDBTemplateFields(True)
                Clear()
            Else
                UIUtilities.ShowDialog(Me, "Invoice DB Template Field", "Duplicate Invoice DB template field is not allowed.", Dialogmessagetype.Failure)
                Exit Sub
            End If
           
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckDuplicateFieldName(ByVal FieldName As String, ByVal FileUploadTemplateCode As String) As Boolean
        Dim ObjICInvoiceDBFieldColl As New ICCollectionInvoiceDataTemplateFieldsCollection


        ObjICInvoiceDBFieldColl.Query.Where(ObjICInvoiceDBFieldColl.Query.FieldName.ToLower = FieldName.ToLower And ObjICInvoiceDBFieldColl.Query.CollectionInvoiceTemplateID = FileUploadTemplateCode)
        If ObjICInvoiceDBFieldColl.Query.Load Then
            Return True
        Else
            Return False
        End If


    End Function
    Private Sub Clear()
        Dim objICInvoiceDBTemplate As New ICCollectionInvoiceDataTemplate
        objICInvoiceDBTemplate.LoadByPrimaryKey(FileUploadTemplateCode)
        txtFieldName.Text = ""
        txtFieldLength.Text = ""
        ddlFieldType.ClearSelection()
        HideFieldLengthTable(ddlFieldType.SelectedValue.ToString, objICInvoiceDBTemplate.TemplateType, objICInvoiceDBTemplate.IsFixLength)
        chkReference.Checked = False
        rblFieldType.ClearSelection()
    End Sub
    Private Function GetMaxFieldOrder(ByVal FileUploadTemplateCode As String, ByVal FieldType As String) As Integer
        Dim qryObjICCollInvoiceDBField As New ICCollectionInvoiceDataTemplateFieldsQuery("qryObjICCollInvoiceDBField")
        Dim dt As New DataTable

        qryObjICCollInvoiceDBField.Select((qryObjICCollInvoiceDBField.FieldOrder.Max.Coalesce("'0'")).As("FieldOrder"))
        qryObjICCollInvoiceDBField.Where(qryObjICCollInvoiceDBField.CollectionInvoiceTemplateID = FileUploadTemplateCode And qryObjICCollInvoiceDBField.FieldType = FieldType)
        dt = qryObjICCollInvoiceDBField.LoadDataTable
        Return CInt(dt.Rows(0)(0))


    End Function
    Protected Sub ddlFieldType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFieldType.SelectedIndexChanged
        Try
            Dim objICInvoiceDBTemplate As New ICCollectionInvoiceDataTemplate
            objICInvoiceDBTemplate.LoadByPrimaryKey(FileUploadTemplateCode)
            HideFieldLengthTable(ddlFieldType.SelectedValue.ToString, objICInvoiceDBTemplate.TemplateType, objICInvoiceDBTemplate.IsFixLength)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub HideFieldLengthTable(ByVal FieldType As String, ByVal TemplateType As String, ByVal IsFixLength As Boolean)

        tblFieldType.Style.Add("display", "none")
        rfvFieldType.Enabled = False
        rvLength.Enabled = False
        rvLength.ErrorMessage = ""
        If FieldType <> "0" Then
            If FieldType = "String" Then
                rvLength.MinimumValue = 1
                rvLength.MaximumValue = 999
                rvLength.ErrorMessage = "Length value must be from 1 to 999"
                rvLength.Type = ValidationDataType.Integer
                rvLength.Enabled = True
                txtFieldLength.Text = Nothing
            ElseIf FieldType = "Integer" Then
                rvLength.MinimumValue = 1
                rvLength.MaximumValue = 10
                rvLength.ErrorMessage = "Length value must be from 1 to 10"
                rvLength.Type = ValidationDataType.Integer
                rvLength.Enabled = True
                txtFieldLength.Text = Nothing
            ElseIf FieldType = "Decimal" Then
                rvLength.MinimumValue = 1
                rvLength.MaximumValue = 23
                rvLength.ErrorMessage = "Length value must be from 1 to 23"
                rvLength.Enabled = True
                txtFieldLength.Text = Nothing
                rvLength.Type = ValidationDataType.Integer
            ElseIf FieldType = "Date" Then
                rvLength.MinimumValue = 1
                rvLength.MaximumValue = 23
                rvLength.ErrorMessage = "Length value must be from 1 to 23"
                rvLength.Type = ValidationDataType.Integer
                rvLength.Enabled = True
                txtFieldLength.Text = Nothing
            End If
            If IsFixLength = False Then
                If TemplateType = "Dual" Then
                    tblFieldType.Style.Remove("display")
                    rfvFieldType.Enabled = True
                    rvLength.Enabled = True
                ElseIf TemplateType = "Single" Then
                    tblFieldType.Style.Add("display", "none")
                    rfvFieldType.Enabled = False
                Else
                    tblFieldType.Style.Add("display", "none")
                    rfvFieldType.Enabled = False
                End If
            ElseIf IsFixLength = True Then
                txtFieldLength.Text = ""
                tblFieldType.Style.Add("display", "none")
                rfvFieldType.Enabled = False
            End If
        End If
       
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL())
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvInvoiceDBTemplate_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles gvInvoiceDBTemplate.ItemCommand
        Try
            If e.CommandName = "del" Then
                Dim objICFieldList As New ICCollectionInvoiceDataTemplateFields

                If objICFieldList.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                   
                    ICCollectionInvoiceDataTemplateFieldsListController.DeleteTemplateFieldByFieldID(objICFieldList.CollectionFieldID.ToString, FileUploadTemplateCode, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)

                    ICCollectionInvoiceDataTemplateFieldsListController.UpdateInvoiceDBTemplatesFieldOrder(FileUploadTemplateCode, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, e.Item.Cells(5).Text.ToString)
                    UIUtilities.ShowDialog(Me, "Delete Invoice DB Template Field", "Invoice DB template field deleted successfully.", Dialogmessagetype.Success)
                    LoadGVInvoiceDBTemplateFields(True)
                End If
        
            End If

        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Delete Invoice DB Template Field", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckValidReferenceFieldsSelected() As Boolean
        Try
            Dim rowGVFields As GridDataItem
            Dim RefCount As Integer = 0
            Dim Result As Boolean = True
            For Each rowGVFields In gvInvoiceDBTemplate.Items
                If rowGVFields.GetDataKeyValue("IsReference") = True Then
                    RefCount = RefCount + 1
                End If
            Next
            If RefCount = 0 Or RefCount > 5 Then
                Result = False
            End If

            Return Result


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function CheckValidDecimalFields() As Boolean
        Try
            Dim rowGVFields As GridDataItem
            Dim RefCount As Integer = 0
            Dim Result As Boolean = True
            For Each rowGVFields In gvInvoiceDBTemplate.Items
                If rowGVFields.Item("FieldDBType").Text.ToString = "Decimal" Then
                    RefCount = RefCount + 1
                End If
            Next
            If RefCount = 0 Or RefCount < 2 Then
                Result = False
            End If

            Return Result


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function CheckValidDateFields() As Boolean
        Try
            Dim rowGVFields As GridDataItem
            Dim RefCount As Integer = 0
            Dim Result As Boolean = True
            For Each rowGVFields In gvInvoiceDBTemplate.Items
                If rowGVFields.Item("FieldDBType").Text.ToString = "Date" Then
                    RefCount = RefCount + 1
                End If
            Next
            If RefCount = 0 Then
                Result = False
            End If

            Return Result


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    'Private Function CheckDetailFieldSelected() As Boolean
    '    Try
    '        Dim rowGVFields As GridDataItem
    '        Dim RefCount As Integer = 0
    '        Dim Result As Boolean = True
    '        For Each rowGVFields In gvInvoiceDBTemplate.Items
    '            If rowGVFields.GetDataKeyValue("FieldType") = "Detail" Then
    '                RefCount = RefCount + 1
    '            End If
    '        Next
    '        If RefCount = 0 Then
    '            For Each rowGVFields In gvInvoiceDBTemplate.Items
    '                If rowGVFields.GetDataKeyValue("FieldType") = "NonFlexi" Then
    '                    RefCount = RefCount + 1
    '                End If
    '            Next
    '        End If

    '        If RefCount = 0 Then
    '            Result = False
    '        End If
    '        Return Result

    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Function

    Private Function CheckDetailFieldSelected() As Boolean
        Try
            Dim rowGVFields As GridDataItem
            Dim DetailCount As Integer = 0
            Dim NonFlexiCount As Integer = 0
            Dim Result As Boolean = True

            For Each rowGVFields In gvInvoiceDBTemplate.Items
                If rowGVFields.GetDataKeyValue("FieldType") = "Detail" Then
                    DetailCount = DetailCount + 1
                End If
            Next

            For Each rowGVFields In gvInvoiceDBTemplate.Items
                If rowGVFields.GetDataKeyValue("FieldType") = "NonFlexi" Then
                    NonFlexiCount = NonFlexiCount + 1
                End If
            Next

            If NonFlexiCount = 0 Or DetailCount = 0 Then
                Result = False
            End If
            Return Result

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function CheckDetailFieldSelected(ByVal FieldType As String) As Boolean
        Try
            Dim rowGVFields As GridDataItem
            Dim RefCount As Integer = 0
            Dim Result As Boolean = True
            For Each rowGVFields In gvInvoiceDBTemplate.Items
                If rowGVFields.GetDataKeyValue("FieldDBType") = FieldType Then
                    RefCount = RefCount + 1
                End If
            Next
            If FieldType = "Date" And RefCount < 1 Then
                Result = False
            Else
                If RefCount = 0 Then
                    Result = False
                End If
            End If
            Return Result

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnPublish_Click(sender As Object, e As EventArgs) Handles btnPublish.Click
        Try
            Dim objICInvoiceDBDataTemplate As New ICCollectionInvoiceDataTemplate
            Dim str As String = Nothing
            Dim StrDetail As String = Nothing
            Dim ActionStr As String = Nothing

            ''Reference Field Check
            If CheckValidReferenceFieldsSelected() = False Then
                UIUtilities.ShowDialog(Me, "Publish Invoice DB Template", "Minimum 1 and maximum 5 reference fields must be selected.", Dialogmessagetype.Failure)
                Exit Sub
            End If
            If CheckValidDecimalFields() = False Then
                UIUtilities.ShowDialog(Me, "Publish Invoice DB Template", "Minimum 2 decimal fields must be included", Dialogmessagetype.Failure)
                Exit Sub
            End If

            If CheckDetailFieldSelected("Date") = False Then
                UIUtilities.ShowDialog(Me, "Publish Invocie DB Template", "Minimum 1 date field must be included", Dialogmessagetype.Failure)
                Exit Sub
            End If
            objICInvoiceDBDataTemplate.LoadByPrimaryKey(FileUploadTemplateCode)
            If objICInvoiceDBDataTemplate.TemplateType = "Dual" Then
                If CheckDetailFieldSelected("NonFlexi") = False Then
                    UIUtilities.ShowDialog(Me, "Publish Invocie DB Template", "At least one NonFlexi field must be selected for dual template.", Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If CheckDetailFieldSelected("Detail") = False Then
                    UIUtilities.ShowDialog(Me, "Publish Invocie DB Template", "At least one detail field must be selected for dual template.", Dialogmessagetype.Failure)
                    Exit Sub
                End If
            End If

            If Me.UserInfo.IsSuperUser = False Then
                If objICInvoiceDBDataTemplate.CreateBy = Me.UserInfo.UserID.ToString Then
                    UIUtilities.ShowDialog(Me, "Publish Invoice DB Template", "Invoice DB Template must be published by user other than maker.", Dialogmessagetype.Failure)
                    Exit Sub
                End If
            End If




            str = ICCollectionSQLController.GetSQLStringWithFieldsByTemplateID(FileUploadTemplateCode, ActionStr, StrDetail)
            'str = GetSQLStringWithFieldsByTemplateID(FileUploadTemplateCode, ActionStr)
            Try
                If objICInvoiceDBDataTemplate.TemplateType = "Dual" Then
                    If ICCollectionInvoiceDataTemplateController.DoesTableExist("IC_InvoiceDB_" & objICInvoiceDBDataTemplate.TemplateName) = False Then
                        If ICCollectionInvoiceDataTemplateController.DoesTableExist("IC_InvoiceDB_" & objICInvoiceDBDataTemplate.TemplateName & "_Detail") = False Then
                            Try
                                If ICCollectionSQLController.ExecuteSqlStatementForCreateTableVIATemplate(str) = True Then
                                    Try
                                        If ICCollectionSQLController.ExecuteSqlStatementForCreateTableVIATemplate(StrDetail) = True Then
                                            objICInvoiceDBDataTemplate.IsPublished = True
                                            objICInvoiceDBDataTemplate.IsActive = True
                                            ICCollectionInvoiceDataTemplateController.AddInvoiceDataTemplate(objICInvoiceDBDataTemplate, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                            ICUtilities.AddAuditTrail(ActionStr, "Invoice Data Template", objICInvoiceDBDataTemplate.CollectionInvoiceTemplateID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")
                                            UIUtilities.ShowDialog(Me, "Invoice Data Template Management", "Invoice data base for template [ " & objICInvoiceDBDataTemplate.TemplateName & " ] created successfully.", ICBO.IC.Dialogmessagetype.Success)
                                            LoadGVInvoiceDBTemplateFields(True)
                                            Exit Sub
                                        Else
                                            ICCollectionInvoiceDataTemplateController.DeleteInvoiceDataBase(objICInvoiceDBDataTemplate.CollectionInvoiceTemplateID, Me.UserId.ToString, Me.UserInfo.Username.ToString, "NonFlexi")
                                            UIUtilities.ShowDialog(Me, "Invoice Data Template Management", "unable to create data base for template for detail [ " & objICInvoiceDBDataTemplate.TemplateName & " ].", ICBO.IC.Dialogmessagetype.Failure)
                                            LoadGVInvoiceDBTemplateFields(True)
                                            Exit Sub
                                        End If

                                    Catch ex As Exception
                                        ICCollectionInvoiceDataTemplateController.DeleteInvoiceDataBase(objICInvoiceDBDataTemplate.CollectionInvoiceTemplateID, Me.UserId.ToString, Me.UserInfo.Username.ToString, "NonFlexi")
                                        UIUtilities.ShowDialog(Me, "Invoice Data Template Management", "unable to create data base for template for detail [ " & objICInvoiceDBDataTemplate.TemplateName & " ].", ICBO.IC.Dialogmessagetype.Failure)
                                        LoadGVInvoiceDBTemplateFields(True)
                                        Exit Sub
                                    End Try
                                End If
                            Catch ex As Exception
                                UIUtilities.ShowDialog(Me, "Invoice Data Template Management", "Unable to create data base for template [ " & objICInvoiceDBDataTemplate.TemplateName & " ] due to " & ex.Message.ToString & "", ICBO.IC.Dialogmessagetype.Failure)
                                LoadGVInvoiceDBTemplateFields(True)
                                Exit Sub
                            End Try
                        Else
                            UIUtilities.ShowDialog(Me, "Invoice Data Template Management", "Invoice data base for template for detail [ " & objICInvoiceDBDataTemplate.TemplateName & " ] already exists.", ICBO.IC.Dialogmessagetype.Failure)
                            LoadGVInvoiceDBTemplateFields(True)
                            Exit Sub
                        End If

                    Else
                        UIUtilities.ShowDialog(Me, "Invoice Data Template Management", "Invoice data base for template [ " & objICInvoiceDBDataTemplate.TemplateName & " ] already exists.", ICBO.IC.Dialogmessagetype.Failure)
                        LoadGVInvoiceDBTemplateFields(True)
                        Exit Sub
                    End If
                Else
                    If ICCollectionInvoiceDataTemplateController.DoesTableExist("IC_InvoiceDB_" & objICInvoiceDBDataTemplate.TemplateName) = False Then
                        If ICCollectionSQLController.ExecuteSqlStatementForCreateTableVIATemplate(str) = True Then
                            objICInvoiceDBDataTemplate.IsPublished = True
                            objICInvoiceDBDataTemplate.IsActive = True
                            ICCollectionInvoiceDataTemplateController.AddInvoiceDataTemplate(objICInvoiceDBDataTemplate, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            ICUtilities.AddAuditTrail(ActionStr, "Invoice Data Template", objICInvoiceDBDataTemplate.CollectionInvoiceTemplateID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")
                            UIUtilities.ShowDialog(Me, "Invoice Data Template Management", "Invoice data base for template [ " & objICInvoiceDBDataTemplate.TemplateName & " ] created successfully.", ICBO.IC.Dialogmessagetype.Success)
                            LoadGVInvoiceDBTemplateFields(True)
                            Exit Sub
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Invoice Data Template Management", "Invoice data base for template [ " & objICInvoiceDBDataTemplate.TemplateName & " ] already exists.", ICBO.IC.Dialogmessagetype.Failure)
                        LoadGVInvoiceDBTemplateFields(True)
                        Exit Sub
                    End If
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End Try
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvInvoiceDBTemplate_ItemCreated(sender As Object, e As GridItemEventArgs) Handles gvInvoiceDBTemplate.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvInvoiceDBTemplate.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    

    Protected Sub gvInvoiceDBTemplate_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles gvInvoiceDBTemplate.NeedDataSource
        Try
            LoadGVInvoiceDBTemplateFields(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvInvoiceDBTemplate_PageIndexChanged(sender As Object, e As GridPageChangedEventArgs) Handles gvInvoiceDBTemplate.PageIndexChanged
        Try
            gvInvoiceDBTemplate.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnCancel2_Click(sender As Object, e As EventArgs) Handles btnCancel2.Click
        Try
            Response.Redirect(NavigateURL())
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
