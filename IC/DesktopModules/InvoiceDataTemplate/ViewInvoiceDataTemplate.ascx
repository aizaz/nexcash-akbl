﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewInvoiceDataTemplate.ascx.vb"
 Inherits="DesktopModules_InvoiceDataTemplate_ViewInvoiceDataTemplate" %>

<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to delete this Record? If databse exists against template it will also be deleted permanently.") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to remove Record(s)? If databse exists against template(s) it will also be deleted permanently") == true) {
            return true;
        }
        else {
            return false;
        }
    }  
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;

        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    //    }
</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnSaveInvoiceDataTemplate" runat="server" Text="Add Invoice Data Template" CssClass="btn" />
                        <asp:HiddenField ID="hfFUTemplateID" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <asp:Label ID="lblInvoiceDataTemplateListHeader" runat="server" Text="Invoice Data Template List" CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <asp:Label ID="lblRNF" runat="server" Text="Record Not Found." CssClass="headingblue"
                            Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                    <telerik:RadGrid ID="gvInvoiceDataTemplate" runat="server" AllowPaging="True" 
                            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                            PageSize="10">
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView NoMasterRecordsText="" DataKeyNames="CollectionInvoiceTemplateID,IsPublished">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Approve" DataField="IsApproved">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="TemplateName" HeaderText="Name" SortExpression="TemplateName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TemplateFormat" HeaderText="Format" SortExpression="TemplateFormat">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="SheetName" HeaderText="Sheet Name"
                                        SortExpression="SheetName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FieldDelimiter" HeaderText="Field Delimiter"
                                        SortExpression="FieldDelimiter">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TemplateType" HeaderText="Template Type"
                                        SortExpression="TemplateType">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Fix Length" DataField="IsFixLength">
                                    </telerik:GridCheckBoxColumn>
                                      <telerik:GridCheckBoxColumn HeaderText="Is Published" DataField="IsPublished">
                                    </telerik:GridCheckBoxColumn>
                                     <telerik:GridCheckBoxColumn HeaderText="Is Active" DataField="IsActive">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridTemplateColumn HeaderText="Edit">
                                        <ItemTemplate>
                                        <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("AddInvoiceDataTemplate", "&mid=" & Me.ModuleId & "&id="& Eval("CollectionInvoiceTemplateID"))%>'
                                            ToolTip="Edit">Edit</asp:HyperLink>
                                    </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Manage Fields">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlManageFields" runat="server" ImageUrl="~/images/add.gif"
                                                NavigateUrl='<%#NavigateURL("ManageFields", "&mid=" & Me.ModuleId & "&id=" & Eval("CollectionInvoiceTemplateID"))%>'
                                                ToolTip="Add Fields" Text="Manage Fields"></asp:HyperLink>
                                        </ItemTemplate>
                                          <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridTemplateColumn>
                                  <telerik:GridTemplateColumn HeaderText="Delete">
                                       <ItemTemplate>
                                        <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("CollectionInvoiceTemplateID") %>'
                                            CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                            ToolTip="Delete" />
                                    </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnBulkDeleteInvoiceDataTemplate" runat="server" OnClientClick="javascript: return conBukDelete();"
                            Text="Delete Invoice Data Template" CssClass="btn" />
                    </td>
                </tr>
               </table>
</td>
</tr></table>