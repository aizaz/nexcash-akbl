﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveInvoiceDataTemplate.ascx.vb"
    Inherits="DesktopModules_InvoiceDataTemplate_SaveInvoiceDataTemplate" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<style type="text/css">
    .auto-style1 {
        width: 25%;
        height: 25px;
    }
</style>

<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to remove Record(s)?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;

        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }    
</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z_]*$" ErrorMessage="Invalid Name" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInvoiceDataTemplateName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    
</telerik:RadInputManager>
<telerik:RadInputManager ID="rimSheet" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior3" Validation-IsRequired="true"
        EmptyMessage="" ErrorMessage="Invalid Sheet Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtSheetName" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<telerik:RadInputManager ID="rimDelemiter" runat="server" Enabled="true">
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior3" Validation-IsRequired="true"
        EmptyMessage="" ErrorMessage="Invalid Delimiter Character">
        <TargetControls>
            <telerik:TargetInput ControlID="txtdelimiter" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<telerik:RadInputManager ID="rimHeaderAndDetail" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="regHeader" Validation-IsRequired="true" EmptyMessage=""
        ErrorMessage="Invalid Header Identifier">
        <TargetControls>
            <telerik:TargetInput ControlID="txtHeaderIdentifier" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reDetail" Validation-IsRequired="true" EmptyMessage=""
        ErrorMessage="Invalid Detail Identifier">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDetailIdentifier" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblInvoiceDataTemplateName" runat="server" Text="Name" CssClass="lbl"></asp:Label>
            <asp:Label ID="ReqInvoiceDataTemplateName" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblFormat" runat="server" Text="Format" CssClass="lbl">
            </asp:Label>
            <asp:Label ID="ReqFormat" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtInvoiceDataTemplateName" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RadioButtonList ID="RbFormat" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                <asp:ListItem Value=".txt">.txt</asp:ListItem>
                <asp:ListItem Value=".csv">.csv</asp:ListItem>
                <asp:ListItem Value=".xls">.xls</asp:ListItem>
                <asp:ListItem Value=".xlsx">.xlsx</asp:ListItem>
            </asp:RadioButtonList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvFileFormat" runat="server" ControlToValidate="RbFormat"
                ErrorMessage="Select File Format" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
            &nbsp;
            </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblSheetOrFileFormat" runat="server" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqSheetNameOrFixLength" runat="server" ForeColor="Red" Text="*" Visible="False"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <table id="tblXLS" runat="server" sttblDelimitAndDualSettingyle="display: none" width="100%">
                <tr align="left" style="width: 100%" valign="top">
                    <td align="left" style="width: 100%" valign="top">
                        <asp:TextBox ID="txtSheetName" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <table id="tblTXT" runat="server" style="display: none; width= 100%">
                <tr align="left" style="width: 100%" valign="top">
                    <td align="left" style="width: 100%" valign="top">
                        <asp:RadioButtonList ID="rbFieldFormate" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                            <asp:ListItem Value="0">Fixed Length</asp:ListItem>
                            <asp:ListItem Value="1">Delimit Character</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvFieldFormat" runat="server" ControlToValidate="rbFieldFormate" Display="Dynamic" Enabled="false" ErrorMessage="Select field format" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
           
            <table id="tblTemplateType" runat="server" style="display: none" width="100%">
                <%--style="display:none"--%>
                <tr>
                    <td align="left" valign="top" width="100%">
                        <asp:Label ID="lblTemplateType" runat="server" CssClass="lbl" Text="Template Type"></asp:Label>
                        <br />
                        <asp:RadioButtonList ID="rbTemplateType" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                            <asp:ListItem>Single</asp:ListItem>
                            <asp:ListItem>Dual</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvTemplateType" runat="server" ControlToValidate="rbTemplateType" Display="Dynamic" ErrorMessage="Select template type" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="auto-style1">
            <%--<table id="tblTemplateType" runat="server" style="display: none" width="100%">
                
                <tr>
                    <td align="left" valign="top" width="100%">
                        <asp:Label ID="lblTemplateType" runat="server" Text="Template Type" CssClass="lbl"></asp:Label><br />
                        <asp:RadioButtonList ID="rbTemplateType" runat="server" RepeatDirection="Horizontal"
                            AutoPostBack="True">
                            <asp:ListItem>Single</asp:ListItem>
                            <asp:ListItem>Dual</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvTemplateType" runat="server" ControlToValidate="rbTemplateType"
                            ErrorMessage="Select template type" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>--%>
        </td>
        <td align="left" valign="top" class="auto-style1">
            &nbsp;
        </td>
        <td align="left" valign="top" class="auto-style1">
            <table id="tblDelimitAndDualSetting" runat="server" width="100%" style="display: none">
                <tr>
                    <td align="left" valign="top" width="100%">
                        <asp:Panel ID="pnlDelimeter" runat="server" Width="100%">
                            <asp:Label ID="lblDelimitCharacter" runat="server" CssClass="lbl" Text="Delimit Character"></asp:Label>
                            <asp:Label ID="lblReqDelimitCharacter" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            <br />
                            <asp:TextBox ID="txtdelimiter" runat="server" CssClass="txtbox" MaxLength="1"></asp:TextBox>
                        </asp:Panel>
                        <br />
                        <asp:Panel ID="pnlHeaderIdentifier" runat="server" Width="100%">
                            <asp:Label ID="lblHeaderIdentifier" runat="server" CssClass="lbl" Text="Header Identifier"></asp:Label>
                            <asp:Label ID="lblReqHeaderIdentifier" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            <br />
                            <asp:TextBox ID="txtHeaderIdentifier" runat="server" CssClass="txtbox" MaxLength="2"></asp:TextBox>
                        </asp:Panel>
                        <br />
                        <asp:Panel ID="pnlDetailIdentifier" runat="server" Width="100%">
                            <asp:Label ID="lblDetailIdentifier" runat="server" CssClass="lbl" Text="Detail Identifier"></asp:Label>
                            <asp:Label ID="lblReqDetailIdentifier" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            <br />
                            <asp:TextBox ID="txtDetailIdentifier" runat="server" CssClass="txtbox" MaxLength="2"></asp:TextBox>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </td>
        <td align="left" valign="top" class="auto-style1">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;&nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="72px" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px"
                CausesValidation="False" />
            &nbsp;
        </td>
    </tr>
</table>
