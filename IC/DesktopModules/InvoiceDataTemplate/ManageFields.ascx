﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ManageFields.ascx.vb"
    Inherits="DesktopModules_InvoiceDataTemplateManagement_ManageFields" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }


    function ValidateChkList(source, arguments) {

        arguments.IsValid = IsCheckBoxChecked() ? true : false;



    }

    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function AdjustWidth(ddl) {
        var maxWidth = 0;
        var Counter = 0;
        for (var i = 0; i < ddl.length; i++) {
            if (ddl.options[i].text.length > maxWidth) {
                Counter = Counter + 1;
                maxWidth = ddl.options[i].text.length;
            }
        }
        if (Counter = 1) { ddl.style.width = "250px"; }

        else {
            alert(Counter);
            ddl.style.width = maxWidth + "px";
        }
        //-->
    }
</script>
<script language="javascript" type="text/javascript">
    function AdjustWidth(ddl) {
        var maxWidth = 0;
        var Counter = 0;
        for (var i = 0; i < ddl.length; i++) {
            if (ddl.options[i].text.length > maxWidth) {
                Counter = Counter + 1;
                maxWidth = ddl.options[i].text.length;
            }
        }
        if (Counter = 1) { ddl.style.width = "250px"; }

        else {
            alert(Counter);
            ddl.style.width = maxWidth + "px";
        }
        //-->
    }
</script>
<%--<style type="text/css">
    .ctrDropDown {
        width: 250px;
    }

    .ctrDropDownClick {
        width: 600px;
    }

    .plainDropDown {
        width: 250px;
    }

    .RadGrid_Default {
        font: 12px/16px "segoe ui",arial,sans-serif;
    }

    .RadGrid_Default {
        border: 1px solid #828282;
        background: #fff;
        color: #333;
    }

    .RadGrid_Default {
        font: 12px/16px "segoe ui",arial,sans-serif;
    }

    .RadGrid_Default {
        border: 1px solid #828282;
        background: #fff;
        color: #333;
    }

        .RadGrid_Default .rgHeaderDiv {
            background: #eee 0 -7550px repeat-x url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
        }

        .RadGrid_Default .rgHeaderDiv {
            background: #eee 0 -7550px repeat-x url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
        }

        .RadGrid_Default .rgMasterTable {
            font: 12px/16px "segoe ui",arial,sans-serif;
        }

    .RadGrid .rgMasterTable {
        border-collapse: separate;
        border-spacing: 0;
    }

    .RadGrid_Default .rgMasterTable {
        font: 12px/16px "segoe ui",arial,sans-serif;
    }

    .RadGrid .rgMasterTable {
        border-collapse: separate;
        border-spacing: 0;
    }

    .RadGrid .rgClipCells .rgHeader {
        overflow: hidden;
    }

    .RadGrid .rgClipCells .rgHeader {
        overflow: hidden;
    }

    .RadGrid_Default .rgHeader {
        color: #333;
    }

    .RadGrid_Default .rgHeader {
        border: 0;
        border-bottom: 1px solid #828282;
        background: #eaeaea 0 -2300px repeat-x url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
    }

    .RadGrid .rgHeader {
        padding-top: 5px;
        padding-bottom: 4px;
        text-align: left;
        font-weight: normal;
    }

    .RadGrid .rgHeader {
        padding-left: 7px;
        padding-right: 7px;
    }

    .RadGrid .rgHeader {
        cursor: default;
    }

    .RadGrid_Default .rgHeader {
        color: #333;
    }

    .RadGrid_Default .rgHeader {
        border: 0;
        border-bottom: 1px solid #828282;
        background: #eaeaea 0 -2300px repeat-x url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
    }

    .RadGrid .rgHeader {
        padding-top: 5px;
        padding-bottom: 4px;
        text-align: left;
        font-weight: normal;
    }

    .RadGrid .rgHeader {
        padding-left: 7px;
        padding-right: 7px;
    }

    .RadGrid .rgHeader {
        cursor: default;
    }

    .RadGrid_Default .rgHeader a {
        color: #333;
    }

    .RadGrid .rgHeader a {
        text-decoration: none;
    }

    .RadGrid_Default .rgHeader a {
        color: #333;
    }

    .RadGrid .rgHeader a {
        text-decoration: none;
    }

    .RadGrid_Default .rgFooterDiv {
        background: #eee;
    }

    .RadGrid_Default .rgFooterDiv {
        background: #eee;
    }

    .RadGrid_Default .rgFooter {
        background: #eee;
    }

    .RadGrid_Default .rgFooter {
        background: #eee;
    }

    .RadGrid_Default .rgPager {
        background: #eee;
    }

    .RadGrid .rgPager {
        cursor: default;
    }

    .RadGrid_Default .rgPager {
        background: #eee;
    }

    .RadGrid .rgPager {
        cursor: default;
    }

    .RadGrid .rgArrPart1 {
        padding-right: 0;
    }

    .RadGrid .rgWrap {
        float: left;
        padding: 0 10px;
        line-height: 22px;
        white-space: nowrap;
    }

    .RadGrid .rgArrPart1 {
        padding-right: 0;
    }

    .RadGrid .rgWrap {
        float: left;
        padding: 0 10px;
        line-height: 22px;
        white-space: nowrap;
    }

    .RadGrid_Default .rgPageFirst {
        background-position: 0 -550px;
    }

    .RadGrid_Default .rgPageFirst {
        background-image: url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
    }

    .RadGrid .rgPageFirst {
        margin-right: 1px;
    }

    .RadGrid .rgPageFirst {
        width: 22px;
        height: 22px;
        vertical-align: top;
    }

    .RadGrid .rgPageFirst {
        width: 16px;
        height: 16px;
        border: 0;
        margin: 0;
        padding: 0;
        background-color: transparent;
        background-repeat: no-repeat;
        vertical-align: middle;
        font-size: 1px;
        cursor: pointer;
    }

    .RadGrid_Default .rgPageFirst {
        background-position: 0 -550px;
    }

    .RadGrid_Default .rgPageFirst {
        background-image: url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
    }

    .RadGrid .rgPageFirst {
        margin-right: 1px;
    }

    .RadGrid .rgPageFirst {
        width: 22px;
        height: 22px;
        vertical-align: top;
    }

    .RadGrid .rgPageFirst {
        width: 16px;
        height: 16px;
        border: 0;
        margin: 0;
        padding: 0;
        background-color: transparent;
        background-repeat: no-repeat;
        vertical-align: middle;
        font-size: 1px;
        cursor: pointer;
    }

    .RadGrid_Default .rgPagePrev {
        background-position: 0 -700px;
    }

    .RadGrid_Default .rgPagePrev {
        background-image: url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
    }

    .RadGrid .rgPagePrev {
        margin-right: 1px;
    }

    .RadGrid .rgPagePrev {
        width: 22px;
        height: 22px;
        vertical-align: top;
    }

    .RadGrid .rgPagePrev {
        width: 16px;
        height: 16px;
        border: 0;
        margin: 0;
        padding: 0;
        background-color: transparent;
        background-repeat: no-repeat;
        vertical-align: middle;
        font-size: 1px;
        cursor: pointer;
    }

    .RadGrid_Default .rgPagePrev {
        background-position: 0 -700px;
    }

    .RadGrid_Default .rgPagePrev {
        background-image: url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
    }

    .RadGrid .rgPagePrev {
        margin-right: 1px;
    }

    .RadGrid .rgPagePrev {
        width: 22px;
        height: 22px;
        vertical-align: top;
    }

    .RadGrid .rgPagePrev {
        width: 16px;
        height: 16px;
        border: 0;
        margin: 0;
        padding: 0;
        background-color: transparent;
        background-repeat: no-repeat;
        vertical-align: middle;
        font-size: 1px;
        cursor: pointer;
    }

    .RadGrid .rgNumPart {
        padding: 0;
    }

    .RadGrid .rgNumPart {
        padding: 0;
    }

    .RadGrid_Default .rgNumPart a.rgCurrentPage {
        background-position: 100% -1450px;
    }

    .RadGrid_Default .rgNumPart a.rgCurrentPage {
        background: no-repeat url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
    }

    .RadGrid .rgNumPart a.rgCurrentPage {
        cursor: default;
    }

    .RadGrid_Default .rgNumPart a.rgCurrentPage {
        background-position: 100% -1450px;
    }

    .RadGrid_Default .rgNumPart a.rgCurrentPage {
        background: no-repeat url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
    }

    .RadGrid .rgNumPart a.rgCurrentPage {
        cursor: default;
    }

    .RadGrid_Default .rgNumPart a {
        color: #000;
    }

    .RadGrid .rgNumPart a {
        float: left;
        line-height: 22px;
        margin: 0;
        padding: 0 5px 0 0;
        text-decoration: none;
    }

    .RadGrid_Default .rgNumPart a {
        color: #000;
    }

    .RadGrid .rgNumPart a {
        float: left;
        line-height: 22px;
        margin: 0;
        padding: 0 5px 0 0;
        text-decoration: none;
    }

    .RadGrid .rgArrPart2 {
        padding-left: 0;
    }

    .RadGrid .rgArrPart2 {
        padding-left: 0;
    }

    .RadGrid_Default .rgPageNext {
        background-position: 0 -850px;
    }

    .RadGrid_Default .rgPageNext {
        background-image: url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
    }

    .RadGrid .rgPageNext {
        margin-left: 1px;
    }

    .RadGrid .rgPageNext {
        width: 22px;
        height: 22px;
        vertical-align: top;
    }

    .RadGrid .rgPageNext {
        width: 16px;
        height: 16px;
        border: 0;
        margin: 0;
        padding: 0;
        background-color: transparent;
        background-repeat: no-repeat;
        vertical-align: middle;
        font-size: 1px;
        cursor: pointer;
    }

    .RadGrid_Default .rgPageNext {
        background-position: 0 -850px;
    }

    .RadGrid_Default .rgPageNext {
        background-image: url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
    }

    .RadGrid .rgPageNext {
        margin-left: 1px;
    }

    .RadGrid .rgPageNext {
        width: 22px;
        height: 22px;
        vertical-align: top;
    }

    .RadGrid .rgPageNext {
        width: 16px;
        height: 16px;
        border: 0;
        margin: 0;
        padding: 0;
        background-color: transparent;
        background-repeat: no-repeat;
        vertical-align: middle;
        font-size: 1px;
        cursor: pointer;
    }

    .RadGrid_Default .rgPageLast {
        background-position: 0 -1000px;
    }

    .RadGrid_Default .rgPageLast {
        background-image: url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
    }

    .RadGrid .rgPageLast {
        margin-left: 1px;
    }

    .RadGrid .rgPageLast {
        width: 22px;
        height: 22px;
        vertical-align: top;
    }

    .RadGrid .rgPageLast {
        width: 16px;
        height: 16px;
        border: 0;
        margin: 0;
        padding: 0;
        background-color: transparent;
        background-repeat: no-repeat;
        vertical-align: middle;
        font-size: 1px;
        cursor: pointer;
    }

    .RadGrid_Default .rgPageLast {
        background-position: 0 -1000px;
    }

    .RadGrid_Default .rgPageLast {
        background-image: url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
    }

    .RadGrid .rgPageLast {
        margin-left: 1px;
    }

    .RadGrid .rgPageLast {
        width: 22px;
        height: 22px;
        vertical-align: top;
    }

    .RadGrid .rgPageLast {
        width: 16px;
        height: 16px;
        border: 0;
        margin: 0;
        padding: 0;
        background-color: transparent;
        background-repeat: no-repeat;
        vertical-align: middle;
        font-size: 1px;
        cursor: pointer;
    }

    .RadGrid .rgPagerLabel {
        margin: 0 4px 0 0;
        vertical-align: top;
    }

    .RadGrid .rgPagerLabel {
        margin: 0 4px 0 0;
        vertical-align: top;
    }

    .RadGrid .rgPager .RadComboBox {
        margin: 0 4px 0 0;
        vertical-align: top;
    }

    .RadGrid .rgPager .RadComboBox {
        margin: 0 4px 0 0;
        vertical-align: top;
    }

    .RadComboBox_Default {
        font: 12px "Segoe UI",Arial,sans-serif;
        color: #333;
    }

    .RadComboBox {
        vertical-align: middle;
        display: -moz-inline-stack;
        display: inline-block;
    }

    .RadComboBox {
        text-align: left;
    }

    .RadComboBox_Default {
        font: 12px "Segoe UI",Arial,sans-serif;
        color: #333;
    }

    .RadComboBox {
        vertical-align: middle;
        display: -moz-inline-stack;
        display: inline-block;
    }

    .RadComboBox {
        text-align: left;
    }

        .RadComboBox * {
            margin: 0;
            padding: 0;
        }

        .RadComboBox * {
            margin: 0;
            padding: 0;
        }

    .RadComboBox_Default .rcbInputCellLeft {
        background-image: url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.ComboBox.rcbSprite.png');
    }

    .RadComboBox .rcbInputCellLeft {
        background-color: transparent;
        background-repeat: no-repeat;
    }

    .RadComboBox_Default .rcbInputCellLeft {
        background-image: url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.ComboBox.rcbSprite.png');
    }

    .RadComboBox .rcbInputCellLeft {
        background-color: transparent;
        background-repeat: no-repeat;
    }

    .RadComboBox .rcbReadOnly .rcbInput {
        cursor: default;
    }

    .RadComboBox .rcbReadOnly .rcbInput {
        cursor: default;
    }

    .RadComboBox_Default .rcbInput {
        font: 12px "Segoe UI",Arial,sans-serif;
        color: #333;
    }

    .RadComboBox .rcbInput {
        text-align: left;
    }

    .RadComboBox_Default .rcbInput {
        font: 12px "Segoe UI",Arial,sans-serif;
        color: #333;
    }

    .RadComboBox .rcbInput {
        text-align: left;
    }

    .RadComboBox_Default .rcbArrowCellRight {
        background-image: url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.ComboBox.rcbSprite.png');
    }

    .RadComboBox .rcbArrowCellRight {
        background-color: transparent;
        background-repeat: no-repeat;
    }

    .RadComboBox_Default .rcbArrowCellRight {
        background-image: url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.ComboBox.rcbSprite.png');
    }

    .RadComboBox .rcbArrowCellRight {
        background-color: transparent;
        background-repeat: no-repeat;
    }

    .RadGrid_Default .rgInfoPart {
        color: #8a8a8a;
    }

    .RadGrid .rgInfoPart {
        float: right;
    }

    .RadGrid_Default .rgInfoPart {
        color: #8a8a8a;
    }

    .RadGrid .rgInfoPart {
        float: right;
    }
</style>--%>
<telerik:RadInputManager ID="radFieldName" runat="server">
    <telerik:RegExpTextBoxSetting BehaviorID="reName" Validation-IsRequired="true" ErrorMessage="Invalid Field Name" Validation-ValidationGroup="Save"
        EmptyMessage="" ValidationExpression="^[a-zA-Z0-9]+$">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFieldName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>

</telerik:RadInputManager>
<telerik:RadInputManager ID="radFieldLenght" runat="server" Enabled="true">
    <telerik:RegExpTextBoxSetting BehaviorID="reFiedLength" Validation-IsRequired="true" ValidationExpression="^[0-9]{1,3}$" Validation-ValidationGroup="Save" EmptyMessage="" ErrorMessage="Invalid Field Length">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFieldLength" />

        </TargetControls>

    </telerik:RegExpTextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue">Manage Invoice DB Template Fields</asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr>
        <td align="left" style="width: 100%">
            <table id="tblAddFields" runat="server" style="width: 100%">


                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblFieldName" runat="server" Text="Field Name" CssClass="lbl"></asp:Label>
                        <asp:Label ID="Label8" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 28%">
                        <asp:Label ID="lblFieldDBType" runat="server" Text="Select Field DB Type" CssClass="lbl"></asp:Label>
                        <asp:Label ID="lblReqFieldDBType" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 22%">&nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtFieldName" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlFieldType" runat="server" CssClass="dropdown" AutoPostBack="True">
                            <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                            <asp:ListItem>Integer</asp:ListItem>
                            <asp:ListItem>Decimal</asp:ListItem>
                            <asp:ListItem>String</asp:ListItem>
                            <asp:ListItem>Date</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 33%">
                        <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" ControlToValidate="ddlFieldType"
                            Display="Dynamic" ErrorMessage="Please select Field DB Type" InitialValue="0" SetFocusOnError="True" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" valign="top" style="width: 17%">&nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblFieldLength" runat="server" Text="Field Length" CssClass="lbl"></asp:Label>
                        <asp:Label ID="Label9" runat="server" Text="*" ForeColor="Red"></asp:Label>

                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblIsReference" runat="server" Text="Is Reference Field" CssClass="lbl"></asp:Label>
                        <br />
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtFieldLength" runat="server" CssClass="txtbox" MaxLength="3"></asp:TextBox>

                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:CheckBox ID="chkReference" runat="server" Text=" " CssClass="chkBox" />
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:RangeValidator ID="rvLength" runat="server" ControlToValidate="txtFieldLength" Display="Dynamic" SetFocusOnError="True" ValidationGroup="Save"></asp:RangeValidator>
                    </td>

                    <td align="left" valign="top" style="width: 25%"></td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <table id="tblFieldType" runat="server" style="width: 100%;">
                            <tr>
                                <td align="left" style="width: 100%;">
                                    <asp:Label ID="lblFieldType" runat="server" Text="Field Type" CssClass="lbl"></asp:Label>
                                    <asp:Label ID="lblReqFieldType" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                </td>


                            </tr>
                            <tr>
                                <td align="left" style="width: 100%;">
                                    <asp:RadioButtonList ID="rblFieldType" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem>NonFlexi</asp:ListItem>
                                        <asp:ListItem>Detail</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>


                            </tr>
                            <tr>
                                <td align="left" style="width: 100%;">
                                    <asp:RequiredFieldValidator ID="rfvFieldType" runat="server" ControlToValidate="rblFieldType" Display="Dynamic" ErrorMessage="Please Select Field Type" InitialValue="" SetFocusOnError="True" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                </td>


                            </tr>
                        </table>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" colspan="4">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px" ValidationGroup="Save" />
                        <asp:Button ID="btnPublish" runat="server" Text="Publish" CssClass="btn" Width="71px" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" CausesValidation="False"
                            Width="75px" />
                    </td>
                </tr>
            </table>


        </td>

    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" style="width: 25%">
            <asp:Button ID="btnCancel2" runat="server" Text="Cancel" CssClass="btnCancel" CausesValidation="False"
                Width="75px" />
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">

            <telerik:RadGrid ID="gvInvoiceDBTemplate" runat="server" AllowPaging="True" 
                            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                            PageSize="10">
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView NoMasterRecordsText="" DataKeyNames="CollectionFieldID,IsReference,FieldDBType" >
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="FieldName" HeaderText="Field Name" SortExpression="FieldName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FieldOrder" HeaderText="Field Order" SortExpression="FieldOrder">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FieldDBType" HeaderText="Field DB Type" SortExpression="FieldDBType">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FieldType" HeaderText="Field Type" SortExpression="FieldType">
                                    </telerik:GridBoundColumn>
                                    
                                    <telerik:GridBoundColumn DataField="FieldLength" HeaderText="Length" SortExpression="FieldLength">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="IsReference" DataField="IsReference">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Label ID="Delete" runat="server" Text="Delete"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("CollectionFieldID") %>'
                                                CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">&nbsp;&nbsp;&nbsp;
        </td>
    </tr>
</table>
