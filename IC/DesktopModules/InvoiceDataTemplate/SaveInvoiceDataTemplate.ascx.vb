﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_InvoiceDataTemplate_SaveInvoiceDataTemplate
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private FileUploadTemplateCode As String
    Private htRights As Hashtable

    Protected Sub DesktopModules_FileUploadTemplate_SaveFileUploadTemplate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            FileUploadTemplateCode = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                'LoadTemplateFieldsList()
                
                If FileUploadTemplateCode.ToString() = "0" Then
                    Clear()
                    lblPageHeader.Text = "Add Invoice Data Template"
                    btnSave.Text = "Save"
                 
                    pnlHeaderIdentifier.Style.Add("display", "none")
                    'pnlDelimeter.Style.Add("display", "none")
                    'pnlDetail.Style.Add("display", "none")
                    rimDelemiter.Enabled = False
                    rimHeaderAndDetail.Enabled = False
                    rimSheet.Enabled = False
                    btnSave.Visible = False

                Else
                    If ICCollectionInvoiceDataTemplateFieldsListController.CheckInvoiceStatusAgainstTemplate(FileUploadTemplateCode) = True Then
                        UIUtilities.ShowDialog(Me, "Error", "Invoice Data Template can not updated due to following reason: <br /> 1. Files exists in pending read status", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    lblPageHeader.Text = "Edit Invoice Data Template"
                    btnSave.Text = "Update"
                    Clear()
                    Dim objICInvoiceDataTemp As New ICCollectionInvoiceDataTemplate

                    If objICInvoiceDataTemp.LoadByPrimaryKey(FileUploadTemplateCode) Then

                        txtInvoiceDataTemplateName.Text = objICInvoiceDataTemp.TemplateName.ToString()
                        '  txtInvoiceDataTemplateCode.Text = objICInvoiceDataTemp.FileUploadTemplateCode.ToString
                        If objICInvoiceDataTemp.TemplateFormat.Equals(".txt") Then
                            RbFormat.SelectedIndex = 0
                        End If
                        If objICInvoiceDataTemp.TemplateFormat.Equals(".xls") Then
                            RbFormat.SelectedIndex = 2
                        End If
                        If objICInvoiceDataTemp.TemplateFormat.Equals(".xlsx") Then
                            RbFormat.SelectedIndex = 3
                        End If
                        If objICInvoiceDataTemp.TemplateFormat.Equals(".csv") Then
                            RbFormat.SelectedIndex = 1
                        End If
                        SetFileFormat()
                        SetFieldFormat()
                        If objICInvoiceDataTemp.TemplateFormat.Equals(".xls") Then
                            txtSheetName.Text = objICInvoiceDataTemp.SheetName.ToString()
                        End If
                        If objICInvoiceDataTemp.TemplateFormat.Equals(".xlsx") Then
                            txtSheetName.Text = objICInvoiceDataTemp.SheetName.ToString()
                        End If
                        If objICInvoiceDataTemp.TemplateFormat.Equals(".csv") Then
                            txtSheetName.Text = objICInvoiceDataTemp.SheetName.ToString()
                        End If

                        If objICInvoiceDataTemp.TemplateFormat.Equals(".txt") Then
                            If Not objICInvoiceDataTemp.IsFixLength Is Nothing Then
                                If objICInvoiceDataTemp.IsFixLength = True Then
                                    rbFieldFormate.SelectedValue = 0
                                    SetFieldFormat()


                                Else
                                    '     rbFieldFormate.SelectedValue = 0
                                    If objICInvoiceDataTemp.IsFixLength = False Then
                                        rbFieldFormate.SelectedValue = 1
                                    End If
                                    SetFieldFormat()
                                    If objICInvoiceDataTemp.TemplateType = "Single" Then
                                        rbTemplateType.SelectedValue = "Single"
                                        SetDelimitAndHeaderAndDetail()
                                        txtdelimiter.Text = objICInvoiceDataTemp.FieldDelimiter


                                    ElseIf objICInvoiceDataTemp.TemplateType = "Dual" Then
                                        rbTemplateType.SelectedValue = "Dual"
                                        SetDelimitAndHeaderAndDetail()

                                        txtdelimiter.Text = objICInvoiceDataTemp.FieldDelimiter
                                        txtHeaderIdentifier.Text = objICInvoiceDataTemp.HeaderIdentifier
                                        txtDetailIdentifier.Text = objICInvoiceDataTemp.DetailIdentifier

                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Function CheckHeaderDetailAndDelimiterIDentifier() As Boolean
        Dim Result As Boolean = False
        If (txtdelimiter.Text = txtHeaderIdentifier.Text) Or (txtdelimiter.Text = txtDetailIdentifier.Text) Then
            Result = True
            Return Result
            Exit Function
        ElseIf (txtHeaderIdentifier.Text = txtdelimiter.Text) Or (txtHeaderIdentifier.Text = txtDetailIdentifier.Text) Then
            Result = True
            Return Result
            Exit Function
        ElseIf (txtDetailIdentifier.Text = txtdelimiter.Text) Or (txtDetailIdentifier.Text = txtHeaderIdentifier.Text) Then
            Result = True
            Return Result
            Exit Function
        End If
        Return Result
    End Function
    
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Invoice Data Template")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    
    
    
    
    Private Sub Clear()
        txtInvoiceDataTemplateName.Text = ""
        txtSheetName.Text = ""
        rbFieldFormate.ClearSelection()
        RbFormat.ClearSelection()
        tblXLS.Style.Add("display", "none")
        tblTXT.Style.Add("display", "none")
        lblSheetOrFileFormat.Style.Add("display", "none")
        txtSheetName.Text = ""
        txtdelimiter.Text = ""
        lblReqSheetNameOrFixLength.Visible = False
        rimDelemiter.Enabled = False
        rimSheet.Enabled = False
        rfvFieldFormat.Enabled = False
        txtHeaderIdentifier.Text = ""
        txtDetailIdentifier.Text = ""

        tblDelimitAndDualSetting.Style.Add("display", "none")
        tblTemplateType.Style.Add("display", "none")
        btnSave.Visible = False
    End Sub
    

    

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If rbTemplateType.SelectedValue.ToString = "Dual" Then
                If CheckHeaderDetailAndDelimiterIDentifier() = True Then
                    UIUtilities.ShowDialog(Me, "Error", "Duplicat header, detail and delimiter character is not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            End If

            Dim objICInvoiceTemplate As New ICCollectionInvoiceDataTemplate



          
           

            objICInvoiceTemplate.TemplateName = txtInvoiceDataTemplateName.Text.ToString()
            objICInvoiceTemplate.FileUploadTemplateCode = Nothing
            objICInvoiceTemplate.TemplateFormat = RbFormat.Text.ToString()
            'objICInvoiceTemplate.IsPublished = False

            If RbFormat.Text.ToString() = ".txt" Then
                If rbFieldFormate.SelectedIndex = 0 Then
                    objICInvoiceTemplate.IsFixLength = True
                Else
                    objICInvoiceTemplate.IsFixLength = False
                End If
                If rbFieldFormate.SelectedIndex = 1 Then
                    objICInvoiceTemplate.FieldDelimiter = txtdelimiter.Text
                End If
            End If
            If rbTemplateType.SelectedValue.ToString = "Single" Then
                objICInvoiceTemplate.TemplateType = "Single"
            ElseIf rbTemplateType.SelectedValue.ToString = "Dual" Then
                objICInvoiceTemplate.TemplateType = "Dual"
                objICInvoiceTemplate.HeaderIdentifier = txtHeaderIdentifier.Text.ToString
                objICInvoiceTemplate.DetailIdentifier = txtDetailIdentifier.Text.ToString
            End If

            If RbFormat.Text.ToString() = ".xls" Then
                objICInvoiceTemplate.SheetName = txtSheetName.Text.ToString()
            End If
            If RbFormat.Text.ToString() = ".xlsx" Then
                objICInvoiceTemplate.SheetName = txtSheetName.Text.ToString()
            End If
            If RbFormat.Text.ToString() = ".csv" Then
                objICInvoiceTemplate.SheetName = txtSheetName.Text.ToString()
            End If


            If FileUploadTemplateCode = "0" Then
                objICInvoiceTemplate.CreateBy = Me.UserId
                objICInvoiceTemplate.CreateDate = Date.Now
               
                If CheckDuplicateTemplateNameAndCode(objICInvoiceTemplate, False) = False Then
                    ICCollectionInvoiceDataTemplateController.AddInvoiceDataTemplate(objICInvoiceTemplate, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                  
                    UIUtilities.ShowDialog(Me, "Invoice Data Template", "Invoice Data Template saved successfully.", ICBO.IC.Dialogmessagetype.Success)
                Else
                    UIUtilities.ShowDialog(Me, "Error", "Duplicate invoice data template is not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Else
               
                objICInvoiceTemplate.CollectionInvoiceTemplateID = FileUploadTemplateCode
                If CheckDuplicateTemplateNameAndCode(objICInvoiceTemplate, True) = False Then
                    ICCollectionInvoiceDataTemplateController.AddInvoiceDataTemplate(objICInvoiceTemplate, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)

                    UIUtilities.ShowDialog(Me, "Invoice Data Template", "Invoice data template updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Error", "Duplicate invoice data template is not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            End If
            Clear()
         

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    

    

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    Protected Sub rbFieldFormate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbFieldFormate.SelectedIndexChanged
        Try
            SetFieldFormat()
            'ApplyJavaScript()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub SetFieldFormat()
        txtdelimiter.Text = ""
        txtHeaderIdentifier.Text = ""
        txtDetailIdentifier.Text = ""
        rbTemplateType.ClearSelection()

        rimDelemiter.Enabled = False
        If rbFieldFormate.SelectedValue.ToString() = "0" Then
            tblTemplateType.Style.Add("display", "none")
            tblDelimitAndDualSetting.Style.Add("display", "none")
            rfvTemplateType.Enabled = False
            rimHeaderAndDetail.Enabled = False
           
            btnSave.Visible = True
           
        End If
        If rbFieldFormate.SelectedValue.ToString() = "1" Then
            tblTemplateType.Style.Remove("display")
            rfvTemplateType.Enabled = True
            tblDelimitAndDualSetting.Style.Add("display", "none")
            rimHeaderAndDetail.Enabled = False
           
            btnSave.Visible = False
          
        End If
    End Sub
    Protected Sub RbFormat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbFormat.SelectedIndexChanged
        Try
            SetFileFormat()
            'ApplyJavaScript()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetFileFormat()
        Try
            tblXLS.Style.Add("display", "none")
            tblTXT.Style.Add("display", "none")
            lblSheetOrFileFormat.Style.Remove("display")
            rbFieldFormate.ClearSelection()
            rbTemplateType.ClearSelection()
            pnlDelimeter.Style.Add("display", "none")
            txtSheetName.Text = ""
            txtdelimiter.Text = ""
            txtHeaderIdentifier.Text = ""
            txtDetailIdentifier.Text = ""
            rimSheet.Enabled = False
            rfvFieldFormat.Enabled = False
            If RbFormat.Text.ToString() = ".xls" Then
                lblSheetOrFileFormat.Text = "Sheet Name"
                lblReqSheetNameOrFixLength.Visible = True
                tblXLS.Style.Remove("display")
                rimSheet.Enabled = True
                tblTemplateType.Style.Add("display", "none")
                tblDelimitAndDualSetting.Style.Add("display", "none")
                rfvTemplateType.Enabled = False
                rimHeaderAndDetail.Enabled = False
                rbTemplateType.ClearSelection()
                txtdelimiter.Text = ""
                txtDetailIdentifier.Text = ""
                txtHeaderIdentifier.Text = ""
                rimDelemiter.Enabled = False
                btnSave.Visible = True
            End If
            If RbFormat.Text.ToString() = ".xlsx" Then
                lblSheetOrFileFormat.Text = "Sheet Name"
                lblReqSheetNameOrFixLength.Visible = True
                tblXLS.Style.Remove("display")
                rimSheet.Enabled = True
                tblTemplateType.Style.Add("display", "none")
                tblDelimitAndDualSetting.Style.Add("display", "none")
                rbTemplateType.ClearSelection()
                txtdelimiter.Text = ""
                txtDetailIdentifier.Text = ""
                txtHeaderIdentifier.Text = ""
                rimDelemiter.Enabled = False
                rfvTemplateType.Enabled = False
                rimHeaderAndDetail.Enabled = False
              
                'pnlFieldsList.Visible = True
                'pnlDetail.Visible = False

                btnSave.Visible = True
            End If
            If RbFormat.Text.ToString() = ".txt" Then
                lblSheetOrFileFormat.Text = "Field Format"
                tblTXT.Style.Remove("display")
                lblReqSheetNameOrFixLength.Visible = True
                rfvFieldFormat.Enabled = True
                rfvTemplateType.Enabled = False
                rimHeaderAndDetail.Enabled = False
                rimDelemiter.Enabled = False
                
                btnSave.Visible = False
            End If
            If RbFormat.Text.ToString() = ".csv" Then

                lblSheetOrFileFormat.Text = "Sheet Name"
                lblReqSheetNameOrFixLength.Visible = True
                tblXLS.Style.Remove("display")
                rimSheet.Enabled = True
                tblTemplateType.Style.Add("display", "none")
                tblDelimitAndDualSetting.Style.Add("display", "none")
                rbTemplateType.ClearSelection()
                txtdelimiter.Text = ""
                txtDetailIdentifier.Text = ""
                txtHeaderIdentifier.Text = ""
                rimDelemiter.Enabled = False
                rfvTemplateType.Enabled = False
                rimHeaderAndDetail.Enabled = False
              
                btnSave.Visible = True
                'pnlFieldsList.Visible = True
               
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub rbTemplateType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbTemplateType.SelectedIndexChanged
        Try

            SetDelimitAndHeaderAndDetail()
            'ApplyJavaScript()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetDelimitAndHeaderAndDetail()
        If rbTemplateType.SelectedValue = "Single" Then
            tblDelimitAndDualSetting.Style.Remove("display")
            pnlDelimeter.Style.Remove("display")

            pnlHeaderIdentifier.Style.Add("display", "none")
            pnlDetailIdentifier.Style.Add("display", "none")

            rimDelemiter.Enabled = True
            rimHeaderAndDetail.Enabled = False
          
            btnSave.Visible = True
        ElseIf rbTemplateType.SelectedValue = "Dual" Then
            tblDelimitAndDualSetting.Style.Remove("display")
            pnlDelimeter.Style.Remove("display")
            pnlHeaderIdentifier.Style.Remove("display")
            pnlDetailIdentifier.Style.Remove("display")
            rimHeaderAndDetail.Enabled = True
            rimDelemiter.Enabled = True
           
            btnSave.Visible = True
        End If
    End Sub
    Private Function CheckDuplicateTemplateNameAndCode(ByVal objICInvoiceTemplate As ICCollectionInvoiceDataTemplate, ByVal IsUpdate As Boolean) As Boolean

        Dim collICInvoiceTemplateField As New ICCollectionInvoiceDataTemplateCollection
        Dim objICInvoiceTemplateForUpdate As New ICCollectionInvoiceDataTemplate
        Dim Result As Boolean = False


        collICInvoiceTemplateField.es.Connection.CommandTimeout = 3600
        objICInvoiceTemplateForUpdate.es.Connection.CommandTimeout = 3600



        If IsUpdate = False Then
            ' collICInvoiceTemplateField.Query.Where(collICInvoiceTemplateField.Query.TemplateName = objICInvoiceTemplate.TemplateName Or collICInvoiceTemplateField.Query.FileUploadTemplateCode = objICInvoiceTemplate.FileUploadTemplateCode)
            collICInvoiceTemplateField.Query.Where(collICInvoiceTemplateField.Query.TemplateName = objICInvoiceTemplate.TemplateName)
            collICInvoiceTemplateField.Query.Load()
            If collICInvoiceTemplateField.Query.Load Then
                Result = True
            End If
        ElseIf IsUpdate = True Then
            objICInvoiceTemplateForUpdate.LoadByPrimaryKey(FileUploadTemplateCode)
            If Not objICInvoiceTemplateForUpdate.TemplateName = objICInvoiceTemplate.TemplateName Then
                collICInvoiceTemplateField.Query.Where(collICInvoiceTemplateField.Query.TemplateName = objICInvoiceTemplate.TemplateName)
                collICInvoiceTemplateField.Query.Load()
                If collICInvoiceTemplateField.Query.Load Then
                    Result = True
                End If
            End If
            'If Not objICInvoiceTemplateForUpdate.FileUploadTemplateCode = objICInvoiceTemplate.FileUploadTemplateCode Then
            '    collICInvoiceTemplateField.Query.Where(collICInvoiceTemplateField.Query.FileUploadTemplateCode = objICInvoiceTemplate.FileUploadTemplateCode)
            '    collICInvoiceTemplateField.Query.Load()
            '    If collICInvoiceTemplateField.Query.Load Then
            '        Result = True
            '    End If
            'End If
        End If



        Return Result
    End Function


    
    Private Sub MoveUp(ByVal FieldID As String)

        ' CheckOrders(pcatid)

        Dim cat As New ICCollectionInvoiceDataTemplateFields
        cat.es.Connection.CommandTimeout = 3600
        cat.LoadByPrimaryKey(FieldID, FileUploadTemplateCode)

        If Not cat.FieldOrder = 1 Then

            'Dim cat2 As New ICTemplateFields
            'cat2.es.Connection.CommandTimeout = 3600
            'cat2.Query.Where(cat2.Query.FieldOrder = cat.FieldOrder - 1 And cat2.Query.TemplateID = cat.TemplateID)

            'If cat2.Query.Load Then
            '    cat2.FieldOrder += 1
            '    cat2.Save()

            'End If

            cat.FieldOrder = cat.FieldOrder - 1

            cat.Save()

            'If rbTemplateType.SelectedValue = "Dual" Then
            '    LoadAssignedFieldsListForTemplate(Me.gvAssignedTemplateFieldForSingleTemplate.CurrentPageIndex + 1, Me.gvAssignedTemplateFieldForSingleTemplate.PageSize, True, "Detail", "FixFormat")
            '    LoadDetailFieldListForDualTemplateType(Me.gvDetailFields.CurrentPageIndex + 1, Me.gvDetailFields.PageSize, True)
            '    CheckSelectCheckBoxOnUpDateTemplate("Detail")
            'Else
            '    LoadAssignedFieldsListForTemplate(Me.gvAssignedTemplateFieldForSingleTemplate.CurrentPageIndex + 1, Me.gvAssignedTemplateFieldForSingleTemplate.PageSize, True, "TemplateHeader", "NonFlexi")
            '    LoadFieldsList(Me.gvFieldsList.CurrentPageIndex + 1, Me.gvFieldsList.PageSize, True)
            '    CheckSelectCheckBoxOnUpDateTemplate("Header")
            'End If
            '   Me.FillTreeview(Me.TreeView1)

        End If


    End Sub

    Private Sub MoveDown(ByVal templateid As String, ByVal FieldID As String)


        Dim fieldcoll As New ICCollectionInvoiceDataTemplateFieldsCollection
        fieldcoll.es.Connection.CommandTimeout = 3600
        fieldcoll.Query.Where(fieldcoll.Query.CollectionInvoiceTemplateID = templateid)

        fieldcoll.Query.Load()

        Dim cat As New ICCollectionInvoiceDataTemplateFields
        cat.es.Connection.CommandTimeout = 3600
        cat.LoadByPrimaryKey(FieldID, templateid)




        If Not cat.FieldOrder = fieldcoll.Count Then

            Dim cat2 As New ICCollectionInvoiceDataTemplateFields
            cat2.es.Connection.CommandTimeout = 3600

            cat2.Query.Where(cat2.Query.FieldOrder = cat.FieldOrder + 1 And cat.Query.CollectionInvoiceTemplateID = templateid)


            If cat2.Query.Load Then


                cat2.FieldOrder -= 1


                cat2.Save()

            End If

            cat.FieldOrder = cat.FieldOrder + 1


            cat.Save()

            

        End If

    End Sub

    


    

    'Protected Sub btnCancel0_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel0.Click
    '    Try
    '        Dim objICInvoiceDataTemplateManagement As New ICCollectionInvoiceDataTemplate
    '        Dim str As String = Nothing
    '        Dim ActionStr As String = Nothing
    '        Dim CollectionInvoiceDataTemplateID As Integer = Nothing
    '        CollectionInvoiceDataTemplateID = CInt(ViewState("CollectionInvoiceDataTemplateID"))
    '        objICInvoiceDataTemplateManagement.LoadByPrimaryKey(CollectionInvoiceDataTemplateID)
    '        str = ICCollectionInvoiceDataTemplateController.GetSQLStringWithFieldsByTemplateID(CollectionInvoiceDataTemplateID, ActionStr)
    '        Try
    '            If ICCollectionInvoiceDataTemplateController.ExecuteSqlStatementForCreateTableVIATemplate(str) = True Then
    '                objICInvoiceDataTemplateManagement.IsPublished = True
    '                ICCollectionInvoiceDataTemplateController.AddInvoiceDataTemplate(objICInvoiceDataTemplateManagement, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)
    '                ICUtilities.AddAuditTrail(ActionStr, "Invoice Data Template", CollectionInvoiceDataTemplateID, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")
    '                UIUtilities.ShowDialog(Me, "Invoice Data Template Management", "Invoice data base for template [ " & objICInvoiceDataTemplateManagement.TemplateName & " ] created successfully.", ICBO.IC.Dialogmessagetype.Success)
    '                Exit Sub
    '            End If
    '        Catch ex As Exception
    '            ProcessModuleLoadException(Me, ex, False)
    '            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
    '        End Try

    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
End Class
