﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_ApprovalGroupUsers_ViewApprovalGroupUsers
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_AccountsManagement_ViewAccounts_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnTagApprovalGroup.Visible = CBool(htRights("Add"))
                ' btnDeleteTagging.Visible = CBool(htRights("Delete"))
                LoadAccounts(True)



            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
#End Region
#Region "Grid View Events"

    Protected Sub gvAccountsDetails_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvApprovalGroupTagging.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                ' Dim arrPageSizes() As String = {"1", "2", "5", "10", "50"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvApprovalGroupTagging.MasterTableView.ClientID)
                Next
                'If gvAccountsDetails.Items.Count > 0 Then
                '    myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
                'End If
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    'Protected Sub gvAccountsDetails_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvAccountsDetails.RowCommand
    '    Try
    '        If e.CommandName = "del" Then
    '            Dim ICAccounts As New ICAccounts
    '            Dim cmdArg1 As String = Nothing
    '            Dim cmdArg2 As String = Nothing
    '            Dim cmdArg3 As String = Nothing
    '            Dim s As String()
    '            s = e.CommandArgument.ToString().Split(";")
    '            cmdArg1 = s.GetValue(0)
    '            cmdArg2 = s.GetValue(1)
    '            cmdArg3 = s.GetValue(2)
    '            ICAccounts.es.Connection.CommandTimeout = 3600

    '            ICAccounts.LoadByPrimaryKey(cmdArg1, cmdArg2, cmdArg3)
    '            If ICAccounts.LoadByPrimaryKey(cmdArg1, cmdArg2, cmdArg3) Then
    '                If ICAccounts.IsApproved Then
    '                    UIUtilities.ShowDialog(Me, "Warning", "Account is approved and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
    '                Else
    '                    ICAccountsController.DeleteAccount(cmdArg1, cmdArg2, cmdArg3)

    '                    ICUtilities.AddAuditTrail("Account : " & ICAccounts.AccountNumber.ToString() & " Deleted", "Account", ICAccounts.BranchCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
    '                    UIUtilities.ShowDialog(Me, "Delete Account", "Account deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
    '                    LoadAccounts()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
    '            UIUtilities.ShowDialog(Me, "Deleted Account", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
    '            Exit Sub
    '        End If
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
#End Region
#Region "Button Events"
    Protected Sub btnSaveAccounts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTagApprovalGroup.Click
        Page.Validate()

        If Page.IsValid Then
            Response.Redirect(NavigateURL("TaggUsers", "&mid=" & Me.ModuleId & "&ApprovalGroupID=0"), False)
        End If


    End Sub
#End Region
#Region "Other Functions/Routines"
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "User Approval Group Tagging")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadAccounts(ByVal DoDataBind As Boolean)
        Try

            GetAllAccountsForRadGridByGroupAndCompanyCode(Me.gvApprovalGroupTagging.CurrentPageIndex + 1, Me.gvApprovalGroupTagging.PageSize, DoDataBind, Me.gvApprovalGroupTagging)
            If gvApprovalGroupTagging.Items.Count > 0 Then
                gvApprovalGroupTagging.Visible = True
                lblRNF.Visible = False
                ' btnDeleteTagging.Visible = CBool(htRights("Delete"))
                gvApprovalGroupTagging.Columns(4).Visible = CBool(htRights("Update"))
                gvApprovalGroupTagging.Columns(5).Visible = CBool(htRights("Delete"))
            Else
                gvApprovalGroupTagging.Visible = False
                '  btnDeleteTagging.Visible = False

                lblRNF.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
    Public Shared Sub GetAllAccountsForRadGridByGroupAndCompanyCode(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid)


        Dim qryObjICApprovalGroup As New ICApprovalGroupManagementQuery("qryObjICApprovalGroup")
        Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
        Dim qryObjICApprovalGroupUsers As New ICApprovalGroupUsersQuery("qryObjICApprovalGroupUsers")
        Dim dt As New DataTable



        qryObjICApprovalGroupUsers.Select(qryObjICApprovalGroup.GroupName, qryObjICCompany.CompanyName, qryObjICApprovalGroup.IsActive, qryObjICApprovalGroupUsers.ApprovalGroupID)
        qryObjICApprovalGroupUsers.InnerJoin(qryObjICApprovalGroup).On(qryObjICApprovalGroupUsers.ApprovalGroupID = qryObjICApprovalGroup.ApprovalGroupID)
        qryObjICApprovalGroupUsers.InnerJoin(qryObjICCompany).On(qryObjICApprovalGroup.CompanyCode = qryObjICCompany.CompanyCode)
        qryObjICApprovalGroupUsers.Where(qryObjICCompany.IsActive = True And qryObjICCompany.IsApprove = True And qryObjICCompany.IsPaymentsAllowed = True)
        qryObjICApprovalGroupUsers.Where(qryObjICApprovalGroup.IsActive = True)

        qryObjICApprovalGroupUsers.GroupBy(qryObjICApprovalGroupUsers.ApprovalGroupID, qryObjICApprovalGroup.GroupName, qryObjICCompany.CompanyName, qryObjICApprovalGroup.IsActive)
        dt = qryObjICApprovalGroupUsers.LoadDataTable


        If Not PageNumber = 0 Then
            qryObjICApprovalGroup.es.PageNumber = PageNumber
            qryObjICApprovalGroup.es.PageSize = PageSize
            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        Else
            qryObjICApprovalGroup.es.PageNumber = 1
            qryObjICApprovalGroup.es.PageSize = PageSize
            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        End If
    End Sub
    
#End Region
    

    Protected Sub gvApprovalGroupTagging_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvApprovalGroupTagging.NeedDataSource
        Try
            LoadAccounts(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvApprovalGroupTagging_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvApprovalGroupTagging.PageIndexChanged
        Try
            gvApprovalGroupTagging.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub gvAccountsDetails_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvApprovalGroupTagging.ItemCommand


        Try
            Dim CollObjICApprovalGroupUser As ICApprovalGroupUsersCollection
            Dim PassToApproveCount As Integer = 0
            Dim FailToApproveCount As Integer = 0
            Dim ApprovalGroupID As String


            If e.CommandName.ToString = "del" Then
                If Page.IsValid Then

                    ApprovalGroupID = Nothing
                    ApprovalGroupID = e.CommandArgument.ToString

                    If ICApprovalGroupUserController.IsApprovalGroupTaggedWithApprovalRuleCondition(ApprovalGroupID) = True Then
                        UIUtilities.ShowDialog(Me, "User Aproval Group Tagging", "Please delete associated aproval rule condition record(s) first", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If

                    CollObjICApprovalGroupUser = New ICApprovalGroupUsersCollection


                    CollObjICApprovalGroupUser = ICApprovalGroupUserController.GetApprovalGroupTaggedUserByApprovalGroupID(ApprovalGroupID)

                    For Each objICApprovalGroupUser As ICApprovalGroupUsers In CollObjICApprovalGroupUser

                        Try




                            ICApprovalGroupUserController.DeleteTaggedApprovalGroupUser(objICApprovalGroupUser.UserApprovalGroupID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                            PassToApproveCount = PassToApproveCount + 1
                        Catch ex As Exception
                            FailToApproveCount = FailToApproveCount + 1
                        End Try
                    Next

                End If
            End If
            If Not PassToApproveCount = 0 And Not FailToApproveCount = 0 Then
                LoadAccounts(True)
                UIUtilities.ShowDialog(Me, "User Aproval Group Tagging", "[ " & PassToApproveCount.ToString & " ] User(s) deleted successfully.<br /> [ " & FailToApproveCount.ToString & " ] User(s)  can not be deleted due to following reasons:<br /> 1. Deleted associated record(s) first.", ICBO.IC.Dialogmessagetype.Success)
                Exit Sub
            ElseIf Not PassToApproveCount = 0 And FailToApproveCount = 0 Then
                LoadAccounts(True)
                UIUtilities.ShowDialog(Me, "User Aproval Group Tagging", PassToApproveCount.ToString & " User(s)  deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                Exit Sub
            ElseIf Not FailToApproveCount = 0 And PassToApproveCount = 0 Then
                LoadAccounts(True)
                UIUtilities.ShowDialog(Me, "User Aproval Group Tagging", "[ " & FailToApproveCount.ToString & " ] User(s)  can not be deleted due to following reasons: <br /> 1. Deleted associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckGVCompanySelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvCompanyRow As GridDataItem In gvApprovalGroupTagging.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvCompanyRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    'Protected Sub btnDeleteAccounts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteTagging.Click
    '    Try
    '        Dim chkSelect As CheckBox
    '        Dim PassToApproveCount As Integer = 0
    '        Dim FailToApproveCount As Integer = 0
    '        Dim collObjICApprovalGroupUser As ICApprovalGroupUsersCollection
    '        Dim ApprovalGroupID As String

    '        If CheckGVCompanySelected() = False Then
    '            UIUtilities.ShowDialog(Me, "User Approval Group Tagging", "Please select atleast one account.", ICBO.IC.Dialogmessagetype.Failure)
    '            Exit Sub
    '        End If
    '        For Each rowGVAccounts In gvApprovalGroupTagging.Items
    '            chkSelect = New CheckBox
    '            chkSelect = DirectCast(rowGVAccounts.Cells(0).FindControl("chkSelect"), CheckBox)
    '            If chkSelect.Checked = True Then
    '                ApprovalGroupID = Nothing
    '                ApprovalGroupID = rowGVAccounts("ApprovalGroupID").ToString

    '                collObjICApprovalGroupUser = New ICApprovalGroupUsersCollection
    '                collObjICApprovalGroupUser = ICApprovalGroupUserController.GetApprovalGroupTaggedUserByApprovalGroupID(ApprovalGroupID)
    '                For Each objICApprovalGroupUser As ICApprovalGroupUsers In collObjICApprovalGroupUser

    '                    Try
    '                        ICApprovalGroupUserController.DeleteTaggedApprovalGroupUser(objICApprovalGroupUser.UserApprovalGroupID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
    '                        PassToApproveCount = PassToApproveCount + 1
    '                    Catch ex As Exception
    '                        FailToApproveCount = FailToApproveCount + 1
    '                    End Try
    '                Next
    '            End If
    '        Next

    '        If Not PassToApproveCount = 0 And Not FailToApproveCount = 0 Then
    '            LoadAccounts(True)
    '            UIUtilities.ShowDialog(Me, "User Approval Group Tagging", "[ " & PassToApproveCount.ToString & " ] Users(s) deleted successfully.<br /> [ " & FailToApproveCount.ToString & " ] Users(s) can not be deleted due to following reasons:<br /> 1. Deleted associated record(s) first", ICBO.IC.Dialogmessagetype.Success)
    '            Exit Sub
    '        ElseIf Not PassToApproveCount = 0 And FailToApproveCount = 0 Then
    '            LoadAccounts(True)
    '            UIUtilities.ShowDialog(Me, "User Approval Group Tagging", PassToApproveCount.ToString & " Users(s) deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
    '            Exit Sub
    '        ElseIf Not FailToApproveCount = 0 And PassToApproveCount = 0 Then
    '            LoadAccounts(True)
    '            UIUtilities.ShowDialog(Me, "User Approval Group Tagging", "[ " & FailToApproveCount.ToString & " ] Users(s) can not be deleted due to following reasons: <br /> 1. Deleted associated record(s) first", ICBO.IC.Dialogmessagetype.Failure)
    '            Exit Sub
    '        End If

    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

    Protected Sub gvAccountsDetails_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvApprovalGroupTagging.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvApprovalGroupTagging.MasterTableView.ClientID & "','0');"
        End If

    End Sub
End Class
