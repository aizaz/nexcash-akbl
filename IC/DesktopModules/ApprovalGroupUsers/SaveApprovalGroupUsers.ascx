﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveApprovalGroupUsers.ascx.vb" Inherits="DesktopModules_ApprovalGroupUsers_SaveApprovalGroupUsers" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }





    function ValidateCheckClientUserList(source, args) {
        var chkListModules = document.getElementById('<%= chklstUser.ClientID%>');
        var chkListinputs = chkListModules.getElementsByTagName("input");
        for (var i = 0; i < chkListinputs.length; i++) {
            if (chkListinputs[i].checked) {
                args.IsValid = true;
                return;
            }
        }
        args.IsValid = false;
    }


    </script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
            }
        }
    }
</script>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>
<style type="text/css">
    .auto-style1 {
        width: 25%;
        height: 23px;
    }
    .auto-style2 {
        width: 33%;
        height: 23px;
    }
    .auto-style3 {
        width: 17%;
        height: 23px;
    }
</style>
<telerik:RadInputManager ID="radSaveAccounts" runat="server" Enabled="true">
           <telerik:TextBoxSetting BehaviorID="radAccountTitle" Validation-IsRequired="true" ErrorMessage="Invalid Account Title"  >
            <TargetControls>
                <telerik:TargetInput ControlID="txtAccountTitle" />
            </TargetControls>
        </telerik:TextBoxSetting> 
            <telerik:TextBoxSetting BehaviorID="radFetchedAccountTitle" Validation-IsRequired="true" ErrorMessage="Invalid Account Title">
            <TargetControls>
                <telerik:TargetInput ControlID="txtFetchedAccountTitle" />
            </TargetControls>
        </telerik:TextBoxSetting> 
        <telerik:TextBoxSetting BehaviorID="radCSTSwiftCode" Validation-IsRequired="false" ErrorMessage="Invalid Code" EmptyMessage="">
            <TargetControls>
                <telerik:TargetInput ControlID="txtCustomerSWFTCode" />
            </TargetControls>
        </telerik:TextBoxSetting> 
                   
         
        <telerik:RegExpTextBoxSetting BehaviorID="radAccountNo" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9 ]+$" ErrorMessage="Invalid Account No."
        EmptyMessage="" >
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountNumber" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
        <telerik:RegExpTextBoxSetting BehaviorID="radCurrency" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9 ]+$" ErrorMessage="Invalid Currency">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountCurrency" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
         <telerik:RegExpTextBoxSetting BehaviorID="radBranchCode" Validation-IsRequired="true" 
        ValidationExpression="^[a-zA-Z0-9 ]+$" ErrorMessage="Invalid Branch Code">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBranchCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
 
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqGroup" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 28%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqCompany" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 22%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlGroup" runat="server" 
                ControlToValidate="ddlGroup" Display="Dynamic" 
                ErrorMessage="Please select Group" InitialValue="0" 
                SetFocusOnError="True" ValidationGroup="Save"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 33%">
            <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" 
                ControlToValidate="ddlCompany" Display="Dynamic" 
                ErrorMessage="Please select Company" InitialValue="0" 
                SetFocusOnError="True" ValidationGroup="Save"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 17%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="auto-style1">
            <asp:Label ID="lblApprovalGroup" runat="server" Text="Select Approval Group" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqApprovalGroup" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" class="auto-style1">
            </td>
        <td align="left" valign="top" class="auto-style2">
            <asp:Label ID="lblUserType" runat="server" Text="Select User Type" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqUserType" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" class="auto-style3">
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlApprovalGroup" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 33%">
            <asp:DropDownList ID="ddlUserType" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem Value="Client User">Client User</asp:ListItem>
                <asp:ListItem>Bank User</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 17%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlApprovalGroup" runat="server" 
                ControlToValidate="ddlApprovalGroup" Display="Dynamic" 
                ErrorMessage="Please select Approval Group" InitialValue="0" 
                SetFocusOnError="True" ValidationGroup="Save"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 33%">
            <asp:RequiredFieldValidator ID="rfvddlUserType" runat="server" 
                ControlToValidate="ddlUserType" Display="Dynamic" 
                ErrorMessage="Please select User Type" InitialValue="0" 
                SetFocusOnError="True" ValidationGroup="Save"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 17%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblLocation" runat="server" Text="Select Location" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqLocation" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 33%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 17%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlLocation" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 33%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 17%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="auto-style1">
            <asp:RequiredFieldValidator ID="rfvddlLocation" runat="server" 
                ControlToValidate="ddlLocation" Display="Dynamic" 
                ErrorMessage="Please select Location" InitialValue="0" 
                SetFocusOnError="True" ValidationGroup="Save"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" class="auto-style1">
            </td>
        <td align="left" valign="top" class="auto-style2">
        </td>
        <td align="left" valign="top" class="auto-style3">
            </td>
    </tr>
        <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
          
                <table id="tblUsers" runat="server" style="width:100%">
                    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
                                   <asp:Label ID="lblUser" runat="server" Text="Select User" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqUser" runat="server" Text="*" ForeColor="Red"></asp:Label></td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;&nbsp;</td>
    </tr>
        <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
                                    <asp:Panel ID="pnlClientUser" runat="server" Width="100%" ScrollBars="Vertical"
                Height="68px" >
                <asp:CheckBoxList ID="chklstUser" runat="server" CssClass="chkBox" Width="90%" >
                </asp:CheckBoxList>
            </asp:Panel></td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
                     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
                                        <asp:CustomValidator ID="CVUserList" runat="server" ClientValidationFunction="ValidateCheckClientUserList" ErrorMessage="Please Select Atleast one User" ValidationGroup="Save"></asp:CustomValidator>
            </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
             &nbsp;
            </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
                </table>
           
         </td>
       
    </tr>
        
       
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;&nbsp;
            
            
            <asp:Button ID="btnSave2" runat="server" Text="Save" CssClass="btn" 
                Width="70px" CausesValidation="true" UseSubmitBehavior="False" ValidationGroup="Save"/>
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False"
               />
            &nbsp;
            </td>
    </tr>
    </table>


<table width="100%">

    <tr>
                    <td align="left" valign="top">
                        <telerik:RadGrid ID="gvUser" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid" >
                            <ClientSettings>
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Select" DataField="IsApprove">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>    
                                    <telerik:GridBoundColumn DataField="UserApprovalGroupID" HeaderText="ID" SortExpression="UserApprovalGroupID">
                                    </telerik:GridBoundColumn> 
                                    <telerik:GridBoundColumn DataField="UserType" HeaderText="User Type" SortExpression="UserType">
                                    </telerik:GridBoundColumn>                              
                                    <telerik:GridBoundColumn DataField="UserName" HeaderText="User Name" SortExpression="UserName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisplayName" HeaderText="Display Name" SortExpression="DisplayName">
                                    </telerik:GridBoundColumn>
<%--                                    <telerik:GridTemplateColumn>
                                       <HeaderTemplate>
                                            <asp:Label ID="Edit" runat="server" Text="Edit"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                           <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("Add", "&mid=" & Me.ModuleId & "&id=" & Eval("TaggesUserID"))%>'
                                            ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn> --%>                                  
                                    <telerik:GridTemplateColumn>
                                         <HeaderTemplate>
                                            <asp:Label ID="Delete" runat="server" Text="Delete"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                           <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("UserApprovalGroupID")%>'
                                            CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                            ToolTip="Delete" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />

<FilterMenu EnableImageSprites="False"></FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
     <tr>
                    <td align="center" valign="top">
                        <%--                                    <telerik:GridTemplateColumn>
                                       <HeaderTemplate>
                                            <asp:Label ID="Edit" runat="server" Text="Edit"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                           <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("Add", "&mid=" & Me.ModuleId & "&id=" & Eval("TaggesUserID"))%>'
                                            ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn> --%>&nbsp;<asp:Button ID="btnDeleteTaggedUser" runat="server"  Text="Delete Tagged User" 
                        OnClientClick="javascript: return con();"  CssClass="btn"  />
                    </td>
                </tr>  
              
</table>
