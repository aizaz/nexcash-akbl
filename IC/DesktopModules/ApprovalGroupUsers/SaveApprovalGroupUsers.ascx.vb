﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_ApprovalGroupUsers_SaveApprovalGroupUsers
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ApprovalGroupID As String = Nothing
    Private htRights As Hashtable
#Region "Page Load"

    'Add code for taggedclientuserforaccount by JAWWAD 13-03-2014

    ' Addition code for [4 eye principle] by Farhan Hassan 07-04-2015
    Protected Sub DesktopModules_AccountsManagement_SaveAccount_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            ApprovalGroupID = Request.QueryString("ApprovalGroupID").ToString()
            If Page.IsPostBack = False Then

                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlCompany()
                LoadddlApprovalGroup()
                LoadddlLocation(ddlUserType.SelectedValue)
                If ApprovalGroupID.ToString() = "0" Then
                    Clear()
                    lblPageHeader.Text = "Tag User, Aproval Group"
                    btnSave2.Text = "Save"
                    btnSave2.Visible = CBool(htRights("Add"))
                    gvUser.Visible = False
                    btnDeleteTaggedUser.Visible = False
                    tblUsers.Style.Add("display", "none")
                Else
                    Dim CollObjICApprovalGroupUser As New ICApprovalGroupUsersCollection
                    Dim ObjICApprovalGroupUser As New ICApprovalGroupUsers
                    lblPageHeader.Text = "Update User, Aproval Group"
                    btnSave2.Text = "Update"
                    btnSave2.Visible = CBool(htRights("Update"))

                    CollObjICApprovalGroupUser = ICApprovalGroupUserController.GetApprovalGroupTaggedUserByApprovalGroupID(ApprovalGroupID)
                    ObjICApprovalGroupUser = CollObjICApprovalGroupUser(0)

                    LoadddlGroup()
                    ddlGroup.SelectedValue = ObjICApprovalGroupUser.UpToICApprovalGroupManagementByApprovalGroupID.UpToICCompanyByCompanyCode.GroupCode
                    LoadddlCompany()
                    ddlCompany.SelectedValue = ObjICApprovalGroupUser.UpToICApprovalGroupManagementByApprovalGroupID.CompanyCode
                    LoadddlApprovalGroup()
                    ddlApprovalGroup.SelectedValue = ObjICApprovalGroupUser.ApprovalGroupID

                    LoadddlLocation(ddlUserType.SelectedValue.ToString)
                    tblUsers.Style.Add("display", "none")

                    ddlGroup.Enabled = False
                    ddlCompany.Enabled = False
                    ddlApprovalGroup.Enabled = False


                    LoadgvTaggedAccountUser(True)


                End If
                'btnSave.Visible = ArrRights(1)

            End If



        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "User Approval Group Tagging")
            ViewState("htRights") = htRights

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#Region "Button Events"

    'Add code for taggedclientuserforaccount by JAWWAD 13-03-2014
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave2.Click
        If Page.IsValid Then
            Try
                Dim objICApprovalGroupUser As ICApprovalGroupUsers
                Dim UserCount As Integer = 0
                For Each user As ListItem In chklstUser.Items

                    If user.Selected = True Then
                        objICApprovalGroupUser = New ICApprovalGroupUsers

                        objICApprovalGroupUser.ApprovalGroupID = ddlApprovalGroup.SelectedValue.ToString
                        objICApprovalGroupUser.UserID = user.Value
                        objICApprovalGroupUser.Creator = Me.UserId.ToString
                        objICApprovalGroupUser.CreationDate = Date.Now
                        objICApprovalGroupUser.CreatedBy = Me.UserId.ToString
                        objICApprovalGroupUser.CreatedDate = Date.Now
                        ICApprovalGroupUserController.AddApprovalGroupUsers(objICApprovalGroupUser, False, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                        UpdateConditionUsersByGroupID(ddlApprovalGroup.SelectedValue.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                        UserCount = UserCount + 1
                    End If

                Next


                UIUtilities.ShowDialog(Me, "Tag User Aproval Group", "[ " & UserCount & " ] users tagged successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())


            Catch ex As Exception
                If ex.Message.Contains("Incorrect syntax near the keyword") Then
                    UIUtilities.ShowDialog(Me, "Save Account", "Can not add duplicate Account.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub
    Private Function UpdateConditionUsersByGroupID(ByVal GroupID As String, ByVal UsersID As String, ByVal UsersName As String) As Boolean
        Dim collObjICAppGroupUsers As New ICApprovalGroupUsersCollection
        Dim dtConditions As DataTable
        Dim ArrayConditionType As New ArrayList
        Dim TaggedUserCount As Integer = 0
        ''Required Condition Type to update users
        ArrayConditionType.Add("Any")
        ArrayConditionType.Add("All")

        ''Get Conditions
        dtConditions = ICApprovalRuleConditionsController.GetAllApprovalRuleConditionsByApprovalGroupIDAndConditionType(GroupID, ArrayConditionType)


        If dtConditions.Rows.Count > 0 Then
            ''Get Approval Group Users Collection
            collObjICAppGroupUsers = ICApprovalGroupUserController.GetApprovalGroupTaggedUserByApprovalGroupID(ApprovalGroupID)
            For Each dr As DataRow In dtConditions.Rows
                For Each objICAppGroupUser As ICApprovalGroupUsers In collObjICAppGroupUsers
                    ''Verify duplicate user against same condition
                    If ICApprovalRuleConditionUserController.IsUserTaggedWithCondition(objICAppGroupUser.UserID, objICAppGroupUser.ApprovalGroupID, dr("ApprovalRuleConditionID")) = False Then
                        ''Update Users
                        AddApprovalRuleConditionUser(objICAppGroupUser.UserID, dr("ApprovalRuleConditionID"), UsersID, UsersName)
                        ''Un-approve Approval Rule
                        Dim objICApprovalCondition As New ICApprovalRuleConditions
                        Dim objICApprovalRule As New ICApprovalRule
                        Dim ActionString As String = Nothing
                        objICApprovalCondition.LoadByPrimaryKey(dr("ApprovalRuleConditionID"))

                        ActionString = "Aproval Rule [ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.ApprovalRuleName.ToString() & " ], For Company "
                        ActionString += "[ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.CompanyCode.ToString() & " ][ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.UpToICCompanyByCompanyCode.CompanyName.ToString() & " ],"
                        ActionString += " Payment Nature [ " & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.PaymentNatureCode & " ][ "
                        ActionString += "" & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ], From Amount [ "
                        ActionString += "" & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.FromAmount & " ] , To Amount [" & objICApprovalCondition.UpToICApprovalRuleByApprovalRuleID.ToAmount & " ], IsApproved [ "
                        ActionString += "updated"


                        objICApprovalRule.LoadByPrimaryKey(objICApprovalCondition.ApprovalRuleID)

                        objICApprovalRule.CreatedBy = Me.UserInfo.UserID
                        objICApprovalRule.CreatedDate = Date.Now
                        ICApprovalRuleManagementController.AddApprovalRule(objICApprovalRule, True, Me.UserInfo.UserID, Me.UserInfo.Username, ActionString)

                    End If
                Next
            Next
        End If
    End Function
    Private Sub AddApprovalRuleConditionUser(ByVal AppUserID As String, ByVal AppConditionID As String, ByVal UsersID As String, ByVal UsersName As String)
        Dim objICAppRuleCondUser As New ICApprovalRuleConditionUsers
        objICAppRuleCondUser.UserID = AppUserID
        objICAppRuleCondUser.ApprovaRuleCondID = AppConditionID
        objICAppRuleCondUser.CreatedBy = UsersID
        objICAppRuleCondUser.CreatedDate = Date.Now

        objICAppRuleCondUser.UserCount = objICAppRuleCondUser.UpToICApprovalRuleConditionsByApprovaRuleCondID.ICApprovalRuleConditionUsersCollectionByApprovaRuleCondID(0).UserCount

        ICApprovalRuleConditionUserController.AddApprovalRuleCondtionsUser(objICAppRuleCondUser, False, UsersID, UsersName)
    End Sub



    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)


        'ICTaggedClientUserForAccountController.SendDailyAccountBalanceToTaggedClientUser()
    End Sub

#End Region

#Region "Drop Down Events"
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlApprovalGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlApprovalGroup.Items.Clear()
            ddlApprovalGroup.Items.Add(lit)
            ddlApprovalGroup.AppendDataBoundItems = True
            ddlApprovalGroup.DataSource = ICApprovalGroupController.GetAllActiveApprovalGroupsByCompanyCode(ddlCompany.SelectedValue.ToString)
            ddlApprovalGroup.DataTextField = "GroupName"
            ddlApprovalGroup.DataValueField = "ApprovalGroupID"
            ddlApprovalGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub LoadddlLocation(ByVal UserType As String)
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlLocation.Items.Clear()
            ddlLocation.Items.Add(lit)
            ddlLocation.AppendDataBoundItems = True
            If UserType <> "0" Then
                If UserType = "Bank User" Then
                    ddlLocation.DataSource = ICOfficeController.GetPrincipalBankBranchActiveAndApprove
                    ddlLocation.DataTextField = "OfficeName"
                    ddlLocation.DataValueField = "OfficeID"
                    ddlLocation.DataBind()
                ElseIf UserType = "Client User" Then
                    ddlLocation.DataSource = ICOfficeController.GetOfficeByCompanyCodeActiveAndApprove(ddlCompany.SelectedValue)
                    ddlLocation.DataTextField = "OfficeName"
                    ddlLocation.DataValueField = "OfficeID"
                    ddlLocation.DataBind()
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        LoadddlCompany()
        LoadddlApprovalGroup()
        ddlUserType.ClearSelection()
        LoadddlLocation(ddlUserType.SelectedValue.ToString)
        chklstUser.DataSource = Nothing
        chklstUser.DataBind()
        tblUsers.Style.Add("display", "none")
        LoadgvTaggedAccountUser(True)


        'txtAccountNumber.Text = ""
        'txtAccountTitle.Text = ""
        'txtBranchCode.Text = ""
        'txtAccountCurrency.Text = ""
    End Sub
#End Region

#Region "Other Functions/Routines"
    Private Sub Clear()


        ddlGroup.ClearSelection()
        ddlCompany.ClearSelection()
        ddlLocation.ClearSelection()
        ddlUserType.ClearSelection()
        chklstUser.Items.Clear()


    End Sub

#End Region

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged

        LoadddlApprovalGroup()
        ddlUserType.ClearSelection()
        LoadddlLocation(ddlUserType.SelectedValue.ToString)
        chklstUser.DataSource = Nothing
        chklstUser.DataBind()
        tblUsers.Style.Add("display", "none")
        LoadgvTaggedAccountUser(True)
    End Sub

    Private Sub ClearonUpdate(ByVal IsUpdate As Boolean)


        pnlClientUser.Visible = False


        gvUser.Visible = False
        btnDeleteTaggedUser.Visible = False
    End Sub


    'created for USER TAGGING SECTION WITH ACCOUNTS By JAWWAD 12-03-2014
#Region "USER TAGGING SECTION"

    'Created to load client user in Checklist
    Private Sub LoadClientUser()
        Try

            chklstUser.DataSource = ICApprovalGroupUserController.GetUntaggedUserByUserTypeAndCompanyCodeAndApprovalGroupCode(ddlUserType.SelectedValue.ToString, ddlApprovalGroup.SelectedValue.ToString, ddlLocation.SelectedValue.ToString, ddlGroup.SelectedValue.ToString)
            chklstUser.DataTextField = "UserName"
            chklstUser.DataValueField = "UserID"
            chklstUser.DataBind()




        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    'Created to load gv in add page( to load selected user)
    Private Sub LoadgvTaggedAccountUser(ByVal DoDataBind As Boolean)
        Try



            ICApprovalGroupUserController.GetTaggedApprovalGroupUserForRadGrid(Me.gvUser.CurrentPageIndex + 1, Me.gvUser.PageSize, Me.gvUser, DoDataBind, ddlApprovalGroup.SelectedValue.ToString)
            If gvUser.Items.Count > 0 Then
                gvUser.Visible = True
                'lblRNF.Visible = False
                btnDeleteTaggedUser.Visible = CBool(htRights("Delete"))
                'btnApproveCompany.Visible = CBool(htRights("Approve"))
                gvUser.Columns(5).Visible = CBool(htRights("Delete"))
                CVUserList.Enabled = False
            Else
                gvUser.Visible = False
                'lblRNF.Visible = True
                btnDeleteTaggedUser.Visible = False
                If ApprovalGroupID <> "0" Then
                    CVUserList.Enabled = False
                Else
                    CVUserList.Enabled = True
                End If
                'btnApproveCompany.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    'created for add page gv functionality
#Region "GVCLIENTUSER"

    Protected Sub gvClientUser_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles gvUser.ItemCommand

        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    Dim icTaggesClientUser As New ICApprovalGroupUsers
                    icTaggesClientUser.es.Connection.CommandTimeout = 3600
                    If icTaggesClientUser.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                        'If icPdc.IsApproved Then
                        '    UIUtilities.ShowDialog(Me, "Warning", "Country is approved and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
                        'Else
                        If ICApprovalGroupUserController.IsUserTaggedWithApprovalRuleCondition(ApprovalGroupID, icTaggesClientUser.UserID) = True Then
                            UIUtilities.ShowDialog(Me, "User Aproval Group Tagging", "Please delete associated aproval rule condition record first", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If


                        ICApprovalGroupUserController.DeleteTaggedApprovalGroupUser(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        UIUtilities.ShowDialog(Me, "User Aproval Group Tagging", "User deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                        LoadgvTaggedAccountUser(True)
                        ' End If
                    End If

                End If
            End If
            LoadClientUser()
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "User Aproval Group Tagging", "Tagged User can not be delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvClientUser_ItemCreated(sender As Object, e As GridItemEventArgs) Handles gvUser.ItemCreated

        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvUser.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvClientUser_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles gvUser.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvUser.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub gvClientUser_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles gvUser.NeedDataSource
        LoadgvTaggedAccountUser(False)
    End Sub

    Protected Sub gvClientUser_PageIndexChanged(sender As Object, e As GridPageChangedEventArgs) Handles gvUser.PageIndexChanged
        gvUser.CurrentPageIndex = e.NewPageIndex
    End Sub

#End Region

    'created from button delete pdc below
    Private Function CheckgvPdcForProcessAll() As Boolean
        Try
            Dim rowgvPdc As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvPdc In gvUser.Items
                chkSelect = New CheckBox

                chkSelect = DirectCast(rowgvPdc.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    'Created to delete selected user(from button/bulk delete) from the gv in add page
    Protected Sub btnDeletePdc_Click(sender As Object, e As EventArgs) Handles btnDeleteTaggedUser.Click
        If Page.IsValid Then
            Try
                Dim rowgvPdc As GridDataItem
                Dim chkSelect As CheckBox
                Dim TaggesUserID As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0
                Dim icTaggesClientUser As ICApprovalGroupUsers
                If CheckgvPdcForProcessAll() = True Then
                    For Each rowgvPdc In gvUser.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvPdc.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            TaggesUserID = rowgvPdc("UserApprovalGroupID").Text.ToString()
                            icTaggesClientUser = New ICApprovalGroupUsers
                            If icTaggesClientUser.LoadByPrimaryKey(TaggesUserID.ToString()) Then
                                Try
                                    If ICApprovalGroupUserController.IsUserTaggedWithApprovalRuleCondition(icTaggesClientUser.ApprovalGroupID, icTaggesClientUser.UserID) = True Then
                                        FailCount = FailCount + 1
                                        Continue For
                                    End If

                                    ICApprovalGroupUserController.DeleteTaggedApprovalGroupUser(TaggesUserID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                    PassCount = PassCount + 1
                                    Continue For
                                Catch ex As Exception

                                    FailCount = FailCount + 1
                                    Continue For
                                End Try
                                'End If
                            End If


                        End If

                    Next

                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "User Aproval Group Tagging", "[" & FailCount.ToString() & "] Users can not be deleted due to following reasones : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "User Aproval Group Tagging", "[" & PassCount.ToString() & "] Users deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        'LoadgvTaggedAccountUser(True)
                        'LoadClientUser(True)
                        Exit Sub
                    End If

                    UIUtilities.ShowDialog(Me, "User Aproval Group Tagging", "[" & PassCount.ToString() & "] Users deleted successfully. [" & FailCount.ToString() & "] Users can not be deleted due to following reasons : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "User Aproval Group Tagging", "Please select atleast one(1) Client User.", ICBO.IC.Dialogmessagetype.Warning)
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

#End Region

    Protected Sub ddlApprovalGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlApprovalGroup.SelectedIndexChanged
        
        ddlUserType.ClearSelection()
        LoadddlLocation(ddlUserType.SelectedValue.ToString)
        chklstUser.DataSource = Nothing
        chklstUser.DataBind()
        tblUsers.Style.Add("display", "none")
        LoadgvTaggedAccountUser(True)
    End Sub

    Protected Sub ddlUserType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserType.SelectedIndexChanged

        LoadddlLocation(ddlUserType.SelectedValue.ToString)
        chklstUser.DataSource = Nothing
        chklstUser.DataBind()
        tblUsers.Style.Add("display", "none")
        LoadgvTaggedAccountUser(True)
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        LoadClientUser()
        tblUsers.Style.Remove("display")
        LoadgvTaggedAccountUser(True)
    End Sub

   
End Class
