﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_CollectionInvoiceDBQueueManagement_CollectionInvoiceDBQueueManagement
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
    Private AssignUserRolsID As New ArrayList


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()
            If Page.IsPostBack = False Then
                If Me.UserInfo.IsSuperUser = False Then

                    ViewState("htRights") = Nothing
                    GetPageAccessRights()
                    LoadddlGroup()
                    LoadddlCompany()
                    LoadddlCollectionNature()
                    LoadddlCollectionNatureTemplate()
                    tblMain.Style.Remove("display")
                    tblReferenceField.Style.Add("display", "none")
                    tblResult.Style.Add("display", "none")
                Else
                    UIUtilities.ShowDialog(Me, "Invoice DB Queue Management", "Sorry! You are not authorized to login.", Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        AssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Invoice DB Queue Management") = True Then
                    If Not AssignUserRolsID.Contains(RoleID.ToString) Then
                        AssignUserRolsID.Add(RoleID.ToString)
                    End If
                End If
            Next
        End If
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Invoice DB Queue Management")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICUserRolesCollectionAccountAndCollectionNatureController.GetCollectionGroupTaggedWithUserForDropDown(Me.UserId.ToString, AssignUserRolsID)
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICUserRolesCollectionAccountAndCollectionNatureController.GetCollectionCompanyTaggedWithUserForDropDownByGroupCode(Me.UserId.ToString, AssignUserRolsID, ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCollectionNature()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCollNature.Items.Clear()
            ddlCollNature.Items.Add(lit)
            ddlCollNature.AppendDataBoundItems = True
            ddlCollNature.DataSource = ICUserRolesCollectionAccountAndCollectionNatureController.GetCollectionNatureTaggedWithUserForDropDownByCompanyCodeInvoiceDBAvailable(Me.UserId.ToString, AssignUserRolsID, ddlCompany.SelectedValue.ToString())
            ddlCollNature.DataTextField = "CollAccountAndNature"
            ddlCollNature.DataValueField = "Value"
            ddlCollNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCollectionNatureTemplate()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCollNatureTemplate.Items.Clear()
            ddlCollNatureTemplate.Items.Add(lit)
            ddlCollNatureTemplate.AppendDataBoundItems = True
            ddlCollNatureTemplate.DataSource = ICCollectionInvoiceDataTemplateController.GetTemplatesByCollectionNature(ddlCollNature.SelectedValue.ToString)
            ddlCollNatureTemplate.DataTextField = "TemplateName"
            ddlCollNatureTemplate.DataValueField = "CollectionInvoiceTemplateID"
            ddlCollNatureTemplate.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        LoadddlCompany()
        LoadddlCollectionNature()
        LoadddlCollectionNatureTemplate()

    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        LoadddlCollectionNature()
        LoadddlCollectionNatureTemplate()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(41))
    End Sub

    
    Private Sub Clear()
        LoadddlGroup()
        LoadddlCompany()
        LoadddlCollectionNature()

    End Sub

    Protected Sub ddlCollNature_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCollNature.SelectedIndexChanged
        Try
            LoadddlCollectionNatureTemplate()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCollNatureTemplate_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCollNatureTemplate.SelectedIndexChanged
        Try
            If ddlCollNatureTemplate.SelectedValue.ToString <> "0" Then
                Dim objICCollectionNature As New ICCollectionNature
                objICCollectionNature.LoadByPrimaryKey(ddlCollNature.SelectedValue.ToString)
                DisableReferenceFieldValidation()
                tblMain.Style.Add("display", "none")
                tblResult.Style.Add("display", "none")
                tblReferenceField.Style.Remove("display")
                SetReferenceFieldVisibilityByCollectionNature(objICCollectionNature)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub DisableReferenceFieldValidation()
        DirectCast(Me.Control.FindControl("trReferenceField1"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField2"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField3"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField4"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField5"), HtmlTableRow).Style.Add("display", "none")
        rfvReferenceField1.Enabled = False
        rfvReferenceField2.Enabled = False
        rfvReferenceField3.Enabled = False
        rfvReferenceField4.Enabled = False
        rfvReferenceField5.Enabled = False
        RadInputReferenceField1.Enabled = False
        RadInputReferenceField2.Enabled = False
        RadInputReferenceField3.Enabled = False
        RadInputReferenceField4.Enabled = False
        RadInputReferenceField5.Enabled = False
        txtReferenceField1.Text = ""
        txtReferenceField2.Text = ""
        txtReferenceField3.Text = ""
        txtReferenceField4.Text = ""
        txtReferenceField5.Text = ""
    End Sub
    Private Sub SetReferenceFieldVisibilityByCollectionNature(ByVal objICCollectionNature As ICCollectionNature)
        Dim objICReferencFieldsCollection As New ICCollectionInvoiceDataTemplateFieldsCollection
        objICReferencFieldsCollection = ICCollectionInvoiceDataTemplateFieldsListController.GetReferenceFieldsCollectionInvoiceTemplateOfDualFile(objICCollectionNature.InvoiceTemplateID.ToString)
        If objICCollectionNature.RefField1Caption IsNot Nothing Then
            If objICCollectionNature.RefField1RequiredforSearching = True Then
                lblReferenceField1.Text = objICCollectionNature.RefField1Caption
                DirectCast(Me.Control.FindControl("trReferenceField1"), HtmlTableRow).Style.Remove("display")
                RadInputReferenceField1.Enabled = True
                rfvReferenceField1.Enabled = True
                rfvReferenceField1.ErrorMessage = ""
                txtReferenceField1.MaxLength = objICReferencFieldsCollection(0).FieldLength
                If objICReferencFieldsCollection(0).FieldDBType = "Integer" Then
                    rfvReferenceField1.ValidationExpression = "^[0-9]*$"
                    rfvReferenceField1.ErrorMessage = "Only numbers are allowed."
                ElseIf objICReferencFieldsCollection(0).FieldDBType = "Decimal" Then
                    rfvReferenceField1.ValidationExpression = "^\d+\.\d{0,2}$"
                    rfvReferenceField1.ErrorMessage = "Only decimal values are allowed."
                ElseIf objICReferencFieldsCollection(0).FieldDBType = "String" Then
                    rfvReferenceField1.Enabled = False
                End If
                If objICCollectionNature.RefField1RequiredforInput = True Then
                    RadInputReferenceField1.GetSettingByBehaviorID("reReferenceFieldOne").Validation.IsRequired = True
                End If
            End If
        End If
        If objICCollectionNature.RefField2Caption IsNot Nothing Then
            If objICCollectionNature.RefField2RequiredforSearching = True Then
                lblReferenceField2.Text = objICCollectionNature.RefField2Caption
                DirectCast(Me.Control.FindControl("trReferenceField2"), HtmlTableRow).Style.Remove("display")
                RadInputReferenceField2.Enabled = True
                rfvReferenceField2.Enabled = True
                rfvReferenceField2.ErrorMessage = ""
                txtReferenceField2.MaxLength = objICReferencFieldsCollection(1).FieldLength
                If objICReferencFieldsCollection(1).FieldDBType = "Integer" Then
                    rfvReferenceField2.ValidationExpression = "^[0-9]*$"
                    rfvReferenceField2.ErrorMessage = "Only numbers are allowed."
                ElseIf objICReferencFieldsCollection(1).FieldDBType = "Decimal" Then
                    rfvReferenceField2.ValidationExpression = "^\d+\.\d{0,2}$"
                    rfvReferenceField2.ErrorMessage = "Only decimal values are allowed."
                ElseIf objICReferencFieldsCollection(1).FieldDBType = "String" Then
                    rfvReferenceField2.Enabled = False
                End If
                If objICCollectionNature.RefField2RequiredforInput = True Then
                    RadInputReferenceField2.GetSettingByBehaviorID("reReferenceFieldTwo").Validation.IsRequired = True
                End If
            End If
        End If
        If objICCollectionNature.RefField3Caption IsNot Nothing Then
            If objICCollectionNature.RefField3RequiredforSearching = True Then
                lblReferenceField3.Text = objICCollectionNature.RefField3Caption
                DirectCast(Me.Control.FindControl("trReferenceField3"), HtmlTableRow).Style.Remove("display")
                RadInputReferenceField3.Enabled = True
                rfvReferenceField3.Enabled = True
                rfvReferenceField3.ErrorMessage = ""
                txtReferenceField3.MaxLength = objICReferencFieldsCollection(2).FieldLength
                If objICReferencFieldsCollection(2).FieldDBType = "Integer" Then
                    rfvReferenceField3.ValidationExpression = "^[0-9]*$"
                    rfvReferenceField3.ErrorMessage = "Only numbers are allowed."
                ElseIf objICReferencFieldsCollection(2).FieldDBType = "Decimal" Then
                    rfvReferenceField3.ValidationExpression = "^\d+\.\d{0,2}$"
                    rfvReferenceField3.ErrorMessage = "Only decimal values are allowed."
                ElseIf objICReferencFieldsCollection(2).FieldDBType = "String" Then
                    rfvReferenceField3.Enabled = False
                End If
                If objICCollectionNature.RefField3RequiredforInput = True Then
                    RadInputReferenceField3.GetSettingByBehaviorID("reReferenceFieldThree").Validation.IsRequired = True
                End If
            End If
        End If
        If objICCollectionNature.RefField4Caption IsNot Nothing Then
            If objICCollectionNature.RefField4RequiredforSearching = True Then
                lblReferenceField4.Text = objICCollectionNature.RefField4Caption
                DirectCast(Me.Control.FindControl("trReferenceField4"), HtmlTableRow).Style.Remove("display")
                RadInputReferenceField4.Enabled = True
                rfvReferenceField4.Enabled = True
                rfvReferenceField4.ErrorMessage = ""
                txtReferenceField4.MaxLength = objICReferencFieldsCollection(3).FieldLength
                If objICReferencFieldsCollection(3).FieldDBType = "Integer" Then
                    rfvReferenceField4.ValidationExpression = "^[0-9]*$"
                    rfvReferenceField4.ErrorMessage = "Only numbers are allowed."
                ElseIf objICReferencFieldsCollection(3).FieldDBType = "Decimal" Then
                    rfvReferenceField4.ValidationExpression = "^\d+\.\d{0,2}$"
                    rfvReferenceField4.ErrorMessage = "Only decimal values are allowed."
                ElseIf objICReferencFieldsCollection(3).FieldDBType = "String" Then
                    rfvReferenceField4.Enabled = False
                End If
                If objICCollectionNature.RefField4RequiredforInput = True Then
                    RadInputReferenceField4.GetSettingByBehaviorID("reReferenceFieldFour").Validation.IsRequired = True
                End If
            End If
        End If
        If objICCollectionNature.RefField5Caption IsNot Nothing Then
            If objICCollectionNature.RefField5RequiredforSearching = True Then
                lblReferenceField5.Text = objICCollectionNature.RefField5Caption
                DirectCast(Me.Control.FindControl("trReferenceField5"), HtmlTableRow).Style.Remove("display")
                RadInputReferenceField5.Enabled = True
                rfvReferenceField5.ErrorMessage = ""
                rfvReferenceField5.Enabled = True
                txtReferenceField5.MaxLength = objICReferencFieldsCollection(4).FieldLength
                If objICReferencFieldsCollection(4).FieldDBType = "Integer" Then
                    rfvReferenceField5.ValidationExpression = "^[0-9]*$"
                    rfvReferenceField5.ErrorMessage = "Only numbers are allowed."
                ElseIf objICReferencFieldsCollection(4).FieldDBType = "Decimal" Then
                    rfvReferenceField5.ValidationExpression = "^\d+\.\d{0,2}$"
                    rfvReferenceField5.ErrorMessage = "Only decimal values are allowed."
                ElseIf objICReferencFieldsCollection(4).FieldDBType = "String" Then
                    rfvReferenceField5.Enabled = False
                End If
                If objICCollectionNature.RefField5RequiredforInput = True Then
                    RadInputReferenceField5.GetSettingByBehaviorID("reReferenceFieldFive").Validation.IsRequired = True
                End If
            End If
            If CBool(htRights("Search")) = True Then
                btnSearch.Visible = True
            Else
                btnSearch.Visible = False
            End If
        End If
    End Sub
    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Try
            Response.Redirect(NavigateURL())
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If Page.IsValid Then
            Try

                tblReferenceField.Style.Add("display", "none")
                tblResult.Style.Remove("display")

                Dim objICCollectionInvoiceDBTemplate As New ICCollectionInvoiceDataTemplate
                objICCollectionInvoiceDBTemplate.LoadByPrimaryKey(ddlCollNatureTemplate.SelectedValue.ToString)
                Dim ReferenceField1, ReferenceField2, ReferenceField3, ReferenceField4, ReferenceField5 As String
                Dim dt As New DataTable
                If txtReferenceField1.Text <> "" Then
                    ReferenceField1 = txtReferenceField1.Text
                Else
                    ReferenceField1 = Nothing
                End If
                If txtReferenceField2.Text <> "" Then
                    ReferenceField2 = txtReferenceField2.Text
                Else
                    ReferenceField2 = Nothing
                End If
                If txtReferenceField3.Text <> "" Then
                    ReferenceField3 = txtReferenceField3.Text
                Else
                    ReferenceField3 = Nothing
                End If
                If txtReferenceField4.Text <> "" Then
                    ReferenceField4 = txtReferenceField4.Text
                Else
                    ReferenceField4 = Nothing
                End If
                If txtReferenceField5.Text <> "" Then
                    ReferenceField5 = txtReferenceField5.Text
                Else
                    ReferenceField5 = Nothing

                End If
                dt = ICCollectionSQLController.GetInvoiceRecordsByReferenceFields(objICCollectionInvoiceDBTemplate, ReferenceField1, ReferenceField2, ReferenceField3, ReferenceField4, ReferenceField5)
                If dt.Rows.Count > 0 Then
                    gvInvoiceRecords.DataSource = Nothing
                    gvInvoiceRecords.DataSource = dt
                    gvInvoiceRecords.DataBind()
                    btnUpDate.Visible = CBool(htRights("Update"))
                    lblRNF.Visible = False
                Else
                    lblRNF.Visible = True
                    gvInvoiceRecords.Visible = False
                    btnUpDate.Visible = False
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub gvInvoiceRecords_ItemCreated(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles gvInvoiceRecords.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvInvoiceRecords.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvInvoiceRecords_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles gvInvoiceRecords.ItemDataBound
        Dim chkProcessAll As CheckBox
        Dim imgBtn As LinkButton
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvInvoiceRecords.MasterTableView.ClientID & "','0');"
        End If
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim Ref1, Ref2, Ref3, Ref4, Ref5 As String
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Ref1 = ""
            Ref2 = ""
            Ref3 = ""
            Ref4 = ""
            Ref5 = ""
            SetReferenceFieldValuesByTemplateFieldsByGridRow(Ref1, Ref2, Ref3, Ref4, Ref5, item)
            imgBtn = New LinkButton
            imgBtn = DirectCast(item.Cells(1).FindControl("lbCollDetail"), LinkButton)
            imgBtn.OnClientClick = "showurldialog('Collection Details','/ShowCollectionsDetails.aspx?Ref1=" & Ref1 & "-" & Ref2 & "-" & Ref3 & "-" & Ref4 & "-" & Ref5 & "' ,true,700,650);return false;"
        End If
    End Sub
    Private Sub SetReferenceFieldValuesByTemplateFieldsByGridRow(ByRef Ref1 As String, ByRef Ref2 As String, ByRef Ref3 As String, ByRef Ref4 As String, ByRef Ref5 As String, ByVal gvRow As GridDataItem)
        Dim objICInvoiceDBTemplateFields As New ICCollectionInvoiceDataTemplateFieldsCollection

        objICInvoiceDBTemplateFields = ICCollectionInvoiceDataTemplateFieldsListController.GetReferenceFieldsCollectionInvoiceTemplateOfDualFile(gvRow.Item("TemplateID").Text.ToString)
        For i = 0 To objICInvoiceDBTemplateFields.Count - 1
            Select Case i.ToString
                Case "0"

                    Ref1 = gvRow.Item(objICInvoiceDBTemplateFields(i).FieldName.ToString).Text.ToString

                Case "1"

                    Ref2 = gvRow.Item(objICInvoiceDBTemplateFields(i).FieldName.ToString).Text.ToString

                Case "2"

                    Ref3 = gvRow.Item(objICInvoiceDBTemplateFields(i).FieldName.ToString).Text.ToString

                Case "3"

                    Ref4 = gvRow.Item(objICInvoiceDBTemplateFields(i).FieldName.ToString).Text.ToString

                Case "4"

                    Ref5 = gvRow.Item(objICInvoiceDBTemplateFields(i).FieldName.ToString).Text.ToString

            End Select
        Next
    End Sub
    Protected Sub btnBack2_Click(sender As Object, e As EventArgs) Handles btnBack2.Click
        Try
            Response.Redirect(NavigateURL())
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckgvBankBranchForProcessAll() As Boolean
        Try
            Dim rowgvBankBranch As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvBankBranch In gvInvoiceRecords.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvBankBranch.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function CheckValidStatus(ByVal ToStatus As String) As String
        Try
            Dim rowgvBankBranch As GridDataItem
            Dim RowCount As Integer = 0
            Dim Result As Boolean = False
            Dim Msg As String = ""
            For Each rowgvBankBranch In gvInvoiceRecords.Items
                RowCount = RowCount + 1
                If rowgvBankBranch.GetDataKeyValue("Status") = ToStatus Then

                    Msg += "From and To Satus are same in row " & RowCount & "<br />"
                    Return Msg
                    Exit Function
                End If
            Next
            If Msg = "" Then
                Msg = "OK"
            End If
            Return Msg
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnUpDate_Click(sender As Object, e As EventArgs) Handles btnUpDate.Click
        If Page.IsValid Then
            Try
                Dim CurrentStatus As String = Nothing
                Dim RequiredStatus As String = Nothing
                Dim chkSelect As CheckBox
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0
                If CheckgvBankBranchForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Invoice DB Queue", "Please select atleast one(1) invoice", ICBO.IC.Dialogmessagetype.Warning)
                    Exit Sub
                End If
                If CheckValidStatus(ddlAction.SelectedValue.ToString) <> "OK" Then
                    UIUtilities.ShowDialog(Me, "Invoice DB Queue", CheckValidStatus(ddlAction.SelectedValue.ToString).ToString, ICBO.IC.Dialogmessagetype.Warning)
                    Exit Sub
                End If
                ''Set ReferenceFieldValue

                Dim ReferenceField1, ReferenceField2, ReferenceField3, ReferenceField4, ReferenceField5 As String
                Dim dt As New DataTable
                If txtReferenceField1.Text <> "" Then
                    ReferenceField1 = txtReferenceField1.Text
                Else
                    ReferenceField1 = Nothing
                End If
                If txtReferenceField2.Text <> "" Then
                    ReferenceField2 = txtReferenceField2.Text
                Else
                    ReferenceField2 = Nothing
                End If
                If txtReferenceField3.Text <> "" Then
                    ReferenceField3 = txtReferenceField3.Text
                Else
                    ReferenceField3 = Nothing
                End If
                If txtReferenceField4.Text <> "" Then
                    ReferenceField4 = txtReferenceField4.Text
                Else
                    ReferenceField4 = Nothing
                End If
                If txtReferenceField5.Text <> "" Then
                    ReferenceField5 = txtReferenceField5.Text
                Else
                    ReferenceField5 = Nothing
                End If

                For Each gvInvoiceRecordsRow As GridDataItem In gvInvoiceRecords.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvInvoiceRecordsRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        Try

                            If ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(gvInvoiceRecordsRow.GetDataKeyValue("TemplateID").ToString, ReferenceField1, ReferenceField2, ReferenceField3, ReferenceField4, ReferenceField5, gvInvoiceRecordsRow.GetDataKeyValue("Status").ToString, ddlAction.SelectedValue.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString) Then
                                PassCount = PassCount + 1
                                Continue For
                            Else
                                FailCount = FailCount + 1
                                Continue For
                            End If
                        Catch ex As Exception
                            FailCount = FailCount + 1
                            Continue For
                        End Try
                    End If
                Next

                If Not PassCount = 0 And Not FailCount = 0 Then

                    UIUtilities.ShowDialog(Me, "Invoice DB Queue", "[ " & PassCount.ToString & " ] Invoice status updated successfully.<br /> [ " & FailCount.ToString & " ] Invoice status can not be updated", ICBO.IC.Dialogmessagetype.Success, NavigateURL(185))
                    Exit Sub
                ElseIf Not PassCount = 0 And FailCount = 0 Then

                    UIUtilities.ShowDialog(Me, "Invoice DB Queue", PassCount.ToString & " Invoice status updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL(185))
                    Exit Sub
                ElseIf Not PassCount = 0 And FailCount = 0 Then

                    UIUtilities.ShowDialog(Me, "Invoice DB Queue", "[ " & PassCount.ToString & " ] Invoice status can not be updated", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(185))
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

   
End Class
