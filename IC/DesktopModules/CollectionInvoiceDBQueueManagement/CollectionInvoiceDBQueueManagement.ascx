﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CollectionInvoiceDBQueueManagement.ascx.vb"
    Inherits="DesktopModules_CollectionInvoiceDBQueueManagement_CollectionInvoiceDBQueueManagement" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }


    function ValidateChkList(source, arguments) {

        arguments.IsValid = IsCheckBoxChecked() ? true : false;



    }


    $(document).ready(function () {

        // Dialog
        $('#urldialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#loading').hide();



            },
            resizable: false
        });
       


    });
    function AdjustWidth(ddl) {
        var maxWidth = 0;
        var Counter = 0;
        for (var i = 0; i < ddl.length; i++) {
            if (ddl.options[i].text.length > maxWidth) {
                Counter = Counter + 1;
                maxWidth = ddl.options[i].text.length;
            }
        }
        if (Counter = 1) { ddl.style.width = "250px"; }
        
        else {
            alert(Counter);
            ddl.style.width = maxWidth + "px";
        }
        //-->
    }
    function showurldialog(title, url, modal, width, height) {
        //  alert(modal);
        $('#urldialog').dialog("option", "title", title);
        $('#urldialog').dialog("option", "modal", modal);
        $('#urldialog').dialog("option", "width", width);
        $('#urldialog').dialog("option", "height", height);
        $('#urldialog').dialog('open');
        $('#dialogiframe').hide();
        $('#loading').show();
        $('#dialogiframe').attr("src", url);
        $('#dialogiframe').load(function () {
            $('#loading').hide();
            $('#dialogiframe').show();
        });
        return false;

    };

    function CloseIframe() {
        $('#urldialog').dialog('close');
    }
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
            }
        }
    }
</script>

<style type="text/css">
.ctrDropDown{
    width:250px;
   
}
.ctrDropDownClick{
    
    width:600px;
 
}
.plainDropDown{
    width:250px;
   
}
    .auto-style2 {
        width: 25%;
        height: 26px;
    }
</style>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:TextBoxSetting BehaviorID="reName" Validation-IsRequired="true" ErrorMessage="Invalid Account Title"
        EmptyMessage="Enter Account Title">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior3" Validation-IsRequired="true"
        ErrorMessage="Invalid Account No." EmptyMessage="Enter Account Number">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountNumber" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehaviorCurrency" Validation-IsRequired="true"
        ErrorMessage="Invalid Currency" EmptyMessage="Enter Currency">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountCurrency" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior2" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9 .-_/]{2,20}$" ErrorMessage="Invalid Area Code"
        EmptyMessage="Enter Area Code">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBranchCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
             <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue">Invoice DB Queue Management</asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
          <table id="tblMain" runat="server" style="width:100%;">
         
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label8" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 28%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 22%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlGroup" runat="server" ControlToValidate="ddlGroup"
                Display="Dynamic" ErrorMessage="Please select Group" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 33%">
            <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" ControlToValidate="ddlCompany"
                Display="Dynamic" ErrorMessage="Please select Company" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 17%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCollNature" runat="server" Text="Select Collection Nature" CssClass="lbl"></asp:Label><asp:Label
                ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCollectionNatueTemplate" runat="server" Text="Select Invoice DB Template" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqCollNatureTemp" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="auto-style2">
            <%--<asp:DropDownList ID="ddlAccount" runat="server" CssClass="dropdown" >
            </asp:DropDownList>--%>
       <asp:DropDownList ID="ddlCollNature" runat="server" class="dropdown" AutoPostBack="true">
            </asp:DropDownList>
            <%--<asp:DropDownList ID="ddlCollNature" runat="server" class="ctrDropDown"  onblur="this.className='ctrDropDown';" onmousedown="this.className='ctrDropDownClick';" onchange="this.className='ctrDropDown';" AutoPostBack="true">
            </asp:DropDownList>--%>
           
            <br />
        </td>
        <td align="left" valign="top" class="auto-style2">
        </td>
        <td align="left" valign="top" class="auto-style2">
         <asp:DropDownList ID="ddlCollNatureTemplate" runat="server" class="dropdown" AutoPostBack="true">
            </asp:DropDownList>
            <%--<asp:DropDownList ID="ddlCollNatureTemplate" runat="server" class="ctrDropDown"  onblur="this.className='ctrDropDown';" onmousedown="this.className='ctrDropDownClick';" onchange="this.className='ctrDropDown';" AutoPostBack="true">
            </asp:DropDownList>--%>
           
            </td>
        <td align="left" valign="top" class="auto-style2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlCollNature" runat="server" ControlToValidate="ddlCollNature"
                Display="Dynamic" ErrorMessage="Please select Collection Nature" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlCollNatureTemp" runat="server" ControlToValidate="ddlCollNatureTemplate"
                Display="Dynamic" ErrorMessage="Please select Invoice DB Template" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;&nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" CausesValidation="False"
                Width="75px" />
            &nbsp;
        </td>
    </tr>
          </table></td>
    </tr>
      <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
           <table id="tblReferenceField" runat="server" style="width:100%;">
               <tr  align="left">
                   <td align="left" style="width:25%;">
            <asp:Label ID="lblPageHeader0" runat="server" CssClass="headingblue">Reference Field Detail</asp:Label>
                   </td>
                   <td align="left" style="width:25%;"></td>
                   <td align="left" style="width:25%;"></td>
                   <td align="left" style="width:25%;"></td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr id="trReferenceField1" runat="server" align="left">
                   <td align="center" style="width:25%;">
            <asp:Label ID="lblReferenceField1" runat="server" CssClass="lbl"></asp:Label></td>
                   <td align="left" style="width:25%;">
            <asp:TextBox ID="txtReferenceField1" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
                   </td>
                   <td align="left" style="width:25%;">
                       <asp:RegularExpressionValidator ID="rfvReferenceField1" runat="server" Display="Dynamic" SetFocusOnError="True" ControlToValidate="txtReferenceField1" ValidationGroup="Search"></asp:RegularExpressionValidator>
                   </td>
                   <td align="left" style="width:25%;">
<telerik:RadInputManager ID="RadInputReferenceField1" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="reReferenceFieldOne" Validation-IsRequired="false" ErrorMessage="Invalid Reference Field One" Validation-ValidationGroup="Search"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceField1" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
                   </td>
               </tr>
               <tr id="trReferenceField2" runat="server" align="left">
                   <td align="center"  style="width:25%;">
            <asp:Label ID="lblReferenceField2" runat="server" CssClass="lbl"></asp:Label></td>
                   <td align="left" style="width:25%;">
            <asp:TextBox ID="txtReferenceField2" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
                   </td>
                   <td align="left" style="width:25%;">
                       <asp:RegularExpressionValidator ID="rfvReferenceField2" runat="server" Display="Dynamic" SetFocusOnError="True" ControlToValidate="txtReferenceField2" ValidationGroup="Search"></asp:RegularExpressionValidator>
                   </td>
                   <td align="left" style="width:25%;">
<telerik:RadInputManager ID="RadInputReferenceField2" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="reReferenceFieldTwo" Validation-IsRequired="false" ErrorMessage="Invalid Reference Field Two" Validation-ValidationGroup="Search"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceField2" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
                   </td>
               </tr>
               <tr id="trReferenceField3" runat="server" align="left">
                   <td align="center"  style="width:25%;">
            <asp:Label ID="lblReferenceField3" runat="server" CssClass="lbl"></asp:Label></td>
                   <td align="left" style="width:25%;">
            <asp:TextBox ID="txtReferenceField3" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
                   </td>
                   <td align="left" style="width:25%;">
                       <asp:RegularExpressionValidator ID="rfvReferenceField3" runat="server" Display="Dynamic" SetFocusOnError="True" ControlToValidate="txtReferenceField3" ValidationGroup="Search"></asp:RegularExpressionValidator>
                   </td>
                   <td align="left" style="width:25%;">
<telerik:RadInputManager ID="RadInputReferenceField3" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="reReferenceFieldThree" Validation-IsRequired="false" ErrorMessage="Invalid Reference Field Three" Validation-ValidationGroup="Search"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceField3" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
                   </td>
               </tr>
               <tr id="trReferenceField4" runat="server" align="left">
                   <td align="center" style="width:25%;">
            <asp:Label ID="lblReferenceField4" runat="server" CssClass="lbl"></asp:Label></td>
                   <td align="left" style="width:25%;">
            <asp:TextBox ID="txtReferenceField4" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
                   </td>
                   <td align="left" style="width:25%;">
                       <asp:RegularExpressionValidator ID="rfvReferenceField4" runat="server" Display="Dynamic" SetFocusOnError="True" ControlToValidate="txtReferenceField4" ValidationGroup="Search"></asp:RegularExpressionValidator>
                   </td>
                   <td align="left" style="width:25%;">
<telerik:RadInputManager ID="RadInputReferenceField4" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="reReferenceFieldFour" Validation-IsRequired="false" ErrorMessage="Invalid Reference Field Four" Validation-ValidationGroup="Search"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceField4" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
                   </td>
               </tr>
               <tr  id="trReferenceField5" runat="server" align="left">
                   <td align="center"  style="width:25%;">
            <asp:Label ID="lblReferenceField5" runat="server" CssClass="lbl"></asp:Label></td>
                   <td align="left" style="width:25%;">
            <asp:TextBox ID="txtReferenceField5" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
                   </td>
                   <td align="left" style="width:25%;">
                       <asp:RegularExpressionValidator ID="rfvReferenceField5" runat="server" Display="Dynamic" SetFocusOnError="True" ControlToValidate="txtReferenceField5" ValidationGroup="Search"></asp:RegularExpressionValidator>
                   </td>
                   <td align="left" style="width:25%;">
<telerik:RadInputManager ID="RadInputReferenceField5" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="reReferenceFieldFive" Validation-IsRequired="false" ErrorMessage="Invalid Reference Field Five" Validation-ValidationGroup="Search"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceField" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
                   </td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr  align="left">
                   <td align="center" colspan="4"><asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnCancel"
                Width="75px" ValidationGroup="Search" />
                       <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btnCancel" CausesValidation="False"
                Width="75px" />
                   </td>
               </tr>
           </table>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
           <table id="tblResult" runat="server" style="width:100%;">
               <tr  align="left">
                   <td align="left" style="width:25%;">
            <asp:Label ID="lblInvoiceDetails" runat="server" CssClass="headingblue">Invoice Details</asp:Label>
                   </td>
                   <td align="left" style="width:25%;"></td>
                   <td align="left" style="width:25%;"></td>
                   <td align="left" style="width:25%;"></td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">
                       &nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">
                        <asp:Label ID="lblRNF" runat="server" Font-Bold="False" Text="No Record Found" Visible="False"
                            CssClass="headingblue"></asp:Label>
                    </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">
            <asp:Label ID="lblAction" runat="server" CssClass="headingblue">Invoice Status</asp:Label>
                       <asp:Label
                ID="Label9" runat="server" Text="*" ForeColor="Red"></asp:Label>
                   </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">
            <asp:DropDownList ID="ddlAction" runat="server" CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem>Active</asp:ListItem>
                <asp:ListItem>In Active</asp:ListItem>
                <asp:ListItem>Paid</asp:ListItem>
                <asp:ListItem>Un Paid</asp:ListItem>
            </asp:DropDownList>
                   </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">
            <asp:RequiredFieldValidator ID="rfvddlAction" runat="server" ControlToValidate="ddlAction"
                Display="Dynamic" ErrorMessage="Please select Status" InitialValue="0" SetFocusOnError="True" ValidationGroup="Update"></asp:RequiredFieldValidator>
                   </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr id="tr1" runat="server" align="left">
                   <td align="left" colspan="4">
                        <telerik:RadGrid ID="gvInvoiceRecords" runat="server" AllowPaging="false" AllowSorting="false"
                            AutoGenerateColumns="true" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                            <ClientSettings>
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView TableLayout="Fixed" DataKeyNames="Status,TemplateID">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                     <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="3%">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Checked="false" Text=""
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Checked="False" Text="" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridTemplateColumn>

                                     <telerik:GridTemplateColumn HeaderText="Detail">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbCollDetail" runat="server" Text="Detail"
                                                ToolTip="Show Detail" ForeColor="Blue" Enabled="false"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridTemplateColumn>

                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">
                       &nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr  align="left">
                   <td align="center" colspan="4"><asp:Button ID="btnUpDate" runat="server" Text="Update" CssClass="btnCancel"
                Width="75px" ValidationGroup="Update" />
                       <asp:Button ID="btnBack2" runat="server" Text="Back" CssClass="btnCancel" CausesValidation="False"
                Width="75px" />
                   </td>
               </tr>
           </table>
        </td>
    </tr>
</table>
<div id="urldialog" title="">
    <iframe id='dialogiframe' frameborder="0" allowtransparency="true" src="" style="border-style: none;
        width: 100%; height: 100%; overflow-x: hidden; background-color: transparent;">
    </iframe>
    <div id='loading' style="display: none; margin: 0px auto; text-align: center; width: 100%;
        height: 100%; position: relative">
        <img src='../../../images/progressbar.gif' style="left: 45%; top: 50%; position: absolute;" />
    </div>
</div>
