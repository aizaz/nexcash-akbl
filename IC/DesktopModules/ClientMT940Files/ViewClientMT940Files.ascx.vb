﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports Raptorious.SharpMt940Lib
Partial Class DesktopModules_ClientMT940Files_ViewClientMT940Files
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase 
    Private ArrRights As ArrayList
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_DesktopModules_ClientMT940Files_ViewClientMT940Files_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlCompany()

                LoadGVClientMT940Files(True)
                gvSOA.DataSource = Nothing
                gvSOA.DataBind()
                pnlGVSOA.Style.Add("display", "none")
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
#End Region
#Region "Grid View Events"

    Protected Sub gvAccountsDetails_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvMT940Files.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                ' Dim arrPageSizes() As String = {"1", "2", "5", "10", "50"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvMT940Files.MasterTableView.ClientID)
                Next
                'If gvAccountsDetails.Items.Count > 0 Then
                '    myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
                'End If
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

#Region "Other Functions/Routines"
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client MT940 Statements")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Private Sub LoadGVClientMT940Files(ByVal DoDataBind As Boolean)
        Try

            Dim GroupCode, CompanyCode As String
            GroupCode = "0"
            CompanyCode = "0"

            If ddlGroup.SelectedValue.ToString <> "0" Then
                GroupCode = ddlGroup.SelectedValue.ToString
            End If
            If ddlcompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlcompany.SelectedValue.ToString
            End If

            GetAllClientMT940Files(Me.gvMT940Files.CurrentPageIndex + 1, Me.gvMT940Files.PageSize, DoDataBind, Me.gvMT940Files, GroupCode, CompanyCode)
            If gvMT940Files.Items.Count > 0 Then
                gvMT940Files.Visible = True
                lblRNF.Visible = False
                'btnDeleteAccounts.Visible = CBool(htRights("Delete"))

                gvMT940Files.Columns(4).Visible = CBool(htRights("View MT940 Statement"))
                'gvMT940Files.Columns(5).Visible = CBool(htRights("Export MT940 Statement"))
            Else
                gvMT940Files.Visible = False
                'btnDeleteAccounts.Visible = False
                lblRNF.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
    Public Shared Sub GetAllClientMT940Files(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid, ByVal GroupCode As String, ByVal CompanyCode As String)
        Dim qryObjICFiles As New ICFilesQuery("qryObjICFiles")
        Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
        Dim dt As New DataTable
        qryObjICFiles.Select(qryObjICFiles.FileID, qryObjICFiles.FileName, qryObjICFiles.AccountNumber, qryObjICCompany.CompanyName, qryObjICFiles.OriginalFileName)
        qryObjICFiles.Select(qryObjICFiles.CreatedDate)
        qryObjICFiles.LeftJoin(qryObjICCompany).On(qryObjICFiles.CompanyCode = qryObjICCompany.CompanyCode)
        qryObjICFiles.Where(qryObjICFiles.FileType = "MT940 File")
        If GroupCode <> "0" Then
            qryObjICFiles.Where(qryObjICCompany.GroupCode = GroupCode)
        End If
        If CompanyCode <> "0" Then
            qryObjICFiles.Where(qryObjICCompany.CompanyCode = CompanyCode)
        End If
        qryObjICFiles.OrderBy(qryObjICFiles.FileID.Descending)
        dt = qryObjICFiles.LoadDataTable
        If Not PageNumber = 0 Then
            qryObjICFiles.es.PageNumber = PageNumber
            qryObjICFiles.es.PageSize = PageSize
            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        Else
            qryObjICFiles.es.PageNumber = 1
            qryObjICFiles.es.PageSize = PageSize
            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        End If
    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            Dim objICuser As New ICUser

            objICuser.LoadByPrimaryKey(Me.UserInfo.UserID.ToString)

            lit.Value = "0"
            lit.Text = "-- All --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.Enabled = True
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataBind()
            If objICuser.UserType = "Client User" Then
                If ddlGroup.Items.Count > 0 Then
                    ddlGroup.SelectedValue = objICuser.UpToICOfficeByOfficeCode.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupCode
                    ddlGroup.Enabled = False
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlcompany.Enabled = True
            ddlcompany.Items.Clear()
            ddlcompany.Items.Add(lit)
            ddlcompany.AppendDataBoundItems = True
            ddlcompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString)
            ddlcompany.DataValueField = "CompanyCode"
            ddlcompany.DataTextField = "CompanyName"
            ddlcompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
#End Region
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
            LoadGVClientMT940Files(True)
            pnlGVSOA.Style.Add("display", "none")
            gvSOA.DataSource = Nothing
            gvSOA.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAccountsDetails_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvMT940Files.NeedDataSource
        Try
            LoadGVClientMT940Files(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAccountsDetails_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvMT940Files.PageIndexChanged
        Try
            gvMT940Files.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlcompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcompany.SelectedIndexChanged
        Try
            LoadGVClientMT940Files(True)
            pnlGVSOA.Style.Add("display", "none")
            gvSOA.DataSource = Nothing
            gvSOA.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAccountsDetails_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvMT940Files.ItemCommand
        Try
            Dim objICFiles As New ICFiles
            Dim dt As New DataTable
            Dim objCustomerStatementMsgColl As New List(Of CustomerStatementMessage)
            Dim format As New Mt940Format.AbnAmro
            Dim SrNo As Integer = 0
            Dim OpeningBalance As Double = 0
            Dim RunningBalance As Double = 0
            Dim i As Integer = 0
            Dim dr As DataRow
            If e.CommandName.ToString = "View" Then
                If objICFiles.LoadByPrimaryKey(e.CommandArgument.ToString) Then
                    dt = DesignDataTable().Clone
                    Dim reader As New System.IO.StreamReader(objICFiles.FileLocation)

                    'Dim reader As New System.IO.StreamReader("F:\1003201502011924106SWIFT3.txt")
                    Try
                        objCustomerStatementMsgColl = Mt940Parser.Parse(format, reader)
                        reader.Close()
                        reader.Dispose()
                    Catch ex As Exception

                        reader.Close()
                        reader.Dispose()
                    End Try
                    For Each StatementMessage As CustomerStatementMessage In objCustomerStatementMsgColl
                        If i = 0 Then
                            dr = dt.NewRow
                            OpeningBalance = StatementMessage.OpeningBalance.Balance.Value
                            RunningBalance = OpeningBalance
                            SrNo = SrNo + 1
                            dr("SerialNo") = SrNo
                            If StatementMessage.OpeningBalance.DebitCredit = DebitCredit.Credit Then
                                dr("RunningBalance") = RunningBalance & " " & " CR"
                            ElseIf StatementMessage.OpeningBalance.DebitCredit = DebitCredit.Debit Then
                                dr("RunningBalance") = RunningBalance & " " & " DR"
                            End If
                             dr("TransactionDate") =CDate(StatementMessage.OpeningBalance.EntryDate.Date).ToString("ddMMMyyyy")
                            dr("TransactionDetails") = "Opening Balance"
                            dt.Rows.Add(dr)
                            i = 1
                        End If
                        For Each StatementTransaction As Transaction In StatementMessage.Transactions
                            dr = dt.NewRow
                            SrNo = SrNo + 1
                            dr("SerialNo") = SrNo
                            dr("TransactionDate") = CDate(StatementTransaction.EntryDate).ToString("ddMMMyyyy")
                            dr("ValueDate") = CDate(StatementTransaction.ValueDate).ToString("ddMMMyyyy")
                            dr("TransactionType") = StatementTransaction.TransactionType
                            dr("TransactionDetails") = StatementTransaction.Description
                            If StatementTransaction.DebitCredit.ToString = "Credit" Then
                                dr("Withdrawl") = CDbl("0.00")
                                dr("Deposit") = CDbl(StatementTransaction.Amount.Value).ToString("N2")
                                RunningBalance = CDbl(RunningBalance + CDbl(StatementTransaction.Amount.Value)).ToString("N2")
                                If RunningBalance < 0 Then
                                    dr("RunningBalance") = RunningBalance.ToString.Replace("-", "") & " DR"
                                Else
                                    dr("RunningBalance") = RunningBalance & " CR"
                                End If
                            ElseIf StatementTransaction.DebitCredit.ToString = "Debit" Then
                                dr("Withdrawl") = CDbl(StatementTransaction.Amount.Value)
                                dr("Deposit") = CDbl("0.00")
                                RunningBalance = CDbl(RunningBalance - CDbl(StatementTransaction.Amount.Value)).ToString("N2")
                                If RunningBalance < 0 Then
                                    dr("RunningBalance") = RunningBalance.ToString.Replace("-", "") & " DR"
                                Else
                                    dr("RunningBalance") = RunningBalance & " CR"
                                End If
                            End If
                            dt.Rows.Add(dr)
                        Next
                    Next
                    pnlGVSOA.Style.Remove("display")
                    gvSOA.DataSource = dt
                    gvSOA.DataBind()
                    gvMT940Files.DataSource = Nothing
                    gvMT940Files.DataBind()
                    gvMT940Files.Visible = False
                End If
            End If
        Catch ex As Exception

            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Error ", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                LoadGVClientMT940Files(True)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function DesignDataTable() As DataTable
        Dim dtAccountStatement As New DataTable
        dtAccountStatement.Columns.Add(New DataColumn("SerialNo", GetType(System.String)))
        dtAccountStatement.Columns.Add(New DataColumn("TransactionDate", GetType(System.DateTime)))
        dtAccountStatement.Columns.Add(New DataColumn("ValueDate", GetType(System.DateTime)))
        dtAccountStatement.Columns.Add(New DataColumn("TransactionType", GetType(System.String)))
        dtAccountStatement.Columns.Add(New DataColumn("TransactionDetails", GetType(System.String)))
        dtAccountStatement.Columns.Add(New DataColumn("Withdrawl", GetType(System.Double)))
        dtAccountStatement.Columns.Add(New DataColumn("Deposit", GetType(System.Double)))
        dtAccountStatement.Columns.Add(New DataColumn("RunningBalance", GetType(System.String)))
        dtAccountStatement.Columns.Add(New DataColumn("BranchSwiftCode", GetType(System.String)))
        dtAccountStatement.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
        Return dtAccountStatement
    End Function
    
    Private Function CheckGVCompanySelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvCompanyRow As GridDataItem In gvMT940Files.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvCompanyRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub gvAccountsDetails_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvMT940Files.ItemDataBound
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim ibtnProcess As New ImageButton
            Dim AppPath As String = "" 'DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
            Dim hlDownload As New HyperLink

            hlDownload = DirectCast(e.Item.Cells(4).FindControl("hlDownload"), HyperLink)
            hlDownload.NavigateUrl = "/DownloadFile.aspx?fileid=" & e.Item.Cells(2).Text.ToString().EncryptString()
        End If
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        If Page.IsValid Then
            Try
                Response.Redirect(NavigateURL(41))
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    
End Class
