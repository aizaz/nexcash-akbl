﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewClientMT940Files.ascx.vb"
    Inherits="DesktopModules_ClientMT940Files_ViewClientMT940Files" %>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure to remove this record ?") == true) {

            return true;
        }
        else {
            return false;
        }
    }
    function delcon(item) {


        if (window.confirm("Are you sure to remove this " + item + "?") == true) {

            return true;

        }
        else {

            return false;

        }
    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to remove Record(s)?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        //      alert(grid)
        //        if ((grid.rows.length -1) != 10 ) {
        //        if ((grid.rows.length) >= 12) {
        //            //            alert('More than 10 rows')
        //            for (i = 0; i < grid.rows.length - 1; i++) {
        //                cell = grid.rows[i].cells[CellNo];
        //                for (j = 0; j < cell.childNodes.length - 1; j++) {
        //                    if (cell.childNodes[j].type == "checkbox") {
        //                        cell.childNodes[j].checked = document.getElementById(idOfControllingCheckbox).checked;
        //                    }
        //                }
        //            }
        //        }
        //        else {
        //                     alert(grid.rows.length)
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    //    }
</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table width="100%">
                <tr>
                    <td align="center" valign="top" colspan="4">
                        <asp:HiddenField ID="hfAccountCode" runat="server" Visible="true" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" colspan="4">
                        <asp:Label ID="lblMT940List" runat="server" Text="MT940 Files List" CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr style="width: 100%">
                    <td align="left" valign="middle" style="width: 25%">
                        <asp:Label ID="lblGroup" runat="server" Text="Group" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="middle" style="width: 25%">&nbsp;
                        </td>
                    <td align="left" valign="middle" style="width: 25%">
                        <asp:Label ID="lblCompany" runat="server" Text="Company" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="middle" style="width: 25%">&nbsp;
                    </td>
                </tr>
                <tr style="width: 100%">
                    <td align="left" valign="middle" style="width: 25%">
                        <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="middle" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="middle" style="width: 25%">
                        <asp:DropDownList ID="ddlcompany" runat="server" CssClass="dropdown"
                            AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="middle" style="width: 25%">&nbsp;
                    </td>
                </tr>
                <tr style="width: 100%">
                    <td align="left" valign="middle" style="width: 25%">&nbsp;</td>
                    <td align="left" valign="middle" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="middle" style="width: 25%">&nbsp;</td>
                    <td align="left" valign="middle" style="width: 25%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" colspan="4">
                        <asp:Label ID="lblRNF" runat="server" Font-Bold="False" Text="No Record Found" Visible="False"
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" colspan="4">
                        <telerik:RadGrid ID="gvMT940Files" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <ClientSettings>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <MasterTableView TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="FileID" HeaderText="File ID" SortExpression="FileID">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="OriginalFileName" HeaderText="File Name" SortExpression="OriginalFileName">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridBoundColumn DataField="AccountNumber" HeaderText="Account Number" SortExpression="AccountNumber">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>--%>
                                    <telerik:GridBoundColumn DataField="CreatedDate" HeaderText="Creation Date" SortExpression="CreatedDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="View">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ibtnProcess" runat="server" CommandName="View" CommandArgument='<%#Eval("FileID") %>' CausesValidation="false"
                                                ImageUrl="~/images/Play-Pressed-icon(1).png" ToolTip="View MT940"></asp:ImageButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Download">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlDownload" runat="server" ImageUrl="~/images/file.gif" ToolTip="Export MT940"></asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="left" colspan="4">
                        <asp:Panel ID="pnlGVSOA" runat="server" Width="100%">
                            <table style="width: 100%">
                                <tr style="width: 100%">
                                    <td>

                                        <telerik:RadGrid ID="gvSOA" runat="server" AllowPaging="false" AllowSorting="false" AutoGenerateColumns="False" CellSpacing="0" CssClass="RadGrid" PageSize="500" ShowFooter="True" Width="100%">
                                            <ClientSettings>
                                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                            </ClientSettings>
                                            <AlternatingItemStyle CssClass="rgAltRow" />
                                            <MasterTableView TableLayout="Auto" Width="100%">
                                                <CommandItemSettings ExportToPdfText="Export to PDF" />
                                                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                                </RowIndicatorColumn>
                                                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                                </ExpandCollapseColumn>
                                                <Columns>

                                                    <telerik:GridBoundColumn DataField="SerialNo" HeaderText="SerialNo" SortExpression="SerialNo">
                                                         <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" />
                                                   <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </telerik:GridBoundColumn>

                                                    <telerik:GridBoundColumn DataField="TransactionDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Post Date" HtmlEncode="false" SortExpression="TransactionDate">
                                                          <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="12%" />
                                                   <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="12%" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="ValueDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Value Date" HtmlEncode="false" SortExpression="ValueDate">
                                                          <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="12%" />
                                                   <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="12%" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </telerik:GridBoundColumn>
                                                    <%--<telerik:GridBoundColumn DataField="ValueDate" HeaderText="Value Date" SortExpression="ValueDate">
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" />
                                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" />
                                                    </telerik:GridBoundColumn>--%>
                                                   <%-- <telerik:GridBoundColumn DataField="TransactionType" HeaderText="Transaction Type" SortExpression="TransactionType">
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20%" />
                                                   <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20%" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </telerik:GridBoundColumn>--%>
                                                    <telerik:GridBoundColumn DataField="TransactionDetails" HeaderText="Details" SortExpression="TransactionDetails">
                                                         <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20%" />
                                                   <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20%" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="Withdrawl" DataFormatString="{0:N2}" HeaderText="Withdrawl" HtmlEncode="false" SortExpression="Withdrawl">
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" />
                                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="Deposit" DataFormatString="{0:N2}" HeaderText="Deposit" HtmlEncode="false" SortExpression="Deposit">
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" />
                                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn DataField="RunningBalance" DataFormatString="{0:N2}" HeaderText="Balance" HtmlEncode="false" SortExpression="RunningBalance">
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" />
                                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" />
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </telerik:GridBoundColumn>
                                                </Columns>
                                            </MasterTableView>
                                            <PagerStyle AlwaysVisible="True" />
                                            <HeaderStyle CssClass="GridHeader" />
                                            <ItemStyle CssClass="GridItem" />
                                            <AlternatingItemStyle CssClass="GridItem" />
                                            <FilterMenu EnableImageSprites="False">
                                            </FilterMenu>
                                        </telerik:RadGrid>



                                    </td>
                                </tr>

                                <tr style="width: 100%">
                                    <td align="middle">



                                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn" />



                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
