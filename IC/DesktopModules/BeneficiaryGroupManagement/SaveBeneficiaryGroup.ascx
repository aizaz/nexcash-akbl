﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveBeneficiaryGroup.ascx.vb" Inherits="DesktopModules_BeneficiaryGroupManagement_SaveBeneficiaryGroup" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .style1 {
        width: 30%;
    }
</style>
<telerik:RadInputManager ID="rad" runat="server">
    <telerik:RegExpTextBoxSetting BehaviorID="reGroupCode" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9_\-\/\\]{1,10}$" ErrorMessage="Invalid Group Code"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneGroupCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reGroupName" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9 ]{1,250}$" ErrorMessage="Invalid Group Name"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtGroupName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
<%--    <telerik:RegExpTextBoxSetting BehaviorID="reGroupTransLimit" ValidationExpression="^[0-9,.]{1,8}$"
        Validation-IsRequired="true" ErrorMessage="Invalid Group Transaction Limit" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtGroupTransLimit" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>--%>
   <%--  <telerik:NumericTextBoxSetting BehaviorID="FromAmount" Validation-IsRequired="true"
        AllowRounding="false" DecimalDigits="0" ErrorMessage="Invalid From Amount" MaxValue="9999999999999" MinValue="1"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtGroupTransLimit" />
        </TargetControls>
    </telerik:NumericTextBoxSetting>--%>

   <telerik:NumericTextBoxSetting BehaviorID="FromAmount" Validation-IsRequired="true"
        AllowRounding="false" ErrorMessage="Invalid From Amount" MinValue="1" DecimalDigits="2"
        EmptyMessage="" Type="Number">
        <TargetControls>
            <telerik:TargetInput ControlID="txtGroupTransLimit" />
        </TargetControls>
    </telerik:NumericTextBoxSetting>

</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">Note:&nbsp; Fields marked as
            <asp:Label ID="lblFieldsReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" class="style1">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblGroupReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblCompanyReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="true">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%"></td>
        <td align="left" valign="top" class="style1">
            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="true">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlGroup" runat="server"
                ControlToValidate="ddlGroup" CssClass="lblEror" Display="Dynamic"
                ErrorMessage="Please select Group" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server"
                ControlToValidate="ddlCompany" CssClass="lblEror" Display="Dynamic"
                ErrorMessage="Please select Company" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblPaymentNature" runat="server" Text="Select Payment Nature" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblPaymentNatureReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblBeneGroupCode" runat="server" Text="Group Code" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblBeneGroupCodeReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlPaymentNature" runat="server" CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:TextBox ID="txtBeneGroupCode" runat="server" CssClass="txtbox" MaxLength="20"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlPaymentNature" runat="server"
                ControlToValidate="ddlPaymentNature" CssClass="lblEror" Display="Dynamic"
                ErrorMessage="Please select Payment Nature" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGroupName" runat="server" Text="Group Name" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblGroupNameReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblGroupTransLimit" runat="server" Text="Group Transaction Limit" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblGroupTransLimitReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtGroupName" runat="server" CssClass="txtbox" MaxLength="250"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:TextBox ID="txtGroupTransLimit" runat="server" CssClass="txtbox" MaxLength="14"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
           <%-- <asp:CompareValidator ID="cmpvalGroupTransLimit" runat="server" ValueToCompare="0" ControlToValidate="txtGroupTransLimit"
                ErrorMessage="Please enter number greater than zero" Operator="GreaterThan" Type="Double" Enabled="true"></asp:CompareValidator>--%>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblActive" runat="server" Text="Is Active" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkActive" runat="server" Text=" " CssClass="chkBox" Checked="True" />
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">&nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False" />
            &nbsp;
        </td>
    </tr>
</table>
