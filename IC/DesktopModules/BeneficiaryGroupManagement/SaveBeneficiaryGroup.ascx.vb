﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals

Partial Class DesktopModules_BeneficiaryGroupManagement_SaveBeneficiaryGroup
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
    Private BeneGroupID As String

#Region "Page Load"
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            BeneGroupID = Request.QueryString("id").ToString()

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()

                If BeneGroupID.ToString() = "0" Then
                    Clear()
                    lblPageHeader.Text = "Add Beneficiary Group"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    lblPageHeader.Text = "Edit Beneficiary Group"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    txtBeneGroupCode.ReadOnly = True

                    Dim objBeneGroup As New ICBeneGroup
                    Dim objICCompany As New ICCompany
                    Dim objICGroup As New ICGroup
                    If objBeneGroup.LoadByPrimaryKey(BeneGroupID) Then
                        txtBeneGroupCode.Text = objBeneGroup.BeneGroupCode
                        txtGroupName.Text = objBeneGroup.BeneGroupName
                        txtGroupTransLimit.Text = objBeneGroup.BeneTransactionLimit
                        chkActive.Checked = objBeneGroup.IsActive
                        objICCompany.LoadByPrimaryKey(objBeneGroup.CompanyCode)
                        objICGroup.LoadByPrimaryKey(objICCompany.GroupCode)
                        LoadddlGroup()
                        ddlGroup.SelectedValue = objICGroup.GroupCode.ToString
                        LoadddlCompany()
                        ddlCompany.SelectedValue = objICCompany.CompanyCode.ToString
                        LoadPaymentNature()
                        ddlPaymentNature.SelectedValue = objBeneGroup.PaymentNatureCode
                    End If
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Beneficiary Group Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        ddlGroup.ClearSelection()
        ddlCompany.ClearSelection()
        ddlPaymentNature.ClearSelection()
        LoadddlGroup()
        LoadddlCompany()
        LoadPaymentNature()
        txtBeneGroupCode.Text = Nothing
        txtGroupName.Text = Nothing
        txtGroupTransLimit.Text = Nothing
        chkActive.Checked = True
    End Sub

#End Region

#Region "Button Events"
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                'If CheckLimitIsValid(txtGroupTransLimit) <> "OK" Then
                '    UIUtilities.ShowDialog(Me, "Save Beneficiary Group", "Invalid Beneficiary Group Limit.", ICBO.IC.Dialogmessagetype.Failure)
                '    Exit Sub
                'End If

                CheckLimitsisValid(txtGroupTransLimit.Text)

                Dim objBeneGroup As New ICBeneGroup

                objBeneGroup.BeneGroupCode = txtBeneGroupCode.Text
                objBeneGroup.BeneGroupName = txtGroupName.Text
                objBeneGroup.BeneTransactionLimit = CDbl(txtGroupTransLimit.Text)
                objBeneGroup.IsActive = chkActive.Checked
                objBeneGroup.PaymentNatureCode = ddlPaymentNature.SelectedValue.ToString()
                objBeneGroup.CompanyCode = ddlCompany.SelectedValue.ToString()

                If BeneGroupID = "0" Then
                    objBeneGroup.CreateBy = Me.UserId
                    objBeneGroup.CreateDate = Date.Now
                    objBeneGroup.Creater = Me.UserId
                    objBeneGroup.CreationDate = Date.Now
                    If CheckDuplicate(objBeneGroup) = False Then
                        ICBeneficiaryGroupManagementController.AddBeneficiaryGroup(objBeneGroup, False, Me.UserId, Me.UserInfo.Username)
                        UIUtilities.ShowDialog(Me, "Save Beneficiary Group", "Beneficiary Group added successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Else
                        UIUtilities.ShowDialog(Me, "Save Beneficiary Group", "Can not add duplicate Beneficiary Group.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else
                    objBeneGroup.BeneGroupCode = BeneGroupID
                    objBeneGroup.CreateBy = Me.UserId
                    objBeneGroup.CreateDate = Date.Now
                    If CheckDuplicate(objBeneGroup) = False Then
                        ICBeneficiaryGroupManagementController.AddBeneficiaryGroup(objBeneGroup, True, Me.UserId, Me.UserInfo.Username)
                        UIUtilities.ShowDialog(Me, "Save Beneficiary Group", "Beneficiary Group updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    Else
                        UIUtilities.ShowDialog(Me, "Save Beneficiary Group", "Can not add duplicate Beneficiary Group.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                Clear()

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub


    Private Sub CheckLimitsisValid(ByVal TextBoxForVerify As String)
        Dim result As Double
        If TextBoxForVerify.ToString() <> "" Then

            If TextBoxForVerify.Contains(".") Then
                Dim str As String()
                Dim strBDeci, strADeci As String
                Dim TotAmt As String = ""
                str = TextBoxForVerify.Split(".")
                strBDeci = str(0).ToString()
                strADeci = str(1).ToString()
                TotAmt = strBDeci & strADeci
                If TotAmt.Length > 13 Then
                    TextBoxForVerify = Nothing
                    UIUtilities.ShowDialog(Me, "Save Beneficiary Group", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If strBDeci.Length > 11 Then
                    TextBoxForVerify = Nothing
                    UIUtilities.ShowDialog(Me, "Save Beneficiary Group", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Else
                If TextBoxForVerify.Length > 11 Then
                    TextBoxForVerify = Nothing
                    UIUtilities.ShowDialog(Me, "Save Beneficiary Group", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            End If

            Try
                If Double.TryParse(TextBoxForVerify.Trim(), result) = False Then
                    TextBoxForVerify = Nothing
                    UIUtilities.ShowDialog(Me, "Save Beneficiary Group", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                TextBoxForVerify = Nothing
                UIUtilities.ShowDialog(Me, "Save Beneficiary Group", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End Try

            If CDbl(TextBoxForVerify.ToString()) < 1 Then
                UIUtilities.ShowDialog(Me, "Save Beneficiary Group", "Amount should be greater than or equal to 1.", ICBO.IC.Dialogmessagetype.Failure)
                TextBoxForVerify = ""
                Exit Sub
            End If

        End If
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    Private Function CheckDuplicate(ByVal ICBeneGroup As ICBeneGroup) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim objBeneGroup As New ICBeneGroup
            Dim collBeneGroup As New ICBeneGroupCollection

            If BeneGroupID = "0" Then
                If objBeneGroup.LoadByPrimaryKey(ICBeneGroup.BeneGroupCode) Then
                    rslt = True
                End If
                collBeneGroup.LoadAll()
                collBeneGroup.Query.Where(collBeneGroup.Query.BeneGroupName.ToLower.Trim = ICBeneGroup.BeneGroupName.ToLower.Trim)
                If collBeneGroup.Query.Load() Then
                    rslt = True
                End If
            Else
                collBeneGroup = New ICBeneGroupCollection
                collBeneGroup.Query.Where(collBeneGroup.Query.BeneGroupName.ToLower.Trim = ICBeneGroup.BeneGroupName.ToLower.Trim And collBeneGroup.Query.BeneGroupCode <> ICBeneGroup.BeneGroupCode)
                If collBeneGroup.Query.Load() Then
                    rslt = True
                Else
                    rslt = False
                End If
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

#End Region

#Region "Load Drop Down"

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICBeneficiaryGroupManagementController.GetAllActiveGroupsForBene()
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICBeneficiaryGroupManagementController.GetAllActiveCompaniesByGroupCodeForBene(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadPaymentNature()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlPaymentNature.Items.Clear()
            ddlPaymentNature.Items.Add(lit)
            ddlPaymentNature.AppendDataBoundItems = True
            ddlPaymentNature.DataSource = ICBeneficiaryGroupManagementController.GetPaymentNatureByCompanyCode(ddlCompany.SelectedValue.ToString)
            ddlPaymentNature.DataTextField = "PaymentNatureName"
            ddlPaymentNature.DataValueField = "PaymentNatureCode"
            ddlPaymentNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
            LoadPaymentNature()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadPaymentNature()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

End Class
