﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_BeneficiaryGroupManagement_ViewBeneficiaryGroup
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable

#Region "Page Load"
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnSaveBeneGroup.Visible = CBool(htRights("Add"))
                LoadBeneficiariesGroup(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Beneficiary Group Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Button Events"

    Protected Sub btnSaveBeneGroup_Click(sender As Object, e As EventArgs) Handles btnSaveBeneGroup.Click
        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveBeneGroup", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If
    End Sub

    Protected Sub btnDeleteGroupBene_Click(sender As Object, e As EventArgs) Handles btnDeleteGroupBene.Click
        If Page.IsValid Then
            Try
                Dim rowgvBeneGroup As GridDataItem
                Dim chkSelect As CheckBox
                Dim BeneGroupCode As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0

                If CheckgvBeneGroupForProcessAll() = True Then
                    For Each rowgvBeneGroup In gvBeneGroup.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvBeneGroup.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            BeneGroupCode = rowgvBeneGroup("BeneGroupCode").Text.ToString()
                            Dim objBeneGroup As New ICBeneGroup
                            If objBeneGroup.LoadByPrimaryKey(BeneGroupCode.ToString()) Then
                                Try
                                    ICBeneficiaryGroupManagementController.DeleteBeneGroup(BeneGroupCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                    PassCount = PassCount + 1
                                Catch ex As Exception
                                    If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                        FailCount = FailCount + 1
                                    End If
                                End Try
                            End If
                        End If
                    Next

                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Beneficiary Group", "[" & FailCount.ToString() & "] Beneficiary Group can not be deleted due to following reasons : <br /> 1. Beneficiary Group is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Beneficiary Group", "[" & PassCount.ToString() & "] Beneficiary Group deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Delete Beneficiary Group", "[" & PassCount.ToString() & "] Beneficiary Group deleted successfully. [" & FailCount.ToString() & "] Beneficiary Group can not be deleted due to following reasons : <br /> 1. Beneficiary Group is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Delete Beneficiary Group", "Please select atleast one(1) Beneficiary Group.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

#End Region

#Region "Grid View Events"

    Protected Sub gvBeneGroup_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvBeneGroup.ItemCommand
        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    Dim objBeneGroup As New ICBeneGroup
                    If objBeneGroup.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                        ICBeneficiaryGroupManagementController.DeleteBeneGroup(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        UIUtilities.ShowDialog(Me, "Delete Beneficiary Group", "Beneficiary Group deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                        LoadBeneficiariesGroup(True)
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Delete Beneficiary Group", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvBeneGroup_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvBeneGroup.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvBeneGroup.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvBeneGroup_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvBeneGroup.NeedDataSource
        LoadBeneficiariesGroup(False)
    End Sub

    Protected Sub gvBeneGroup_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvBeneGroup.PageIndexChanged
        gvBeneGroup.CurrentPageIndex = e.NewPageIndex

    End Sub

    Protected Sub gvBeneGroup_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvBeneGroup.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvBeneGroup.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Private Sub LoadBeneficiariesGroup(ByVal IsBind As Boolean)
        Try
            ICBeneficiaryGroupManagementController.GetgvBeneGroup(Me.gvBeneGroup.CurrentPageIndex + 1, Me.gvBeneGroup.PageSize, Me.gvBeneGroup, IsBind)
            If gvBeneGroup.Items.Count > 0 Then
                lblBeneGroupList.Visible = True
                lblBeneGroupRNF.Visible = False
                gvBeneGroup.Visible = True

                btnDeleteGroupBene.Visible = CBool(htRights("Delete"))
                gvBeneGroup.Columns(6).Visible = CBool(htRights("Delete"))
            Else
                lblBeneGroupList.Visible = True
                gvBeneGroup.Visible = False
                btnDeleteGroupBene.Visible = False
                lblBeneGroupRNF.Visible = True
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Function CheckgvBeneGroupForProcessAll() As Boolean
        Try
            Dim rowgvBeneGroup As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvBeneGroup In gvBeneGroup.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvBeneGroup.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

#End Region

End Class
