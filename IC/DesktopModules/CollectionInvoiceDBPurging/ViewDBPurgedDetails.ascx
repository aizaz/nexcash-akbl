﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewDBPurgedDetails.ascx.vb" Inherits="DesktopModules_CollectionInvoiceDBPurging_ViewDBPurgedDetails" %>
<script type="text/javascript">
    function del() {
        if (window.confirm("Are you sure to purge the database ?") == true) {

            return true;
        }
        else {
            return false;
        }
    }
</script>

<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">Note:&nbsp; Fields marked as
            <asp:Label ID="lblReqFieldsMarked" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqGroup" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 28%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqCompany" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 22%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlGroup" runat="server" ControlToValidate="ddlGroup"
                Display="Dynamic" ErrorMessage="Please select Group" InitialValue="0" ValidationGroup="InvoiceDBPurge"
                SetFocusOnError="True" ToolTip="Required Field"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 33%">
            <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" ControlToValidate="ddlCompany"
                Display="Dynamic" ErrorMessage="Please select Company" InitialValue="0" ValidationGroup="InvoiceDBPurge"
                SetFocusOnError="True" ToolTip="Required Field"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 17%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCollAccountNature" runat="server"
                Text="Select Collection Nature" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqCollAccountNature" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblInvoiceDataTemplate" runat="server" Text="Select Invoice Data Template" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqInvoiceDataTemplate" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCollNature" runat="server" CssClass="dropdown"
                AutoPostBack="True">
            </asp:DropDownList>
            <br />
        </td>
        <td align="left" valign="top" style="width: 25%"></td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlInvoiceDataTemplate" runat="server" CssClass="dropdown"
                AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlCollNature" runat="server" ControlToValidate="ddlCollNature"
                Display="Dynamic" ValidationGroup="InvoiceDBPurge"
                ErrorMessage="Please select Collection Nature"
                InitialValue="0" SetFocusOnError="True" ToolTip="Required Field"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlInvoiceDataTemplate" runat="server" ControlToValidate="ddlInvoiceDataTemplate"
                Display="Dynamic" ValidationGroup="InvoiceDBPurge"
                ErrorMessage="Please select Invoice Data Template"
                InitialValue="0" SetFocusOnError="True" ToolTip="Required Field"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <asp:Panel ID="pnlDateFields" runat="server" Visible="false">
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:Label ID="lblInvoiceTemplateDateFields" runat="server" Text="Select Invoice Template Date Fields" CssClass="lbl"></asp:Label>
                <asp:Label ID="lblReqInvoiceTemplateDateFields" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                <div runat="server" id="divlblStartDate" style="float: left; width: 50%; text-align: left; clear: none">
                    <asp:Label ID="lblStartDate" runat="server" Text="Start Date" CssClass="lbl"></asp:Label>
                    <asp:Label
                ID="lblReqStartDate" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
                <div runat="server" id="divlblEndDate" visible="true" style="float: left; width: 50%; text-align: left; clear: none">
                    <asp:Label ID="lblEndDate" runat="server" Text="End Date" CssClass="lbl"></asp:Label>
                      <asp:Label
                ID="lblReqEndDate" runat="server" Text="*" ForeColor="Red"></asp:Label>
                </div>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:DropDownList ID="ddlInvoiceTemplateDateFields" runat="server" CssClass="dropdown"
                    AutoPostBack="false">
                </asp:DropDownList>
                <br />
            </td>
            <td align="left" valign="top" style="width: 25%"></td>
            <td align="left" valign="top" style="width: 25%">
                <div runat="server" id="divStartDate" style="float: left; width: 50%; text-align: left; clear: none">
                    <telerik:RadDatePicker ID="dpStartDate" runat="server" CssClass="txtbox" ValidationGroup="InvoiceDBPurge"
                        Culture="en-gb" DateInput-DateFormat="dd-MMM-yyyy" DateInput-EmptyMessage="-- Select --" DateInput-ReadOnly="true" ImagesPath="">
                    </telerik:RadDatePicker>
                </div>

                <div runat="server" id="divEndDate" visible="true" style="float: left; width: 50%; text-align: left; clear: none">
                    <telerik:RadDatePicker ID="dpEndDate" runat="server" CssClass="txtbox" Culture="en-gb" ValidationGroup="InvoiceDBPurge"
                        DateInput-DateFormat="dd-MMM-yyyy" DateInput-EmptyMessage="-- Select --" DateInput-ReadOnly="true" ImagesPath="">
                    </telerik:RadDatePicker>
                </div>

            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:RequiredFieldValidator ID="rfvInvoiceTemplateDateFields" runat="server" ControlToValidate="ddlInvoiceTemplateDateFields"
                    Display="Dynamic" ValidationGroup="InvoiceDBPurge"
                    ErrorMessage="Please select Invoice Template Date Fields"
                    InitialValue="0" SetFocusOnError="True" ToolTip="Required Field"></asp:RequiredFieldValidator>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                <div runat="server" id="divrfvStartDate" style="float: left; width: 50%; text-align: left; clear: none">
                    <asp:RequiredFieldValidator ID="rfvdpStartDate" runat="server" ControlToValidate="dpStartDate" ValidationGroup="InvoiceDBPurge"
                        Display="Dynamic" ErrorMessage="Please select Start Date" ForeColor="Red" InitialValue="" SetFocusOnError="True" ToolTip="Required Field"></asp:RequiredFieldValidator>
                </div>
                <div runat="server" id="divrfvEndDate" visible="true" style="float: right; width: 50%; text-align: left; clear: none">
                    <asp:RequiredFieldValidator ID="rfvdpEndDate" runat="server" ControlToValidate="dpEndDate" Display="Dynamic"
                        ErrorMessage="Please select End Date" ForeColor="Red" InitialValue="" ValidationGroup="InvoiceDBPurge"
                        SetFocusOnError="True" ToolTip="Required Field"></asp:RequiredFieldValidator>
                </div>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
    </asp:Panel>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn" Width="71px" CausesValidation="true" ValidationGroup="InvoiceDBPurge" Visible="false" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" CausesValidation="False" Width="75px" />
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <telerik:RadGrid ID="gvDBPurged" runat="server" AllowPaging="False" AllowSorting="False" Width="1250px"
                AutoGenerateColumns="true" CellSpacing="0" PageSize="100" ShowFooter="True" CssClass="RadGrid">
                <ClientSettings>
                    <Scrolling UseStaticHeaders="true" AllowScroll="true" />
                </ClientSettings>
                <AlternatingItemStyle CssClass="rgAltRow" />
                <MasterTableView DataKeyNames="">
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                    </ExpandCollapseColumn>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>                 
                </MasterTableView>
                <ItemStyle CssClass="rgRow" />
                <PagerStyle AlwaysVisible="True" />
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <asp:Label ID="lblRNF" runat="server" Text="Record Not Found" CssClass="headingblue" Visible="false"></asp:Label>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnBulkPurge" runat="server" Text="Delete"
                OnClientClick="javascript: return del();" CssClass="btn" Width="71px" Visible="False" />
        </td>
    </tr>
</table>
