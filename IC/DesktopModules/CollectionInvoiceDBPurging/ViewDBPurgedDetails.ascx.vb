﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_CollectionInvoiceDBPurging_ViewDBPurgedDetails
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
    Private AssignUserRolsID As New ArrayList

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()
            If Page.IsPostBack = False Then
                If Me.UserInfo.IsSuperUser = False Then
                    ViewState("htRights") = Nothing
                    GetPageAccessRights()
                    LoadddlGroup()
                    LoadddlCompany()
                    LoadddlCollNature()
                    LoadddlInvoiceDataTemplate()
                    lblPageHeader.Text = "Invoice DB Purging"
                Else
                    UIUtilities.ShowDialog(Me, "Invoice DB Purging", "Sorry! You are not authorized to login.", Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        AssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Invoice DB Purging") = True Then
                    If Not AssignUserRolsID.Contains(RoleID.ToString) Then
                        AssignUserRolsID.Add(RoleID.ToString)
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Invoice DB Purging")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#Region "Drop Downs"

     Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICUserRolesCollectionAccountAndCollectionNatureController.GetCollectionGroupTaggedWithUserForDropDown(Me.UserId.ToString, AssignUserRolsID)
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

   Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICUserRolesCollectionAccountAndCollectionNatureController.GetCollectionCompanyTaggedWithUserForDropDownByGroupCode(Me.UserId.ToString, AssignUserRolsID, ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlCollNature()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCollNature.Items.Clear()
            ddlCollNature.Items.Add(lit)
            ddlCollNature.AppendDataBoundItems = True
            ddlCollNature.DataSource = ICUserRolesCollectionAccountAndCollectionNatureController.GetCollectionNatureTaggedWithUserForDropDownByCompanyCodeInvoiceDBAvailable(Me.UserId.ToString, AssignUserRolsID, ddlCompany.SelectedValue.ToString())
            ddlCollNature.DataTextField = "CollAccountAndNature"
            ddlCollNature.DataValueField = "Value"
            ddlCollNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlInvoiceDataTemplate()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlInvoiceDataTemplate.Items.Clear()
            ddlInvoiceDataTemplate.Items.Add(lit)
            ddlInvoiceDataTemplate.AppendDataBoundItems = True
            ddlInvoiceDataTemplate.DataSource = ICCollectionInvoiceDataTemplateController.GetTemplatesByCollectionNature(ddlCollNature.SelectedValue.ToString)
            ddlInvoiceDataTemplate.DataTextField = "TemplateName"
            ddlInvoiceDataTemplate.DataValueField = "CollectionInvoiceTemplateID"
            ddlInvoiceDataTemplate.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlInvoiceDataTemplateDateFields(ByVal IsBind As Boolean)
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            Dim collInvoiceDataTemplateFieldsCollection As New ICCollectionInvoiceDataTemplateFieldsCollection
            ddlInvoiceTemplateDateFields.Items.Clear()
            ddlInvoiceTemplateDateFields.Items.Add(lit)
            ddlInvoiceTemplateDateFields.AppendDataBoundItems = True
            collInvoiceDataTemplateFieldsCollection = ICCollectionInvoiceDataTemplateFieldsListController.GetFieldsByTemplateIDandDataType(ddlInvoiceDataTemplate.SelectedValue.ToString(), "Date")

            If collInvoiceDataTemplateFieldsCollection.Count > 0 Then
                ddlInvoiceTemplateDateFields.DataSource = collInvoiceDataTemplateFieldsCollection
                ddlInvoiceTemplateDateFields.DataTextField = "FieldName"
                ddlInvoiceTemplateDateFields.DataValueField = "FieldName"
                ddlInvoiceTemplateDateFields.DataBind()
                dpStartDate.SelectedDate = Nothing
                dpEndDate.SelectedDate = Nothing
                rfvInvoiceTemplateDateFields.Enabled = True
                rfvdpStartDate.Enabled = True
                rfvdpEndDate.Enabled = True
                pnlDateFields.Visible = True
                btnSearch.Visible = True
            Else
                pnlDateFields.Visible = False
                rfvInvoiceTemplateDateFields.Enabled = False
                rfvdpStartDate.Enabled = False
                rfvdpEndDate.Enabled = False
                btnSearch.Visible = False
                If IsBind = True Then
                    UIUtilities.ShowDialog(Me, "Invoice DB Purging", "Please tagged date field with invoice data template.", ICBO.IC.Dialogmessagetype.Warning)
                    Exit Sub
                End If
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
            LoadddlCompany()
            LoadddlCollNature()
            LoadddlInvoiceDataTemplate()
            LoadddlInvoiceDataTemplateDateFields(False)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadddlCollNature()
            LoadddlCollNature()
            LoadddlInvoiceDataTemplate()
            LoadddlInvoiceDataTemplateDateFields(False)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCollNature_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCollNature.SelectedIndexChanged
        Try
            LoadddlInvoiceDataTemplate()
            LoadddlInvoiceDataTemplate()
            LoadddlInvoiceDataTemplateDateFields(False)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlInvoiceDataTemplate_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlInvoiceDataTemplate.SelectedIndexChanged
        Try
            If ddlInvoiceDataTemplate.SelectedValue <> "0" Then
                LoadddlInvoiceDataTemplateDateFields(True)
            Else
                LoadddlInvoiceDataTemplateDateFields(False)
            End If


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Buttons"
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If Page.IsValid Then
            Try
                Dim IsPurged As Boolean = False
                If (dpStartDate.SelectedDate.HasValue And dpEndDate.SelectedDate.HasValue) Then
                    If dpEndDate.SelectedDate < dpStartDate.SelectedDate Then
                        UIUtilities.ShowDialog(Me, "Invoice DB Purging", "End Date must be greater than or equal to Start Date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                LoadgvDBPurged(True)

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnBulkPurge_Click(sender As Object, e As EventArgs) Handles btnBulkPurge.Click
        If Page.IsValid Then
            Try
                Dim objCollNature As New ICCollectionNature
                objCollNature.LoadByPrimaryKey(ddlCollNature.SelectedValue)
                If ICCollectionSQLController.DBPurged(objCollNature, dpStartDate.SelectedDate.Value, dpEndDate.SelectedDate.Value, ddlInvoiceTemplateDateFields.SelectedValue) = True Then
                    ICUtilities.AddAuditTrail(gvDBPurged.Items.Count & " Records deleted successfully.", "Invoice DB Purging", "", Me.UserId.ToString(), Me.UserInfo.Username.ToString(), "DELETE")
                    UIUtilities.ShowDialog(Me, "Invoice DB Purging", "DB successfully purged.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    ICUtilities.AddAuditTrail("Records not deleted.", "Invoice DB Purging", "", Me.UserId.ToString(), Me.UserInfo.Username.ToString(), "DELETE")
                    UIUtilities.ShowDialog(Me, "Invoice DB Purging", "DB not purged.", ICBO.IC.Dialogmessagetype.Failure)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL(41), False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

#Region "Grid View"
    Private Sub LoadgvDBPurged(ByVal IsBind As Boolean)
        Try
            Dim dt As New DataTable
            Dim objCollNature As New ICCollectionNature
            If objCollNature.LoadByPrimaryKey(ddlCollNature.SelectedValue) Then
                dt = ICCollectionSQLController.GetRecordsforDBPurged(objCollNature, dpStartDate.SelectedDate.Value, dpEndDate.SelectedDate.Value, ddlInvoiceTemplateDateFields.SelectedValue)
            End If
            If dt.Rows.Count > 0 And IsBind = True Then
                gvDBPurged.Visible = True
                gvDBPurged.DataSource = dt
                gvDBPurged.DataBind()
                lblRNF.Visible = False
                btnBulkPurge.Visible = True
            Else
                gvDBPurged.Visible = False
                If IsBind = True Then
                    lblRNF.Visible = True
                Else
                    lblRNF.Visible = False
                End If
                btnBulkPurge.Visible = False
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    'Protected Sub gvDBPurged_ItemCreated(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles gvDBPurged.ItemCreated
    '    Try
    '        If TypeOf (e.Item) Is GridPagerItem Then
    '            Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
    '            myPageSizeCombo.Items.Clear()
    '            Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
    '            For x As Integer = 0 To UBound(arrPageSizes)
    '                Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
    '                myPageSizeCombo.Items.Add(myRadComboBoxItem)
    '                myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvDBPurged.MasterTableView.ClientID)
    '            Next
    '            myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
    '        End If
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

    Protected Sub gvDBPurged_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvDBPurged.NeedDataSource
        Try
            LoadgvDBPurged(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

End Class
