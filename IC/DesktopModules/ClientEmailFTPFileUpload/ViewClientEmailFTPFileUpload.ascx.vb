﻿Imports ICBO
Imports ICBO.IC
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports Telerik.Web.UI
Imports System.Runtime.Remoting.Messaging
Imports System.Threading
Imports Microsoft.Exchange.WebServices.Data
Imports EntitySpaces.Core
Partial Class DesktopModules_AgentFile_ViewAgentFile
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrICAssignUserRolsID As New ArrayList
    Private htRights As Hashtable
    Private Delegate Sub LongRun(ByVal UsersID As String, ByVal FileID As String, ByVal AcqMode As String, ByVal dtVerified As DataTable, ByVal dtVerifiedHeader As DataTable, ByVal dtVerifiedDetail As DataTable, ByVal IsBeneAdded As Boolean, ByVal TrnxAmount As Double, ByVal InstCount As Integer)

    Protected Sub DesktopModules_AcqAgentFileList_ViewAcqAgentFileList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then

                ViewState("htRights") = Nothing
                GetPageAccessRights()
                Dim objICUser As New ICUser
                Dim objICGroup As New ICGroup
                Dim dt As New DataTable
                objICUser.es.Connection.CommandTimeout = 3600
                objICGroup.es.Connection.CommandTimeout = 3600

                If Me.UserInfo.IsSuperUser = False Then
                    If objICUser.LoadByPrimaryKey(Me.UserId) Then
                        If objICUser.UserType = "Client User" Then
                            objICGroup = objICUser.UpToICOfficeByOfficeCode.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode
                            LoadddlGroup()
                            ViewddlGroup.SelectedValue = objICGroup.GroupCode.ToString
                            ViewddlGroup.Enabled = False
                            SetArraListRoleID()
                            DesignDts()

                            LoadddlCompany()
                            LoadddlAccountPaymentNatureByUserID()
                            LoadddlFileUploadTemplate()


                            HideGvClientFiles()
                        ElseIf objICUser.UserType.ToString = "Bank User" Then
                            SetArraListRoleID()
                            DesignDts()
                            LoadddlGroup()
                            LoadddlCompany()
                            LoadddlAccountPaymentNatureByUserID()

                            LoadddlFileUploadTemplate()

                            HideGvClientFiles()
                        End If
                    End If
                Else
                    UIUtilities.ShowDialog(Me, "Error", "Sorry! You are not authorized to login.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub DesignDts()
        Dim dtAccountNumber As New DataTable
        dtAccountNumber.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("BranchCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("Currency", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("PaymentNatureCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("OfficeID", GetType(System.String)))
        ViewState("AccountNumber") = dtAccountNumber
    End Sub
    Private Sub FillDesignedDtByAPNatureByAssignedRole()
        Dim dtAssignedAPNatureOfficeID As New DataTable
        dtAssignedAPNatureOfficeID.Rows.Clear()
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserSecond(Me.UserId.ToString, ArrICAssignUserRolsID)
        dtAssignedAPNatureOfficeID.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAssignedAPNatureOfficeID.Rows
            dr("DropDown") = "False"
        Next
        ViewState("AccountNumber") = Nothing
        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID

    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "File Upload")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "File Upload") = True Then
                    If Not ArrICAssignUserRolsID.Contains(RoleID.ToString) Then
                        ArrICAssignUserRolsID.Add(RoleID.ToString)
                    End If
                End If
            Next
        End If
    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            objICUser.es.Connection.CommandTimeout = 3600
            ViewddlGroup.Enabled = True
            Dim lit As New ListItem
            lit.Text = Nothing
            lit.Value = Nothing
            If objICUser.LoadByPrimaryKey(Me.UserId) Then
                If objICUser.UserType = "Client User" Then
                    lit.Value = "0"
                    lit.Text = "-- All --"
                    ViewddlGroup.Items.Clear()
                    ViewddlGroup.Items.Add(lit)
                    ViewddlGroup.AppendDataBoundItems = True
                    ViewddlGroup.DataSource = ICGroupController.GetAllActiveAndApproveGroups
                    ViewddlGroup.DataTextField = "GroupName"
                    ViewddlGroup.DataValueField = "GroupCode"
                    ViewddlGroup.DataBind()
                ElseIf objICUser.UserType = "Bank User" Then

                    ViewddlGroup.Items.Clear()
                    dt = ICUserRolesPaymentNatureController.GetAllTaggedGroupsByUserID(objICUser.UserID.ToString, ArrICAssignUserRolsID)
                    If dt.Rows.Count = 1 Then
                        For Each dr As DataRow In dt.Rows
                            lit.Value = dr("GroupCode")
                            lit.Text = dr("GroupName")
                            ViewddlGroup.Items.Add(lit)
                            lit.Selected = True
                            ViewddlGroup.Enabled = False
                        Next
                    ElseIf dt.Rows.Count > 1 Then
                        lit.Value = "0"
                        lit.Text = "-- All --"
                        ViewddlGroup.Items.Clear()
                        ViewddlGroup.Items.Add(lit)
                        ViewddlGroup.AppendDataBoundItems = True
                        ViewddlGroup.DataSource = dt
                        ViewddlGroup.DataTextField = "GroupName"
                        ViewddlGroup.DataValueField = "GroupCode"
                        ViewddlGroup.DataBind()
                    Else

                        lit.Value = "0"
                        lit.Text = "-- All --"
                        ViewddlGroup.Items.Clear()
                        ViewddlGroup.Items.Add(lit)
                        ViewddlGroup.AppendDataBoundItems = True

                    End If
                Else

                    lit.Value = "0"
                    lit.Text = "-- All --"
                    ViewddlGroup.Items.Clear()
                    ViewddlGroup.Items.Add(lit)
                    ViewddlGroup.AppendDataBoundItems = True
                End If
            End If




        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, ViewddlGroup.SelectedValue.ToString, ArrICAssignUserRolsID)
            ViewddlCompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ViewddlCompany.Items.Clear()
                    ViewddlCompany.Items.Add(lit)
                    lit.Selected = True
                    ViewddlCompany.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- All --"
                ViewddlCompany.Items.Clear()
                ViewddlCompany.Items.Add(lit)
                ViewddlCompany.AppendDataBoundItems = True
                ViewddlCompany.DataSource = dt
                ViewddlCompany.DataTextField = "CompanyName"
                ViewddlCompany.DataValueField = "CompanyCode"
                ViewddlCompany.DataBind()
                ViewddlCompany.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- All --"
                ViewddlCompany.Items.Clear()
                ViewddlCompany.Items.Add(lit)
                ViewddlCompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccountPaymentNatureByUserID()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ViewddlAcccountPaymentNature.Items.Clear()
            ViewddlAcccountPaymentNature.Items.Add(lit)
            lit.Selected = True
            ViewddlAcccountPaymentNature.AppendDataBoundItems = True
            ViewddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUser(Me.UserId.ToString, ViewddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
            ViewddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
            ViewddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
            ViewddlAcccountPaymentNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub SetViewStateDtOnAPNatureIndexChnage()
        Dim dtAccountNumber As New DataTable
        Dim AccountNumber, BranchCode, Currency, PaymentNatureCode As String
        Dim ArrayListOfficeID As New ArrayList
        Dim drAccountNo As DataRow
        Dim collDR As DataRow()
        AccountNumber = Nothing
        BranchCode = Nothing
        Currency = Nothing
        PaymentNatureCode = Nothing
        FillDesignedDtByAPNatureByAssignedRole()
        dtAccountNumber = ViewState("AccountNumber")
        ViewState("AccountNumber") = Nothing
        AccountNumber = ViewddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0)
        BranchCode = ViewddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1)
        Currency = ViewddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2)
        PaymentNatureCode = ViewddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)
        collDR = dtAccountNumber.Select("AccountNumber='" & AccountNumber & "-" & BranchCode & "-" & Currency & "' AND PaymentNature='" & PaymentNatureCode & "'")
        For Each dr In collDR

            ArrayListOfficeID.Add(dr("OfficeID").ToString)

        Next
        dtAccountNumber.Rows.Clear()
        If ArrayListOfficeID.Count > 0 Then
            For Each OfficeID In ArrayListOfficeID
                drAccountNo = dtAccountNumber.NewRow()
                drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
                drAccountNo("PaymentNature") = PaymentNatureCode
                drAccountNo("OfficeID") = OfficeID
                dtAccountNumber.Rows.Add(drAccountNo)
            Next
        Else
            drAccountNo = dtAccountNumber.NewRow()
            drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
            drAccountNo("PaymentNature") = PaymentNatureCode
            drAccountNo("OfficeID") = "0"
            dtAccountNumber.Rows.Add(drAccountNo)
        End If

        For Each dr As DataRow In dtAccountNumber.Rows
            dr("DropDown") = "True"
        Next
        ViewState("AccountNumber") = dtAccountNumber

    End Sub

    Private Sub LoadddlFileUploadTemplate()
        Try
            Dim objICAPNature As New ICAccountsPaymentNature
            Dim StrArray As String() = Nothing
            objICAPNature.es.Connection.CommandTimeout = 3600
            Dim lit As ListItem
            Dim dt As New DataTable
            ViewddlFileUploadTemplate.Enabled = True
            If ViewddlAcccountPaymentNature.SelectedValue.ToString <> "0" Then
                StrArray = ViewddlAcccountPaymentNature.SelectedValue.ToString.Split("-")
                objICAPNature.LoadByPrimaryKey(StrArray(0).ToString, StrArray(1).ToString, StrArray(2), StrArray(3).ToString)

                dt = ICAccountPaymentNatureFileUploadTemplateController.GetTemplatesByAccountPaymentNature(objICAPNature)
                If dt.Rows.Count = 1 Then
                    For Each dr As DataRow In dt.Rows
                        lit = New ListItem
                        ViewddlFileUploadTemplate.Items.Clear()
                        lit.Value = dr("TemplateID")
                        lit.Text = dr("TemplateName")
                        ViewddlFileUploadTemplate.Items.Add(lit)
                        lit.Selected = True
                        ViewddlFileUploadTemplate.Enabled = False
                    Next
                ElseIf dt.Rows.Count > 0 Then
                    lit = New ListItem
                    lit.Value = "0"
                    lit.Text = "-- All --"

                    ViewddlFileUploadTemplate.Items.Clear()
                    ViewddlFileUploadTemplate.Items.Add(lit)
                    ViewddlFileUploadTemplate.AppendDataBoundItems = True
                    ViewddlFileUploadTemplate.DataSource = dt
                    ViewddlFileUploadTemplate.DataTextField = "TemplateName"
                    ViewddlFileUploadTemplate.DataValueField = "TemplateID"
                    ViewddlFileUploadTemplate.DataBind()
                Else
                    lit = New ListItem
                    lit.Value = "0"
                    lit.Text = "-- All --"
                    ViewddlFileUploadTemplate.Items.Clear()
                    ViewddlFileUploadTemplate.Items.Add(lit)
                End If
            Else
                lit = New ListItem
                lit.Value = "0"
                lit.Text = "-- All --"
                ViewddlFileUploadTemplate.Items.Clear()
                ViewddlFileUploadTemplate.Items.Add(lit)
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadgvClientFiles(ByVal IsBind As Boolean)
        Try

            Dim GroupCode, CompannyCode, TemplateID As String
            GroupCode = ViewddlGroup.SelectedValue.ToString()
            CompannyCode = ViewddlCompany.SelectedValue.ToString()
            Dim dtAccountNumber As New DataTable

            If ViewState("AccountNumber") Is Nothing Then
                'dtAccountNumber = 0
            Else
                dtAccountNumber = ViewState("AccountNumber")
            End If

            If dtAccountNumber.Rows.Count = 0 Then
                UIUtilities.ShowDialog(Me, "Error", "You do not have access to any transactional data. Please contact Al Baraka Administrator.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                Exit Sub
            End If

            TemplateID = ViewddlFileUploadTemplate.SelectedValue.ToString()

            If ViewddlAcccountPaymentNature.SelectedValue.ToString <> "0" Then
                ICFilesController.GetClientEmailFTPFilesGV(GroupCode.ToString(), CompannyCode.ToString(), dtAccountNumber, TemplateID.ToString(), Me.UserId.ToString, gvClientFiles.CurrentPageIndex + 1, gvClientFiles.PageSize, gvClientFiles, IsBind)
                If gvClientFiles.Items.Count > 0 Then
                    gvClientFiles.Columns(13).Visible = CBool(htRights("Download"))
                    btnRefresh.Visible = True
                    gvClientFiles.Visible = True
                    lblRNF.Visible = False
                Else
                    gvClientFiles.Visible = True
                    lblRNF.Visible = True
                    gvClientFiles.DataSource = Nothing
                    btnRefresh.Visible = False
                    gvClientFiles.Visible = False

                End If
            Else
                lblRNF.Visible = True
                gvClientFiles.DataSource = Nothing
                btnRefresh.Visible = False

                gvClientFiles.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
   
    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ViewddlCompany.SelectedIndexChanged
        Try
            SetArraListRoleID()
            FillDesignedDtByAPNatureByAssignedRole()
            LoadddlAccountPaymentNatureByUserID()

            LoadgvClientFiles(True)

            HideGvClientFiles()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ViewddlGroup.SelectedIndexChanged
        Try
            SetArraListRoleID()
            FillDesignedDtByAPNatureByAssignedRole()
            LoadddlCompany()
            LoadddlAccountPaymentNatureByUserID()
            LoadddlFileUploadTemplate()
            HideGvClientFiles()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlAcccountPaymentNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ViewddlAcccountPaymentNature.SelectedIndexChanged

        If ViewddlAcccountPaymentNature.SelectedValue.ToString = "0" Then
            SetArraListRoleID()
            LoadddlFileUploadTemplate()
            HideGvClientFiles()
        Else
            SetArraListRoleID()
            LoadddlFileUploadTemplate()
            SetViewStateDtOnAPNatureIndexChnage()
            LoadgvClientFiles(True)
        End If


    End Sub
    Private Sub HideGvClientFiles()
        lblRNF.Visible = True
        gvClientFiles.DataSource = Nothing
        btnRefresh.Visible = False
        gvClientFiles.Visible = False
        ViewState("AccountNumber") = Nothing
    End Sub


    Protected Sub ddlFileUploadTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ViewddlFileUploadTemplate.SelectedIndexChanged
        Try
            SetArraListRoleID()
            LoadgvClientFiles(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvClientFiles_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvClientFiles.ItemCommand
        Try

            If e.CommandName = "ProcessFile" Then
                hfClientFileID.Value = e.CommandArgument.ToString()
                ReadInstructionsInFile(hfClientFileID.Value.ToString())
            ElseIf e.CommandName = "del" Then
                Dim objICFiles As New ICFiles
                Dim aFileInfo As IO.FileInfo
                If objICFiles.LoadByPrimaryKey(e.CommandArgument.ToString) Then
                    If objICFiles.Status = "Pending Read" Then
                        If ICFilesController.DeleteClientUserFilesByFileID(objICFiles.FileID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString) = True Then
                            UIUtilities.ShowDialog(Me, "Client File Upload", "File deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                            aFileInfo = New IO.FileInfo(objICFiles.FileLocation)

                            If aFileInfo.Exists Then
                                aFileInfo.Delete()
                            End If
                            LoadgvClientFiles(True)
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Client File Upload", "Uploaded File can not be deleted  due to following reasons : <br />1.  File is already parsed.<br />2.  File is in " & "In Process" & " Status. <br />3. File is in " & "Rejected" & " Status.", ICBO.IC.Dialogmessagetype.Failure)
                        LoadgvClientFiles(True)
                    End If
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvClientFiles_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvClientFiles.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvClientFiles.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvClientFiles_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvClientFiles.NeedDataSource
        LoadgvClientFiles(False)
    End Sub

    Protected Sub gvClientFiles_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvClientFiles.PageIndexChanged
        gvClientFiles.CurrentPageIndex = e.NewPageIndex
    End Sub

    Private Sub ReadInstructionsInFile(ByVal FileID As String)
        Try

            Dim collAccountPaymentNatureFileUploadTemplateFields As New ICAccountPaymentNatureFileUploadTemplatefieldsCollection
            collAccountPaymentNatureFileUploadTemplateFields.es.Connection.CommandTimeout = 3600

            Dim HeadercollAccountPaymentNatureFileUploadTemplateFields As New ICAccountPaymentNatureFileUploadTemplatefieldsCollection
            HeadercollAccountPaymentNatureFileUploadTemplateFields.es.Connection.CommandTimeout = 3600
            Dim DetailcollAccountPaymentNatureFileUploadTemplateFields As New ICAccountPaymentNatureFileUploadTemplatefieldsCollection
            DetailcollAccountPaymentNatureFileUploadTemplateFields.es.Connection.CommandTimeout = 3600


            Dim aFileInfo As IO.FileInfo
            Dim objTemplate As New ICTemplate
            Dim objICFile As New ICFiles

            objTemplate.es.Connection.CommandTimeout = 3600
            objICFile.es.Connection.CommandTimeout = 3600
            Dim LineNo As Integer = 0
            Dim LoopCount As Integer = 0
            Dim FilePath As String = ""

            If objICFile.LoadByPrimaryKey(FileID) Then
                objTemplate.LoadByPrimaryKey(objICFile.FileUploadTemplateID)
                If objTemplate.TemplateType = "Dual" Then
                    ViewState("dtVarifiedHeader") = Nothing
                    ViewState("dtVarifiedDetail") = Nothing
                    ViewState("dtUnVerified") = Nothing
                    FilePath = objICFile.FileLocation.ToString()
                    HeadercollAccountPaymentNatureFileUploadTemplateFields = ICAccountPaymentNatureFileUploadTemplateFieldsController.GetTemplateFieldsByAccountPaymentNatureTemplateOfDualFile(objICFile.AccountNumber, objICFile.BranchCode, objICFile.Currency, objICFile.PaymentNatureCode, objICFile.FileUploadTemplateID, "Header")
                    DetailcollAccountPaymentNatureFileUploadTemplateFields = ICAccountPaymentNatureFileUploadTemplateFieldsController.GetTemplateFieldsByAccountPaymentNatureTemplateOfDualFile(objICFile.AccountNumber, objICFile.BranchCode, objICFile.Currency, objICFile.PaymentNatureCode, objICFile.FileUploadTemplateID, "Detail")
                    If objTemplate.TemplateFormat.Trim().ToString() = ".txt" Then
                        aFileInfo = New IO.FileInfo(FilePath.ToString())

                        If aFileInfo.Exists Then
                            Dim mreader As New System.IO.StreamReader(aFileInfo.FullName)
                            Do While mreader.Peek <> -1
                                LineNo = LineNo + 1
                                GetDataFromTXTDualFile(mreader.ReadLine(), HeadercollAccountPaymentNatureFileUploadTemplateFields, DetailcollAccountPaymentNatureFileUploadTemplateFields, LineNo, objTemplate, LoopCount)
                                LoopCount = LoopCount + 1
                            Loop
                            mreader.Close()
                            mreader.Dispose()
                        End If
                    End If
                    If Not ViewState("dtVarifiedHeader") Is Nothing Then
                        If DirectCast(ViewState("dtVarifiedHeader"), DataTable).Rows.Count > 0 Then
                            gvVerified.DataSource = DirectCast(ViewState("dtVarifiedHeader"), DataTable)
                            gvVerified.DataBind()

                            'Set FileID, FileName, TnxCount and Total Amount
                            Dim dt As New DataTable
                            Dim dr As DataRow
                            Dim Amount As Double = 0
                            dt = DirectCast(ViewState("dtVarifiedHeader"), DataTable)

                            For Each dr In dt.Rows
                                Amount = Amount + CDbl(dr("Amount-3").ToString())
                            Next



                            txtFileID.Text = objICFile.FileID.ToString()
                            txtFileName.Text = objICFile.OriginalFileName.ToString()
                            txtTnxCount.Text = dt.Rows.Count.ToString()
                            txtTotalAmount.Text = Amount.ToString("N2")







                            rimProcessFile.Enabled = True
                            tblProcess.Style.Remove("display")
                            tblView.Style.Add("display", "none")
                            pnlVerified.Visible = True
                            lblVerified.Visible = True
                            lblVerified.Text = "Verified Instruction Header List"
                            btnProceed.Visible = True
                            btnCancel.Visible = True
                        End If
                    End If
                    If Not ViewState("dtVarifiedDetail") Is Nothing Then
                        If DirectCast(ViewState("dtVarifiedDetail"), DataTable).Rows.Count > 0 Then
                            gvVerifiedDetail.DataSource = DirectCast(ViewState("dtVarifiedDetail"), DataTable)
                            gvVerifiedDetail.DataBind()


                            pnlVerifiedDetail.Visible = True
                            lblVerifiedDetail.Visible = True
                        End If
                    End If
                    If Not ViewState("dtUnVerified") Is Nothing Then
                        If DirectCast(ViewState("dtUnVerified"), DataTable).Rows.Count > 0 Then
                            Dim DtForEmail As New DataTable
                            Dim objICUser As New ICUser
                            objICUser.LoadByPrimaryKey(Me.UserId.ToString)
                            Dim APNLocation As String = objICFile.AccountNumber & "-" & objICFile.BranchCode & "-" & objICFile.Currency & "-" & objICFile.PaymentNatureCode & "-" & objICUser.OfficeCode.ToString
                            Dim TempString As String = ""
                            Dim ErrorMessageForEmail As String = Nothing
                            DtForEmail = DirectCast(ViewState("dtUnVerified"), DataTable)
                            For Each dr As DataRow In DtForEmail.Rows
                                If TempString = "" Then
                                    TempString = dr("Message").ToString
                                    If Not ErrorMessageForEmail Is Nothing Then
                                        If ErrorMessageForEmail.Contains(TempString) = False Then
                                            ErrorMessageForEmail += TempString & "<br/>"
                                        End If
                                    Else
                                        ErrorMessageForEmail += TempString & "<br/>"
                                    End If
                                ElseIf TempString <> dr("Message").ToString Then
                                    TempString = ""
                                    TempString = dr("Message").ToString
                                    If ErrorMessageForEmail.Contains(TempString) = False Then
                                        ErrorMessageForEmail += TempString & "<br/>"
                                    End If
                                End If
                            Next


                            gvUnverified.DataSource = DirectCast(ViewState("dtUnVerified"), DataTable)
                            gvUnverified.DataBind()

                            rimProcessFile.Enabled = True
                            tblProcess.Style.Remove("display")
                            tblView.Style.Add("display", "none")
                            pnlUnVerified.Visible = True
                            lblUnverified.Visible = True
                            btnProceed.Visible = False
                            btnReject.Visible = True
                            btnCancel.Visible = True
                            EmailUtilities.Fileprocessedbyuserandfailed(objICFile.OriginalFileName.ToString, ErrorMessageForEmail, Me.UserInfo.Username.ToString, APNLocation)
                            SMSUtilities.Fileprocessedbyuserandfailed(Me.UserInfo.Username.ToString, APNLocation)
                        End If
                    End If
                    Exit Sub
                Else
                    collAccountPaymentNatureFileUploadTemplateFields = ICAccountPaymentNatureFileUploadTemplateFieldsController.GetTemplateFieldsByAccountPaymentNatureTemplate(objICFile.AccountNumber, objICFile.BranchCode, objICFile.Currency, objICFile.PaymentNatureCode, objICFile.FileUploadTemplateID)
                    ViewState("dtVarified") = Nothing
                    ViewState("dtUnVerified") = Nothing
                    FilePath = objICFile.FileLocation.ToString()

                    If objTemplate.TemplateFormat.Trim().ToString() = ".txt" Then
                        aFileInfo = New IO.FileInfo(FilePath.ToString())
                        If aFileInfo.Exists Then
                            Dim mreader As New System.IO.StreamReader(aFileInfo.FullName)
                            Do While mreader.Peek <> -1
                                LineNo = LineNo + 1
                                GetDataFromTXTFile(mreader.ReadLine(), collAccountPaymentNatureFileUploadTemplateFields, LineNo, objTemplate, LoopCount)
                                LoopCount = LoopCount + 1

                            Loop
                            mreader.Close()
                            mreader.Dispose()
                        End If
                    Else ' in case of xls or xlsx files

                        Dim ConnStr As String = ""
                        Dim cmdStr As String = ""
                        Dim dsExcel As New DataSet
                        Dim drExcel As DataRow

                        'xlsx
                        If objTemplate.TemplateFormat.Trim().ToString() = ".xlsx" Then
                            FilePath = objICFile.FileLocation.ToString()
                            ConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FilePath & "; Extended Properties='Excel 12.0 Xml; HDR=Yes; IMEX=1'"

                        ElseIf objTemplate.TemplateFormat.Trim().ToString() = ".xls" Then
                            FilePath = objICFile.FileLocation.ToString()
                            ConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FilePath & "; Extended Properties='Excel 12.0; HDR=Yes; IMEX=1'"
                        ElseIf objTemplate.TemplateFormat.Trim().ToString() = ".csv" Then
                            FilePath = Request.PhysicalApplicationPath.ToString() & "UploadedFiles\"
                            ConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FilePath & "; Extended Properties='Text; HDR=Yes; FMT=Delimited'"
                        End If
                        Dim Conn As New OleDb.OleDbConnection(ConnStr)
                        If objTemplate.TemplateFormat.Trim().ToString() = ".csv" Then
                            cmdStr = "select * from " & objICFile.FileName.ToString & ""
                        Else
                            cmdStr = "select * from [" & objTemplate.SheetName.Trim.ToString() & "$]"
                        End If


                        Dim cmd As New OleDb.OleDbCommand(cmdStr, Conn)
                        Dim da As New OleDb.OleDbDataAdapter(cmd)

                        da.Fill(dsExcel, "ExcelFile")
                        Conn.Close()
                        Conn.Dispose()






                        If Not dsExcel.Tables(0).Rows.Count = 0 Then
                            For Each drExcel In dsExcel.Tables(0).Rows
                                LineNo = LineNo + 1
                                GetDataFromEXCELFile(drExcel, collAccountPaymentNatureFileUploadTemplateFields, LineNo, dsExcel.Tables(0).Columns.Count, objTemplate, LoopCount)
                                LoopCount = LoopCount + 1
                            Next
                        End If
                    End If
                End If

                txtFileID.Text = objICFile.FileID.ToString()
                txtFileName.Text = objICFile.OriginalFileName.ToString()
                txtTnxCount.Text = Nothing
                txtTotalAmount.Text = Nothing


                If Not ViewState("dtVarified") Is Nothing Then
                    If DirectCast(ViewState("dtVarified"), DataTable).Rows.Count > 0 Then
                        gvVerified.DataSource = DirectCast(ViewState("dtVarified"), DataTable)
                        gvVerified.DataBind()

                        'Set FileID, FileName, TnxCount and Total Amount
                        Dim dt As New DataTable
                        Dim dr As DataRow
                        Dim Amount As Double = 0
                        dt = DirectCast(ViewState("dtVarified"), DataTable)

                        For Each dr In dt.Rows
                            Amount = Amount + CDbl(dr("Amount-3").ToString())
                        Next



                        txtFileID.Text = objICFile.FileID.ToString()
                        txtFileName.Text = objICFile.OriginalFileName.ToString()
                        txtTnxCount.Text = dt.Rows.Count.ToString()
                        txtTotalAmount.Text = Amount.ToString("N2")







                        rimProcessFile.Enabled = True
                        tblProcess.Style.Remove("display")
                        tblView.Style.Add("display", "none")
                        pnlVerified.Visible = True
                        lblVerified.Visible = True
                        btnProceed.Visible = True
                        btnCancel.Visible = True
                    End If
                End If
                If Not ViewState("dtUnVerified") Is Nothing Then
                    If DirectCast(ViewState("dtUnVerified"), DataTable).Rows.Count > 0 Then
                        Dim ErrorMessageForEmail As String = Nothing
                        Dim DtForEmail As New DataTable
                        DtForEmail = DirectCast(ViewState("dtUnVerified"), DataTable)

                        gvUnverified.DataSource = DtForEmail
                        gvUnverified.DataBind()
                        Dim objICUser As New ICUser
                        objICUser.LoadByPrimaryKey(Me.UserId.ToString)
                        Dim APNLocation As String = objICFile.AccountNumber & "-" & objICFile.BranchCode & "-" & objICFile.Currency & "-" & objICFile.PaymentNatureCode & "-" & objICUser.OfficeCode.ToString
                        Dim TempString As String = ""
                        For Each dr As DataRow In DtForEmail.Rows
                            If TempString = "" Then
                                TempString = dr("Message").ToString
                                If Not ErrorMessageForEmail Is Nothing Then
                                    If ErrorMessageForEmail.Contains(TempString) = False Then
                                        ErrorMessageForEmail += TempString & "<br/>"
                                    End If
                                Else
                                    ErrorMessageForEmail += TempString & "<br/>"
                                End If
                            ElseIf TempString <> dr("Message").ToString Then
                                TempString = ""
                                TempString = dr("Message").ToString
                                If ErrorMessageForEmail.Contains(TempString) = False Then
                                    ErrorMessageForEmail += TempString & "<br/>"
                                End If
                            End If
                        Next




                        rimProcessFile.Enabled = True
                        tblProcess.Style.Remove("display")
                        tblView.Style.Add("display", "none")
                        pnlUnVerified.Visible = True
                        lblUnverified.Visible = True
                        btnProceed.Visible = False
                        btnReject.Visible = True
                        btnCancel.Visible = True
                        EmailUtilities.Fileprocessedbyuserandfailed(objICFile.OriginalFileName.ToString, ErrorMessageForEmail, Me.UserInfo.Username.ToString, APNLocation)
                        SMSUtilities.Fileprocessedbyuserandfailed(Me.UserInfo.Username.ToString, APNLocation)
                    End If
                End If


            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetDataFromTXTFile(ByVal FileLine As String, ByVal collAccountPaymentNatureTemplateFields As ICAccountPaymentNatureFileUploadTemplatefieldsCollection, ByVal LineNumber As Integer, ByVal FileTemplate As ICTemplate, ByVal LoopCount As Integer)
        Dim dtVarified, dtUnVerified As New DataTable
        Dim drVarified, drUnVerified As DataRow
        Dim objAccountPaymentNatureTemplateField As New ICAccountPaymentNatureFileUploadTemplatefields


        objAccountPaymentNatureTemplateField.es.Connection.CommandTimeout = 3600
        FileTemplate.es.Connection.CommandTimeout = 3600


        Dim StartIndex As Integer
        Dim ErrorMsg As String = ""
        Dim arrInsruction As String()
        Dim CountColumn As Integer = 0
        Try
            StartIndex = 0


            If ViewState("dtVarified") Is Nothing Then
                For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                    dtVarified.Columns.Add(New DataColumn(objAccountPaymentNatureTemplateField.FieldName.ToString() & "-" & objAccountPaymentNatureTemplateField.FieldID.ToString(), GetType(System.String)))
                Next
                dtUnVerified.Columns.Add(New DataColumn("Data", GetType(System.String)))
                dtUnVerified.Columns.Add(New DataColumn("Message", GetType(System.String)))
                dtUnVerified.Columns.Add(New DataColumn("LineNo", GetType(System.String)))
            Else
                dtVarified = DirectCast(ViewState("dtVarified"), DataTable)
                dtUnVerified = DirectCast(ViewState("dtUnVerified"), DataTable)
            End If
            If FileTemplate.IsFixLength = True Then
                ErrorMsg = CheckDataTXT(FileLine, collAccountPaymentNatureTemplateFields, FileTemplate, LoopCount)
                If ErrorMsg = "" Then
                    drVarified = dtVarified.NewRow()
                    For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                        If objAccountPaymentNatureTemplateField.FieldName.ToString().Trim() = "Amount" Then
                            drVarified(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1) = CDbl(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim())
                        Else
                            drVarified(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1) = FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()
                        End If
                        StartIndex = StartIndex + objAccountPaymentNatureTemplateField.FixLength
                    Next
                    dtVarified.Rows.Add(drVarified)
                Else
                    drUnVerified = dtUnVerified.NewRow()
                    drUnVerified("Data") = FileLine.ToString()
                    drUnVerified("Message") = ErrorMsg.ToString()
                    drUnVerified("LineNo") = LineNumber
                    dtUnVerified.Rows.Add(drUnVerified)
                End If

                ViewState("dtVarified") = dtVarified
                ViewState("dtUnVerified") = dtUnVerified
            Else 'CSV File               
                ErrorMsg = CheckDataTXT(FileLine, collAccountPaymentNatureTemplateFields, FileTemplate, LoopCount)
                arrInsruction = FileLine.Split(FileTemplate.FieldDelimiter.ToString().Trim())
                If ErrorMsg = "" Then
                    drVarified = dtVarified.NewRow()
                    For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                            drVarified(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1) = CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim())
                        Else
                            drVarified(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1) = arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()
                        End If
                    Next
                    dtVarified.Rows.Add(drVarified)
                Else
                    drUnVerified = dtUnVerified.NewRow()
                    drUnVerified("Data") = FileLine.ToString()
                    drUnVerified("Message") = ErrorMsg.ToString()
                    drUnVerified("LineNo") = LineNumber
                    dtUnVerified.Rows.Add(drUnVerified)
                End If
                ViewState("dtVarified") = dtVarified
                ViewState("dtUnVerified") = dtUnVerified
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetDataFromTXTDualFile(ByVal FileLine As String, ByVal HeadercollAccountPaymentNatureTemplateFields As ICAccountPaymentNatureFileUploadTemplatefieldsCollection, ByVal DetailcollAccountPaymentNatureTemplateFields As ICAccountPaymentNatureFileUploadTemplatefieldsCollection, ByVal LineNumber As Integer, ByVal FileTemplate As ICTemplate, ByVal LoopCount As Integer)
        Dim dtVarifiedHeader, dtVarifiedDetail, dtUnVerified As New DataTable
        Dim drVarifiedHeader, drVarifiedDetail, drUnVerified As DataRow
        Dim objAccountPaymentNatureTemplateField As New ICAccountPaymentNatureFileUploadTemplatefields


        objAccountPaymentNatureTemplateField.es.Connection.CommandTimeout = 3600



        Dim StartIndex As Integer
        Dim ErrorMsg As String = ""
        Dim IdentifierChar As String = ""
        Dim arrInsruction As String()
        Dim CountColumn As Integer = 0
        Dim HeaderDetailRelate As Integer = 0
        Try
            StartIndex = 0

            If ViewState("dtVarifiedHeader") Is Nothing Then
                ' Design Header Datatable
                dtVarifiedHeader.Columns.Add(New DataColumn("HeaderDetailRelationNo", GetType(System.String)))
                For Each objAccountPaymentNatureTemplateField In HeadercollAccountPaymentNatureTemplateFields
                    dtVarifiedHeader.Columns.Add(New DataColumn(objAccountPaymentNatureTemplateField.FieldName.ToString() & "-" & objAccountPaymentNatureTemplateField.FieldID.ToString(), GetType(System.String)))
                Next
                ' Design Detail Datatable
                dtVarifiedDetail.Columns.Add(New DataColumn("HeaderDetailRelationNo", GetType(System.String)))
                For Each objAccountPaymentNatureTemplateField In DetailcollAccountPaymentNatureTemplateFields
                    dtVarifiedDetail.Columns.Add(New DataColumn(objAccountPaymentNatureTemplateField.FieldName.ToString() & "-" & objAccountPaymentNatureTemplateField.FieldID.ToString(), GetType(System.String)))
                Next

                dtUnVerified.Columns.Add(New DataColumn("Data", GetType(System.String)))
                dtUnVerified.Columns.Add(New DataColumn("Message", GetType(System.String)))
                dtUnVerified.Columns.Add(New DataColumn("LineNo", GetType(System.String)))
            Else
                dtVarifiedHeader = DirectCast(ViewState("dtVarifiedHeader"), DataTable)
                dtVarifiedDetail = DirectCast(ViewState("dtVarifiedDetail"), DataTable)
                dtUnVerified = DirectCast(ViewState("dtUnVerified"), DataTable)
            End If
            If dtVarifiedHeader.Rows.Count > 0 Then
                HeaderDetailRelate = dtVarifiedHeader.Rows.Count
            End If

            arrInsruction = FileLine.Split(FileTemplate.FieldDelimiter.ToString().Trim())
            IdentifierChar = arrInsruction(0).ToString()
            If IdentifierChar.ToString() = FileTemplate.HeaderIdentifier.ToString() Then
                ErrorMsg = CheckDataTXTDualFile(arrInsruction, HeadercollAccountPaymentNatureTemplateFields, DetailcollAccountPaymentNatureTemplateFields, FileTemplate, "Header", LoopCount)
                If ErrorMsg = "" Then
                    drVarifiedHeader = dtVarifiedHeader.NewRow()
                    drVarifiedHeader(0) = (HeaderDetailRelate + 1).ToString()
                    For Each objAccountPaymentNatureTemplateField In HeadercollAccountPaymentNatureTemplateFields
                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                            drVarifiedHeader(CInt(objAccountPaymentNatureTemplateField.FieldOrder)) = CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim())
                        Else
                            drVarifiedHeader(CInt(objAccountPaymentNatureTemplateField.FieldOrder)) = arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()
                        End If
                    Next
                    dtVarifiedHeader.Rows.Add(drVarifiedHeader)
                Else
                    drUnVerified = dtUnVerified.NewRow()
                    drUnVerified("Data") = FileLine.ToString()
                    drUnVerified("Message") = ErrorMsg.ToString()
                    drUnVerified("LineNo") = LineNumber
                    dtUnVerified.Rows.Add(drUnVerified)
                End If
            ElseIf IdentifierChar.ToString() = FileTemplate.DetailIdentifier.ToString() Then
                ErrorMsg = CheckDataTXTDualFile(arrInsruction, HeadercollAccountPaymentNatureTemplateFields, DetailcollAccountPaymentNatureTemplateFields, FileTemplate, "Detail", LoopCount)
                If ErrorMsg = "" Then
                    drVarifiedDetail = dtVarifiedDetail.NewRow()
                    drVarifiedDetail(0) = HeaderDetailRelate.ToString()
                    For Each objAccountPaymentNatureTemplateField In DetailcollAccountPaymentNatureTemplateFields
                        Dim chk As Integer
                        Dim chkDouble As Double
                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString().Contains("Detail_FF_Amt_") Then
                            If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString() <> "" Then
                                Try

                                    If Double.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), chkDouble) = True Then
                                        drVarifiedDetail(CInt(objAccountPaymentNatureTemplateField.FieldOrder)) = CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim())
                                    Else
                                        drVarifiedDetail(CInt(objAccountPaymentNatureTemplateField.FieldOrder)) = Nothing
                                    End If
                                Catch ex As Exception
                                    drVarifiedDetail(CInt(objAccountPaymentNatureTemplateField.FieldOrder)) = Nothing
                                    Continue For
                                End Try

                            Else
                                drVarifiedDetail(CInt(objAccountPaymentNatureTemplateField.FieldOrder)) = Nothing
                            End If

                        Else
                            drVarifiedDetail(CInt(objAccountPaymentNatureTemplateField.FieldOrder)) = arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()
                        End If
                    Next
                    dtVarifiedDetail.Rows.Add(drVarifiedDetail)
                Else
                    drUnVerified = dtUnVerified.NewRow()
                    drUnVerified("Data") = FileLine.ToString()
                    drUnVerified("Message") = ErrorMsg.ToString()
                    drUnVerified("LineNo") = LineNumber
                    dtUnVerified.Rows.Add(drUnVerified)
                End If
            End If
            ViewState("dtVarifiedHeader") = dtVarifiedHeader
            ViewState("dtVarifiedDetail") = dtVarifiedDetail
            ViewState("dtUnVerified") = dtUnVerified
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckDataTXT(ByVal FileLine As String, ByVal collAccountPaymentNatureTemplateFields As ICAccountPaymentNatureFileUploadTemplatefieldsCollection, ByVal FileTemplate As ICTemplate, ByVal LoopCount As String) As String
        Dim objAccountPaymentNatureTemplateField As ICAccountPaymentNatureFileUploadTemplatefields

        Dim Msg As String = ""
        Dim PaymentMode As String = ""
        Dim StartIndex As Integer = 0
        Dim result As Integer
        Dim ResultDouble As Double
        Dim arrInsruction As String()
        Dim cnt As Integer = 0
        Dim ProductTypeCode As String = Nothing
        Dim dtProductTypeLoop As DataTable
        Dim dtBankCodeLoop As DataTable
        Dim dtBankNameLoop As DataTable
        Dim dtPrintLocationLoop As DataTable
        Dim dtPrincipalBankBrLoop As DataTable
        Dim dtProductType As New DataTable
        Dim dtUBPCompany As New DataTable
        Dim BeneIDType As New ArrayList
        Try
            If LoopCount = 0 Then
                objAccountPaymentNatureTemplateField = New ICAccountPaymentNatureFileUploadTemplatefields
                objAccountPaymentNatureTemplateField = collAccountPaymentNatureTemplateFields(0)
                '' Product Type
                dtProductTypeLoop = New DataTable
                ViewState("dtProductTypeLoop") = Nothing
                dtProductTypeLoop = ICProductTypeController.IsProductTypeExistByProductTypeCode2(FileTemplate.TemplateID.ToString, objAccountPaymentNatureTemplateField)
                ViewState("dtProductTypeLoop") = dtProductTypeLoop
                '' Banks
                dtBankCodeLoop = New DataTable
                ViewState("dtBankCodeLoop") = Nothing
                dtBankCodeLoop = ICBankController.IsBankExistByBankCodeForBulkUpLoad
                ViewState("dtBankCodeLoop") = dtBankCodeLoop

                dtBankNameLoop = New DataTable
                ViewState("dtBankNameLoop") = Nothing
                dtBankNameLoop = dtBankCodeLoop.Clone
                dtBankNameLoop = dtBankCodeLoop
                ViewState("dtBankNameLoop") = dtBankNameLoop
                '' Print Location
                dtPrintLocationLoop = New DataTable
                ViewState("dtPrintLocationLoop") = Nothing
                dtPrintLocationLoop = ICOfficeController.IsPrintLocationExistByPrintLocationNameOrCodeForBulkUpLoad(objAccountPaymentNatureTemplateField.AccountNumber.ToString, objAccountPaymentNatureTemplateField.BranchCode.ToString, objAccountPaymentNatureTemplateField.Currency.ToString, objAccountPaymentNatureTemplateField.PaymentNatureCode.ToString)
                ViewState("dtPrintLocationLoop") = dtPrintLocationLoop
                '' DD Payable Location

                dtPrintLocationLoop = New DataTable
                ViewState("dtDDPayableLocationLoop") = Nothing
                dtPrintLocationLoop = ICCityController.IsCityExistByCityNameOrCodeForBulkUpload()
                ViewState("dtDDPayableLocationLoop") = dtPrintLocationLoop
                '' Beneficiary City
                ViewState("dtBeneficiaryCityLoop") = Nothing
                ViewState("dtBeneficiaryCityLoop") = dtPrintLocationLoop
                ''Bene Province
                dtPrintLocationLoop = New DataTable
                ViewState("dtBeneficiaryProvinceLoop") = Nothing
                dtPrintLocationLoop = ICProvinceController.IsProvinceExistByCountryNameOrCodeForBulkUpload
                ViewState("dtBeneficiaryProvinceLoop") = dtPrintLocationLoop

                ''Bene Country
                dtPrintLocationLoop = New DataTable
                ViewState("dtBeneficiaryCountryLoop") = Nothing
                dtPrintLocationLoop = ICCountryController.IsCountryExistByCountryNameForBulkUpload()
                ViewState("dtBeneficiaryCountryLoop") = dtPrintLocationLoop


                ''Bene PICK Up Location For COTC
                dtPrincipalBankBrLoop = New DataTable
                ViewState("dtPrincipalBankBrLoop") = Nothing
                dtPrincipalBankBrLoop = ICOfficeController.GetPrincipalBankBranchActiveAndApprove
                ViewState("dtPrincipalBankBrLoop") = dtPrincipalBankBrLoop

                '' Bene ID Type 
                BeneIDType.Add("cnic")
                BeneIDType.Add("passport")
                BeneIDType.Add("nicop")
                BeneIDType.Add("driving license")
                ViewState("BeneIDType") = BeneIDType


                ''UBPCompany
                dtUBPCompany = New DataTable
                ViewState("dtUBPCompany") = Nothing
                dtUBPCompany = ICUBPSCompanyController.GetAllUBPActiveApproveCompany
                ViewState("dtUBPCompany") = dtUBPCompany

            End If
            objAccountPaymentNatureTemplateField = New ICAccountPaymentNatureFileUploadTemplatefields
            If FileTemplate.IsFixLength = True Then
                For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                    ' Get Product Type
                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ProductTypeCode" Then
                        If FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                            Msg = "Product Type Code is empty."
                            Exit For
                        Else
                            dtProductTypeLoop = New DataTable
                            dtProductTypeLoop = DirectCast(ViewState("dtProductTypeLoop"), DataTable)
                            Dim drProductTypeRow As DataRow()
                            drProductTypeRow = dtProductTypeLoop.Select("ProductTypeCode='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "'")
                            If dtProductTypeLoop.Rows.Count > 0 Then
                                If drProductTypeRow.Count = 0 Then
                                    Msg = "Invalid Product Type Code."
                                    Exit For
                                Else
                                    Dim objProductType As New ICProductType
                                    If objProductType.LoadByPrimaryKey(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()) Then
                                        PaymentMode = objProductType.DisbursementMode.ToString().ToLower()
                                        ProductTypeCode = Nothing
                                        ProductTypeCode = objProductType.ProductTypeCode.ToString
                                        hfDualDisbMode.Value = PaymentMode.ToString()
                                    Else
                                        Msg = "Invalid Product Type Code."
                                        Exit For
                                    End If
                                End If
                            Else
                                Msg = "Invalid Product Type Code."
                                Exit For
                            End If
                        End If
                    End If
                    StartIndex = StartIndex + objAccountPaymentNatureTemplateField.FixLength
                Next
                StartIndex = 0
                If PaymentMode.ToString() <> "" Then
                    ' Check Product Type wise Required Fields Data
                    Select Case PaymentMode.ToString()
                        Case "bill payment"
                            For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields


                                Dim FieldTypeMessage As String = Nothing
                                FieldTypeMessage = ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBType(objAccountPaymentNatureTemplateField.FieldID, FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FieldType, objAccountPaymentNatureTemplateField.FixLength)
                                If Not FieldTypeMessage = "OK" Then
                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                                    Exit For
                                End If
                                Dim objICUBPCompany As New ICUBPSCompany
                                For Each objICAPNPTypeFields As ICAccountPaymentNatureFileUploadTemplatefields In collAccountPaymentNatureTemplateFields
                                    Select Case objICAPNPTypeFields.FieldName
                                        Case "UBPCompanyCode"
                                            Dim UBPSCompanyID As String = ""
                                            UBPSCompanyID = ICUBPSCompanyController.GetUBPCompanyIDByCompanyNameOrCodeorID(FileLine.Substring(StartIndex, objICAPNPTypeFields.FixLength).ToString().Trim())
                                            If UBPSCompanyID <> "" Then
                                                If objICUBPCompany.LoadByPrimaryKey(UBPSCompanyID) = True Then
                                                Else
                                                    Msg = Msg & " " & "Invalid UBP Company Code"
                                                    Exit For
                                                End If
                                            Else
                                                Msg = Msg & " " & "Invalid UBP Company Code"
                                                Exit For
                                            End If

                                    End Select
                                Next
                                If Msg <> "" Then
                                    Exit For
                                End If

                                If objAccountPaymentNatureTemplateField.IsRequiredBillPayment = True Then
                                    If FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            If objICUBPCompany IsNot Nothing Then
                                                If objICUBPCompany.MultiplesOfAmount = "Only Billed Amount" Then
                                                    If FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() <> "0" Then
                                                        Msg = Msg & " " & "Amount must be zero in case of only billed amount"
                                                        Exit For
                                                    End If

                                                End If
                                            Else
                                                Msg = Msg & " " & "Invalid UBP Company"
                                                Exit For
                                            End If

                                            If IsNumeric(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()) = True Then
                                                Try
                                                    If Double.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), ResultDouble) = False Then
                                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                        Exit For
                                                    End If
                                                Catch ex As Exception
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End Try
                                                If objICUBPCompany.MultiplesOfAmount = "Only Billed Amount" Then
                                                    If CDbl(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()) <> 0 Then
                                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be equal to zero (0) in only billed amount"
                                                        Exit For
                                                    End If
                                                Else
                                                    If CDbl(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()) < 1 Then
                                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                        Exit For
                                                    End If
                                                End If


                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                            If objICUBPCompany.MultiplesOfAmount <> "Only Billed Amount" Then
                                                Dim EnterdAmountRem As String = ""
                                                EnterdAmountRem = ICUBPSCompanyController.CheckUBPCompanyAmountAcceptance(CDbl(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()), objICUBPCompany.UBPSCompanyID.ToString)
                                                If EnterdAmountRem <> "OK" Then
                                                    Msg = Msg & " " & EnterdAmountRem
                                                    Exit For
                                                End If
                                            End If
                                        End If


                                        ''' value date
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For

                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For

                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                            dtBankCodeLoop = New DataTable
                                            dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                            If dtBankCodeLoop.Rows.Count > 0 Then
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    Dim drBankLoop As DataRow()
                                                    drBankLoop = dtBankCodeLoop.Select("BankCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim())
                                                    If drBankLoop.Count = 0 Then
                                                        Msg = "Invalid Bank Code."
                                                        Exit For
                                                    End If
                                                Else
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBranchCode" Then
                                            dtPrincipalBankBrLoop = New DataTable
                                            dtPrincipalBankBrLoop = DirectCast(ViewState("dtPrincipalBankBrLoop"), DataTable)
                                            If dtPrincipalBankBrLoop.Rows.Count > 0 Then
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower(), i) = True Then
                                                    Dim drPRincipalBankBrLoop As DataRow()
                                                    drPRincipalBankBrLoop = dtPrincipalBankBrLoop.Select("OfficeID=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                    If drPRincipalBankBrLoop.Count = 0 Then
                                                        Dim drPRincipalBankBrLoop2 As DataRow()
                                                        drPRincipalBankBrLoop2 = dtPrincipalBankBrLoop.Select("OfficeCode='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "'")
                                                        If drPRincipalBankBrLoop2.Count = 0 Then
                                                            Msg = "Invalid Pick Up Location."
                                                            Exit For
                                                        End If

                                                    End If
                                                Else
                                                    Dim drPRincipalBankBrLoop As DataRow()
                                                    drPRincipalBankBrLoop = dtPrincipalBankBrLoop.Select("OfficeCode='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "'")
                                                    If drPRincipalBankBrLoop.Count = 0 Then
                                                        Dim drPRincipalBankBrLoop2 As DataRow()
                                                        drPRincipalBankBrLoop2 = dtPrincipalBankBrLoop.Select("OfficeName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "'")
                                                        If drPRincipalBankBrLoop2.Count = 0 Then
                                                            Msg = "Invalid Pick Up Location."
                                                            Exit For
                                                        End If

                                                    End If
                                                End If
                                            Else
                                                Msg = "Invalid Pick Up Location."
                                                Exit For
                                            End If
                                        End If

                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryIdentificationType" Then
                                            BeneIDType = New ArrayList
                                            BeneIDType = ViewState("BeneIDType")
                                            If BeneIDType.Contains(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower) = False Then
                                                Msg = "Invalid Identification Type"
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "UBPCompanyCode" Then
                                            dtUBPCompany = New DataTable
                                            dtUBPCompany = DirectCast(ViewState("dtUBPCompany"), DataTable)
                                            If dtUBPCompany.Rows.Count > 0 Then
                                                Dim drUBPCompanyLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drUBPCompanyLoop = dtUBPCompany.Select("UBPSCompanyID=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                    If drUBPCompanyLoop.Count = 0 Then
                                                        drUBPCompanyLoop = dtUBPCompany.Select("CompanyCode='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "'")
                                                    End If
                                                Else
                                                    drUBPCompanyLoop = dtUBPCompany.Select("CompanyCode='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "'")
                                                    If drUBPCompanyLoop.Count = 0 Then
                                                        drUBPCompanyLoop = dtUBPCompany.Select("CompanyName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "'")
                                                    End If
                                                End If
                                                If drUBPCompanyLoop.Count = 0 Then
                                                    Msg = "Invalid UBP Company."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid UBP Company."
                                                Exit For
                                            End If
                                        End If
                                        If objICUBPCompany.UBPSCompanyID IsNot Nothing Then
                                            If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "UBPReferenceFeild" Then
                                                If ICUBPSCompanyController.CheckReferenceFieldLength(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().Length.ToString, objICUBPCompany.UBPSCompanyID) = False Then
                                                    Msg = "Invalid UBP reference field."
                                                    Exit For
                                                End If
                                                If ICUBPSCompanyController.CheckReferenceFieldDataWithRegularExpression(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), objICUBPCompany.UBPSCompanyID) = False Then
                                                    Msg = "Invalid UBP reference data."
                                                    Exit For
                                                End If
                                            End If
                                        Else
                                            Msg = "Invalid UBP company"
                                            Exit For
                                        End If
                                    End If
                                Else

                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                        dtBankCodeLoop = New DataTable
                                        dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                        If dtBankCodeLoop.Rows.Count > 0 Then
                                            Dim i As Integer = 0
                                            If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                Dim drBankLoop As DataRow()
                                                drBankLoop = dtBankCodeLoop.Select("BankCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim())
                                                If drBankLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Bank Code."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "UBPCompanyCode" Then
                                        dtUBPCompany = New DataTable
                                        dtUBPCompany = DirectCast(ViewState("dtUBPCompany"), DataTable)
                                        If dtUBPCompany.Rows.Count > 0 Then
                                            Dim drUBPCompanyLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                drUBPCompanyLoop = dtUBPCompany.Select("UBPSCompanyID=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                If drUBPCompanyLoop.Count = 0 Then
                                                    drUBPCompanyLoop = dtUBPCompany.Select("CompanyCode='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "'")
                                                End If
                                            Else
                                                drUBPCompanyLoop = dtUBPCompany.Select("CompanyCode='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "'")
                                                If drUBPCompanyLoop.Count = 0 Then
                                                    drUBPCompanyLoop = dtUBPCompany.Select("CompanyName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "'")
                                                End If
                                            End If
                                            If drUBPCompanyLoop.Count = 0 Then
                                                Msg = "Invalid UBP Company."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid UBP Company."
                                            Exit For
                                        End If
                                    End If
                                End If
                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                    If objICUBPCompany IsNot Nothing Then
                                        If objICUBPCompany.MultiplesOfAmount = "Only Billed Amount" Then
                                            If FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() <> "0" Then
                                                Msg = Msg & " " & "Amount must be zero in case of only billed amount"
                                                Exit For
                                            End If

                                        End If
                                    Else
                                        Msg = Msg & " " & "Invalid UBP Company"
                                        Exit For
                                    End If
                                    If IsNumeric(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()) = True Then
                                        Try
                                            If Double.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), ResultDouble) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                        If objICUBPCompany.MultiplesOfAmount = "Only Billed Amount" Then
                                            If CDbl(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()) <> 0 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be equal to zero (0) in only billed amount"
                                                Exit For
                                            End If
                                        Else
                                            If CDbl(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()) < 1 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                Exit For
                                            End If
                                        End If


                                    Else
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                        Exit For
                                    End If
                                    If objICUBPCompany.MultiplesOfAmount <> "Only Billed Amount" Then
                                        Dim EnterdAmountRem As String = ""
                                        EnterdAmountRem = ICUBPSCompanyController.CheckUBPCompanyAmountAcceptance(CDbl(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()), objICUBPCompany.UBPSCompanyID.ToString)
                                        If EnterdAmountRem <> "OK" Then
                                            Msg = Msg & " " & EnterdAmountRem
                                            Exit For
                                        End If
                                    End If
                                End If
                                StartIndex = StartIndex + objAccountPaymentNatureTemplateField.FixLength
                            Next
                        Case "cotc"
                            For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields


                                Dim FieldTypeMessage As String = Nothing
                                FieldTypeMessage = ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBType(objAccountPaymentNatureTemplateField.FieldID, FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FieldType, objAccountPaymentNatureTemplateField.FixLength)
                                If Not FieldTypeMessage = "OK" Then
                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                                    Exit For
                                End If


                                If objAccountPaymentNatureTemplateField.IsRequiredCOTC = True Then
                                    If FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            If IsNumeric(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()) = True Then
                                                Try
                                                    If Double.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), ResultDouble) = False Then
                                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                        Exit For
                                                    End If
                                                Catch ex As Exception
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End Try
                                                If CDbl(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()) < 1 Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                    Exit For
                                                End If

                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If

                                        End If


                                        ''' value date
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For

                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For

                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                            dtBankCodeLoop = New DataTable
                                            dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                            If dtBankCodeLoop.Rows.Count > 0 Then
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    Dim drBankLoop As DataRow()
                                                    drBankLoop = dtBankCodeLoop.Select("BankCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim())
                                                    If drBankLoop.Count = 0 Then
                                                        Msg = "Invalid Bank Code."
                                                        Exit For
                                                    End If
                                                Else
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBranchCode" Then
                                            dtPrincipalBankBrLoop = New DataTable
                                            dtPrincipalBankBrLoop = DirectCast(ViewState("dtPrincipalBankBrLoop"), DataTable)
                                            If dtPrincipalBankBrLoop.Rows.Count > 0 Then
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower(), i) = True Then
                                                    Dim drPRincipalBankBrLoop As DataRow()
                                                    drPRincipalBankBrLoop = dtPrincipalBankBrLoop.Select("OfficeID=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                    If drPRincipalBankBrLoop.Count = 0 Then
                                                        Dim drPRincipalBankBrLoop2 As DataRow()
                                                        drPRincipalBankBrLoop2 = dtPrincipalBankBrLoop.Select("OfficeCode='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "'")
                                                        If drPRincipalBankBrLoop2.Count = 0 Then
                                                            Msg = "Invalid Pick Up Location."
                                                            Exit For
                                                        End If

                                                    End If
                                                Else
                                                    Dim drPRincipalBankBrLoop As DataRow()
                                                    drPRincipalBankBrLoop = dtPrincipalBankBrLoop.Select("OfficeCode='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "'")
                                                    If drPRincipalBankBrLoop.Count = 0 Then
                                                        Dim drPRincipalBankBrLoop2 As DataRow()
                                                        drPRincipalBankBrLoop2 = dtPrincipalBankBrLoop.Select("OfficeName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "'")
                                                        If drPRincipalBankBrLoop2.Count = 0 Then
                                                            Msg = "Invalid Pick Up Location."
                                                            Exit For
                                                        End If

                                                    End If
                                                End If
                                            Else
                                                Msg = "Invalid Pick Up Location."
                                                Exit For
                                            End If
                                        End If

                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryIdentificationType" Then
                                            BeneIDType = New ArrayList
                                            BeneIDType = ViewState("BeneIDType")
                                            If BeneIDType.Contains(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower) = False Then
                                                Msg = "Invalid Identification Type"
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        End If
                                    End If
                                Else

                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                    End If
                                End If

                                StartIndex = StartIndex + objAccountPaymentNatureTemplateField.FixLength
                            Next

                        Case "cheque"
                            For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields


                                Dim FieldTypeMessage As String = Nothing
                                FieldTypeMessage = ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBType(objAccountPaymentNatureTemplateField.FieldID, FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FieldType, objAccountPaymentNatureTemplateField.FixLength)
                                If Not FieldTypeMessage = "OK" Then
                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                                    Exit For
                                End If


                                If objAccountPaymentNatureTemplateField.IsRequiredCQ = True Then
                                    If FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            If IsNumeric(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()) = True Then
                                                Try
                                                    If Double.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), ResultDouble) = False Then
                                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                        Exit For
                                                    End If
                                                Catch ex As Exception
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End Try
                                                If CDbl(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()) < 1 Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                    Exit For
                                                End If

                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If

                                        End If


                                        ''' value date
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For

                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For

                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                            dtBankCodeLoop = New DataTable
                                            dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                            If dtBankCodeLoop.Rows.Count > 0 Then
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    Dim drBankLoop As DataRow()
                                                    drBankLoop = dtBankCodeLoop.Select("BankCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim())
                                                    If drBankLoop.Count = 0 Then
                                                        Msg = "Invalid Bank Code."
                                                        Exit For
                                                    End If
                                                Else
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drPrintLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                                Else
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                                End If
                                                If drPrintLocationLoop.Count = 0 Then
                                                    Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        End If

                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtDDPayableLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drdtDDPayableLocationLoop.Count = 0 Then
                                                    Msg = "Invalid DD Payable Location."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        End If
                                    End If
                                Else

                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                    End If
                                End If

                                StartIndex = StartIndex + objAccountPaymentNatureTemplateField.FixLength
                            Next
                        Case "dd"
                            For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                                Dim FieldTypeMessage As String = Nothing
                                FieldTypeMessage = ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBType(objAccountPaymentNatureTemplateField.FieldID, FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FieldType, objAccountPaymentNatureTemplateField.FixLength)
                                If Not FieldTypeMessage = "OK" Then
                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                                    Exit For
                                End If
                                If objAccountPaymentNatureTemplateField.IsRequiredDD = True Then
                                    If FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            If IsNumeric(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()) = True Then
                                                Try
                                                    If Double.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), ResultDouble) = False Then
                                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                        Exit For
                                                    End If
                                                Catch ex As Exception
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End Try
                                                If CDbl(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()) < 1 Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                    Exit For
                                                End If

                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If

                                        End If

                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For

                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                            dtBankCodeLoop = New DataTable
                                            dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                            If dtBankCodeLoop.Rows.Count > 0 Then
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    Dim drBankLoop As DataRow()
                                                    drBankLoop = dtBankCodeLoop.Select("BankCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim())
                                                    If drBankLoop.Count = 0 Then
                                                        Msg = "Invalid Bank Code."
                                                        Exit For
                                                    End If
                                                Else
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drPrintLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                                Else
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                                End If
                                                If drPrintLocationLoop.Count = 0 Then
                                                    Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then
                                            Dim dtBankBranches As New DataTable
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtDDPayableLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drdtDDPayableLocationLoop.Count = 0 Then
                                                    Msg = "Invalid DD Payable Location."
                                                    Exit For
                                                Else
                                                    dtBankBranches = ICOfficeController.GetAllPrinciaplBankBranchesByCityCode(drdtDDPayableLocationLoop(0)("CityCode").ToString)
                                                    If dtBankBranches.Rows.Count > 0 Then
                                                        Msg = "Principal Bank branch exists in specified city. DD cannot be made"
                                                        Exit For
                                                    End If
                                                End If
                                            Else
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        End If
                                    End If
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                    End If
                                End If

                                StartIndex = StartIndex + objAccountPaymentNatureTemplateField.FixLength
                            Next
                        Case "direct credit"
                            For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                                Dim FieldTypeMessage As String = Nothing
                                FieldTypeMessage = ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBType(objAccountPaymentNatureTemplateField.FieldID, FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FieldType, objAccountPaymentNatureTemplateField.FixLength)
                                If Not FieldTypeMessage = "OK" Then
                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                                    Exit For
                                End If
                                If objAccountPaymentNatureTemplateField.IsRequiredFT = True Then
                                    If FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            If IsNumeric(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()) = True Then
                                                Try
                                                    If Double.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), ResultDouble) = False Then
                                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                        Exit For
                                                    End If
                                                Catch ex As Exception
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End Try
                                                If CDbl(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()) < 1 Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                    Exit For
                                                End If

                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If

                                        End If

                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For

                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                            dtBankCodeLoop = New DataTable
                                            dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                            If dtBankCodeLoop.Rows.Count > 0 Then
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    Dim drBankLoop As DataRow()
                                                    drBankLoop = dtBankCodeLoop.Select("BankCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim())
                                                    If drBankLoop.Count = 0 Then
                                                        Msg = "Invalid Bank Code."
                                                        Exit For
                                                    End If
                                                Else
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drPrintLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                                Else
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                                End If
                                                If drPrintLocationLoop.Count = 0 Then
                                                    Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtDDPayableLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drdtDDPayableLocationLoop.Count = 0 Then
                                                    Msg = "Invalid DD Payable Location."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryAccountNo" Then

                                            If Regex.IsMatch(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), "^[0-9]+$") = False Then
                                                Msg = Msg & " " & " Invalid Beneficiary Account Number"
                                                Exit For
                                            End If



                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryAccountType" Then
                                        If FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower <> "gl" And FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower <> "rb" Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " invalid beneficiary account type"
                                            Exit For
                                        Else
                                            If FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower = "gl" Then

                                                For Each objICAPNPTypeFields As ICAccountPaymentNatureFileUploadTemplatefields In collAccountPaymentNatureTemplateFields
                                                    Select Case objICAPNPTypeFields.FieldName
                                                        Case "BeneficiaryAccountNo"
                                                            If FileLine.Substring(StartIndex, objICAPNPTypeFields.FixLength).ToString().Trim().Length <> 17 Then
                                                                Msg = Msg & " " & "Invalid Beneficiary Account Number Length"
                                                                Exit For
                                                            End If
                                                    End Select
                                                Next
                                            End If
                                        End If



                                    End If
                                    End If
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryAccountNo" Then
                                        If Not FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                            If Regex.IsMatch(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), "^[0-9]+$") = False Then
                                                Msg = Msg & " " & " Invalid Beneficiary Account Number"
                                                Exit For
                                            End If
                                        End If
                                        



                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryAccountType" Then
                                        If Not FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                            If FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower <> "gl" And FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower <> "rb" Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " invalid beneficiary account type"
                                                Exit For
                                            Else
                                                If FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower = "gl" Then

                                                    For Each objICAPNPTypeFields As ICAccountPaymentNatureFileUploadTemplatefields In collAccountPaymentNatureTemplateFields
                                                        Select Case objICAPNPTypeFields.FieldName
                                                            Case "BeneficiaryAccountNo"
                                                                If FileLine.Substring(StartIndex, objICAPNPTypeFields.FixLength).ToString().Trim().Length <> 17 Then
                                                                    Msg = Msg & " " & "Invalid Beneficiary Account Number Length"
                                                                    Exit For
                                                                End If
                                                        End Select
                                                    Next
                                                End If
                                            End If

                                        End If

                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                    End If
                                    If Not FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then

                                    End If

                                End If
                                StartIndex = StartIndex + objAccountPaymentNatureTemplateField.FixLength
                            Next
                        Case "other credit"
                            For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                                Dim FieldTypeMessage As String = Nothing
                                FieldTypeMessage = ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBType(objAccountPaymentNatureTemplateField.FieldID, FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FieldType, objAccountPaymentNatureTemplateField.FixLength)
                                If Not FieldTypeMessage = "OK" Then
                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                                    Exit For
                                End If

                                If objAccountPaymentNatureTemplateField.IsRequiredIBFT = True Then
                                    If FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            If IsNumeric(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()) = True Then
                                                Try
                                                    If Double.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), ResultDouble) = False Then
                                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                        Exit For
                                                    End If
                                                Catch ex As Exception
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End Try
                                                If CDbl(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()) < 1 Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                    Exit For
                                                End If

                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If

                                        End If

                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For

                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                            dtBankCodeLoop = New DataTable
                                            dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                            If dtBankCodeLoop.Rows.Count > 0 Then
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    Dim drBankLoop As DataRow()
                                                    drBankLoop = dtBankCodeLoop.Select("BankCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim())
                                                    If drBankLoop.Count = 0 Then
                                                        Msg = "Invalid Bank Code."
                                                        Exit For
                                                    End If
                                                Else
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drPrintLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                                Else
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                                End If
                                                If drPrintLocationLoop.Count = 0 Then
                                                    Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtDDPayableLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drdtDDPayableLocationLoop.Count = 0 Then
                                                    Msg = "Invalid DD Payable Location."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        End If
                                    End If
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                    End If

                                End If
                                StartIndex = StartIndex + objAccountPaymentNatureTemplateField.FixLength
                            Next
                        Case "po"
                            For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                                Dim FieldTypeMessage As String = Nothing
                                FieldTypeMessage = ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBType(objAccountPaymentNatureTemplateField.FieldID, FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FieldType, objAccountPaymentNatureTemplateField.FixLength)
                                If Not FieldTypeMessage = "OK" Then
                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                                    Exit For
                                End If


                                If objAccountPaymentNatureTemplateField.IsRequiredPO = True Then
                                    If FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            If IsNumeric(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()) = True Then
                                                Try
                                                    If Double.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), ResultDouble) = False Then
                                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                        Exit For
                                                    End If
                                                Catch ex As Exception
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End Try
                                                If CDbl(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim()) < 1 Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                    Exit For
                                                End If

                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If

                                        End If

                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                            dtBankCodeLoop = New DataTable
                                            dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                            If dtBankCodeLoop.Rows.Count > 0 Then
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    Dim drBankLoop As DataRow()
                                                    drBankLoop = dtBankCodeLoop.Select("BankCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim())
                                                    If drBankLoop.Count = 0 Then
                                                        Msg = "Invalid Bank Code."
                                                        Exit For
                                                    End If
                                                Else
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drPrintLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                                Else
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                                End If
                                                If drPrintLocationLoop.Count = 0 Then
                                                    Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtDDPayableLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drdtDDPayableLocationLoop.Count = 0 Then
                                                    Msg = "Invalid DD Payable Location."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim().ToLower() & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        End If
                                    End If
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                    End If

                                End If
                                StartIndex = StartIndex + objAccountPaymentNatureTemplateField.FixLength
                            Next
                    End Select
                End If
            Else  'CSV File                
                arrInsruction = FileLine.Split(FileTemplate.FieldDelimiter.ToString().Trim())
                For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields

                    Dim FieldTypeMessage As String = Nothing
                    FieldTypeMessage = ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBType(objAccountPaymentNatureTemplateField.FieldID, arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FieldType, objAccountPaymentNatureTemplateField.FixLength)
                    If Not FieldTypeMessage = "OK" Then
                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                        Exit For
                    End If
                    ' Check DB Maximum Length
                    If ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBLength(objAccountPaymentNatureTemplateField.FieldID, arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FixLength) = False Then
                        Msg = Msg & " Database field length for " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " exceeded."
                        Exit For
                    End If
                    ' Get Product Type
                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ProductTypeCode" Then
                        If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                            Msg = "Product Type Code is empty."
                            Exit For
                        Else
                            dtProductTypeLoop = New DataTable
                            dtProductTypeLoop = DirectCast(ViewState("dtProductTypeLoop"), DataTable)
                            Dim drProductTypeRow As DataRow()
                            drProductTypeRow = dtProductTypeLoop.Select("ProductTypeCode='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "'")
                            If dtProductTypeLoop.Rows.Count > 0 Then
                                If drProductTypeRow.Count = 0 Then
                                    Msg = "Invalid Product Type Code."
                                    Exit For
                                Else
                                    Dim objProductType As New ICProductType
                                    If objProductType.LoadByPrimaryKey(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()) Then
                                        PaymentMode = objProductType.DisbursementMode.ToString().ToLower()
                                        ProductTypeCode = Nothing
                                        ProductTypeCode = objProductType.ProductTypeCode.ToString
                                        hfDualDisbMode.Value = PaymentMode.ToString()
                                    Else
                                        Msg = "Invalid Product Type Code."
                                        Exit For
                                    End If
                                End If
                            Else
                                Msg = "Invalid Product Type Code."
                                Exit For
                            End If
                        End If
                    End If
                Next
                If PaymentMode.ToString() <> "" Then
                    ' Check Product Type wise Required Fields Data
                    Select Case PaymentMode.ToString()
                        Case "bill payment"

                            For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                                Dim objICUBPCompany As New ICUBPSCompany
                                For Each objICAPNPTypeFields As ICAccountPaymentNatureFileUploadTemplatefields In collAccountPaymentNatureTemplateFields
                                    Select Case objICAPNPTypeFields.FieldName
                                        Case "UBPCompanyCode"
                                            Dim UBPSCompanyID As String = ""
                                            UBPSCompanyID = ICUBPSCompanyController.GetUBPCompanyIDByCompanyNameOrCodeorID(arrInsruction(objICAPNPTypeFields.FieldOrder - 1).ToString().Trim())
                                            If UBPSCompanyID <> "" Then
                                                If objICUBPCompany.LoadByPrimaryKey(UBPSCompanyID) = True Then
                                                Else
                                                    Msg = Msg & " " & "Invalid UBP Company Code"
                                                    Exit For
                                                End If
                                            Else
                                                Msg = Msg & " " & "Invalid UBP Company Code"
                                                Exit For
                                            End If

                                    End Select
                                Next
                                If Msg <> "" Then
                                    Exit For
                                End If
                                If objAccountPaymentNatureTemplateField.IsRequiredBillPayment = True Then

                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then

                                            'Dim objICAPNPTypeFields As New ICAccountPaymentNatureFileUploadTemplatefields
                                            If objICUBPCompany IsNot Nothing Then
                                                If objICUBPCompany.MultiplesOfAmount = "Only Billed Amount" Then
                                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() <> "0" Then
                                                        Msg = Msg & " " & "Amount must be zero in case of only billed amount"
                                                        Exit For
                                                    End If

                                                End If
                                            Else
                                                Msg = Msg & " " & "Invalid UBP Company"
                                                Exit For
                                            End If

                                            Try
                                                If Double.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), ResultDouble) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                            If objICUBPCompany.MultiplesOfAmount = "Only Billed Amount" Then
                                                If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()) <> 0 Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be equal to zero (0) in only billed amount"
                                                    Exit For
                                                End If
                                            Else
                                                If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()) < 1 Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                    Exit For
                                                End If
                                            End If

                                            If IsNumeric(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()) = True Then
                                                'If Double.TryParse(FileLine.Substring(StartIndex, AcqAgentTemplateFields.FixLength).ToString().Trim(), result) = False Then
                                                '    Msg = Msg & " " & AcqAgentTemplateFields.FieldName.ToString() & " is not in correct format."
                                                '    Exit For
                                                'End If
                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                            If objICUBPCompany.MultiplesOfAmount <> "Only Billed Amount" Then
                                                Dim EnterdAmountRem As String = ""
                                                EnterdAmountRem = ICUBPSCompanyController.CheckUBPCompanyAmountAcceptance(CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()), objICUBPCompany.UBPSCompanyID)
                                                If EnterdAmountRem <> "OK" Then
                                                    Msg = Msg & " " & EnterdAmountRem
                                                    Exit For
                                                End If
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                            Dim i As Integer = 0
                                            dtBankCodeLoop = New DataTable
                                            dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                            If dtBankCodeLoop.Rows.Count > 0 Then
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    Dim drBankLoop As DataRow()
                                                    drBankLoop = dtBankCodeLoop.Select("BankCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim())
                                                    If drBankLoop.Count = 0 Then
                                                        Msg = "Invalid Bank Code."
                                                        Exit For
                                                    End If
                                                Else
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If

                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBranchCode" Then
                                            dtPrincipalBankBrLoop = New DataTable
                                            dtPrincipalBankBrLoop = DirectCast(ViewState("dtPrincipalBankBrLoop"), DataTable)
                                            If dtPrincipalBankBrLoop.Rows.Count > 0 Then
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    Dim drPRincipalBankBrLoop As DataRow()
                                                    drPRincipalBankBrLoop = dtPrincipalBankBrLoop.Select("OfficeID=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                    If drPRincipalBankBrLoop.Count = 0 Then
                                                        Dim drPRincipalBankBrLoop2 As DataRow()
                                                        drPRincipalBankBrLoop2 = dtPrincipalBankBrLoop.Select("OfficeCode='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "'")

                                                        If drPRincipalBankBrLoop2.Count = 0 Then
                                                            Msg = "Invalid Pick Up Location."
                                                            Exit For
                                                        End If
                                                    End If
                                                Else
                                                    Dim drPRincipalBankBrLoop As DataRow()
                                                    drPRincipalBankBrLoop = dtPrincipalBankBrLoop.Select("OfficeCode='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "'")
                                                    If drPRincipalBankBrLoop.Count = 0 Then
                                                        Dim drPRincipalBankBrLoop2 As DataRow()
                                                        drPRincipalBankBrLoop2 = dtPrincipalBankBrLoop.Select("OfficeName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                        If drPRincipalBankBrLoop2.Count = 0 Then
                                                            Msg = "Invalid Pick Up Location."
                                                            Exit For
                                                        End If

                                                    End If
                                                End If
                                            Else
                                                Msg = "Invalid Pick Up Location."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryIdentificationType" Then
                                            If BeneIDType.Contains(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower) = False Then
                                                Msg = "Invalid Identification Type"
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "UBPCompanyCode" Then
                                            dtUBPCompany = New DataTable
                                            dtUBPCompany = DirectCast(ViewState("dtUBPCompany"), DataTable)
                                            If dtUBPCompany.Rows.Count > 0 Then
                                                Dim drUBPCompanyLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drUBPCompanyLoop = dtUBPCompany.Select("UBPSCompanyID=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                    If drUBPCompanyLoop.Count = 0 Then
                                                        drUBPCompanyLoop = dtUBPCompany.Select("CompanyCode='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "'")
                                                    End If
                                                Else
                                                    drUBPCompanyLoop = dtUBPCompany.Select("CompanyCode='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "'")
                                                    If drUBPCompanyLoop.Count = 0 Then
                                                        drUBPCompanyLoop = dtUBPCompany.Select("CompanyName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "'")
                                                    End If
                                                End If
                                                If drUBPCompanyLoop.Count = 0 Then
                                                    Msg = "Invalid UBP Company."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid UBP Company."
                                                Exit For
                                            End If
                                        End If
                                        If objICUBPCompany.UBPSCompanyID IsNot Nothing Then
                                            If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "UBPReferenceFeild" Then
                                                If ICUBPSCompanyController.CheckReferenceFieldLength(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), objICUBPCompany.UBPSCompanyID) = False Then
                                                    Msg = "Invalid UBP reference field."
                                                    Exit For
                                                End If
                                                If ICUBPSCompanyController.CheckReferenceFieldDataWithRegularExpression(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), objICUBPCompany.UBPSCompanyID) = False Then
                                                    Msg = "Invalid UBP reference data."
                                                    Exit For
                                                End If
                                            End If
                                        End If
                                    End If
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                        Dim i As Integer = 0
                                        dtBankCodeLoop = New DataTable
                                        dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                        If dtBankCodeLoop.Rows.Count > 0 Then
                                            If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                Dim drBankLoop As DataRow()
                                                drBankLoop = dtBankCodeLoop.Select("BankCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim())
                                                If drBankLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If

                                        Else
                                            Msg = "Invalid Bank Code."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "UBPCompanyCode" Then
                                        dtUBPCompany = New DataTable
                                        dtUBPCompany = DirectCast(ViewState("dtUBPCompany"), DataTable)
                                        If dtUBPCompany.Rows.Count > 0 Then
                                            Dim drUBPCompanyLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                drUBPCompanyLoop = dtUBPCompany.Select("UBPSCompanyID=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                If drUBPCompanyLoop.Count = 0 Then
                                                    drUBPCompanyLoop = dtUBPCompany.Select("CompanyCode='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "'")
                                                End If
                                            Else
                                                drUBPCompanyLoop = dtUBPCompany.Select("CompanyCode='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "'")
                                                If drUBPCompanyLoop.Count = 0 Then
                                                    drUBPCompanyLoop = dtUBPCompany.Select("CompanyName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "'")
                                                End If
                                            End If
                                            If drUBPCompanyLoop.Count = 0 Then
                                                Msg = "Invalid UBP Company."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid UBP Company."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then


                                        If objICUBPCompany IsNot Nothing Then
                                            If objICUBPCompany.MultiplesOfAmount = "Only Billed Amount" Then
                                                If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() <> "0" Then
                                                    Msg = Msg & " " & "Amount must be zero in case of only billed amount"
                                                    Exit For
                                                End If

                                            End If
                                        Else
                                            Msg = Msg & " " & "Invalid UBP Company"
                                            Exit For
                                        End If
                                        Try
                                            If Double.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), ResultDouble) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                        If objICUBPCompany.MultiplesOfAmount = "Only Billed Amount" Then
                                            If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()) <> 0 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be equal to zero (0) in only billed amount"
                                                Exit For
                                            End If
                                        Else
                                            If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()) < 1 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                Exit For
                                            End If
                                        End If

                                        If IsNumeric(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()) = True Then
                                          
                                        Else
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End If
                                        If objICUBPCompany.MultiplesOfAmount <> "Only Billed Amount" Then
                                            Dim EnterdAmountRem As String = ""
                                            EnterdAmountRem = ICUBPSCompanyController.CheckUBPCompanyAmountAcceptance(CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()), objICUBPCompany.UBPSCompanyID)
                                            If EnterdAmountRem <> "OK" Then
                                                Msg = Msg & " " & EnterdAmountRem
                                                Exit For
                                            End If
                                        End If
                                    End If
                                End If
                            Next
                        Case "cotc"
                            For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredCOTC = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            Try
                                                If Double.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), ResultDouble) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                            If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()) < 1 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                Exit For
                                            End If
                                            If IsNumeric(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()) = True Then
                                               
                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                            Dim i As Integer = 0
                                            dtBankCodeLoop = New DataTable
                                            dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                            If dtBankCodeLoop.Rows.Count > 0 Then
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    Dim drBankLoop As DataRow()
                                                    drBankLoop = dtBankCodeLoop.Select("BankCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim())
                                                    If drBankLoop.Count = 0 Then
                                                        Msg = "Invalid Bank Code."
                                                        Exit For
                                                    End If
                                                Else
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If

                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBranchCode" Then
                                            dtPrincipalBankBrLoop = New DataTable
                                            dtPrincipalBankBrLoop = DirectCast(ViewState("dtPrincipalBankBrLoop"), DataTable)
                                            If dtPrincipalBankBrLoop.Rows.Count > 0 Then
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    Dim drPRincipalBankBrLoop As DataRow()
                                                    drPRincipalBankBrLoop = dtPrincipalBankBrLoop.Select("OfficeID=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                    If drPRincipalBankBrLoop.Count = 0 Then
                                                        Dim drPRincipalBankBrLoop2 As DataRow()
                                                        drPRincipalBankBrLoop2 = dtPrincipalBankBrLoop.Select("OfficeCode='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "'")

                                                        If drPRincipalBankBrLoop2.Count = 0 Then
                                                            Msg = "Invalid Pick Up Location."
                                                            Exit For
                                                        End If
                                                    End If
                                                Else
                                                    Dim drPRincipalBankBrLoop As DataRow()
                                                    drPRincipalBankBrLoop = dtPrincipalBankBrLoop.Select("OfficeCode='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "'")
                                                    If drPRincipalBankBrLoop.Count = 0 Then
                                                        Dim drPRincipalBankBrLoop2 As DataRow()
                                                        drPRincipalBankBrLoop2 = dtPrincipalBankBrLoop.Select("OfficeName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                        If drPRincipalBankBrLoop2.Count = 0 Then
                                                            Msg = "Invalid Pick Up Location."
                                                            Exit For
                                                        End If

                                                    End If
                                                End If
                                            Else
                                                Msg = "Invalid Pick Up Location."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryIdentificationType" Then
                                            If BeneIDType.Contains(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower) = False Then
                                                Msg = "Invalid Identification Type"
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        End If
                                    End If
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                End If
                            Next
                        Case "cheque"
                            For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredCQ = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            Try
                                                If Double.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), ResultDouble) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                            If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()) < 1 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                Exit For
                                            End If
                                            If IsNumeric(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()) = True Then
                                               
                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                            Dim i As Integer = 0
                                            dtBankCodeLoop = New DataTable
                                            dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                            If dtBankCodeLoop.Rows.Count > 0 Then
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    Dim drBankLoop As DataRow()
                                                    drBankLoop = dtBankCodeLoop.Select("BankCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim())
                                                    If drBankLoop.Count = 0 Then
                                                        Msg = "Invalid Bank Code."
                                                        Exit For
                                                    End If
                                                Else
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If

                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drPrintLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                                Else
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower() & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                                End If
                                                If drPrintLocationLoop.Count = 0 Then
                                                    Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtDDPayableLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtDDPayableLocationLoop.Count = 0 Then
                                                    Msg = "Invalid DD Payable Location."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        End If
                                    End If
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                End If
                            Next
                        Case "dd"
                            For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredDD = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            Try
                                                If Double.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), ResultDouble) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                            If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()) < 1 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                Exit For
                                            End If
                                            If IsNumeric(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()) = True Then
                                               
                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                            If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                                Try
                                                    Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                    Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                    Dim ResultDate As Date = Nothing
                                                    If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                        Exit For
                                                    ElseIf ValueDate < Now.Date Then
                                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                        Exit For
                                                    End If
                                                Catch ex As Exception
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End Try
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                            If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                                Try
                                                    Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                    Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                    Dim ResultDate As Date = Nothing
                                                    If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                        Exit For
                                                    ElseIf ValueDate < Now.Date Then
                                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                        Exit For
                                                    End If
                                                Catch ex As Exception
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End Try
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                            Dim i As Integer = 0
                                            dtBankCodeLoop = New DataTable
                                            dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                            If dtBankCodeLoop.Rows.Count > 0 Then
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    Dim drBankLoop As DataRow()
                                                    drBankLoop = dtBankCodeLoop.Select("BankCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim())
                                                    If drBankLoop.Count = 0 Then
                                                        Msg = "Invalid Bank Code."
                                                        Exit For
                                                    End If
                                                Else
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If

                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drPrintLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                                Else
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower() & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                                End If
                                                If drPrintLocationLoop.Count = 0 Then
                                                    Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then
                                            Dim dtBankBranches As New DataTable
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtDDPayableLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtDDPayableLocationLoop.Count = 0 Then
                                                    Msg = "Invalid DD Payable Location."
                                                    Exit For
                                                Else
                                                    dtBankBranches = ICOfficeController.GetAllPrinciaplBankBranchesByCityCode(drdtDDPayableLocationLoop(0)("CityCode").ToString)
                                                    If dtBankBranches.Rows.Count > 0 Then
                                                        Msg = "Principal Bank branch exists in specified city. DD cannot be made"
                                                        Exit For
                                                    End If
                                                End If
                                            Else
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        End If
                                    End If
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                End If
                            Next
                        Case "direct credit"
                            For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredFT = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            Try
                                                If Double.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), ResultDouble) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                            If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()) < 1 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                Exit For
                                            End If
                                            If IsNumeric(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()) = True Then
                                               
                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                            Dim i As Integer = 0
                                            dtBankCodeLoop = New DataTable
                                            dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                            If dtBankCodeLoop.Rows.Count > 0 Then
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    Dim drBankLoop As DataRow()
                                                    drBankLoop = dtBankCodeLoop.Select("BankCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim())
                                                    If drBankLoop.Count = 0 Then
                                                        Msg = "Invalid Bank Code."
                                                        Exit For
                                                    End If
                                                Else
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If

                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drPrintLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                                Else
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower() & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                                End If
                                                If drPrintLocationLoop.Count = 0 Then
                                                    Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtDDPayableLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtDDPayableLocationLoop.Count = 0 Then
                                                    Msg = "Invalid DD Payable Location."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryAccountNo" Then

                                            If Regex.IsMatch(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), "^[0-9]+$") = False Then
                                                Msg = Msg & " " & " Invalid Beneficiary Account Number"
                                                Exit For
                                            End If





                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryAccountType" Then

                                            If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower <> "gl" And arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower <> "rb" Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " invalid beneficiary account type"
                                                Exit For
                                            Else
                                                If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower = "gl" Then

                                                    For Each objICAPNPTypeFields As ICAccountPaymentNatureFileUploadTemplatefields In collAccountPaymentNatureTemplateFields
                                                        Select Case objICAPNPTypeFields.FieldName
                                                            Case "BeneficiaryAccountNo"
                                                                If arrInsruction(objICAPNPTypeFields.FieldOrder - 1).ToString().Trim().ToLower.Length <> 17 Then
                                                                    Msg = Msg & " " & objICAPNPTypeFields.FieldName & "Invalid Beneficiary Account Number Length"
                                                                    Exit For
                                                                End If
                                                        End Select
                                                    Next
                                                End If
                                            End If



                                        End If

                                    End If
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryAccountNo" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                            If System.Text.RegularExpressions.Regex.IsMatch(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), "^[0-9]+$") = False Then
                                                Msg = Msg & " " & " Invalid Beneficiary Account Number"
                                                Exit For
                                            End If
                                        End If

                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryAccountType" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                            If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower <> "gl" And arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower <> "rb" Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " invalid beneficiary account type"
                                                Exit For
                                            Else
                                                If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower = "gl" Then

                                                    For Each objICAPNPTypeFields As ICAccountPaymentNatureFileUploadTemplatefields In collAccountPaymentNatureTemplateFields
                                                        Select Case objICAPNPTypeFields.FieldName
                                                            Case "BeneficiaryAccountNo"
                                                                If arrInsruction(objICAPNPTypeFields.FieldOrder - 1).ToString().Trim().ToLower.Length <> 17 Then
                                                                    Msg = Msg & " " & objICAPNPTypeFields.FieldName & "Invalid Beneficiary Account Number Length"
                                                                    Exit For
                                                                End If
                                                        End Select
                                                    Next
                                                End If
                                            End If

                                        End If

                                    End If


                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                End If
                            Next
                        Case "other credit"
                            For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredIBFT = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            Try
                                                If Double.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), ResultDouble) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                            If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()) < 1 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                Exit For
                                            End If
                                            If IsNumeric(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()) = True Then
                                               
                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                            Dim i As Integer = 0
                                            dtBankCodeLoop = New DataTable
                                            dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                            If dtBankCodeLoop.Rows.Count > 0 Then
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    Dim drBankLoop As DataRow()
                                                    drBankLoop = dtBankCodeLoop.Select("BankCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim())
                                                    If drBankLoop.Count = 0 Then
                                                        Msg = "Invalid Bank Code."
                                                        Exit For
                                                    End If
                                                Else
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If

                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drPrintLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                                Else
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower() & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                                End If
                                                If drPrintLocationLoop.Count = 0 Then
                                                    Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtDDPayableLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtDDPayableLocationLoop.Count = 0 Then
                                                    Msg = "Invalid DD Payable Location."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        End If
                                    End If
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                End If
                            Next
                        Case "po"
                            For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredPO = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            Try
                                                If Double.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), ResultDouble) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                            If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()) < 1 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                Exit For
                                            End If
                                            If IsNumeric(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()) = True Then
                                              
                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                            Dim i As Integer = 0
                                            dtBankCodeLoop = New DataTable
                                            dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                            If dtBankCodeLoop.Rows.Count > 0 Then
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    Dim drBankLoop As DataRow()
                                                    drBankLoop = dtBankCodeLoop.Select("BankCode=" & FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FixLength).ToString().Trim())
                                                    If drBankLoop.Count = 0 Then
                                                        Msg = "Invalid Bank Code."
                                                        Exit For
                                                    End If
                                                Else
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If

                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drPrintLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                                Else
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower() & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                                End If
                                                If drPrintLocationLoop.Count = 0 Then
                                                    Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtDDPayableLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtDDPayableLocationLoop.Count = 0 Then
                                                    Msg = "Invalid DD Payable Location."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim().ToLower & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        End If
                                    End If
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If

                                End If
                            Next
                    End Select
                End If
            End If
            Return Msg
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function CheckDataTXTDualFile(ByVal arrInsruction As String(), ByVal HeadercollAccountPaymentNatureTemplateFields As ICAccountPaymentNatureFileUploadTemplatefieldsCollection, ByVal DetailcollAccountPaymentNatureTemplateFields As ICAccountPaymentNatureFileUploadTemplatefieldsCollection, ByVal FileTemplate As ICTemplate, ByVal LineType As String, ByVal LoopCount As Integer) As String
        Dim objAccountPaymentNatureTemplateField As ICAccountPaymentNatureFileUploadTemplatefields

        Dim Msg As String = ""
        Dim PaymentMode As String = ""
        Dim StartIndex As Integer = 0
        Dim result As Integer
        Dim ResultDouble As Double
        Dim cnt As Integer = 0
        Dim ProductTypeCode As String = Nothing
        Dim dtProductTypeLoop As DataTable
        Dim dtBankCodeLoop As DataTable
        Dim dtBankNameLoop As DataTable
        Dim dtPrintLocationLoop As DataTable
        Dim dtPrincipalBankBrLoop As DataTable
        Dim dtProductType As New DataTable
        Dim dtUBPCompany As New DataTable
        Dim BeneIDType As New ArrayList
        Try
            If LineType.ToString() = "Header" Then
                If LoopCount = 0 Then
                    objAccountPaymentNatureTemplateField = New ICAccountPaymentNatureFileUploadTemplatefields
                    objAccountPaymentNatureTemplateField = HeadercollAccountPaymentNatureTemplateFields(0)
                    '' Product Type
                    dtProductTypeLoop = New DataTable
                    ViewState("dtProductTypeLoop") = Nothing
                    dtProductTypeLoop = ICProductTypeController.IsProductTypeExistByProductTypeCode2(FileTemplate.TemplateID.ToString, objAccountPaymentNatureTemplateField)
                    ViewState("dtProductTypeLoop") = dtProductTypeLoop
                    '' Banks
                    dtBankCodeLoop = New DataTable
                    ViewState("dtBankCodeLoop") = Nothing
                    dtBankCodeLoop = ICBankController.IsBankExistByBankCodeForBulkUpLoad
                    ViewState("dtBankCodeLoop") = dtBankCodeLoop

                    dtBankNameLoop = New DataTable
                    ViewState("dtBankNameLoop") = Nothing
                    dtBankNameLoop = dtBankCodeLoop.Clone
                    dtBankNameLoop = dtBankCodeLoop
                    ViewState("dtBankNameLoop") = dtBankNameLoop
                    '' Print Location
                    dtPrintLocationLoop = New DataTable
                    ViewState("dtPrintLocationLoop") = Nothing
                    dtPrintLocationLoop = ICOfficeController.IsPrintLocationExistByPrintLocationNameOrCodeForBulkUpLoad(objAccountPaymentNatureTemplateField.AccountNumber.ToString, objAccountPaymentNatureTemplateField.BranchCode.ToString, objAccountPaymentNatureTemplateField.Currency.ToString, objAccountPaymentNatureTemplateField.PaymentNatureCode.ToString)
                    ViewState("dtPrintLocationLoop") = dtPrintLocationLoop
                    '' DD Payable Location

                    dtPrintLocationLoop = New DataTable
                    ViewState("dtDDPayableLocationLoop") = Nothing
                    dtPrintLocationLoop = ICCityController.IsCityExistByCityNameOrCodeForBulkUpload()
                    ViewState("dtDDPayableLocationLoop") = dtPrintLocationLoop
                    '' Beneficiary City
                    ViewState("dtBeneficiaryCityLoop") = Nothing
                    ViewState("dtBeneficiaryCityLoop") = dtPrintLocationLoop
                    ''Bene Province
                    dtPrintLocationLoop = New DataTable
                    ViewState("dtBeneficiaryProvinceLoop") = Nothing
                    dtPrintLocationLoop = ICProvinceController.IsProvinceExistByCountryNameOrCodeForBulkUpload
                    ViewState("dtBeneficiaryProvinceLoop") = dtPrintLocationLoop

                    ''Bene Country
                    dtPrintLocationLoop = New DataTable
                    ViewState("dtBeneficiaryCountryLoop") = Nothing
                    dtPrintLocationLoop = ICCountryController.IsCountryExistByCountryNameForBulkUpload()
                    ViewState("dtBeneficiaryCountryLoop") = dtPrintLocationLoop


                    ''Bene PICK Up Location For COTC
                    dtPrincipalBankBrLoop = New DataTable
                    ViewState("dtPrincipalBankBrLoop") = Nothing
                    dtPrincipalBankBrLoop = ICOfficeController.GetPrincipalBankBranchActiveAndApprove
                    ViewState("dtPrincipalBankBrLoop") = dtPrincipalBankBrLoop

                    '' Bene ID Type 
                    BeneIDType.Add("cnic")
                    BeneIDType.Add("passport")
                    BeneIDType.Add("nicop")
                    BeneIDType.Add("driving license")
                    ViewState("BeneIDType") = BeneIDType


                    ''UBPCompany
                    dtUBPCompany = New DataTable
                    ViewState("dtUBPCompany") = Nothing
                    dtUBPCompany = ICUBPSCompanyController.GetAllUBPActiveApproveCompany
                    ViewState("dtUBPCompany") = dtUBPCompany
                End If
                objAccountPaymentNatureTemplateField = New ICAccountPaymentNatureFileUploadTemplatefields
                For Each objAccountPaymentNatureTemplateField In HeadercollAccountPaymentNatureTemplateFields
                    ' Check DB Maximum Length
                    If ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBLength(objAccountPaymentNatureTemplateField.FieldID, arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName.ToString, objAccountPaymentNatureTemplateField.FixLength) = False Then
                        Msg = Msg & " Database field length for " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " exceeded."
                        Exit For
                    End If
                    Dim FieldTypeMessage As String = Nothing
                    FieldTypeMessage = ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBType(objAccountPaymentNatureTemplateField.FieldID, arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName.ToString, objAccountPaymentNatureTemplateField.FieldType.ToString, objAccountPaymentNatureTemplateField.FixLength)
                    If Not FieldTypeMessage = "OK" Then
                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                        Exit For
                    End If

                    ' Get Product Type
                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ProductTypeCode" Then
                        If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                            Msg = "Product Type Code is empty."
                            Exit For
                        Else
                            dtProductTypeLoop = New DataTable
                            dtProductTypeLoop = DirectCast(ViewState("dtProductTypeLoop"), DataTable)
                            Dim drProductTypeRow As DataRow()
                            drProductTypeRow = dtProductTypeLoop.Select("ProductTypeCode='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "'")
                            If dtProductTypeLoop.Rows.Count > 0 Then
                                If drProductTypeRow.Count = 0 Then
                                    Msg = "Invalid Product Type Code."
                                    Exit For
                                Else
                                    Dim objProductType As New ICProductType
                                    If objProductType.LoadByPrimaryKey(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) Then
                                        PaymentMode = objProductType.DisbursementMode.ToString().ToLower()
                                        ProductTypeCode = Nothing
                                        ProductTypeCode = objProductType.ProductTypeCode.ToString
                                        hfDualDisbMode.Value = PaymentMode.ToString()
                                    Else
                                        Msg = "Invalid Product Type Code."
                                        Exit For
                                    End If
                                End If
                            Else
                                Msg = "Invalid Product Type Code."
                                Exit For
                            End If
                        End If
                    End If
                Next
                If PaymentMode.ToString() <> "" Then
                    ' Check Product Type wise Required Fields Data
                    Select Case PaymentMode.ToString()

                        Case "bill payment"
                            Dim objICUBPCompany As New ICUBPSCompany
                            For Each objICAPNPTypeFields As ICAccountPaymentNatureFileUploadTemplatefields In HeadercollAccountPaymentNatureTemplateFields
                                Select Case objICAPNPTypeFields.FieldName
                                    Case "UBPCompanyCode"
                                        Dim UBPSCompanyID As String = ""
                                        UBPSCompanyID = ICUBPSCompanyController.GetUBPCompanyIDByCompanyNameOrCodeorID(arrInsruction(objICAPNPTypeFields.FieldOrder).ToString().Trim())
                                        If UBPSCompanyID <> "" Then
                                            If objICUBPCompany.LoadByPrimaryKey(UBPSCompanyID) = True Then
                                            Else
                                                Msg = Msg & " " & "Invalid UBP Company Code"
                                                Exit For
                                            End If
                                        Else
                                            Msg = Msg & " " & "Invalid UBP Company Code"
                                            Exit For
                                        End If

                                End Select
                            Next
                            If Msg <> "" Then
                                Return Msg
                                Exit Function
                            End If

                            For Each objAccountPaymentNatureTemplateField In HeadercollAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredBillPayment = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            If objICUBPCompany IsNot Nothing Then
                                                If objICUBPCompany.MultiplesOfAmount = "Only Billed Amount" Then
                                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() <> "0" Then
                                                        Msg = Msg & " " & "Amount must be zero in case of only billed amount"
                                                        Exit For
                                                    End If

                                                End If
                                            Else
                                                Msg = Msg & " " & "Invalid UBP Company"
                                                Exit For
                                            End If

                                            Try
                                                If Double.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), ResultDouble) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                            If objICUBPCompany.MultiplesOfAmount = "Only Billed Amount" Then
                                                If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) <> 0 Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be equal to zero (0) in only billed amount"
                                                    Exit For
                                                End If
                                            Else
                                                If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) < 1 Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                    Exit For
                                                End If
                                            End If

                                            If IsNumeric(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) = True Then
                                               
                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                            If objICUBPCompany.MultiplesOfAmount <> "Only Billed Amount" Then
                                                Dim EnterdAmountRem As String = ""
                                                EnterdAmountRem = ICUBPSCompanyController.CheckUBPCompanyAmountAcceptance(CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()), objICUBPCompany.UBPSCompanyID.ToString)
                                                If EnterdAmountRem <> "OK" Then
                                                    Msg = Msg & " " & EnterdAmountRem
                                                    Exit For
                                                End If
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim datetw As String = arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()
                                                Dim ValueDate As Date = Date.ParseExact(datetw, format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim datetw As String = arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()
                                                Dim ValueDate As Date = Date.ParseExact(datetw, format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBranchCode" Then
                                            dtPrincipalBankBrLoop = New DataTable
                                            dtPrincipalBankBrLoop = DirectCast(ViewState("dtPrincipalBankBrLoop"), DataTable)
                                            If dtPrincipalBankBrLoop.Rows.Count > 0 Then
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    Dim drPRincipalBankBrLoop As DataRow()
                                                    drPRincipalBankBrLoop = dtPrincipalBankBrLoop.Select("OfficeID=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                    If drPRincipalBankBrLoop.Count = 0 Then
                                                        Dim drPRincipalBankBrLoop2 As DataRow()
                                                        drPRincipalBankBrLoop2 = dtPrincipalBankBrLoop.Select("OfficeCode='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "'")
                                                        If drPRincipalBankBrLoop2.Count = 0 Then
                                                            Msg = "Invalid Pick Up Location."
                                                            Exit For
                                                        End If
                                                    End If
                                                Else
                                                    Dim drPRincipalBankBrLoop As DataRow()
                                                    drPRincipalBankBrLoop = dtPrincipalBankBrLoop.Select("OfficeCode='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "'")
                                                    If drPRincipalBankBrLoop.Count = 0 Then
                                                        Dim drPRincipalBankBrLoop2 As DataRow()
                                                        drPRincipalBankBrLoop2 = dtPrincipalBankBrLoop.Select("OfficeName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "'")
                                                        If drPRincipalBankBrLoop2.Count = 0 Then
                                                            Msg = "Invalid Pick Up Location."
                                                            Exit For
                                                        End If

                                                    End If
                                                End If
                                            Else
                                                Msg = "Invalid Pick Up Location."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryIdentificationType" Then
                                            BeneIDType = New ArrayList
                                            BeneIDType = ViewState("BeneIDType")
                                            If BeneIDType.Contains(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower) = False Then
                                                Msg = "Invalid Identification Type"
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "UBPCompanyCode" Then
                                            dtUBPCompany = New DataTable
                                            dtUBPCompany = DirectCast(ViewState("dtUBPCompany"), DataTable)
                                            If dtUBPCompany.Rows.Count > 0 Then
                                                Dim drUBPCompanyLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drUBPCompanyLoop = dtUBPCompany.Select("UBPSCompanyID=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                    If drUBPCompanyLoop.Count = 0 Then
                                                        drUBPCompanyLoop = dtUBPCompany.Select("CompanyCode='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                    End If
                                                Else
                                                    drUBPCompanyLoop = dtUBPCompany.Select("CompanyCode='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                    If drUBPCompanyLoop.Count = 0 Then
                                                        drUBPCompanyLoop = dtUBPCompany.Select("CompanyName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                    End If
                                                End If
                                                If drUBPCompanyLoop.Count = 0 Then
                                                    Msg = "Invalid UBP Company."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid UBP Company."
                                                Exit For
                                            End If
                                        End If
                                        If objICUBPCompany.UBPSCompanyID IsNot Nothing Then
                                            If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "UBPReferenceFeild" Then
                                                If ICUBPSCompanyController.CheckReferenceFieldLength(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), objICUBPCompany.UBPSCompanyID) = False Then
                                                    Msg = "Invalid UBP reference field."
                                                    Exit For
                                                End If
                                                If ICUBPSCompanyController.CheckReferenceFieldDataWithRegularExpression(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), objICUBPCompany.UBPSCompanyID) = False Then
                                                    Msg = "Invalid UBP reference data."
                                                    Exit For
                                                End If
                                            End If
                                        End If
                                    End If
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                        If objICUBPCompany IsNot Nothing Then
                                            If objICUBPCompany.MultiplesOfAmount = "Only Billed Amount" Then
                                                If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() <> "0" Then
                                                    Msg = Msg & " " & "Amount must be zero in case of only billed amount"
                                                    Exit For
                                                End If

                                            End If
                                        Else
                                            Msg = Msg & " " & "Invalid UBP Company"
                                            Exit For
                                        End If
                                        Try
                                            If Double.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), ResultDouble) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                        If objICUBPCompany.MultiplesOfAmount = "Only Billed Amount" Then
                                            If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) <> 0 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be equal to zero (0) in only billed amount"
                                                Exit For
                                            End If
                                        Else
                                            If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) < 1 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                Exit For
                                            End If
                                        End If
                                        If IsNumeric(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) = True Then
                                           
                                        Else
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End If
                                        If objICUBPCompany.MultiplesOfAmount <> "Only Billed Amount" Then
                                            Dim EnterdAmountRem As String = ""
                                            EnterdAmountRem = ICUBPSCompanyController.CheckUBPCompanyAmountAcceptance(CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()), objICUBPCompany.UBPSCompanyID.ToString)
                                            If EnterdAmountRem <> "OK" Then
                                                Msg = Msg & " " & EnterdAmountRem
                                                Exit For
                                            End If
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim datetw As String = arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()
                                                Dim ValueDate As Date = Date.ParseExact(datetw, format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If



                                    End If

                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "UBPCompanyCode" Then
                                        dtUBPCompany = New DataTable
                                        dtUBPCompany = DirectCast(ViewState("dtUBPCompany"), DataTable)
                                        If dtUBPCompany.Rows.Count > 0 Then
                                            Dim drUBPCompanyLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                drUBPCompanyLoop = dtUBPCompany.Select("UBPSCompanyID=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                If drUBPCompanyLoop.Count = 0 Then
                                                    drUBPCompanyLoop = dtUBPCompany.Select("CompanyCode='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                            Else
                                                drUBPCompanyLoop = dtUBPCompany.Select("CompanyCode='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                If drUBPCompanyLoop.Count = 0 Then
                                                    drUBPCompanyLoop = dtUBPCompany.Select("CompanyName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                            End If
                                            If drUBPCompanyLoop.Count = 0 Then
                                                Msg = "Invalid UBP Company."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid UBP Company."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                        dtBankCodeLoop = New DataTable
                                        dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                        If dtBankCodeLoop.Rows.Count > 0 Then
                                            Dim i As Integer = 0
                                            If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                Dim drBankLoop As DataRow()
                                                drBankLoop = dtBankCodeLoop.Select("BankCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim())
                                                If drBankLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Bank Code."
                                            Exit For
                                        End If
                                    End If

                                End If
                            Next


                        Case "cotc"
                            For Each objAccountPaymentNatureTemplateField In HeadercollAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredCOTC = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            Try
                                                If Double.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), ResultDouble) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                            If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) < 1 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                Exit For
                                            End If
                                            If IsNumeric(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) = True Then
                                               
                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim datetw As String = arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()
                                                Dim ValueDate As Date = Date.ParseExact(datetw, format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim datetw As String = arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()
                                                Dim ValueDate As Date = Date.ParseExact(datetw, format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBranchCode" Then
                                            dtPrincipalBankBrLoop = New DataTable
                                            dtPrincipalBankBrLoop = DirectCast(ViewState("dtPrincipalBankBrLoop"), DataTable)
                                            If dtPrincipalBankBrLoop.Rows.Count > 0 Then
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    Dim drPRincipalBankBrLoop As DataRow()
                                                    drPRincipalBankBrLoop = dtPrincipalBankBrLoop.Select("OfficeID=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                    If drPRincipalBankBrLoop.Count = 0 Then
                                                        Dim drPRincipalBankBrLoop2 As DataRow()
                                                        drPRincipalBankBrLoop2 = dtPrincipalBankBrLoop.Select("OfficeCode='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "'")
                                                        If drPRincipalBankBrLoop2.Count = 0 Then
                                                            Msg = "Invalid Pick Up Location."
                                                            Exit For
                                                        End If
                                                    End If
                                                Else
                                                    Dim drPRincipalBankBrLoop As DataRow()
                                                    drPRincipalBankBrLoop = dtPrincipalBankBrLoop.Select("OfficeCode='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "'")
                                                    If drPRincipalBankBrLoop.Count = 0 Then
                                                        Dim drPRincipalBankBrLoop2 As DataRow()
                                                        drPRincipalBankBrLoop2 = dtPrincipalBankBrLoop.Select("OfficeName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "'")
                                                        If drPRincipalBankBrLoop2.Count = 0 Then
                                                            Msg = "Invalid Pick Up Location."
                                                            Exit For
                                                        End If

                                                    End If
                                                End If
                                            Else
                                                Msg = "Invalid Pick Up Location."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryIdentificationType" Then
                                            BeneIDType = New ArrayList
                                            BeneIDType = ViewState("BeneIDType")
                                            If BeneIDType.Contains(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower) = False Then
                                                Msg = "Invalid Identification Type"
                                                Exit For
                                            End If

                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If

                                        End If
                                    End If


                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim datetw As String = arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()
                                                Dim ValueDate As Date = Date.ParseExact(datetw, format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If



                                    End If




                                End If
                            Next
                        Case "cheque"
                            For Each objAccountPaymentNatureTemplateField In HeadercollAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredCQ = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            Try
                                                If Double.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), ResultDouble) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                            If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) < 1 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                Exit For
                                            End If
                                            If IsNumeric(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) = True Then
                                               
                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim datetw As String = arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()
                                                Dim ValueDate As Date = Date.ParseExact(datetw, format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim datetw As String = arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()
                                                Dim ValueDate As Date = Date.ParseExact(datetw, format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                            dtBankCodeLoop = New DataTable
                                            dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                            If dtBankCodeLoop.Rows.Count > 0 Then
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    Dim drBankLoop As DataRow()
                                                    drBankLoop = dtBankCodeLoop.Select("BankCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim())
                                                    If drBankLoop.Count = 0 Then
                                                        Msg = "Invalid Bank Code."
                                                        Exit For
                                                    End If
                                                Else
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drPrintLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                                Else
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower() & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                                End If
                                                If drPrintLocationLoop.Count = 0 Then
                                                    Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        End If

                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtDDPayableLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtDDPayableLocationLoop.Count = 0 Then
                                                    Msg = "Invalid DD Payable Location."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If

                                        End If
                                    End If


                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim datetw As String = arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()
                                                Dim ValueDate As Date = Date.ParseExact(datetw, format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If



                                    End If




                                End If
                            Next
                        Case "dd"
                            For Each objAccountPaymentNatureTemplateField In HeadercollAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredDD = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else

                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            Try
                                                If Double.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), ResultDouble) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                            If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) < 1 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                Exit For
                                            End If
                                            If IsNumeric(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) = True Then
                                               
                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                            dtBankCodeLoop = New DataTable
                                            dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                            If dtBankCodeLoop.Rows.Count > 0 Then
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    Dim drBankLoop As DataRow()
                                                    drBankLoop = dtBankCodeLoop.Select("BankCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim())
                                                    If drBankLoop.Count = 0 Then
                                                        Msg = "Invalid Bank Code."
                                                        Exit For
                                                    End If
                                                Else
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drPrintLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                                Else
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower() & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                                End If
                                                If drPrintLocationLoop.Count = 0 Then
                                                    Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then
                                            ''Cheque Principal Bank Branch in Specified City
                                            Dim dtBankBranches As New DataTable

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtDDPayableLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")

                                                Else
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")

                                                End If
                                                If drdtDDPayableLocationLoop.Count = 0 Then
                                                    Msg = "Invalid DD Payable Location."
                                                    Exit For
                                                Else
                                                    dtBankBranches = ICOfficeController.GetAllPrinciaplBankBranchesByCityCode(drdtDDPayableLocationLoop(0)("CityCode").ToString)
                                                    If dtBankBranches.Rows.Count > 0 Then
                                                        Msg = "Principal Bank branch exists in specified city. DD cannot be made"
                                                        Exit For
                                                    End If
                                                End If
                                            Else
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If

                                        End If
                                    End If



                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If

                                End If
                            Next
                        Case "direct credit"
                            For Each objAccountPaymentNatureTemplateField In HeadercollAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredFT = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            Try
                                                If Double.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), ResultDouble) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                            If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) < 1 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                Exit For
                                            End If
                                            If IsNumeric(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) = True Then
                                              
                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                            dtBankCodeLoop = New DataTable
                                            dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                            If dtBankCodeLoop.Rows.Count > 0 Then
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    Dim drBankLoop As DataRow()
                                                    drBankLoop = dtBankCodeLoop.Select("BankCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim())
                                                    If drBankLoop.Count = 0 Then
                                                        Msg = "Invalid Bank Code."
                                                        Exit For
                                                    End If
                                                Else
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drPrintLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                                Else
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower() & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                                End If
                                                If drPrintLocationLoop.Count = 0 Then
                                                    Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtDDPayableLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtDDPayableLocationLoop.Count = 0 Then
                                                    Msg = "Invalid DD Payable Location."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If

                                        End If
                                    End If

                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryAccountNo" Then

                                        If Regex.IsMatch(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), "^[0-9]+$") = False Then
                                            Msg = Msg & " " & " Invalid Beneficiary Account Number"
                                            Exit For
                                        End If



                                    End If

                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryAccountType" Then
                                        If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower <> "rb" And arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower <> "gl" Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " invalid beneficiary account type"
                                            Exit For
                                        Else
                                            If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower = "gl" Then
                                                For Each objICAPNPTypeFields As ICAccountPaymentNatureFileUploadTemplatefields In HeadercollAccountPaymentNatureTemplateFields
                                                    Select Case objICAPNPTypeFields.FieldName
                                                        Case "BeneficiaryAccountNo"
                                                            If arrInsruction(objICAPNPTypeFields.FieldOrder).ToString().Trim().Length <> 17 Then
                                                                Msg = Msg & " " & objICAPNPTypeFields.FieldName.ToString() & " invalid beneficiary account length"
                                                                Exit For
                                                            End If

                                                    End Select
                                                Next
                                            End If
                                        End If
                                    End If



                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryAccountNo" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                            If Regex.IsMatch(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), "^[0-9]+$") = False Then
                                                Msg = Msg & " " & " Invalid Beneficiary Account Number"
                                                Exit For
                                            End If
                                        End If


                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If
                                    End If
                                End If
                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryAccountType" Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() <> "" Then
                                        If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower <> "rb" And arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower <> "gl" Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " invalid beneficiary account type"
                                            Exit For
                                        Else
                                            If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower = "gl" Then
                                                For Each objICAPNPTypeFields As ICAccountPaymentNatureFileUploadTemplatefields In HeadercollAccountPaymentNatureTemplateFields
                                                    Select Case objICAPNPTypeFields.FieldName
                                                        Case "BeneficiaryAccountNo"
                                                            If arrInsruction(objICAPNPTypeFields.FieldOrder).ToString().Trim().Length <> 17 Then
                                                                Msg = Msg & " " & objICAPNPTypeFields.FieldName.ToString() & " invalid beneficiary account length"
                                                                Exit For
                                                            End If

                                                    End Select
                                                Next
                                            End If
                                        End If
                                    End If
                                End If
                            Next
                        Case "other credit"
                            For Each objAccountPaymentNatureTemplateField In HeadercollAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredIBFT = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            Try
                                                If Double.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), ResultDouble) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                            If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) < 1 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & "  should be greater than or equal to 1"
                                                Exit For
                                            End If
                                            If IsNumeric(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) = True Then
                                                
                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                            dtBankCodeLoop = New DataTable
                                            dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                            If dtBankCodeLoop.Rows.Count > 0 Then
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    Dim drBankLoop As DataRow()
                                                    drBankLoop = dtBankCodeLoop.Select("BankCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim())
                                                    If drBankLoop.Count = 0 Then
                                                        Msg = "Invalid Bank Code."
                                                        Exit For
                                                    End If
                                                Else
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drPrintLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                                Else
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower() & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                                End If
                                                If drPrintLocationLoop.Count = 0 Then
                                                    Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtDDPayableLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtDDPayableLocationLoop.Count = 0 Then
                                                    Msg = "Invalid DD Payable Location."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If

                                        End If


                                    End If
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If



                                    End If

                                End If
                            Next
                        Case "po"
                            For Each objAccountPaymentNatureTemplateField In HeadercollAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredPO = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                            Try
                                                If Double.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), ResultDouble) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                            If CDbl(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) < 1 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & "  should be greater than or equal to 1"
                                                Exit For
                                            End If
                                            If IsNumeric(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) = True Then
                                               
                                            Else
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then

                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try


                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                            dtBankCodeLoop = New DataTable
                                            dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                            If dtBankCodeLoop.Rows.Count > 0 Then
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    Dim drBankLoop As DataRow()
                                                    drBankLoop = dtBankCodeLoop.Select("BankCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim())
                                                    If drBankLoop.Count = 0 Then
                                                        Msg = "Invalid Bank Code."
                                                        Exit For
                                                    End If
                                                Else
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                            dtBankNameLoop = New DataTable
                                            dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                            If dtBankNameLoop.Rows.Count > 0 Then
                                                Dim drBankNameLoop As DataRow()
                                                drBankNameLoop = dtBankNameLoop.Select("BankName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "'")
                                                If drBankNameLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Name."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drPrintLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                                Else
                                                    drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower() & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                                End If
                                                If drPrintLocationLoop.Count = 0 Then
                                                    Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtDDPayableLocationLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtDDPayableLocationLoop.Count = 0 Then
                                                    Msg = "Invalid DD Payable Location."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryCountryLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Country."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drdtBeneficiaryProvinceLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary Province."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If
                                        End If
                                        If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then

                                            dtPrintLocationLoop = New DataTable
                                            dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                            If dtPrintLocationLoop.Rows.Count > 0 Then
                                                Dim drBeneficiaryCityLoop As DataRow()
                                                Dim i As Integer = 0
                                                If Integer.TryParse(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), i) = True Then
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() & "")
                                                Else
                                                    drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim().ToLower & "'")
                                                End If
                                                If drBeneficiaryCityLoop.Count = 0 Then
                                                    Msg = "Invalid Beneficiary City."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If

                                        End If

                                    End If
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If

                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        If Not arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                            Try
                                                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                                Dim ValueDate As Date = Date.ParseExact(arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                                Dim ResultDate As Date = Nothing
                                                If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                    Exit For
                                                ElseIf ValueDate < Now.Date Then
                                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                    Exit For
                                                End If
                                            Catch ex As Exception
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End Try
                                        End If



                                    End If

                                End If
                            Next
                    End Select
                End If
            ElseIf LineType.ToString() = "Detail" Then
                If Not hfDualDisbMode.Value Is Nothing Then
                    PaymentMode = hfDualDisbMode.Value.ToString()
                End If

                For Each objAccountPaymentNatureTemplateField In DetailcollAccountPaymentNatureTemplateFields
                    ' Check DB Maximum Length
                    If ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBLength(objAccountPaymentNatureTemplateField.FieldID, arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FixLength) = False Then
                        Msg = Msg & " Database field length for " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " exceeded."
                        Exit For
                    End If

                    Dim FieldTypeMessage As String = Nothing
                    FieldTypeMessage = ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBType(objAccountPaymentNatureTemplateField.FieldID, arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FieldType, objAccountPaymentNatureTemplateField.FixLength)
                    If Not FieldTypeMessage = "OK" Then
                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                        Exit For
                    End If



                  
                Next
                If PaymentMode.ToString() <> "" Then
                    ' Check Product Type wise Required Fields Data
                    Select Case PaymentMode.ToString()
                        Case "bill payment"
                            For Each objAccountPaymentNatureTemplateField In DetailcollAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredBillPayment = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        Dim FieldTypeMessage As String = Nothing
                                        FieldTypeMessage = ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBType(objAccountPaymentNatureTemplateField.FieldID, arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FieldType, objAccountPaymentNatureTemplateField.FixLength)
                                        If Not FieldTypeMessage = "OK" Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                                            Exit For
                                        End If

                                    End If
                                End If
                            Next
                        Case "cotc"
                            For Each objAccountPaymentNatureTemplateField In DetailcollAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredCOTC = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        Dim FieldTypeMessage As String = Nothing
                                        FieldTypeMessage = ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBType(objAccountPaymentNatureTemplateField.FieldID, arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FieldType, objAccountPaymentNatureTemplateField.FixLength)
                                        If Not FieldTypeMessage = "OK" Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                                            Exit For
                                        End If

                                    End If
                                End If
                            Next
                        Case "cheque"
                            For Each objAccountPaymentNatureTemplateField In DetailcollAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredCQ = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        Dim FieldTypeMessage As String = Nothing
                                        FieldTypeMessage = ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBType(objAccountPaymentNatureTemplateField.FieldID, arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FieldType, objAccountPaymentNatureTemplateField.FixLength)
                                        If Not FieldTypeMessage = "OK" Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                                            Exit For
                                        End If
                                       
                                    End If
                                End If
                            Next
                        Case "dd"
                            For Each objAccountPaymentNatureTemplateField In DetailcollAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredCQ = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        Dim FieldTypeMessage As String = Nothing
                                        FieldTypeMessage = ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBType(objAccountPaymentNatureTemplateField.FieldID, arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FieldType, objAccountPaymentNatureTemplateField.FixLength)
                                        If Not FieldTypeMessage = "OK" Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                                            Exit For
                                        End If
                                      
                                    End If
                                End If
                            Next
                        Case "direct credit"
                            For Each objAccountPaymentNatureTemplateField In DetailcollAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredCQ = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        Dim FieldTypeMessage As String = Nothing
                                        FieldTypeMessage = ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBType(objAccountPaymentNatureTemplateField.FieldID, arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FieldType, objAccountPaymentNatureTemplateField.FixLength)
                                        If Not FieldTypeMessage = "OK" Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                                            Exit For
                                        End If
                                       
                                    End If
                                End If
                            Next
                        Case "other credit"
                            For Each objAccountPaymentNatureTemplateField In DetailcollAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredCQ = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        Dim FieldTypeMessage As String = Nothing
                                        FieldTypeMessage = ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBType(objAccountPaymentNatureTemplateField.FieldID, arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FieldType, objAccountPaymentNatureTemplateField.FixLength)
                                        If Not FieldTypeMessage = "OK" Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                                            Exit For
                                        End If
                                       
                                    End If
                                End If
                            Next
                        Case "po"
                            For Each objAccountPaymentNatureTemplateField In DetailcollAccountPaymentNatureTemplateFields
                                If objAccountPaymentNatureTemplateField.IsRequiredCQ = True Then
                                    If arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is empty."
                                        Exit For
                                    Else
                                        Dim FieldTypeMessage As String = Nothing
                                        FieldTypeMessage = ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBType(objAccountPaymentNatureTemplateField.FieldID, arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FieldType, objAccountPaymentNatureTemplateField.FixLength)
                                        If Not FieldTypeMessage = "OK" Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                                            Exit For
                                        End If
                                        
                                    End If
                                End If
                            Next
                    End Select
                End If
            End If
            Return Msg
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Sub GetDataFromEXCELFile(ByVal drExcel As DataRow, ByVal collAccountPaymentNatureTemplateFields As ICAccountPaymentNatureFileUploadTemplatefieldsCollection, ByVal LineNumber As Integer, ByVal ColomnCount As Integer, ByVal FileTemplate As ICTemplate, ByVal LoopCount As Integer)
        Dim dtVarified, dtUnVerified As New DataTable
        Dim drVarified, drUnVerified As DataRow
        Dim objAccountPaymentNatureTemplateField As New ICAccountPaymentNatureFileUploadTemplatefields


        objAccountPaymentNatureTemplateField.es.Connection.CommandTimeout = 3600
        FileTemplate.es.Connection.CommandTimeout = 3600

        Dim ErrorMsg As String = ""
        Dim CountColumn As Integer = 0
        Dim LineData As String = ""
        Dim i As Integer = 0
        Try

            If ViewState("dtVarified") Is Nothing Then
                For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                    dtVarified.Columns.Add(New DataColumn(objAccountPaymentNatureTemplateField.FieldName.ToString() & "-" & objAccountPaymentNatureTemplateField.FieldID.ToString(), GetType(System.String)))
                Next
                dtUnVerified.Columns.Add(New DataColumn("Data", GetType(System.String)))
                dtUnVerified.Columns.Add(New DataColumn("Message", GetType(System.String)))
                dtUnVerified.Columns.Add(New DataColumn("LineNo", GetType(System.String)))
            Else
                dtVarified = DirectCast(ViewState("dtVarified"), DataTable)
                dtUnVerified = DirectCast(ViewState("dtUnVerified"), DataTable)
            End If

            ErrorMsg = CheckDataEXCEL(drExcel, collAccountPaymentNatureTemplateFields, FileTemplate, LoopCount)
            If ErrorMsg = "" Then
                drVarified = dtVarified.NewRow()
                For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                    If objAccountPaymentNatureTemplateField.FieldName.ToString().Trim() = "Amount" Then
                        drVarified(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1) = CDbl(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1))
                    Else
                        drVarified(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1) = drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()
                    End If
                Next
                dtVarified.Rows.Add(drVarified)
            Else

                For i = 0 To ColomnCount - 1
                    If i = 0 Then
                        LineData = drExcel(i).ToString()
                    Else
                        LineData = LineData & " , " & drExcel(i).ToString()
                    End If

                Next

                drUnVerified = dtUnVerified.NewRow()
                drUnVerified("Data") = LineData.ToString()
                drUnVerified("Message") = ErrorMsg.ToString()
                drUnVerified("LineNo") = LineNumber
                dtUnVerified.Rows.Add(drUnVerified)
            End If

            ViewState("dtVarified") = dtVarified
            ViewState("dtUnVerified") = dtUnVerified
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckDataEXCEL(ByVal drExcel As DataRow, ByVal collAccountPaymentNatureTemplateFields As ICAccountPaymentNatureFileUploadTemplatefieldsCollection, ByVal FileTemplate As ICTemplate, ByVal LoopCount As Integer) As String
        Dim objAccountPaymentNatureTemplateField As ICAccountPaymentNatureFileUploadTemplatefields
        Dim Msg As String = ""
        Dim PaymentMode As String = ""
        Dim StartIndex As Integer = 0
        Dim result As Integer
        Dim ResultDouble As Double
        Dim cnt As Integer = 0
        Dim ProductTypeCode As String = Nothing
        Dim dtProductTypeLoop As DataTable
        Dim dtBankCodeLoop As DataTable
        Dim dtBankNameLoop As DataTable
        Dim dtPrintLocationLoop As DataTable
        Dim dtProductType As New DataTable
        Dim dtUBPCompany As New DataTable
        Dim dtPrincipalBankBrLoop As DataTable
        Dim BeneIDType As ArrayList


        Try
            If LoopCount = 0 Then
                objAccountPaymentNatureTemplateField = New ICAccountPaymentNatureFileUploadTemplatefields
                objAccountPaymentNatureTemplateField = collAccountPaymentNatureTemplateFields(0)
                '' Product Type
                dtProductTypeLoop = New DataTable
                ViewState("dtProductTypeLoop") = Nothing
                dtProductTypeLoop = ICProductTypeController.IsProductTypeExistByProductTypeCode2(FileTemplate.TemplateID.ToString, objAccountPaymentNatureTemplateField)
                ViewState("dtProductTypeLoop") = dtProductTypeLoop
                '' Banks
                dtBankCodeLoop = New DataTable
                ViewState("dtBankCodeLoop") = Nothing
                dtBankCodeLoop = ICBankController.IsBankExistByBankCodeForBulkUpLoad
                ViewState("dtBankCodeLoop") = dtBankCodeLoop

                dtBankNameLoop = New DataTable
                ViewState("dtBankNameLoop") = Nothing
                dtBankNameLoop = dtBankCodeLoop.Clone
                dtBankNameLoop = dtBankCodeLoop
                ViewState("dtBankNameLoop") = dtBankNameLoop
                '' Print Location
                dtPrintLocationLoop = New DataTable
                ViewState("dtPrintLocationLoop") = Nothing
                dtPrintLocationLoop = ICOfficeController.IsPrintLocationExistByPrintLocationNameOrCodeForBulkUpLoad(objAccountPaymentNatureTemplateField.AccountNumber.ToString, objAccountPaymentNatureTemplateField.BranchCode.ToString, objAccountPaymentNatureTemplateField.Currency.ToString, objAccountPaymentNatureTemplateField.PaymentNatureCode.ToString)
                ViewState("dtPrintLocationLoop") = dtPrintLocationLoop
                '' DD Payable Location

                dtPrintLocationLoop = New DataTable
                ViewState("dtDDPayableLocationLoop") = Nothing
                dtPrintLocationLoop = ICCityController.IsCityExistByCityNameOrCodeForBulkUpload()
                ViewState("dtDDPayableLocationLoop") = dtPrintLocationLoop
                '' Beneficiary City
                ViewState("dtBeneficiaryCityLoop") = Nothing
                ViewState("dtBeneficiaryCityLoop") = dtPrintLocationLoop
                ''Bene Province
                dtPrintLocationLoop = New DataTable
                ViewState("dtBeneficiaryProvinceLoop") = Nothing
                dtPrintLocationLoop = ICProvinceController.IsProvinceExistByCountryNameOrCodeForBulkUpload
                ViewState("dtBeneficiaryProvinceLoop") = dtPrintLocationLoop

                ''Bene Country
                dtPrintLocationLoop = New DataTable
                ViewState("dtBeneficiaryCountryLoop") = Nothing
                dtPrintLocationLoop = ICCountryController.IsCountryExistByCountryNameForBulkUpload()
                ViewState("dtBeneficiaryCountryLoop") = dtPrintLocationLoop


                ''Bene PICK Up Location For COTC
                dtPrincipalBankBrLoop = New DataTable
                ViewState("dtPrincipalBankBrLoop") = Nothing
                dtPrincipalBankBrLoop = ICOfficeController.GetPrincipalBankBranchActiveAndApprove
                ViewState("dtPrincipalBankBrLoop") = dtPrincipalBankBrLoop

                '' Bene ID Type 
                BeneIDType = New ArrayList
                BeneIDType.Add("cnic")
                BeneIDType.Add("passport")
                BeneIDType.Add("nicop")
                BeneIDType.Add("driving license")
                ViewState("BeneIDType") = BeneIDType


                ''UBPCompany
                dtUBPCompany = New DataTable
                ViewState("dtUBPCompany") = Nothing
                dtUBPCompany = ICUBPSCompanyController.GetAllUBPActiveApproveCompany
                ViewState("dtUBPCompany") = dtUBPCompany

            End If
            objAccountPaymentNatureTemplateField = New ICAccountPaymentNatureFileUploadTemplatefields
            For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                ' Check DB Maximum Length



                Dim FieldTypeMessage As String = Nothing

                FieldTypeMessage = ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBType(objAccountPaymentNatureTemplateField.FieldID, drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FieldType, objAccountPaymentNatureTemplateField.FixLength)
                If Not FieldTypeMessage = "OK" Then
                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                    Exit For
                End If


                If ICAccountPaymentNatureFileUploadTemplateFieldsController.CheckFieldDBLength(objAccountPaymentNatureTemplateField.FieldID, drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FixLength) = False Then
                    Msg = Msg & " Database field length for " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " exceeded."
                    Exit For
                End If




                ' Get Product Type
                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ProductTypeCode" Then
                    If drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                        Msg = "Product Type Code is empty."
                        Exit For
                    Else
                        dtProductTypeLoop = New DataTable
                        dtProductTypeLoop = DirectCast(ViewState("dtProductTypeLoop"), DataTable)
                        Dim drProductTypeRow As DataRow()
                        drProductTypeRow = dtProductTypeLoop.Select("ProductTypeCode='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                        If dtProductTypeLoop.Rows.Count > 0 Then
                            If drProductTypeRow.Count = 0 Then
                                Msg = "Invalid Product Type Code."
                                Exit For
                            Else
                                Dim objProductType As New ICProductType
                                If objProductType.LoadByPrimaryKey(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) Then
                                    PaymentMode = objProductType.DisbursementMode.ToString().ToLower()
                                    ProductTypeCode = Nothing
                                    ProductTypeCode = objProductType.ProductTypeCode.ToString

                                Else
                                    Msg = "Invalid Product Type Code."
                                    Exit For
                                End If
                            End If
                        Else
                            Msg = "Invalid Product Type Code."
                            Exit For
                        End If
                    End If
                End If
            Next
            If PaymentMode.ToString() <> "" Then
                ' Check Product Type wise Required Fields Data
                Select Case PaymentMode.ToString()
                    Case "bill payment"
                        For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                            Dim objICUBPCompany As New ICUBPSCompany

                            For Each objICAPNPTypeFields As ICAccountPaymentNatureFileUploadTemplatefields In collAccountPaymentNatureTemplateFields
                                Select Case objICAPNPTypeFields.FieldName
                                    Case "UBPCompanyCode"
                                        Dim UBPSCompanyID As String = ""
                                        UBPSCompanyID = ICUBPSCompanyController.GetUBPCompanyIDByCompanyNameOrCodeorID(drExcel(CInt(objICAPNPTypeFields.FieldOrder) - 1).ToString().Trim())
                                        If UBPSCompanyID <> "" Then
                                            If objICUBPCompany.LoadByPrimaryKey(UBPSCompanyID) = True Then
                                            Else
                                                Msg = Msg & " " & "Invalid UBP Company Code"
                                                Exit For
                                            End If
                                        Else
                                            Msg = Msg & " " & "Invalid UBP Company Code"
                                            Exit For
                                        End If

                                End Select
                            Next
                            If Msg <> "" Then
                                Exit For
                            End If
                            If objAccountPaymentNatureTemplateField.IsRequiredBillPayment = True Then
                                If drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString().Trim() & " is empty."
                                    Exit For
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                        If objICUBPCompany IsNot Nothing Then
                                            If objICUBPCompany.MultiplesOfAmount = "Only Billed Amount" Then
                                                If drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() <> "0" Then
                                                    Msg = Msg & " " & "Amount must be zero in case of only billed amount"
                                                    Exit For
                                                End If

                                            End If
                                        Else
                                            Msg = Msg & " " & "Invalid UBP Company"
                                            Exit For
                                        End If
                                        Try
                                            If Double.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), ResultDouble) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                        If objICUBPCompany.MultiplesOfAmount = "Only Billed Amount" Then
                                            If CDbl(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) <> 0 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be equal to zero (0) in only billed amount"
                                                Exit For
                                            End If
                                        Else
                                            If CDbl(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) < 1 Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                                Exit For
                                            End If
                                        End If
                                        If IsNumeric(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) = True Then
                                           
                                        Else
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End If

                                        If objICUBPCompany IsNot Nothing Then
                                            If objICUBPCompany.MultiplesOfAmount <> "Only Billed Amount" Then
                                                Dim EnterdAmountRem As String = ""
                                                EnterdAmountRem = ICUBPSCompanyController.CheckUBPCompanyAmountAcceptance(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), objICUBPCompany.UBPSCompanyID.ToString)
                                                If EnterdAmountRem <> "OK" Then
                                                    Msg = Msg & " " & EnterdAmountRem
                                                    Exit For
                                                End If
                                            End If
                                        Else
                                            Msg = Msg & " " & "Invalid UBP Company"
                                            Exit For
                                        End If
                                    End If

                                    ''Value Date

                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBranchCode" Then
                                        dtPrincipalBankBrLoop = New DataTable
                                        dtPrincipalBankBrLoop = DirectCast(ViewState("dtPrincipalBankBrLoop"), DataTable)
                                        If dtPrincipalBankBrLoop.Rows.Count > 0 Then
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                Dim drPRincipalBankBrLoop As DataRow()
                                                drPRincipalBankBrLoop = dtPrincipalBankBrLoop.Select("OfficeID=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                                If drPRincipalBankBrLoop.Count = 0 Then
                                                    Dim drPRincipalBankBrLoop2 As DataRow()
                                                    drPRincipalBankBrLoop2 = dtPrincipalBankBrLoop.Select("OfficeCode='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                                    If drPRincipalBankBrLoop2.Count = 0 Then
                                                        Msg = "Invalid Pick Up Location."
                                                        Exit For
                                                    End If

                                                End If
                                            Else
                                                Dim drPRincipalBankBrLoop As DataRow()
                                                drPRincipalBankBrLoop = dtPrincipalBankBrLoop.Select("OfficeCode='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                                If drPRincipalBankBrLoop.Count = 0 Then
                                                    Dim drPRincipalBankBrLoop2 As DataRow()
                                                    drPRincipalBankBrLoop2 = dtPrincipalBankBrLoop.Select("OfficeName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                                    If drPRincipalBankBrLoop2.Count = 0 Then
                                                        Msg = "Invalid Pick Up Location."
                                                        Exit For
                                                    End If

                                                End If
                                            End If
                                        Else
                                            Msg = "Invalid Pick Up Location."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryIdentificationType" Then
                                        BeneIDType = New ArrayList
                                        BeneIDType = ViewState("BeneIDType")
                                        If BeneIDType.Contains(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower) = False Then
                                            Msg = "Invalid Identification Type"
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                        dtBankNameLoop = New DataTable
                                        dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                        If dtBankNameLoop.Rows.Count > 0 Then
                                            Dim drBankNameLoop As DataRow()
                                            drBankNameLoop = dtBankNameLoop.Select("BankName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                            If drBankNameLoop.Count = 0 Then
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Bank Name."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drPrintLocationLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                            Else
                                                drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                            End If
                                            If drPrintLocationLoop.Count = 0 Then
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Print Location or Print Location is Not Tagged."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtDDPayableLocationLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtDDPayableLocationLoop.Count = 0 Then
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid DD Payable Location."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtBeneficiaryCountryLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary Country."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtBeneficiaryProvinceLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary Province."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drBeneficiaryCityLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drBeneficiaryCityLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary City."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "UBPCompanyCode" Then
                                        dtUBPCompany = New DataTable
                                        dtUBPCompany = DirectCast(ViewState("dtUBPCompany"), DataTable)
                                        If dtUBPCompany.Rows.Count > 0 Then
                                            Dim drUBPCompanyLoop As DataRow()
                                            Dim i As Integer = 0
                                            Try
                                                If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                    drUBPCompanyLoop = dtUBPCompany.Select("UBPSCompanyID=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                                    If drUBPCompanyLoop.Count = 0 Then
                                                        drUBPCompanyLoop = dtUBPCompany.Select("CompanyCode='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                                    End If
                                                Else
                                                    drUBPCompanyLoop = dtUBPCompany.Select("CompanyCode='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                                    If drUBPCompanyLoop.Count = 0 Then
                                                        drUBPCompanyLoop = dtUBPCompany.Select("CompanyName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                                    End If
                                                End If
                                            Catch ex As Exception
                                                drUBPCompanyLoop = dtUBPCompany.Select("CompanyCode='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                                If drUBPCompanyLoop.Count = 0 Then
                                                    drUBPCompanyLoop = dtUBPCompany.Select("CompanyName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                                End If
                                            End Try
                                            If drUBPCompanyLoop.Count = 0 Then
                                                Msg = "Invalid UBP Company."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid UBP Company."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "UBPReferenceFeild" Then
                                        If objICUBPCompany IsNot Nothing Then
                                            If ICUBPSCompanyController.CheckReferenceFieldLength(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().Length.ToString, objICUBPCompany.UBPSCompanyID) = False Then
                                                Msg = "Invalid UBP reference field length."
                                                Exit For
                                            End If
                                            If ICUBPSCompanyController.CheckReferenceFieldDataWithRegularExpression(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), objICUBPCompany.UBPSCompanyID) = False Then
                                                Msg = "Invalid UBP reference data."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid UBP Company for reference field validation"
                                            Exit For
                                        End If
                                    End If
                                End If
                            Else
                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                    If Not drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If

                                End If
                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                    If Not drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If

                                End If
                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                    dtBankCodeLoop = New DataTable
                                    dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                    If dtBankCodeLoop.Rows.Count > 0 Then
                                        Dim i As Integer = 0
                                        If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                            Dim drBankLoop As DataRow()
                                            drBankLoop = dtBankCodeLoop.Select("BankCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim())
                                            If drBankLoop.Count = 0 Then
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Bank Code."
                                            Exit For
                                        End If

                                    Else
                                        Msg = "Invalid Bank Code."
                                        Exit For
                                    End If
                                End If
                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "UBPCompanyCode" Then
                                    dtUBPCompany = New DataTable
                                    dtUBPCompany = DirectCast(ViewState("dtUBPCompany"), DataTable)
                                    If dtUBPCompany.Rows.Count > 0 Then
                                        Dim drUBPCompanyLoop As DataRow()
                                        Dim i As Integer = 0
                                        If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                            drUBPCompanyLoop = dtUBPCompany.Select("UBPSCompanyID=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            If drUBPCompanyLoop.Count = 0 Then
                                                drUBPCompanyLoop = dtUBPCompany.Select("CompanyCode='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                            End If
                                        Else
                                            drUBPCompanyLoop = dtUBPCompany.Select("CompanyCode='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                            If drUBPCompanyLoop.Count = 0 Then
                                                drUBPCompanyLoop = dtUBPCompany.Select("CompanyName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                            End If
                                        End If
                                        If drUBPCompanyLoop.Count = 0 Then
                                            Msg = "Invalid UBP Company."
                                            Exit For
                                        End If
                                    Else
                                        Msg = "Invalid UBP Company."
                                        Exit For
                                    End If
                                End If
                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                    If objICUBPCompany IsNot Nothing Then
                                        If objICUBPCompany.MultiplesOfAmount = "Only Billed Amount" Then
                                            If drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() <> "0" Then
                                                Msg = Msg & " " & "Amount must be zero in case of only billed amount"
                                                Exit For
                                            End If

                                        End If
                                    Else
                                        Msg = Msg & " " & "Invalid UBP Company"
                                        Exit For
                                    End If
                                    Try
                                        If Double.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), ResultDouble) = False Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End If
                                    Catch ex As Exception
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                        Exit For
                                    End Try
                                    If objICUBPCompany.MultiplesOfAmount = "Only Billed Amount" Then
                                        If CDbl(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) <> 0 Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be equal to zero (0) in only billed amount"
                                            Exit For
                                        End If
                                    Else
                                        If CDbl(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) < 1 Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                            Exit For
                                        End If
                                    End If

                                    If IsNumeric(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) = True Then
                                       
                                    Else
                                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                        Exit For
                                    End If

                                    If objICUBPCompany IsNot Nothing Then
                                        If objICUBPCompany.MultiplesOfAmount <> "Only Billed Amount" Then
                                            Dim EnterdAmountRem As String = ""
                                            EnterdAmountRem = ICUBPSCompanyController.CheckUBPCompanyAmountAcceptance(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), objICUBPCompany.UBPSCompanyID.ToString)
                                            If EnterdAmountRem <> "OK" Then
                                                Msg = Msg & " " & EnterdAmountRem
                                                Exit For
                                            End If
                                        End If
                                    Else
                                        Msg = Msg & " " & "Invalid UBP Company"
                                        Exit For
                                    End If
                                End If
                            End If
                        Next
                    Case "cotc"
                        For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                            If objAccountPaymentNatureTemplateField.IsRequiredCOTC = True Then
                                If drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString().Trim() & " is empty."
                                    Exit For
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                        Try
                                            If Double.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), ResultDouble) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                        If CDbl(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) < 1 Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                            Exit For
                                        End If
                                        If IsNumeric(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) = True Then
                                           
                                        Else
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End If
                                    End If

                                    ''Value Date

                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBranchCode" Then
                                        dtPrincipalBankBrLoop = New DataTable
                                        dtPrincipalBankBrLoop = DirectCast(ViewState("dtPrincipalBankBrLoop"), DataTable)
                                        If dtPrincipalBankBrLoop.Rows.Count > 0 Then
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                Dim drPRincipalBankBrLoop As DataRow()
                                                drPRincipalBankBrLoop = dtPrincipalBankBrLoop.Select("OfficeID=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                                If drPRincipalBankBrLoop.Count = 0 Then
                                                    Dim drPRincipalBankBrLoop2 As DataRow()
                                                    drPRincipalBankBrLoop2 = dtPrincipalBankBrLoop.Select("OfficeCode='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                                    If drPRincipalBankBrLoop2.Count = 0 Then
                                                        Msg = "Invalid Pick Up Location."
                                                        Exit For
                                                    End If

                                                End If
                                            Else
                                                Dim drPRincipalBankBrLoop As DataRow()
                                                drPRincipalBankBrLoop = dtPrincipalBankBrLoop.Select("OfficeCode='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                                If drPRincipalBankBrLoop.Count = 0 Then
                                                    Dim drPRincipalBankBrLoop2 As DataRow()
                                                    drPRincipalBankBrLoop2 = dtPrincipalBankBrLoop.Select("OfficeName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                                    If drPRincipalBankBrLoop2.Count = 0 Then
                                                        Msg = "Invalid Pick Up Location."
                                                        Exit For
                                                    End If

                                                End If
                                            End If
                                        Else
                                            Msg = "Invalid Pick Up Location."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryIdentificationType" Then
                                        BeneIDType = New ArrayList
                                        BeneIDType = ViewState("BeneIDType")
                                        If BeneIDType.Contains(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower) = False Then
                                            Msg = "Invalid Identification Type"
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                        dtBankNameLoop = New DataTable
                                        dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                        If dtBankNameLoop.Rows.Count > 0 Then
                                            Dim drBankNameLoop As DataRow()
                                            drBankNameLoop = dtBankNameLoop.Select("BankName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                            If drBankNameLoop.Count = 0 Then
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Bank Name."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drPrintLocationLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                            Else
                                                drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                            End If
                                            If drPrintLocationLoop.Count = 0 Then
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Print Location or Print Location is Not Tagged."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtDDPayableLocationLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtDDPayableLocationLoop.Count = 0 Then
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid DD Payable Location."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtBeneficiaryCountryLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary Country."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtBeneficiaryProvinceLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary Province."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drBeneficiaryCityLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drBeneficiaryCityLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary City."
                                            Exit For
                                        End If
                                    End If
                                End If
                            Else
                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                    If Not drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If

                                End If
                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                    If Not drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If

                                End If
                            End If
                        Next
                    Case "cheque"
                        For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                            If objAccountPaymentNatureTemplateField.IsRequiredCQ = True Then
                                If drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString().Trim() & " is empty."
                                    Exit For
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                        Try
                                            If Double.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), ResultDouble) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                        If CDbl(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) < 1 Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                            Exit For
                                        End If
                                        If IsNumeric(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) = True Then
                                          
                                        Else
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End If
                                    End If

                                    ''Value Date

                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                        dtBankCodeLoop = New DataTable
                                        dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                        If dtBankCodeLoop.Rows.Count > 0 Then
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                Dim drBankLoop As DataRow()
                                                drBankLoop = dtBankCodeLoop.Select("BankCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim())
                                                If drBankLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If

                                        Else
                                            Msg = "Invalid Bank Code."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                        dtBankNameLoop = New DataTable
                                        dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                        If dtBankNameLoop.Rows.Count > 0 Then
                                            Dim drBankNameLoop As DataRow()
                                            drBankNameLoop = dtBankNameLoop.Select("BankName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                            If drBankNameLoop.Count = 0 Then
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Bank Name."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drPrintLocationLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                            Else
                                                drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                            End If
                                            If drPrintLocationLoop.Count = 0 Then
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Print Location or Print Location is Not Tagged."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtDDPayableLocationLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtDDPayableLocationLoop.Count = 0 Then
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid DD Payable Location."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtBeneficiaryCountryLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary Country."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtBeneficiaryProvinceLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary Province."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drBeneficiaryCityLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drBeneficiaryCityLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary City."
                                            Exit For
                                        End If
                                    End If
                                End If
                            Else
                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                    If Not drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If

                                End If
                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                    If Not drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If

                                End If
                            End If
                        Next
                    Case "dd"
                        For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                            If objAccountPaymentNatureTemplateField.IsRequiredDD = True Then
                                If drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString().Trim() & " is empty."
                                    Exit For
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                        Try
                                            If Double.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), ResultDouble) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                        If CDbl(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) < 1 Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                            Exit For
                                        End If
                                        If IsNumeric(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) = True Then
                                          
                                        Else
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End If
                                    End If
                                    ''Value Date

                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For

                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                        dtBankCodeLoop = New DataTable
                                        dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                        If dtBankCodeLoop.Rows.Count > 0 Then
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                Dim drBankLoop As DataRow()
                                                drBankLoop = dtBankCodeLoop.Select("BankCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim())
                                                If drBankLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If

                                        Else
                                            Msg = "Invalid Bank Code."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                        dtBankNameLoop = New DataTable
                                        dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                        If dtBankNameLoop.Rows.Count > 0 Then
                                            Dim drBankNameLoop As DataRow()
                                            drBankNameLoop = dtBankNameLoop.Select("BankName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                            If drBankNameLoop.Count = 0 Then
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Bank Name."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drPrintLocationLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                            Else
                                                drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                            End If
                                            If drPrintLocationLoop.Count = 0 Then
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Print Location or Print Location is Not Tagged."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then
                                        Dim dtBankBranches As New DataTable
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtDDPayableLocationLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtDDPayableLocationLoop.Count = 0 Then
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            Else
                                                dtBankBranches = ICOfficeController.GetAllPrinciaplBankBranchesByCityCode(drdtDDPayableLocationLoop(0)("CityCode").ToString)
                                                If dtBankBranches.Rows.Count > 0 Then
                                                    Msg = "Principal Bank branch exists in specified city. DD cannot be made"
                                                    Exit For
                                                End If
                                            End If
                                        Else
                                            Msg = "Invalid DD Payable Location."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtBeneficiaryCountryLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary Country."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtBeneficiaryProvinceLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary Province."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drBeneficiaryCityLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drBeneficiaryCityLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary City."
                                            Exit For
                                        End If
                                    End If
                                End If
                            Else
                                ''Value Date

                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                    If Not drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If

                                End If
                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                    If Not drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If

                                End If
                            End If
                        Next
                    Case "direct credit"
                        For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                            If objAccountPaymentNatureTemplateField.IsRequiredFT = True Then
                                If drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString().Trim() & " is empty."
                                    Exit For
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                        Try
                                            If Double.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), ResultDouble) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                        If CDbl(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) < 1 Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                            Exit For
                                        End If
                                        If IsNumeric(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) = True Then
                                           
                                        Else
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End If
                                    End If
                                    ''Value Date

                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                        dtBankCodeLoop = New DataTable
                                        dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                        If dtBankCodeLoop.Rows.Count > 0 Then
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                Dim drBankLoop As DataRow()
                                                drBankLoop = dtBankCodeLoop.Select("BankCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim())
                                                If drBankLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If

                                        Else
                                            Msg = "Invalid Bank Code."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                        dtBankNameLoop = New DataTable
                                        dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                        If dtBankNameLoop.Rows.Count > 0 Then
                                            Dim drBankNameLoop As DataRow()
                                            drBankNameLoop = dtBankNameLoop.Select("BankName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                            If drBankNameLoop.Count = 0 Then
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Bank Name."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drPrintLocationLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                            Else
                                                drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                            End If
                                            If drPrintLocationLoop.Count = 0 Then
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Print Location or Print Location is Not Tagged."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtDDPayableLocationLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtDDPayableLocationLoop.Count = 0 Then
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid DD Payable Location."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtBeneficiaryCountryLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary Country."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtBeneficiaryProvinceLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary Province."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drBeneficiaryCityLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drBeneficiaryCityLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary City."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryAccountNo" Then
                                        If Regex.IsMatch(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), "^[0-9]+$") = False Then
                                            Msg = Msg & " " & " Invalid Beneficiary Account Number"
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryAccountType" Then
                                        If drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower <> "gl" And drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower <> "rb" Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " invalid beneficiary account type"
                                            Exit For
                                        Else
                                            If drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower = "gl" Then

                                                For Each objICAPNPTypeFields As ICAccountPaymentNatureFileUploadTemplatefields In collAccountPaymentNatureTemplateFields
                                                    Select Case objICAPNPTypeFields.FieldName
                                                        Case "BeneficiaryAccountNo"
                                                            If drExcel(CInt(objICAPNPTypeFields.FieldOrder) - 1).ToString().Trim().Length <> 17 Then
                                                                Msg = Msg & " " & "Invalid Beneficiary Account Number Length"
                                                                Exit For
                                                            End If
                                                    End Select
                                                Next
                                            End If
                                        End If
                                    End If
                                End If
                            Else
                                ''Value Date
                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryAccountNo" Then
                                    If Not drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                        If Regex.IsMatch(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), "^[0-9]+$") = False Then
                                            Msg = Msg & " " & " Invalid Beneficiary Account Number"
                                            Exit For
                                        End If
                                    End If
                                End If
                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                    If Not drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If

                                End If
                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                    If Not drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If

                                End If
                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryAccountType" Then
                                    If drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() <> "" Then
                                        If drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower <> "gl" And drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower <> "rb" Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " invalid beneficiary account type"
                                            Exit For
                                        Else
                                            If drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower = "gl" Then

                                                For Each objICAPNPTypeFields As ICAccountPaymentNatureFileUploadTemplatefields In collAccountPaymentNatureTemplateFields
                                                    Select Case objICAPNPTypeFields.FieldName
                                                        Case "BeneficiaryAccountNo"
                                                            If drExcel(CInt(objICAPNPTypeFields.FieldOrder) - 1).ToString().Trim().Length <> 17 Then
                                                                Msg = Msg & " " & "Invalid Beneficiary Account Number Length"
                                                                Exit For
                                                            End If
                                                    End Select
                                                Next
                                            End If
                                        End If
                                    End If

                                End If
                            End If
                        Next
                    Case "other credit"
                        For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                            If objAccountPaymentNatureTemplateField.IsRequiredIBFT = True Then
                                If drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString().Trim() & " is empty."
                                    Exit For
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                        Try
                                            If Double.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), ResultDouble) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                        If CDbl(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) < 1 Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                            Exit For
                                        End If
                                        If IsNumeric(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) = True Then
                                           
                                        Else
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End If
                                    End If
                                    ''Value Date

                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                        dtBankCodeLoop = New DataTable
                                        dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                        If dtBankCodeLoop.Rows.Count > 0 Then
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                Dim drBankLoop As DataRow()
                                                drBankLoop = dtBankCodeLoop.Select("BankCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim())
                                                If drBankLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If

                                        Else
                                            Msg = "Invalid Bank Code."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                        dtBankNameLoop = New DataTable
                                        dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                        If dtBankNameLoop.Rows.Count > 0 Then
                                            Dim drBankNameLoop As DataRow()
                                            drBankNameLoop = dtBankNameLoop.Select("BankName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                            If drBankNameLoop.Count = 0 Then
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Bank Name."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drPrintLocationLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                            Else
                                                drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                            End If
                                            If drPrintLocationLoop.Count = 0 Then
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Print Location or Print Location is Not Tagged."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtDDPayableLocationLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtDDPayableLocationLoop.Count = 0 Then
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid DD Payable Location."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtBeneficiaryCountryLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary Country."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtBeneficiaryProvinceLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary Province."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drBeneficiaryCityLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drBeneficiaryCityLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary City."
                                            Exit For
                                        End If
                                    End If
                                End If
                            Else
                                ''Value Date

                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                    If Not drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If

                                End If
                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                    If Not drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If

                                End If
                            End If
                        Next
                    Case "po"
                        For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                            If objAccountPaymentNatureTemplateField.IsRequiredPO = True Then
                                If drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString().Trim() & " is empty."
                                    Exit For
                                Else
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "Amount" Then
                                        Try
                                            If Double.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), ResultDouble) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                        If CDbl(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) < 1 Then
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " should be greater than or equal to 1"
                                            Exit For
                                        End If
                                        If IsNumeric(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) = True Then
                                            
                                        Else
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End If
                                    End If
                                    ''Value Date

                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankCode" Then
                                        dtBankCodeLoop = New DataTable
                                        dtBankCodeLoop = DirectCast(ViewState("dtBankCodeLoop"), DataTable)
                                        If dtBankCodeLoop.Rows.Count > 0 Then
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                Dim drBankLoop As DataRow()
                                                drBankLoop = dtBankCodeLoop.Select("BankCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim())
                                                If drBankLoop.Count = 0 Then
                                                    Msg = "Invalid Bank Code."
                                                    Exit For
                                                End If
                                            Else
                                                Msg = "Invalid Bank Code."
                                                Exit For
                                            End If

                                        Else
                                            Msg = "Invalid Bank Code."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryBankName" Then
                                        dtBankNameLoop = New DataTable
                                        dtBankNameLoop = DirectCast(ViewState("dtBankNameLoop"), DataTable)
                                        If dtBankNameLoop.Rows.Count > 0 Then
                                            Dim drBankNameLoop As DataRow()
                                            drBankNameLoop = dtBankNameLoop.Select("BankName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "'")
                                            If drBankNameLoop.Count = 0 Then
                                                Msg = "Invalid Bank Name."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Bank Name."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintLocation" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtPrintLocationLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drPrintLocationLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeID=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & " AND ProductTypeCode='" & ProductTypeCode & "'")
                                            Else
                                                drPrintLocationLoop = dtPrintLocationLoop.Select("OfficeName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "' AND ProductTypeCode='" & ProductTypeCode & "'")
                                            End If
                                            If drPrintLocationLoop.Count = 0 Then
                                                Msg = "Invalid Print Location or Print Location is Not Tagged."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Print Location or Print Location is Not Tagged."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "DDPayableLocation" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtDDPayableLocationLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtDDPayableLocationLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtDDPayableLocationLoop = dtPrintLocationLoop.Select("CityName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtDDPayableLocationLoop.Count = 0 Then
                                                Msg = "Invalid DD Payable Location."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid DD Payable Location."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCountry" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCountryLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtBeneficiaryCountryLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtBeneficiaryCountryLoop = dtPrintLocationLoop.Select("CountryName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtBeneficiaryCountryLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary Country."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary Country."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryProvince" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryProvinceLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drdtBeneficiaryProvinceLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drdtBeneficiaryProvinceLoop = dtPrintLocationLoop.Select("ProvinceName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drdtBeneficiaryProvinceLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary Province."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary Province."
                                            Exit For
                                        End If
                                    End If
                                    If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "BeneficiaryCity" Then
                                        dtPrintLocationLoop = New DataTable
                                        dtPrintLocationLoop = DirectCast(ViewState("dtBeneficiaryCityLoop"), DataTable)
                                        If dtPrintLocationLoop.Rows.Count > 0 Then
                                            Dim drBeneficiaryCityLoop As DataRow()
                                            Dim i As Integer = 0
                                            If Integer.TryParse(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), i) = True Then
                                                drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityCode=" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() & "")
                                            Else
                                                drBeneficiaryCityLoop = dtPrintLocationLoop.Select("CityName='" & drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim().ToLower & "'")
                                            End If
                                            If drBeneficiaryCityLoop.Count = 0 Then
                                                Msg = "Invalid Beneficiary City."
                                                Exit For
                                            End If
                                        Else
                                            Msg = "Invalid Beneficiary City."
                                            Exit For
                                        End If
                                    End If
                                End If
                            Else
                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "ValueDate" Then
                                    If Not drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If

                                End If
                                If objAccountPaymentNatureTemplateField.FieldName.Trim.ToString() = "PrintDate" Then
                                    If Not drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                                        Try
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                                            Dim ResultDate As Date = Nothing
                                            If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                                Exit For
                                            ElseIf ValueDate < Now.Date Then
                                                Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " must be greater than or equal to current date."
                                                Exit For
                                            End If
                                        Catch ex As Exception
                                            Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is not in correct format."
                                            Exit For
                                        End Try
                                    End If

                                End If
                            End If
                        Next
                End Select
            End If
            Return Msg
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try
    End Function


    Private Function SaveVerifiedInstructions() As Boolean
        Dim dtVerified, dtVerifiedDetail As New DataTable
        Dim drVer As DataRow
        Dim dcVer As DataColumn
        Dim drVerDetail As DataRow
        Dim dcVerDetail As DataColumn
        Dim objInstruction As ICInstruction
        Dim objInstructionDetail As ICInstructionDetail
        Dim objBene As ICBeneficiary
        Dim objUser As New ICUser
        Dim objFile As New ICFiles
        Dim objCompany As New ICCompany
        Dim ActionStr As String = ""
        Dim HeaderDetailRelationNo As Integer = 0
        Dim IsDualFile As Boolean = False
        Dim objICProductType As ICProductType
        Dim StrAccountDetails As String()
        objUser.es.Connection.CommandTimeout = 3600
        objFile.es.Connection.CommandTimeout = 3600
        objCompany.es.Connection.CommandTimeout = 3600


        Dim FieldName, FieldType, FieldID As String
        Dim colName As String()
        FieldName = ""
        FieldType = ""
        FieldID = ""
        Try
            objFile.LoadByPrimaryKey(hfClientFileID.Value.ToString())
            objCompany.LoadByPrimaryKey(objFile.CompanyCode.ToString())
            If Not ViewState("dtVarifiedHeader") Is Nothing Then
                dtVerified = DirectCast(ViewState("dtVarifiedHeader"), DataTable)
                dtVerifiedDetail = DirectCast(ViewState("dtVarifiedDetail"), DataTable)
                IsDualFile = True
            Else
                dtVerified = DirectCast(ViewState("dtVarified"), DataTable)
            End If
            If dtVerified.Rows.Count > 0 Then
                For Each drVer In dtVerified.Rows
                    objUser = New ICUser
                    objUser.es.Connection.CommandTimeout = 3600
                    objUser.LoadByPrimaryKey(Me.UserId)

                    objInstruction = New ICInstruction
                    objBene = New ICBeneficiary
                    objInstructionDetail = New ICInstructionDetail
                    objInstruction.es.Connection.CommandTimeout = 3600
                    objBene.es.Connection.CommandTimeout = 3600
                    objInstructionDetail.es.Connection.CommandTimeout = 3600
                    objICProductType = New ICProductType
                    objICProductType.LoadByPrimaryKey(drVer("ProductTypeCode-65").ToString)
                    Dim IsInstructonDetailForSingleInstruction As Boolean = False
                    Dim CheckDetailCount As Integer = 0
                    Dim EmptyAccountDetails As String = Nothing
                    If objICProductType.DisbursementMode = "Direct Credit" Then
                        Try
                            StrAccountDetails = Nothing
                            StrAccountDetails = (CBUtilities.TitleFetch(drVer("BeneficiaryAccountNo").ToString, ICBO.CBUtilities.AccountType.RB, "IC Client Type", drVer("BeneficiaryAccountNo").ToString).ToString.Split(";"))
                        Catch ex As Exception
                            StrAccountDetails = Nothing
                            EmptyAccountDetails = "-;-;-"
                            StrAccountDetails = EmptyAccountDetails.Split(";")
                        End Try
                    End If

                    For Each dcVer In dtVerified.Columns
                        ActionStr = ""

                        If dcVer.ColumnName.ToString().Contains("-") Then
                            colName = dcVer.ColumnName.ToString().Split("-")
                            FieldName = colName(0).ToString()
                            FieldID = colName(1).ToString()
                            Dim objICField As New ICFieldsList
                            objICField.es.Connection.CommandTimeout = 3600
                            objICField.LoadByPrimaryKey(FieldID)
                            FieldType = ""
                            FieldType = objICField.FieldType.ToString

                            If FieldType.ToString() = "NonFlexi" Then
                                Select Case FieldName.ToString()
                                    Case "Description"
                                        objInstruction.Description = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Remarks"
                                        objInstruction.Remarks = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Amount"
                                        objInstruction.Amount = drVer(dcVer.ColumnName.ToString()).ToString()
                                        objInstruction.AmountInWords = NumToWord.SpellNumber(CDbl(drVer(dcVer.ColumnName.ToString()).ToString())).TrimStart().TrimEnd().Trim().ToString()
                                    Case "AmountInWords"
                                        objInstruction.AmountInWords = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryAccountNo"


                                        objInstruction.BeneficiaryAccountNo = drVer(dcVer.ColumnName.ToString()).ToString()
                                        objBene.BeneAccountNumber = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneAccountBranchCode"
                                        If objICProductType.DisbursementMode = "Direct Credit" Then
                                            If drVer(dcVer.ColumnName.ToString()).ToString() <> "" Then
                                                objInstruction.BeneAccountBranchCode = drVer(dcVer.ColumnName.ToString()).ToString()
                                            Else
                                                objInstruction.BeneAccountBranchCode = StrAccountDetails(1).ToString
                                            End If
                                        Else
                                            objInstruction.BeneAccountBranchCode = drVer(dcVer.ColumnName.ToString()).ToString()

                                        End If
                                    Case "BeneAccountCurrency"
                                        If objICProductType.DisbursementMode = "Direct Credit" Then
                                            If drVer(dcVer.ColumnName.ToString()).ToString() <> "" Then
                                                objInstruction.BeneAccountCurrency = drVer(dcVer.ColumnName.ToString()).ToString()
                                            Else
                                                objInstruction.BeneAccountCurrency = StrAccountDetails(2).ToString
                                            End If
                                        Else
                                            objInstruction.BeneAccountCurrency = drVer(dcVer.ColumnName.ToString()).ToString()

                                        End If

                                    Case "BeneficiaryAccountTitle"
                                        If objICProductType.DisbursementMode = "Direct Credit" Then
                                            If drVer(dcVer.ColumnName.ToString()).ToString() <> "" Then
                                                objInstruction.BeneficiaryAccountTitle = drVer(dcVer.ColumnName.ToString()).ToString()
                                            Else
                                                objInstruction.BeneficiaryAccountTitle = StrAccountDetails(0).ToString
                                            End If
                                        Else
                                            objInstruction.BeneficiaryAccountTitle = drVer(dcVer.ColumnName.ToString()).ToString()

                                        End If

                                    Case "BeneficiaryAddress"
                                        objInstruction.BeneficiaryAddress = drVer(dcVer.ColumnName.ToString()).ToString()
                                        objBene.BeneAddress1 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryBankCode"
                                        objInstruction.BeneficiaryBankCode = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryBankName"
                                        objInstruction.BeneficiaryBankName = drVer(dcVer.ColumnName.ToString()).ToString()
                                        objBene.BankName = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryBankAddress"
                                        objInstruction.BeneficiaryBankAddress = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryBranchCode"
                                        objInstruction.BeneficiaryBranchCode = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryBranchName"
                                        objInstruction.BeneficiaryBranchName = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryCity"
                                        Dim objCity As New ICCity
                                        objCity = ICCityController.GetCityByCityNameOrCode(drVer(dcVer.ColumnName.ToString()).ToString())
                                        If Not objCity Is Nothing Then
                                            objInstruction.BeneficiaryCity = objCity.CityCode
                                            objBene.BeneCity = objCity.CityCode
                                        End If
                                    Case "BeneficiaryCNIC"
                                        objInstruction.BeneficiaryCNIC = drVer(dcVer.ColumnName.ToString()).ToString()
                                        objBene.BeneIDNO = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryCountry"
                                        Dim objCountry As New ICCountry
                                        objCountry = ICCountryController.GetCountryByCountryNameOrCode(drVer(dcVer.ColumnName.ToString()).ToString())
                                        If Not objCountry Is Nothing Then
                                            objInstruction.BeneficiaryCountry = objCountry.CountryCode
                                        End If
                                    Case "BeneficiaryEmail"
                                        objInstruction.BeneficiaryEmail = drVer(dcVer.ColumnName.ToString()).ToString()
                                        objBene.BeneEmail = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryMobile"
                                        objInstruction.BeneficiaryMobile = drVer(dcVer.ColumnName.ToString()).ToString()
                                        objBene.BeneCellNumber = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryName"
                                        objInstruction.BeneficiaryName = drVer(dcVer.ColumnName.ToString()).ToString()
                                        objBene.BeneName = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryPhone"
                                        objInstruction.BeneficiaryPhone = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryProvince"
                                        Dim objProvince As New ICProvince
                                        objProvince = ICProvinceController.GetProvinceByCountryNameOrCode(drVer(dcVer.ColumnName.ToString()).ToString())
                                        If Not objProvince Is Nothing Then
                                            objInstruction.BeneficiaryProvince = objProvince.ProvinceCode
                                            objBene.BeneProvince = objProvince.ProvinceCode
                                        End If
                                    Case "ClientBankCode"
                                        objInstruction.ClientBankCode = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "ClientBankName"
                                        objInstruction.ClientBankName = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "ClientBranchAddress"
                                        objInstruction.ClientBranchAddress = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "ClientBranchCode"
                                        objInstruction.ClientBranchCode = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "ClientBranchName"
                                        objInstruction.ClientBranchName = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "DetailAmount"
                                        objInstruction.DetailAmount = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "DDPayableLocation"
                                        Dim objCity As New ICCity
                                        objCity = ICCityController.GetCityByCityNameOrCode(drVer(dcVer.ColumnName.ToString()).ToString())
                                        If Not objCity Is Nothing Then
                                            objInstruction.DDDrawnCityCode = objCity.CityCode
                                            objInstruction.DDPayableLocation = objCity.CityName
                                        End If
                                    Case "InstrumentNo"
                                        objInstruction.InstrumentNo = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "PrintLocation"
                                        Dim objOffice As New ICOffice
                                        objOffice = ICOfficeController.GetPrintLocationByPrintLocationNameOrCode(drVer(dcVer.ColumnName.ToString()).ToString())
                                        If Not objOffice Is Nothing Then
                                            objInstruction.PrintLocationCode = objOffice.OfficeID
                                            objInstruction.PrintLocationName = objOffice.OfficeName
                                        End If
                                    Case "ProductTypeCode"
                                        Dim objProductType As New ICProductType
                                        If objProductType.LoadByPrimaryKey(drVer(dcVer.ColumnName.ToString()).Trim.ToString()) Then
                                            objInstruction.ProductTypeCode = objProductType.ProductTypeCode
                                            objInstruction.PaymentMode = objProductType.DisbursementMode
                                        End If
                                    Case "TXN_Code"
                                        objInstruction.TXNCode = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "ReferenceField1"
                                        objInstruction.ReferenceField1 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "ReferenceField2"
                                        objInstruction.ReferenceField2 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "ReferenceField3"
                                        objInstruction.ReferenceField3 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "ValueDate"
                                        If Not drVer(dcVer.ColumnName.ToString()).ToString() = "" Then

                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drVer(dcVer.ColumnName.ToString()).ToString(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)
                                            objInstruction.ValueDate = ValueDate
                                        End If
                                    Case "PrintDate"
                                        If Not drVer(dcVer.ColumnName.ToString()).ToString() = "" Then
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim PrintDate As Date = Date.ParseExact(drVer(dcVer.ColumnName.ToString()).ToString(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)
                                            objInstruction.PrintDate = PrintDate
                                        End If
                                End Select
                            ElseIf FieldType.ToString() = "DetailField" Then
                                If CheckDetailCount = 0 Then
                                    IsInstructonDetailForSingleInstruction = True
                                    CheckDetailCount = CheckDetailCount + 1
                                End If
                                Select Case FieldName.ToString
                                    Case "Detail_Amount"
                                        objInstructionDetail.DetailAmount = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneAccountNo"
                                        objInstructionDetail.DetailBeneAccountNo = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneAddress"
                                        objInstructionDetail.DetailBeneAddress = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneBankName"
                                        objInstructionDetail.DetailBeneBankName = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneBranchAddress"
                                        objInstructionDetail.DetailBeneBranchAddress = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneBranchCode"
                                        objInstructionDetail.DetailBeneBranchCode = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneBranchName"
                                        objInstructionDetail.DetailBeneBranchName = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneCNIC"
                                        objInstructionDetail.DetailBeneCNIC = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneCountry"
                                        objInstructionDetail.DetailBeneCountry = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneProvince"
                                        objInstructionDetail.DetailBeneProvince = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneCity"
                                        objInstructionDetail.DetailBeneCity = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneEmail"
                                        objInstructionDetail.DetailBeneEmail = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneMobile"
                                        objInstructionDetail.DetailBeneMobile = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BenePhone"
                                        objInstructionDetail.DetailBenePhone = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_InvoiceNo"
                                        objInstructionDetail.DetailInvoiceNo = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_IS_NO"
                                        objInstructionDetail.DetailISNO = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_TXN_CODE"
                                        objInstructionDetail.DetailTXNCODE = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_DetailAmt"
                                        objInstructionDetail.DetailDetailAmt = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_1"
                                        objInstructionDetail.DetailFFText1 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_2"
                                        objInstructionDetail.DetailFFText2 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_3"
                                        objInstructionDetail.DetailFFText3 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_4"
                                        objInstructionDetail.DetailFFText4 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_5"
                                        objInstructionDetail.DetailFFText5 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_6"
                                        objInstructionDetail.DetailFFText6 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_7"
                                        objInstructionDetail.DetailFFText7 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_8"
                                        objInstructionDetail.DetailFFText8 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_9"
                                        objInstructionDetail.DetailFFText9 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_10"
                                        objInstructionDetail.DetailFFText10 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_11"
                                        objInstructionDetail.DetailFFText11 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_12"
                                        objInstructionDetail.DetailFFText12 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_13"
                                        objInstructionDetail.DetailFFText13 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_14"
                                        objInstructionDetail.DetailFFText14 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_15"
                                        objInstructionDetail.DetailFFText15 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_16"
                                        objInstructionDetail.DetailFFText16 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_17"
                                        objInstructionDetail.DetailFFText17 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_18"
                                        objInstructionDetail.DetailFFText18 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_19"
                                        objInstructionDetail.DetailFFText19 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_20"
                                        objInstructionDetail.DetailFFText20 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Amt_1"
                                        objInstructionDetail.DetailFFAmt1 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Amt_2"
                                        objInstructionDetail.DetailFFAmt2 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Amt_3"
                                        objInstructionDetail.DetailFFAmt3 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Amt_4"
                                        objInstructionDetail.DetailFFAmt4 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Amt_5"
                                        objInstructionDetail.DetailFFAmt5 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Amt_6"
                                        objInstructionDetail.DetailFFAmt6 = drVer(dcVer.ColumnName.ToString()).ToString()
                                End Select
                            End If
                        Else
                            If IsDualFile = True Then
                                HeaderDetailRelationNo = CInt(drVer(0).ToString())
                            End If
                        End If
                    Next


                    objInstruction.ClientName = objCompany.CompanyName
                    objInstruction.ClientEmail = objCompany.EmailAddress
                    objInstruction.ClientMobile = objCompany.PhoneNumber2
                    objInstruction.ClientAddress = objCompany.Address
                    objInstruction.CreatedOfficeCode = objUser.UpToICOfficeByOfficeCode.OfficeID

                    objInstruction.ClientCity = objUser.UpToICOfficeByOfficeCode.UpToICCityByCityID.CityCode.ToString
                    objInstruction.ClientProvince = objUser.UpToICOfficeByOfficeCode.UpToICCityByCityID.UpToICProvinceByProvinceCode.ProvinceCode.ToString
                    objInstruction.ClientCountry = objUser.UpToICOfficeByOfficeCode.UpToICCityByCityID.UpToICProvinceByProvinceCode.UpToICCountryByCountryCode.CountryCode.ToString
                    objInstruction.ClientAccountNo = objFile.AccountNumber.ToString()
                    objInstruction.ClientAccountBranchCode = objFile.BranchCode.ToString()
                    objInstruction.ClientAccountCurrency = objFile.Currency.ToString()
                    objInstruction.PaymentNatureCode = objFile.PaymentNatureCode.ToString()
                    objInstruction.AcquisitionMode = "File Upload"
                    objInstruction.CompanyCode = objCompany.CompanyCode
                    objInstruction.GroupCode = objCompany.GroupCode
                    objInstruction.FileBatchNo = objFile.FileBatchNo
                    objInstruction.FileID = objFile.FileID
                    objInstruction.Status = 1
                    objInstruction.LastStatus = 1
                    objInstruction.CreateBy = Me.UserId
                    objInstruction.CreateDate = Date.Now
                    ActionStr += "Instruction via File Upload for Beneficiary [ " & objInstruction.BeneficiaryName.ToString & " ] of Client [ " & objCompany.CompanyName & " ], "
                    ActionStr += "of Amount [ " & objInstruction.Amount.ToString & "] [ " & objInstruction.AmountInWords.ToString & " ] added. Client Account Number [ " & objInstruction.ClientAccountNo & " ],"
                    ActionStr += " Branch Code [" & objInstruction.ClientAccountBranchCode & " ], Currency [" & objInstruction.ClientAccountCurrency & " ]."
                    ActionStr += " Product Type [ " & objInstruction.ProductTypeCode & " ], Disbursement Mode [ " & objInstruction.PaymentMode & " ], Payment Nature [ " & objInstruction.PaymentNatureCode & " ]."


                    objInstruction.InstructionID = ICInstructionController.AddInstruction(objInstruction, objBene, Me.UserId, Me.UserInfo.Username, ActionStr.ToString())
                    If IsInstructonDetailForSingleInstruction = True Then
                        ' Add Instruction Detail for Single Instruction
                        ICInstructionController.AddInstructionDetail(objInstructionDetail, Me.UserId, Me.UserInfo.Username)
                    End If
                    If objInstruction.InstructionID IsNot Nothing Then
                        If IsDualFile = True Then
                            For Each drVerDetail In dtVerifiedDetail.Rows
                                If CInt(drVerDetail(0).ToString()) = HeaderDetailRelationNo Then
                                    objInstructionDetail = New ICInstructionDetail
                                    objInstructionDetail.InstructionID = objInstruction.InstructionID
                                    For Each dcVerDetail In dtVerifiedDetail.Columns
                                        If dcVerDetail.ColumnName.ToString().Contains("-") Then
                                            colName = dcVerDetail.ColumnName.ToString().Split("-")
                                            Dim objICField As New ICFieldsList
                                            objICField.es.Connection.CommandTimeout = 3600


                                            FieldName = colName(0).ToString()
                                            FieldID = colName(1).ToString()
                                            objICField.LoadByPrimaryKey(FieldID)
                                            FieldType = ""

                                            FieldType = objICField.FieldType.ToString()
                                            If FieldType.ToString() = "FixFormat" Then
                                                Select Case FieldName.ToString()
                                                    Case "FF_Amt_1"
                                                        objInstructionDetail.FFAmt1 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_Amt_2"
                                                        objInstructionDetail.FFAmt2 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_Amt_3"
                                                        objInstructionDetail.FFAmt3 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_Amt_4"
                                                        objInstructionDetail.FFAmt4 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_Amt_5"
                                                        objInstructionDetail.FFAmt5 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_Amt_6"
                                                        objInstructionDetail.FFAmt6 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_1"
                                                        objInstructionDetail.FFTxt1 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_2"
                                                        objInstructionDetail.FFTxt2 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_3"
                                                        objInstructionDetail.FFTxt3 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_4"
                                                        objInstructionDetail.FFTxt4 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_5"
                                                        objInstructionDetail.FFTxt5 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_6"
                                                        objInstructionDetail.FFTxt6 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_7"
                                                        objInstructionDetail.FFTxt7 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_8"
                                                        objInstructionDetail.FFTxt8 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_9"
                                                        objInstructionDetail.FFTxt9 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_10"
                                                        objInstructionDetail.FFTxt10 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_11"
                                                        objInstructionDetail.FFTxt11 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_12"
                                                        objInstructionDetail.FFTxt12 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_13"
                                                        objInstructionDetail.FFTxt13 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_14"
                                                        objInstructionDetail.FFTxt14 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_15"
                                                        objInstructionDetail.FFTxt15 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_16"
                                                        objInstructionDetail.FFTxt16 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_17"
                                                        objInstructionDetail.FFTxt17 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_18"
                                                        objInstructionDetail.FFTxt18 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_19"
                                                        objInstructionDetail.FFTxt19 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_20"
                                                        objInstructionDetail.FFTxt20 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                End Select
                                            End If
                                        End If
                                    Next
                                    ' Add Instruction Detail
                                    ICInstructionController.AddInstructionDetail(objInstructionDetail, Me.UserId, Me.UserInfo.Username)
                                End If
                            Next
                        End If
                        For Each dcVer In dtVerified.Columns
                            If dcVer.ColumnName.ToString().Contains("-") Then
                                Dim objICField As New ICFieldsList
                                objICField.es.Connection.CommandTimeout = 3600

                                colName = dcVer.ColumnName.ToString().Split("-")
                                FieldName = colName(0).ToString()
                                FieldID = colName(1).ToString()
                                objICField.LoadByPrimaryKey(FieldID)
                                FieldType = ""

                                FieldType = objICField.FieldType.ToString()

                                If FieldType.ToString() = "FlexiField" Then
                                    Dim objFlexiFieldData As New ICFlexiFieldData

                                    objFlexiFieldData.es.Connection.CommandTimeout = 3600

                                    objFlexiFieldData.FieldID = FieldID
                                    objFlexiFieldData.InstructionID = objInstruction.InstructionID
                                    objFlexiFieldData.FieldValue = drVer(dcVer.ColumnName.ToString()).ToString()
                                    ICFlexiFieldDataController.AddFlexiFieldData(objFlexiFieldData)
                                End If
                            End If
                        Next
                    End If


                Next
            End If
            Return True
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        If Page.IsValid Then
            Try
                Dim objFile As New ICFiles
                Dim Action As String = ""
                Dim AuditType As String = ""


                If objFile.LoadByPrimaryKey(hfClientFileID.Value.ToString()) Then
                    objFile.Status = "Rejected"
                    objFile.ProcessDate = Date.Now
                    objFile.ProcessBy = Me.UserId

                    Action = "Client File: File [Name: " & objFile.FileName & " ; ID: " & objFile.FileID & "] is rejected [Status: " & objFile.Status & " ; ProcessedBy: " & objFile.ProcessBy & " ; ProcessDate: " & objFile.ProcessDate & "] by User [ID: " & Me.UserId.ToString() & " ; Name: " & Me.UserInfo.Username.ToString() & "]."
                    AuditType = "Client File"
                    ICFilesController.UpdateClientFile(objFile, Me.UserId, Me.UserInfo.Username, Action.ToString(), AuditType)

                    UIUtilities.ShowDialog(Me, "Client File Upload", "File rejected.", IC.Dialogmessagetype.Success, NavigateURL())
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        
        Response.Redirect(NavigateURL(41), False)
        Exit Sub
    End Sub

    Protected Sub btnProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        If Page.IsValid Then
            Try
                Dim objICFiles As New ICFiles
                objICFiles.LoadByPrimaryKey(hfClientFileID.Value.ToString)
                objICFiles.Status = "In Process"
                Dim ActionStr As String = Nothing
                ActionStr = "Client File [ " & objICFiles.OriginalFileName & " ] of Client [ " & objICFiles.UpToICCompanyByCompanyCode.CompanyName & " ] status updated from"
                ActionStr += " [ " & objICFiles.Status & " ] to [ In Process ]. Action was taken by User [ " & Me.UserId.ToString & " ] [ " & Me.UserInfo.Username.ToString & " ]"
                ICFilesController.UpdateClientFile(objICFiles, Me.UserId.ToString, Me.UserInfo.Username.ToString, ActionStr, "UPDATE")
                Dim res As IAsyncResult = ParseAndUploadInstructionInFileVIAAsynchronusMethod(Me.UserId.ToString, hfClientFileID.Value.ToString, "File Upload", ViewState("dtVarified"), ViewState("dtVarifiedHeader"), ViewState("dtVarifiedDetail"), ViewState("IsBeneAdded"), txtTotalAmount.Text, txtTnxCount.Text)
                Session("FileUploadingResult") = res

                UIUtilities.ShowDialog(Me, "Client File Upload", "File parsing is in progress.", ICBO.IC.Dialogmessagetype.Success, NavigateURL(101))

            Catch ex As Exception
                Dim objICFiles As New ICFiles
                objICFiles.LoadByPrimaryKey(hfClientFileID.Value.ToString)
                objICFiles.Status = "Pending Read"
                objICFiles.ProcessDate = Nothing
                objICFiles.TotalAmount = Nothing
                objICFiles.TransactionCount = Nothing
                objICFiles.ProcessBy = Nothing
                Dim ActionStr As String = Nothing
                ActionStr = "Client File [ " & objICFiles.OriginalFileName & " ] of Client [ " & objICFiles.UpToICCompanyByCompanyCode.CompanyName & " ] status updated from"
                ActionStr += " [ " & objICFiles.Status & " ] to [ Pending Read ] while parsing while faile. Action was taken by User [ " & Me.UserId.ToString & " ] [ " & Me.UserInfo.Username.ToString & " ]"
                ICFilesController.UpdateClientFile(objICFiles, Me.UserId.ToString, Me.UserInfo.Username.ToString, ActionStr, "Client File")
                ICInstructionController.DeleteInstructionByFileIDInError(hfClientFileID.Value.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Sub CallBackMethod(ByVal ar As IAsyncResult)
        Dim result As AsyncResult = CType(ar, AsyncResult)
        Dim longm As LongRun = CType(result.AsyncDelegate, LongRun)

    End Sub
    Private Function ParseAndUploadInstructionInFileVIAAsynchronusMethod(ByVal UsersID As String, ByVal UploadedFileID As String, ByVal AcqMode As String, ByVal dtVerified As DataTable, ByVal dtVerifiedHeader As DataTable, ByVal dtVerifiedDetail As DataTable, Optional IsBeneAdded As Boolean = 0, Optional AmountInFile As Double = Nothing, Optional TxnCount As Integer = Nothing) As IAsyncResult
        Try
            Dim longm As LongRun = New LongRun(AddressOf ICFilesController.AsynChronusMethodForParseAndSaveInstructions) ' Delegate will call LongRunningMethod

            Dim ar As IAsyncResult = longm.BeginInvoke(Me.UserId.ToString, hfClientFileID.Value.ToString, AcqMode, ViewState("dtVarified"), ViewState("dtVarifiedHeader"), ViewState("dtVarifiedDetail"), IsBeneAdded, AmountInFile, TxnCount, New AsyncCallback(AddressOf CallBackMethod), Nothing)

            '//”CHECK” will go as function parameter to LongRunningMethod
            Return ar     ' //Once LongRunningMethod get over CallBackMethod method will be invoked. 

        Catch ex As Exception
            Throw ex
        End Try



    End Function



    Protected Sub gvClientFiles_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvClientFiles.ItemDataBound

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then

            Dim AppPath As String = "" ' DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()

            Dim hlDownload As New HyperLink

            hlDownload = DirectCast(e.Item.Cells(13).FindControl("hlDownload"), HyperLink)

            hlDownload.NavigateUrl = "/DownloadFile.aspx?fileid=" & e.Item.Cells(2).Text.ToString().EncryptString()

        End If
    End Sub
    
    Private Sub Clear()
        Try
            Dim objICUser As New ICUser
            Dim objICGroup As New ICGroup

            objICUser.es.Connection.CommandTimeout = 3600
            objICGroup.es.Connection.CommandTimeout = 3600
            If Me.UserInfo.IsSuperUser = False Then
                If objICUser.LoadByPrimaryKey(Me.UserId) Then
                    If objICUser.UserType = "Client User" Then
                        objICGroup = objICUser.UpToICOfficeByOfficeCode.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode
                        LoadddlGroup()
                        ViewddlGroup.SelectedValue = objICGroup.GroupCode.ToString
                        ViewddlGroup.Enabled = False
                        SetArraListRoleID()
                        LoadddlCompany()
                        LoadddlAccountPaymentNatureByUserID()
                        LoadddlFileUploadTemplate()
                    ElseIf objICUser.UserType.ToString = "Bank User" Then
                        SetArraListRoleID()
                        LoadddlGroup()
                        LoadddlCompany()
                        LoadddlAccountPaymentNatureByUserID()
                        LoadddlFileUploadTemplate()
                    End If
                End If
            Else
                UIUtilities.ShowDialog(Me, "Error", "Sorry! You are not authorized to login.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    '#End Region

    Protected Sub btnCancelMain_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelMain.Click
        Page.Validate()

        If Page.IsValid Then
            Try
                Response.Redirect(NavigateURL(41))
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click

        Try
                LoadgvClientFiles(True)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub




End Class

