﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CollectionValueDateModificationBulk.ascx.vb"
    Inherits="DesktopModules_BulkTitleFetch_BulkTitleFetch" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .auto-style1 {
        height: 23px;
    }
</style>
<%--<style type="text/css">
    .headingblue
    {
        font-weight: 700;
    }
</style>--%>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Value Date Modification Bulk" CssClass="headingblue"></asp:Label>
                        <br />
                        Note:&nbsp; Fields marked as
                        <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        &nbsp;are required.
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4" class="auto-style1">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblAccountsFile" runat="server" Text="Select File" CssClass="lbl"></asp:Label>
                        <asp:Label ID="Label27" runat="server" ForeColor="Red" Text="*"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 50%; font-weight: 700;" colspan="2">
                        <asp:FileUpload ID="fuplBulkAccountsFile" runat="server" />
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 15%">
                        <asp:RequiredFieldValidator ID="rfvFileUpload" runat="server" ErrorMessage="Please Select File."
                            ControlToValidate="fuplBulkAccountsFile" ValidationGroup="main"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" colspan="4">
                        &nbsp;
                        <asp:Button ID="btnShow" runat="server" CssClass="btn" Text="Show" Width="75px" ValidationGroup="main"/>
                        <asp:Button ID="btnProceed" runat="server" CssClass="btn" Text="Proceed" Width="75px" />
                        <asp:Button ID="btnCancel" runat="server" CssClass="btnCancel" Text="Cancel" 
                            CausesValidation="False" />
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <asp:Label ID="lblNRF" runat="server" Text="No Record Found." CssClass="headingblue"
                            Visible="false"></asp:Label>
                        <br />
                        <telerik:RadGrid ID="gvDatat" runat="server" AllowPaging="false" AllowSorting="false"
                            AutoGenerateColumns="true" CellSpacing="0" ShowFooter="True" PageSize="500" CssClass="RadGrid">
                           <AlternatingItemStyle CssClass="rgAltRow" />
                           <ClientSettings>
                           <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                           </ClientSettings>
                           <MasterTableView TableLayout="Fixed"></MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                        
                        <telerik:RadGrid ID="gvResult" runat="server" AllowPaging="false" AllowSorting="false"
                            AutoGenerateColumns="true" CellSpacing="0" ShowFooter="True" PageSize="500" CssClass="RadGrid">
                           <AlternatingItemStyle CssClass="rgAltRow" />
                           <ClientSettings>
                           <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                           </ClientSettings>
                            <MasterTableView TableLayout="Fixed">
                             
                                <CommandItemSettings ExportToPdfText="Export to PDF" ></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                </Columns>
                                 <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                        
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
