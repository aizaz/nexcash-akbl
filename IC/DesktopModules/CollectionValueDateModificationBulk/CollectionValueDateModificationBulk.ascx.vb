﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Entities.Portals
Imports DotNetNuke.Security.Roles
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports Telerik.Web.UI
Partial Class DesktopModules_BulkTitleFetch_BulkTitleFetch
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase

  
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnShow.Visible = CBool(htRights("Show"))
                btnProceed.Visible = False
                gvDatat.Style.Add("display", "none")
                gvResult.Style.Add("display", "none")
               
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Value Date Modification Bulk")
            ViewState("htRights") = htRights
          
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnShow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShow.Click
        Try
            Dim FileExtention As String = System.IO.Path.GetExtension(fuplBulkAccountsFile.PostedFile.FileName)
            Dim connectionString As String = ""
            Dim FileName As String = fuplBulkAccountsFile.FileName
            Dim FileLocation As String = Server.MapPath("~/UploadedFiles/") & FileName.ToString()

            If FileExtention <> ".xlsx" Then
                UIUtilities.ShowDialog(Me, "Value Date Modification Bulk", "Please select the .xlsx file.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            Dim aFileInfo As IO.FileInfo
            'Save file on HDD.
            fuplBulkAccountsFile.SaveAs(FileLocation)
            ''Check whether file extension is xls or xslx
            If FileExtention = ".xlsx" Then
                connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FileLocation & "; Extended Properties='Excel 12.0 Xml; HDR=Yes; IMEX=1;'"
            End If

            'Create OleDB Connection and OleDb Command
            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = "Sheet1$"
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()
            'Delete file from HDD.
            aFileInfo = New IO.FileInfo(FileLocation)
            If aFileInfo.Exists Then
                aFileInfo.Delete()
            End If
            If dtExcelRecords.Columns.Count <> 3 Then
                UIUtilities.ShowDialog(Me, "Value Date Modification Bulk", "Invalid number of columns", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            'If dtExcelRecords.Rows.Count > 500 Then
            '    UIUtilities.ShowDialog(Me, "Value Date Modification Bulk", "Invalid number of columns", ICBO.IC.Dialogmessagetype.Failure)
            '    Exit Sub
            'End If
            gvDatat.DataSource = Nothing
            gvDatat.DataSource = dtExcelRecords
            gvDatat.DataBind()
            If gvDatat.Items.Count > 0 Then
                gvDatat.Style.Remove("display")
                btnShow.Visible = False
                btnProceed.Visible = CBool(htRights("Process"))
            Else
                gvDatat.Style.Add("display", "none")
                btnShow.Visible = CBool(htRights("Show"))
                btnProceed.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            btnShow.Visible = True
        End Try
    End Sub

    

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL(41))
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnProceed_Click(sender As Object, e As EventArgs) Handles btnProceed.Click
        Try
            Dim dt As New DataTable
            Dim dt2 As New DataTable
            Dim BankCode, InstrumentNo, ValueDate As String
            Dim RefDate As Date = Nothing
            Dim RefDate2 As Date = Nothing
            Dim i As Integer
            Dim dr As DataRow
            dt.Columns.Add(New DataColumn("BankCode", GetType(System.String)))
            dt.Columns.Add(New DataColumn("InstrumentNo", GetType(System.String)))
            dt.Columns.Add(New DataColumn("ValueDate", GetType(System.String)))
            dt.Columns.Add(New DataColumn("Remarks", GetType(System.String)))

            If gvDatat.Items.Count > 0 Then
                For Each gvResultRow As GridDataItem In gvDatat.Items
                    BankCode = Nothing
                    InstrumentNo = Nothing
                    ValueDate = Nothing
                    BankCode = gvResultRow.Item("BankCode").Text.ToString
                    InstrumentNo = gvResultRow.Item("InstrumentNo").Text.ToString
                    ValueDate = gvResultRow.Item("ValueDate").Text.ToString
                    If BankCode = "" Then
                        dr = dt.NewRow
                        dr(0) = BankCode
                        dr(1) = InstrumentNo
                        dr(2) = ValueDate
                        dr(3) = "Bank Code is Empty"
                        dt.Rows.Add(dr)
                        Continue For
                    End If
                    If Integer.TryParse(BankCode, i) = False Then
                        dr = dt.NewRow
                        dr(0) = BankCode
                        dr(1) = InstrumentNo
                        dr(2) = ValueDate
                        dr(3) = "Invalid Bank Code"
                        dt.Rows.Add(dr)
                        Continue For
                    End If
                    If InstrumentNo = "" Then
                        dr = dt.NewRow
                        dr(0) = BankCode
                        dr(1) = InstrumentNo
                        dr(2) = ValueDate
                        dr(3) = "Instrument No is Empty"
                        dt.Rows.Add(dr)
                        Continue For
                    End If
                    If ValueDate = "" Then
                        dr = dt.NewRow
                        dr(0) = BankCode
                        dr(1) = InstrumentNo
                        dr(2) = ValueDate
                        dr(3) = "Value Date is Empty"
                        dt.Rows.Add(dr)
                        Continue For
                    End If

                    If Date.TryParse(ValueDate, RefDate) = False Then
                        dr = dt.NewRow
                        dr(0) = BankCode
                        dr(1) = InstrumentNo
                        dr(2) = ValueDate
                        dr(3) = "Invalid Value date"
                        dt.Rows.Add(dr)
                        Continue For
                    End If
                    'Date.TryParse(Date.Now/d.ToString, RefDate2)
                    'If RefDate <= RefDate2 Then
                    '    dr = dt.NewRow
                    '    dr(0) = BankCode
                    '    dr(1) = InstrumentNo
                    '    dr(2) = ValueDate
                    '    dr(3) = "Invalid Value date"
                    '    dt.Rows.Add(dr)
                    '    Continue For
                    'End If
                    If ICHolidayManagementController.IsHoliday(RefDate.ToString) = True Then
                        dr = dt.NewRow
                        dr(0) = BankCode
                        dr(1) = InstrumentNo
                        dr(2) = ValueDate
                        dr(3) = "Entered date is holiday"
                        dt.Rows.Add(dr)
                        Continue For
                    End If
                    If ICWorkingDaysController.IsWorkingDay(RefDate.ToString("dddd")) = False Then
                        dr = dt.NewRow
                        dr(0) = BankCode
                        dr(1) = InstrumentNo
                        dr(2) = ValueDate
                        dr(3) = "Entered date is non working day"
                        dt.Rows.Add(dr)
                        Continue For
                    End If
                    dt2 = New DataTable
                    dt2 = ICCollectionsController.GetCollectionsForValueDateByBankCodeAndInstrumentNo(BankCode, InstrumentNo)
                    If dt2.Rows(0)("CollectionStatusName") = "Cleared" Then
                        dr = dt.NewRow
                        dr(0) = BankCode
                        dr(1) = InstrumentNo
                        dr(2) = ValueDate
                        dr(3) = "Instrument already cleared"
                        dt.Rows.Add(dr)
                        Continue For
                    End If
                    Try
                        If ICCollectionsController.ModifyValueDateByBankCodeAndInstrumentNo(BankCode, InstrumentNo, ValueDate, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString) = True Then
                            dr = dt.NewRow
                            dr(0) = BankCode
                            dr(1) = InstrumentNo
                            dr(2) = ValueDate
                            dr(3) = "Value Date updated successfully"
                            dt.Rows.Add(dr)
                            Continue For
                        Else
                            dr = dt.NewRow
                            dr(0) = BankCode
                            dr(1) = InstrumentNo
                            dr(2) = ValueDate
                            dr(3) = "Value Date not updated successfully"
                            dt.Rows.Add(dr)
                            Continue For
                        End If
                    Catch ex As Exception
                        dr = dt.NewRow
                        dr(0) = BankCode
                        dr(1) = InstrumentNo
                        dr(2) = ValueDate
                        dr(3) = "Value Date not updated successfully"
                        dt.Rows.Add(dr)
                        Continue For
                    End Try
                Next
            Else
                UIUtilities.ShowDialog(Me, "Value Date Modification Bulk", "Please select the .xlsx file.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL)
                Exit Sub
            End If
            If dt.Rows.Count > 0 Then
                gvDatat.DataSource = Nothing
                gvDatat.DataBind()
                gvDatat.Visible = False
                gvResult.DataSource = Nothing
                gvResult.DataSource = dt
                gvResult.DataBind()
                gvResult.Style.Remove("display")
                btnProceed.Visible = False
            Else
                gvResult.DataSource = Nothing
                gvResult.DataBind()
                gvResult.Style.Add("display", "none")
                btnProceed.Visible = False
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
