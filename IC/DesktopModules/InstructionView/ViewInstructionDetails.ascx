﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewInstructionDetails.ascx.vb"
    Inherits="DesktopModules_InstructionView_ViewInstructionDetails" %>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 50%">
            <asp:Label ID="lblViewInstructionDetails" runat="server" Text="Instruction Details"
                CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 50%">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False" />
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%" colspan="2">
            <asp:Label ID="lblRNF" runat="server" Font-Bold="False" Text="No Record Found"
                Visible="False" CssClass="headingblue"></asp:Label>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%" colspan="2">
            <asp:DetailsView ID="dvViewInsDetails" runat="server" AutoGenerateColumns="False"
                CssClass="Grid" Width="100%" AllowPaging="True" EnableModelValidation="True"
                AutoGenerateRows="False">
                <AlternatingRowStyle CssClass="GridAltItem" />
                <FieldHeaderStyle Width="180px" HorizontalAlign="Left" VerticalAlign="Top" />
                <Fields>
                    <asp:BoundField DataField="InstructionID" HeaderText="Instruction No" SortExpression="InstructionID">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ClientName" HeaderText="Client Name" SortExpression="ClientName">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                     <asp:BoundField DataField="Amount" HeaderText="Amount" 
                        SortExpression="Amount" DataFormatString="{0:N2}" HtmlEncode="False">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="AmountInWords" HeaderText="Amount in Words" SortExpression="AmountInWords">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                     <asp:BoundField DataField="BeneName" HeaderText="Benefeciary Name" SortExpression="BeneName">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                       <asp:BoundField DataField="BeneficiaryEmail" HeaderText="Benefeciary Email" SortExpression="BeneficiaryEmail">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="BeneCellNo" HeaderText="Benefeciary Cell No" SortExpression="BeneCellNo">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="BeneficiaryAccountNo" HeaderText="Beneficiary Account No" SortExpression="BeneficiaryAccountNo">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="BeneficiaryAddress" HeaderText="Beneficiary Address" SortExpression="BeneficiaryAddress">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="BeneficiaryCountry" HeaderText="Beneficiary Country" SortExpression="Beneficiary Country">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                     <asp:BoundField DataField="BeneficiaryProvince" HeaderText="Beneficiary Province" SortExpression="BeneficiaryProvince">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="BeneficiaryCity" HeaderText="Beneficiary City" SortExpression="BeneficiaryCity">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                      <asp:BoundField DataField="PrintLocationName" HeaderText="Print Location" SortExpression="PrintLocationName">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="BeneficiaryCNIC" HeaderText="Benefeciary Identification CNIC No" SortExpression="BeneficiaryCNIC">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                     <asp:BoundField DataField="ClientAccountNo" HeaderText="Client Account No" SortExpression="ClientAccountNo">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                         <asp:BoundField DataField="ClientCountry" HeaderText="Client Country" SortExpression="ClientCountry">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ClientProvince" HeaderText="Client Province" SortExpression="ClientProvince">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ClientCity" HeaderText="Client City" SortExpression="ClientCity">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                 
                    <asp:BoundField DataField="BeneficiaryBankCode" HeaderText="Bank Code" SortExpression="BeneficiaryBankCode">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="BankName" HeaderText="Bank Name" SortExpression="BankName">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="BankAccountTitle" HeaderText="Bank Account Title" SortExpression="BankAccountTitle">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                
                    <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                  
                    <asp:BoundField DataField="Remarks" HeaderText="UNSC/OFAC Remarks" SortExpression="Remarks">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                  
                    <asp:BoundField DataField="CreateDate" HeaderText="Remitt Date" 
                        SortExpression="CreateDate" DataFormatString="{0:dd-MMM-yyyy HH:mm:ss}" 
                        HtmlEncode="False">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                      <asp:BoundField DataField="ValueDate" HeaderText="Value Date" 
                        SortExpression="ValueDate" DataFormatString="{0:dd-MMM-yyyy HH:mm:ss}" 
                        HtmlEncode="False">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                   <asp:BoundField DataField="LastStatus" HeaderText="Last Status" SortExpression="Last Status">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="FileID" HeaderText="File ID" SortExpression="FileID">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                
                    <asp:BoundField DataField="AcqMode" HeaderText="Acquisition Mode" SortExpression="AcqMode">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ErrorMessage" HeaderText="Error Message" SortExpression="ErrorMessage">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                     <asp:BoundField DataField="CreatedBy" HeaderText="Created By" SortExpression="CreatedBy">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                     <asp:BoundField DataField="DisbursedBy" HeaderText="Disbursed By" SortExpression="DisbursedBy">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:BoundField DataField="DisbursedDate" HeaderText="Disbursed Date" SortExpression="DisbursedDate">
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
       
                </Fields>
                <HeaderStyle CssClass="GridHeader" />
                <PagerStyle CssClass="GridPager" />
                <RowStyle CssClass="GridItem" />
            </asp:DetailsView>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%" colspan="2">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%" colspan="2">

            &nbsp;</td>
    </tr>
</table>
