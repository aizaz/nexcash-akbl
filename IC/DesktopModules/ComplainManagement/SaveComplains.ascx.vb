﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_ComplainManagement_SaveComplains
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase

    Private ComplainID As String
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            ComplainID = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                'If FRCComplainsController.GetStatusOfCopmlainsByComplainID(ComplainID).ToString() = "Close" Then
                '    btnSave.Visible = False
                'End If
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                lblResponceRq.Visible = True
                lblResponse.Visible = True
                txtResponse.Visible = True
                radResponse.Enabled = True
                lblPageHeader.Text = "Complain Details"
                btnSave.Text = "Reply"
                txtSubject.ReadOnly = True
                txtComplain.ReadOnly = True
                txtDisplayName.ReadOnly = True
                txtPhoneNo.ReadOnly = True
                txtEmailAddress.ReadOnly = True
                'txtSubject.Enabled = False
                'txtComplain.Enabled = False
                'txtDisplayName.Enabled = False
                'txtPhoneNo.Enabled = False
                'txtEmailAddress.Enabled = False
                Dim cComplain As New ICComplains()
                cComplain.es.Connection.CommandTimeout = 3600
                If cComplain.LoadByPrimaryKey(ComplainID) Then
                    txtSubject.MaxLength = 150
                    txtSubject.Text = "RE: " & cComplain.Subject.ToString()
                    txtComplain.Text = cComplain.ComplainMessage
                    txtDisplayName.Text = cComplain.Name
                    txtPhoneNo.Text = cComplain.PhoneNo
                    txtEmailAddress.Text = cComplain.Email
                End If
                LoadComplains(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub Clear()
        txtSubject.Text = ""
        txtComplain.Text = ""
        txtDisplayName.Text = ""
        txtPhoneNo.Text = ""
        txtEmailAddress.Text = ""
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Complain")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim cComplain As New ICComplains()
                Dim chkComplain As New ICComplains()

                cComplain.es.Connection.CommandTimeout = 3600
                chkComplain.es.Connection.CommandTimeout = 3600

                cComplain.Subject = txtSubject.Text.ToString()
                cComplain.ComplainMessage = txtResponse.Text.ToString()
                cComplain.Name = txtDisplayName.Text.ToString()
                cComplain.PhoneNo = txtPhoneNo.Text.ToString()
                If txtEmailAddress.Text = "" Or txtEmailAddress.Text = "Enter Email Address" Then
                    cComplain.Email = Nothing
                Else
                    cComplain.Email = txtEmailAddress.Text.ToString()
                End If
                chkComplain.LoadByPrimaryKey(ComplainID)
                cComplain.ParentID = chkComplain.ComplainID
                cComplain.Sender = chkComplain.Sender
                cComplain.Receiver = Me.UserId
                ICComplainsController.AddComplain(cComplain, chkComplain, True, Me.UserInfo.UserID.ToString, Me.UserInfo.Username)
                'FRCUtilities.AddAuditTrail("Reply on complain with parentid[" & chkComplain.ComplainID.ToString() & "] and subject[" & chkComplain.Subject.ToString() & "] added", "Complain", cComplain.ComplainID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                ICComplainsController.UpdateComplainStatus(ComplainID, ddlComplainStatus.SelectedValue.ToString(), Me.UserInfo.UserID.ToString, Me.UserInfo.Username)
                'FRCUtilities.AddAuditTrail("Complain with complainid[" & ComplainID.ToString() & "] and subject[" & chkComplain.Subject.ToString() & "] status updated as [" & ddlComplainStatus.SelectedValue.ToString() & "] by User[" & Me.UserInfo.Username.ToString() & "]", "Complain", cComplain.ComplainID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                If ddlComplainStatus.SelectedValue.ToString() = "Open" Then
                    EmailUtilities.GenerateEmailsForComplains("Open", chkComplain.Sender, False, txtSubject.Text.ToString(), ICUtilities.GetSettingValue("ApplicationURL"))
                ElseIf ddlComplainStatus.SelectedValue.ToString() = "Close" Then
                    EmailUtilities.GenerateEmailsForComplains("Close", chkComplain.Sender, False, txtSubject.Text.ToString(), ICUtilities.GetSettingValue("ApplicationURL"))
                End If
                UIUtilities.ShowDialog(Me, "Complain", "Reply added successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Clear()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    'Private Sub LoadComplains()
    '    Dim chkComplain As New ICComplains()
    '    chkComplain.es.Connection.CommandTimeout = 3600
    '    If chkComplain.LoadByPrimaryKey(ComplainID) Then
    '        gvComplains.DataSource = ICComplainsController.GetAllCopmlainsParentID(chkComplain.ComplainID)
    '        gvComplains.DataBind()
    '        If gvComplains.Rows.Count > 0 Then
    '            gvComplains.Visible = True
    '        Else
    '            gvComplains.Visible = False
    '        End If
    '    End If
    'End Sub

    Private Sub LoadComplains(ByVal IsBind As Boolean)

        Try
            Dim chkComplain As New ICComplains()

            chkComplain.es.Connection.CommandTimeout = 3600
            If chkComplain.LoadByPrimaryKey(ComplainID) Then
                ICComplainsController.GetAllCopmlainsParentID(chkComplain.ComplainID, gvComplains.CurrentPageIndex + 1, gvComplains.PageSize, gvComplains, IsBind)

                If gvComplains.Items.Count > 0 Then
                    gvComplains.Visible = True
                Else
                    gvComplains.Visible = False
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#Region " old load gv without telerik gv"

    'commit this gv because

    'Protected Sub gvComplains_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvComplains.PageIndexChanging
    '    gvComplains.PageIndex = e.NewPageIndex
    '    LoadComplains()
    'End Sub

    'Protected Sub gvComplains_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvComplains.RowCommand
    '    Try
    '        If e.CommandName = "ViewDesc" Then
    '            UIUtilities.ShowDialog(Me, "Response", ICComplainsController.GetCopmlainDescriptionByComplainID(e.CommandArgument.ToString()), ICBO.IC.Dialogmessagetype.Success)
    '        End If
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

#End Region

    Protected Sub gvComplains_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles gvComplains.ItemCommand
        Try
            If e.CommandName = "ViewDesc" Then
                UIUtilities.ShowDialog(Me, "Response", ICComplainsController.GetCopmlainDescriptionByComplainID(e.CommandArgument.ToString()), ICBO.IC.Dialogmessagetype.Success)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvComplains_ItemCreated(sender As Object, e As GridItemEventArgs) Handles gvComplains.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvComplains.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvComplains_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles gvComplains.ItemDataBound
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Dim objICUser As ICUser
            Dim SenderID As String = item.GetDataKeyValue("Sender")
            Dim ReceiverID As String = item.GetDataKeyValue("Receiver")
            objICUser = New ICUser
            If SenderID IsNot Nothing And SenderID IsNot "" Then
                objICUser.LoadByPrimaryKey(SenderID)
                item.Cells(4).Text = objICUser.DisplayName.ToString
            End If
            objICUser = New ICUser
            If ReceiverID IsNot Nothing And ReceiverID IsNot "" Then
                objICUser.LoadByPrimaryKey(ReceiverID)
                item.Cells(5).Text = objICUser.DisplayName.ToString
            Else
                item.Cells(5).Text = "-"
            End If



        End If
    End Sub

    Protected Sub gvComplains_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles gvComplains.NeedDataSource
        LoadComplains(False)

    End Sub



    Protected Sub gvComplains_PageIndexChanged(sender As Object, e As GridPageChangedEventArgs) Handles gvComplains.PageIndexChanged
        gvComplains.CurrentPageIndex = e.NewPageIndex
        LoadComplains(True)
    End Sub
End Class
