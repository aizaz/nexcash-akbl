﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports Telerik.Reporting
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports System.Drawing

Partial Class DesktopModules_SOADetail_SOADetail
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Dim dtDa As DataTable
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ' txtCurrentDate.Text = Date.Now.ToString("dd-MMM-yyyy")
                Dim objICUser As New ICUser
                objICUser.es.Connection.CommandTimeout = 3600
                objICUser.LoadByPrimaryKey(Me.UserId)
                raddpToDate.MaxDate = Date.Now
                raddpFromDate.MaxDate = Date.Now

                raddpToDate.SelectedDate = Date.Now
                raddpFromDate.SelectedDate = Nothing


                btnExportExcel.Visible = False
                btnExportPDF.Visible = False
                'btnExportMt940.Visible = False
                ddlCompany.Enabled = True

                If objICUser.UserType = "Client User" Then
                    Dim dtUserCompanies As New DataTable
                    dtUserCompanies = GetAllCompaniesTaggedWithUserByUserID(Me.UserId)
                    If dtUserCompanies.Rows.Count = 1 Then
                        LoadddlCompanyForClientUsers(dtUserCompanies)
                        ddlCompany.SelectedValue = dtUserCompanies.Rows(0)(0)
                        ddlCompany.Enabled = False
                    Else

                        LoadddlCompanyForClientUsers(dtUserCompanies)

                    End If
                    LoadddlAccountNumbers()
                    rvSOA.Visible = False
                    gvSOA.Visible = False
                ElseIf objICUser.UserType = "Bank User" Then
                    Dim dtUserCompanies As New DataTable
                    dtUserCompanies = GetAllCompaniesTaggedWithUserByUserID(Me.UserId)
                    If dtUserCompanies.Rows.Count = 1 Then
                        LoadddlCompanyForClientUsers(dtUserCompanies)
                        ddlCompany.SelectedValue = dtUserCompanies.Rows(0)(0)
                        ddlCompany.Enabled = False
                    Else

                        LoadddlCompanyForClientUsers(dtUserCompanies)

                    End If
                    LoadddlAccountNumbers()
                    rvSOA.Visible = False
                    gvSOA.Visible = False
                Else
                    'UIUtilities.ShowDialog(Me, "Error", "Sorry! You are not authorized", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    'Private Sub LoadddlCompanyForClientUsers(ByVal dtUserCompany As DataTable)
    '    Try
    '        Dim lit As New ListItem
    '        Dim collCompany As New ICCompanyCollection
    '        Dim objCompany As New ICCompany
    '        Dim dr As DataRow
    '        lit.Value = "0"
    '        lit.Text = "-- All --"
    '        ddlCompany.Items.Clear()
    '        ddlCompany.Items.Add(lit)
    '        collCompany = ICCompanyController.GetCompanyActiveAndApprove()


    '        For Each objCompany In collCompany
    '            For Each dr In dtUserCompany.Rows
    '                If objCompany.CompanyCode.ToString().Trim().ToLower() = dr("CompanyCode").ToString().Trim().ToLower() Then
    '                    lit = New ListItem
    '                    lit.Value = objCompany.CompanyCode.ToString()
    '                    lit.Text = objCompany.CompanyName.ToString()
    '                    ddlCompany.Items.Add(lit)
    '                End If
    '            Next
    '        Next
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    Private Sub LoadddlCompanyForClientUsers(ByVal dtUserCompany As DataTable)
        Try
            Dim lit As New ListItem


            Dim dr As DataRow
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)




            For Each dr In dtUserCompany.Rows

                lit = New ListItem
                lit.Value = dr("CompanyCode")
                lit.Text = dr("CompanyName")
                ddlCompany.Items.Add(lit)

            Next

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function GetAllCompaniesTaggedWithUserByUserID(ByVal UsersID As String) As DataTable

        Dim dt As New DataTable
        Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
        Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
        Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

        qryObjICUserRolesAPNature.Select(qryObjICCompany.CompanyCode, qryObjICCompany.CompanyName)
        qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAccounts.Currency)
        qryObjICUserRolesAPNature.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
        qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.UserID = UsersID)
        qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.IsApproved = True)
        qryObjICUserRolesAPNature.es.Distinct = True
        dt = qryObjICUserRolesAPNature.LoadDataTable
        Return dt

    End Function
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Detail Statement Of Account")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccountNumbers()
        Try
            Dim lit As New ListItem
            lit.Text = "-- Please Select --"
            lit.Value = "0"
            ddlAccountNo.Items.Clear()
            ddlAccountNo.Items.Add(lit)
            lit.Selected = True
            ddlAccountNo.AppendDataBoundItems = True
            ddlAccountNo.DataSource = ICAccountsController.GetAllAccountsTaggedWithUserByUserIDAndCompanyCode(Me.UserId.ToString, ddlCompany.SelectedValue.ToString)
            ddlAccountNo.DataTextField = "MainAccountNumber"
            ddlAccountNo.DataValueField = "AccountNumber"
            ddlAccountNo.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub ddlAccountNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAccountNo.SelectedIndexChanged
        If ddlAccountNo.SelectedValue.ToString() = "0" Then
            txtAccountCurrency.Text = ""
            txtAccountTitle.Text = ""
            rvSOA.Visible = False

        Else
            txtAccountCurrency.Text = ""
            txtAccountTitle.Text = ""
            txtAccountTitle.Text = ddlAccountNo.SelectedValue.ToString().Split("-")(3)
            txtAccountCurrency.Text = ddlAccountNo.SelectedValue.ToString().Split("-")(2)
            rvSOA.Visible = False
            'btnExportMt940.Visible = False
            btnExportExcel.Visible = False
            btnExportPDF.Visible = False
        End If
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetCompanyActiveAndApprove()
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()



        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        Try
            BindSOAGrid(True)
            Dim StrAuditTrail As String = Nothing
            Dim objICUser As New ICUser
            objICUser.LoadByPrimaryKey(Me.UserId)
            StrAuditTrail += "Detail Statement Of Account is Viewed by user [ " & objICUser.UserID & " ] [ " & objICUser.UserName & " ]."
            StrAuditTrail += "For Account No [ " & ddlAccountNo.SelectedValue.ToString.Split("-")(0) & " ] , Title [ " & txtAccountTitle.Text.ToString & " ] "
            StrAuditTrail += " date range was [ " & raddpFromDate.SelectedDate & " ] and [ " & raddpToDate.SelectedDate & " ]  at [ " & Date.Now & " ]."
            ICUtilities.AddAuditTrail(StrAuditTrail, "Detail Statement Of Account", "Detail SOA", Me.UserId.ToString, Me.UserInfo.Username.ToString, "VIEW")
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub BindSOAGrid(ByVal IsDataBind As Boolean)
        Try
            ' Generate Cover Note Report and Show Print Dialog
            If raddpFromDate.SelectedDate Is Nothing Then
                UIUtilities.ShowDialog(Me, "Error", "Please select from date", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            If raddpToDate.SelectedDate Is Nothing Then
                UIUtilities.ShowDialog(Me, "Error", "Please select to date", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            If raddpToDate.SelectedDate < raddpFromDate.SelectedDate Then
                UIUtilities.ShowDialog(Me, "Error", "To date should be greater than from date.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            Dim FromDate, ToDate As Date
            FromDate = Nothing
            ToDate = Nothing
            FromDate = raddpFromDate.SelectedDate
            ToDate = raddpToDate.SelectedDate
            Dim dt As New DataTable
            '  CBUtilities.GetAccountStatementForRadGrid(ddlAccountNo.SelectedValue.ToString.Split("-")(0), ddlAccountNo.SelectedValue.ToString.Split("-")(1), ddlAccountNo.SelectedValue.ToString.Split("-")(2), FromDate, ToDate, ddlCompany.SelectedItem.ToString, IsDataBind, gvSOA, gvSOA.CurrentPageIndex + 1)
            GetAccountStatementForRadGrid(ddlAccountNo.SelectedValue.ToString.Split("-")(0), ddlAccountNo.SelectedValue.ToString.Split("-")(1), ddlAccountNo.SelectedValue.ToString.Split("-")(2), FromDate, ToDate, ddlCompany.SelectedItem.ToString, IsDataBind, gvSOA, gvSOA.CurrentPageIndex + 1)



            If gvSOA.Items.Count > 0 Then
                gvSOA.Visible = True
                btnExportExcel.Visible = CBool(htRights("Export Excel"))
                btnExportPDF.Visible = CBool(htRights("Export PDF"))
                'btnExportMt940.Visible = CBool(htRights("Export MT940"))
                lblNRF.Visible = False
            Else
                btnExportExcel.Visible = False
                btnExportPDF.Visible = False
                'btnExportMt940.Visible = False
                gvSOA.Visible = False
                lblNRF.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadddlAccountNumbers()
            txtAccountCurrency.Text = ""
            txtAccountTitle.Text = ""
            rvSOA.Visible = False
            btnExportExcel.Visible = False
            btnExportPDF.Visible = False
            'btnExportMt940.Visible = False
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnExportPDF_Click(sender As Object, e As System.EventArgs) Handles btnExportPDF.Click
        Try
            Dim FromDate, ToDate As Date

            Dim temptextbox As New Telerik.Reporting.TextBox

            FromDate = Nothing
            ToDate = Nothing
            FromDate = raddpFromDate.SelectedDate
            ToDate = raddpToDate.SelectedDate
            Dim AppPath As String = Request.PhysicalApplicationPath.ToString() & "Reports\"
            Dim dt As New DataTable
            Dim settings As New System.Xml.XmlReaderSettings()

            settings.IgnoreWhitespace = True
            Dim xmlReader As System.Xml.XmlReader = System.Xml.XmlReader.Create(AppPath & "SOADetailReport.trdx", settings)
            Dim xmlSerializer As New Telerik.Reporting.XmlSerialization.ReportXmlSerializer()
            Dim report As Telerik.Reporting.Report = DirectCast(xmlSerializer.Deserialize(xmlReader), Telerik.Reporting.Report)


            Dim txtClientName As Telerik.Reporting.TextBox = TryCast(report.Items.Find("textBox33", True)(0), Telerik.Reporting.TextBox)
            Dim txtFromDate2 As Telerik.Reporting.TextBox = TryCast(report.Items.Find("textBox39", True)(0), Telerik.Reporting.TextBox)
            Dim txtToDate2 As Telerik.Reporting.TextBox = TryCast(report.Items.Find("textBox41", True)(0), Telerik.Reporting.TextBox)
            Dim txtAccountNo As Telerik.Reporting.TextBox = TryCast(report.Items.Find("textBox45", True)(0), Telerik.Reporting.TextBox)

            txtClientName.Value = ddlCompany.SelectedItem.ToString
            txtFromDate2.Value = CDate(raddpFromDate.SelectedDate).ToString("dd-MMM-yyyy")
            txtToDate2.Value = CDate(raddpToDate.SelectedDate).ToString("dd-MMM-yyyy")
            txtAccountNo.Value = ddlAccountNo.SelectedValue.ToString.Split("-")(0).ToString



            ' dt = CBUtilities.GetAccountStatement(ddlAccountNo.SelectedValue.ToString.Split("-")(0), ddlAccountNo.SelectedValue.ToString.Split("-")(1), ddlAccountNo.SelectedValue.ToString.Split("-")(2), FromDate, ToDate, ddlCompany.SelectedItem.ToString)
            dt = GetAccountStatement(ddlAccountNo.SelectedValue.ToString.Split("-")(0), ddlAccountNo.SelectedValue.ToString.Split("-")(1), ddlAccountNo.SelectedValue.ToString.Split("-")(2), FromDate, ToDate, ddlCompany.SelectedItem.ToString)
            If dt.Rows.Count > 0 Then
                report.DataSource = dt

                rvSOA.ShowExportGroup = False
                rvSOA.ReportSource = report
                rvSOA.RefreshReport()
                lblNRF.Visible = False
                '  rvSOA.Visible = True
                rvSOA.Visible = False
                Dim StrAuditTrail As String = Nothing
                Dim objICUser As New ICUser
                objICUser.LoadByPrimaryKey(Me.UserId)
                StrAuditTrail += "Detail Statement Of Account is Exported in PDF by user [ " & objICUser.UserID & " ] [ " & objICUser.UserName & " ]."
                StrAuditTrail += "For Account No [ " & ddlAccountNo.SelectedValue.ToString.Split("-")(0) & " ] , Title [ " & txtAccountTitle.Text.ToString & " ] "
                StrAuditTrail += " date range was [ " & raddpFromDate.SelectedDate & " ] and [ " & raddpToDate.SelectedDate & " ]  at [ " & Date.Now & " ]."
                ICUtilities.AddAuditTrail(StrAuditTrail, "Detail Statement Of Account", "Detail SOA", Me.UserId.ToString, Me.UserInfo.Username.ToString, "EXPORT PDF")
            Else
                rvSOA.Visible = False

                lblNRF.Visible = True
            End If



            Dim reportProcessor As New Telerik.Reporting.Processing.ReportProcessor()
            Dim instanceReportSource As New Telerik.Reporting.InstanceReportSource()
            instanceReportSource.ReportDocument = rvSOA.Report
            Dim result As Telerik.Reporting.Processing.RenderingResult = reportProcessor.RenderReport("PDF", instanceReportSource, Nothing)

            Dim fileName As String = result.DocumentName & "." & result.Extension
            Response.Clear()
            Response.ContentType = result.MimeType
            Response.Cache.SetCacheability(HttpCacheability.Private)
            Response.Expires = -1
            Response.Buffer = True
            Response.AddHeader("Content-Disposition", String.Format("{0};FileName=""{1}""", "attachment", fileName))
            Response.BinaryWrite(result.DocumentBytes)
            Response.End()





        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportExcel.Click
        Try

            gvSOA.ExportSettings.ExportOnlyData = True
            gvSOA.ExportSettings.FileName = "Detail SOA - " & ddlAccountNo.SelectedItem.Text.ToString()
            gvSOA.ExportSettings.IgnorePaging = True
            gvSOA.ExportSettings.OpenInNewWindow = True
            gvSOA.MasterTableView.ExportToExcel()
            Dim StrAuditTrail As String = Nothing
            Dim objICUser As New ICUser
            objICUser.LoadByPrimaryKey(Me.UserId)
            StrAuditTrail += "Detail Statement Of Account is Exported in Excel by user [ " & objICUser.UserID & " ] [ " & objICUser.UserName & " ]."
            StrAuditTrail += "For Account No [ " & ddlAccountNo.SelectedValue.ToString.Split("-")(0) & " ] , Title [ " & txtAccountTitle.Text.ToString & " ] "
            StrAuditTrail += " date range was [ " & raddpFromDate.SelectedDate & " ] and [ " & raddpToDate.SelectedDate & " ]  at [ " & Date.Now & " ]."
            ICUtilities.AddAuditTrail(StrAuditTrail, "Detail Statement Of Account", "Detail SOA", Me.UserId.ToString, Me.UserInfo.Username.ToString, "EXPORT EXCEL")

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

     Protected Sub gvSOA_ExportCellFormatting(ByVal sender As Object, ByVal e As Telerik.Web.UI.ExportCellFormattingEventArgs) Handles gvSOA.ExportCellFormatting
        If e.FormattedColumn.UniqueName.ToString() = "SourceAccount" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If
        If e.FormattedColumn.UniqueName.ToString() = "DestinationAccount" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If
    End Sub

    Protected Sub gvSOA_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvSOA.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvSOA.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub



    Protected Sub gvSOA_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvSOA.NeedDataSource
        Try
            BindSOAGrid(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvSOA_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvSOA.PageIndexChanged
        Try
            gvSOA.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    
    
#Region "BL CODE BY AIZAZ 03-Apr-2014"
    Public Shared Sub GetAccountStatementForRadGrid(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal FromDate As Date, ByVal ToDate As Date, ByVal ClientName As String, ByVal DoDataBind As Boolean, ByVal rg As RadGrid, ByVal PageNumber As Integer)
        Dim RunningBalance As Double = 0
        Dim OpeningBalance As Double = 0
        Dim AvailableBalance As Double = 0
        Dim tempOpenBalance As Double = 0
        Dim ClosingBalance As String = ""
        Dim LastRow As Integer = 0
        Dim dtAccountStatement As New DataTable
        Dim drAccountStatement As DataRow
        Dim cnt As Integer = 0
        Dim ProductCode, SchemeCode As String
        Dim dtSoneriTrnas As New DataTable
        Dim MBMNo As String = Nothing
        Dim drTemp As DataRow
        Dim dtFinal As New DataTable
        Dim drFinal As DataRow
        Dim InstrumentNo As String = ""
        Dim FromFTAccount As String = ""
        Dim ToFTAccount As String = ""
        Dim ReferenceField1 As String = Nothing
        Dim ReferenceField2 As String = Nothing
        Dim ReferenceField3 As String = Nothing
        BranchCode = Nothing
        ProductCode = Nothing
        SchemeCode = Nothing
        Try

            dtAccountStatement.Columns.Add(New DataColumn("SerialNo", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Date", GetType(System.DateTime)))
            dtAccountStatement.Columns.Add(New DataColumn("ValueDate", GetType(System.DateTime)))
            dtAccountStatement.Columns.Add(New DataColumn("TransactionType", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("ChequeNo", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("BranchName", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Narrations", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Debit", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Credit", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Amount", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("RemitterBank", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("SourceAccount", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("DestinationAccount", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Ref1", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Ref2", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Ref3", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("ClientName", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("AccountNo", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("OpeningBalance", GetType(System.Double)))
            dtAccountStatement.Columns.Add(New DataColumn("AvailableBalance", GetType(System.Double)))
            dtAccountStatement.Columns.Add(New DataColumn("StatementDate", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("FromDate", GetType(System.DateTime)))
            dtAccountStatement.Columns.Add(New DataColumn("ToDate", GetType(System.DateTime)))
            dtAccountStatement.Columns.Add(New DataColumn("ClosingBalance", GetType(System.String)))



            'AvailableBalance = CBUtilities.BalanceInquiry(AccountNumber, CBUtilities.AccountType.RB, "Client Account", AccountNumber)



            'AvailableBalance = OpeningBalance
            ' Get Al Baraka Transactions from SP
            MBMNo = AccountNumber.Substring(AccountNumber.Length - 7, 7)
            BranchCode = AccountNumber.Substring(0, 4)
            ProductCode = AccountNumber.Substring(AccountNumber.Length - 11, 2)
            SchemeCode = AccountNumber.Substring(AccountNumber.Length - 9, 2)

            dtSoneriTrnas = New DataTable()
           
            dtSoneriTrnas = GetFunctionOFIRISForDetailSOA(BranchCode, ProductCode, SchemeCode, MBMNo, FromDate.Date, ToDate.Date, "1", 0, 0, "")





            If Not dtSoneriTrnas Is Nothing Then
                If dtSoneriTrnas.Rows.Count > 0 Then

                    OpeningBalance = dtSoneriTrnas.Rows(0)("BALANCE")
                    RunningBalance = OpeningBalance
                    For Each drSoneriTran In dtSoneriTrnas.Rows
                        drAccountStatement = dtAccountStatement.NewRow()
                        cnt = cnt + 1
                        If cnt = 1 Then
                            drAccountStatement("Amount") = RunningBalance.ToString("N2")
                            If drAccountStatement("Amount").ToString.Contains("-") Then
                                drAccountStatement("Amount") = drAccountStatement("Amount").ToString.Replace("-", "") & " " & "DR"
                            Else
                                drAccountStatement("Amount") = drAccountStatement("Amount").ToString & " " & "CR"
                            End If
                            drAccountStatement("Narrations") = drSoneriTran("NARRATIONS").ToString()
                            drAccountStatement("Date") = CDate(drSoneriTran("TRANSDATE").ToString()).ToString("MM/dd/yyyy")
                        Else
                            drAccountStatement("SerialNo") = cnt.ToString()
                            drAccountStatement("Date") = CDate(drSoneriTran("TRANSDATE").ToString()).ToString("MM/dd/yyyy")
                            InstrumentNo = Nothing
                            FromFTAccount = Nothing
                            ToFTAccount = Nothing
                            ReferenceField1 = Nothing
                            ReferenceField2 = Nothing
                            ReferenceField3 = Nothing
                            If drSoneriTran("SRC_MBM_NUMBER").ToString <> "" Then
                                FromFTAccount = drSoneriTran("SRC_MBM_NUMBER")
                            End If
                            If drSoneriTran("DEST_MBM_NUMBER").ToString <> "" Then
                                ToFTAccount = drSoneriTran("DEST_MBM_NUMBER")
                            End If

                            InstrumentNo = drSoneriTran("PIT_INSTDESC") & " " & drSoneriTran("MGT_INSTRUMENTNO")

                            drAccountStatement("Narrations") = GetCustomizedSOADetailByNarration(drSoneriTran("NARRATIONS").ToString(), drSoneriTran("PSY_SYSTCODE").ToString(), InstrumentNo, drSoneriTran("DRCR").ToString(), FromFTAccount, ToFTAccount, ReferenceField1, ReferenceField2, ReferenceField3)
                            drAccountStatement("ChequeNo") = InstrumentNo
                            drAccountStatement("ValueDate") = CDate(drSoneriTran("VALUEDATE").ToString()).ToString("MM/dd/yyyy")
                            drAccountStatement("TransactionType") = drSoneriTran("TRANDESC").ToString()
                            drAccountStatement("Ref1") = ReferenceField1
                            drAccountStatement("Ref2") = ReferenceField2
                            drAccountStatement("Ref3") = ReferenceField3
                            drAccountStatement("SourceAccount") = FromFTAccount
                            drAccountStatement("DestinationAccount") = ToFTAccount
                            drAccountStatement("BranchName") = drSoneriTran("LOCA_REQ_DESC").ToString()
                            'drAccountStatement("Narrations") = drSoneriTran("NARRATIONS").ToString()
                            drAccountStatement("RemitterBank") = drSoneriTran("PIM_DESCRIPTION_SRC") & "-" & drSoneriTran("PIM_DESCRIPTION_DES")
                            
                            If drSoneriTran("DRCR").ToString() = "DR" Then
                                RunningBalance = RunningBalance - CDbl(drSoneriTran("DEBITF").ToString())

                                drAccountStatement("Credit") = CDbl(drSoneriTran("CREDITF")).ToString("N2")
                                drAccountStatement("Debit") = CDbl(drSoneriTran("DEBITF")).ToString("N2")
                                drAccountStatement("Amount") = RunningBalance.ToString("N2")
                                If drAccountStatement("Amount").ToString.Contains("-") Then
                                    drAccountStatement("Amount") = drAccountStatement("Amount").ToString.Replace("-", "") & " " & "DR"
                                Else
                                    drAccountStatement("Amount") = drAccountStatement("Amount").ToString & " " & "CR"
                                End If

                            ElseIf drSoneriTran("DRCR").ToString() = "CR" Then
                                RunningBalance = RunningBalance + CDbl(drSoneriTran("CREDITF").ToString())

                                drAccountStatement("Debit") = CDbl(drSoneriTran("DEBITF")).ToString("N2")
                                drAccountStatement("Credit") = CDbl(drSoneriTran("CREDITF")).ToString("N2")
                                drAccountStatement("Amount") = RunningBalance.ToString("N2")
                                If drAccountStatement("Amount").ToString.Contains("-") Then
                                    drAccountStatement("Amount") = drAccountStatement("Amount").ToString.Replace("-", "") & " " & "DR"
                                Else
                                    drAccountStatement("Amount") = drAccountStatement("Amount").ToString & " " & "CR"
                                End If
                            End If
                            drAccountStatement("ClientName") = ClientName.ToString()
                            drAccountStatement("OpeningBalance") = OpeningBalance
                            drAccountStatement("AvailableBalance") = AvailableBalance
                            drAccountStatement("StatementDate") = Date.Now.ToString("dd-MMM-yyyy")
                            drAccountStatement("FromDate") = FromDate
                            drAccountStatement("ToDate") = ToDate
                            drAccountStatement("ClosingBalance") = 0
                            If cnt = dtSoneriTrnas.Rows.Count Then
                                ClosingBalance = drAccountStatement("Amount")
                            End If
                        End If
                        dtAccountStatement.Rows.Add(drAccountStatement)
                    Next


                    For Each drFinal In dtAccountStatement.Rows
                        drFinal("ClosingBalance") = ClosingBalance
                    Next


                End If
            End If



            If Not PageNumber = 0 Then

                rg.DataSource = dtAccountStatement

                If DoDataBind Then
                    rg.DataBind()
                End If

            End If
        Catch ex As Exception
            Throw New Exception("CNT : " & cnt & "" & ex.Message)
        End Try
    End Sub
    Public Shared Function GetCustomizedSOADetailByNarration(ByVal Narration As String, ByVal VoucherType As String, ByRef InstrumentNo As String, ByVal DRCR As String, ByRef FromFTAccountNo As String, ByRef ToFTAccountNo As String, ByRef Ref1 As String, ByRef Ref2 As String, ByRef Ref3 As String) As String
        Try
            Dim CustMessage As String = Nothing
            Dim ArraySplitedNarration As String()
            Dim InstructionID As String = Nothing
            Dim objICInstruction As ICInstruction
            Dim objICInstructionColl As ICInstructionCollection
            Dim objICSweepActionInstruction As ICSweepActionInstructions
            Dim objICOffice As New ICOffice
            Dim objICBank As New ICBank
            If VoucherType = ICUtilities.GetSettingValue("SystemCode").ToString Then

                If Narration.Contains(":") = True Then
                    ArraySplitedNarration = Narration.Split(":")
                    If ArraySplitedNarration(0).Split(" ")(0).ToString = "Instruction" Then
                        objICInstruction = New ICInstruction
                        If ArraySplitedNarration(0).Split(" ")(1).ToString <> "Reversed" Then
                            If objICInstruction.LoadByPrimaryKey(ArraySplitedNarration(0).Split(" ")(1).ToString) Then
                                CustMessage += "ST-"
                                CustMessage += objICInstruction.ProductTypeCode & "-"
                                CustMessage += "TRAN REF NO:" & objICInstruction.InstructionID & "-"

                                If DRCR = "CR" Then
                                    If objICInstruction.ClubID IsNot Nothing Then
                                        CustMessage += "ClubbedInstruction No " & objICInstruction.ClubID & " -"
                                    End If
                                End If
                                CustMessage += objICInstruction.BeneficiaryName & "-"
                                If objICInstruction.PaymentMode = "DD" Then
                                    If objICOffice.LoadByPrimaryKey(objICInstruction.DrawnOnBranchCode) Then
                                        If objICBank.LoadByPrimaryKey(objICOffice.BankCode) Then
                                            CustMessage += objICBank.BankName & "-"
                                        End If
                                    End If
                                End If
                                If objICInstruction.FileBatchNo IsNot Nothing And objICInstruction.FileBatchNo <> "" And objICInstruction.FileBatchNo <> "SI" Then
                                    CustMessage += "File Batch No - " & objICInstruction.FileBatchNo & "-"
                                End If
                                Ref1 = objICInstruction.ReferenceField1
                                Ref2 = objICInstruction.ReferenceField2
                                Ref3 = objICInstruction.ReferenceField3
                                If objICInstruction.PaymentMode = "Direct Credit" Or objICInstruction.PaymentMode = "Other Credit" Or objICInstruction.PaymentMode = "COTC" Then
                                    InstrumentNo = Nothing
                                    InstrumentNo = objICInstruction.ProductTypeCode
                                ElseIf objICInstruction.PaymentMode = "PO" Or objICInstruction.PaymentMode = "DD" Then
                                    InstrumentNo = Nothing
                                    InstrumentNo += objICInstruction.ProductTypeCode & " " & objICInstruction.InstrumentNo
                                End If
                                If objICInstruction.PaymentMode = "Direct Credit" Then
                                    FromFTAccountNo = Nothing
                                    ToFTAccountNo = Nothing
                                    FromFTAccountNo = objICInstruction.ClientAccountNo
                                    ToFTAccountNo = objICInstruction.BeneficiaryAccountNo
                                End If
                                CustMessage = CustMessage.Remove(CustMessage.Length - 1, 1)
                            Else
                                CustMessage = Narration
                            End If
                        ElseIf ArraySplitedNarration(0).Split(" ")(1).ToString = "Reversed" Then
                            If objICInstruction.LoadByPrimaryKey(ArraySplitedNarration(0).Split(" ")(2).ToString) Then
                                CustMessage += "ST-"
                                CustMessage += objICInstruction.ProductTypeCode & "-"
                                CustMessage += "TRAN REF NO:" & objICInstruction.InstructionID & "-"

                                If DRCR = "CR" Then
                                    If objICInstruction.ClubID IsNot Nothing Then
                                        CustMessage += "ClubbedInstruction No " & objICInstruction.ClubID & " -" & ArraySplitedNarration(0).Split(" ")(1).ToString & "-"
                                    End If
                                End If
                                CustMessage += objICInstruction.BeneficiaryName & "-"
                                If objICInstruction.PaymentMode = "DD" Then
                                    If objICOffice.LoadByPrimaryKey(objICInstruction.DrawnOnBranchCode) Then
                                        If objICBank.LoadByPrimaryKey(objICOffice.BankCode) Then
                                            CustMessage += objICBank.BankName & "-"
                                        End If
                                    End If
                                End If
                                If objICInstruction.FileBatchNo IsNot Nothing And objICInstruction.FileBatchNo <> "" And objICInstruction.FileBatchNo <> "SI" Then
                                    CustMessage += "File Batch No - " & objICInstruction.FileBatchNo & "-"
                                End If
                                Ref1 = objICInstruction.ReferenceField1
                                Ref2 = objICInstruction.ReferenceField2
                                Ref3 = objICInstruction.ReferenceField3
                                If objICInstruction.PaymentMode = "Direct Credit" Or objICInstruction.PaymentMode = "Other Credit" Or objICInstruction.PaymentMode = "COTC" Then
                                    InstrumentNo = Nothing
                                    InstrumentNo = objICInstruction.ProductTypeCode
                                ElseIf objICInstruction.PaymentMode = "PO" Or objICInstruction.PaymentMode = "DD" Then
                                    InstrumentNo = Nothing
                                    InstrumentNo += objICInstruction.ProductTypeCode & " " & objICInstruction.InstrumentNo
                                End If
                                'If objICInstruction.PaymentMode = "Direct Credit" Then
                                '    FromFTAccountNo = Nothing
                                '    ToFTAccountNo = Nothing
                                '    FromFTAccountNo = objICInstruction.ClientAccountNo
                                '    ToFTAccountNo = objICInstruction.BeneficiaryAccountNo
                                'End If
                                CustMessage = CustMessage.Remove(CustMessage.Length - 1, 1)
                            Else
                                CustMessage = Narration
                            End If
                        End If
                       

                    ElseIf ArraySplitedNarration(0).Split(" ")(0).ToString = "ClubedInstruction" Then
                        objICInstructionColl = New ICInstructionCollection

                        If ArraySplitedNarration(0).Split(" ")(1).ToString <> "Reversed" Then
                            objICInstructionColl.Query.Where(objICInstructionColl.Query.ClubID = ArraySplitedNarration(0).Split(" ")(1).ToString)
                            If objICInstructionColl.Query.Load Then
                                CustMessage += "ST-"
                                objICInstruction = New ICInstruction
                                objICInstruction = objICInstructionColl(0)
                                CustMessage += ArraySplitedNarration(0).Split(" ")(0) & " " & ArraySplitedNarration(0).Split(" ")(1).ToString & "-"
                                CustMessage += objICInstruction.ProductTypeCode & "-"
                                Ref3 = objICInstruction.ReferenceField3
                                'If objICInstruction.PaymentMode = "Direct Credit" Or objICInstruction.PaymentMode = "Other Credit" Or objICInstruction.PaymentMode = "COTC" Then
                                InstrumentNo = Nothing
                                InstrumentNo = objICInstruction.ProductTypeCode
                                'End If
                                CustMessage = CustMessage.Remove(CustMessage.Length - 1, 1)
                            Else
                                CustMessage = Narration
                            End If
                        ElseIf ArraySplitedNarration(0).Split(" ")(1).ToString = "Reversed" Then
                            objICInstructionColl.Query.Where(objICInstructionColl.Query.ClubID = ArraySplitedNarration(0).Split(" ")(2).ToString)
                            If objICInstructionColl.Query.Load Then
                                CustMessage += "ST-"
                                objICInstruction = New ICInstruction
                                objICInstruction = objICInstructionColl(0)
                                CustMessage += ArraySplitedNarration(0).Split(" ")(0) & " " & ArraySplitedNarration(0).Split(" ")(1).ToString & "-"
                                CustMessage += objICInstruction.ProductTypeCode & "-"
                                Ref3 = objICInstruction.ReferenceField3
                                'If objICInstruction.PaymentMode = "Direct Credit" Or objICInstruction.PaymentMode = "Other Credit" Or objICInstruction.PaymentMode = "COTC" Then
                                InstrumentNo = Nothing
                                InstrumentNo = objICInstruction.ProductTypeCode
                                'End If
                                CustMessage = CustMessage.Remove(CustMessage.Length - 1, 1)
                            Else
                                CustMessage = Narration
                            End If
                        End If
                       
                    ElseIf ArraySplitedNarration(0).Split(" ")(0).ToString = "Sweep" Then
                        objICSweepActionInstruction = New ICSweepActionInstructions
                        objICSweepActionInstruction.LoadByPrimaryKey(ArraySplitedNarration(0).Split(" ")(3).ToString)
                        CustMessage += "ST-"
                        InstrumentNo = Nothing
                        InstrumentNo = ArraySplitedNarration(0).Split(" ")(0).ToString
                        CustMessage += Narration & "-" & " Action ID " & objICSweepActionInstruction.SweepActionID & "-"
                        CustMessage = CustMessage.Remove(CustMessage.Length - 1, 1)
                        FromFTAccountNo = Nothing
                        ToFTAccountNo = Nothing
                        FromFTAccountNo = objICSweepActionInstruction.FromAccountNo
                        ToFTAccountNo = objICSweepActionInstruction.ToAccountNo
                    Else
                        CustMessage = Narration
                    End If
                Else
                    CustMessage = Narration

                End If
            Else
                CustMessage = Narration
            End If
            Return CustMessage
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    'Public Shared Function GetFunctionOFIRISForDetailSOA(ByVal BranchCode As String, ByVal p As String, ByVal s As String, ByVal m As String, ByVal dateStart As String, ByVal dateEnd As String, ByVal opt As Char, ByVal amt_range_from As Decimal, ByVal amt_range_to As Decimal, ByVal Trans_Desc As String) As DataTable
    '    Try
    '        'Dim FilePath As String = ICUtilities.GetSettingValue("PhysicalApplicationPath") & "UploadedFiles/SOA DETAIL 2.xls"

    '        Dim FilePath As String = "F:\Collection Changes\Aamir\SOA SP DATA.xls"

    '        Dim connectionString As String = ""

    '        connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FilePath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""

    '        Dim con As New OleDbConnection(connectionString)
    '        Dim cmd As New OleDbCommand()
    '        cmd.CommandType = System.Data.CommandType.Text
    '        cmd.Connection = con
    '        Dim dAdapter As New OleDbDataAdapter(cmd)
    '        Dim dtExcelRecords As New DataTable()
    '        con.Open()
    '        Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
    '        Dim getExcelSheetName As String = "Sheet1$"
    '        cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
    '        dAdapter.SelectCommand = cmd
    '        dAdapter.Fill(dtExcelRecords)
    '        con.Close()



    '        Return dtExcelRecords

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    Public Shared Function GetFunctionOFIRISForDetailSOA(ByVal BranchCode As String, ByVal p As String, ByVal s As String, ByVal m As String, ByVal dateStart As String, ByVal dateEnd As String, ByVal opt As Char, ByVal amt_range_from As Decimal, ByVal amt_range_to As Decimal, ByVal Trans_Desc As String) As DataTable
        Try
            Dim dt As New DataTable
            Dim constring As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString.ToString()
            Dim Consql As New SqlConnection(constring)
            Dim da As New SqlDataAdapter
            If Consql.State = ConnectionState.Closed Then
                Consql.Open()
            Else
                Consql.Close()
            End If

            Dim cmd As New SqlCommand("dbo.SP_CMS_CASH_MANAGEMENT_SOA_DETAIL", Consql)
            'cmd.CommandText = SqlDataSourceCommandType.StoredProcedure
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@BRANCHCODE", BranchCode)
            cmd.Parameters.AddWithValue("@PRODCODE", p)
            cmd.Parameters.AddWithValue("@SCHEMCODE", s)
            cmd.Parameters.AddWithValue("@ACCOUNT", m)
            cmd.Parameters.AddWithValue("@DATEFROM", dateStart)
            cmd.Parameters.AddWithValue("@DATETO", dateEnd)
            'cmd.Parameters.AddWithValue("@opt", opt)
            'cmd.Parameters.AddWithValue("@Amt_Range_From", amt_range_from)
            'cmd.Parameters.AddWithValue("@Amt_Range_to", amt_range_to)
            'cmd.Parameters.AddWithValue("@Trans_Desc", Trans_Desc.ToString())

            da.SelectCommand = cmd
            da.Fill(dt)
            Dim st As String = ""

            'While (sqldr.Read)
            '    st = sqldr(0)
            'End While
            cmd.ExecuteReader()

            If Consql.State = ConnectionState.Open Then
                Consql.Close()
            End If
            Return dt

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function GetAccountStatement(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal FromDate As Date, ByVal ToDate As Date, ByVal ClientName As String) As DataTable
        Dim RunningBalance As Double = 0
        Dim OpeningBalance As Double = 0
        Dim AvailableBalance As Double = 0
        Dim tempOpenBalance As Double = 0
        Dim DebitCount As Integer = 0
        Dim CreditCount As Integer
        Dim TotalDebitAmount As Double = 0
        Dim TotalCreditAmount As Double = 0
        Dim ClosingBalance As String = ""
        Dim LastRow As Integer = 0
        Dim dtAccountStatement As New DataTable
        Dim drAccountStatement As DataRow
        Dim cnt As Integer = 0
        Dim ProductCode, SchemeCode As String
        Dim dtSoneriTrnas As New DataTable
        Dim MBMNo As String = Nothing
        Dim dtFinal As New DataTable
        Dim drFinal As DataRow
        Dim drTemp As DataRow
        Dim RowCount As Integer = 0
        Dim InstrumentNo As String = ""
        Dim FromFTAccount As String = ""
        Dim ToFTAccount As String = ""
        Dim ReferenceField1 As String = Nothing
        Dim ReferenceField2 As String = Nothing
        Dim ReferenceField3 As String = Nothing
        BranchCode = Nothing
        ProductCode = Nothing
        SchemeCode = Nothing
        Try
           dtAccountStatement.Columns.Add(New DataColumn("SerialNo", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Date", GetType(System.DateTime)))
            dtAccountStatement.Columns.Add(New DataColumn("ValueDate", GetType(System.DateTime)))
            dtAccountStatement.Columns.Add(New DataColumn("TransactionType", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("ChequeNo", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("BranchName", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Narrations", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Debit", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Credit", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Amount", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("RemitterBank", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("SourceAccount", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("DestinationAccount", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Ref1", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Ref2", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Ref3", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("ClientName", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("AccountNo", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("OpeningBalance", GetType(System.Double)))
            dtAccountStatement.Columns.Add(New DataColumn("AvailableBalance", GetType(System.Double)))
            dtAccountStatement.Columns.Add(New DataColumn("StatementDate", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("FromDate", GetType(System.DateTime)))
            dtAccountStatement.Columns.Add(New DataColumn("ToDate", GetType(System.DateTime)))
            dtAccountStatement.Columns.Add(New DataColumn("ClosingBalance", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("TotalDebitAmount", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("TotalCreditAmount", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("DebitCount", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("CreditCount", GetType(System.String)))

            'AvailableBalance = OpeningBalance
            ' Get Al Baraka Transactions from SP
            MBMNo = AccountNumber.Substring(AccountNumber.Length - 7, 7)
            BranchCode = AccountNumber.Substring(0, 4)
            ProductCode = AccountNumber.Substring(AccountNumber.Length - 11, 2)
            SchemeCode = AccountNumber.Substring(AccountNumber.Length - 9, 2)

            dtSoneriTrnas = New DataTable()
            

            dtSoneriTrnas = GetFunctionOFIRISForDetailSOA(BranchCode, ProductCode, SchemeCode, MBMNo, FromDate.Date, ToDate.Date, "1", 0, 0, "")





            If Not dtSoneriTrnas Is Nothing Then
                If dtSoneriTrnas.Rows.Count > 0 Then

                    OpeningBalance = dtSoneriTrnas.Rows(0)("BALANCE")
                    RunningBalance = OpeningBalance
                    For Each drSoneriTran In dtSoneriTrnas.Rows
                        drAccountStatement = dtAccountStatement.NewRow()
                        cnt = cnt + 1
                        If cnt = 1 Then
                            drAccountStatement("Amount") = RunningBalance.ToString("N2")
                            If drAccountStatement("Amount").ToString.Contains("-") Then
                                drAccountStatement("Amount") = drAccountStatement("Amount").ToString.Replace("-", "") & " " & "DR"
                            Else
                                drAccountStatement("Amount") = drAccountStatement("Amount").ToString & " " & "CR"
                            End If
                            drAccountStatement("Narrations") = drSoneriTran("NARRATIONS").ToString()
                            drAccountStatement("Date") = CDate(drSoneriTran("TRANSDATE").ToString()).ToString("MM/dd/yyyy")
                            'drAccountStatement("ChequeNo") = ClientName.ToString()
                        Else
                            drAccountStatement("SerialNo") = cnt.ToString()
                            drAccountStatement("Date") = CDate(drSoneriTran("TRANSDATE").ToString()).ToString("MM/dd/yyyy")
                            drAccountStatement("ValueDate") = CDate(drSoneriTran("VALUEDATE").ToString()).ToString("MM/dd/yyyy")
                            drAccountStatement("TransactionType") = drSoneriTran("TRANDESC").ToString()
                            InstrumentNo = Nothing
                            FromFTAccount = Nothing
                            ToFTAccount = Nothing
                            ReferenceField1 = Nothing
                            ReferenceField2 = Nothing
                            ReferenceField3 = Nothing
                            If drSoneriTran("SRC_MBM_NUMBER").ToString <> "" Then
                                FromFTAccount = drSoneriTran("SRC_MBM_NUMBER")
                            End If
                            If drSoneriTran("DEST_MBM_NUMBER").ToString <> "" Then
                                ToFTAccount = drSoneriTran("DEST_MBM_NUMBER")
                            End If
                            InstrumentNo = drSoneriTran("PIT_INSTDESC") & " " & drSoneriTran("MGT_INSTRUMENTNO")
                            drAccountStatement("Narrations") = GetCustomizedSOADetailByNarration(drSoneriTran("NARRATIONS").ToString(), drSoneriTran("PSY_SYSTCODE").ToString(), InstrumentNo, drSoneriTran("DRCR").ToString(), FromFTAccount, ToFTAccount, ReferenceField1, ReferenceField2, ReferenceField3)
                            drAccountStatement("ChequeNo") = InstrumentNo
                            drAccountStatement("BranchName") = drSoneriTran("BRANCH_ORGINATING").ToString()
                            drAccountStatement("RemitterBank") = drSoneriTran("PIM_DESCRIPTION_SRC") & "-" & drSoneriTran("PIM_DESCRIPTION_DES")
                            drAccountStatement("Ref1") = ReferenceField1
                            drAccountStatement("Ref2") = ReferenceField2
                            drAccountStatement("Ref3") = ReferenceField3
                            drAccountStatement("SourceAccount") = FromFTAccount
                            drAccountStatement("DestinationAccount") = ToFTAccount
                            If drSoneriTran("DRCR").ToString() = "DR" Then
                                RunningBalance = RunningBalance - CDbl(drSoneriTran("DEBITF").ToString())
                                TotalDebitAmount = TotalDebitAmount + CDbl(drSoneriTran("DEBITF").ToString())
                                DebitCount = DebitCount + 1
                                drAccountStatement("Credit") = CDbl(drSoneriTran("CREDITF")).ToString("N2")
                                drAccountStatement("Debit") = CDbl(drSoneriTran("DEBITF")).ToString("N2")
                                drAccountStatement("Amount") = RunningBalance.ToString("N2")
                                If drAccountStatement("Amount").ToString.Contains("-") Then
                                    drAccountStatement("Amount") = drAccountStatement("Amount").ToString.Replace("-", "") & " " & "DR"
                                Else
                                    drAccountStatement("Amount") = drAccountStatement("Amount").ToString & " " & "CR"
                                End If

                            ElseIf drSoneriTran("DRCR").ToString() = "CR" Then
                                RunningBalance = RunningBalance + CDbl(drSoneriTran("CREDITF").ToString())
                                TotalCreditAmount = TotalCreditAmount + CDbl(drSoneriTran("CREDITF").ToString())
                                CreditCount = CreditCount + 1
                                drAccountStatement("Debit") = CDbl(drSoneriTran("DEBITF")).ToString("N2")
                                drAccountStatement("Credit") = CDbl(drSoneriTran("CREDITF")).ToString("N2")
                                drAccountStatement("Amount") = RunningBalance.ToString("N2")
                                If drAccountStatement("Amount").ToString.Contains("-") Then
                                    drAccountStatement("Amount") = drAccountStatement("Amount").ToString.Replace("-", "") & " " & "DR"
                                Else
                                    drAccountStatement("Amount") = drAccountStatement("Amount").ToString & " " & "CR"
                                End If
                            End If
                            drAccountStatement("ClientName") = ClientName.ToString()
                            drAccountStatement("OpeningBalance") = OpeningBalance
                            drAccountStatement("AvailableBalance") = AvailableBalance
                            drAccountStatement("StatementDate") = Date.Now.ToString("dd-MMM-yyyy")
                            drAccountStatement("FromDate") = FromDate.ToString("dd-MMM-yyyy")
                            drAccountStatement("ToDate") = ToDate.ToString("dd-MMM-yyyy")
                            drAccountStatement("ClosingBalance") = 0
                            drAccountStatement("AccountNumber") = AccountNumber
                            If cnt = dtSoneriTrnas.Rows.Count Then
                                ClosingBalance = drAccountStatement("Amount")
                            End If
                        End If
                        dtAccountStatement.Rows.Add(drAccountStatement)
                    Next

                    For Each drFinal In dtAccountStatement.Rows
                        drFinal("ClosingBalance") = ClosingBalance
                        drFinal("DebitCount") = DebitCount.ToString
                        drFinal("CreditCount") = CreditCount.ToString
                        drFinal("TotalDebitAmount") = TotalDebitAmount.ToString("N2")
                        drFinal("TotalCreditAmount") = TotalCreditAmount.ToString("N2")
                    Next


                End If
            End If

            Return dtAccountStatement
        Catch ex As Exception
            Throw New Exception("CNT : " & cnt & "" & ex.Message)
        End Try
    End Function

#End Region




End Class

