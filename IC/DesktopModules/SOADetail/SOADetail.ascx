﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SOADetail.ascx.vb" Inherits="DesktopModules_SOADetail_SOADetail" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="Telerik.ReportViewer.WebForms, Version=7.0.13.220, Culture=neutral, PublicKeyToken=a9d7983dfcc261be"
    Namespace="Telerik.ReportViewer.WebForms" TagPrefix="telerik" %>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:TextBoxSetting BehaviorID="reCurrentDate" Validation-IsRequired="false"
        EmptyMessage="Current Date" ErrorMessage="Current Date">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCurrentDate" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reAccountTitle" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Account Title">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reAccountCurrency" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Account Currency">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountCurrency" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Detail Statement Of Account" CssClass="headingblue"></asp:Label>
                        <br />
                        Note:&nbsp; Fields marked as
                        <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        &nbsp;are required.
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <table width="100%">
                            <%--<tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblCurrentDate" runat="server" Text="Current Date" 
                                        CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:TextBox ID="txtCurrentDate" runat="server" CssClass="txtbox" 
                                        ReadOnly="True"></asp:TextBox>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                            </tr>--%>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblFromDate" runat="server" Text="Date From" CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblToDate" runat="server" Text="To Date" CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <telerik:RadDatePicker ID="raddpFromDate" runat="server" CssClass="txtbox">
                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                        </Calendar>
                                        <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                            LabelWidth="40%" type="text" value="">
                                        </DateInput>
                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                    </telerik:RadDatePicker>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <telerik:RadDatePicker ID="raddpToDate" runat="server" CssClass="txtbox">
                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                        </Calendar>
                                        <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                            LabelWidth="40%" type="text" value="">
                                        </DateInput>
                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                    </telerik:RadDatePicker>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblCompany" runat="server" Text="Company" CssClass="lbl"></asp:Label>
                                    <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblAccountNo" runat="server" Text="Account" CssClass="lbl"></asp:Label>
                                    <asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:DropDownList ID="ddlCompany" runat="server" Width="100%" CssClass="dropdown"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:DropDownList ID="ddlAccountNo" runat="server" Width="100%" CssClass="dropdown"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" ControlToValidate="ddlCompany"
                                        CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Company" SetFocusOnError="True"
                                        ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:RequiredFieldValidator ID="rfvddlAccount" runat="server" ControlToValidate="ddlAccountNo"
                                        CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Account" SetFocusOnError="True"
                                        ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblAccountTitle" runat="server" Text="Account Title" CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblAccountCurrency" runat="server" Text="Currency" CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:TextBox ID="txtAccountTitle" runat="server" CssClass="txtbox" MaxLength="150"
                                        ReadOnly="True"></asp:TextBox>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:TextBox ID="txtAccountCurrency" runat="server" CssClass="txtbox" ReadOnly="True"></asp:TextBox>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="center" valign="middle" style="width: 100%" colspan="4">
                                    <asp:Button ID="btnSearch" runat="server" Text="View" CssClass="btn" />
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%">
                        <asp:Button ID="btnExportExcel" runat="server" Text="Export In Excel" CssClass="btn" />
                        &nbsp;
                        <asp:Button ID="btnExportPDF" runat="server" Text="Export In PDF" CssClass="btn" />
                    &nbsp;
                        </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Label ID="lblNRF" runat="server" Text="No Record Found." CssClass="headingblue"
                            Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%;display: none">
                    <td align="center" valign="top" style="width: 100%; display: none">
                        <telerik:ReportViewer ID="rvSOA" runat="server" Width="880px">
              
                        
                        
                        </telerik:ReportViewer>
                    </td>
                     <td align="center" valign="top" style="width: 100%;">
                        <%--<telerik:ReportViewer ID="rvSOA" runat="server" Width="880px">
              
                        
                        
                        </telerik:ReportViewer>--%>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        &nbsp;
                        <telerik:RadGrid ID="gvSOA" runat="server" AllowPaging="True" Width="100%" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" CssClass="RadGrid" PageSize="100">
                            <ClientSettings>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView TableLayout="Fixed"  Width="100%">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <%--<telerik:GridBoundColumn DataField="SerialNo" HeaderText="S No." SortExpression="SerialNo">
                                    <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="3%" />
                                    <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" Width="3%" />
                                    </telerik:GridBoundColumn>--%>
                                     <%--<telerik:GridBoundColumn DataField="BranchCode" HeaderText="Br. Code" SortExpression="BranchCode">
                                      <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="4%" />
                                    <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" Width="4%" />
                                    </telerik:GridBoundColumn>--%>
                                     <telerik:GridBoundColumn DataField="Date" HeaderText="Txn. Date" SortExpression="Date"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                       <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="7%" />
                                    <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" Width="7%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ValueDate" HeaderText="Value Date" SortExpression="ValueDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                       <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="7%" />
                                    <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" Width="7%" />
                                    </telerik:GridBoundColumn>
                                   <telerik:GridBoundColumn DataField="TransactionType" HeaderText="Transaction Type" SortExpression="TransactionType">
                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="6%" />
                                    <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" Width="6%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ChequeNo" HeaderText="Txn. Ref No. / Inst. / Voucher No." SortExpression="ChequeNo">
                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="8%" />
                                    <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" Width="8%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BranchName" HeaderText="Br. Name" SortExpression="BranchName">
                                      <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="8%" />
                                    <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" Width="8%" />
                                    </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="Narrations" HeaderText="Narration / Transaction Detail" SortExpression="Narrations">
                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="6%" />
                                    <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" Width="6%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Debit" HeaderText="Withdrawal"
                                        SortExpression="Debit">
                                         <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="6%" />
                                    <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" Width="6%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Credit" HeaderText="Deposit"
                                        SortExpression="Credit">
                                         <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="6%" />
                                    <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" Width="6%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Amount" HeaderText="Balance" SortExpression="Amount"
                                        HtmlEncode="false" DataFormatString="{0:N2}">
                                          <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="10%" />
                                    <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" Width="10%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="RemitterBank" HeaderText="Remitter Bank" SortExpression="RemitterBank">
                                        <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="6%" />
                                    <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" Width="6%" />
                                    </telerik:GridBoundColumn>
                                    
                                    <telerik:GridBoundColumn DataField="SourceAccount" HeaderText="Source Account"
                                        SortExpression="SourceAccount">
                                         <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="7%" />
                                    <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" Width="7%" />
                                    </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="DestinationAccount" HeaderText="Destination Account"
                                        SortExpression="DestinationAccount">
                                         <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="7%" />
                                    <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" Width="7%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Ref1" HeaderText="Ref 1" SortExpression="Ref1">
                                    <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="5%" />
                                    <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" Width="5%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Ref2" HeaderText="Ref 2" SortExpression="Ref2">
                                    <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="5%" />
                                    <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" Width="5%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Ref3" HeaderText="Ref 3" SortExpression="Ref3">
                                    <ItemStyle VerticalAlign="Top" HorizontalAlign="Left" Width="5%" />
                                    <HeaderStyle VerticalAlign="Top" HorizontalAlign="Left" Width="5%" />
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                            <PagerStyle AlwaysVisible="True" />
                            <HeaderStyle CssClass="GridHeader" />
                            <ItemStyle CssClass="GridItem" />
                            <AlternatingItemStyle CssClass="GridItem" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
