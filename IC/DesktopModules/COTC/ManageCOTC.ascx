﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ManageCOTC.ascx.vb" Inherits="DesktopModules_COTC_ManageCOTC" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register assembly="Telerik.ReportViewer.WebForms,  Version=7.0.13.220, Culture=neutral, PublicKeyToken=A9D7983DFCC261BE" namespace="Telerik.ReportViewer.WebForms" tagprefix="telerik" %>
    <script language="javascript" type="text/javascript">        
        function textboxMultilineMaxNumber(txt, maxLen) {
            try {
                if (txt.value.length > (maxLen - 1)) return false;
            } catch (e) {
            }
        }   

</script>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:TextBoxSetting BehaviorID="rvRIN" Validation-IsRequired="false" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRIN" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<telerik:RadInputManager ID="rimCOTC" runat="server" >
    <telerik:RegExpTextBoxSetting BehaviorID="rvBeneCNIC" Validation-IsRequired="true"
        ValidationExpression="^[0-9 -]*$" ErrorMessage="Invalid ID Number" EmptyMessage="Enter Beneficiary ID No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneCNIC" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rvBeneIDNOType" Validation-IsRequired="true"  ErrorMessage="Invalid ID Number Type"  EmptyMessage="Enter Beneficiary ID Type">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneIDNOType" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="rvBeneCellNo" Validation-IsRequired="true"
        ValidationExpression="^[0-9]*$" ErrorMessage="Invalid Cell No" EmptyMessage="Enter Cell No">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneCellNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rvBeneAddress1" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Address is missing">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneAddress1" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rvBeneAddress2" Validation-IsRequired="false"
        EmptyMessage="Enter Address 2">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneAddress2" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rvBeneAddress3" Validation-IsRequired="false"
        EmptyMessage="Enter Address 3">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneAddress3" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="rvRemitterCellNo" Validation-IsRequired="false"
        ValidationExpression="^[0-9]*$" ErrorMessage="Invalid Cell No" EmptyMessage="Enter Cell No">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRemitterCellNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>    
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue" Text="Cash Over The Counter"></asp:Label>
            <br />
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label5" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <table width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="Label1" runat="server" Text="RIN" CssClass="lbl"></asp:Label>
                        <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtRIN" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Button ID="btnVerify" runat="server" Text="Verify" CausesValidation="true" CssClass="btn" />
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                    
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <table width="100%" id="tblInstructionInfo" runat="server">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top"  style="width: 25%">
                        <asp:Label ID="lblInstNo" runat="server" Text="Instruction No." CssClass="lbl" Font-Bold="True"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblClientName" runat="server" CssClass="lbl" Font-Bold="True" 
                            Text="Client Name"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top"  style="width: 25%">
                        <asp:Label ID="lblprvInstNo" runat="server" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblprvClientName" runat="server" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top"  style="width: 25%">
                        <asp:Label ID="Label15" runat="server" Text="Beneficiary Name" CssClass="lbl" Font-Bold="True"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="Label19" runat="server" Text="Beneficiary Cell No." CssClass="lbl"
                            Font-Bold="True"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top"  style="width: 25%">
                        <asp:Label ID="lblprvBeneName" runat="server" Text="" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblprvBeneCellNo" runat="server" CssClass="lbl"></asp:Label>
                        </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top"  style="width: 25%">
                        <asp:Label ID="Label46" runat="server" CssClass="lbl" Font-Bold="True" Text="Amount (PKR)"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="Label47" runat="server" Text="Beneficiary ID No Type" CssClass="lbl" 
                            Font-Bold="True"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top"  style="width: 25%">
                        <asp:Label ID="lblprvAmountPKR" runat="server" CssClass="lbl" Text=""></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblprvBeneIDNoType" runat="server" CssClass="lbl"></asp:Label>
                        </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top"  style="width: 25%">
                        <asp:Label ID="lblBeneEmail" runat="server" CssClass="lbl" Font-Bold="True" 
                            Text="Beneficiary Email"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="Label36" runat="server" Text="Beneficiary ID No" CssClass="lbl" 
                            Font-Bold="True"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top"  style="width: 25%">
                        <asp:Label ID="lblprvBeneEmailID" runat="server" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblprvBeneIDNo" runat="server" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top"  style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top"  style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                    <asp:HiddenField ID="hfRemittID" runat="server" />
                        &nbsp;</td>
                </tr>
                <tr align="center" valign="top" style="width: 100%">
                    <td align="center" valign="top" colspan="4">
                        &nbsp;<asp:Button ID="btnPrintReceipt" runat="server" Text="Print" 
                            CssClass="btn" ValidationGroup="Verify" />
                    &nbsp;<asp:Button ID="btnProceed" runat="server" Text="Pay" CssClass="btn" ValidationGroup="Verify" />
                    &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" 
                            CausesValidation="False" />
                    </td>
                </tr>                
            </table>
        </td>
        </tr> 
</table>
<telerik:ReportViewer ID="rvRemittanceReport" runat="server" Visible="true" CssClass="hidden">
                        </telerik:ReportViewer>

