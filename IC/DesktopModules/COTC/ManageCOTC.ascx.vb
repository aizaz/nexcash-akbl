﻿Imports ICBO
Imports ICBO.IC
Imports Telerik.Reporting.Processing
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.ReportViewer

Partial Class DesktopModules_COTC_ManageCOTC
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            
            If Page.IsPostBack = False Then
                btnPrintReceipt.Attributes.Item("onclick") = "if (Page_ClientValidate('Verify')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnPrintReceipt, Nothing).ToString() & " } "
                btnProceed.Attributes.Item("onclick") = "if (Page_ClientValidate('Verify')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnProceed, Nothing).ToString() & " } "
                Dim objICUser As New ICUser

                objICUser.LoadByPrimaryKey(Me.UserId)
                If objICUser.UserType = "Bank User" Then
                    If objICUser.UpToICOfficeByOfficeCode.IsApprove = False Then
                        UIUtilities.ShowDialog(Me, "Error", "Sorry! Your branch is not approved.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                        Exit Sub
                    End If
                    If objICUser.UpToICOfficeByOfficeCode.BranchCashTillAccountNumber Is Nothing Or objICUser.UpToICOfficeByOfficeCode.BranchCashTillAccountNumber = "" Then
                        UIUtilities.ShowDialog(Me, "Error", "Sorry! Branch Cash Till account is not tagged with your branch.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                        Exit Sub
                    End If
                    ViewState("htRights") = Nothing
                    GetPageAccessRights()
                    tblInstructionInfo.Visible = False
                    rimCOTC.Enabled = False
                Else
                    UIUtilities.ShowDialog(Me, "Error", "Sorry! You are not authorized to login.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Manage COTC")
            ViewState("htRights") = htRights

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        Dim objICInstruction As New ICInstruction
        Dim ToAccountNo, ToAccountBranchCode, ToAccountCurrency, ToAccountClientNo, ToAccountProfitCentr, ToAccountSeqNo, ToAccountType As String
        Dim objICProductType As New ICProductType
        Dim AccountStatus As String = Nothing
        Dim CashTillAccountStatus As String = Nothing
        Dim AccountBalance As Double = 0
        Dim objICUser As New ICUser
        Dim StanNo As String = Nothing
        Dim StrAction As String = Nothing


        Try
                objICUser.LoadByPrimaryKey(Me.UserId.ToString())

                objICInstruction.LoadByPrimaryKey(hfRemittID.Value.ToString())
                If (objICInstruction.DisbursedBy Is Nothing) Then
                    UIUtilities.ShowDialog(Me, "Error", "Please Print receipt first.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Try
                    If CBUtilities.GetEODStatus() = "01" Then
                        UIUtilities.ShowDialog(Me, "Error", "EOD Status : " & CBUtilities.GetErrorMessageForTransactionFromResponseCode("01", "EOD"), Dialogmessagetype.Failure, NavigateURL(41))
                        Exit Sub
                    End If
                Catch ex As Exception
                    UIUtilities.ShowDialog(Me, "Error", "EOD Status : " & ex.Message.ToString, Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End Try
                If objICInstruction.Status.ToString <> "48" Then
                    UIUtilities.ShowDialog(Me, "Error", "Invalid selected instruction status", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                objICProductType.LoadByPrimaryKey(objICInstruction.ProductTypeCode)
                If objICProductType.IsFunded = False Then
                    ''Client Account Status
                    Try
                        AccountStatus = CBUtilities.AccountStatus(objICInstruction.ClientAccountNo.ToString, CBUtilities.AccountType.RB, "Instruction", objICInstruction.InstructionID.ToString, "Debit", "COTC", objICInstruction.ClientAccountBranchCode)

                    Catch ex As Exception
                        UIUtilities.ShowDialog(Me, "Error", "Account Status: " & ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End Try
                    ''Client Account Balance
                    Try
                        AccountBalance = CBUtilities.BalanceInquiry(objICInstruction.ClientAccountNo.ToString, CBUtilities.AccountType.RB, "Instruction", objICInstruction.InstructionID.ToString, objICInstruction.ClientAccountBranchCode)
                        If AccountBalance = -1 Then
                            UIUtilities.ShowDialog(Me, "Error", "Funds not available", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        Else
                            If objICInstruction.Amount > AccountBalance Then
                                UIUtilities.ShowDialog(Me, "Error", "Funds not available", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        End If
                    Catch ex As Exception
                        UIUtilities.ShowDialog(Me, "Error", "Account Status: " & ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End Try
                End If

                ''Cash Till Account Status if RB
                ToAccountNo = objICUser.UpToICOfficeByOfficeCode.BranchCashTillAccountNumber
                ToAccountBranchCode = objICUser.UpToICOfficeByOfficeCode.OfficeCode.ToString
                'ToAccountBranchCode = objICUser.UpToICOfficeByOfficeCode.BranchCashTillAccountBranchCode
                ToAccountCurrency = objICUser.UpToICOfficeByOfficeCode.BranchCashTillAccountCurrency
                ToAccountProfitCentr = ""
                ToAccountSeqNo = ""
                ToAccountClientNo = ""
                ToAccountType = objICUser.UpToICOfficeByOfficeCode.BranchCashTillAccountType
                If objICUser.UpToICOfficeByOfficeCode.BranchCashTillAccountType = "RB" Then
                    Try
                        CashTillAccountStatus = CBUtilities.AccountStatus(ToAccountNo, CBUtilities.AccountType.RB, "Instruction", objICInstruction.InstructionID.ToString, "Credit", "COTC", ToAccountBranchCode)
                        If CashTillAccountStatus = "Active" Then
                            If objICProductType.IsFunded = False Then
                                If CBUtilities.FundsTransfer(StanNo, objICInstruction.ClientAccountNo, objICInstruction.ClientAccountBranchCode, objICInstruction.ClientAccountCurrency, "", "", "", CBUtilities.AccountType.RB, ToAccountNo, ToAccountBranchCode, ToAccountCurrency, ToAccountClientNo, ToAccountSeqNo, ToAccountProfitCentr, CBUtilities.AccountType.RB, objICInstruction.Amount, "Instruction", objICInstruction.InstructionID.ToString, "49", objICInstruction.Status, Me.UserInfo.UserID.ToString, Nothing, objICInstruction.ProductTypeCode.ToString, Nothing, Nothing, "Funds Transfer") = True Then
                                    StrAction = Nothing
                                    StrAction = "Instruction With ID: [ " & objICInstruction.InstructionID & " ], From Account number [ " & objICInstruction.PayableAccountNumber & "], From Branch Code [" & objICInstruction.PayableAccountBranchCode & "], To Account number [" & ToAccountNo & "], To Branch Code [" & ToAccountBranchCode & "] disbursed succcefully. Action was taken by user"
                                    StrAction += " [" & Me.UserInfo.UserID.ToString & " ] [ " & Me.UserInfo.Username & " ]"
                                    objICInstruction.DisbursedBranchCode = objICUser.UpToICOfficeByOfficeCode.OfficeID
                                    ICInstructionController.UpDateInstruction(objICInstruction, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                                    ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "49", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "", "UPDATE", StrAction)
                                    UIUtilities.ShowDialog(Me, "COTC", "Instruction disbursed successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL(41))
                                Else
                                    UIUtilities.ShowDialog(Me, "COTC", "Instruction not disbursed successfully", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                                End If
                            Else
                                If objICProductType.FundedAccountType = "RB" Then
                                    If CBUtilities.FundsTransfer(StanNo, objICInstruction.PayableAccountNumber, objICInstruction.PayableAccountBranchCode, objICInstruction.PayableAccountCurrency, "", "", "", CBUtilities.AccountType.RB, ToAccountNo, ToAccountBranchCode, ToAccountCurrency, ToAccountClientNo, ToAccountSeqNo, ToAccountProfitCentr, CBUtilities.AccountType.RB, objICInstruction.Amount, "Instruction", objICInstruction.InstructionID.ToString, "49", objICInstruction.Status, Me.UserInfo.UserID.ToString, Nothing, objICInstruction.ProductTypeCode.ToString, Nothing, Nothing, "Funds Transfer") = True Then
                                        StrAction = Nothing
                                        StrAction = "Instruction With ID: [ " & objICInstruction.InstructionID & " ], From Account number [ " & objICInstruction.PayableAccountNumber & "], From Branch Code [" & objICInstruction.PayableAccountBranchCode & "], To Account number [" & ToAccountNo & "], To Branch Code [" & ToAccountBranchCode & "] disbursed succcefully. Action was taken by user"
                                        StrAction += " [" & Me.UserInfo.UserID.ToString & " ] [ " & Me.UserInfo.Username & " ]"
                                        objICInstruction.DisbursedBranchCode = objICUser.UpToICOfficeByOfficeCode.OfficeID
                                        ICInstructionController.UpDateInstruction(objICInstruction, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                                        ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "49", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "", "UPDATE", StrAction)
                                        UIUtilities.ShowDialog(Me, "COTC", "Instruction disbursed successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL(41))
                                    Else
                                        UIUtilities.ShowDialog(Me, "COTC", "Instruction not disbursed successfully", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                                    End If
                                Else
                                    If CBUtilities.FundsTransfer(StanNo, objICInstruction.PayableAccountNumber, objICInstruction.PayableAccountBranchCode, objICInstruction.PayableAccountCurrency, objICInstruction.PayableClientNo, objICInstruction.PayableSeqNo, objICInstruction.PayableProfitCentre, CBUtilities.AccountType.GL, ToAccountNo, ToAccountBranchCode, ToAccountCurrency, ToAccountClientNo, ToAccountSeqNo, ToAccountProfitCentr, CBUtilities.AccountType.RB, objICInstruction.Amount, "Instruction", objICInstruction.InstructionID.ToString, "49", objICInstruction.Status, Me.UserInfo.UserID.ToString, Nothing, objICInstruction.ProductTypeCode.ToString, Nothing, Nothing, "Funds Transfer") = True Then
                                        StrAction = Nothing
                                        StrAction = "Instruction With ID: [ " & objICInstruction.InstructionID & " ], From Account number [ " & objICInstruction.PayableAccountNumber & "], From Branch Code [" & objICInstruction.PayableAccountBranchCode & "], To Account number [" & ToAccountNo & "], To Branch Code [" & ToAccountBranchCode & "] disbursed succcefully. Action was taken by user"
                                        StrAction += " [" & Me.UserInfo.UserID.ToString & " ] [ " & Me.UserInfo.Username & " ]"
                                        objICInstruction.DisbursedBranchCode = objICUser.UpToICOfficeByOfficeCode.OfficeID
                                        ICInstructionController.UpDateInstruction(objICInstruction, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                                        ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "49", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "", "UPDATE", StrAction)
                                        UIUtilities.ShowDialog(Me, "COTC", "Instruction disbursed successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL(41))
                                    Else
                                        UIUtilities.ShowDialog(Me, "COTC", "Instruction not disbursed successfully", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                                    End If
                                End If



                            End If
                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Cash Till Account Status: " & CashTillAccountStatus.ToString, ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    Catch ex As Exception
                        UIUtilities.ShowDialog(Me, "Error", "Cash Till Account Status: " & ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End Try

                Else
                    If objICProductType.IsFunded = False Then
                        If CBUtilities.FundsTransfer(StanNo, objICInstruction.ClientAccountNo, objICInstruction.ClientAccountBranchCode, objICInstruction.ClientAccountCurrency, "", "", "", CBUtilities.AccountType.RB, ToAccountNo, ToAccountBranchCode, ToAccountCurrency, ToAccountClientNo, ToAccountSeqNo, ToAccountProfitCentr, CBUtilities.AccountType.GL, objICInstruction.Amount, "Instruction", objICInstruction.InstructionID.ToString, "49", objICInstruction.Status, Me.UserInfo.UserID.ToString, Nothing, objICInstruction.ProductTypeCode.ToString, Nothing, Nothing, "Funds Transfer") = True Then
                            StrAction = Nothing
                            StrAction = "Instruction With ID: [ " & objICInstruction.InstructionID & " ], From Account number [ " & objICInstruction.PayableAccountNumber & "], From Branch Code [" & objICInstruction.PayableAccountBranchCode & "], To Account number [" & ToAccountNo & "], To Branch Code [" & ToAccountBranchCode & "] disbursed succcefully. Action was taken by user"
                            StrAction += " [" & Me.UserInfo.UserID.ToString & " ] [ " & Me.UserInfo.Username & " ]"
                            objICInstruction.DisbursedBranchCode = objICUser.UpToICOfficeByOfficeCode.OfficeID

                            ICInstructionController.UpDateInstruction(objICInstruction, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "49", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "", "UPDATE", StrAction)
                            UIUtilities.ShowDialog(Me, "COTC", "Instruction disbursed successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL(41))
                        Else
                            UIUtilities.ShowDialog(Me, "COTC", "Instruction not disbursed successfully", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))

                        End If
                    Else
                        If objICProductType.FundedAccountType = "RB" Then
                            If CBUtilities.FundsTransfer(StanNo, objICInstruction.PayableAccountNumber, objICInstruction.PayableAccountBranchCode, objICInstruction.PayableAccountCurrency, "", "", "", CBUtilities.AccountType.RB, ToAccountNo, ToAccountBranchCode, ToAccountCurrency, ToAccountClientNo, ToAccountSeqNo, ToAccountProfitCentr, CBUtilities.AccountType.GL, objICInstruction.Amount, "Instruction", objICInstruction.InstructionID.ToString, "49", objICInstruction.Status, Me.UserInfo.UserID.ToString, Nothing, objICInstruction.ProductTypeCode.ToString, Nothing, Nothing, "Funds Transfer") = True Then
                                StrAction = Nothing
                                StrAction = "Instruction With ID: [ " & objICInstruction.InstructionID & " ], From Account number [ " & objICInstruction.PayableAccountNumber & "], From Branch Code [" & objICInstruction.PayableAccountBranchCode & "], To Account number [" & ToAccountNo & "], To Branch Code [" & ToAccountBranchCode & "] disbursed succcefully. Action was taken by user"
                                StrAction += " [" & Me.UserInfo.UserID.ToString & " ] [ " & Me.UserInfo.Username & " ]"
                                objICInstruction.DisbursedBranchCode = objICUser.UpToICOfficeByOfficeCode.OfficeID
                                ICInstructionController.UpDateInstruction(objICInstruction, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                                ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "49", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "", "UPDATE", StrAction)
                                UIUtilities.ShowDialog(Me, "COTC", "Instruction disbursed successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL(41))
                            Else
                                UIUtilities.ShowDialog(Me, "COTC", "Instruction not disbursed successfully", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                            End If
                        Else
                            If CBUtilities.FundsTransfer(StanNo, objICInstruction.PayableAccountNumber, objICInstruction.PayableAccountBranchCode, objICInstruction.PayableAccountCurrency, objICInstruction.PayableClientNo, objICInstruction.PayableSeqNo, objICInstruction.PayableProfitCentre, CBUtilities.AccountType.GL, ToAccountNo, ToAccountBranchCode, ToAccountCurrency, ToAccountClientNo, ToAccountSeqNo, ToAccountProfitCentr, CBUtilities.AccountType.GL, objICInstruction.Amount, "Instruction", objICInstruction.InstructionID.ToString, "49", objICInstruction.Status, Me.UserInfo.UserID.ToString, Nothing, objICInstruction.ProductTypeCode.ToString, Nothing, Nothing, "Funds Transfer") = True Then
                                StrAction = Nothing
                                StrAction = "Instruction With ID: [ " & objICInstruction.InstructionID & " ], From Account number [ " & objICInstruction.PayableAccountNumber & "], From Branch Code [" & objICInstruction.PayableAccountBranchCode & "], To Account number [" & ToAccountNo & "], To Branch Code [" & ToAccountBranchCode & "] disbursed succcefully. Action was taken by user"
                                StrAction += " [" & Me.UserInfo.UserID.ToString & " ] [ " & Me.UserInfo.Username & " ]"
                                objICInstruction.DisbursedBranchCode = objICUser.UpToICOfficeByOfficeCode.OfficeID
                                ICInstructionController.UpDateInstruction(objICInstruction, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                                ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "49", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "", "UPDATE", StrAction)
                                UIUtilities.ShowDialog(Me, "COTC", "Instruction disbursed successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL(41))
                            Else
                                UIUtilities.ShowDialog(Me, "COTC", "Instruction not disbursed successfully", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                            End If
                        End If
                    End If


                End If
                'UIUtilities.ShowDialog(Me, "COTC", "Instruction not disbursed successfully", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(176))

                'Response.Redirect(NavigateURL(41), False)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub
    Protected Sub btnVerify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVerify.Click
        Dim objICInstruction As New ICInstruction
        Dim objICOffice As New ICOffice
        Dim objICUser As New ICUser



        Dim Action As String = ""
        objICUser.LoadByPrimaryKey(Me.UserId)
        If Page.IsValid Then
            Try
                If txtRIN.Text = "" Or txtRIN.Text Is Nothing Then
                    UIUtilities.ShowDialog(Me, "Error", "Please enter RIN", ICBO.IC.Dialogmessagetype.Failure)
                    txtRIN.Text = ""
                    rimCOTC.Enabled = False
                    Exit Sub
                End If
                objICInstruction = ICInstructionController.GetInstructionToDisburse(txtRIN.Text.ToString())
                If objICInstruction.InstructionID Is Nothing Then
                    UIUtilities.ShowDialog(Me, "Error", "Invalid RIN.", ICBO.IC.Dialogmessagetype.Failure)
                    txtRIN.Text = ""
                    rimCOTC.Enabled = False
                    Exit Sub
                ElseIf objICInstruction.Status.ToString() = "49" Then
                    UIUtilities.ShowDialog(Me, "Error", "This transaction has already been processed.", ICBO.IC.Dialogmessagetype.Failure)
                    txtRIN.Text = ""
                    rimCOTC.Enabled = False
                    Exit Sub
                ElseIf objICInstruction.UpToICCompanyByCompanyCode.IsApprove = False Then
                    UIUtilities.ShowDialog(Me, "Error", "Client is not approved", ICBO.IC.Dialogmessagetype.Failure)
                    txtRIN.Text = ""
                    rimCOTC.Enabled = False
                    Exit Sub
                End If
                If objICInstruction.BeneficiaryBranchCode IsNot Nothing And objICInstruction.BeneficiaryBranchCode <> "" And objICInstruction.BeneficiaryBranchCode <> "0" Then
                    If objICInstruction.BeneficiaryBranchCode <> objICUser.OfficeCode Then
                        objICOffice.LoadByPrimaryKey(objICInstruction.BeneficiaryBranchCode)
                        UIUtilities.ShowDialog(Me, "Error", "Disbursement branch for instruction is " & objICOffice.OfficeName & "", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                        Exit Sub
                    End If
                End If






                hfRemittID.Value = objICInstruction.InstructionID.ToString()
                ViewInstructionInfo(objICInstruction)
                'If Not objICInstruction.DisbursedBy Is Nothing Then
                '    btnPrintReceipt.Enabled = False

                'End If



                Action = "Instruction : Instruction with InstructionID [" & objICInstruction.InstructionID.ToString() & "] was verified at COTC by user [" & Me.UserInfo.Username.ToString() & "]."

                ICUtilities.AddAuditTrail(Action.ToString(), "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString(), Me.UserInfo.Username.ToString(), "COTC Verify")
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
   
    Private Sub ViewInstructionInfo(ByVal objICInstruction As ICInstruction)
        'tblInstructionInfo.Style.Remove("display")
        tblInstructionInfo.Visible = True

        If Not objICInstruction.BeneficiaryName = "" Or objICInstruction.BeneficiaryName Is Nothing Then
            lblprvBeneName.Text = objICInstruction.BeneficiaryName
        End If
        If Not objICInstruction.BeneficiaryIDNo = "" Or objICInstruction.BeneficiaryIDNo Is Nothing Then
            lblprvBeneIDNo.Text = objICInstruction.BeneficiaryIDNo
        End If
        If Not objICInstruction.BeneficiaryIDType = "" Or objICInstruction.BeneficiaryIDType Is Nothing Then
            '    txtBeneIDNOType.Text = ""
            'Else
            lblprvBeneIDNoType.Text = objICInstruction.BeneficiaryIDType
        End If
        If Not objICInstruction.CompanyCode Is Nothing Then
            lblprvClientName.Text = objICInstruction.UpToICCompanyByCompanyCode.CompanyName
        End If
        'If Not objICInstruction.BeneficiaryAddress = "" Or objICInstruction.BeneficiaryAddress Is Nothing Then
        '    txtBeneAddress1.Text = objICInstruction.BeneficiaryAddress
        'End If
       
        If Not objICInstruction.BeneficiaryMobile = "" Or objICInstruction.BeneficiaryMobile Is Nothing Then
            lblprvBeneCellNo.Text = objICInstruction.BeneficiaryMobile
        End If
        
        If objICInstruction.BeneficiaryEmail = "" Or objICInstruction.BeneficiaryEmail Is Nothing Then
            lblprvBeneEmailID.Text = "N/A"
        Else
            lblprvBeneEmailID.Text = objICInstruction.BeneficiaryEmail
        End If
        If objICInstruction.InstructionID Is Nothing Then
            lblprvInstNo.Text = "N/A"
        Else
            lblprvInstNo.Text = objICInstruction.InstructionID
        End If
        lblprvAmountPKR.Text = CDbl(objICInstruction.Amount.ToString()).ToString("N2")
        hfRemittID.Value = objICInstruction.InstructionID.ToString()
        rimCOTC.Enabled = True
    End Sub
    Private Sub PrinRemittancePaymentReceipt(ByVal InstructionID As String)
        Dim rpt As New Telerik.Reporting.Report
       
        rpt = ICInstructionProcessController.PerformPrintitngOnCOTCInstructions(InstructionID, Me.UserInfo.Username.ToString, Me.UserInfo.UserID.ToString, "Print")

        If rpt IsNot Nothing Then
            'rpt.DataSource = Intelligenes.FRC.FRCInstructionController.GetInstructionForPaymentPrinting(RemittID)
            rvRemittanceReport.Report = rpt
            rvRemittanceReport.RefreshReport()
            Dim scr As String = "window.onload = function() { " & Me.rvRemittanceReport.ClientID & ".PrintReport() }"
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "scr", scr, True)
        Else
            UIUtilities.ShowDialog(Me, "Error", "There is an issue in printing. Please contact Al Baraka administrator", ICBO.IC.Dialogmessagetype.Failure)
            Exit Sub
        End If
    End Sub

    Protected Sub btnPrintReceipt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrintReceipt.Click
        Dim objICInstruction As New ICInstruction

        Dim Action As String = ""
        Dim cuser As New ICUser


        Try
                objICInstruction.LoadByPrimaryKey(hfRemittID.Value.ToString())
                'If objICInstruction.DisbursedBy Is Nothing Then

                cuser.LoadByPrimaryKey(Me.UserId)


                objICInstruction.DisbursedBy = Me.UserId
                objICInstruction.DisbursedDate = Date.Now
                objICInstruction.Save()
                objICInstruction = New ICInstruction
                objICInstruction.LoadByPrimaryKey(hfRemittID.Value.ToString())


                PrinRemittancePaymentReceipt(objICInstruction.InstructionID)
                Action = "Instruction : Instruction with RemittID [" & objICInstruction.InstructionID.ToString() & "] was printed by user [" & Me.UserInfo.Username.ToString() & "]."
                ICUtilities.AddAuditTrail(Action.ToString(), "Instruction", objICInstruction.InstructionID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString(), "COTC Print")
                ' btnPrintReceipt.Enabled = False

                'Else
                '    UIUtilities.ShowDialog(Me, "COTC", "This transaction has already been processed.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                '    btnPrintReceipt.Enabled = False
                'End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub

    

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim objICInstruction As New ICInstruction
        objICInstruction.es.Connection.CommandTimeout = 3600
        Dim Action As String = ""
        Try
            objICInstruction.LoadByPrimaryKey(hfRemittID.Value.ToString())

            Action = "Instruction : Instruction with InstructionID [" & objICInstruction.InstructionID.ToString() & "] was view by user [" & Me.UserInfo.Username.ToString() & "]."

            ICUtilities.AddAuditTrail(Action.ToString(), "Instruction", objICInstruction.InstructionID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString(), "View")
            Response.Redirect(NavigateURL(41), False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
    
End Class
