﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Partial Class DesktopModules_Country_SaveCountry
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private CountryCode As String
    Private htRights As Hashtable


#Region "Page Load"

    'display code added by Jawwad 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If


            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnSave.Visible = CBool(htRights("Mark EOD"))
                If ICUtilities.GetSettingValue("EODStatus") = "In Process" Then
                    chkActive.Checked = True
                ElseIf ICUtilities.GetSettingValue("EODStatus") = "Not In Process" Then
                    chkActive.Checked = False
                Else
                    chkActive.Checked = False

                End If
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Manage EOD Status")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    

#End Region

#Region "Button"

    'display code added by Jawwad 
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim ActionStr As String = Nothing
                If chkActive.Checked = True Then
                    ActionStr = "EOD Status is updated from [ " & ICUtilities.GetSettingValue("EODStatus") & " ] to "
                    ICUtilities.SetSettingValue("EODStatus", "In Process")
                    ActionStr += "[ " & ICUtilities.GetSettingValue("EODStatus") & " ] . Action was taken by user [ " & Me.UserInfo.UserID & " ]"
                    ActionStr += " [ " & Me.UserInfo.Username.ToString & " ] "
                Else
                    ActionStr = "EOD Status is updated from [ " & ICUtilities.GetSettingValue("EODStatus") & " ] to "
                    ICUtilities.SetSettingValue("EODStatus", "Not In Process")
                    ActionStr += "[ " & ICUtilities.GetSettingValue("EODStatus") & " ] . Action was taken by user [ " & Me.UserInfo.UserID & " ]"
                    ActionStr += " [ " & Me.UserInfo.Username.ToString & " ] "
                End If
                ICUtilities.AddAuditTrail(ActionStr, "Manage EOD Status", "Manage EOD Status", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "UPDATE")
                UIUtilities.ShowDialog(Me, "Manage EOD Status", "EOD Status updated successfully", ICBO.IC.Dialogmessagetype.Success, NavigateURL(41))


            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(41), False)
    End Sub

    

#End Region

   
End Class
