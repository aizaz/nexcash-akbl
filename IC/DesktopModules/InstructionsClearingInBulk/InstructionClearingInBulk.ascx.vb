﻿Imports ICBO
Imports ICBO.IC

Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals

Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports System.Drawing
Imports Telerik.Web.UI

Partial Class DesktopModules_InstructionsClearingInBulk_InstructionClearingInBulk
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private arrStatues As New ArrayList
    Private arrUserRoles As New ArrayList
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_InstructionsClearingInBulk_InstructionClearingInBulk_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            lblMessageForQueue.Style.Add("display", "none")
            If Page.IsPostBack = False Then
                lblMessageForQueue.Text = ICUtilities.GetSettingValue("ErrorMessage")
                btnProceed.Attributes.Item("onclick") = "if (Page_ClientValidate('BulkClearing')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnProceed, Nothing).ToString() & " } "
                btnClear.Attributes.Item("onclick") = "if (Page_ClientValidate('Clear')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";document.getElementById('<%=lblMessageForQueue.ClientID%>').style.display = 'block';this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnClear, Nothing).ToString() & " } "
                If Me.UserInfo.IsSuperUser = False Then
                    Dim objUser As New ICUser
                    objUser.LoadByPrimaryKey(Me.UserId.ToString())
                    If objUser.UserType.ToString() <> "Bank User" Then
                        UIUtilities.ShowDialog(Me, "Bulk Instruction Clearing", "Only authorized Bank Users allowed", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                        Exit Sub
                    Else
                        Dim objUserBranch As New ICOffice
                        If objUserBranch.LoadByPrimaryKey(objUser.OfficeCode) Then
                            If Not (objUserBranch.IsActive = True And objUserBranch.IsApprove = True) Then
                                UIUtilities.ShowDialog(Me, "Bulk Instruction Clearing", "User Branch is not approved.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                                Exit Sub
                            End If
                        End If

                    End If

                End If
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                PageLoad()
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub PageLoad()
        SetArraListRoleID()
        SetdtAPNatureByAssignedRoles()
        SetProductType()
        SetTransferTypeByProductType()
        gvBulkInstructionsForNIFT.DataSource = Nothing
        gvBulkInstructionsForNIFT.DataBind()
        gvBulkInstructions.DataSource = Nothing
        gvBulkInstructions.DataBind()


        gvBulkInstructionsForNIFT.Visible = False
        gvBulkInstructions.Visible = False



        gvBulkInstructionsForNIFT.Visible = False
        gvBulkInstructions.Visible = False


        btnClear.Visible = False
        btnProceed.Visible = True


    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Bulk Clearing")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        arrUserRoles.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Bulk Clearing") = True Then
                    arrUserRoles.Add(RoleID.ToString)
                End If
            Next
        End If
    End Sub

    Private Sub SetdtAPNatureByAssignedRoles()
        Try
            Dim dtAssignedAPNatureOfficeID As New DataTable
            dtAssignedAPNatureOfficeID.Rows.Clear()
            dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserSecond(Me.UserId.ToString, arrUserRoles)
            ViewState("AccountNumber") = Nothing
            ViewState("AccountNumber") = dtAssignedAPNatureOfficeID
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetTransferTypeByProductType()

        Try

            Dim lit As ListItem
            lit = New ListItem
            lit.Text = "-- Select --"
            lit.Value = "0"
            ddlTransferType.Items.Clear()
            ddlTransferType.Items.Add(lit)
            ddlTransferType.AppendDataBoundItems = True

            'lit = New ListItem
            'lit.Text = "Funds Transfer"
            'lit.Value = "Funds Transfer"
            'ddlTransferType.Items.Add(lit)
            lit = New ListItem
            lit.Text = "Inter City"
            lit.Value = "InterCity"
            ddlTransferType.Items.Add(lit)
            lit = New ListItem
            lit.Text = "Same Day"
            lit.Value = "Same Day"
            ddlTransferType.Items.Add(lit)
            lit = New ListItem
            lit.Text = "Normal"
            lit.Value = "Normal"
            ddlTransferType.Items.Add(lit)
            If CBool(htRights("Funds Transfer")) = True Then
                lit = New ListItem
                lit.Text = "Funds Transfer"
                lit.Value = "Funds Transfer"
                ddlTransferType.Items.Add(lit)
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    'Private Sub SetTransferTypeByProductType()

    '    Try

    '        Dim lit As ListItem
    '        lit = New ListItem
    '        lit.Text = "-- Select --"
    '        lit.Value = "0"
    '        ddlTransferType.Items.Clear()
    '        ddlTransferType.Items.Add(lit)
    '        ddlTransferType.AppendDataBoundItems = True
    '        'lit = New ListItem
    '        'lit.Text = "Inter City"
    '        'lit.Value = "InterCity"
    '        'ddlTransferType.Items.Add(lit)
    '        'lit = New ListItem
    '        'lit.Text = "Same Day"
    '        'lit.Value = "Same Day"
    '        'ddlTransferType.Items.Add(lit)
    '        'lit = New ListItem
    '        'lit.Text = "Normal"
    '        'lit.Value = "Normal"
    '        'ddlTransferType.Items.Add(lit)
    '        If CBool(htRights("Funds Transfer")) = True Then
    '            lit = New ListItem
    '            lit.Text = "Funds Transfer"
    '            lit.Value = "Funds Transfer"
    '            ddlTransferType.Items.Add(lit)
    '        Else
    '            lit = New ListItem
    '            lit.Text = "Inter City"
    '            lit.Value = "InterCity"
    '            ddlTransferType.Items.Add(lit)
    '            lit = New ListItem
    '            lit.Text = "Same Day"
    '            lit.Value = "Same Day"
    '            ddlTransferType.Items.Add(lit)
    '            lit = New ListItem
    '            lit.Text = "Normal"
    '            lit.Value = "Normal"
    '            ddlTransferType.Items.Add(lit)
    '        End If

    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    Private Sub SetProductType()
        Try
            'Dim lit As ListItem
            'lit = New ListItem
            'lit.Text = "-- Select --"
            'lit.Value = "0"
            'ddlProductType.Items.Add(lit)

            'If htRights("Cheque Instructions") = True Then
            '    lit = New ListItem
            '    lit.Text = "Cheque"
            '    lit.Value = "Cheque"
            '    ddlProductType.Items.Add(lit)
            'End If
            ''If htRights("DD Instructions") = True Then
            ''    lit = New ListItem
            ''    lit.Text = "DD"
            ''    lit.Value = "DD"
            ''    ddlProductType.Items.Add(lit)
            ''End If
            'If htRights("PO Instructions") = True Then
            '    lit = New ListItem
            '    lit.Text = "PO"
            '    lit.Value = "PO"
            '    ddlProductType.Items.Add(lit)
            'End If
            'If ddlProductType.Items.Count = 2 Then
            '    ddlProductType.Items(1).Selected = True
            '    ddlProductType.Enabled = False
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region
    Private Sub LoadGVBulkInstructions()
        Try
            Dim dtBulkClear As New DataTable
            dtBulkClear = DirectCast(ViewState("dtBulkClear"), DataTable)
            gvBulkInstructions.DataSource = dtBulkClear
            gvBulkInstructions.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(41), False)
    End Sub

    Protected Sub btnProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        Dim str As String = System.IO.Path.GetExtension(fuBulkInnstructionClearing.PostedFile.FileName)
        Dim FilePath As String = ""
        Dim FileName As String = Nothing
        Dim connectionString As String = ""
        Dim i As Integer = 0
        Dim dt As New DataTable
        Dim dr As DataRow
        If Page.IsValid Then
            Try
                If fuBulkInnstructionClearing.HasFile Then
                    If ddlTransferType.SelectedValue.ToString = "Funds Transfer" Then
                        If str = ".xls" Then
                            Dim aFileInfo2 As IO.FileInfo
                            aFileInfo2 = New IO.FileInfo(Server.MapPath("~/UploadedFiles/BulkClearing_") & fuBulkInnstructionClearing.FileName.ToString())
                            'Delete file from HDD. 
                            If aFileInfo2.Exists Then
                                aFileInfo2.Delete()
                            End If
                            fuBulkInnstructionClearing.SaveAs(Server.MapPath("~/UploadedFiles/BulkClearing_") & fuBulkInnstructionClearing.FileName.ToString())
                            FilePath = Server.MapPath("~/UploadedFiles/BulkClearing_") & fuBulkInnstructionClearing.FileName.ToString()
                            connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FilePath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""

                            Dim con As New OleDbConnection(connectionString)
                            Dim cmd As New OleDbCommand()
                            cmd.CommandType = System.Data.CommandType.Text
                            cmd.Connection = con
                            Dim dAdapter As New OleDbDataAdapter(cmd)
                            Dim dtExcelRecords As New DataTable()
                            con.Open()
                            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                            Dim getExcelSheetName As String = "Sheet1$"
                            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
                            dAdapter.SelectCommand = cmd
                            dAdapter.Fill(dtExcelRecords)
                            con.Close()
                            If ddlTransferType.SelectedValue.ToString = "Funds Transfer" Then
                                If dtExcelRecords.Columns.Count <> 4 Then
                                    UIUtilities.ShowDialog(Me, "Bulk Instruction Clearing", "No of Columns not match.", ICBO.IC.Dialogmessagetype.Failure)
                                    Dim aFileInfo As IO.FileInfo
                                    aFileInfo = New IO.FileInfo(FilePath.ToString())
                                    'Delete file from HDD. 
                                    If aFileInfo.Exists Then
                                        aFileInfo.Delete()
                                    End If
                                    Exit Sub
                                End If

                                ProcessFileAndGetValidInstructions(dtExcelRecords, FilePath.ToString())
                                'Else
                                '    If dtExcelRecords.Columns.Count <> 3 Then
                                '        UIUtilities.ShowDialog(Me, "Bulk Instruction Clearing", "No of Columns not match.", ICBO.IC.Dialogmessagetype.Failure)
                                '        Dim aFileInfo As IO.FileInfo
                                '        aFileInfo = New IO.FileInfo(FilePath.ToString())
                                '        'Delete file from HDD. 
                                '        If aFileInfo.Exists Then
                                '            aFileInfo.Delete()
                                '        End If
                                '        Exit Sub
                                '    End If

                            End If

                        Else
                            UIUtilities.ShowDialog(Me, "Bulk Instruction Clearing", "Only .xls file allowed.", ICBO.IC.Dialogmessagetype.Failure)
                        End If
                    Else
                        If str = ".txt" Then
                            Dim aFileInfo2 As IO.FileInfo
                            Dim NIFTCode As String = Nothing
                            Dim LineData As String = Nothing
                            NIFTCode = ICUtilities.GetSettingValue("NIFTCMSCODE")
                            aFileInfo2 = New IO.FileInfo(Server.MapPath("~/UploadedFiles/BulkClearing_") & fuBulkInnstructionClearing.FileName.ToString())
                            'Delete file from HDD. 
                            If aFileInfo2.Exists Then
                                aFileInfo2.Delete()
                            End If
                            fuBulkInnstructionClearing.SaveAs(Server.MapPath("~/UploadedFiles/BulkClearing_") & fuBulkInnstructionClearing.FileName.ToString())
                            FilePath = Server.MapPath("~/UploadedFiles/BulkClearing_") & fuBulkInnstructionClearing.FileName.ToString()
                            Dim arrInsruction As String()
                            Dim aFileInfo As IO.FileInfo
                            aFileInfo = New IO.FileInfo(FilePath.ToString())
                            If aFileInfo.Exists Then
                                Dim mreader As New System.IO.StreamReader(aFileInfo.FullName)

                                dt = DesignDTForClearing()
                                Try
                                    Do While mreader.Peek <> -1
                                        LineData = Nothing
                                        LineData = mreader.ReadLine()
                                        If LineData IsNot Nothing Then
                                            If LineData.Contains("|") Then
                                                arrInsruction = Nothing
                                                arrInsruction = LineData.Split("|")
                                                dr = dt.NewRow
                                                If arrInsruction.Length = 10 Then
                                                    If arrInsruction(6).ToString = NIFTCode Then
                                                        For i = 0 To arrInsruction.Length - 1
                                                            dr(i) = arrInsruction(i).ToString
                                                        Next
                                                        dt.Rows.Add(dr)
                                                        i = i + 1
                                                    End If

                                                End If

                                            End If
                                        Else
                                            If aFileInfo.Exists Then
                                                aFileInfo.Delete()
                                            End If
                                            UIUtilities.ShowDialog(Me, "Bulk Instruction Clearing", "Invalid Data in file", ICBO.IC.Dialogmessagetype.Failure)
                                            Exit Sub
                                        End If
                                    Loop
                                    mreader.Close()
                                    mreader.Dispose()
                                Catch ex As Exception
                                    mreader.Close()
                                    mreader.Dispose()
                                    Throw ex
                                End Try

                                If dt.Rows.Count = 0 Then
                                    UIUtilities.ShowDialog(Me, "Bulk Instruction Clearing", "Invalid Data in file", ICBO.IC.Dialogmessagetype.Failure)

                                    aFileInfo = New IO.FileInfo(FilePath.ToString())
                                    'Delete file from HDD. 
                                    If aFileInfo.Exists Then
                                        aFileInfo.Delete()
                                    End If
                                    Exit Sub
                                End If
                                aFileInfo = New IO.FileInfo(FilePath.ToString())
                                'Delete file from HDD. 
                                If aFileInfo.Exists Then
                                    aFileInfo.Delete()
                                End If
                                ProcessFileAndGetValidInstructionsForNiftInstructions(dt, FilePath)
                            Else

                                UIUtilities.ShowDialog(Me, "Bulk Instruction Clearing", "Only .txt file allowed.", ICBO.IC.Dialogmessagetype.Failure)
                            End If
                        End If
                    End If
                Else
                    UIUtilities.ShowDialog(Me, "Bulk Instruction Clearing", "Invalid Data in file", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception

                Dim aFileInfo As IO.FileInfo
                aFileInfo = New IO.FileInfo(FilePath.ToString())
                'Delete file from HDD. 
                If aFileInfo.Exists Then
                    aFileInfo.Delete()
                End If
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Function DesignDTForClearing() As DataTable
        Dim dt As New DataTable

        dt.Columns.Add(New DataColumn("InstructionDate", GetType(System.String)))
        dt.Columns.Add(New DataColumn("FileNo", GetType(System.String)))
        dt.Columns.Add(New DataColumn("ReferenceNo", GetType(System.String)))
        dt.Columns.Add(New DataColumn("OtherBankCode", GetType(System.String)))
        dt.Columns.Add(New DataColumn("OtherBranchCode", GetType(System.String)))
        dt.Columns.Add(New DataColumn("OurBankCode", GetType(System.String)))
        dt.Columns.Add(New DataColumn("OurBranchCode", GetType(System.String)))
        dt.Columns.Add(New DataColumn("InstrumentNo", GetType(System.String)))
        dt.Columns.Add(New DataColumn("Amount", GetType(System.String)))
        dt.Columns.Add(New DataColumn("AccountNo", GetType(System.String)))
        Return dt
    End Function
    Private Sub ProcessFileAndGetValidInstructions(ByVal dt As DataTable, ByVal FilePath As String)
        ' dt Columns
        ' 0 Instrument Number
        ' 1 BankCode
        ' 2 BeneAccountNumber
        ' 3 BeneAccountTitle
        ' 4 Amount
        ' 5 ClearingType
        Try
            Dim dtBulkCLearing As New DataTable
            Dim chkDuplicateInstrumentNo As String = ""
            Dim DupInst As String()
            Dim InstrumentNoForSearch As String = Nothing
            Dim IsDuplicate As Boolean = False
            Dim InstructionID As String = ""
            Dim drBulkClearing, dr As DataRow
            Dim ResultString As String = ""
            Dim Remarks As String = ""
            Dim TransferType As String = ""
            Dim chkAmount As Double = 0
            Dim dtAccounts As New DataTable
            Dim objBank As New ICBank
            Dim objInstruction As New ICInstruction
            Dim i As Integer = 0
            dtAccounts = DirectCast(ViewState("AccountNumber"), DataTable)
            dtBulkCLearing.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
            dtBulkCLearing.Columns.Add(New DataColumn("Account No.", GetType(System.String)))
            dtBulkCLearing.Columns.Add(New DataColumn("InstrumentNo", GetType(System.String)))
            dtBulkCLearing.Columns.Add(New DataColumn("BeneAccountNumber", GetType(System.String)))
            dtBulkCLearing.Columns.Add(New DataColumn("BeneAccountBranchCode", GetType(System.String)))
            dtBulkCLearing.Columns.Add(New DataColumn("BeneAccountCurrency", GetType(System.String)))
            dtBulkCLearing.Columns.Add(New DataColumn("BeneAccountTitle", GetType(System.String)))
            dtBulkCLearing.Columns.Add(New DataColumn("FetchedAccountTitle", GetType(System.String)))
            dtBulkCLearing.Columns.Add(New DataColumn("TransferType", GetType(System.String)))
            dtBulkCLearing.Columns.Add(New DataColumn("Amount", GetType(System.Double)))
            dtBulkCLearing.Columns.Add(New DataColumn("InstructionAmount", GetType(System.Double)))
            dtBulkCLearing.Columns.Add(New DataColumn("ClearingType", GetType(System.String)))
            dtBulkCLearing.Columns.Add(New DataColumn("Remarks", GetType(System.String)))
            For Each dr In dt.Rows
                Remarks = ""
                TransferType = ""
                drBulkClearing = dtBulkCLearing.NewRow()
                IsDuplicate = False
                ' Instrument Number
                If dr(2).ToString().Trim() = "" Then
                    drBulkClearing("InstructionID") = "-"
                    drBulkClearing("InstrumentNo") = "-"
                    drBulkClearing("Account No.") = "-"
                    drBulkClearing("BeneAccountNumber") = "-"
                    drBulkClearing("BeneAccountBranchCode") = "-"
                    drBulkClearing("BeneAccountCurrency") = "-"
                    drBulkClearing("BeneAccountTitle") = "-"
                    drBulkClearing("FetchedAccountTitle") = "-"
                    drBulkClearing("TransferType") = "-"
                    drBulkClearing("Amount") = 0
                    drBulkClearing("InstructionAmount") = 0
                    drBulkClearing("ClearingType") = "-"
                    Remarks = Remarks.ToString() & "Instrument Number is Empty."
                ElseIf Regex.IsMatch(dr(2).ToString().Trim(), "^[0-9]{1,9}$") = False Then
                    drBulkClearing("InstructionID") = "-"
                    drBulkClearing("InstrumentNo") = "-"
                    drBulkClearing("BeneAccountNumber") = "-"
                    drBulkClearing("BeneAccountBranchCode") = "-"
                    drBulkClearing("BeneAccountCurrency") = "-"
                    drBulkClearing("BeneAccountTitle") = "-"
                    drBulkClearing("FetchedAccountTitle") = "-"
                    drBulkClearing("TransferType") = "-"
                    drBulkClearing("Amount") = 0
                    drBulkClearing("InstructionAmount") = 0
                    drBulkClearing("ClearingType") = "-"
                    drBulkClearing("Account No.") = "-"
                    Remarks = Remarks.ToString() & "Instrument Number is not valid."
                ElseIf dr(2).ToString().Trim.Length() > 30 Then
                    drBulkClearing("InstructionID") = "-"
                    drBulkClearing("InstrumentNo") = "-"
                    drBulkClearing("BeneAccountNumber") = "-"
                    drBulkClearing("BeneAccountBranchCode") = "-"
                    drBulkClearing("BeneAccountCurrency") = "-"
                    drBulkClearing("BeneAccountTitle") = "-"
                    drBulkClearing("FetchedAccountTitle") = "-"
                    drBulkClearing("TransferType") = "-"
                    drBulkClearing("Amount") = 0
                    drBulkClearing("InstructionAmount") = 0
                    drBulkClearing("ClearingType") = "-"
                    drBulkClearing("Account No.") = "-"
                    Remarks = Remarks.ToString() & "Instrument Number length exceeded."
                Else
                    If chkDuplicateInstrumentNo.Contains("|") Then
                        DupInst = chkDuplicateInstrumentNo.Split("|")
                        For i = 0 To DupInst.Length - 1
                            If dr(2).ToString().Trim() = DupInst(i).ToString().Trim() Then
                                IsDuplicate = True
                            End If
                        Next
                    End If
                    If IsDuplicate = True Then
                        drBulkClearing("InstructionID") = "-"
                        drBulkClearing("InstrumentNo") = dr(2).ToString().Trim()
                        drBulkClearing("InstructionAmount") = 0
                        Remarks = Remarks.ToString() & "Duplicate Instrument Number ; "
                    Else
                        chkDuplicateInstrumentNo = chkDuplicateInstrumentNo & dr(2).ToString().Trim() & "|"
                        InstrumentNoForSearch = Nothing
                        drBulkClearing("InstrumentNo") = dr(2).ToString().Trim()
                       
                        InstrumentNoForSearch = dr(2).ToString().Trim()


                        ResultString = ICInstructionProcessController.GetInstructionForClearing("Instrument Number", InstrumentNoForSearch, dtAccounts)
                        If ResultString.ToString().Contains("|") Then
                            objInstruction = New ICInstruction
                            InstructionID = ResultString.ToString().Split("|")(1).ToString()
                            objInstruction = ICInstructionController.GetInstructionForClearing(InstructionID)(0)
                            drBulkClearing("InstructionID") = objInstruction.InstructionID
                            drBulkClearing("InstructionAmount") = objInstruction.Amount
                            Remarks = Remarks.ToString() & ResultString.ToString().Split("|")(0)
                        Else
                            drBulkClearing("InstructionID") = "-"
                            drBulkClearing("InstructionAmount") = 0
                            Remarks = Remarks.ToString() & ResultString.ToString() & " ; "
                        End If
                    End If
                End If
                ' Bene Account number
                If ddlTransferType.SelectedValue.ToString = "Funds Transfer" Then
                    If dr(3).ToString().Trim() <> "" Then
                        If dr(3).ToString().Trim().Length > 100 Then
                            drBulkClearing("InstructionID") = "-"
                            drBulkClearing("InstrumentNo") = "-"
                            drBulkClearing("BeneAccountNumber") = "-"
                            drBulkClearing("BeneAccountBranchCode") = "-"
                            drBulkClearing("BeneAccountCurrency") = "-"
                            drBulkClearing("BeneAccountTitle") = "-"
                            drBulkClearing("FetchedAccountTitle") = "-"
                            drBulkClearing("TransferType") = "-"
                            drBulkClearing("Amount") = 0
                            drBulkClearing("InstructionAmount") = 0
                            drBulkClearing("ClearingType") = "-"
                            drBulkClearing("Account No.") = "-"
                            Remarks = Remarks.ToString() & "Beneficiary account no length exceeded."
                            drBulkClearing("FetchedAccountTitle") = "-"
                        Else
                            Dim StrAccountDetails As String()
                            Dim AccountStatus As String = ""
                            Try
                                AccountStatus = CBUtilities.AccountStatus(dr(3).ToString().Trim(), ICBO.CBUtilities.AccountType.RB, "Beneficiary Account Status", dr(3).ToString().Trim(), "Credit", "Bulk Clearing", dr(3).ToString().Trim()).ToString()
                            Catch ex As Exception
                                AccountStatus = ex.Message.ToString
                            End Try
                            If AccountStatus = "Active" Then
                                Try
                                    StrAccountDetails = (CBUtilities.TitleFetch(dr(3).ToString().Trim(), ICBO.CBUtilities.AccountType.RB, "Beneficiary Account Title", dr(3).ToString().Trim()).ToString.Split(";"))
                                    drBulkClearing("BeneAccountNumber") = dr(3).ToString().Trim()
                                    drBulkClearing("BeneAccountBranchCode") = StrAccountDetails(1).ToString
                                    drBulkClearing("BeneAccountCurrency") = StrAccountDetails(2).ToString
                                    drBulkClearing("FetchedAccountTitle") = StrAccountDetails(0).ToString
                                Catch ex As Exception
                                    drBulkClearing("InstructionID") = "-"
                                    drBulkClearing("InstrumentNo") = "-"
                                    drBulkClearing("BeneAccountNumber") = "-"
                                    drBulkClearing("BeneAccountBranchCode") = "-"
                                    drBulkClearing("BeneAccountCurrency") = "-"
                                    drBulkClearing("BeneAccountTitle") = "-"
                                    drBulkClearing("FetchedAccountTitle") = "-"
                                    drBulkClearing("TransferType") = "-"
                                    drBulkClearing("Amount") = 0
                                    drBulkClearing("InstructionAmount") = 0
                                    drBulkClearing("ClearingType") = "-"
                                    drBulkClearing("Account No.") = "-"
                                    Remarks = Remarks.ToString() & "Bene Account Title : " & ex.Message.ToString() & " ; "
                                    drBulkClearing("FetchedAccountTitle") = "-"
                                End Try
                            Else
                                drBulkClearing("InstructionID") = "-"
                                drBulkClearing("InstrumentNo") = "-"
                                drBulkClearing("BeneAccountNumber") = "-"
                                drBulkClearing("BeneAccountBranchCode") = "-"
                                drBulkClearing("BeneAccountCurrency") = "-"
                                drBulkClearing("BeneAccountTitle") = "-"
                                drBulkClearing("FetchedAccountTitle") = "-"
                                drBulkClearing("TransferType") = "-"
                                drBulkClearing("Amount") = 0
                                drBulkClearing("InstructionAmount") = 0
                                drBulkClearing("ClearingType") = "-"
                                drBulkClearing("Account No.") = "-"
                                Remarks = Remarks.ToString() & "Bene Account Status : " & AccountStatus.ToString() & " ; "
                                drBulkClearing("FetchedAccountTitle") = "-"
                            End If
                        End If
                    Else
                        drBulkClearing("InstructionID") = "-"
                        drBulkClearing("InstrumentNo") = "-"
                        drBulkClearing("BeneAccountNumber") = "-"
                        drBulkClearing("BeneAccountBranchCode") = "-"
                        drBulkClearing("BeneAccountCurrency") = "-"
                        drBulkClearing("BeneAccountTitle") = "-"
                        drBulkClearing("FetchedAccountTitle") = "-"
                        drBulkClearing("TransferType") = "-"
                        drBulkClearing("Amount") = 0
                        drBulkClearing("InstructionAmount") = 0
                        drBulkClearing("Account No.") = "-"
                        drBulkClearing("ClearingType") = "-"
                        Remarks = Remarks.ToString() & "Beneficiary account no is empty"
                        drBulkClearing("FetchedAccountTitle") = "-"
                    End If
                Else
                    drBulkClearing("BeneAccountNumber") = "-"
                    drBulkClearing("BeneAccountBranchCode") = "-"
                    drBulkClearing("BeneAccountCurrency") = "-"
                    drBulkClearing("BeneAccountTitle") = "-"
                    drBulkClearing("FetchedAccountTitle") = "-"
                End If

                ' Amount
                If dr(0).ToString().Trim() <> "" Then
                    If Double.TryParse(dr(0).ToString().Trim(), chkAmount) = False Then
                        drBulkClearing("Amount") = 0
                        Remarks = Remarks & "Amount is not in correct format ; "
                    Else
                        drBulkClearing("Amount") = CDbl(dr(0).ToString().Trim())
                    End If
                Else
                    drBulkClearing("Amount") = 0
                End If
                ' Clearing Type
                If ddlTransferType.SelectedValue.ToString().Trim() <> "" Then
                    If ddlTransferType.SelectedValue.ToString().Trim().ToLower = "normal" Then
                        drBulkClearing("ClearingType") = "Normal"
                        TransferType = "Clearing"
                    ElseIf ddlTransferType.SelectedValue.ToString().Trim().ToLower = "intercity" Then
                        drBulkClearing("ClearingType") = "Intercity"
                        TransferType = "Clearing"
                    ElseIf ddlTransferType.SelectedValue.ToString().Trim().ToLower = "same day" Then
                        drBulkClearing("ClearingType") = "Same Day"
                        TransferType = "Clearing"
                    ElseIf ddlTransferType.SelectedValue.ToString().Trim().ToLower = "funds transfer" Then
                        drBulkClearing("ClearingType") = "Funds Transfer"
                        TransferType = "Funds Transfer"
                    End If
                End If
                If dr(1).ToString().Trim() <> "" Then
                    drBulkClearing("Account No.") = dr(1).ToString().Trim()
                Else
                    drBulkClearing("Account No.") = "-"
                End If

                drBulkClearing("TransferType") = TransferType.ToString()
                drBulkClearing("Remarks") = Remarks.ToString()
                dtBulkCLearing.Rows.Add(drBulkClearing)
            Next
            If dtBulkCLearing.Rows.Count > 0 Then
                gvBulkInstructions.DataSource = dtBulkCLearing
                gvBulkInstructions.DataBind()
                gvBulkInstructions.Visible = True
                gvBulkInstructionsForNIFT.DataSource = Nothing
                gvBulkInstructionsForNIFT.Visible = False
                pnlBulkClearingInstructions.Visible = True
                btnProceed.Visible = False
                btnClear.Visible = htRights("Clear")
            End If
            ViewState("dtClearing") = dtBulkCLearing
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ProcessFileAndGetValidInstructionsForNiftInstructions(ByVal dt As DataTable, ByVal FilePath As String)
        ' dt Columns
        ' 0 Instrument Number
        ' 1 BankCode
        ' 2 BeneAccountNumber
        ' 3 BeneAccountTitle
        ' 4 Amount
        ' 5 ClearingType
        Try
            Dim dtBulkCLearing As New DataTable
            Dim chkDuplicateInstrumentNo As String = ""
            Dim DupInst As String()
            Dim InstrumentNoForSearch As String = Nothing
            Dim IsDuplicate As Boolean = False
            Dim InstructionID As String = ""
            Dim drBulkClearing, dr As DataRow
            Dim ResultString As String = ""
            Dim Remarks As String = ""
            Dim TransferType As String = ""
            Dim chkAmount As Double = 0
            Dim dtAccounts As New DataTable
            Dim objBank As New ICBank
            Dim objInstruction As New ICInstruction
            Dim i As Integer = 0
            Dim j As Integer = 0
            dtAccounts = DirectCast(ViewState("AccountNumber"), DataTable)
            dtBulkCLearing.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
            dtBulkCLearing.Columns.Add(New DataColumn("InstructionDate", GetType(System.String)))
            dtBulkCLearing.Columns.Add(New DataColumn("Account No.", GetType(System.String)))
            dtBulkCLearing.Columns.Add(New DataColumn("InstrumentNo", GetType(System.String)))
            dtBulkCLearing.Columns.Add(New DataColumn("TransferType", GetType(System.String)))
            dtBulkCLearing.Columns.Add(New DataColumn("Amount", GetType(System.Double)))
            dtBulkCLearing.Columns.Add(New DataColumn("InstructionAmount", GetType(System.Double)))
            dtBulkCLearing.Columns.Add(New DataColumn("ClearingType", GetType(System.String)))
            dtBulkCLearing.Columns.Add(New DataColumn("Remarks", GetType(System.String)))
            dtBulkCLearing.Columns.Add(New DataColumn("NIFTCode", GetType(System.String)))

            For Each dr In dt.Rows
                Remarks = ""
                TransferType = ""
                drBulkClearing = dtBulkCLearing.NewRow()
                IsDuplicate = False
                ' Instrument Number
                If dr(7).ToString().Trim() = "" Then
                    drBulkClearing("InstructionID") = "-"
                    drBulkClearing("InstrumentNo") = "-"
                    drBulkClearing("Account No.") = "-"
                    drBulkClearing("TransferType") = "-"
                    drBulkClearing("Amount") = 0
                    drBulkClearing("InstructionAmount") = 0
                    drBulkClearing("ClearingType") = "-"
                    Remarks = Remarks.ToString() & "Instrument Number is Empty."
                ElseIf Regex.IsMatch(dr(7).ToString().Trim(), "^[0-9]{1,10}$") = False Then
                    drBulkClearing("InstructionID") = "-"
                    drBulkClearing("InstrumentNo") = "-"

                    drBulkClearing("TransferType") = "-"
                    drBulkClearing("Amount") = 0
                    drBulkClearing("InstructionAmount") = 0
                    drBulkClearing("ClearingType") = "-"
                    drBulkClearing("Account No.") = "-"
                    Remarks = Remarks.ToString() & "Instrument Number is not valid."
                ElseIf dr(7).ToString().Trim.Length() <> 10 Then
                    drBulkClearing("InstructionID") = "-"
                    drBulkClearing("InstrumentNo") = "-"
                    drBulkClearing("TransferType") = "-"
                    drBulkClearing("Amount") = 0
                    drBulkClearing("InstructionAmount") = 0
                    drBulkClearing("ClearingType") = "-"
                    drBulkClearing("Account No.") = "-"
                    Remarks = Remarks.ToString() & "Instrument Number length is not valid."
                Else
                    If chkDuplicateInstrumentNo.Contains("|") Then
                        DupInst = chkDuplicateInstrumentNo.Split("|")
                        For i = 0 To DupInst.Length - 1
                            If dr(7).ToString().Trim() = DupInst(i).ToString().Trim() Then
                                IsDuplicate = True
                            End If
                        Next
                    End If
                    If IsDuplicate = True Then
                        drBulkClearing("InstructionID") = "-"
                        drBulkClearing("InstrumentNo") = dr(7).ToString().Trim()
                        drBulkClearing("InstructionAmount") = 0
                        Remarks = Remarks.ToString() & "Duplicate Instrument Number ; "
                    Else
                        chkDuplicateInstrumentNo = chkDuplicateInstrumentNo & dr(7).ToString().Trim() & "|"
                        drBulkClearing("InstrumentNo") = dr(7).ToString().Trim()
                    End If
                End If
                If dr(0).ToString().Trim() <> "" Then
                    Try
                        Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy", "yyyy-MM-dd"}
                        Dim datetw As String = dr(0).ToString().Trim()
                        Dim ValueDate As Date = Date.ParseExact(datetw, format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                        Dim ResultDate As Date = Nothing
                        If Date.TryParse(ValueDate.ToString, ResultDate) = False Then
                            drBulkClearing("InstructionID") = "-"
                            drBulkClearing("InstrumentNo") = "-"
                            drBulkClearing("Account No.") = "-"
                            drBulkClearing("TransferType") = "-"
                            drBulkClearing("Amount") = 0
                            drBulkClearing("InstructionAmount") = 0
                            drBulkClearing("ClearingType") = "-"
                            Remarks = Remarks.ToString() & "Instrument date is not in correct format."
                            drBulkClearing("InstructionDate") = dr(0).ToString().Trim()

                        ElseIf ValueDate.Date <> Now.Date Then
                            drBulkClearing("InstructionID") = "-"
                            drBulkClearing("InstrumentNo") = "-"
                            drBulkClearing("Account No.") = "-"
                            drBulkClearing("TransferType") = "-"
                            drBulkClearing("Amount") = 0
                            drBulkClearing("InstructionAmount") = 0
                            drBulkClearing("ClearingType") = "-"
                            drBulkClearing("InstructionDate") = CDate(dr(0).ToString().Trim()).ToString("dd-MMM-yyyy")
                            Remarks = Remarks.ToString() & "Instrument date must be equal to current date"
                        Else
                            drBulkClearing("InstructionDate") = CDate(ValueDate.Date).ToString("dd-MMM-yyyy")
                            InstrumentNoForSearch = Nothing

                            If dr(7).ToString().Trim().Length = 10 And IsDuplicate = False Then
                                Try
                                    If Integer.TryParse(dr(7).ToString().Trim(), j) = True Then

                                        InstrumentNoForSearch = CInt(dr(7).ToString().Trim())

                                        ResultString = ICInstructionProcessController.GetInstructionForClearing("Instrument Number", InstrumentNoForSearch, dtAccounts)
                                        If ResultString.ToString().Contains("|") Then
                                            objInstruction = New ICInstruction
                                            InstructionID = ResultString.ToString().Split("|")(1).ToString()
                                            objInstruction = ICInstructionController.GetInstructionForClearing(InstructionID)(0)
                                            drBulkClearing("InstructionID") = objInstruction.InstructionID
                                            drBulkClearing("InstructionAmount") = objInstruction.Amount
                                            Remarks = Remarks.ToString() & ResultString.ToString().Split("|")(0)
                                        Else
                                            drBulkClearing("InstructionID") = "-"
                                            drBulkClearing("InstructionAmount") = 0
                                            Remarks = Remarks.ToString() & ResultString.ToString() & " ; "
                                        End If
                                    Else
                                        drBulkClearing("InstructionID") = "-"
                                        drBulkClearing("InstrumentNo") = dr(7).ToString().Trim()
                                        drBulkClearing("Account No.") = "-"
                                        drBulkClearing("TransferType") = "-"
                                        drBulkClearing("Amount") = 0
                                        drBulkClearing("InstructionAmount") = 0
                                        drBulkClearing("ClearingType") = "-"
                                        Remarks = Remarks.ToString() & "Instrument date is not in correct format."
                                        drBulkClearing("InstructionDate") = dr(0).ToString().Trim()
                                    End If
                                Catch ex As Exception
                                    drBulkClearing("InstructionID") = "-"
                                    drBulkClearing("InstrumentNo") = dr(7).ToString().Trim()
                                    drBulkClearing("Account No.") = "-"
                                    drBulkClearing("TransferType") = "-"
                                    drBulkClearing("Amount") = 0
                                    drBulkClearing("InstructionAmount") = 0
                                    drBulkClearing("ClearingType") = "-"
                                    Remarks = Remarks.ToString() & "Instrument Number is invalid"
                                    drBulkClearing("InstructionDate") = "-"
                                End Try
                            Else
                                drBulkClearing("InstructionID") = "-"
                                drBulkClearing("InstrumentNo") = dr(7).ToString().Trim()
                                drBulkClearing("Account No.") = "-"
                                drBulkClearing("TransferType") = "-"
                                drBulkClearing("Amount") = 0
                                drBulkClearing("InstructionAmount") = 0
                                drBulkClearing("ClearingType") = "-"
                                'Remarks = Remarks.ToString() & "Instrument Number is invalid"
                                drBulkClearing("InstructionDate") = "-"
                            End If
                            

                        End If
                    Catch ex As Exception
                        drBulkClearing("InstructionID") = "-"
                        drBulkClearing("InstrumentNo") = "-"
                        drBulkClearing("Account No.") = "-"
                        drBulkClearing("TransferType") = "-"
                        drBulkClearing("Amount") = 0
                        drBulkClearing("InstructionAmount") = 0
                        drBulkClearing("ClearingType") = "-"
                        Remarks = Remarks.ToString() & ex.Message.ToString
                        drBulkClearing("InstructionDate") = "-"

                    End Try
                Else
                    drBulkClearing("InstructionID") = "-"
                    drBulkClearing("InstrumentNo") = "-"
                    drBulkClearing("Account No.") = "-"
                    drBulkClearing("TransferType") = "-"
                    drBulkClearing("Amount") = 0
                    drBulkClearing("InstructionAmount") = 0
                    drBulkClearing("ClearingType") = "-"
                    Remarks = Remarks.ToString() & "Instrument date is empty."
                    drBulkClearing("InstructionDate") = dr(0).ToString().Trim()

                End If
                ' Amount
                If dr(9).ToString().Trim() <> "" Then
                    If Double.TryParse(dr(9).ToString().Trim(), chkAmount) = False Then
                        drBulkClearing("Amount") = 0
                        Remarks = Remarks & "Amount is not in correct format ; "
                    Else
                        drBulkClearing("Amount") = CDbl(dr(9).ToString().Trim())
                    End If
                Else
                    drBulkClearing("Amount") = 0
                End If

                ' Clearing Type
                If ddlTransferType.SelectedValue.ToString().Trim() <> "" Then
                    If ddlTransferType.SelectedValue.ToString().Trim().ToLower = "normal" Then
                        drBulkClearing("ClearingType") = "Normal"
                        TransferType = "Clearing"
                    ElseIf ddlTransferType.SelectedValue.ToString().Trim().ToLower = "intercity" Then
                        drBulkClearing("ClearingType") = "Intercity"
                        TransferType = "Clearing"
                    ElseIf ddlTransferType.SelectedValue.ToString().Trim().ToLower = "same day" Then
                        drBulkClearing("ClearingType") = "Same Day"
                        TransferType = "Clearing"
                    End If
                End If
                If dr(8).ToString().Trim() <> "" Then
                    drBulkClearing("Account No.") = dr(8).ToString().Trim()
                Else
                    drBulkClearing("Account No.") = "-"
                End If

                drBulkClearing("TransferType") = TransferType.ToString()
                drBulkClearing("Remarks") = Remarks.ToString()
                dtBulkCLearing.Rows.Add(drBulkClearing)
            Next
            If dtBulkCLearing.Rows.Count > 0 Then
                gvBulkInstructionsForNIFT.DataSource = dtBulkCLearing
                gvBulkInstructionsForNIFT.DataBind()
                pnlBulkClearingInstructions.Visible = True
                gvBulkInstructions.DataSource = Nothing
                gvBulkInstructions.Visible = False
                gvBulkInstructionsForNIFT.Visible = True
                btnProceed.Visible = False
                btnClear.Visible = htRights("Clear")
            End If
            ViewState("dtClearing") = dtBulkCLearing
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub DesignDtForSummary()
        ViewState("Summary") = Nothing
        Dim dtSummary As New DataTable

        dtSummary.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
        dtSummary.Columns.Add(New DataColumn("Status", GetType(System.String)))
        dtSummary.Columns.Add(New DataColumn("Message", GetType(System.String)))
        dtSummary.Columns.Add(New DataColumn("IsChequeReturned", GetType(System.Boolean)))
        ViewState("Summary") = dtSummary
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click

        Dim dtClear As New DataTable
            Dim drClear As DataRow
            Dim ToAccountNumber As String = ""
            Dim ToAccountBranchCode As String = ""
            Dim ToAccountCurrency As String = ""
            Dim ToSeqNo As String = ""
            Dim ToProfitCentre As String = ""
            Dim ToClientNo As String = ""
            Dim ToAccountType As String = ""
            Dim dt As New DataTable
            Dim ArryListInstID As New ArrayList
            Dim ClearInstCount As Integer = 0
            Dim PhysicalInstrumentNumber As String = ""
            Dim InstructionID As String = ""
            Dim Result As String = ""
            Dim PassCount, FailCount As Integer
            Dim objUser As New ICUser
            Dim objUserBranch As New ICOffice
            Dim dtVerifiedInstruction As DataTable
            ArryListInstID.Clear()
            DesignDtForSummary()
            Dim objICProductType As ICProductType
            Dim objICInstruction As ICInstruction
            Dim dtSummary As DataTable
            Dim dr As DataRow
            dtSummary = New DataTable
            dtSummary = DirectCast(ViewState("Summary"), DataTable)
            Dim ClearingType As String = Nothing
            Dim TransferType As String = Nothing
            Dim ClientAccountStatus As String = Nothing


            Try
                If CBUtilities.GetEODStatus() = "01" Then
                    UIUtilities.ShowDialog(Me, "Error", "EOD Status : " & CBUtilities.GetErrorMessageForTransactionFromResponseCode("01", "EOD"), Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If
            Catch ex As Exception
                UIUtilities.ShowDialog(Me, "Error", "EOD Status : " & ex.Message.ToString, Dialogmessagetype.Failure, NavigateURL(41))
                Exit Sub
            End Try


            Try
                PassCount = 0
                FailCount = 0
                dtClear = DirectCast(ViewState("dtClearing"), DataTable)
                objUser = New ICUser
                objUserBranch = New ICOffice
                objUser.LoadByPrimaryKey(Me.UserId.ToString())
                objUserBranch.LoadByPrimaryKey(objUser.OfficeCode)
                If dtClear.Rows.Count > 0 Then
                    For Each drClear In dtClear.Rows
                        ToAccountNumber = ""
                        ToAccountBranchCode = ""
                        ToAccountCurrency = ""
                        ToAccountType = ""
                        ToSeqNo = ""
                        ToProfitCentre = ""
                        ToClientNo = ""
                        ClearingType = Nothing
                        TransferType = Nothing
                        If drClear("InstructionID").ToString() <> "-" Then
                            InstructionID = drClear("InstructionID").ToString()
                            If drClear("TransferType").ToString() = "Funds Transfer" Then
                                ToAccountNumber = drClear("BeneAccountNumber").ToString()
                                ToAccountBranchCode = drClear("BeneAccountBranchCode").ToString
                                ToAccountCurrency = drClear("BeneAccountCurrency").ToString
                                ToAccountType = "RB"
                                ToSeqNo = ""
                                ToProfitCentre = ""
                                ToClientNo = ""
                                ClearingType = "Funds Transfer"
                            ElseIf drClear("TransferType").ToString() = "Clearing" Then
                                ClearingType = "Clearing"
                                If drClear("ClearingType").ToString() = "Normal" Then
                                    TransferType = "Normal"
                                    If objUserBranch.ClearingNormalAccountNumber Is Nothing Then
                                        dr = dtSummary.NewRow
                                        objICInstruction = New ICInstruction
                                        objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                                        dr("InstructionID") = InstructionID.ToString
                                        dr("Status") = objICInstruction.Status.ToString
                                        dr("IsChequeReturned") = False
                                        dr("Message") = "Instruction not processed successfully . Normal account is not tagged with user branch"
                                        dtSummary.Rows.Add(dr)
                                        FailCount = FailCount + 1
                                        Continue For
                                    End If
                                    If objUserBranch.ClearingNormalAccountType.ToString() = "GL" Then
                                        ToAccountNumber = objUserBranch.ClearingNormalAccountNumber.ToString()
                                        ToAccountBranchCode = objUser.ClearingBranchCode
                                        ToAccountCurrency = objUserBranch.ClearingNormalAccountCurrency.ToString()
                                        ToSeqNo = ""
                                        ToProfitCentre = ""
                                        ToClientNo = ""
                                        ToAccountType = objUserBranch.ClearingNormalAccountType.ToString
                                    Else
                                        ToAccountNumber = objUserBranch.ClearingNormalAccountNumber.ToString()
                                        ToAccountBranchCode = objUser.ClearingBranchCode
                                        ToAccountCurrency = objUserBranch.ClearingNormalAccountCurrency.ToString()
                                        ToSeqNo = ""
                                        ToProfitCentre = ""
                                        ToClientNo = ""
                                        ToAccountType = objUserBranch.ClearingNormalAccountType.ToString
                                    End If

                                ElseIf drClear("ClearingType").ToString() = "Intercity" Then
                                    TransferType = "InterCity"
                                    If objUserBranch.InterCityAccountNumber Is Nothing Then
                                        dr = dtSummary.NewRow

                                        objICInstruction = New ICInstruction
                                        objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                                        dr("InstructionID") = InstructionID.ToString
                                        dr("Status") = objICInstruction.Status.ToString
                                        dr("IsChequeReturned") = False
                                        dr("Message") = "Instruction not processed successfully . Intercity account is not tagged with user branch"
                                        dtSummary.Rows.Add(dr)
                                        FailCount = FailCount + 1
                                        Continue For
                                    End If

                                    If objUserBranch.InterCityAccountType.ToString() = "GL" Then
                                        ToAccountNumber = objUserBranch.InterCityAccountNumber.ToString()
                                        ToAccountBranchCode = objUser.ClearingBranchCode
                                        ToAccountCurrency = objUserBranch.InterCityAccountCurrency.ToString()
                                        ToSeqNo = ""
                                        ToProfitCentre = ""
                                        ToClientNo = ""
                                        ToAccountType = objUserBranch.InterCityAccountType.ToString
                                    Else
                                        ToAccountNumber = objUserBranch.InterCityAccountNumber.ToString()
                                        ToAccountBranchCode = objUser.ClearingBranchCode
                                        ToAccountCurrency = objUserBranch.InterCityAccountCurrency.ToString()
                                        ToSeqNo = ""
                                        ToProfitCentre = ""
                                        ToClientNo = ""
                                        ToAccountType = objUserBranch.InterCityAccountType.ToString
                                    End If


                                ElseIf drClear("ClearingType").ToString() = "Same Day" Then
                                    TransferType = "SameDay"
                                    If objUserBranch.SameDayAccountNumber Is Nothing Then
                                        dr = dtSummary.NewRow

                                        objICInstruction = New ICInstruction
                                        objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                                        dr("InstructionID") = InstructionID.ToString
                                        dr("Status") = objICInstruction.Status.ToString
                                        dr("IsChequeReturned") = False
                                        dr("Message") = "Instruction not processed successfully . Same day account is not tagged with user branch"
                                        dtSummary.Rows.Add(dr)
                                        FailCount = FailCount + 1
                                        Continue For
                                    End If
                                    If objUserBranch.SameDayAccountType.ToString() = "GL" Then
                                        ToAccountNumber = objUserBranch.SameDayAccountNumber.ToString()
                                        ToAccountBranchCode = objUser.ClearingBranchCode
                                        ToAccountCurrency = objUserBranch.SameDayAccountCurrency.ToString()
                                        ToSeqNo = ""
                                        ToProfitCentre = ""
                                        ToClientNo = ""
                                        ToAccountType = objUserBranch.SameDayAccountType.ToString
                                    Else
                                        ToAccountNumber = objUserBranch.SameDayAccountNumber.ToString()
                                        ToAccountBranchCode = objUser.ClearingBranchCode
                                        ToAccountCurrency = objUserBranch.SameDayAccountCurrency.ToString()
                                        ToSeqNo = ""
                                        ToProfitCentre = ""
                                        ToClientNo = ""
                                        ToAccountType = objUserBranch.SameDayAccountType.ToString
                                    End If
                                End If
                            End If
                            If (ToAccountNumber.ToString() <> "" And ToAccountBranchCode.ToString() <> "" And ToAccountCurrency.ToString() <> "") Then
                                dr = dtSummary.NewRow
                                ArryListInstID.Add(InstructionID)
                                PhysicalInstrumentNumber = Nothing
                                objICInstruction = New ICInstruction
                                objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                                If objICInstruction.Status = "30" Then
                                    dr("InstructionID") = InstructionID.ToString
                                    dr("Status") = objICInstruction.Status.ToString
                                    dr("IsChequeReturned") = False
                                    dr("Message") = "Instruction already cleared."
                                    dtSummary.Rows.Add(dr)
                                    FailCount = FailCount + 1
                                    Continue For
                                ElseIf objICInstruction.Status = "38" Then
                                    dr("InstructionID") = InstructionID.ToString
                                    dr("Status") = objICInstruction.Status.ToString
                                    dr("IsChequeReturned") = False
                                    dr("Message") = "Instruction already Pending for Clearing Approval."
                                    dtSummary.Rows.Add(dr)
                                    FailCount = FailCount + 1
                                    Continue For
                                ElseIf objICInstruction.Status = "5" Then
                                    dr("InstructionID") = InstructionID.ToString
                                    dr("Status") = objICInstruction.Status.ToString
                                    dr("IsChequeReturned") = False
                                    dr("Message") = "Instrument has been cancelled."
                                    dtSummary.Rows.Add(dr)
                                    FailCount = FailCount + 1
                                    Continue For
                                ElseIf objICInstruction.Status = "32" Then
                                    dr("InstructionID") = InstructionID.ToString
                                    dr("Status") = objICInstruction.Status.ToString
                                    dr("IsChequeReturned") = False
                                    dr("Message") = "Instrument has been stopped."
                                    dtSummary.Rows.Add(dr)
                                    FailCount = FailCount + 1
                                    Continue For
                                ElseIf objICInstruction.Status = "52" Or objICInstruction.Status = "39" Or objICInstruction.Status = "40" Then
                                    dr("InstructionID") = InstructionID.ToString
                                    dr("Status") = objICInstruction.Status.ToString
                                    dr("IsChequeReturned") = False
                                    dr("Message") = "Instruction is already in Processing."
                                    dtSummary.Rows.Add(dr)
                                    FailCount = FailCount + 1
                                    Continue For
                                End If
                                If objICInstruction.PaymentMode = "Cheque" Then
                                    ClientAccountStatus = Nothing
                                    Try
                                        ClientAccountStatus = Nothing
                                        'ClientAccountStatus = CBUtilities.AccountStatusForClearing(objInstruction.ClientAccountNo, CBUtilities.AccountType.RB, "CQClientAccount", objInstruction.InstructionID, "Debit")
                                        ClientAccountStatus = CBUtilities.AccountStatus(objICInstruction.ClientAccountNo, CBUtilities.AccountType.RB, "Client Account Status", objICInstruction.InstructionID, "Debit", "Clearing", objICInstruction.ClientAccountBranchCode)
                                        If ClientAccountStatus = "Active" Then
                                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "52", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE")
                                            Result = ICInstructionProcessController.InstructionClearing(InstructionID.ToString(), ToAccountNumber.ToString(), ToAccountBranchCode.ToString(), ToAccountCurrency.ToString(), ToSeqNo.ToString, ToClientNo.ToString, ToProfitCentre.ToString, ToAccountType.ToString, Me.UserId, Me.UserInfo.Username, PhysicalInstrumentNumber, objUserBranch.OfficeID.ToString, "30", objICInstruction.Status.ToString, ClearingType, TransferType)
                                        Else
                                            dr("InstructionID") = "-"
                                            dr("Status") = objICInstruction.Status.ToString
                                            dr("IsChequeReturned") = False
                                            dr("Message") = "Instruction not processed successfully . Status [ " & ClientAccountStatus.ToString & " ]"
                                            dtSummary.Rows.Add(dr)
                                            FailCount = FailCount + 1
                                            Continue For
                                        End If
                                    Catch ex As Exception
                                        dr("InstructionID") = "-"
                                        dr("Status") = objICInstruction.Status.ToString
                                        dr("IsChequeReturned") = False
                                        dr("Message") = "Instruction not processed successfully . Status [ " & ClientAccountStatus.ToString & " ]"
                                        dtSummary.Rows.Add(dr)
                                        FailCount = FailCount + 1
                                        Continue For
                                    End Try
                                Else
                                    ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "52", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE")
                                    Result = ICInstructionProcessController.InstructionClearing(InstructionID.ToString(), ToAccountNumber.ToString(), ToAccountBranchCode.ToString(), ToAccountCurrency.ToString(), ToSeqNo.ToString, ToClientNo.ToString, ToProfitCentre.ToString, ToAccountType.ToString, Me.UserId, Me.UserInfo.Username, PhysicalInstrumentNumber, objUserBranch.OfficeID.ToString, "30", objICInstruction.Status.ToString, ClearingType, TransferType)
                                End If

                                objICInstruction = New ICInstruction
                                objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                                If (Result.ToString() = "OK" Or Result.ToString() = "Pending For Approval") Then
                                    dr("InstructionID") = InstructionID.ToString
                                    dr("Status") = objICInstruction.Status.ToString
                                    dr("IsChequeReturned") = False
                                    dr("Message") = "Instruction processed successfully . Status [ " & Result.ToString & " ]"
                                    dtSummary.Rows.Add(dr)

                                    If drClear("TransferType").ToString() = "Funds Transfer" Then
                                        objICInstruction.ClearingType = "Funds Transfer"
                                        objICInstruction.Save()
                                    ElseIf drClear("TransferType").ToString() = "Clearing" Then
                                        If drClear("ClearingType").ToString() = "Normal" Then
                                            objICInstruction.ClearingType = "Normal"
                                            objICInstruction.Save()
                                        ElseIf drClear("ClearingType").ToString() = "Intercity" Then
                                            objICInstruction.ClearingType = "Inter City"
                                            objICInstruction.Save()
                                        ElseIf drClear("ClearingType").ToString() = "Same Day" Then
                                            objICInstruction.ClearingType = "Same Day"
                                            objICInstruction.Save()
                                        End If
                                    End If
                                    PassCount = PassCount + 1
                                ElseIf Result.ToString = "Funds Not Available" Then
                                    dr("InstructionID") = InstructionID.ToString
                                    dr("Status") = objICInstruction.Status.ToString
                                    dr("IsChequeReturned") = True
                                    dr("Message") = "Instruction not processed successfully . Status [ " & Result.ToString & " ]"
                                    dtSummary.Rows.Add(dr)
                                    FailCount = FailCount + 1
                                Else
                                    dr("InstructionID") = InstructionID.ToString
                                    dr("Status") = objICInstruction.Status.ToString
                                    dr("IsChequeReturned") = False
                                    dr("Message") = "Instruction not processed successfully . Status [ " & Result.ToString & " ]"
                                    dtSummary.Rows.Add(dr)
                                    FailCount = FailCount + 1
                                End If
                            Else


                            End If
                        Else
                            dr = dtSummary.NewRow
                            dr("InstructionID") = "-"
                            dr("Status") = ""
                            dr("IsChequeReturned") = False
                            'dr("Message") = "Invalid Instruction ID"
                            dr("Message") = drClear("Remarks")
                            dtSummary.Rows.Add(dr)
                            FailCount = FailCount + 1
                        End If
                    Next
                    PageLoad()
                    ddlTransferType.SelectedValue = "0"
                    If dtSummary.Rows.Count > 0 Then

                        gvChequeReturned.DataSource = Nothing
                        gvChequeReturned.DataSource = dtSummary
                        gvChequeReturned.DataBind()
                        If gvChequeReturned.Items.Count > 0 Then
                            SetVisibilityOfCancelAndChequeReturned(dtSummary)
                        Else
                            'btnChequeReturned.Visible = False
                            'btnCancelGrid.Visible = False
                        End If
                        pnlShowSummary1.Style.Remove("display")
                        Dim scr As String
                        scr = "window.onload = function () {showsumdialog1('Transaction Summary',true,700,600);};"
                        Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                        'Response.Redirect(NavigateURL(118))
                    Else
                        gvChequeReturned.DataSource = Nothing
                        pnlShowSummary1.Style.Add("display", "none")
                    End If
                    'UIUtilities.ShowDialog(Me, "Bulk Instruction Clearing", "[" & PassCount.ToString() & "] Instruction(s) processed successfully.<br />[" & FailCount.ToString() & "] Instruction(s) not processed successfully", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    If Not PassCount = 0 Then
                        Dim ArryListPassStatus As ArrayList
                        ArryListPassStatus = New ArrayList
                        ArryListPassStatus.Add(30)
                        dt = ICInstructionController.GetAllInstructionAccountAmountAndProductTypeWiseByArrayListInstructionIDAndStatusWithInstrumentNo(ArryListInstID, "30")
                        EmailUtilities.TransactionsCleared(dt)
                        SMSUtilities.Transactionscleared(dt, ArryListInstID, ArryListPassStatus)
                        dtVerifiedInstruction = New DataTable
                        dtVerifiedInstruction = ICInstructionController.GetAllInstructionAccountAmountAndProductTypeWiseByArrayListInstructionIDAndStatusWithInstrumentNoForClearingEmail(ArryListInstID, "38")
                        If dtVerifiedInstruction.Rows.Count > 0 Then
                            ArryListPassStatus = New ArrayList
                            ArryListPassStatus.Add(38)
                            EmailUtilities.TransactionsAreReadyForClearing(dtVerifiedInstruction)
                            SMSUtilities.TransactionsreadyforClearing(dtVerifiedInstruction, ArryListInstID, ArryListPassStatus)
                        End If
                    End If

                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub
    Private Sub SetVisibilityOfCancelAndChequeReturned(ByVal dt As DataTable)
        'Dim Result As Boolean = False
        'For Each dr As DataRow In dt.Rows
        '    If dr("IsChequeReturned") = True Then
        '        Result = True
        '        ''btnChequeReturned.Visible = CBool(htRights("Clear"))
        '        btnCancelGrid.Visible = True
        '        Exit For
        '    End If
        'Next
        'If Result = False Then
        '    'btnChequeReturned.Visible = False
        'End If
    End Sub
    Private Function CheckGVChequeReturnedCheckedForProcessAll() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvChequeReturned.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True And chkSelect.Enabled = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Sub HideGV()
        gvBulkInstructionsForNIFT.DataSource = Nothing
        gvBulkInstructionsForNIFT.DataBind()
        gvBulkInstructionsForNIFT.Visible = False
        btnProceed.Visible = True
        btnClear.Visible = False
    End Sub
    Protected Sub ddlTransferType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTransferType.SelectedIndexChanged
        Try
            If ddlTransferType.SelectedValue.ToString = "InterCity" Or ddlTransferType.SelectedValue.ToString = "Same Day" Or ddlTransferType.SelectedValue.ToString = "Normal" Then
                Dim objICUser As New ICUser
                objICUser.LoadByPrimaryKey(Me.UserId.ToString)
                If objICUser.ClearingBranchCode Is Nothing Or objICUser.ClearingBranchCode = "" Then
                    SetTransferTypeByProductType()
                    UIUtilities.ShowDialog(Me, "Error", "Clearing Branch Code is not tagged with user", ICBO.IC.Dialogmessagetype.Failure)
                End If
            End If
            HideGV()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub gvBulkInstructionsForNIFT_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles gvBulkInstructionsForNIFT.ItemDataBound

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
           
            If item.GetDataKeyValue("Remarks") = "OK" Then
                If item.GetDataKeyValue("Amount") <> item.GetDataKeyValue("InstructionAmount") Then
                    gvBulkInstructionsForNIFT.AlternatingItemStyle.CssClass = "GridItemComparison"
                    e.Item.CssClass = "GridItemComparison"
                Else
                    gvBulkInstructionsForNIFT.AlternatingItemStyle.CssClass = "GridItem"
                    e.Item.CssClass = "GridItem"
                End If
            Else
                gvBulkInstructionsForNIFT.AlternatingItemStyle.CssClass = "GridItem"

                e.Item.CssClass = "GridItem"
            End If
        End If
    End Sub

    Protected Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(NavigateURL(119), False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnCancelGrid_Click(sender As Object, e As EventArgs) Handles btnCancelGrid2.Click
        Try

            Response.Redirect(NavigateURL(119))
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
