﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InstructionClearingInBulk.ascx.vb"
    Inherits="DesktopModules_InstructionsClearingInBulk_InstructionClearingInBulk" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;

        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to clear selected instruction(s)?\nFinancial transaction(s) is in process, please do not refresh the page.") == true) {
        var btnCancel = document.getElementById('<%=btnClear.ClientID%>')
         btnCancel.value = "Processing...";
         btnCancel.disabled = true;
         var ErrorMessage = document.getElementById('<%=lblMessageForQueue.ClientID%>')
            ErrorMessage.value = "Financial transaction(s) is in process, please do not refresh the page";
            ErrorMessage.style.display = 'inherit';
            var a = btnCancel.name;
            __doPostBack(a, '');
            return true;
        }
        else {
            var ErrorMessage2 = document.getElementById('<%=lblMessageForQueue.ClientID%>')
            ErrorMessage2.value = "";
            ErrorMessage2.style.display = 'none';
            return false;
        }

        }
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }

    $(document).ready(function () {

        // Dialog
        $('#urldialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#loading').hide();



            },
            resizable: false
        });
        // Summary Dialog
        $('#sumdialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#sumdialog').hide();
                     //      window.location.reload();
                //                window.location.reload();sa
            },
            resizable: false
        });
        $('#sumdialog1').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                //  $('#sumdialog1').hide();
                //     window.location.reload();
                //                window.location.reload();sa
            },

            resizable: false
        });



    });
    function showurldialog(title, url, modal, width, height) {
        //  alert(modal);
        $('#urldialog').dialog("option", "title", title);
        $('#urldialog').dialog("option", "modal", modal);
        $('#urldialog').dialog("option", "width", width);
        $('#urldialog').dialog("option", "height", height);
        $('#urldialog').dialog('open');
        $('#dialogiframe').hide();
        $('#loading').show();
        $('#dialogiframe').attr("src", url);
        $('#dialogiframe').load(function () {
            $('#loading').hide();
            $('#dialogiframe').show();
        });
        return false;

    };

    function CloseIframe() {
        $('#urldialog').dialog('close');
    }
    function showsumdialog1(title, modal, width, height) {
        //  alert(modal);
        $('#sumdialog1').dialog("option", "title", title);
        $('#sumdialog1').dialog("option", "modal", modal);
        $('#sumdialog1').dialog("option", "width", width);
        $('#sumdialog1').dialog("option", "height", height);
        $('#sumdialog1').dialog('open');
        $('#sumdialog1').dialog();
        return false;

    };
    function showsumdialog(title, modal, width, height) {
        //  alert(modal);
        $('#sumdialog').dialog("option", "title", title);
        $('#sumdialog').dialog("option", "modal", modal);
        $('#sumdialog').dialog("option", "width", width);
        $('#sumdialog').dialog("option", "height", height);
        $('#sumdialog').dialog('open');
        $('#sumdialog').dialog();
        return false;

    };
    function CloseSummaryDialogBox() {
        $('#sumdialog1').dialog('close');
    }
   
</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:TextBoxSetting BehaviorID="radInstructionNo" Validation-IsRequired="true"
        ErrorMessage="Invalid Instruction No." EmptyMessage="Enter Instruction No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstructionNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radInstrumentNo" Validation-IsRequired="true"
        EmptyMessage="Enter Instrument No." ErrorMessage="Invalid Instrument No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstrumentNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radBeneAccountNo" Validation-IsRequired="true"
        EmptyMessage="Enter Benificiary Account No" ErrorMessage="Invalid Benificiary Account No">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneAccountNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radBeneAccounTitle" Validation-IsRequired="true"
        EmptyMessage="Enter Beneficiary Account Title" ErrorMessage="Invalid Account Title">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneAccounTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue">Bulk Instruction Clearing</asp:Label>
            <br /><br />
                        <asp:Label ID="lblMessageForQueue" runat="server" Text="" CssClass="" ForeColor="Red" >
                        </asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
          <%--  <asp:Label ID="lblCompanyGroup" runat="server" Text="Select Product" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label5" runat="server" Text="*" ForeColor="Red"></asp:Label>--%>
            <asp:Label ID="lblTransferType" runat="server" Text="Select Transfer Type" 
                CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqTransferType" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyGroup0" runat="server" Text="Select File" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label6" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
           <%-- <asp:DropDownList ID="ddlProductType" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>--%>
            <asp:DropDownList ID="ddlTransferType" runat="server" CssClass="dropdown" 
                AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:FileUpload ID="fuBulkInnstructionClearing" runat="server" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <%--<asp:RequiredFieldValidator ID="rfvddlProductType" runat="server" ControlToValidate="ddlProductType"
                Display="Dynamic" ErrorMessage="Please select Product Type" InitialValue="0"
                SetFocusOnError="True" ValidationGroup="BulkClearing"></asp:RequiredFieldValidator>--%>
            <asp:RequiredFieldValidator ID="rfvddlTransferType" runat="server" ControlToValidate="ddlTransferType"
                Display="Dynamic" ErrorMessage="Please select Transfer Type" InitialValue="0"
                SetFocusOnError="True" ValidationGroup="BulkClearing"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvFile" runat="server" ControlToValidate="fuBulkInnstructionClearing"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select File" SetFocusOnError="True"
                ToolTip="Required Field" ValidationGroup="BulkClearing"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnProceed" runat="server" Text="Proceed" CssClass="btn" Width="71px"
                ValidationGroup="BulkClearing" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Back" CssClass="btnCancel"
                Width="75px" CausesValidation="False" />
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <asp:Panel ID="pnlBulkClearingInstructions" runat="server" Width="100%" ScrollBars="Vertical"
                Height="270px" Visible="false">
                <table style="width: 100%">
                    <tr>
                        <td style="width: 100%">
                            <telerik:RadGrid ID="gvBulkInstructions" runat="server" AllowPaging="false" AllowSorting="false"
                                AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100">
                                <AlternatingItemStyle CssClass="rgAltRow" />
                                <MasterTableView>
                                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="InstructionID" HeaderText="Instruction No." SortExpression="InstructionID">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="InstrumentNo" HeaderText="Instrument No" SortExpression="InstrumentNo">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="BeneAccountNumber" HeaderText="Beneficiary Account No"
                                            SortExpression="BeneAccountNumber">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="BeneAccountBranchCode" HeaderText="Beneficiary Account Branch Code"
                                            SortExpression="BeneAccountBranchCode">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="BeneAccountCurrency" HeaderText="Beneficiary Account Currency"
                                            SortExpression="BeneAccountCurrency">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="BeneAccountTitle" HeaderText="Beneficiary Account Title"
                                            SortExpression="BeneAccountTitle">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="FetchedAccountTitle" HeaderText="Fetched Account Title"
                                            SortExpression="FetchedAccountTitle">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TransferType" HeaderText="Transfer Type" SortExpression="TransferType">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Amount" HeaderText="Amount" SortExpression="Amount">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="InstructionAmount" HeaderText="Instruction Amount"
                                            SortExpression="InstructionAmount">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Account No." HeaderText="Account No." SortExpression="Account No.">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ClearingType" HeaderText="Clearing Type" SortExpression="ClearingType">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Remarks" HeaderText="Remarks" SortExpression="Remarks">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ItemStyle CssClass="rgRow" />
                            </telerik:RadGrid><br />
                            <telerik:RadGrid ID="gvBulkInstructionsForNIFT" runat="server" AllowPaging="false" AllowSorting="false" AutoGenerateColumns="False" CellSpacing="0" PageSize="100" ShowFooter="True">
                                <AlternatingItemStyle CssClass="rgAltRow" />
                                <MasterTableView DataKeyNames="NIFTCode,Amount,InstructionAmount,Remarks">
                                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="InstructionID" HeaderText="Instruction No." SortExpression="InstructionID">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="InstrumentNo" HeaderText="Instrument No" SortExpression="InstrumentNo">
                                        </telerik:GridBoundColumn>
                                         <telerik:GridBoundColumn DataField="InstructionDate" HeaderText="Instruction Date" SortExpression="InstructionDate"
                                             HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="TransferType" HeaderText="Transfer Type" SortExpression="TransferType">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Amount" HeaderText="Amount" SortExpression="Amount">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="InstructionAmount" HeaderText="Instruction Amount" SortExpression="InstructionAmount">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Account No." HeaderText="Account No." SortExpression="Account No.">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ClearingType" HeaderText="Clearing Type" SortExpression="ClearingType">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Remarks" HeaderText="Remarks" SortExpression="Remarks">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ItemStyle CssClass="rgRow" />
                            </telerik:RadGrid>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;<asp:Button ID="btnClear" runat="server" Text="Save" CssClass="btn" CausesValidation="False" ValidationGroup="Clear"
                Visible="False" OnClientClick="javascript: return conBukDelete();"/>
            &nbsp;&nbsp;
        </td>
    </tr>
</table>
<div id="sumdialog1" title="" style="text-align: center">
    <%-- <asp:Panel ID="pnlShowSummary1" runat="server" Style="display: none; width: 100%"
        ScrollBars="Vertical" Height="200px">--%>
    <table id="pnlShowSummary1" runat="server" style="display: none">
        <tr>
            <td align="left" valign="top">
                <telerik:RadGrid ID="gvChequeReturned" runat="server" CssClass="RadGrid" AllowSorting="false" Width="600px"
                    AllowPaging="false" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                    PageSize="100">
                    <%--<ClientSettings>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>--%>
                    <MasterTableView DataKeyNames="IsChequeReturned,InstructionID">
                        <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                        <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                        </ExpandCollapseColumn>
                        <Columns>
                          <%-- <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox"  Text=""
                                TextAlign="Right" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox"  Text="" Enabled="false" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>--%>
                    <telerik:GridBoundColumn DataField="InstructionID" HeaderText="Instruction No." SortExpression="InstructionID">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                    <%-- <telerik:GridBoundColumn DataField="ReferenceNo" HeaderText="Reference No." SortExpression="ReferenceNo">
                                    </telerik:GridBoundColumn>--%>
                    <telerik:GridBoundColumn DataField="Message" HeaderText="Message" SortExpression="Message">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                        </EditFormSettings>
                        <PagerStyle AlwaysVisible="True" />
                    </MasterTableView>
                    <HeaderStyle CssClass="GridHeader" />
                    <ItemStyle CssClass="GridItem" />
                    <AlternatingItemStyle CssClass="GridItem" />
                    <PagerStyle AlwaysVisible="True" />
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                </telerik:RadGrid>
            </td>
        </tr>
        <tr>
            <td align="center" valign="top">
                <%--<asp:Button ID="btnChequeReturned" runat="server" Text="Cheque Returned" CssClass="btn" />--%>
                <asp:Button ID="btnCancelGrid2" runat="server" Text="Close" CssClass="btnCancel" OnClientClick="CloseSummaryDialogBox();return false;"/>
            </td>
        </tr>
    </table>
    <%--<ClientSettings>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>--%>
</div>
<div id="sumdialog" title="" style="text-align: center">
    <asp:Panel ID="pnlShowSummary" runat="server" Style="display: none; width: 100%"
        ScrollBars="Vertical" Height="600px">
        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnCancel" OnClientClick="CloseSummaryDialogBox();return false;" /><br />
        <telerik:RadGrid ID="gvShowSummary" runat="server" AllowPaging="false" Width="100%"
            CssClass="RadGrid" AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0"
            ShowFooter="True" PageSize="100">
            <AlternatingItemStyle CssClass="rgAltRow" />
            <MasterTableView DataKeyNames="IsChequeReturned">
                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                </RowIndicatorColumn>
                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                </ExpandCollapseColumn>
                <Columns>
                  <%--  <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Left">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox"  Text=""
                                TextAlign="Right" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox"  Text="" Enabled="false" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>--%>
                    <telerik:GridBoundColumn DataField="InstructionID" HeaderText="Instruction No." SortExpression="InstructionID">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                    <%-- <telerik:GridBoundColumn DataField="ReferenceNo" HeaderText="Reference No." SortExpression="ReferenceNo">
                                    </telerik:GridBoundColumn>--%>
                    <telerik:GridBoundColumn DataField="Message" HeaderText="Message" SortExpression="Message">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
                <PagerStyle AlwaysVisible="True" />
            </MasterTableView>
            <ItemStyle CssClass="rgRow" />
            <PagerStyle AlwaysVisible="True" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
    </asp:Panel>
</div>
<div id="urldialog" title="">
    <iframe id='dialogiframe' frameborder="0" allowtransparency="true" src="" style="border-style: none;
        width: 100%; height: 100%; overflow-x: hidden; background-color: transparent;">
    </iframe>
    <div id='loading' style="display: none; margin: 0px auto; text-align: center; width: 100%;
        height: 100%; position: relative">
        <img src='../../../../images/progressbar.gif' style="left: 45%; top: 50%; position: absolute;" />
    </div>
</div>
