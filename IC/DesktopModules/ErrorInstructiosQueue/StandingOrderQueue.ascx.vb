﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports EntitySpaces.Core
Partial Class DesktopModules_StandingOrderQueue_StandingOrderQueue
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrICAssignUserRolsID As New ArrayList
    Private ArrICAssignedOfficeID As New ArrayList
    Private ArrICAssignedStatus As New ArrayList
    Private ArrICAssignedPaymentModes As New ArrayList
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()

            If Page.IsPostBack = False Then

                'btnRetry.Attributes.Item("onclick") = "this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnRetry, Nothing).ToString()
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                SetArraListAssignedPaymentModes()
                'SetArraListAssignedOfficeIDs()
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
                LoadddlGroup()
                LoadddlCompany()
                LoadddlAccountPaymentNatureByUserID()
                LoadddlProductTypeByAccountPaymentNature()
                LoadddlStatus()
                ClearAllTextBoxes()
                FillDesignedDtByAPNatureByAssignedRole()
                Dim dtPageLoad As New DataTable
                dtPageLoad = DirectCast(ViewState("AccountNumber"), DataTable)
                If dtPageLoad.Rows.Count > 0 Then
                    LoadgvAccountPaymentNatureProductType(True)
                Else
                    UIUtilities.ShowDialog(Me, "Error", "You do not have access to any transactional data. Please contact Al Baraka Administrator.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Error Queue")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Error Queue") = True Then
                    If Not ArrICAssignUserRolsID.Contains(RoleID.ToString) Then
                        ArrICAssignUserRolsID.Add(RoleID.ToString)
                    End If
                End If
            Next
        End If
    End Sub
    Private Function PerformFirstLegReversal(ByVal RelatedID As String) As Boolean
        Try
            Dim objICFLRev As New ICFirstLegReversedInstructions
            Dim FromAccountType As CBUtilities.AccountType
            Dim ToAccountType As CBUtilities.AccountType
            Dim LastStatus As String = ""
            Dim StanNo As String = ""
            If objICFLREv.LoadByPrimaryKey(RelatedID) Then
                If objICFLRev.Status.ToString = "37" And objICFLRev.EntityType.ToString = "Instruction" Then

                    Try
                       
                        If objICFLRev.ToAccountType.ToString = 2 Then
                            ToAccountType = CBUtilities.AccountType.RB
                        ElseIf objICFLRev.ToAccountType = 3 Then
                            ToAccountType = CBUtilities.AccountType.GL
                        End If
                        If objICFLRev.FromAccountType.ToString = 2 Then
                            FromAccountType = CBUtilities.AccountType.RB
                        ElseIf objICFLRev.FromAccountType = 3 Then
                            FromAccountType = CBUtilities.AccountType.GL
                        End If
                        LastStatus = objICFLRev.LastStatus
                        ICFirstLegReversedInstructionsController.UpDateFirstLegReversedInstructionsStatus(objICFLRev.FirstLegReversedID.ToString, objICFLRev.Status.ToString, "39", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                        If CBUtilities.DoTransaction(StanNo, objICFLRev.RelatedID, objICFLRev.FromAccountBranchCode, objICFLRev.FromAccountProductCode, objICFLRev.FromAccountSchemeCode, objICFLRev.FromAccountNo, objICFLRev.RBIndicator, objICFLRev.ToAccountBranchCode, objICFLRev.ToAccountProductCode, objICFLRev.ToAccountSchemeCode, objICFLRev.ToAccountNo, objICFLRev.TxnType, objICFLRev.VoucherType, objICFLRev.InstrumentType, objICFLRev.DebitGLCode, objICFLRev.CreditGLCode, objICFLRev.InstrumentNo, objICFLRev.Amount, objICFLRev.RelatedID, objICFLRev.IsReversal, Nothing, objICFLRev.EntityType, FromAccountType, ToAccountType, objICFLRev.TransactionBranchCode) = True Then
                            objICFLRev = New ICFirstLegReversedInstructions
                            objICFLRev.LoadByPrimaryKey(RelatedID)
                            ICFirstLegReversedInstructionsController.UpDateFirstLegReversedInstructionsStatus(objICFLRev.FirstLegReversedID.ToString, objICFLRev.Status.ToString, objICFLRev.LastStatus.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                            Return True
                        Else
                            objICFLRev = New ICFirstLegReversedInstructions
                            objICFLRev.LoadByPrimaryKey(RelatedID)
                            ICFirstLegReversedInstructionsController.UpDateFirstLegReversedInstructionsStatus(objICFLRev.FirstLegReversedID.ToString, objICFLRev.Status.ToString, objICFLRev.LastStatus.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                            objICFLRev = New ICFirstLegReversedInstructions
                            objICFLRev.FirstLegReversedID = RelatedID
                            objICFLRev.LastStatus = LastStatus
                            ICFirstLegReversedInstructionsController.AddICFirstLegRevInstruction(objICFLRev, True, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                            Return False
                        End If
                    Catch ex As Exception
                        objICFLRev = New ICFirstLegReversedInstructions
                        objICFLRev.LoadByPrimaryKey(RelatedID)
                        ICFirstLegReversedInstructionsController.UpDateFirstLegReversedInstructionsStatus(objICFLRev.FirstLegReversedID.ToString, objICFLRev.Status.ToString, objICFLRev.LastStatus.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                        objICFLRev = New ICFirstLegReversedInstructions
                        objICFLRev.LoadByPrimaryKey(RelatedID)
                        objICFLRev.FirstLegReversedID = RelatedID
                        objICFLRev.LastStatus = LastStatus
                        objICFLRev.ErrorMessage = ex.Message.ToString
                        ICFirstLegReversedInstructionsController.AddICFirstLegRevInstruction(objICFLRev, True, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                        Return False
                    End Try
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub SetArraListAssignedOfficeIDs()
        Dim dt As New DataTable
        Dim objICUser As New ICUser
        ArrICAssignedOfficeID.Clear()
        dt = ICUserRolesPaymentNatureController.GetAllTaggedLocationsWithUserByUSerIDAndRoleID(Me.UserId.ToString, ArrICAssignUserRolsID)
        If dt.Rows.Count > 0 Then
            objICUser.LoadByPrimaryKey(Me.UserId)
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("OfficeID").ToString
                ArrICAssignedOfficeID.Add(RoleID.ToString)
            Next
            ArrICAssignedOfficeID.Add(objICUser.OfficeCode)
        End If
    End Sub
    Private Sub SetArraListAssignedStatus()

        ArrICAssignedStatus.Clear()
        If ddlStatus.SelectedValue.ToString <> "0" Then
            ArrICAssignedStatus.Add(ddlStatus.SelectedValue.ToString)
        Else
            ArrICAssignedStatus.Add(37)
            ArrICAssignedStatus.Add(39)
        End If
    End Sub
    Private Sub SetArraListAssignedPaymentModes()
        ArrICAssignedPaymentModes.Clear()
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, ddlGroup.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlCompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlCompany.Items.Clear()
                    ddlCompany.Items.Add(lit)
                    lit.Selected = True
                    ddlCompany.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
                ddlCompany.DataSource = dt
                ddlCompany.DataTextField = "CompanyName"
                ddlCompany.DataValueField = "CompanyCode"
                ddlCompany.DataBind()
                ddlCompany.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedGroupsByUserID(Me.UserId.ToString, ArrICAssignUserRolsID)
            ddlGroup.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("GroupCode")
                    lit.Text = dr("GroupName")
                    ddlGroup.Items.Clear()
                    ddlGroup.Items.Add(lit)
                    lit.Selected = True
                    ddlGroup.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlGroup.Items.Clear()
                ddlGroup.Items.Add(lit)
                ddlGroup.AppendDataBoundItems = True
                ddlGroup.DataSource = dt
                ddlGroup.DataTextField = "GroupName"
                ddlGroup.DataValueField = "GroupCode"
                ddlGroup.DataBind()
                ddlGroup.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlGroup.Items.Clear()
                ddlGroup.Items.Add(lit)
                ddlGroup.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetViewStateDtOnAPNatureIndexChnage()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim AccountNumber, BranchCode, Currency, PaymentNatureCode As String
        Dim ArrayListOfficeID As New ArrayList
        Dim drAccountNo As DataRow
        AccountNumber = Nothing
        BranchCode = Nothing
        Currency = Nothing
        PaymentNatureCode = Nothing
        dtAccountNumber = ViewState("AccountNumber")
        ViewState("AccountNumber") = Nothing
        AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0)
        BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1)
        Currency = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2)
        PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)
        For Each dr As DataRow In dtAccountNumber.Rows
            If dr("AccountNumber").ToString.Split("-")(0) = AccountNumber And dr("AccountNumber").ToString.Split("-")(1) = BranchCode And dr("AccountNumber").ToString.Split("-")(2) = Currency And dr("PaymentNature").ToString = PaymentNatureCode Then
                ArrayListOfficeID.Add(dr("OfficeID").ToString)
            End If
        Next
        dtAccountNumber.Rows.Clear()
        If ArrayListOfficeID.Count > 0 Then
            For Each OfficeID In ArrayListOfficeID
                drAccountNo = dtAccountNumber.NewRow()
                drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
                drAccountNo("PaymentNature") = PaymentNatureCode
                drAccountNo("OfficeID") = OfficeID
                dtAccountNumber.Rows.Add(drAccountNo)
            Next
        Else
            drAccountNo = dtAccountNumber.NewRow()
            drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
            drAccountNo("PaymentNature") = PaymentNatureCode
            drAccountNo("OfficeID") = "0"
            dtAccountNumber.Rows.Add(drAccountNo)
        End If
        'dtAccountNumber.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAccountNumber.Rows
            dr("DropDown") = "True"
        Next
        ViewState("AccountNumber") = dtAccountNumber
    End Sub
    Private Sub LoadddlAccountPaymentNatureByUserID()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlAcccountPaymentNature.Items.Clear()
            ddlAcccountPaymentNature.Items.Add(lit)
            lit.Selected = True
            ddlAcccountPaymentNature.AppendDataBoundItems = True
            ddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForDropDown(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
            ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
            ddlAcccountPaymentNature.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlStatus()
        Try
            Dim lit As ListItem
            Dim dt As New DataTable
            lit = New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlStatus.Items.Clear()
            ddlStatus.Items.Add(lit)
            ddlStatus.AppendDataBoundItems = True
            dt = ICInstructionStatusController.GetAllDistinctStatus()
            For Each dr As DataRow In dt.Rows
                If dr("StatusName") = "Pending CB" Or dr("StatusName") = "Error" Then
                    lit = New ListItem
                    lit.Value = dr("StatusID")
                    lit.Text = dr("StatusName")
                    ddlStatus.DataTextField = "StatusName"
                     ddlStatus.Items.Add(lit)
                End If
            Next
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlProductTypeByAccountPaymentNature()
        Try
            Try
                Dim lit As New ListItem
                Dim objICAPNature As New ICAccountsPaymentNature
                objICAPNature.es.Connection.CommandTimeout = 3600
                ddlProductType.Items.Clear()
                lit.Value = "0"
                lit.Text = "-- All --"
                ddlProductType.Items.Add(lit)
                ddlProductType.AppendDataBoundItems = True
                If ddlAcccountPaymentNature.SelectedValue.ToString <> "0" Then
                    If objICAPNature.LoadByPrimaryKey(ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)) Then
                        ddlProductType.DataSource = ICAccountPaymentNatureProductTypeController.GetAllProductTypesByAccountAndPaymentNature(objICAPNature)
                        ddlProductType.DataTextField = "ProductTypeName"
                        ddlProductType.DataValueField = "ProductTypeCode"
                        ddlProductType.DataBind()
                    End If
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadgvAccountPaymentNatureProductType(ByVal DoDataBind As Boolean)
        Try
           
            Dim AccountNumber, BranchCode, Currency, PaymentNatureCode, GroupCode, CompanyCode, ProductTypeCode, InstrumentNo, InstructionNo, Status As String
            Dim FromDate, ToDate As String
            Dim Amount As Double = 0
            AccountNumber = ""
            BranchCode = ""
            Currency = ""
            Currency = ""
            PaymentNatureCode = ""
            CompanyCode = "0"
            Status = "0"
            ProductTypeCode = ""
            InstructionNo = ""
            InstrumentNo = ""
            GroupCode = ""
            FromDate = Nothing
            ToDate = Nothing
            Dim dtAccountNumber As New DataTable
            dtAccountNumber = ViewState("AccountNumber")
            If ddlGroup.SelectedValue.ToString <> "0" Then
                GroupCode = ddlGroup.SelectedValue.ToString
            End If
            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            End If
            If ddlProductType.SelectedValue.ToString <> "0" Then
                ProductTypeCode = ddlProductType.SelectedValue.ToString
            End If
            If txtInstrumentNo.Text.ToString <> "" And txtInstrumentNo.Text.ToString <> "Enter Instrument No." Then
                InstrumentNo = txtInstrumentNo.Text.ToString
            End If
            If txtInstructionNo.Text.ToString <> "" And txtInstructionNo.Text.ToString <> "Enter Instruction No." Then
                InstructionNo = txtInstructionNo.Text.ToString
            End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                FromDate = raddpCreationFromDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If Not raddpCreationToDate.SelectedDate Is Nothing Then
                ToDate = raddpCreationToDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            SetArraListAssignedStatus()
            GetAllInstructionsForRadGridSecondForInstructionTrackingAndErrorQueueForRevertFirstLeg("Creation Date", FromDate, ToDate, dtAccountNumber, ProductTypeCode, InstructionNo, InstrumentNo, Me.gvAuthorization.CurrentPageIndex + 1, Me.gvAuthorization.PageSize, Me.gvAuthorization, DoDataBind, Me.UserId.ToString, Amount, "=", CompanyCode, GroupCode, ArrICAssignedStatus, "0")
            If gvAuthorization.Items.Count > 0 Then
                gvAuthorization.Visible = True
                lblNRF.Visible = False
                gvAuthorization.Columns(10).Visible = CBool(htRights("Retry"))
                btnCancelInst.Visible = CBool(htRights("Retry"))
                'SetJavaScript()
            Else
                lblNRF.Visible = True
                gvAuthorization.Visible = False
                btnCancelInst.Visible = False
                'btnRetry.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
  

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            'SetArraListRoleID()
            LoadddlAccountPaymentNatureByUserID()
            LoadddlProductTypeByAccountPaymentNature()
            LoadddlStatus()
            LoadgvAccountPaymentNatureProductType(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub FillDesignedDtByAPNatureByAssignedRole()
        Dim dtAssignedAPNatureOfficeID As New DataTable
        dtAssignedAPNatureOfficeID.Rows.Clear()
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserSecond(Me.UserId.ToString, ArrICAssignUserRolsID)
        dtAssignedAPNatureOfficeID.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAssignedAPNatureOfficeID.Rows
            dr("DropDown") = "False"
        Next
        ViewState("AccountNumber") = Nothing
        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID

    End Sub
    Protected Sub ddlAcccountPaymentNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcccountPaymentNature.SelectedIndexChanged
        Try
            If ddlAcccountPaymentNature.SelectedValue.ToString = "0" Then
                SetArraListRoleID()
                LoadddlProductTypeByAccountPaymentNature()
                FillDesignedDtByAPNatureByAssignedRole()
                LoadddlStatus()
                LoadgvAccountPaymentNatureProductType(True)
            Else
                FillDesignedDtByAPNatureByAssignedRole()
                LoadddlProductTypeByAccountPaymentNature()
                SetViewStateDtOnAPNatureIndexChnage()
                LoadddlStatus()
                LoadgvAccountPaymentNatureProductType(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged
        Try
            LoadddlStatus()
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ClearAllTextBoxes()

        txtInstructionNo.Text = ""
        txtInstrumentNo.Text = ""
    
    End Sub
    Private Function CheckgvInstructionAuthentication() As Boolean
        Try

            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVInstruction In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVInstruction.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub gvAuthorization_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles gvAuthorization.ItemCommand
        Try
            Try
                If CBUtilities.GetEODStatus() = "01" Then
                    UIUtilities.ShowDialog(Me, "Error", "EOD Status : " & CBUtilities.GetErrorMessageForTransactionFromResponseCode("01", "EOD"), Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                UIUtilities.ShowDialog(Me, "Error", "EOD Status : " & ex.Message.ToString, Dialogmessagetype.Failure)
                Exit Sub
            End Try
            Dim objICFLRevcoll As New ICFirstLegReversedInstructionsCollection
            Dim objICFLRev As ICFirstLegReversedInstructions
            Dim objICInstruction As New ICInstruction
            Dim FromAccountType As CBUtilities.AccountType
            Dim ToAccountType As CBUtilities.AccountType
            If e.CommandName = "Revert" Then
                objICFLRevcoll.Query.Where(objICFLRevcoll.Query.RelatedID = e.CommandArgument.ToString)
                objICFLRevcoll.Query.OrderBy(objICFLRevcoll.Query.FirstLegReversedID.Ascending)
                If objICFLRevcoll.Query.Load Then
                    If objICFLRevcoll.Count > 0 Then
                        For Each objICFLRevForProcess As ICFirstLegReversedInstructions In objICFLRevcoll
                            objICFLRev = New ICFirstLegReversedInstructions
                            If objICFLRev.LoadByPrimaryKey(objICFLRevForProcess.FirstLegReversedID) = True Then
                                If objICFLRev.EntityType = "Instruction" And objICFLRev.Status.ToString = "37" And objICFLRev.ReversalType Is Nothing Then
                                    If PerformFirstLegReversal(objICFLRevForProcess.FirstLegReversedID) = True Then
                                        objICInstruction.LoadByPrimaryKey(objICFLRev.RelatedID)
                                        'ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, objICInstruction.LastStatus.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "", "REVERT TO PREVIOUS STATE")
                                        ICFirstLegReversedInstructionsController.DeleteFirstLegReversedInstruction(objICFLRev.FirstLegReversedID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username)
                                        'LoadgvAccountPaymentNatureProductType(True)
                                        'UIUtilities.ShowDialog(Me, "Error Queue", "Instruction processed successfully.", Dialogmessagetype.Success)
                                        'Exit Sub
                                    End If
                                ElseIf objICFLRev.EntityType = "Instruction" And objICFLRev.Status.ToString = "37" And objICFLRev.ReversalType = "Complete" Then

                                    If objICFLRev.ToAccountType.ToString = 2 Then
                                        ToAccountType = CBUtilities.AccountType.RB
                                    ElseIf objICFLRev.ToAccountType = 3 Then
                                        ToAccountType = CBUtilities.AccountType.GL
                                    End If
                                    If objICFLRev.FromAccountType.ToString = 2 Then
                                        FromAccountType = CBUtilities.AccountType.RB
                                    ElseIf objICFLRev.FromAccountType = 3 Then
                                        FromAccountType = CBUtilities.AccountType.GL
                                    End If
                                    If CBUtilities.FundsTransfer(Nothing, objICFLRev.FromAccountNo, objICFLRev.FromAccountBranchCode, objICFLRev.FromAccountCurrency, "", "", "", FromAccountType, objICFLRev.ToAccountNo, objICFLRev.ToAccountBranchCode, objICFLRev.ToAccountCurrency, "", "", "", ToAccountType, objICFLRev.Amount, objICFLRev.EntityType, objICFLRev.RelatedID, objICFLRev.LastStatus, "37", Me.UserInfo.UserID.ToString, Nothing, objICFLRev.ProductTypeCode, Nothing, Nothing, "Reversal", Nothing) = True Then
                                        objICInstruction.LoadByPrimaryKey(objICFLRev.RelatedID)
                                        ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, objICFLRev.LastStatus, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "", "REVERT TO PREVIOUS STATE")
                                        ICFirstLegReversedInstructionsController.DeleteFirstLegReversedInstruction(objICFLRev.FirstLegReversedID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username)
                                        LoadgvAccountPaymentNatureProductType(True)
                                        UIUtilities.ShowDialog(Me, "Error Queue", "Instruction processed successfully.", Dialogmessagetype.Success)
                                        Exit Sub
                                    Else
                                        LoadgvAccountPaymentNatureProductType(True)
                                        UIUtilities.ShowDialog(Me, "Error Queue", "Instruction not processed successfully.", Dialogmessagetype.Failure)
                                        Exit Sub
                                    End If
                                ElseIf objICFLRev.Status.ToString = "52" Then

                                    If objICFLRev.ToAccountType.ToString = 2 Then
                                        ToAccountType = CBUtilities.AccountType.RB
                                    ElseIf objICFLRev.ToAccountType = 3 Then
                                        ToAccountType = CBUtilities.AccountType.GL
                                    End If
                                    If objICFLRev.FromAccountType.ToString = 2 Then
                                        FromAccountType = CBUtilities.AccountType.RB
                                    ElseIf objICFLRev.FromAccountType = 3 Then
                                        FromAccountType = CBUtilities.AccountType.GL
                                    End If
                                    If CBUtilities.FundsTransfer(Nothing, objICFLRev.FromAccountNo, objICFLRev.FromAccountBranchCode, objICFLRev.FromAccountCurrency, "", "", "", FromAccountType, objICFLRev.ToAccountNo, objICFLRev.ToAccountBranchCode, objICFLRev.ToAccountCurrency, "", "", "", ToAccountType, objICFLRev.Amount, objICFLRev.EntityType, objICFLRev.RelatedID, objICFLRev.LastStatus, "37", Me.UserInfo.UserID.ToString, Nothing, objICFLRev.ProductTypeCode, Nothing, Nothing, "Reversal", Nothing) = True Then
                                        objICInstruction.LoadByPrimaryKey(objICFLRev.RelatedID)
                                        ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, objICFLRev.LastStatus, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "", "REVERT TO PREVIOUS STATE")
                                        ICFirstLegReversedInstructionsController.DeleteFirstLegReversedInstruction(objICFLRev.FirstLegReversedID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username)
                                        LoadgvAccountPaymentNatureProductType(True)
                                        UIUtilities.ShowDialog(Me, "Error Queue", "Instruction processed successfully.", Dialogmessagetype.Success)
                                        Exit Sub
                                    Else
                                        LoadgvAccountPaymentNatureProductType(True)
                                        UIUtilities.ShowDialog(Me, "Error Queue", "Instruction not processed successfully.", Dialogmessagetype.Failure)
                                        Exit Sub
                                    End If
                                Else
                                    UIUtilities.ShowDialog(Me, "Error", "Invalid selected entity type", Dialogmessagetype.Failure)
                                    LoadgvAccountPaymentNatureProductType(True)
                                    Exit Sub
                                End If
                            Else
                                UIUtilities.ShowDialog(Me, "Error", "Invalid selected entity type", Dialogmessagetype.Failure)
                                LoadgvAccountPaymentNatureProductType(True)
                                Exit Sub
                            End If

                        Next
                    Else
                        For Each objICFLRevForProcess As ICFirstLegReversedInstructions In objICFLRevcoll
                            objICFLRev = New ICFirstLegReversedInstructions
                            If objICFLRev.LoadByPrimaryKey(objICFLRevForProcess.FirstLegReversedID) = True Then
                                If objICFLRev.EntityType = "Instruction" And objICFLRev.Status.ToString = "37" And objICFLRev.ReversalType Is Nothing Then
                                    If PerformFirstLegReversal(objICFLRevForProcess.FirstLegReversedID) = True Then
                                        objICInstruction.LoadByPrimaryKey(objICFLRev.RelatedID)
                                        ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, objICFLRev.LastStatus.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "", "REVERT TO PREVIOUS STATE")
                                        ICFirstLegReversedInstructionsController.DeleteFirstLegReversedInstruction(objICFLRev.FirstLegReversedID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username)
                                        LoadgvAccountPaymentNatureProductType(True)
                                        UIUtilities.ShowDialog(Me, "Error Queue", "Instruction processed successfully.", Dialogmessagetype.Success)
                                        Exit Sub
                                    End If
                                ElseIf objICFLRev.EntityType = "Instruction" And objICFLRev.Status.ToString = "37" And objICFLRev.ReversalType = "Complete" Then

                                    If objICFLRev.ToAccountType.ToString = 2 Then
                                        ToAccountType = CBUtilities.AccountType.RB
                                    ElseIf objICFLRev.ToAccountType = 3 Then
                                        ToAccountType = CBUtilities.AccountType.GL
                                    End If
                                    If objICFLRev.FromAccountType.ToString = 2 Then
                                        FromAccountType = CBUtilities.AccountType.RB
                                    ElseIf objICFLRev.FromAccountType = 3 Then
                                        FromAccountType = CBUtilities.AccountType.GL
                                    End If
                                    If CBUtilities.FundsTransfer(Nothing, objICFLRev.FromAccountNo, objICFLRev.FromAccountBranchCode, objICFLRev.FromAccountCurrency, "", "", "", FromAccountType, objICFLRev.ToAccountNo, objICFLRev.ToAccountBranchCode, objICFLRev.ToAccountCurrency, "", "", "", ToAccountType, objICFLRev.Amount, objICFLRev.EntityType, objICFLRev.RelatedID, objICFLRev.LastStatus, "37", Me.UserInfo.UserID.ToString, Nothing, objICFLRev.ProductTypeCode, Nothing, Nothing, "Reversal", Nothing) = True Then
                                        objICInstruction.LoadByPrimaryKey(objICFLRev.RelatedID)
                                        ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, objICFLRev.LastStatus.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "", "REVERT TO PREVIOUS STATE")
                                        ICFirstLegReversedInstructionsController.DeleteFirstLegReversedInstruction(objICFLRev.FirstLegReversedID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username)
                                        LoadgvAccountPaymentNatureProductType(True)
                                        UIUtilities.ShowDialog(Me, "Error Queue", "Instruction processed successfully.", Dialogmessagetype.Success)
                                        Exit Sub
                                    Else
                                        LoadgvAccountPaymentNatureProductType(True)
                                        UIUtilities.ShowDialog(Me, "Error Queue", "Instruction not processed successfully.", Dialogmessagetype.Failure)
                                        Exit Sub
                                    End If
                                End If
                            End If
                        Next
                    End If
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            LoadgvAccountPaymentNatureProductType(True)
            UIUtilities.ShowDialog(Me, "Error Queue", "Instruction not processed successfully.", Dialogmessagetype.Failure)
            Exit Sub
        End Try
    End Sub
    Private Function GetFirstLegReversedSingleObjectByRelatedID(ByVal RelatedID As String) As ICFirstLegReversedInstructions
        Dim objICFLREV As New ICFirstLegReversedInstructions
        objICFLREV.Query.Where(objICFLREV.Query.RelatedID = RelatedID)
        objICFLREV.Query.Load()
        Return objICFLREV
    End Function
    Protected Sub gvAuthorization_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAuthorization.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAuthorization_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemDataBound
        Dim chkProcessAll As CheckBox
        Dim chkSelect As CheckBox
        Dim objICFLVReversedcoll As ICFirstLegReversedInstructionsCollection
        Dim objICFLVReversed As ICFirstLegReversedInstructions
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','0');"
        End If
        Dim imgBtn As LinkButton
        Dim AppPath As String = "" ' = DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            objICFLVReversed = New ICFirstLegReversedInstructions
            objICFLVReversedcoll = New ICFirstLegReversedInstructionsCollection
            Dim btnRetry As Button
            imgBtn = New LinkButton
            imgBtn = DirectCast(item.Cells(1).FindControl("lbInstructionID"), LinkButton)
            imgBtn.OnClientClick = "showurldialog('Instruction Details',' " & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & item.GetDataKeyValue("InstructionID").ToString().EncryptString() & "' ,true,700,650);return false;"
            imgBtn.Enabled = True
            btnRetry = New Button
            btnRetry = DirectCast(item.Cells(9).FindControl("btnRetry"), Button)
            btnRetry.Visible = True
            objICFLVReversedcoll.Query.Where(objICFLVReversedcoll.Query.RelatedID = item.GetDataKeyValue("InstructionID"))
            objICFLVReversedcoll.Query.OrderBy(objICFLVReversedcoll.Query.FirstLegReversedID.Ascending)
            objICFLVReversedcoll.Query.Load()
            If objICFLVReversedcoll.Query.Load = True Then
                objICFLVReversed = objICFLVReversedcoll(0)
                If objICFLVReversed.IsFirstLEgReversed = True Then
                    btnRetry.Visible = True
                Else
                    btnRetry.Visible = False

                End If
            Else
                btnRetry.Visible = False
            End If
            'If item.GetDataKeyValue("IsFirstLEgReversed") = 1 Then
            '    btnRetry.Visible = True
            'Else
            '    btnRetry.Visible = False
            'End If
            If item.GetDataKeyValue("IsAmendmentComplete") = True Then
                gvAuthorization.AlternatingItemStyle.CssClass = "GridItemComplete"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItemComplete"
            Else
                gvAuthorization.AlternatingItemStyle.CssClass = "GridItem"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItem"
            End If
            chkSelect = New CheckBox
            chkSelect = DirectCast(item.Cells(0).FindControl("chkSelect"), CheckBox)
            chkSelect.Enabled = False
            chkSelect.Visible = False
            If item.GetDataKeyValue("status").ToString = "39" Then
                chkSelect.Enabled = True
                chkSelect.Visible = True
            End If

        End If
    End Sub

    'Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
    '    Try
    '        LoadgvAccountPaymentNatureProductType(True)
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    Private Function CheckgvClientRoleCheckedForProcessAll() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True And chkSelect.Enabled = True And chkSelect.Visible = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    

    Protected Sub gvAuthorization_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAuthorization.NeedDataSource
        Try
            LoadgvAccountPaymentNatureProductType(False)
            'SetJavaScript()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL(41))
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click

        Try
                LoadgvAccountPaymentNatureProductType(True)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
            LoadddlAccountPaymentNatureByUserID()
            LoadddlProductTypeByAccountPaymentNature()
            FillDesignedDtByAPNatureByAssignedRole()
            LoadddlStatus()
            LoadgvAccountPaymentNatureProductType(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    

    Protected Sub raddpCreationFromDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationFromDate.SelectedDateChanged
        Try
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    
                    UIUtilities.ShowDialog(Me, "Error", "From date should be less than to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub raddpCreationToDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationToDate.SelectedDateChanged
        Try
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then

                    UIUtilities.ShowDialog(Me, "Error", "From date should be less than to date.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub

                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        Try
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Public Shared Sub GetAllInstructionsForRadGridSecondForInstructionTrackingAndErrorQueueForRevertFirstLeg(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal UsersID As String, ByVal Amount As Double, ByVal AmountOperator As String, ByVal CompanyCode As String, ByVal GroupCode As String, ByVal Status As ArrayList, ByVal FileBatchNo As String)
        Dim dt As DataTable
        Dim params As New EntitySpaces.Interfaces.esParameters
        Dim StrQuery As String = Nothing
        Dim WhereClasue As String = Nothing
        WhereClasue = " Where "
        StrQuery = "Select InstructionID,convert(date,IC_Instruction.CreateDate) as CreateDate,convert(date,ValueDate) as ValueDate,IC_Instruction.Amount,StatusName, "
        StrQuery += "isnull(BeneficiaryName,'-') as BeneficiaryName,isnull(BeneficiaryAddress,'-') as BeneficiaryAddress,PaymentMode,ISNULL(IC_Instruction.InstrumentNo,'-') as InstrumentNo,"
        StrQuery += "ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,ISNULL(IC_Office.OfficeCode + '-' + IC_Office.OfficeName,'-') as OfficeName,"
        StrQuery += "ISNULL(ReferenceNo,'-') as ReferenceNo,IC_Bank.BankName,isnull(PreviousPrintLocationCode,'-') as PreviousPrintLocationCode,"
        StrQuery += "ISNULL(PreviousPrintLocationName,'-') as PreviousPrintLocationName,CONVERT(date,VerificationDate) as VerificationDate,"
        StrQuery += "CONVERT(date,ApprovalDate) as ApprovalDate,CONVERT(date,StaleDate) as staledate,CONVERT(date,CancellationDate) as CancellationDate,"
        StrQuery += "isnull(BeneficiaryAccountNo,'-') as BeneficiaryAccountNo,isnull(BeneAccountBranchCode,'-') as BeneAccountBranchCode,"
        StrQuery += "isnull(BeneAccountCurrency,'-') as BeneAccountCurrency,LastPrintDate,BeneficiaryAccountNo,BeneAccountBranchCode,BeneAccountCurrency,"
        StrQuery += "CONVERT(date,RevalidationDate) as revalidationdate,IC_Instruction.ProductTypeCode,IC_Instruction.CompanyCode,ic_instruction.status,IC_Company.CompanyName,"
        StrQuery += "IC_ProductType.ProductTypeName,"
        StrQuery += "case isnull(convert(varchar,IsAmendmentComplete),CONVERT(varchar,'false')) "
        StrQuery += "when 'false' then "
        StrQuery += "CONVERT(varchar,'false') "
        StrQuery += "else "
        StrQuery += "IsAmendmentComplete "
        StrQuery += " end "
        StrQuery += "as IsAmendmentComplete, "
        StrQuery += "case ic_instruction.FileBatchNo when 'SI' "
        'StrQuery += " then 'SI' else ic_instruction.FileBatchNo + '-' + IC_Files.OriginalFileName end as FileBatchNoName,case isnull(IC_FirstLegReversedInstructions.IsFirstLEgReversed,0) when 0  then 0 else IC_FirstLegReversedInstructions.IsFirstLEgReversed end as IsFirstLEgReversed,FirstLegReversedID "
        StrQuery += " then 'SI' else ic_instruction.FileBatchNo + '-' + IC_Files.OriginalFileName end as FileBatchNoName "
        StrQuery += "from IC_Instruction "
        StrQuery += "left join IC_Office on IC_Instruction.PrintLocationCode=IC_Office.OfficeID "
        StrQuery += "left join IC_Bank on IC_Instruction.BeneficiaryBankCode=IC_Bank.BankCode "
        StrQuery += "left join IC_Company on IC_Instruction.CompanyCode=IC_Company.CompanyCode "
        StrQuery += "left join IC_ProductType on IC_Instruction.ProductTypeCode=IC_ProductType.ProductTypeCode "
        StrQuery += "left join IC_InstructionStatus on IC_Instruction.Status=IC_InstructionStatus.StatusID "
        StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID "
        'StrQuery += "left join IC_FirstLegReversedInstructions on IC_Instruction.InstructionID=IC_FirstLegReversedInstructions.RelatedID "

        If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And Amount = 0 Then
            If DateType.ToString = "Creation Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.CreateDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.CreateDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Value Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.ValueDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.ValueDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Approval Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Stale Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.StaleDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.StaleDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Cancellation Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Last Print Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Revalidation Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Verfication Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            End If
            If ProductTypeCode.ToString <> "" Then
                WhereClasue += " AND IC_Instruction.ProductTypeCode=@ProductTypeCode"
                params.Add("ProductTypeCode", ProductTypeCode)
            End If
            If CompanyCode.ToString <> "0" Then
                WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                params.Add("CompanyCode", CompanyCode)
            End If
           
        Else
            If InstructionNo.ToString <> "" Then
                WhereClasue += " AND InstructionID=@InstructionNo"
                params.Add("InstructionNo", InstructionNo)
            End If
            If InstrumentNo.ToString <> "" Then
                WhereClasue += " AND InstrumentNo=@InstrumentNo"
                params.Add("InstrumentNo", InstrumentNo)
            End If
           
            If CompanyCode.ToString <> "0" Then
                WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                params.Add("CompanyCode", CompanyCode)
            End If
            If Not Amount < 1 Then
                If AmountOperator.ToString = "=" Then
                    WhereClasue += " AND Amount = @Amount"
                    params.Add("Amount", Amount)
                ElseIf AmountOperator.ToString = "<" Then
                    WhereClasue += " AND Amount < @Amoun"
                    params.Add("Amount", Amount)
                ElseIf AmountOperator.ToString = ">" Then
                    WhereClasue += " AND Amount >  @Amount"
                    params.Add("Amount", Amount)
                ElseIf AmountOperator.ToString = "<=" Then
                    WhereClasue += " And Amount <=  @Amount"
                    params.Add("Amount", Amount)
                ElseIf AmountOperator.ToString = ">=" Then
                    WhereClasue += " And Amount >=  @Amount"
                    params.Add("Amount", Amount)
                ElseIf AmountOperator.ToString = "<>" Then
                    WhereClasue += " And Amount <>  @Amount"
                    params.Add("Amount", Amount)
                End If
            End If
        End If
        WhereClasue += " And IC_Instruction.Status In ("
        For Each Stat In Status
            WhereClasue += Stat & ","
        Next
        WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
        WhereClasue += ") "
        'WhereClasue += " And IC_FirstLegReversedInstructions.Status<>52 "
        WhereClasue += " And ( "

        If AccountNumber.Rows.Count > 0 Then
            WhereClasue += " ( "
            WhereClasue += "ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + ic_instruction.PaymentNatureCode + '" & "-" & "' + Convert(Varchar,CreatedOfficeCode) IN ("
            For Each dr As DataRow In AccountNumber.Rows

                WhereClasue += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "-" & dr("OfficeID").ToString & "',"

            Next
            WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
            WhereClasue += ")"
            WhereClasue += " )"
            If AccountNumber(0)(3) = False Then
                WhereClasue += " OR (IC_Instruction.CreateBy=@UsersID)"
                params.Add("UsersID", UsersID)
            End If
        Else
            WhereClasue += " OR (IC_Instruction.CreateBy=@UsersID)"
            params.Add("UsersID", UsersID)
        End If
        WhereClasue += " ) "

        If WhereClasue.Contains(" Where  AND ") = True Then
            WhereClasue = WhereClasue.Replace(" Where  AND ", "Where ")
        End If
        StrQuery += WhereClasue
        StrQuery += " Order By InstructionID Desc"
        Dim utill As New esUtility()
        dt = New DataTable
        dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)
        If Not CurrentPage = 0 Then

            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        Else
            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        End If

    End Sub

    Protected Sub btnCancelInst_Click(sender As Object, e As EventArgs) Handles btnCancelInst.Click

        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim ArryListInstructionId As New ArrayList
                Dim objICInstruction As ICInstruction
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim StrAuditTrailAction As String
                Dim dr As DataRow
                ArryListInstructionId.Clear()


                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Error Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                Dim dt As New DataTable
                dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Message", GetType(System.String)))

                For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True And chkSelect.Enabled = True And chkSelect.Visible = True Then
                        ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                    End If
                Next
                If ArryListInstructionId.Count > 0 Then
                    For Each InstructionID In ArryListInstructionId
                        objICInstruction = New ICInstruction
                        objICInstruction.es.Connection.CommandTimeout = 3600
                        objICInstruction.LoadByPrimaryKey(InstructionID)
                        dr = dt.NewRow
                        Try
                            StrAuditTrailAction = Nothing
                            StrAuditTrailAction += "Instruction with ID [ " & InstructionID & " ]  updated."
                            StrAuditTrailAction += "Status updated from  [ " & objICInstruction.Status & " ][ " & objICInstruction.UpToICInstructionStatusByStatus.StatusName & " ], to status [ " & "5" & "] "
                            StrAuditTrailAction += "by user [ " & Me.UserInfo.UserID & " ] [ " & Me.UserInfo.Username & " ]"
                            objICInstruction.CancellationDate = Date.Now

                            ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId, Me.UserInfo.Username, StrAuditTrailAction, "UPDATE")
                            ICInstructionController.UpdateInstructionStatus(InstructionID, objICInstruction.Status, "5", Me.UserId.ToString, Me.UserInfo.Username.ToString, "Remarks", "CANCEL", "")
                            dr("InstructionID") = InstructionID.ToString
                            dr("Message") = "Instruction(s) cancelled successfully"
                            dr("Status") = "5"
                            dt.Rows.Add(dr)
                            PassToCancelCount = PassToCancelCount + 1

                            Continue For
                        Catch ex As Exception
                            'ICUtilities.AddAuditTrail("Instruction Skip: Cancel instruction fails on Verification queue due to " & ex.Message.ToString, "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            objICInstruction.SkipReason = "Instruction Skip: Cancel instruction fails on Verification queue due to " & ex.Message.ToString & ". Action was taken by [ " & Me.UserId & " ] [ " & Me.UserInfo.Username & " ]."
                            ICInstructionController.UpdateInstructionSkipReason(InstructionID, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            'dr("Message") = "Instruction(s) not cancelled successfully due to " & ex.Message.ToString
                            dr("Message") = "Instruction(s) skipped"
                            dr("Status") = objICInstruction.Status.ToString
                            dr("InstructionID") = objICInstruction.InstructionID.ToString
                            dt.Rows.Add(dr)
                            FailToCancelCount = FailToCancelCount + 1

                            Continue For
                        End Try

                    Next
                    LoadgvAccountPaymentNatureProductType(True)
                    If dt.Rows.Count > 0 Then
                        gvShowSummary.DataSource = Nothing
                        gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                        gvShowSummary.DataBind()
                        pnlShowSummary.Style.Remove("display")
                        Dim scr As String
                        scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                        Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                    Else
                        gvShowSummary.DataSource = Nothing
                        pnlShowSummary.Style.Add("display", "none")
                    End If

                    If Not PassToCancelCount = 0 Then
                        Dim dtInstructionInfo As New DataTable
                        Dim ArrayListStatus As New ArrayList
                        ArrayListStatus.Clear()
                        ArrayListStatus.Add(5)
                        dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArryListInstructionId, ArrayListStatus, False)
                        EmailUtilities.Cancelledtransactions(dtInstructionInfo, Me.UserInfo.Username.ToString, "APNatureAndLocation")
                        SMSUtilities.Cancelledtransactions(Me.UserInfo.Username.ToString, dtInstructionInfo, ArrayListStatus, ArryListInstructionId, "APNatureAndLocation")
                    End If

                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub


End Class

