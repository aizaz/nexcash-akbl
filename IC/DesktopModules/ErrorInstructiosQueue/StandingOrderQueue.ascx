﻿
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="StandingOrderQueue.ascx.vb"
    Inherits="DesktopModules_StandingOrderQueue_StandingOrderQueue" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:TextBoxSetting BehaviorID="reTnxCount" Validation-IsRequired="false" EmptyMessage="Transaction Count"
        ErrorMessage="Transaction Count">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTnxCount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reTotalAmount" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Total Amount">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTotalAmount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reCurrency" Validation-IsRequired="false" EmptyMessage=""
        ErrorMessage="Currency">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCurrency" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rePaymentMode" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Payment Mode">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPaymentMode" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reAvailableBalance" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Available Balance">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAvailableBalance" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reInstructionNo" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Enter Instruction No." ValidationExpression="[0-9]{0,8}">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstructionNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reInstrumentNo" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Enter Instrument No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstrumentNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reReferenceNo" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Enter Reference No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reBaneName" Validation-IsRequired="false" EmptyMessage=""
        ErrorMessage="Enter Baneficiary Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneName" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reAmount" Validation-IsRequired="false" EmptyMessage=""
        ErrorMessage="Enter Amount">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAmount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reRemarks" Validation-IsRequired="false" EmptyMessage=""
        ErrorMessage="Enter Remarks" Validation-ValidationGroup="Remarks">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRemarks" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<script type="text/javascript">

   
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to cancel selected instruction(s)?") == true) {
            var btnCancel = document.getElementById('<%=btnCancel.ClientID%>')
            btnCancel.value = "Processing...";
            btnCancel.disabled = true;

            var a = btnCancel.name;
            __doPostBack(a, '');
            return true;
        }
        else {
            return false;
        }
    }
    
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }

    $(document).ready(function () {

        // Dialog
        $('#urldialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#loading').hide();



            },
            resizable: false
        });
        // Summary Dialog
        $('#sumdialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#sumdialog').hide();
                //                window.location.reload();
                //                window.location.reload();sa
            },
            resizable: false
        });
    });
    function showsumdialog(title, modal, width, height) {
        //  alert(modal);
        $('#sumdialog').dialog("option", "title", title);
        $('#sumdialog').dialog("option", "modal", modal);
        $('#sumdialog').dialog("option", "width", width);
        $('#sumdialog').dialog("option", "height", height);
        $('#sumdialog').dialog('open');

        return false;

    };


    function showurldialog(title, url, modal, width, height) {
        //  alert(modal);
        $('#urldialog').dialog("option", "title", title);
        $('#urldialog').dialog("option", "modal", modal);
        $('#urldialog').dialog("option", "width", width);
        $('#urldialog').dialog("option", "height", height);
        $('#urldialog').dialog('open');
        $('#dialogiframe').hide();
        $('#loading').show();
        $('#dialogiframe').attr("src", url);
        $('#dialogiframe').load(function () {
            $('#loading').hide();
            $('#dialogiframe').show();
        });
        return false;

    };

    function CloseIframe() {
        $('#urldialog').dialog('close');
    }
</script>
<script language="javascript" type="text/javascript">
    function AdjustWidth(ddl) {
        var maxWidth = 0;
        var Counter = 0;
        for (var i = 0; i < ddl.length; i++) {
            if (ddl.options[i].text.length > maxWidth) {
                Counter = Counter + 1;
                maxWidth = ddl.options[i].text.length;
            }
        }
        if (Counter = 1) { ddl.style.width = "250px"; }

        else {
            alert(Counter);
            ddl.style.width = maxWidth + "px";
        }
        //-->
    }
</script>
<style type="text/css">
.ctrDropDown{
    width:250px;
   
}
.ctrDropDownClick{
    
    width:600px;
 
}
.plainDropDown{
    width:250px;
   
}
</style>
<%--<asp:Label ID="lblscript" runat="server" Text=""></asp:Label>--%>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%" colspan="4">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Error Queue" CssClass="lblHeadingForQueue"></asp:Label>
                        <br />
                        <%--  Note:&nbsp; Fields marked as
                        <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        &nbsp;are required.--%>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%" colspan="4">
                        <table align="left" valign="top" style="width: 100%">
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%">
                                    <table align="left" valign="top" style="width: 100%">
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 25%">
                                                <asp:Label ID="lblDateFrom" runat="server" Text="Date From" CssClass="lbl"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="width: 25%">
                                                <telerik:RadDatePicker ID="raddpCreationFromDate" runat="server" Width="180px" AutoPostBack="true">
                                                    <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                    </Calendar>
                                                    <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                        Font-Size="10.5px" LabelWidth="40%" type="text" value="">
                                                    </DateInput>
                                                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                </telerik:RadDatePicker>
                                            </td>
                                            <td align="left" valign="top" style="width: 25%">
                                                <asp:Label ID="lblCreationDateTo" runat="server" Text="Date To" CssClass="lbl"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="width: 25%">
                                                <telerik:RadDatePicker ID="raddpCreationToDate" runat="server" Width="180px" AutoPostBack="true">
                                                    <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                    </Calendar>
                                                    <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                        LabelWidth="40%" type="text" value="" Font-Size="10.5px">
                                                    </DateInput>
                                                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="left" valign="top" style="width: 50%">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lbl" runat="server" Text="Group" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblAccountPaymentNature0" runat="server" Text="Company" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblAccountPaymentNature" runat="server" Text="Account Payment Nature"
                            CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblProductType" runat="server" Text="Product Type" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                    <div style= "width:250px; overflow:hidden;">
            <asp:DropDownList ID="ddlAcccountPaymentNature" runat="server" class="ctrDropDown"  onblur="this.className='ctrDropDown';" onmousedown="this.className='ctrDropDownClick';" onchange="this.className='ctrDropDown';" AutoPostBack="true">
            </asp:DropDownList>
            </div>
                        <%--<asp:DropDownList ID="ddlAcccountPaymentNature" runat="server" CssClass="dropdown"
                            AutoPostBack="True">
                        </asp:DropDownList>--%>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlProductType" runat="server" CssClass="dropdown" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblStatus" runat="server" Text="Status"
                            CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblInstructionNo" runat="server" Text="Instruction No." CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="dropdown" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtInstructionNo" runat="server" CssClass="txtbox" MaxLength="8"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblInstrumentNo" runat="server" Text="Instrument No." CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtInstrumentNo" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%" colspan="4">
                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn" CausesValidation="False" />
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%" colspan="4">
                        
                        <asp:Button ID="btnCancelInst" runat="server" Text="Cancel Instruction" CssClass="btnCancel" CausesValidation="False" />
                        
                        <asp:Button ID="btnCancel" runat="server" Text="Back" CssClass="btnCancel" CausesValidation="False" />
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%" colspan="4">
                        <asp:Label ID="lblNRF" runat="server" Text="No Record Found." CssClass="headingblue"
                            Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%" colspan="4">
                        <telerik:RadGrid ID="gvAuthorization" runat="server" AllowPaging="True" Width="100%"
                            CssClass="RadGrid" AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0"
                            ShowFooter="True" PageSize="100">
                            <ClientSettings>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <MasterTableView DataKeyNames="ClientAccountCurrency,InstructionID,IsAmendmentComplete,status" TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                       <telerik:GridTemplateColumn ItemStyle-Width="6%">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Checked="false" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox2" Checked="false" Text="" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="6%" />
                                         <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="6%" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Instruction No." HeaderStyle-Width="8%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbInstructionID" runat="server" Text='<%#Eval("InstructionID") %>'
                                                ToolTip="Instruction No." ForeColor="Blue" Enabled="false"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                         <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridTemplateColumn>
                                     <telerik:GridBoundColumn DataField="FileBatchNoName" HeaderText="Batch No" SortExpression="FileBatchNoName">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CreateDate" HeaderText="Creation Date" SortExpression="CreateDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}" HeaderStyle-Width="7%">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ValueDate" HeaderText="Value Date" SortExpression="ValueDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}" HeaderStyle-Width="8%">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>
                                   
                                    <telerik:GridBoundColumn DataField="BeneficiaryName" HeaderText="Bene Name" SortExpression="BeneficiaryName">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="12%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="12%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Amount" HeaderText="Amount" SortExpression="Amount"
                                        HtmlEncode="false" DataFormatString="{0:N2}">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProductTypeCode" HeaderText="Payment Mode" SortExpression="ProductTypeCode">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                      <telerik:GridBoundColumn DataField="StatusName" HeaderText="Status" SortExpression="StatusName">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InstrumentNo" HeaderText="Instrument No." SortExpression="InstrumentNo">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                  <telerik:GridTemplateColumn HeaderText="Revert">
                                      <ItemTemplate>
                                          <asp:Button ID="btnRetry" runat="server" Text="Revert" CssClass="btn" CausesValidation="False" Height="20px" CommandName="Revert" CommandArgument='<%# Eval("InstructionID") %>' />
                                      </ItemTemplate>
                                                 <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="7%" />
                                  </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                           <HeaderStyle CssClass="GridHeader" />
                             <ItemStyle CssClass="GridItem" />
                            <AlternatingItemStyle CssClass="GridItem" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                        
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div id="sumdialog" title="" style="text-align: center">
    <asp:Panel ID="pnlShowSummary" runat="server" Style="display: none; width: 100%"
        ScrollBars="Vertical" Height="600px">
        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnCancel" /><br />
        <telerik:RadGrid ID="gvShowSummary" runat="server" AllowPaging="false" Width="100%" CssClass="RadGrid"
            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
            PageSize="10">
            <AlternatingItemStyle CssClass="rgAltRow" />
            <MasterTableView>
                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                </RowIndicatorColumn>
                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="InstructionID" HeaderText="Instruction No." SortExpression="InstructionID">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                    <%-- <telerik:GridBoundColumn DataField="ReferenceNo" HeaderText="Reference No." SortExpression="ReferenceNo">
                                    </telerik:GridBoundColumn>--%>
                    <telerik:GridBoundColumn DataField="Message" HeaderText="Message" SortExpression="Message">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
                <PagerStyle AlwaysVisible="True" />
            </MasterTableView>
            <ItemStyle CssClass="rgRow" />
            <PagerStyle AlwaysVisible="True" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
    </asp:Panel>
</div>
<div id="urldialog" title="">
    <iframe id='dialogiframe' frameborder="0" allowtransparency="true" src="" style="border-style: none;
        width: 100%; height: 100%; overflow-x: hidden; background-color: transparent;">
    </iframe>
    <div id='loading' style="display: none; margin: 0px auto; text-align: center; width: 100%;
        height: 100%; position: relative">
        <img src='../../../../images/progressbar.gif' style="left: 45%; top: 50%; position: absolute;" />
    </div>
</div>
