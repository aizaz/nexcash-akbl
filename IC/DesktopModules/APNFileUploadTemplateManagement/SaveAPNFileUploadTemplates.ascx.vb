﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports Telerik.Web.UI

Partial Class DesktopModules_AccountsManagement_SaveAccount
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private AccntNumber As String
    Private BranchCode As String
    Private Currency As String
    Private PaymentNatureCode As String
    Private TemplateID As String

    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_AccountsManagement_SaveAccount_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            AccntNumber = Request.QueryString("AccountNumber").ToString()
            BranchCode = Request.QueryString("BranchCode").ToString()
            Currency = Request.QueryString("Currency").ToString()
            PaymentNatureCode = Request.QueryString("PaymentNatureCode").ToString()
            TemplateID = Request.QueryString("TemplateID").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlCompany()
                LoadAccountPaymentNatureByCompanyCode()
                LoadFileUploadTemplates()
               
                'pnlFlexiFields.Style.Add("display", "none")

                If AccntNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" And PaymentNatureCode.ToString() = "0" And TemplateID.ToString() = "0" Then

                    lblPageHeader.Text = "Add Account Payment Nature File Upload Template"
                    btnSave.Text = "Save"

                    'lblRNFFlexiFields.Visible = True
                    btnSave.Visible = CBool(htRights("Add"))

                Else
                    If ICAccountPaymentNatureFileUploadTemplateController.CheckIsInstructionExistInPendingReadStatusAgainstPaymentNatureAndAccount(PaymentNatureCode) = True Then
                        UIUtilities.ShowDialog(Me, "Error", "Account payment nature file upload template can not be updated because file exists in pending read status against payment nature.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If

                    lblPageHeader.Text = "Edit Account Payment Nature File Upload Template"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    'txtAccountNumber.ReadOnly = True
                    Dim objICAPNFUTemplate As New ICAccountPayNatureFileUploadTemplate
                    Dim objICCompany As New ICCompany
                    Dim objICGroup As New ICGroup
                    Dim objICACcountPaymentNature As New ICAccountsPaymentNature
                    Dim objICTemplate As New ICTemplate


                    objICAPNFUTemplate.es.Connection.CommandTimeout = 3600
                    objICCompany.es.Connection.CommandTimeout = 3600
                    objICGroup.es.Connection.CommandTimeout = 3600
                    objICACcountPaymentNature.es.Connection.CommandTimeout = 3600


                    objICTemplate.LoadByPrimaryKey(TemplateID)
                    If Not objICTemplate.TemplateType = "Dual" Then
                        If objICAPNFUTemplate.LoadByPrimaryKey(TemplateID, AccntNumber.ToString, BranchCode.ToString, Currency.ToString, PaymentNatureCode.ToString) Then
                            objICCompany = objICAPNFUTemplate.UpToICAccountsPaymentNatureByAccountNumber.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode
                            objICGroup = objICCompany.UpToICGroupByGroupCode
                            LoadddlGroup()
                            ddlGroup.SelectedValue = objICGroup.GroupCode.ToString
                            LoadddlCompany()
                            ddlCompany.SelectedValue = objICCompany.CompanyCode.ToString
                            LoadAccountPaymentNatureByCompanyCode()
                            ddlAccountPaymentNature.SelectedValue = objICAPNFUTemplate.AccountNumber + "-" + objICAPNFUTemplate.BranchCode + "-" + objICAPNFUTemplate.Currency + "-" + objICAPNFUTemplate.PaymentNatureCode
                            ddlFileUploadTemplate.SelectedValue = objICAPNFUTemplate.TemplateID.ToString

                            ddlGroup.Enabled = False
                            ddlCompany.Enabled = False
                            ddlAccountPaymentNature.Enabled = False
                            ddlFileUploadTemplate.Enabled = False
                        End If
                    Else
                        If objICAPNFUTemplate.LoadByPrimaryKey(TemplateID, AccntNumber.ToString, BranchCode.ToString, Currency.ToString, PaymentNatureCode.ToString) Then
                            objICCompany = objICAPNFUTemplate.UpToICAccountsPaymentNatureByAccountNumber.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode
                            objICGroup = objICCompany.UpToICGroupByGroupCode
                            LoadddlGroup()
                            ddlGroup.SelectedValue = objICGroup.GroupCode.ToString
                            LoadddlCompany()
                            ddlCompany.SelectedValue = objICCompany.CompanyCode.ToString
                            LoadAccountPaymentNatureByCompanyCode()
                            ddlAccountPaymentNature.SelectedValue = objICAPNFUTemplate.AccountNumber + "-" + objICAPNFUTemplate.BranchCode + "-" + objICAPNFUTemplate.Currency + "-" + objICAPNFUTemplate.PaymentNatureCode
                            ddlFileUploadTemplate.SelectedValue = objICAPNFUTemplate.TemplateID.ToString


                            ddlGroup.Enabled = False
                            ddlCompany.Enabled = False
                            ddlAccountPaymentNature.Enabled = False
                            ddlFileUploadTemplate.Enabled = False
                        End If
                    End If



                End If
                'btnSave.Visible = ArrRights(1)
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Account Payment Nature File Upload Template Management")
            ViewState("htRights") = htRights

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#Region "Button Events"
    Private Sub Clear()
        ddlGroup.ClearSelection()
        ddlCompany.ClearSelection()
        ddlAccountPaymentNature.ClearSelection()
        ddlFileUploadTemplate.ClearSelection()
      
        'pnlFlexiFields.Style.Add("display", "none")
    End Sub
    Private Function checkDuplicateAPNFUTemplate(ByVal objAPNFUTemplate As ICAccountPayNatureFileUploadTemplate) As Boolean
        Dim Result As Boolean = False
        Dim objICAPNFUTemplateColl As New ICAccountPayNatureFileUploadTemplateCollection
        objICAPNFUTemplateColl.es.Connection.CommandTimeout = 3600




        objICAPNFUTemplateColl.Query.Where(objICAPNFUTemplateColl.Query.AccountNumber = objAPNFUTemplate.AccountNumber And objICAPNFUTemplateColl.Query.BranchCode = objAPNFUTemplate.BranchCode And objICAPNFUTemplateColl.Query.Currency = objAPNFUTemplate.Currency And objICAPNFUTemplateColl.Query.TemplateID = objAPNFUTemplate.TemplateID And objICAPNFUTemplateColl.Query.PaymentNatureCode = objAPNFUTemplate.PaymentNatureCode)

        objICAPNFUTemplateColl.Query.Load()
        If objICAPNFUTemplateColl.Query.Load Then
            Result = True
        End If
        Return Result
    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim objICAPNFUploadTemplate As New ICAccountPayNatureFileUploadTemplate

                objICAPNFUploadTemplate.es.Connection.CommandTimeout = 3600



                Dim StrArrayAccountPNature As String()
                StrArrayAccountPNature = Nothing

                If Not AccntNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" And PaymentNatureCode.ToString() = "0" And TemplateID.ToString() = "0" Then
                    objICAPNFUploadTemplate.LoadByPrimaryKey(TemplateID, AccntNumber, BranchCode, Currency, PaymentNatureCode)
                End If
                objICAPNFUploadTemplate.es.Connection.CommandTimeout = 3600
                StrArrayAccountPNature = ddlAccountPaymentNature.SelectedValue.Split("-")
                objICAPNFUploadTemplate.AccountNumber = StrArrayAccountPNature(0).ToString
                objICAPNFUploadTemplate.BranchCode = StrArrayAccountPNature(1).ToString
                objICAPNFUploadTemplate.Currency = StrArrayAccountPNature(2).ToString
                objICAPNFUploadTemplate.PaymentNatureCode = StrArrayAccountPNature(3).ToString
                objICAPNFUploadTemplate.TemplateID = ddlFileUploadTemplate.SelectedValue.ToString
                If AccntNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" And PaymentNatureCode.ToString() = "0" And TemplateID.ToString() = "0" Then
                    If checkDuplicateAPNFUTemplate(objICAPNFUploadTemplate) = False Then
                        objICAPNFUploadTemplate.CreatedBy = Me.UserId
                        objICAPNFUploadTemplate.CreatedDate = Date.Now
                        objICAPNFUploadTemplate.Creater = Me.UserId
                        objICAPNFUploadTemplate.CreationDate = Date.Now
                        ICAccountPaymentNatureFileUploadTemplateController.AddAccountPaymentNatureFileUpload(objICAPNFUploadTemplate, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        AddAccountPaymentNatureFileUploadTemplateFields(objICAPNFUploadTemplate, False, objICAPNFUploadTemplate.UpToICAccountsPaymentNatureByAccountNumber.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode)
                        UIUtilities.ShowDialog(Me, "Account Payment Nature File Upload Template", "Account payment nature file upload template added successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL("SaveAPNFileUploadTemplates", "&mid=" & Me.ModuleId & "&AccountNumber=0" & "&BranchCode=0" & "&Currency=0" & "&PaymentNatureCode=0" & "&TemplateID=0"))
                        Clear()
                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Duplicate account payment nature file upload template is not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If

                Else
                    objICAPNFUploadTemplate.CreatedBy = Me.UserId
                    objICAPNFUploadTemplate.CreatedDate = Date.Now
                    ICAccountPaymentNatureFileUploadTemplateController.AddAccountPaymentNatureFileUpload(objICAPNFUploadTemplate, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    ICAccountPaymentNatureFileUploadTemplateFieldsController.DeleteAPNFileUploadTemplateFields(objICAPNFUploadTemplate, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    AddAccountPaymentNatureFileUploadTemplateFields(objICAPNFUploadTemplate, True, objICAPNFUploadTemplate.UpToICAccountsPaymentNatureByAccountNumber.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode)

                    UIUtilities.ShowDialog(Me, "Account Payment Nature File Upload Template", "Account payment nature file upload template updated succcessfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL)
                    Clear()
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

#End Region
    Private Function GetLastFieldorder(ByVal objICAPNFUTemplate As ICAccountPayNatureFileUploadTemplate) As Integer
        Dim objICAPNFUTemplateFieldColl As New ICAccountPaymentNatureFileUploadTemplatefields
        objICAPNFUTemplateFieldColl.es.Connection.CommandTimeout = 3600
        objICAPNFUTemplateFieldColl.Query.Select(objICAPNFUTemplateFieldColl.Query.FieldOrder)
        objICAPNFUTemplateFieldColl.Query.Where(objICAPNFUTemplateFieldColl.Query.TemplateID = objICAPNFUTemplate.TemplateID And objICAPNFUTemplateFieldColl.Query.AccountNumber = objICAPNFUTemplate.AccountNumber And objICAPNFUTemplateFieldColl.Query.BranchCode = objICAPNFUTemplate.BranchCode And objICAPNFUTemplateFieldColl.Query.Currency = objICAPNFUTemplate.Currency And objICAPNFUTemplateFieldColl.Query.PaymentNatureCode = objICAPNFUTemplate.PaymentNatureCode)
        objICAPNFUTemplateFieldColl.Query.OrderBy(objICAPNFUTemplateFieldColl.Query.FieldOrder, EntitySpaces.DynamicQuery.esOrderByDirection.Descending)
        objICAPNFUTemplateFieldColl.Query.es.Top = 1
        If objICAPNFUTemplateFieldColl.Query.Load Then
            Return objICAPNFUTemplateFieldColl.Query.LoadDataTable()(0)(0)
        Else
            Return 0
        End If
    End Function
    Private Function GetLastFieldorderForDualFormatFile(ByVal objICAPNFUTemplate As ICAccountPayNatureFileUploadTemplate) As Integer
        Dim objICAPNFUTemplateFieldColl As New ICAccountPaymentNatureFileUploadTemplatefields
        objICAPNFUTemplateFieldColl.es.Connection.CommandTimeout = 3600
        objICAPNFUTemplateFieldColl.Query.Select(objICAPNFUTemplateFieldColl.Query.FieldOrder)
        objICAPNFUTemplateFieldColl.Query.Where(objICAPNFUTemplateFieldColl.Query.TemplateID = objICAPNFUTemplate.TemplateID And objICAPNFUTemplateFieldColl.Query.AccountNumber = objICAPNFUTemplate.AccountNumber And objICAPNFUTemplateFieldColl.Query.BranchCode = objICAPNFUTemplate.BranchCode And objICAPNFUTemplateFieldColl.Query.Currency = objICAPNFUTemplate.Currency And objICAPNFUTemplateFieldColl.Query.PaymentNatureCode = objICAPNFUTemplate.PaymentNatureCode)
        objICAPNFUTemplateFieldColl.Query.Where(objICAPNFUTemplateFieldColl.Query.FieldType = "NonFlexi" Or objICAPNFUTemplateFieldColl.Query.FieldType = "FlexiField")
        objICAPNFUTemplateFieldColl.Query.OrderBy(objICAPNFUTemplateFieldColl.Query.FieldOrder, EntitySpaces.DynamicQuery.esOrderByDirection.Descending)
        objICAPNFUTemplateFieldColl.Query.es.Top = 1
        If objICAPNFUTemplateFieldColl.Query.Load Then
            Return objICAPNFUTemplateFieldColl.Query.LoadDataTable()(0)(0)
        Else
            Return 0
        End If
    End Function
    Private Function GetLastFieldorderForFixFormatFieldsForDualTemplate(ByVal objICAPNFUTemplate As ICAccountPayNatureFileUploadTemplate) As Integer
        Dim objICAPNFUTemplateFieldColl As New ICAccountPaymentNatureFileUploadTemplatefields
        objICAPNFUTemplateFieldColl.es.Connection.CommandTimeout = 3600
        objICAPNFUTemplateFieldColl.Query.Select(objICAPNFUTemplateFieldColl.Query.FieldOrder)
        objICAPNFUTemplateFieldColl.Query.Where(objICAPNFUTemplateFieldColl.Query.TemplateID = objICAPNFUTemplate.TemplateID And objICAPNFUTemplateFieldColl.Query.AccountNumber = objICAPNFUTemplate.AccountNumber And objICAPNFUTemplateFieldColl.Query.BranchCode = objICAPNFUTemplate.BranchCode And objICAPNFUTemplateFieldColl.Query.Currency = objICAPNFUTemplate.Currency And objICAPNFUTemplateFieldColl.Query.PaymentNatureCode = objICAPNFUTemplate.PaymentNatureCode)
        objICAPNFUTemplateFieldColl.Query.Where(objICAPNFUTemplateFieldColl.Query.FieldType = "FixFormat")
        objICAPNFUTemplateFieldColl.Query.OrderBy(objICAPNFUTemplateFieldColl.Query.FieldOrder, EntitySpaces.DynamicQuery.esOrderByDirection.Descending)
        objICAPNFUTemplateFieldColl.Query.es.Top = 1
        If objICAPNFUTemplateFieldColl.Query.Load Then
            Return objICAPNFUTemplateFieldColl.Query.LoadDataTable()(0)(0)
        Else
            Return 0
        End If
    End Function
    Private Sub AddAccountPaymentNatureFileUploadTemplateFields(ByVal objICAPNFUTemplate As ICAccountPayNatureFileUploadTemplate, ByVal IsUpdate As Boolean, ByVal objCompany As ICCompany)
        Dim objICTemplateFieldsColl As ICTemplateFieldsCollection
        Dim objICAPNFUTemplateFields As ICAccountPaymentNatureFileUploadTemplatefields
        Dim ActionString As String = Nothing
        Dim FieldName As String = Nothing
        Dim FieldName2 As String = Nothing
        ActionString += "Account payment nature file upload template with account number [ " & objICAPNFUTemplate.AccountNumber & " ], payment nature [ " & objICAPNFUTemplate.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] "
        ActionString += ", file upload template [ " & objICAPNFUTemplate.UpToICTemplateByTemplateID.TemplateName & " ] [ " & objICAPNFUTemplate.UpToICTemplateByTemplateID.FileUploadTemplateCode & " ] added with fields [ "
        Dim objICTemplate As New ICTemplate

        objICTemplate.es.Connection.CommandTimeout = 3600

        objICTemplate.LoadByPrimaryKey(objICAPNFUTemplate.TemplateID.ToString)

        If Not objICTemplate.TemplateType = "Dual" Then
            objICTemplateFieldsColl = New ICTemplateFieldsCollection
            objICTemplateFieldsColl = ICTemplateFieldController.GetTemplateFieldsByTemplateIDAndFieldType(objICTemplate.TemplateID.ToString, "NonFlexi")
            If objICTemplateFieldsColl.Count > 0 Then
                For Each objICTemplateField As ICTemplateFields In objICTemplateFieldsColl
                    objICAPNFUTemplateFields = New ICAccountPaymentNatureFileUploadTemplatefields
                    objICAPNFUTemplateFields.es.Connection.CommandTimeout = 3600
                    objICAPNFUTemplateFields.AccountNumber = objICAPNFUTemplate.AccountNumber
                    objICAPNFUTemplateFields.BranchCode = objICAPNFUTemplate.BranchCode
                    objICAPNFUTemplateFields.Currency = objICAPNFUTemplate.Currency
                    objICAPNFUTemplateFields.PaymentNatureCode = objICAPNFUTemplate.PaymentNatureCode
                    objICAPNFUTemplateFields.TemplateID = objICAPNFUTemplate.TemplateID
                    objICAPNFUTemplateFields.FieldID = objICTemplateField.FieldID
                    objICAPNFUTemplateFields.IsRequired = objICTemplateField.IsRequired
                    objICAPNFUTemplateFields.MustRequired = objICTemplateField.IsMustRequired
                    objICAPNFUTemplateFields.FieldOrder = objICTemplateField.FieldOrder
                    objICAPNFUTemplateFields.FieldType = objICTemplateField.FieldType
                    objICAPNFUTemplateFields.FixLength = CInt(objICTemplateField.FixLength)
                    objICAPNFUTemplateFields.FieldName = objICTemplateField.FieldName


                    If objCompany.IsAllowOpenPayment = True And objCompany.IsBeneRequired = True And objICTemplateField.FieldName = "BeneficiaryCode" Then
                        objICAPNFUTemplateFields.IsRequired = False
                        objICAPNFUTemplateFields.MustRequired = False
                        objICAPNFUTemplateFields.IsRequiredFT = False
                        objICAPNFUTemplateFields.IsRequiredPO = False
                        objICAPNFUTemplateFields.IsRequiredCQ = False
                        objICAPNFUTemplateFields.IsRequiredDD = False
                        objICAPNFUTemplateFields.IsRequiredIBFT = False
                        objICAPNFUTemplateFields.IsRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = False
                        objICAPNFUTemplateFields.IsMustRequiredFT = False
                        objICAPNFUTemplateFields.IsMustRequiredPO = False
                        objICAPNFUTemplateFields.IsMustRequiredCQ = False
                        objICAPNFUTemplateFields.IsMustRequiredDD = False
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = False
                        objICAPNFUTemplateFields.IsRequiredBillPayment = False
                    ElseIf objCompany.IsAllowOpenPayment = True And objCompany.IsBeneRequired = False And objICTemplateField.FieldName = "BeneficiaryCode" Then
                        objICAPNFUTemplateFields.IsRequired = False
                        objICAPNFUTemplateFields.MustRequired = False
                        objICAPNFUTemplateFields.IsRequiredFT = False
                        objICAPNFUTemplateFields.IsRequiredPO = False
                        objICAPNFUTemplateFields.IsRequiredCQ = False
                        objICAPNFUTemplateFields.IsRequiredDD = False
                        objICAPNFUTemplateFields.IsRequiredIBFT = False
                        objICAPNFUTemplateFields.IsRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = False
                        objICAPNFUTemplateFields.IsMustRequiredFT = False
                        objICAPNFUTemplateFields.IsMustRequiredPO = False
                        objICAPNFUTemplateFields.IsMustRequiredCQ = False
                        objICAPNFUTemplateFields.IsMustRequiredDD = False
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = False
                        objICAPNFUTemplateFields.IsRequiredBillPayment = False
                    ElseIf objCompany.IsAllowOpenPayment = False And objCompany.IsBeneRequired = False And objICTemplateField.FieldName = "BeneficiaryCode" Then
                        objICAPNFUTemplateFields.IsRequired = False
                        objICAPNFUTemplateFields.MustRequired = False
                        objICAPNFUTemplateFields.IsRequiredFT = False
                        objICAPNFUTemplateFields.IsRequiredPO = False
                        objICAPNFUTemplateFields.IsRequiredCQ = False
                        objICAPNFUTemplateFields.IsRequiredDD = False
                        objICAPNFUTemplateFields.IsRequiredIBFT = False
                        objICAPNFUTemplateFields.IsRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = False
                        objICAPNFUTemplateFields.IsMustRequiredFT = False
                        objICAPNFUTemplateFields.IsMustRequiredPO = False
                        objICAPNFUTemplateFields.IsMustRequiredCQ = False
                        objICAPNFUTemplateFields.IsMustRequiredDD = False
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = False
                        objICAPNFUTemplateFields.IsRequiredBillPayment = False
                    Else
                        objICAPNFUTemplateFields.IsRequired = objICTemplateField.IsRequired
                        objICAPNFUTemplateFields.MustRequired = objICTemplateField.IsMustRequired
                        objICAPNFUTemplateFields.IsRequiredFT = objICTemplateField.IsRequiredFT
                        objICAPNFUTemplateFields.IsRequiredPO = objICTemplateField.IsRequiredPO
                        objICAPNFUTemplateFields.IsRequiredCQ = objICTemplateField.IsRequiredCheque
                        objICAPNFUTemplateFields.IsRequiredDD = objICTemplateField.IsRequiredDD
                        objICAPNFUTemplateFields.IsRequiredIBFT = objICTemplateField.IsRequiredIBFT
                        objICAPNFUTemplateFields.IsRequiredCOTC = objICTemplateField.IsRequiredCOTC
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = objICTemplateField.IsMustRequiredIBFT
                        objICAPNFUTemplateFields.IsMustRequiredFT = objICTemplateField.IsMustRequiredFT
                        objICAPNFUTemplateFields.IsMustRequiredPO = objICTemplateField.IsMustRequiredPO
                        objICAPNFUTemplateFields.IsMustRequiredCQ = objICTemplateField.IsMustRequiredCheque
                        objICAPNFUTemplateFields.IsMustRequiredDD = objICTemplateField.IsMustRequiredDD
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = objICTemplateField.IsMustRequiredCOTC
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = objICTemplateField.IsMustRequiredBillPayment
                        objICAPNFUTemplateFields.IsRequiredBillPayment = objICTemplateField.IsRequiredBillPayment
                    End If



                    objICAPNFUTemplateFields.CreatedBy = Me.UserId
                    objICAPNFUTemplateFields.CreatedDate = Date.Now
                    FieldName += objICTemplateField.FieldName & " , "

                    If IsUpdate = False Then
                        ICAccountPaymentNatureFileUploadTemplateFieldsController.AddAPNFUploadTemplateFields(objICAPNFUTemplateFields, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    Else
                        ICAccountPaymentNatureFileUploadTemplateFieldsController.AddAPNFUploadTemplateFields(objICAPNFUTemplateFields, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    End If
                Next
                FieldName2 = FieldName.Remove(FieldName.Length - 2)
                ActionString += FieldName2
                ICUtilities.AddAuditTrail(ActionString & " ]", "Account Payment Nature File Upload Template Field", objICAPNFUTemplate.TemplateID.ToString + "" + objICAPNFUTemplate.AccountNumber.ToString + "" + objICAPNFUTemplate.BranchCode.ToString + "" + objICAPNFUTemplate.Currency.ToString + "" + objICAPNFUTemplate.PaymentNatureCode.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")
            End If



        Else

            objICTemplateFieldsColl = New ICTemplateFieldsCollection
            objICTemplateFieldsColl = ICTemplateFieldController.GetTemplateFieldsByTemplateIDAndFieldType(objICTemplate.TemplateID.ToString, "NonFlexi")
            If objICTemplateFieldsColl.Count > 0 Then
                For Each objICTemplateField As ICTemplateFields In objICTemplateFieldsColl
                    objICAPNFUTemplateFields = New ICAccountPaymentNatureFileUploadTemplatefields
                    objICAPNFUTemplateFields.es.Connection.CommandTimeout = 3600
                    objICAPNFUTemplateFields.AccountNumber = objICAPNFUTemplate.AccountNumber
                    objICAPNFUTemplateFields.BranchCode = objICAPNFUTemplate.BranchCode
                    objICAPNFUTemplateFields.Currency = objICAPNFUTemplate.Currency
                    objICAPNFUTemplateFields.PaymentNatureCode = objICAPNFUTemplate.PaymentNatureCode
                    objICAPNFUTemplateFields.TemplateID = objICAPNFUTemplate.TemplateID
                    objICAPNFUTemplateFields.FieldID = objICTemplateField.FieldID
                    objICAPNFUTemplateFields.IsRequired = objICTemplateField.IsRequired
                    objICAPNFUTemplateFields.MustRequired = objICTemplateField.IsMustRequired
                    objICAPNFUTemplateFields.FieldOrder = objICTemplateField.FieldOrder
                    objICAPNFUTemplateFields.FieldType = objICTemplateField.FieldType
                    objICAPNFUTemplateFields.FixLength = CInt(objICTemplateField.FixLength)
                    objICAPNFUTemplateFields.FieldName = objICTemplateField.FieldName

                    If objCompany.IsAllowOpenPayment = True And objCompany.IsBeneRequired = True And objICTemplateField.FieldName = "BeneficiaryCode" Then
                        objICAPNFUTemplateFields.IsRequired = False
                        objICAPNFUTemplateFields.MustRequired = False
                        objICAPNFUTemplateFields.IsRequiredFT = False
                        objICAPNFUTemplateFields.IsRequiredPO = False
                        objICAPNFUTemplateFields.IsRequiredCQ = False
                        objICAPNFUTemplateFields.IsRequiredDD = False
                        objICAPNFUTemplateFields.IsRequiredIBFT = False
                        objICAPNFUTemplateFields.IsRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = False
                        objICAPNFUTemplateFields.IsMustRequiredFT = False
                        objICAPNFUTemplateFields.IsMustRequiredPO = False
                        objICAPNFUTemplateFields.IsMustRequiredCQ = False
                        objICAPNFUTemplateFields.IsMustRequiredDD = False
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = False
                        objICAPNFUTemplateFields.IsRequiredBillPayment = False
                    ElseIf objCompany.IsAllowOpenPayment = True And objCompany.IsBeneRequired = False And objICTemplateField.FieldName = "BeneficiaryCode" Then
                        objICAPNFUTemplateFields.IsRequired = False
                        objICAPNFUTemplateFields.MustRequired = False
                        objICAPNFUTemplateFields.IsRequiredFT = False
                        objICAPNFUTemplateFields.IsRequiredPO = False
                        objICAPNFUTemplateFields.IsRequiredCQ = False
                        objICAPNFUTemplateFields.IsRequiredDD = False
                        objICAPNFUTemplateFields.IsRequiredIBFT = False
                        objICAPNFUTemplateFields.IsRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = False
                        objICAPNFUTemplateFields.IsMustRequiredFT = False
                        objICAPNFUTemplateFields.IsMustRequiredPO = False
                        objICAPNFUTemplateFields.IsMustRequiredCQ = False
                        objICAPNFUTemplateFields.IsMustRequiredDD = False
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = False
                        objICAPNFUTemplateFields.IsRequiredBillPayment = False
                    ElseIf objCompany.IsAllowOpenPayment = False And objCompany.IsBeneRequired = False And objICTemplateField.FieldName = "BeneficiaryCode" Then
                        objICAPNFUTemplateFields.IsRequired = False
                        objICAPNFUTemplateFields.MustRequired = False
                        objICAPNFUTemplateFields.IsRequiredFT = False
                        objICAPNFUTemplateFields.IsRequiredPO = False
                        objICAPNFUTemplateFields.IsRequiredCQ = False
                        objICAPNFUTemplateFields.IsRequiredDD = False
                        objICAPNFUTemplateFields.IsRequiredIBFT = False
                        objICAPNFUTemplateFields.IsRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = False
                        objICAPNFUTemplateFields.IsMustRequiredFT = False
                        objICAPNFUTemplateFields.IsMustRequiredPO = False
                        objICAPNFUTemplateFields.IsMustRequiredCQ = False
                        objICAPNFUTemplateFields.IsMustRequiredDD = False
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = False
                        objICAPNFUTemplateFields.IsRequiredBillPayment = False
                    Else
                        objICAPNFUTemplateFields.IsRequired = objICTemplateField.IsRequired
                        objICAPNFUTemplateFields.MustRequired = objICTemplateField.IsMustRequired
                        objICAPNFUTemplateFields.IsRequiredFT = objICTemplateField.IsRequiredFT
                        objICAPNFUTemplateFields.IsRequiredPO = objICTemplateField.IsRequiredPO
                        objICAPNFUTemplateFields.IsRequiredCQ = objICTemplateField.IsRequiredCheque
                        objICAPNFUTemplateFields.IsRequiredDD = objICTemplateField.IsRequiredDD
                        objICAPNFUTemplateFields.IsRequiredIBFT = objICTemplateField.IsRequiredIBFT
                        objICAPNFUTemplateFields.IsRequiredCOTC = objICTemplateField.IsRequiredCOTC
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = objICTemplateField.IsMustRequiredIBFT
                        objICAPNFUTemplateFields.IsMustRequiredFT = objICTemplateField.IsMustRequiredFT
                        objICAPNFUTemplateFields.IsMustRequiredPO = objICTemplateField.IsMustRequiredPO
                        objICAPNFUTemplateFields.IsMustRequiredCQ = objICTemplateField.IsMustRequiredCheque
                        objICAPNFUTemplateFields.IsMustRequiredDD = objICTemplateField.IsMustRequiredDD
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = objICTemplateField.IsMustRequiredCOTC
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = objICTemplateField.IsMustRequiredBillPayment
                        objICAPNFUTemplateFields.IsRequiredBillPayment = objICTemplateField.IsRequiredBillPayment
                    End If




                    objICAPNFUTemplateFields.CreatedBy = Me.UserId
                    objICAPNFUTemplateFields.CreatedDate = Date.Now
                    FieldName += objICTemplateField.FieldName & " , "
                    If IsUpdate = False Then
                        ICAccountPaymentNatureFileUploadTemplateFieldsController.AddAPNFUploadTemplateFields(objICAPNFUTemplateFields, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    Else
                        ICAccountPaymentNatureFileUploadTemplateFieldsController.AddAPNFUploadTemplateFields(objICAPNFUTemplateFields, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    End If


                Next
            End If


            objICTemplateFieldsColl = New ICTemplateFieldsCollection
            objICTemplateFieldsColl = ICTemplateFieldController.GetTemplateFieldsByTemplateIDAndFieldType(objICTemplate.TemplateID.ToString, "FixFormat")
            If objICTemplateFieldsColl.Count > 0 Then
                For Each objICTemplateField As ICTemplateFields In objICTemplateFieldsColl
                    objICAPNFUTemplateFields = New ICAccountPaymentNatureFileUploadTemplatefields
                    objICAPNFUTemplateFields.es.Connection.CommandTimeout = 3600
                    objICAPNFUTemplateFields.AccountNumber = objICAPNFUTemplate.AccountNumber
                    objICAPNFUTemplateFields.BranchCode = objICAPNFUTemplate.BranchCode
                    objICAPNFUTemplateFields.Currency = objICAPNFUTemplate.Currency
                    objICAPNFUTemplateFields.PaymentNatureCode = objICAPNFUTemplate.PaymentNatureCode
                    objICAPNFUTemplateFields.TemplateID = objICAPNFUTemplate.TemplateID
                    objICAPNFUTemplateFields.FieldID = objICTemplateField.FieldID
                    objICAPNFUTemplateFields.IsRequired = objICTemplateField.IsRequired
                    objICAPNFUTemplateFields.MustRequired = objICTemplateField.IsMustRequired
                    objICAPNFUTemplateFields.FieldOrder = objICTemplateField.FieldOrder
                    objICAPNFUTemplateFields.FieldType = objICTemplateField.FieldType
                    objICAPNFUTemplateFields.FixLength = CInt(objICTemplateField.FixLength)
                    objICAPNFUTemplateFields.FieldName = objICTemplateField.FieldName
                    If objCompany.IsAllowOpenPayment = True And objCompany.IsBeneRequired = True And objICTemplateField.FieldName = "BeneficiaryCode" Then
                        objICAPNFUTemplateFields.IsRequired = False
                        objICAPNFUTemplateFields.MustRequired = False
                        objICAPNFUTemplateFields.IsRequiredFT = False
                        objICAPNFUTemplateFields.IsRequiredPO = False
                        objICAPNFUTemplateFields.IsRequiredCQ = False
                        objICAPNFUTemplateFields.IsRequiredDD = False
                        objICAPNFUTemplateFields.IsRequiredIBFT = False
                        objICAPNFUTemplateFields.IsRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = False
                        objICAPNFUTemplateFields.IsMustRequiredFT = False
                        objICAPNFUTemplateFields.IsMustRequiredPO = False
                        objICAPNFUTemplateFields.IsMustRequiredCQ = False
                        objICAPNFUTemplateFields.IsMustRequiredDD = False
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = False
                        objICAPNFUTemplateFields.IsRequiredBillPayment = False
                    ElseIf objCompany.IsAllowOpenPayment = True And objCompany.IsBeneRequired = False And objICTemplateField.FieldName = "BeneficiaryCode" Then
                        objICAPNFUTemplateFields.IsRequired = False
                        objICAPNFUTemplateFields.MustRequired = False
                        objICAPNFUTemplateFields.IsRequiredFT = False
                        objICAPNFUTemplateFields.IsRequiredPO = False
                        objICAPNFUTemplateFields.IsRequiredCQ = False
                        objICAPNFUTemplateFields.IsRequiredDD = False
                        objICAPNFUTemplateFields.IsRequiredIBFT = False
                        objICAPNFUTemplateFields.IsRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = False
                        objICAPNFUTemplateFields.IsMustRequiredFT = False
                        objICAPNFUTemplateFields.IsMustRequiredPO = False
                        objICAPNFUTemplateFields.IsMustRequiredCQ = False
                        objICAPNFUTemplateFields.IsMustRequiredDD = False
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = False
                        objICAPNFUTemplateFields.IsRequiredBillPayment = False
                    ElseIf objCompany.IsAllowOpenPayment = False And objCompany.IsBeneRequired = False And objICTemplateField.FieldName = "BeneficiaryCode" Then
                        objICAPNFUTemplateFields.IsRequired = False
                        objICAPNFUTemplateFields.MustRequired = False
                        objICAPNFUTemplateFields.IsRequiredFT = False
                        objICAPNFUTemplateFields.IsRequiredPO = False
                        objICAPNFUTemplateFields.IsRequiredCQ = False
                        objICAPNFUTemplateFields.IsRequiredDD = False
                        objICAPNFUTemplateFields.IsRequiredIBFT = False
                        objICAPNFUTemplateFields.IsRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = False
                        objICAPNFUTemplateFields.IsMustRequiredFT = False
                        objICAPNFUTemplateFields.IsMustRequiredPO = False
                        objICAPNFUTemplateFields.IsMustRequiredCQ = False
                        objICAPNFUTemplateFields.IsMustRequiredDD = False
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = False
                        objICAPNFUTemplateFields.IsRequiredBillPayment = False
                    Else
                        objICAPNFUTemplateFields.IsRequired = objICTemplateField.IsRequired
                        objICAPNFUTemplateFields.MustRequired = objICTemplateField.IsMustRequired
                        objICAPNFUTemplateFields.IsRequiredFT = objICTemplateField.IsRequiredFT
                        objICAPNFUTemplateFields.IsRequiredPO = objICTemplateField.IsRequiredPO
                        objICAPNFUTemplateFields.IsRequiredCQ = objICTemplateField.IsRequiredCheque
                        objICAPNFUTemplateFields.IsRequiredDD = objICTemplateField.IsRequiredDD
                        objICAPNFUTemplateFields.IsRequiredIBFT = objICTemplateField.IsRequiredIBFT
                        objICAPNFUTemplateFields.IsRequiredCOTC = objICTemplateField.IsRequiredCOTC
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = objICTemplateField.IsMustRequiredIBFT
                        objICAPNFUTemplateFields.IsMustRequiredFT = objICTemplateField.IsMustRequiredFT
                        objICAPNFUTemplateFields.IsMustRequiredPO = objICTemplateField.IsMustRequiredPO
                        objICAPNFUTemplateFields.IsMustRequiredCQ = objICTemplateField.IsMustRequiredCheque
                        objICAPNFUTemplateFields.IsMustRequiredDD = objICTemplateField.IsMustRequiredDD
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = objICTemplateField.IsMustRequiredCOTC
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = objICTemplateField.IsMustRequiredBillPayment
                        objICAPNFUTemplateFields.IsRequiredBillPayment = objICTemplateField.IsRequiredBillPayment
                    End If
                    objICAPNFUTemplateFields.CreatedBy = Me.UserId
                    objICAPNFUTemplateFields.CreatedDate = Date.Now
                    FieldName += objICTemplateField.FieldName & " , "
                    If IsUpdate = False Then
                        ICAccountPaymentNatureFileUploadTemplateFieldsController.AddAPNFUploadTemplateFields(objICAPNFUTemplateFields, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    Else
                        ICAccountPaymentNatureFileUploadTemplateFieldsController.AddAPNFUploadTemplateFields(objICAPNFUTemplateFields, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    End If
                Next
                FieldName2 = FieldName.Remove(FieldName.Length - 2)
                ActionString += FieldName2
                ICUtilities.AddAuditTrail(ActionString & "]", "Account Payment Nature File Upload Template Field", objICAPNFUTemplate.TemplateID.ToString + "" + objICAPNFUTemplate.AccountNumber.ToString + "" + objICAPNFUTemplate.BranchCode.ToString + "" + objICAPNFUTemplate.Currency.ToString + "" + objICAPNFUTemplate.PaymentNatureCode.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")

            End If

        End If
    End Sub
#Region "Drop Down Events"
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
#End Region
#Region "Other Functions/Routines"

    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return Result
        End Try
    End Function


#End Region
    Private Sub LoadAccountPaymentNatureByCompanyCode()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlAccountPaymentNature.Items.Clear()
            ddlAccountPaymentNature.Items.Add(lit)
            ddlAccountPaymentNature.AppendDataBoundItems = True
            ddlAccountPaymentNature.DataSource = ICAccountPaymentNatureController.GetAccountPaymentNatureByCompanyCode(ddlCompany.SelectedValue.ToString)
            ddlAccountPaymentNature.DataTextField = "AccountAndPaymentNature"
            ddlAccountPaymentNature.DataValueField = "AccountPaymentNature"
            ddlAccountPaymentNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadFileUploadTemplates()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlFileUploadTemplate.Items.Clear()
            ddlFileUploadTemplate.Items.Add(lit)
            ddlFileUploadTemplate.AppendDataBoundItems = True
            ddlFileUploadTemplate.DataSource = ICFileUploadTemplateController.GetAllFileUploadTemplates()
            ddlFileUploadTemplate.DataTextField = "TemplateName"
            ddlFileUploadTemplate.DataValueField = "TemplateID"
            ddlFileUploadTemplate.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    
    
    Private Sub LoadFlexiFieldsByCompanyCode(ByVal DoDataBind As Boolean)
        'Try
        '    ICFlexiFieldsController.GetFlexiFieldOrderByCompanyCodeForRadGrid(ddlCompany.SelectedValue.ToString, Me.gvFlexiFields.CurrentPageIndex + 1, Me.gvFlexiFields.PageSize, True, Me.gvFlexiFields)


        '    If gvFlexiFields.Items.Count > 0 Then

        '        pnlFlexiFields.Style.Remove("display")

        '        lblRNFFlexiFields.Visible = False

        '    Else

        '        lblRNFFlexiFields.Visible = True
        '        pnlFlexiFields.Style.Add("display", "none")
        '    End If

        'Catch ex As Exception
        '    ProcessModuleLoadException(Me, ex, False)
        '    UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        'End Try
    End Sub
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        LoadddlCompany()
        LoadAccountPaymentNatureByCompanyCode()
    End Sub
    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadAccountPaymentNatureByCompanyCode()
            LoadFlexiFieldsByCompanyCode(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
   

    'Protected Sub gvFlexiFields_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvFlexiFields.ItemDataBound
    '    Dim chkProcessAll As CheckBox

    '    If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
    '        chkProcessAll = New CheckBox
    '        chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
    '        chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvFlexiFields.MasterTableView.ClientID & "','0');"
    '    End If
    'End Sub


    'Protected Sub gvFlexiFields_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvFlexiFields.NeedDataSource
    '    Try
    '        'LoadAssignedFieldsForAPNFUTemplate(False)
    '    Catch ex As Exception

    '    End Try
    'End Sub


End Class
