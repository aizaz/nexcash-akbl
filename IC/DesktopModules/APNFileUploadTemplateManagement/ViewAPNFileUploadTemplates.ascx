﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewAPNFileUploadTemplates.ascx.vb"
    Inherits="DesktopModules_AccountsManagement_ViewAccounts" %>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure to remove this record ?") == true) {

            return true;
        }
        else {
            return false;
        }
    }
    function delcon(item) {


        if (window.confirm("Are you sure to remove this " + item + "?") == true) {

            return true;

        }
        else {

            return false;

        }
    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to remove Record(s)?") == true) {
            return true;
        }
        else {
            return false;
        }
    }  
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        //      alert(grid)
        //        if ((grid.rows.length -1) != 10 ) {
        //        if ((grid.rows.length) >= 12) {
        //            //            alert('More than 10 rows')
        //            for (i = 0; i < grid.rows.length - 1; i++) {
        //                cell = grid.rows[i].cells[CellNo];
        //                for (j = 0; j < cell.childNodes.length - 1; j++) {
        //                    if (cell.childNodes[j].type == "checkbox") {
        //                        cell.childNodes[j].checked = document.getElementById(idOfControllingCheckbox).checked;
        //                    }
        //                }
        //            }
        //        }
        //        else {
        //                     alert(grid.rows.length)
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    //    }
</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table width="100%">
                <tr>
                    <td align="center" valign="top" colspan="4">
                        <asp:Button ID="btnSaveAPNFUTemplate" runat="server" 
                            Text="Add Account Payment Nature File Upload Template" CssClass="btn"
                            CausesValidation="False" />
                        <asp:HiddenField ID="hfAccountCode" runat="server" Visible="true" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" colspan="4">
                        <asp:Label ID="lblAPNFUListHeader" runat="server" 
                            Text="Account, Payment Nature File Upload Template List" 
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr style="width: 100%">
                    <td align="left" valign="middle" style="width: 25%">
                        <asp:Label ID="lblGroup" runat="server" Text="Group" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="middle" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="middle" style="width: 25%">
                        <asp:Label ID="lblCompany" runat="server" Text="Company" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="middle" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr style="width: 100%">
                    <td align="left" valign="middle" style="width: 25%">
                        <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="middle" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="middle" style="width: 25%">
                        <asp:DropDownList ID="ddlcompany" runat="server" CssClass="dropdown" 
                            AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="middle" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr style="width: 100%">
                    <td align="left" valign="middle" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="middle" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="middle" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="middle" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" colspan="4">
                        <asp:Label ID="lblRNF" runat="server" Font-Bold="False" Text="No Record Found" Visible="False"
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" colspan="4">
                        <telerik:RadGrid ID="gvAPNFUTemplate" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" CssClass="RadGrid"
                            PageSize="100">
                            <ClientSettings>
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                           <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView DataKeyNames="AccountNumber,BranchCode,Currency,TemplateID,PaymentNatureCode" TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Approve" DataField="IsApproved">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="CompanyName" HeaderText="Company" SortExpression="CompanyName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="Account Number" SortExpression="AccountNumber">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PaymentNatureName" HeaderText="Payment Nature" SortExpression="PaymentNatureName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TemplateName" HeaderText="File Upload Template" SortExpression="TemplateName">
                                    </telerik:GridBoundColumn>
                                
                                    <telerik:GridTemplateColumn HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif"
                                            NavigateUrl='<%#NavigateURL("SaveAPNFileUploadTemplates", "&mid=" & Me.ModuleId & "&TemplateID="& Eval("TemplateID") & "&PaymentNatureCode="& Eval("PaymentNatureCode") & "&AccountNumber="& Eval("AccountNumber") & "&BranchCode="& Eval("BranchCode") & "&Currency="& Eval("Currency") )%>'
                                                ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server"
          CommandArgument='<%#Eval("AccountNumber").tostring + ";" +Eval("BranchCode").tostring + ";" +Eval("Currency").tostring + ";" +Eval("PaymentNatureCode").tostring + ";" +Eval("TemplateID").tostring %>'                                    
                                                CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                 <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="center" colspan="4">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td valign="middle" align="center" colspan="4">
                        <%--<asp:GridView ID="gvAccountsDetails" runat="server" AutoGenerateColumns="false" CssClass="Grid"
                            AllowPaging="true" AllowSorting="true" EnableModelValidation="true" Width="100%">
                            <AlternatingRowStyle CssClass="GridAltItem" />
                            <Columns>
                                <asp:BoundField DataField="AccountNumber" HeaderText="Account No." SortExpression="AccountNumber">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="BranchCode" HeaderText="Branch Code" SortExpression="BranchCode">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Currency" HeaderText="Currency" SortExpression="Currency">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlAddMasterSeries" runat="server" ImageUrl="~/images/add.gif"
                                            ToolTip="Add Master Series" Text="Add Master Series"></asp:HyperLink>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                </asp:TemplateField>
                                <asp:CheckBoxField DataField="isActive" HeaderText="Active" Text="">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="20px" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="20px" />
                                </asp:CheckBoxField>
                                <asp:CheckBoxField DataField="IsApproved" HeaderText="Approve" Text="">
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="20px" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="20px" />
                                </asp:CheckBoxField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveAccount", "&mid=" & Me.ModuleId & "&AccountNumber="& Eval("AccountNumber") & "&BranchCode="& Eval("BranchCode") & "&Currency="& Eval("Currency") )%>'
                                            ToolTip="Edit">Edit</asp:HyperLink>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("AccountNumber") + ";" +Eval("BranchCode") + ";" +Eval("Currency") %>'
                                            CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                            ToolTip="Delete" />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="GridHeader" />
                            <PagerStyle CssClass="GridPager" />
                            <RowStyle CssClass="GridItem" />
                        </asp:GridView>--%>
                        <asp:Button ID="btnDeleteAPNFUTemplate" runat="server" 
                            Text="Delete Account Payment Nature File Upload Templates" CssClass="btn" OnClientClick="javascript: return conBukDelete();"
                            CausesValidation="False" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
