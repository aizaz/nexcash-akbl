﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Partial Class DesktopModules_AccountsManagement_ViewAccounts
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_AccountsManagement_ViewAccounts_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
           
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlCompany()
                LoadAPNFileUploadTemplates(True)
                btnSaveAPNFUTemplate.Visible = CBool(htRights("Add"))

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
#End Region
#Region "Grid View Events"

    Protected Sub gvAPNFUTemplate_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvAPNFUTemplate.ItemCommand
        Try
            Dim objICAPNFUTemplate As New ICAccountPayNatureFileUploadTemplate
            objICAPNFUTemplate.es.Connection.CommandTimeout = 3600
            Dim StrArray As String()

            If e.CommandName.ToString = "del" Then
                If Page.IsValid Then
                    StrArray = e.CommandArgument.ToString.Split(";")
                    If ICAccountPaymentNatureFileUploadTemplateController.CheckIsInstructionExistInPendingReadStatusAgainstPaymentNatureAndAccount(StrArray(3).ToString) = False Then
                        If objICAPNFUTemplate.LoadByPrimaryKey(StrArray(4).ToString, StrArray(0).ToString, StrArray(1).ToString, StrArray(2).ToString, StrArray(3).ToString) Then
                            If ICAccountPaymentNatureFileUploadTemplateController.IsTemplateIsTaggedWithAPNProductType(objICAPNFUTemplate) = False Then
                                ICAccountPaymentNatureFileUploadTemplateFieldsController.DeleteAPNFileUploadTemplateFields(objICAPNFUTemplate, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                ICAccountPaymentNatureFileUploadTemplateController.DleteAPNFileUploadTemplate(objICAPNFUTemplate, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                UIUtilities.ShowDialog(Me, "Delete Account Payment Nature File Upload Template", "Account payment nature file upload template deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                                LoadAPNFileUploadTemplates(True)
                            Else
                                UIUtilities.ShowDialog(Me, "Error ", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                                LoadAPNFileUploadTemplates(False)
                            End If
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Error ", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                        LoadAPNFileUploadTemplates(False)
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Error ", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                LoadAPNFileUploadTemplates(False)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAccountsDetails_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAPNFUTemplate.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                ' Dim arrPageSizes() As String = {"1", "2", "5", "10", "50"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAPNFUTemplate.MasterTableView.ClientID)
                Next
                'If gvAccountsDetails.Items.Count > 0 Then
                '    myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
                'End If
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


#End Region
#Region "Button Events"
    Protected Sub btnSaveAccounts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAPNFUTemplate.Click
        Page.Validate()

        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveAPNFileUploadTemplates", "&mid=" & Me.ModuleId & "&AccountNumber=0" & "&BranchCode=0" & "&Currency=0" & "&PaymentNatureCode=0" & "&TemplateID=0"), False)
        End If

    End Sub
#End Region
#Region "Other Functions/Routines"
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Account Payment Nature File Upload Template Management")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            ' str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function

    Private Sub LoadAPNFileUploadTemplates(ByVal DoDataBind As Boolean)
        Try
            Dim GroupCode, CompanyCode As String

            If ddlGroup.SelectedValue.ToString <> "0" Then
                GroupCode = ddlGroup.SelectedValue.ToString
            Else
                GroupCode = ""
            End If
            If ddlcompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlcompany.SelectedValue.ToString
            Else
                CompanyCode = ""
            End If
            ICAccountPaymentNatureFileUploadTemplateController.GetAllAccountPaymentNatureFileUploadTemplateForRadGrid(GroupCode, CompanyCode, Me.gvAPNFUTemplate.CurrentPageIndex + 1, Me.gvAPNFUTemplate.PageSize, DoDataBind, Me.gvAPNFUTemplate)

            If gvAPNFUTemplate.Items.Count > 0 Then
                gvAPNFUTemplate.Visible = True
                lblRNF.Visible = False
                gvAPNFUTemplate.Columns(6).Visible = CBool(htRights("Delete"))
                btnDeleteAPNFUTemplate.Visible = CBool(htRights("Delete"))
            Else
                gvAPNFUTemplate.Visible = False
                lblRNF.Visible = True
                btnDeleteAPNFUTemplate.Visible = False
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try




    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlcompany.Items.Clear()
            ddlcompany.Items.Add(lit)
            ddlcompany.AppendDataBoundItems = True
            ddlcompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString)
            ddlcompany.DataValueField = "CompanyCode"
            ddlcompany.DataTextField = "CompanyName"
            ddlcompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
            LoadAPNFileUploadTemplates(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAPNFUTemplate_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAPNFUTemplate.NeedDataSource
        Try
            LoadAPNFileUploadTemplates(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAPNFUTemplate_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvAPNFUTemplate.PageIndexChanged
        Try
            gvAPNFUTemplate.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlcompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcompany.SelectedIndexChanged
        Try
            LoadAPNFileUploadTemplates(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    
    Protected Sub gvAPNFUTemplate_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAPNFUTemplate.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAPNFUTemplate.MasterTableView.ClientID & "','0');"
        End If
    End Sub
    Private Function CheckGVAPNFUTemplateSelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvAPNFUTemplateRow As GridDataItem In gvAPNFUTemplate.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvAPNFUTemplateRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnDeleteAPNFUTemplate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteAPNFUTemplate.Click
        Page.Validate()
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim PassToDeleteCount As Integer = 0
                Dim FailToDeleteCount As Integer = 0
                Dim objICAPNFUTemplate As ICAccountPayNatureFileUploadTemplate
                Dim AccountNumber, BranchCode, Currency, TemplateID, PaymentNatureCode As String

                If CheckGVAPNFUTemplateSelected() = False Then
                    UIUtilities.ShowDialog(Me, "Delete Account Payment Nature File Upload Template", "Please select atleast one account payment nature file upload template.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVAccounts In gvAPNFUTemplate.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVAccounts.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        AccountNumber = ""
                        BranchCode = ""
                        Currency = ""
                        TemplateID = ""
                        PaymentNatureCode = ""
                        AccountNumber = rowGVAccounts.GetDataKeyValue("AccountNumber").ToString
                        BranchCode = rowGVAccounts.GetDataKeyValue("BranchCode").ToString
                        Currency = rowGVAccounts.GetDataKeyValue("Currency").ToString
                        TemplateID = rowGVAccounts.GetDataKeyValue("TemplateID").ToString
                        PaymentNatureCode = rowGVAccounts.GetDataKeyValue("PaymentNatureCode").ToString
                        objICAPNFUTemplate = New ICAccountPayNatureFileUploadTemplate
                        objICAPNFUTemplate.es.Connection.CommandTimeout = 3600
                        objICAPNFUTemplate.LoadByPrimaryKey(TemplateID.ToString, AccountNumber, BranchCode, Currency, PaymentNatureCode)
                        If ICAccountPaymentNatureFileUploadTemplateController.CheckIsInstructionExistInPendingReadStatusAgainstPaymentNatureAndAccount(PaymentNatureCode) = FailToDeleteCount Then
                            If ICAccountPaymentNatureFileUploadTemplateController.IsTemplateIsTaggedWithAPNProductType(objICAPNFUTemplate) = False Then
                                Try

                                    ICAccountPaymentNatureFileUploadTemplateFieldsController.DeleteAPNFileUploadTemplateFields(objICAPNFUTemplate, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                    ICAccountPaymentNatureFileUploadTemplateController.DleteAPNFileUploadTemplate(objICAPNFUTemplate, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                    PassToDeleteCount = PassToDeleteCount + 1
                                Catch ex As Exception
                                    FailToDeleteCount = FailToDeleteCount + 1
                                    Continue For

                                End Try
                            Else
                                FailToDeleteCount = FailToDeleteCount + 1
                                Continue For
                            End If
                        Else
                            FailToDeleteCount = FailToDeleteCount + 1

                        End If

                    End If
                Next

                If Not PassToDeleteCount = 0 And Not FailToDeleteCount = 0 Then
                    LoadAPNFileUploadTemplates(True)
                    UIUtilities.ShowDialog(Me, "Delete Account Payment Nature File Upload Template", "[ " & PassToDeleteCount.ToString & " ] Account payment nature file upload template(s) deleted successfuly.<br /> [ " & FailToDeleteCount.ToString & " ] Account(s) can not be deleted due to following reasons:<br /> 1. Deleted associated record(s) first.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToDeleteCount = 0 And FailToDeleteCount = 0 Then
                    LoadAPNFileUploadTemplates(True)
                    UIUtilities.ShowDialog(Me, "Delete Account Payment Nature File Upload Template", PassToDeleteCount.ToString & " Account payment nature file upload template(s) deleted successfuly.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToDeleteCount = 0 And PassToDeleteCount = 0 Then
                    LoadAPNFileUploadTemplates(True)
                    UIUtilities.ShowDialog(Me, "Delete Account Payment Nature File Upload Template", "[ " & FailToDeleteCount.ToString & " ] Account payment nature file upload template(s) can not be deleted due to following reasons: <br /> 1. Deleted associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
End Class
