﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewBeneficiary.ascx.vb" Inherits="DesktopModules_BeneficiaryManagement_ViewBeneficiary" %>

<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        if (cell.childNodes[j].childNodes[0].disabled == false) {
                            cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                        }

                    }
                }
            }
        }
    }
</script>

<script type="text/javascript">
    function checkAllCheckboxesforDelBeneGrid(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
            }
        }
    }
</script>

<telerik:RadInputManager ID="rad" runat="server">
    <telerik:RegExpTextBoxSetting BehaviorID="reBeneName" Validation-IsRequired="false"
        ValidationExpression="^[a-zA-Z0-9 @._-]{1,250}$" ErrorMessage="Invalid Beneficiary Name"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
</telerik:RadInputManager>

<table align="left" valign="top" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%" colspan="4">
                        <asp:Button ID="btnSaveBene" runat="server" Text="Add Beneficiary" CssClass="btn" />
                        <asp:Button ID="btnAddBulkBene" runat="server" Text="Add Bulk Beneficiary" CssClass="btn" />
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%" colspan="4">
                        <asp:Label ID="lblBeneList" runat="server" Text="Beneficiary List" CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblBene" runat="server" Text="Select Beneficiary Group" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblBeneName" runat="server" Text="Beneficiary Name" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                </tr>

                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlBeneGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
                            <asp:ListItem Value="0">-- All --</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtBeneName" runat="server" CssClass="txtbox" MaxLength="250"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%" colspan="4">&nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%" colspan="4">
                        <asp:Button ID="btnSearch" runat="server" CssClass="btn" Text="Search" Width="75px" CausesValidation="False" />
                    </td>
                </tr>

                <%--Comment work is Delete Approval Coding--%>
                <%--   <tr>
                    <td align="left" valign="top">
                        <table width="100%">
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" colspan="4">
                                    <asp:Label ID="lblBeneRNF" runat="server" Text="Record Not Found." CssClass="headingblue" Visible="false"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <telerik:RadGrid ID="gvdelBene" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                            <ClientSettings>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView TableLayout="Fixed" DataKeyNames="BeneficiaryAddedVia,BeneficiaryStatus">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Select" DataField="IsApproved" UniqueName="SelectAll">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="BeneID" HeaderText="Bene ID" SortExpression="BeneID" UniqueName="BeneID">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneficiaryCode" HeaderText="Beneficiary Code" SortExpression="BeneficiaryCode" UniqueName="BeneCode">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneficiaryName" HeaderText="Beneficiary Name" SortExpression="BeneficiaryName" UniqueName="BeneficiaryName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PaymentNatureName" HeaderText="Payment Nature Name" SortExpression="PaymentNatureName" UniqueName="PaymentNature">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Active" DataField="IsActive" UniqueName="IsActive">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Approve" DataField="IsApproved" UniqueName="IsApproved">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridTemplateColumn UniqueName="Edit">
                                        <HeaderTemplate>
                                            <asp:Label ID="Edit" runat="server" Text="Edit"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveBene", "&mid=" & Me.ModuleId & "&id=" & Eval("BeneID"))%>'
                                                ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="Delete">
                                        <HeaderTemplate>
                                            <asp:Label ID="Delete" runat="server" Text="Delete Approval"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("BeneID") %>'
                                                CommandName="del" ImageUrl="~/images/lock.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete Approval" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False"></FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnApproveBene" runat="server" Text="Approve Beneficiaries" CssClass="btn" Width="153px" />
                        &nbsp;<asp:Button ID="btnDeleteApprovalBene" runat="server" Text="Send for Delete Approval" CssClass="btn" Width="181px" />
                        &nbsp;<asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn" Width="65px" />
                    </td>
                </tr>
               <tr>
                    <td align="left" valign="top">
                        <asp:Label ID="lblBeneDelList" runat="server" Text="Delete Beneficiary List" CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <telerik:RadGrid ID="gvBeneDelete" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                            <ClientSettings>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView TableLayout="Fixed" DataKeyNames="BeneficiaryAddedVia,BeneficiaryStatus">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Select" DataField="IsApproved" UniqueName="SelectAll">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="BeneID" HeaderText="Bene ID" SortExpression="BeneID" UniqueName="BeneID">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneficiaryCode" HeaderText="Beneficiary Code" SortExpression="BeneficiaryCode" UniqueName="BeneCode">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneficiaryName" HeaderText="Beneficiary Name" SortExpression="BeneficiaryName" UniqueName="BeneficiaryName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PaymentNatureName" HeaderText="Payment Nature Name" SortExpression="PaymentNatureName" UniqueName="PaymentNature">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Active" DataField="IsActive" UniqueName="IsActive">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Approve" DataField="IsApproved" UniqueName="IsApproved">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblDelReject" runat="server" Text="Cancel Approval"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelReject" runat="server" CommandArgument='<%#Eval("BeneID") %>'
                                                CommandName="DelReject" ImageUrl="~/images/Unlock.PNG" ToolTip="Cancel Approval" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblDelApprove" runat="server" Text="Delete"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelApprove" runat="server" CommandArgument='<%#Eval("BeneID") %>'
                                                CommandName="DelApprove" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False"></FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnCancelBeneApproval" runat="server" Text="Cancel Beneficiaries Approval" CssClass="btn" Width="186px" />
                        &nbsp;<asp:Button ID="btnDeleteBene" runat="server" OnClientClick="javascript: return con();" Text="Delete Beneficiaries" CssClass="btn" Width="133px" />
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" colspan="4">
                        <asp:Label ID="lblDelBeneRNF" runat="server" Text="Record Not Found." CssClass="headingblue" Visible="false"></asp:Label>
                    </td>
                </tr>--%>

                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%" colspan="4">
                        <table align="left" valign="top" style="width: 100%">
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" colspan="4">
                                    <asp:Label ID="lblBeneRNF" runat="server" Text="Record Not Found." CssClass="headingblue" Visible="false"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%" colspan="4">
                        <telerik:RadGrid ID="gvBene" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                            <ClientSettings>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView TableLayout="Fixed" DataKeyNames="BeneficiaryAddedVia,BeneficiaryStatus">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Select" DataField="IsApproved" UniqueName="SelectAll">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="BeneID" HeaderText="Bene ID" SortExpression="BeneID" UniqueName="BeneID">
                                    </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="BeneGroupName" HeaderText="Group Name" SortExpression="BeneGroupName" UniqueName="BeneGroupName">
                                    </telerik:GridBoundColumn>
                                    
                                    <telerik:GridBoundColumn DataField="BeneficiaryCode" HeaderText="Beneficiary Code" SortExpression="BeneficiaryCode" UniqueName="BeneCode">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneficiaryName" HeaderText="Beneficiary Name" SortExpression="BeneficiaryName" UniqueName="BeneficiaryName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PaymentNatureName" HeaderText="Payment Nature Name" SortExpression="PaymentNatureName" UniqueName="PaymentNature">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Active" DataField="IsActive" UniqueName="IsActive">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Approve" DataField="IsApproved" UniqueName="IsApproved">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridTemplateColumn UniqueName="Edit">
                                        <HeaderTemplate>
                                            <asp:Label ID="Edit" runat="server" Text="Edit"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveBene", "&mid=" & Me.ModuleId & "&id=" & Eval("BeneID"))%>'
                                                ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="Delete">
                                        <HeaderTemplate>
                                            <asp:Label ID="Delete" runat="server" Text="Delete"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("BeneID") %>'
                                                CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False"></FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%" colspan="4">
                        <asp:Button ID="btnApproveBene" runat="server" Text="Approve Beneficiaries" CssClass="btn" Width="153px" />
                        &nbsp;<asp:Button ID="btnDeleteBene" runat="server" OnClientClick="javascript: return con();" Text="Delete Beneficiaries" CssClass="btn" Width="133px" />
                        &nbsp;<asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn" Width="65px" />
                    </td>
                </tr>

            </table>
        </td>
    </tr>
</table>
