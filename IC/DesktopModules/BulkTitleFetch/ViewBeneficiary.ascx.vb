﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_BeneficiaryManagement_ViewBeneficiary
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable

#Region "Page Load"
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnSaveBene.Visible = CBool(htRights("Add"))
                LoadddlBene()
                LoadBeneficiaries(True)
                'LoadDeleteBeneficiaries(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Beneficiary Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Button Events"
    Protected Sub btnSaveBene_Click(sender As Object, e As EventArgs) Handles btnSaveBene.Click
        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveBene", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If
    End Sub

    Protected Sub btnAddBulkBene_Click(sender As Object, e As EventArgs) Handles btnAddBulkBene.Click
        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveBulkBene", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If
    End Sub

    Protected Sub btnApproveBene_Click(sender As Object, e As EventArgs) Handles btnApproveBene.Click
        If Page.IsValid Then
            Try
                Dim rowgvBene As GridDataItem
                Dim chkSelect As CheckBox
                Dim BeneID As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0
                Dim objBene As New ICBene

                If CheckgvBeneForProcessAll() = True Then
                    For Each rowgvBene In gvBene.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvBene.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            BeneID = rowgvBene("BeneID").Text.ToString()
                            If objBene.LoadByPrimaryKey(BeneID.ToString()) Then
                                If Me.UserInfo.IsSuperUser = False Then
                                    If objBene.IsApproved Then
                                        FailCount = FailCount + 1
                                        Continue For
                                    Else
                                        If objBene.CreateBy = Me.UserId Then
                                            FailCount = FailCount + 1
                                            Continue For
                                        ElseIf objBene.BeneficiaryCode = "" Or objBene.BeneficiaryCode Is Nothing Then
                                            FailCount = FailCount + 1
                                            Continue For
                                        Else
                                            ICBeneficiaryManagementController.ApproveBene(BeneID.ToString(), chkSelect.Checked, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                            PassCount = PassCount + 1
                                            Continue For
                                        End If
                                    End If
                                Else
                                    If objBene.BeneficiaryCode = "" Or objBene.BeneficiaryCode Is Nothing Then
                                        FailCount = FailCount + 1
                                        Continue For
                                    End If
                                    ICBeneficiaryManagementController.ApproveBene(BeneID.ToString(), chkSelect.Checked, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                    PassCount = PassCount + 1
                                    Continue For
                                End If
                            End If
                        End If
                    Next

                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Approve Beneficiary", "[" & FailCount.ToString() & "] Beneficiary can not be approve due to following reasons : <br /> 1. Beneficiary is approve.<br /> 2. Beneficiary must be approve by the user other than the maker.<br /> 3. Beneficiary code is not defined.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Approve Beneficiary", "[" & PassCount.ToString() & "] Beneficiary approve successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Approve Beneficiary", "[" & PassCount.ToString() & "] Beneficiary approve successfully. [" & FailCount.ToString() & "] Beneficiary can not be approve due to following reasons : <br /> 1. Beneficiary is approve.<br /> 2. Beneficiary must be approve by the user other than the maker.<br /> 3. Beneficiary code is not defined.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Approve Beneficiary", "Please select atleast one(1) Beneficiary.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnDeleteBene_Click(sender As Object, e As EventArgs) Handles btnDeleteBene.Click
        If Page.IsValid Then
            Try
                Dim rowgvdelBene As GridDataItem
                Dim chkSelect As CheckBox
                Dim BeneID As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0

                If CheckgvBeneForProcessAll() = True Then
                    For Each rowgvdelBene In gvBene.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvdelBene.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            BeneID = rowgvdelBene("BeneID").Text.ToString()
                            Dim objBene As New ICBene
                            If objBene.LoadByPrimaryKey(BeneID.ToString()) Then
                                Try
                                    If Me.UserInfo.IsSuperUser = False Then
                                        If objBene.IsApproved = False Then
                                            'ICBeneficiaryManagementController.DeleteBeneProductType(BeneID)
                                            ICBeneficiaryManagementController.DeleteBene(BeneID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                            PassCount = PassCount + 1
                                        Else
                                            FailCount = FailCount + 1
                                        End If
                                    Else
                                        ICBeneficiaryManagementController.DeleteBene(BeneID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                        PassCount = PassCount + 1
                                    End If
                                Catch ex As Exception
                                    If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                        FailCount = FailCount + 1
                                    End If
                                End Try
                            End If
                        End If
                    Next
                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Beneficiary", "[" & FailCount.ToString() & "] Beneficiary can not be deleted due to following reasons : <br /> 1. Beneficiary must be delete by the user other than the approver.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Beneficiary", "[" & PassCount.ToString() & "] Beneficiary deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Delete Beneficiary", "[" & PassCount.ToString() & "] Beneficiary deleted successfully. [" & FailCount.ToString() & "] Beneficiary can not be deleted due to following reasons : <br /> 1. Beneficiary must be delete by the user other than the approver.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Delete Beneficiary", "Please select atleast one(1) Beneficiary.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        If Page.IsValid Then
            Try
                Dim dt As New DataTable
                dt = ICBeneficiaryManagementController.GetBeneDetailsForExport()
                gvBene.DataSource = dt
                gvBene.AutoGenerateColumns = True
                gvBene.ExportSettings.ExportOnlyData = True
                gvBene.MasterTableView.AllowPaging = False
                gvBene.ExportSettings.IgnorePaging = True
                gvBene.PageSize = 500
                gvBene.MasterTableView.GetColumn("SelectAll").Visible = False
                gvBene.MasterTableView.GetColumn("BeneID").Visible = False
                gvBene.MasterTableView.GetColumn("BeneGroupName").Visible = False
                gvBene.MasterTableView.GetColumn("BeneCode").Visible = False
                gvBene.MasterTableView.GetColumn("BeneficiaryName").Visible = False
                gvBene.MasterTableView.GetColumn("PaymentNature").Visible = False
                gvBene.MasterTableView.GetColumn("IsActive").Visible = False
                gvBene.MasterTableView.GetColumn("IsApproved").Visible = False
                gvBene.MasterTableView.GetColumn("Edit").Visible = False
                gvBene.MasterTableView.GetColumn("Delete").Visible = False
                gvBene.Rebind()
                gvBene.ExportSettings.OpenInNewWindow = True
                gvBene.ExportSettings.FileName = "Beneficiary Details " & Date.Now.ToString("dd-MMM-yyyy")
                gvBene.MasterTableView.ExportToExcel()

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Protected Sub gvBene_ExportCellFormatting(sender As Object, e As ExportCellFormattingEventArgs) Handles gvBene.ExportCellFormatting
        If e.FormattedColumn.UniqueName.ToString() = "BeneficiaryCode" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If
        If e.FormattedColumn.UniqueName.ToString() = "BeneficiaryAccountNo" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If
        If e.FormattedColumn.UniqueName.ToString() = "BeneficiaryName" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If
        If e.FormattedColumn.UniqueName.ToString() = "BeneAccountBranchCode" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If
        If e.FormattedColumn.UniqueName.ToString() = "BeneficiaryBankCode" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If
        If e.FormattedColumn.UniqueName.ToString() = "BeneficiaryBranchCode" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If
        If e.FormattedColumn.UniqueName.ToString() = "BeneficiaryCNIC" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If
        If e.FormattedColumn.UniqueName.ToString() = "BeneficiaryMobile" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If
        If e.FormattedColumn.UniqueName.ToString() = "BeneficiaryPhone" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If
        If e.FormattedColumn.UniqueName.ToString() = "BeneficiaryEmail" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If
        If e.FormattedColumn.UniqueName.ToString() = "BeneficiaryAddress" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If
        If e.FormattedColumn.UniqueName.ToString() = "UBPReferenceNo" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If
        If e.FormattedColumn.UniqueName.ToString() = "BeneficiaryIDNo" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If
    End Sub

#End Region

#Region "Drop Down"

    'Protected Sub ddlBeneName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBeneGroup.SelectedIndexChanged
    '    Try
    '        LoadBeneficiaries(True)
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

    Private Sub LoadddlBene()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlBeneGroup.Items.Clear()
            ddlBeneGroup.Items.Add(lit)
            ddlBeneGroup.AppendDataBoundItems = True
            ddlBeneGroup.DataSource = ICBeneficiaryGroupManagementController.GetAllActiveBeneGroup()
            ddlBeneGroup.DataValueField = "BeneGroupCode"
            ddlBeneGroup.DataTextField = "BeneGroupName"
            ddlBeneGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

#Region "Grid View Events"

    ''************ Comment work is Delete Approval Coding ********************

    'Protected Sub btnDeleteBene_Click(sender As Object, e As EventArgs) Handles btnDeleteBene.Click
    '    Try
    '        Dim rowgvdelBene As GridDataItem
    '        Dim chkSelect As CheckBox
    '        Dim BeneID As String = ""
    '        Dim PassCount As Integer = 0
    '        Dim FailCount As Integer = 0

    '        If CheckgvBeneDelForProcessAll() = True Then
    '            For Each rowgvdelBene In gvBeneDelete.Items
    '                chkSelect = New CheckBox
    '                chkSelect = DirectCast(rowgvdelBene.Cells(0).FindControl("chkSelect"), CheckBox)
    '                If chkSelect.Checked = True Then
    '                    BeneID = rowgvdelBene("BeneID").Text.ToString()
    '                    Dim objBene As New ICBene
    '                    If objBene.LoadByPrimaryKey(BeneID.ToString()) Then
    '                        Try
    '                            If Me.UserInfo.IsSuperUser = False Then
    '                                If objBene.IsApproved = False Then
    '                                    If objBene.ApprovalforDeletionStatus = True Then
    '                                        If objBene.ApprovalByforDeletion <> Me.UserId Then
    '                                            ICBeneficiaryManagementController.DeleteBene(BeneID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
    '                                            PassCount = PassCount + 1
    '                                        Else
    '                                            FailCount = FailCount + 1
    '                                        End If
    '                                    Else
    '                                        FailCount = FailCount + 1
    '                                    End If
    '                                Else
    '                                    FailCount = FailCount + 1
    '                                End If
    '                            Else
    '                                ICBeneficiaryManagementController.DeleteBene(BeneID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
    '                                PassCount = PassCount + 1
    '                            End If
    '                        Catch ex As Exception
    '                            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
    '                                FailCount = FailCount + 1
    '                            End If
    '                        End Try
    '                    End If
    '                End If
    '            Next
    '            If PassCount = 0 Then
    '                UIUtilities.ShowDialog(Me, "Delete Beneficiary", "[" & FailCount.ToString() & "] Beneficiary can not be deleted due to following reasons : <br /> 1. Beneficiary must be delete by the user other than the approver.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
    '                Exit Sub
    '            End If
    '            If FailCount = 0 Then
    '                UIUtilities.ShowDialog(Me, "Delete Beneficiary", "[" & PassCount.ToString() & "] Beneficiary deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
    '                Exit Sub
    '            End If
    '            UIUtilities.ShowDialog(Me, "Delete Beneficiary", "[" & PassCount.ToString() & "] Beneficiary deleted successfully. [" & FailCount.ToString() & "] Beneficiary can not be deleted due to following reasons : <br /> 1. Beneficiary must be delete by the user other than the approver.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
    '        Else
    '            UIUtilities.ShowDialog(Me, "Delete Beneficiary", "Please select atleast one(1) Beneficiary.", ICBO.IC.Dialogmessagetype.Warning)
    '        End If
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

    'Protected Sub btnDeleteApprovalBene_Click(sender As Object, e As EventArgs) Handles btnDeleteApprovalBene.Click
    '    Try
    '        Try
    '            Dim rowgvBene As GridDataItem
    '            Dim chkSelect As CheckBox
    '            Dim BeneID As String = ""
    '            Dim PassCount As Integer = 0
    '            Dim FailCount As Integer = 0
    '            Dim objBene As New ICBene

    '            If CheckgvBeneForProcessAll() = True Then
    '                For Each rowgvBene In gvBene.Items
    '                    chkSelect = New CheckBox
    '                    chkSelect = DirectCast(rowgvBene.Cells(0).FindControl("chkSelect"), CheckBox)
    '                    If chkSelect.Checked = True Then
    '                        BeneID = rowgvBene("BeneID").Text.ToString()
    '                        If objBene.LoadByPrimaryKey(BeneID.ToString()) Then
    '                            If objBene.ApprovalforDeletionStatus = False Then
    '                                If Me.UserInfo.IsSuperUser = False Then
    '                                    If objBene.IsApproved Then
    '                                        FailCount = FailCount + 1
    '                                        Continue For
    '                                    Else
    '                                        ICBeneficiaryManagementController.ApprovedBeneforDeleteApproval(objBene.BeneID, Me.UserId, Me.UserInfo.Username)
    '                                        PassCount = PassCount + 1
    '                                        Continue For
    '                                    End If
    '                                Else
    '                                    ICBeneficiaryManagementController.ApprovedBeneforDeleteApproval(objBene.BeneID, Me.UserId, Me.UserInfo.Username)
    '                                    PassCount = PassCount + 1
    '                                    Continue For
    '                                End If
    '                            Else
    '                                FailCount = FailCount + 1
    '                                Continue For
    '                            End If
    '                        End If
    '                    End If
    '                Next

    '                If PassCount = 0 Then
    '                    UIUtilities.ShowDialog(Me, "Delete Approval Beneficiary", "[" & FailCount.ToString() & "] Beneficiary can not be approve due to following reasons : <br /> 1. Beneficiary is approved and can not be sent for delete approval.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
    '                    Exit Sub
    '                End If
    '                If FailCount = 0 Then
    '                    UIUtilities.ShowDialog(Me, "Delete Approval Beneficiary", "[" & PassCount.ToString() & "] Beneficiary is sent for delete approval.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
    '                    Exit Sub
    '                End If
    '                UIUtilities.ShowDialog(Me, "Delete Approval Beneficiary", "[" & PassCount.ToString() & "] Beneficiary is sent for delete approval. [" & FailCount.ToString() & "] Beneficiary can not be approve due to following reasons : <br /> 1. Beneficiary is approved and can not be sent for delete approval.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
    '            Else
    '                UIUtilities.ShowDialog(Me, "Delete Approval Beneficiary", "Please select atleast one(1) Beneficiary.", ICBO.IC.Dialogmessagetype.Warning)
    '            End If
    '        Catch ex As Exception
    '            ProcessModuleLoadException(Me, ex, False)
    '            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '        End Try
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

    'Protected Sub btnCancelBeneApproval_Click(sender As Object, e As EventArgs) Handles btnCancelBeneApproval.Click
    '    Try
    '        Dim rowgvdelBene As GridDataItem
    '        Dim chkSelect As CheckBox
    '        Dim BeneID As String = ""
    '        Dim PassCount As Integer = 0
    '        Dim FailCount As Integer = 0

    '        If CheckgvBeneDelForProcessAll() = True Then
    '            For Each rowgvdelBene In gvBeneDelete.Items
    '                chkSelect = New CheckBox
    '                chkSelect = DirectCast(rowgvdelBene.Cells(0).FindControl("chkSelect"), CheckBox)
    '                If chkSelect.Checked = True Then
    '                    BeneID = rowgvdelBene("BeneID").Text.ToString()
    '                    Dim objBene As New ICBene
    '                    If objBene.LoadByPrimaryKey(BeneID.ToString()) Then
    '                        ICBeneficiaryManagementController.RejectBeneforDeleteApproval(objBene.BeneID, Me.UserId, Me.UserInfo.Username)
    '                        PassCount = PassCount + 1
    '                    End If
    '                End If
    '            Next
    '            If FailCount = 0 Then
    '                UIUtilities.ShowDialog(Me, "Cancel Approval Beneficiary", "[" & PassCount.ToString() & "] Beneficiary is cancelled for deletion.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
    '                Exit Sub
    '            End If
    '        Else
    '            UIUtilities.ShowDialog(Me, "Cancel Approval Beneficiary", "Please select atleast one(1) Beneficiary.", ICBO.IC.Dialogmessagetype.Warning)
    '        End If
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

    'Private Function CheckgvBeneDelForProcessAll() As Boolean
    '    Try
    '        Dim rowgvBeneDel As GridDataItem
    '        Dim chkSelect As CheckBox
    '        Dim Result As Boolean = False
    '        For Each rowgvBeneDel In gvBeneDelete.Items
    '            chkSelect = New CheckBox
    '            chkSelect = DirectCast(rowgvBeneDel.Cells(0).FindControl("chkSelect"), CheckBox)
    '            If chkSelect.Checked = True Then
    '                Result = True
    '                Exit For
    '            End If
    '        Next
    '        Return Result
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Function

    'Private Sub LoadBeneficiaries(ByVal IsBind As Boolean)
    '    Try
    '        ICBeneficiaryManagementController.GetgvBene(ddlBene.SelectedValue.ToString(), Me.gvdelBene.CurrentPageIndex + 1, Me.gvdelBene.PageSize, Me.gvdelBene, IsBind)
    '        If gvdelBene.Items.Count > 0 Then
    '            lblBeneList.Visible = True
    '            lblBeneRNF.Visible = False
    '            btnExport.Visible = True
    '            gvdelBene.Visible = True

    '            btnDeleteApprovalBene.Visible = CBool(htRights("Approve"))
    '            btnApproveBene.Visible = CBool(htRights("Approve"))
    '            btnExport.Visible = CBool(htRights("Export"))
    '            gvdelBene.Columns(8).Visible = CBool(htRights("Delete"))
    '        Else
    '            lblBeneList.Visible = True
    '            gvdelBene.Visible = False
    '            btnApproveBene.Visible = False
    '            btnDeleteApprovalBene.Visible = False
    '            lblBeneRNF.Visible = True
    '            btnExport.Visible = False
    '        End If

    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

    'Protected Sub gvBeneDelete_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvBeneDelete.ItemCommand
    '    Try
    '        If e.CommandName = "DelReject" Then
    '            Dim objBene As New ICBene
    '            If objBene.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
    '                ICBeneficiaryManagementController.RejectBeneforDeleteApproval(objBene.BeneID, Me.UserId, Me.UserInfo.Username)
    '                UIUtilities.ShowDialog(Me, "Cancel Approval Beneficiary", "Beneficiary is cancelled for deletion.", ICBO.IC.Dialogmessagetype.Success)
    '                LoadBeneficiaries(True)
    '                LoadDeleteBeneficiaries(True)
    '                LoadddlBene()
    '            End If
    '        End If

    '        If e.CommandName = "DelApprove" Then
    '            Dim objBene As New ICBene
    '            If objBene.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
    '                If Me.UserInfo.IsSuperUser = False Then
    '                    If objBene.ApprovalforDeletionStatus = True Then
    '                        If objBene.ApprovalByforDeletion <> Me.UserId Then
    '                            ICBeneficiaryManagementController.DeleteBene(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
    '                            UIUtilities.ShowDialog(Me, "Delete Beneficiary", "Beneficiary deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
    '                            LoadBeneficiaries(True)
    '                            LoadDeleteBeneficiaries(True)
    '                            LoadddlBene()
    '                        Else
    '                            UIUtilities.ShowDialog(Me, "Delete Beneficiary", "Beneficiary must be delete by the user other than the approver.", ICBO.IC.Dialogmessagetype.Failure)
    '                        End If
    '                    End If
    '                Else
    '                    ICBeneficiaryManagementController.DeleteBene(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
    '                    UIUtilities.ShowDialog(Me, "Delete Beneficiary", "Beneficiary deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
    '                    LoadBeneficiaries(True)
    '                    LoadDeleteBeneficiaries(True)
    '                    LoadddlBene()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
    '            UIUtilities.ShowDialog(Me, "Delete Beneficiary", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
    '            Exit Sub
    '        End If
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

    'Protected Sub gvBeneDelete_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvBeneDelete.ItemCreated
    '    Try
    '        If TypeOf (e.Item) Is GridPagerItem Then
    '            Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
    '            myPageSizeCombo.Items.Clear()
    '            Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
    '            For x As Integer = 0 To UBound(arrPageSizes)
    '                Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
    '                myPageSizeCombo.Items.Add(myRadComboBoxItem)
    '                myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvBeneDelete.MasterTableView.ClientID)
    '            Next
    '            myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
    '        End If
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

    'Protected Sub gvBeneDelete_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvBeneDelete.NeedDataSource
    '    LoadDeleteBeneficiaries(False)
    'End Sub

    'Protected Sub gvBeneDelete_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvBeneDelete.PageIndexChanged
    '    gvBeneDelete.CurrentPageIndex = e.NewPageIndex
    'End Sub

    'Private Sub LoadDeleteBeneficiaries(ByVal IsBind As Boolean)
    '    Try
    '        ICBeneficiaryManagementController.GetgvBeneDelete(Me.gvBeneDelete.CurrentPageIndex + 1, Me.gvBeneDelete.PageSize, Me.gvBeneDelete, IsBind)
    '        If gvBeneDelete.Items.Count > 0 Then
    '            lblBeneDelList.Visible = True
    '            lblDelBeneRNF.Visible = False
    '            gvBeneDelete.Visible = True

    '            btnDeleteBene.Visible = CBool(htRights("Delete"))
    '            btnCancelBeneApproval.Visible = CBool(htRights("Approve"))
    '            gvBeneDelete.Columns(7).Visible = CBool(htRights("Approve"))
    '            gvBeneDelete.Columns(8).Visible = CBool(htRights("Approve"))
    '        Else
    '            lblBeneDelList.Visible = False
    '            gvBeneDelete.Visible = False
    '            btnDeleteBene.Visible = False
    '            btnCancelBeneApproval.Visible = False
    '            lblDelBeneRNF.Visible = False
    '        End If

    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

    'Protected Sub gvBene_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvdelBene.ItemCommand
    '    Try
    '        If e.CommandName = "del" Then
    '            Dim objBene As New ICBene
    '            If objBene.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
    '                If Me.UserInfo.IsSuperUser = False Then
    '                    If objBene.IsApproved = True Then
    '                        UIUtilities.ShowDialog(Me, "Delete Approval Beneficiary", "Beneficiary is approved and can not be sent for delete approval.", ICBO.IC.Dialogmessagetype.Failure)
    '                    Else
    '                        ICBeneficiaryManagementController.ApprovedBeneforDeleteApproval(objBene.BeneID, Me.UserId, Me.UserInfo.Username)
    '                        UIUtilities.ShowDialog(Me, "Delete Approval Beneficiary", "Beneficiary is sent for delete approval.", ICBO.IC.Dialogmessagetype.Success)
    '                        LoadBeneficiaries(True)
    '                        LoadDeleteBeneficiaries(True)
    '                        LoadddlBene()
    '                    End If
    '                Else
    '                    ICBeneficiaryManagementController.ApprovedBeneforDeleteApproval(objBene.BeneID, Me.UserId, Me.UserInfo.Username)
    '                    UIUtilities.ShowDialog(Me, "Delete Approval Beneficiary", "Beneficiary is sent for delete approval.", ICBO.IC.Dialogmessagetype.Success)
    '                    LoadBeneficiaries(True)
    '                    LoadDeleteBeneficiaries(True)
    '                    LoadddlBene()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
    '            UIUtilities.ShowDialog(Me, "Delete Approval Beneficiary", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
    '            Exit Sub
    '        End If
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

    'Protected Sub gvBeneDelete_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvBeneDelete.ItemDataBound
    '    Dim chkProcessAll As CheckBox

    '    If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
    '        chkProcessAll = New CheckBox
    '        chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
    '        chkProcessAll.Attributes("onclick") = "checkAllCheckboxesforDelBeneGrid('" & chkProcessAll.ClientID & "','" & gvBeneDelete.MasterTableView.ClientID & "','0');"
    '    End If

    '    If (e.Item.ItemType = Telerik.Web.UI.GridItemType.Item) Or (e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem) Then
    '        ' If gvBeneDelete.Items.Count > 0 Then
    '        Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
    '        Dim BeneAddedVia As String = Nothing
    '        Dim BeneStatus As String = Nothing
    '        Dim IsColorChange As Boolean = False
    '        BeneAddedVia = item.GetDataKeyValue("BeneficiaryAddedVia").ToString()
    '        BeneStatus = item.GetDataKeyValue("BeneficiaryStatus").ToString()

    '        If (BeneAddedVia.ToString().Trim.ToLower = "beneficiary management" Or BeneAddedVia.ToString().Trim.ToLower = "beneficiary bulk management") Then
    '            IsColorChange = False  'item.Style.Add("background-color", "#74BA61")
    '        Else
    '            IsColorChange = True
    '        End If
    '        If IsColorChange = True Then
    '            item.Style.Add("background-color", "#74BA61")
    '        End If
    '        If BeneStatus <> "Ready for approve" Then
    '            Dim chkSelect As New CheckBox
    '            chkSelect = DirectCast(item.Cells(0).FindControl("chkSelect"), CheckBox)
    '            chkSelect.Enabled = False
    '            chkSelect.Checked = False
    '        End If
    '        'End If
    '    End If

    'End Sub

    Protected Sub gvBene_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvBene.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvBene.MasterTableView.ClientID & "','0');"
        End If

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Or e.Item.ItemType = Telerik.Web.UI.GridItemType.EditItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Dim IsColorChange As Boolean = False
            Dim BeneAddedVia As String = Nothing
            Dim BeneStatus As String = Nothing

            BeneAddedVia = item.GetDataKeyValue("BeneficiaryAddedVia").ToString()
            BeneStatus = item.GetDataKeyValue("BeneficiaryStatus").ToString()

            If (BeneAddedVia.ToString().Trim.ToLower = "beneficiary management" Or BeneAddedVia.ToString().Trim.ToLower = "beneficiary bulk management") Then
                IsColorChange = False  'item.Style.Add("background-color", "#74BA61") 
            Else
                IsColorChange = True
            End If
            If IsColorChange = True Then
                item.Style.Add("background-color", "#74BA61 !important")
            End If
            'If BeneStatus <> "Ready for approve" Then
            '    Dim chkSelect As New CheckBox
            '    chkSelect = DirectCast(item.Cells(0).FindControl("chkSelect"), CheckBox)
            '    chkSelect.Enabled = False
            '    chkSelect.Checked = False
            'End If
        End If
    End Sub

    Protected Sub gvBene_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvBene.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvBene.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvBene_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvBene.NeedDataSource
        LoadBeneficiaries(False)
    End Sub

    Protected Sub gvBene_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvBene.PageIndexChanged
        gvBene.CurrentPageIndex = e.NewPageIndex

    End Sub

    Protected Sub gvBene_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvBene.ItemCommand
        Try
            If e.CommandName = "del" Then
                Dim objBene As New ICBene
                If objBene.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                    If Me.UserInfo.IsSuperUser = False Then
                        If objBene.IsApproved = True Then
                            UIUtilities.ShowDialog(Me, "Delete Beneficiary", "Beneficiary is approved and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        Else
                            'ICBeneficiaryManagementController.DeleteBeneProductType(e.CommandArgument.ToString())
                            ICBeneficiaryManagementController.DeleteBene(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                            UIUtilities.ShowDialog(Me, "Delete Beneficiary", "Beneficiary deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                            LoadBeneficiaries(True)
                            LoadddlBene()
                        End If
                    Else
                        ICBeneficiaryManagementController.DeleteBene(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        UIUtilities.ShowDialog(Me, "Delete Beneficiary", "Beneficiary deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                        LoadBeneficiaries(True)
                        LoadddlBene()
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Delete Beneficiary", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Function CheckgvBeneForProcessAll() As Boolean
        Try
            Dim rowgvBene As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvBene In gvBene.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvBene.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Private Sub LoadBeneficiaries(ByVal IsBind As Boolean)
        Try
            Dim BeneName As String = ""
            If txtBeneName.text <> "" Then
                BeneName = txtBeneName.text
            End If

            GetgvBene(ddlBeneGroup.SelectedValue.ToString(), Me.gvBene.CurrentPageIndex + 1, Me.gvBene.PageSize, Me.gvBene, IsBind, BeneName)
            If gvBene.Items.Count > 0 Then
                lblBeneList.Visible = True
                lblBeneRNF.Visible = False
                gvBene.Visible = True

                btnApproveBene.Visible = CBool(htRights("Approve"))
                btnExport.Visible = CBool(htRights("Export"))
                btnDeleteBene.Visible = CBool(htRights("Delete"))
                gvBene.Columns(9).Visible = CBool(htRights("Delete"))
            Else
                lblBeneList.Visible = True
                gvBene.Visible = False
                btnApproveBene.Visible = False
                btnExport.Visible = False
                btnDeleteBene.Visible = False
                lblBeneRNF.Visible = True
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


#End Region
    Private Sub GetgvBene(ByVal BeneCode As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal BeneName As String)
        Dim qryBene As New ICBeneQuery("qryBene")
        Dim qryPN As New ICPaymentNatureQuery("qryPN")
        Dim qryBeneGroup As New ICBeneGroupQuery("qryBeneGroup")

        If Not pagenumber = 0 Then
            qryBene.Select(qryBene.BeneID, qryBene.BeneficiaryCode, qryBene.BeneficiaryName, qryPN.PaymentNatureName, qryBene.BeneficiaryAddedVia)
            qryBene.Select(qryBene.IsActive, qryBene.IsApproved, qryBene.ApprovalforDeletionStatus, qryBene.BeneficiaryStatus, qryBeneGroup.BeneGroupName)
            qryBene.InnerJoin(qryPN).On(qryBene.PaymentNatureCode = qryPN.PaymentNatureCode)
            qryBene.InnerJoin(qryBeneGroup).On(qryBene.BeneGroupCode = qryBeneGroup.BeneGroupCode)
            'qryBene.Where(qryBene.ApprovalforDeletionStatus = False Or qryBene.ApprovalforDeletionStatus.IsNull)
            If BeneCode <> "0" Then
                qryBene.Where(qryBene.BeneGroupCode = BeneCode)
            End If
            If BeneName <> "" Then
                qryBene.Where(qryBene.BeneficiaryName.Cast(EntitySpaces.DynamicQuery.esCastType.String).ToLower.Like("%" & BeneName.ToLower & "%"))
            End If
            qryBene.OrderBy(qryBene.BeneID.Descending)
            rg.DataSource = qryBene.LoadDataTable()

            If DoDataBind Then
                rg.DataBind()
            End If
        End If
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If Page.IsValid Then
            Try
                LoadBeneficiaries(True)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

   
   
    Protected Sub ddlBeneGroup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlBeneGroup.SelectedIndexChanged
        Try
            LoadBeneficiaries(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    
End Class
