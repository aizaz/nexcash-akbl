﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Entities.Portals
Imports DotNetNuke.Security.Roles
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Partial Class DesktopModules_BulkTitleFetch_BulkTitleFetch
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
  
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("dtAccounts") = Nothing
                btnShow.Visible = CBool(htRights("Show"))
                gvBulkTitleFetch.Style.Add("display", "none")
                gvInstructions.Style.Add("display", "none")
                btnExport.Visible = False
                btnFetch.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Dim userCtrl As New RoleController
        Dim iUserRole As New UserRoleInfo
        Try
            btnFetch.Visible = False
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Bulk Title Fetch")
            ViewState("htRights") = htRights
          
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnShow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShow.Click
        If Page.IsValid Then
            Try
                Dim FileExtention As String = System.IO.Path.GetExtension(fuplBulkAccountsFile.PostedFile.FileName)
                Dim connectionString As String = ""
                Dim FileName As String = fuplBulkAccountsFile.FileName
                Dim FileLocation As String = Server.MapPath("~/UploadedFiles/") & FileName.ToString()

                Dim aFileInfo As IO.FileInfo
                'Save file on HDD.
                fuplBulkAccountsFile.SaveAs(FileLocation)
                ''Check whether file extension is xls or xslx
                If FileExtention = ".xls" Then
                    connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FileLocation & "; Extended Properties='Excel 12.0; HDR=Yes; IMEX=1;'"
                ElseIf FileExtention = ".xlsx" Then
                    connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FileLocation & "; Extended Properties='Excel 12.0 Xml; HDR=Yes; IMEX=1;'"
                Else
                    UIUtilities.ShowDialog(Me, "Bulk Title Fetch", "Invalid file format. Upload only .xls or .xlsx file.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                'Create OleDB Connection and OleDb Command
                Dim con As New OleDbConnection(connectionString)
                Dim cmd As New OleDbCommand()
                cmd.CommandType = System.Data.CommandType.Text
                cmd.Connection = con
                Dim dAdapter As New OleDbDataAdapter(cmd)
                Dim dtExcelRecords As New DataTable()
                con.Open()
                Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim getExcelSheetName As String = "Sheet1$"
                cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
                dAdapter.SelectCommand = cmd
                dAdapter.Fill(dtExcelRecords)
                con.Close()
                ViewState("dtAccounts") = dtExcelRecords
                'Delete file from HDD.
                aFileInfo = New IO.FileInfo(FileLocation)
                If aFileInfo.Exists Then
                    aFileInfo.Delete()
                End If

                If dtExcelRecords.Columns.Count <> 4 Then
                    ViewState("dtAccounts") = Nothing
                    UIUtilities.ShowDialog(Me, "Bulk Title Fetch", "No. of columns do not match", Dialogmessagetype.Failure, NavigateURL())
                    Exit Sub
                End If

                gvInstructions.DataSource = Nothing
                gvInstructions.DataSource = dtExcelRecords
                gvInstructions.DataBind()
                If gvInstructions.Items.Count > 0 Then
                    gvInstructions.Style.Remove("display")
                    btnShow.Visible = False
                    btnFetch.Visible = CBool(htRights("Title Fetch"))
                    btnExport.Visible = False
                Else
                    gvInstructions.Style.Add("display", "none")
                    btnShow.Visible = True
                    btnFetch.Visible = False
                    btnExport.Visible = False
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                btnShow.Visible = True
            End Try
        End If
    End Sub
    Protected Sub gvSOA_ExportCellFormatting(ByVal sender As Object, ByVal e As Telerik.Web.UI.ExportCellFormattingEventArgs) Handles gvBulkTitleFetch.ExportCellFormatting
        If e.FormattedColumn.UniqueName.ToString() = "AccountNumber" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If
        'If e.FormattedColumn.UniqueName.ToString() = "TransferAccount" Then
        '    e.Cell.Style("mso-number-format") = "\@"
        'End If
    End Sub
    Protected Sub btnFetch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFetch.Click
        Dim dtFinal As New DataTable
        Dim dtAccounts As New DataTable
        Dim drFinal, drAccounts As DataRow
        Dim BankName, AccTitle, Remarks As String
        Dim SRRN As String = Nothing
        Dim CustomerID As String = Nothing
        Dim objNPBank As ICBank
        Dim PBankIMD As String = Nothing

        'If Page.IsValid Then
        Try
                dtFinal.Columns.Add(New DataColumn("BankIMD", GetType(System.String)))
                dtFinal.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
                dtFinal.Columns.Add(New DataColumn("BankName", GetType(System.String)))
                dtFinal.Columns.Add(New DataColumn("AccountTitle", GetType(System.String)))
                dtFinal.Columns.Add(New DataColumn("FetchedAccountTitle", GetType(System.String)))
                dtFinal.Columns.Add(New DataColumn("Remarks", GetType(System.String)))

                If Not ViewState("dtAccounts") Is Nothing Then
                    dtAccounts = DirectCast(ViewState("dtAccounts"), DataTable)
                    'ViewState("dtAccounts") = Nothing

                    For Each drAccounts In dtAccounts.Rows
                        BankName = ""
                        AccTitle = ""
                        Remarks = ""
                        drFinal = dtFinal.NewRow()
                        If drAccounts(0).ToString() <> "" Or drAccounts(1).ToString() <> "" Then
                            drFinal("BankIMD") = drAccounts(0).ToString()
                            BankName = ICBankController.GetPrincipalBankNameByBankIMD(drAccounts(0).ToString())
                            If BankName.ToString() = "-" Then
                                drFinal("BankName") = "-"
                                drFinal("AccountNumber") = CStr(drAccounts(1).ToString()).ToString()
                                drFinal("AccountTitle") = drAccounts(2).ToString()
                                drFinal("FetchedAccountTitle") = "-"
                                Remarks = "Invalid Bank IMD."
                            Else
                                drFinal("BankName") = BankName.ToString()
                                drFinal("AccountNumber") = CStr(drAccounts(1).ToString()).ToString()
                                drFinal("AccountTitle") = drAccounts(2).ToString()
                                Try
                                    If ICBankController.IsPrincipalBank("", BankName.ToString) = True Then
                                        If drAccounts(1).ToString <> "" Then
                                            If CBUtilities.IsNormalAccountNoOrIBAN(drAccounts(1).ToString().Trim) Then
                                                If CBUtilities.ValidateIBAN(drAccounts(1).ToString().Trim) Then
                                                    Try
                                                        AccTitle = CBUtilities.TitleFetch(drAccounts(1).ToString().Trim, ICBO.CBUtilities.AccountType.RB, "Bulk Title Fetch", drAccounts(1).ToString().Trim)
                                                        drFinal("FetchedAccountTitle") = AccTitle.ToString()
                                                        Remarks = "-"
                                                    Catch ex As Exception
                                                        drFinal("FetchedAccountTitle") = "Failed to fetch account title due to " & ex.Message.ToString
                                                        Remarks = "-"
                                                    End Try
                                                Else
                                                    drFinal("FetchedAccountTitle") = "-"
                                                    Remarks = "Invalid International Bank Account Number (IBAN)"
                                                End If
                                            Else
                                                Try
                                                    AccTitle = ""
                                                    AccTitle = CBUtilities.TitleFetch(drAccounts(1).ToString().Trim, ICBO.CBUtilities.AccountType.RB, "Bulk Title Fetch", drAccounts(1).ToString().Trim)
                                                    drFinal("FetchedAccountTitle") = AccTitle.ToString()
                                                    Remarks = "-"
                                                Catch ex As Exception
                                                    Remarks = "Invalid Account Number"
                                                End Try
                                            End If
                                        Else
                                            drFinal("FetchedAccountTitle") = "-"
                                            Remarks = "Invalid Account Number"
                                        End If
                                    Else
                                        If drAccounts(1).ToString <> "" Then
                                            objNPBank = New ICBank
                                            objNPBank = ICBankController.GetBankByName(BankName)

                                            If objNPBank Is Nothing Then
                                                drFinal("FetchedAccountTitle") = "-"
                                                Remarks = "Invalid Non-Principal Bank"
                                                dtFinal.Rows.Add(drFinal)
                                                Continue For
                                            End If
                                            PBankIMD = Nothing
                                            PBankIMD = ICBankController.GetPrincipleBank.BankIMD
                                            If PBankIMD Is Nothing Then
                                                drFinal("FetchedAccountTitle") = "-"
                                                Remarks = "Invalid Principal Bank IMD"
                                                dtFinal.Rows.Add(drFinal)
                                                Continue For
                                            End If
                                            If drAccounts(3).ToString = "" Then
                                                drFinal("FetchedAccountTitle") = "-"
                                                Remarks = "Invalid Principal Bank Reference Account No"
                                                dtFinal.Rows.Add(drFinal)
                                                Continue For
                                            End If
                                            If CBUtilities.IsNormalAccountNoOrIBAN(drAccounts(1).ToString().Trim) Then
                                                If CBUtilities.ValidateIBAN(drAccounts(1).ToString().Trim) Then
                                                    If drAccounts(0).ToString <> "" Then
                                                        If drAccounts(1).ToString <> "" Then
                                                            Try
                                                                CustomerID = Nothing
                                                                CustomerID = ICUtilities.GetCustomerIDCountFromICSettings
                                                                SRRN = Nothing
                                                                AccTitle = ""
                                                                AccTitle = CBUtilities.IBFTTitleFetch(drAccounts(0).ToString().Trim, drAccounts(1).ToString().Trim, ICBO.CBUtilities.AccountType.RB, "Bulk Title Fetch", drAccounts(1).ToString().Trim, "", "", 0, "", SRRN, CustomerID, objNPBank.BankID, objNPBank.BankName, drAccounts(3).ToString, PBankIMD)
                                                                drFinal("FetchedAccountTitle") = AccTitle.ToString()
                                                                Remarks = "-"
                                                            Catch ex As Exception
                                                                drFinal("FetchedAccountTitle") = "Failed to fetch account title due to " & ex.Message.ToString
                                                                Remarks = "-"
                                                            End Try
                                                        Else
                                                            drFinal("FetchedAccountTitle") = AccTitle.ToString()
                                                            Remarks = "Invalid Account Number"
                                                        End If
                                                    Else
                                                        drFinal("FetchedAccountTitle") = "-"
                                                        Remarks = "Invalid Bank IMD"
                                                    End If
                                                Else
                                                    drFinal("FetchedAccountTitle") = "-"
                                                    Remarks = "Invalid International Bank Account Number (IBAN)"
                                                End If
                                            Else
                                                If drAccounts(0).ToString <> "" Then
                                                    Try
                                                        CustomerID = Nothing
                                                        CustomerID = ICUtilities.GetCustomerIDCountFromICSettings
                                                        SRRN = Nothing
                                                        AccTitle = ""
                                                        AccTitle = CBUtilities.IBFTTitleFetch(drAccounts(0).ToString().Trim, drAccounts(1).ToString().Trim, ICBO.CBUtilities.AccountType.RB, "Bulk Title Fetch", drAccounts(1).ToString().Trim, "", "", 0, "", SRRN, CustomerID, objNPBank.BankID, objNPBank.BankName, drAccounts(3).ToString, PBankIMD)
                                                        drFinal("FetchedAccountTitle") = AccTitle.ToString()
                                                        Remarks = "-"
                                                    Catch ex As Exception
                                                        drFinal("FetchedAccountTitle") = "-"
                                                        Remarks = "Failed to fetch title due to " & ex.Message.ToString
                                                    End Try
                                                Else
                                                    drFinal("FetchedAccountTitle") = "-"
                                                    Remarks = "Invalid Bank IMD"
                                                End If
                                            End If
                                        Else
                                            drFinal("FetchedAccountTitle") = "-"
                                            Remarks = "Invalid Account Number"
                                        End If
                                    End If

                                Catch ex As Exception
                                    drFinal("FetchedAccountTitle") = "-"
                                    Remarks = "Unable to fetch title due to [" & ex.Message.ToString() & "]"
                                End Try
                            End If
                            drFinal("Remarks") = Remarks.ToString()
                            dtFinal.Rows.Add(drFinal)
                        End If
                    Next
                    gvBulkTitleFetch.DataSource = Nothing
                    gvBulkTitleFetch.DataSource = dtFinal
                    gvBulkTitleFetch.DataBind()
                    If gvBulkTitleFetch.Items.Count > 0 Then
                        gvBulkTitleFetch.Style.Remove("display")
                        gvInstructions.Style.Add("display", "none")
                        btnFetch.Visible = False
                        btnExport.Visible = CBool(htRights("Export Excel"))
                    Else
                        gvBulkTitleFetch.Style.Add("display", "none")
                        gvInstructions.Style.Add("display", "none")
                        btnFetch.Visible = False
                        btnExport.Visible = False
                    End If

                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                btnFetch.Visible = True
            End Try
        'End If
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        'If Page.IsValid Then
        Try
            gvBulkTitleFetch.ExportSettings.ExportOnlyData = True
            gvBulkTitleFetch.ExportSettings.IgnorePaging = True
            gvBulkTitleFetch.ExportSettings.OpenInNewWindow = True
            gvBulkTitleFetch.ExportSettings.FileName = "Account Titles"
            gvBulkTitleFetch.MasterTableView.ExportToExcel()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
        'End If
    End Sub
    Public Sub ExporttoExcel()
        Try

            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=AccountTitles.xls")
            Response.Charset = ""
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Response.ContentType = "application/vnd.xls"
            Dim stringWrite As New System.IO.StringWriter
            Dim htmlWrite As New HtmlTextWriter(stringWrite)
            Dim tblExcel As Table = DirectCast(gvBulkTitleFetch.Controls(0), Table)
            tblExcel.RenderControl(htmlWrite)
            ' gvInstructions.RenderControl(htmlWrite)
            Response.Write(stringWrite.ToString())
            Response.Flush()
            Response.End()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        Try
                Response.Redirect(NavigateURL(41))
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub
End Class
