﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data
Imports System.Data.OleDb

Partial Class DesktopModules_BulkTitleFetch_TitleFetchInBulk
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private PNCode As String
    Private ArrRights As ArrayList
#Region "Page Load"
    Protected Sub DesktopModules_PaymentNature_SavePaymentNature_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Page.IsPostBack = False Then
                LoadGVBulkInstructions()
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
    Private Sub LoadGVBulkInstructions()

        'dtDa = ICAccountPaymentNatureProductTypeController.GetAllAccountPaymentNatureProductType(ddlCompany.SelectedValue.ToString())
        'ViewState("DataTable") = dtDa

        Dim connectionString As String = ""
        Dim fileLocation As String = Server.MapPath("~/Excel Sheets For Grids/BulkTitleFetch.xlsx")

        'Check whether file extension is xls or xslx

        'If fileExtension = ".xls" Then
        '    connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
        'ElseIf fileExtension = ".xlsx" Then
        connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
        'End If

        'Create OleDB Connection and OleDb Command

        Dim con As New OleDbConnection(connectionString)
        Dim cmd As New OleDbCommand()
        cmd.CommandType = System.Data.CommandType.Text
        cmd.Connection = con
        Dim dAdapter As New OleDbDataAdapter(cmd)
        Dim dtExcelRecords As New DataTable()
        con.Open()
        Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
        Dim getExcelSheetName As String = "Sheet1$"
        cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
        dAdapter.SelectCommand = cmd
        dAdapter.Fill(dtExcelRecords)
        con.Close()
        gvBulkTitleFetch.DataSource = dtExcelRecords

        gvBulkTitleFetch.DataBind()
    End Sub


End Class
