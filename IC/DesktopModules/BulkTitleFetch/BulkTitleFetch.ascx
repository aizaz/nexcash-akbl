﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BulkTitleFetch.ascx.vb"
    Inherits="DesktopModules_BulkTitleFetch_BulkTitleFetch" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<style type="text/css">
    .headingblue
    {
        font-weight: 700;
    }
</style>--%>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Bulk Title Fetch" CssClass="headingblue"></asp:Label>
                        <br />
                        Note:&nbsp; Fields marked as
                        <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        &nbsp;are required.
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblAccountsFile" runat="server" Text="Accounts File" CssClass="lbl"></asp:Label>
                        <asp:Label ID="Label27" runat="server" ForeColor="Red" Text="*"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 50%; font-weight: 700;" colspan="2">
                        <asp:FileUpload ID="fuplBulkAccountsFile" runat="server" />
            
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 15%">
                        <asp:RequiredFieldValidator ID="rfvFileUpload" runat="server" ErrorMessage="Please Select File."
                            ControlToValidate="fuplBulkAccountsFile"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Button ID="btnShow" runat="server" CssClass="btn" Text="Show" Width="75px" />
                        <asp:Button ID="btnFetch" runat="server" CssClass="btn" Text="Fetch Titles"
                            Width="90px" Visible="false" CausesValidation="False" />
                        <asp:Button ID="btnExport" runat="server" CssClass="btn" Text="Export" Width="90px"
                            Visible="false" CausesValidation="False" />
                        <asp:Button ID="btnCancel" runat="server" CssClass="btnCancel" Text="Cancel" 
                            CausesValidation="False" />
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <asp:Label ID="lblNRF" runat="server" Text="No Record Found." CssClass="headingblue"
                            Visible="false"></asp:Label>
                        <br />
                        <telerik:RadGrid ID="gvInstructions" runat="server" AllowPaging="false" AllowSorting="false"
                            AutoGenerateColumns="true" CellSpacing="0" ShowFooter="True" PageSize="500" CssClass="RadGrid">
                           <AlternatingItemStyle CssClass="rgAltRow" />
                           <ClientSettings>
                           <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                           </ClientSettings>
                           <MasterTableView TableLayout="Fixed"></MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                        
                        <telerik:RadGrid ID="gvBulkTitleFetch" runat="server" AllowPaging="false" AllowSorting="false"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="500" CssClass="RadGrid">
                           <AlternatingItemStyle CssClass="rgAltRow" />
                           <ClientSettings>
                           <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                           </ClientSettings>
                            <MasterTableView TableLayout="Fixed">
                             
                                <CommandItemSettings ExportToPdfText="Export to PDF" ></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="BankIMD" HeaderText="Bank IMD" SortExpression="BankIMD">
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="Account Number" SortExpression="AccountNumber">
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BankName" HeaderText="Bank Name" SortExpression="BankName">
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridBoundColumn>
                                   <telerik:GridBoundColumn DataField="AccountTitle" HeaderText="Account Title" SortExpression="AccountTitle">
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridBoundColumn>
                                      <telerik:GridBoundColumn DataField="FetchedAccountTitle" HeaderText="Fetched Account Title" SortExpression="FetchedAccountTitle">
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridBoundColumn>
                                      <telerik:GridBoundColumn DataField="Remarks" HeaderText="Remarks" SortExpression="Remarks">
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridBoundColumn>
                                </Columns>
                                 <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                        
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
