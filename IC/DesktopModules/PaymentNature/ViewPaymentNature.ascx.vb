﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_PaymentNature_ViewPaymentNature
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Dim dtDa As DataTable
    Private htRights As Hashtable

#Region "Page Load"
    Protected Sub DesktopModules_PaymentNature_ViewPaymentNature_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
          
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing
                btnAddPaymentNature.Visible = CBool(htRights("Add"))

                LoadPaymentNature(True)

                ViewState("SortExp") = Nothing
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Payment Nature Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region


    Private Sub LoadPaymentNature(ByVal IsBind As Boolean)
        Try
            ICPaymentNatureController.GetPaymentNaturesgv(gvPaymentNature.CurrentPageIndex + 1, gvPaymentNature.PageSize, gvPaymentNature, IsBind)

            If gvPaymentNature.Items.Count > 0 Then
                gvPaymentNature.Visible = True
                lblPaymentNaturesRNF.Visible = False
                btnDeletePaymentNatures.Visible = CBool(htRights("Delete"))
                gvPaymentNature.Columns(5).Visible = CBool(htRights("Delete"))
            Else
                gvPaymentNature.Visible = False
                btnDeletePaymentNatures.Visible = False
                lblPaymentNaturesRNF.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnAddPaymentNature_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPaymentNature.Click
        If Page.IsValid Then
            Response.Redirect(NavigateURL("SavePaymentNature", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If
    End Sub

    Private Function CheckgvPaymentNatureForProcessAll() As Boolean
        Try
            Dim rowgvPaymentNatures As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvPaymentNatures In gvPaymentNature.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvPaymentNatures.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnDeletePaymentNatures_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeletePaymentNatures.Click
        If Page.IsValid Then
            Try
                Dim rowgvPaymentNatures As GridDataItem
                Dim chkSelect As CheckBox
                Dim PaymentNaturesCode As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0

                If CheckgvPaymentNatureForProcessAll() = True Then
                    For Each rowgvPaymentNatures In gvPaymentNature.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvPaymentNatures.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            PaymentNaturesCode = rowgvPaymentNatures("PaymentNatureCode").Text.ToString()
                            Dim icPaymentNatures As New ICPaymentNature
                            icPaymentNatures.es.Connection.CommandTimeout = 3600
                            If icPaymentNatures.LoadByPrimaryKey(PaymentNaturesCode.ToString()) Then
                                Try
                                    ICPaymentNatureController.DeletePaymentNature(PaymentNaturesCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                    PassCount = PassCount + 1
                                Catch ex As Exception
                                    If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                        FailCount = FailCount + 1
                                    End If
                                End Try
                                ' End If
                            End If
                        End If

                    Next

                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Payment Natures", "[" & FailCount.ToString() & "] Payment Natures can not be deleted due to following reasones : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Payment Natures", "[" & PassCount.ToString() & "] Payment Natures deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Delete Payment Natures", "[" & PassCount.ToString() & "] Payment Natures deleted successfully. [" & FailCount.ToString() & "] Payment Natures can not be deleted due to following reasons : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Delete Payment Natures", "Please select atleast one(1) Payment Nature.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Protected Sub gvPaymentNature_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvPaymentNature.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvPaymentNature.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub gvPaymentNature_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvPaymentNature.ItemCommand
        Try

            If e.CommandName = "del" Then
                If Page.IsValid Then
                    ICPaymentNatureController.DeletePaymentNature(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    UIUtilities.ShowDialog(Me, "Delete Payment Nature", "Payment Nature deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                    LoadPaymentNature(True)
                End If
            End If

        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Deleted  Payment Nature", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvPaymentNature_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvPaymentNature.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvPaymentNature.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvPaymentNature_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvPaymentNature.NeedDataSource
        LoadPaymentNature(False)
    End Sub

    Protected Sub gvPaymentNature_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvPaymentNature.PageIndexChanged
        gvPaymentNature.CurrentPageIndex = e.NewPageIndex
    End Sub
End Class
