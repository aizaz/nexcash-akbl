﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals

Partial Class DesktopModules_PaymentNature_SavePaymentNature
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private PNCode As String
    Private htRights As Hashtable

#Region "Page Load"
    Protected Sub DesktopModules_PaymentNature_SavePaymentNature_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            PNCode = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing



                If PNCode.ToString() = "0" Then
                    Clear()
                    lblPageHeader.Text = "Add Payment Nature"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    lblPageHeader.Text = "Edit Payment Nature"
                    btnSave.Text = "Update"
                    txtPaymentNatureCode.ReadOnly = True
                    txtPaymentNatureName.Enabled = True
                    btnSave.Visible = CBool(htRights("Update"))
                    Dim ICPayMentNature As New ICPaymentNature
                    ICPayMentNature.es.Connection.CommandTimeout = 3600
                    ICPayMentNature.LoadByPrimaryKey(PNCode)
                    If ICPayMentNature.LoadByPrimaryKey(PNCode) Then
                        txtPaymentNatureCode.Text = ICPayMentNature.PaymentNatureCode.ToString()
                        txtPaymentNatureName.Text = ICPayMentNature.PaymentNatureName.ToString()
                        chkActive.Checked = ICPayMentNature.IsActive
                    End If

                End If
          
            End If
          If PNCode.ToString <> "0" Then
                Dim ICPayNature As New ICPaymentNature
                ICPayNature.es.Connection.CommandTimeout = 3600
                ICPayNature.LoadByPrimaryKey(PNCode)
            
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        txtPaymentNatureCode.Text = ""
        txtPaymentNatureName.Text = ""
        chkActive.Checked = True
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Payment Nature Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


#End Region

#Region "Button Events"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim ICPaymentNature As New ICPaymentNature
                ICPaymentNature.es.Connection.CommandTimeout = 3600

                If txtPaymentNatureName.Text.Trim = "" Then
                    UIUtilities.ShowDialog(Me, "Error", "Invalid payment nature name", Dialogmessagetype.Failure)
                    Exit Sub
                End If


                ICPaymentNature.PaymentNatureCode = txtPaymentNatureCode.Text.ToString()
                ICPaymentNature.PaymentNatureName = txtPaymentNatureName.Text.ToString().Trim
                ICPaymentNature.IsActive = chkActive.Checked

                If PNCode = "0" Then
                    ICPaymentNature.CreatedBy = Me.UserId
                    ICPaymentNature.CreateDate = Date.Now
                    ICPaymentNature.Creater = Me.UserId
                    ICPaymentNature.CreationDate = Date.Now
                    If CheckDuplicate(ICPaymentNature) = False Then
                        ICPaymentNatureController.AddPaymentNature(ICPaymentNature, False, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        UIUtilities.ShowDialog(Me, "Save Payment Nature", "Payment nature added successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Else
                        UIUtilities.ShowDialog(Me, "Save Payment Nature", "Can not add duplicate Payment Nature.", ICBO.IC.Dialogmessagetype.Failure)
                    End If
                Else

                    ICPaymentNature.CreatedBy = Me.UserId
                    ICPaymentNature.CreateDate = Date.Now

                    If CheckDuplicate(ICPaymentNature) = False Then
                        'ICPaymentNature.LoadByPrimaryKey(PNCode.ToString)
                        ICPaymentNature.PaymentNatureCode = PNCode
                        ICPaymentNatureController.AddPaymentNature(ICPaymentNature, True, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        UIUtilities.ShowDialog(Me, "Save Payment Nature", "Payment Nature updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    Else
                        UIUtilities.ShowDialog(Me, "Save Payment Nature", "Can not add duplicate Payment Nature Name.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                Clear()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub
    Private Function CheckDuplicate(ByVal ICPNature As ICPaymentNature) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim collPaymentNature As ICPaymentNatureCollection

            If PNCode = "0" Then
                collPaymentNature = New ICPaymentNatureCollection
                collPaymentNature.Query.Where(collPaymentNature.Query.PaymentNatureName.ToLower.Trim = ICPNature.PaymentNatureName.ToLower.Trim)
                collPaymentNature.Query.Load()
                If collPaymentNature.Count > 0 Then

                    rslt = True

                Else
                    collPaymentNature = New ICPaymentNatureCollection
                    collPaymentNature.Query.Where(collPaymentNature.Query.PaymentNatureCode.ToLower.Trim = ICPNature.PaymentNatureCode.ToLower.Trim)
                    collPaymentNature.Query.Load()
                    If collPaymentNature.Count > 0 Then

                        rslt = True
                    End If
                End If

            Else

                collPaymentNature = New ICPaymentNatureCollection
                collPaymentNature.Query.Where(collPaymentNature.Query.PaymentNatureName.ToLower.Trim = ICPNature.PaymentNatureName.ToLower.Trim And collPaymentNature.Query.PaymentNatureCode.ToLower.Trim <> ICPNature.PaymentNatureCode.ToLower.Trim)
                collPaymentNature.Query.Load()
                If collPaymentNature.Count > 0 Then

                    rslt = True
                End If

            End If

                Return rslt

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    'Private Function CheckDuplicate(ByVal ICPNature As ICPaymentNature) As Boolean
    '    Try
    '        Dim rslt As Boolean = False
    '        Dim ICPaymentNature As New ICPaymentNature
    '        Dim collPaymentNature As New ICPaymentNatureCollection

    '        ICPaymentNature.es.Connection.CommandTimeout = 3600
    '        collPaymentNature.es.Connection.CommandTimeout = 3600

    '        If PNCode = "0" Then
    '            If ICPaymentNature.LoadByPrimaryKey(ICPNature.PaymentNatureCode) Then
    '                rslt = True
    '            End If
    '            collPaymentNature.LoadAll()
    '            collPaymentNature.Query.Where(collPaymentNature.Query.PaymentNatureName.ToLower.Trim = ICPNature.PaymentNatureName.ToLower.Trim)
    '            If collPaymentNature.Query.Load() Then
    '                rslt = True
    '            End If


    '        Else

    '            collPaymentNature = New ICPaymentNatureCollection
    '            If ICPaymentNature.LoadByPrimaryKey(ICPNature.PaymentNatureCode) Then
    '                rslt = True
    '            End If
    '            collPaymentNature.LoadAll()

    '            collPaymentNature.Query.Where(collPaymentNature.Query.PaymentNatureName.ToLower.Trim = ICPNature.PaymentNatureName.ToLower.Trim)
    '            If collPaymentNature.Query.Load() Then
    '                rslt = True
    '            Else
    '                rslt = False
    '            End If

    '        End If

    '        Return rslt

    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Function

#End Region

End Class
