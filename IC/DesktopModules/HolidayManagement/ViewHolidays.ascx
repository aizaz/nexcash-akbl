﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewHolidays.ascx.vb"
    Inherits="DesktopModules_HolidayManagement_ViewHolidays" %>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure to remove this record ?") == true) {

            return true;
        }
        else {
            return false;
        }
    }
    function delcon(item) {


        if (window.confirm("Are you sure to remove this " + item + "?") == true) {

            return true;

        }
        else {

            return false;

        }
    }
</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table width="100%">
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnSaveHoliday" runat="server" Text="Add Holiday" 
                            CssClass="btn" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle">
                        <asp:Label ID="lblHolidayManagement" runat="server" Text="Holiday Management" 
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="left">
                        <asp:GridView ID="gvHoliday" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                            AllowPaging="True" AllowSorting="True" EnableModelValidation="True" 
                            Width="100%">
                            <AlternatingRowStyle CssClass="GridAltItem" />
                            <Columns>
                                <asp:BoundField DataField="HolidayName" HeaderText="Holiday" SortExpression="HolidayName">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="HolidayDate" HeaderText="Dated" 
                                    SortExpression="HolidayDate" DataFormatString="{0:dd-MMM-yyyy}" 
                                    HtmlEncode="False">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>                                
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveHolidays", "&mid=" & Me.ModuleId & "&hid="& Eval("HolidayID"))%>'
                                            ToolTip="Edit">Edit</asp:HyperLink>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("HolidayID")%>'
                                            CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                            ToolTip="Delete" />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="GridHeader" />
                            <PagerStyle CssClass="GridPager" />
                            <RowStyle CssClass="GridItem" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
