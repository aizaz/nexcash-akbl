﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Partial Class DesktopModules_HolidayManagement_SaveHolidays
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private HolidayID As String
    Private ArrRights As ArrayList
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            HolidayID = Request.QueryString("hid").ToString()
            If Page.IsPostBack = False Then
                sharedCalendar.RangeMinDate = Date.Now
                txtHolidayDate.SelectedDate = Date.Now
                If HolidayID = "0" Then
                    lblPageHeader.Text = "Add Holiday"
                    btnSave.Text = "Save"
                Else
                    lblPageHeader.Text = "Update Holiday"
                    btnSave.Text = "Update"
                    Dim cICHoliday As New ICHolidayManagement
                    If cICHoliday.LoadByPrimaryKey(HolidayID) Then
                        txtHolidayName.Text = cICHoliday.HolidayName
                        txtHolidayDate.SelectedDate = cICHoliday.HolidayDate
                        txtHolidayDescription.Text = cICHoliday.HolidayDescription
                    End If
                End If
            End If
            If CheckIsAdminOrSuperUser() = False Then
                ManagePageAccessRights()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ManagePageAccessRights()
        Try
            ArrRights = ICBankRoleRightsController.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Holiday Management")
            If ArrRights.Count > 0 Then
                If HolidayID.ToString() = "0" Then
                    btnSave.Visible = ArrRights(0)
                Else
                    btnSave.Visible = ArrRights(1)
                  
                End If
            Else
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return Result
        End Try
    End Function
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub
    Private Sub Clear()
        sharedCalendar.RangeMinDate = Date.Now
        txtHolidayDate.SelectedDate = Date.Now
        txtHolidayName.Text = Nothing
        txtHolidayDescription.Text = Nothing
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim cHoliday As New ICHolidayManagement
            Dim cHID As String = ""
            cHoliday.HolidayID = HolidayID
            cHoliday.HolidayName = txtHolidayName.Text.ToString()
            cHoliday.HolidayDate = txtHolidayDate.SelectedDate
            cHoliday.HolidayDescription = txtHolidayDescription.Text.ToString()

            If HolidayID.ToString() = "0" Then
                If ICHolidayManagementController.CheckDuplicateHoliday(cHoliday) = False Then
                    cHID = ICHolidayManagementController.SaveHoliday(cHoliday, False)
                    'ICUtilities.AddAuditTrail("Holiday : " & cHoliday.HolidayName.ToString() & " Added", "Holiday", cHID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    UIUtilities.ShowDialog(Me, "Save Holiday", "Holiday added successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Clear()
                Else
                    UIUtilities.ShowDialog(Me, "Save Holiday", "Cannot add duplicate Holiday.", ICBO.IC.Dialogmessagetype.Failure)
                End If
            Else
                cHID = ICHolidayManagementController.SaveHoliday(cHoliday, True)
                'ICUtilities.AddAuditTrail("Holiday : " & cHoliday.HolidayName.ToString() & " Updated", "Holiday", cHID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                UIUtilities.ShowDialog(Me, "Save Holiday", "Holiday updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
