﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Partial Class DesktopModules_HolidayManagement_ViewHolidays
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Dim dtDa As DataTable
#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If CheckIsAdminOrSuperUser() = False Then
                ManagePageAccessRights()
            End If
            If Page.IsPostBack = False Then
                LoadgvHolidays()
                ViewState("SortExp") = Nothing
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
#Region "Grid View Events"

    Public Property GridViewSortDirection() As SortDirection
        Get
            If ViewState("sortDirection") Is Nothing Then
                ViewState("sortDirection") = SortDirection.Descending
            ElseIf ViewState("sortDirection") = SortDirection.Ascending Then
                ViewState("sortDirection") = SortDirection.Descending
            ElseIf ViewState("sortDirection") = SortDirection.Descending Then
                ViewState("sortDirection") = SortDirection.Ascending
            End If

            Return DirectCast(ViewState("sortDirection"), SortDirection)
        End Get
        Set(ByVal value As SortDirection)
            ViewState("sortDirection") = value
        End Set
    End Property
    Private Sub SortGridView(ByVal sortExpression As String, ByVal direction As String)
        '  You can cache the DataTable for improving performance
        Try
            Dim dt As DataTable = ViewState("DataTable")
            Dim sortedDt As New DataTable
            dt.DefaultView.Sort = sortExpression + direction

            gvHoliday.DataSource = dt.DefaultView
            gvHoliday.DataBind()


            ViewState("dtGrid") = dt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub
    Protected Sub gvHoliday_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvHoliday.Sorting
        ViewState("SortExp") = e.SortExpression
        Dim sortExpression As String = CStr(ViewState("SortExp")).ToString()
        If GridViewSortDirection() = SortDirection.Ascending Then
            SortGridView(sortExpression, ASCENDING)
        Else
            SortGridView(sortExpression, DESCENDING)
        End If
    End Sub

    Protected Sub gvHoliday_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvHoliday.RowCommand
        Try
            If e.CommandName = "del" Then
                Dim ICHoliday As New ICHolidayManagement
                ICHoliday.es.Connection.CommandTimeout = 3600

                If ICHoliday.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                    ICHolidayManagementController.DeleteHoliday(ICHoliday.HolidayID)
                    'ICUtilities.AddAuditTrail("Holiday : " & ICHoliday.HolidayName.ToString() & " Deleted", "Holiday", ICHoliday.HolidayID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    UIUtilities.ShowDialog(Me, "Delete Holiday", "Holiday deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                    LoadgvHolidays()
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Deleted Holiday", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvHoliday_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvHoliday.PageIndexChanging
        gvHoliday.PageIndex = e.NewPageIndex
        If (Not ViewState("sortDirection") Is Nothing And Not ViewState("SortExp") Is Nothing) Then
            Dim sortExpression As String = CStr(ViewState("SortExp")).ToString()
            If GridViewSortDirection() = SortDirection.Ascending Then
                SortGridView(sortExpression, ASCENDING)
            Else
                SortGridView(sortExpression, DESCENDING)
            End If
        Else
            LoadgvHolidays()
        End If
    End Sub
    Protected Sub gvHoliday_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvHoliday.PageIndexChanged
        If (Not ViewState("sortDirection") Is Nothing And Not ViewState("SortExp") Is Nothing) Then
            Dim sortExpression As String = CStr(ViewState("SortExp")).ToString()
            If GridViewSortDirection() = SortDirection.Ascending Then
                SortGridView(sortExpression, ASCENDING)
            Else
                SortGridView(sortExpression, DESCENDING)
            End If
        Else
            LoadgvHolidays()
        End If
    End Sub
#End Region
#Region "Button Events"
    Protected Sub btnSaveHoliday_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveHoliday.Click
        Response.Redirect(NavigateURL("SaveHolidays", "&mid=" & Me.ModuleId & "&hid=0"), False)
    End Sub
#End Region
#Region "Other Functions/Routines"
    Private Sub ManagePageAccessRights()
        Try
            ArrRights = ICBankRoleRightsController.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Holiday Management")
            If ArrRights.Count > 0 Then
                btnSaveHoliday.Visible = ArrRights(0)
            Else
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            ' str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function
    Private Sub LoadgvHolidays()
        Try
            Dim collICHolidays As New ICHolidayManagementCollection

            collICHolidays.es.Connection.CommandTimeout = 3600
            collICHolidays = ICHolidayManagementController.GetAllHolidays()
            dtDa = collICHolidays.Query.LoadDataTable()
            ViewState("DataTable") = dtDa
            gvHoliday.DataSource = dtDa
            gvHoliday.DataBind()


            If CheckIsAdminOrSuperUser() = False Then
                If ArrRights.Count > 0 Then
                    gvHoliday.Columns(3).Visible = ArrRights(2)
                Else
                    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
End Class
