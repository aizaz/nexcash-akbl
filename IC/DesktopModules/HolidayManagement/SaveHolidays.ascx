﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveHolidays.ascx.vb" Inherits="DesktopModules_HolidayManagement_SaveHolidays" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }
   
</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
           <telerik:TextBoxSetting BehaviorID="reName" Validation-IsRequired="true" ErrorMessage="Invalid Holiday Name" EmptyMessage="Enter Holiday Name">
            <TargetControls>
                <telerik:TargetInput ControlID="txtHolidayName" />
            </TargetControls>
        </telerik:TextBoxSetting>            
         <telerik:TextBoxSetting BehaviorID="RagExpBehavior3" Validation-IsRequired="true" ErrorMessage="Invalid Holiday Description" EmptyMessage="Enter Holiday Description">
            <TargetControls>
                <telerik:TargetInput ControlID="txtHolidayDescription" />
            </TargetControls>
          </telerik:TextBoxSetting>             
          <telerik:TextBoxSetting BehaviorID="RagExpBehavior4" Validation-IsRequired="true" ErrorMessage="Invalid Holiday Date" EmptyMessage="Enter Holiday Date">
            <TargetControls>
                <telerik:TargetInput ControlID="txtHolidayDate" />
            </TargetControls>
          </telerik:TextBoxSetting>

</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblHolidayName" runat="server" Text="Holiday Name" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label8" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 28%">
            <asp:Label ID="lblHolidayDate" runat="server" Text="Select Holiday Date" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 22%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtHolidayName" runat="server" CssClass="txtbox" 
                MaxLength="100"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <telerik:RadDatePicker ID="txtHolidayDate" runat="server" 
                CssClass="dropdown" DateInput-DateFormat="dd-MMM-yyyy" 
                DateInput-EmptyMessage="-- Select --" ImagesPath=""  
                SharedCalendarID="sharedCalendar" >
            </telerik:RadDatePicker>
        </td>
        <td align="left" valign="top" style="width: 25%">
            <telerik:RadCalendar ID="sharedCalendar" runat="server" 
                EnableMultiSelect="false"  />
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 33%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 17%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblHolidayDescription" runat="server" Text="Holiday Description" 
                CssClass="lbl"></asp:Label><asp:Label
                ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtHolidayDescription" runat="server" CssClass="txtbox" MaxLength="500"
                TextMode="MultiLine" onkeypress="return textboxMultilineMaxNumber(this,500)"
                Height="70px"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False"
               />
            &nbsp;
        </td>
    </tr>
</table>
