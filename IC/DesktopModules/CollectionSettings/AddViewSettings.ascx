﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddViewSettings.ascx.vb" Inherits="DesktopModules_CollectionSettings_AddViewSettings" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadInputManager ID="rad" runat="server">
    <telerik:TextBoxSetting BehaviorID="radDomain" Validation-IsRequired="true" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDomain" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radPassword" Validation-IsRequired="false" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radServer" Validation-IsRequired="true" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtServer" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radUserName" Validation-IsRequired="true" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtUserName" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>

<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblHeading" runat="server" Text="Collection Settings" CssClass="headingblue"></asp:Label>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;          
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;     
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblEmailAddressSetupforInvoiceDBImport" runat="server" Text="Email Address Settings for Invoice DB Import" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%"></td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;          
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;     
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblServer" runat="server" Text="Server" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqServer" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblDomain" runat="server" Text="Domain" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqDomain" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtServer" runat="server" CssClass="txtbox"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtDomain" runat="server" CssClass="txtbox"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;          
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;     
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblUserName" runat="server" Text="User Name" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqUserName" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblPassword" runat="server" Text="Password" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqPassword" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtUserName" runat="server" CssClass="txtbox"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="txtbox"></asp:TextBox>
            <br />
            <asp:LinkButton ID="lnkPassword" runat="server" CausesValidation="False" Enabled="False">Change Password</asp:LinkButton>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;          
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;     
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblPOPayableAccountsSettings" runat="server" Text="PO Payable Account Settings" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%"></td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;          
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;     
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblPOPayableAccount" runat="server" Text="PO Payable Account" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqPOPayableAccount" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblPOPayableAccounts" runat="server" Text="Select PO Payable Account" CssClass="lbl" Visible = False></asp:Label>
            <asp:Label ID="lblReqPOPayableAccounts" runat="server" Text="*" ForeColor="Red" Visible = False></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlPOPayableAccount" runat="server" CssClass="dropdown" AutoPostBack ="true" >
                <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem Value="Central">Central PO Payable Account</asp:ListItem>
                <asp:ListItem Value="Branch Wise">Branch Wise PO Payable Account</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlPOPayableAccounts" runat="server" CssClass="dropdown" Visible = False>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlPOPayableAccount" runat="server" ControlToValidate="ddlPOPayableAccount"
                Display="Dynamic" ErrorMessage="Please select PO Payable Account" InitialValue="0"
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlPOPayableAccounts" runat="server" ControlToValidate="ddlPOPayableAccounts"
                Display="Dynamic" ErrorMessage="Please select PO Payable Account" InitialValue="0" Enabled="false"
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;          
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;     
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblValueDateClearingExecutionSettings" runat="server" Text="Value Date Clearing Execution Settings" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%"></td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;          
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;     
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblValueDateClearingProcess" runat="server" Text="Value Date Clearing Process" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqValueDateClearingProcess" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblValueDateClearingType" runat="server" Text="Value Date Clearing Type" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqValueDateClearing" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlValueDateClearingProcess" runat="server" CssClass="dropdown">
                <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem Value="Enable">Enable</asp:ListItem>
                <asp:ListItem Value="Disable">Disable</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlValueDateClearingType" runat="server" CssClass="dropdown" AutoPostBack="true">
                <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem Value="Auto">Auto Value Date Clearing</asp:ListItem>
                <asp:ListItem Value="Manual">Manual Value Date Clearing</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlValueDateClearingProcess" runat="server" ControlToValidate="ddlValueDateClearingProcess"
                Display="Dynamic" ErrorMessage="Please select Value Date Clearing Process" InitialValue="0"
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlValueDateClearingType" runat="server" ControlToValidate="ddlValueDateClearingType"
                Display="Dynamic" ErrorMessage="Please select Value Date Clearing Type" InitialValue="0"
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <asp:Panel ID="pnlValueDateExecutionTime" runat="server" Visible="false">
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:Label ID="lblValueDateExecutionTime" runat="server" Text="Execution Time" CssClass="lbl"></asp:Label>
                <asp:Label ID="lblReqValueDateExecutionTime" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;     
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <telerik:RadTimePicker ID="radtpValueDateExecutionTime" runat="server" CssClass="txtbox" StartTime="00:00:00" Interval="60:00:00" EndTime="23:00:00" Columns="3" Culture="en-gb" DateInput-EmptyMessage="-- Select --" ImagesPath="">
                </telerik:RadTimePicker>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;     
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:RequiredFieldValidator ID="rfvValueDateExecutionTime" runat="server" ControlToValidate="radtpValueDateExecutionTime"
                    Display="Dynamic" ErrorMessage="Please select Execution Time" InitialValue="" Enabled="false"
                    SetFocusOnError="True"></asp:RequiredFieldValidator>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;     
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
    </asp:Panel>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;          
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;     
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px" CausesValidation="true" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False" />
        </td>
    </tr>
</table>
