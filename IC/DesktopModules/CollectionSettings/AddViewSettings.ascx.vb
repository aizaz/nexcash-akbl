﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_CollectionSettings_AddViewSettings
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase

#Region "Page Load"
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                If ICUtilities.CheckIsAdminOrSuperUser(Me.PortalId, Me.UserId) Then
                    CheckFieldsExists()                                
                Else
                    UIUtilities.ShowDialog(Me, "Error", "You do not have access.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

#Region "Other Functions"
    Private Sub CheckFieldsExists()
        Try
            If Not ICUtilities.GetCollectionSettingValue("POPayableAccount") Is Nothing Then
                ddlPOPayableAccount.SelectedValue = ICUtilities.GetCollectionSettingValue("POPayableAccount")
                If Not ICUtilities.GetCollectionSettingValue("POPayableAccountType") Is Nothing And Not ICUtilities.GetCollectionSettingValue("POPayableAccountNumber") Is Nothing And Not ICUtilities.GetCollectionSettingValue("POPayableAccountBranchCode") Is Nothing And Not ICUtilities.GetCollectionSettingValue("POPayableAccountCurrency") Is Nothing Then
                    ddlPOPayableAccount_SelectedIndexChanged(ddlPOPayableAccount, Nothing)
                    LoadddlPOPayableAccount()
                    ddlPOPayableAccounts.SelectedValue = ICUtilities.GetCollectionSettingValue("POPayableAccountType") & "-" & ICUtilities.GetCollectionSettingValue("POPayableAccountNumber") & "-" & ICUtilities.GetCollectionSettingValue("POPayableAccountBranchCode") & "-" & ICUtilities.GetCollectionSettingValue("POPayableAccountCurrency") & "-" & "-" & "-" & "-" & "-" & "-"
                End If
            Else
                ddlPOPayableAccount.SelectedValue = "0"
            End If

            If Not ICUtilities.GetCollectionSettingValue("ValueDateClearingProcess") Is Nothing Then
                ddlValueDateClearingProcess.SelectedValue = ICUtilities.GetCollectionSettingValue("ValueDateClearingProcess")
            Else
                ddlValueDateClearingProcess.SelectedValue = "0"
            End If

            If Not ICUtilities.GetCollectionSettingValue("Server") Is Nothing Then
                txtServer.Text = ICUtilities.GetCollectionSettingValue("Server")
            Else
                txtServer.Text = Nothing
            End If

            If Not ICUtilities.GetCollectionSettingValue("Domain") Is Nothing Then
                txtDomain.Text = ICUtilities.GetCollectionSettingValue("Domain")
            Else
                txtDomain.Text = Nothing
            End If

            If Not ICUtilities.GetCollectionSettingValue("UserName") Is Nothing Then
                txtUserName.Text = ICUtilities.GetCollectionSettingValue("UserName")
            Else
                txtUserName.Text = Nothing
            End If

            If Not ICUtilities.GetCollectionSettingValue("Password") Is Nothing Then
                txtPassword.Text = ICUtilities.GetCollectionSettingValue("Password")
                If txtPassword.Text.Trim = "" Then
                    rad.GetSettingByBehaviorID("radPassword").Validation.IsRequired = True
                    txtPassword.Enabled = True
                    lnkPassword.Enabled = False
                Else
                    rad.GetSettingByBehaviorID("radPassword").Validation.IsRequired = False
                    txtPassword.Enabled = False
                    lnkPassword.Enabled = True
                End If
            Else
                txtPassword.Text = Nothing
                rad.GetSettingByBehaviorID("radPassword").Validation.IsRequired = True
                txtPassword.Enabled = True
                lnkPassword.Enabled = False
            End If

            ddlValueDateClearingType_SelectedIndexChanged(ddlValueDateClearingType, Nothing)

            If Not ICUtilities.GetCollectionSettingValue("ValueDateClearingType") Is Nothing Then
                ddlValueDateClearingType.SelectedValue = ICUtilities.GetCollectionSettingValue("ValueDateClearingType")
            Else
                ddlValueDateClearingType.SelectedValue = "0"
            End If

            If ICUtilities.GetCollectionSettingValue("ValueDateClearingExecutionTime") <> "" Then
                Dim SchedularTime As DateTime = DateTime.Parse(ICUtilities.GetCollectionSettingValue("ValueDateClearingExecutionTime"))
                radtpValueDateExecutionTime.SelectedDate = SchedularTime
            Else
                radtpValueDateExecutionTime.SelectedDate = Nothing
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

#Region "Button Events"
    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL(41), False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            Dim ActionStr As String = ""
            ActionStr = "Settings"
            Dim SchedularTime As DateTime
            If Not radtpValueDateExecutionTime.SelectedDate Is Nothing Then
                SchedularTime = radtpValueDateExecutionTime.SelectedDate.ToString()
            End If

            ICUtilities.SetCollectionSettingValue("POPayableAccount", ddlPOPayableAccount.SelectedValue.ToString())
            ICUtilities.SetCollectionSettingValue("ValueDateClearingProcess", ddlValueDateClearingProcess.SelectedValue.ToString())
            ICUtilities.SetCollectionSettingValue("Server", txtServer.Text)
            ICUtilities.SetCollectionSettingValue("Domain", txtDomain.Text)
            ICUtilities.SetCollectionSettingValue("UserName", txtUserName.Text)
            If txtPassword.Enabled = True Then
                ICUtilities.SetCollectionSettingValue("Password", txtPassword.Text)
            End If
            ICUtilities.SetCollectionSettingValue("ValueDateClearingType", ddlValueDateClearingType.SelectedValue.ToString())

            If ddlValueDateClearingType.SelectedValue = "Auto" Then
                ICUtilities.SetCollectionSettingValue("ValueDateClearingExecutionTime", SchedularTime.ToString("HH:mm:ss"))
            Else
                ICUtilities.SetCollectionSettingValue("ValueDateClearingExecutionTime", Nothing)
            End If

            If ddlPOPayableAccount.SelectedValue = "Central" Then
                ICUtilities.SetCollectionSettingValue("POPayableAccountType", ddlPOPayableAccounts.SelectedValue.Split("-")(0).ToString())
                ICUtilities.SetCollectionSettingValue("POPayableAccountNumber", ddlPOPayableAccounts.SelectedValue.Split("-")(1).ToString())
                ICUtilities.SetCollectionSettingValue("POPayableAccountBranchCode", ddlPOPayableAccounts.SelectedValue.Split("-")(2).ToString())
                ICUtilities.SetCollectionSettingValue("POPayableAccountCurrency", ddlPOPayableAccounts.SelectedValue.Split("-")(3).ToString())
            Else
                ICUtilities.SetCollectionSettingValue("POPayableAccountType", Nothing)
                ICUtilities.SetCollectionSettingValue("POPayableAccountNumber", Nothing)
                ICUtilities.SetCollectionSettingValue("POPayableAccountBranchCode", Nothing)
                ICUtilities.SetCollectionSettingValue("POPayableAccountCurrency", Nothing)
            End If

            UIUtilities.ShowDialog(Me, "Collection Settings", "Settings saved successfully.", ICBO.IC.Dialogmessagetype.Success)
            ICUtilities.AddAuditTrail(ActionStr & " Added ", "Collection Settings", Nothing, Me.UserId, Me.UserInfo.Username.ToString(), "ADD")

            txtPassword.Enabled = False
            lnkPassword.Enabled = True
            rad.GetSettingByBehaviorID("radPassword").Validation.IsRequired = False
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub lnkPassword_Click(sender As Object, e As EventArgs) Handles lnkPassword.Click
        txtPassword.Enabled = True
        rad.GetSettingByBehaviorID("radPassword").Validation.IsRequired = True
    End Sub

#End Region

    Protected Sub ddlValueDateClearingType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlValueDateClearingType.SelectedIndexChanged
        Try
            If ddlValueDateClearingType.SelectedValue = "Auto" Then
                pnlValueDateExecutionTime.Visible = True
                rfvValueDateExecutionTime.Enabled = True
            Else
                pnlValueDateExecutionTime.Visible = False
                rfvValueDateExecutionTime.Enabled = False
                radtpValueDateExecutionTime.Clear()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlPOPayableAccount()
        Try
            ddlPOPayableAccounts.Items.Clear()
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlPOPayableAccounts.Items.Add(lit)
            ddlPOPayableAccounts.AppendDataBoundItems = True
            ddlPOPayableAccounts.DataSource = ICOfficeController.GetOutwardClearingAccountByAccountType("Payable Account")
            ddlPOPayableAccounts.DataTextField = "AccountTitle"
            ddlPOPayableAccounts.DataValueField = "AccountNumber"
            ddlPOPayableAccounts.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlPOPayableAccount_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPOPayableAccount.SelectedIndexChanged
        Try
            If ddlPOPayableAccount.SelectedValue = "Central" Then
                lblPOPayableAccounts.Visible = True
                lblReqPOPayableAccounts.Visible = True
                ddlPOPayableAccounts.Visible = True
                rfvddlPOPayableAccounts.Enabled = True
                LoadddlPOPayableAccount()
            Else
                lblPOPayableAccounts.Visible = False
                lblReqPOPayableAccounts.Visible = False
                ddlPOPayableAccounts.Visible = False
                rfvddlPOPayableAccounts.Enabled = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
