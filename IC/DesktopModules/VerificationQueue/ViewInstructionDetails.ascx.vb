﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Partial Class DesktopModules_InstructionView_ViewInstructionDetails
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private InstructionID As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            InstructionID = Request.QueryString("InstructionID").ToString()
            'Typ = Request.QueryString("typ").ToString()
            If Page.IsPostBack = False Then
                LoaddvInsDetails(InstructionID)
            End If
            If dvViewInsDetails.Rows.Count = 0 Then
                lblRNF.Visible = True
            Else
                lblRNF.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoaddvInsDetails(ByVal InstructionID As String)
        Try

            dvViewInsDetails.DataSource = ICInstructionController.GetInstructionDetailsByInstructionID(InstructionID)
            dvViewInsDetails.DataBind()

            If dvViewInsDetails.Rows.Count > 0 Then
                dvViewInsDetails.Visible = True
                lblRNF.Visible = False
            Else
                dvViewInsDetails.Visible = False
                lblRNF.Visible = True
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub dvViewInsDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DetailsViewPageEventArgs) Handles dvViewInsDetails.PageIndexChanging
        dvViewInsDetails.PageIndex = e.NewPageIndex
        LoaddvInsDetails(InstructionID)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub
End Class
