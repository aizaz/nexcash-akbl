﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InstructionVerificationQueue.ascx.vb"
    Inherits="DesktopModules_InstructionStatus_InstructionStatus" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">


    function conVerify() {
        if (window.confirm("Are you sure you wish to verify selected instruction(s)?") == true) {
            var btnVerify = document.getElementById('<%=btnVerify.ClientID%>')
            btnVerify.value = "Processing...";
            btnVerify.disabled = true;

            var a = btnVerify.name;
            __doPostBack(a, '');
            return true;
        }
        else {
            return false;
        }
    }


    function conSendForAmend() {
        if (window.confirm("Are you sure you wish to send for amend selected instruction(s)?") == true) {
            var btnSendForAmend = document.getElementById('<%=btnSendForAmend.ClientID%>')
            btnSendForAmend.value = "Processing...";
            btnSendForAmend.disabled = true;

            var a = btnSendForAmend.name;
            __doPostBack(a, '');
            return true;
        }
        else {
            return false;
        }
    }

    function conBukDelete() {
        if (window.confirm("Are you sure you wish to cancel selected instruction(s)?") == true) {
            var btnCancel = document.getElementById('<%=btnCancelInstructions.ClientID%>')
            btnCancel.value = "Processing...";
            btnCancel.disabled = true;

            var a = btnCancel.name;
            __doPostBack(a, '');
            return true;
        }
        else {
            return false;
        }
    }
    function ReloadPage() {

        location.reload(true);
    }
    function DisAbleCancelButton() {
        var btnCancel = document.getElementById('<%=btnCancelInstructions.ClientID%>')

        btnCancel.value = "Processing...";
        btnCancel.disabled = true;


    }
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;

        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }

    $(document).ready(function () {

        // Dialog
        $('#urldialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#loading').hide();



            },
            resizable: false
        });
        // Summary Dialog
        $('#sumdialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#sumdialog').hide();
                //  location.reload(false);
                //                window.location.reload();
                //                window.location.reload();sa
            },
            resizable: false
        });

       

    });
    function showsumdialog(title, modal, width, height) {
        //  alert(modal);
        $('#sumdialog').dialog("option", "title", title);
        $('#sumdialog').dialog("option", "modal", modal);
        $('#sumdialog').dialog("option", "width", width);
        $('#sumdialog').dialog("option", "height", height);
        $('#sumdialog').dialog('open');
        $('#sumdialog').dialog();
        return false;

    };
    function CloseSummaryDialogBox() {
        $('#sumdialog').dialog('close');
    }

    function showurldialog(title, url, modal, width, height) {
        //  alert(modal);
        $('#urldialog').dialog("option", "title", title);
        $('#urldialog').dialog("option", "modal", modal);
        $('#urldialog').dialog("option", "width", width);
        $('#urldialog').dialog("option", "height", height);
        $('#urldialog').dialog('open');
        $('#dialogiframe').hide();
        $('#loading').show();
        $('#dialogiframe').attr("src", url);
        $('#dialogiframe').load(function () {
            $('#loading').hide();
            $('#dialogiframe').show();
        });
        return false;

    };

    function CloseIframe() {
        $('#urldialog').dialog('close');
    }

  

   
</script>
<script language="javascript" type="text/javascript">
    function AdjustWidth(ddl) {
        var maxWidth = 0;
        var Counter = 0;
        for (var i = 0; i < ddl.length; i++) {
            if (ddl.options[i].text.length > maxWidth) {
                Counter = Counter + 1;
                maxWidth = ddl.options[i].text.length;
            }
        }
        if (Counter = 1) { ddl.style.width = "250px"; }

        else {
            alert(Counter);
            ddl.style.width = maxWidth + "px";
        }
        //-->
    }
</script>
<style type="text/css">
    .ctrDropDown
    {
        width: 290px;
        font-size: 11px;
        height: 19px;
    }
    .ctrDropDownClick
    {
        width: 430px;
        font-size: 11px;
        height: 19px;
    }
    .plainDropDown
    {
        width: 290px;
        font-size: 11px;
        height: 19px;
    }
    .btnCancel2
    {}
</style>
<telerik:RadInputManager ID="RadInputManager3" runat="server" EnableEmbeddedSkins="false">
    <telerik:TextBoxSetting BehaviorID="reTnxCount" Validation-IsRequired="false" ErrorMessage="Transaction Count">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTnxCount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reTotalAmount" Validation-IsRequired="false"
        ErrorMessage="Total Amount">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTotalAmount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reCurrency" Validation-IsRequired="false" ErrorMessage="Currency">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCurrency" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rePaymentMode" Validation-IsRequired="false"
        ErrorMessage="Payment Mode">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPaymentMode" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reAvailableBalance" Validation-IsRequired="false"
        ErrorMessage="Available Balance">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAvailableBalance" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reInstructionNo" Validation-IsRequired="false"
        ErrorMessage="Enter Instruction No." ValidationExpression="[0-9]{0,8}">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstructionNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reInstrumentNo" Validation-IsRequired="false"
        ErrorMessage="Enter Instrument No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstrumentNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reReferenceNo" Validation-IsRequired="false"
        ErrorMessage="Enter Reference No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reRemarks" ErrorMessage="Enter Remarks" Validation-ValidationGroup="VerifyAmend" Validation-IsRequired="true">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRemarks" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {

                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
                else {

                    if (cell.childNodes[j].type == "checkbox") {
                        cell.childNodes[j].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }

                }
            }
        }
    }
</script>
<table align="left" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Instruction Verification Queue"
                            CssClass="lblHeadingForQueue"></asp:Label>
                        <%--    <br />
                        Note:&nbsp; Fields marked as
                        <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        &nbsp;are required.--%>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>--%>
                        <asp:Panel ID="pnlHeader" runat="server" DefaultButton="btnSearch">
                           
                            <table align="left" style="width: 100%">
                                <tr align="left" valign="top" style="width: 100%">
                                    <td align="left" valign="top" style="width: 43%">
                                        <table align="left" style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    &nbsp;
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:RadioButtonList ID="rbtnlstDateSelection" runat="server" RepeatDirection="Horizontal"
                                                        Font-Size="10px" AutoPostBack="True">
                                                        <asp:ListItem Value="Creation Date"> Creation Date</asp:ListItem>
                                                        <asp:ListItem Value="Value Date"> Value Date</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblDateFrom" runat="server" Text="Date From" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <telerik:RadDatePicker ID="raddpCreationFromDate" runat="server" Width="180px" AutoPostBack="true">
                                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                        </Calendar>
                                                        <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                            Font-Size="10.5px" LabelWidth="40%" type="text" value="">
                                                        </DateInput>
                                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                    </telerik:RadDatePicker>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%; height: 25%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblDateTo" runat="server" Text="Date To" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <telerik:RadDatePicker ID="raddpCreationToDate" runat="server" Width="180px" AutoPostBack="true">
                                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                        </Calendar>
                                                        <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                            LabelWidth="40%" type="text" value="" Font-Size="10.5px">
                                                        </DateInput>
                                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                    </telerik:RadDatePicker>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 45%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 25%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblCompany" runat="server" Text="Company" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdownForQueue" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblAccountPaymentNature" runat="server" Text="Account Payment Nature"
                                                        CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <div style="width: 290px; overflow: hidden;">
                                                        <%-- <asp:DropDownList ID="DropDownList1" runat="server" class="ctrDropDown"  onblur="this.className='ctrDropDown';" onmousedown="this.className='ctrDropDownClick';" onchange="this.className='ctrDropDown';" AutoPostBack="true">--%>
                                                        <%--</asp:DropDownList>--%>
                                                        <asp:DropDownList ID="ddlAcccountPaymentNature" runat="server" class="ctrDropDown"
                                                            onblur="this.className='ctrDropDown';" onmousedown="this.className='ctrDropDownClick';"
                                                            onchange="this.className='ctrDropDown';" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </div>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblBatch" runat="server" Text="Batch" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:DropDownList ID="ddlBatch" runat="server" CssClass="dropdownForQueue" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblProductType" runat="server" Text="Product Type" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:DropDownList ID="ddlProductType" runat="server" CssClass="dropdownForQueue"
                                                        AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblRemarks" runat="server" Text="Remarks" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:TextBox ID="txtRemarks" runat="server" Width="288px" CssClass="txtboxForQueue"
                                                        TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    &nbsp;
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:Button ID="btnVerify" runat="server" Text="Verify" CssClass="btn2" ValidationGroup="Verify"
                                                         OnClientClick ="javascript: return conVerify();" />
                                                    <asp:Button ID="btnSendForAmend" runat="server" Text="Send For Amendment" CssClass="btn2"
                                                        ValidationGroup="VerifyAmend" OnClientClick ="javascript: return conSendForAmend();" />
                                                    <asp:Button ID="btnCancelInstructions" runat="server" Text="Cancel Instructions"
                                                        OnClientClick="javascript: return conBukDelete();" CssClass="btnCancel2" ValidationGroup="Verify"/>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="right" valign="top" style="width: 57%">
                                        <table align="right" style="width: 100%">
                                            <tr>
                                                <td align="right" valign="top" style="width: 30%">
                                                    <fieldset class="fieldsetForIE7LeftTD">
                                                        <legend class="legendForIE7LeftTD">Transaction Summary</legend>
                                                        <table align="right" style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td align="center" valign="top" style="width: 100%">
                                                                    &nbsp;
                                                                    <asp:Label ID="lblTnxCount" runat="server" Text="Transaction Count" CssClass="lblForFieldSetLabels"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr align="left" valign="top" style="width: 100%">
                                                                <td align="center" valign="top" style="width: 100%">
                                                                    <asp:TextBox ID="txtTnxCount" runat="server" MaxLength="150" Width="120px" Font-Size="10px"
                                                                        ReadOnly="True"></asp:TextBox>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr valign="top" style="width: 100%">
                                                                <td align="center" valign="top" style="width: 100%">
                                                                    <asp:Label ID="lblTotalAmount" runat="server" Text="Total Amount" CssClass="lblForFieldSetLabels"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr align="center" valign="top" style="width: 100%">
                                                                <td align="center" valign="top" style="width: 100%">
                                                                    <asp:TextBox ID="txtTotalAmount" runat="server" MaxLength="150" Width="120px" Font-Size="10px"
                                                                        HtmlEncode="false" DataFormatString="{0:N2}" ReadOnly="true"></asp:TextBox><br />
                                                                </td>
                                                            </tr>
                                                            <tr align="center" valign="top" style="width: 100%">
                                                                <td align="center" valign="top">
                                                                    <asp:Button ID="btnSummary" runat="server" Text="Summary" CssClass="btn2" CausesValidation="False" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                                <td align="left" valign="top" style="width: 70%">
                                                    <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="left" valign="top" style="width: 100%">
                                                                <fieldset class="fieldsetForIE7RightTopTD">
                                                                    <legend class="legendForIE7RightTopTD">Search</legend>
                                                                    <table align="left" style="width: 100%">
                                                                        <tr align="left" valign="top" style="width: 100%">
                                                                            <td align="left" valign="top" style="width: 25%">
                                                                                <asp:Label ID="lblInstructionNo" runat="server" Text="Instruction No." CssClass="lblForFieldSetLabels"></asp:Label>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 25%">
                                                                                <asp:Label ID="lblInstrumentNo" runat="server" Text="Instrument No." CssClass="lblForFieldSetLabels"></asp:Label>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 25%">
                                                                                <asp:Label ID="Label2" runat="server" Text="Reference No." CssClass="lblForFieldSetLabels"></asp:Label>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 25%">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr align="left" valign="top" style="width: 100%">
                                                                            <td align="left" valign="top" style="width: 28%">
                                                                                <asp:TextBox ID="txtInstructionNo" runat="server" Width="120px" Font-Size="10px"
                                                                                    MaxLength="8"></asp:TextBox>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 28%">
                                                                                <asp:TextBox ID="txtInstrumentNo" runat="server" Width="120px" Font-Size="10px" MaxLength="18"></asp:TextBox>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 28%">
                                                                                <asp:TextBox ID="txtReferenceNo" runat="server" Width="120px" Font-Size="10px" MaxLength="18"></asp:TextBox>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 16%">
                                                                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn2" CausesValidation="False" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <%--                <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 45%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 25%; height: 1px">
                                                </td>
                                            </tr>--%>
                                                        <tr>
                                                            <td align="left" valign="top" style="width: 100%">
                                                                <fieldset class="fieldsetForIE7RightBottomTD">
                                                                    <legend class="legendForIE7RightBottomTD">Check Balance</legend>
                                                                    <table align="left" style="width: 100%">
                                                                        <tr align="left" valign="top" style="width: 100%">
                                                                            <td align="left" valign="top" style="width: 60%">
                                                                                <asp:Label ID="Label4" runat="server" Text="Select Account" CssClass="lblForFieldSetLabels"></asp:Label>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 40%">
                                                                                <asp:Label ID="lblAvailableBalance" runat="server" Text="Available Balance" CssClass="lblForFieldSetLabels"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr align="left" valign="top" style="width: 100%">
                                                                            <td align="left" valign="top" style="width: 60%">
                                                                                <asp:DropDownList ID="ddlAccount" runat="server" CssClass="dropdownForQueue2" AutoPostBack="True">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 40%">
                                                                                <asp:TextBox ID="txtAvailableBalance" runat="server" MaxLength="150" Width="120px"
                                                                                    Font-Size="10px" ReadOnly="true" HtmlEncode="false" DataFormatString="{0:N2}"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <%--  </ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <telerik:RadGrid ID="gvAuthorization" runat="server" CssClass="RadGrid" AllowPaging="True"
                            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                            PageSize="100">
                            <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="true" />
                            </ClientSettings>
                            <%-- <AlternatingItemStyle CssClass="rgAltRow" />--%>
                            <MasterTableView DataKeyNames="InstructionID,IsAmendmentComplete" TableLayout="Fixed"
                                NoMasterRecordsText="">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Checked="false" Text=""
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Checked="False" Text="" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Instruction No." HeaderStyle-Width="8%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbInstructionID" runat="server" Text='<%#Eval("InstructionID") %>'
                                                ToolTip="Instruction No." ForeColor="Blue" Enabled="false"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="FileBatchNo" HeaderText="Batch No" SortExpression="FileBatchNo">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridBoundColumn DataField="InstructionID" HeaderText="Instruction No." SortExpression="InstructionID">
                                    </telerik:GridBoundColumn>--%>
                                    <%-- <telerik:GridBoundColumn DataField="ReferenceNo" HeaderText="Reference No." SortExpression="ReferenceNo">
                                    </telerik:GridBoundColumn>--%>
                                    <telerik:GridBoundColumn DataField="CreateDate" HeaderText="Creation Date" SortExpression="CreateDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}" HeaderStyle-Width="8%">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ValueDate" HeaderText="Value Date" SortExpression="ValueDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}" HeaderStyle-Width="8%">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneficiaryName" HeaderText="Bene Name" SortExpression="BeneficiaryName">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Amount" HeaderText="Amount" SortExpression="Amount"
                                        HtmlEncode="false" DataFormatString="{0:N2}">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProductTypeCode" HeaderText="Payment Mode" SortExpression="ProductTypeCode">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InstrumentNo" HeaderText="Instrument No." SortExpression="InstrumentNo">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="OfficeName" HeaderText="Print Location" SortExpression="OfficeName">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneficiaryAccountNo" HeaderText="Account No."
                                        SortExpression="BeneficiaryAccountNo">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneAccountBranchCode" HeaderText="Branch Code"
                                        SortExpression="BeneAccountBranchCode">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                      <telerik:GridBoundColumn DataField="CompanyName" HeaderText="UBPS Company Name"
                                        SortExpression="CompanyName">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="UBPReferenceField" HeaderText="Consumer ID"
                                        SortExpression="UBPReferenceField">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                  <%--  <telerik:GridBoundColumn DataField="BeneAccountCurrency" HeaderText="Currency" SortExpression="BeneAccountCurrency">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>--%>
                                    <%--<telerik:GridTemplateColumn HeaderText="View Details">
                                     
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlDetails" runat="server" ImageUrl="~/images/view.gif" NavigateUrl='<%#NavigateURL("ViewInstructionDetails", "&mid=" & Me.ModuleId & "&InstructionID="& Eval("InstructionID"))%>'
                                                ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>
                                    <%-- <telerik:GridTemplateColumn HeaderText="View Details">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ibButton" runat="server" ImageUrl="~/images/view.gif" ToolTip="Instruction Details"
                                                CausesValidation="false" CommandArgument='<%#Eval("InstructionID") %>'></asp:ImageButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridTemplateColumn>--%>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <%--     <ItemStyle CssClass="rgRow" />--%>
                            <PagerStyle AlwaysVisible="True" />
                            <HeaderStyle CssClass="GridHeader" />
                            <ItemStyle CssClass="GridItem" />
                            <AlternatingItemStyle CssClass="GridItem" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                        <br />
                        <asp:Label ID="lblNRF" runat="server" Text="No Record Found." CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div id="sumdialog" title="" style="text-align: center">
    <asp:Panel ID="pnlShowSummary" runat="server" Style="display: none; width: 100%"
        ScrollBars="Vertical" Height="600px">
        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnCancel" OnClientClick="CloseSummaryDialogBox();return false;"/><br />
        <telerik:RadGrid ID="gvShowSummary" runat="server" AllowPaging="false" Width="540px"
            CssClass="RadGrid" AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0"
            ShowFooter="True" PageSize="100">
            <AlternatingItemStyle CssClass="rgAltRow" />
            <MasterTableView>
                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                </RowIndicatorColumn>
                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="InstructionID" HeaderText="Instruction No." SortExpression="InstructionID">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                    <%-- <telerik:GridBoundColumn DataField="ReferenceNo" HeaderText="Reference No." SortExpression="ReferenceNo">
                                    </telerik:GridBoundColumn>--%>
                    <telerik:GridBoundColumn DataField="Message" HeaderText="Message" SortExpression="Message">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
                <PagerStyle AlwaysVisible="True" />
            </MasterTableView>
            <ItemStyle CssClass="rgRow" />
            <PagerStyle AlwaysVisible="True" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
    </asp:Panel>
</div>
<div id="urldialog" title="">
    <iframe id='dialogiframe' frameborder="0" allowtransparency="true" src="" style="border-style: none;
        width: 100%; height: 100%; overflow-x: hidden; background-color: transparent;">
    </iframe>
    <div id='loading' style="display: none; margin: 0px auto; text-align: center; width: 100%;
        height: 100%; position: relative">
        <img src='../../../images/progressbar.gif' style="left: 45%; top: 50%; position: absolute;" />
    </div>
</div>
