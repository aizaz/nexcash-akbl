﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewFileTemplateFields.ascx.vb" Inherits="DesktopModules_FileTemplateFields_ViewFileTemplateFields" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .style1
    {
        width: 25%;
        height: 25px;
    }
    .style2
    {
        width: 15%;
    }
    .style3
    {
        width: 15%;
        height: 25px;
    }
    .style4
    {
        width: 25%;
        height: 23px;
    }
    .style5
    {
        width: 15%;
        height: 23px;
    }
    .style6
    {
        height: 141px;
    }
</style>
<telerik:radinputmanager ID="RadInputManager1" runat="server">
               <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior2" EmptyMessage="type.."
            Type="Number">
                    </telerik:NumericTextBoxSetting>       
                    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior2" Validation-IsRequired="true"
            ValidationExpression="^[a-zA-Z0-9 .-_/]*$" ErrorMessage="Invalid Character" EmptyMessage="Enter FileUploadTemplate Code">
            <TargetControls>
                <telerik:TargetInput ControlID="txtFileUploadTemplateCode" />
            </TargetControls>
        </telerik:RegExpTextBoxSetting>
        <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="true"
            ValidationExpression="^[a-zA-Z ]*$" ErrorMessage="Invalid Character" EmptyMessage="Enter FileUploadTemplate Name">
            <TargetControls>
                <telerik:TargetInput ControlID="txtFileUploadTemplateName" />
            </TargetControls>
        </telerik:RegExpTextBoxSetting>  
        <telerik:TextBoxSetting BehaviorID="RagExpBehavior3" Validation-IsRequired="false" EmptyMessage="Enter FileUploadTemplate Description">
            <TargetControls>
                <telerik:TargetInput ControlID="txtFileUploadTemplateDescription" />
            </TargetControls>
        </telerik:TextBoxSetting>  
        <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior4" Validation-IsRequired="true"
            ValidationExpression="^[a-zA-Z0-9]*$" ErrorMessage="Invalid Character" EmptyMessage="Enter FileUploadTemplate IMD">
            <TargetControls>
                <telerik:TargetInput ControlID="txtFileUploadTemplateIMD" />
            </TargetControls>
        </telerik:RegExpTextBoxSetting>
        <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior5" Validation-IsRequired="true"
            ValidationExpression="^[0-9- ]*$" ErrorMessage="Invalid Character" EmptyMessage="Enter FileUploadTemplate Phone No.">
            <TargetControls>
                <telerik:TargetInput ControlID="txtFileUploadTemplatePhoneNo" />
            </TargetControls>
        </telerik:RegExpTextBoxSetting>

         <telerik:TextBoxSetting BehaviorID="RagExpBehavior6" Validation-IsRequired="false" EmptyMessage="Enter FileUploadTemplate Website">
            <TargetControls>
                <telerik:TargetInput ControlID="txtFileUploadTemplateWebsite" />
            </TargetControls>
        </telerik:TextBoxSetting>  
                
    </telerik:radinputmanager>
    <script type = "text/javascript" language = "javascript">
            function numeralsOnly(evt)
            {
            evt = (evt) ? evt : event;
            var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : ((evt.which) ? evt.which : 0));
            if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode != 46))
            {
            alert("Enter numerals only in this field!");
            return false;
            }
        return true;
        function con() {
            if (window.confirm("Are you sure you wish to remove this Record?") == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function delcon(item) {
            if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
                return true;
            }
            else {
                return false;
            }
        }    
}
</script>
<table width="100%">
<tr align="left" valign="top" style="width: 100%">
        <td align="left"  valign="top" colspan="2">
            &nbsp;</td>
        <td align="left" valign="top" colspan="2">            &nbsp;</td>
    </tr>
    
    <tr>
        <td align="center" valign="top" colspan="4">
                        <asp:Label ID="lblAcqAgentListHeader0" runat="server" 
                            Text="File Upload Template Fields" CssClass="headingblue"></asp:Label>
                    </td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style4">
                        <asp:Label ID="lblAcqAgentListHeader1" runat="server" 
                            Text="Select File Upload Template" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" class="style4">
            &nbsp;</td>
        <td align="left" valign="top" class="style5">
            </td>
        <td align="left" valign="top" class="style4">
            </td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlUploadTemplate" runat="server" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" class="style2">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:RequiredFieldValidator ID="rfvAcqAgent" runat="server" 
                ControlToValidate="ddlUploadTemplate" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Template" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" class="style1">
            
            &nbsp;</td>
        <td align="left" valign="top" class="style3">
            </td>
        <td align="left" valign="top" class="style1">
            </td>
    </tr>
    

    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4" class="style6">
            <asp:GridView ID="gvTemplateField" runat="server" DataKeyNames="FieldID,isRequired,MustRequired" 
                AutoGenerateColumns="False" CssClass="Grid" EnableModelValidation="True" 
                Width="100%">
                <AlternatingRowStyle CssClass="GridAltItem" />
                <Columns>                
                    <asp:BoundField DataField="FieldName" HeaderText="Field Name" 
                        SortExpression="FieldName">
                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>            
                     <asp:CheckBoxField DataField="isRequired" HeaderText="Required">
                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:CheckBoxField>
                    <asp:BoundField DataField="FixLength" HeaderText="Fix Length" 
                        SortExpression="FixLength">
                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                     <asp:TemplateField>
                        <ItemTemplate>
                               <asp:ImageButton ID="btnmoveup0" runat="server"   CommandName="Up" CommandArgument='<%#Eval("FieldID") %>'
                                   ImageUrl="~/images/action_up.gif"  />
                        </ItemTemplate>                     
                    </asp:TemplateField>
                      <asp:TemplateField  >
                        <ItemTemplate>
                             <asp:ImageButton ID="btnmoveDown0" runat="server"    CommandName="Down" CommandArgument='<%#Eval("FieldID") %>'
                                 ImageUrl="~/images/action_down.gif"     />                               
                        </ItemTemplate>                     
                    </asp:TemplateField>
                       <asp:TemplateField  >
                        <ItemTemplate>
                             <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("FieldID") %>'
                                            CommandName="del" ImageUrl="~/images/delete.gif"
                                            ToolTip="Delete" />                                  
                        </ItemTemplate>                     
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="GridHeader" />
                <PagerStyle CssClass="GridPager" />
                <RowStyle CssClass="GridItem" />
            </asp:GridView>
        </td>
    </tr>
    

    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <asp:GridView ID="gvFixlength" runat="server" DataKeyNames="FieldID,isRequired"  
                AutoGenerateColumns="False" CssClass="Grid" EnableModelValidation="True" 
                Width="100%">
                <AlternatingRowStyle CssClass="GridAltItem" />
                <Columns>                  
                  <asp:TemplateField HeaderText="Select"  >
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSelect" runat="server" Checked='<%# Bind("isRequired") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                 
                    <asp:BoundField DataField="FieldName" HeaderText="Field Name" 
                        SortExpression="FieldName">
                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Required" SortExpression="Required">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkRequired" runat="server" Checked='<%# Bind("isRequired") %>' 
                                  />
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Field Length" >
                        <ItemTemplate>
                           <asp:TextBox ID="txtFieldLegth" runat="server" onkeypress="return numeralsOnly(event)" Text='<%# Bind("FieldDBLength") %>'></asp:TextBox>
                            <%--<asp:TextBox ID="txtFieldLegth" runat="server"></asp:TextBox>--%> 
                            <asp:RangeValidator runat="server" ID="RangeValidator1"   ControlToValidate="txtFieldLegth" Type="Integer"   Display="Dynamic" ErrorMessage = "Length is greater than Fixed." ForeColor="Red"   MinimumValue="1"  MaximumValue='<%# Eval("FieldDBLength") %>' Enabled="true"></asp:RangeValidator>                           
                        </ItemTemplate>                      
                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />                                        
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle CssClass="GridHeader" />
                <PagerStyle CssClass="GridPager" />
                <RowStyle CssClass="GridItem" />
            </asp:GridView>
        </td>
    </tr>
       <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            &nbsp;</td>
    </tr>

    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
    
   
          <asp:Button ID="btnSave" runat="server" Text="Save" Width="77px" CausesValidation="true" />
    
   
        </td>
    </tr>
     </table>

