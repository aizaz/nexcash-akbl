﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals

Partial Class DesktopModules_FileTemplateFields_ViewFileTemplateFields
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private FileUploadTemplateCode As String
    Private ArrRights As ArrayList
#Region "Page Load"
    Protected Sub DesktopModules_ViewFileUploadTemplaod_ViewFileUploadTemplate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If CheckIsAdminOrSuperUser() = False Then
                ManagePageAccessRights()
            End If
            If Page.IsPostBack = False Then
                LoadddlTemplate()
            End If
            If ddlUploadTemplate.SelectedValue.ToString() = "0" Then
                btnSave.Visible = False
            Else
                btnSave.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
#Region "Grid View Events"
    Protected Sub gvTemplateField_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTemplateField.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim IbtnDel As New ImageButton
            Try
                IbtnDel = DirectCast(e.Row.Cells(5).FindControl("IbtnDelete"), ImageButton)

                If CBool(gvTemplateField.DataKeys(e.Row.RowIndex)(2).ToString()) = True Then
                    IbtnDel.Visible = False
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub gvTemplateField_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvTemplateField.RowCommand
        '  Protected Sub Gridviewselectbus_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Dim i As Int16 = 1
        If e.CommandName = "del" Then
            '    Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim cat As New ICTemplateFields
            cat.es.Connection.CommandTimeout = 3600
            Dim order As Integer = 0

            If cat.LoadByPrimaryKey(Convert.ToInt64(e.CommandArgument.ToString()), ddlUploadTemplate.SelectedValue) Then
                order = cat.FieldOrder
                cat.MarkAsDeleted()
                cat.Save()
                Dim fieldcoll As New ICTemplateFieldsCollection
                fieldcoll.es.Connection.CommandTimeout = 3600

                fieldcoll.Query.Where(fieldcoll.Query.TemplateID = ddlUploadTemplate.SelectedValue And fieldcoll.Query.FieldOrder > order)
                fieldcoll.Query.Load()
                For Each cat In fieldcoll
                    MoveUp(Convert.ToInt64(cat.FieldID))
                Next
                ' fieldcoll.Save()
                LoadTemplateFields()
            End If
        End If



        If e.CommandName = "Up" Then
            '    Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            MoveUp(Convert.ToInt64(e.CommandArgument.ToString()))

        End If
        If e.CommandName = "Down" Then
            MoveDown(ddlUploadTemplate.SelectedValue, Convert.ToInt64(e.CommandArgument.ToString()))
        End If

    End Sub

    Protected Sub gvTemplateField_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvTemplateField.RowEditing

    End Sub
    Protected Sub gvFixlength_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvFixlength.RowEditing

    End Sub

    Protected Sub gvFixlength_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvFixlength.RowCommand
        '  Protected Sub Gridviewselectbus_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Dim i As Int16 = 1
        If e.CommandName = "del" Then
            '    Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim cat As New ICTemplateFields
            cat.es.Connection.CommandTimeout = 3600
            Dim order As Integer = 0
            'Dim acq As New FRCAcqAgentTemplateFieldCollection
            'acq.Query.Where(acq.Query.FieldID.Equals(Convert.ToInt64(e.CommandArgument.ToString())))
            'If acq.Query.Load Then
            '    UIUtilities.ShowDialog(Me, "Template Filed", "Cannot deleted : Template Filed have related data ", Intelligenes.FRC.Dialogmessagetype.Failure)
            'Else
            If cat.LoadByPrimaryKey(Convert.ToInt64(e.CommandArgument.ToString()), ddlUploadTemplate.SelectedValue) Then
                order = cat.FieldOrder
                cat.MarkAsDeleted()
                cat.Save()
                Dim fieldcoll As New ICTemplateFieldsCollection
                fieldcoll.es.Connection.CommandTimeout = 3600

                fieldcoll.Query.Where(fieldcoll.Query.TemplateID = ddlUploadTemplate.SelectedValue And fieldcoll.Query.FieldOrder = order)
                fieldcoll.Query.Load()
                For Each cat In fieldcoll
                    MoveUp(Convert.ToInt64(cat.FieldID))
                Next
                ' fieldcoll.Save()
                LoadTemplateFields()
            End If
            ' End If


        End If
        If e.CommandName = "Up" Then
            MoveUp(e.CommandArgument.ToString)
        End If
        If e.CommandName = "Down" Then
            MoveDown(ddlUploadTemplate.SelectedValue, e.CommandArgument.ToString)
        End If
    End Sub
    Protected Sub gvFixlength_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFixlength.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim chkSelect, chkIsRequired As CheckBox
            Try
                chkSelect = DirectCast(e.Row.Cells(0).FindControl("chkSelect"), CheckBox)
                chkIsRequired = DirectCast(e.Row.Cells(2).FindControl("chkRequired"), CheckBox)

                If chkSelect.Checked = True Then
                    chkSelect.Enabled = False
                End If
                If chkIsRequired.Checked = True Then
                    chkIsRequired.Enabled = False
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
#End Region
#Region "Button Events"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim objICTemplate As New ICTemplate
                objICTemplate.es.Connection.CommandTimeout = 3600
                If objICTemplate.LoadByPrimaryKey(ddlUploadTemplate.SelectedValue) Then
                    Dim gvRow As GridViewRow
                    For Each gvRow In gvFixlength.Rows
                        Dim FieldID As String = gvFixlength.DataKeys(gvRow.RowIndex)(0).ToString()
                        Dim txtFieldLegth As New TextBox
                        Dim chkSelect, chkIsRequired As CheckBox
                        If objICTemplate.IsFixLength = True Then
                            txtFieldLegth = gvRow.FindControl("txtFieldLegth")
                        End If
                        chkSelect = DirectCast(gvRow.Cells(0).FindControl("chkSelect"), CheckBox)
                        chkIsRequired = DirectCast(gvRow.Cells(2).FindControl("chkRequired"), CheckBox)
                        If chkSelect.Checked = True Then
                            Dim cTemplateField As New ICTemplateFields
                            Dim chkTF As New ICTemplateFields

                            cTemplateField.es.Connection.CommandTimeout = 3600
                            chkTF.es.Connection.CommandTimeout = 3600

                            cTemplateField.FieldID = FieldID.ToString()
                            cTemplateField.FieldName = gvRow.Cells(1).Text.ToString()
                            cTemplateField.TemplateID = ddlUploadTemplate.SelectedValue.ToString()
                            cTemplateField.FieldOrder = getLastFieldorder(ddlUploadTemplate.SelectedValue.ToString()) + 1
                            If objICTemplate.IsFixLength = True Then
                                cTemplateField.FixLength = txtFieldLegth.Text.ToString()
                            Else
                                cTemplateField.FixLength = 1
                            End If
                            cTemplateField.IsRequired = chkIsRequired.Checked
                            If chkIsRequired.Enabled = True Then
                                'cTemplateField.MustRequired = False
                            Else
                                'cTemplateField.MustRequired = True
                            End If
                            cTemplateField.CreateDate = Date.Now
                            cTemplateField.CreateBy = Me.UserId

                            If chkTF.LoadByPrimaryKey(FieldID, ddlUploadTemplate.SelectedValue.ToString()) = False Then
                                ICTemplateFieldController.InsertTemplateField(cTemplateField)
                            Else
                                ICTemplateFieldController.UpdateTemplateField(FieldID, ddlUploadTemplate.SelectedValue.ToString(), cTemplateField)
                            End If
                        End If
                    Next
                End If
                UIUtilities.ShowDialog(Me, "Add Fields", "Fields added successfully.", ICBO.IC.Dialogmessagetype.Success)
                LoadTemplateFields()
                LoadTemplateFieldsList()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub
#End Region
#Region "Drop Down Events"
    Private Sub LoadddlTemplate()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlUploadTemplate.Items.Clear()
            ddlUploadTemplate.Items.Add(lit)
            ddlUploadTemplate.AppendDataBoundItems = True
            'ddlUploadTemplate.DataSource = ICFileUploadTemplateController.GetFileTemplate()
            ddlUploadTemplate.DataTextField = "TemplateName"
            ddlUploadTemplate.DataValueField = "TemplateID"
            ddlUploadTemplate.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region
#Region "Other Functions/Routines"
    Private Sub LoadTemplateFieldsList()
        Try
            Dim objICTemplate As New ICTemplate
            objICTemplate.es.Connection.CommandTimeout = 3600
            'gvFixlength.DataSource = ICTemplateFieldListController.GetTemplateField()
            gvFixlength.DataBind()
            gvFixlength.Columns(3).Visible = False
            If objICTemplate.LoadByPrimaryKey(ddlUploadTemplate.SelectedValue) Then
                If Not objICTemplate.IsFixLength Is Nothing Then
                    If objICTemplate.IsFixLength = True Then
                        gvFixlength.Columns(3).Visible = True
                    End If
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadTemplateFields()
        Dim objICTemplate As New ICTemplate
        objICTemplate.es.Connection.CommandTimeout = 3600
        If objICTemplate.LoadByPrimaryKey(ddlUploadTemplate.SelectedValue) Then
            gvTemplateField.DataSource = ICTemplateFieldController.GetTemplateFieldsTemplateID(ddlUploadTemplate.SelectedValue.ToString())
            gvTemplateField.DataBind()
            gvTemplateField.Columns(2).Visible = False
            If Not objICTemplate.IsFixLength Is Nothing Then
                If objICTemplate.IsFixLength = True Then
                    gvTemplateField.Columns(2).Visible = True
                End If
            End If
        End If
    End Sub

    Private Function getLastFieldorder(ByVal templateid As Integer) As Integer
        Dim objICTemplateFieldListCollection As New ICTemplateFieldsCollection
        objICTemplateFieldListCollection.es.Connection.CommandTimeout = 3600
        objICTemplateFieldListCollection.Query.Where(objICTemplateFieldListCollection.Query.TemplateID = templateid)
        objICTemplateFieldListCollection.Query.es.Top = 1
        objICTemplateFieldListCollection.Query.OrderBy(objICTemplateFieldListCollection.Query.FieldOrder.Descending)

        objICTemplateFieldListCollection.Query.Select(objICTemplateFieldListCollection.Query.FieldOrder)

        If objICTemplateFieldListCollection.Query.Load Then
            Return objICTemplateFieldListCollection(0).FieldOrder
        Else
            Return 0
        End If
    End Function

    Protected Sub ddlUploadTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUploadTemplate.SelectedIndexChanged
        Try
            LoadTemplateFieldsList()
            LoadTemplateFields()
            ManagePageAccessRights()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub



    Private Sub MoveUp(ByVal FieldID As String)

        ' CheckOrders(pcatid)

        Dim cat As New ICTemplateFields
        cat.es.Connection.CommandTimeout = 3600
        cat.LoadByPrimaryKey(FieldID, ddlUploadTemplate.SelectedValue)

        If Not cat.FieldOrder = 1 Then

            Dim cat2 As New ICTemplateFields
            cat2.es.Connection.CommandTimeout = 3600
            cat2.Query.Where(cat2.Query.FieldOrder = cat.FieldOrder - 1 And cat2.Query.TemplateID = cat.TemplateID)

            If cat2.Query.Load Then
                cat2.FieldOrder += 1
                cat2.Save()

            End If

            cat.FieldOrder = cat.FieldOrder - 1

            cat.Save()

            LoadTemplateFields()
            '   Me.FillTreeview(Me.TreeView1)

        End If


    End Sub

    Private Sub MoveDown(ByVal templateid As String, ByVal FieldID As String)


        Dim fieldcoll As New ICTemplateFieldsCollection
        fieldcoll.es.Connection.CommandTimeout = 3600
        fieldcoll.Query.Where(fieldcoll.Query.TemplateID = templateid)

        fieldcoll.Query.Load()

        Dim cat As New ICTemplateFields
        cat.es.Connection.CommandTimeout = 3600
        cat.LoadByPrimaryKey(FieldID, templateid)




        If Not cat.FieldOrder = fieldcoll.Count Then

            Dim cat2 As New ICTemplateFields
            cat2.es.Connection.CommandTimeout = 3600

            cat2.Query.Where(cat2.Query.FieldOrder = cat.FieldOrder + 1 And cat.Query.TemplateID = templateid)


            If cat2.Query.Load Then


                cat2.FieldOrder -= 1


                cat2.Save()

            End If

            cat.FieldOrder = cat.FieldOrder + 1


            cat.Save()

            LoadTemplateFields()

        End If

    End Sub

    Private Sub ManagePageAccessRights()
        Try
            ArrRights = ICBankRoleRightsController.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "File Template Fields")
            btnSave.Visible = ArrRights(0)
            gvTemplateField.Columns(5).Visible = ArrRights(2)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            ' str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function

#End Region
    
End Class
