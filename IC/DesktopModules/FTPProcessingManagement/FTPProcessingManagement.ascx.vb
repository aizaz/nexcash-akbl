﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Security.Permissions
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Net
Imports System.Data
Imports System.Data.OleDb
Partial Class DesktopModules_User_Limits_ViewUserLimits
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private FTPSettingsID As String
    Private htRights As Hashtable

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            FTPSettingsID = Request.QueryString("FTPSettingsID").ToString()
           
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing


                If FTPSettingsID = "0" Then
                    Clear()

                    lblPageHeader.Text = "Add FTP Settings"
                    savebtn.Text = "Save"
                    txtFTPPassword.Enabled = True
                    savebtn.Visible = CBool(htRights("Add"))
                    lnkChangePassword.Visible = False
                    RadInputManager1.GetSettingByBehaviorID("regFTPPassword").Validation.IsRequired = True
                    RadInputManager1.GetSettingByBehaviorID("regFTPConfirmPassword").Validation.IsRequired = True
                    LoadddlGroup()
                Else

                    lblPageHeader.Text = "Edit FTP Settings"
                    savebtn.Text = "Update"
                    RadInputManager1.GetSettingByBehaviorID("regFTPPassword").Validation.IsRequired = False
                    RadInputManager1.GetSettingByBehaviorID("regFTPConfirmPassword").Validation.IsRequired = False

                    ddlUserType.Enabled = False
                    ddlUser.Enabled = False
                    ddlGroup.Enabled = False
                    ddlCompany.Enabled = False
                    ddlAccPNature.Enabled = False
                    ddlFileUploadTemplate.Enabled = False
                    savebtn.Visible = CBool(htRights("Update"))

                    txtFTPPassword.Enabled = False
                    revPassword.Enabled = False
                    comparePasswords.Enabled = False
                    txtConfirmPassword.Enabled = False

                    'If txtFTPPassword.Enabled = True Then
                    '    revPassword.Enabled = True
                    '    comparePasswords.Enabled = True
                    '    '  txtConfirmPassword.Enabled = True
                    '    txtConfirmPassword.ReadOnly = False
                    '    'radPassword.Enabled = True
                    'Else
                    '    revPassword.Enabled = False
                    '    comparePasswords.Enabled = False
                    '    txtConfirmPassword.Enabled = False
                    '    txtConfirmPassword.ReadOnly = False
                    '    'radPassword.Enabled = False
                    'End If


                    Dim ObjICFTP As New ICFTPSettings
                    Dim objICACCPaymentNature As New ICAccountsPaymentNature
                    Dim objICAccounts As New ICAccounts
                    Dim objICCompany As New ICCompany
                    Dim objICUser As New ICUser
                    Dim objICGroup As New ICGroup

                    objICCompany.es.Connection.CommandTimeout = 3600
                    objICAccounts.es.Connection.CommandTimeout = 3600
                    ObjICFTP.es.Connection.CommandTimeout = 3600
                    objICUser.es.Connection.CommandTimeout = 3600
                    objICGroup.es.Connection.CommandTimeout = 3600



                    ObjICFTP.LoadByPrimaryKey(FTPSettingsID)
                    objICACCPaymentNature = ObjICFTP.UpToICAccountsPaymentNatureByAccountNumber
                    objICAccounts = objICACCPaymentNature.UpToICAccountsByAccountNumber
                    objICCompany = objICAccounts.UpToICCompanyByCompanyCode
                    objICGroup = objICCompany.UpToICGroupByGroupCode
                    objICUser = ObjICFTP.UpToICUserByUserID


                    txtFTPFolderName.Text = ObjICFTP.FTPFolderName.ToString
                    txtFTPIPAddress.Text = ObjICFTP.FTPIPAddress.ToString
                    txtFTPPassword.Text = ObjICFTP.FTPPassword.ToString
                    txtFTPUserName.Text = ObjICFTP.FTPUserID.ToString
                    ddlUserType.SelectedValue = objICUser.UserType
                    LoadddlGroup()
                    ddlGroup.SelectedValue = objICGroup.GroupCode
                    LoadddlCompany()
                    ddlCompany.SelectedValue = objICCompany.CompanyCode
                    LoadddlAccountPaymentNature()
                    ddlAccPNature.SelectedValue = ObjICFTP.AccountNumber & "-" & ObjICFTP.BranchCode & "-" & ObjICFTP.Currency & "-" & ObjICFTP.PaymentNatureCode

                    LoadddlFileUploadTemplate()
                    ddlFileUploadTemplate.SelectedValue = ObjICFTP.FileUploadTemplateCode

                    lnkChangePassword.Visible = True
                    txtFTPPassword.Enabled = False
                    txtFTPPassword.Text = ObjICFTP.FTPPassword
                    'radPassword.Enabled = False


                    If ddlUserType.SelectedItem.Text = "Bank User" Then
                        LoadddlUsers()
                        ddlUser.SelectedValue = objICUser.UserID
                    ElseIf ddlUserType.SelectedItem.Text = "Client User" Then
                        LoadddlUsers()
                        ddlUser.SelectedValue = objICUser.UserID
                    End If


                End If

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub


    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client FTP Processing Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        ddlUserType.ClearSelection()
        ddlGroup.ClearSelection()
        ddlUser.ClearSelection()

        LoadddlCompany()
        LoadddlAccountPaymentNature()
        LoadddlFileUploadTemplate()

        txtFTPFolderName.Text = ""
        txtFTPIPAddress.Text = ""
        txtFTPPassword.Text = ""
        txtFTPUserName.Text = ""

        If ddlUserType.SelectedValue = "0" Then
            LoadddlUsers()
        End If

    End Sub

#End Region

#Region "Buttons"

    Protected Sub savebtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles savebtn.Click
        If Page.IsValid Then

            Try
                Dim objICFTP As New ICFTPSettings
                Dim objICFTPForUpdate As New ICFTPSettings
                Dim lit As New ListItem
                objICFTP.es.Connection.CommandTimeout = 3600
                objICFTPForUpdate.es.Connection.CommandTimeout = 3600
                objICFTP.AccountNumber = ddlAccPNature.SelectedValue.Split("-")(0).ToString()
                objICFTP.BranchCode = ddlAccPNature.SelectedValue.Split("-")(1).ToString()
                objICFTP.Currency = ddlAccPNature.SelectedValue.Split("-")(2).ToString()
                objICFTP.PaymentNatureCode = ddlAccPNature.SelectedValue.Split("-")(3).ToString()
                objICFTP.GroupCode = ddlGroup.SelectedValue.ToString()
                objICFTP.FileUploadTemplateCode = ddlFileUploadTemplate.SelectedValue.ToString()
                objICFTP.FTPIPAddress = txtFTPIPAddress.Text
                objICFTP.FTPPassword = txtFTPPassword.Text
                objICFTP.FTPFolderName = txtFTPFolderName.Text
                objICFTP.FTPUserID = txtFTPUserName.Text
                objICFTP.UserID = ddlUser.SelectedValue.ToString()



                If FTPSettingsID = "0" Then

                    objICFTP.CreatedBy = Me.UserId
                    objICFTP.CreatedDate = Date.Now
                    objICFTP.Creater = Me.UserId
                    objICFTP.CreationDate = Date.Now
                    If DuplicateFileUploadTemplateUser(objICFTP) = False Then
                        ICFTPProcessingController.AddFTPSettings(objICFTP, False, Me.UserId.ToString(), Me.UserInfo.Username.ToString())

                    Else
                        UIUtilities.ShowDialog(Me, "Save FTP Settings", "Can not add duplicate FTP Settings.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If

                    UIUtilities.ShowDialog(Me, "Save FTP Settings", "FTP Settings added successfully.", ICBO.IC.Dialogmessagetype.Success)

                Else

                    'If txtFTPPassword.Enabled = True Then
                    '    RadInputManager1.GetSettingByBehaviorID("regFTPConfirmPassword").Validation.IsRequired = True
                    'Else
                    '    RadInputManager1.GetSettingByBehaviorID("regFTPConfirmPassword").Validation.IsRequired = False
                    'End If

                    objICFTPForUpdate.LoadByPrimaryKey(FTPSettingsID)
                    objICFTP.CreatedBy = Me.UserId
                    objICFTP.CreatedDate = Date.Now
                    If txtFTPPassword.Text.ToString <> "" And txtFTPPassword.Text.ToString <> "Enter FTP Password" Then
                        If objICFTPForUpdate.FTPPassword <> txtFTPPassword.Text.ToString Then
                            objICFTP.FTPPassword = txtFTPPassword.Text.ToString
                        Else
                            objICFTP.FTPPassword = objICFTPForUpdate.FTPPassword
                        End If
                    Else
                        objICFTP.FTPPassword = objICFTPForUpdate.FTPPassword
                    End If
                    objICFTP.FTPSettingsID = objICFTPForUpdate.FTPSettingsID
                    ICFTPProcessingController.AddFTPSettings(objICFTP, True, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    UIUtilities.ShowDialog(Me, "Save FTP Settings", "FTP Settings updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())

                End If
                Clear()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Private Function DuplicateFileUploadTemplateUser(ByVal FTPSettings As ICFTPSettings) As Boolean
        Try
           
            Dim rslt As Boolean = False
            Dim collFTPSettings As New ICFTPSettingsCollection

            collFTPSettings.Query.Where(collFTPSettings.Query.FTPIPAddress.ToLower.Trim = FTPSettings.FTPIPAddress.ToLower.Trim And collFTPSettings.Query.FTPFolderName.ToLower.Trim = FTPSettings.FTPFolderName.ToLower.Trim And collFTPSettings.Query.UserID = FTPSettings.UserID)

            If collFTPSettings.Query.Load() Then
                rslt = True
            End If

            Return rslt


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub cancelbtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelbtn.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

#End Region

#Region "Drop Down"

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            If ddlGroup.SelectedValue.ToString() = "0" Then
                ddlUserType.SelectedValue = "0"
                LoadddlUsers()
                LoadddlCompany()

                LoadddlAccountPaymentNature()
                LoadddlFileUploadTemplate()
            Else
                If ddlUserType.SelectedValue.ToString = "Bank User" Then
                    LoadddlCompany()

                    LoadddlUsers()
                    LoadddlAccountPaymentNature()
                    LoadddlFileUploadTemplate()

                ElseIf ddlUserType.SelectedValue.ToString = "Client User" Then

                    LoadddlUsers()
                    LoadddlCompany()

                    LoadddlAccountPaymentNature()
                    LoadddlFileUploadTemplate()

                ElseIf ddlUserType.SelectedValue.ToString() = "0" Then
                    LoadddlUsers()
                    LoadddlCompany()

                    LoadddlAccountPaymentNature()
                    LoadddlFileUploadTemplate()
                End If
            End If
            

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveAndApproveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlUserType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUserType.SelectedIndexChanged
        Try
            If ddlUserType.SelectedValue = "Bank User" Then
                LoadddlUsers()
                LoadddlAccountPaymentNature()
                LoadddlFileUploadTemplate()

            ElseIf ddlUserType.SelectedValue = "Client User" Then
                LoadddlAccountPaymentNature()
                LoadddlFileUploadTemplate()
                LoadddlUsers()

            ElseIf ddlUserType.SelectedValue = "0" Then
                LoadddlGroup()
                LoadddlCompany()
                LoadddlAccountPaymentNature()
                LoadddlFileUploadTemplate()
                LoadddlUsers()

            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadddlAccountPaymentNature()

            If ddlCompany.SelectedValue = "0" Then
                LoadddlFileUploadTemplate()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
   
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString)
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlAccPNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAccPNature.SelectedIndexChanged
        Try
            LoadddlFileUploadTemplate()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlAccountPaymentNature()
        Try

            Dim lit As New ListItem
          
            Dim dtICAccountPayNature As New DataTable

            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlAccPNature.Items.Clear()
            ddlAccPNature.Items.Add(lit)
            lit.Selected = True
            ddlAccPNature.AppendDataBoundItems = True
            ddlAccPNature.DataSource = ICAccountPaymentNatureController.GetAccountPaymentNatureByCompanyCode(ddlCompany.SelectedValue.ToString)
            ddlAccPNature.DataTextField = "AccountAndPaymentNature"
            ddlAccPNature.DataValueField = "AccountPaymentNature"
            ddlAccPNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlFileUploadTemplate()
        Try
            Dim objICAPNature As New ICAccountsPaymentNature
            Dim StrArray As String() = Nothing
            objICAPNature.es.Connection.CommandTimeout = 3600
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlFileUploadTemplate.Items.Clear()
            ddlFileUploadTemplate.Items.Add(lit)
            ddlFileUploadTemplate.AppendDataBoundItems = True
            If ddlAccPNature.SelectedValue.ToString <> "0" Then
                StrArray = ddlAccPNature.SelectedValue.ToString.Split("-")
                objICAPNature.LoadByPrimaryKey(StrArray(0).ToString, StrArray(1).ToString, StrArray(2), StrArray(3).ToString)
                ddlFileUploadTemplate.AppendDataBoundItems = True
                ddlFileUploadTemplate.DataSource = ICAccountPaymentNatureFileUploadTemplateController.GetTemplatesByAccountPaymentNature(objICAPNature)
                ddlFileUploadTemplate.DataTextField = "TemplateName"
                ddlFileUploadTemplate.DataValueField = "TemplateID"
                ddlFileUploadTemplate.DataBind()
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlUsers()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlUser.Items.Clear()
            ddlUser.Items.Add(lit)
            ddlUser.AppendDataBoundItems = True
            If ddlUserType.SelectedValue = "Client User" Then

                ddlUser.DataSource = ICUserController.GetAllUsersByUserGroupCode(ddlGroup.SelectedValue.ToString(), "Client User", "FTPSettings", "", "", "", "", "")
                ddlUser.DataTextField = "UserName"
                ddlUser.DataValueField = "UserID"
                ddlUser.DataBind()

            ElseIf ddlUserType.SelectedValue = "Bank User" Then
                ddlUser.DataSource = ICUserController.GetAllActiveAndApproveUsersOfPrincipalBankforFTPSettings()
                ddlUser.DataTextField = "UserName"
                ddlUser.DataValueField = "UserID"
                ddlUser.DataBind()

            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

   
    Protected Sub lnkChangePassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkChangePassword.Click
        txtFTPPassword.Enabled = True
        txtConfirmPassword.Enabled = True
        RadInputManager1.GetSettingByBehaviorID("regFTPPassword").Validation.IsRequired = True
        RadInputManager1.GetSettingByBehaviorID("regFTPConfirmPassword").Validation.IsRequired = True
        revPassword.Enabled = True
        comparePasswords.Enabled = True
        'radPassword.Enabled = True
    End Sub


End Class
