﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Security.Permissions
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports Telerik.Web.UI

Partial Class DesktopModules_FTPProcessingManagement_ViewFTPProcessingManagement
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable

#Region "Page Load"

    Protected Sub DesktopModules_FTPProcessingManagement_ViewFTPProcessingManagement_Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing
                btnAddEmailSettings.Visible = CBool(htRights("Add"))
                LoadddlGroup()
                LoadgvFTPSettingTaggedUsers(True)

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client FTP Processing Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
       ddlGroup.ClearSelection()
        ddlCompany.ClearSelection()
        ddlAccPNature.ClearSelection()
        ddlFileUploadTemplate.ClearSelection()

    End Sub


#End Region


#Region "Button"

    Protected Sub btnAddEmailSettings_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddEmailSettings.Click
        Page.Validate()

        If Page.IsValid Then
            Try
                Response.Redirect(NavigateURL("SaveFTPSettings", "&mid=" & Me.ModuleId & "&FTPSettingsID=0"), False)

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnBulkDeleteFTPSettings_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBulkDeleteFTPSettings.Click
        Page.Validate()
        If Page.IsValid Then
            Try
                Dim rowgvFTPTaggedUsers As GridDataItem
                Dim chkSelect As CheckBox
                Dim FTPSettingID As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0

                If CheckgvFTPTaggedUsersForProcessAll() = True Then
                    For Each rowgvFTPTaggedUsers In gvFTPSettingTaggedUsers.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvFTPTaggedUsers.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            FTPSettingID = rowgvFTPTaggedUsers("FTPSettingsID").Text.ToString()
                            Dim objICFTP As New ICFTPSettings
                            objICFTP.es.Connection.CommandTimeout = 3600
                            If objICFTP.LoadByPrimaryKey(FTPSettingID.ToString()) Then
                                Try
                                    ICFTPProcessingController.DeleteFTPSettings(objICFTP, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                    PassCount = PassCount + 1
                                Catch ex As Exception
                                    If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                        FailCount = FailCount + 1
                                    End If
                                End Try
                            End If
                        End If


                    Next

                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete FTP Settings", "[" & FailCount.ToString() & "] FTP Settings can not be deleted due to following reasons : <br /> 1. FTP Settings is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete FTP Settings", "[" & PassCount.ToString() & "] FTP Settings deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Delete FTP Settings", "[" & PassCount.ToString() & "] FTP Settings deleted successfully. [" & FailCount.ToString() & "] FTP Settings can not be deleted due to following reasons : <br /> 1. FTP Settings is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Delete FTP Settings", "Please select atleast one(1) FTP Settings.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If


    End Sub


    Private Function CheckgvFTPTaggedUsersForProcessAll() As Boolean
        Try
            Dim rowgvFTPTaggedUsers As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvFTPTaggedUsers In gvFTPSettingTaggedUsers.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvFTPTaggedUsers.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function


#End Region

#Region "Grid View"

    Protected Sub gvFTPSettingTaggedUsers_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvFTPSettingTaggedUsers.NeedDataSource
        Try
            LoadgvFTPSettingTaggedUsers(False)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvFTPSettingTaggedUsers_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvFTPSettingTaggedUsers.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvFTPSettingTaggedUsers.MasterTableView.ClientID)
                Next

                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvFTPSettingTaggedUsers_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvFTPSettingTaggedUsers.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvFTPSettingTaggedUsers.MasterTableView.ClientID & "','0');"
        End If

    End Sub

    Protected Sub gvFTPSettingTaggedUsers_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvFTPSettingTaggedUsers.ItemCommand
        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    Dim objFTPTaggedUsers As New ICFTPSettings
                    objFTPTaggedUsers.es.Connection.CommandTimeout = 3600
                    objFTPTaggedUsers.LoadByPrimaryKey(e.CommandArgument.ToString())
                    ICFTPProcessingController.DeleteFTPSettings(objFTPTaggedUsers, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    UIUtilities.ShowDialog(Me, "Delete FTP Processing", "FTP Settings with Assigned User deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                    LoadgvFTPSettingTaggedUsers(True)
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Deleted FTP Settings", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadgvFTPSettingTaggedUsers(ByVal IsBind As Boolean)
        Try
            ICFTPProcessingController.GetFTPSettingTaggedUsersgv(gvFTPSettingTaggedUsers.CurrentPageIndex + 1, gvFTPSettingTaggedUsers.PageSize, gvFTPSettingTaggedUsers, IsBind, ddlGroup.SelectedValue.ToString, ddlCompany.SelectedValue.ToString, ddlAccPNature.SelectedValue.ToString, ddlFileUploadTemplate.SelectedValue.ToString)

            If gvFTPSettingTaggedUsers.Items.Count > 0 Then
                gvFTPSettingTaggedUsers.Visible = True
                lblRNF.Visible = False
                gvFTPSettingTaggedUsers.Columns(10).Visible = CBool(htRights("Delete"))
                btnBulkDeleteFTPSettings.Visible = CBool(htRights("Delete"))
            Else
                lblRNF.Visible = True
                gvFTPSettingTaggedUsers.Visible = False
                btnBulkDeleteFTPSettings.Visible = False
                'lblProductTypeRNF.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

   
#End Region

#Region "Drop Down"

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveAndApproveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString)
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlAccountPaymentNature()
        Try
            Dim lit As New ListItem
            Dim dtICAccountPayNature As New DataTable

            lit.Value = "0"
            lit.Text = "-- All --"
            ddlAccPNature.Items.Clear()
            ddlAccPNature.Items.Add(lit)
            lit.Selected = True
            ddlAccPNature.AppendDataBoundItems = True
            ddlAccPNature.DataSource = ICAccountPaymentNatureController.GetAccountPaymentNatureByCompanyCode(ddlCompany.SelectedValue.ToString)
            ddlAccPNature.DataTextField = "AccountAndPaymentNature"
            ddlAccPNature.DataValueField = "AccountPaymentNature"
            ddlAccPNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlFileUploadTemplate()
        Try
           Dim objICAPNature As New ICAccountsPaymentNature
            Dim StrArray As String() = Nothing
            objICAPNature.es.Connection.CommandTimeout = 3600
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlFileUploadTemplate.Items.Clear()
            ddlFileUploadTemplate.Items.Add(lit)
            ddlFileUploadTemplate.AppendDataBoundItems = True
            If ddlAccPNature.SelectedValue.ToString <> "0" Then
                StrArray = ddlAccPNature.SelectedValue.ToString.Split("-")
                objICAPNature.LoadByPrimaryKey(StrArray(0).ToString, StrArray(1).ToString, StrArray(2), StrArray(3).ToString)
                ddlFileUploadTemplate.AppendDataBoundItems = True
                ddlFileUploadTemplate.DataSource = ICAccountPaymentNatureFileUploadTemplateController.GetTemplatesByAccountPaymentNature(objICAPNature)
                ddlFileUploadTemplate.DataTextField = "TemplateName"
                ddlFileUploadTemplate.DataValueField = "TemplateID"
                ddlFileUploadTemplate.DataBind()
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try


            LoadddlCompany()
            LoadddlAccountPaymentNature()
            LoadddlFileUploadTemplate()
            LoadgvFTPSettingTaggedUsers(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadddlAccountPaymentNature()

            LoadddlFileUploadTemplate()
            LoadgvFTPSettingTaggedUsers(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlAccPNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAccPNature.SelectedIndexChanged
        Try

            LoadddlFileUploadTemplate()
            LoadgvFTPSettingTaggedUsers(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlFileUploadTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFileUploadTemplate.SelectedIndexChanged
        Try
            LoadgvFTPSettingTaggedUsers(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

  End Class
