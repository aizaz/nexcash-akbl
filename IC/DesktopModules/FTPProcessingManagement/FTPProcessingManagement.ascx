﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FTPProcessingManagement.ascx.vb"
    Inherits="DesktopModules_User_Limits_ViewUserLimits" %>

<telerik:RadInputManager ID="RadInputManager1" runat="server" Enabled="true">

    <telerik:RegExpTextBoxSetting BehaviorID="regFTPIPAddress" ErrorMessage="Invalid IP Address" 
        EmptyMessage="" Validation-IsRequired="true" >  

        <TargetControls> 
            <telerik:TargetInput ControlID="txtFTPIPAddress" /> 
        </TargetControls> 
    </telerik:RegExpTextBoxSetting>


    <telerik:RegExpTextBoxSetting BehaviorID="regFTPUserName" ErrorMessage="Invalid FTP User ID"
        EmptyMessage="" Validation-IsRequired="true">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFTPUserName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting  BehaviorID="regFTPPassword" Validation-IsRequired="true" 
        EmptyMessage="" ErrorMessage="Invalid FTP Password">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFTPPassword" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting> 
      <telerik:RegExpTextBoxSetting  BehaviorID="regFTPConfirmPassword" Validation-IsRequired="true" 
        EmptyMessage="" ErrorMessage="Invalid Confirm Password">
        <TargetControls>
            <telerik:TargetInput ControlID="txtConfirmPassword" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting> 
   <%-- <telerik:TextBoxSetting BehaviorID="regFTPFolderName" Validation-IsRequired="true"
        EmptyMessage="Enter FTP Folder Name" ErrorMessage="Invalid FTP Folder Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFTPFolderName" />
        </TargetControls>
    </telerik:TextBoxSetting>--%>

      <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior2" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9 ]{1,100}$" ErrorMessage="Invalid FTP Folder Name"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFTPFolderName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>


</telerik:RadInputManager>
<script language="javascript" type="text/javascript">
    function AdjustWidth(ddl) {
        var maxWidth = 0;
        var Counter = 0;
        for (var i = 0; i < ddl.length; i++) {
            if (ddl.options[i].text.length > maxWidth) {
                Counter = Counter + 1;
                maxWidth = ddl.options[i].text.length;
            }
        }
        if (Counter = 1) { ddl.style.width = "250px"; }

        else {
            alert(Counter);
            ddl.style.width = maxWidth + "px";
        }
        //-->
    }
</script>
<style type="text/css">
.ctrDropDown{
    width:250px;
   
}
.ctrDropDownClick{
    
    width:600px;
 
}
.plainDropDown{
    width:250px;
   
}
</style>
<%--<telerik:RadInputManager ID="radPassword" runat="server">
    <telerik:TextBoxSetting BehaviorID="REPassword" Validation-IsRequired="true" EmptyMessage="Enter Password">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFTPPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
   
</telerik:RadInputManager>--%>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2" style="width: 50%">
            <asp:Label ID="lblPageHeader" runat="server" Text="Add FTP Settings" CssClass="headingblue"></asp:Label>
            <br />
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" colspan="2" style="width: 50%">
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2" style="width: 50%">
            &nbsp;
        </td>
        <td align="left" valign="top" colspan="2" style="width: 50%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="Label2" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblUsrType" runat="server" Text="*" ForeColor="Red"></asp:Label>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
                <asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True" CssClass="dropdown">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
            </asp:DropDownList>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" CssClass="dropdown">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
            </asp:DropDownList>
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlGroup" runat="server" ControlToValidate="ddlGroup"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Group" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
                <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" ControlToValidate="ddlCompany"
                    CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Company" SetFocusOnError="True"
                    ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="Label4" runat="server" Text="Select User Type" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblUsrType0" runat="server" Text="*" ForeColor="Red"></asp:Label>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                <asp:Label ID="Label5" runat="server" Text="Select User" CssClass="lbl"></asp:Label>
                <asp:Label ID="lblUsrType1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlUserType" runat="server" AutoPostBack="True" CssClass="dropdown">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem>Bank User</asp:ListItem>
                <asp:ListItem>Client User</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
                <asp:DropDownList ID="ddlUser" runat="server" AutoPostBack="True" CssClass="dropdown">
                    <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlUserType" runat="server" ControlToValidate="ddlUserType"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select User Type" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlUser" runat="server" ControlToValidate="ddlUser"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select User" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblSelectAccountPNature" runat="server" Text="Select Account Payment Nature"
                CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqAccPNature" runat="server" Text="*" ForeColor="Red"></asp:Label>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                <asp:Label ID="lblFileUploadTemplate" runat="server" Text="Select File Upload Template"
                    CssClass="lbl"></asp:Label>
                <asp:Label ID="lblReqFileUploadTemplate" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <%--<asp:DropDownList ID="ddlAccPNature" runat="server" CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
            </asp:DropDownList>--%>
             <div style= "width:250px; overflow:hidden;">
            <asp:DropDownList ID="ddlAccPNature" runat="server" class="ctrDropDown"  onblur="this.className='ctrDropDown';" onmousedown="this.className='ctrDropDownClick';" onchange="this.className='ctrDropDown';" AutoPostBack="true">
            </asp:DropDownList>
            </div>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlFileUploadTemplate" runat="server" AutoPostBack="True" CssClass="dropdown">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlAccPNature" runat="server" ControlToValidate="ddlAccPNature"
                Display="Dynamic" ErrorMessage="Please select Account Payment Nature" InitialValue="0"
                SetFocusOnError="True"></asp:RequiredFieldValidator>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                <asp:RequiredFieldValidator ID="rfvddlFileUploadTemplate" runat="server" ControlToValidate="ddlFileUploadTemplate"
                    Display="Dynamic" ErrorMessage="Please select File Upload Template" InitialValue="0"
                    SetFocusOnError="True"></asp:RequiredFieldValidator>
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblFTPIPAddress" runat="server" Text="FTP IP Address" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqAccPNature1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                <asp:Label ID="lblCompany2" runat="server" Text="FTP User ID" CssClass="lbl"></asp:Label>
                <asp:Label ID="lblReqAccPNature2" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtFTPIPAddress" runat="server" CssClass="txtbox" MaxLength="20"></asp:TextBox>
            <br />
               </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                <asp:TextBox ID="txtFTPUserName" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                <br />
            </td>
        
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompany3" runat="server" Text="Password" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqAccPNature3" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
           <asp:Label ID="lblConfirmPassword" runat="server" Text="Confirm Password" CssClass="lbl"></asp:Label><asp:Label
                ID="Label7" runat="server" Text="*" ForeColor="Red"></asp:Label>
           </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtFTPPassword" runat="server" CssClass="txtbox" TextMode="Password"
                MaxLength="20"></asp:TextBox>
            <asp:RegularExpressionValidator ID="revPassword" runat="server" ControlToValidate="txtFTPPassword"
                Display="Dynamic" ErrorMessage="Password length must have at least 5 characters and maximum 20 characters" ValidationExpression="^[\s\S]{5,20}$">
                </asp:RegularExpressionValidator>
                
                </td> 
            <td align="center"  valign="top" style="width: 25%">
                &nbsp;
            <asp:LinkButton ID="lnkChangePassword" runat="server" CausesValidation="False">Change Password</asp:LinkButton>
            </td>
            <td align="left" valign="top" style="width: 25%">
                 <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="txtbox" TextMode="Password"
                 MaxLength="8"></asp:TextBox>
            <br />
            <asp:CompareValidator ID="comparePasswords" runat="server" ControlToCompare="txtFTPPassword"
                ControlToValidate="txtConfirmPassword" ErrorMessage="Password did not match up!"
                Display="Dynamic" />
            <br />
          
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompany4" runat="server" Text="Folder Name" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqAccPNature4" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
   
  
      <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
                <asp:TextBox ID="txtFTPFolderName" runat="server" CssClass="txtbox" MaxLength="100"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
      <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
           </td> 
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="savebtn" runat="server" Text="Save" CssClass="btn" CausesValidation ="true" 
                Width="74px" />
            <asp:Button ID="cancelbtn" runat="server" Text="Cancel" CssClass="btnCancel"
                Width="75px" CausesValidation="False" />
    </tr>
  
</table>
