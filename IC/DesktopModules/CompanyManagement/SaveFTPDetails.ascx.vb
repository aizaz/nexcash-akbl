﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Partial Class DesktopModules_CompanyManagement_SaveFTPDetails
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Dim dtDa As DataTable
    Private CompanyCode As String
#Region "Page Load"
    Protected Sub DesktopModules_CompanyOfficeManagement_ViewCompanyOffice_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            CompanyCode = Request.QueryString("id").ToString()
            If CheckIsAdminOrSuperUser() = False Then
                'ManagePageAccessRights()
            End If
            If Page.IsPostBack = False Then

                'SetHeaderValues()
                ViewState("SortExp") = Nothing
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region


#Region "Other Functions/Routines"
    'Private Sub ManagePageAccessRights()
    '    Try
    '        ArrRights = FRCRoleRightController.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Manage Area")
    '        If ArrRights.Count > 0 Then
    '            btnSaveBank.Visible = ArrRights(0)
    '        Else
    '            UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
    '        End If
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            ' str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function

    'Private Sub SetHeaderValues()
    '    Try
    '        Dim cICAccPayNatProTyp As New ICAccountsPaymentNatureProductType

    '        If cICAccPayNatProTyp.LoadByPrimaryKey(AccountPaymentNatureProductTypeCode) Then
    '            txtBranchCode.Text = cICAccPayNatProTyp.BranchCode.ToString()
    '            txtCurrency.Text = cICAccPayNatProTyp.Currency.ToString()
    '            txtAccountNo.Text = cICAccPayNatProTyp.AccountNumber.ToString()
    '            'txtProductType.Text = cICAccPayNatProTyp.UpToICProductTypeByProductTypeCode.DisbursementMode.ToString()

    '            txtCurrency.ReadOnly = True
    '            txtAccountNo.ReadOnly = True
    '            'txtProductType.ReadOnly = True
    '            txtBranchCode.ReadOnly = True

    '        End If
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
#End Region




    'Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '    Response.Redirect(NavigateURL(), False)
    'End Sub


End Class
