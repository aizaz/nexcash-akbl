﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveFTPDetails.ascx.vb" Inherits="DesktopModules_CompanyManagement_SaveFTPDetails" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
    <style type="text/css">

    .headingblue
    {
        font-weight: 700;
    }
    </style>
    <script type="text/javascript">
        function con() {
            if (window.confirm("Are you sure you wish to remove this Record?") == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function delcon(item) {
            if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
                return true;
            }
            else {
                return false;
            }
        }    
</script>
<telerik:RadInputManager ID="rimFTP" runat="server">
    <telerik:TextBoxSetting BehaviorID="ftpID" EmptyMessage="Enter FTP User ID">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFTPID" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="ftpAddress" EmptyMessage="Enter FTP IP Address">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFTPIPAddress" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="ftpPassword" EmptyMessage="Enter FTP Server Password">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFTPPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="ftpFolderName" EmptyMessage="Enter FTP Folder Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFolderName" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table id="tblFTPInfo" width="100%" runat="server" >
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%">
            <asp:Label ID="lblPageHeader" runat="server" Text="Add FTP Details" CssClass="headingblue"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 50%">
                                    &nbsp;</td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
                                </td>
                                <td align="left" valign="top" style="width: 50%">
                                    &nbsp;</td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%">
                                    &nbsp;</td>
                                <td align="left" valign="top" style="width: 50%">
                                    &nbsp;</td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%">
                                    <asp:Label ID="lblFTPID" runat="server" Text="FTP User ID" CssClass="lbl"></asp:Label>
                                    <strong>
                                        <asp:Label ID="Label37" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </strong>
                                </td>
                                <td align="left" valign="top" style="width: 50%">
                                    <asp:Label ID="lblFTPIPAddress" runat="server" Text="FTP IP Address" CssClass="lbl"></asp:Label>
                                    <strong>
                                        <asp:Label ID="Label36" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </strong>
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%">
                                    <asp:TextBox ID="txtFTPID" runat="server" CssClass="txtbox" MaxLength="15"></asp:TextBox>
                                </td>
                                <td align="left" valign="top" style="width: 50%">
                                    <asp:TextBox ID="txtFTPIPAddress" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%">
                                    &nbsp;</td>
                                <td align="left" valign="top" style="width: 50%">
                                    &nbsp;</td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%">
                                    <asp:Label ID="lblFTPPassword" runat="server" Text="FTP Password" CssClass="lbl"></asp:Label>
                                    <strong>
                                        <asp:Label ID="Label35" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </strong>
                                </td>
                                 <td align="left" valign="top" style="width: 50%">
                                    <asp:Label ID="lblFolder" runat="server" Text="Folder Name" CssClass="lbl"></asp:Label>
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%">
                                    <asp:TextBox ID="txtFTPPassword" runat="server" CssClass="txtbox" MaxLength="15"
                                        TextMode="Password"></asp:TextBox>
                                </td>
                                 <td align="left" valign="top" style="width: 50%">
                                    <asp:TextBox ID="txtFolderName" runat="server" CssClass="txtbox" MaxLength="300"></asp:TextBox>
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%" >
                                    &nbsp;</td>
                                 <td align="left" valign="top" style="width: 50%">
                                    &nbsp;</td>
                            </tr>
                            <tr align="left" valign="top" style="width: 50%">
                                <td align="center" valign="top" colspan="2" >
                                    <asp:Button ID="btnFetchFTP" runat="server" CssClass="btn" Text="Test FTP" CausesValidation="False" />
                                &nbsp;<asp:Button ID="btnSaveFTPDetails" runat="server" CssClass="btn" Text="Save" 
                                        CausesValidation="False" Width="80px" />
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%">
                                    &nbsp;</td>
                                 <td align="left" valign="top" style="width: 50%">
                                    &nbsp;</td>
                            </tr>
                           
                        </table>