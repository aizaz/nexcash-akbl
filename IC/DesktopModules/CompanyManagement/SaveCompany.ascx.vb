﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports FtpLib
Imports System.Net
Imports System.DirectoryServices.AccountManagement
Partial Class DesktopModules_CompanyManagement_SaveCompany
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private CompanyCode As String
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_CompanyManagement_SaveCompany_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            CompanyCode = Request.QueryString("id").ToString()

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlCountry()
                LoadddlProvince()
                LoadddlCity()
                LoadddlGroup()
                'raddpBillingDate.MinDate = Date.Now
                'raddpBillingDate.SelectedDate = Date.Now
                raddpBillingDate.SelectedDate = Nothing
                ClearFetchingMechanism()
                ClearFTPDetails()
                radFTPDetails.Enabled = False
                tblVIAFTP.Style.Add("display", "none")
                rfvddlFetchingFrequency.Enabled = False
                If CompanyCode.ToString() = "0" Then
                    Clear()
                    lblIsApproved.Visible = False
                    chkApproved.Visible = False
                    btnApproved.Visible = False
                    lblPageHeader.Text = "Add Company"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                    lnkChangePassword.Visible = False

                    DirectCast(Me.Control.FindControl("trFetchingMechanism"), HtmlTableRow).Style.Add("display", "none")
                Else
                    lblIsApproved.Visible = CBool(htRights("Approve"))
                    chkApproved.Visible = CBool(htRights("Approve"))
                    btnApproved.Visible = CBool(htRights("Approve"))
                    lblPageHeader.Text = "Edit Company"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    btnApproved.Visible = CBool(htRights("Approve"))
                    'txtCompanyName.ReadOnly = True
                    Dim objICCompany As New ICCompany
                    objICCompany.es.Connection.CommandTimeout = 3600

                    If objICCompany.LoadByPrimaryKey(CompanyCode) Then
                        'txtCompanyCode.Text = ICCompany.CompanyCode.ToString()
                        txtCompanyName.Text = objICCompany.CompanyName.ToString()
                        txtCompanyPhone1.Text = objICCompany.PhoneNumber1.ToString()
                        txtCompanyPhone2.Text = objICCompany.PhoneNumber2
                        txtCompanyAddress.Text = objICCompany.Address.ToString()
                        txtCompanyEmail.Text = objICCompany.EmailAddress.ToString()
                        chkActive.Checked = objICCompany.IsActive
                        chkApproved.Checked = objICCompany.IsApprove
                        ddlGroup.SelectedValue = objICCompany.GroupCode.ToString()
                        chkSkipPaymentQueue.Checked = objICCompany.SkipPaymentQueue
                        If objICCompany.IsAllowOpenPayment = True Then
                            chkboxlstIsBene.Items(0).Selected = True
                        End If
                        If objICCompany.IsBeneRequired = True Then
                            chkboxlstIsBene.Items(1).Selected = True
                        End If
                        'chkSingleSignatory.Checked = objICCompany.IsSingleSignatory

                        Dim objICCity As New ICCity
                        Dim objICProvince As New ICProvince
                        Dim objICCountry As New ICCountry

                        objICCity.es.Connection.CommandTimeout = 3600
                        objICProvince.es.Connection.CommandTimeout = 3600
                        objICCountry.es.Connection.CommandTimeout = 3600

                        objICCity = objICCompany.UpToICCityByCity()
                        objICProvince = objICCity.UpToICProvinceByProvinceCode()
                        objICCountry = objICProvince.UpToICCountryByCountryCode()
                        LoadddlCountry()
                        ddlCountry.SelectedValue = objICCountry.CountryCode
                        LoadddlProvince()
                        ddlProvince.SelectedValue = objICProvince.ProvinceCode()
                        LoadddlCity()
                        ddlCity.SelectedValue = objICCity.CityCode
                        ddlGroup.Enabled = False
                        If objICCompany.IsApprove = True Then
                            lblIsApproved.Visible = True
                            chkApproved.Visible = True
                            chkApproved.Enabled = False
                            btnApproved.Visible = False
                        End If



                        ''MT 940 Work



                        If objICCompany.IsMT940StatementAllowed = True Then
                            chkAllowFetchMT940.Checked = True
                            DirectCast(Me.Control.FindControl("trFetchingMechanism"), HtmlTableRow).Style.Remove("display")
                            tblVIAFTP.Style.Remove("display")
                            'rfvddlFetchingFrequency.Enabled = True
                            rfvddlFetchingFrequency.Enabled = False
                            If objICCompany.MT940FetchingFrequency IsNot Nothing Then
                                ddlFetchingFrequency.SelectedValue = objICCompany.MT940FetchingFrequency
                            End If
                            If objICCompany.MT940StatementLocation = "FTP" Then
                                rbFetchingMechanism.SelectedValue = "VIA FTP"
                            ElseIf objICCompany.MT940StatementLocation = "Network" Then
                                rbFetchingMechanism.SelectedValue = "VIA Network"
                            End If
                            lnkChangePassword.Visible = True

                            txtFTPHost.Text = objICCompany.HostName
                            txtFTPUserName.Text = objICCompany.UserName
                            txtFTPPassword.Text = objICCompany.LocationPassword
                            txtFTPFolder.Text = objICCompany.FolderName
                            txtFTPPassword.Enabled = False
                            lblReqFTPPassword.Visible = False
                            radFTPDetails.GetSettingByBehaviorID("reFTPPassword").Validation.IsRequired = False
                            'radFTPDetails.Enabled = True
                            radFTPDetails.Enabled = False
                        Else
                            DirectCast(Me.Control.FindControl("trFetchingMechanism"), HtmlTableRow).Style.Add("display", "none")
                            tblVIAFTP.Style.Add("display", "none")
                            rfvddlFetchingFrequency.Enabled = False
                            rfvFetchingMechanism.Enabled = False
                            lnkChangePassword.Visible = False
                        End If


                        ''Package Invoice Work
                        If objICCompany.PackageInvoiceBillingDate IsNot Nothing Then
                            raddpBillingDate.SelectedDate = objICCompany.PackageInvoiceBillingDate
                        End If



                    End If
                End If
                'btnSave.Visible = ArrRights(1)
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Company Management")
            ViewState("htRights") = htRights

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
#Region "Button Events"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        'For Each validator As BaseValidator In Page.Validators
        '    If validator.Enabled = True And validator.IsValid = False Then
        '        Dim clientID As String
        '        clientID = validator.ClientID
        '    End If
        'Next

        If Page.IsValid Then
            Try


                Dim objICCompany As New ICCompany
                Dim objICCompanyFrom As New ICCompany
                Dim Action As String = ""
                objICCompany.es.Connection.CommandTimeout = 3600

                If CompanyCode <> "0" Then
                    objICCompanyFrom.LoadByPrimaryKey(CompanyCode.ToString)
                    objICCompany.CompanyCode = CompanyCode
                End If

                objICCompany.GroupCode = ddlGroup.SelectedValue.ToString()

                objICCompany.CompanyName = txtCompanyName.Text.ToString()
                objICCompany.PhoneNumber1 = txtCompanyPhone1.Text.ToString()
                objICCompany.PhoneNumber2 = txtCompanyPhone2.Text.ToString()
                objICCompany.Address = txtCompanyAddress.Text.ToString()
                objICCompany.EmailAddress = txtCompanyEmail.Text.ToString()
                objICCompany.City = ddlCity.SelectedValue.ToString()
                objICCompany.IsActive = chkActive.Checked
                objICCompany.SkipPaymentQueue = chkSkipPaymentQueue.Checked
                objICCompany.IsPaymentsAllowed = True
                objICCompany.IsSingleSignatory = False
                If chkboxlstIsBene.Items(0).Selected = True Then
                    objICCompany.IsAllowOpenPayment = True
                Else
                    objICCompany.IsAllowOpenPayment = False
                End If
                If chkboxlstIsBene.Items(1).Selected = True Then
                    objICCompany.IsBeneRequired = True
                Else
                    objICCompany.IsBeneRequired = False
                End If
                '' MT 940 Work
                objICCompany.HostName = txtFTPHost.Text
                objICCompany.UserName = txtFTPUserName.Text
                objICCompany.FolderName = txtFTPFolder.Text
                objICCompany.IsMT940StatementAllowed = chkAllowFetchMT940.Checked


                ''Package Invoice Date

                If raddpBillingDate.SelectedDate IsNot Nothing Then
                    objICCompany.PackageInvoiceBillingDate = raddpBillingDate.SelectedDate
                Else
                    objICCompany.PackageInvoiceBillingDate = Nothing
                End If


                If chkAllowFetchMT940.Checked = True Then
                    If rbFetchingMechanism.SelectedValue = "VIA FTP" Then
                        objICCompany.MT940StatementLocation = "FTP"
                        If CompanyCode = "0" Then
                            If CheckDuplicateFTPAndNetworkLocations(objICCompany, False) = True Then
                                UIUtilities.ShowDialog(Me, "Save Company", "Duplicate FTP Credentials are not allowed", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        Else
                            If CheckDuplicateFTPAndNetworkLocations(objICCompany, True) = True Then
                                UIUtilities.ShowDialog(Me, "Save Company", "Duplicate FTP Credentials are not allowed", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        End If
                        If CheckFTPCredentials(txtFTPHost.Text.ToString, txtFTPUserName.Text, txtFTPPassword.Text) = False Then
                            UIUtilities.ShowDialog(Me, "Save Company", "Invalid FTP Credentials", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    ElseIf rbFetchingMechanism.SelectedValue = "VIA Network" Then
                        objICCompany.MT940StatementLocation = "Network"


                        If CompanyCode = "0" Then
                            If CheckDuplicateFTPAndNetworkLocations(objICCompany, False) = True Then
                                UIUtilities.ShowDialog(Me, "Save Company", "Duplicate Network Credentials are not allowed", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        Else
                            If CheckDuplicateFTPAndNetworkLocations(objICCompany, True) = True Then
                                UIUtilities.ShowDialog(Me, "Save Company", "Duplicate Network Credentials are not allowed", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        End If
                        If ValiDateNetworkCredentials(txtFTPHost.Text, txtFTPUserName.Text, txtFTPPassword.Text, txtFTPFolder.Text) = False Then
                            UIUtilities.ShowDialog(Me, "Save Company", "Invalid Network Credentials", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    Else
                        objICCompany.MT940StatementLocation = Nothing
                    End If

                    If ddlFetchingFrequency.SelectedValue <> "0" Then
                        objICCompany.MT940FetchingFrequency = ddlFetchingFrequency.SelectedValue.ToString
                    Else
                        objICCompany.MT940FetchingFrequency = Nothing
                    End If

                    If txtFTPPassword.Enabled = True Then
                        objICCompany.LocationPassword = txtFTPPassword.Text
                    Else
                        objICCompany.LocationPassword = objICCompanyFrom.LocationPassword
                    End If
                Else
                    objICCompany.LocationPassword = Nothing
                    objICCompany.MT940StatementLocation = Nothing
                    objICCompany.MT940FetchingFrequency = Nothing
                    objICCompany.HostName = Nothing
                    objICCompany.UserName = Nothing
                    objICCompany.FolderName = Nothing
                End If




                If CompanyCode = "0" Then
                    objICCompany.CreatedBy = Me.UserId
                    objICCompany.CreatedDate = Date.Now
                    objICCompany.Creater = Me.UserId
                    objICCompany.CreationDate = Date.Now
                    If CheckDuplicate(objICCompany) = False Then
                        Action = Nothing
                        Action = "Company Name [ " & objICCompany.CompanyName & " ]; "
                        Action += "Phone 1 [ " & objICCompany.PhoneNumber1 & " ] ; "
                        Action += "Phone 2 [ " & objICCompany.PhoneNumber2 & " ] ; "
                        Action += "Email Address [ " & objICCompany.EmailAddress & " ] ; "
                        Action += "City [ " & objICCompany.UpToICCityByCity.CityName & " ] ; "
                        Action += "Province [ " & objICCompany.UpToICCityByCity.UpToICProvinceByProvinceCode.ProvinceName & " ] ; "
                        Action += "Group [ " & objICCompany.UpToICGroupByGroupCode.GroupName & " ] ; "
                        Action += "Skip Payment Queue [ " & objICCompany.SkipPaymentQueue & " ] ; "
                        Action += "Single Signatory [ " & objICCompany.IsSingleSignatory & " ]; "
                        Action += "MT 940 Statements Allowed [ " & objICCompany.IsMT940StatementAllowed & " ]"
                        Action += "Is Active [ " & objICCompany.IsActive & " ] added ."
                        ICCompanyController.AddCompany(objICCompany, False, Me.UserId.ToString, Me.UserInfo.Username.ToString, Action.ToString)
                        UIUtilities.ShowDialog(Me, "Save Company", "Company added successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Else
                        UIUtilities.ShowDialog(Me, "Save Company", "Can not add duplicate Company.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else
                    'objICCompanyFrom.LoadByPrimaryKey(CompanyCode.ToString)

                    If objICCompanyFrom.IsBeneRequired = True And objICCompany.IsBeneRequired = False Then
                        If IsCompanyTaggedWithBene(CompanyCode) = True Then
                            UIUtilities.ShowDialog(Me, "Save Company", "Company can not update as associated with Beneficiary.", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    End If



                    objICCompany.CreatedBy = Me.UserId
                    objICCompany.CreatedDate = Date.Now
                    Action = Nothing
                    Action = "Company " & objICCompany.CompanyName & " Updated : Company Name from [ " & objICCompanyFrom.CompanyName & " ] to [ " & objICCompany.CompanyName & " ] ; "
                    Action += "Company Phone 1 from [ " & objICCompanyFrom.PhoneNumber1 & " ] to [ " & objICCompany.PhoneNumber1 & " ] ; "
                    Action += "Company Phone 2 from [ " & objICCompanyFrom.PhoneNumber2 & " ] to [ " & objICCompany.PhoneNumber2 & " ] ; "
                    Action += "Email Address from [ " & objICCompanyFrom.EmailAddress & " ] to [ " & objICCompany.EmailAddress & " ] ; "
                    Action += "City from [ " & objICCompanyFrom.UpToICCityByCity.CityName & " ] to [ " & objICCompany.UpToICCityByCity.CityName & " ] ; "
                    Action += "Province from [ " & objICCompanyFrom.UpToICCityByCity.UpToICProvinceByProvinceCode.ProvinceName & " ] to [ " & objICCompany.UpToICCityByCity.UpToICProvinceByProvinceCode.ProvinceName & " ] ; "
                    Action += "Group from [ " & objICCompanyFrom.UpToICGroupByGroupCode.GroupName & " ] to [ " & objICCompany.UpToICGroupByGroupCode.GroupName & " ] ; "
                    Action += "Skip Payment Queue from [ " & objICCompanyFrom.SkipPaymentQueue & " ] to [ " & objICCompany.SkipPaymentQueue & " ] ; "
                    Action += "Single Signatory from [ " & objICCompanyFrom.IsSingleSignatory & " ] to [ " & objICCompany.IsSingleSignatory & " ] ; "
                    Action += "MT 940 Statements Allowed from [ " & objICCompanyFrom.IsMT940StatementAllowed & " ] to [ " & objICCompany.IsMT940StatementAllowed & " ] ;"
                    Action += "Is Active from [ " & objICCompanyFrom.IsActive & " to " & objICCompany.IsActive & " ]."
                    If CheckDuplicate(objICCompany) = False Then
                        ICCompanyController.AddCompany(objICCompany, True, Me.UserId.ToString, Me.UserInfo.Username.ToString, Action.ToString)
                        UIUtilities.ShowDialog(Me, "Save Company", "Company updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    Else
                        UIUtilities.ShowDialog(Me, "Save Company", "Can not add duplicate Company.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub


                    End If

                End If
                Clear()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Function IsCompanyTaggedWithBene(ByVal CompanyCode As String) As Boolean
        Dim collBene As New ICBeneCollection
        collBene.Query.Where(collBene.Query.CompanyCode = CompanyCode)
        If collBene.Query.Load() Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub
    Private Function CheckFTPCredentials(ByVal FTPAddress As String, ByVal FTPUserID As String, ByVal FTPPassword As String) As Boolean
        Try
            Dim ftp As FtpConnection
           
            ftp = New FtpConnection(FTPAddress, FTPUserID, FTPPassword)
            ftp.Open()
            ftp.Login()
            ftp.Close()
            ftp.Dispose()
            Return True
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function ValiDateNetworkCredentials(ByVal FTPAddress As String, ByVal FTPUserID As String, ByVal FTPPassword As String, ByVal FTPFolderName As String) As Boolean
        Try
            'Dim theNetworkCredential As New NetworkCredential("aizaz", "312Adeena#@!", "INTELLIGENES.NET")
            Dim authenticated As Boolean = False
            Dim theNetworkCredential As New NetworkCredential(FTPUserID, FTPPassword, ICUtilities.GetSettingValue("Domain"))
            Dim theNetcache As New CredentialCache
            Dim FileNames As String() = Nothing
            Dim PC As New PrincipalContext(DirectoryServices.AccountManagement.ContextType.Domain, ICUtilities.GetSettingValue("Domain"))
            authenticated = PC.ValidateCredentials(FTPUserID, FTPPassword)
            If authenticated = True Then
                Return True
            Else
                Throw New Exception("Unable to login. Please contact your Network Administrator")
            End If
            theNetcache.Remove(New Uri("\\" & FTPAddress), "Basic")
            theNetcache.Add(New Uri("\\" & FTPAddress), "Basic", theNetworkCredential)
            FileNames = System.IO.Directory.GetFiles("\\" & FTPAddress & "\" & FTPFolderName)

            'CollfileInfo = DirInfo.GetFiles("*.txt")
            'theNetcache.Remove(New Uri("\\aizaz-pc"), "Basic")
            'Return True
            'Else
            'Return False
            'End If
            'If DirInfo.Exists Then
            '    Return True
            'Else
            '    Return False
            'End If
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    
#End Region
#Region "Drop Down Events"
    Private Sub LoadddlCountry()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCountry.Items.Clear()
            ddlCountry.Items.Add(lit)
            ddlCountry.AppendDataBoundItems = True
            ddlCountry.DataSource = ICCountryController.GetCountryActiveAndApproved()
            ddlCountry.DataTextField = "CountryName"
            ddlCountry.DataValueField = "CountryCode"
            ddlCountry.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlProvince()
        Try
            ddlProvince.Items.Clear()
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlProvince.Items.Add(lit)
            ddlProvince.AppendDataBoundItems = True
            ddlProvince.DataSource = ICProvinceController.GetProvinceActiveAndApprove(ddlCountry.SelectedValue.ToString())
            ddlProvince.DataTextField = "ProvinceName"
            ddlProvince.DataValueField = "ProvinceCode"
            ddlProvince.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCity()
        Try
            ddlCity.Items.Clear()
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCity.Items.Add(lit)
            ddlCity.AppendDataBoundItems = True
            ddlCity.DataSource = ICCityController.GetCityActiveAndApprovedByProvinceCode(ddlProvince.SelectedValue.ToString())
            ddlCity.DataTextField = "CityName"
            ddlCity.DataValueField = "CityCode"
            ddlCity.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try




    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        LoadddlProvince()
        LoadddlCity()
    End Sub

    Protected Sub ddlProvince_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProvince.SelectedIndexChanged
        LoadddlCity()
    End Sub
#End Region
#Region "Other Functions/Routines"

    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return Result
        End Try
    End Function
    Private Sub Clear()
        'txtCompanyCode.Text = ""
        txtCompanyName.Text = ""
        txtCompanyPhone1.Text = ""
        txtCompanyPhone2.Text = ""
        txtCompanyEmail.Text = ""
        txtCompanyAddress.Text = ""

        ddlCity.ClearSelection()
        ddlProvince.ClearSelection()
        ddlCountry.ClearSelection()
        ddlGroup.ClearSelection()
        chkActive.Checked = True
        chkApproved.Checked = False
        chkSkipPaymentQueue.Checked = False
        ' chkSingleSignatory.Checked = False
        chkAllowFetchMT940.Checked = False
    End Sub

    Private Function CheckDuplicate(ByVal objICCompany As ICCompany) As Boolean
        Try
            Dim rslt As Boolean = False

            Dim objICCompanyColl As ICCompanyCollection
            objICCompanyColl = New ICCompanyCollection

            If CompanyCode = "0" Then
                objICCompanyColl.Query.Where(objICCompanyColl.Query.CompanyName.ToLower.Trim = objICCompany.CompanyName.ToLower.Trim)
                If objICCompanyColl.Query.Load() Then
                    rslt = True
                End If
            Else
                objICCompanyColl.Query.Where(objICCompanyColl.Query.CompanyName.ToLower.Trim = objICCompany.CompanyName.ToLower.Trim)
                objICCompanyColl.Query.Where(objICCompanyColl.Query.CompanyCode.ToLower.Trim <> CompanyCode.ToLower.Trim)
                If objICCompanyColl.Query.Load() Then
                    rslt = True
                End If
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
#End Region

    Protected Sub btnApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproved.Click
        If Page.IsValid Then
            Try

                Dim objICCompany As New ICCompany
                objICCompany.es.Connection.CommandTimeout = 3600

                objICCompany.LoadByPrimaryKey(CompanyCode)
                If ICCompanyController.IsAllDataExistForCompanyToApprove(CompanyCode) = True Then
                    If CheckIsAdminOrSuperUser() = False Then
                        If objICCompany.CreatedBy = Me.UserId Then
                            UIUtilities.ShowDialog(Me, "Error", "Company must be approved by user other than maker", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        Else
                            ICCompanyController.ApproveCompany(objICCompany.CompanyCode, chkApproved.Checked, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            If chkApproved.Checked = True Then
                                UIUtilities.ShowDialog(Me, "Save Company", "Company approved successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                            ElseIf chkApproved.Checked = False Then
                                UIUtilities.ShowDialog(Me, "Save Company", "Company not approved successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                            End If
                        End If
                    Else
                        ICCompanyController.ApproveCompany(objICCompany.CompanyCode, chkApproved.Checked, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        If chkApproved.Checked = True Then
                            UIUtilities.ShowDialog(Me, "Save Company", "Company approved successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        ElseIf chkApproved.Checked = False Then
                            UIUtilities.ShowDialog(Me, "Save Company", "Company not approved successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        End If
                    End If
                Else
                    UIUtilities.ShowDialog(Me, "Save Company", "Company not approve successfully due to following reason<br/>1. Required tagging is not completed for company to approve.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())

                End If



            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub


    Protected Sub chkAllowFetchMT940_CheckedChanged(sender As Object, e As EventArgs) Handles chkAllowFetchMT940.CheckedChanged
        Try
            If chkAllowFetchMT940.Checked = True Then
                DirectCast(Me.Control.FindControl("trFetchingMechanism"), HtmlTableRow).Style.Remove("display")
                ClearFetchingMechanism()
                ClearFTPDetails()

                tblVIAFTP.Style.Add("display", "none")

                radFTPDetails.Enabled = False

                rfvddlFetchingFrequency.Enabled = True
                rfvFetchingMechanism.Enabled = True
            Else
                DirectCast(Me.Control.FindControl("trFetchingMechanism"), HtmlTableRow).Style.Add("display", "none")
                ClearFetchingMechanism()
                ClearFTPDetails()

                tblVIAFTP.Style.Add("display", "none")

                radFTPDetails.Enabled = False

                rfvddlFetchingFrequency.Enabled = False
                rfvFetchingMechanism.Enabled = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ClearFetchingMechanism()
        rbFetchingMechanism.ClearSelection()
        ddlFetchingFrequency.ClearSelection()
        radFTPDetails.GetSettingByBehaviorID("reFTPFost").Validation.IsRequired = "false"
        radFTPDetails.GetSettingByBehaviorID("reFTPUserName").Validation.IsRequired = "false"
        radFTPDetails.GetSettingByBehaviorID("reFTPFolder").Validation.IsRequired = "false"
        radFTPDetails.GetSettingByBehaviorID("reFTPPassword").Validation.IsRequired = "false"

    End Sub
    Private Sub ClearFTPDetails()
        txtFTPHost.Text = ""
        txtFTPUserName.Text = ""
        txtFTPPassword.Text = ""
        txtFTPFolder.Text = ""
    End Sub
    Protected Sub lnkChangePasswordFTP_Click(sender As Object, e As EventArgs) Handles lnkChangePassword.Click
        Try
            txtFTPPassword.Enabled = True
            lblReqFTPPassword.Visible = False
            radFTPDetails.GetSettingByBehaviorID("reFTPPassword").Validation.IsRequired = True
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub rbFetchingMechanism_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbFetchingMechanism.SelectedIndexChanged
        Try
            If rbFetchingMechanism.SelectedValue = "VIA FTP" Then
                ClearFTPDetails()
                tblVIAFTP.Style.Remove("display")
                radFTPDetails.Enabled = True
                radFTPDetails.GetSettingByBehaviorID("reFTPFost").Validation.IsRequired = True
                radFTPDetails.GetSettingByBehaviorID("reFTPFolder").Validation.IsRequired = True
            ElseIf rbFetchingMechanism.SelectedValue = "VIA Network" Then
                ClearFTPDetails()
                tblVIAFTP.Style.Remove("display")
                radFTPDetails.Enabled = True
                radFTPDetails.GetSettingByBehaviorID("reFTPFost").Validation.IsRequired = True
                radFTPDetails.GetSettingByBehaviorID("reFTPFolder").Validation.IsRequired = True
            Else
                ClearFTPDetails()
                tblVIAFTP.Style.Add("display", "none")
                radFTPDetails.Enabled = False
            End If
        Catch ex As Exception

        End Try
    End Sub
    

    Public Shared Function CheckDuplicateFTPAndNetworkLocations(ByVal objCompany As ICCompany, ByVal IsUpdate As Boolean) As Boolean
        Dim objICCompany As New ICCompany
        Dim Result As Boolean = False
        objICCompany.Query.Where(objICCompany.Query.HostName = objCompany.HostName And objICCompany.Query.FolderName = objCompany.FolderName)
        objICCompany.Query.Where(objICCompany.Query.MT940StatementLocation = objCompany.MT940StatementLocation)
        objICCompany.Query.Where(objICCompany.Query.UserName = objCompany.UserName)
        If IsUpdate = True Then
            objICCompany.Query.Where(objICCompany.Query.CompanyCode <> objCompany.CompanyCode)
        End If
        If objICCompany.Query.Load() Then
            Result = True
        End If
        Return Result
    End Function

    
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub
End Class
