﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveWebServiceDetails.ascx.vb" Inherits="DesktopModules_CompanyManagement_SaveWebServiceDetails" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
    <style type="text/css">

    .headingblue
    {
        font-weight: 700;
    }
    </style>
    <script type="text/javascript">
        function con() {
            if (window.confirm("Are you sure you wish to remove this Record?") == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function delcon(item) {
            if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
                return true;
            }
            else {
                return false;
            }
        }    
</script>
<telerik:RadInputManager ID="rimFTP" runat="server">
    <telerik:TextBoxSetting BehaviorID="ftpID" EmptyMessage="Enter FTP User ID">
        <TargetControls>
            <telerik:TargetInput ControlID="txtWSID" />
        </TargetControls>
    </telerik:TextBoxSetting>
   
    <telerik:TextBoxSetting BehaviorID="ftpPassword" EmptyMessage="Enter FTP Server Password">
        <TargetControls>
            <telerik:TargetInput ControlID="txtWSPasword" />
        </TargetControls>
    </telerik:TextBoxSetting>
      <telerik:TextBoxSetting BehaviorID="radWSIPAddress" EmptyMessage="Enter FTP Server Password">
        <TargetControls>
            <telerik:TargetInput ControlID="txtWSIPAddress" />
        </TargetControls>
    </telerik:TextBoxSetting>
    
    
</telerik:RadInputManager>
<table id="tblFTPInfo" width="100%" runat="server" >
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%">
            <asp:Label ID="lblPageHeader" runat="server" Text="Add Web Service Details" CssClass="headingblue"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 50%">
                                    &nbsp;</td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
                                </td>
                                <td align="left" valign="top" style="width: 50%">
                                    &nbsp;</td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%">
                                    &nbsp;</td>
                                <td align="left" valign="top" style="width: 50%">
                                    &nbsp;</td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%">
                                    <asp:Label ID="lblWSUserID" runat="server" Text="Web Service  User ID" 
                                        CssClass="lbl"></asp:Label>
                                    <strong>
                                        <asp:Label ID="Label37" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </strong>
                                </td>
                                <td align="left" valign="top" style="width: 50%">
                                    <asp:Label ID="lblWSPasword" runat="server" Text="Web Service Password" 
                                        CssClass="lbl"></asp:Label>
                                    <strong>
                                        <asp:Label ID="Label36" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </strong>
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%">
                                    <asp:TextBox ID="txtWSID" runat="server" CssClass="txtbox" MaxLength="15"></asp:TextBox>
                                </td>
                                <td align="left" valign="top" style="width: 50%">
                                    <asp:TextBox ID="txtWSPasword" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%" >
                                    <asp:Label ID="lblWSPasword0" runat="server" Text="Web Service IP Address" 
                                        CssClass="lbl"></asp:Label>
                                    <strong>
                                        <asp:Label ID="Label38" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </strong>
                                </td>
                                 <td align="left" valign="top" style="width: 50%">
                                    &nbsp;</td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%" >
                                    <asp:TextBox ID="txtWSIPAddress" runat="server" CssClass="txtbox" 
                                        MaxLength="50"></asp:TextBox>
                                </td>
                                 <td align="left" valign="top" style="width: 50%">
                                     &nbsp;</td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%" >
                                    &nbsp;</td>
                                 <td align="left" valign="top" style="width: 50%">
                                     &nbsp;</td>
                            </tr>
                            <tr align="left" valign="top" style="width: 50%">
                                <td align="center" valign="top" colspan="2" >
                                &nbsp;<asp:Button ID="btnSaveFTPDetails" runat="server" CssClass="btn" Text="Save" 
                                        CausesValidation="False" Width="80px" />
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%">
                                    &nbsp;</td>
                                 <td align="left" valign="top" style="width: 50%">
                                    &nbsp;</td>
                            </tr>
                           
                        </table>