﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveCompany.ascx.vb" Inherits="DesktopModules_CompanyManagement_SaveCompany" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<style type="text/css">
    .style1
    {
        width: 25%;
        height: 23px;
    }
.RadPicker{vertical-align:middle}.rdfd_{position:absolute}.RadPicker .rcTable{table-layout:auto}.RadPicker .RadInput{vertical-align:baseline}.RadInput_Default{font:12px "segoe ui",arial,sans-serif}.riSingle{box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;-webkit-box-sizing:border-box;-khtml-box-sizing:border-box}.riSingle{position:relative;display:inline-block;white-space:nowrap;text-align:left}.RadInput{vertical-align:middle}.riSingle .riTextBox{box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;-webkit-box-sizing:border-box;-khtml-box-sizing:border-box}.RadPicker_Default .rcCalPopup{background-position:0 0}.RadPicker_Default .rcCalPopup{background-image:url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Calendar.sprite.gif')}.RadPicker .rcCalPopup{display:block;overflow:hidden;width:22px;height:22px;background-color:transparent;background-repeat:no-repeat;text-indent:-2222px;text-align:center}
    div.RadPicker table.rcSingle .rcInputCell{padding-right:0}div.RadPicker table.rcSingle .rcInputCell{padding-right:0}div.RadPicker table.rcSingle .rcInputCell{padding-right:0}div.RadPicker table.rcSingle .rcInputCell{padding-right:0}.RadPicker table.rcTable .rcInputCell{padding:0 4px 0 0}.RadPicker table.rcTable .rcInputCell{padding:0 4px 0 0}.RadPicker table.rcTable .rcInputCell{padding:0 4px 0 0}.RadPicker table.rcTable .rcInputCell{padding:0 4px 0 0}.RadPicker table.rcTable .rcInputCell{padding:0 4px 0 0}.RadPicker table.rcTable .rcInputCell{padding:0 4px 0 0}.RadPicker table.rcTable .rcInputCell{padding:0 4px 0 0}.RadPicker table.rcTable .rcInputCell{padding:0 4px 0 0}
    </style>--%>

<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }
   </script>
<script language="javascript" type="text/javascript">
    function ValidateIsBene(source, args) {
        var chkListModules = document.getElementById('<%= chkboxlstIsBene.ClientID %>');
        var chkListinputs = chkListModules.getElementsByTagName("input");
        for (var i = 0; i < chkListinputs.length; i++) {
            if (chkListinputs[i].checked) {
                args.IsValid = true;
                return;
            }
        }
        args.IsValid = false;
    }
</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior2" EmptyMessage="type.."
        Type="Number">
    </telerik:NumericTextBoxSetting>
   
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior2" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9.-_/]{2,10}$" ErrorMessage="Invalid Company Code"
        EmptyMessage="Enter Company Code">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCompanyCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <%--<telerik:DateInputSetting BehaviorID="reDateInput" Validation-IsRequired="true"
         ErrorMessage="Invalid Billing Date"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="raddpBillingDate" />
        </TargetControls>
    </telerik:DateInputSetting>
    
   <%--  <telerik:RegExpTextBoxSetting BehaviorID="reCuttoftimestart" Validation-IsRequired="false"
        ValidationExpression="^(([0]?[1-9]|1[0-2])(:)([0-5][0-9]))$" ErrorMessage="Invalid time"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCutoffTimeStart" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>--%>
  
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="true"
        EmptyMessage="" ErrorMessage="Invalid Company Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCompanyName" />
        </TargetControls>
    </telerik:TextBoxSetting>
     <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehaviorPhone1" ValidationExpression="^\+?[\d- ]{2,15}$"
         ErrorMessage="Invalid Phone Number" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCompanyPhone1" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior3" ValidationExpression="^\+?[\d- ]{2,15}$"
        ErrorMessage="Invalid Phone Number" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCompanyPhone2" />
        </TargetControls>
        </telerik:RegExpTextBoxSetting>
         <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehaviorEmail" Validation-IsRequired="false"
        EmptyMessage="" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.%]\w+)*\.\w+([-.]\w+)*$"
        ErrorMessage="Invalid Email">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCompanyEmail" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
     <telerik:TextBoxSetting BehaviorID="RagExpBehaviorAddress" 
        ErrorMessage="Invalid Address" EmptyMessage="" Validation-IsRequired="true">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCompanyAddress" />
        </TargetControls>
    </telerik:TextBoxSetting>
 
</telerik:RadInputManager>
<telerik:RadInputManager ID="radFTPDetails" runat="server" Enabled="false">
    
   
    <telerik:RegExpTextBoxSetting BehaviorID="reFTPFost" Validation-IsRequired="false"
        ValidationExpression="^[a-zA-Z0-9-._/]{1,100}$" ErrorMessage="Invalid Server Name"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFTPHost" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
     <telerik:RegExpTextBoxSetting BehaviorID="reFTPUserName" Validation-IsRequired="false"
        ValidationExpression="^[a-zA-Z0-9-._/]{1,150}$" ErrorMessage="Invalid User Name"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFTPUserName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
   <telerik:TextBoxSetting BehaviorID="reFTPFolder" Validation-IsRequired="false"
         ErrorMessage="Invalid Folder"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFTPFolder" />
        </TargetControls>
    </telerik:TextBoxSetting>
  
    <telerik:TextBoxSetting BehaviorID="reFTPPassword" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Invalid Password">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFTPPassword" />
        </TargetControls>
    </telerik:TextBoxSetting>
   
 
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGrpCode" runat="server" Text="Select Group" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" 
                CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvGroup" runat="server" 
                ControlToValidate="ddlGroup" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Group" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyName" runat="server" Text="Company Name" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyPhone1" runat="server" Text="Company Phone 1" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtCompanyName" runat="server" CssClass="txtbox" 
                MaxLength="150"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtCompanyPhone1" runat="server" CssClass="txtbox" 
                MaxLength="15"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyPhone2" runat="server" Text="Company Phone 2" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyEmail" runat="server" Text="Company Email" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtCompanyPhone2" runat="server" CssClass="txtbox" 
                MaxLength="15"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtCompanyEmail" runat="server" CssClass="txtbox" 
                MaxLength="100"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyAddress" runat="server" Text="Company Address" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label12" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtCompanyAddress" runat="server" CssClass="txtbox" 
                MaxLength="250" TextMode="MultiLine" Height="70px"  onkeypress="return textboxMultilineMaxNumber(this,250)"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
        </td>
        <td align="left" valign="top" class="style1">
        </td>
        <td align="left" valign="top" class="style1">
        </td>
        <td align="left" valign="top" class="style1">
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyCountry" runat="server" Text="Select Country" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label9" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyProvince" runat="server" Text="Select Province" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label10" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCountry" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlProvince" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvGroup0" runat="server" 
                ControlToValidate="ddlCountry" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Country" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvGroup1" runat="server" 
                ControlToValidate="ddlProvince" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Province" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyCity" runat="server" Text="Select City" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label11" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBillingDate" runat="server" Text="Billing Date" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCity" runat="server" 
                CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 40%">
            <telerik:RadDatePicker ID="raddpBillingDate" runat="server"
                DateInput-DateFormat="dd-MMM-yyyy" DateInput-EmptyMessage=""
                EnableTyping="false" ImagesPath=""  Skin="Telerik">
<Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x" Skin="Telerik"></Calendar>

<DateInput DisplayDateFormat="dd" DateFormat="dd-MMM-yyyy" DisplayText="" LabelWidth="40%" 
    type="text" value="" ReadOnly="True" ></DateInput>

<DatePopupButton ImageUrl="" HoverImageUrl="" Visible="true"></DatePopupButton>
            </telerik:RadDatePicker>
                 <td align="left" valign="top" style="width: 10%">
                                                                
                                                                &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvcity" runat="server" 
                ControlToValidate="ddlCity" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select City" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;<asp:Label ID="lblCompanyCountry0" runat="server" CssClass="lbl" Text="Beneficiary Management"></asp:Label>
            <asp:Label ID="Label13" runat="server" ForeColor="Red" Text="*"></asp:Label>
            &nbsp; &nbsp; &nbsp;
            <asp:CheckBoxList ID="chkboxlstIsBene" runat="server" CssClass="lbl" RepeatDirection="Vertical">
                <asp:ListItem Value="0"> Open Payments</asp:ListItem>
                <asp:ListItem Value="1"> Beneficiary Required</asp:ListItem>
            </asp:CheckBoxList>
           <br />
            <%--  <asp:RequiredFieldValidator ID="rfvIsBene" runat="server"
                ControlToValidate="rbtnlstIsBene" CssClass="lblEror" Display="Dynamic"
                ErrorMessage="Please select one option" SetFocusOnError="True"
                ToolTip="Required Field"></asp:RequiredFieldValidator>--%>
            <asp:CustomValidator ID="cusvalIsBene" runat="server" ClientValidationFunction="ValidateIsBene" ErrorMessage="Please select atleast one option"></asp:CustomValidator>
            &nbsp; &nbsp; &nbsp; </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblSkipPaymentQueue" runat="server" Text="Skip Payment Queue" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <%--<asp:Label ID="lblIsSingleSignatory" runat="server" Text="Is Single Signatory" 
                CssClass="lbl"></asp:Label>--%>
            <asp:Label ID="lblSkipAllowFetchMT940Statements" runat="server" Text="Fetch MT 940 Statements" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkSkipPaymentQueue" runat="server" Text=" " 
                CssClass="chkBox" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
           <%-- <asp:CheckBox ID="chkSingleSignatory" runat="server" Text=" " 
                CssClass="chkBox" />--%>
            <asp:CheckBox ID="chkAllowFetchMT940" runat="server" Text=" " 
                CssClass="chkBox" AutoPostBack="True" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <%--<tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>--%>
    <%--<tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            &nbsp;</td>
        <td align="left" valign="top" class="style1">
            </td>
        <td align="left" valign="top" class="style1">
            </td>
        <td align="left" valign="top" class="style1">
            </td>
    </tr>--%>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    
    <tr align="left" valign="top" style="width: 100%" id="trFetchingMechanism" runat="server">
        <td align="top" valign="left" colspan="4">

            <table id="tblFetchingMechanismType" runat="server" style="width:100%" >
        <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblFetchingMechanismType" runat="server" Text="Select Fetching Mechanism Type" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqFetchMechanismType" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblFetchingFrequency" runat="server" Text="Select Fetching Frequency" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqFetchFrequency" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
        <tr style="width:100%">
                <td align="left" valign="top" style="width: 25%">
                    <asp:RadioButtonList ID="rbFetchingMechanism" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                        <asp:ListItem>VIA FTP</asp:ListItem>
                        <asp:ListItem>VIA Network</asp:ListItem>
                    </asp:RadioButtonList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlFetchingFrequency" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem>Daily</asp:ListItem>
                <asp:ListItem>Weekly</asp:ListItem>
                <asp:ListItem>Monthly</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        </tr>
          <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvFetchingMechanism" runat="server" Enabled="False" ErrorMessage="Please Select Fetching Mechanism" ControlToValidate="rbFetchingMechanism"></asp:RequiredFieldValidator>
              </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlFetchingFrequency" runat="server" 
                ControlToValidate="ddlFetchingFrequency" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Frequency" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0" Enabled="False"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    </table>

        </td>
    
    </tr>
  
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <table id="tblVIAFTP" runat="server" style="width:100%">
                <tr style="width:100%">
                    <td  align="left" valign="top" style="width:25%">
            <asp:Label ID="lblFetchVIAFTPDetails" runat="server" Text="Login Details" 
                CssClass="lbl"></asp:Label>
                    </td>
                    <td  align="left" valign="top" style="width:25%"></td>
                    <td  align="left" valign="top" style="width:25%"></td>
                    <td  align="left" valign="top" style="width:25%"></td>
                </tr>
                <tr style="width:100%">
                    <td  align="left" valign="top" style="width:25%">&nbsp;</td>
                    <td  align="left" valign="top" style="width:25%">&nbsp;</td>
                    <td  align="left" valign="top" style="width:25%">&nbsp;</td>
                    <td  align="left" valign="top" style="width:25%">&nbsp;</td>
                </tr>
                <tr style="width:100%">
                    <td  align="left" valign="top" style="width:25%">
            <asp:Label ID="lblFTPHost" runat="server" Text="Server Name" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqFTPHost" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td  align="left" valign="top" style="width:25%">&nbsp;</td>
                    <td  align="left" valign="top" style="width:25%">
            <asp:Label ID="lblFTPUserName" runat="server" Text="User Name" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqFTPUserName" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td  align="left" valign="top" style="width:25%">&nbsp;</td>
                </tr>
                <tr style="width:100%">
                    <td  align="left" valign="top" style="width:25%">
            <asp:TextBox ID="txtFTPHost" runat="server" CssClass="txtbox" 
                MaxLength="100"></asp:TextBox>
                    </td>
                    <td  align="left" valign="top" style="width:25%">&nbsp;</td>
                    <td  align="left" valign="top" style="width:25%">
            <asp:TextBox ID="txtFTPUserName" runat="server" CssClass="txtbox" 
                MaxLength="150"></asp:TextBox>
                    </td>
                    <td  align="left" valign="top" style="width:25%">&nbsp;</td>
                </tr>
                <tr style="width:100%">
                    <td  align="left" valign="top" style="width:25%">&nbsp;</td>
                    <td  align="left" valign="top" style="width:25%">&nbsp;</td>
                    <td  align="left" valign="top" style="width:25%">&nbsp;</td>
                    <td  align="left" valign="top" style="width:25%">&nbsp;</td>
                </tr>
                <tr style="width:100%">
                    <td  align="left" valign="top" style="width:25%">
            <asp:Label ID="lblFTPPassword" runat="server" Text="Password" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqFTPPassword" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td  align="left" valign="top" style="width:25%">&nbsp;</td>
                    <td  align="left" valign="top" style="width:25%">
            <asp:Label ID="lblFTPFolder" runat="server" Text="Folder" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqFTPFolder" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td  align="left" valign="top" style="width:25%">&nbsp;</td>
                </tr>
                <tr style="width:100%">
                    <td  align="left" valign="top" style="width:25%">
            <asp:TextBox ID="txtFTPPassword" runat="server" CssClass="txtbox" 
                MaxLength="100" TextMode="Password"></asp:TextBox>
                    </td>
                    <td  align="left" valign="top" style="width:25%">
            <asp:LinkButton ID="lnkChangePassword" runat="server" CausesValidation="False">Change Password</asp:LinkButton>
                    </td>
                    <td  align="left" valign="top" style="width:25%">
            <asp:TextBox ID="txtFTPFolder" runat="server" CssClass="txtbox" 
                MaxLength="100"></asp:TextBox>
                    </td>
                    <td  align="left" valign="top" style="width:25%">&nbsp;</td>
                </tr>
            </table>
            <%--  <telerik:RegExpTextBoxSetting BehaviorID="reCuttoftimestart" Validation-IsRequired="false"
        ValidationExpression="^(([0]?[1-9]|1[0-2])(:)([0-5][0-9]))$" ErrorMessage="Invalid time"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCutoffTimeStart" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>--%></td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIsActive" runat="server" Text="Is Active" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIsApproved" runat="server" Text="Is Approved" CssClass="lbl" Visible="False"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkActive" runat="server" Text=" " CssClass="chkBox" Checked="True" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
            <asp:CheckBox ID="chkApproved" runat="server" Text=" " Visible="False" CssClass="chkBox" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnApproved" runat="server" Text="Approve" CssClass="btn" Visible="False"
                Width="71px" />
            &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False"
                />
            &nbsp;
        </td>
    </tr>
</table>
