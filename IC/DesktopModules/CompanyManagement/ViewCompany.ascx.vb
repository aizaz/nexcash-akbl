﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports FtpLib
Imports System.Net
Imports System.DirectoryServices.AccountManagement
Partial Class DesktopModules_CompanyManagement_ViewCompany
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_CompanyManagement_ViewBank_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnAddCompany.Visible = CBool(htRights("Add"))
                LoadCompany(0, Me.gvCompany.PageSize, Me.gvCompany, True)

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Company Management")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
#Region "Drop Down Events"
    Private Sub LoadCompany(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal RadGrid As RadGrid, ByVal DoDataBind As Boolean)
        Try
            ICCompanyController.GetAllCompanyForRadGrid(PageNumber, PageSize, RadGrid, DoDataBind)
            If gvCompany.Items.Count > 0 Then
                gvCompany.Visible = True
                lblRNF.Visible = False
                btnDeleteCompanies.Visible = CBool(htRights("Delete"))
                btnApproveCompany.Visible = CBool(htRights("Approve"))
                gvCompany.Columns(7).Visible = CBool(htRights("Delete"))
            Else
                gvCompany.Visible = False
                lblRNF.Visible = True
                btnDeleteCompanies.Visible = False
                btnApproveCompany.Visible = False
            End If
            'If CheckIsAdminOrSuperUser() = False Then
            '    If ArrRights.Count > 0 Then
            '        gvCompany.Columns(5).Visible = ArrRights(2)
            '    Else
            '        UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL())
            '        Exit Sub
            '    End If
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try





    End Sub


#End Region
#Region "Button Events"
    Protected Sub btnSaveBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCompany.Click
        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveCompany", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If
        'ICCompanyController.DownloadAllMT940StatementsFromLocation()
    End Sub
#End Region

    
    Protected Sub gvCompany_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvCompany.ItemCommand
        Try
            Dim objICCompany As New ICCompany
            objICCompany.es.Connection.CommandTimeout = 3600
            If e.CommandName = "del" Then
                If objICCompany.LoadByPrimaryKey(e.CommandArgument.ToString) Then
                    If objICCompany.IsApprove = True Then
                        UIUtilities.ShowDialog(Me, "Error", "Company is approved and can not be deleted", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    Else
                        ICCompanyController.DeleteCompanyByCode(objICCompany.CompanyCode.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        UIUtilities.ShowDialog(Me, "Delete Company", "Company deleted successfully", ICBO.IC.Dialogmessagetype.Success)
                        LoadCompany(Me.gvCompany.CurrentPageIndex + 1, Me.gvCompany.PageSize, Me.gvCompany, True)
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Error ", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                LoadCompany(Me.gvCompany.CurrentPageIndex + 1, Me.gvCompany.PageSize, Me.gvCompany, False)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvCompany_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvCompany.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvCompany.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvCompany_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvCompany.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAllCompany"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvCompany.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub gvCompany_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvCompany.NeedDataSource
        Try
            LoadCompany(Me.gvCompany.CurrentPageIndex + 1, Me.gvCompany.PageSize, Me.gvCompany, False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    
    Protected Sub gvCompany_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvCompany.PageIndexChanged
        Try
            gvCompany.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnDeleteCompanies_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteCompanies.Click
        Try
            Dim chkBox As CheckBox
            Dim objICCompany As ICCompany
            Dim PassToDeleteCount As Integer = 0
            Dim FailToDeleteCount As Integer = 0
            Dim CompanyCode As String

            If CheckGVCompanySelected() = False Then
                UIUtilities.ShowDialog(Me, "Error", "Please select at least one company", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            For Each gvCompanyRow As GridDataItem In gvCompany.Items
                chkBox = New CheckBox
                chkBox = DirectCast(gvCompanyRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkBox.Checked = True Then
                    objICCompany = New ICCompany
                    CompanyCode = gvCompanyRow.GetDataKeyValue("CompanyCode")
                    If objICCompany.LoadByPrimaryKey(CompanyCode.ToString) Then
                        If objICCompany.IsApprove = True Then
                            FailToDeleteCount = FailToDeleteCount + 1
                        Else
                            ICCompanyController.DeleteCompanyByCode(objICCompany.CompanyCode.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            PassToDeleteCount = PassToDeleteCount + 1

                        End If
                    End If
                End If
            Next
            If Not PassToDeleteCount = 0 And Not FailToDeleteCount = 0 Then
                LoadCompany(Me.gvCompany.CurrentPageIndex + 1, Me.gvCompany.PageSize, Me.gvCompany, True)
                UIUtilities.ShowDialog(Me, "Delete Company", "[ " & PassToDeleteCount.ToString & " ] Company(s) deleted successfuly.<br /> [ " & FailToDeleteCount.ToString & " ] Company(s) can not be deleted due to following reason: <br /> 1. Company is approved<br /> 2. Delete associated records", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            ElseIf Not PassToDeleteCount = 0 And FailToDeleteCount = 0 Then
                LoadCompany(Me.gvCompany.CurrentPageIndex + 1, Me.gvCompany.PageSize, Me.gvCompany, True)
                UIUtilities.ShowDialog(Me, "Delete Company", PassToDeleteCount.ToString & " Company(s) deleted successfuly.", ICBO.IC.Dialogmessagetype.Success)

                Exit Sub
            ElseIf Not FailToDeleteCount = 0 And PassToDeleteCount = 0 Then
                LoadCompany(Me.gvCompany.CurrentPageIndex + 1, Me.gvCompany.PageSize, Me.gvCompany, True)
                UIUtilities.ShowDialog(Me, "Delete Company", "[ " & FailToDeleteCount.ToString & " ] Company(s) can not be deleted due to following reasons: <br /> 1. Company is approved<br /> 2. Delete associated records", ICBO.IC.Dialogmessagetype.Failure)

                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckGVCompanySelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvCompanyRow As GridDataItem In gvCompany.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvCompanyRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnApproveCompany_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveCompany.Click
        If Page.IsValid Then
            Try

                Dim chkSelect As CheckBox
                Dim CompanyCode As String = ""
                Dim PassToApproveCount As Integer = 0
                Dim FailToApproveCount As Integer = 0
                Dim objICCompany As ICCompany
                If CheckGVCompanySelected() = False Then
                    UIUtilities.ShowDialog(Me, "Save Company", "Please select atleast one company.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVCompany In gvCompany.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVCompany.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        CompanyCode = rowGVCompany.GetDataKeyValue("CompanyCode").ToString
                        objICCompany = New ICCompany
                        objICCompany.es.Connection.CommandTimeout = 3600
                        objICCompany.LoadByPrimaryKey(CompanyCode)
                        If ICCompanyController.IsAllDataExistForCompanyToApprove(CompanyCode) = True Then
                            If Me.UserInfo.IsSuperUser = False Then
                                If objICCompany.IsApprove = True Or objICCompany.CreatedBy = Me.UserId Then
                                    FailToApproveCount = FailToApproveCount + 1
                                Else
                                    Try

                                        ICCompanyController.ApproveCompany(objICCompany.CompanyCode.ToString, chkSelect.Checked, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                        PassToApproveCount = PassToApproveCount + 1
                                    Catch ex As Exception
                                        FailToApproveCount = FailToApproveCount + 1
                                        Continue For
                                    End Try
                                End If

                            Else
                                If objICCompany.IsApprove = True Then
                                    FailToApproveCount = FailToApproveCount + 1
                                Else
                                    Try
                                        ICCompanyController.ApproveCompany(objICCompany.CompanyCode.ToString, chkSelect.Checked, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                        PassToApproveCount = PassToApproveCount + 1
                                    Catch ex As Exception
                                        FailToApproveCount = FailToApproveCount + 1
                                        Continue For

                                    End Try
                                End If
                            End If
                        Else
                            FailToApproveCount = FailToApproveCount + 1
                            Continue For
                        End If


                    End If
                Next

                If Not PassToApproveCount = 0 And Not FailToApproveCount = 0 Then
                    LoadCompany(Me.gvCompany.CurrentPageIndex + 1, Me.gvCompany.PageSize, Me.gvCompany, True)
                    UIUtilities.ShowDialog(Me, "Save Company", "[ " & PassToApproveCount.ToString & " ] Company(s) approved successfully.<br /> [ " & FailToApproveCount.ToString & " ] Company(s) can not be approve due to following reasons: <br /> 1. Company is already approved<br /> 2. Company must be approved by user other than maker<br/>3. Required tagging is not completed for company to approve.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToApproveCount = 0 And FailToApproveCount = 0 Then
                    LoadCompany(Me.gvCompany.CurrentPageIndex + 1, Me.gvCompany.PageSize, Me.gvCompany, True)
                    UIUtilities.ShowDialog(Me, "Save Company", PassToApproveCount.ToString & " Company(s) approved successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToApproveCount = 0 And PassToApproveCount = 0 Then
                    LoadCompany(Me.gvCompany.CurrentPageIndex + 1, Me.gvCompany.PageSize, Me.gvCompany, True)
                    UIUtilities.ShowDialog(Me, "Save Company", "[ " & FailToApproveCount.ToString & " ] Company(s) can not be approved due to following reasons: <br /> 1. Company is already approve<br /> 2. Company must be approved by user other than maker<br/>3. Required tagging is not completed for company to approve.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
End Class
