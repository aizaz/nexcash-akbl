﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ChangePassword.ascx.vb" Inherits="DesktopModules_Change_Password_ChangePassword" %>


<style type="text/css">
    .Grid
    {
        border: 1px solid #EEEEEE;
    }
    
    
    .Grid td
    {
        border: 1px solid #EEEEEE;
    }
    
    .tr
    {
        background-color: #F9F9F9;
        height: 40px;
        padding: 5px;
        text-align: center;
    }
    
    .Altertr
    {
        background-color: #FFFFFF;
        height: 40px;
        padding: 5px;
        text-align: center;
    }
    
    .gridView
    {
        border: 1px solid #eeeeee;
    }
    .RowStyle
    {
        background: #F9F9F9;
        vertical-align: middle;
        text-align: center;
        font-family: Arial;
        font-size: 14px;
        padding: 5px;
        color: #7e7c7c;
    }
    
    .AltRowStyle
    {
        background: #FFFFFF;
        vertical-align: middle;
        text-align: center;
        font-family: Arial;
        font-weight: normal;
        font-size: 14px;
        padding: 5px;
        color: #7e7c7c;
    }
    
    .HeaderStyle
    {
        background: #E7E7E7;
        vertical-align: middle;
        text-align: center;
        font-family: Arial;
        font-size: 14px;
        color: #7e7c7c;
    }
    
    .lblMainHeading
    {
        font-family: Arial;
        font-weight: bold;
        font-size: 18px;
        padding: 5px;
        color: #7e7c7c;
    }
    
    .lblHeading
    {
    }
    
    .lblSubHeading
    {
        font-family: Arial;
        font-weight: bold;
        font-size: 14px;
        padding: 5px;
        color: #7e7c7c;
    }
    
    .lblSubHeadingValue
    {
        font-family: Arial;
        font-weight: normal;
        font-size: 14px;
        padding: 5px;
        color: #7e7c7c;
    }

    
    .lblHeadingOposite1
    {
        font-family: Arial;
        font-weight: normal;
        font-size: 14px;
        color: #7e7c7c;
        vertical-align:top;
    }
    
    
    
    .lblError
    {
        background: #FFFFFF;
        vertical-align: middle;
        text-align: center;
        font-family: Arial;
        font-weight: normal;
        font-size: 14px;
        padding: 5px;
        color: #7e7c7c;
    }
    
      .lblErrors
    {
        font-family: Arial;
        font-weight: bold;
        font-size: 14px;
        padding: 5px;
        color: Red;
    }
    </style>

<asp:Panel ID="pnlforgot" runat="server">
    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
        
        <tr>
            <td valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center" colspan="2" valign="middle">
                            &nbsp;&nbsp;<asp:Label ID="Label3" runat="server" Font-Size="14px" ForeColor="Red" 
                                Visible="False" Width="334px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="middle">
                            <table align="center" width="100%">
                                <tr>
                                    <td align="center" valign="middle">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td align="left" colspan="2">
                                                    <span class="text">Enter current password:</span></td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="2">
                                                    <asp:TextBox ID="txtCurrentPassword" runat="server" CssClass="txtbox" MaxLength="12" 
                                                        TextMode="Password" ValidationGroup="change"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                                        ControlToValidate="txtCurrentPassword" ErrorMessage="*" ValidationGroup="change"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="2">
                                                    <span class="text">Set new password:</span></td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="2">
                                                    <asp:TextBox ID="txtNewPassword" runat="server" CssClass="txtbox" MaxLength="12" 
                                                        TextMode="Password" ValidationGroup="change"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                                                        ControlToValidate="txtNewPassword" ErrorMessage="*" ValidationGroup="change"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="2">
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                                        ControlToValidate="txtNewPassword" Display="Dynamic" ErrorMessage="Must have at least 1 number, 1 special character, 1 upper case letter, 1 lower case letter,
        and minimum of 7 characters." ValidationExpression="(?=^.{7,}$)((?=.*\d)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
        ValidationGroup="change"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="2">
                                                    <span class="text">Confirm the new password:</span></td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="2">
                                                    <asp:TextBox ID="txtConfrimNewPassword" runat="server" CssClass="txtbox" MaxLength="12" 
                                                        TextMode="Password" ValidationGroup="change"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                                                        ControlToValidate="txtConfrimNewPassword" ErrorMessage="*" ValidationGroup="change"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="2">
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                                                        ControlToValidate="txtConfrimNewPassword" Display="Dynamic" ErrorMessage="Must have at least 1 number, 1 special character, 1 upper case letter, 1 lower case letter,
        and minimum of 7 characters." ValidationExpression="(?=^.{7,}$)((?=.*\d)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
        ValidationGroup="change"></asp:RegularExpressionValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="2">
                                                    <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                                        ControlToCompare="txtNewPassword" ControlToValidate="txtConfrimNewPassword" 
                                                        ErrorMessage="The passwords you typed do not match. Type the new password in both text boxes." 
                                                        ValidationGroup="change"></asp:CompareValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" colspan="2" valign="top">
                                                    <table width="100%">
                                                        <tr align="left" valign="top">
                                                            <td style="width:100px;">
                                                                <asp:Label ID="Label6" runat="server" CssClass="lblErrors">Please Note:</asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="Label1" runat="server" CssClass="lblHeadingOposite1" Text="Your password should be 7-12 characters with at least 1 alphabet, 1 digit and 1 
                                                                    special character." Width="100%"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top">
                                                </td>
                                                <td align="left">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right" colspan="2">
                                                    <table style="width: 100%;">
                                                        <tr>
                                                            <td align="left">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="help">
                                                                <asp:ImageButton ID="btnsendpassword" runat="server" 
                                                                    ImageUrl="image/btnsubmit.png" ValidationGroup="change" />
                                                                </span>
                                                            </td>
                                                            <td align="right">
                                                                <%--<span class="help">
                                                                <asp:LinkButton ID="btnforgotpassword0" runat="server" CausesValidation="False">Back to Login page</asp:LinkButton>
                                                                </span>--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="24">
                            &nbsp;
                        </td>
                        <td style="padding-left: 150px;" width="312">
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>