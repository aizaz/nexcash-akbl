﻿Imports ICBO
Imports ICBO.IC
Partial Class DesktopModules_Change_Password_ChangePassword
    Inherits Entities.Modules.PortalModuleBase
    Protected Sub btnsendpassword_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnsendpassword.Click
        If Page.IsValid Then
            Try
                Dim url As String
                url = NavigateURL(41)
                Dim userInfo As DotNetNuke.Entities.Users.UserInfo = DotNetNuke.Entities.Users.UserController.GetUserByName(Me.PortalId, Me.UserInfo.Username)
                Dim cuser As New ICUser
                Dim HashedPassword As String = ""
                cuser.es.Connection.CommandTimeout = 3600
                cuser.LoadByPrimaryKey(Me.UserId)
                Dim password As String = DotNetNuke.Entities.Users.UserController.GetPassword(userInfo, userInfo.Membership.PasswordAnswer)
                If password = txtCurrentPassword.Text Then
                    If txtCurrentPassword.Text <> txtNewPassword.Text Then
                        ' Check Password History
                        HashedPassword = ICUserPasswordHistoryController.GenerateMD5Hash(txtConfrimNewPassword.Text.ToString().Trim())
                        HashedPassword = HashedPassword.Trim.ToLower.ToString()

                        If ICUserPasswordHistoryController.CheckUserPasswordHistory(Me.UserId, HashedPassword) = True Then
                            UserController.ChangePassword(Me.UserInfo, txtCurrentPassword.Text, txtConfrimNewPassword.Text)
                            ' Add new password in password history table
                            Dim objICUserPWDHistory As New ICUserPasswordHistory
                            objICUserPWDHistory.UserID = Me.UserId
                            objICUserPWDHistory.Password = HashedPassword
                            objICUserPWDHistory.CreatedDate = Date.Now
                            objICUserPWDHistory.CreatedBy = Me.UserId
                            ICUserPasswordHistoryController.AddICUserPasswordHistory(objICUserPWDHistory)

                            cuser.Password = txtNewPassword.Text.ToString
                            ICUserController.AddICUser(cuser, True, cuser.UserID, cuser.UserName, "Password changed by User [UserName: " & cuser.UserName & "]", cuser.UserType)
                            cuser = New ICUser
                            cuser.es.Connection.CommandTimeout = 3600
                            cuser.LoadByPrimaryKey(Me.UserId)
                            cuser.IsApproved = True
                            cuser.Save()
                            UIUtilities.ShowDialog(Me, "Password Information", "Your password has been changed successfully", Dialogmessagetype.Success, NavigateURL(41))
                            EmailUtilities.PasswordChange(cuser, txtConfrimNewPassword.Text.ToString)
                            SMSUtilities.PasswordChange(cuser, txtConfrimNewPassword.Text.ToString)
                        Else
                            UIUtilities.ShowDialog(Me, "Password Information", "The new password should not be same as previous two passwords, please try again", Dialogmessagetype.Failure)
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Password Information", "New password cannot be the same as the old password", Dialogmessagetype.Failure)
                    End If
                Else
                    UIUtilities.ShowDialog(Me, "Password Information", "Current password is incorrect", Dialogmessagetype.Failure)
                End If
            Catch ex As Exception
                UIUtilities.ShowDialog(Me, "Password Information", ex.Message, Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Protected Sub DesktopModules_Change_Password_ChangePassword_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            If Not Request.QueryString("PasswordExpired") Is Nothing Then
                UIUtilities.ShowDialog(Me, "Password Expired", "Your password is expired and it must be changed.", Dialogmessagetype.Warning)
            End If
        End If
    End Sub
End Class
