﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports EntitySpaces.Core

Partial Class DesktopModules_UserRoleCollectionAccountCollectionNatureTagging_UserRoleCollectionAccountCollectionNatureTagging
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                tblPaymentNaturesTreeView.Style.Add("display", "none")
                tblUserRolesPaymentNature.Style.Add("display", "none")
                btnDeleteAccountPaymentNatures.Visible = False
                btnSave.Visible = False
                LoadddlUser()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "User Role Collection Account Collection Nature Tagging")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlUser()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBankRole.Items.Clear()
            ddlBankRole.Items.Add(lit)
            ddlBankRole.AppendDataBoundItems = True
            lblUser.Text = "Select Bank Role"
            rfvUser.Text = "Please select Bank Role"
            ddlBankRole.DataSource = ICRoleController.GetAllICRolesByRoleType("Bank Role")
            ddlBankRole.DataTextField = "RoleName"
            ddlBankRole.DataValueField = "RoleID"
            ddlBankRole.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub SetgvUserRoles()
        Try
            tblPaymentNaturesTreeView.Style.Add("display", "none")
            tblUserRolesPaymentNature.Style.Add("display", "none")
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub CheckMarkAlreadyTaggedAccountPaymentNatureWithRole(ByVal UserID As String, ByVal RoleID As String)
        Dim collICUserRolesCollectionAccNature As New ICUserRolesCollectionAccountsAndCollectionNatureCollection
        'Dim objICUserRolesAPNAndPLocationColl As ICUserRolesAccountPaymentNatureAndLocationsCollection
        'Dim dtAllAPNLoc As New DataTable
        'Dim arrRowLocation As DataRow()
        collICUserRolesCollectionAccNature.es.Connection.CommandTimeout = 3600


        collICUserRolesCollectionAccNature = ICUserRoleCollectionAccountNatureController.GetAllUserRolesCollectionAccountNatureByRoleIDAndUserID(RoleID, UserID)
        ' dtAllAPNLoc = ICUserRolesAccountPaymentNatureAndLocationsController.GetAllUserRolesAPNLocationsWithAccPayNatByAPNUserIDAndRoleID(RoleID, UserID)
        For Each objICUserRoleCollAccNature As ICUserRolesCollectionAccountsAndCollectionNature In collICUserRolesCollectionAccNature
            For Each radTv As RadTreeNode In radtvAccountsPaymentNature.Nodes
                For Each radTVGroupNode As RadTreeNode In radTv.Nodes
                    For Each radCompnayNode As RadTreeNode In radTVGroupNode.Nodes
                        For Each radAPNatureNode As RadTreeNode In radCompnayNode.Nodes
                            If radAPNatureNode.Value = objICUserRoleCollAccNature.AccountNumber & "-" & objICUserRoleCollAccNature.BranchCode & "-" & objICUserRoleCollAccNature.Currency & "-" & objICUserRoleCollAccNature.CollectionNatureCode Then
                                radAPNatureNode.Checked = True
                                'arrRowLocation = Nothing
                                'arrRowLocation = dtAllAPNLoc.Select("AccountPaymentNature = '" & objICUserRoleAPNature.AccountNumber & "-" & objICUserRoleAPNature.BranchCode & "-" & objICUserRoleAPNature.Currency & "-" & objICUserRoleAPNature.PaymentNatureCode & "'")

                                'If arrRowLocation.Count > 0 Then
                                '    For Each drLoc As DataRow In arrRowLocation
                                '        For Each radtVLocationNode As RadTreeNode In radAPNatureNode.Nodes
                                '            If radtVLocationNode.Value = drLoc("OfficeID").ToString() Then
                                '                If radtVLocationNode.Checked = False Then
                                '                    radtVLocationNode.Checked = True
                                '                End If
                                '            End If
                                '        Next
                                '    Next
                                'End If
                            End If
                        Next
                    Next
                Next
            Next
        Next
    End Sub

    'Public Shared Sub GetAccountsPaymentNatureByRoleIDAndUserID(ByVal UserID As String, ByVal RoleID As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid)
    '    Dim StrQuery As String = Nothing
    '    Dim WhereClause As String = Nothing
    '    Dim GroupByClause As String = Nothing
    '    Dim OrderByClause As String = Nothing
    '    OrderByClause = " Order By g.GroupName,c.CompanyName,URAPN.AccountNumber,PN.PaymentNatureName desc"
    '    WhereClause = " Where "
    '    GroupByClause = " Group By "
    '    StrQuery = "select g.GroupName,c.CompanyName,URAPN.AccountNumber,URAPN.BranchCode,URAPN.Currency,URAPN.PaymentNatureCode,"
    '    StrQuery += "URAPN.RolesID,"
    '    If UserID <> "0" Then
    '        StrQuery += "URAPN.UserID,"
    '    End If
    '    StrQuery += "pn.PaymentNatureName,URAPN.isApproved from IC_UserRolesAccountAndPaymentNature as URAPN "
    '    StrQuery += "inner join IC_AccountsPaymentNature as APN on URAPN.AccountNumber=APN.AccountNumber and "
    '    StrQuery += "URAPN.BranchCode=APN.BranchCode and URAPN.Currency=APN.Currency "
    '    StrQuery += "inner join IC_Accounts as A on APN.AccountNumber=a.AccountNumber and APN.BranchCode=a.BranchCode and "
    '    StrQuery += "URAPN.Currency=a.Currency inner join IC_Company as C on a.CompanyCode=c.CompanyCode inner join IC_Group "
    '    StrQuery += "as G on c.GroupCode=g.GroupCode inner join IC_PaymentNature as PN on URAPN.PaymentNatureCode=pn.PaymentNatureCode "
    '    GroupByClause += "g.GroupName,c.CompanyName,URAPN.AccountNumber,URAPN.BranchCode,URAPN.Currency,URAPN.PaymentNatureCode,URAPN.RolesID,"
    '    If UserID <> "0" Then
    '        GroupByClause += "URAPN.UserID,"
    '    End If
    '    GroupByClause += "pn.PaymentNatureName,URAPN.isApproved "
    '    If UserID <> "0" Then
    '        WhereClause += " AND URAPN.UserID=" & UserID
    '    End If
    '    If RoleID.ToString() <> "0" Then
    '        WhereClause += " AND URAPN.RolesID=" & RoleID
    '    End If
    '    If WhereClause.Contains("Where  AND") Then
    '        WhereClause = WhereClause.Replace("Where  AND", " Where ")
    '    End If
    '    StrQuery += WhereClause & GroupByClause & OrderByClause
    '    Dim util As New esUtility
    '    Dim dt As New DataTable
    '    dt = util.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery)

    '    If Not PageNumber = 0 Then

    '        rg.DataSource = dt
    '        If DoDataBind = True Then
    '            rg.DataBind()
    '        End If
    '    Else

    '        rg.DataSource = dt
    '        If DoDataBind = True Then
    '            rg.DataBind()
    '        End If
    '    End If
    'End Sub

    Public Shared Sub GetCollectionAccountsCollectionNatureByRoleIDAndUserID(ByVal UserID As String, ByVal RoleID As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid)
        Dim params As New EntitySpaces.Interfaces.esParameters

        Dim StrQuery As String = Nothing
        Dim WhereClause As String = Nothing
        Dim GroupByClause As String = Nothing
        Dim OrderByClause As String = Nothing
        OrderByClause = " Order By g.GroupName,c.CompanyName,URAPN.AccountNumber,PN.CollectionNatureName desc"
        WhereClause = " Where "
        GroupByClause = " Group By "
        StrQuery = "select g.GroupName,c.CompanyName,URAPN.AccountNumber,URAPN.BranchCode,URAPN.Currency,URAPN.CollectionNatureCode,"
        StrQuery += "URAPN.RoleID,"
        If UserID <> "0" Then
            StrQuery += "URAPN.UserID,"
        End If
        StrQuery += "pn.CollectionNatureName from IC_UserRolesCollectionAccountsAndCollectionNature as URAPN "
        StrQuery += "inner join IC_CollectionAccountsCollectionNature as APN on URAPN.AccountNumber=APN.AccountNumber and "
        StrQuery += "URAPN.BranchCode=APN.BranchCode and URAPN.Currency=APN.Currency "
        StrQuery += "inner join IC_CollectionAccounts as A on APN.AccountNumber=a.AccountNumber and APN.BranchCode=a.BranchCode and "
        StrQuery += "URAPN.Currency=a.Currency inner join IC_Company as C on a.CompanyCode=c.CompanyCode inner join IC_Group "
        StrQuery += "as G on c.GroupCode=g.GroupCode inner join IC_CollectionNature as PN on URAPN.CollectionNatureCode=pn.CollectionNatureCode "
        GroupByClause += "g.GroupName,c.CompanyName,URAPN.AccountNumber,URAPN.BranchCode,URAPN.Currency,URAPN.CollectionNatureCode,URAPN.RoleID,"
        If UserID <> "0" Then
            GroupByClause += "URAPN.UserID,"
        End If
        GroupByClause += "pn.CollectionNatureName "
        If UserID <> "0" Then
            WhereClause += " AND URAPN.UserID=@UserID"
            params.Add("UserID", UserID)
        End If
        If RoleID.ToString() <> "0" Then
            WhereClause += " AND URAPN.RoleID=@RoleID"
            params.Add("RoleID", RoleID)
        End If
        If WhereClause.Contains("Where  AND") Then
            WhereClause = WhereClause.Replace("Where  AND", " Where ")
        End If
        StrQuery += WhereClause & GroupByClause & OrderByClause
        Dim util As New esUtility
        Dim dt As New DataTable
        dt = util.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)

        If Not PageNumber = 0 Then

            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        Else

            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        End If
    End Sub

    Private Sub LoadgvPaymentNature(ByVal UserID As String, ByVal RoleID As String, ByVal DoDataBind As Boolean)
        Try
            gvPaymentNature.DataSource = Nothing
            GetCollectionAccountsCollectionNatureByRoleIDAndUserID(UserID, RoleID, Me.gvPaymentNature.CurrentPageIndex + 1, Me.gvPaymentNature.PageSize, DoDataBind, Me.gvPaymentNature)
            If gvPaymentNature.Items.Count > 0 Then
                tblUserRolesPaymentNature.Style.Remove("display")
                gvPaymentNature.Visible = True
                lblPaymentNatureList.Visible = True
                lblRNFAPNList.Visible = False
                btnDeleteAccountPaymentNatures.Visible = CBool(htRights("Delete"))
                gvPaymentNature.Columns(5).Visible = CBool(htRights("Delete"))
            Else
                tblUserRolesPaymentNature.Style.Add("display", "none")
                lblPaymentNatureList.Visible = False
                lblRNFAPNList.Visible = True
                btnDeleteAccountPaymentNatures.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlUser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBankRole.SelectedIndexChanged
        Try
            If ddlBankRole.SelectedValue.ToString <> "0" Then
                Dim objICRole As New ICRole
                ViewState("RoleID") = Nothing
                ViewState("RoleID") = ddlBankRole.SelectedValue.ToString
                objICRole.LoadByPrimaryKey(ddlBankRole.SelectedValue.ToString)
                LoadgvPaymentNature("0", ddlBankRole.SelectedValue.ToString, True)
                lblPaymentNatureList.Text = ""
                lblPaymentNatureList.Text = objICRole.RoleName.ToString() & " 's Collection Account, Nature List"
                tblUserRolesPaymentNature.Style.Remove("display")
                BindTreeView("Bank User")
                If gvPaymentNature.Items.Count > 0 Then
                    gvPaymentNature.Visible = True
                    lblPaymentNatureList.Visible = True
                    lblRNFAPNList.Visible = False
                    CheckMarkAlreadyTaggedAccountPaymentNatureWithRole("0", ViewState("RoleID").ToString)
                    radtvAccountsPaymentNature.ExpandAllNodes()
                Else
                    radtvAccountsPaymentNature.UncheckAllNodes()
                    radtvAccountsPaymentNature.CollapseAllNodes()
                    lblPaymentNatureList.Visible = True
                    gvPaymentNature.Visible = False
                    lblRNFAPNList.Visible = True
                End If

                lblAssignAccountPaymentNature.Text = ""
                lblAssignAccountPaymentNature.Text = "Assign Collection Account, Nature to Role [ " & objICRole.RoleName.ToString() & " ]"
            ElseIf ddlBankRole.SelectedValue.ToString = "0" Then
                gvPaymentNature.DataSource = Nothing
                tblPaymentNaturesTreeView.Style.Add("display", "none")
                tblUserRolesPaymentNature.Style.Add("display", "none")
                btnDeleteAccountPaymentNatures.Visible = False
                btnSave.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub BindTreeView(ByVal UserID As String)
        Try
            radtvAccountsPaymentNature.Nodes.Clear()
            Dim objICUser As New ICUser
            Dim dtGroupColl As DataTable
            Dim objICCompanyColl As ICCompanyCollection
            'Dim objICGroup As ICGroup
            'Dim objICOfficeColl As ICOfficeCollection
            ' Dim dtBankBranch As New DataTable
            Dim radGroupNode, radCompanyNode, radAPNatureNode As RadTreeNode
            'Dim radOfficeNode, radBrNode As RadTreeNode
            Dim dtALLCollectionAccNature As New DataTable
            'Dim dtALLOffices As New DataTable
            Dim arrRowCollectionAccNature As DataRow()
            'Dim arrRowClientOffices As DataRow()
            objICUser.es.Connection.CommandTimeout = 3600
            If UserID = "Bank User" Then
                dtGroupColl = New DataTable
                'objICGroupColl = ICGroupController.GetAllActiveGroups

                dtGroupColl = ICGroupController.GetAllActiveGroupsIsCollectionAllowonCompany()

                ' dtBankBranch = ICOfficeController.GetPrincipalBankBranchForRoleAPNTagging
                For Each drGroupInLoop As DataRow In dtGroupColl.Rows
                    radGroupNode = New RadTreeNode
                    radGroupNode.Text = drGroupInLoop("GroupName").ToString
                    radGroupNode.Value = drGroupInLoop("GroupCode").ToString
                    objICCompanyColl = New ICCompanyCollection
                    objICCompanyColl = ICCompanyController.GetAllActiveCollectionCompaniesByGroupCode(radGroupNode.Value.ToString)

                    dtALLCollectionAccNature = ICCollectionAccountsCollectionNatureController.GetAllCollectionAccountNature()
                    'dtALLOffices = ICOfficeController.GetAllOfficeWithCompanyCodeActiveAndApprove()

                    For Each objICCompany As ICCompany In objICCompanyColl
                        radCompanyNode = New RadTreeNode
                        'objICOfficeColl = New ICOfficeCollection
                        'objICOfficeColl.es.Connection.CommandTimeout = 3600
                        radCompanyNode.Text = objICCompany.CompanyName.ToString
                        radCompanyNode.Value = objICCompany.CompanyCode

                        arrRowCollectionAccNature = Nothing
                        'arrRowClientOffices = Nothing

                        arrRowCollectionAccNature = dtALLCollectionAccNature.Select("CompanyCode = '" & objICCompany.CompanyCode & "'", "AccountAndPaymentNature")
                        'arrRowClientOffices = dtALLOffices.Select("CompanyCode = '" & objICCompany.CompanyCode & "'", "OfficeName")

                        If arrRowCollectionAccNature.Count > 0 Then
                            For Each dr As DataRow In arrRowCollectionAccNature
                                radAPNatureNode = New RadTreeNode
                                radAPNatureNode.Text = dr("AccountAndPaymentNature")
                                radAPNatureNode.Value = dr("AccountPaymentNature")

                                'If dtBankBranch.Rows.Count > 0 Then
                                '    For Each drBranch As DataRow In dtBankBranch.Rows
                                '        radBrNode = New RadTreeNode
                                '        radBrNode.Text = drBranch(1).ToString
                                '        radBrNode.Value = drBranch(0).ToString
                                '        radAPNatureNode.Nodes.Add(radBrNode)
                                '    Next
                                'End If
                                'If arrRowClientOffices.Count > 0 Then
                                '    For Each drCO As DataRow In arrRowClientOffices
                                '        radOfficeNode = New RadTreeNode
                                '        radOfficeNode.Text = drCO("OfficeName").ToString
                                '        radOfficeNode.Value = drCO("OfficeID").ToString
                                '        radAPNatureNode.Nodes.Add(radOfficeNode)
                                '    Next
                                'End If

                                radCompanyNode.Nodes.Add(radAPNatureNode)
                            Next
                        End If
                        radGroupNode.Nodes.Add(radCompanyNode)
                    Next
                    radMainTreeNode.Nodes.Add(radGroupNode)
                Next

                radtvAccountsPaymentNature.Nodes.Add(radMainTreeNode)
                radtvAccountsPaymentNature.CheckBoxes = True

                If radtvAccountsPaymentNature.Nodes.Count > 0 Then
                    tblPaymentNaturesTreeView.Style.Remove("display")
                    btnSave.Visible = CBool(htRights("Tag"))
                Else
                    tblPaymentNaturesTreeView.Style.Add("display", "none")
                    btnSave.Visible = False
                End If
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If CheckIsAtLeastOneAccountPaymentNatureSelected() = False Then
                UIUtilities.ShowDialog(Me, "Error", "Please select at least one collection account collection nature", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If

            Dim ArrayListUSers As New ArrayList
            Dim objICRole As New ICRole
            Dim objRoleController As New RoleController
            objICRole.LoadByPrimaryKey(ViewState("RoleID").ToString)
            ArrayListUSers = objRoleController.GetUsersInRole(Me.PortalId, objICRole.RoleName)
            If ArrayListUSers.Count = 0 Then
                UIUtilities.ShowDialog(Me, "Error", "No users are assigned to role.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            DeleteOldTaggingByRoleIDAndUserID(ViewState("RoleID").ToString, "0")
            SaveUserRoleAccountPaymentNatureTagged()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub SaveUserRoleAccountPaymentNatureTagged()
        Try
            Dim PassedAPNature As String = ""
            Dim objICUserRolesCollAccNature As ICUserRolesCollectionAccountsAndCollectionNature
            'Dim objICUserRolesPrintLocation As ICUserRolesAccountPaymentNatureAndLocations
            Dim collICUserRoleCollAccNature As New ICUserRolesCollectionAccountsAndCollectionNatureCollection
            'Dim objICUserRoleAPNLocationColl As New ICUserRolesAccountPaymentNatureAndLocationsCollection
            Dim objICAuditTrailColl As New ICAuditTrailCollection
            Dim objICAuditTrail As ICAuditTrail
            Dim StrAuditTrailAction As String = Nothing

            Dim PassToSave As Integer = 0
            Dim FailToSave As Integer = 0
            Dim ArrayListUSers As New ArrayList
            Dim objRoleController As New RoleController
            Dim objICRole As New ICRole
            Dim objRoleInfo As UserRoleInfo
            objICRole.LoadByPrimaryKey(ViewState("RoleID").ToString)
            ArrayListUSers = objRoleController.GetUsersInRole(Me.PortalId, objICRole.RoleName)
            If ArrayListUSers.Count > 0 Then
                For i = 0 To ArrayListUSers.Count - 1
                    objRoleInfo = New UserRoleInfo
                    objRoleInfo = ArrayListUSers(i)
                    For Each node As RadTreeNode In radtvAccountsPaymentNature.Nodes
                        If node.Checked = True And node.Nodes.Count > 0 And Not node.Value.ToString <> "top" Then
                            If node.Nodes.Count > 0 Then
                                For Each radGroupNode As RadTreeNode In node.Nodes
                                    If radGroupNode.Nodes.Count > 0 Then
                                        If radGroupNode.Checked = True Then
                                            For Each radCompanyNode As RadTreeNode In radGroupNode.Nodes
                                                If radCompanyNode.Nodes.Count > 0 Then
                                                    If radCompanyNode.Checked = True Then
                                                        For Each radAPNarureNode As RadTreeNode In radCompanyNode.Nodes
                                                            objICAuditTrail = New ICAuditTrail
                                                            objICUserRolesCollAccNature = New ICUserRolesCollectionAccountsAndCollectionNature
                                                            Dim iCount As Integer = 0
                                                            If radAPNarureNode.Checked = True Then
                                                                objICUserRolesCollAccNature.AccountNumber = radAPNarureNode.Value.ToString.Split("-")(0)
                                                                objICUserRolesCollAccNature.BranchCode = radAPNarureNode.Value.ToString.Split("-")(1)
                                                                objICUserRolesCollAccNature.Currency = radAPNarureNode.Value.ToString.Split("-")(2)
                                                                objICUserRolesCollAccNature.CollectionNatureCode = radAPNarureNode.Value.ToString.Split("-")(3)
                                                                objICUserRolesCollAccNature.RoleID = CInt(ViewState("RoleID"))
                                                                objICUserRolesCollAccNature.UserID = objRoleInfo.UserID
                                                                objICUserRolesCollAccNature.CreatedBy = Me.UserId.ToString
                                                                objICUserRolesCollAccNature.CreatedDate = Date.Now
                                                                objICUserRolesCollAccNature.IsApproved = True

                                                                collICUserRoleCollAccNature.Add(objICUserRolesCollAccNature)
                                                                StrAuditTrailAction = Nothing

                                                                Dim objRole As New ICRole
                                                                Dim objCollAcc As New ICCollectionAccounts
                                                                Dim objCompany As New ICCompany
                                                                Dim objGroup As New ICGroup
                                                                Dim objUser As New ICUser
                                                                objCollAcc.LoadByPrimaryKey(objICUserRolesCollAccNature.AccountNumber, objICUserRolesCollAccNature.BranchCode, objICUserRolesCollAccNature.Currency)
                                                                objCompany.LoadByPrimaryKey(objCollAcc.CompanyCode)
                                                                objGroup.LoadByPrimaryKey(objCompany.GroupCode)
                                                                objRole.LoadByPrimaryKey(objICUserRolesCollAccNature.RoleID)
                                                                objUser.LoadByPrimaryKey(objICUserRolesCollAccNature.UserID)
                                                                StrAuditTrailAction = "Collection account [ " & objICUserRolesCollAccNature.AccountNumber & " ] collection nature [ " & objICUserRolesCollAccNature.CollectionNatureCode & " ] "
                                                                StrAuditTrailAction += " of group [ " & objGroup.GroupCode & " ] for role [ " & objRole.RoleID & " ]"
                                                                StrAuditTrailAction += " for user [ " & objUser.UserName & " ] added"

                                                                PassToSave = PassToSave + 1
                                                                'For Each radLocationNode As RadTreeNode In radAPNarureNode.Nodes

                                                                '    If radLocationNode.Checked = True Then
                                                                '        If iCount = 0 Then
                                                                '            StrAuditTrailAction += " with location [ "
                                                                '            iCount = iCount + 1
                                                                '        End If
                                                                '        objICAuditTrail = New ICAuditTrail
                                                                '        objICUserRolesPrintLocation = New ICUserRolesAccountPaymentNatureAndLocations
                                                                '        objICUserRolesPrintLocation.AccountNumber = objICUserRolesCollAccNature.AccountNumber
                                                                '        objICUserRolesPrintLocation.BranchCode = objICUserRolesCollAccNature.BranchCode
                                                                '        objICUserRolesPrintLocation.Currency = objICUserRolesCollAccNature.Currency
                                                                '        objICUserRolesPrintLocation.PaymentNatureCode = objICUserRolesCollAccNature.PaymentNatureCode
                                                                '        objICUserRolesPrintLocation.OfficeID = radLocationNode.Value
                                                                '        objICUserRolesPrintLocation.UserID = objICUserRolesCollAccNature.UserID
                                                                '        objICUserRolesPrintLocation.RoleID = objICUserRolesCollAccNature.RolesID
                                                                '        objICUserRolesPrintLocation.CreatedBy = Me.UserId.ToString
                                                                '        objICUserRolesPrintLocation.CreatedDate = Date.Now
                                                                '        objICUserRolesPrintLocation.Creater = Me.UserId.ToString
                                                                '        objICUserRolesPrintLocation.CreationDate = Date.Now
                                                                '        StrAuditTrailAction += radLocationNode.Value.ToString & ";"
                                                                '        objICUserRoleAPNLocationColl.Add(objICUserRolesPrintLocation)
                                                                '    End If
                                                                'Next
                                                                'If StrAuditTrailAction.Contains(";") = True Then
                                                                '    StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                                                                '    StrAuditTrailAction += " ]."
                                                                'Else
                                                                '    StrAuditTrailAction += "."
                                                                'End If
                                                                objICAuditTrail.AuditAction = StrAuditTrailAction
                                                                objICAuditTrail.AuditType = "User Role Collection Account, Nature Tagging"
                                                                objICAuditTrail.RelatedID = objICUserRolesCollAccNature.AccountNumber.ToString + "" + objICUserRolesCollAccNature.BranchCode.ToString + "" + objICUserRolesCollAccNature.Currency.ToString + "" + objICUserRolesCollAccNature.CollectionNatureCode.ToString
                                                                objICAuditTrail.AuditDate = Date.Now
                                                                objICAuditTrail.UserID = Me.UserId
                                                                objICAuditTrail.Username = Me.UserInfo.Username.ToString
                                                                objICAuditTrail.ActionType = "ADD"
                                                                If Not HttpContext.Current Is Nothing Then
                                                                    objICAuditTrail.IPAddress = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                                                                End If
                                                                objICAuditTrailColl.Add(objICAuditTrail)
                                                            End If
                                                        Next
                                                    End If
                                                End If
                                            Next
                                        End If
                                    End If
                                Next
                            End If
                        End If
                    Next
                Next
            End If

            If collICUserRoleCollAccNature.Count > 0 Then
                collICUserRoleCollAccNature.Save()
            End If
            'If objICUserRoleAPNLocationColl.Count > 0 Then
            '    objICUserRoleAPNLocationColl.Save()
            'End If
            If objICAuditTrailColl.Count > 0 Then
                objICAuditTrailColl.Save()
            End If
            If Not FailToSave = 0 And Not PassToSave = 0 Then
                UIUtilities.ShowDialog(Me, "User Role Collection Account, Nature Tagging", PassToSave & " Collection Account, Nature tagged successfully. <br />" & FailToSave & " Collection Account, Nature not tagged successfully due to following reasons:<br /> 1. Collection Account, Nature and location is not selected", ICBO.IC.Dialogmessagetype.Success)
            ElseIf Not FailToSave = 0 And PassToSave = 0 Then
                UIUtilities.ShowDialog(Me, "User Role Collection Account, Nature Tagging", "Collection Account, Nature not tagged successfully due to following reasons:<br /> 1. Collection Account, Nature and location is not selected", ICBO.IC.Dialogmessagetype.Failure)
            ElseIf FailToSave = 0 And Not PassToSave = 0 Then
                UIUtilities.ShowDialog(Me, "User Role Collection Account, Nature Tagging", PassToSave & " Collection Account, Nature tagged successfully.", ICBO.IC.Dialogmessagetype.Success)
            End If

            LoadgvPaymentNature("0", ViewState("RoleID").ToString, True)
            BindTreeView("Bank User")
            If gvPaymentNature.Items.Count > 0 Then
                CheckMarkAlreadyTaggedAccountPaymentNatureWithRole("0", ViewState("RoleID").ToString)
                radtvAccountsPaymentNature.ExpandAllNodes()
            Else
                radtvAccountsPaymentNature.CheckChildNodes = False
                radtvAccountsPaymentNature.CollapseAllNodes()
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnDeleteAccountPaymentNatures_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteAccountPaymentNatures.Click
        Try
            Dim AccountNumber, BranchCode, Currency, CollectionNatureCode, UserID, RoleID As String
            Dim PassToDeleteCount As Integer = 0
            Dim FailToDeleteCount As Integer = 0
            Dim collICUserCollRoleAccNature As ICUserRolesCollectionAccountsAndCollectionNatureCollection
            'Dim objICUserRolesAPNatureLocationsColl As ICUserRolesAccountPaymentNatureAndLocationsCollection
            Dim collICUserRoleCollAccNatureForDelete As New ICUserRolesCollectionAccountsAndCollectionNatureCollection
            'Dim objICUserRolesAPNatureLocationsCollForDelete As New ICUserRolesAccountPaymentNatureAndLocationsCollection
            Dim TaggingType As String = Nothing
            Dim objICAuditTrailColl As New ICAuditTrailCollection
            Dim objICAuditTrail As ICAuditTrail
            Dim StrAuditTrailAction As String = Nothing
            Dim chkSelect As CheckBox
            If CheckgvPNatureForProcessAll() = False Then
                UIUtilities.ShowDialog(Me, "User Role Collection Account, Nature Tagging", "Please select atleast one record.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            TaggingType = "Bank"

            For Each gvUserRolesAPNatureRow As GridDataItem In gvPaymentNature.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvUserRolesAPNatureRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    AccountNumber = Nothing
                    BranchCode = Nothing
                    Currency = Nothing
                    CollectionNatureCode = Nothing
                    UserID = Nothing
                    RoleID = Nothing
                    AccountNumber = gvUserRolesAPNatureRow.GetDataKeyValue("AccountNumber")
                    BranchCode = gvUserRolesAPNatureRow.GetDataKeyValue("BranchCode")
                    Currency = gvUserRolesAPNatureRow.GetDataKeyValue("Currency")
                    CollectionNatureCode = gvUserRolesAPNatureRow.GetDataKeyValue("CollectionNatureCode")
                    RoleID = gvUserRolesAPNatureRow.GetDataKeyValue("RoleID")
                    UserID = "0"

                    collICUserCollRoleAccNature = New ICUserRolesCollectionAccountsAndCollectionNatureCollection
                    collICUserCollRoleAccNature.es.Connection.CommandTimeout = 3600
                    collICUserCollRoleAccNature = ICUserRoleCollectionAccountNatureController.GetAllUserRolesbyCollectionAccountCollectionNature(CollectionNatureCode, AccountNumber, BranchCode, Currency, RoleID, UserID)
                    If collICUserCollRoleAccNature.Count > 0 Then
                        For Each objICUserRoleCollAccNature As ICUserRolesCollectionAccountsAndCollectionNature In collICUserCollRoleAccNature
                            objICAuditTrail = New ICAuditTrail
                            StrAuditTrailAction = Nothing
                            Dim objRole As New ICRole
                            Dim objCollAcc As New ICCollectionAccounts
                            Dim objCompany As New ICCompany
                            Dim objGroup As New ICGroup
                            Dim objUser As New ICUser
                            objCollAcc.LoadByPrimaryKey(objICUserRoleCollAccNature.AccountNumber, objICUserRoleCollAccNature.BranchCode, objICUserRoleCollAccNature.Currency)
                            objCompany.LoadByPrimaryKey(objCollAcc.CompanyCode)
                            objGroup.LoadByPrimaryKey(objCompany.GroupCode)
                            objRole.LoadByPrimaryKey(objICUserRoleCollAccNature.RoleID)
                            objUser.LoadByPrimaryKey(objICUserRoleCollAccNature.UserID)
                            StrAuditTrailAction = "Collection Account [ " & objICUserRoleCollAccNature.AccountNumber & " ] Collection Nature [ " & objICUserRoleCollAccNature.CollectionNatureCode & "] "
                            StrAuditTrailAction += " of group [ " & objGroup.GroupCode & " ] for role [ " & objRole.RoleID & " ]"
                            StrAuditTrailAction += " for user [ " & objUser.UserName & " ] deleted "

                            'Dim ICount As Integer = 0
                            Try
                                'objICUserRolesAPNatureLocationsColl = New ICUserRolesAccountPaymentNatureAndLocationsCollection
                                'objICUserRolesAPNatureLocationsColl.es.Connection.CommandTimeout = 3600
                                'objICUserRolesAPNatureLocationsColl = ICUserRolesAccountPaymentNatureAndLocationsController.GetAllUserRolesAccountPaymentNatureLocations(objICUserRoleAPNature.PaymentNatureCode, objICUserRoleAPNature.AccountNumber, objICUserRoleAPNature.BranchCode, objICUserRoleAPNature.Currency, objICUserRoleAPNature.RolesID, objICUserRoleAPNature.UserID)
                                'If objICUserRolesAPNatureLocationsColl.Count > 0 Then
                                '    For Each objICUserRoleAPNatureLocations As ICUserRolesAccountPaymentNatureAndLocations In objICUserRolesAPNatureLocationsColl
                                '        If ICount = 0 Then
                                '            StrAuditTrailAction += "with location [ "
                                '            ICount = ICount + 1
                                '        End If
                                '        StrAuditTrailAction += objICUserRoleAPNatureLocations.OfficeID.ToString & ";"
                                '        objICUserRolesAPNatureLocationsCollForDelete.Add(objICUserRoleAPNatureLocations)
                                '    Next
                                'End If
                                'If StrAuditTrailAction.Contains(";") = True Then
                                '    StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                                '    StrAuditTrailAction += " ]."
                                'Else
                                '    StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                                '    StrAuditTrailAction += " ."
                                'End If
                                objICAuditTrail.AuditAction = StrAuditTrailAction
                                objICAuditTrail.AuditType = "User Role Collection Account, Nature Tagging"
                                objICAuditTrail.RelatedID = objICUserRoleCollAccNature.AccountNumber.ToString + "" + objICUserRoleCollAccNature.BranchCode.ToString + "" + objICUserRoleCollAccNature.Currency.ToString + "" + objICUserRoleCollAccNature.CollectionNatureCode.ToString
                                objICAuditTrail.AuditDate = Date.Now
                                objICAuditTrail.UserID = Me.UserId
                                objICAuditTrail.Username = Me.UserInfo.Username.ToString
                                objICAuditTrail.ActionType = "DELETE"
                                If Not HttpContext.Current Is Nothing Then
                                    objICAuditTrail.IPAddress = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                                End If
                                collICUserRoleCollAccNatureForDelete.Add(objICUserRoleCollAccNature)
                                objICAuditTrailColl.Add(objICAuditTrail)
                                PassToDeleteCount = PassToDeleteCount + 1
                            Catch ex As Exception
                                FailToDeleteCount = FailToDeleteCount + 1
                                Continue For
                            End Try
                        Next
                    End If
                End If
            Next
            If collICUserRoleCollAccNatureForDelete.Count > 0 Then
                collICUserRoleCollAccNatureForDelete.MarkAllAsDeleted()
                collICUserRoleCollAccNatureForDelete.Save()
            End If
            If objICAuditTrailColl.Count > 0 Then
                objICAuditTrailColl.Save()
            End If
            If Not PassToDeleteCount = 0 And Not FailToDeleteCount = 0 Then
                LoadgvPaymentNature("0", ViewState("RoleID").ToString, True)
                If gvPaymentNature.Items.Count > 0 Then
                    CheckMarkAlreadyTaggedAccountPaymentNatureWithRole("0", ViewState("RoleID").ToString)
                Else
                    radtvAccountsPaymentNature.UncheckAllNodes()
                    radtvAccountsPaymentNature.CollapseAllNodes()
                End If

                UIUtilities.ShowDialog(Me, "User Role Collection Account, Nature Tagging", "[ " & PassToDeleteCount.ToString & " ] User Role Collection Account, Nature(s) deleted successfuly.<br /> [ " & FailToDeleteCount.ToString & " ] User Role Collection Account, Nature(s) can not be deleted due to following reason: <br /> 1. Delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            ElseIf Not PassToDeleteCount = 0 And FailToDeleteCount = 0 Then
                LoadgvPaymentNature("0", ViewState("RoleID").ToString, True)
                If gvPaymentNature.Items.Count > 0 Then
                    CheckMarkAlreadyTaggedAccountPaymentNatureWithRole("0", ViewState("RoleID").ToString)
                Else
                    radtvAccountsPaymentNature.UncheckAllNodes()
                    radtvAccountsPaymentNature.CollapseAllNodes()
                End If

                UIUtilities.ShowDialog(Me, "User Role Collection Account, Nature Tagging", PassToDeleteCount.ToString & " User Role Collection Account, Nature(s) deleted successfuly.", ICBO.IC.Dialogmessagetype.Success)

                Exit Sub
            ElseIf Not FailToDeleteCount = 0 And PassToDeleteCount = 0 Then
                LoadgvPaymentNature("0", ViewState("RoleID").ToString, True)
                If gvPaymentNature.Items.Count > 0 Then
                    CheckMarkAlreadyTaggedAccountPaymentNatureWithRole("0", ViewState("RoleID").ToString)
                Else
                    radtvAccountsPaymentNature.UncheckAllNodes()
                    radtvAccountsPaymentNature.CollapseAllNodes()
                End If

                UIUtilities.ShowDialog(Me, "User Role Collection Account, Nature Tagging", "[ " & FailToDeleteCount.ToString & " ] User Role Collection Account, Nature(s) can not be deleted due to following reasons: <br /> 1. Delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvPaymentNature_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvPaymentNature.ItemCommand
        Try
            If e.CommandName = "del" Then
                Dim objICUserRolesCollAccNature As New ICUserRolesCollectionAccountsAndCollectionNature
                Dim collICUserRolesCollAccNature As New ICUserRolesCollectionAccountsAndCollectionNatureCollection
                Dim objICUserRolesCollAccNatureForDelete As New ICUserRolesCollectionAccountsAndCollectionNatureCollection
                'Dim objICUserRolesAPNatureLocationsCollForDelete As New ICUserRolesAccountPaymentNatureAndLocationsCollection
                'Dim objICUserRolesAPNatureLocationsColl As New ICUserRolesAccountPaymentNatureAndLocationsCollection
                Dim TaggingType As String = Nothing
                Dim objICAuditTrailColl As New ICAuditTrailCollection
                Dim objICAuditTrail As ICAuditTrail
                Dim StrAuditTrailAction As String = Nothing
                objICUserRolesCollAccNature.es.Connection.CommandTimeout = 3600
                'objICUserRolesAPNatureLocationsColl.es.Connection.CommandTimeout = 3600
                TaggingType = "Bank"

                Dim UserID As String = Nothing
                collICUserRolesCollAccNature = ICUserRoleCollectionAccountNatureController.GetAllUserRolesbyCollectionAccountCollectionNature(e.CommandArgument.ToString.Split(";")(4), e.CommandArgument.ToString.Split(";")(0), e.CommandArgument.ToString.Split(";")(1), e.CommandArgument.ToString.Split(";")(2), e.CommandArgument.ToString.Split(";")(3), "0")
                For Each objICUserRoleCollAccNature As ICUserRolesCollectionAccountsAndCollectionNature In collICUserRolesCollAccNature
                    objICAuditTrail = New ICAuditTrail
                    StrAuditTrailAction = Nothing
                    Dim objRole As New ICRole
                    Dim objCollAcc As New ICCollectionAccounts
                    Dim objCompany As New ICCompany
                    Dim objGroup As New ICGroup
                    Dim objUser As New ICUser
                    objCollAcc.LoadByPrimaryKey(objICUserRoleCollAccNature.AccountNumber, objICUserRoleCollAccNature.BranchCode, objICUserRoleCollAccNature.Currency)
                    objCompany.LoadByPrimaryKey(objCollAcc.CompanyCode)
                    objGroup.LoadByPrimaryKey(objCompany.GroupCode)
                    objRole.LoadByPrimaryKey(objICUserRoleCollAccNature.RoleID)
                    objUser.LoadByPrimaryKey(objICUserRoleCollAccNature.UserID)
                    StrAuditTrailAction = "Collection Account [ " & objICUserRoleCollAccNature.AccountNumber & " ] Collection Nature [ " & objICUserRoleCollAccNature.CollectionNatureCode & "] "
                    StrAuditTrailAction += " of group [ " & objGroup.GroupCode & " ] for role [ " & objRole.RoleID & " ]"
                    StrAuditTrailAction += " for user [ " & objUser.UserName & " ] deleted "

                    Dim ICount As Integer = 0

                    'objICUserRolesAPNatureLocationsColl = New ICUserRolesAccountPaymentNatureAndLocationsCollection
                    'objICUserRolesAPNatureLocationsColl = ICUserRolesAccountPaymentNatureAndLocationsController.GetAllUserRolesAccountPaymentNatureLocations(e.CommandArgument.ToString.Split(";")(4), e.CommandArgument.ToString.Split(";")(0), e.CommandArgument.ToString.Split(";")(1), e.CommandArgument.ToString.Split(";")(2), e.CommandArgument.ToString.Split(";")(3), objICUserRoleCollAccNature.UserID.ToString)
                    'If objICUserRolesAPNatureLocationsColl.Count > 0 Then
                    '    For Each objICUserRoleAPNatureLocation As ICUserRolesAccountPaymentNatureAndLocations In objICUserRolesAPNatureLocationsColl
                    '        If ICount = 0 Then
                    '            StrAuditTrailAction += "with location [ "
                    '            ICount = ICount + 1
                    '        End If
                    '        StrAuditTrailAction += objICUserRoleAPNatureLocation.OfficeID.ToString & ";"
                    '        objICUserRolesAPNatureLocationsCollForDelete.Add(objICUserRoleAPNatureLocation)
                    '    Next
                    'End If

                    'If StrAuditTrailAction.Contains(";") = True Then
                    '    StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                    '    StrAuditTrailAction += " ]."
                    'Else
                    '    StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                    '    StrAuditTrailAction += " ."
                    'End If
                    objICAuditTrail.AuditAction = StrAuditTrailAction
                    objICAuditTrail.AuditType = "User Role Collection Account, Nature Tagging"
                    objICAuditTrail.RelatedID = objICUserRoleCollAccNature.AccountNumber.ToString + "" + objICUserRoleCollAccNature.BranchCode.ToString + "" + objICUserRoleCollAccNature.Currency.ToString + "" + objICUserRoleCollAccNature.CollectionNatureCode.ToString
                    objICAuditTrail.AuditDate = Date.Now
                    objICAuditTrail.UserID = Me.UserId
                    objICAuditTrail.Username = Me.UserInfo.Username.ToString
                    objICAuditTrail.ActionType = "DELETE"
                    If Not HttpContext.Current Is Nothing Then
                        objICAuditTrail.IPAddress = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                    End If
                    objICUserRolesCollAccNatureForDelete.Add(objICUserRoleCollAccNature)
                    objICAuditTrailColl.Add(objICAuditTrail)
                Next
                'If objICUserRolesAPNatureLocationsCollForDelete.Count > 0 Then
                '    objICUserRolesAPNatureLocationsCollForDelete.MarkAllAsDeleted()
                '    objICUserRolesAPNatureLocationsCollForDelete.Save()
                'End If
                If objICUserRolesCollAccNatureForDelete.Count > 0 Then
                    objICUserRolesCollAccNatureForDelete.MarkAllAsDeleted()
                    objICUserRolesCollAccNatureForDelete.Save()
                End If
                If objICAuditTrailColl.Count > 0 Then
                    objICAuditTrailColl.Save()
                End If
                UIUtilities.ShowDialog(Me, "User Role Collection Account, Nature Tagging", "User Role Collection Account, Nature deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                LoadgvPaymentNature("0", ViewState("RoleID").ToString, True)
                BindTreeView("Bank User")
                If gvPaymentNature.Items.Count > 0 Then
                    CheckMarkAlreadyTaggedAccountPaymentNatureWithRole("0", ViewState("RoleID").ToString)
                Else
                    radtvAccountsPaymentNature.UncheckAllNodes()
                    radtvAccountsPaymentNature.CollapseAllNodes()
                End If

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvPaymentNature_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvPaymentNature.ItemDataBound
        Dim chkProcessAll As CheckBox
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAllAccountPaymentNatures"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvPaymentNature.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Private Function CheckgvPNatureForProcessAll() As Boolean
        Try
            Dim rowGVPNature As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVPNature In gvPaymentNature.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVPNature.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Private Sub DeleteOldTaggingByRoleIDAndUserID(ByVal RoleID As String, ByVal UserId As String)
        Try
            Dim collICUserRolesCollAccNature As New ICUserRolesCollectionAccountsAndCollectionNatureCollection
            Dim collICUserRolesCollAccNatureForDelete As New ICUserRolesCollectionAccountsAndCollectionNatureCollection
            'Dim objICUserRolesAPNatureLocationsCollForDelete As New ICUserRolesAccountPaymentNatureAndLocationsCollection
            'Dim objICUserRolesAPNatureLocationsColl As New ICUserRolesAccountPaymentNatureAndLocationsCollection
            Dim objICAuditTrailColl As New ICAuditTrailCollection
            Dim objICAuditTrail As ICAuditTrail
            Dim TaggingType As String = Nothing
            Dim StrAuditTrailAction As String = Nothing
            TaggingType = "Bank"

            collICUserRolesCollAccNature.es.Connection.CommandTimeout = 3600
            'objICUserRolesAPNatureLocationsColl.es.Connection.CommandTimeout = 36
            collICUserRolesCollAccNature = ICUserRoleCollectionAccountNatureController.GetAllUserRolesCollectionAccountNatureByRoleIDAndUserID(RoleID.ToString, UserId.ToString)
            If collICUserRolesCollAccNature.Count > 0 Then
                For Each objICUserRolesCollAccNature As ICUserRolesCollectionAccountsAndCollectionNature In collICUserRolesCollAccNature
                    objICAuditTrail = New ICAuditTrail
                    StrAuditTrailAction = Nothing
                    Dim objRole As New ICRole
                    Dim objCollAcc As New ICCollectionAccounts
                    Dim objCompany As New ICCompany
                    Dim objGroup As New ICGroup
                    Dim objUser As New ICUser
                    objCollAcc.LoadByPrimaryKey(objICUserRolesCollAccNature.AccountNumber, objICUserRolesCollAccNature.BranchCode, objICUserRolesCollAccNature.Currency)
                    objCompany.LoadByPrimaryKey(objCollAcc.CompanyCode)
                    objGroup.LoadByPrimaryKey(objCompany.GroupCode)
                    objRole.LoadByPrimaryKey(objICUserRolesCollAccNature.RoleID)
                    objUser.LoadByPrimaryKey(objICUserRolesCollAccNature.UserID)
                    StrAuditTrailAction = "Collection Account [ " & objICUserRolesCollAccNature.AccountNumber & " ] Collection Nature [ " & objICUserRolesCollAccNature.CollectionNatureCode & "] "
                    StrAuditTrailAction += " of group [ " & objGroup.GroupCode & " ] for role [ " & objRole.RoleID & " ]"
                    StrAuditTrailAction += " for user [ " & objUser.UserName & " ] deleted on update "

                    'Dim ICount As Integer = 0
                    'objICUserRolesAPNatureLocationsColl = New ICUserRolesAccountPaymentNatureAndLocationsCollection
                    'objICUserRolesAPNatureLocationsColl = ICUserRolesAccountPaymentNatureAndLocationsController.GetAllUserRolesAccountPaymentNatureLocations(objICUserRolesCollAccNature.PaymentNatureCode, objICUserRolesCollAccNature.AccountNumber, objICUserRolesCollAccNature.BranchCode, objICUserRolesCollAccNature.Currency, objICUserRolesCollAccNature.RolesID, objICUserRolesCollAccNature.UserID)
                    'If objICUserRolesAPNatureLocationsColl.Count > 0 Then
                    '    For Each objICUserRoleAPNatureLocation As ICUserRolesAccountPaymentNatureAndLocations In objICUserRolesAPNatureLocationsColl
                    '        If ICount = 0 Then
                    '            StrAuditTrailAction += "with location [ "
                    '            ICount = ICount + 1
                    '        End If
                    '        StrAuditTrailAction += objICUserRoleAPNatureLocation.OfficeID.ToString & ";"
                    '        objICUserRolesAPNatureLocationsCollForDelete.Add(objICUserRoleAPNatureLocation)
                    '    Next
                    'End If
                    'If StrAuditTrailAction.Contains(";") = True Then
                    '    StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                    '    StrAuditTrailAction += " ]."
                    'Else
                    '    StrAuditTrailAction += "."
                    'End If
                    objICAuditTrail.AuditAction = StrAuditTrailAction
                    objICAuditTrail.AuditType = "User Role Collection Account, Nature Tagging"
                    objICAuditTrail.RelatedID = objICUserRolesCollAccNature.AccountNumber.ToString + "" + objICUserRolesCollAccNature.BranchCode.ToString + "" + objICUserRolesCollAccNature.Currency.ToString + "" + objICUserRolesCollAccNature.CollectionNatureCode.ToString
                    objICAuditTrail.AuditDate = Date.Now
                    objICAuditTrail.UserID = Me.UserId
                    objICAuditTrail.Username = Me.UserInfo.Username.ToString
                    objICAuditTrail.ActionType = "DELETE"
                    If Not HttpContext.Current Is Nothing Then
                        objICAuditTrail.IPAddress = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                    End If
                    collICUserRolesCollAccNatureForDelete.Add(objICUserRolesCollAccNature)
                    objICAuditTrailColl.Add(objICAuditTrail)
                Next
            End If
            'If objICUserRolesAPNatureLocationsCollForDelete.Count > 0 Then
            '    objICUserRolesAPNatureLocationsCollForDelete.MarkAllAsDeleted()
            '    objICUserRolesAPNatureLocationsCollForDelete.Save()
            'End If
            If collICUserRolesCollAccNatureForDelete.Count > 0 Then
                collICUserRolesCollAccNatureForDelete.MarkAllAsDeleted()
                collICUserRolesCollAccNatureForDelete.Save()
            End If
            If objICAuditTrailColl.Count > 0 Then
                objICAuditTrailColl.Save()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Function CheckIsAtLeastOneChildNodeIsChecked(ByVal ParentNode As RadTreeNode) As Boolean
        Dim Result As Boolean = False
        If ParentNode.Nodes.Count > 0 Then
            For Each childNode As RadTreeNode In ParentNode.Nodes
                If childNode.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
        End If
        Return Result
    End Function

    Private Function CheckIsAtLeastOneAccountPaymentNatureSelected() As Boolean
        Dim Result As Boolean = False
        For Each radMainTreeNode As RadTreeNode In radtvAccountsPaymentNature.Nodes
            For Each radGroupNode As RadTreeNode In radMainTreeNode.Nodes
                For Each radCompanyNode As RadTreeNode In radGroupNode.Nodes
                    For Each radAPNatureNode As RadTreeNode In radCompanyNode.Nodes
                        If radAPNatureNode.Checked = True Then
                            Result = True
                            Exit For
                        End If
                    Next
                Next
            Next
        Next
        Return Result
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(41))
    End Sub

    Protected Sub gvPaymentNature_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvPaymentNature.NeedDataSource
        Try
            If Not ViewState("RoleID") Is Nothing Then
                LoadgvPaymentNature("0", ViewState("RoleID").ToString, False)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class

