﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports EntitySpaces.Core
Partial Class DesktopModules_ApprovePrintLocationAmendment_ApprovePrintLocationAmendment
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrICAssignUserRolsID As New ArrayList
    Private ArrICAssignedOfficeID As New ArrayList
    Private ArrICAssignedStatus As New ArrayList
    Private ArrICAssignedPaymentModes As New ArrayList
    Private ArrICAssignedStatusForApproval As New ArrayList
    Private ArrICAssignedCompanies As New ArrayList
    Private ArrICAssignedANos As New ArrayList
    Private ArrICAssignedBranchCodes As New ArrayList
    Private ArrICAssignedCurrency As New ArrayList
    Private ArrICAssignedPayNatures As New ArrayList
    Private htRights As Hashtable
    Protected Sub DesktopModules_PrintLocationAmendmentQueue_PrintLocationAmendmentQueue_Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()

            If Page.IsPostBack = False Then
                btnApproveAmendedPrintLocation.Attributes.Item("onclick") = "this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnApproveAmendedPrintLocation, Nothing).ToString()
                'btnCancelAmendedPrintLocation.Attributes.Item("onclick") = "this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnCancelAmendedPrintLocation, Nothing).ToString()

                Dim dtPageLoad As New DataTable
                DesignDts()
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                FillDesignedDtByAPNatureByAssignedRole()
                gvPenidngApprovalChangePrintLocation.DataSource = Nothing
                dtPageLoad = DirectCast(ViewState("AccountNumber"), DataTable)
                If dtPageLoad.Rows.Count > 0 Then
                    LoadgvAccountPaymentNatureProductType(True)
                Else
                    UIUtilities.ShowDialog(Me, "Error", "You do not have access to any transactional data. Please contact Al Baraka Administrator.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                    btnApproveAmendedPrintLocation.Visible = False
                    btnCancelAmendedPrintLocation.Visible = False

                    gvPenidngApprovalChangePrintLocation.Visible = False
                    lblNRF.Visible = True
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub DesignDts()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim dtOfficeID As New DataTable
        dtAccountNumber.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("BranchCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("Currency", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("PaymentNatureCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("OfficeID", GetType(System.String)))
        ViewState("AccountNo") = dtAccountNumber
    End Sub
    Private Sub FillDesignedDtByAPNatureByAssignedRole()
        Dim dtAccountNumber As DataTable
        Dim dtAssignedAPNatureOfficeID As New DataTable

        dtAccountNumber = New DataTable
        dtAccountNumber = ViewState("AccountNo")
        dtAccountNumber.Rows.Clear()
        dtAssignedAPNatureOfficeID.Rows.Clear()
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserSecond(Me.UserId.ToString, ArrICAssignUserRolsID)
        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID
       
    End Sub
    Private Sub SetGVPendingApprovalAmendmentPrintLocation()

        'If CBool(htRights("Approve Print Location Amendment")) = True Or CBool(htRights("Cancel Print Location Amendment")) = True Then
        '    LoadgvAccountPaymentNatureProductType(True)
        'Else
        '    pnlApprovePrintLocations.Visible = False
        '    gvPenidngApprovalChangePrintLocation.Visible = False
        '    gvPenidngApprovalChangePrintLocation.DataSource = Nothing
        '    lblApproveAmendLocations.Visible = False
        '    lblNRFPendingApproval.Visible = False
        '    btnCancelAmendedPrintLocation.Visible = False
        '    btnApproveAmendedPrintLocation.Visible = False
        '    btnCancel.Visible = False
        'End If
    End Sub
    Private Sub LoadgvAccountPaymentNatureProductType(ByVal DoDataBind As Boolean)
        Try
           
            Dim dt As New DataTable
            dt = ViewState("AccountNumber")
            SetArraListAssignedStatusForApproval()
            SetArraListAssignedOfficeIDs()
            SetArraListAssignedPaymentModes()
            ICInstructionController.GetInstructionsForApprovalAfterAmendmentForRadGrid(dt, Me.gvPenidngApprovalChangePrintLocation.CurrentPageIndex + 1, Me.gvPenidngApprovalChangePrintLocation.PageSize, Me.gvPenidngApprovalChangePrintLocation, DoDataBind, Me.UserId.ToString, ArrICAssignedPaymentModes)
            If gvPenidngApprovalChangePrintLocation.Items.Count > 0 Then
                lblNRF.Visible = False
                gvPenidngApprovalChangePrintLocation.Visible = True
                btnApproveAmendedPrintLocation.Visible = CBool(htRights("Approve Print Location Amendment"))
                btnCancelAmendedPrintLocation.Visible = CBool(htRights("Cancel Print Location Amendment"))
            Else

                gvPenidngApprovalChangePrintLocation.Visible = False
                lblNRF.Visible = True
                btnCancelAmendedPrintLocation.Visible = False
                btnApproveAmendedPrintLocation.Visible = False
                btnCancel.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Public Shared Sub GetInstructionsForApprovalAfterAmendmentForRadGrid(ByVal dtAccountNumber As DataTable, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal UsersID As String, ByVal AssignedPaymentModes As ArrayList)
        Dim objICUser As New ICUser
        objICUser.LoadByPrimaryKey(UsersID.ToString)
        Dim ArrAND As New ArrayList
        Dim ArrOR As New ArrayList
        Dim dt As DataTable
        Dim StrQuery As String = Nothing
        Dim WhereClasue As String = Nothing
        WhereClasue = " Where "
        StrQuery = "Select InstructionID,convert(date,IC_Instruction.CreateDate) as CreateDate,convert(date,ValueDate) as ValueDate,Amount,"
        StrQuery += "isnull(BeneficiaryName,'-') as BeneficiaryName,isnull(BeneficiaryAddress,'-') as BeneficiaryAddress,PaymentMode,ISNULL(InstrumentNo,'-') as InstrumentNo,"
        StrQuery += "ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,ISNULL(IC_Office.OfficeCode + '-' + IC_Office.OfficeName,'-') as OfficeName,"
        StrQuery += "FileBatchNo,ISNULL(ReferenceNo,'-') as ReferenceNo,IC_Bank.BankName,isnull(PreviousPrintLocationCode,'-') as PreviousPrintLocationCode,"
        StrQuery += "ISNULL(PreviousPrintLocationName,'-') as PreviousPrintLocationName,CONVERT(date,VerificationDate) as VerificationDate,"
        StrQuery += "CONVERT(date,ApprovalDate) as ApprovalDate,CONVERT(date,StaleDate) as staledate,CONVERT(date,CancellationDate) as CancellationDate,"
        StrQuery += "isnull(BeneficiaryAccountNo,'-') as BeneficiaryAccountNo,isnull(BeneAccountBranchCode,'-') as BeneAccountBranchCode,VerificationDate,"
        StrQuery += "isnull(BeneAccountCurrency,'-') as BeneAccountCurrency,LastPrintDate,"
        StrQuery += "CONVERT(date,RevalidationDate) as revalidationdate,IC_Instruction.ProductTypeCode,IC_Instruction.CompanyCode,status,IC_Company.CompanyName,"
        StrQuery += "IC_ProductType.ProductTypeName,ISNULL(co.OfficeCode + ' - ' + co.OfficeName,'-') as ChangedLocation,"
        StrQuery += "case isnull(convert(varchar,IsAmendmentComplete),CONVERT(varchar,'false')) "
        StrQuery += "when 'false' then "
        StrQuery += "CONVERT(varchar,'false') "
        StrQuery += "else "
        StrQuery += "IsAmendmentComplete "
        StrQuery += " end "
        StrQuery += "as IsAmendmentComplete "
        StrQuery += "from IC_Instruction "
        StrQuery += "left join IC_Office on IC_Instruction.PreviousPrintLocationCode=IC_Office.OfficeID "
        StrQuery += "left join IC_Bank on IC_Instruction.BeneficiaryBankCode=IC_Bank.BankCode "
        StrQuery += "left join IC_Company on IC_Instruction.CompanyCode=IC_Company.CompanyCode "
        StrQuery += "left join IC_ProductType on IC_Instruction.ProductTypeCode=IC_ProductType.ProductTypeCode "
        StrQuery += "left join IC_Office as CO on IC_Instruction.AmendedPrintLocationCode=co.OfficeID "
        WhereClasue += " And Status Not IN (21,30) And IsPrintLocationAmended=1 And PrintLocationCode IS NULL "
        If dtAccountNumber.Rows.Count > 0 Then
            WhereClasue += " And ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + PaymentNatureCode + " & "'-'" & "+ Convert(Varchar,CreatedOfficeCode) IN ("
            For Each dr As DataRow In dtAccountNumber.Rows
                WhereClasue += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "-" & dr("OfficeID").ToString & "',"
            Next
            WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
            WhereClasue += " )"
        End If

        If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
            WhereClasue += "AND PaymentMode IN ("
            For i = 0 To AssignedPaymentModes.Count - 1
                WhereClasue += "'" & AssignedPaymentModes(i) & "',"
            Next
            WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
            WhereClasue += ")"
        End If
        If WhereClasue.Contains(" Where  And ") = True Then
            WhereClasue = WhereClasue.Replace(" Where  And ", "Where ")
        End If
        StrQuery += WhereClasue
        StrQuery += " Order By InstructionID Desc"
        Dim utill As New esUtility()
        dt = New DataTable
        dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery)
        If Not CurrentPage = 0 Then
            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        Else
            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        End If

    End Sub
  
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Approve Print Location Amendment Queue")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Approve Print Location Amendment Queue") = True Then
                    ArrICAssignUserRolsID.Add(RoleID.ToString)
                End If
            Next
        End If
    End Sub
    Private Sub SetArraListAssignedOfficeIDs()
        Dim dt As New DataTable
        Dim objICUser As New ICUser
        objICUser.LoadByPrimaryKey(Me.UserId)
        ArrICAssignedOfficeID.Clear()
        dt = ICUserRolesPaymentNatureController.GetAllTaggedLocationsWithUserByUSerIDAndRoleID(Me.UserId.ToString, ArrICAssignUserRolsID)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("OfficeID").ToString
                ArrICAssignedOfficeID.Add(RoleID.ToString)
            Next
        End If
        If Not ArrICAssignedOfficeID.Contains(objICUser.OfficeCode.ToString) Then
            ArrICAssignedOfficeID.Add(objICUser.OfficeCode)
        End If
    End Sub

    Private Sub SetArraListAssignedStatusForApproval()
        Dim dt As New DataTable
        ArrICAssignedStatusForApproval.Clear()


    End Sub

    Private Sub SetArraListAssignedPaymentModes()
        ArrICAssignedPaymentModes.Clear()
        If CBool(htRights("Cheque Instructions")) = True Then
            ArrICAssignedPaymentModes.Add("Cheque")
        End If
        If CBool(htRights("DD Instructions")) = True Then
            ArrICAssignedPaymentModes.Add("DD")
        End If
        If CBool(htRights("PO Instructions")) = True Then
            ArrICAssignedPaymentModes.Add("PO")
        End If
    End Sub
    'Protected Sub ddlLocationType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocationType.SelectedIndexChanged
    '    Try
    '        LoadddlBankOrGroupByLocationType(ddlBankOrClient)
    '    Catch ex As Exception

    '    End Try
    'End Sub
    Private Function CheckgvClientRoleCheckedForProcessAll() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvPenidngApprovalChangePrintLocation.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnApproveAmendedPrintLocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveAmendedPrintLocation.Click
        If Page.IsValid Then


            Try

                Dim objICInstruction As ICInstruction
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim objICUser As New ICUser
                Dim objICFromOffice As New ICOffice
                Dim objICToOffice As New ICOffice
                Dim StrAuditTrailAction As String
                Dim chkSelect As CheckBox



                objICUser.es.Connection.CommandTimeout = 3600

                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Approve Amendment Print Location Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Dim dr As DataRow
                Dim dt As New DataTable
                dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Message", GetType(System.String)))

                objICUser.LoadByPrimaryKey(Me.UserId.ToString)

                For Each gvAmendPrintLocationRow As GridDataItem In gvPenidngApprovalChangePrintLocation.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvAmendPrintLocationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        objICInstruction = New ICInstruction
                        objICInstruction.es.Connection.CommandTimeout = 3600
                        objICInstruction.LoadByPrimaryKey(gvAmendPrintLocationRow.GetDataKeyValue("InstructionID").ToString)
                        dr = dt.NewRow
                        Try

                            If Not objICInstruction.PrintLocationAmendedBy = Me.UserId Then

                                Try
                                    objICFromOffice = New ICOffice
                                    objICToOffice = New ICOffice
                                    objICToOffice.es.Connection.CommandTimeout = 3600
                                    objICFromOffice.es.Connection.CommandTimeout = 3600
                                    objICFromOffice.LoadByPrimaryKey(objICInstruction.PreviousPrintLocationCode.ToString)
                                    objICToOffice.LoadByPrimaryKey(objICInstruction.AmendedPrintLocationCode.ToString)

                                    If objICUser.UserType.ToString = "Client User" Then

                                        If objICFromOffice.OffceType.ToString = "Branch Office" And objICToOffice.OffceType.ToString = "Branch Office" Then
                                            StrAuditTrailAction = Nothing
                                            StrAuditTrailAction += "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skip on approve print location amendment queue due to "
                                            StrAuditTrailAction += " print location to and from are not of client."
                                            objICInstruction.SkipReason = StrAuditTrailAction
                                            ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                            'ICUtilities.AddAuditTrail(StrAuditTrailAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.ToString, "SKIP")
                                            dr("InstructionID") = objICInstruction.InstructionID.ToString()
                                            'dr("Message") = "Instruction Skip: print location amendment not approved due to unauthorized access."
                                            dr("Message") = "Instruction(s) Skipped"
                                            dr("Status") = objICInstruction.Status.ToString
                                            dt.Rows.Add(dr)
                                            FailToCancelCount = FailToCancelCount + 1
                                            Continue For
                                        ElseIf objICFromOffice.OffceType.ToString = "Branch Office" And objICToOffice.OffceType.ToString = "Company Office" Then
                                            StrAuditTrailAction = Nothing
                                            StrAuditTrailAction += "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skip on approve print location amendment queue due to "
                                            StrAuditTrailAction += " print location to and from are not of client."

                                            objICInstruction.SkipReason = StrAuditTrailAction
                                            ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                            'ICUtilities.AddAuditTrail(StrAuditTrailAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.ToString, "SKIP")
                                            dr("InstructionID") = objICInstruction.InstructionID.ToString()
                                            'dr("Message") = "Instruction Skip: print location amendment not approved due to unauthorized access."
                                            dr("Message") = "Instruction(s) Skipped"
                                            dr("Status") = objICInstruction.Status.ToString
                                            dt.Rows.Add(dr)
                                            FailToCancelCount = FailToCancelCount + 1
                                            Continue For
                                        ElseIf objICFromOffice.OffceType.ToString = "Company Office" And objICToOffice.OffceType.ToString = "Branch Office" Then
                                            StrAuditTrailAction = Nothing
                                            StrAuditTrailAction += "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skip on approve print location amendment queue due to "
                                            StrAuditTrailAction += " print location to and from are not of client."

                                            objICInstruction.SkipReason = StrAuditTrailAction
                                            ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                            'ICUtilities.AddAuditTrail(StrAuditTrailAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.ToString, "SKIP")
                                            dr("InstructionID") = objICInstruction.InstructionID.ToString()
                                            'dr("Message") = "Instruction Skip: print location amendment not approved due to unauthorized access."
                                            dr("Message") = "Instruction(s) Skipped"
                                            dr("Status") = objICInstruction.Status.ToString
                                            dt.Rows.Add(dr)
                                            FailToCancelCount = FailToCancelCount + 1
                                            Continue For
                                        End If
                                    End If



                                    objICInstruction.PrintLocationCode = objICInstruction.AmendedPrintLocationCode
                                    objICInstruction.PrintLocationName = objICInstruction.AmendedPrintLocationName

                                    objICInstruction.IsPrintLocationAmendmentApproved = True
                                    objICInstruction.PrintLocationAmendmentApprovedBy = Me.UserId
                                    objICInstruction.PrintLocationApprovedAmendmentDate = Date.Now
                                    objICInstruction.IsPrintLocationAmended = False





                                    StrAuditTrailAction = Nothing
                                    StrAuditTrailAction += "Print location of Instruction with ID [ " & objICInstruction.InstructionID & " ] after amendment from [ " & objICFromOffice.OfficeCode & " ][ " & objICFromOffice.OfficeName & " ] to [ " & objICToOffice.OfficeCode & " ][ " & objICToOffice.OfficeName & " ] approved."
                                    StrAuditTrailAction += "Action was taken by user [ " & Me.UserInfo.UserID & " ] [ " & Me.UserInfo.Username & " ]"
                                    objICInstruction.AmendedPrintLocationCode = Nothing
                                    objICInstruction.AmendedPrintLocationName = Nothing
                                    objICInstruction.PreviousPrintLocationCode = Nothing
                                    objICInstruction.PreviousPrintLocationName = Nothing

                                    ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId, Me.UserInfo.Username, StrAuditTrailAction, "UPDATE")
                                    PassToCancelCount = PassToCancelCount + 1
                                    dr("InstructionID") = objICInstruction.InstructionID.ToString()
                                    dr("Message") = "Print location amendment approved successfully"
                                    dr("Status") = objICInstruction.Status.ToString
                                    dt.Rows.Add(dr)
                                    Continue For
                                Catch ex As Exception
                                    objICInstruction.SkipReason = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID.ToString & " ] skip on approve print location amendment queue due to " & ex.Message.ToString & ". Action was taken by [ " & Me.UserId & " ] [ " & Me.UserInfo.Username & " ]"
                                    ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                    'ICUtilities.AddAuditTrail("Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID.ToString & " ] skip on approve print location amendment queue due to " & ex.Message.ToString, "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.ToString, "SKIP")
                                    dr("InstructionID") = objICInstruction.InstructionID.ToString()
                                    'dr("Message") = "Instruction(s) Skipped: print location amendment not approved due to " & ex.Message.ToString
                                    dr("Message") = "Instruction(s) Skipped"
                                    dr("Status") = objICInstruction.Status.ToString
                                    dt.Rows.Add(dr)
                                    FailToCancelCount = FailToCancelCount + 1
                                    Continue For
                                End Try
                            Else
                                StrAuditTrailAction = Nothing
                                StrAuditTrailAction += "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID.ToString & " ] skip on approve print location amendment queue due to "
                                StrAuditTrailAction += "allow re-print maker [  " & objICInstruction.ReprintingAllowedBy & " ] is same as [ " & Me.UserId.ToString & " ] [ " & Me.UserInfo.Username.ToString & " ]"
                                objICInstruction.SkipReason = StrAuditTrailAction
                                ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                'ICUtilities.AddAuditTrail(StrAuditTrailAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.ToString, "SKIP")
                                dr("InstructionID") = objICInstruction.InstructionID.ToString()
                                'dr("Message") = "Instruction Skip: print location amendment must be approved by other than maker."
                                dr("Message") = "Instruction(s) Skipped"
                                dr("Status") = objICInstruction.Status.ToString
                                dt.Rows.Add(dr)
                                FailToCancelCount = FailToCancelCount + 1
                                Continue For
                            End If
                        Catch ex As Exception
                            objICInstruction.SkipReason = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID.ToString & " ] skip on approve print location amendment queue due to " & ex.Message.ToString & ". Action was taken by [ " & Me.UserId & " ] [ " & Me.UserInfo.Username & " ]"
                            ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            'ICUtilities.AddAuditTrail("Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID.ToString & " ] skip on approve print location amendment queue due to " & ex.Message.ToString, "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.ToString, "SKIP")
                            dr("InstructionID") = objICInstruction.InstructionID.ToString()
                            'dr("Message") = "Instruction Skip: print location amendment not approved due to " & ex.Message.ToString
                            dr("Message") = "Instruction(s) Skipped"
                            dr("Status") = objICInstruction.Status.ToString
                            dt.Rows.Add(dr)
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For
                        End Try
                    End If

                Next
                LoadgvAccountPaymentNatureProductType(True)

                If dt.Rows.Count > 0 Then
                    gvShowSummary.DataSource = Nothing
                    gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                    gvShowSummary.DataBind()
                    pnlShowSummary.Style.Remove("display")
                    Dim scr As String
                    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                Else
                    gvShowSummary.DataSource = Nothing
                    pnlShowSummary.Style.Add("display", "none")
                End If
                'If FailToCancelCount = 0 And PassToCancelCount <> 0 Then
                '    UIUtilities.ShowDialog(Me, "Approve Amendment Print Location Queue", "[ " & PassToCancelCount & " ] Instruction(s) print location amendment approved successfully", ICBO.IC.Dialogmessagetype.Success)
                '    LoadgvAccountPaymentNatureProductType(True)
                'ElseIf FailToCancelCount <> 0 And PassToCancelCount = 0 Then
                '    UIUtilities.ShowDialog(Me, "Approve Amendment Print Location Queue", "[ " & FailToCancelCount & " ] Instruction(s) print location amendment not approved successfully", ICBO.IC.Dialogmessagetype.Failure)
                '    LoadgvAccountPaymentNatureProductType(True)
                'ElseIf FailToCancelCount <> 0 And PassToCancelCount <> 0 Then
                '    UIUtilities.ShowDialog(Me, "Approve Amendment Print Location Queue", "[ " & PassToCancelCount & " ] Instruction(s) print location amendment approved successfully <br/ > [ " & FailToCancelCount & " ] Instruction(s) Instruction(s) print location amendment not approved successfully", ICBO.IC.Dialogmessagetype.Failure)
                '    LoadgvAccountPaymentNatureProductType(True)
                'End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnCancelAmendedPrintLocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelAmendedPrintLocation.Click
        If Page.IsValid Then
            Try

                Dim objICInstruction As ICInstruction
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim StrAuditTrailAction As String
                Dim AmendedPrintLocationCode As String = Nothing
                Dim AmendedPrintLocationName As String = Nothing
                Dim chkSelect As CheckBox
                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Approve Amendment Print Location Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Dim dr As DataRow
                Dim dt As New DataTable
                dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Message", GetType(System.String)))
                For Each gvAmendPrintLocationRow As GridDataItem In gvPenidngApprovalChangePrintLocation.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvAmendPrintLocationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        objICInstruction = New ICInstruction
                        objICInstruction.es.Connection.CommandTimeout = 3600
                        objICInstruction.LoadByPrimaryKey(gvAmendPrintLocationRow.GetDataKeyValue("InstructionID").ToString)
                        dr = dt.NewRow
                        Try
                            AmendedPrintLocationCode = Nothing
                            AmendedPrintLocationName = Nothing
                            AmendedPrintLocationCode = objICInstruction.AmendedPrintLocationCode
                            AmendedPrintLocationName = objICInstruction.AmendedPrintLocationName

                            objICInstruction.PrintLocationCode = objICInstruction.PreviousPrintLocationCode
                            objICInstruction.PrintLocationName = objICInstruction.PreviousPrintLocationName
                            objICInstruction.PreviousPrintLocationCode = Nothing
                            objICInstruction.PreviousPrintLocationName = Nothing
                            objICInstruction.AmendedPrintLocationCode = Nothing
                            objICInstruction.AmendedPrintLocationName = Nothing
                            objICInstruction.IsPrintLocationAmendmentCancelled = True
                            objICInstruction.PrintLocationAmendmentCancelledBy = Me.UserId
                            objICInstruction.PrintLocationAmendmentCancellDate = Date.Now

                            objICInstruction.IsPrintLocationAmended = False
                            objICInstruction.PrintLocationAmendmentDate = Nothing
                            objICInstruction.PrintLocationAmendedBy = Nothing



                            objICInstruction.IsPrintLocationAmendmentApproved = False
                            objICInstruction.PrintLocationApprovedAmendmentDate = Nothing
                            objICInstruction.PrintLocationAmendmentApprovedBy = Nothing

                            StrAuditTrailAction = Nothing
                            StrAuditTrailAction += "Print location amendment cancelled for instruction with ID [ " & objICInstruction.InstructionID & " ] and set its previous values from [ " & AmendedPrintLocationCode & " ][ " & AmendedPrintLocationName & " ] to [ " & objICInstruction.PreviousPrintLocationCode & " ][ " & objICInstruction.PreviousPrintLocationName & " ]."
                            StrAuditTrailAction += "Action was taken by user [ " & Me.UserInfo.UserID & " ] [ " & Me.UserInfo.Username & " ]"
                            ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId, Me.UserInfo.Username, StrAuditTrailAction, "UPDATE")
                            PassToCancelCount = PassToCancelCount + 1
                            dr("InstructionID") = objICInstruction.InstructionID.ToString()
                            dr("Message") = "Print location amendment cancelled successfully"
                            dr("Status") = objICInstruction.Status.ToString
                            dt.Rows.Add(dr)
                            Continue For
                        Catch ex As Exception
                            objICInstruction.SkipReason = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID.ToString & " ] skip from cancel print location amendment on approve print location amendment queue due to " & ex.Message.ToString & ". Action was taken by [ " & Me.UserId & " ] [ " & Me.UserInfo.Username & " ]"
                            ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            'ICUtilities.AddAuditTrail("Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID.ToString & " ] skip from cancel print location amendment on approve print location amendment queue due to " & ex.Message.ToString, "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.ToString, "SKIP")
                            dr("InstructionID") = objICInstruction.InstructionID.ToString()
                            'dr("Message") = "Print location amendment not cancelled successfully due to " & ex.Message.ToString
                            dr("Message") = "Instruction(s) skipped"
                            dr("Status") = objICInstruction.Status.ToString
                            dt.Rows.Add(dr)
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For
                        End Try
                    End If

                Next
                LoadgvAccountPaymentNatureProductType(True)

                If dt.Rows.Count > 0 Then
                    gvShowSummary.DataSource = Nothing
                    gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                    gvShowSummary.DataBind()
                    pnlShowSummary.Style.Remove("display")
                    Dim scr As String
                    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                Else
                    gvShowSummary.DataSource = Nothing
                    pnlShowSummary.Style.Add("display", "none")
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub gvPenidngApprovalChangePrintLocation_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvPenidngApprovalChangePrintLocation.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvPenidngApprovalChangePrintLocation.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvPenidngApprovalChangePrintLocation_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvPenidngApprovalChangePrintLocation.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvPenidngApprovalChangePrintLocation.MasterTableView.ClientID & "','0');"
        End If
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            If item.GetDataKeyValue("IsAmendmentComplete") = True Then

                gvPenidngApprovalChangePrintLocation.AlternatingItemStyle.CssClass = "GridItemComplete"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItemComplete"


            Else
                gvPenidngApprovalChangePrintLocation.AlternatingItemStyle.CssClass = "GridItem"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItem"


            End If

        End If
    End Sub

    Protected Sub gvPenidngApprovalChangePrintLocation_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvPenidngApprovalChangePrintLocation.NeedDataSource
        Try
            LoadgvAccountPaymentNatureProductType(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        Try
                Response.Redirect(NavigateURL(41))
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub


End Class

