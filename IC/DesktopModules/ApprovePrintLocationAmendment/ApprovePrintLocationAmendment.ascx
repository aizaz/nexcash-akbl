﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ApprovePrintLocationAmendment.ascx.vb"
    Inherits="DesktopModules_ApprovePrintLocationAmendment_ApprovePrintLocationAmendment" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to cancel selected instruction(s)?") == true) {
            var btnCancel = document.getElementById('<%=btnCancelAmendedPrintLocation.ClientID%>')
            btnCancel.value = "Processing...";
            btnCancel.disabled = true;

            var a = btnCancel.name;
            __doPostBack(a, '');
            return true;
        }
        else {
            return false;
        }
    }
    $(document).ready(function () {

        // Dialog
        $('#urldialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#loading').hide();



            },
            resizable: false
        });
        // Summary Dialog
        $('#sumdialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#sumdialog').hide();
                //                window.location.reload();
                //                window.location.reload();sa
            },
            resizable: false
        });
    });
    function showsumdialog(title, modal, width, height) {
        //  alert(modal);
        $('#sumdialog').dialog("option", "title", title);
        $('#sumdialog').dialog("option", "modal", modal);
        $('#sumdialog').dialog("option", "width", width);
        $('#sumdialog').dialog("option", "height", height);
        $('#sumdialog').dialog('open');

        return false;

    };


    function showurldialog(title, url, modal, width, height) {
        //  alert(modal);
        $('#urldialog').dialog("option", "title", title);
        $('#urldialog').dialog("option", "modal", modal);
        $('#urldialog').dialog("option", "width", width);
        $('#urldialog').dialog("option", "height", height);
        $('#urldialog').dialog('open');
        $('#dialogiframe').hide();
        $('#loading').show();
        $('#dialogiframe').attr("src", url);
        $('#dialogiframe').load(function () {
            $('#loading').hide();
            $('#dialogiframe').show();
        });
        return false;

    };

    function CloseIframe() {
        $('#urldialog').dialog('close');
    }
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;

        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }


</script>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:TextBoxSetting BehaviorID="reTnxCount" Validation-IsRequired="false" EmptyMessage="Transaction Count"
        ErrorMessage="Transaction Count">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTnxCount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reTotalAmount" Validation-IsRequired="false"
        EmptyMessage="Total Amount" ErrorMessage="Total Amount">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTotalAmount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reCurrency" Validation-IsRequired="false" EmptyMessage="Currency"
        ErrorMessage="Currency">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCurrency" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rePaymentMode" Validation-IsRequired="false"
        EmptyMessage="Payment Mode" ErrorMessage="Payment Mode">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPaymentMode" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reAvailableBalance" Validation-IsRequired="false"
        EmptyMessage="Available Balance" ErrorMessage="Available Balance">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAvailableBalance" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reInstructionNo" Validation-IsRequired="false"
        EmptyMessage="Enter Instruction No." ErrorMessage="Enter Instruction No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstructionNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reInstrumentNo" Validation-IsRequired="false"
        EmptyMessage="Enter Instrument No." ErrorMessage="Enter Instrument No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstrumentNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reReferenceNo" Validation-IsRequired="false"
        EmptyMessage="Enter Reference No." ErrorMessage="Enter Reference No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reRemarks" Validation-IsRequired="true" EmptyMessage="Enter Remarks"
        ErrorMessage="Enter Remarks">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRemarks" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Approve Print Location Amendment Queue"
                            CssClass="headingblue"></asp:Label>
                        <br />
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top">
                        <asp:Label ID="lblNRF" runat="server" Text="No Record Found." CssClass="headingblue"
                            Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top">
                        &nbsp;
                        <telerik:RadGrid ID="gvPenidngApprovalChangePrintLocation" runat="server" AllowPaging="True" CssClass="RadGrid"
                             AllowSorting="false" AutoGenerateColumns="False" CellSpacing="0"
                            ShowFooter="True" PageSize="100">
                            <ClientSettings>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView DataKeyNames="ClientAccountCurrency,InstructionID,IsAmendmentComplete" TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderStyle-Width="4%">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" " TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox2" Text="" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="InstructionID" HeaderText="Instruction No." SortExpression="InstructionID" HeaderStyle-Width="8%">
                                          <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>
                                      <telerik:GridBoundColumn DataField="FileBatchNo" HeaderText="Batch No" SortExpression="FileBatchNo">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CreateDate" HeaderText="Creation Date" SortExpression="CreateDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}" HeaderStyle-Width="8%">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ValueDate" HeaderText="Value Date" SortExpression="ValueDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}" HeaderStyle-Width="8%">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneficiaryName" HeaderText="Bene Name" SortExpression="BeneficiaryName">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="13%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="13%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Amount" HeaderText="Amount" SortExpression="Amount"
                                        HtmlEncode="false" DataFormatString="{0:N2}">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProductTypeCode" HeaderText="Payment Mode" SortExpression="ProductTypeCode">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="10%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="10%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InstrumentNo" HeaderText="Instrument No." SortExpression="InstrumentNo">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneficiaryAccountNo" HeaderText="Account No."
                                        SortExpression="BeneficiaryAccountNo">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="9%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="9%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneAccountBranchCode" HeaderText="Branch Code"
                                        SortExpression="BeneAccountBranchCode">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridBoundColumn DataField="BeneAccountCurrency" HeaderText="Currency" SortExpression="BeneAccountCurrency">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>--%>
                                    <telerik:GridBoundColumn DataField="OfficeName" HeaderText="Previous Print location"
                                        SortExpression="OfficeName">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ChangedLocation" HeaderText="New Print Location"
                                        SortExpression="ChangedLocation">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                             <HeaderStyle CssClass="GridHeader" />
                            <ItemStyle CssClass="GridItem" />
                            <AlternatingItemStyle CssClass="GridItem" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top">
                        <asp:Button ID="btnApproveAmendedPrintLocation" runat="server" CausesValidation="False"
                            CssClass="btn" Text="Approve Print Location" />
                        
                       
                            <asp:Button ID="btnCancelAmendedPrintLocation" runat="server"
                            CssClass="btnCancel" Text="Cancel Print Location" OnClientClick="javascript: return conBukDelete();"/>
                             <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="btnCancel"
                            Text="Cancel" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div id="sumdialog" title="" style="text-align: center">
    <asp:Panel ID="pnlShowSummary" runat="server" Style="display: none; width: 100%"
        ScrollBars="Vertical" Height="600px">
        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnCancel" /><br />
        <telerik:RadGrid ID="gvShowSummary" runat="server" AllowPaging="false" Width="100%" CssClass="RadGrid"
            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
            PageSize="10">
            <AlternatingItemStyle CssClass="rgAltRow" />
            <MasterTableView>
                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                </RowIndicatorColumn>
                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="InstructionID" HeaderText="Instruction No." SortExpression="InstructionID">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                    <%-- <telerik:GridBoundColumn DataField="ReferenceNo" HeaderText="Reference No." SortExpression="ReferenceNo">
                                    </telerik:GridBoundColumn>--%>
                    <telerik:GridBoundColumn DataField="Message" HeaderText="Message" SortExpression="Message">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
                <PagerStyle AlwaysVisible="True" />
            </MasterTableView>
            <ItemStyle CssClass="rgRow" />
            <PagerStyle AlwaysVisible="True" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
    </asp:Panel>
</div>
