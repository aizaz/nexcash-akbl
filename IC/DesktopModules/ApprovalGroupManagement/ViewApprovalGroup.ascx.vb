﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_GroupManagement_ViewApprovalGroup
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Dim dtDa As DataTable
    Private htRights As Hashtable

#Region "Page Load"
    Protected Sub DesktopModules_GroupManagement_ViewApprovalGroup_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlCompany()
                btnSaveApprovalGroup.Visible = CBool(htRights("Add"))
                '' btnDeleteGroups.Visible = CBool(htRights("Delete"))
                LoadApprovalGroupgv(True)

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Approval Group Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Grid View"
    Private Sub LoadApprovalGroupgv(ByVal IsBind As Boolean)
        Try
            ICApprovalGroupController.GetApprovalGroupgv(gvApprovalGroup.CurrentPageIndex + 1, gvApprovalGroup.PageSize, gvApprovalGroup, IsBind, ddlGroup.SelectedValue.ToString, ddlCompany.SelectedValue.ToString)

            If gvApprovalGroup.Items.Count > 0 Then
                gvApprovalGroup.Visible = True
                lblRNF.Visible = False
                '   btnDeleteGroups.Visible = CBool(htRights("Delete"))
                gvApprovalGroup.Columns(4).Visible = CBool(htRights("Update"))
                gvApprovalGroup.Columns(5).Visible = CBool(htRights("Delete"))
            Else
                gvApprovalGroup.Visible = False
                '  btnDeleteGroups.Visible = False
                lblRNF.Visible = True
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Protected Sub gvApprovalGroup_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvApprovalGroup.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvApprovalGroup.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvApprovalGroup_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvApprovalGroup.ItemCommand
        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    Dim ICApprovalGroup As New ICApprovalGroupManagement

                    ICApprovalGroup.es.Connection.CommandTimeout = 3600

                    If ICApprovalGroup.LoadByPrimaryKey(e.CommandArgument.ToString()) Then

                        If ICApprovalGroup.IsActive = False Then

                            ICApprovalGroupController.DeleteApprovalGroup(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())


                            UIUtilities.ShowDialog(Me, "Aproval Group", "Aproval Group deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                            LoadApprovalGroupgv(True)
                        Else
                            UIUtilities.ShowDialog(Me, "Aproval Group", "Aproval Group is active and Cannot be deleted", ICBO.IC.Dialogmessagetype.Failure)
                        End If

                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Deleted Aproval Group", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    '    Protected Sub gvApprovalGroup_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvApprovalGroup.NeedDataSource
    '        LoadGroupgv(False)
    '    End Sub

    '    Protected Sub gvApprovalGroup_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvApprovalGroup.ItemDataBound
    '        Dim chkProcessAll As CheckBox

    '        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
    '            chkProcessAll = New CheckBox
    '            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
    '            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvApprovalGroup.MasterTableView.ClientID & "','0');"
    '        End If
    '    End Sub

    '    Protected Sub gvApprovalGroup_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvApprovalGroup.ItemCreated
    '        Try
    '            If TypeOf (e.Item) Is GridPagerItem Then
    '                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
    '                myPageSizeCombo.Items.Clear()
    '                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
    '                For x As Integer = 0 To UBound(arrPageSizes)
    '                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
    '                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
    '                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvApprovalGroup.MasterTableView.ClientID)
    '                Next
    '                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
    '            End If
    '        Catch ex As Exception
    '            ProcessModuleLoadException(Me, ex, False)
    '            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '        End Try
    '    End Sub

    '    Protected Sub gvApprovalGroup_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvApprovalGroup.ItemCommand
    '        Try
    '            If e.CommandName = "del" Then
    '                Dim ICGroup As New ICGroup

    '                ICGroup.es.Connection.CommandTimeout = 3600
    '                If ICGroup.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
    '                    ICGroupController.DeleteGroup(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
    '                    UIUtilities.ShowDialog(Me, "Delete Group", "Group deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
    '                    LoadGroupgv(True)
    '                End If
    '            End If
    '        Catch ex As Exception
    '            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
    '                UIUtilities.ShowDialog(Me, "Deleted Group", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
    '                Exit Sub
    '            End If
    '            ProcessModuleLoadException(Me, ex, False)
    '            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '        End Try
    '    End Sub

    '    Protected Sub gvApprovalGroup_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvApprovalGroup.PageIndexChanged
    '        gvApprovalGroup.CurrentPageIndex = e.NewPageIndex
    '    End Sub

    '#End Region

    '#Region "Buttons"

    '    Protected Sub btnSaveBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveGroup.Click
    '        Response.Redirect(NavigateURL("SaveGroup", "&mid=" & Me.ModuleId & "&id=0"), False)
    '    End Sub

    '    Protected Sub btnDeleteGroups_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteGroups.Click
    '        Try
    '            Dim rowgvApprovalGroup As GridDataItem
    '            Dim chkSelect As CheckBox
    '            Dim GroupCode As String = ""
    '            Dim PassCount As Integer = 0
    '            Dim FailCount As Integer = 0

    '            If CheckgvApprovalGroupForProcessAll() = True Then
    '                For Each rowgvApprovalGroup In gvApprovalGroup.Items
    '                    chkSelect = New CheckBox
    '                    chkSelect = DirectCast(rowgvApprovalGroup.Cells(0).FindControl("chkSelectAll"), CheckBox)
    '                    If chkSelect.Checked = True Then
    '                        GroupCode = rowgvApprovalGroup("GroupCode").Text.ToString()
    '                        Dim icGroup As New ICGroup
    '                        icGroup.es.Connection.CommandTimeout = 3600
    '                        If icGroup.LoadByPrimaryKey(GroupCode.ToString()) Then
    '                            Try
    '                                ICGroupController.DeleteGroup(GroupCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
    '                                PassCount = PassCount + 1
    '                            Catch ex As Exception
    '                                If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
    '                                    FailCount = FailCount + 1
    '                                End If
    '                            End Try
    '                        End If
    '                    End If

    '                Next

    '                If PassCount = 0 Then
    '                    UIUtilities.ShowDialog(Me, "Delete Group", "[" & FailCount.ToString() & "] Groups can not be deleted due to following reasons : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
    '                    Exit Sub
    '                End If
    '                If FailCount = 0 Then
    '                    UIUtilities.ShowDialog(Me, "Delete Group", "[" & PassCount.ToString() & "] Groups deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
    '                    Exit Sub
    '                End If
    '                UIUtilities.ShowDialog(Me, "Delete Group", "[" & PassCount.ToString() & "] Groups deleted successfully. [" & FailCount.ToString() & "] Groups can not be deleted due to following reasons : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
    '            Else
    '                UIUtilities.ShowDialog(Me, "Delete Group", "Please select atleast one(1) Group.", ICBO.IC.Dialogmessagetype.Warning)
    '            End If
    '        Catch ex As Exception
    '            ProcessModuleLoadException(Me, ex, False)
    '            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '        End Try

    '    End Sub

    '    Private Function CheckgvApprovalGroupForProcessAll() As Boolean
    '        Try
    '            Dim rowgvApprovalGroup As GridDataItem
    '            Dim chkSelect As CheckBox
    '            Dim Result As Boolean = False
    '            For Each rowgvApprovalGroup In gvApprovalGroup.Items
    '                chkSelect = New CheckBox
    '                chkSelect = DirectCast(rowgvApprovalGroup.Cells(0).FindControl("chkSelectAll"), CheckBox)
    '                If chkSelect.Checked = True Then
    '                    Result = True
    '                    Exit For
    '                End If
    '            Next
    '            Return Result
    '        Catch ex As Exception
    '            ProcessModuleLoadException(Me, ex, False)
    '            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '        End Try
    '    End Function

#End Region

    Protected Sub gvApprovalGroup_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles gvApprovalGroup.NeedDataSource
        LoadApprovalGroupgv(False)
    End Sub

    Protected Sub btnSaveApprovalGroup_Click(sender As Object, e As EventArgs) Handles btnSaveApprovalGroup.Click
        If Page.IsValid Then
            Response.Redirect(NavigateURL("AddApprovalGroup", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If
    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICApprovalGroupController.GetAllActiveGroups
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            'ddlCompany.DataSource = ICApprovalGroupController.GetAllActiveApproveIspaymentAllowedCompaniesByApprovlGroupCode(ddlGroup.SelectedValue.ToString)
            ddlCompany.DataSource = ICApprovalGroupController.GetAllActiveApproveIspaymentAllowedCompaniesByApprovlGroupCode(ddlGroup.SelectedValue.ToString)
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
            LoadApprovalGroupgv(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadApprovalGroupgv(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
