﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals

Partial Class DesktopModules_GroupManagement_SaveApprovalGroup
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ApprovalGroupid As String
    Private CompanyCode As String
    Private htRights As Hashtable

#Region "Page Load"
    Protected Sub DesktopModules_GroupManagement_SaveGroup_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            ApprovalGroupid = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlCompany()

                If ApprovalGroupid.ToString() = "0" Then
                    Clear()
                    lblPageHeader.Text = "Add Group"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    lblPageHeader.Text = "Edit Group"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))

                    
                    Dim ICApprovalGroup As New ICApprovalGroupManagement
                    If ICApprovalGroup.LoadByPrimaryKey(ApprovalGroupid) Then
                        txtApprovalGroupName.Text = ICApprovalGroup.GroupName.ToString()
                        chkActive.Checked = ICApprovalGroup.IsActive
                        LoadddlGroup()
                        ddlGroup.SelectedValue = ICApprovalGroup.UpToICCompanyByCompanyCode.GroupCode
                        LoadddlCompany()
                        ddlCompany.SelectedValue = ICApprovalGroup.CompanyCode
                        ddlGroup.Enabled = False
                        ddlCompany.Enabled = False
                    End If
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Approval Group Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        LoadddlGroup()
        LoadddlCompany()
        txtApprovalGroupName.Text = ""
        chkActive.Checked = True

    End Sub

#End Region

    '#Region "Buttons"
    '    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '        Try
    '            Dim ICGroup As New ICGroup
    '            ICGroup.es.Connection.CommandTimeout = 3600

    '            If GroupCode.ToString() <> "0" Then
    '                ICGroup.LoadByPrimaryKey(GroupCode)
    '            End If

    '            ICGroup.GroupName = txtGroupName.Text.ToString()
    '            ICGroup.IsActive = chkActive.Checked

    '            If GroupCode = "0" Then
    '                ICGroup.CreatedBy = Me.UserId
    '                ICGroup.CreatedDate = Date.Now
    '                ICGroup.Creater = Me.UserId
    '                ICGroup.CreationDate = Date.Now
    'If CheckDuplicate(ICGroup) = False Then
    '                    ICGroupController.AddGroup(ICGroup, False, Me.UserId, Me.UserInfo.Username)
    '                    UIUtilities.ShowDialog(Me, "Save Group", "Group added successfully.", ICBO.IC.Dialogmessagetype.Success)
    '                    Clear()
    '                Else
    '                    UIUtilities.ShowDialog(Me, "Save Group", "Can not add duplicate Group.", ICBO.IC.Dialogmessagetype.Failure)
    '                End If
    '            Else
    '                ICGroup.CreatedBy = Me.UserId
    '                ICGroup.CreatedDate = Date.Now
    '                If CheckDuplicate(ICGroup) = False Then
    '                    ICGroupController.AddGroup(ICGroup, True, Me.UserId, Me.UserInfo.Username)
    '                    UIUtilities.ShowDialog(Me, "Save Group", "Group updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
    '                    Clear()
    '                Else
    '                    UIUtilities.ShowDialog(Me, "Save Group", "Can not add duplicate Group.", ICBO.IC.Dialogmessagetype.Failure)
    '                End If
    '            End If

    '        Catch ex As Exception
    '            ProcessModuleLoadException(Me, ex, False)
    '            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '        End Try
    '    End Sub

    '    Private Function CheckDuplicate(ByVal CGroup As ICGroup) As Boolean
    '        Try
    '            Dim rslt As Boolean = False
    '            Dim ICGroup As New ICGroup
    '            Dim collGroup As New ICGroupCollection

    '            ICGroup.es.Connection.CommandTimeout = 3600
    '            collGroup.es.Connection.CommandTimeout = 3600

    '            collGroup.Query.Where(collGroup.Query.GroupName.ToLower.Trim = CGroup.GroupName.ToLower.Trim)
    '            If collGroup.Query.Load() Then
    '                rslt = True
    '            End If
    '            Return rslt
    '        Catch ex As Exception
    '            ProcessModuleLoadException(Me, ex, False)
    '            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '        End Try
    '    End Function

    '    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    '        Response.Redirect(NavigateURL(), False)
    '    End Sub

    '#End Region

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICApprovalGroupController.GetAllActiveGroups
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            'ddlCompany.DataSource = ICApprovalGroupController.GetAllActiveApproveIspaymentAllowedCompaniesByApprovlGroupCode(ddlGroup.SelectedValue.ToString)
            ddlCompany.DataSource = ICApprovalGroupController.GetAllActiveApproveIspaymentAllowedCompaniesByApprovlGroupCode(ddlGroup.SelectedValue.ToString)
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim ICApprovalGroup As New ICApprovalGroupManagement
                ICApprovalGroup.es.Connection.CommandTimeout = 3600

                If ICApprovalGroup.ToString() <> "0" Then
                    ICApprovalGroup.LoadByPrimaryKey(ApprovalGroupid)
                End If

                ICApprovalGroup.GroupName = txtApprovalGroupName.Text.ToString()
                ICApprovalGroup.IsActive = chkActive.Checked
                ICApprovalGroup.CompanyCode = ddlCompany.SelectedValue.ToString

                If ApprovalGroupid = "0" Then

                    ICApprovalGroup.CreatedBy = Me.UserId
                    ICApprovalGroup.CreateDate = Date.Now
                    If CheckDuplicate(ICApprovalGroup, False) = False Then
                        ICApprovalGroupController.AddApprovalGroup(ICApprovalGroup, False, Me.UserId, Me.UserInfo.Username)
                        UIUtilities.ShowDialog(Me, "Save Group", "Group added successfully.", ICBO.IC.Dialogmessagetype.Success)
                        Clear()
                    Else
                        UIUtilities.ShowDialog(Me, "Save Group", "Can not add duplicate Group.", ICBO.IC.Dialogmessagetype.Failure)
                    End If
                Else
                    ICApprovalGroup.CreatedBy = Me.UserId
                    ICApprovalGroup.CreateDate = Date.Now
                    If CheckDuplicate(ICApprovalGroup, True) = False Then
                        ICApprovalGroupController.AddApprovalGroup(ICApprovalGroup, True, Me.UserId, Me.UserInfo.Username)
                        UIUtilities.ShowDialog(Me, "Save Group", "Group updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Clear()
                    Else
                        UIUtilities.ShowDialog(Me, "Save Group", "Can not add duplicate Group.", ICBO.IC.Dialogmessagetype.Failure)
                    End If
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Private Function CheckDuplicate(ByVal CApprovalGroup As ICApprovalGroupManagement, ByVal IsUpdate As Boolean) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim ICApprovalGroup As New ICApprovalGroupManagement
            Dim collApprovalGroup As New ICApprovalGroupManagementCollection

            'ICGroup.es.Connection.CommandTimeout = 3600
            'collGroup.es.Connection.CommandTimeout = 3600

            If IsUpdate = False Then
                collApprovalGroup.Query.Where(collApprovalGroup.Query.GroupName.ToLower.Trim = CApprovalGroup.GroupName.ToLower.Trim)
                If collApprovalGroup.Query.Load() Then
                    rslt = True
                End If
            Else
                collApprovalGroup.Query.Where(collApprovalGroup.Query.GroupName.ToLower.Trim = CApprovalGroup.GroupName.ToLower.Trim)
                collApprovalGroup.Query.Where(collApprovalGroup.Query.ApprovalGroupID <> CApprovalGroup.ApprovalGroupID)
                If collApprovalGroup.Query.Load() Then
                    rslt = True
                End If
            End If




            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click



        Response.Redirect(NavigateURL(), False)


    End Sub


    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
