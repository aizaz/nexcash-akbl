﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BankSecondaryApprovalQueue.ascx.vb" Inherits="DesktopModules_BankSecondaryApprovalQueue_BankSecondaryApprovalQueue" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Bank Secondary Approval Queue" 
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <table width="100%">
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblAccountPaymentNature" runat="server" Text="Account Payment Nature"
                                        CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                                <td align="left" valign="top" style="width: 25%">
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:DropDownList ID="ddlAccountPaymentNature" runat="server" Width="100%" CssClass="dropdown">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Label ID="lblNRF" runat="server" Text="No Record Found." CssClass="headingblue"
                            Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <telerik:RadGrid ID="gvSecondaryApprovalQueue" runat="server" 
                            AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" 
                            PageSize="10">
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView>
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="" DataField="">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select" TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="InstructionNo" HeaderText="Instruction No." SortExpression="InstructionNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Dated" HeaderText="Dated" SortExpression="Dated"
                                        DataFormatString="{0:dd-MMM-yyyy}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Bene Name" HeaderText="Bene Name" SortExpression="Bene Name">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Amount" HeaderText="Amount" DataFormatString="{0:N2}"
                                        SortExpression="Amount">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProductType" HeaderText="Product Type" SortExpression="ProductType">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InstrumentNo" HeaderText="Instrument No." SortExpression="InstrumentNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Print Location" HeaderText="Print Location" SortExpression="Print Location">
                                    </telerik:GridBoundColumn>                                  
                                    <telerik:GridTemplateColumn DataField="View Details">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlDetails" runat="server" ImageUrl="~/images/view.gif" ToolTip="Instruction Details">Detail</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%">
                        <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn" />
                        &nbsp;
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" />
                        </td>
                </tr>               
            </table>
        </td>
    </tr>
</table>
