﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveCollectionProductType.ascx.vb"
    Inherits="DesktopModules_CollectionProductTypeManagement_SaveCollectionProductType" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .style1
    {
        width: 1316px;
    }
    .btn
    {
    }
</style>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }

</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior2" EmptyMessage="type.."
        Type="Number">
    </telerik:NumericTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior2" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9]{1,10}$" ErrorMessage="Invalid Product Type Code"
        EmptyMessage="Enter Product Type Code">
        <TargetControls>
            <telerik:TargetInput ControlID="txtProductTypeCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="regFloatDays" Validation-IsRequired="true"
        ValidationExpression="^[0-9]{1,5}" ErrorMessage="Invalid Float Days" EmptyMessage="Enter Float Days">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFloatDays" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="true"
        EmptyMessage="Enter Product Type Name" ErrorMessage="Invalid Product Type Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtProductTypeName" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 33%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 17%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblProductTypeCode" runat="server" Text="Product Type Code" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblProductTypeCodeReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblProductTypeName" runat="server" Text="Product Type Name" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblProductTypeNameReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:TextBox ID="txtProductTypeCode" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtProductTypeName" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblCollModes" runat="server" Text="Select Collection Mode" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblCollModesReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblFloatDays" runat="server" Text="Float Days" CssClass="lbl"></asp:Label>
            <asp:Label ID="FloatDaysReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:DropDownList ID="ddlCollModes" runat="server" CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem>Cash</asp:ListItem>
                <asp:ListItem>Cheque</asp:ListItem>
                <asp:ListItem>Demand Draft</asp:ListItem>
                <asp:ListItem>Pay Order</asp:ListItem>
                <asp:ListItem>Fund Transfer with Cheque</asp:ListItem>
                <asp:ListItem>Fund Transfer without Cheque</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtFloatDays" runat="server" CssClass="txtbox" MaxLength="5"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:RequiredFieldValidator ID="rfvddlCollModes" runat="server" ControlToValidate="ddlCollModes"
                Display="Dynamic" ErrorMessage="Please select Collection Mode" InitialValue="0"
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblIsActive" runat="server" Text="Is Active" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIsApproved" runat="server" Text="Is Approved" CssClass="lbl" Visible="False"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
     <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:CheckBox ID="chkActive" runat="server" Text=" " CssClass="chkBox" Checked="True" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkApproved" runat="server" Text=" " Visible="False" CssClass="chkBox" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
   
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnApproved" runat="server" Text="Approved" CssClass="btn" Visible="False"
                Width="71px" CausesValidation="False" Height="26px" />
            &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px"
                Height="26px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel"
                Width="75px" CausesValidation="False" Height="26px" />
            &nbsp;
        </td>
    </tr>
</table>
