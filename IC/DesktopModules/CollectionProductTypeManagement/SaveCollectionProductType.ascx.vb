﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_CollectionProductTypeManagement_SaveCollectionProductType
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ProductTypeCode As String
    Private htRights As Hashtable

#Region "Page Load"

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            ProductTypeCode = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing

                If ProductTypeCode.ToString() = "0" Then
                    Clear()

                    lblIsApproved.Visible = False
                    chkApproved.Visible = False
                    btnApproved.Visible = False
                   
                    lblPageHeader.Text = "Add Collection Product Type"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))

                Else
                    lblIsApproved.Visible = True
                    chkApproved.Visible = True
                    btnApproved.Visible = True
                    lblPageHeader.Text = "Edit Collection Product Type"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))

                    btnApproved.Visible = CBool(htRights("Approve"))
                   
                    txtProductTypeCode.ReadOnly = True
                    txtProductTypeName.ReadOnly = True
                    Dim ICCollectionProductType As New ICCollectionProductType
                    ICCollectionProductType.es.Connection.CommandTimeout = 3600
                    ICCollectionProductType.LoadByPrimaryKey(ProductTypeCode)
                    If ICCollectionProductType.LoadByPrimaryKey(ProductTypeCode) Then
                        txtProductTypeCode.Text = ICCollectionProductType.ProductTypeCode.ToString()
                        txtProductTypeName.Text = ICCollectionProductType.ProductTypeName.ToString()
                        txtFloatDays.Text = ICCollectionProductType.FloatDays
                        ddlCollModes.SelectedValue = ICCollectionProductType.DisbursementMode

                        If ICCollectionProductType.IsApproved = True Then
                            chkApproved.Enabled = False
                            btnApproved.Visible = False
                        End If

                        chkActive.Checked = ICCollectionProductType.IsActive
                        chkApproved.Checked = ICCollectionProductType.IsApproved
                    End If
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Collection Product Type Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        txtProductTypeCode.Text = ""
        txtProductTypeName.Text = ""
        txtFloatDays.Text = ""
        ddlCollModes.ClearSelection()
        chkApproved.Checked = False
        chkActive.Checked = True
    End Sub

#End Region


#Region "Button"

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim ICCollPType As New ICCollectionProductType
            ICCollPType.es.Connection.CommandTimeout = 3600
            ICCollPType.ProductTypeCode = txtProductTypeCode.Text.ToString()
            ICCollPType.ProductTypeName = txtProductTypeName.Text.ToString()
            ICCollPType.FloatDays = txtFloatDays.Text
            ICCollPType.DisbursementMode = ddlCollModes.SelectedValue.ToString()
            ICCollPType.IsActive = chkActive.Checked


            If ProductTypeCode = "0" Then
                ICCollPType.CreateBy = Me.UserId
                ICCollPType.CreateDate = Date.Now

                If CheckDuplicateCodeName(ICCollPType) = False Then
                    ICCollectionProductTypeController.AddCollectionProductType(ICCollPType, False, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    UIUtilities.ShowDialog(Me, "Save Collection Product Type", "Collection Product Type added successfully.", ICBO.IC.Dialogmessagetype.Success)
                Else
                    UIUtilities.ShowDialog(Me, "Save Collection Product Type", "Can not add duplicate Collection Product Type.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Else

                ICCollectionProductTypeController.AddCollectionProductType(ICCollPType, True, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                UIUtilities.ShowDialog(Me, "Save Collection Product Type", "Collection Product Type updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
            End If

            Clear()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Function CheckDuplicateCodeName(ByVal cProductType As ICCollectionProductType) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim ICProdctType As New ICCollectionProductType
            Dim collProductType As New ICCollectionProductTypeCollection

            ICProdctType.es.Connection.CommandTimeout = 3600
            collProductType.es.Connection.CommandTimeout = 3600

            collProductType.Query.Where(collProductType.Query.ProductTypeCode.ToLower.Trim = cProductType.ProductTypeCode.ToLower.Trim)
            If collProductType.Query.Load() Then
                rslt = True
            End If

            collProductType = New ICCollectionProductTypeCollection
            collProductType.Query.Where(collProductType.Query.ProductTypeName.ToLower.Trim = cProductType.ProductTypeName.ToLower.Trim)
            If collProductType.Query.Load() Then
                rslt = True
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    Protected Sub btnApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproved.Click
        Dim objCollProductType As New ICCollectionProductType
        objCollProductType.es.Connection.CommandTimeout = 3600
        Try
            objCollProductType.LoadByPrimaryKey(ProductTypeCode.ToString())
            If Me.UserInfo.IsSuperUser = False Then
                If objCollProductType.CreateBy = Me.UserId Then
                    UIUtilities.ShowDialog(Me, "Approve Collection Product Type", "Collection Product Type must be approve by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            End If
            If chkApproved.Checked Then
                ICCollectionProductTypeController.ApproveCollectionProductType(ProductTypeCode.ToString(), chkApproved.Checked, Me.UserId, Me.UserInfo.Username)
                UIUtilities.ShowDialog(Me, "Approve Collection Product Type", "Collection Product Type approve.", ICBO.IC.Dialogmessagetype.Success)
            Else
                UIUtilities.ShowDialog(Me, "Approve Collection Product Type", "Please checked the Approved Check Box.", ICBO.IC.Dialogmessagetype.Warning)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region



#Region "Drop Down"

    Private Sub LoadddlCollModes()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCollModes.Items.Clear()
            ddlCollModes.Items.Add(lit)
            ddlCollModes.AppendDataBoundItems = True
            ddlCollModes.DataSource = ICCollectionProductTypeController.GetAllCollectionProductTypesActiveAndApprove()
            ddlCollModes.DataTextField = "ProductTypeName"
            ddlCollModes.DataValueField = "ProductTypeCode"
            ddlCollModes.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

End Class
