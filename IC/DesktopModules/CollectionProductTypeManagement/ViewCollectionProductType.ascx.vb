﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_CollectionProductTypeManagement_ViewCollectionProductType
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable

#Region "Page Load"

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing
                btnSaveProductType.Visible = CBool(htRights("Add"))


                LoadProductTypes(True)
                ViewState("SortExp") = Nothing
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Collection Product Type Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region


#Region "Grid View"

    Private Sub LoadProductTypes(ByVal IsBind As Boolean)
        Try
            ICCollectionProductTypeController.GetCollectionProductTypesgv(gvProductType.CurrentPageIndex + 1, gvProductType.PageSize, gvProductType, IsBind)

            If gvProductType.Items.Count > 0 Then
                lblProductTypeListHeader.Visible = True
                gvProductType.Visible = True

                lblProductTypeRNF.Visible = False
                btnDeleteProductTypes.Visible = CBool(htRights("Delete"))
                btnApproveProductTypes.Visible = CBool(htRights("Approve"))
                gvProductType.Columns(7).Visible = CBool(htRights("Delete"))
            Else
                lblProductTypeListHeader.Visible = True
                gvProductType.Visible = False
                btnApproveProductTypes.Visible = False
                btnDeleteProductTypes.Visible = False
                lblProductTypeRNF.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Protected Sub gvProductType_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvProductType.ItemCommand
        Try
            If e.CommandName = "del" Then
                Dim ICCollectionProductType As New ICCollectionProductType

                ICCollectionProductType.es.Connection.CommandTimeout = 3600
                ICCollectionProductType.LoadByPrimaryKey(e.CommandArgument.ToString())

                If ICCollectionProductType.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                    If ICCollectionProductType.IsApproved Then
                        UIUtilities.ShowDialog(Me, "Warning", "Product Type is approved and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
                    Else
                        ICCollectionProductTypeController.DeleteCollectionProductType(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        UIUtilities.ShowDialog(Me, "Delete Collection Product Type", "Collection Product Type deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                        LoadProductTypes(True)
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Deleted Product Type", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvProductType_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvProductType.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvProductType.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvProductType_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvProductType.NeedDataSource
        LoadProductTypes(False)
    End Sub

    Protected Sub gvProductType_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvProductType.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvProductType.MasterTableView.ClientID & "','0');"
        End If

    End Sub

#End Region


#Region "Button"

    Protected Sub btnSaveBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveProductType.Click
        Response.Redirect(NavigateURL("SaveCollectionProductType", "&mid=" & Me.ModuleId & "&id=0"), False)
    End Sub

    Protected Sub btnDeleteProductTypes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteProductTypes.Click
        Try
            Dim rowgvCollProdType As GridDataItem
            Dim chkSelect As CheckBox
            Dim ProductTypeCode As String = ""
            Dim PassCount As Integer = 0
            Dim FailCount As Integer = 0

            If CheckgvProductTypeForProcessAll() = True Then
                For Each rowgvCollProdType In gvProductType.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowgvCollProdType.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        ProductTypeCode = rowgvCollProdType("ProductTypeCode").Text.ToString()
                        Dim ICCollectionProductType As New ICCollectionProductType
                        ICCollectionProductType.es.Connection.CommandTimeout = 3600
                        If ICCollectionProductType.LoadByPrimaryKey(ProductTypeCode.ToString()) Then
                            If ICCollectionProductType.IsApproved Then
                                FailCount = FailCount + 1
                            Else
                                Try
                                    ICCollectionProductTypeController.DeleteCollectionProductType(ProductTypeCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                    PassCount = PassCount + 1
                                Catch ex As Exception
                                    If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                        FailCount = FailCount + 1
                                    End If
                                End Try
                            End If
                        End If
                    End If

                Next

                If PassCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Delete Collection Product Type", "[" & FailCount.ToString() & "] Collection Product Type can not be deleted due to following reasons : <br /> 1. Collection Product Type is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    Exit Sub
                End If
                If FailCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Delete Collection Product Type", "[" & PassCount.ToString() & "] Collection Product Type deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    Exit Sub
                End If
                UIUtilities.ShowDialog(Me, "Delete Collection Product Type", "[" & PassCount.ToString() & "] Collection Product Type deleted successfully. [" & FailCount.ToString() & "] Countries can not be deleted due to following reasons : <br /> 1. Country is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
            Else
                UIUtilities.ShowDialog(Me, "Delete Collection Product Type", "Please select atleast one(1) Collection Product Type.", ICBO.IC.Dialogmessagetype.Warning)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Function CheckgvProductTypeForProcessAll() As Boolean
        Try
            Dim rowgvProductType As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvProductType In gvProductType.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvProductType.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnApproveProductTypes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveProductTypes.Click
        Try
            Dim rowgvProductType As GridDataItem
            Dim chkSelect As CheckBox
            Dim ProductTypeCode As String = ""
            Dim PassCount As Integer = 0
            Dim FailCount As Integer = 0
            Dim objCollectionProductType As New ICCollectionProductType

            If CheckgvProductTypeForProcessAll() = True Then
                For Each rowgvProductType In gvProductType.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowgvProductType.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        ProductTypeCode = rowgvProductType("ProductTypeCode").Text.ToString()
                        objCollectionProductType.es.Connection.CommandTimeout = 3600
                        If objCollectionProductType.LoadByPrimaryKey(ProductTypeCode.ToString()) Then
                            If objCollectionProductType.IsApproved Then
                                FailCount = FailCount + 1
                            Else
                                If Me.UserInfo.IsSuperUser = False Then
                                    If objCollectionProductType.CreateBy = Me.UserId Then
                                        FailCount = FailCount + 1
                                    Else
                                        ICCollectionProductTypeController.ApproveCollectionProductType(ProductTypeCode.ToString(), chkSelect.Checked, Me.UserId, Me.UserInfo.Username)
                                        PassCount = PassCount + 1
                                    End If
                                Else
                                    ICCollectionProductTypeController.ApproveCollectionProductType(ProductTypeCode.ToString(), chkSelect.Checked, Me.UserId, Me.UserInfo.Username)
                                    PassCount = PassCount + 1
                                End If
                            End If
                        End If
                    End If

                Next
                If PassCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Approve Collection Product Type", "[" & FailCount.ToString() & "] Collection Product Type can not be approve due to following reasons : <br /> 1. Collection Product Type is approve.<br /> 2. Collection Product Type must be approve by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    Exit Sub
                End If
                If FailCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Approve Collection Product Type", "[" & PassCount.ToString() & "] Collection Product Type approve successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    Exit Sub
                End If
                UIUtilities.ShowDialog(Me, "Approve Collection Product Type", "[" & PassCount.ToString() & "] Collection Product Type approve successfully. [" & FailCount.ToString() & "] Collection Product Type can not be approve due to following reasons : <br /> 1. Collection Product Type is approve.<br /> 2. Collection Product Type must be approve by the user other than the maker.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
            Else
                UIUtilities.ShowDialog(Me, "Approve Collection Product Type", "Please select atleast one(1) Collection Product Type.", ICBO.IC.Dialogmessagetype.Warning)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

#End Region

End Class
