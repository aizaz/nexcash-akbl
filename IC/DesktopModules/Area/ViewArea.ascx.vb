﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Raptorious.SharpMt940Lib
Partial Class DesktopModules_Bank_ViewBank
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Dim dtDa As DataTable

    Protected Sub DesktopModules_Bank_ViewBank_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            


                If CheckIsAdminOrSuperUser() = False Then
                    'ManagePageAccessRights()
                End If
                If Page.IsPostBack = False Then
                    LoadArea()
                    ViewState("SortExp") = Nothing
                End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    'Private Sub ManagePageAccessRights()
    '    Try
    '        ArrRights = FRCRoleRightController.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Manage Area")
    '        If ArrRights.Count > 0 Then
    '            btnSaveBank.Visible = ArrRights(0)
    '        Else
    '            UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
    '        End If
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            ' str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function
    Private Sub LoadArea()
        Dim ICAreaColl As New IC.ICAreaCollection
        ICAreaColl.es.Connection.CommandTimeout = 3600

        ICAreaColl = ICAreaController.GetAllAreas()
        dtDa = ICAreaColl.Query.LoadDataTable()
        ViewState("DataTable") = dtDa
        gvBank.DataSource = dtDa
        gvBank.DataBind()

       



        If CheckIsAdminOrSuperUser() = False Then
            If ArrRights.Count > 0 Then
                gvBank.Columns(5).Visible = ArrRights(2)
            Else
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        End If




    End Sub

    Protected Sub gvBank_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvBank.PageIndexChanged
        If (Not ViewState("sortDirection") Is Nothing And Not ViewState("SortExp") Is Nothing) Then
            Dim sortExpression As String = CStr(ViewState("SortExp")).ToString()
            If GridViewSortDirection() = SortDirection.Ascending Then
                SortGridView(sortExpression, ASCENDING)
            Else
                SortGridView(sortExpression, DESCENDING)
            End If
        Else
            LoadArea()
        End If
    End Sub
    Protected Sub gvBank_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvBank.RowCommand
        Try
            If e.CommandName = "del" Then
                Dim ICArea As New ICArea

                ICArea.es.Connection.CommandTimeout = 3600

                ICArea.LoadByPrimaryKey(e.CommandArgument.ToString())
                If ICArea.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                    If ICArea.IsApproved Then
                        UIUtilities.ShowDialog(Me, "Warning", "Bank is approved and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
                    Else
                        ICAreaController.DeleteArea(e.CommandArgument.ToString())

                        ICUtilities.AddAuditTrail("Area : " & ICArea.AreaName.ToString() & " Deleted", "Area", ICArea.AreaCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString(), "")
                        UIUtilities.ShowDialog(Me, "Delete ", "Bank deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                        LoadArea()
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Deleted Bank", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnSaveBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveArea.Click
        If Page.IsValid Then


            Response.Redirect(NavigateURL("SaveArea", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If
    End Sub

    Protected Sub gvBank_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBank.PageIndexChanging
        gvBank.PageIndex = e.NewPageIndex
        If (Not ViewState("sortDirection") Is Nothing And Not ViewState("SortExp") Is Nothing) Then
            Dim sortExpression As String = CStr(ViewState("SortExp")).ToString()
            If GridViewSortDirection() = SortDirection.Ascending Then
                SortGridView(sortExpression, ASCENDING)
            Else
                SortGridView(sortExpression, DESCENDING)
            End If
        Else
            LoadArea()
        End If

    End Sub
    Public Property GridViewSortDirection() As SortDirection
        Get
            If ViewState("sortDirection") Is Nothing Then
                ViewState("sortDirection") = SortDirection.Descending
            ElseIf ViewState("sortDirection") = SortDirection.Ascending Then
                ViewState("sortDirection") = SortDirection.Descending
            ElseIf ViewState("sortDirection") = SortDirection.Descending Then
                ViewState("sortDirection") = SortDirection.Ascending
            End If

            Return DirectCast(ViewState("sortDirection"), SortDirection)
        End Get
        Set(ByVal value As SortDirection)
            ViewState("sortDirection") = value
        End Set
    End Property


    Private Sub SortGridView(ByVal sortExpression As String, ByVal direction As String)
        '  You can cache the DataTable for improving performance
        Try
            Dim dt As DataTable = ViewState("DataTable")
            Dim sortedDt As New DataTable
            dt.DefaultView.Sort = sortExpression + direction

            gvBank.DataSource = dt.DefaultView
            gvBank.DataBind()


            ViewState("dtGrid") = dt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub

    Protected Sub gvBank_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvBank.Sorting
        ViewState("SortExp") = e.SortExpression
        Dim sortExpression As String = CStr(ViewState("SortExp")).ToString()
        If GridViewSortDirection() = SortDirection.Ascending Then
            SortGridView(sortExpression, ASCENDING)
        Else
            SortGridView(sortExpression, DESCENDING)
        End If
    End Sub
End Class
