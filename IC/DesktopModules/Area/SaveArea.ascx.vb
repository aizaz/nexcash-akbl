﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals

Partial Class DesktopModules_Area_SaveArea
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private AreaCode As String
    Private ArrRights As ArrayList
#Region "Page Load"
    Protected Sub DesktopModules_Bank_SaveBank_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            AreaCode = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                LoadddlCity()
                If AreaCode.ToString() = "0" Then
                    Clear()
                    lblIsApproved.Visible = False
                    chkApproved.Visible = False
                    btnApproved.Visible = False
                    lblPageHeader.Text = "Add Area"
                    btnSave.Text = "Save"
                Else
                    lblIsApproved.Visible = True
                    chkApproved.Visible = True
                    btnApproved.Visible = True
                    lblPageHeader.Text = "Edit Area"
                    btnSave.Text = "Update"
                    txtAreaCode.ReadOnly = True
                    Dim ICArea As New ICArea
                    ICArea.es.Connection.CommandTimeout = 3600
                    ICArea.LoadByPrimaryKey(AreaCode)
                    If ICArea.LoadByPrimaryKey(AreaCode) Then
                        txtAreaCode.Text = ICArea.AreaCode.ToString()
                        txtAreaName.Text = ICArea.AreaName.ToString()
                        ddlCity.SelectedValue = ICArea.UpToICCityByCityCode.CityCode
                      
                        chkActive.Checked = ICArea.IsActive
                        chkApproved.Checked = ICArea.IsApproved
                    End If
                End If
                'btnSave.Visible = ArrRights(1)
            End If
            If CheckIsAdminOrSuperUser() = False Then
                'ManagePageAccessRights()
            End If
            If AreaCode.ToString <> "0" Then
                Dim icArea As New ICArea
                icArea.es.Connection.CommandTimeout = 3600
                icArea.LoadByPrimaryKey(AreaCode)
                If icArea.IsApproved = True Then
                    btnApproved.Visible = False
                    chkApproved.Enabled = False
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
    
    'Private Sub ManagePageAccessRights()
    '    Try
    '        ArrRights = FRCRoleRightController.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Manage Bank")
    '        If ArrRights.Count > 0 Then
    '            If BankCode.ToString() = "0" Then
    '                btnSave.Visible = ArrRights(0)
    '            Else
    '                btnSave.Visible = ArrRights(1)
    '                chkApproved.Enabled = ArrRights(3)
    '                btnApproved.Visible = ArrRights(3)
    '            End If
    '        Else
    '            UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", Intelligenes.FRC.Dialogmessagetype.Warning, NavigateURL(40))
    '        End If
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, Intelligenes.FRC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return Result
        End Try
    End Function
    Private Sub Clear()
        txtAreaCode.Text = ""
        txtAreaName.Text = ""
        ddlCity.ClearSelection()
        chkActive.Checked = True
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then


            Try
                Dim ICArea As New ICArea
                ICArea.es.Connection.CommandTimeout = 3600
                ICArea.AreaCode = txtAreaCode.Text.ToString()
                ICArea.AreaName = txtAreaName.Text.ToString()
                ICArea.CityCode = ddlCity.SelectedValue.ToString()
                ICArea.IsActive = chkActive.Checked
                If AreaCode = "0" Then
                    ICArea.CreateBy = Me.UserId
                    ICArea.CreateDate = Date.Now
                    If CheckDuplicate(ICArea) = False Then
                        ICAreaController.AddArea(ICArea, False)
                        ICUtilities.AddAuditTrail("Area " & ICArea.AreaName.ToString() & " Added", "Area", ICArea.AreaCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString(), "")
                        UIUtilities.ShowDialog(Me, "Save Area", "Area added successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Else
                        UIUtilities.ShowDialog(Me, "SaveArea", "Can not add duplicate Area.", ICBO.IC.Dialogmessagetype.Failure)
                    End If
                Else

                    ICAreaController.AddArea(ICArea, True)
                    ICUtilities.AddAuditTrail("Area  " & ICArea.AreaName.ToString() & " Updated", "Area", ICArea.AreaCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString(), "")
                    UIUtilities.ShowDialog(Me, "Save Area", "Area updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                End If
                Clear()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Function CheckDuplicate(ByVal cArea As ICArea) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim ICArea As New ICArea
            Dim collArea As New ICAreaCollection

            ICArea.es.Connection.CommandTimeout = 3600
            collArea.es.Connection.CommandTimeout = 3600


            If ICArea.LoadByPrimaryKey(cArea.AreaCode) Then
                rslt = True
            End If
            collArea.LoadAll()
            collArea.Query.Where(collArea.Query.AreaName.Like(cArea.AreaName))
            If collArea.Query.Load() Then
                rslt = True
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub
    Protected Sub btnApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproved.Click
        If Page.IsValid Then


            Dim ICArea As New ICArea

            ICArea.es.Connection.CommandTimeout = 3600

            Try
                ICArea.LoadByPrimaryKey(AreaCode.ToString())
                ICAreaController.ApproveArea(AreaCode.ToString(), chkApproved.Checked, Me.UserId.ToString())
                If chkApproved.Checked Then
                    ICUtilities.AddAuditTrail("Area : " & ICArea.AreaName.ToString() & " Approved", "Area", ICArea.AreaCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString(), "")
                    UIUtilities.ShowDialog(Me, "Save Area", "Area" & ICArea.AreaName.ToString() & " approved.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    ICUtilities.AddAuditTrail("Area: " & ICArea.AreaName.ToString() & " Not Approved", "Area", ICArea.AreaCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString(), "")
                    UIUtilities.ShowDialog(Me, "Save Bank", "Bank not approved.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Sub LoadddlCity()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCity.Items.Clear()
            ddlCity.Items.Add(lit)
            ddlCity.AppendDataBoundItems = True
            ddlCity.DataSource = ICCityController.GetAllCityActiveAndApproved()
            ddlCity.DataTextField = "CityName"
            ddlCity.DataValueField = "CityCode"
            ddlCity.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
