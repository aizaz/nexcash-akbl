﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewAccountPaymentNatureProductTypeSecond.ascx.vb"
    Inherits="DesktopModules_AccountPaymentNatureProductTypeManagement_ViewAccountPaymentNatureProductTypeSecond" %>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure to remove this record ?") == true) {

            return true;
        }
        else {
            return false;
        }
    }
    function delcon(item) {


        if (window.confirm("Are you sure to remove this " + item + "?") == true) {

            return true;

        }
        else {

            return false;

        }
    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to remove Record(s)?") == true) {
            return true;
        }
        else {
            return false;
        }
    }  
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        //      alert(grid)
        //        if ((grid.rows.length -1) != 10 ) {
        //        if ((grid.rows.length) >= 12) {
        //            //            alert('More than 10 rows')
        //            for (i = 0; i < grid.rows.length - 1; i++) {
        //                cell = grid.rows[i].cells[CellNo];
        //                for (j = 0; j < cell.childNodes.length - 1; j++) {
        //                    if (cell.childNodes[j].type == "checkbox") {
        //                        cell.childNodes[j].checked = document.getElementById(idOfControllingCheckbox).checked;
        //                    }
        //                }
        //            }
        //        }
        //        else {
        //                     alert(grid.rows.length)
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    //    }
</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table width="100%">
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnSaveAPNPType" runat="server" Text="Add Account Payment Nature Product Type"
                            CssClass="btn" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle">
                        <asp:Label ID="lblAPNPTypeList" runat="server" Text="Account,Payment Nature and Product Type List"
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle">
                        <table width="100%">
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 28%">
                                    <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 22%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="left">
                        <%--                        <asp:GridView ID="gvAccountPaymentNatureProductTypeDetails" runat="server" AutoGenerateColumns="false"
                            CssClass="Grid" AllowPaging="true" AllowSorting="true" EnableModelValidation="true" DataKeyNames="AccountsPaymentNatureProductTypeCode"
                            Width="100%">
                            <AlternatingRowStyle CssClass="GridAltItem" />
                            <Columns>
                            <asp:BoundField DataField="AccountTitle" HeaderText="Account Title" SortExpression="AccountTitle">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="AccountNumber" HeaderText="Account No." SortExpression="AccountNumber">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="PaymentNatureName" HeaderText="Payment Nature" SortExpression="PaymentNatureName">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ProductTypeName" HeaderText="Product Type" SortExpression="ProductTypeName">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>                               
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("AccountsPaymentNatureProductTypeCode")%>'
                                            CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                            ToolTip="Delete" />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="GridHeader" />
                            <PagerStyle CssClass="GridPager" />
                            <RowStyle CssClass="GridItem" />
                        </asp:GridView>--%>
                        <asp:Label ID="lblRNF" runat="server" Font-Bold="False" Text="No Record Found" Visible="False"
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="left">
                        <telerik:RadGrid ID="gvAPNPType" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100"
                            CssClass="RadGrid">
                            <ClientSettings>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView  DataKeyNames="ProductTypeCode,CompanyCode" TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select" TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="CompanyName" HeaderText="Company Name" SortExpression="CompanyName">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                                    </telerik:GridBoundColumn>
                                 

                                    <telerik:GridBoundColumn DataField="ProductTypeName" HeaderText="Product Type" SortExpression="ProductTypeName">
                                    
                                         <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="AccountPaymentNature" HeaderText="Account Payment Nature"
                                        SortExpression="AccountPaymentNature" AllowSorting="false">
                                          <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20%" />
                                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20%" />
                                    </telerik:GridBoundColumn>
<%--Added IsApprove by Farhan--%>
                                     <telerik:GridCheckBoxColumn HeaderText="Approved" DataField="IsApproved">
                                   <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridCheckBoxColumn>

                                    <telerik:GridTemplateColumn HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("AddAccountPaymentNatureProductType", "&mid=" & Me.ModuleId & "&ProductTypeCode=" & Eval("ProductTypeCode") & "&CompanyCode=" & Eval("CompanyCode"))%>'
                                                ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                          <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                    </telerik:GridTemplateColumn>
                       
                                    <telerik:GridTemplateColumn HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandName="del" ImageUrl="~/images/delete.gif"
                                                CommandArgument='<%#Eval("ProductTypeCode").tostring + ";" + Eval("CompanyCode").tostring %>'
                                                OnClientClick="javascript: return con();" ToolTip="Delete" />
                                        </ItemTemplate>
                                          <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="center">
                          <asp:Button ID="btnApproveAPNPType" runat="server" CssClass="btn" Text="Approve Account Payment Nature Product Type" />
                        <asp:Button ID="btnDeleteAPNPType" runat="server" Text="Delete Account Payment Nature Product Type"
                            OnClientClick="javascript: return conBukDelete();" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
