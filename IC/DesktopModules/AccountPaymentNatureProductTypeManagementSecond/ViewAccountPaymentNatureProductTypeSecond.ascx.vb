﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.Configuration
Imports System.Web.Configuration
Imports Telerik.Web.UI

Partial Class DesktopModules_AccountPaymentNatureProductTypeManagement_ViewAccountPaymentNatureProductTypeSecond
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
  Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_AccountPaymentNatureProductTypeManagement_ViewAccountPaymentNatureProductType_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlCompany()
                LoadgvAccountPaymentNatureProductType(True)
                btnSaveAPNPType.Visible = CBool(htRights("Add"))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Account Payment Nature Product Type Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


#Region "Button Events"
    Protected Sub btnSaveAccPayNatProTyp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAPNPType.Click
        If Page.IsValid Then

            Response.Redirect(NavigateURL("AddAccountPaymentNatureProductType", "&mid=" & Me.ModuleId & "&CompanyCode=0" & "&ProductTypeCode=0"), False)

        End If

    End Sub
#End Region

    Private Sub LoadgvAccountPaymentNatureProductType(ByVal DoDataBind As Boolean)
        Try
            Dim GroupCode, CompanyCode As String

            If ddlGroup.SelectedValue.ToString <> "0" Then
                GroupCode = ddlGroup.SelectedValue.ToString
            Else
                GroupCode = ""
            End If
            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            Else
                CompanyCode = ""
            End If
            ICAccountPaymentNatureProductTypeController.GetAllAccountPaymentNatureProductTypeForRadGrid(GroupCode, CompanyCode, Me.gvAPNPType.CurrentPageIndex + 1, Me.gvAPNPType.PageSize, DoDataBind, Me.gvAPNPType)

            If gvAPNPType.Items.Count > 0 Then
                gvAPNPType.Visible = True
                lblRNF.Visible = False

                gvAPNPType.Columns(5).Visible = CBool(htRights("Update"))
                '  gvAPNPType.Columns(4).Visible = CBool(htRights("Approve"))
                gvAPNPType.Columns(6).Visible = CBool(htRights("Delete"))
                btnApproveAPNPType.Visible = CBool(htRights("Approve"))
                btnDeleteAPNPType.Visible = CBool(htRights("Delete"))
            Else
                gvAPNPType.Visible = False
                btnApproveAPNPType.Visible = False
                btnDeleteAPNPType.Visible = False
                lblRNF.Visible = True

            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString)
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#Region "DropDown Events"
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged

        LoadddlCompany()
        LoadgvAccountPaymentNatureProductType(True)
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        LoadgvAccountPaymentNatureProductType(True)
    End Sub
#End Region

    Protected Sub gvAPNPType_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvAPNPType.ItemCommand
        Try

            Dim objICAPNPTypeColl As New ICAccountsPaymentNatureProductTypeCollection
            Dim dt As New DataTable
            Dim StrArray As String() = Nothing
            Dim objICAPNPType As ICAccountsPaymentNatureProductType
            If e.CommandName.ToString = "del" Then
                If Page.IsValid Then
                    dt = ICAccountPaymentNatureProductTypeController.GetAllAccountPaymanetNatureProductTypeByProductTypeCodeAndCompanyCode(e.CommandArgument.ToString.Split(";")(0), e.CommandArgument.ToString.Split(";")(1))
                    For Each dr As DataRow In dt.Rows
                        objICAPNPType = New ICAccountsPaymentNatureProductType
                        If objICAPNPType.LoadByPrimaryKey(dr("AccountNumber").ToString, dr("BranchCode").ToString, dr("Currency").ToString, dr("PaymentNatureCode").ToString, dr("ProductTypeCode").ToString) Then
                            If objICAPNPType.IsApproved = True Then
                                UIUtilities.ShowDialog(Me, "Approved Warning", "Account is already approved and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
                                Continue For
                            End If

                            ICFilesController.DeleteICFilesByAPNPType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Both", "Both")
                            ICAccountsPaymentNatureProductTypePrintLocationsController.DeleteAllAPNPTypePrintLocationsByAPNPType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            ICAccountsPaymentNatureProductTypeTemplatesController.DeleteAllAPNPTypeFileUploadTemplates(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            DeletePackageChargesByAPNPType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            ICAccountPaymentNatureProductTypeController.DeleteAccountPaymentNatureProductType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        End If
                    Next
                    UIUtilities.ShowDialog(Me, "Delete Account Payment Nature Product Type", "Account payment nature product type deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                    LoadgvAccountPaymentNatureProductType(True)
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Error ", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                LoadgvAccountPaymentNatureProductType(False)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub gvAPNPType_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAPNPType.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                ' Dim arrPageSizes() As String = {"1", "2", "5", "10", "50"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAPNPType.MasterTableView.ClientID)
                Next
                'If gvAccountsDetails.Items.Count > 0 Then
                '    myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
                'End If
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAPNPType_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAPNPType.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAPNPType.MasterTableView.ClientID & "','0');"
        End If
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Dim dt As New DataTable
            dt = ICAccountPaymentNatureProductTypeController.GetAllTaggedAccountPaymentNatureByCompanyAndProductTypeCode(item.GetDataKeyValue("CompanyCode").ToString, item.GetDataKeyValue("ProductTypeCode").ToString)
            If dt.Rows.Count > 0 Then
                Dim AccountPaymentNature As String = ""
                Dim AccountPaymentNature2 As String = ""
                For Each dr As DataRow In dt.Rows
                    AccountPaymentNature += dr("PaymentNatureName").ToString & " ; "
                Next
                AccountPaymentNature2 = AccountPaymentNature.Remove(AccountPaymentNature.Length - 2)
                item.Cells(5).Text = AccountPaymentNature2
            Else
                item.Cells(5).Text = ";"

            End If
           
        End If
    End Sub
    Private Function CheckGVAPNPTypeSelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvAPNPTypeRow As GridDataItem In gvAPNPType.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvAPNPTypeRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnDeleteAPNPType_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteAPNPType.Click
        If Page.IsValid Then


            Try
                Dim chkSelect As CheckBox
                Dim PassToDeleteCount As Integer = 0
                Dim FailToDeleteCount As Integer = 0
                Dim objICAPNPTypeColl As ICAccountsPaymentNatureProductTypeCollection
                Dim ProductTypeCode, CompanyCode As String
                Dim dt As New DataTable
                ''  Dim ObjICAccountsPNPT As New ICAccountsPaymentNatureProductType

                If CheckGVAPNPTypeSelected() = False Then
                    UIUtilities.ShowDialog(Me, "Delete Account Payment Nature Product Type", "Please select atleast one account payment nature product type.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVAccountsPNPT In gvAPNPType.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVAccountsPNPT.Cells(0).FindControl("chkSelect"), CheckBox)


                    If chkSelect.Checked = True Then

                        ProductTypeCode = Nothing

                        ProductTypeCode = rowGVAccountsPNPT.GetDataKeyValue("ProductTypeCode").ToString
                        CompanyCode = rowGVAccountsPNPT.GetDataKeyValue("CompanyCode").ToString

                        objICAPNPTypeColl = New ICAccountsPaymentNatureProductTypeCollection
                        objICAPNPTypeColl = ICAccountPaymentNatureProductTypeController.GetAllAccountPaymanetNatureProductTypeByProductTypeCodeForDelete(ProductTypeCode.ToString)


                        For Each objICAPNPType As ICAccountsPaymentNatureProductType In objICAPNPTypeColl
                            Try


                                If objICAPNPType.IsApproved = True Then
                                    FailToDeleteCount = FailToDeleteCount + 1
                                    Continue For
                                End If

                                If objICAPNPType.UpToICProductTypeByProductTypeCode.DisbursementMode = "Cheque" Or objICAPNPType.UpToICProductTypeByProductTypeCode.DisbursementMode = "DD" Or objICAPNPType.UpToICProductTypeByProductTypeCode.DisbursementMode = "PO" Then
                                    ICFilesController.DeleteICFilesByAPNPType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Both", "Both")
                                    ICAccountsPaymentNatureProductTypePrintLocationsController.DeleteAllAPNPTypePrintLocationsByAPNPType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                End If
                                ICAccountsPaymentNatureProductTypeTemplatesController.DeleteAllAPNPTypeFileUploadTemplates(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                DeletePackageChargesByAPNPType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                ICAccountPaymentNatureProductTypeController.DeleteAccountPaymentNatureProductType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                PassToDeleteCount = PassToDeleteCount + 1
                                Continue For
                            Catch ex As Exception
                                FailToDeleteCount = FailToDeleteCount + 1
                                Continue For
                            End Try

                        Next

                    End If
                Next

                If Not PassToDeleteCount = 0 And Not FailToDeleteCount = 0 Then
                    LoadgvAccountPaymentNatureProductType(True)
                    UIUtilities.ShowDialog(Me, "Delete Account Payment Nature Product Type", "[ " & PassToDeleteCount.ToString & " ] Account payment nature product type(s) deleted successfuly.<br /> [ " & FailToDeleteCount.ToString & " ] Account payment nature product type(s) can not be deleted due to following reasons:<br /> 1. Deleted associated record(s) first.<br /> 2. Record is Approved.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToDeleteCount = 0 And FailToDeleteCount = 0 Then
                    LoadgvAccountPaymentNatureProductType(True)
                    UIUtilities.ShowDialog(Me, "Delete Account Payment Nature Product Type", PassToDeleteCount.ToString & " Account payment nature product type(s) deleted successfuly.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToDeleteCount = 0 And PassToDeleteCount = 0 Then
                    LoadgvAccountPaymentNatureProductType(True)
                    UIUtilities.ShowDialog(Me, "Delete Account Payment Nature Product Type", "[ " & FailToDeleteCount.ToString & " ] Account payment nature product type(s) can not be deleted due to following reasons: <br /> 1. Deleted associated record(s) first.<br /> 2. Record is Approved.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Public Shared Function DeletePackageChargesByAPNPType(ByVal objICAPNPType As ICAccountsPaymentNatureProductType, ByVal UserID As String, ByVal UserName As String) As Boolean
        Dim objICPackageChargesColl As New ICPackageChargesCollection
        Dim StrAuditTrail As String = Nothing
        Dim Result As Boolean = False

        objICPackageChargesColl.Query.Where(objICPackageChargesColl.Query.AccountNumber = objICAPNPType.AccountNumber, objICPackageChargesColl.Query.BranchCode = objICAPNPType.BranchCode)
        objICPackageChargesColl.Query.Where(objICPackageChargesColl.Query.Currency = objICAPNPType.Currency And objICPackageChargesColl.Query.ProductTypeCode = objICAPNPType.ProductTypeCode)
        objICPackageChargesColl.Query.Where(objICPackageChargesColl.Query.PaymentNatureCode = objICAPNPType.PaymentNatureCode)
        objICPackageChargesColl.Query.OrderBy(objICPackageChargesColl.Query.ChargeId.Ascending)
        If objICPackageChargesColl.Query.Load Then
            For Each objICPackageCharge As ICPackageCharges In objICPackageChargesColl
                StrAuditTrail = Nothing
                StrAuditTrail = "Package Charge With ID : [ " & objICPackageCharge.ChargeId & " ] for account no [ " & objICPackageCharge.AccountNumber & " ]"
                StrAuditTrail += "Branch Code : [ " & objICPackageCharge.BranchCode & " ] for currency[ " & objICPackageCharge.Currency & " ]"
                StrAuditTrail += "Product Type Code : [ " & objICPackageCharge.ProductTypeCode & " ] for Payment nature code[ " & objICPackageCharge.PaymentNatureCode & " ]"
                StrAuditTrail += " status [ " & objICPackageCharge.ChargeAmount & " ] from date [ " & objICPackageCharge.ChargeFromDate & " ] To date [ " & objICPackageCharge.ChargeToDate & " ]"
                StrAuditTrail += " deleted"
                'objICPackageCharge.MarkAsDeleted()
                'objICPackageCharge.Save()
                ICUtilities.AddAuditTrail(StrAuditTrail, "Package Charges", objICPackageCharge.ChargeId.ToString, UserID, UserName, "DELETE")
            Next
            objICPackageChargesColl.MarkAllAsDeleted()
            objICPackageChargesColl.Save()
        End If
        Return True
    End Function
    Protected Sub gvAPNPType_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAPNPType.NeedDataSource
        Try
            LoadgvAccountPaymentNatureProductType(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnApproveAPNPType_Click(sender As Object, e As EventArgs) Handles btnApproveAPNPType.Click
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim PassToApproveCount As Integer = 0
                Dim FailToApproveCount As Integer = 0
                Dim objICAccountsPNPT As ICAccountsPaymentNatureProductType
                Dim AccountNumber, BranchCode, Currency, PaymentNatureCode, ProductTypeCode, CompanyCode As String



                If CheckGVCompanySelected() = False Then
                    UIUtilities.ShowDialog(Me, "Approve Accounts Payment Nature Product Type", "Please select atleast one Account Payment Nature Product Type.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVAccountsPNPT In gvAPNPType.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVAccountsPNPT.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        AccountNumber = ""
                        BranchCode = ""
                        Currency = ""
                        PaymentNatureCode = ""
                        ProductTypeCode = ""
                        CompanyCode = ""
                        CompanyCode = rowGVAccountsPNPT.GetDataKeyValue("CompanyCode").ToString
                        ProductTypeCode = rowGVAccountsPNPT.GetDataKeyValue("ProductTypeCode").ToString

                        Dim dt As DataTable

                        objICAccountsPNPT = New ICAccountsPaymentNatureProductType
                        dt = ICAccountPaymentNatureProductTypeController.GetAllAccountPaymanetNatureProductTypeByProductTypeCodeAndCompanyCode(ProductTypeCode.ToString, CompanyCode.ToString)

                        For Each dr As DataRow In dt.Rows



                            objICAccountsPNPT.LoadByPrimaryKey(dr("AccountNumber").ToString, dr("BranchCode").ToString, dr("Currency").ToString, dr("PaymentNatureCode").ToString, dr("productTypeCode").ToString)
                            If objICAccountsPNPT.IsApproved = True Then
                                FailToApproveCount = FailToApproveCount + 1
                                Continue For
                            Else
                                If Me.UserInfo.IsSuperUser = False Then
                                    If objICAccountsPNPT.CreateBy = Me.UserId Then
                                        FailToApproveCount = FailToApproveCount + 1
                                        Continue For
                                    Else
                                        ICAccountPaymentNatureProductTypeController.ApproveAccountPNPT(dr("AccountNumber").ToString, dr("BranchCode").ToString, dr("Currency").ToString, dr("PaymentNatureCode").ToString, dr("productTypeCode").ToString, Me.UserId, Me.UserInfo.Username, True)
                                        PassToApproveCount = PassToApproveCount + 1
                                    End If
                                Else
                                    ICAccountPaymentNatureProductTypeController.ApproveAccountPNPT(dr("AccountNumber").ToString, dr("BranchCode").ToString, dr("Currency").ToString, dr("PaymentNatureCode").ToString, dr("productTypeCode").ToString, Me.UserId, Me.UserInfo.Username, True)
                                    PassToApproveCount = PassToApproveCount + 1

                                End If
                            End If
                        Next

                    End If
                Next

                If Not PassToApproveCount = 0 And Not FailToApproveCount = 0 Then
                    '  LoadgvAccountPaymentNatureProductType(Me.gvAPNPType.CurrentPageIndex + 1, True, Me.gvAPNPType.PageSize)
                    LoadgvAccountPaymentNatureProductType(True)
                    UIUtilities.ShowDialog(Me, "Account, Payment Nature Product Type", "[ " & PassToApproveCount.ToString & " ] Account, Payment Nature Product Type Approved successfully.<br /> [ " & FailToApproveCount.ToString & " ] Account, Payment Nature Product Type can not be Approved due to following reasons:<br /> 1. Account, Payment Nature Product Type Must be approved by user Other than the maker<br /> 2. Account, Payment Nature Product Type is already approved", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToApproveCount = 0 And FailToApproveCount = 0 Then
                    'LoadgvAccountPaymentNatureProductType(Me.gvAPNPType.CurrentPageIndex + 1, True, Me.gvAPNPType.PageSize)
                    LoadgvAccountPaymentNatureProductType(True)

                    UIUtilities.ShowDialog(Me, "Account, Payment Nature Product Type", PassToApproveCount.ToString & " Account, Payment Nature Product Type approved successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToApproveCount = 0 And PassToApproveCount = 0 Then
                    ' LoadgvAccountPaymentNatureProductType(Me.gvAPNPType.CurrentPageIndex + 1, True, Me.gvAPNPType.PageSize)
                    LoadgvAccountPaymentNatureProductType(True)
                    UIUtilities.ShowDialog(Me, "Account, Payment Nature Product Type", "[ " & FailToApproveCount.ToString & " ] Account, Payment Nature Product Type can not be Approved due to following reasons: <br /> 1. Account,Payment Nature Product must be approved by user Other than the maker<br /> 2.Account,Payment Nature Product Type is already approved", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Private Function CheckGVCompanySelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvCompanyRow As GridDataItem In gvAPNPType.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvCompanyRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
End Class
