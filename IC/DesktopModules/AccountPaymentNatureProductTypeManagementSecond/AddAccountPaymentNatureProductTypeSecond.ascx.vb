﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_AccountPaymentNatureProductTypeManagement_AddAccountPaymentNatureProductTypeSecond
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase

    Private ProductTypeCode As String
    Private CompanyCode As String
    Private htRights As Hashtable
    Dim IsClientPrintReportAdded As Boolean = False
    Dim IsClientReIssuancePrintReportAdded As Boolean = False
    Dim IsBankPrintReportAdded As Boolean = False
    Dim IsBankReIssuancePrintReportAdded As Boolean = False
    Dim FileIDForClientPrintTemplate As Integer = 0
    Dim FileIDForClientReIssuancePrintTemplate As Integer = 0
    Dim FileIDForBankPrintTemplate As Integer = 0
    Dim FileIDForBankReIssuancePrintTemplate As Integer = 0


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            ProductTypeCode = Request.QueryString("ProductTypeCode").ToString()
            CompanyCode = Request.QueryString("CompanyCode").ToString()



            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlCompany()
                LoadddlAccountPaymentNature()
                LoadddlProductType()


                DirectCast(Me.Control.FindControl("trClientLocations"), HtmlTableRow).Style.Add("display", "none")
                DirectCast(Me.Control.FindControl("trBankLocations"), HtmlTableRow).Style.Add("display", "none")
                DirectCast(Me.Control.FindControl("trCOTCPrintTemplate"), HtmlTableRow).Style.Add("display", "none")
                chkListBankPrintLocations.Items.Clear()
                chkListClientLocations.Items.Clear()
                If ProductTypeCode.ToString() = "0" And CompanyCode.ToString = "0" Then

                    lblIsApproved.Visible = False
                    chkApproved.Visible = False
                    btnApprove.Visible = False

                    lblPageHeader.Text = "Add Account, Payment Nature & Product Type"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                    btnApprove.Visible = False


                Else
                    ' Edit Phase

                    'According to Rights it will be visible true or false
                    'lblIsApproved.Visible = CBool(htRights("Approve"))
                    'chkApproved.Visible = CBool(htRights("Approve"))
                    'btnApprove.Visible = CBool(htRights("Approve"))

                    chkApproved.Visible = CBool(htRights("Approve"))
                    lblPageHeader.Text = "Edit Account, Payment Nature & Product Type"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    Dim objICAPNPType As New ICAccountsPaymentNatureProductType
                    Dim objICAPNPTypeColl As New ICAccountsPaymentNatureProductTypeCollection
                    Dim objICGroup As New ICGroup
                    Dim objICCompant As New ICCompany
                    Dim objICAccounts As New ICAccounts
                    Dim objICAPNature As New ICAccountsPaymentNature
                    Dim dt As New DataTable
                    objICAPNPType.es.Connection.CommandTimeout = 3600
                    objICGroup.es.Connection.CommandTimeout = 3600
                    objICCompant.es.Connection.CommandTimeout = 3600
                    objICAccounts.es.Connection.CommandTimeout = 3600
                    objICAPNature.es.Connection.CommandTimeout = 3600
                    dt = ICAccountPaymentNatureProductTypeController.GetAllAccountPaymanetNatureProductTypeByProductTypeCodeAndCompanyCode(ProductTypeCode, CompanyCode)
                    objICAPNPType.LoadByPrimaryKey(dt.Rows(0)("AccountNumber").ToString, dt.Rows(0)("BranchCode").ToString, dt.Rows(0)("Currency").ToString, dt.Rows(0)("PaymentNatureCode").ToString, dt.Rows(0)("ProductTypeCode").ToString)
                    objICAPNature = objICAPNPType.UpToICAccountsPaymentNatureByAccountNumber
                    objICAccounts = objICAPNature.UpToICAccountsByAccountNumber
                    objICCompant = objICAccounts.UpToICCompanyByCompanyCode
                    ddlGroup.SelectedValue = objICCompant.GroupCode
                    LoadddlCompany()
                    ddlCompany.SelectedValue = objICCompant.CompanyCode
                    LoadddlAccountPaymentNature()


                    ddlProductType.SelectedValue = objICAPNPType.ProductTypeCode.ToString

                    If objICAPNPType.IsApproved = True Then

                        lblIsApproved.Visible = CBool(htRights("Approve"))
                        btnApprove.Visible = False
                        chkApproved.Checked = CBool(htRights("Approve"))
                        chkApproved.Enabled = False
                    Else

                        lblIsApproved.Visible = CBool(htRights("Approve"))
                        btnApprove.Visible = CBool(htRights("Approve"))
                        chkApproved.Visible = CBool(htRights("Approve"))
                        chkApproved.Checked = False
                        chkApproved.Enabled = CBool(htRights("Approve"))

                    End If







                    SetPrintLocationViADisbModeOfProduct()
                    CheckedMarkTaggedAPNPtypeAndTemplates(objICAPNPType)
                    radtvAccountsPaymentNatureTemplates.ExpandAllNodes()
                    CheckedMarkAssignedPrintLocations(objICAPNPType)
                    ddlGroup.Enabled = False
                    ddlCompany.Enabled = False
                    ddlProductType.Enabled = False


                    If objICAPNPType.PackageId.HasValue Then




                        Me.ddlPackage.SelectedValue = objICAPNPType.PackageId









                    End If


                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Account Payment Nature Product Type Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub CheckedMarkTaggedAPNPtypeAndTemplates(ByVal objICAPNPType As ICAccountsPaymentNatureProductType)
        Dim objICAPNPTypeTemplateColl As ICAccountsPaymentNatureProductTypeTemplatesCollection
        Dim objICAPNPtypeColl As New ICAccountsPaymentNatureProductTypeCollection


        objICAPNPtypeColl.es.Connection.CommandTimeout = 3600


        objICAPNPtypeColl = ICAccountPaymentNatureProductTypeController.GetAllAccountPaymanetNatureProductTypeByProductTypeCodeForDelete(objICAPNPType.ProductTypeCode.ToString)

        For Each radMainNode As RadTreeNode In radtvAccountsPaymentNatureTemplates.Nodes
            For Each radAPNNode As RadTreeNode In radMainNode.Nodes
                For Each objICAPNPTypeSecond As ICAccountsPaymentNatureProductType In objICAPNPtypeColl
                    If radAPNNode.Value.Split("-")(0) = objICAPNPTypeSecond.AccountNumber And radAPNNode.Value.Split("-")(1) = objICAPNPTypeSecond.BranchCode And radAPNNode.Value.Split("-")(2) = objICAPNPTypeSecond.Currency And radAPNNode.Value.Split("-")(3) = objICAPNPTypeSecond.PaymentNatureCode Then
                        radAPNNode.Checked = True
                        objICAPNPTypeTemplateColl = New ICAccountsPaymentNatureProductTypeTemplatesCollection
                        objICAPNPTypeTemplateColl.es.Connection.CommandTimeout = 3600
                        objICAPNPTypeTemplateColl = ICAccountsPaymentNatureProductTypeTemplatesController.GetAllAPNPTypeTemplatesByAPNPType(objICAPNPTypeSecond)
                        For Each radFUTNode As RadTreeNode In radAPNNode.Nodes
                            For Each objICAPNPTYpeTemplate As ICAccountsPaymentNatureProductTypeTemplates In objICAPNPTypeTemplateColl
                                If radFUTNode.Value = objICAPNPTYpeTemplate.TemplateID Then
                                    radFUTNode.Checked = True
                                End If
                            Next
                        Next
                    End If
                Next
            Next
        Next
    End Sub
    Private Sub CheckedMarkAssignedPrintLocations(ByVal objICAPNPType As ICAccountsPaymentNatureProductType)
        Dim objICAPNPTypePrintLocationColl As New ICAccountPaymentNatureProductTypeAndPrintLocationsCollection
        objICAPNPTypePrintLocationColl.es.Connection.CommandTimeout = 3600
        objICAPNPTypePrintLocationColl = ICAccountsPaymentNatureProductTypePrintLocationsController.GetAllAPNPTypePrintLocationsByAPNPType(objICAPNPType)
        If objICAPNPType.UpToICProductTypeByProductTypeCode.DisbursementMode = "Cheque" Then
            If objICAPNPTypePrintLocationColl.Count > 0 Then
                If chkListBankPrintLocations.Items.Count > 0 Then
                    For Each objICAPNPTypePrintLocation As ICAccountPaymentNatureProductTypeAndPrintLocations In objICAPNPTypePrintLocationColl
                        For Each lit As ListItem In chkListBankPrintLocations.Items
                            If lit.Value = objICAPNPTypePrintLocation.OfficeID Then
                                lit.Selected = True
                            End If
                        Next
                    Next
                End If
                If chkListClientLocations.Items.Count > 0 Then
                    For Each objICAPNPTypePrintLocation As ICAccountPaymentNatureProductTypeAndPrintLocations In objICAPNPTypePrintLocationColl
                        For Each lit As ListItem In chkListClientLocations.Items
                            If lit.Value = objICAPNPTypePrintLocation.OfficeID Then
                                lit.Selected = True
                            End If
                        Next
                    Next
                End If
            End If
        ElseIf objICAPNPType.UpToICProductTypeByProductTypeCode.DisbursementMode = "PO" Or objICAPNPType.UpToICProductTypeByProductTypeCode.DisbursementMode = "DD" Then
            If objICAPNPTypePrintLocationColl.Count > 0 Then
                If chkListBankPrintLocations.Items.Count > 0 Then
                    For Each objICAPNPTypePrintLocation As ICAccountPaymentNatureProductTypeAndPrintLocations In objICAPNPTypePrintLocationColl
                        For Each lit As ListItem In chkListBankPrintLocations.Items
                            If lit.Value = objICAPNPTypePrintLocation.OfficeID Then
                                lit.Selected = True
                            End If
                        Next
                    Next
                End If
            End If
        End If

    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString)
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccountPaymentNature()
        Try
            Dim dt As New DataTable
            Dim dtTemplates As DataTable
            Dim radAPNNode As RadTreeNode
            Dim radFUTemplateNode As RadTreeNode
            Dim objICAPNature As ICAccountsPaymentNature
            radtvAccountsPaymentNatureTemplates.Nodes.Clear()
            radMainTreeNode.Nodes.Clear()
            dt = ICAccountPaymentNatureController.GetAccountPaymentNatureByCompanyCode(ddlCompany.SelectedValue.ToString)
            For Each dr As DataRow In dt.Rows
                radAPNNode = New RadTreeNode
                radAPNNode.Text = dr("AccountAndPaymentNature")
                radAPNNode.Value = dr("AccountPaymentNature")
                dtTemplates = New DataTable
                objICAPNature = New ICAccountsPaymentNature
                objICAPNature.LoadByPrimaryKey(radAPNNode.Value.Split("-")(0), radAPNNode.Value.Split("-")(1), radAPNNode.Value.Split("-")(2), radAPNNode.Value.Split("-")(3))
                dtTemplates = ICAccountPaymentNatureFileUploadTemplateController.GetTemplatesByAccountPaymentNature(objICAPNature)
                For Each drTemplates As DataRow In dtTemplates.Rows
                    radFUTemplateNode = New RadTreeNode
                    radFUTemplateNode.Text = drTemplates("TemplateName")
                    radFUTemplateNode.Value = drTemplates("TemplateID")
                    radAPNNode.Nodes.Add(radFUTemplateNode)

                Next
                radMainTreeNode.Nodes.Add(radAPNNode)
            Next
            radtvAccountsPaymentNatureTemplates.Nodes.Add(radMainTreeNode)
            radtvAccountsPaymentNatureTemplates.CheckBoxes = True



        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckIsAPNAndTemplateIsSelected() As Boolean
        Dim Result As Boolean = False




        If radtvAccountsPaymentNatureTemplates.Nodes.Count = 0 Then
            Return Result
            Exit Function
        End If

        For Each radMainTreeNode As RadTreeNode In radtvAccountsPaymentNatureTemplates.Nodes
            If radMainTreeNode.Nodes.Count > 0 Then
                For Each radAPNNode As RadTreeNode In radMainTreeNode.Nodes


                    If radAPNNode.Checked = True Then
                        '' here if
                        If radAPNNode.Nodes.Count > 0 Then


                            For Each radFUTemplateNodes As RadTreeNode In radAPNNode.Nodes
                                If radFUTemplateNodes.Checked = True Then
                                    Result = True
                                    Exit For
                                End If
                            Next
                        Else
                            Result = False
                            Return Result
                            Exit Function
                        End If

                        If Result = False Then
                            Return Result
                            Exit Function
                        End If

                        ''here else

                    End If

                Next
            End If
        Next

        Return Result
    End Function
    Private Sub LoadddlProductType()
        Try
            Dim lit As ListItem
            lit = New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlProductType.Items.Clear()
            ddlProductType.Items.Add(lit)
            ddlProductType.AppendDataBoundItems = True
            ddlProductType.DataSource = ICProductTypeController.GetAllProductTypesActiveAndApprove()
            ddlProductType.DataTextField = "ProductTypeName"
            ddlProductType.DataValueField = "ProductTypeCode"
            ddlProductType.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        LoadddlCompany()
        LoadddlAccountPaymentNature()


    End Sub
    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Dim objICProductType As New ICProductType
        objICProductType.es.Connection.CommandTimeout = 3600
        If ddlCompany.SelectedValue.ToString <> "0" Then
            If ddlProductType.SelectedValue.ToString <> "0" Then
                objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)
                If objICProductType.DisbursementMode = "Cheque" Then
                    LoadClientPrintLocations()
                    DirectCast(Me.Control.FindControl("trClientLocations"), HtmlTableRow).Style.Remove("display")
                    'rfvPrintTemplateForClient.Enabled = True
                    'rfvReissuanceTempBankforClient.Enabled = True
                End If
            End If
        End If
        LoadddlAccountPaymentNature()



    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then

            Try
                Dim objICProductType As New ICProductType
                Dim objICAPNPType As New ICAccountsPaymentNatureProductType


                Dim DisbursementMode As String = Nothing
                objICAPNPType.es.Connection.CommandTimeout = 3600
                objICProductType.es.Connection.CommandTimeout = 3600


                objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)
                If CheckIsAPNAndTemplateIsSelected() = False Then
                    UIUtilities.ShowDialog(Me, "Error", "Please select at least one account payment nature and file upload template.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                If objICProductType.DisbursementMode = "Cheque" Or objICProductType.DisbursementMode = "PO" Or objICProductType.DisbursementMode = "DD" Then
                    If CheckIsPrintLocationSelectedAgainstProductType(objICProductType.DisbursementMode.ToString) = False Then
                        UIUtilities.ShowDialog(Me, "Error", "Please select at least one print location.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                objICAPNPType.ProductTypeCode = ddlProductType.SelectedValue.ToString


                If ProductTypeCode = "0" Then
                    If CheckPrintTemplateAndReissuanceTemplateSelected(objICAPNPType.UpToICProductTypeByProductTypeCode.DisbursementMode.ToString) = True Then
                        If objICAPNPType.UpToICProductTypeByProductTypeCode.DisbursementMode.ToString = "COTC" Then
                            UIUtilities.ShowDialog(Me, "Error", "Please select print template only in .trdx format.", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Please select at least one print template/re-issuance print template only in .trdx format.", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    End If
                End If


                If ProductTypeCode.ToString() = "0" Then

                    AddAPNAndFileUploadTemplate(objICProductType)
                    UIUtilities.ShowDialog(Me, "Account Payment Nature Product Type", "Account payment nature product type added successfully.", ICBO.IC.Dialogmessagetype.Success)



                    ''Remove company approval




                    'Dim objICCompany As New ICCompany
                    'Dim StrActionForCompany As String = Nothing
                    'objICCompany.es.Connection.CommandTimeout = 3600
                    'objICCompany.LoadByPrimaryKey(ddlCompany.SelectedValue.ToString)
                    'objICCompany.IsApprove = False
                    'objICCompany.CreatedBy = Me.UserId
                    'objICCompany.CreatedDate = Date.Now
                    'StrActionForCompany = Nothing
                    'StrActionForCompany += "Compnay [ " & objICCompany.CompanyName & " ] is unapproved on tagging account, payment nature and product type. Action was taken by user [ " & Me.UserId.ToString & " ] [ " & Me.UserInfo.Username.ToString & " ]"
                    'ICCompanyController.AddCompany(objICCompany, True, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrActionForCompany)


                    SetDefaultValuesForAddingTemplates()
                    Clear()
                Else
                    objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)
                    Dim dt As New DataTable
                    dt = ICAccountPaymentNatureProductTypeController.GetAllAccountPaymanetNatureProductTypeByProductTypeCodeAndCompanyCode(objICProductType.ProductTypeCode.ToString, ddlCompany.SelectedValue.ToString)



                    For Each dr As DataRow In dt.Rows
                        If objICAPNPType.LoadByPrimaryKey(dr("AccountNumber").ToString, dr("BranchCode").ToString, dr("Currency").ToString, dr("PaymentNatureCode").ToString, dr("ProductTypeCode").ToString) Then
                            If CheckPrintTemplateAndReissuanceTemplateSelectedOnUpdate(objICAPNPType) = False Then
                                If objICAPNPType.UpToICProductTypeByProductTypeCode.DisbursementMode = "COTC" Then
                                    UIUtilities.ShowDialog(Me, "Error", "Please select print template only in .trdx format.", ICBO.IC.Dialogmessagetype.Failure)

                                    Exit Sub
                                Else
                                    UIUtilities.ShowDialog(Me, "Error", "Please select at least one print template/re-issuance print template only in .trdx format.", ICBO.IC.Dialogmessagetype.Failure)

                                    Exit Sub
                                End If
                            End If
                            If fupClientPrintTemplate.HasFile = True Then
                                ICFilesController.DeleteICFilesByAPNPType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Client", "Print")

                            End If
                            If fupReissuanceTemplateClient.HasFile = True Then
                                ICFilesController.DeleteICFilesByAPNPType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Client", "Re-Issuance")

                            End If
                            If fupPrintTemplateBank.HasFile = True Then
                                ICFilesController.DeleteICFilesByAPNPType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Bank", "Print")

                            End If
                            If fupReIssuanceTemplateBank.HasFile = True Then
                                ICFilesController.DeleteICFilesByAPNPType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Bank", "Re-Issuance")
                            End If
                            If fupCOTCPrintTemplate.HasFile = True Then
                                ICFilesController.DeleteICFilesByAPNPType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Bank", "Print")
                            End If
                            ICAccountsPaymentNatureProductTypePrintLocationsController.DeleteAllAPNPTypePrintLocationsByAPNPType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            ICAccountsPaymentNatureProductTypeTemplatesController.DeleteAllAPNPTypeFileUploadTemplates(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            DeletePackageChargesByAPNPType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            ICAccountPaymentNatureProductTypeController.DeleteAccountPaymentNatureProductType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        End If
                    Next

                    AddAPNAndFileUploadTemplate(objICProductType)
                    UIUtilities.ShowDialog(Me, "Account Payment Nature Product Type", "Account payment nature product type updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL)
                    SetDefaultValuesForAddingTemplates()
                    Clear()
                End If

            Catch ex As Exception
                If ex.Message.Contains("Violation of PRIMARY KEY constraint") Then
                    UIUtilities.ShowDialog(Me, "Account Payment Nature Product Type", "Duplicate Account payment nature product type is not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Public Shared Function DeletePackageChargesByAPNPType(ByVal objICAPNPType As ICAccountsPaymentNatureProductType, ByVal UserID As String, ByVal UserName As String) As Boolean
        Dim objICPackageChargesColl As New ICPackageChargesCollection
        Dim StrAuditTrail As String = Nothing
        Dim Result As Boolean = False

        objICPackageChargesColl.Query.Where(objICPackageChargesColl.Query.AccountNumber = objICAPNPType.AccountNumber, objICPackageChargesColl.Query.BranchCode = objICAPNPType.BranchCode)
        objICPackageChargesColl.Query.Where(objICPackageChargesColl.Query.Currency = objICAPNPType.Currency And objICPackageChargesColl.Query.ProductTypeCode = objICAPNPType.ProductTypeCode)
        objICPackageChargesColl.Query.Where(objICPackageChargesColl.Query.PaymentNatureCode = objICAPNPType.PaymentNatureCode)
        objICPackageChargesColl.Query.OrderBy(objICPackageChargesColl.Query.ChargeId.Ascending)
        If objICPackageChargesColl.Query.Load Then
            For Each objICPackageCharge As ICPackageCharges In objICPackageChargesColl
                StrAuditTrail = Nothing
                StrAuditTrail = "Package Charge With ID : [ " & objICPackageCharge.ChargeId & " ] for account no [ " & objICPackageCharge.AccountNumber & " ]"
                StrAuditTrail += "Branch Code : [ " & objICPackageCharge.BranchCode & " ] for currency[ " & objICPackageCharge.Currency & " ]"
                StrAuditTrail += "Product Type Code : [ " & objICPackageCharge.ProductTypeCode & " ] for Payment nature code[ " & objICPackageCharge.PaymentNatureCode & " ]"
                StrAuditTrail += " status [ " & objICPackageCharge.ChargeAmount & " ] from date [ " & objICPackageCharge.ChargeFromDate & " ] To date [ " & objICPackageCharge.ChargeToDate & " ]"
                StrAuditTrail += " deleted"
                'objICPackageCharge.MarkAsDeleted()
                'objICPackageCharge.Save()
                ICUtilities.AddAuditTrail(StrAuditTrail, "Package Charges", objICPackageCharge.ChargeId.ToString, UserID, UserName, "DELETE")
            Next
            objICPackageChargesColl.MarkAllAsDeleted()
            objICPackageChargesColl.Save()
        End If
        Return True
    End Function
    Private Sub SetDefaultValuesForAddingTemplates()
        IsClientPrintReportAdded = False
        IsClientReIssuancePrintReportAdded = False
        IsBankPrintReportAdded = False
        IsBankReIssuancePrintReportAdded = False
        FileIDForClientPrintTemplate = 0
        FileIDForClientReIssuancePrintTemplate = 0
        FileIDForBankPrintTemplate = 0
        FileIDForBankReIssuancePrintTemplate = 0
    End Sub
    Private Sub DeletePrintTemplates(ByVal objICAPNPType As ICAccountsPaymentNatureProductType)
        If fupCOTCPrintTemplate.HasFile = True Then
            ICFilesController.DeleteICFilesByAPNPType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Bank", "Print")
        End If
        If fupPrintTemplateBank.HasFile = True Then
            ICFilesController.DeleteICFilesByAPNPType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Bank", "Print")
        End If
        If fupReIssuanceTemplateBank.HasFile = True Then
            ICFilesController.DeleteICFilesByAPNPType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Bank", "Re-Issuance")
        End If
        If fupClientPrintTemplate.HasFile = True Then
            ICFilesController.DeleteICFilesByAPNPType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Client", "Print")
        End If
        If fupReissuanceTemplateClient.HasFile = True Then
            ICFilesController.DeleteICFilesByAPNPType(objICAPNPType, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Client", "Re-Issuance")
        End If
    End Sub
    Private Function CheckPrintTemplateAndReissuanceTemplateSelected(ByVal DisbursementMode As String) As Boolean
        Dim Result As Boolean = False
        Dim PrintTemplateContentType As String = ""
        Dim ReIssuancePRintTemplate As String = ""
        If DisbursementMode = "Cheque" Then
            If chkListBankPrintLocations.Items.Count > 0 Then
                For Each lit As ListItem In chkListBankPrintLocations.Items
                    If lit.Selected = True Then
                        If fupPrintTemplateBank.HasFile = False Or fupReIssuanceTemplateBank.HasFile = False Then
                            Result = True
                            Return Result
                            Exit Function
                            '.trdx
                            'ElseIf Not fupPrintTemplateBank.PostedFile.ContentType = "text/xml" Or Not fupReIssuanceTemplateBank.PostedFile.ContentType = "text/xml" Then
                        ElseIf fupPrintTemplateBank.HasFile = True And fupReIssuanceTemplateBank.HasFile = True Then
                            PrintTemplateContentType = ""
                            ReIssuancePRintTemplate = ""
                            PrintTemplateContentType = System.IO.Path.GetExtension(fupPrintTemplateBank.PostedFile.FileName)
                            ReIssuancePRintTemplate = System.IO.Path.GetExtension(fupReIssuanceTemplateBank.PostedFile.FileName)
                            If Not PrintTemplateContentType = ".trdx" Or Not ReIssuancePRintTemplate = ".trdx" Then
                                Result = True
                                Return Result
                                Exit Function
                            End If

                        End If
                    End If

                Next
            End If
            If chkListClientLocations.Items.Count > 0 Then
                For Each lit As ListItem In chkListClientLocations.Items
                    If lit.Selected = True Then
                        If fupClientPrintTemplate.HasFile = False Or fupReissuanceTemplateClient.HasFile = False Then
                            Result = True
                            Return Result
                            Exit Function
                        ElseIf fupClientPrintTemplate.HasFile = True And fupReissuanceTemplateClient.HasFile = True Then
                            PrintTemplateContentType = ""
                            ReIssuancePRintTemplate = ""
                            PrintTemplateContentType = System.IO.Path.GetExtension(fupClientPrintTemplate.PostedFile.FileName)
                            ReIssuancePRintTemplate = System.IO.Path.GetExtension(fupReissuanceTemplateClient.PostedFile.FileName)
                            If Not PrintTemplateContentType = ".trdx" Or Not ReIssuancePRintTemplate = ".trdx" Then
                                Result = True
                                Return Result
                                Exit Function
                            End If

                        End If
                    End If
                Next
            End If
        ElseIf DisbursementMode = "PO" Or DisbursementMode = "DD" Then
            If chkListBankPrintLocations.Items.Count > 0 Then
                For Each lit As ListItem In chkListBankPrintLocations.Items
                    If lit.Selected = True Then
                        If fupPrintTemplateBank.HasFile = False Or fupReIssuanceTemplateBank.HasFile = False Then
                            Result = True
                            Return Result
                            Exit Function
                            '.trdx
                            'ElseIf Not fupPrintTemplateBank.PostedFile.ContentType = "text/xml" Or Not fupReIssuanceTemplateBank.PostedFile.ContentType = "text/xml" Then
                        ElseIf fupPrintTemplateBank.HasFile = True And fupReIssuanceTemplateBank.HasFile = True Then
                            PrintTemplateContentType = ""
                            ReIssuancePRintTemplate = ""
                            PrintTemplateContentType = System.IO.Path.GetExtension(fupPrintTemplateBank.PostedFile.FileName)
                            ReIssuancePRintTemplate = System.IO.Path.GetExtension(fupReIssuanceTemplateBank.PostedFile.FileName)
                            If Not PrintTemplateContentType = ".trdx" Or Not ReIssuancePRintTemplate = ".trdx" Then
                                Result = True
                                Return Result
                                Exit Function
                            End If
                        End If
                    End If
                Next
            End If
        ElseIf DisbursementMode = "COTC" Then
            If fupCOTCPrintTemplate.HasFile = False Then
                Result = True
                Return Result
                Exit Function
                '.trdx
                'ElseIf Not fupPrintTemplateBank.PostedFile.ContentType = "text/xml" Or Not fupReIssuanceTemplateBank.PostedFile.ContentType = "text/xml" Then
            ElseIf fupCOTCPrintTemplate.HasFile = True Then
                PrintTemplateContentType = ""
                ReIssuancePRintTemplate = ""
                PrintTemplateContentType = System.IO.Path.GetExtension(fupCOTCPrintTemplate.PostedFile.FileName)
                If Not PrintTemplateContentType = ".trdx" Then
                    Result = True
                    Return Result
                    Exit Function
                End If
            End If
        End If
        Return Result
    End Function
    Private Function CheckPrintTemplateAndReissuanceTemplateSelectedOnUpdate(ByVal objAPNPtype As ICAccountsPaymentNatureProductType) As Boolean
        Dim Result As Boolean = True
        Dim PrintTemplateContentType As String = ""
        Dim ReIssuancePRintTemplate As String = ""

        If objAPNPtype.UpToICProductTypeByProductTypeCode.DisbursementMode = "Cheque" Then
            If fupPrintTemplateBank.HasFile = True And fupReIssuanceTemplateBank.HasFile = True Then
                PrintTemplateContentType = ""
                ReIssuancePRintTemplate = ""
                PrintTemplateContentType = System.IO.Path.GetExtension(fupPrintTemplateBank.PostedFile.FileName)
                ReIssuancePRintTemplate = System.IO.Path.GetExtension(fupReIssuanceTemplateBank.PostedFile.FileName)
                If Not PrintTemplateContentType = ".trdx" Or Not ReIssuancePRintTemplate = ".trdx" Then
                    Result = False
                    Return Result
                    Exit Function
                End If
            ElseIf fupPrintTemplateBank.HasFile = False And fupReIssuanceTemplateBank.HasFile = True Then
                PrintTemplateContentType = ""
                ReIssuancePRintTemplate = ""
                ReIssuancePRintTemplate = System.IO.Path.GetExtension(fupReIssuanceTemplateBank.PostedFile.FileName)
                If Not ReIssuancePRintTemplate = ".trdx" Then
                    Result = False
                    Return Result
                    Exit Function
                End If
            ElseIf fupPrintTemplateBank.HasFile = True And fupReIssuanceTemplateBank.HasFile = False Then
                PrintTemplateContentType = ""
                ReIssuancePRintTemplate = ""
                PrintTemplateContentType = System.IO.Path.GetExtension(fupPrintTemplateBank.PostedFile.FileName)
                If Not PrintTemplateContentType = ".trdx" Then
                    Result = False
                    Return Result
                    Exit Function
                End If
            End If
            If fupClientPrintTemplate.HasFile = True And fupReissuanceTemplateClient.HasFile = True Then
                PrintTemplateContentType = ""
                ReIssuancePRintTemplate = ""
                PrintTemplateContentType = System.IO.Path.GetExtension(fupClientPrintTemplate.PostedFile.FileName)
                ReIssuancePRintTemplate = System.IO.Path.GetExtension(fupReissuanceTemplateClient.PostedFile.FileName)
                If Not PrintTemplateContentType = ".trdx" Or Not ReIssuancePRintTemplate = ".trdx" Then
                    Result = False
                    Return Result
                    Exit Function
                End If
            ElseIf fupClientPrintTemplate.HasFile = True And fupReissuanceTemplateClient.HasFile = False Then
                PrintTemplateContentType = ""
                ReIssuancePRintTemplate = ""
                PrintTemplateContentType = System.IO.Path.GetExtension(fupClientPrintTemplate.PostedFile.FileName)
                If Not PrintTemplateContentType = ".trdx" Then
                    Result = False
                    Return Result
                    Exit Function
                End If
            ElseIf fupClientPrintTemplate.HasFile = False And fupReissuanceTemplateClient.HasFile = True Then
                PrintTemplateContentType = ""
                ReIssuancePRintTemplate = ""

                ReIssuancePRintTemplate = System.IO.Path.GetExtension(fupReissuanceTemplateClient.PostedFile.FileName)
                If Not PrintTemplateContentType = ".trdx" Or Not ReIssuancePRintTemplate = ".trdx" Then
                    Result = False
                    Return Result
                    Exit Function
                End If

            End If




        ElseIf objAPNPtype.UpToICProductTypeByProductTypeCode.DisbursementMode = "PO" Or objAPNPtype.UpToICProductTypeByProductTypeCode.DisbursementMode = "DD" Then
            If fupPrintTemplateBank.HasFile = True And fupReIssuanceTemplateBank.HasFile = True Then
                PrintTemplateContentType = ""
                ReIssuancePRintTemplate = ""
                PrintTemplateContentType = System.IO.Path.GetExtension(fupPrintTemplateBank.PostedFile.FileName)
                ReIssuancePRintTemplate = System.IO.Path.GetExtension(fupReIssuanceTemplateBank.PostedFile.FileName)
                If Not PrintTemplateContentType = ".trdx" Or Not ReIssuancePRintTemplate = ".trdx" Then
                    Result = False
                    Return Result
                    Exit Function
                End If
            ElseIf fupPrintTemplateBank.HasFile = False And fupReIssuanceTemplateBank.HasFile = True Then
                PrintTemplateContentType = ""
                ReIssuancePRintTemplate = ""
                ReIssuancePRintTemplate = System.IO.Path.GetExtension(fupReIssuanceTemplateBank.PostedFile.FileName)
                If Not ReIssuancePRintTemplate = ".trdx" Then
                    Result = False
                    Return Result
                    Exit Function
                End If
            ElseIf fupPrintTemplateBank.HasFile = True And fupReIssuanceTemplateBank.HasFile = False Then
                PrintTemplateContentType = ""
                ReIssuancePRintTemplate = ""
                PrintTemplateContentType = System.IO.Path.GetExtension(fupPrintTemplateBank.PostedFile.FileName)
                If Not PrintTemplateContentType = ".trdx" Then
                    Result = False
                    Return Result
                    Exit Function
                End If
            End If
        ElseIf objAPNPtype.UpToICProductTypeByProductTypeCode.DisbursementMode = "COTC" Then
            If fupCOTCPrintTemplate.HasFile = True Then
                PrintTemplateContentType = ""
                PrintTemplateContentType = System.IO.Path.GetExtension(fupCOTCPrintTemplate.PostedFile.FileName)
                If Not PrintTemplateContentType = ".trdx" Then
                    Result = False
                    Return Result
                    Exit Function
                End If
            End If
        End If
        Return Result
    End Function
    Private Sub AddPrintLocationsofAPNPTypeAndPrintTemplates(ByVal objICAPNPType As ICAccountsPaymentNatureProductType)
        Dim objICAPNPTypePrintLocation As ICAccountPaymentNatureProductTypeAndPrintLocations
        Dim objICAPNPTypeSecond As ICAccountsPaymentNatureProductType
        Dim CountBankPrintLocations As Integer = 0
        Dim CountClientPrintLocations As Integer = 0
        Dim ActionString As String = Nothing
        If objICAPNPType.UpToICProductTypeByProductTypeCode.DisbursementMode = "COTC" Then
            If fupCOTCPrintTemplate.HasFile = True Then

                ActionString = "Account payment nature product type print template [ " & fupPrintTemplateBank.FileName & " ] for bank print locations added for account number [ " & objICAPNPType.AccountNumber & " ], payment nature [ " & objICAPNPType.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] and product type [ " & objICAPNPType.UpToICProductTypeByProductTypeCode.ProductTypeName & " ]."
                If IsBankPrintReportAdded = False Then
                    Dim FileID As Integer = 0
                    FileID = ICUtilities.UploadFiletoDB(fupCOTCPrintTemplate, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, fupCOTCPrintTemplate.PostedFile.FileName.ToString, Me.UserId, Me.UserInfo.Username.ToString, ActionString.ToString, "Account Payment Nature Product Type Print Template")
                    IsBankPrintReportAdded = True
                    FileIDForBankPrintTemplate = FileID
                Else
                    ICUtilities.AddAuditTrail(ActionString, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")
                End If
                If FileIDForBankPrintTemplate > 0 Then
                    objICAPNPTypeSecond = New ICAccountsPaymentNatureProductType
                    objICAPNPTypeSecond.es.Connection.CommandTimeout = 3600
                    objICAPNPTypeSecond.LoadByPrimaryKey(objICAPNPType.AccountNumber, objICAPNPType.BranchCode, objICAPNPType.Currency, objICAPNPType.PaymentNatureCode, objICAPNPType.ProductTypeCode)
                    objICAPNPTypeSecond.PrintTemplateFileIDForBank = FileIDForBankPrintTemplate
                    objICAPNPTypeSecond.Save()
                End If
            Else
                ActionString = "Account payment nature product type print template [ " & fupPrintTemplateBank.FileName & " ] for bank print locations added for account number [ " & objICAPNPType.AccountNumber & " ], payment nature [ " & objICAPNPType.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] and product type [ " & objICAPNPType.UpToICProductTypeByProductTypeCode.ProductTypeName & " ]."

                If CInt(ViewState("FileIDForBankPrintTemplate")) > 0 Then
                    objICAPNPTypeSecond = New ICAccountsPaymentNatureProductType
                    objICAPNPTypeSecond.es.Connection.CommandTimeout = 3600
                    objICAPNPTypeSecond.LoadByPrimaryKey(objICAPNPType.AccountNumber, objICAPNPType.BranchCode, objICAPNPType.Currency, objICAPNPType.PaymentNatureCode, objICAPNPType.ProductTypeCode)
                    objICAPNPTypeSecond.PrintTemplateFileIDForBank = ViewState("FileIDForBankPrintTemplate")
                    objICAPNPTypeSecond.Save()
                End If
                ICUtilities.AddAuditTrail(ActionString, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")
            End If
        ElseIf objICAPNPType.UpToICProductTypeByProductTypeCode.DisbursementMode = "Cheque" Then
            For Each lit As ListItem In chkListBankPrintLocations.Items
                If lit.Selected = True Then
                    objICAPNPTypePrintLocation = New ICAccountPaymentNatureProductTypeAndPrintLocations
                    objICAPNPTypePrintLocation.AccountNumber = objICAPNPType.AccountNumber
                    objICAPNPTypePrintLocation.BranchCode = objICAPNPType.BranchCode
                    objICAPNPTypePrintLocation.Currency = objICAPNPType.Currency
                    objICAPNPTypePrintLocation.PaymentNatureCode = objICAPNPType.PaymentNatureCode
                    objICAPNPTypePrintLocation.ProductTypeCode = objICAPNPType.ProductTypeCode
                    objICAPNPTypePrintLocation.OfficeID = lit.Value
                    objICAPNPTypePrintLocation.CreatedBy = Me.UserId
                    objICAPNPTypePrintLocation.CreatedOn = Date.Now
                    objICAPNPTypePrintLocation.Creater = Me.UserId
                    objICAPNPTypePrintLocation.CreationDate = Date.Now
                    ICAccountsPaymentNatureProductTypePrintLocationsController.AddAPNPTypePrintLocations(objICAPNPTypePrintLocation, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    CountBankPrintLocations = CountBankPrintLocations + 1
                End If
            Next

            If CountBankPrintLocations > 0 Then
                If fupPrintTemplateBank.HasFile = True Then

                    ActionString = "Account payment nature product type print template [ " & fupPrintTemplateBank.FileName & " ] for bank print locations added for account number [ " & objICAPNPType.AccountNumber & " ], payment nature [ " & objICAPNPType.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] and product type [ " & objICAPNPType.UpToICProductTypeByProductTypeCode.ProductTypeName & " ]."
                    If IsBankPrintReportAdded = False Then
                        Dim FileID As Integer = 0
                        FileID = ICUtilities.UploadFiletoDB(fupPrintTemplateBank, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, fupPrintTemplateBank.PostedFile.FileName.ToString, Me.UserId, Me.UserInfo.Username.ToString, ActionString.ToString, "Account Payment Nature Product Type Print Template")
                        IsBankPrintReportAdded = True
                        FileIDForBankPrintTemplate = FileID
                    Else
                        ICUtilities.AddAuditTrail(ActionString, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")
                    End If
                    If FileIDForBankPrintTemplate > 0 Then
                        objICAPNPTypeSecond = New ICAccountsPaymentNatureProductType
                        objICAPNPTypeSecond.es.Connection.CommandTimeout = 3600
                        objICAPNPTypeSecond.LoadByPrimaryKey(objICAPNPType.AccountNumber, objICAPNPType.BranchCode, objICAPNPType.Currency, objICAPNPType.PaymentNatureCode, objICAPNPType.ProductTypeCode)
                        objICAPNPTypeSecond.PrintTemplateFileIDForBank = FileIDForBankPrintTemplate
                        objICAPNPTypeSecond.Save()
                    End If
                Else
                    ActionString = "Account payment nature product type print template [ " & fupPrintTemplateBank.FileName & " ] for bank print locations added for account number [ " & objICAPNPType.AccountNumber & " ], payment nature [ " & objICAPNPType.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] and product type [ " & objICAPNPType.UpToICProductTypeByProductTypeCode.ProductTypeName & " ]."

                    If CInt(ViewState("FileIDForBankPrintTemplate")) > 0 Then
                        objICAPNPTypeSecond = New ICAccountsPaymentNatureProductType
                        objICAPNPTypeSecond.es.Connection.CommandTimeout = 3600
                        objICAPNPTypeSecond.LoadByPrimaryKey(objICAPNPType.AccountNumber, objICAPNPType.BranchCode, objICAPNPType.Currency, objICAPNPType.PaymentNatureCode, objICAPNPType.ProductTypeCode)
                        objICAPNPTypeSecond.PrintTemplateFileIDForBank = ViewState("FileIDForBankPrintTemplate")
                        objICAPNPTypeSecond.Save()
                    End If
                    ICUtilities.AddAuditTrail(ActionString, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")
                End If
                If fupReIssuanceTemplateBank.HasFile = True Then

                    ActionString = Nothing
                    ActionString = "Account payment nature product type re-issuance print template [ " & fupReIssuanceTemplateBank.FileName & " ] for bank print locations added for account number [ " & objICAPNPType.AccountNumber & " ], payment nature [ " & objICAPNPType.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] and product type [ " & objICAPNPType.UpToICProductTypeByProductTypeCode.ProductTypeName & " ]."


                    If IsBankReIssuancePrintReportAdded = False Then
                        Dim ReIssuanceFileID As Integer = 0
                        ReIssuanceFileID = ICUtilities.UploadFiletoDB(fupReIssuanceTemplateBank, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, fupReIssuanceTemplateBank.PostedFile.FileName.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, ActionString.ToString, "Account Payment Nature Product Type Re-Issuance Print Template")
                        IsBankReIssuancePrintReportAdded = True
                        FileIDForBankReIssuancePrintTemplate = ReIssuanceFileID
                    Else
                        ICUtilities.AddAuditTrail(ActionString, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")
                    End If


                    If FileIDForBankReIssuancePrintTemplate > 0 Then
                        objICAPNPTypeSecond = New ICAccountsPaymentNatureProductType
                        objICAPNPTypeSecond.es.Connection.CommandTimeout = 3600
                        objICAPNPTypeSecond.LoadByPrimaryKey(objICAPNPType.AccountNumber, objICAPNPType.BranchCode, objICAPNPType.Currency, objICAPNPType.PaymentNatureCode, objICAPNPType.ProductTypeCode)
                        objICAPNPTypeSecond.ReIssuanceTemplateFileIDForBank = FileIDForBankReIssuancePrintTemplate
                        objICAPNPTypeSecond.Save()
                    End If
                Else



                    ActionString = Nothing
                    ActionString = "Account payment nature product type re-issuance print template [ " & fupReIssuanceTemplateBank.FileName & " ] for bank print locations added for account number [ " & objICAPNPType.AccountNumber & " ], payment nature [ " & objICAPNPType.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] and product type [ " & objICAPNPType.UpToICProductTypeByProductTypeCode.ProductTypeName & " ]."
                    If CInt(ViewState("FileIDForBankReIssuancePrintTemplate")) > 0 Then
                        objICAPNPTypeSecond = New ICAccountsPaymentNatureProductType
                        objICAPNPTypeSecond.es.Connection.CommandTimeout = 3600
                        objICAPNPTypeSecond.LoadByPrimaryKey(objICAPNPType.AccountNumber, objICAPNPType.BranchCode, objICAPNPType.Currency, objICAPNPType.PaymentNatureCode, objICAPNPType.ProductTypeCode)
                        objICAPNPTypeSecond.ReIssuanceTemplateFileIDForBank = CInt(ViewState("FileIDForBankReIssuancePrintTemplate"))
                        objICAPNPTypeSecond.Save()
                    End If
                    ICUtilities.AddAuditTrail(ActionString, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")

                End If
            End If


            For Each lit As ListItem In chkListClientLocations.Items
                If lit.Selected = True Then
                    objICAPNPTypePrintLocation = New ICAccountPaymentNatureProductTypeAndPrintLocations
                    objICAPNPTypePrintLocation.AccountNumber = objICAPNPType.AccountNumber
                    objICAPNPTypePrintLocation.BranchCode = objICAPNPType.BranchCode
                    objICAPNPTypePrintLocation.Currency = objICAPNPType.Currency
                    objICAPNPTypePrintLocation.PaymentNatureCode = objICAPNPType.PaymentNatureCode
                    objICAPNPTypePrintLocation.ProductTypeCode = objICAPNPType.ProductTypeCode
                    objICAPNPTypePrintLocation.OfficeID = lit.Value
                    objICAPNPTypePrintLocation.CreatedBy = Me.UserId
                    objICAPNPTypePrintLocation.CreatedOn = Date.Now
                    ICAccountsPaymentNatureProductTypePrintLocationsController.AddAPNPTypePrintLocations(objICAPNPTypePrintLocation, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    CountClientPrintLocations = CountClientPrintLocations + 1
                End If
            Next
            If CountClientPrintLocations > 0 Then
                If fupClientPrintTemplate.HasFile = True Then


                    ActionString = Nothing
                    ActionString = "Account Payment Nature Product Type Print Template [ " & fupClientPrintTemplate.FileName & " ] for client print locations added against account number [ " & objICAPNPType.AccountNumber & " ], payment nature [ " & objICAPNPType.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] and product type [ " & objICAPNPType.UpToICProductTypeByProductTypeCode.ProductTypeName & " ]"
                    If IsClientPrintReportAdded = False Then

                        Dim FileID As Integer = 0
                        FileID = ICUtilities.UploadFiletoDB(fupClientPrintTemplate, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, fupClientPrintTemplate.PostedFile.FileName.ToString, Me.UserId, Me.UserInfo.Username.ToString, ActionString.ToString, "Account Payment Nature Product Type Print Template")
                        IsClientPrintReportAdded = True
                        FileIDForClientPrintTemplate = FileID
                    Else
                        ICUtilities.AddAuditTrail(ActionString, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")
                    End If

                    If FileIDForClientPrintTemplate > 0 Then
                        objICAPNPTypeSecond = New ICAccountsPaymentNatureProductType
                        objICAPNPTypeSecond.es.Connection.CommandTimeout = 3600
                        objICAPNPTypeSecond.LoadByPrimaryKey(objICAPNPType.AccountNumber, objICAPNPType.BranchCode, objICAPNPType.Currency, objICAPNPType.PaymentNatureCode, objICAPNPType.ProductTypeCode)
                        objICAPNPTypeSecond.PrintTemplateIDForClient = FileIDForClientPrintTemplate
                        objICAPNPTypeSecond.Save()
                    End If
                Else

                    ActionString = Nothing
                    ActionString = "Account Payment Nature Product Type Print Template [ " & fupClientPrintTemplate.FileName & " ] for client print locations added against account number [ " & objICAPNPType.AccountNumber & " ], payment nature [ " & objICAPNPType.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] and product type [ " & objICAPNPType.UpToICProductTypeByProductTypeCode.ProductTypeName & " ]"
                    If CInt(ViewState("FileIDForClientPrintTemplate")) > 0 Then
                        objICAPNPTypeSecond = New ICAccountsPaymentNatureProductType
                        objICAPNPTypeSecond.es.Connection.CommandTimeout = 3600
                        objICAPNPTypeSecond.LoadByPrimaryKey(objICAPNPType.AccountNumber, objICAPNPType.BranchCode, objICAPNPType.Currency, objICAPNPType.PaymentNatureCode, objICAPNPType.ProductTypeCode)
                        objICAPNPTypeSecond.PrintTemplateIDForClient = CInt(ViewState("FileIDForClientPrintTemplate"))
                        objICAPNPTypeSecond.Save()
                    End If
                    ICUtilities.AddAuditTrail(ActionString, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")
                End If
                If fupReissuanceTemplateClient.HasFile = True Then


                    ActionString = Nothing
                    ActionString = "Account payment nature product type reissuance print template [ " & fupReissuanceTemplateClient.FileName & " ] for client print locations added for account number [ " & objICAPNPType.AccountNumber & " ], payment nature [ " & objICAPNPType.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] and product type [ " & objICAPNPType.UpToICProductTypeByProductTypeCode.ProductTypeName & " ]."
                    If IsClientReIssuancePrintReportAdded = False Then
                        Dim ReIssuanceFileID As Integer = 0
                        ReIssuanceFileID = ICUtilities.UploadFiletoDB(fupReissuanceTemplateClient, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, fupReissuanceTemplateClient.PostedFile.FileName.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, ActionString.ToString, "Account Payment Nature Product Type Re-Issuance Print Template")
                        IsClientReIssuancePrintReportAdded = True
                        FileIDForClientReIssuancePrintTemplate = ReIssuanceFileID
                    Else

                        ICUtilities.AddAuditTrail(ActionString, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")

                    End If

                    If FileIDForClientReIssuancePrintTemplate > 0 Then
                        objICAPNPTypeSecond = New ICAccountsPaymentNatureProductType
                        objICAPNPTypeSecond.es.Connection.CommandTimeout = 3600
                        objICAPNPTypeSecond.LoadByPrimaryKey(objICAPNPType.AccountNumber, objICAPNPType.BranchCode, objICAPNPType.Currency, objICAPNPType.PaymentNatureCode, objICAPNPType.ProductTypeCode)
                        objICAPNPTypeSecond.ReIssuancePrintTemplateIDForClient = FileIDForClientReIssuancePrintTemplate
                        objICAPNPTypeSecond.Save()
                    End If
                Else



                    ActionString = Nothing
                    ActionString = "Account payment nature product type reissuance print template [ " & fupReissuanceTemplateClient.FileName & " ] for client print locations added for account number [ " & objICAPNPType.AccountNumber & " ], payment nature [ " & objICAPNPType.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] and product type [ " & objICAPNPType.UpToICProductTypeByProductTypeCode.ProductTypeName & " ]."
                    If CInt(ViewState("FileIDForClientReIssuancePrintTemplate")) > 0 Then
                        objICAPNPTypeSecond = New ICAccountsPaymentNatureProductType
                        objICAPNPTypeSecond.es.Connection.CommandTimeout = 3600
                        objICAPNPTypeSecond.LoadByPrimaryKey(objICAPNPType.AccountNumber, objICAPNPType.BranchCode, objICAPNPType.Currency, objICAPNPType.PaymentNatureCode, objICAPNPType.ProductTypeCode)
                        objICAPNPTypeSecond.ReIssuancePrintTemplateIDForClient = CInt(ViewState("FileIDForClientReIssuancePrintTemplate"))
                        objICAPNPTypeSecond.Save()
                    End If
                    ICUtilities.AddAuditTrail(ActionString, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")
                End If
            End If

        ElseIf objICAPNPType.UpToICProductTypeByProductTypeCode.DisbursementMode = "PO" Or objICAPNPType.UpToICProductTypeByProductTypeCode.DisbursementMode = "DD" Then
            For Each lit As ListItem In chkListBankPrintLocations.Items
                If lit.Selected = True Then
                    objICAPNPTypePrintLocation = New ICAccountPaymentNatureProductTypeAndPrintLocations
                    objICAPNPTypePrintLocation.AccountNumber = objICAPNPType.AccountNumber
                    objICAPNPTypePrintLocation.BranchCode = objICAPNPType.BranchCode
                    objICAPNPTypePrintLocation.Currency = objICAPNPType.Currency
                    objICAPNPTypePrintLocation.PaymentNatureCode = objICAPNPType.PaymentNatureCode
                    objICAPNPTypePrintLocation.ProductTypeCode = objICAPNPType.ProductTypeCode
                    objICAPNPTypePrintLocation.OfficeID = lit.Value
                    objICAPNPTypePrintLocation.CreatedBy = Me.UserId
                    objICAPNPTypePrintLocation.CreatedOn = Date.Now
                    objICAPNPTypePrintLocation.Creater = Me.UserId
                    objICAPNPTypePrintLocation.CreationDate = Date.Now
                    ICAccountsPaymentNatureProductTypePrintLocationsController.AddAPNPTypePrintLocations(objICAPNPTypePrintLocation, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    CountBankPrintLocations = CountBankPrintLocations + 1
                End If
            Next
            If CountBankPrintLocations > 0 Then
                If fupPrintTemplateBank.HasFile = True Then

                    ActionString = "Account payment nature product type print template [ " & fupPrintTemplateBank.FileName & " ] for bank print locations added for account number [ " & objICAPNPType.AccountNumber & " ], payment nature [ " & objICAPNPType.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] and product type [ " & objICAPNPType.UpToICProductTypeByProductTypeCode.ProductTypeName & " ]."
                    If IsBankPrintReportAdded = False Then
                        Dim FileID As Integer = 0
                        FileID = ICUtilities.UploadFiletoDB(fupPrintTemplateBank, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, fupPrintTemplateBank.PostedFile.FileName.ToString, Me.UserId, Me.UserInfo.Username.ToString, ActionString.ToString, "Account Payment Nature Product Type Print Template")
                        IsBankPrintReportAdded = True
                        FileIDForBankPrintTemplate = FileID
                    Else
                        ICUtilities.AddAuditTrail(ActionString, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")
                    End If
                    If FileIDForBankPrintTemplate > 0 Then
                        objICAPNPTypeSecond = New ICAccountsPaymentNatureProductType
                        objICAPNPTypeSecond.es.Connection.CommandTimeout = 3600
                        objICAPNPTypeSecond.LoadByPrimaryKey(objICAPNPType.AccountNumber, objICAPNPType.BranchCode, objICAPNPType.Currency, objICAPNPType.PaymentNatureCode, objICAPNPType.ProductTypeCode)
                        objICAPNPTypeSecond.PrintTemplateFileIDForBank = FileIDForBankPrintTemplate
                        objICAPNPTypeSecond.Save()
                    End If
                Else

                    ActionString = "Account payment nature product type print template [ " & fupPrintTemplateBank.FileName & " ] for bank print locations added for account number [ " & objICAPNPType.AccountNumber & " ], payment nature [ " & objICAPNPType.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] and product type [ " & objICAPNPType.UpToICProductTypeByProductTypeCode.ProductTypeName & " ]."
                    If CInt(ViewState("FileIDForBankPrintTemplate")) > 0 Then
                        objICAPNPTypeSecond = New ICAccountsPaymentNatureProductType
                        objICAPNPTypeSecond.es.Connection.CommandTimeout = 3600
                        objICAPNPTypeSecond.LoadByPrimaryKey(objICAPNPType.AccountNumber, objICAPNPType.BranchCode, objICAPNPType.Currency, objICAPNPType.PaymentNatureCode, objICAPNPType.ProductTypeCode)
                        objICAPNPTypeSecond.PrintTemplateFileIDForBank = CInt(ViewState("FileIDForBankPrintTemplate"))
                        objICAPNPTypeSecond.Save()
                    End If
                    ICUtilities.AddAuditTrail(ActionString, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")
                End If
                If fupReIssuanceTemplateBank.HasFile = True Then


                    ActionString = Nothing
                    ActionString = "Account payment nature product type re-issuance print template [ " & fupReIssuanceTemplateBank.FileName & " ] for bank print locations added for account number [ " & objICAPNPType.AccountNumber & " ], payment nature [ " & objICAPNPType.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] and product type [ " & objICAPNPType.UpToICProductTypeByProductTypeCode.ProductTypeName & " ]."


                    If IsBankReIssuancePrintReportAdded = False Then
                        Dim ReIssuanceFileID As Integer = 0
                        ReIssuanceFileID = ICUtilities.UploadFiletoDB(fupReIssuanceTemplateBank, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, fupReIssuanceTemplateBank.PostedFile.FileName.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, ActionString.ToString, "Account Payment Nature Product Type Re-Issuance Print Template")
                        IsBankReIssuancePrintReportAdded = True
                        FileIDForBankReIssuancePrintTemplate = ReIssuanceFileID
                    Else
                        ICUtilities.AddAuditTrail(ActionString, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")
                    End If
                    If FileIDForBankReIssuancePrintTemplate > 0 Then
                        objICAPNPTypeSecond = New ICAccountsPaymentNatureProductType
                        objICAPNPTypeSecond.es.Connection.CommandTimeout = 3600
                        objICAPNPTypeSecond.LoadByPrimaryKey(objICAPNPType.AccountNumber, objICAPNPType.BranchCode, objICAPNPType.Currency, objICAPNPType.PaymentNatureCode, objICAPNPType.ProductTypeCode)
                        objICAPNPTypeSecond.ReIssuanceTemplateFileIDForBank = FileIDForBankReIssuancePrintTemplate

                        objICAPNPTypeSecond.Save()
                    End If
                Else

                    ActionString = Nothing
                    ActionString = "Account payment nature product type re-issuance print template [ " & fupReIssuanceTemplateBank.FileName & " ] for bank print locations added for account number [ " & objICAPNPType.AccountNumber & " ], payment nature [ " & objICAPNPType.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] and product type [ " & objICAPNPType.UpToICProductTypeByProductTypeCode.ProductTypeName & " ]."
                    If CInt(ViewState("FileIDForBankReIssuancePrintTemplate")) > 0 Then
                        objICAPNPTypeSecond = New ICAccountsPaymentNatureProductType
                        objICAPNPTypeSecond.es.Connection.CommandTimeout = 3600
                        objICAPNPTypeSecond.LoadByPrimaryKey(objICAPNPType.AccountNumber, objICAPNPType.BranchCode, objICAPNPType.Currency, objICAPNPType.PaymentNatureCode, objICAPNPType.ProductTypeCode)
                        objICAPNPTypeSecond.ReIssuanceTemplateFileIDForBank = CInt(ViewState("FileIDForBankReIssuancePrintTemplate"))

                        objICAPNPTypeSecond.Save()
                    End If
                    ICUtilities.AddAuditTrail(ActionString, "Account Payment Nature Product Type", objICAPNPType.AccountNumber + "" + objICAPNPType.BranchCode + "" + objICAPNPType.Currency + "" + objICAPNPType.PaymentNatureCode + "" + objICAPNPType.ProductTypeCode, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")

                End If
            End If
        End If
        CountBankPrintLocations = 0
        CountClientPrintLocations = 0

    End Sub
    Private Sub AddAPNAndFileUploadTemplate(ByVal objICProductType As ICProductType)
        Dim objAPNPTypeTemplate As ICAccountsPaymentNatureProductTypeTemplates
        Dim objICAPNPTypeForSave As ICAccountsPaymentNatureProductType


        For Each radMainTreeNode As RadTreeNode In radtvAccountsPaymentNatureTemplates.Nodes
            For Each radAPNatureNode As RadTreeNode In radMainTreeNode.Nodes
                If radAPNatureNode.Checked = True Then
                    objICAPNPTypeForSave = New ICAccountsPaymentNatureProductType


                    ''If new
                    If objICAPNPTypeForSave.LoadByPrimaryKey(radAPNatureNode.Value.Split("-")(0), radAPNatureNode.Value.Split("-")(1), radAPNatureNode.Value.Split("-")(2), radAPNatureNode.Value.Split("-")(3), ddlProductType.SelectedValue.ToString) = False Then
                        objICAPNPTypeForSave.AccountNumber = radAPNatureNode.Value.Split("-")(0)
                        objICAPNPTypeForSave.BranchCode = radAPNatureNode.Value.Split("-")(1)
                        objICAPNPTypeForSave.Currency = radAPNatureNode.Value.Split("-")(2)
                        objICAPNPTypeForSave.PaymentNatureCode = radAPNatureNode.Value.Split("-")(3)
                        objICAPNPTypeForSave.CreateBy = Me.UserId
                        objICAPNPTypeForSave.CreateDate = Date.Now
                        objICAPNPTypeForSave.Creater = Me.UserId
                        objICAPNPTypeForSave.CreationDate = Date.Now
                        objICAPNPTypeForSave.ProductTypeCode = ddlProductType.SelectedValue.ToString


                        If Me.ddlPackage.SelectedIndex = 0 Then

                            objICAPNPTypeForSave.PackageId = Nothing


                        Else
                            objICAPNPTypeForSave.PackageId = Me.ddlPackage.SelectedValue



                        End If



                        ICAccountPaymentNatureProductTypeController.AddAccountPaymentNatureProductType(objICAPNPTypeForSave, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        For Each radFUTemplateNode As RadTreeNode In radAPNatureNode.Nodes
                            If radFUTemplateNode.Checked = True Then
                                objAPNPTypeTemplate = New ICAccountsPaymentNatureProductTypeTemplates
                                objAPNPTypeTemplate.es.Connection.CommandTimeout = 3600
                                objAPNPTypeTemplate.TemplateID = radFUTemplateNode.Value
                                objAPNPTypeTemplate.AccountNumber = objICAPNPTypeForSave.AccountNumber
                                objAPNPTypeTemplate.BranchCode = objICAPNPTypeForSave.BranchCode
                                objAPNPTypeTemplate.Currency = objICAPNPTypeForSave.Currency
                                objAPNPTypeTemplate.PaymentNatureCode = objICAPNPTypeForSave.PaymentNatureCode
                                objAPNPTypeTemplate.ProductCode = objICAPNPTypeForSave.ProductTypeCode
                                objAPNPTypeTemplate.CreatedBy = objICAPNPTypeForSave.CreateBy
                                objAPNPTypeTemplate.CreatedDate = objICAPNPTypeForSave.CreateDate
                                objAPNPTypeTemplate.Creater = objICAPNPTypeForSave.Creater
                                objAPNPTypeTemplate.CreationDate = objICAPNPTypeForSave.CreationDate


                                ICAccountsPaymentNatureProductTypeTemplatesController.AddAPNPTTemplate(objAPNPTypeTemplate, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            End If
                        Next
                        If objICProductType.DisbursementMode = "Cheque" Or objICProductType.DisbursementMode = "PO" Or objICProductType.DisbursementMode = "DD" Or objICProductType.DisbursementMode = "COTC" Then
                            AddPrintLocationsofAPNPTypeAndPrintTemplates(objICAPNPTypeForSave)
                        End If
                    Else

                        If objICProductType.DisbursementMode = "Cheque" Or objICProductType.DisbursementMode = "PO" Or objICProductType.DisbursementMode = "DD" Then
                            DeletePrintTemplates(objICAPNPTypeForSave)
                        End If

                        ICAccountsPaymentNatureProductTypePrintLocationsController.DeleteAllAPNPTypePrintLocationsByAPNPType(objICAPNPTypeForSave, Me.UserId.ToString, Me.UserInfo.Username.ToString)

                        ICAccountsPaymentNatureProductTypeTemplatesController.DeleteAllAPNPTypeFileUploadTemplates(objICAPNPTypeForSave, Me.UserId.ToString, Me.UserInfo.Username.ToString)

                        objICAPNPTypeForSave.CreateBy = Me.UserId
                        objICAPNPTypeForSave.CreateDate = Date.Now


                        If Me.ddlPackage.SelectedIndex = 0 Then

                            objICAPNPTypeForSave.PackageId = Nothing


                        Else
                            objICAPNPTypeForSave.PackageId = Me.ddlPackage.SelectedValue



                        End If





                        ICAccountPaymentNatureProductTypeController.AddAccountPaymentNatureProductType(objICAPNPTypeForSave, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        For Each radFUTemplateNode As RadTreeNode In radAPNatureNode.Nodes
                            If radFUTemplateNode.Checked = True Then
                                objAPNPTypeTemplate = New ICAccountsPaymentNatureProductTypeTemplates
                                objAPNPTypeTemplate.es.Connection.CommandTimeout = 3600
                                objAPNPTypeTemplate.TemplateID = radFUTemplateNode.Value
                                objAPNPTypeTemplate.AccountNumber = objICAPNPTypeForSave.AccountNumber
                                objAPNPTypeTemplate.BranchCode = objICAPNPTypeForSave.BranchCode
                                objAPNPTypeTemplate.Currency = objICAPNPTypeForSave.Currency
                                objAPNPTypeTemplate.PaymentNatureCode = objICAPNPTypeForSave.PaymentNatureCode
                                objAPNPTypeTemplate.ProductCode = objICAPNPTypeForSave.ProductTypeCode
                                objAPNPTypeTemplate.CreatedBy = objICAPNPTypeForSave.CreateBy
                                objAPNPTypeTemplate.CreatedDate = objICAPNPTypeForSave.CreateDate
                                ICAccountsPaymentNatureProductTypeTemplatesController.AddAPNPTTemplate(objAPNPTypeTemplate, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            End If
                        Next
                        If objICProductType.DisbursementMode = "Cheque" Or objICProductType.DisbursementMode = "PO" Or objICProductType.DisbursementMode = "DD" Or objICProductType.DisbursementMode = "COTC" Then
                            AddPrintLocationsofAPNPTypeAndPrintTemplates(objICAPNPTypeForSave)
                        End If
                        'Dim objICCompany As New ICCompany
                        'Dim StrActionForCompany As String = Nothing
                        'objICCompany.es.Connection.CommandTimeout = 3600
                        'objICCompany.LoadByPrimaryKey(ddlCompany.SelectedValue.ToString)
                        'objICCompany.IsApprove = False
                        'StrActionForCompany = Nothing
                        'StrActionForCompany += "Compnay [ " & objICCompany.CompanyName & " ] is unapproved on updating account, payment nature and product type. Action was taken by user [ " & Me.UserId.ToString & " ] [ " & Me.UserInfo.Username.ToString & " ]"
                        'ICCompanyController.AddCompany(objICCompany, True, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrActionForCompany)

                    End If



                End If
            Next
        Next

    End Sub
    Private Sub Clear()
        LoadddlGroup()
        LoadddlCompany()
        LoadddlAccountPaymentNature()
        LoadddlProductType()

        LoadBankPrintLocations()
        LoadClientPrintLocations()
        DirectCast(Me.Control.FindControl("trClientLocations"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBankLocations"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trCOTCPrintTemplate"), HtmlTableRow).Style.Add("display", "none")
        chkListBankPrintLocations.Items.Clear()
        chkListClientLocations.Items.Clear()

    End Sub
    Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged
        Try
            SetPrintLocationViADisbModeOfProduct()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetPrintLocationViADisbModeOfProduct()
        Dim DisbursementMode As String = ""
        Dim objICProduct As New ICProductType
        objICProduct.es.Connection.CommandTimeout = 3600
        hlDownloadPrintTemplateBank.Visible = False
        hlDownloadReIssuanceTemplateForBank.Visible = False
        hlDownloadClientPrintTemplate.Visible = False
        hlReissuanceTemplateClient.Visible = False
        hlDownloadPrintTemplateBank.NavigateUrl = ""
        hlDownloadReIssuanceTemplateForBank.NavigateUrl = ""
        hlDownloadClientPrintTemplate.NavigateUrl = ""
        hlReissuanceTemplateClient.NavigateUrl = ""
        hlCOTCPrintTemplate.Visible = False
        hlCOTCPrintTemplate.NavigateUrl = ""

        ViewState("FileIDForBankPrintTemplate") = Nothing
        ViewState("FileIDForBankReIssuancePrintTemplate") = Nothing
        ViewState("FileIDForClientPrintTemplate") = Nothing
        ViewState("FileIDForClientReIssuancePrintTemplate") = Nothing

        If ddlProductType.SelectedValue.ToString <> "0" Then
            objICProduct.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)
            DisbursementMode = objICProduct.DisbursementMode.ToString

            LoadPackages(DisbursementMode)




            If DisbursementMode.ToString = "" Then
                DirectCast(Me.Control.FindControl("trClientLocations"), HtmlTableRow).Style.Add("display", "none")
                DirectCast(Me.Control.FindControl("trBankLocations"), HtmlTableRow).Style.Add("display", "none")
                DirectCast(Me.Control.FindControl("trCOTCPrintTemplate"), HtmlTableRow).Style.Add("display", "none")
                chkListBankPrintLocations.Items.Clear()
                chkListClientLocations.Items.Clear()
            ElseIf DisbursementMode.ToString = "COTC" Then
                DirectCast(Me.Control.FindControl("trCOTCPrintTemplate"), HtmlTableRow).Style.Remove("display")
                DirectCast(Me.Control.FindControl("trClientLocations"), HtmlTableRow).Style.Add("display", "none")
                DirectCast(Me.Control.FindControl("trBankLocations"), HtmlTableRow).Style.Add("display", "none")
                If ProductTypeCode = "0" Then
                    hlDownloadPrintTemplateBank.Visible = False
                    hlDownloadReIssuanceTemplateForBank.Visible = False
                    hlDownloadClientPrintTemplate.Visible = False
                    hlReissuanceTemplateClient.Visible = False
                    hlDownloadPrintTemplateBank.NavigateUrl = ""
                    hlDownloadReIssuanceTemplateForBank.NavigateUrl = ""
                    hlDownloadClientPrintTemplate.NavigateUrl = ""
                    hlReissuanceTemplateClient.NavigateUrl = ""
                    hlCOTCPrintTemplate.Visible = False
                    hlCOTCPrintTemplate.NavigateUrl = ""
                Else
                    Dim dt As New DataTable
                    Dim objICAPNPType As New ICAccountsPaymentNatureProductType

                    dt = ICAccountPaymentNatureProductTypeController.GetAllAccountPaymanetNatureProductTypeByProductTypeCodeAndCompanyCode(ProductTypeCode, CompanyCode)
                    objICAPNPType.LoadByPrimaryKey(dt.Rows(0)("AccountNumber").ToString, dt.Rows(0)("BranchCode").ToString, dt.Rows(0)("Currency").ToString, dt.Rows(0)("PaymentNatureCode").ToString, dt.Rows(0)("ProductTypeCode").ToString)
                    Dim AppPath As String = "" ' = DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
                    hlCOTCPrintTemplate.Visible = True
                    ViewState("FileIDForBankPrintTemplate") = objICAPNPType.PrintTemplateFileIDForBank
                    hlCOTCPrintTemplate.NavigateUrl = AppPath & "/DownloadFile.aspx?fileid=" & objICAPNPType.PrintTemplateFileIDForBank.ToString.EncryptString()


                End If
            ElseIf DisbursementMode.ToString = "PO" Then
                DirectCast(Me.Control.FindControl("trClientLocations"), HtmlTableRow).Style.Add("display", "none")
                DirectCast(Me.Control.FindControl("trCOTCPrintTemplate"), HtmlTableRow).Style.Add("display", "none")
                DirectCast(Me.Control.FindControl("trBankLocations"), HtmlTableRow).Style.Remove("display")
                LoadBankPrintLocations()
                chkListClientLocations.Items.Clear()
                If ProductTypeCode = "0" Then
                    hlDownloadPrintTemplateBank.Visible = False
                    hlDownloadReIssuanceTemplateForBank.Visible = False
                    hlDownloadClientPrintTemplate.Visible = False
                    hlReissuanceTemplateClient.Visible = False
                    hlDownloadPrintTemplateBank.NavigateUrl = ""
                    hlDownloadReIssuanceTemplateForBank.NavigateUrl = ""
                    hlDownloadClientPrintTemplate.NavigateUrl = ""
                    hlReissuanceTemplateClient.NavigateUrl = ""
                    hlCOTCPrintTemplate.Visible = False
                    hlCOTCPrintTemplate.NavigateUrl = ""
                Else
                    Dim dt As New DataTable
                    Dim objICAPNPType As New ICAccountsPaymentNatureProductType

                    dt = ICAccountPaymentNatureProductTypeController.GetAllAccountPaymanetNatureProductTypeByProductTypeCodeAndCompanyCode(ProductTypeCode, CompanyCode)
                    objICAPNPType.LoadByPrimaryKey(dt.Rows(0)("AccountNumber").ToString, dt.Rows(0)("BranchCode").ToString, dt.Rows(0)("Currency").ToString, dt.Rows(0)("PaymentNatureCode").ToString, dt.Rows(0)("ProductTypeCode").ToString)
                    Dim AppPath As String = "" ' = DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
                    hlDownloadPrintTemplateBank.Visible = True
                    hlDownloadReIssuanceTemplateForBank.Visible = True

                    ViewState("FileIDForBankPrintTemplate") = objICAPNPType.PrintTemplateFileIDForBank
                    ViewState("FileIDForBankReIssuancePrintTemplate") = objICAPNPType.ReIssuanceTemplateFileIDForBank

                    hlDownloadPrintTemplateBank.NavigateUrl = AppPath & "/DownloadFile.aspx?fileid=" & objICAPNPType.PrintTemplateFileIDForBank.ToString.EncryptString()
                    hlDownloadReIssuanceTemplateForBank.NavigateUrl = AppPath & "/DownloadFile.aspx?fileid=" & objICAPNPType.ReIssuanceTemplateFileIDForBank.ToString.EncryptString()
                End If
            ElseIf DisbursementMode.ToString = "DD" Then
                DirectCast(Me.Control.FindControl("trClientLocations"), HtmlTableRow).Style.Add("display", "none")
                DirectCast(Me.Control.FindControl("trCOTCPrintTemplate"), HtmlTableRow).Style.Add("display", "none")
                DirectCast(Me.Control.FindControl("trBankLocations"), HtmlTableRow).Style.Remove("display")
                LoadBankPrintLocations()
                chkListClientLocations.Items.Clear()
                If ProductTypeCode = "0" Then
                    hlDownloadPrintTemplateBank.Visible = False
                    hlDownloadReIssuanceTemplateForBank.Visible = False
                    hlDownloadClientPrintTemplate.Visible = False
                    hlReissuanceTemplateClient.Visible = False
                    hlDownloadPrintTemplateBank.NavigateUrl = ""
                    hlDownloadReIssuanceTemplateForBank.NavigateUrl = ""
                    hlDownloadClientPrintTemplate.NavigateUrl = ""
                    hlReissuanceTemplateClient.NavigateUrl = ""
                    hlCOTCPrintTemplate.Visible = False
                    hlCOTCPrintTemplate.NavigateUrl = ""
                Else
                    Dim dt As New DataTable
                    Dim objICAPNPType As New ICAccountsPaymentNatureProductType

                    dt = ICAccountPaymentNatureProductTypeController.GetAllAccountPaymanetNatureProductTypeByProductTypeCodeAndCompanyCode(ProductTypeCode, CompanyCode)
                    objICAPNPType.LoadByPrimaryKey(dt.Rows(0)("AccountNumber").ToString, dt.Rows(0)("BranchCode").ToString, dt.Rows(0)("Currency").ToString, dt.Rows(0)("PaymentNatureCode").ToString, dt.Rows(0)("ProductTypeCode").ToString)
                    Dim AppPath As String = "" ' = DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
                    hlDownloadPrintTemplateBank.Visible = True
                    hlDownloadReIssuanceTemplateForBank.Visible = True
                    ViewState("FileIDForBankPrintTemplate") = objICAPNPType.PrintTemplateFileIDForBank
                    ViewState("FileIDForBankReIssuancePrintTemplate") = objICAPNPType.ReIssuanceTemplateFileIDForBank
                    hlDownloadPrintTemplateBank.NavigateUrl = AppPath & "/DownloadFile.aspx?fileid=" & objICAPNPType.PrintTemplateFileIDForBank.ToString.EncryptString()
                    hlDownloadReIssuanceTemplateForBank.NavigateUrl = AppPath & "/DownloadFile.aspx?fileid=" & objICAPNPType.ReIssuanceTemplateFileIDForBank.ToString.EncryptString()
                End If
            ElseIf DisbursementMode.ToString = "Cheque" Then
                DirectCast(Me.Control.FindControl("trBankLocations"), HtmlTableRow).Style.Remove("display")
                DirectCast(Me.Control.FindControl("trCOTCPrintTemplate"), HtmlTableRow).Style.Add("display", "none")
                LoadBankPrintLocations()
                If Not chkListBankPrintLocations.Items.Count > 0 Then
                    DirectCast(Me.Control.FindControl("trBankLocations"), HtmlTableRow).Style.Add("display", "none")
                End If
                If ProductTypeCode = "0" Then
                    hlDownloadPrintTemplateBank.Visible = False
                    hlDownloadReIssuanceTemplateForBank.Visible = False
                    hlDownloadClientPrintTemplate.Visible = False
                    hlReissuanceTemplateClient.Visible = False
                    hlCOTCPrintTemplate.Visible = False
                    hlCOTCPrintTemplate.NavigateUrl = ""
                    hlDownloadPrintTemplateBank.NavigateUrl = ""
                    hlDownloadReIssuanceTemplateForBank.NavigateUrl = ""
                    hlDownloadClientPrintTemplate.NavigateUrl = ""
                    hlReissuanceTemplateClient.NavigateUrl = ""
                Else
                    Dim dt As New DataTable
                    Dim objICAPNPType As New ICAccountsPaymentNatureProductType

                    dt = ICAccountPaymentNatureProductTypeController.GetAllAccountPaymanetNatureProductTypeByProductTypeCodeAndCompanyCode(ProductTypeCode, CompanyCode)
                    objICAPNPType.LoadByPrimaryKey(dt.Rows(0)("AccountNumber").ToString, dt.Rows(0)("BranchCode").ToString, dt.Rows(0)("Currency").ToString, dt.Rows(0)("PaymentNatureCode").ToString, dt.Rows(0)("ProductTypeCode").ToString)

                    Dim AppPath As String = "" ' = DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
                    hlDownloadPrintTemplateBank.Visible = True
                    hlDownloadReIssuanceTemplateForBank.Visible = True
                    hlDownloadClientPrintTemplate.Visible = True
                    hlReissuanceTemplateClient.Visible = True
                    ViewState("FileIDForBankPrintTemplate") = objICAPNPType.PrintTemplateFileIDForBank
                    ViewState("FileIDForBankReIssuancePrintTemplate") = objICAPNPType.ReIssuanceTemplateFileIDForBank
                    ViewState("FileIDForClientPrintTemplate") = objICAPNPType.PrintTemplateIDForClient
                    ViewState("FileIDForClientReIssuancePrintTemplate") = objICAPNPType.ReIssuancePrintTemplateIDForClient
                    hlDownloadPrintTemplateBank.NavigateUrl = AppPath & "/DownloadFile.aspx?fileid=" & objICAPNPType.PrintTemplateFileIDForBank.ToString.EncryptString()
                    hlDownloadReIssuanceTemplateForBank.NavigateUrl = AppPath & "/DownloadFile.aspx?fileid=" & objICAPNPType.ReIssuanceTemplateFileIDForBank.ToString.EncryptString()
                    hlDownloadClientPrintTemplate.NavigateUrl = AppPath & "/DownloadFile.aspx?fileid=" & objICAPNPType.PrintTemplateIDForClient.ToString.EncryptString()
                    hlReissuanceTemplateClient.NavigateUrl = AppPath & "/DownloadFile.aspx?fileid=" & objICAPNPType.ReIssuancePrintTemplateIDForClient.ToString.EncryptString()
                End If


                'rfvReissuanceTempBankforBank.Enabled = True
                'rfvPrintTemplateForBank.Enabled = True
                If ddlCompany.SelectedValue.ToString <> "0" Then
                    DirectCast(Me.Control.FindControl("trClientLocations"), HtmlTableRow).Style.Remove("display")
                    LoadClientPrintLocations()
                    If Not chkListClientLocations.Items.Count > 0 Then
                        DirectCast(Me.Control.FindControl("trClientLocations"), HtmlTableRow).Style.Add("display", "none")
                    End If
                    'rfvPrintTemplateForClient.Enabled = True
                    'rfvReissuanceTempBankforClient.Enabled = True
                Else
                    chkListClientLocations.Items.Clear()
                    DirectCast(Me.Control.FindControl("trClientLocations"), HtmlTableRow).Style.Add("display", "none")
                    'rfvPrintTemplateForClient.Enabled = False
                    'rfvReissuanceTempBankforClient.Enabled = False
                End If
            ElseIf DisbursementMode.ToString = "Direct Credit" Then
                DirectCast(Me.Control.FindControl("trClientLocations"), HtmlTableRow).Style.Add("display", "none")
                DirectCast(Me.Control.FindControl("trBankLocations"), HtmlTableRow).Style.Add("display", "none")
                DirectCast(Me.Control.FindControl("trCOTCPrintTemplate"), HtmlTableRow).Style.Add("display", "none")
                chkListBankPrintLocations.Items.Clear()
                chkListClientLocations.Items.Clear()
                hlDownloadPrintTemplateBank.Visible = False
                hlDownloadReIssuanceTemplateForBank.Visible = False
                hlDownloadClientPrintTemplate.Visible = False
                hlReissuanceTemplateClient.Visible = False
                hlDownloadPrintTemplateBank.NavigateUrl = ""
                hlDownloadReIssuanceTemplateForBank.NavigateUrl = ""
                hlDownloadClientPrintTemplate.NavigateUrl = ""
                hlReissuanceTemplateClient.NavigateUrl = ""
                hlCOTCPrintTemplate.Visible = False
                hlCOTCPrintTemplate.NavigateUrl = ""
            ElseIf DisbursementMode.ToString = "Other Credit" Or DisbursementMode.ToString = "Bill Payment" Then
                DirectCast(Me.Control.FindControl("trClientLocations"), HtmlTableRow).Style.Add("display", "none")
                DirectCast(Me.Control.FindControl("trBankLocations"), HtmlTableRow).Style.Add("display", "none")
                DirectCast(Me.Control.FindControl("trCOTCPrintTemplate"), HtmlTableRow).Style.Add("display", "none")
                chkListBankPrintLocations.Items.Clear()
                chkListClientLocations.Items.Clear()
                hlDownloadPrintTemplateBank.Visible = False
                hlDownloadReIssuanceTemplateForBank.Visible = False
                hlDownloadClientPrintTemplate.Visible = False
                hlReissuanceTemplateClient.Visible = False
                hlDownloadPrintTemplateBank.NavigateUrl = ""
                hlDownloadReIssuanceTemplateForBank.NavigateUrl = ""
                hlDownloadClientPrintTemplate.NavigateUrl = ""
                hlReissuanceTemplateClient.NavigateUrl = ""
                hlCOTCPrintTemplate.Visible = False
                hlCOTCPrintTemplate.NavigateUrl = ""
            End If
        End If

    End Sub

    Private Sub LoadPackages(ByVal DisbursementMode As String)


        Try

            Me.ddlPackage.Items.Clear()



            Dim item As New ListItem("No Package Selected", "-1")

            Me.ddlPackage.Items.Add(item)





            Dim packagecoll As IC.ICPackageCollection

            packagecoll = ICPackageManagementController.GetAllActivePackageByID(DisbursementMode)


            If Not packagecoll Is Nothing Then



                For Each package As IC.ICPackage In packagecoll


                    Me.ddlPackage.Items.Add(New ListItem(package.Title & " (" & package.PackageType & ")", package.PackageID))





                Next



            End If




        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try





    End Sub


    Private Function CheckIsPrintLocationSelectedAgainstProductType(ByVal DisbursementMode As String) As Boolean
        Dim Result As Boolean = False
        If DisbursementMode = "Cheque" Then
            For Each lit As ListItem In chkListBankPrintLocations.Items
                If lit.Selected = True Then
                    Result = True
                    Exit For
                End If
            Next
            If Result = False Then
                For Each lit As ListItem In chkListClientLocations.Items
                    If lit.Selected = True Then
                        Result = True
                        Exit For
                    End If
                Next
            End If
        ElseIf DisbursementMode = "PO" Or DisbursementMode = "DD" Then
            For Each lit As ListItem In chkListBankPrintLocations.Items
                If lit.Selected = True Then
                    Result = True
                    Exit For
                End If
            Next
        End If
        Return Result
    End Function

    Private Sub LoadBankPrintLocations()
        Try
            chkListBankPrintLocations.Items.Clear()
            chkListBankPrintLocations.AppendDataBoundItems = True
            chkListBankPrintLocations.DataSource = ICOfficeController.GetPrincipalBankBranchActiveAndApprovePrintingLocations()
            chkListBankPrintLocations.DataTextField = "OfficeName"
            chkListBankPrintLocations.DataValueField = "OfficeID"
            chkListBankPrintLocations.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadClientPrintLocations()
        Try
            chkListClientLocations.Items.Clear()
            chkListClientLocations.AppendDataBoundItems = True
            chkListClientLocations.DataSource = ICOfficeController.GetOfficeByCompanyCodeActiveAndApprovePrintingLocatoionSecond(ddlCompany.SelectedValue.ToString)
            chkListClientLocations.DataTextField = "OfficeName"
            chkListClientLocations.DataValueField = "OfficeID"
            chkListClientLocations.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        'Dim objICAccountsPNPT As New ICAccountsPaymentNatureProductType
        If Page.IsValid Then
            Try
                ' Dim chkSelect As CheckBox
                Dim PassToApproveCount As Integer = 0
                Dim FailToApproveCount As Integer = 0
                Dim objICAccountsPNPT As New ICAccountsPaymentNatureProductType
                Dim AccountNumber, BranchCode, Currency, PaymentNatureCode, ProductTypeCode, CompanyCode As String


                If Me.UserInfo.IsSuperUser = False Then
                    If objICAccountsPNPT.CreateBy = Me.UserId Then
                        UIUtilities.ShowDialog(Me, "Account Approve", "Account must be approved by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                If chkApproved.Checked = False Then
                    UIUtilities.ShowDialog(Me, "Approve Account", "Please checked the Approved Check Box.", ICBO.IC.Dialogmessagetype.Warning)
                    Exit Sub
                Else
                    AccountNumber = ""
                    BranchCode = ""
                    Currency = ""
                    PaymentNatureCode = ""
                    ProductTypeCode = Request.QueryString("ProductTypeCode").ToString()
                    CompanyCode = Request.QueryString("CompanyCode").ToString()

                    Dim dt As DataTable

                    '  objICAccountsPNPT = New ICAccountsPaymentNatureProductType
                    dt = ICAccountPaymentNatureProductTypeController.GetAllAccountPaymanetNatureProductTypeByProductTypeCodeAndCompanyCode(ProductTypeCode.ToString, CompanyCode.ToString)

                    For Each dr As DataRow In dt.Rows

                        objICAccountsPNPT.LoadByPrimaryKey(dr("AccountNumber").ToString, dr("BranchCode").ToString, dr("Currency").ToString, dr("PaymentNatureCode").ToString, dr("productTypeCode").ToString)

                        ICAccountPaymentNatureProductTypeController.ApproveAccountPNPT(dr("AccountNumber").ToString, dr("BranchCode").ToString, dr("Currency").ToString, dr("PaymentNatureCode").ToString, dr("productTypeCode").ToString, Me.UserId, Me.UserInfo.Username, True)

                    Next
                    UIUtilities.ShowDialog(Me, "Approve Account", "Account approved succesfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                End If


            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
End Class
