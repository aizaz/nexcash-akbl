﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddAccountPaymentNatureProductTypeSecond.ascx.vb"
    Inherits="DesktopModules_AccountPaymentNatureProductTypeManagement_AddAccountPaymentNatureProductTypeSecond" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }


    function ValidateChkList(source, arguments) {

        arguments.IsValid = IsCheckBoxChecked() ? true : false;



    }

</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:TextBoxSetting BehaviorID="reName" Validation-IsRequired="true" ErrorMessage="Invalid Account Title"
        EmptyMessage="Enter Account Title">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior3" Validation-IsRequired="true"
        ErrorMessage="Invalid Account No." EmptyMessage="Enter Account Number">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountNumber" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehaviorCurrency" Validation-IsRequired="true"
        ErrorMessage="Invalid Currency" EmptyMessage="Enter Currency">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountCurrency" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior2" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9 .-_/]{2,20}$" ErrorMessage="Invalid Area Code"
        EmptyMessage="Enter Area Code">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBranchCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="lblReqHeader" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqGroup" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 28%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqComapny" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 22%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlGroup" runat="server" ControlToValidate="ddlGroup"
                Display="Dynamic" ErrorMessage="Please select Group" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 33%">
            <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" ControlToValidate="ddlCompany"
                Display="Dynamic" ErrorMessage="Please select Company" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 17%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblProductType" runat="server" Text="Select Product Type" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqProductType" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAccount" runat="server" Text="Select Account, Payment Nature" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqAPNature" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlProductType" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
            <br />
            <asp:RequiredFieldValidator ID="rfvddlProductType" runat="server" ControlToValidate="ddlProductType"
                Display="Dynamic" ErrorMessage="Please select Product" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%" rowspan="3">
            <asp:Panel ID="pnlfileUploadTemplate" runat="server" Height="100" ScrollBars="Vertical"
                Width="328px">
                <telerik:RadTreeView ID="radtvAccountsPaymentNatureTemplates" runat="server" TriStateCheckBoxes="true"
                    CheckChildNodes="false">
                    <Nodes>
                        <telerik:RadTreeNode id="radMainTreeNode" runat="server" Checkable="true" Value="top"
                            Text="(Select All)">
                        </telerik:RadTreeNode>
                    </Nodes>
                </telerik:RadTreeView>
            </asp:Panel>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%;height:16px">
            <asp:Label ID="lblProductType0" runat="server" Text="Select Package" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            </td>
        <td align="left" valign="top" style="width: 25%">
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlPackage" runat="server" CssClass="dropdown">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            </td>
        <td align="left" valign="top" style="width: 25%">
            </td>
    </tr>
    <tr runat="server" id="trCOTCPrintTemplate" style="display:none" width="100%">
        <td align="left" valign="top" colspan="4">
            <table runat="server" id="tblCOTCPrintTemplate" width="100%">
                <tr>
                     <td align="left" valign="top" style="width: 25%">
                                        <asp:Label ID="lblCOTCPrintTemplate" runat="server" CssClass="lbl" Text="COTC Print Template"></asp:Label>
                                        <asp:Label ID="lblReqCOTCPrintTemplate" runat="server" ForeColor="Red" Text="*"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            </td>
        <td align="left" valign="top" style="width: 25%">
            </td>


                </tr>
                   <tr>
                     <td align="left" valign="top" style="width: 25%">
                                            <asp:FileUpload ID="fupCOTCPrintTemplate" runat="server" />
                                           
                                            <asp:HyperLink ID="hlCOTCPrintTemplate" runat="server" ImageUrl="~/images/file.gif" ToolTip="Download File"></asp:HyperLink>
        </td>
        <td align="left" valign="top" style="width: 25%">
            </td>
        <td align="left" valign="top" style="width: 25%">
            </td>


                </tr>
            </table>
        </td>
 </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <table runat="server" id="tblBankLocations" width="100%">
                <tr runat="server" id="trBankLocations" style="display: none" width="100%">
                    <td style="width: 100%">
                        <asp:Panel ID="pnlBankLocations" runat="server" Width="100%" Height="150px">
                            <table style="width: 100%">
                                <tr>
                                    <td align="left" style="width: 25%" valign="top">
                                        <asp:Label ID="lblSelectBankLocation" runat="server" CssClass="lbl" Text="Select Bank Print Location"></asp:Label>
                                        <asp:Label ID="lblReqSelectBankLocations" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 25%" valign="top">
                                    </td>
                                    <td align="left" style="width: 25%" valign="top">
                                        <asp:Label ID="lblPrintTemplateForBank" runat="server" CssClass="lbl" Text="Select Print Template"></asp:Label>
                                        <asp:Label ID="lblReqPrintTemplateForBank" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 25%" valign="top">
                                    </td>
                                    <tr>
                                        <td align="left" style="width: 25%" valign="top">
                                            <asp:Panel ID="pnlBankPrintLocations" runat="server" Height="100" ScrollBars="Vertical"
                                                Width="328px">
                                                <asp:CheckBoxList ID="chkListBankPrintLocations" runat="server" CssClass="dropdown">
                                                </asp:CheckBoxList>
                                                <br />
                                            </asp:Panel>
                                        </td>
                                        <td align="left" style="width: 25%" valign="top">
                                            &nbsp;
                                        </td>
                                        <td align="left" style="width: 25%" valign="top">
                                            <asp:FileUpload ID="fupPrintTemplateBank" runat="server" />
                                           
                                            <asp:HyperLink ID="hlDownloadPrintTemplateBank" runat="server" ImageUrl="~/images/file.gif" ToolTip="Download File"></asp:HyperLink>
                                            <br />

                                            <%--<asp:RequiredFieldValidator ID="rfvPrintTemplateForBank" runat="server" ControlToValidate="fupPrintTemplateBank"
                                CssClass="lblEror" Display="Dynamic" 
                                    ErrorMessage="Select print template in xml format." SetFocusOnError="True"
                                ToolTip="Required Field"></asp:RequiredFieldValidator>--%><br />
                                            <asp:Label ID="lblReIssuanceTemplateForBank" runat="server" CssClass="lbl" Text="Select Re-Issuance Print Template"></asp:Label>
                                            <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="*"></asp:Label><br />
                                            <asp:FileUpload ID="fupReIssuanceTemplateBank" runat="server" />
                                            <asp:HyperLink ID="hlDownloadReIssuanceTemplateForBank" runat="server" ImageUrl="~/images/file.gif" ToolTip="Download File"></asp:HyperLink>
                                            <%--<asp:RequiredFieldValidator ID="rfvReissuanceTempBankforBank" 
                                    runat="server" ControlToValidate="fupReIssuanceTemplateBank"
                                CssClass="lblEror" Display="Dynamic" 
                                    ErrorMessage="Select reissuance template in xml format." SetFocusOnError="True"
                                ToolTip="Required Field"></asp:RequiredFieldValidator>--%>
                                        </td>
                                        <td align="left" style="width: 25%" valign="top">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr runat="server" id="trClientLocations" style="display: none" width="100%">
                    <td style="width: 100%">
                        <asp:Panel ID="pnlClientLocations" runat="server" Width="100%" Height="150px">
                            <table style="width: 100%">
                                <tr>
                                    <td align="left" style="width: 25%" valign="top">
                                        <asp:Label ID="lblSelectclientLocations" runat="server" CssClass="lbl" Text="Select Client Location"></asp:Label>
                                        <asp:Label ID="lblReqSelectclientLocations" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 25%" valign="top">
                                    </td>
                                    <td align="left" style="width: 25%" valign="top">
                                        <asp:Label ID="lblPrintTemplateClient" runat="server" CssClass="lbl" Text="Select Print Template"></asp:Label>
                                        <asp:Label ID="lblReqClientPrintTemplate" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 25%" valign="top">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width: 25%" valign="top">
                                        <asp:Panel ID="pnlUsers1" runat="server" Height="100" ScrollBars="Vertical" Width="328px">
                                            <asp:CheckBoxList ID="chkListClientLocations" runat="server" CssClass="dropdown">
                                            </asp:CheckBoxList>
                                        </asp:Panel>
                                    </td>
                                    <td align="left" style="width: 25%" valign="top">
                                        &nbsp;
                                    </td>
                                    <td align="left" style="width: 25%" valign="top">
                                        <asp:FileUpload ID="fupClientPrintTemplate" runat="server" />
                                        <asp:HyperLink ID="hlDownloadClientPrintTemplate" runat="server" ImageUrl="~/images/file.gif" ToolTip="Download File"></asp:HyperLink>
                                        <br />

                                        <%--<asp:RequiredFieldValidator ID="rfvPrintTemplateForClient" runat="server" 
                                    ControlToValidate="fupPrintTemplateBank" CssClass="lblEror" Display="Dynamic" 
                                    ErrorMessage="Select print template in xml format." SetFocusOnError="True" 
                                    ToolTip="Required Field">--%><%--</asp:RequiredFieldValidator>--%>
                                        <br />
                                        <asp:Label ID="lblPrintTemplateClientReissuance" runat="server" CssClass="lbl" Text="Select Re-Issuance Print Template"></asp:Label>
                                        <asp:Label ID="lblReqClientPrintTemplate0" runat="server" ForeColor="Red" Text="*"></asp:Label><br />
                                        <asp:FileUpload ID="fupReissuanceTemplateClient" runat="server" />
                                        <asp:HyperLink ID="hlReissuanceTemplateClient" runat="server" ImageUrl="~/images/file.gif" ToolTip="Download File"></asp:HyperLink>
                                        <br />
                                        <%--<asp:RequiredFieldValidator ID="rfvReissuanceTempBankforClient" runat="server" 
                                    ControlToValidate="fupReIssuanceTemplateBank" CssClass="lblEror" 
                                    Display="Dynamic" ErrorMessage="Select reissuance template in xml format." 
                                    SetFocusOnError="True" ToolTip="Required Field">--%></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" style="width: 25%" valign="top">
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
      <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        
            <asp:Label ID="lblIsApproved" runat="server" Text="Is Approved" CssClass="lbl" 
                Visible="true"></asp:Label>

        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
           
        
       <asp:CheckBox ID="chkApproved" runat="server" Text=" " Visible="true" CssClass="txtbox" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn" Width="71px" CausesValidation="true"/>
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px"
                CausesValidation="False" />
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;
        </td>
    </tr>
</table>
