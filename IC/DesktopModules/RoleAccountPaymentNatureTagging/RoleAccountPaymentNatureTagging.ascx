﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RoleAccountPaymentNatureTagging.ascx.vb"
    Inherits="DesktopModules_RoleAccountPaymentNatureTagging_RoleAccountPaymentNatureTagging" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:RegExpTextBoxSetting BehaviorID="REUserName" Validation-IsRequired="true"
        ValidationExpression="\b(\w*)\b(.)*\b(-)*\b(_)*\b" ErrorMessage="Invalid Name"
        EmptyMessage="Enter User Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtUserName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior7" Validation-IsRequired="true"
        ValidationExpression="\b(\w*)\b(.)*\b(-)*\b(_)*\b" ErrorMessage="Invalid Display  Name"
        EmptyMessage="Enter Display Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDisplayName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="false"
        EmptyMessage="Enter Role Type">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRoleType" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<style type="text/css">
    .style1
    {
        height: 22px;
    }
    .btn
    {
        margin-left: 0px;
    }
</style>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
    function ValidateChkList(source, arguments) {

        arguments.IsValid = IsCheckBoxChecked() ? true : false;



    }


</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        //      alert(grid)
        //        if ((grid.rows.length -1) != 10 ) {
        //        if ((grid.rows.length) >= 12) {
        //            //            alert('More than 10 rows')
        //            for (i = 0; i < grid.rows.length - 1; i++) {
        //                cell = grid.rows[i].cells[CellNo];
        //                for (j = 0; j < cell.childNodes.length - 1; j++) {
        //                    if (cell.childNodes[j].type == "checkbox") {
        //                        cell.childNodes[j].checked = document.getElementById(idOfControllingCheckbox).checked;
        //                    }
        //                }
        //            }
        //        }
        //        else {
        //                     alert(grid.rows.length)
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    //    }
</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="center" valign="top">
                        <table width="100%">
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" colspan="2">
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Role, Account &amp; Payment Nature Tagging"
                                        CssClass="headingblue"></asp:Label>
                                    <br />
                                    Note:&nbsp; Fields marked as
                                    <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                    &nbsp;are required.
                                </td>
                                <td align="left" valign="top" colspan="2">
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" colspan="2">
                                </td>
                                <td align="left" valign="top" colspan="2">
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblUserType" runat="server" Text="Select User Type" CssClass="lbl"></asp:Label>
                                    <asp:Label ID="lblReqUserType" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblUser" runat="server" Text="Select User" CssClass="lbl"></asp:Label>
                                    <asp:Label ID="lblReqUser" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:DropDownList ID="ddlUserType" runat="server" CssClass="dropdown" AutoPostBack="True">
                                        <asp:ListItem Selected="True" Value="0">-- Select --</asp:ListItem>
                                        <asp:ListItem Value="Bank User">Bank User</asp:ListItem>
                                        <asp:ListItem Value="Client User">Client User</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:DropDownList ID="ddlUser" runat="server" CssClass="dropdown" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:RequiredFieldValidator ID="rfvUserType" runat="server" ControlToValidate="ddlUserType"
                                        CssClass="lblEror" Display="Dynamic" ErrorMessage="Please Select User Type" SetFocusOnError="True"
                                        ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:RequiredFieldValidator ID="rfvUser" runat="server" ControlToValidate="ddlUser"
                                        CssClass="lblEror" Display="Dynamic" ErrorMessage="Please Select User" SetFocusOnError="True"
                                        ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblUserGroup" runat="server" Text="Select User" CssClass="lbl" Visible="False"></asp:Label>
                                    <asp:Label ID="lblReqUserGroup" runat="server" Text="*" ForeColor="Red" Visible="False"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <%--<tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" />
                                    &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" CausesValidation="False" />
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>--%>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:DropDownList ID="ddlUserGroup" runat="server" CssClass="dropdown" AutoPostBack="True"
                                        Visible="False">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:RequiredFieldValidator ID="rfvUserGroup" runat="server" ControlToValidate="ddlUser"
                                        CssClass="lblEror" Display="Dynamic" ErrorMessage="Please Select User" SetFocusOnError="True"
                                        ToolTip="Required Field" InitialValue="0" Visible="False"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <table id="tblUserRoles" runat="server" style="display: none; width: 100%">
                            <tr>
                                <td align="left" valign="top" style="width: 100%">
                                    <asp:Label ID="lblUserAssignedRoles" runat="server" CssClass="headingblue" Font-Bold="False"
                                        Text="User Role's List"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" style="width: 100%">
                                    <asp:Label ID="lblRNFUserRoles" runat="server" CssClass="headingblue" Font-Bold="False"
                                        Text="No Record Found" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" style="width: 100%">
                                    <telerik:RadGrid ID="gvUserRoles" runat="server" AllowPaging="false" AllowSorting="false"
                                        CssClass="RadGrid" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                                        PageSize="100">
                                        <AlternatingItemStyle CssClass="rgAltRow" />
                                        <ClientSettings>
                                            <Scrolling AllowScroll="true" UseStaticHeaders="true" ScrollHeight="150px" />
                                        </ClientSettings>
                                        <MasterTableView DataKeyNames="RoleID" TableLayout="Fixed">
                                            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                            </RowIndicatorColumn>
                                            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                            </ExpandCollapseColumn>
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="RoleName" HeaderText="Role Name" SortExpression="RoleName">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridTemplateColumn HeaderText="View">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="IbtnShowAccountPaymentNature" runat="server" CommandArgument='<%#Eval("RoleID") %>'
                                                            CommandName="ShowAccountPaymentNature" ImageUrl="~/images/view.gif" ToolTip="Show Account & Payment Nature" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                        </MasterTableView>
                                        <ItemStyle CssClass="rgRow" />
                                    </telerik:RadGrid>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <table id="tblUserRolesPaymentNature" runat="server" style="display: none; width: 100%">
                            <tr>
                                <td align="left" valign="top" style="width: 100%">
                                    <asp:Label ID="lblPaymentNatureList" runat="server" Font-Bold="False" CssClass="headingblue"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" colspan="4">
                                    <asp:Label ID="lblRNFAPNList" runat="server" Text="No Record Found" Visible="False"
                                        Font-Bold="False" CssClass="headingblue"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" colspan="4">
                                    <telerik:RadGrid ID="gvPaymentNature" runat="server" AllowPaging="true" AllowSorting="false"
                                        AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100"
                                        CssClass="RadGrid">
                                        <%--  <AlternatingItemStyle CssClass="rgAltRow" />--%>
                                        <ClientSettings>
                                            <Scrolling UseStaticHeaders="true" AllowScroll="true" />
                                        </ClientSettings>
                                        <MasterTableView DataKeyNames="AccountNumber,BranchCode,Currency,PaymentNatureCode,RolesID"
                                            TableLayout="Fixed">
                                            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                            </RowIndicatorColumn>
                                            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                            </ExpandCollapseColumn>
                                            <Columns>
                                                <telerik:GridTemplateColumn>
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkSelectAllAccountPaymentNatures" runat="server" CssClass="chkBox"
                                                            Text=" Select" TextAlign="Right" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridBoundColumn DataField="GroupName" HeaderText="Group" SortExpression="GroupName">
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="11%" />
                                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="11%" />
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="CompanyName" HeaderText="Company" SortExpression="CompanyName">
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="11%" />
                                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="11%" />
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="Account" SortExpression="AccountNumber">
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="11%" />
                                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="11%" />
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="PaymentNatureName" HeaderText="Payment Nature"
                                                    SortExpression="PaymentNatureName">
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                                </telerik:GridBoundColumn>
                                                <telerik:GridCheckBoxColumn HeaderText="Approve" DataField="isApproved">
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                                </telerik:GridCheckBoxColumn>
                                                <telerik:GridTemplateColumn ItemStyle-Width="16px" HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="IbtnDelete" runat="server" CommandName="del" ImageUrl="~/images/delete.gif"
                                                            CommandArgument='<%#Eval("AccountNumber").tostring + ";" + Eval("BranchCode").tostring + ";" + Eval("Currency").tostring + ";" + Eval("RolesID").tostring + ";" + Eval("PaymentNatureCode").tostring %>'
                                                            OnClientClick="javascript: return con();" ToolTip="Delete Account, Payment Nature" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                        </MasterTableView>
                                        <ItemStyle CssClass="rgRow" />
                                    </telerik:RadGrid>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnApproveAccountPaymentNatures" runat="server" Text="Approve Accounts & PaymentNatures"
                            CssClass="btn" Width="248px" />
                        <asp:Button ID="btnDeleteAccountPaymentNatures" runat="server" Text="Delete Accounts & PaymentNatures"
                            CssClass="btn" Width="241px" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                    </td>
                </tr>
                <tr align="center" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%">
                        <table id="tblPaymentNaturesTreeView" runat="server" style="display: none; width: 100%">
                            <tr>
                                <td align="left" valign="top" style="width: 100%">
                                    <asp:Label ID="lblAssignAccountPaymentNature" runat="server" Text="Blue" Font-Bold="False"
                                        CssClass="headingblue"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:Label ID="lblRNFTreeView" runat="server" Text="No Record Found" Visible="False"
                                        Font-Bold="False" CssClass="headingblue"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:Panel ID="pnlAssignAcoountPaymentNature" runat="server" Width="100%" Height="200px"
                                        ScrollBars="Vertical">
                                        <telerik:RadTreeView ID="radtvAccountsPaymentNature" runat="server" TriStateCheckBoxes="true"
                                            >
                                            <Nodes>
                                                <telerik:RadTreeNode id="radMainTreeNode" runat="server" Checkable="true" Value="top"
                                                    Text="(Select All)">
                                                </telerik:RadTreeNode>
                                            </Nodes>
                                        </telerik:RadTreeView>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="75px" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px"
                            CausesValidation="False" />
                    </td>
                </tr>
                <tr align="center" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
