﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports EntitySpaces.Core
Partial Class DesktopModules_RoleAccountPaymentNatureTagging_RoleAccountPaymentNatureTagging
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private UserCode As String
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Dim dtDa As DataTable
    Private RolID As Integer = 0
    Private RoleType As String = ""
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                tblUserRoles.Style.Add("display", "none")
                tblPaymentNaturesTreeView.Style.Add("display", "none")
                tblUserRolesPaymentNature.Style.Add("display", "none")
                btnDeleteAccountPaymentNatures.Visible = False
                btnApproveAccountPaymentNatures.Visible = False
                btnSave.Visible = False
                LoadddlUser(ddlUserType.SelectedValue.ToString)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "User’s Role Tagging with Accounts & Payment Natures")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlUser(ByVal UserType As String)
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlUser.Items.Clear()
            ddlUser.Items.Add(lit)
            ddlUser.AppendDataBoundItems = True
            If ddlUserType.SelectedValue.ToString = "Bank User" Then
                lblUser.Text = "Select Role"
                rfvUser.Text = "Please select Role"
                ddlUser.DataSource = ICRoleController.GetAllICRolesByRoleType("Bank Role")
                ddlUser.DataTextField = "RoleName"
                ddlUser.DataValueField = "RoleID"
                ddlUser.DataBind()
                lblUserGroup.Visible = False
                lblReqUserGroup.Visible = False
                ddlUserGroup.ClearSelection()
                ddlUserGroup.Visible = False
                rfvUserGroup.Enabled = False
                rfvUserGroup.Visible = False
            ElseIf ddlUserType.SelectedValue.ToString = "Client User" Then
                lblUser.Text = "Select Group"
                rfvUser.Text = "Please select Group"
                ddlUser.DataSource = ICGroupController.GetAllActiveGroups
                ddlUser.DataTextField = "GroupName"
                ddlUser.DataValueField = "GroupCode"
                ddlUser.DataBind()
                LoadddlUserGroup()

            ElseIf ddlUserType.SelectedValue.ToString = "0" Then
                lblUser.Text = "Select User"
                rfvUser.Text = "Please select User"
                ddlUser.ClearSelection()
                lblUserGroup.Visible = False
                lblReqUserGroup.Visible = False
                ddlUserGroup.ClearSelection()
                ddlUserGroup.Visible = False
                rfvUserGroup.Enabled = False
                rfvUserGroup.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlUserGroup()
        Try
            If ddlUserType.Text = "Client User" Then
                lblUserGroup.Visible = True
                lblReqUserGroup.Visible = True
                ddlUserGroup.ClearSelection()
                ddlUserGroup.Visible = True
                rfvUserGroup.Enabled = True
                rfvUserGroup.Visible = True
                Dim lit As New ListItem
                lit.Value = "0"
                lit.Text = "-- Please Select --"
                ddlUserGroup.Items.Clear()
                ddlUserGroup.Items.Add(lit)
                ddlUserGroup.AppendDataBoundItems = True
                ddlUserGroup.DataSource = ICUserController.GetAllActiveAndApproveUsersByGroupCode(ddlUser.SelectedValue.ToString)
                ddlUserGroup.DataValueField = "UserID"
                ddlUserGroup.DataTextField = "UserName"
                ddlUserGroup.DataBind()
            Else
                lblUserGroup.Visible = False
                lblReqUserGroup.Visible = False
                ddlUserGroup.ClearSelection()
                ddlUserGroup.Visible = False
                rfvUserGroup.Enabled = False
                rfvUserGroup.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckRoleAlReadyAssign(ByVal RoleName As String) As Boolean
        Dim Result As Boolean = False
        Dim gvRow As Telerik.Web.UI.GridItem

        For Each gvRow In gvUserRoles.Items
            If gvRow.Cells(0).Text.ToString() = RoleName.ToString() Then
                Result = True
            End If
        Next
        Return Result
    End Function
    Private Sub SetgvUserRoles()
        Try
            If ddlUserType.SelectedValue.ToString = "Bank User" Then
                tblUserRoles.Style.Add("display", "none")
                lblRNFUserRoles.Visible = False
                tblPaymentNaturesTreeView.Style.Add("display", "none")
                tblUserRolesPaymentNature.Style.Add("display", "none")
                'ICUserRolesController.GetAssignedUserRolesForRadGrid(ddlUser.SelectedValue.ToString, Me.gvUserRoles.CurrentPageIndex + 1, Me.gvUserRoles.PageSize, Me.gvUserRoles, True)
                'If gvUserRoles.Items.Count > 0 Then
                '    tblUserRoles.Style.Remove("display")
                '    'pnlUserRoleList.Style.Remove("display")
                '    lblRNFUserRoles.Visible = False
                'Else
                '    tblUserRoles.Style.Remove("display")
                '    'pnlUserRoleList.Style.Add("display", "none")
                '    lblRNFUserRoles.Visible = True
                'End If
            ElseIf ddlUserType.SelectedValue.ToString = "Client User" Then
                ICUserRolesController.GetAssignedUserRolesForRadGrid(ddlUserGroup.SelectedValue.ToString, Me.gvUserRoles.CurrentPageIndex + 1, Me.gvUserRoles.PageSize, Me.gvUserRoles, True)
                If gvUserRoles.Items.Count > 0 Then
                    tblUserRoles.Style.Remove("display")
                    'pnlUserRoleList.Style.Remove("display")
                    lblRNFUserRoles.Visible = False
                Else
                    tblUserRoles.Style.Remove("display")
                    'pnlUserRoleList.Style.Add("display", "none")
                    lblRNFUserRoles.Visible = True
                End If
            ElseIf ddlUserType.SelectedValue.ToString = "0" Then
                tblUserRoles.Style.Add("display", "none")
                lblRNFUserRoles.Visible = False
                tblPaymentNaturesTreeView.Style.Add("display", "none")
                tblUserRolesPaymentNature.Style.Add("display", "none")
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub gvUserRoles_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvUserRoles.ItemCommand
        Try

            If e.CommandName = "ShowAccountPaymentNature" Then
                Dim objICRole As New ICRole
                objICRole.es.Connection.CommandTimeout = 3600
                objICRole.LoadByPrimaryKey(e.CommandArgument.ToString)
                ViewState("RoleID") = Nothing
                ViewState("RoleID") = objICRole.RoleID
                lblPaymentNatureList.Text = objICRole.RoleName.ToString() & "s Account & Payment Nature List"
                If ddlUserType.SelectedValue.ToString = "Client User" Then
                    LoadgvPaymentNature(ddlUserGroup.SelectedValue.ToString, e.CommandArgument.ToString, True)
                    If gvPaymentNature.Items.Count > 0 Then
                        CheckMarkAlreadyTaggedAccountPaymentNatureWithRole(ddlUserGroup.SelectedValue.ToString, ViewState("RoleID").ToString)
                        radtvAccountsPaymentNature.ExpandAllNodes()
                    Else
                        radtvAccountsPaymentNature.UncheckAllNodes()
                        radtvAccountsPaymentNature.CollapseAllNodes()

                    End If
                ElseIf ddlUserType.SelectedValue.ToString = "Bank User" Then
                    LoadgvPaymentNature("0", e.CommandArgument.ToString, True)

                End If

                lblAssignAccountPaymentNature.Text = "Assign Account & Payment Nature to Role [ " & objICRole.RoleName.ToString() & " ]"
                If ddlUserType.SelectedValue.ToString = "Client User" Then
                    BindTreeView(ddlUserGroup.SelectedValue.ToString)
                    If gvPaymentNature.Items.Count > 0 Then
                        CheckMarkAlreadyTaggedAccountPaymentNatureWithRole(ddlUserGroup.SelectedValue.ToString, ViewState("RoleID").ToString)
                        radtvAccountsPaymentNature.ExpandAllNodes()
                    Else
                        radtvAccountsPaymentNature.UncheckAllNodes()
                        radtvAccountsPaymentNature.CollapseAllNodes()

                    End If
                Else
                    BindTreeView("Bank User")
                    If gvPaymentNature.Items.Count > 0 Then
                        CheckMarkAlreadyTaggedAccountPaymentNatureWithRole(ddlUser.SelectedValue.ToString, ViewState("RoleID").ToString)
                        radtvAccountsPaymentNature.ExpandAllNodes()
                    Else
                        radtvAccountsPaymentNature.UncheckAllNodes()
                        radtvAccountsPaymentNature.CollapseAllNodes()

                    End If
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    'Private Sub CheckMarkAlreadyTaggedAccountPaymentNatureWithRole(ByVal UserID As String, ByVal RoleID As String)
    '    Dim objICUserRolesAPNColl As New ICUserRolesAccountAndPaymentNatureCollection
    '    Dim objICUserRolesAPNAndPLocationColl As ICUserRolesAccountPaymentNatureAndLocationsCollection
    '    Dim dt As New DataTable
    '    objICUserRolesAPNColl.es.Connection.CommandTimeout = 3600


    '    objICUserRolesAPNColl = ICUserRolesPaymentNatureController.GetAllUserRolesAccountPaymentNatureByRoleIDAndUserID(RoleID, UserID)
    '    For Each objICUserRoleAPNature As ICUserRolesAccountAndPaymentNature In objICUserRolesAPNColl
    '        For Each radTv As RadTreeNode In radtvAccountsPaymentNature.Nodes
    '            For Each radTVGroupNode As RadTreeNode In radTv.Nodes
    '                For Each radCompnayNode As RadTreeNode In radTVGroupNode.Nodes
    '                    For Each radAPNatureNode As RadTreeNode In radCompnayNode.Nodes
    '                        If radAPNatureNode.Value = objICUserRoleAPNature.AccountNumber & "-" & objICUserRoleAPNature.BranchCode & "-" & objICUserRoleAPNature.Currency & "-" & objICUserRoleAPNature.PaymentNatureCode Then
    '                            radAPNatureNode.Checked = True
    '                            objICUserRolesAPNAndPLocationColl = New ICUserRolesAccountPaymentNatureAndLocationsCollection
    '                            objICUserRolesAPNAndPLocationColl.es.Connection.CommandTimeout = 3600
    '                            objICUserRolesAPNAndPLocationColl = ICUserRolesAccountPaymentNatureAndLocationsController.GetAllUserRolesAPNAndPLocationsByAPNUserIDAndRoleID(RoleID, UserID, objICUserRoleAPNature.PaymentNatureCode, objICUserRoleAPNature.AccountNumber, objICUserRoleAPNature.BranchCode, objICUserRoleAPNature.Currency)
    '                            dt = objICUserRolesAPNAndPLocationColl.Query.LoadDataTable
    '                            For Each objICUserRoleAPNPrintLocation As ICUserRolesAccountPaymentNatureAndLocations In objICUserRolesAPNAndPLocationColl
    '                                For Each radtVLocationNode As RadTreeNode In radAPNatureNode.Nodes
    '                                    If radtVLocationNode.Value = objICUserRoleAPNPrintLocation.OfficeID Then
    '                                        If radtVLocationNode.Checked = False Then
    '                                            radtVLocationNode.Checked = True
    '                                        End If
    '                                    End If
    '                                Next
    '                            Next

    '                        End If
    '                    Next
    '                Next
    '            Next

    '        Next
    '    Next
    'End Sub
    ' Aizaz Work [Dated: 01-Nov-2013]
    Private Sub CheckMarkAlreadyTaggedAccountPaymentNatureWithRole(ByVal UserID As String, ByVal RoleID As String)
        Dim objICUserRolesAPNColl As New ICUserRolesAccountAndPaymentNatureCollection
        Dim objICUserRolesAPNAndPLocationColl As ICUserRolesAccountPaymentNatureAndLocationsCollection
        Dim dtAllAPNLoc As New DataTable
        Dim arrRowLocation As DataRow()
        objICUserRolesAPNColl.es.Connection.CommandTimeout = 3600


        objICUserRolesAPNColl = ICUserRolesPaymentNatureController.GetAllUserRolesAccountPaymentNatureByRoleIDAndUserID(RoleID, UserID)
        dtAllAPNLoc = ICUserRolesAccountPaymentNatureAndLocationsController.GetAllUserRolesAPNLocationsWithAccPayNatByAPNUserIDAndRoleID(RoleID, UserID)
        For Each objICUserRoleAPNature As ICUserRolesAccountAndPaymentNature In objICUserRolesAPNColl
            For Each radTv As RadTreeNode In radtvAccountsPaymentNature.Nodes
                For Each radTVGroupNode As RadTreeNode In radTv.Nodes
                    For Each radCompnayNode As RadTreeNode In radTVGroupNode.Nodes
                        For Each radAPNatureNode As RadTreeNode In radCompnayNode.Nodes
                            If radAPNatureNode.Value = objICUserRoleAPNature.AccountNumber & "-" & objICUserRoleAPNature.BranchCode & "-" & objICUserRoleAPNature.Currency & "-" & objICUserRoleAPNature.PaymentNatureCode Then
                                radAPNatureNode.Checked = True
                                'objICUserRolesAPNAndPLocationColl = New ICUserRolesAccountPaymentNatureAndLocationsCollection
                                'objICUserRolesAPNAndPLocationColl.es.Connection.CommandTimeout = 3600
                                'objICUserRolesAPNAndPLocationColl = ICUserRolesAccountPaymentNatureAndLocationsController.GetAllUserRolesAPNAndPLocationsByAPNUserIDAndRoleID(RoleID, UserID, objICUserRoleAPNature.PaymentNatureCode, objICUserRoleAPNature.AccountNumber, objICUserRoleAPNature.BranchCode, objICUserRoleAPNature.Currency)
                                ''   dt = objICUserRolesAPNAndPLocationColl.Query.LoadDataTable
                                ' Aizaz Work [Dated: 01-Nov-2013]
                                arrRowLocation = Nothing

                                arrRowLocation = dtAllAPNLoc.Select("AccountPaymentNature = '" & objICUserRoleAPNature.AccountNumber & "-" & objICUserRoleAPNature.BranchCode & "-" & objICUserRoleAPNature.Currency & "-" & objICUserRoleAPNature.PaymentNatureCode & "'")

                                If arrRowLocation.Count > 0 Then
                                    For Each drLoc As DataRow In arrRowLocation
                                        For Each radtVLocationNode As RadTreeNode In radAPNatureNode.Nodes
                                            If radtVLocationNode.Value = drLoc("OfficeID").ToString() Then
                                                If radtVLocationNode.Checked = False Then
                                                    radtVLocationNode.Checked = True
                                                End If
                                            End If
                                        Next
                                    Next
                                End If
                            End If
                        Next
                    Next
                Next
            Next
        Next
    End Sub
    Public Shared Sub GetAccountsPaymentNatureByRoleIDAndUserID(ByVal UserID As String, ByVal RoleID As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid)
        Dim params As New EntitySpaces.Interfaces.esParameters
        Dim StrQuery As String = Nothing
        Dim WhereClause As String = Nothing
        Dim GroupByClause As String = Nothing
        Dim OrderByClause As String = Nothing
        OrderByClause = " Order By g.GroupName,c.CompanyName,URAPN.AccountNumber,PN.PaymentNatureName desc"
        WhereClause = " Where "
        GroupByClause = " Group By "
        StrQuery = "select g.GroupName,c.CompanyName,URAPN.AccountNumber,URAPN.BranchCode,URAPN.Currency,URAPN.PaymentNatureCode,"
        StrQuery += "URAPN.RolesID,"
        If UserID <> "0" Then
            StrQuery += "URAPN.UserID,"
        End If
        StrQuery += "pn.PaymentNatureName,URAPN.isApproved from IC_UserRolesAccountAndPaymentNature as URAPN "
        StrQuery += "inner join IC_AccountsPaymentNature as APN on URAPN.AccountNumber=APN.AccountNumber and "
        StrQuery += "URAPN.BranchCode=APN.BranchCode and URAPN.Currency=APN.Currency "
        StrQuery += "inner join IC_Accounts as A on APN.AccountNumber=a.AccountNumber and APN.BranchCode=a.BranchCode and "
        StrQuery += "URAPN.Currency=a.Currency inner join IC_Company as C on a.CompanyCode=c.CompanyCode inner join IC_Group "
        StrQuery += "as G on c.GroupCode=g.GroupCode inner join IC_PaymentNature as PN on URAPN.PaymentNatureCode=pn.PaymentNatureCode "
        GroupByClause += "g.GroupName,c.CompanyName,URAPN.AccountNumber,URAPN.BranchCode,URAPN.Currency,URAPN.PaymentNatureCode,URAPN.RolesID,"
        If UserID <> "0" Then
            GroupByClause += "URAPN.UserID,"
        End If
        GroupByClause += "pn.PaymentNatureName,URAPN.isApproved "
        If UserID <> "0" Then
            WhereClause += " AND URAPN.UserID=@UserID"
            params.Add("UserID", UserID)
        End If
        If RoleID.ToString() <> "0" Then
            WhereClause += " AND URAPN.RolesID=@RoleID"
            params.Add("RoleID", RoleID)
        End If

        WhereClause += " AND A.IsApproved=1 AND A.isActive=1"
        If WhereClause.Contains("Where  AND") Then
            WhereClause = WhereClause.Replace("Where  AND", " Where ")
        End If
        StrQuery += WhereClause & GroupByClause & OrderByClause
        Dim util As New esUtility
        Dim dt As New DataTable
        dt = util.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)

        If Not PageNumber = 0 Then

            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        Else

            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        End If
    End Sub
    Private Sub LoadgvPaymentNature(ByVal UserID As String, ByVal RoleID As String, ByVal DoDataBind As Boolean)
        Try
            gvPaymentNature.DataSource = Nothing
            GetAccountsPaymentNatureByRoleIDAndUserID(UserID, RoleID, Me.gvPaymentNature.CurrentPageIndex + 1, Me.gvPaymentNature.PageSize, DoDataBind, Me.gvPaymentNature)
            'ICUserRolesPaymentNatureController.GetAccountsPaymentNatureByRoleIDAndUserID(UserID, RoleID, Me.gvPaymentNature.CurrentPageIndex + 1, Me.gvPaymentNature.PageSize, DoDataBind, Me.gvPaymentNature)
            If gvPaymentNature.Items.Count > 0 Then
                tblUserRolesPaymentNature.Style.Remove("display")
                gvPaymentNature.Visible = True
                lblPaymentNatureList.Visible = True
                lblRNFAPNList.Visible = False
                'pnlPaymentNature.Visible = True
                btnDeleteAccountPaymentNatures.Visible = CBool(htRights("Delete"))
                btnApproveAccountPaymentNatures.Visible = CBool(htRights("Approve"))
                gvPaymentNature.Columns(6).Visible = CBool(htRights("Delete"))
            Else
                tblUserRolesPaymentNature.Style.Add("display", "none")
                lblPaymentNatureList.Visible = False
                'pnlPaymentNature.Visible = False
                lblRNFAPNList.Visible = True
                btnDeleteAccountPaymentNatures.Visible = False
                btnApproveAccountPaymentNatures.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub gvUserRoles_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvUserRoles.ItemDataBound
        Try

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Protected Sub ddlUserType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUserType.SelectedIndexChanged
        If ddlUserType.SelectedValue.ToString <> "0" Then
            LoadddlUser(ddlUserType.SelectedValue.ToString())
            tblPaymentNaturesTreeView.Style.Add("display", "none")
            tblUserRoles.Style.Add("display", "none")
            tblUserRolesPaymentNature.Style.Add("display", "none")
            btnDeleteAccountPaymentNatures.Visible = False
            btnApproveAccountPaymentNatures.Visible = False
            btnSave.Visible = False
        Else
            tblPaymentNaturesTreeView.Style.Add("display", "none")
            tblUserRoles.Style.Add("display", "none")
            tblUserRolesPaymentNature.Style.Add("display", "none")
            LoadddlUser(ddlUserType.SelectedValue.ToString())
            lblUser.Text = "Select User"
            rfvUser.Text = "Please select Group"
            lblUserGroup.Visible = False
            lblReqUserGroup.Visible = False
            ddlUserGroup.ClearSelection()
            ddlUserGroup.Visible = False
            rfvUserGroup.Enabled = False
            btnDeleteAccountPaymentNatures.Visible = False
            btnApproveAccountPaymentNatures.Visible = False
            btnSave.Visible = False
        End If
    End Sub
    Protected Sub ddlUser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUser.SelectedIndexChanged
        Try
            If ddlUser.SelectedValue.ToString <> "0" Then
                If ddlUserType.SelectedValue.ToString = "Client User" Then
                    LoadddlUserGroup()
                    tblPaymentNaturesTreeView.Style.Add("display", "none")
                    tblUserRoles.Style.Add("display", "none")
                    tblUserRolesPaymentNature.Style.Add("display", "none")
                    btnApproveAccountPaymentNatures.Visible = False
                    btnDeleteAccountPaymentNatures.Visible = False
                    btnSave.Visible = False
                Else
                    Dim objICRole As New ICRole
                    ViewState("RoleID") = Nothing
                    ViewState("RoleID") = ddlUser.SelectedValue.ToString
                    objICRole.LoadByPrimaryKey(ddlUser.SelectedValue.ToString)
                    LoadgvPaymentNature("0", ddlUser.SelectedValue.ToString, True)
                    lblPaymentNatureList.Text = ""
                    lblPaymentNatureList.Text = objICRole.RoleName.ToString() & " s Account & Payment Nature List"
                    tblUserRolesPaymentNature.Style.Remove("display")
                    BindTreeView("Bank User")
                    If gvPaymentNature.Items.Count > 0 Then
                        gvPaymentNature.Visible = True
                        lblPaymentNatureList.Visible = True
                        lblRNFAPNList.Visible = False
                        CheckMarkAlreadyTaggedAccountPaymentNatureWithRole("0", ViewState("RoleID").ToString)
                        radtvAccountsPaymentNature.ExpandAllNodes()
                    Else
                        radtvAccountsPaymentNature.UncheckAllNodes()
                        radtvAccountsPaymentNature.CollapseAllNodes()
                        lblPaymentNatureList.Visible = True
                        gvPaymentNature.Visible = False
                        lblRNFAPNList.Visible = True
                    End If

                    lblAssignAccountPaymentNature.Text = ""
                    lblAssignAccountPaymentNature.Text = "Assign Account & Payment Nature to Role [ " & objICRole.RoleName.ToString() & " ]"
                    gvUserRoles.DataSource = Nothing
                    tblUserRoles.Style.Add("display", "none")
                  
                End If
            ElseIf ddlUser.SelectedValue.ToString = "0" Then
                gvPaymentNature.DataSource = Nothing
                gvUserRoles.DataSource = Nothing
                tblPaymentNaturesTreeView.Style.Add("display", "none")
                tblUserRoles.Style.Add("display", "none")
                tblUserRolesPaymentNature.Style.Add("display", "none")
                btnApproveAccountPaymentNatures.Visible = False
                btnDeleteAccountPaymentNatures.Visible = False
                btnSave.Visible = False
                LoadddlUserGroup()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Protected Sub ddlUserGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUserGroup.SelectedIndexChanged
        Try
            If ddlUserGroup.SelectedValue.ToString <> "0" Then
                tblPaymentNaturesTreeView.Style.Add("display", "none")
                tblUserRoles.Style.Add("display", "none")
                tblUserRolesPaymentNature.Style.Add("display", "none")
                SetgvUserRoles()
                btnApproveAccountPaymentNatures.Visible = False
                btnDeleteAccountPaymentNatures.Visible = False
                btnSave.Visible = False
            ElseIf ddlUserGroup.SelectedValue.ToString = "0" Then
                tblPaymentNaturesTreeView.Style.Add("display", "none")
                tblUserRoles.Style.Add("display", "none")
                tblUserRolesPaymentNature.Style.Add("display", "none")
                btnApproveAccountPaymentNatures.Visible = False
                btnDeleteAccountPaymentNatures.Visible = False
                btnSave.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    'Private Sub BindTreeView(ByVal UserID As String)
    '    Try
    '        radtvAccountsPaymentNature.Nodes.Clear()
    '        Dim objICUser As New ICUser
    '        Dim objICGroupColl As ICGroupCollection
    '        Dim objICCompanyColl As ICCompanyCollection
    '        Dim objICGroup As ICGroup
    '        Dim objICOfficeColl As ICOfficeCollection
    '        Dim dtBankBranch As New DataTable
    '        Dim radGroupNode, radCompanyNode, radAPNatureNode, radOfficeNode, radBrNode As RadTreeNode
    '        Dim dtAPNature As DataTable
    '        objICUser.es.Connection.CommandTimeout = 3600
    '        If UserID = "Bank User" Then
    '            objICGroupColl = New ICGroupCollection
    '            objICGroupColl = ICGroupController.GetAllActiveGroups
    '            dtBankBranch = ICOfficeController.GetPrincipalBankBranchForRoleAPNTagging
    '            For Each objICGroupInLoop As ICGroup In objICGroupColl
    '                radGroupNode = New RadTreeNode
    '                radGroupNode.Text = objICGroupInLoop.GroupName.ToString
    '                radGroupNode.Value = objICGroupInLoop.GroupCode.ToString
    '                objICCompanyColl = New ICCompanyCollection
    '                objICCompanyColl = ICCompanyController.GetAllActiveCompaniesByGroupCode(radGroupNode.Value.ToString)
    '                For Each objICCompany As ICCompany In objICCompanyColl
    '                    radCompanyNode = New RadTreeNode
    '                    dtAPNature = New DataTable
    '                    objICOfficeColl = New ICOfficeCollection
    '                    objICOfficeColl.es.Connection.CommandTimeout = 3600
    '                    radCompanyNode.Text = objICCompany.CompanyName.ToString
    '                    radCompanyNode.Value = objICCompany.CompanyCode
    '                    dtAPNature = ICAccountPaymentNatureController.GetAccountPaymentNatureByCompanyCode(radCompanyNode.Value.ToString)
    '                    objICOfficeColl = ICOfficeController.GetOfficeByCompanyCodeActiveAndApproveSecond(radCompanyNode.Value.ToString)
    '                    If dtAPNature.Rows.Count > 0 Then
    '                        For Each dr As DataRow In dtAPNature.Rows
    '                            radAPNatureNode = New RadTreeNode
    '                            radAPNatureNode.Text = dr("AccountAndPaymentNature")
    '                            radAPNatureNode.Value = dr("AccountPaymentNature")

    '                            If dtBankBranch.Rows.Count > 0 Then
    '                                For Each drBranch As DataRow In dtBankBranch.Rows
    '                                    radBrNode = New RadTreeNode
    '                                    radBrNode.Text = drBranch(1).ToString
    '                                    radBrNode.Value = drBranch(0).ToString
    '                                    radAPNatureNode.Nodes.Add(radBrNode)
    '                                Next
    '                            End If
    '                            If objICOfficeColl.Count > 0 Then
    '                                For Each objICOffice As ICOffice In objICOfficeColl
    '                                    radOfficeNode = New RadTreeNode
    '                                    radOfficeNode.Text = objICOffice.OfficeName.ToString
    '                                    radOfficeNode.Value = objICOffice.OfficeID.ToString
    '                                    radAPNatureNode.Nodes.Add(radOfficeNode)
    '                                Next
    '                            End If

    '                            radCompanyNode.Nodes.Add(radAPNatureNode)
    '                        Next
    '                    End If
    '                    radGroupNode.Nodes.Add(radCompanyNode)
    '                Next

    '                radMainTreeNode.Nodes.Add(radGroupNode)
    '            Next

    '            radtvAccountsPaymentNature.Nodes.Add(radMainTreeNode)
    '            radtvAccountsPaymentNature.CheckBoxes = True

    '            If radtvAccountsPaymentNature.Nodes.Count > 0 Then
    '                tblPaymentNaturesTreeView.Style.Remove("display")
    '                btnSave.Visible = CBool(htRights("Tag"))
    '            Else
    '                tblPaymentNaturesTreeView.Style.Add("display", "none")
    '                btnSave.Visible = False
    '            End If
    '        Else
    '            objICUser.LoadByPrimaryKey(UserID)

    '            objICGroup = New ICGroup
    '            objICGroup.es.Connection.CommandTimeout = 3600
    '            objICGroup = objICUser.UpToICOfficeByOfficeCode.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode
    '            objICCompanyColl = New ICCompanyCollection
    '            objICCompanyColl.es.Connection.CommandTimeout = 3600
    '            objICCompanyColl = ICCompanyController.GetAllActiveCompaniesByGroupCode(objICGroup.GroupCode.ToString)
    '            radGroupNode = New RadTreeNode
    '            radGroupNode.Text = objICGroup.GroupName.ToString
    '            radGroupNode.Value = objICGroup.GroupCode.ToString
    '            For Each objICCompany As ICCompany In objICCompanyColl
    '                radCompanyNode = New RadTreeNode
    '                dtAPNature = New DataTable
    '                objICOfficeColl = New ICOfficeCollection
    '                objICOfficeColl.es.Connection.CommandTimeout = 3600
    '                radCompanyNode.Text = objICCompany.CompanyName.ToString
    '                radCompanyNode.Value = objICCompany.CompanyCode
    '                dtAPNature = ICAccountPaymentNatureController.GetAccountPaymentNatureByCompanyCode(radCompanyNode.Value.ToString)
    '                objICOfficeColl = ICOfficeController.GetOfficeByCompanyCodeActiveAndApproveSecond(radCompanyNode.Value.ToString)
    '                If dtAPNature.Rows.Count > 0 Then
    '                    For Each dr As DataRow In dtAPNature.Rows
    '                        radAPNatureNode = New RadTreeNode
    '                        radAPNatureNode.Text = dr("AccountAndPaymentNature")
    '                        radAPNatureNode.Value = dr("AccountPaymentNature")



    '                        If objICOfficeColl.Count > 0 Then
    '                            For Each objICOffice As ICOffice In objICOfficeColl
    '                                radOfficeNode = New RadTreeNode
    '                                radOfficeNode.Text = objICOffice.OfficeName.ToString
    '                                radOfficeNode.Value = objICOffice.OfficeID.ToString
    '                                radAPNatureNode.Nodes.Add(radOfficeNode)
    '                            Next
    '                        End If
    '                        radCompanyNode.Nodes.Add(radAPNatureNode)
    '                    Next
    '                End If
    '                radGroupNode.Nodes.Add(radCompanyNode)
    '            Next
    '            radMainTreeNode.Nodes.Add(radGroupNode)
    '            radtvAccountsPaymentNature.Nodes.Add(radMainTreeNode)
    '            radtvAccountsPaymentNature.CheckBoxes = True

    '            If radtvAccountsPaymentNature.Nodes.Count > 0 Then
    '                tblPaymentNaturesTreeView.Style.Remove("display")
    '                btnSave.Visible = CBool(htRights("Tag"))
    '            Else
    '                tblPaymentNaturesTreeView.Style.Add("display", "none")
    '                btnSave.Visible = False
    '            End If
    '        End If

    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    Private Sub BindTreeView(ByVal UserID As String)
        Try
            radtvAccountsPaymentNature.Nodes.Clear()
            Dim objICUser As New ICUser
            Dim objICGroupColl As ICGroupCollection
            Dim objICCompanyColl As ICCompanyCollection
            Dim objICGroup As ICGroup
            Dim objICOfficeColl As ICOfficeCollection
            Dim dtBankBranch As New DataTable
            Dim radGroupNode, radCompanyNode, radAPNatureNode, radOfficeNode, radBrNode As RadTreeNode
            ' Aizaz Work [Dated: 01-Nov-2013]
            Dim dtALLAPNature As New DataTable
            Dim dtALLOffices As New DataTable
            Dim arrRowAPNature As DataRow()
            Dim arrRowClientOffices As DataRow()


            objICUser.es.Connection.CommandTimeout = 3600
            If UserID = "Bank User" Then
                objICGroupColl = New ICGroupCollection
                objICGroupColl = ICGroupController.GetAllActiveGroups
                dtBankBranch = ICOfficeController.GetPrincipalBankBranchForRoleAPNTagging
                For Each objICGroupInLoop As ICGroup In objICGroupColl
                    radGroupNode = New RadTreeNode
                    radGroupNode.Text = objICGroupInLoop.GroupName.ToString
                    radGroupNode.Value = objICGroupInLoop.GroupCode.ToString
                    objICCompanyColl = New ICCompanyCollection
                    objICCompanyColl = ICCompanyController.GetAllActiveCompaniesByGroupCode(radGroupNode.Value.ToString)

                    ' Aizaz Work [Dated: 01-Nov-2013]

                    dtALLAPNature = ICAccountPaymentNatureController.GetAllAccountPaymentNatureWithCompanyCode()
                    dtALLOffices = ICOfficeController.GetAllOfficeWithCompanyCodeActiveAndApprove()



                    For Each objICCompany As ICCompany In objICCompanyColl
                        radCompanyNode = New RadTreeNode
                        'dtALLAPNature = New DataTable
                        objICOfficeColl = New ICOfficeCollection
                        objICOfficeColl.es.Connection.CommandTimeout = 3600
                        radCompanyNode.Text = objICCompany.CompanyName.ToString
                        radCompanyNode.Value = objICCompany.CompanyCode


                        ' Aizaz Work [Dated: 01-Nov-2013]
                        arrRowAPNature = Nothing
                        arrRowClientOffices = Nothing

                        arrRowAPNature = dtALLAPNature.Select("CompanyCode = '" & objICCompany.CompanyCode & "'", "AccountAndPaymentNature")
                        arrRowClientOffices = dtALLOffices.Select("CompanyCode = '" & objICCompany.CompanyCode & "'", "OfficeName")

                        If arrRowAPNature.Count > 0 Then
                            For Each dr As DataRow In arrRowAPNature
                                radAPNatureNode = New RadTreeNode
                                radAPNatureNode.Text = dr("AccountAndPaymentNature")
                                radAPNatureNode.Value = dr("AccountPaymentNature")

                                If dtBankBranch.Rows.Count > 0 Then
                                    For Each drBranch As DataRow In dtBankBranch.Rows
                                        radBrNode = New RadTreeNode
                                        radBrNode.Text = drBranch(1).ToString
                                        radBrNode.Value = drBranch(0).ToString
                                        radAPNatureNode.Nodes.Add(radBrNode)
                                    Next
                                End If
                                If arrRowClientOffices.Count > 0 Then
                                    For Each drCO As DataRow In arrRowClientOffices
                                        radOfficeNode = New RadTreeNode
                                        radOfficeNode.Text = drCO("OfficeName").ToString
                                        radOfficeNode.Value = drCO("OfficeID").ToString
                                        radAPNatureNode.Nodes.Add(radOfficeNode)
                                    Next
                                End If

                                radCompanyNode.Nodes.Add(radAPNatureNode)
                            Next
                        End If
                        radGroupNode.Nodes.Add(radCompanyNode)
                    Next

                    radMainTreeNode.Nodes.Add(radGroupNode)
                Next

                radtvAccountsPaymentNature.Nodes.Add(radMainTreeNode)
                radtvAccountsPaymentNature.CheckBoxes = True

                If radtvAccountsPaymentNature.Nodes.Count > 0 Then
                    tblPaymentNaturesTreeView.Style.Remove("display")
                    btnSave.Visible = CBool(htRights("Tag"))
                Else
                    tblPaymentNaturesTreeView.Style.Add("display", "none")
                    btnSave.Visible = False
                End If
            Else
                objICUser.LoadByPrimaryKey(UserID)

                objICGroup = New ICGroup
                objICGroup.es.Connection.CommandTimeout = 3600
                objICGroup = objICUser.UpToICOfficeByOfficeCode.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode
                objICCompanyColl = New ICCompanyCollection
                objICCompanyColl.es.Connection.CommandTimeout = 3600
                objICCompanyColl = ICCompanyController.GetAllActiveCompaniesByGroupCode(objICGroup.GroupCode.ToString)
                radGroupNode = New RadTreeNode
                radGroupNode.Text = objICGroup.GroupName.ToString
                radGroupNode.Value = objICGroup.GroupCode.ToString
                For Each objICCompany As ICCompany In objICCompanyColl
                    radCompanyNode = New RadTreeNode
                    dtALLAPNature = New DataTable
                    objICOfficeColl = New ICOfficeCollection
                    objICOfficeColl.es.Connection.CommandTimeout = 3600
                    radCompanyNode.Text = objICCompany.CompanyName.ToString
                    radCompanyNode.Value = objICCompany.CompanyCode
                    dtALLAPNature = ICAccountPaymentNatureController.GetAccountPaymentNatureByCompanyCode(radCompanyNode.Value.ToString)
                    objICOfficeColl = ICOfficeController.GetOfficeByCompanyCodeActiveAndApproveSecond(radCompanyNode.Value.ToString)
                    If dtALLAPNature.Rows.Count > 0 Then
                        For Each dr As DataRow In dtALLAPNature.Rows
                            radAPNatureNode = New RadTreeNode
                            radAPNatureNode.Text = dr("AccountAndPaymentNature")
                            radAPNatureNode.Value = dr("AccountPaymentNature")



                            If objICOfficeColl.Count > 0 Then
                                For Each objICOffice As ICOffice In objICOfficeColl
                                    radOfficeNode = New RadTreeNode
                                    radOfficeNode.Text = objICOffice.OfficeName.ToString
                                    radOfficeNode.Value = objICOffice.OfficeID.ToString
                                    radAPNatureNode.Nodes.Add(radOfficeNode)
                                Next
                            End If
                            radCompanyNode.Nodes.Add(radAPNatureNode)
                        Next
                    End If
                    radGroupNode.Nodes.Add(radCompanyNode)
                Next
                radMainTreeNode.Nodes.Add(radGroupNode)
                radtvAccountsPaymentNature.Nodes.Add(radMainTreeNode)
                radtvAccountsPaymentNature.CheckBoxes = True

                If radtvAccountsPaymentNature.Nodes.Count > 0 Then
                    tblPaymentNaturesTreeView.Style.Remove("display")
                    btnSave.Visible = CBool(htRights("Tag"))
                Else
                    tblPaymentNaturesTreeView.Style.Add("display", "none")
                    btnSave.Visible = False
                End If
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                If CheckIsAtLeastOneAccountPaymentNatureSelected() = False Then
                    UIUtilities.ShowDialog(Me, "Error", "Please select at least one account payment nature", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If ddlUserType.SelectedValue.ToString = "Bank User" Then
                    Dim ArrayListUSers As New ArrayList
                    Dim objICRole As New ICRole
                    Dim objRoleController As New RoleController
                    objICRole.LoadByPrimaryKey(ViewState("RoleID").ToString)
                    ArrayListUSers = objRoleController.GetUsersInRole(Me.PortalId, objICRole.RoleName)
                    If ArrayListUSers.Count = 0 Then
                        UIUtilities.ShowDialog(Me, "Error", "No users are assigned to role.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    DeleteOldTaggingByRoleIDAndUserID(ViewState("RoleID").ToString, "0")
                ElseIf ddlUserType.SelectedValue.ToString = "Client User" Then
                    DeleteOldTaggingByRoleIDAndUserID(ViewState("RoleID").ToString, ddlUserGroup.SelectedValue.ToString)
                End If
                SaveUserRoleAccountPaymentNatureTagged()

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Sub SaveUserRoleAccountPaymentNatureTagged()
        Try
            Dim PassedAPNature As String = ""
            Dim objICUserRolesAPNature As ICUserRolesAccountAndPaymentNature
            Dim objICUserRolesPrintLocation As ICUserRolesAccountPaymentNatureAndLocations
            Dim objICUserRoleAPNColl As New ICUserRolesAccountAndPaymentNatureCollection
            Dim objICUserRoleAPNLocationColl As New ICUserRolesAccountPaymentNatureAndLocationsCollection
            Dim objICAuditTrailColl As New ICAuditTrailCollection
            Dim objICAuditTrail As ICAuditTrail
            Dim StrAuditTrailAction As String = Nothing




            Dim PassToSave As Integer = 0
            Dim FailToSave As Integer = 0
            Dim ArrayListUSers As New ArrayList
            Dim objRoleController As New RoleController
            If ddlUserType.SelectedValue.ToString = "Client User" Then
                For Each node As RadTreeNode In radtvAccountsPaymentNature.Nodes
                    If node.Checked = True And node.Nodes.Count > 0 And Not node.Value.ToString <> "top" Then
                        If node.Nodes.Count > 0 Then
                            For Each radGroupNode As RadTreeNode In node.Nodes
                                If radGroupNode.Nodes.Count > 0 Then
                                    If radGroupNode.Checked = True Then
                                        For Each radCompanyNode As RadTreeNode In radGroupNode.Nodes
                                            If radCompanyNode.Nodes.Count > 0 Then
                                                If radCompanyNode.Checked = True Then
                                                    For Each radAPNarureNode As RadTreeNode In radCompanyNode.Nodes

                                                        
                                                        If radAPNarureNode.Checked = True Then
                                                            objICAuditTrail = New ICAuditTrail
                                                            objICUserRolesAPNature = New ICUserRolesAccountAndPaymentNature
                                                            Dim iCount As Integer = 0
                                                            'If PassedAPNature.ToString <> radAPNarureNode.Text.ToString Then
                                                            'If CheckIsAtLeastOneChildNodeIsChecked(radAPNarureNode) = False Then
                                                            '    FailToSave = FailToSave + 1
                                                            '    Continue For
                                                            'End If

                                                            'objICAuditTrail = New ICAuditTrail

                                                            objICUserRolesAPNature.AccountNumber = radAPNarureNode.Value.ToString.Split("-")(0)
                                                            objICUserRolesAPNature.BranchCode = radAPNarureNode.Value.ToString.Split("-")(1)
                                                            objICUserRolesAPNature.Currency = radAPNarureNode.Value.ToString.Split("-")(2)
                                                            objICUserRolesAPNature.PaymentNatureCode = radAPNarureNode.Value.ToString.Split("-")(3)
                                                            objICUserRolesAPNature.RolesID = CInt(ViewState("RoleID"))
                                                            objICUserRolesAPNature.UserID = ddlUserGroup.SelectedValue.ToString
                                                            objICUserRolesAPNature.CreateBy = Me.UserId.ToString
                                                            objICUserRolesAPNature.CreateDate = Date.Now
                                                            objICUserRolesAPNature.Creater = Me.UserId.ToString
                                                            objICUserRolesAPNature.CreationDate = Date.Now
                                                            'ICUserRolesPaymentNatureController.AddUserRolesPaymentNature(objICUserRolesAPNature, False, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Client")
                                                            objICUserRoleAPNColl.Add(objICUserRolesAPNature)
                                                            StrAuditTrailAction = Nothing
                                                            StrAuditTrailAction = "Account [ " & objICUserRolesAPNature.AccountNumber & " ] payment nature [ " & objICUserRolesAPNature.PaymentNatureCode & " ] "
                                                            StrAuditTrailAction += " of group [ " & objICUserRolesAPNature.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupCode & " ] for role [ " & objICUserRolesAPNature.UpToICRoleByRolesID.RoleID & " ]"
                                                            StrAuditTrailAction += " for user [ " & objICUserRolesAPNature.UpToICUserByUserID.UserName & " ] added "
                                                            
                                                            PassToSave = PassToSave + 1
                                                            'If radAPNarureNode.Nodes.Count > 0 Then
                                                            For Each radLocationNode As RadTreeNode In radAPNarureNode.Nodes
                                                                If iCount = 0 Then
                                                                    StrAuditTrailAction += " with location [ "
                                                                    iCount = iCount + 1
                                                                End If
                                                                If radLocationNode.Checked = True Then

                                                                    objICUserRolesPrintLocation = New ICUserRolesAccountPaymentNatureAndLocations
                                                                    objICUserRolesPrintLocation.AccountNumber = objICUserRolesAPNature.AccountNumber
                                                                    objICUserRolesPrintLocation.BranchCode = objICUserRolesAPNature.BranchCode
                                                                    objICUserRolesPrintLocation.Currency = objICUserRolesAPNature.Currency
                                                                    objICUserRolesPrintLocation.PaymentNatureCode = objICUserRolesAPNature.PaymentNatureCode
                                                                    objICUserRolesPrintLocation.OfficeID = radLocationNode.Value
                                                                    objICUserRolesPrintLocation.UserID = objICUserRolesAPNature.UserID
                                                                    objICUserRolesPrintLocation.RoleID = objICUserRolesAPNature.RolesID
                                                                    objICUserRolesPrintLocation.CreatedBy = Me.UserId.ToString
                                                                    objICUserRolesPrintLocation.CreatedDate = Date.Now
                                                                    objICUserRolesPrintLocation.Creater = Me.UserId.ToString
                                                                    objICUserRolesPrintLocation.CreationDate = Date.Now
                                                                    StrAuditTrailAction += radLocationNode.Value.ToString & ";"
                                                                    objICUserRoleAPNLocationColl.Add(objICUserRolesPrintLocation)
                                                                    'ICUserRolesAccountPaymentNatureAndLocationsController.AddUserRolesPaymentNatureAndLocations(objICUserRolesPrintLocation, False, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Client")

                                                                End If
                                                            Next
                                                            If StrAuditTrailAction.Contains(";") = True Then
                                                                StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                                                                StrAuditTrailAction += " ]."
                                                            Else
                                                                StrAuditTrailAction += "."
                                                            End If
                                                            objICAuditTrail.AuditAction = StrAuditTrailAction
                                                            objICAuditTrail.AuditType = "User Role Account Payment Nature"
                                                            objICAuditTrail.RelatedID = objICUserRolesAPNature.AccountNumber.ToString + "" + objICUserRolesAPNature.BranchCode.ToString + "" + objICUserRolesAPNature.Currency.ToString + "" + objICUserRolesAPNature.PaymentNatureCode.ToString
                                                            objICAuditTrail.AuditDate = Date.Now
                                                            objICAuditTrail.UserID = Me.UserId
                                                            objICAuditTrail.Username = Me.UserInfo.Username.ToString
                                                            objICAuditTrail.ActionType = "ADD"
                                                            If Not HttpContext.Current Is Nothing Then
                                                                objICAuditTrail.IPAddress = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                                                            End If
                                                            objICAuditTrailColl.Add(objICAuditTrail)
                                                        End If
                                                    Next
                                                End If
                                            End If
                                        Next
                                    End If
                                End If
                            Next
                        End If
                    End If
                Next
            ElseIf ddlUserType.SelectedValue.ToString = "Bank User" Then
                Dim objICRole As New ICRole
                Dim objRoleInfo As UserRoleInfo
                objICRole.LoadByPrimaryKey(ViewState("RoleID").ToString)
                ArrayListUSers = objRoleController.GetUsersInRole(Me.PortalId, objICRole.RoleName)
                If ArrayListUSers.Count > 0 Then
                    For i = 0 To ArrayListUSers.Count - 1
                        objRoleInfo = New UserRoleInfo
                        objRoleInfo = ArrayListUSers(i)
                        For Each node As RadTreeNode In radtvAccountsPaymentNature.Nodes
                            If node.Checked = True And node.Nodes.Count > 0 And Not node.Value.ToString <> "top" Then
                                If node.Nodes.Count > 0 Then
                                    For Each radGroupNode As RadTreeNode In node.Nodes
                                        If radGroupNode.Nodes.Count > 0 Then
                                            If radGroupNode.Checked = True Then
                                                For Each radCompanyNode As RadTreeNode In radGroupNode.Nodes
                                                    If radCompanyNode.Nodes.Count > 0 Then
                                                        If radCompanyNode.Checked = True Then
                                                            For Each radAPNarureNode As RadTreeNode In radCompanyNode.Nodes
                                                                objICAuditTrail = New ICAuditTrail
                                                                objICUserRolesAPNature = New ICUserRolesAccountAndPaymentNature
                                                                Dim iCount As Integer = 0
                                                                If radAPNarureNode.Checked = True Then
                                                                    'If PassedAPNature.ToString <> radAPNarureNode.Text.ToString Then
                                                                    'If CheckIsAtLeastOneChildNodeIsChecked(radAPNarureNode) = False Then
                                                                    '    FailToSave = FailToSave + 1
                                                                    '    Continue For
                                                                    'End If

                                                                    objICUserRolesAPNature.AccountNumber = radAPNarureNode.Value.ToString.Split("-")(0)
                                                                    objICUserRolesAPNature.BranchCode = radAPNarureNode.Value.ToString.Split("-")(1)
                                                                    objICUserRolesAPNature.Currency = radAPNarureNode.Value.ToString.Split("-")(2)
                                                                    objICUserRolesAPNature.PaymentNatureCode = radAPNarureNode.Value.ToString.Split("-")(3)
                                                                    objICUserRolesAPNature.RolesID = CInt(ViewState("RoleID"))
                                                                    objICUserRolesAPNature.UserID = objRoleInfo.UserID
                                                                    objICUserRolesAPNature.CreateBy = Me.UserId.ToString
                                                                    objICUserRolesAPNature.CreateDate = Date.Now
                                                                    objICUserRolesAPNature.Creater = Me.UserId.ToString
                                                                    objICUserRolesAPNature.CreationDate = Date.Now
                                                                    objICUserRoleAPNColl.Add(objICUserRolesAPNature)
                                                                    'ICUserRolesPaymentNatureController.AddUserRolesPaymentNature(objICUserRolesAPNature, False, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Bank")
                                                                    StrAuditTrailAction = Nothing
                                                                    StrAuditTrailAction = "Account [ " & objICUserRolesAPNature.AccountNumber & " ] payment nature [ " & objICUserRolesAPNature.PaymentNatureCode & " ] "
                                                                    StrAuditTrailAction += " of group [ " & objICUserRolesAPNature.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupCode & " ] for role [ " & objICUserRolesAPNature.UpToICRoleByRolesID.RoleID & " ]"
                                                                    StrAuditTrailAction += " for user [ " & objICUserRolesAPNature.UpToICUserByUserID.UserName & " ] added"
                                                                    
                                                                    PassToSave = PassToSave + 1
                                                                    'If radAPNarureNode.Nodes.Count > 0 Then
                                                                    For Each radLocationNode As RadTreeNode In radAPNarureNode.Nodes

                                                                        If radLocationNode.Checked = True Then
                                                                            If iCount = 0 Then
                                                                                StrAuditTrailAction += " with location [ "
                                                                                iCount = iCount + 1
                                                                            End If
                                                                            objICAuditTrail = New ICAuditTrail
                                                                            objICUserRolesPrintLocation = New ICUserRolesAccountPaymentNatureAndLocations
                                                                            objICUserRolesPrintLocation.AccountNumber = objICUserRolesAPNature.AccountNumber
                                                                            objICUserRolesPrintLocation.BranchCode = objICUserRolesAPNature.BranchCode
                                                                            objICUserRolesPrintLocation.Currency = objICUserRolesAPNature.Currency
                                                                            objICUserRolesPrintLocation.PaymentNatureCode = objICUserRolesAPNature.PaymentNatureCode
                                                                            objICUserRolesPrintLocation.OfficeID = radLocationNode.Value
                                                                            objICUserRolesPrintLocation.UserID = objICUserRolesAPNature.UserID
                                                                            objICUserRolesPrintLocation.RoleID = objICUserRolesAPNature.RolesID
                                                                            objICUserRolesPrintLocation.CreatedBy = Me.UserId.ToString
                                                                            objICUserRolesPrintLocation.CreatedDate = Date.Now
                                                                            objICUserRolesPrintLocation.Creater = Me.UserId.ToString
                                                                            objICUserRolesPrintLocation.CreationDate = Date.Now
                                                                            StrAuditTrailAction += radLocationNode.Value.ToString & ";"
                                                                            objICUserRoleAPNLocationColl.Add(objICUserRolesPrintLocation)
                                                                            'ICUserRolesAccountPaymentNatureAndLocationsController.AddUserRolesPaymentNatureAndLocations(objICUserRolesPrintLocation, False, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Bank")

                                                                        End If
                                                                    Next
                                                                    'End If
                                                                    'End If
                                                                    If StrAuditTrailAction.Contains(";") = True Then
                                                                        StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                                                                        StrAuditTrailAction += " ]."
                                                                    Else
                                                                        StrAuditTrailAction += "."
                                                                    End If
                                                                    objICAuditTrail.AuditAction = StrAuditTrailAction
                                                                    objICAuditTrail.AuditType = "User Role Account Payment Nature"
                                                                    objICAuditTrail.RelatedID = objICUserRolesAPNature.AccountNumber.ToString + "" + objICUserRolesAPNature.BranchCode.ToString + "" + objICUserRolesAPNature.Currency.ToString + "" + objICUserRolesAPNature.PaymentNatureCode.ToString
                                                                    objICAuditTrail.AuditDate = Date.Now
                                                                    objICAuditTrail.UserID = Me.UserId
                                                                    objICAuditTrail.Username = Me.UserInfo.Username.ToString
                                                                    objICAuditTrail.ActionType = "ADD"
                                                                    If Not HttpContext.Current Is Nothing Then
                                                                        objICAuditTrail.IPAddress = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                                                                    End If
                                                                    objICAuditTrailColl.Add(objICAuditTrail)
                                                                End If


                                                            Next

                                                        End If

                                                    End If
                                                Next
                                            End If


                                        End If
                                    Next


                                End If


                            End If
                        Next

                    Next
                End If
            End If
            If objICUserRoleAPNColl.Count > 0 Then
                objICUserRoleAPNColl.Save()
            End If
            If objICUserRoleAPNLocationColl.Count > 0 Then
                objICUserRoleAPNLocationColl.Save()
            End If
            If objICAuditTrailColl.Count > 0 Then
                objICAuditTrailColl.Save()
            End If
            If Not FailToSave = 0 And Not PassToSave = 0 Then
                UIUtilities.ShowDialog(Me, "User Roles Account Payment Nature Tagging", PassToSave & " Account payment nature tagged successfully. <br />" & FailToSave & " Account payment nature not tagged successfully due to following reasons:<br /> 1. Account payment nature and location is not selected", ICBO.IC.Dialogmessagetype.Success)
            ElseIf Not FailToSave = 0 And PassToSave = 0 Then
                UIUtilities.ShowDialog(Me, "User Roles Account Payment Nature Tagging", "Account payment nature not tagged successfully due to following reasons:<br /> 1. Account payment nature and location is not selected", ICBO.IC.Dialogmessagetype.Failure)
            ElseIf FailToSave = 0 And Not PassToSave = 0 Then
                UIUtilities.ShowDialog(Me, "User Roles Account Payment Nature Tagging", PassToSave & " Account payment nature tagged successfully.", ICBO.IC.Dialogmessagetype.Success)
            End If
            If ddlUserType.SelectedValue.ToString = "Bank User" Then
                LoadgvPaymentNature("0", ViewState("RoleID").ToString, True)
                BindTreeView("Bank User")
                If gvPaymentNature.Items.Count > 0 Then
                    CheckMarkAlreadyTaggedAccountPaymentNatureWithRole("0", ViewState("RoleID").ToString)
                    radtvAccountsPaymentNature.ExpandAllNodes()
                Else
                    radtvAccountsPaymentNature.CheckChildNodes = False
                    radtvAccountsPaymentNature.CollapseAllNodes()
                End If
            ElseIf ddlUserType.SelectedValue.ToString = "Client User" Then
                LoadgvPaymentNature(ddlUserGroup.SelectedValue.ToString, ViewState("RoleID").ToString, True)
                BindTreeView(ddlUserGroup.SelectedValue.ToString)
                If gvPaymentNature.Items.Count > 0 Then
                    CheckMarkAlreadyTaggedAccountPaymentNatureWithRole(ddlUserGroup.SelectedValue.ToString, ViewState("RoleID").ToString)
                    radtvAccountsPaymentNature.ExpandAllNodes()
                Else
                    radtvAccountsPaymentNature.CollapseAllNodes()
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnDeleteAccountPaymentNatures_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteAccountPaymentNatures.Click

        Try
            Dim AccountNumber, BranchCode, Currency, PaymentNatureCode, UserID, RoleID As String
            Dim PassToDeleteCount As Integer = 0
            Dim FailToDeleteCount As Integer = 0
            Dim objICUserRolesAPNatureColl As ICUserRolesAccountAndPaymentNatureCollection
            Dim objICUserRolesAPNatureLocationsColl As ICUserRolesAccountPaymentNatureAndLocationsCollection
            Dim objICUserRolesAPNatureCollForDelete As New ICUserRolesAccountAndPaymentNatureCollection
            Dim objICUserRolesAPNatureLocationsCollForDelete As New ICUserRolesAccountPaymentNatureAndLocationsCollection
            Dim TaggingType As String = Nothing
            Dim objICAuditTrailColl As New ICAuditTrailCollection
            Dim objICAuditTrail As ICAuditTrail
            Dim StrAuditTrailAction As String = Nothing
            Dim chkSelect As CheckBox
            If CheckgvPNatureForProcessAll() = False Then
                UIUtilities.ShowDialog(Me, "User Roles Account Payment Nature Tagging", "Please select atleast one record.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            If ddlUserType.SelectedValue = "Bank User" Then
                TaggingType = "Bank"
            Else
                TaggingType = "Client"
            End If
            For Each gvUserRolesAPNatureRow As GridDataItem In gvPaymentNature.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvUserRolesAPNatureRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    AccountNumber = Nothing
                    BranchCode = Nothing
                    Currency = Nothing
                    PaymentNatureCode = Nothing
                    UserID = Nothing
                    RoleID = Nothing
                    AccountNumber = gvUserRolesAPNatureRow.GetDataKeyValue("AccountNumber")
                    BranchCode = gvUserRolesAPNatureRow.GetDataKeyValue("BranchCode")
                    Currency = gvUserRolesAPNatureRow.GetDataKeyValue("Currency")
                    PaymentNatureCode = gvUserRolesAPNatureRow.GetDataKeyValue("PaymentNatureCode")
                    RoleID = gvUserRolesAPNatureRow.GetDataKeyValue("RolesID")
                    If ddlUserType.SelectedValue = "Client User" Then
                        UserID = ddlUserGroup.SelectedValue.ToString
                    Else
                        UserID = "0"
                    End If
                    objICUserRolesAPNatureColl = New ICUserRolesAccountAndPaymentNatureCollection
                    objICUserRolesAPNatureColl.es.Connection.CommandTimeout = 3600
                    objICUserRolesAPNatureColl = ICUserRolesPaymentNatureController.GetAllUserRolesAccountPaymentNature(PaymentNatureCode, AccountNumber, BranchCode, Currency, RoleID, UserID)
                    If objICUserRolesAPNatureColl.Count > 0 Then
                        For Each objICUserRoleAPNature As ICUserRolesAccountAndPaymentNature In objICUserRolesAPNatureColl
                            objICAuditTrail = New ICAuditTrail
                            StrAuditTrailAction = Nothing
                            StrAuditTrailAction = "Account [ " & objICUserRoleAPNature.AccountNumber & " ] payment nature [ " & objICUserRoleAPNature.PaymentNatureCode & "] "
                            StrAuditTrailAction += " of group [ " & objICUserRoleAPNature.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupCode & " ] for role [ " & objICUserRoleAPNature.UpToICRoleByRolesID.RoleID & " ]"
                            StrAuditTrailAction += " for user [ " & objICUserRoleAPNature.UpToICUserByUserID.UserName & " ] deleted "
                            Dim ICount As Integer = 0
                            Try
                                objICUserRolesAPNatureLocationsColl = New ICUserRolesAccountPaymentNatureAndLocationsCollection
                                objICUserRolesAPNatureLocationsColl.es.Connection.CommandTimeout = 3600
                                objICUserRolesAPNatureLocationsColl = ICUserRolesAccountPaymentNatureAndLocationsController.GetAllUserRolesAccountPaymentNatureLocations(objICUserRoleAPNature.PaymentNatureCode, objICUserRoleAPNature.AccountNumber, objICUserRoleAPNature.BranchCode, objICUserRoleAPNature.Currency, objICUserRoleAPNature.RolesID, objICUserRoleAPNature.UserID)
                                If objICUserRolesAPNatureLocationsColl.Count > 0 Then
                                    For Each objICUserRoleAPNatureLocations As ICUserRolesAccountPaymentNatureAndLocations In objICUserRolesAPNatureLocationsColl
                                        If ICount = 0 Then
                                            StrAuditTrailAction += "with location [ "
                                            ICount = ICount + 1
                                        End If
                                        StrAuditTrailAction += objICUserRoleAPNatureLocations.OfficeID.ToString & ";"
                                        'ICUserRolesAccountPaymentNatureAndLocationsController.DeleteUserRolesAPNaturesLocations(objICUserRoleAPNatureLocations, Me.UserId.ToString, Me.UserInfo.Username.ToString, TaggingType)
                                        objICUserRolesAPNatureLocationsCollForDelete.Add(objICUserRoleAPNatureLocations)
                                    Next
                                End If
                                If StrAuditTrailAction.Contains(";") = True Then
                                    StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                                    StrAuditTrailAction += " ]."
                                Else
                                    StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                                    StrAuditTrailAction += " ."
                                End If
                                objICAuditTrail.AuditAction = StrAuditTrailAction
                                objICAuditTrail.AuditType = "User Role Account Payment Nature"
                                objICAuditTrail.RelatedID = objICUserRoleAPNature.AccountNumber.ToString + "" + objICUserRoleAPNature.BranchCode.ToString + "" + objICUserRoleAPNature.Currency.ToString + "" + objICUserRoleAPNature.PaymentNatureCode.ToString
                                objICAuditTrail.AuditDate = Date.Now
                                objICAuditTrail.UserID = Me.UserId
                                objICAuditTrail.Username = Me.UserInfo.Username.ToString
                                objICAuditTrail.ActionType = "DELETE"
                                If Not HttpContext.Current Is Nothing Then
                                    objICAuditTrail.IPAddress = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                                End If
                                objICUserRolesAPNatureCollForDelete.Add(objICUserRoleAPNature)
                                'ICUserRolesPaymentNatureController.DeleteUserRolesPaymentNature(objICUserRoleAPNature, Me.UserId.ToString, Me.UserInfo.Username.ToString, TaggingType)
                                objICAuditTrailColl.Add(objICAuditTrail)
                                PassToDeleteCount = PassToDeleteCount + 1
                            Catch ex As Exception
                                FailToDeleteCount = FailToDeleteCount + 1
                                Continue For
                            End Try
                        Next
                    End If
                End If
            Next
            If objICUserRolesAPNatureLocationsCollForDelete.Count > 0 Then
                objICUserRolesAPNatureLocationsCollForDelete.MarkAllAsDeleted()
                objICUserRolesAPNatureLocationsCollForDelete.Save()
            End If
            If objICUserRolesAPNatureCollForDelete.Count > 0 Then
                objICUserRolesAPNatureCollForDelete.MarkAllAsDeleted()
                objICUserRolesAPNatureCollForDelete.Save()
            End If
            If objICAuditTrailColl.Count > 0 Then
                objICAuditTrailColl.Save()
            End If
            If Not PassToDeleteCount = 0 And Not FailToDeleteCount = 0 Then
                If ddlUserType.SelectedValue.ToString = "Bank User" Then
                    LoadgvPaymentNature("0", ViewState("RoleID").ToString, True)
                    If gvPaymentNature.Items.Count > 0 Then
                        CheckMarkAlreadyTaggedAccountPaymentNatureWithRole("0", ViewState("RoleID").ToString)
                    Else
                        radtvAccountsPaymentNature.UncheckAllNodes()
                        radtvAccountsPaymentNature.CollapseAllNodes()
                    End If
                ElseIf ddlUserType.SelectedValue.ToString = "Client User" Then
                    LoadgvPaymentNature(ddlUserGroup.SelectedValue.ToString, ViewState("RoleID").ToString, True)
                    If gvPaymentNature.Items.Count > 0 Then
                        CheckMarkAlreadyTaggedAccountPaymentNatureWithRole(ddlUser.SelectedValue.ToString, ViewState("RoleID").ToString)
                    Else
                        radtvAccountsPaymentNature.UncheckAllNodes()
                        radtvAccountsPaymentNature.CollapseAllNodes()
                    End If
                End If
                UIUtilities.ShowDialog(Me, "User Roles Account Payment Nature Tagging", "[ " & PassToDeleteCount.ToString & " ] User roles account payment nature(s) deleted successfuly.<br /> [ " & FailToDeleteCount.ToString & " ] User roles account payment nature(s) can not be deleted due to following reason: <br /> 1. Delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)

                Exit Sub
            ElseIf Not PassToDeleteCount = 0 And FailToDeleteCount = 0 Then
                If ddlUserType.SelectedValue.ToString = "Bank User" Then
                    LoadgvPaymentNature("0", ViewState("RoleID").ToString, True)
                    If gvPaymentNature.Items.Count > 0 Then
                        CheckMarkAlreadyTaggedAccountPaymentNatureWithRole("0", ViewState("RoleID").ToString)
                    Else
                        radtvAccountsPaymentNature.UncheckAllNodes()
                        radtvAccountsPaymentNature.CollapseAllNodes()
                    End If
                ElseIf ddlUserType.SelectedValue.ToString = "Client User" Then
                    LoadgvPaymentNature(ddlUserGroup.SelectedValue.ToString, ViewState("RoleID").ToString, True)
                    If gvPaymentNature.Items.Count > 0 Then
                        CheckMarkAlreadyTaggedAccountPaymentNatureWithRole(ddlUserGroup.SelectedValue.ToString, ViewState("RoleID").ToString)
                    Else
                        radtvAccountsPaymentNature.UncheckAllNodes()
                        radtvAccountsPaymentNature.CollapseAllNodes()
                    End If
                End If
                UIUtilities.ShowDialog(Me, "User Roles Account Payment Nature Tagging", PassToDeleteCount.ToString & " User roles account payment nature(s) deleted successfuly.", ICBO.IC.Dialogmessagetype.Success)

                Exit Sub
            ElseIf Not FailToDeleteCount = 0 And PassToDeleteCount = 0 Then
                If ddlUserType.SelectedValue.ToString = "Bank User" Then
                    LoadgvPaymentNature("0", ViewState("RoleID").ToString, True)
                    If gvPaymentNature.Items.Count > 0 Then
                        CheckMarkAlreadyTaggedAccountPaymentNatureWithRole("0", ViewState("RoleID").ToString)
                    Else
                        radtvAccountsPaymentNature.UncheckAllNodes()
                        radtvAccountsPaymentNature.CollapseAllNodes()
                    End If
                ElseIf ddlUserType.SelectedValue.ToString = "Client User" Then
                    LoadgvPaymentNature(ddlUserGroup.SelectedValue.ToString, ViewState("RoleID").ToString, True)
                    If gvPaymentNature.Items.Count > 0 Then
                        CheckMarkAlreadyTaggedAccountPaymentNatureWithRole(ddlUser.SelectedValue.ToString, ViewState("RoleID").ToString)
                    Else
                        radtvAccountsPaymentNature.UncheckAllNodes()
                        radtvAccountsPaymentNature.CollapseAllNodes()

                    End If
                End If
                UIUtilities.ShowDialog(Me, "User Roles Account Payment Nature Tagging", "[ " & FailToDeleteCount.ToString & " ] User roles account payment nature(s) can not be deleted due to following reasons: <br /> 1. Delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If



        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvPaymentNature_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvPaymentNature.ItemCommand
        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then

                    Dim objICUserRolesAPNature As New ICUserRolesAccountAndPaymentNature
                    Dim objICUserRolesAPNatureColl As New ICUserRolesAccountAndPaymentNatureCollection
                    Dim objICUserRolesAPNatureCollForDelete As New ICUserRolesAccountAndPaymentNatureCollection
                    Dim objICUserRolesAPNatureLocationsCollForDelete As New ICUserRolesAccountPaymentNatureAndLocationsCollection
                    Dim objICUserRolesAPNatureLocationsColl As New ICUserRolesAccountPaymentNatureAndLocationsCollection
                    Dim TaggingType As String = Nothing
                    Dim objICAuditTrailColl As New ICAuditTrailCollection
                    Dim objICAuditTrail As ICAuditTrail
                    Dim StrAuditTrailAction As String = Nothing
                    objICUserRolesAPNature.es.Connection.CommandTimeout = 3600
                    objICUserRolesAPNatureLocationsColl.es.Connection.CommandTimeout = 3600
                    If ddlUserType.SelectedValue.ToString = "Bank User" Then
                        TaggingType = "Bank"
                    Else
                        TaggingType = "Client"
                    End If
                    Dim UserID As String = Nothing
                    If ddlUserType.SelectedValue.ToString = "Client User" Then
                        UserID = ddlUserGroup.SelectedValue.ToString
                        If objICUserRolesAPNature.LoadByPrimaryKey(e.CommandArgument.ToString.Split(";")(3), e.CommandArgument.ToString.Split(";")(4), UserID, e.CommandArgument.ToString.Split(";")(0), e.CommandArgument.ToString.Split(";")(1), e.CommandArgument.ToString.Split(";")(2)) Then
                            objICAuditTrail = New ICAuditTrail
                            StrAuditTrailAction = Nothing
                            StrAuditTrailAction = "Account [ " & objICUserRolesAPNature.AccountNumber & " ] payment nature [ " & objICUserRolesAPNature.PaymentNatureCode & "] "
                            StrAuditTrailAction += " of group [ " & objICUserRolesAPNature.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupCode & " ] for role [ " & objICUserRolesAPNature.UpToICRoleByRolesID.RoleID & " ]"
                            StrAuditTrailAction += " for user [ " & objICUserRolesAPNature.UpToICUserByUserID.UserName & " ] deleted "
                            Dim ICount As Integer = 0
                            objICUserRolesAPNatureLocationsColl = ICUserRolesAccountPaymentNatureAndLocationsController.GetAllUserRolesAccountPaymentNatureLocations(e.CommandArgument.ToString.Split(";")(4), e.CommandArgument.ToString.Split(";")(0), e.CommandArgument.ToString.Split(";")(1), e.CommandArgument.ToString.Split(";")(2), e.CommandArgument.ToString.Split(";")(3), UserID)
                            If objICUserRolesAPNatureLocationsColl.Count > 0 Then
                                For Each objICUserRoleAPNatureLocation As ICUserRolesAccountPaymentNatureAndLocations In objICUserRolesAPNatureLocationsColl
                                    If ICount = 0 Then
                                        StrAuditTrailAction += "with location [ "
                                        ICount = ICount + 1
                                    End If
                                    StrAuditTrailAction += objICUserRoleAPNatureLocation.OfficeID.ToString & ";"
                                    objICUserRolesAPNatureLocationsCollForDelete.Add(objICUserRoleAPNatureLocation)
                                    ''ICUserRolesAccountPaymentNatureAndLocationsController.DeleteUserRolesAPNaturesLocations(objICUserRoleAPNatureLocation, Me.UserId.ToString, Me.UserInfo.Username.ToString, TaggingType)
                                Next
                            End If
                            If StrAuditTrailAction.Contains(";") = True Then
                                StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                                StrAuditTrailAction += " ]."
                            Else
                                StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                                StrAuditTrailAction += " ."
                            End If
                            objICAuditTrail.AuditAction = StrAuditTrailAction
                            objICAuditTrail.AuditType = "User Role Account Payment Nature"
                            objICAuditTrail.RelatedID = objICUserRolesAPNature.AccountNumber.ToString + "" + objICUserRolesAPNature.BranchCode.ToString + "" + objICUserRolesAPNature.Currency.ToString + "" + objICUserRolesAPNature.PaymentNatureCode.ToString
                            objICAuditTrail.AuditDate = Date.Now
                            objICAuditTrail.UserID = Me.UserId
                            objICAuditTrail.Username = Me.UserInfo.Username.ToString
                            objICAuditTrail.ActionType = "DELETE"
                            If Not HttpContext.Current Is Nothing Then
                                objICAuditTrail.IPAddress = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                            End If
                            objICUserRolesAPNatureCollForDelete.Add(objICUserRolesAPNature)
                            'ICUserRolesPaymentNatureController.DeleteUserRolesPaymentNature(objICUserRolesAPNature, Me.UserId.ToString, Me.UserInfo.Username.ToString, TaggingType)
                            objICAuditTrailColl.Add(objICAuditTrail)

                            If objICUserRolesAPNatureLocationsCollForDelete.Count > 0 Then
                                objICUserRolesAPNatureLocationsCollForDelete.MarkAllAsDeleted()
                                objICUserRolesAPNatureLocationsCollForDelete.Save()
                            End If
                            If objICUserRolesAPNatureCollForDelete.Count > 0 Then
                                objICUserRolesAPNatureCollForDelete.MarkAllAsDeleted()
                                objICUserRolesAPNatureCollForDelete.Save()
                            End If
                            If objICAuditTrailColl.Count > 0 Then
                                objICAuditTrailColl.Save()
                            End If
                            UIUtilities.ShowDialog(Me, "User Roles Account Payment Nature Tagging", "User roles account payment nature deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                            If ddlUserType.SelectedValue.ToString = "Bank User" Then
                                LoadgvPaymentNature("0", ViewState("RoleID").ToString, True)
                                BindTreeView("Bank User")
                                If gvPaymentNature.Items.Count > 0 Then
                                    CheckMarkAlreadyTaggedAccountPaymentNatureWithRole("0", ViewState("RoleID").ToString)
                                Else
                                    radtvAccountsPaymentNature.UncheckAllNodes()
                                    radtvAccountsPaymentNature.CollapseAllNodes()
                                End If
                            ElseIf ddlUserType.SelectedValue.ToString = "Client User" Then
                                LoadgvPaymentNature(ddlUserGroup.SelectedValue.ToString, ViewState("RoleID").ToString, True)
                                BindTreeView(ddlUserGroup.SelectedValue.ToString)
                                If gvPaymentNature.Items.Count > 0 Then
                                    CheckMarkAlreadyTaggedAccountPaymentNatureWithRole(ddlUserGroup.SelectedValue.ToString, ViewState("RoleID").ToString)
                                Else
                                    radtvAccountsPaymentNature.UncheckAllNodes()
                                    radtvAccountsPaymentNature.CollapseAllNodes()
                                End If
                            End If
                        End If
                    Else
                        objICUserRolesAPNatureColl = ICUserRolesPaymentNatureController.GetAllUserRolesAccountPaymentNatureByRoleIDAndAPN(e.CommandArgument.ToString.Split(";")(3), "0", e.CommandArgument.ToString.Split(";")(0), e.CommandArgument.ToString.Split(";")(1), e.CommandArgument.ToString.Split(";")(2), e.CommandArgument.ToString.Split(";")(4))
                        For Each objICUserRoleAPN As ICUserRolesAccountAndPaymentNature In objICUserRolesAPNatureColl
                            objICAuditTrail = New ICAuditTrail
                            StrAuditTrailAction = Nothing
                            StrAuditTrailAction = "Account [ " & objICUserRoleAPN.AccountNumber & " ] payment nature [ " & objICUserRoleAPN.PaymentNatureCode & "] "
                            StrAuditTrailAction += " of group [ " & objICUserRoleAPN.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupCode & " ] for role [ " & objICUserRoleAPN.UpToICRoleByRolesID.RoleID & " ]"
                            StrAuditTrailAction += " for user [ " & objICUserRoleAPN.UpToICUserByUserID.UserName & " ] deleted "
                            Dim ICount As Integer = 0

                            objICUserRolesAPNatureLocationsColl = New ICUserRolesAccountPaymentNatureAndLocationsCollection
                            objICUserRolesAPNatureLocationsColl = ICUserRolesAccountPaymentNatureAndLocationsController.GetAllUserRolesAccountPaymentNatureLocations(e.CommandArgument.ToString.Split(";")(4), e.CommandArgument.ToString.Split(";")(0), e.CommandArgument.ToString.Split(";")(1), e.CommandArgument.ToString.Split(";")(2), e.CommandArgument.ToString.Split(";")(3), objICUserRoleAPN.UserID.ToString)
                            If objICUserRolesAPNatureLocationsColl.Count > 0 Then
                                For Each objICUserRoleAPNatureLocation As ICUserRolesAccountPaymentNatureAndLocations In objICUserRolesAPNatureLocationsColl
                                    If ICount = 0 Then
                                        StrAuditTrailAction += "with location [ "
                                        ICount = ICount + 1
                                    End If
                                    StrAuditTrailAction += objICUserRoleAPNatureLocation.OfficeID.ToString & ";"
                                    objICUserRolesAPNatureLocationsCollForDelete.Add(objICUserRoleAPNatureLocation)
                                    'ICUserRolesAccountPaymentNatureAndLocationsController.DeleteUserRolesAPNaturesLocations(objICUserRoleAPNatureLocation, Me.UserId.ToString, Me.UserInfo.Username.ToString, TaggingType)

                                Next
                            End If

                            If StrAuditTrailAction.Contains(";") = True Then
                                StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                                StrAuditTrailAction += " ]."
                            Else
                                StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                                StrAuditTrailAction += " ."
                            End If
                            objICAuditTrail.AuditAction = StrAuditTrailAction
                            objICAuditTrail.AuditType = "User Role Account Payment Nature"
                            objICAuditTrail.RelatedID = objICUserRoleAPN.AccountNumber.ToString + "" + objICUserRoleAPN.BranchCode.ToString + "" + objICUserRoleAPN.Currency.ToString + "" + objICUserRoleAPN.PaymentNatureCode.ToString
                            objICAuditTrail.AuditDate = Date.Now
                            objICAuditTrail.UserID = Me.UserId
                            objICAuditTrail.Username = Me.UserInfo.Username.ToString
                            objICAuditTrail.ActionType = "DELETE"
                            If Not HttpContext.Current Is Nothing Then
                                objICAuditTrail.IPAddress = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                            End If
                            objICUserRolesAPNatureCollForDelete.Add(objICUserRoleAPN)
                            'ICUserRolesPaymentNatureController.DeleteUserRolesPaymentNature(objICUserRoleAPN, Me.UserId.ToString, Me.UserInfo.Username.ToString, TaggingType)
                            objICAuditTrailColl.Add(objICAuditTrail)
                        Next
                        If objICUserRolesAPNatureLocationsCollForDelete.Count > 0 Then
                            objICUserRolesAPNatureLocationsCollForDelete.MarkAllAsDeleted()
                            objICUserRolesAPNatureLocationsCollForDelete.Save()
                        End If
                        If objICUserRolesAPNatureCollForDelete.Count > 0 Then
                            objICUserRolesAPNatureCollForDelete.MarkAllAsDeleted()
                            objICUserRolesAPNatureCollForDelete.Save()
                        End If
                        If objICAuditTrailColl.Count > 0 Then
                            objICAuditTrailColl.Save()
                        End If
                        UIUtilities.ShowDialog(Me, "User Roles Account Payment Nature Tagging", "User roles account payment nature deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                        If ddlUserType.SelectedValue.ToString = "Bank User" Then
                            LoadgvPaymentNature("0", ViewState("RoleID").ToString, True)
                            BindTreeView("Bank User")
                            If gvPaymentNature.Items.Count > 0 Then
                                CheckMarkAlreadyTaggedAccountPaymentNatureWithRole("0", ViewState("RoleID").ToString)
                            Else
                                radtvAccountsPaymentNature.UncheckAllNodes()
                                radtvAccountsPaymentNature.CollapseAllNodes()
                            End If
                        ElseIf ddlUserType.SelectedValue.ToString = "Client User" Then
                            LoadgvPaymentNature(ddlUserGroup.SelectedValue.ToString, ViewState("RoleID").ToString, True)
                            BindTreeView(ddlUserGroup.SelectedValue.ToString)
                            If gvPaymentNature.Items.Count > 0 Then
                                CheckMarkAlreadyTaggedAccountPaymentNatureWithRole(ddlUserGroup.SelectedValue.ToString, ViewState("RoleID").ToString)
                            Else
                                radtvAccountsPaymentNature.UncheckAllNodes()
                                radtvAccountsPaymentNature.CollapseAllNodes()
                            End If
                        End If

                    End If

                End If

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub gvPaymentNature_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvPaymentNature.ItemDataBound

        Dim chkProcessAll As CheckBox
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAllAccountPaymentNatures"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvPaymentNature.MasterTableView.ClientID & "','0');"
        End If
    End Sub
    Private Function CheckgvPNatureForProcessAll() As Boolean
        Try
            Dim rowGVPNature As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVPNature In gvPaymentNature.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVPNature.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnApproveAccountPaymentNatures_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveAccountPaymentNatures.Click
        If Page.IsValid Then
            Try
                Dim AccountNumber, BranchCode, Currency, PaymentNatureCode, UserID, RoleID As String
                Dim PassToDeleteCount As Integer = 0
                Dim FailToDeleteCount As Integer = 0
                Dim objICUserRolesAPNatureColl As ICUserRolesAccountAndPaymentNatureCollection
                Dim objICUserRolesAPNatureCollForApprove As New ICUserRolesAccountAndPaymentNatureCollection
                Dim objICUserRolesAPNatureLocationsCollForApprove As New ICUserRolesAccountPaymentNatureAndLocationsCollection
                Dim objICUserRolesAPNatureLocationsColl As ICUserRolesAccountPaymentNatureAndLocationsCollection
                Dim objICAuditTrailColl As New ICAuditTrailCollection
                Dim objICAuditTrail As ICAuditTrail
                Dim StrAuditTrailAction As String = Nothing
                Dim chkSelect As CheckBox
                Dim TaggingType As String = Nothing
                If CheckgvPNatureForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "User Roles Account Payment Nature Tagging", "Please select atleast one record.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If ddlUserType.SelectedValue.ToString = "Client User" Then
                    TaggingType = "Client"
                Else
                    TaggingType = "Bank"
                End If
                For Each gvUserRolesAPNatureRow As GridDataItem In gvPaymentNature.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvUserRolesAPNatureRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        AccountNumber = Nothing
                        BranchCode = Nothing
                        Currency = Nothing
                        PaymentNatureCode = Nothing
                        UserID = Nothing
                        RoleID = Nothing
                        AccountNumber = gvUserRolesAPNatureRow.GetDataKeyValue("AccountNumber")
                        BranchCode = gvUserRolesAPNatureRow.GetDataKeyValue("BranchCode")
                        Currency = gvUserRolesAPNatureRow.GetDataKeyValue("Currency")
                        PaymentNatureCode = gvUserRolesAPNatureRow.GetDataKeyValue("PaymentNatureCode")
                        RoleID = gvUserRolesAPNatureRow.GetDataKeyValue("RolesID")
                        If ddlUserType.SelectedValue = "Client User" Then
                            UserID = ddlUserGroup.SelectedValue.ToString
                        Else
                            UserID = "0"
                        End If
                        objICUserRolesAPNatureColl = New ICUserRolesAccountAndPaymentNatureCollection
                        objICUserRolesAPNatureColl.es.Connection.CommandTimeout = 3600
                        objICUserRolesAPNatureColl = ICUserRolesPaymentNatureController.GetAllUserRolesAccountPaymentNature(PaymentNatureCode, AccountNumber, BranchCode, Currency, RoleID, UserID)
                        If objICUserRolesAPNatureColl.Count > 0 Then
                            For Each objICUserRoleAPNature As ICUserRolesAccountAndPaymentNature In objICUserRolesAPNatureColl
                                If objICUserRoleAPNature.IsApproved = False Then
                                    objICAuditTrail = New ICAuditTrail
                                    StrAuditTrailAction = Nothing
                                    StrAuditTrailAction = "Account [ " & objICUserRoleAPNature.AccountNumber & " ] payment nature [ " & objICUserRoleAPNature.PaymentNatureCode & "] "
                                    StrAuditTrailAction += " of group [ " & objICUserRoleAPNature.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupCode & " ] for role [ " & objICUserRoleAPNature.UpToICRoleByRolesID.RoleID & " ]"
                                    StrAuditTrailAction += " for user [ " & objICUserRoleAPNature.UpToICUserByUserID.UserName & " ] approved "
                                    Dim ICount As Integer = 0
                                    If Me.UserInfo.IsSuperUser = False Then
                                        If Not objICUserRoleAPNature.CreateBy = Me.UserId.ToString Then
                                            Try

                                                objICUserRolesAPNatureLocationsColl = New ICUserRolesAccountPaymentNatureAndLocationsCollection
                                                objICUserRolesAPNatureLocationsColl.es.Connection.CommandTimeout = 3600
                                                objICUserRolesAPNatureLocationsColl = ICUserRolesAccountPaymentNatureAndLocationsController.GetAllUserRolesAccountPaymentNatureLocations(objICUserRoleAPNature.PaymentNatureCode, objICUserRoleAPNature.AccountNumber, objICUserRoleAPNature.BranchCode, objICUserRoleAPNature.Currency, objICUserRoleAPNature.RolesID, objICUserRoleAPNature.UserID)
                                                If objICUserRolesAPNatureLocationsColl.Count > 0 Then
                                                    For Each objICUserRoleAPNatureLocations As ICUserRolesAccountPaymentNatureAndLocations In objICUserRolesAPNatureLocationsColl
                                                        If ICount = 0 Then
                                                            StrAuditTrailAction += "with location [ "
                                                            ICount = ICount + 1
                                                        End If
                                                        StrAuditTrailAction += objICUserRoleAPNatureLocations.OfficeID.ToString & ";"
                                                        objICUserRoleAPNatureLocations.IsApprove = chkSelect.Checked
                                                        objICUserRoleAPNatureLocations.ApprovedBy = Me.UserId.ToString
                                                        objICUserRoleAPNatureLocations.ApprovedDate = Date.Now
                                                        objICUserRolesAPNatureLocationsCollForApprove.Add(objICUserRoleAPNatureLocations)
                                                        'ICUserRolesAccountPaymentNatureAndLocationsController.ApproveUserRolesAPNaturesLocations(objICUserRoleAPNatureLocations, chkSelect.Checked, Me.UserId.ToString, Me.UserInfo.Username.ToString, TaggingType)

                                                    Next
                                                End If
                                                If StrAuditTrailAction.Contains(";") = True Then
                                                    StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                                                    StrAuditTrailAction += " ]."
                                                Else
                                                    StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                                                    StrAuditTrailAction += " ."
                                                End If
                                                objICAuditTrail.AuditAction = StrAuditTrailAction
                                                objICAuditTrail.AuditType = "User Role Account Payment Nature"
                                                objICAuditTrail.RelatedID = objICUserRoleAPNature.AccountNumber.ToString + "" + objICUserRoleAPNature.BranchCode.ToString + "" + objICUserRoleAPNature.Currency.ToString + "" + objICUserRoleAPNature.PaymentNatureCode.ToString
                                                objICAuditTrail.AuditDate = Date.Now
                                                objICAuditTrail.UserID = Me.UserId
                                                objICAuditTrail.Username = Me.UserInfo.Username.ToString
                                                objICAuditTrail.ActionType = "APPROVE"
                                                If Not HttpContext.Current Is Nothing Then
                                                    objICAuditTrail.IPAddress = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                                                End If
                                                objICUserRoleAPNature.IsApproved = chkSelect.Checked
                                                objICUserRoleAPNature.ApprovedBy = Me.UserId.ToString
                                                objICUserRoleAPNature.ApprovedOn = Date.Now
                                                objICUserRolesAPNatureCollForApprove.Add(objICUserRoleAPNature)
                                                'ICUserRolesPaymentNatureController.ApproveUserRolesAPNatures(objICUserRoleAPNature, chkSelect.Checked, Me.UserId.ToString, Me.UserInfo.Username.ToString, TaggingType)
                                                objICAuditTrailColl.Add(objICAuditTrail)
                                                PassToDeleteCount = PassToDeleteCount + 1
                                            Catch ex As Exception
                                                FailToDeleteCount = FailToDeleteCount + 1
                                                Continue For
                                            End Try
                                        Else
                                            FailToDeleteCount = FailToDeleteCount + 1
                                            Continue For
                                        End If
                                    Else

                                        Try
                                            objICUserRolesAPNatureLocationsColl = New ICUserRolesAccountPaymentNatureAndLocationsCollection
                                            objICUserRolesAPNatureLocationsColl.es.Connection.CommandTimeout = 3600
                                            objICUserRolesAPNatureLocationsColl = ICUserRolesAccountPaymentNatureAndLocationsController.GetAllUserRolesAccountPaymentNatureLocations(objICUserRoleAPNature.PaymentNatureCode, objICUserRoleAPNature.AccountNumber, objICUserRoleAPNature.BranchCode, objICUserRoleAPNature.Currency, objICUserRoleAPNature.RolesID, objICUserRoleAPNature.UserID)
                                            If objICUserRolesAPNatureLocationsColl.Count > 0 Then
                                                For Each objICUserRoleAPNatureLocations As ICUserRolesAccountPaymentNatureAndLocations In objICUserRolesAPNatureLocationsColl
                                                    If ICount = 0 Then
                                                        StrAuditTrailAction += "with location [ "
                                                        ICount = ICount + 1
                                                    End If
                                                    StrAuditTrailAction += objICUserRoleAPNatureLocations.OfficeID.ToString & ";"
                                                    objICUserRoleAPNatureLocations.IsApprove = chkSelect.Checked
                                                    objICUserRoleAPNatureLocations.ApprovedBy = Me.UserId.ToString
                                                    objICUserRoleAPNatureLocations.ApprovedDate = Date.Now
                                                    objICUserRolesAPNatureLocationsCollForApprove.Add(objICUserRoleAPNatureLocations)
                                                    'ICUserRolesAccountPaymentNatureAndLocationsController.ApproveUserRolesAPNaturesLocations(objICUserRoleAPNatureLocations, chkSelect.Checked, Me.UserId.ToString, Me.UserInfo.Username.ToString, TaggingType)
                                                    'objICAuditTrailColl.Add(objICAuditTrail)
                                                Next
                                            End If

                                            If StrAuditTrailAction.Contains(";") = True Then
                                                StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                                                StrAuditTrailAction += " ]."
                                            Else
                                                StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                                                StrAuditTrailAction += " ."
                                            End If

                                            objICAuditTrail.AuditAction = StrAuditTrailAction
                                            objICAuditTrail.AuditType = "User Role Account Payment Nature"
                                            objICAuditTrail.RelatedID = objICUserRoleAPNature.AccountNumber.ToString + "" + objICUserRoleAPNature.BranchCode.ToString + "" + objICUserRoleAPNature.Currency.ToString + "" + objICUserRoleAPNature.PaymentNatureCode.ToString
                                            objICAuditTrail.AuditDate = Date.Now
                                            objICAuditTrail.UserID = Me.UserId
                                            objICAuditTrail.Username = Me.UserInfo.Username.ToString
                                            objICAuditTrail.ActionType = "APPROVE"
                                            If Not HttpContext.Current Is Nothing Then
                                                objICAuditTrail.IPAddress = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                                            End If
                                            objICUserRoleAPNature.IsApproved = chkSelect.Checked
                                            objICUserRoleAPNature.ApprovedBy = Me.UserId.ToString
                                            objICUserRoleAPNature.ApprovedOn = Date.Now
                                            objICUserRolesAPNatureCollForApprove.Add(objICUserRoleAPNature)
                                            'ICUserRolesPaymentNatureController.ApproveUserRolesAPNatures(objICUserRoleAPNature, chkSelect.Checked, Me.UserId.ToString, Me.UserInfo.Username.ToString, TaggingType)
                                            objICAuditTrailColl.Add(objICAuditTrail)
                                            PassToDeleteCount = PassToDeleteCount + 1
                                        Catch ex As Exception
                                            FailToDeleteCount = FailToDeleteCount + 1
                                            Continue For
                                        End Try
                                    End If
                                Else
                                    FailToDeleteCount = FailToDeleteCount + 1
                                    Continue For
                                End If
                            Next
                        End If
                    End If
                Next
                If objICUserRolesAPNatureCollForApprove.Count > 0 Then
                    objICUserRolesAPNatureCollForApprove.Save()
                End If
                If objICUserRolesAPNatureLocationsCollForApprove.Count > 0 Then
                    objICUserRolesAPNatureLocationsCollForApprove.Save()
                End If
                If objICAuditTrailColl.Count > 0 Then
                    objICAuditTrailColl.Save()
                End If
                If Not PassToDeleteCount = 0 And Not FailToDeleteCount = 0 Then
                    If ddlUserType.SelectedValue.ToString = "Bank User" Then
                        LoadgvPaymentNature("0", ViewState("RoleID").ToString, True)
                    ElseIf ddlUserType.SelectedValue.ToString = "Client User" Then
                        LoadgvPaymentNature(ddlUserGroup.SelectedValue.ToString, ViewState("RoleID").ToString, True)
                    End If
                    UIUtilities.ShowDialog(Me, "User Roles Account Payment Nature Tagging", "[ " & PassToDeleteCount.ToString & " ] User roles account payment nature(s) approved successfully.<br /> [ " & FailToDeleteCount.ToString & " ] User roles account payment nature(s) can not be approved due to following reason: <br /> 1. User roles account payment nature(s) already approved.<br /> 2. User roles account payment nature(s) must be approved by other than maker.", ICBO.IC.Dialogmessagetype.Failure)

                    Exit Sub
                ElseIf Not PassToDeleteCount = 0 And FailToDeleteCount = 0 Then
                    If ddlUserType.SelectedValue.ToString = "Bank User" Then
                        LoadgvPaymentNature("0", ViewState("RoleID").ToString, True)
                    ElseIf ddlUserType.SelectedValue.ToString = "Client User" Then
                        LoadgvPaymentNature(ddlUserGroup.SelectedValue.ToString, ViewState("RoleID").ToString, True)
                    End If
                    UIUtilities.ShowDialog(Me, "User Roles Account Payment Nature Tagging", PassToDeleteCount.ToString & " User roles account payment nature(s) approved successfully.", ICBO.IC.Dialogmessagetype.Success)

                    Exit Sub
                ElseIf Not FailToDeleteCount = 0 And PassToDeleteCount = 0 Then
                    If ddlUserType.SelectedValue.ToString = "Bank User" Then
                        LoadgvPaymentNature("0", ViewState("RoleID").ToString, True)
                    ElseIf ddlUserType.SelectedValue.ToString = "Client User" Then
                        LoadgvPaymentNature(ddlUserGroup.SelectedValue.ToString, ViewState("RoleID").ToString, True)
                    End If
                    UIUtilities.ShowDialog(Me, "User Roles Account Payment Nature Tagging", "[ " & FailToDeleteCount.ToString & " ] User roles account payment nature(s) can not be approved due to following reasons: <br /> 1.User roles account payment nature(s) already approved.<br /> 2. User roles account payment nature(s) must be approved by other than maker.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Sub DeleteOldTaggingByRoleIDAndUserID(ByVal RoleID As String, ByVal UserId As String)
        Try
            Dim objICUserRolesAPNatureColl As New ICUserRolesAccountAndPaymentNatureCollection
            Dim objICUserRolesAPNatureCollForDelete As New ICUserRolesAccountAndPaymentNatureCollection
            Dim objICUserRolesAPNatureLocationsCollForDelete As New ICUserRolesAccountPaymentNatureAndLocationsCollection
            Dim objICUserRolesAPNatureLocationsColl As New ICUserRolesAccountPaymentNatureAndLocationsCollection
            Dim objICAuditTrailColl As New ICAuditTrailCollection
            Dim objICAuditTrail As ICAuditTrail
            Dim TaggingType As String = Nothing
            Dim StrAuditTrailAction As String = Nothing
            If ddlUserType.SelectedValue.ToString = "Client User" Then
                TaggingType = "Client"
            Else
                TaggingType = "Bank"
            End If
            objICUserRolesAPNatureColl.es.Connection.CommandTimeout = 3600
            objICUserRolesAPNatureLocationsColl.es.Connection.CommandTimeout = 36
            objICUserRolesAPNatureColl = ICUserRolesPaymentNatureController.GetAllUserRolesAccountPaymentNatureByRoleIDAndUserID(RoleID.ToString, UserId.ToString)
            If objICUserRolesAPNatureColl.Count > 0 Then
                For Each objICUserRolesAPNature As ICUserRolesAccountAndPaymentNature In objICUserRolesAPNatureColl
                    objICAuditTrail = New ICAuditTrail
                    StrAuditTrailAction = Nothing
                    StrAuditTrailAction = "Account [ " & objICUserRolesAPNature.AccountNumber & " ] payment nature [ " & objICUserRolesAPNature.PaymentNatureCode & "] "
                    StrAuditTrailAction += " of group [ " & objICUserRolesAPNature.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupCode & " ] for role [ " & objICUserRolesAPNature.UpToICRoleByRolesID.RoleID & " ]"
                    StrAuditTrailAction += " for user [ " & objICUserRolesAPNature.UpToICUserByUserID.UserName & " ] deleted on update "
                    Dim ICount As Integer = 0
                    objICUserRolesAPNatureLocationsColl = New ICUserRolesAccountPaymentNatureAndLocationsCollection
                    objICUserRolesAPNatureLocationsColl = ICUserRolesAccountPaymentNatureAndLocationsController.GetAllUserRolesAccountPaymentNatureLocations(objICUserRolesAPNature.PaymentNatureCode, objICUserRolesAPNature.AccountNumber, objICUserRolesAPNature.BranchCode, objICUserRolesAPNature.Currency, objICUserRolesAPNature.RolesID, objICUserRolesAPNature.UserID)
                    If objICUserRolesAPNatureLocationsColl.Count > 0 Then
                        For Each objICUserRoleAPNatureLocation As ICUserRolesAccountPaymentNatureAndLocations In objICUserRolesAPNatureLocationsColl
                            If ICount = 0 Then
                                StrAuditTrailAction += "with location [ "
                                ICount = ICount + 1
                            End If
                            StrAuditTrailAction += objICUserRoleAPNatureLocation.OfficeID.ToString & ";"
                            objICUserRolesAPNatureLocationsCollForDelete.Add(objICUserRoleAPNatureLocation)
                            'ICUserRolesAccountPaymentNatureAndLocationsController.DeleteUserRolesAPNaturesLocations(objICUserRoleAPNatureLocation, Me.UserId.ToString, Me.UserInfo.Username.ToString, TaggingType)
                        Next
                        'objICUserRolesAPNatureLocationsColl.Save()
                    End If
                    If StrAuditTrailAction.Contains(";") = True Then
                        StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                        StrAuditTrailAction += " ]."
                    Else
                        StrAuditTrailAction += "."
                    End If
                    objICAuditTrail.AuditAction = StrAuditTrailAction
                    objICAuditTrail.AuditType = "User Role Account Payment Nature"
                    objICAuditTrail.RelatedID = objICUserRolesAPNature.AccountNumber.ToString + "" + objICUserRolesAPNature.BranchCode.ToString + "" + objICUserRolesAPNature.Currency.ToString + "" + objICUserRolesAPNature.PaymentNatureCode.ToString
                    objICAuditTrail.AuditDate = Date.Now
                    objICAuditTrail.UserID = Me.UserId
                    objICAuditTrail.Username = Me.UserInfo.Username.ToString
                    objICAuditTrail.ActionType = "DELETE"
                    If Not HttpContext.Current Is Nothing Then
                        objICAuditTrail.IPAddress = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                    End If
                    objICUserRolesAPNatureCollForDelete.Add(objICUserRolesAPNature)
                    'ICUserRolesPaymentNatureController.DeleteUserRolesPaymentNature(objICUserRolesAPNature, Me.UserId.ToString, Me.UserInfo.Username.ToString, TaggingType)
                    objICAuditTrailColl.Add(objICAuditTrail)
                Next

            End If
            If objICUserRolesAPNatureLocationsCollForDelete.Count > 0 Then
                objICUserRolesAPNatureLocationsCollForDelete.MarkAllAsDeleted()
                objICUserRolesAPNatureLocationsCollForDelete.Save()
            End If
            If objICUserRolesAPNatureCollForDelete.Count > 0 Then
                objICUserRolesAPNatureCollForDelete.MarkAllAsDeleted()
                objICUserRolesAPNatureCollForDelete.Save()
            End If
            If objICAuditTrailColl.Count > 0 Then
                objICAuditTrailColl.Save()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckIsAtLeastOneChildNodeIsChecked(ByVal ParentNode As RadTreeNode) As Boolean
        Dim Result As Boolean = False
        If ParentNode.Nodes.Count > 0 Then
            For Each childNode As RadTreeNode In ParentNode.Nodes
                If childNode.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
        End If
        Return Result
    End Function
    Private Function CheckIsAtLeastOneAccountPaymentNatureSelected() As Boolean

        Dim Result As Boolean = False
        For Each radMainTreeNode As RadTreeNode In radtvAccountsPaymentNature.Nodes
            For Each radGroupNode As RadTreeNode In radMainTreeNode.Nodes
                For Each radCompanyNode As RadTreeNode In radGroupNode.Nodes
                    For Each radAPNatureNode As RadTreeNode In radCompanyNode.Nodes
                        If radAPNatureNode.Checked = True Then
                            Result = True
                            Exit For
                        End If
                    Next
                Next
            Next
        Next

        Return Result
    End Function
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(41))
    End Sub

    Protected Sub gvPaymentNature_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvPaymentNature.NeedDataSource
        Try
            If Not ViewState("RoleID") Is Nothing Then
                If ddlUserType.SelectedValue.ToString = "Bank User" Then
                    LoadgvPaymentNature("0", ViewState("RoleID").ToString, False)
                ElseIf ddlUserType.SelectedValue.ToString = "Client User" Then
                    LoadgvPaymentNature(ddlUserGroup.SelectedValue.ToString, ViewState("RoleID").ToString, False)
                End If
            End If
           
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
