﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AssignClientRightsToRole.ascx.vb" Inherits="DesktopModules_ClientRoleRights_AssignClientRightsToRole" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function CheckInGridAll(chkView, chkAdd, chkEdit, chkDelete, chkApproved) {

        if (document.getElementById(chkAdd).checked == true) {
            document.getElementById(chkView).checked = true;
            document.getElementById(chkAdd).checked = true;
        }

        if (document.getElementById(chkEdit).checked == true) {
            document.getElementById(chkView).checked = true;
            document.getElementById(chkEdit).checked = true;
        }

        if (document.getElementById(chkDelete).checked == true) {
            document.getElementById(chkView).checked = true;
            document.getElementById(chkDelete).checked = true;
        }

        if (document.getElementById(chkApproved).checked == true) {
            document.getElementById(chkView).checked = true;
            document.getElementById(chkApproved).checked = true;
        }
    }
</script>
<script type="text/javascript">
    function CheckInGridView(chkView, chkAdd, chkEdit, chkDelete, chkApproved) {

        if (document.getElementById(chkView).checked == true) {
        }
        else {
            document.getElementById(chkAdd).checked = false;
            document.getElementById(chkEdit).checked = false;
            document.getElementById(chkDelete).checked = false;
            document.getElementById(chkApproved).checked = false;
        }
    }
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        var cellView;

        if (grid.rows.length > 0) {
            for (i = 0; i < grid.rows.length; i++) {
                if (CellNo != 1) {
                    cell = grid.rows[i].cells[CellNo];
                    cellView = grid.rows[i].cells[1];
                    if (cell.childNodes[0].type == "checkbox") {
                        if (document.getElementById(idOfControllingCheckbox).checked == true) {
                            cell.childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                            cellView.childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                        }
                        else {
                            cell.childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                        }
                    }
                }
                else {
                    cell = grid.rows[i].cells[CellNo];
                    if (cell.childNodes[0].type == "checkbox") {
                        cell.childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
            }
        }
    } 
</script>
<script type="text/javascript">
    function CheckInGridAll1(chkView, chkAdd, chkEdit, chkDelete, chkApproved) {

        if (document.getElementById(chkAdd).checked == true) {
            document.getElementById(chkView).checked = true;
            document.getElementById(chkAdd).checked = true;
        }

        if (document.getElementById(chkEdit).checked == true) {
            document.getElementById(chkView).checked = true;
            document.getElementById(chkEdit).checked = true;
        }

        if (document.getElementById(chkDelete).checked == true) {
            document.getElementById(chkView).checked = true;
            document.getElementById(chkDelete).checked = true;
        }

        if (document.getElementById(chkApproved).checked == true) {
            document.getElementById(chkView).checked = true;
            document.getElementById(chkApproved).checked = true;
        }
    }
</script>
<script type="text/javascript">
    function CheckInGridView1(chkView, chkAdd, chkEdit, chkDelete, chkApproved) {

        if (document.getElementById(chkView).checked == true) {
        }
        else {
            document.getElementById(chkAdd).checked = false;
            document.getElementById(chkEdit).checked = false;
            document.getElementById(chkDelete).checked = false;
            document.getElementById(chkApproved).checked = false;
        }
    }
</script>
<script type="text/javascript">
    function checkAllCheckboxes1(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        var cellView;

        if (grid.rows.length > 0) {
            for (i = 0; i < grid.rows.length; i++) {
                if (CellNo != 1) {
                    cell = grid.rows[i].cells[CellNo];
                    cellView = grid.rows[i].cells[1];
                    if (cell.childNodes[0].type == "checkbox") {
                        if (document.getElementById(idOfControllingCheckbox).checked == true) {
                            cell.childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                            cellView.childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                        }
                        else {
                            cell.childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                        }
                    }
                }
                else {
                    cell = grid.rows[i].cells[CellNo];
                    if (cell.childNodes[0].type == "checkbox") {
                        cell.childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
            }
        }
    } 
</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="false">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRoleType" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" colspan="2">
                        <asp:Label ID="lblPageHeader" runat="server" 
                            Text="Assign Rights To Client Roles" CssClass="headingblue"></asp:Label>
                    </td>
                    <td align="left" valign="top" colspan="2">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 50%">
                        <asp:Label ID="lblRole" runat="server" Text="Select Role" CssClass="lbl"></asp:Label>
                        <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 50%">
            <asp:Label ID="lblIsApproved" runat="server" Text="Is Approved" CssClass="lbl"></asp:Label>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 50%">
                        <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always" EnableViewState="true">
                            <ContentTemplate>--%>
                        <asp:DropDownList ID="ddlRole" runat="server" AutoPostBack="True" CssClass="dropdown">
                        </asp:DropDownList>
                        <br />
                        <asp:RequiredFieldValidator ID="rfvRole" runat="server" ControlToValidate="ddlRole"
                            CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Role" SetFocusOnError="True"
                            ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
                        <%--</ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </td>
                    <td align="left" valign="top" style="width: 50%">
            <asp:CheckBox ID="chkApproved" runat="server" Text=" " 
                CssClass="chkBox" />
                    </td>
                </tr>
               <tr>
                    <td align="left" valign="top" style="width: 100%" colspan="2">
                        <asp:Label ID="lblRightType" runat="server" CssClass="headingblue" Text="Rights List"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 100%" colspan="2">

                        <asp:Panel ID="pnlRightList" runat="server" Width="100%" Height="400px" ScrollBars="Vertical">
                            <asp:UpdatePanel ID="pnlCompanyRights" runat="server" UpdateMode="Always" EnableViewState="true">
                                <ContentTemplate>
                                    <telerik:RadTreeView ID="radtvRights" runat="server" CheckChildNodes="True" TriStateCheckBoxes="true">
                                        <Nodes >
                                            <telerik:RadTreeNode id="radMainTreeNode" runat="server" Checkable="true" Value="top" Text="(Select All)">
                                            </telerik:RadTreeNode>
                                        </Nodes>
                                    </telerik:RadTreeView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>

                       <%-- <asp:Panel ID="pnlRightList" runat="server" Width="100%" Height="400px"
                            ScrollBars="Vertical">
                             <asp:UpdatePanel ID="pnlCompanyRights" runat="server" UpdateMode="Always" EnableViewState="true">
                            <ContentTemplate>
                               
                                <telerik:RadTreeView ID="radtvRights" runat="server" CheckChildNodes="True">
                                </telerik:RadTreeView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </asp:Panel>--%>
                    </td>
                </tr>             
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" colspan="2">
                        <asp:Button ID="btnApproved" runat="server" Text="Approved" CssClass="btn" Width="71px" />
                        &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="79px" />
                        &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False"
                             />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
