﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security
Imports DotNetNuke.Security.Permissions
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_ClientRoleRights_AssignClientRightsToRole
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnSave.Visible = CBool(htRights("Tag"))
                btnApproved.Visible = CBool(htRights("Approve"))
                lblIsApproved.Visible = CBool(htRights("Approve"))
                chkApproved.Visible = CBool(htRights("Approve"))
                SetddlRole()
                lblRightType.Visible = False
                SettvRights()
                radtvRights.CollapseAllNodes()
                radtvRights.CheckChildNodes = False
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client Role Rights Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SettvRights()
        Try
            radtvRights.Nodes.Clear()
            Dim dtAllRights As New DataTable()
            Dim dtRightDetailName As DataTable
            dtAllRights = ICRightsController.GetAllICRights
            If dtAllRights.Rows.Count > 0 Then
                lblRightType.Visible = True
            Else
                lblRightType.Visible = False
            End If

            Dim dtDistintRightName As DataTable = dtAllRights.DefaultView.ToTable(True, "RightName")

            For Each drRights In dtDistintRightName.Rows
                dtRightDetailName = New DataTable
                dtRightDetailName = ICRightsController.GetRightDetailNameByRightName(drRights("RightName").ToString())

                Dim tvMainTreeNode As New RadTreeNode
                tvMainTreeNode.Text = drRights("RightName").ToString()
                tvMainTreeNode.Value = drRights("RightName").ToString()
                tvMainTreeNode.Checkable = True
                Dim rightRows() As DataRow = dtAllRights.Select("RightName = '" & drRights("RightName").ToString() & "'")
                For Each drDetail In dtRightDetailName.Rows
                    Dim tvChildToChildOfMainTreeNode As New RadTreeNode
                    tvChildToChildOfMainTreeNode.Text = drDetail("RightDetailName").ToString()
                    tvChildToChildOfMainTreeNode.Value = drRights("RightName").ToString() & "," & drDetail("RightDetailName").ToString
                    tvChildToChildOfMainTreeNode.Checkable = True
                    tvMainTreeNode.Nodes.Add(tvChildToChildOfMainTreeNode)
                Next
                radMainTreeNode.Nodes.Add(tvMainTreeNode)
                radtvRights.Nodes.Add(radMainTreeNode)
            Next

            radtvRights.CheckBoxes = True
            radtvRights.ExpandAllNodes()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function GetChildrenOfParentNode(ByVal Nodes As ArrayList, ByVal Node As RadTreeNode) As ArrayList
        For Each thisNode As RadTreeNode In Node.Nodes
            Nodes.Add(thisNode)
            GetChildrenOfParentNode(Nodes, thisNode)
        Next
        Return Nodes
    End Function


    Private Sub SetddlRole()
        Dim lit As New ListItem
        Try
            ddlRole.Items.Clear()
            ddlRole.AppendDataBoundItems = True
            lit.Text = "-- Select Role --"
            lit.Value = "0"
            lit.Selected = True
            ddlRole.Items.Add(lit)

            ddlRole.DataSource = ICRoleController.GetAllICRolesByRoleTypeActiveAndApprove("Company Role")
            ddlRole.DataTextField = "RoleName"
            ddlRole.DataValueField = "RoleID"
            ddlRole.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(41), False)
    End Sub



    Protected Sub ddlRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRole.SelectedIndexChanged
        If ddlRole.SelectedValue.ToString() <> "0" Then
            chkApproved.Checked = False
            SetTreeViewByRoleID(ddlRole.SelectedValue.ToString)
            btnSave.Visible = CBool(htRights("Tag"))
            btnApproved.Visible = CBool(htRights("Approve"))
            lblIsApproved.Visible = CBool(htRights("Approve"))
            chkApproved.Visible = CBool(htRights("Approve"))



        ElseIf ddlRole.SelectedValue.ToString() = "0" Then
            SettvRights()
            btnSave.Visible = False
            btnApproved.Visible = False
            chkApproved.Checked = False
            chkApproved.Visible = False
            lblIsApproved.Visible = False
            radtvRights.CollapseAllNodes()
            radtvRights.CheckChildNodes = False
        End If
    End Sub
    Private Sub SetTreeViewByRoleID(ByVal RoleID As String)
        Dim objICRoles As New ICRole
        Dim dtDistinctRightName As New DataTable
        Dim dtDistinctRightDetailName As DataTable
        Dim dtFinalTableForGV As New DataTable
        Dim StrRightName As String = ""
        Dim ChildNodesOfRootNode As ArrayList
        Dim StrRightDetailName As String = ""
        Dim nodeInArrayList As RadTreeNode
        Dim ApproveCount As Integer = 0
        'btnSave.Visible = True
        'btnApproved.Visible = True
        'chkApproved.Visible = True
        'lblIsApproved.Visible = True
        If objICRoles.LoadByPrimaryKey(ddlRole.SelectedValue.ToString()) Then
            dtDistinctRightName = ICBankRoleRightsController.GetDistinctRightNameByRoleID(ddlRole.SelectedValue.ToString)
            If dtDistinctRightName.Rows.Count > 0 Then
                'radtvRights.Nodes.Clear()
                SettvRights()
                For Each dr As DataRow In dtDistinctRightName.Rows
                    For Each RootNodeOfTreeView In radtvRights.GetAllNodes()
                        If RootNodeOfTreeView.GetAllNodes.Count > 0 And Not RootNodeOfTreeView.Value = "top" Then
                            If RootNodeOfTreeView.Text = dr("RightName") Then
                                RootNodeOfTreeView.Checked = True
                                dtDistinctRightDetailName = New DataTable
                                dtDistinctRightDetailName = ICBankRoleRightsController.GetDistinctRightDetailNameByRightName(dr("RightName").ToString, ddlRole.SelectedValue.ToString)
                                ChildNodesOfRootNode = New ArrayList
                                ChildNodesOfRootNode.Clear()
                                ChildNodesOfRootNode = GetChildrenOfParentNode(ChildNodesOfRootNode, RootNodeOfTreeView)



                                ''un comment to check new added right 

                                'For i = 0 To ChildNodesOfRootNode.Count - 1
                                '    nodeInArrayList = ChildNodesOfRootNode(i)
                                '    Dim collRow As DataRow()

                                '    collRow = dtDistinctRightDetailName.Select("RightDetailName='" & nodeInArrayList.Text.ToString & "'")
                                '    If collRow.Count > 0 Then
                                '        If nodeInArrayList.Text = collRow(0).ItemArray(0).ToString Then
                                '            nodeInArrayList.Checked = collRow(0).ItemArray(1)
                                '            If ApproveCount = 0 Then
                                '                If collRow(0).ItemArray(2) = True Then
                                '                    chkApproved.Checked = True
                                '                    ApproveCount = 1
                                '                End If
                                '            End If
                                '        Else
                                '            nodeInArrayList.Checked = False
                                '        End If

                                '    End If

                                'Next


                                For i = 0 To dtDistinctRightDetailName.Rows.Count - 1
                                    nodeInArrayList = ChildNodesOfRootNode(i)
                                    nodeInArrayList.Checked = dtDistinctRightDetailName.Rows(i)(1)
                                    If ApproveCount = 0 Then
                                        If dtDistinctRightDetailName.Rows(i)(2) = True Then
                                            chkApproved.Checked = True
                                            ApproveCount = 1
                                        End If
                                    End If
                                Next

                            End If
                        End If
                    Next
                Next
                ApproveCount = 0
            Else
                SettvRights()
                radtvRights.CollapseAllNodes()
                radtvRights.CheckChildNodes = False
                chkApproved.Checked = False

            End If
        End If
    End Sub
    Private Sub DeleteAllRightsOnRole()

        Dim CurrentRightName As String = ""
        Dim PassedRightName As String = ""

        Try

            For Each rootTreeNode In radtvRights.GetAllNodes()
                If rootTreeNode.Nodes.Count > 0 And Not rootTreeNode.Value = "top" Then
                    Dim name As String = rootTreeNode.Value.ToString
                    If Not PassedRightName = rootTreeNode.Value.ToString Then
                        'Delete DNN Tab Permissions
                        ICBankRightsContoller.DeleteRightToRole(ddlRole.SelectedValue.ToString(), ICBankRightsContoller.GetTabIDByRightName(rootTreeNode.Value.ToString).ToString)
                        'Delete IC_RoleRights Entries
                        CurrentRightName = rootTreeNode.Value.ToString
                        ICBankRoleRightsController.DeleteRoleRight(ddlRole.SelectedValue.ToString(), CurrentRightName, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        'End If
                    End If
                    If Not CurrentRightName = "" Then

                        PassedRightName = CurrentRightName

                    Else
                        PassedRightName = ""
                    End If

                End If

            Next
            DataCache.ClearPortalCache(Me.PortalId, True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub AddRightsOnRole()

        Dim ChildNodesOfRootNode As New ArrayList
        Dim ValueOfChildOfRootNode As String()
        Dim AssignedRightDetailValue As Boolean = False
        Dim NodeInArray As RadTreeNode
        Dim PassedRightName As String = ""
        Try
            For Each RootNodeOfTreeView In radtvRights.GetAllNodes()

                If RootNodeOfTreeView.Checked = True And RootNodeOfTreeView.GetAllNodes.Count > 0 And Not RootNodeOfTreeView.Value = "top" Then
                    Dim CurrentRightName As String = RootNodeOfTreeView.Value.ToString


                    If Not PassedRightName = CurrentRightName Then
                        ChildNodesOfRootNode.Clear()
                        ChildNodesOfRootNode = GetChildrenOfParentNode(ChildNodesOfRootNode, RootNodeOfTreeView)

                        For i = 0 To ChildNodesOfRootNode.Count - 1
                            NodeInArray = New RadTreeNode
                            NodeInArray = ChildNodesOfRootNode(i)
                            If NodeInArray.Checked = True Then
                                AssignedRightDetailValue = True
                            Else
                                AssignedRightDetailValue = False
                            End If
                            ValueOfChildOfRootNode = Nothing
                            ValueOfChildOfRootNode = NodeInArray.Value.Split(",")
                            'If i = 1 Then
                            '    ICBankRightsContoller.AddRightToRole(ddlRole.SelectedValue.ToString(), ICBankRightsContoller.GetTabIDByRightName(CurrentRightName.ToString))
                            '    ICUtilities.AddAuditTrail("Tab Permission : Set " & RootNodeOfTreeView.Text.ToString & " Tab Permission To " & ddlRole.SelectedItem.Text.ToString() & " Role.", "Tab Permission", ddlRole.SelectedValue.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())

                            'End If

                            Dim objICRoleRights As New ICRoleRights

                            objICRoleRights.es.Connection.CommandTimeout = 3600

                            objICRoleRights.RoleID = ddlRole.SelectedValue.ToString()
                            objICRoleRights.RightName = RootNodeOfTreeView.Text.ToString
                            objICRoleRights.RightDetailName = ValueOfChildOfRootNode(1).ToString
                            objICRoleRights.RightValue = AssignedRightDetailValue
                            objICRoleRights.CreatedBy = Me.UserId
                            objICRoleRights.CreatedDate = Date.Now
                            objICRoleRights.Creater = Me.UserId
                            objICRoleRights.CreationDate = Date.Now
                            objICRoleRights.IsApproved = False
                            ICBankRightsContoller.AddRoleRight(objICRoleRights, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)

                        Next
                        PassedRightName = RootNodeOfTreeView.Text.ToString
                    End If

                End If
            Next

            DataCache.ClearPortalCache(Me.PortalId, True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproved.Click
        If Page.IsValid Then
            Dim ChildNodesOfRootNode As New ArrayList
            Dim dtRoleRightsID As DataTable
            Dim AssignedRightDetailValue As Boolean = False
            Dim NodeInArray As RadTreeNode
            Dim PassedRightName As String = ""
            Dim RecordCount As Integer = 0
            Try
                If Not chkApproved.Checked = False Then
                    If ValidateAccessType() = False Then
                        UIUtilities.ShowDialog(Me, "Error", "Assign rights to role first", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    For Each RootNodeOfTreeView In radtvRights.GetAllNodes()
                        If RootNodeOfTreeView.Checked = True And RootNodeOfTreeView.Nodes.Count > 0 And Not RootNodeOfTreeView.Value = "top" Then
                            Dim CurrentRightName As String = RootNodeOfTreeView.Value.ToString
                            If Not PassedRightName = CurrentRightName Then
                                dtRoleRightsID = New DataTable
                                dtRoleRightsID.Clear()
                                dtRoleRightsID = ICBankRoleRightsController.GetRoleRightIDByRightNameAndRoleID(CurrentRightName.ToString, ddlRole.SelectedValue.ToString)
                                If RecordCount = 0 Then
                                    If dtRoleRightsID.Rows.Count = 0 Then
                                        UIUtilities.ShowDialog(Me, "Error", "Assign rights to role first.", ICBO.IC.Dialogmessagetype.Failure)
                                        Exit Sub
                                    End If
                                    For Each dr As DataRow In dtRoleRightsID.Rows
                                        If dr("IsApproved") = True Then
                                            UIUtilities.ShowDialog(Me, "Error", "Role rights are already approved.", ICBO.IC.Dialogmessagetype.Failure)
                                            Exit Sub
                                        End If
                                    Next
                                    For Each dr As DataRow In dtRoleRightsID.Rows
                                        If Me.UserInfo.IsSuperUser = False Then
                                            If dr("CreatedBy").ToString = Me.UserId.ToString Then
                                                UIUtilities.ShowDialog(Me, "Error", "Role rights must be approved by other than maker.", ICBO.IC.Dialogmessagetype.Failure)
                                                Exit Sub
                                            End If
                                        End If
                                    Next
                                End If
                                ICBankRightsContoller.AddRightToRole(ddlRole.SelectedValue.ToString(), ICBankRightsContoller.GetTabIDByRightName(CurrentRightName.ToString), RootNodeOfTreeView.Text.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)


                                ChildNodesOfRootNode.Clear()
                                ChildNodesOfRootNode = GetChildrenOfParentNode(ChildNodesOfRootNode, RootNodeOfTreeView)

                                If dtRoleRightsID.Rows.Count > 0 Then
                                    For i = 0 To dtRoleRightsID.Rows.Count - 1
                                        NodeInArray = New RadTreeNode
                                        NodeInArray = ChildNodesOfRootNode(i)
                                        If NodeInArray.Checked = True Then
                                            ICBankRoleRightsController.ApproveBankRoleRights(dtRoleRightsID.Rows(i)(0).ToString, NodeInArray.Checked, Date.Now, Me.UserId.ToString, Me.UserInfo.Username)
                                        End If

                                    Next
                                End If
                                PassedRightName = RootNodeOfTreeView.Text.ToString
                                RecordCount = 1
                            End If

                        End If
                    Next
                    UIUtilities.ShowDialog(Me, "Role Rights", "Rights for role approved successfully.", ICBO.IC.Dialogmessagetype.Success)
                    DataCache.ClearPortalCache(Me.PortalId, True)
                    SetTreeViewByRoleID(ddlRole.SelectedValue.ToString)
                Else
                    UIUtilities.ShowDialog(Me, "Role Rights", "Please check approve check box.", ICBO.IC.Dialogmessagetype.Failure)
                    'ddlRole.ClearSelection()
                    'SetTreeViewByRoleID(ddlRole.SelectedValue.ToString)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                If ValidateAccessType() = False Then
                    UIUtilities.ShowDialog(Me, "Error", "Please select at least one role right.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                DeleteAllRightsOnRole()
                AddRightsOnRole()
                SetTreeViewByRoleID(ddlRole.SelectedValue.ToString)
                chkApproved.Checked = False
                UIUtilities.ShowDialog(Me, "Client Role Right", "Assign rights to client role successfully.", ICBO.IC.Dialogmessagetype.Success)
                'End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Function ValidateAccessType() As Boolean
        Dim radTreeNode As RadTreeNode
        'Dim parentRadTreeNode As RadTreeNode
        Dim Result As Boolean = False
        Try
            For Each radTreeNode In radtvRights.Nodes
                If radTreeNode.Checked = True Then
                    Result = True
                    Return Result
                    Exit Function
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
End Class
