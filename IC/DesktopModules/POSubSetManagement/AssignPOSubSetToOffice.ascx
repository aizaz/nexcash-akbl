﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AssignPOSubSetToOffice.ascx.vb" Inherits="DesktopModules_POSubSetManagement_AssignPOSubSetToOffice" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .style1
    {
        width: 25%;
        height: 13px;
    }
</style>
<script language="javascript" type="text/javascript">    
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }
    function ValidateChkList(source, arguments) {

        arguments.IsValid = IsCheckBoxChecked() ? true : false;



    }




</script>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
        <telerik:RegExpTextBoxSetting BehaviorID="radSubSeriesFrom" ValidationExpression="[0-9]{1,18}" Validation-IsRequired="true"
        ErrorMessage="Invalid Sub Set From" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtSubSeriesFrom" />
        </TargetControls>
        </telerik:RegExpTextBoxSetting>
   
            <telerik:RegExpTextBoxSetting BehaviorID="radSubSeriesTo" ValidationExpression="[0-9]{1,18}" Validation-IsRequired="true"
        ErrorMessage="Invalid Sub Set To" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtSubSeriesTo" />
        </TargetControls>
        </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reRemarks" Validation-IsRequired="false" ErrorMessage="Invalid Remarks"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRemarks" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
                        Note:&nbsp; Fields marked as
                        <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*"></asp:Label>
                        &nbsp;are required.</td>
    </tr>
    </table>

     <table width="100%">
    <tr>
        <td style="width: 100%">
            <table id="tblSlctBank" runat="server" style="display:none;width: 100%"><%--;display:none;--%>
                <tr>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblSelectBank" runat="server" Text="Select Bank" CssClass="lbl"></asp:Label>
                        <asp:Label ID="lblReqBank" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlBank" runat="server" AutoPostBack="True"
                            CssClass="dropdown">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:RequiredFieldValidator ID="rfvSelectBank" runat="server" ControlToValidate="ddlBank"
                            CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Bank" SetFocusOnError="True"
                            ToolTip="Required Field" InitialValue="0" Enabled="False"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
            </table>
             <table id="tblSlctCompanyOffice" runat="server" style="width: 100%">
                <tr>
                    <td style="width: 25%">
                        <asp:Label ID="lblOffice" runat="server" Text="Select Location" CssClass="lbl"></asp:Label>
                        <asp:Label ID="lblReqLOcation" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                    <td style="width: 25%">
                        <asp:Label ID="lblMasterSeries" runat="server" Text="Select Master Series" 
                            CssClass="lbl"></asp:Label>
                        <asp:Label ID="lblreqOffice0" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%">
                        <asp:DropDownList ID="ddlOffice" runat="server" CssClass="dropdown"
                            AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                    <td style="width: 25%">
                        <asp:DropDownList ID="ddlMasterSeries" runat="server" CssClass="dropdown"
                            AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        <asp:RequiredFieldValidator ID="rfvLocation" runat="server" ControlToValidate="ddlOffice"
                            CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Location" SetFocusOnError="True"
                            ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
                    </td>
                    <td class="style1">
                        &nbsp;
                    </td>
                    <td class="style1">
                        <asp:RequiredFieldValidator ID="rfvMasterSeries" runat="server" ControlToValidate="ddlMasterSeries"
                            CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Master Series"
                            SetFocusOnError="True" ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
                    </td>
                    <td class="style1">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%">
                        <asp:Label ID="lblSubSeriesFrom" runat="server" Text="Sub Set From" 
                            CssClass="lbl"></asp:Label>
                        <asp:Label ID="lblblReqSubSetFrom" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td style="width: 25%">
                        &nbsp;</td>
                    <td style="width: 25%">
                        <asp:Label ID="lblSubSeriesTo" runat="server" Text="Sub Set To" 
                            CssClass="lbl"></asp:Label>
                        <asp:Label ID="lblblReqSubSetTo" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td style="width: 25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 25%">
                                                            <asp:TextBox ID="txtSubSeriesFrom" 
                            runat="server" CssClass="txtbox" MaxLength="18"></asp:TextBox>
                                            </td>
                    <td style="width: 25%">
                        &nbsp;</td>
                    <td style="width: 25%">
                                                            <asp:TextBox ID="txtSubSeriesTo" 
                            runat="server" CssClass="txtbox" MaxLength="18"></asp:TextBox>
                                            </td>
                    <td style="width: 25%">
                        &nbsp;</td>
                </tr>
                 <tr>
                    <td style="width: 25%">
                       <asp:RangeValidator ID="RangeValidator1" runat="server" Type="Double" MinimumValue="0" MaximumValue="1" ErrorMessage="Sub set from is out of range" ControlToValidate="txtSubSeriesFrom"></asp:RangeValidator></td>
                    <td style="width: 25%">
                        &nbsp;</td>
                    <td style="width: 25%">
                         <asp:CompareValidator ID="cmpValidatorSubSetFrom" runat="server" ControlToValidate="txtSubSeriesTo" ControlToCompare="txtSubSeriesFrom" Operator="GreaterThan" Type="Double"  ErrorMessage="Sub set to should be greater than sub set from">  
                </asp:CompareValidator><br /><asp:RangeValidator ID="rvSubSetTo" runat="server" Type="Double" MinimumValue="0" MaximumValue="1" ErrorMessage="Sub set to is out of range" ControlToValidate="txtSubSeriesTo"></asp:RangeValidator> </td>
                    <td style="width: 25%">
                        &nbsp;</td>
                </tr>
                 </table>
        </td>
    </tr> 
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" 
                           CausesValidation="False" />
        </td>
    </tr>
     </table>

    