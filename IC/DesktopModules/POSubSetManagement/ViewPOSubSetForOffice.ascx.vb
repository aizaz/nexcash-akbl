﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_POSubSetManagement_ViewPOSubSetForOffice
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase

    Private htRights As Hashtable

#Region "Page Load"
    Protected Sub DesktopModules_POSubSetManagement_ViewPOSubSetForOffice_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnSaveSubSeries.Visible = CBool(htRights("Add"))
                btnDeleteSubSeries.Visible = CBool(htRights("Delete"))
                LoadddlBankBranch()
                LoadddlMasterSeries()
                LoadGridViewMasterSeries(0, Me.gvViewSubSeries.PageSize, True)

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "PO Sub Set Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#Region "Drop Down Events"


    Private Sub LoadddlBankBranch()
        Try
            Dim lit As New ListItem

            lit.Value = "0"
            lit.Text = "-- All --"
            ddlBankBranch.Items.Clear()
            ddlBankBranch.Items.Add(lit)
            ddlBankBranch.AppendDataBoundItems = True
            ddlBankBranch.DataSource = ICOfficeController.GetPrincipalBankBranchActiveAndApprovePrintingLocations()
            ddlBankBranch.DataTextField = "OfficeName"
            ddlBankBranch.DataValueField = "OfficeID"
            ddlBankBranch.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlMasterSeries()
        Try

            Dim lit As New ListItem
            Dim dt As New DataTable
            Dim ArrStr As String()
            ArrStr = Nothing
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlMasterSeries.Items.Clear()
            ddlMasterSeries.Items.Add(lit)
            ddlMasterSeries.AppendDataBoundItems = True
            'dt = ICMasterSeriesController.GetAllMasterSeries()
            
                For Each MSItem In ICPOMasterSeriesController.GetAllPOMasterSeries()
                    ddlMasterSeries.Items.Add(New ListItem(MSItem.StartFrom & "-" & MSItem.EndsAt, MSItem.POMasterSeriesID))
                Next


            ddlMasterSeries.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region
    Private Sub LoadGridViewMasterSeries(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean)
        Try
            Dim BankBranchCode, MasterSeriesID As String
            Dim AccountNumber As String() = Nothing


            If ddlBankBranch.SelectedValue.ToString <> "0" Then
                BankBranchCode = ddlBankBranch.SelectedValue.ToString
            Else
                BankBranchCode = ""
            End If

            If ddlMasterSeries.SelectedValue.ToString <> "0" Then
                MasterSeriesID = ddlMasterSeries.SelectedValue.ToString
            Else
                MasterSeriesID = ""

            End If

            ICPOInstrumentsController.GetAllAssignedSubSetsToPrintLocationsForRadGrid(PageNumber, PageSize, DoDataBind, Me.gvViewSubSeries, BankBranchCode, MasterSeriesID)
            If gvViewSubSeries.Items.Count > 0 Then
                gvViewSubSeries.Visible = True
                lblRNF.Visible = False
                btnDeleteSubSeries.Visible = CBool(htRights("Delete"))
                gvViewSubSeries.Columns(10).Visible = CBool(htRights("Delete"))
            Else
                gvViewSubSeries.Visible = False
                lblRNF.Visible = True
                btnDeleteSubSeries.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#Region "Other Functions/Routines"

    Private Sub Clear()
        ddlMasterSeries.ClearSelection()
    End Sub



#End Region
    Protected Sub btnSaveSubSeries_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveSubSeries.Click
        Page.Validate()
        If Page.IsValid Then
            Response.Redirect(NavigateURL("AssignPOSubSetToOffice", "&mid=" & Me.ModuleId & "&POMasterSeriesID=0" & "&Officecode=0" & "&SubSetFrom=0" & "&SubSetTo=0"), False)
        End If
    End Sub




    Protected Sub ddlMasterSeries_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMasterSeries.SelectedIndexChanged
        Try
            LoadGridViewMasterSeries(Me.gvViewSubSeries.CurrentPageIndex + 1, Me.gvViewSubSeries.PageSize, True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub




    Protected Sub ddlBankBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBankBranch.SelectedIndexChanged
        Try

            LoadGridViewMasterSeries(Me.gvViewSubSeries.CurrentPageIndex + 1, Me.gvViewSubSeries.PageSize, True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub gvViewSubSeries_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvViewSubSeries.ItemCommand
        Try
            Dim objICSubSet As New ICPOInstruments
            Dim StrArrCommandArgument As String()
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    StrArrCommandArgument = e.CommandArgument.ToString.Split(";")
                    If ICPOInstrumentsController.IsPOInstrumentIsUsedOrAssignedAgainstSubSetIDAndOfficeID(StrArrCommandArgument(0).ToString, StrArrCommandArgument(1).ToString, StrArrCommandArgument(2).ToString, StrArrCommandArgument(3).ToString) = False Then
                        ICPOInstrumentsController.DeletePOInstrumentsAgainstOfficeID(StrArrCommandArgument(0).ToString, StrArrCommandArgument(1).ToString, StrArrCommandArgument(2).ToString, StrArrCommandArgument(3).ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        UIUtilities.ShowDialog(Me, "Delete PO Sub Set", "Sub Set deleted successfuly.", ICBO.IC.Dialogmessagetype.Success)
                        LoadGridViewMasterSeries(Me.gvViewSubSeries.CurrentPageIndex + 1, Me.gvViewSubSeries.PageSize, True)
                    Else
                        LoadGridViewMasterSeries(Me.gvViewSubSeries.CurrentPageIndex + 1, Me.gvViewSubSeries.PageSize, True)
                        UIUtilities.ShowDialog(Me, "Delete PO Sub Set", "Sub Set can not be deleted due to following reasons: <br /> 1. Instrument(s)  is used against sub set<br /> 2. Delete associated records", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvViewSubSeries_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvViewSubSeries.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvViewSubSeries.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvViewSubSeries_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvViewSubSeries.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvViewSubSeries.MasterTableView.ClientID & "','0');"
        End If
        'If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
        '    Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        '    item.Cells(2).Attributes.Add("style", "word-break:break-all;word-wrap:break-word")
        'End If

    End Sub

    Protected Sub gvViewSubSeries_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvViewSubSeries.PageIndexChanged
        Try
            gvViewSubSeries.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvViewSubSeries_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvViewSubSeries.NeedDataSource
        Try
            LoadGridViewMasterSeries(Me.gvViewSubSeries.CurrentPageIndex + 1, Me.gvViewSubSeries.PageSize, False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckGVCompanySelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvViewSubSeriesRow As GridDataItem In gvViewSubSeries.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvViewSubSeriesRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnDeleteSubSeries_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteSubSeries.Click
        Page.Validate()
        If Page.IsValid Then
            Try
                Dim chkBox As CheckBox
                Dim objICSubSet As ICSubSet
                Dim PassToDeleteCount As Integer = 0
                Dim FailToDeleteCount As Integer = 0
                Dim POMasterSeriesID, OfficeCode, SubSetFrom, SubSetTo As String

                If CheckGVCompanySelected() = False Then
                    UIUtilities.ShowDialog(Me, "Error", "Please select at least one sub set", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each gvViewSubSeriesRow As GridDataItem In gvViewSubSeries.Items
                    chkBox = New CheckBox
                    chkBox = DirectCast(gvViewSubSeriesRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkBox.Checked = True Then
                        POMasterSeriesID = Nothing
                        OfficeCode = Nothing
                        SubSetFrom = Nothing
                        SubSetTo = Nothing
                        POMasterSeriesID = gvViewSubSeriesRow.GetDataKeyValue("POMasterSeriesID")
                        OfficeCode = gvViewSubSeriesRow.GetDataKeyValue("OfficeCode")
                        SubSetFrom = gvViewSubSeriesRow.GetDataKeyValue("SubsetFrom")
                        SubSetTo = gvViewSubSeriesRow.GetDataKeyValue("SubsetTo")
                        If ICPOInstrumentsController.IsPOInstrumentIsUsedOrAssignedAgainstSubSetIDAndOfficeID(POMasterSeriesID, OfficeCode, SubSetFrom, SubSetTo) = False Then
                            ICPOInstrumentsController.DeletePOInstrumentsAgainstOfficeID(POMasterSeriesID, OfficeCode, SubSetFrom, SubSetTo, Me.UserId.ToString, Me.UserInfo.Username.ToString)

                            PassToDeleteCount = PassToDeleteCount + 1
                        Else
                            FailToDeleteCount = FailToDeleteCount + 1
                        End If
                    End If
                Next
                If Not PassToDeleteCount = 0 And Not FailToDeleteCount = 0 Then
                    LoadGridViewMasterSeries(Me.gvViewSubSeries.CurrentPageIndex + 1, Me.gvViewSubSeries.PageSize, True)
                    UIUtilities.ShowDialog(Me, "Delete Sub Set", "[ " & PassToDeleteCount.ToString & " ] Sub Set(s) deleted successfuly.<br /> [ " & FailToDeleteCount.ToString & " ] Sub Set(s) can not be deleted due to following reason: <br /> 1. Instrument(s)  is used against sub set<br /> 2. Delete associated records", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                ElseIf Not PassToDeleteCount = 0 And FailToDeleteCount = 0 Then
                    LoadGridViewMasterSeries(Me.gvViewSubSeries.CurrentPageIndex + 1, Me.gvViewSubSeries.PageSize, True)
                    UIUtilities.ShowDialog(Me, "Delete Sub Set", PassToDeleteCount.ToString & " Sub Set(s) deleted successfuly.", ICBO.IC.Dialogmessagetype.Success)

                    Exit Sub
                ElseIf Not FailToDeleteCount = 0 And PassToDeleteCount = 0 Then
                    LoadGridViewMasterSeries(Me.gvViewSubSeries.CurrentPageIndex + 1, Me.gvViewSubSeries.PageSize, True)
                    UIUtilities.ShowDialog(Me, "Delete Sub Set", "[ " & FailToDeleteCount.ToString & " ] Sub Set(s) can not be deleted due to following reasons: <br /> 1. Instrument(s)  is used against sub set<br /> 2. Delete associated records", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
End Class
