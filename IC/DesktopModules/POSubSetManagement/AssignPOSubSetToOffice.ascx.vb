﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Partial Class DesktopModules_POSubSetManagement_AssignPOSubSetToOffice
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase

    Private POMasterSeriesID, OfficeCode, SubSetFrom, SubSetTo As String
    Private htRights As Hashtable
    Protected Sub DesktopModules_POSubSetManagement_AssignPOSubSetToOffice_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            POMasterSeriesID = Request.QueryString("POMasterSeriesID").ToString
            Officecode = Request.QueryString("Officecode").ToString
            SubSetFrom = Request.QueryString("SubSetFrom").ToString
            SubSetTo = Request.QueryString("SubSetTo").ToString
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlBankBranch()
                LoadddlMasterSeries()

                If POMasterSeriesID.ToString = "0" And OfficeCode.ToString = "0" And SubSetFrom.ToString = "0" And SubSetTo.ToString = "0" Then
                    lblPageHeader.Text = "Add PO Sub Set"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    btnSave.Visible = CBool(htRights("Update"))
                    lblPageHeader.Text = "Update PO Sub Set"
                    btnSave.Text = "Update"
                    Dim objICPOInstrument As New ICPOInstruments
                    Dim objICOffice As New ICOffice
                    Dim objICPOMasterSeries As New ICPOMasterSeries


                    objICPOInstrument.es.Connection.CommandTimeout = 3600
                    objICOffice.es.Connection.CommandTimeout = 3600
                    objICPOMasterSeries.es.Connection.CommandTimeout = 3600

                    ddlOffice.Enabled = False

                    ddlMasterSeries.Enabled = False
                    txtSubSeriesFrom.ReadOnly = True
                    objICPOInstrument = ICPOInstrumentsController.GetSinglePOInstrumentObjectByPOIDOFFiceCodeAndsubSetRange(POMasterSeriesID, SubSetFrom, SubSetTo, OfficeCode)
                    objICOffice.LoadByPrimaryKey(objICPOInstrument.OfficeCode.ToString)
                    LoadddlBankBranch()
                    ddlOffice.SelectedValue = objICOffice.OfficeID
                    LoadddlMasterSeries()
                    ddlMasterSeries.SelectedValue = objICPOInstrument.POMasterSeriesID
                    objICPOMasterSeries.LoadByPrimaryKey(ddlMasterSeries.SelectedValue.ToString)
                    RangeValidator1.MinimumValue = CLng(objICPOMasterSeries.StartFrom)
                    RangeValidator1.MaximumValue = CLng(objICPOMasterSeries.EndsAt - 1)
                    rvSubSetTo.MinimumValue = CLng(objICPOMasterSeries.StartFrom + 1)
                    rvSubSetTo.MaximumValue = CLng(objICPOMasterSeries.EndsAt)
                    txtSubSeriesFrom.Text = objICPOInstrument.SubsetFrom
                    txtSubSeriesTo.Text = objICPOInstrument.SubsetTo
                End If


            End If


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "PO Sub Set Management")
            ViewState("htRights") = htRights

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlBankBranch()
        Try
            Dim lit As New ListItem

            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlOffice.Items.Clear()
            ddlOffice.Items.Add(lit)
            ddlOffice.AppendDataBoundItems = True

            ddlOffice.DataSource = ICOfficeController.GetPrincipalBankBranchActiveAndApprovePrintingLocations()
            ddlOffice.DataTextField = "OfficeName"
            ddlOffice.DataValueField = "OfficeID"
            ddlOffice.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlMasterSeries()
        Try

            Dim lit As New ListItem
            Dim dt As New DataTable

            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlMasterSeries.Items.Clear()
            ddlMasterSeries.Items.Add(lit)
            ddlMasterSeries.AppendDataBoundItems = True
            For Each MSItem In ICPOMasterSeriesController.GetAllPOMasterSeries()
                ddlMasterSeries.Items.Add(New ListItem(MSItem.StartFrom & "-" & MSItem.EndsAt, MSItem.POMasterSeriesID))
            Next

            ddlMasterSeries.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL(), False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    
    
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim objICPOInstrument As New ICPOInstruments
                Dim objICPOUInstrumentColl As New ICPOInstrumentsCollection
                Dim objICPOUInstrumentCollForSave As New ICPOInstrumentsCollection
                Dim objICOFficeTo As New ICOffice
                Dim objICPOInstrumentFrom As New ICPOInstruments
                Dim ArrStrForAccount As String()
                Dim SavedSubSetID As Integer = 0
                Dim AssignedLeaves As Long = 0
                Dim UpDatedLeaves As Integer = 0
                Dim TopOrder As Integer = 0
                Dim StrAuditTrail As String = Nothing
                Dim objICAT As New ICAuditTrail
                ArrStrForAccount = Nothing

                'If IsSubSetExistsInMasterSeries(ddlMasterSeries.SelectedValue.ToString, txtSubSeriesFrom.Text.ToString, txtSubSeriesTo.Text.ToString) = False Then
                '    UIUtilities.ShowDialog(Me, "Error", "Sub set not exists", ICBO.IC.Dialogmessagetype.Failure)
                '    Exit Sub
                'End If
                objICPOInstrument.es.Connection.CommandTimeout = 3600
                objICPOInstrumentFrom.es.Connection.CommandTimeout = 3600


                objICPOInstrument.SubsetFrom = CLng(txtSubSeriesFrom.Text)
                objICPOInstrument.SubsetTo = CLng(txtSubSeriesTo.Text)
                objICPOInstrument.OfficeCode = ddlOffice.SelectedValue.ToString
                objICPOInstrument.POMasterSeriesID = ddlMasterSeries.SelectedValue.ToString

                If POMasterSeriesID.ToString = "0" And OfficeCode.ToString = "0" And SubSetFrom.ToString = "0" And SubSetTo.ToString = "0" Then
                    If ICPOInstrumentsController.IsPOInstrumentSeriesIsAssignedOrPrintedForSubSet(objICPOInstrument.SubsetFrom, objICPOInstrument.SubsetTo, objICPOInstrument.POMasterSeriesID) = False Then
                        objICPOInstrument.CreatedDate = Date.Now
                        objICPOInstrument.CreatedBy = Me.UserId
                        objICPOInstrument.CreationDate = Date.Now
                        objICPOInstrument.Creater = Me.UserId
                        objICPOUInstrumentColl = ICPOInstrumentsController.GetPOInstrumentsForSubSetByPOMasterSeriesIDAndSubSetRange(ddlMasterSeries.SelectedValue.ToString, objICPOInstrument.SubsetFrom, objICPOInstrument.SubsetTo)
                        For Each objICPOInstruments As ICPOInstruments In objICPOUInstrumentColl
                            objICPOInstruments.SubsetFrom = objICPOInstrument.SubsetFrom
                            objICPOInstruments.SubsetTo = objICPOInstrument.SubsetTo
                            objICPOInstruments.OfficeCode = objICPOInstrument.OfficeCode
                            objICPOUInstrumentCollForSave.Add(objICPOInstruments)
                        Next
                        If objICPOUInstrumentCollForSave.Count > 0 Then
                            objICPOUInstrumentCollForSave.Save()
                            objICOFficeTo.LoadByPrimaryKey(ddlOffice.SelectedValue.ToString)
                            StrAuditTrail = Nothing
                            StrAuditTrail = "PO Sub set is assigned to branch [ " & objICOFficeTo.OfficeCode & " ] [ " & objICOFficeTo.OfficeName & " ]"
                            StrAuditTrail += " from [ " & objICPOInstrument.SubsetFrom & " ] to [ " & objICPOInstrument.SubsetTo & " ] of PO series [ " & ddlMasterSeries.SelectedValue & " ] ."
                            ICUtilities.AddAuditTrail(StrAuditTrail, "PO Sub Set", ddlOffice.SelectedValue.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ASSIGN")
                        End If
                        UIUtilities.ShowDialog(Me, "Save Sub Set", "Sub set assigned to office successfully.", ICBO.IC.Dialogmessagetype.Success)
                        clear()

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Part of subset is already assigned to another branch", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else
                    objICPOInstrumentFrom = ICPOInstrumentsController.GetSinglePOInstrumentObjectByPOIDOFFiceCodeAndsubSetRange(POMasterSeriesID, SubSetFrom, SubSetTo, OfficeCode)
                    objICPOInstrument.CreatedBy = Me.UserId
                    objICPOInstrument.CreationDate = Date.Now
                    AssignedLeaves = CLng(CLng(objICPOInstrumentFrom.SubsetTo) - CLng(objICPOInstrumentFrom.SubsetFrom))
                    UpDatedLeaves = CLng(CLng(objICPOInstrument.SubsetTo) - CLng(objICPOInstrument.SubsetFrom))
                    If UpDatedLeaves < AssignedLeaves Then
                        TopOrder = AssignedLeaves - UpDatedLeaves
                        If ICPOInstrumentsController.IsPOInstrumentsExistForUpdateOfPOSubSet(TopOrder, objICPOInstrument, objICPOInstrumentFrom.SubsetTo.ToString, False) = True Then
                            ICPOInstrumentsController.UpDatePOInstrumentsForUpdateOfSubSet(TopOrder, objICPOInstrument, objICPOInstrumentFrom.SubsetTo.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, False)
                            objICPOInstrumentFrom = New ICPOInstruments
                            objICPOInstrumentFrom = ICPOInstrumentsController.GetSinglePOInstrumentObjectByPOIDOFFiceCodeAndsubSetRange(POMasterSeriesID, txtSubSeriesFrom.Text.ToString, txtSubSeriesTo.Text.ToString, OfficeCode)
                            ICPOInstrumentsController.UpDateSubSetRangeOnUpDate(objICPOInstrumentFrom.POMasterSeriesID, CLng(txtSubSeriesFrom.Text), CLng(txtSubSeriesTo.Text.ToString), objICPOInstrumentFrom.OfficeCode.ToString, objICPOInstrument)
                            UIUtilities.ShowDialog(Me, "Save Sub Set", "Sub set updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Part of subset is already assigned to another location or sub set is already used.", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    ElseIf UpDatedLeaves > AssignedLeaves Then
                        TopOrder = ((AssignedLeaves - UpDatedLeaves) * (-1))
                        If ICPOInstrumentsController.IsPOInstrumentsExistForUpdateOfPOSubSet(TopOrder, objICPOInstrument, objICPOInstrumentFrom.SubsetTo.ToString, True) = True Then
                            ICPOInstrumentsController.UpDatePOInstrumentsForUpdateOfSubSet(TopOrder, objICPOInstrument, objICPOInstrumentFrom.SubsetTo.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, True)
                            objICPOInstrumentFrom = New ICPOInstruments
                            objICPOInstrumentFrom = ICPOInstrumentsController.GetSinglePOInstrumentObjectByPOIDOFFiceCodeAndsubSetRange(POMasterSeriesID, txtSubSeriesFrom.Text.ToString, txtSubSeriesTo.Text.ToString, OfficeCode)
                            ICPOInstrumentsController.UpDateSubSetRangeOnUpDate(objICPOInstrumentFrom.POMasterSeriesID, CLng(txtSubSeriesFrom.Text), CLng(txtSubSeriesTo.Text.ToString), objICPOInstrumentFrom.OfficeCode.ToString, objICPOInstrumentFrom)
                            UIUtilities.ShowDialog(Me, "Save Sub Set", "Sub set updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Part of subset is already assigned to another location", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                            Exit Sub
                        End If
                    ElseIf UpDatedLeaves = AssignedLeaves Then
                        UIUtilities.ShowDialog(Me, "Save Sub Set", "No changes made in sub set", ICBO.IC.Dialogmessagetype.Failure, NavigateURL)
                        Exit Sub

                    End If
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    
    
    Private Sub clear()
        LoadddlMasterSeries()
        LoadddlBankBranch()
        txtSubSeriesFrom.Text = ""
        txtSubSeriesTo.Text = ""

    End Sub

    Protected Sub ddlMasterSeries_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMasterSeries.SelectedIndexChanged
        If ddlMasterSeries.SelectedValue.ToString <> "0" Then
            Dim objICMasterSeries As New ICPOMasterSeries

            objICMasterSeries.LoadByPrimaryKey(ddlMasterSeries.SelectedValue.ToString)

            RangeValidator1.MinimumValue = CLng(objICMasterSeries.StartFrom)
            RangeValidator1.MaximumValue = CLng(objICMasterSeries.EndsAt - 1)
            rvSubSetTo.MinimumValue = CLng(objICMasterSeries.StartFrom + 1)
            rvSubSetTo.MaximumValue = CLng(objICMasterSeries.EndsAt)
        ElseIf ddlMasterSeries.SelectedValue.ToString = "0" Then
            RangeValidator1.MinimumValue = 0
            RangeValidator1.MaximumValue = 0
            rvSubSetTo.MinimumValue = 0
            rvSubSetTo.MaximumValue = 0
        End If
    End Sub
End Class
