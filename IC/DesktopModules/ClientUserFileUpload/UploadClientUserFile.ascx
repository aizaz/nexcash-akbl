﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UploadClientUserFile.ascx.vb"
    Inherits="DesktopModules_AgentFile_SaveAgentFile" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .Grid
    {
        margin-top: 0px;
    }
    .btn
    {
    }
</style>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior2" EmptyMessage="type.."
        Type="Number">
    </telerik:NumericTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior2" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9 .-_/]*$" ErrorMessage="Invalid Character" EmptyMessage="Enter Bank Code">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBankCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z ]*$" ErrorMessage="Invalid Character" EmptyMessage="Enter Bank Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBankName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior3" Validation-IsRequired="false"
        EmptyMessage="Enter Bank Description">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBankDescription" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior4" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9]*$" ErrorMessage="Invalid Character" EmptyMessage="Enter Bank IMD">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBankIMD" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior5" Validation-IsRequired="true"
        ValidationExpression="^[0-9- ]*$" ErrorMessage="Invalid Character" EmptyMessage="Enter Bank Phone No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBankPhoneNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior6" Validation-IsRequired="false"
        EmptyMessage="Enter Bank Website">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBankWebsite" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue">Upload File</asp:Label>
            <br />
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" colspan="2">
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label5" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label6" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlGroup" runat="server" ControlToValidate="ddlGroup"
                Display="Dynamic" ErrorMessage="Please select Group" InitialValue="0" 
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" ControlToValidate="ddlCompany"
                Enabled="true" Display="Dynamic" ErrorMessage="Please select Company" InitialValue="0"
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:Label ID="lblSelectAccountPNature" runat="server" Text="Select Account Payment Nature"
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:Label ID="lblFileUploadTemplate" runat="server" Text="Select File Upload Template"
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label7" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:DropDownList ID="ddlAcccountPaymentNature" runat="server" CssClass="dropdown"
                AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:DropDownList ID="ddlFileUploadTemplate" runat="server" CssClass="dropdown">
                
            </asp:DropDownList>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlAccPNature" runat="server" ControlToValidate="ddlAcccountPaymentNature"
                Display="Dynamic" ErrorMessage="Please select Account Payment Nature" InitialValue="0"
                Enabled="true" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlFileTemplate" runat="server" ControlToValidate="ddlFileUploadTemplate"
                Enabled="true" Display="Dynamic" 
                ErrorMessage="Please select File Upload Template" InitialValue="0"
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblOFACList" runat="server" Text="Select File" CssClass="lbl"></asp:Label><asp:Label
                ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" colspan="2">
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:FileUpload ID="fuplClientFile" runat="server" />
            <br />
            <asp:RequiredFieldValidator ID="rfvFileUpload" runat="server" ErrorMessage="Please Select File."
                ControlToValidate="fuplClientFile"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" 
                Width="69px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" CausesValidation="False"
                Width="69px" />
            &nbsp;
        </td>
    </tr>
    <%--<tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <asp:Label ID="lblverified" runat="server" Text="Verified Instruction List" CssClass="headingblue"
                Visible="False"></asp:Label>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Panel ID="pnlVerified" runat="server" BorderWidth="1px" Height="200px" ScrollBars="Vertical"
                Visible="false" Width="1179px">
                <telerik:RadGrid ID="gvVerified" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="true" CellSpacing="0" PageSize="4" ShowFooter="True" ItemStyle-HorizontalAlign="Center"
                    HeaderStyle-HorizontalAlign="Center">
                    <AlternatingItemStyle CssClass="rgAltRow" />
                    <MasterTableView>
                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                        </ExpandCollapseColumn>
                    </MasterTableView>
                    <ItemStyle CssClass="rgRow" />
                </telerik:RadGrid>
            </asp:Panel>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <asp:Button ID="btnProceed" runat="server" Text="Proceed" CssClass="btn" Width="76px"
                Visible="False" />
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="Un-Verified Instruction List" valign="top" colspan="4">
            <asp:Label ID="lblUnverified" runat="server" Text="Un-Verified Instruction List"
                CssClass="headingblue" Visible="False"></asp:Label>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="Un-Verified Instruction List" valign="top" colspan="4">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Panel ID="pnlUnVerified" runat="server" BorderWidth="1px" Height="200px" ScrollBars="Both"
                Visible="false" Width="1179px">
                <telerik:RadGrid ID="gvUnverified" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="false" CellSpacing="0" PageSize="3" ShowFooter="True" HeaderStyle-HorizontalAlign="left">
                    <AlternatingItemStyle CssClass="rgAltRow" />
                    <MasterTableView>
                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn DataField="ErrorMessage" HeaderText="Error Message" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-VerticalAlign="Top" SortExpression="ErrorMessage">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ItemStyle CssClass="rgRow" />
                </telerik:RadGrid>
            </asp:Panel>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <asp:Button ID="btnProceed0" runat="server" Text="Cancel" CssClass="btn" Width="76px"
                Visible="False" />
        </td>
    </tr>--%>
</table>
