﻿Imports ICBO
Imports ICBO.IC
Imports System.Threading
Imports System.Runtime.Remoting.Messaging

Partial Class DesktopModules_ClientUserFileUpload_ClientUserFileUploadProgress
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Implements System.Web.UI.ICallbackEventHandler
    Private Delegate Sub LongRun(ByVal name As String)
    Dim result As String

    Protected Sub DesktopModules_BTFStatement_BTFStatement_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        Try
            If Page.IsPostBack = False Then
                Dim cbReference As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "ReceiveServerData", "context")


                Dim callbackScript As String

                callbackScript = "function CallServer(arg, context)" & "{ " + cbReference + ";}window.onload=function () {CallServer('Percent','');}"

                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "CallServer", callbackScript, True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try


    End Sub




    Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult

        Return result
    End Function




    Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent

        'Dim bp As New EStatements.ESTBulkProgress


        'bp.LoadByPrimaryKey(Request.QueryString("id"))



        If eventArgument = "Percent" Then


            If Not Session("FileUploadingResult") Is Nothing Then



                Dim res As IAsyncResult = CType(Session("FileUploadingResult"), IAsyncResult)



                If res.IsCompleted Then
                    'Response.Redirect(NavigateURL(), False)

                    result = "100"

                Else


                    'result = "-1"

                    'result = bp.PercentCompleted



                End If


            Else


                result = "-1"



            End If

            'Else



            '    If bp.StatementsGenerated = 0 Then

            '        result = bp.Message
            '    Else


            '        result = bp.Message


            '    End If




        End If





    End Sub



End Class
