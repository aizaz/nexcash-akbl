﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewClientUserUploadedFile.ascx.vb"
    Inherits="DesktopModules_AgentFile_ViewAgentFile" %>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
    function disableButton(btnId) {
        var btn = document.getElementById(btnId);
        btn.enabled = false;
    }
</script>
<script language="javascript" type="text/javascript">
    function AdjustWidth(ddl) {
        var maxWidth = 0;
        var Counter = 0;
        for (var i = 0; i < ddl.length; i++) {
            if (ddl.options[i].text.length > maxWidth) {
                Counter = Counter + 1;
                maxWidth = ddl.options[i].text.length;
            }
        }
        if (Counter = 1) { ddl.style.width = "250px"; }

        else {
            alert(Counter);
            ddl.style.width = maxWidth + "px";
        }
        //-->
    }
</script>
<style type="text/css">
.ctrDropDown{
    width:250px;
   
}
.ctrDropDownClick{
    
    width:600px;
 
}
.plainDropDown{
    width:250px;
   
}
</style>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table align="left" cellpadding="0" cellspacing="0" style="width: 100%" id="tblView"
                runat="server">
                <tr>
                    <td align="center" valign="top" colspan="4">
                        <asp:HiddenField ID="hfClientFileID" runat="server" />
                        <asp:HiddenField ID="hfDualDisbMode" runat="server" />
                    </td>
                </tr>
                 <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="Label1" runat="server" CssClass="headingblue">Upload File</asp:Label>
            <br />
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" colspan="2">
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label5" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:DropDownList ID="ViewddlGroup" runat="server" CssClass="dropdown" 
                AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:DropDownList ID="ViewddlCompany" runat="server" CssClass="dropdown" 
                AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlGroup" runat="server" ControlToValidate="ViewddlGroup"
                Display="Dynamic" ErrorMessage="Please select Group" InitialValue="0"  ValidationGroup="save"
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" ControlToValidate="ViewddlCompany"
                Enabled="true" Display="Dynamic" ErrorMessage="Please select Company" InitialValue="0"
                SetFocusOnError="True" ValidationGroup="save"></asp:RequiredFieldValidator>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:Label ID="lblSelectAccountPNature" runat="server" Text="Select Account Payment Nature"
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqAccountPNature" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:Label ID="lblFileUploadTemplate" runat="server" Text="Select File Upload Template"
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label7" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
         <div style= "width:250px; overflow:hidden;">
            <asp:DropDownList ID="ViewddlAcccountPaymentNature" runat="server" class="ctrDropDown"  onblur="this.className='ctrDropDown';" onmousedown="this.className='ctrDropDownClick';" onchange="this.className='ctrDropDown';" AutoPostBack="true" >
            </asp:DropDownList>
            </div>
            <%--<asp:DropDownList ID="ViewddlAcccountPaymentNature" runat="server" CssClass="dropdown"
                AutoPostBack="True">
            </asp:DropDownList>--%>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:DropDownList ID="ViewddlFileUploadTemplate" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
                
            </asp:DropDownList>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlAccPNature" runat="server" ControlToValidate="ViewddlAcccountPaymentNature"
                Display="Dynamic" ErrorMessage="Please select Account Payment Nature" InitialValue="0"
                Enabled="true" SetFocusOnError="True" ValidationGroup="save"></asp:RequiredFieldValidator>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlFileTemplate" runat="server" ControlToValidate="ViewddlFileUploadTemplate"
                Enabled="true" Display="Dynamic" 
                ErrorMessage="Please select File Upload Template" InitialValue="0"
                SetFocusOnError="True" ValidationGroup="save"></asp:RequiredFieldValidator>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblSelectFile" runat="server" Text="Select File" CssClass="lbl"></asp:Label><asp:Label
                ID="lblReqFUPTemplate" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" colspan="2">
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:FileUpload ID="fuplClientFile" runat="server" />
            <br />
            <asp:RequiredFieldValidator ID="rfvFileUpload" runat="server" ErrorMessage="Please Select File."
                ControlToValidate="fuplClientFile" ValidationGroup="save"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" 
                Width="69px" ValidationGroup="save" />
            <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CssClass="btn" 
                Width="69px" CausesValidation="False" />
            &nbsp;<asp:Button ID="btnCancelMain" runat="server" Text="Cancel" 
                CssClass="btnCancel" CausesValidation="False" 
                Width="69px" />
            &nbsp;
        </td>
    </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <asp:Label ID="lblAcqAgentFileListListHeader" runat="server" Text="Client File Upload List"
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%">
                        &nbsp;</td>
                    <td style="width: 25%">
                        &nbsp;</td>
                    <td style="width: 25%">
                        &nbsp;</td>
                    <td style="width: 25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 25%">
                        <asp:Label ID="lblRNF" runat="server" Text="No Record Found" CssClass="headingblue"
                            Visible="False"></asp:Label>
                        &nbsp;
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                         <telerik:RadGrid ID="gvClientFiles" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <ClientSettings>
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <MasterTableView TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                 <telerik:GridBoundColumn DataField="FileID" HeaderText="File ID" SortExpression="FileID">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FileBatchNo" HeaderText="Batch No" SortExpression="FileBatchNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CompanyName" HeaderText="Company" SortExpression="CompanyName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PaymentNatureName" HeaderText="Payment Nature"
                                        SortExpression="PaymentNatureName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="OriginalFileName" HeaderText="File Name" SortExpression="OriginalFileName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CreatedDate" HeaderText="Upload Date" SortExpression="CreatedDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="UserName" HeaderText="Uploaded By" SortExpression="UserName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CreatedBy" HeaderText="Process Date" SortExpression="CreatedBy"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProcessBy" HeaderText="Process By" SortExpression="ProcessBy">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TransactionCount" HeaderText="Count" SortExpression="TransactionCount">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TotalAmount" HeaderText="Amount" SortExpression="TotalAmount"
                                        HtmlEncode="false" DataFormatString="{0:N2}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Status" HeaderText="Status" SortExpression="Status">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Process">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ibtnProcess" runat="server" CommandName="ProcessFile" CommandArgument='<%#Eval("FileID") %>' CausesValidation="false"
                                                ImageUrl="~/images/Play-Pressed-icon(1).png" ToolTip="Process File" OnClientClick="javascript:disableButton(this.id);"></asp:ImageButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Download">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlDownload" runat="server" ImageUrl="~/images/file.gif" ToolTip="Download File"></asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                     <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Label ID="Delete" runat="server" Text="Delete"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("FileID") %>'
                                                CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                        <%--<telerik:RadGrid ID="gvClientFiles" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <ClientSettings>
                            
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <MasterTableView TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="FileID" HeaderText="File ID" SortExpression="FileID">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FileBatchNo" HeaderText="Batch No" SortExpression="FileBatchNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CompanyName" HeaderText="Company" SortExpression="CompanyName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PaymentNatureName" HeaderText="Payment Nature"
                                        SortExpression="PaymentNatureName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="OriginalFileName" HeaderText="File Name" SortExpression="OriginalFileName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CreatedDate" HeaderText="Upload Date" SortExpression="CreatedDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="UserName" HeaderText="Uploaded By" SortExpression="UserName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CreatedBy" HeaderText="Process Date" SortExpression="CreatedBy"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProcessBy" HeaderText="Process By" SortExpression="ProcessBy">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TransactionCount" HeaderText="Count" SortExpression="TransactionCount">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TotalAmount" HeaderText="Amount" SortExpression="TotalAmount"
                                        HtmlEncode="false" DataFormatString="{0:N2}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Status" HeaderText="Status" SortExpression="Status">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Process">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ibtnProcess" runat="server" CommandName="ProcessFile" CommandArgument='<%#Eval("FileID") %>' CausesValidation="false"
                                                ImageUrl="~/images/Play-Pressed-icon(1).png" ToolTip="Process File" OnClientClick="javascript:disableButton(this.id);"></asp:ImageButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Download">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlDownload" runat="server" ImageUrl="~/images/file.gif" ToolTip="Download File"></asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                     <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Label ID="Delete" runat="server" Text="Delete"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("FileID") %>'
                                                CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>--%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <telerik:RadInputManager ID="rimProcessFile" runat="server" Enabled="false">
                
                <telerik:TextBoxSetting BehaviorID="reFileID" Validation-IsRequired="false" EmptyMessage="File ID"
                    ErrorMessage="File ID" Validation-ValidationGroup="Proceed">
                    <TargetControls>
                        <telerik:TargetInput ControlID="txtFileID" />
                    </TargetControls>
                </telerik:TextBoxSetting>
                <telerik:TextBoxSetting BehaviorID="reFileName" Validation-IsRequired="false" EmptyMessage="File Name"
                    ErrorMessage="File Name" Validation-ValidationGroup="Proceed">
                    <TargetControls>
                        <telerik:TargetInput ControlID="txtFileName" />
                    </TargetControls>
                </telerik:TextBoxSetting>
                <%--<telerik:TextBoxSetting BehaviorID="reBatchName" Validation-IsRequired="true" EmptyMessage="Batch Name"
                    ErrorMessage="Batch Name">
                    <TargetControls>
                        <telerik:TargetInput ControlID="txtBatchName" />
                    </TargetControls>
                </telerik:TextBoxSetting>--%>
                <telerik:TextBoxSetting BehaviorID="reTnxCount" Validation-IsRequired="false" EmptyMessage="Transaction Count"
                    ErrorMessage="Transaction Count" Validation-ValidationGroup="Proceed">
                    <TargetControls>
                        <telerik:TargetInput ControlID="txtTnxCount" />
                    </TargetControls>
                </telerik:TextBoxSetting>
                <telerik:TextBoxSetting BehaviorID="reTotalAmount" Validation-IsRequired="false"
                    EmptyMessage="TotalAmount" ErrorMessage="TotalAmount" Validation-ValidationGroup="Proceed">
                    <TargetControls>
                        <telerik:TargetInput ControlID="txtTotalAmount" />
                    </TargetControls>
                </telerik:TextBoxSetting>
            </telerik:RadInputManager>
            <table align="left" cellpadding="0" cellspacing="0" style="display: none; width: 100%" 
                id="tblProcess" runat="server">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" colspan="2">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Process File" CssClass="headingblue"></asp:Label>
                        &nbsp;
                    </td>
                    <td align="left" valign="top" colspan="2">
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" colspan="2">
                        Note:&nbsp; Fields marked as
                        <asp:Label ID="Label6" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        &nbsp;are required.
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblFileID" runat="server" Text="File ID" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="blFileName" runat="server" Text="File Name" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtFileID" runat="server" CssClass="txtbox" ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtFileName" runat="server" CssClass="txtbox" ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblTnxCount" runat="server" Text="Transaction Count" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblTotalAmount" runat="server" Text="Total Amount" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtTnxCount" runat="server" CssClass="txtbox" ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtTotalAmount" runat="server" CssClass="txtbox" ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" colspan="4">
                        <asp:Button ID="btnProceed" runat="server" Text="Proceed" CssClass="btn" Width="76px"
                            Visible="False" ValidationGroup="Proceed"/>
                        &nbsp;<asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btn" Width="76px"
                            Visible="False" CausesValidation="False" />
                        &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="76px"
                            Visible="False" CausesValidation="False" />

                           
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" colspan="4">
                        <asp:Label ID="lblVerified" runat="server" Text="Verified Instruction List" CssClass="headingblue"
                            Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" colspan="4">
                        <asp:Panel ID="pnlVerified" runat="server" BorderWidth="1px" Height="200px" ScrollBars="Vertical"
                            Visible="false" Width="1179px">
                            <telerik:RadGrid ID="gvVerified" runat="server" AllowPaging="false" AllowSorting="false"
                                AutoGenerateColumns="true" CellSpacing="0" PageSize="1000"  ShowFooter="True" Visible="true" >
                                <AlternatingItemStyle CssClass="rgAltRow" HorizontalAlign="Left" VerticalAlign="Top" />
                                <MasterTableView>
                                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                    </ExpandCollapseColumn>
                                </MasterTableView>
                                <ItemStyle CssClass="rgRow" HorizontalAlign="Left" VerticalAlign="Top" />
                            </telerik:RadGrid>
                        </asp:Panel>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" colspan="4">
                        <asp:Label ID="lblVerifiedDetail" runat="server" Text="Verified Instruction Detail List"
                            CssClass="headingblue" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" colspan="4">
                        <asp:Panel ID="pnlVerifiedDetail" runat="server" BorderWidth="1px" Height="200px"
                            ScrollBars="Vertical" Visible="false" Width="1179px">
                            <telerik:RadGrid ID="gvVerifiedDetail" runat="server" AllowPaging="false" AllowSorting="false"
                                AutoGenerateColumns="true" CellSpacing="0" PageSize="1000" ShowFooter="True" Visible="true">
                                <AlternatingItemStyle CssClass="rgAltRow" HorizontalAlign="Left" VerticalAlign="Top" />
                                <MasterTableView>
                                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                    </ExpandCollapseColumn>
                                </MasterTableView>
                                <ItemStyle CssClass="rgRow" HorizontalAlign="Left" VerticalAlign="Top" />
                            </telerik:RadGrid>
                        </asp:Panel>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" colspan="4">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" colspan="4">
                        <asp:Label ID="lblUnverified" runat="server" Text="Un-Verified Instruction List"
                            CssClass="headingblue" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" colspan="4">
                        <asp:Panel ID="pnlUnVerified" runat="server" BorderWidth="1px" Height="200px" ScrollBars="Both"
                            Visible="false" Width="1179px">
                            <telerik:RadGrid ID="gvUnverified" runat="server" AllowPaging="false" AllowSorting="false"
                                AutoGenerateColumns="true" CellSpacing="0" PageSize="1000" ShowFooter="True" HeaderStyle-HorizontalAlign="left"
                                Visible="true">
                                <AlternatingItemStyle CssClass="rgAltRow" HorizontalAlign="Left" VerticalAlign="Top" />
                                <MasterTableView>
                                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                    </ExpandCollapseColumn>
                                </MasterTableView>
                                <ItemStyle CssClass="rgRow" HorizontalAlign="Left" VerticalAlign="Top" />
                            </telerik:RadGrid>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
