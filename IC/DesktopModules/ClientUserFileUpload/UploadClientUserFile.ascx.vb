﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.Net.Mime.ContentType
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Partial Class DesktopModules_AgentFile_SaveAgentFile
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrICAssignUserRolsID As New ArrayList
    Private htRights As Hashtable
    Protected Sub DesktopModules_OFACList_SaveOFACList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then

                ViewState("htRights") = Nothing
                GetPageAccessRights()
                Clear()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "File Upload")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                ArrICAssignUserRolsID.Add(RoleID.ToString)
            Next
        End If
    End Sub
    Private Sub Clear()
        Try
            Dim objICUser As New ICUser
            Dim objICGroup As New ICGroup

            objICUser.es.Connection.CommandTimeout = 3600
            objICGroup.es.Connection.CommandTimeout = 3600
            If Me.UserInfo.IsSuperUser = False Then
                If objICUser.LoadByPrimaryKey(Me.UserId) Then
                    If objICUser.UserType = "Client User" Then
                        objICGroup = objICUser.UpToICOfficeByOfficeCode.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode
                        LoadddlGroup()
                        ddlGroup.SelectedValue = objICGroup.GroupCode.ToString
                        ddlGroup.Enabled = False
                        SetArraListRoleID()
                        LoadddlCompany()
                        LoadddlAccountPaymentNatureByUserID()
                        LoadddlFileUploadTemplate()
                    ElseIf objICUser.UserType.ToString = "Bank User" Then
                        SetArraListRoleID()
                        LoadddlGroup()
                        LoadddlCompany()
                        LoadddlAccountPaymentNatureByUserID()
                        LoadddlFileUploadTemplate()
                    End If
                End If
            Else
                UIUtilities.ShowDialog(Me, "Error", "Sorry! You are not authorized to login.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            objICUser.es.Connection.CommandTimeout = 3600
            ddlGroup.Enabled = True
            Dim lit As New ListItem
            lit.Text = Nothing
            lit.Value = Nothing
            If objICUser.LoadByPrimaryKey(Me.UserId) Then
                If objICUser.UserType = "Client User" Then
                    lit.Value = "0"
                    lit.Text = "-- Select --"
                    ddlGroup.Items.Clear()
                    ddlGroup.Items.Add(lit)
                    ddlGroup.AppendDataBoundItems = True
                    ddlGroup.DataSource = ICGroupController.GetAllActiveAndApproveGroups
                    ddlGroup.DataTextField = "GroupName"
                    ddlGroup.DataValueField = "GroupCode"
                    ddlGroup.DataBind()
                ElseIf objICUser.UserType = "Bank User" Then

                    ddlGroup.Items.Clear()
                    dt = ICUserRolesPaymentNatureController.GetAllTaggedGroupsByUserID(objICUser.UserID.ToString, ArrICAssignUserRolsID)
                    If dt.Rows.Count = 1 Then
                        For Each dr As DataRow In dt.Rows
                            lit.Value = dr("GroupCode")
                            lit.Text = dr("GroupName")
                            ddlGroup.Items.Add(lit)
                            lit.Selected = True
                            ddlGroup.Enabled = False
                        Next
                    ElseIf dt.Rows.Count > 1 Then
                        lit.Value = "0"
                        lit.Text = "-- Select --"
                        ddlGroup.Items.Clear()
                        ddlGroup.Items.Add(lit)
                        ddlGroup.AppendDataBoundItems = True
                        ddlGroup.DataSource = dt
                        ddlGroup.DataTextField = "GroupName"
                        ddlGroup.DataValueField = "GroupCode"
                        ddlGroup.DataBind()
                    Else

                        lit.Value = "0"
                        lit.Text = "-- Select --"
                        ddlGroup.Items.Clear()
                        ddlGroup.Items.Add(lit)
                        ddlGroup.AppendDataBoundItems = True

                    End If
                Else

                    lit.Value = "0"
                    lit.Text = "-- Select --"
                    ddlGroup.Items.Clear()
                    ddlGroup.Items.Add(lit)
                    ddlGroup.AppendDataBoundItems = True
                End If
            End If




        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, ddlGroup.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlCompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlCompany.Items.Clear()
                    ddlCompany.Items.Add(lit)
                    lit.Selected = True
                    ddlCompany.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- Select --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
                ddlCompany.DataSource = dt
                ddlCompany.DataTextField = "CompanyName"
                ddlCompany.DataValueField = "CompanyCode"
                ddlCompany.DataBind()
                ddlCompany.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- Select --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccountPaymentNatureByUserID()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Select --"
            ddlAcccountPaymentNature.Items.Clear()
            ddlAcccountPaymentNature.Items.Add(lit)
            lit.Selected = True
            ddlAcccountPaymentNature.AppendDataBoundItems = True
            ddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUser(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
            ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
            ddlAcccountPaymentNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlFileUploadTemplate()
        Try
            Dim objICAPNature As New ICAccountsPaymentNature
            Dim StrArray As String() = Nothing
            objICAPNature.es.Connection.CommandTimeout = 3600
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Select --"
            ddlFileUploadTemplate.Items.Clear()
            ddlFileUploadTemplate.Items.Add(lit)
            ddlFileUploadTemplate.AppendDataBoundItems = True
            If ddlAcccountPaymentNature.SelectedValue.ToString <> "0" Then
                StrArray = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")
                objICAPNature.LoadByPrimaryKey(StrArray(0).ToString, StrArray(1).ToString, StrArray(2), StrArray(3).ToString)
                ddlFileUploadTemplate.AppendDataBoundItems = True
                ddlFileUploadTemplate.DataSource = ICAccountPaymentNatureFileUploadTemplateController.GetTemplatesByAccountPaymentNature(objICAPNature)
                ddlFileUploadTemplate.DataTextField = "TemplateName"
                ddlFileUploadTemplate.DataValueField = "TemplateID"
                ddlFileUploadTemplate.DataBind()
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub



    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub
    Private Sub LoadgvUnverified()
        Try
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            SetArraListRoleID()
            LoadddlCompany()
            LoadddlAccountPaymentNatureByUserID()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            SetArraListRoleID()
            LoadddlAccountPaymentNatureByUserID()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlAcccountPaymentNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcccountPaymentNature.SelectedIndexChanged
        Try
            SetArraListRoleID()
            LoadddlFileUploadTemplate()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim MIMEType As String = ""
                Dim FileName As String = ""
                Dim Action, AuditType As String
                Dim chkCorrectFile As Boolean = False
                Dim objICFile As New ICFiles
                Dim objICFileUploadTemplate As New ICTemplate
                Dim CheckResult As String = ""
                Dim FileLocation As String = ""
                FileLocation = Request.PhysicalApplicationPath.ToString() & "UploadedFiles\"
                objICFile.es.Connection.CommandTimeout = 3600
                objICFileUploadTemplate.es.Connection.CommandTimeout = 3600

                MIMEType = fuplClientFile.PostedFile.ContentType
                Action = ""
                AuditType = ""
                'Check .txt
                If MIMEType = "text/plain" Then
                    chkCorrectFile = True
                End If
                'Check .xls
                If MIMEType = "application/vnd.ms-excel" Then
                    chkCorrectFile = True
                End If
                'Check .xlsx
                If MIMEType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" Then
                    chkCorrectFile = True
                End If

                If chkCorrectFile = True Then
                    If objICFileUploadTemplate.LoadByPrimaryKey(ddlFileUploadTemplate.SelectedValue.ToString()) Then
                        If objICFileUploadTemplate.TemplateFormat.ToString().ToLower.ToString = ".txt" Then
                            If fuplClientFile.FileName.ToString().Split(".")(1).ToString().ToLower.Contains("txt") Or fuplClientFile.FileName.ToString().Split(".")(1).ToString().ToLower.Contains("csv") Then
                                FileName = ddlCompany.SelectedValue.ToString() & "" & Date.Now.ToString("ddMMyyyyHHmmss") & fuplClientFile.FileName
                                ' Save File on HDD

                                fuplClientFile.SaveAs(FileLocation & FileName.ToString())
                                CheckResult = ICFilesController.CheckFileBeforeUpload(ddlFileUploadTemplate.SelectedValue.ToString(), ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(0).ToString(), ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(1).ToString(), ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(2).ToString(), ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(3).ToString(), FileLocation & FileName.ToString()).ToString()
                                If CheckResult.ToString() = "OK" Then
                                    Action = "Client File: File [Name: " & fuplClientFile.FileName.ToString() & "] of Group [Code: " & ddlGroup.SelectedValue.ToString() & " ; Name: " & ddlGroup.SelectedItem.Text.ToString() & "], Company [Code: " & ddlCompany.SelectedValue.ToString() & " ; Name: " & ddlCompany.SelectedItem.Text.ToString() & "] and Template [Code: " & objICFileUploadTemplate.FileUploadTemplateCode.ToString() & " ; Name: " & objICFileUploadTemplate.TemplateName.ToString() & " ; Format : " & objICFileUploadTemplate.TemplateFormat.ToString() & "] is uploaded by User [ID: " & Me.UserId.ToString() & " ; Name: " & Me.UserInfo.Username.ToString() & "]."
                                    AuditType = "Client File"
                                    objICFile.FileID = ICUtilities.UploadFiletoDB(fuplClientFile, "Client File", "ICFileID", FileName, Me.UserId, Me.UserInfo.Username, Action.ToString(), AuditType.ToString())
                                    Dim cICFiles As New ICFiles

                                    cICFiles.es.Connection.CommandTimeout = 3600

                                    cICFiles.LoadByPrimaryKey(objICFile.FileID)
                                    cICFiles.Status = "Pending Read"
                                    cICFiles.AcqMode = "File Upload"
                                    cICFiles.FileBatchNo = ICFilesController.GetFileBatchNo()
                                    cICFiles.OriginalFileName = fuplClientFile.FileName
                                    cICFiles.FileLocation = FileLocation & FileName.ToString()
                                    cICFiles.CompanyCode = ddlCompany.SelectedValue.ToString()
                                    cICFiles.AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(0).ToString()
                                    cICFiles.BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(1).ToString()
                                    cICFiles.Currency = ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(2).ToString()
                                    cICFiles.PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(3).ToString()
                                    cICFiles.FileUploadTemplateID = ddlFileUploadTemplate.SelectedValue.ToString()


                                    Action = "Client File: File [Name: " & cICFiles.FileName & " ; ID: " & cICFiles.FileID & "] is updated as [Status: " & cICFiles.Status & " ; AcquisitionMode: " & cICFiles.AcqMode & " ; BatchNo: " & cICFiles.FileBatchNo & " ; OriginalFileName: " & cICFiles.OriginalFileName & " ; FileLocation: " & cICFiles.FileLocation & " ; CompanyCode: " & cICFiles.CompanyCode & " ; AccountNumber: " & cICFiles.AccountNumber & " ; BranchCode: " & cICFiles.BranchCode & " ; Currency: " & cICFiles.Currency & " ; PaymentNatureCode: " & cICFiles.PaymentNatureCode & " ; TemplateID: " & cICFiles.FileUploadTemplateID & "] by User [ID: " & Me.UserId.ToString() & " ; Name: " & Me.UserInfo.Username.ToString() & "]."
                                    AuditType = "Client File"
                                    ICFilesController.UpdateClientFile(cICFiles, Me.UserId, Me.UserInfo.Username, Action.ToString(), AuditType)
                                Else
                                    Dim aFileInfo As IO.FileInfo
                                    aFileInfo = New IO.FileInfo(FileLocation & FileName.ToString())
                                    'Delete file from HDD.
                                    If aFileInfo.Exists Then
                                        aFileInfo.Delete()
                                    End If
                                    UIUtilities.ShowDialog(Me, "Client File Upload", CheckResult.ToString(), ICBO.IC.Dialogmessagetype.Failure)
                                End If
                            Else
                                UIUtilities.ShowDialog(Me, "Client File Upload", "File Must be in the [" & objICFileUploadTemplate.TemplateFormat.ToString() & " format", ICBO.IC.Dialogmessagetype.Failure)
                            End If
                        End If
                        If objICFileUploadTemplate.TemplateFormat.ToString().ToLower.ToString = ".xls" Then
                            If fuplClientFile.FileName.ToString().Split(".")(1).ToString().ToLower = "xls" Then
                                FileName = ddlCompany.SelectedValue.ToString() & "" & Date.Now.ToString("ddMMyyyyHHmmss") & fuplClientFile.FileName
                                ' Save File on HDD
                                fuplClientFile.SaveAs(FileLocation & FileName.ToString())
                                CheckResult = ICFilesController.CheckFileBeforeUpload(ddlFileUploadTemplate.SelectedValue.ToString(), ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(0).ToString(), ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(1).ToString(), ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(2).ToString(), ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(3).ToString(), FileLocation & FileName.ToString()).ToString()
                                If CheckResult.ToString() = "OK" Then
                                    Action = "Client File: File [Name: " & fuplClientFile.FileName.ToString() & "] of Group [Code: " & ddlGroup.SelectedValue.ToString() & " ; Name: " & ddlGroup.SelectedItem.Text.ToString() & "], Company [Code: " & ddlCompany.SelectedValue.ToString() & " ; Name: " & ddlCompany.SelectedItem.Text.ToString() & "] and Template [Code: " & objICFileUploadTemplate.FileUploadTemplateCode.ToString() & " ; Name: " & objICFileUploadTemplate.TemplateName.ToString() & " ; Format : " & objICFileUploadTemplate.TemplateFormat.ToString() & "] is uploaded by User [ID: " & Me.UserId.ToString() & " ; Name: " & Me.UserInfo.Username.ToString() & "]."
                                    AuditType = "Client File"
                                    objICFile.FileID = ICUtilities.UploadFiletoDB(fuplClientFile, "Client File", "ICFileID", FileName, Me.UserId, Me.UserInfo.Username, Action.ToString(), AuditType.ToString())
                                    Dim cICFiles As New ICFiles

                                    cICFiles.es.Connection.CommandTimeout = 3600

                                    cICFiles.LoadByPrimaryKey(objICFile.FileID)
                                    cICFiles.Status = "Pending Read"
                                    cICFiles.AcqMode = "File Upload"
                                    cICFiles.FileBatchNo = ICFilesController.GetFileBatchNo()
                                    cICFiles.OriginalFileName = fuplClientFile.FileName
                                    cICFiles.FileLocation = FileLocation & FileName.ToString()
                                    cICFiles.CompanyCode = ddlCompany.SelectedValue.ToString()
                                    cICFiles.AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(0).ToString()
                                    cICFiles.BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(1).ToString()
                                    cICFiles.Currency = ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(2).ToString()
                                    cICFiles.PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(3).ToString()
                                    cICFiles.FileUploadTemplateID = ddlFileUploadTemplate.SelectedValue.ToString()

                                    Action = "Client File: File [Name: " & cICFiles.FileName & " ; ID: " & cICFiles.FileID & "] is updated as [Status: " & cICFiles.Status & " ; AcquisitionMode: " & cICFiles.AcqMode & " ; BatchNo: " & cICFiles.FileBatchNo & " ; OriginalFileName: " & cICFiles.OriginalFileName & " ; FileLocation: " & cICFiles.FileLocation & " ; CompanyCode: " & cICFiles.CompanyCode & " ; AccountNumber: " & cICFiles.AccountNumber & " ; BranchCode: " & cICFiles.BranchCode & " ; Currency: " & cICFiles.Currency & " ; PaymentNatureCode: " & cICFiles.PaymentNatureCode & " ; TemplateID: " & cICFiles.FileUploadTemplateID & "] by User [ID: " & Me.UserId.ToString() & " ; Name: " & Me.UserInfo.Username.ToString() & "]."
                                    AuditType = "Client File"
                                    ICFilesController.UpdateClientFile(cICFiles, Me.UserId, Me.UserInfo.Username, Action.ToString(), AuditType)
                                Else
                                    Dim aFileInfo As IO.FileInfo
                                    aFileInfo = New IO.FileInfo(FileLocation & FileName.ToString())
                                    'Delete file from HDD.
                                    If aFileInfo.Exists Then
                                        aFileInfo.Delete()
                                    End If
                                    UIUtilities.ShowDialog(Me, "Client File Upload", CheckResult.ToString(), ICBO.IC.Dialogmessagetype.Failure)
                                End If
                            Else
                                UIUtilities.ShowDialog(Me, "Client File Upload", "File Must be in the [" & objICFileUploadTemplate.TemplateFormat.ToString() & "] format", ICBO.IC.Dialogmessagetype.Failure)
                            End If
                        End If
                        If objICFileUploadTemplate.TemplateFormat.ToString().ToLower.ToString = ".csv" Then
                            If fuplClientFile.FileName.ToString().Split(".")(1).ToString().ToLower = "csv" Then
                                FileName = ddlCompany.SelectedValue.ToString() & "" & Date.Now.ToString("ddMMyyyyHHmmss") & fuplClientFile.FileName
                                ' Save File on HDD
                                fuplClientFile.SaveAs(FileLocation & FileName.ToString())
                                CheckResult = ICFilesController.CheckFileBeforeUpload(ddlFileUploadTemplate.SelectedValue.ToString(), ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(0).ToString(), ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(1).ToString(), ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(2).ToString(), ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(3).ToString(), FileLocation & FileName.ToString()).ToString()
                                If CheckResult.ToString() = "OK" Then
                                    Action = "Client File: File [Name: " & fuplClientFile.FileName.ToString() & "] of Group [Code: " & ddlGroup.SelectedValue.ToString() & " ; Name: " & ddlGroup.SelectedItem.Text.ToString() & "], Company [Code: " & ddlCompany.SelectedValue.ToString() & " ; Name: " & ddlCompany.SelectedItem.Text.ToString() & "] and Template [Code: " & objICFileUploadTemplate.FileUploadTemplateCode.ToString() & " ; Name: " & objICFileUploadTemplate.TemplateName.ToString() & " ; Format : " & objICFileUploadTemplate.TemplateFormat.ToString() & "] is uploaded by User [ID: " & Me.UserId.ToString() & " ; Name: " & Me.UserInfo.Username.ToString() & "]."
                                    AuditType = "Client File"
                                    objICFile.FileID = ICUtilities.UploadFiletoDB(fuplClientFile, "Client File", "ICFileID", FileName, Me.UserId, Me.UserInfo.Username, Action.ToString(), AuditType.ToString())
                                    Dim cICFiles As New ICFiles

                                    cICFiles.es.Connection.CommandTimeout = 3600

                                    cICFiles.LoadByPrimaryKey(objICFile.FileID)
                                    cICFiles.Status = "Pending Read"
                                    cICFiles.AcqMode = "File Upload"
                                    cICFiles.FileBatchNo = ICFilesController.GetFileBatchNo()
                                    cICFiles.OriginalFileName = fuplClientFile.FileName
                                    cICFiles.FileLocation = FileLocation & FileName.ToString()
                                    cICFiles.CompanyCode = ddlCompany.SelectedValue.ToString()
                                    cICFiles.AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(0).ToString()
                                    cICFiles.BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(1).ToString()
                                    cICFiles.Currency = ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(2).ToString()
                                    cICFiles.PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(3).ToString()
                                    cICFiles.FileUploadTemplateID = ddlFileUploadTemplate.SelectedValue.ToString()

                                    Action = "Client File: File [Name: " & cICFiles.FileName & " ; ID: " & cICFiles.FileID & "] is updated as [Status: " & cICFiles.Status & " ; AcquisitionMode: " & cICFiles.AcqMode & " ; BatchNo: " & cICFiles.FileBatchNo & " ; OriginalFileName: " & cICFiles.OriginalFileName & " ; FileLocation: " & cICFiles.FileLocation & " ; CompanyCode: " & cICFiles.CompanyCode & " ; AccountNumber: " & cICFiles.AccountNumber & " ; BranchCode: " & cICFiles.BranchCode & " ; Currency: " & cICFiles.Currency & " ; PaymentNatureCode: " & cICFiles.PaymentNatureCode & " ; TemplateID: " & cICFiles.FileUploadTemplateID & "] by User [ID: " & Me.UserId.ToString() & " ; Name: " & Me.UserInfo.Username.ToString() & "]."
                                    AuditType = "Client File"
                                    ICFilesController.UpdateClientFile(cICFiles, Me.UserId, Me.UserInfo.Username, Action.ToString(), AuditType)
                                Else
                                    Dim aFileInfo As IO.FileInfo
                                    aFileInfo = New IO.FileInfo(FileLocation & FileName.ToString())
                                    'Delete file from HDD.
                                    If aFileInfo.Exists Then
                                        aFileInfo.Delete()
                                    End If
                                    UIUtilities.ShowDialog(Me, "Client File Upload", CheckResult.ToString(), ICBO.IC.Dialogmessagetype.Failure)
                                End If
                            Else
                                UIUtilities.ShowDialog(Me, "Client File Upload", "File Must be in the [" & objICFileUploadTemplate.TemplateFormat.ToString() & "] format", ICBO.IC.Dialogmessagetype.Failure)
                            End If
                        End If
                        If objICFileUploadTemplate.TemplateFormat.ToString().ToLower.ToString = ".xlsx" Then
                            If fuplClientFile.FileName.ToString().Split(".")(1).ToString().ToLower = "xlsx" Then
                                FileName = ddlCompany.SelectedValue.ToString() & "" & Date.Now.ToString("ddMMyyyyHHmmss") & fuplClientFile.FileName
                                ' Save File on HDD
                                fuplClientFile.SaveAs(FileLocation & FileName.ToString())
                                CheckResult = ICFilesController.CheckFileBeforeUpload(ddlFileUploadTemplate.SelectedValue.ToString(), ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(0).ToString(), ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(1).ToString(), ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(2).ToString(), ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(3).ToString(), FileLocation & FileName.ToString()).ToString()
                                If CheckResult.ToString() = "OK" Then
                                    Action = "Client File: File [Name: " & fuplClientFile.FileName.ToString() & "] of Group [Code: " & ddlGroup.SelectedValue.ToString() & " ; Name: " & ddlGroup.SelectedItem.Text.ToString() & "], Company [Code: " & ddlCompany.SelectedValue.ToString() & " ; Name: " & ddlCompany.SelectedItem.Text.ToString() & "] and Template [Code: " & objICFileUploadTemplate.FileUploadTemplateCode.ToString() & " ; Name: " & objICFileUploadTemplate.TemplateName.ToString() & " ; Format : " & objICFileUploadTemplate.TemplateFormat.ToString() & "] is uploaded by User [ID: " & Me.UserId.ToString() & " ; Name: " & Me.UserInfo.Username.ToString() & "]."
                                    AuditType = "Client File"
                                    objICFile.FileID = ICUtilities.UploadFiletoDB(fuplClientFile, "Client File", "ICFileID", FileName, Me.UserId, Me.UserInfo.Username, Action.ToString(), AuditType.ToString())
                                    Dim cICFiles As New ICFiles

                                    cICFiles.es.Connection.CommandTimeout = 3600

                                    cICFiles.LoadByPrimaryKey(objICFile.FileID)
                                    cICFiles.Status = "Pending Read"
                                    cICFiles.AcqMode = "File Upload"
                                    cICFiles.FileBatchNo = ICFilesController.GetFileBatchNo()
                                    cICFiles.OriginalFileName = fuplClientFile.FileName
                                    cICFiles.FileLocation = FileLocation & FileName.ToString()
                                    cICFiles.CompanyCode = ddlCompany.SelectedValue.ToString()
                                    cICFiles.AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(0).ToString()
                                    cICFiles.BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(1).ToString()
                                    cICFiles.Currency = ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(2).ToString()
                                    cICFiles.PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString().Split("-")(3).ToString()
                                    cICFiles.FileUploadTemplateID = ddlFileUploadTemplate.SelectedValue.ToString()

                                    Action = "Client File: File [Name: " & cICFiles.FileName & " ; ID: " & cICFiles.FileID & "] is updated as [Status: " & cICFiles.Status & " ; AcquisitionMode: " & cICFiles.AcqMode & " ; BatchNo: " & cICFiles.FileBatchNo & " ; OriginalFileName: " & cICFiles.OriginalFileName & " ; FileLocation: " & cICFiles.FileLocation & " ; CompanyCode: " & cICFiles.CompanyCode & " ; AccountNumber: " & cICFiles.AccountNumber & " ; BranchCode: " & cICFiles.BranchCode & " ; Currency: " & cICFiles.Currency & " ; PaymentNatureCode: " & cICFiles.PaymentNatureCode & " ; TemplateID: " & cICFiles.FileUploadTemplateID & "] by User [ID: " & Me.UserId.ToString() & " ; Name: " & Me.UserInfo.Username.ToString() & "]."
                                    AuditType = "Client File"
                                    ICFilesController.UpdateClientFile(cICFiles, Me.UserId, Me.UserInfo.Username, Action.ToString(), AuditType)
                                Else
                                    Dim aFileInfo As IO.FileInfo
                                    aFileInfo = New IO.FileInfo(FileLocation & FileName.ToString())
                                    'Delete file from HDD.
                                    If aFileInfo.Exists Then
                                        aFileInfo.Delete()
                                    End If
                                    UIUtilities.ShowDialog(Me, "Client File Upload", CheckResult.ToString(), ICBO.IC.Dialogmessagetype.Failure)
                                End If
                            Else
                                UIUtilities.ShowDialog(Me, "Client File Upload", "File Must be in the [" & objICFileUploadTemplate.TemplateFormat.ToString() & "] format", ICBO.IC.Dialogmessagetype.Failure)
                            End If
                        End If
                    End If
                Else
                    UIUtilities.ShowDialog(Me, "Client File Upload", "Invalid file format.", ICBO.IC.Dialogmessagetype.Failure)
                End If
                If objICFile.FileID > 0 Then
                    UIUtilities.ShowDialog(Me, "Client File Upload", fuplClientFile.FileName.ToString() & " Added Successfully.", ICBO.IC.Dialogmessagetype.Success)

                    Clear()
                    EmailUtilities.Fileuploadedbyuser(fuplClientFile.FileName.ToString(), Me.UserInfo.Username.ToString, "")
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
End Class
