﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewUBPSCompany.ascx.vb"
    Inherits="DesktopModules_UBPSCompanyManagement_ViewUBPSCompany" %>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to remove Record(s)?") == true) {
            return true;
        }
        else {
            return false;
        }
    }  
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        //      alert(grid)
        //        if ((grid.rows.length -1) != 10 ) {
        //        if ((grid.rows.length) >= 12) {
        //            //            alert('More than 10 rows')
        //            for (i = 0; i < grid.rows.length - 1; i++) {
        //                cell = grid.rows[i].cells[CellNo];
        //                for (j = 0; j < cell.childNodes.length - 1; j++) {
        //                    if (cell.childNodes[j].type == "checkbox") {
        //                        cell.childNodes[j].checked = document.getElementById(idOfControllingCheckbox).checked;
        //                    }
        //                }
        //            }
        //        }
        //        else {
        //                     alert(grid.rows.length)
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    //    }
</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="center" valign="top" colspan="4">
                        <asp:Button ID="btnAddCompany" runat="server" Text="Add Company" 
                            CssClass="btn" />
                        <asp:HiddenField ID="hfBankCode" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <asp:Label ID="lblCompanyListHeader" runat="server" Text="UBP Company List" CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
                        </td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
            <asp:Label ID="lblCompanyCode" runat="server" Text="Company Code" 
                CssClass="lbl"></asp:Label>
                        </td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
            <asp:Label ID="lblCompanyName" runat="server" Text="Company Name" 
                CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
            <asp:DropDownList ID="ddlCompanyCode" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
                        </td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
            <asp:DropDownList ID="ddlCompanyName" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
            <asp:Label ID="lblCompanyType" runat="server" Text="Company Type" 
                CssClass="lbl"></asp:Label>
                        </td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
            <asp:DropDownList ID="ddlCompanyType" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
                        </td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <asp:Label ID="lblRNF" runat="server" Font-Bold="False" 
                            Text="No Record Found" Visible="False" CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <telerik:RadGrid ID="gvCompany" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                            <ClientSettings>
                            
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                           <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView NoMasterRecordsText="" DataKeyNames="CompanyCode" TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                  <telerik:GridBoundColumn DataField="UBPSCompanyID" HeaderText="Company ID" SortExpression="UBPSCompanyID">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CompanyCode" HeaderText="Company Code" SortExpression="CompanyCode">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName">
                                    </telerik:GridBoundColumn>
                                   <telerik:GridBoundColumn DataField="CompanyType" HeaderText="Company Type" SortExpression="CompanyType">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="MultiplesOfAmount" HeaderText="Amount Acceptance Criteria" SortExpression="MultiplesOfAmount">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Billing Inquiry" DataField="IsBillingInquiry">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Active" DataField="IsActive">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Approve" DataField="IsApproved">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridTemplateColumn HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveUBPSCompany", "&mid=" & Me.ModuleId & "&id=" & Eval("UBPSCompanyID"))%>'
                                                ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("UBPSCompanyID") %>'
                                                CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid></td>
                </tr>
                <tr>
                    <td align="center" valign="top" colspan="4">
                        
                        <%--<telerik:GridTemplateColumn HeaderText="Add Email Details">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlOnlineFormSetting" runat="server" ImageUrl="~/images/plainbutton.gif"
                                                NavigateUrl='<%# NavigateURL("saveEmailDetails", "&mid=" & Me.ModuleId & "&id="& Eval("CompanyCode")) %>'
                                                ToolTip="Add Email Details"></asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
