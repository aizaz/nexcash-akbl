﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports FtpLib
Imports System.Net



Partial Class DesktopModules_UBPSCompanyManagement_SaveUBPSCompany
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private UBPSCpmanyID As String
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_UBPSCompanyManagement_SaveUBPSCompany_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            UBPSCpmanyID = Request.QueryString("id").ToString()

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()

                If UBPSCpmanyID.ToString() = "0" Then
                    Clear()
                    lblIsApproved.Visible = False
                    chkApproved.Visible = False
                    btnApproved.Visible = False
                    lblPageHeader.Text = "Add UBP Company"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    Clear()
                    Dim chkProcessAllPrimary As New CheckBox
                    chkProcessAllPrimary = DirectCast(chkPerformBillInquiry, CheckBox)

                    chkProcessAllPrimary.Attributes("onclick") = "PerformBillInquiry('" & UBPSCpmanyID & "');"
                    lblIsApproved.Visible = CBool(htRights("Approve"))
                    chkApproved.Visible = CBool(htRights("Approve"))
                    btnApproved.Visible = CBool(htRights("Approve"))
                    lblPageHeader.Text = "Edit UBP Company"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    btnApproved.Visible = CBool(htRights("Approve"))
                    txtUBPSCompanyCode.ReadOnly = True
                    txtCompanyName.ReadOnly = True
                    Dim objICCompany As New ICUBPSCompany

                    If objICCompany.LoadByPrimaryKey(UBPSCpmanyID) Then
                        txtUBPSCompanyCode.Text = objICCompany.CompanyCode
                        txtCompanyName.Text = objICCompany.CompanyName
                        txtRefMinLength.Text = objICCompany.ReferenceFieldMinLength
                        txtRefMaxLength.Text = objICCompany.ReferenceFieldMaxLength
                        txtRefFieldCaption.Text = objICCompany.ReferenceFieldCaption
                        txtReferenceFieldFormat.Text = objICCompany.ReferenceFieldFormat
                        chkActive.Checked = objICCompany.IsActive
                        chkApproved.Checked = objICCompany.IsApproved
                        chkPerformBillInquiry.Checked = objICCompany.IsBillingInquiry
                        rbListRefFieldRegExp.SelectedValue = objICCompany.RegularExpressionForReferenceField
                        ddlCompanyType.SelectedValue = objICCompany.CompanyType
                        ddlAmountAcceptanceCriteria.SelectedValue = objICCompany.MultiplesOfAmount
                        ManageAmountMinAndMax(ddlAmountAcceptanceCriteria.SelectedValue.ToString)
                        chkPerformBillInquiry.Checked = objICCompany.IsBillingInquiry
                        If objICCompany.MaxValue.HasValue Then
                            txtAmountMaxValue.Text = objICCompany.MaxValue
                        End If
                        If objICCompany.MinValue.HasValue Then
                            txtAmountMinValue.Text = objICCompany.MinValue
                        End If
                        If objICCompany.XAmountValue.HasValue Then
                            txtAmountXValue.Text = objICCompany.XAmountValue
                        End If

                        ddlCompanyType.Enabled = True
                        If objICCompany.IsApproved = True Then
                            lblIsApproved.Visible = True
                            chkApproved.Visible = True
                            chkApproved.Enabled = False
                            btnApproved.Visible = False
                        End If



                        ''MT 940 Work







                    End If
                End If
                'btnSave.Visible = ArrRights(1)
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "UBPS Company Management")
            ViewState("htRights") = htRights

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
#Region "Button Events"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        'For Each validator As BaseValidator In Page.Validators
        '    If validator.Enabled = True And validator.IsValid = False Then
        '        Dim clientID As String
        '        clientID = validator.ClientID
        '    End If
        'Next

        If Page.IsValid Then
            Try


                Dim objICUBPSCompany As New ICUBPSCompany
                Dim objICUBPSCompanyFrom As New ICUBPSCompany
                Dim Action As String = ""


                If UBPSCpmanyID <> "0" Then
                    objICUBPSCompanyFrom.LoadByPrimaryKey(UBPSCpmanyID.ToString)
                    objICUBPSCompany.UBPSCompanyID = UBPSCpmanyID
                End If
                objICUBPSCompany.CompanyType = ddlCompanyType.SelectedValue.ToString
                objICUBPSCompany.CompanyCode = txtUBPSCompanyCode.Text
                objICUBPSCompany.CompanyName = txtCompanyName.Text
                objICUBPSCompany.ReferenceFieldMinLength = txtRefMinLength.Text
                objICUBPSCompany.ReferenceFieldMaxLength = txtRefMaxLength.Text
                objICUBPSCompany.ReferenceFieldCaption = txtRefFieldCaption.Text
                objICUBPSCompany.ReferenceFieldFormat = txtReferenceFieldFormat.Text
                objICUBPSCompany.IsActive = chkActive.Checked
                objICUBPSCompany.RegularExpressionForReferenceField = rbListRefFieldRegExp.SelectedValue.ToString
                objICUBPSCompany.IsBillingInquiry = chkPerformBillInquiry.Checked
                If ddlAmountAcceptanceCriteria.SelectedValue.ToString = "Only Billed Amount" Or ddlAmountAcceptanceCriteria.SelectedValue.ToString = "Any Amount" Then
                    objICUBPSCompany.MaxValue = Nothing
                    objICUBPSCompany.MinValue = Nothing
                    objICUBPSCompany.XAmountValue = Nothing
                    objICUBPSCompany.MultiplesOfAmount = ddlAmountAcceptanceCriteria.SelectedValue.ToString
                ElseIf ddlAmountAcceptanceCriteria.SelectedValue.ToString = "Any amount within Min & Max" Then

                    If validateAmount("Any amount within Min & Max") = False Then
                        UIUtilities.ShowDialog(Me, "Save UBP Company", "Invalid Amount", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If

                    objICUBPSCompany.MaxValue = txtAmountMaxValue.Text
                    objICUBPSCompany.MinValue = txtAmountMinValue.Text
                    objICUBPSCompany.XAmountValue = Nothing
                    objICUBPSCompany.MultiplesOfAmount = ddlAmountAcceptanceCriteria.SelectedValue.ToString
                ElseIf ddlAmountAcceptanceCriteria.SelectedValue.ToString = "Multiple of X within Min & Max" Then
                    If validateAmount("Multiple of X within Min & Max") = False Then
                        UIUtilities.ShowDialog(Me, "Save UBP Company", "Invalid Amount", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    objICUBPSCompany.MaxValue = txtAmountMaxValue.Text
                    objICUBPSCompany.MinValue = txtAmountMinValue.Text
                    objICUBPSCompany.XAmountValue = txtAmountXValue.Text
                    objICUBPSCompany.MultiplesOfAmount = ddlAmountAcceptanceCriteria.SelectedValue.ToString
                End If
                If UBPSCpmanyID = "0" Then
                    objICUBPSCompany.CreatedBy = Me.UserId
                    objICUBPSCompany.CreatedDate = Date.Now
                    objICUBPSCompany.Creator = Me.UserId
                    objICUBPSCompany.CreationDate = Date.Now
                    If CheckDuplicate(objICUBPSCompany) = False Then
                        If CheckDuplicateCompanyCode(objICUBPSCompany) = False Then
                            Action = Nothing
                            Action = "UBP Company Name [ " & objICUBPSCompany.CompanyName & " ]; "
                            Action += "Code [ " & objICUBPSCompany.CompanyCode & " ] ; "
                            Action += "Type[ " & ddlCompanyType.SelectedValue.ToString & " ] ; "
                            Action += "Reference Field Minimum Length [ " & objICUBPSCompany.ReferenceFieldMinLength & " ] ; "
                            Action += "Reference Field Maximum Length [ " & objICUBPSCompany.ReferenceFieldMaxLength & " ] ; "
                            Action += "Reference Field Caption [ " & objICUBPSCompany.ReferenceFieldCaption & " ] ; "
                            Action += "Reference Field Format [ " & objICUBPSCompany.ReferenceFieldFormat & " ] ; "
                            Action += "Regular Expression For Reference Field [ " & objICUBPSCompany.RegularExpressionForReferenceField & " ] ; "
                            Action += "Billing Inquiry [ " & objICUBPSCompany.IsBillingInquiry & " ] ; "
                            Action += "Is Active [ " & objICUBPSCompany.IsActive & " ]  added ."
                            ICUBPSCompanyController.AddUBPSCompany(objICUBPSCompany, False, Action, Me.UserId.ToString, Me.UserInfo.Username.ToString, Action.ToString)
                            UIUtilities.ShowDialog(Me, "Save UBP Company", "UBP Company added successfully.", ICBO.IC.Dialogmessagetype.Success)
                        Else
                            UIUtilities.ShowDialog(Me, "Save UBP Company", "Can not add duplicate UBP Company Code.", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Save UBP Company", "Can not add duplicate UBP Company Name.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else


                    objICUBPSCompany.CreatedBy = Me.UserId
                    objICUBPSCompany.CreatedDate = Date.Now
                    Action = Nothing
                    Action = "UBP Company " & objICUBPSCompany.CompanyName & " Updated : UBP Company Name from [ " & objICUBPSCompanyFrom.CompanyName & " ] to [ " & objICUBPSCompany.CompanyName & " ] ; "
                    Action += "Code [ " & objICUBPSCompanyFrom.CompanyCode & " ] to [ " & objICUBPSCompany.CompanyCode & " ] ; "
                    Action += "Type [ " & objICUBPSCompanyFrom.CompanyType & " ] to [ " & objICUBPSCompany.CompanyType & " ] ; "
                    Action += "Reference Field Minimum Length from [ " & objICUBPSCompanyFrom.ReferenceFieldMinLength & " ] to [ " & objICUBPSCompany.ReferenceFieldMinLength & " ] ; "
                    Action += "Reference Field Maximum Length from [ " & objICUBPSCompanyFrom.ReferenceFieldMaxLength & " ] to [ " & objICUBPSCompany.ReferenceFieldMaxLength & " ] ; "
                    Action += "Reference Field Caption [ " & objICUBPSCompanyFrom.ReferenceFieldCaption & " ] to [ " & objICUBPSCompany.ReferenceFieldCaption & " ] ; "
                    Action += "Reference Field Format from [ " & objICUBPSCompanyFrom.ReferenceFieldMinLength & " ] to [ " & objICUBPSCompany.ReferenceFieldMinLength & " ] ; "
                    Action += "Reference Field Regular Expression from [ " & objICUBPSCompanyFrom.RegularExpressionForReferenceField & " ] to [ " & objICUBPSCompany.RegularExpressionForReferenceField & " ] ; "
                    Action += "Billing Inquiry from [ " & objICUBPSCompanyFrom.IsBillingInquiry & " ] to [ " & objICUBPSCompany.IsBillingInquiry & " ] ; "
                    Action += "Is Active from [ " & objICUBPSCompanyFrom.IsActive & " to " & objICUBPSCompany.IsActive & " ]."
                    ICUBPSCompanyController.AddUBPSCompany(objICUBPSCompany, True, Action, Me.UserId.ToString, Me.UserInfo.Username.ToString, Action.ToString)
                    UIUtilities.ShowDialog(Me, "Save Company", "Company updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                End If
                Clear()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Public Function validateAmount(ByVal type As String) As Boolean
        If type = "Any amount within Min & Max" Then
            If txtAmountMaxValue.Text.Trim = "" Or txtAmountMinValue.Text.Trim = "" Then
                Return False
            End If

            Try
                Dim dec As Decimal = CDec(txtAmountMaxValue.Text.Trim)
            Catch ex As Exception
                Return False
            End Try

            Try
                Dim dec As Decimal = CDec(txtAmountMinValue.Text.Trim)
            Catch ex As Exception
                Return False
            End Try
            Return True
        ElseIf type = "Multiple of X within Min & Max" Then
            If txtAmountMaxValue.Text.Trim = "" Or txtAmountMinValue.Text.Trim = "" Or txtAmountXValue.Text.Trim = "" Then
                Return False
            End If

            Try
                Dim dec As Decimal = CDec(txtAmountMaxValue.Text.Trim)
            Catch ex As Exception
                Return False
            End Try

            Try
                Dim dec As Decimal = CDec(txtAmountMinValue.Text.Trim)
            Catch ex As Exception
                Return False
            End Try

            Try
                Dim dec As Decimal = CDec(txtAmountXValue.Text.Trim)
            Catch ex As Exception
                Return False
            End Try
            Return True
        End If
        Return True
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub
    

    

#End Region

#Region "Other Functions/Routines"

    
    Private Sub Clear()
        'txtCompanyCode.Text = ""
        txtUBPSCompanyCode.Text = ""
        txtCompanyName.Text = ""
        txtRefMinLength.Text = ""
        txtRefMaxLength.Text = ""
        txtRefFieldCaption.Text = ""
        txtReferenceFieldFormat.Text = ""
        txtAmountMinValue.Text = ""
        txtAmountMaxValue.Text = ""
        txtAmountXValue.Text = ""
        ddlCompanyType.ClearSelection()
        ddlAmountAcceptanceCriteria.ClearSelection()
        rbListRefFieldRegExp.ClearSelection()

        chkActive.Checked = True
        chkApproved.Checked = False
        chkPerformBillInquiry.Checked = False
        ManageAmountMinAndMax(ddlAmountAcceptanceCriteria.SelectedValue.ToString)
    End Sub

    Private Function CheckDuplicate(ByVal objICCompany As ICUBPSCompany) As Boolean
        Try
            Dim rslt As Boolean = False

            Dim objICCompanyColl As New ICUBPSCompanyCollection
            objICCompanyColl.Query.Where(objICCompanyColl.Query.CompanyName = objICCompany.CompanyName)
            If objICCompanyColl.Query.Load() Then
                rslt = True
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function CheckDuplicateCompanyCode(ByVal objICCompany As ICUBPSCompany) As Boolean
        Try
            Dim rslt As Boolean = False

            Dim objICCompanyColl As New ICUBPSCompanyCollection
            objICCompanyColl.Query.Where(objICCompanyColl.Query.CompanyCode = objICCompany.CompanyCode)
            If objICCompanyColl.Query.Load() Then
                rslt = True
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
#End Region

    Protected Sub btnApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproved.Click


        Try

                Dim objICUBPCompany As New ICUBPSCompany


                objICUBPCompany.LoadByPrimaryKey(UBPSCpmanyID)

                If Me.UserInfo.IsSuperUser = False Then
                    If objICUBPCompany.CreatedBy = Me.UserId Then
                        UIUtilities.ShowDialog(Me, "Error", "Company must be approved by user other than maker", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    Else
                        ICUBPSCompanyController.ApproveUBPSCompanyByCode(objICUBPCompany.UBPSCompanyID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, chkApproved.Checked)
                        If chkApproved.Checked = True Then
                            UIUtilities.ShowDialog(Me, "Save UBP Company", "UBP Company approved successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        ElseIf chkApproved.Checked = False Then
                            UIUtilities.ShowDialog(Me, "Save Company", "UBP Company un approved successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        End If
                    End If
                Else
                    ICUBPSCompanyController.ApproveUBPSCompanyByCode(objICUBPCompany.UBPSCompanyID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, chkApproved.Checked)
                    If chkApproved.Checked = True Then
                        UIUtilities.ShowDialog(Me, "Save UBP Company", "UBP Company approved successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    ElseIf chkApproved.Checked = False Then
                        UIUtilities.ShowDialog(Me, "Save Company", "UBP Company un approved successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    End If
                End If




            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub





    Protected Sub ddlAmountAcceptanceCriteria_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAmountAcceptanceCriteria.SelectedIndexChanged
        Try
            ManageAmountMinAndMax(ddlAmountAcceptanceCriteria.SelectedValue.ToString)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ManageAmountMinAndMax(ByVal AmountSelectCriteria As String)

        Select Case AmountSelectCriteria
            Case "0"
                tblMinMax.Style.Add("display", "none")
                radAmoun.Enabled = False
                radAmoun.GetSettingByBehaviorID("radMaxValue").Validation.IsRequired = False
                radAmoun.GetSettingByBehaviorID("radMinValue").Validation.IsRequired = False
                radAmoun.GetSettingByBehaviorID("radXAmount").Validation.IsRequired = False
                'rfvAmountMinValue.Enabled = False
                'rfvAmountMaxValue.Enabled = False
                cmpMinAndMax.Enabled = False
                'rfvAmountX.Enabled = False
                txtAmountMinValue.Text = ""
                txtAmountMaxValue.Text = ""
                txtAmountXValue.Text = ""
                chkPerformBillInquiry.Checked = False
                chkPerformBillInquiry.Enabled = True
            Case "Any Amount"
                tblMinMax.Style.Add("display", "none")
                radAmoun.Enabled = False
                radAmoun.GetSettingByBehaviorID("radMaxValue").Validation.IsRequired = False
                radAmoun.GetSettingByBehaviorID("radMinValue").Validation.IsRequired = False
                radAmoun.GetSettingByBehaviorID("radXAmount").Validation.IsRequired = False
                'rfvAmountMinValue.Enabled = False
                'rfvAmountMaxValue.Enabled = False
                cmpMinAndMax.Enabled = False
                'rfvAmountX.Enabled = False
                txtAmountMinValue.Text = ""
                txtAmountMaxValue.Text = ""
                txtAmountXValue.Text = ""
                chkPerformBillInquiry.Checked = False
                chkPerformBillInquiry.Enabled = True
            Case "Only Billed Amount"
                tblMinMax.Style.Add("display", "none")
                radAmoun.Enabled = False
                radAmoun.GetSettingByBehaviorID("radMaxValue").Validation.IsRequired = False
                radAmoun.GetSettingByBehaviorID("radMinValue").Validation.IsRequired = False
                radAmoun.GetSettingByBehaviorID("radXAmount").Validation.IsRequired = False
                'rfvAmountMinValue.Enabled = False
                'rfvAmountMaxValue.Enabled = False
                cmpMinAndMax.Enabled = False
                'rfvAmountX.Enabled = False
                txtAmountMinValue.Text = ""
                txtAmountMaxValue.Text = ""
                txtAmountXValue.Text = ""
                chkPerformBillInquiry.Checked = True
                chkPerformBillInquiry.Enabled = False
            Case "Any amount within Min & Max"
                tblMinMax.Style.Remove("display")
                radAmoun.Enabled = True
                radAmoun.GetSettingByBehaviorID("radMaxValue").Validation.IsRequired = True
                radAmoun.GetSettingByBehaviorID("radMinValue").Validation.IsRequired = True
                'rfvAmountMinValue.Enabled = True
                'rfvAmountMaxValue.Enabled = True
                cmpMinAndMax.Enabled = True
                'rfvAmountX.Enabled = False
                DirectCast(Me.Control.FindControl("trAmontXlbl"), HtmlTableRow).Style.Add("display", "none")
                DirectCast(Me.Control.FindControl("trAmontXCntrol"), HtmlTableRow).Style.Add("display", "none")
                'DirectCast(Me.Control.FindControl("trAmontXRfv"), HtmlTableRow).Style.Add("display", "none")
                radAmoun.GetSettingByBehaviorID("radXAmount").Validation.IsRequired = False
                txtAmountMinValue.Text = ""
                txtAmountMaxValue.Text = ""
                txtAmountXValue.Text = ""
                chkPerformBillInquiry.Checked = False
                chkPerformBillInquiry.Enabled = True
            Case "Multiple of X within Min & Max"
                tblMinMax.Style.Remove("display")
                radAmoun.Enabled = True
                radAmoun.GetSettingByBehaviorID("radMaxValue").Validation.IsRequired = True
                radAmoun.GetSettingByBehaviorID("radMinValue").Validation.IsRequired = True
                'rfvAmountMinValue.Enabled = True
                'rfvAmountMaxValue.Enabled = True
                cmpMinAndMax.Enabled = True
                'rfvAmountX.Enabled = True
                DirectCast(Me.Control.FindControl("trAmontXlbl"), HtmlTableRow).Style.Remove("display")
                DirectCast(Me.Control.FindControl("trAmontXCntrol"), HtmlTableRow).Style.Remove("display")
                'DirectCast(Me.Control.FindControl("trAmontXRfv"), HtmlTableRow).Style.Remove("display")
                radAmoun.GetSettingByBehaviorID("radXAmount").Validation.IsRequired = True
                txtAmountMinValue.Text = ""
                txtAmountMaxValue.Text = ""
                txtAmountXValue.Text = ""
                chkPerformBillInquiry.Checked = False
                chkPerformBillInquiry.Enabled = True
        End Select
    End Sub
End Class
