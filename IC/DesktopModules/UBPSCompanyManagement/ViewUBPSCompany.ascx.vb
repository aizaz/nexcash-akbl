﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports FtpLib
Imports System.Net
Partial Class DesktopModules_UBPSCompanyManagement_ViewUBPSCompany
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_UBPSCompanyManagement_ViewUBPSCompany_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnAddCompany.Visible = CBool(htRights("Add"))
                LoadddlCompanyCode()
                LoadddlCompanyName()
                LoadddlCompanyType()
                LoadUBPSCompany(True)

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "UBPS Company Management")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
#Region "Drop Down Events"
    Private Sub LoadUBPSCompany(ByVal DoDataBind As Boolean)
        Try
            Dim CompanyCode, CompanyName, CompanyType As String
            CompanyCode = "0"
            CompanyName = "0"
            CompanyType = "0"
            If ddlCompanyCode.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompanyCode.SelectedValue.ToString
            End If
            If ddlCompanyName.SelectedValue.ToString <> "0" Then
                CompanyName = ddlCompanyName.SelectedItem.ToString
            End If
            If ddlCompanyType.SelectedValue.ToString <> "0" Then
                CompanyType = ddlCompanyType.SelectedValue.ToString
            End If
            ICUBPSCompanyController.GetAllUBPSCompanyGV(CompanyCode, CompanyName, CompanyType, Me.gvCompany, Me.gvCompany.CurrentPageIndex + 1, Me.gvCompany.PageSize, DoDataBind)
            If gvCompany.Items.Count > 0 Then
                gvCompany.Visible = True
                lblRNF.Visible = False
             
                gvCompany.Columns(9).Visible = CBool(htRights("Delete"))
            Else
                gvCompany.Visible = False
                lblRNF.Visible = True
           
            End If
           
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub


#End Region
#Region "Button Events"
    Protected Sub btnSaveBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCompany.Click
        Page.Validate()

        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveUBPSCompany", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If

    End Sub
#End Region


    Protected Sub gvCompany_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvCompany.ItemCommand
        Try
            Dim objICUBPSCompany As New ICUBPSCompany
            If e.CommandName = "del" Then
                If objICUBPSCompany.LoadByPrimaryKey(e.CommandArgument.ToString) Then
                    If objICUBPSCompany.IsApproved = True Then
                        UIUtilities.ShowDialog(Me, "Error", "UBPS Company is approved and can not be deleted", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    Else
                        ICUBPSCompanyController.DeleteUBPSCompanyByCode(objICUBPSCompany.UBPSCompanyID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        UIUtilities.ShowDialog(Me, "Delete UBPS Company", "UBPS Company deleted successfully", ICBO.IC.Dialogmessagetype.Success)
                        LoadUBPSCompany(True)
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Error ", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                LoadUBPSCompany(False)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvCompany_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvCompany.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvCompany.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub



    Protected Sub gvCompany_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvCompany.NeedDataSource
        Try
            LoadUBPSCompany(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub gvCompany_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvCompany.PageIndexChanged
        Try
            gvCompany.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCompanyCode()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompanyCode.Items.Clear()
            ddlCompanyCode.Items.Add(lit)
            ddlCompanyCode.AppendDataBoundItems = True
            ddlCompanyCode.DataSource = ICUBPSCompanyController.GetAllCompanyCode
            ddlCompanyCode.DataTextField = "CompanyCode"
            ddlCompanyCode.DataValueField = "CompanyCode"
            ddlCompanyCode.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCompanyName()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompanyName.Items.Clear()
            ddlCompanyName.Items.Add(lit)
            ddlCompanyName.AppendDataBoundItems = True
            ddlCompanyName.DataSource = ICUBPSCompanyController.GetAllCompanyByCompanyCode(ddlCompanyCode.SelectedValue.ToString)
            ddlCompanyName.DataTextField = "CompanyName"
            ddlCompanyName.DataValueField = "UBPSCompanyID"
            ddlCompanyName.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCompanyType()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompanyType.Items.Clear()
            ddlCompanyType.Items.Add(lit)
            ddlCompanyType.AppendDataBoundItems = True
            ddlCompanyType.DataSource = ICUBPSCompanyController.GetAllCompanyTypeByCompanyName(ddlCompanyName.SelectedValue.ToString)
            ddlCompanyType.DataTextField = "CompanyType"
            ddlCompanyType.DataValueField = "CompanyType"
            ddlCompanyType.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCompanyCode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompanyCode.SelectedIndexChanged
        Try
            LoadddlCompanyName()
            LoadddlCompanyType()
            LoadUBPSCompany(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCompanyName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompanyName.SelectedIndexChanged
        Try

            LoadddlCompanyType()
            LoadUBPSCompany(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCompanyType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompanyType.SelectedIndexChanged
        Try


            LoadUBPSCompany(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
