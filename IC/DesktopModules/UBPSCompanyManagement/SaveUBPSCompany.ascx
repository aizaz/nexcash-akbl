﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveUBPSCompany.ascx.vb" Inherits="DesktopModules_UBPSCompanyManagement_SaveUBPSCompany" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .style1
    {
        width: 25%;
        height: 23px;
    }
.RadPicker{vertical-align:middle}.rdfd_{position:absolute}.RadPicker .rcTable{table-layout:auto}.RadPicker .RadInput{vertical-align:baseline}.RadInput_Default{font:12px "segoe ui",arial,sans-serif}.riSingle{box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;-webkit-box-sizing:border-box;-khtml-box-sizing:border-box}.riSingle{position:relative;display:inline-block;white-space:nowrap;text-align:left}.RadInput{vertical-align:middle}.riSingle .riTextBox{box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;-webkit-box-sizing:border-box;-khtml-box-sizing:border-box}.RadPicker_Default .rcCalPopup{background-position:0 0}.RadPicker_Default .rcCalPopup{background-image:url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Calendar.sprite.gif')}.RadPicker .rcCalPopup{display:block;overflow:hidden;width:22px;height:22px;background-color:transparent;background-repeat:no-repeat;text-indent:-2222px;text-align:center}
    div.RadPicker table.rcSingle .rcInputCell{padding-right:0}div.RadPicker table.rcSingle .rcInputCell{padding-right:0}div.RadPicker table.rcSingle .rcInputCell{padding-right:0}div.RadPicker table.rcSingle .rcInputCell{padding-right:0}.RadPicker table.rcTable .rcInputCell{padding:0 4px 0 0}.RadPicker table.rcTable .rcInputCell{padding:0 4px 0 0}.RadPicker table.rcTable .rcInputCell{padding:0 4px 0 0}.RadPicker table.rcTable .rcInputCell{padding:0 4px 0 0}.RadPicker table.rcTable .rcInputCell{padding:0 4px 0 0}.RadPicker table.rcTable .rcInputCell{padding:0 4px 0 0}.RadPicker table.rcTable .rcInputCell{padding:0 4px 0 0}.RadPicker table.rcTable .rcInputCell{padding:0 4px 0 0}
    </style>

<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }
    function PerformBillInquiry(UBPSCompanyID) {
        var CheckBox=document.getElementById('<%=chkPerformBillInquiry.ClientID %>')
        if (UBPSCompanyID !== 0 && CheckBox.checked == false) {
            alert("Bill inquiry will not performed on company");
            return true;


        }
        else {

            return false;
        }
    }
   </script>
<telerik:RadInputManager ID="radValidation" runat="server">
    <telerik:RegExpTextBoxSetting BehaviorID="radCompanyCode" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9.-_/]{1,10}$" ErrorMessage="Invalid Company Code"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtUBPSCompanyCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radCompanyName" Validation-IsRequired="true"
        EmptyMessage="" ErrorMessage="Invalid Company Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCompanyName" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radReferenceFieldFormat" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Invalid Reference Fiel dFormat">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceFieldFormat" />
        </TargetControls>
    </telerik:TextBoxSetting>
    
     <telerik:RegExpTextBoxSetting BehaviorID="radRefFieldMinLength" Validation-IsRequired="True"
        ErrorMessage="Invalid Reference Field Min Length" EmptyMessage="" ValidationExpression="^[0-9]*$" >
        <TargetControls>
            <telerik:TargetInput ControlID="txtRefMinLength" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
      <telerik:RegExpTextBoxSetting BehaviorID="radRefFieldMaxLength" Validation-IsRequired="True" 
        ErrorMessage="Invalid Reference Field Max Length" EmptyMessage="" ValidationExpression="^[0-9]*$">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRefMaxLength" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="radRefFieldCaption" ErrorMessage="Invalid Reference Field Caption"
        EmptyMessage="" Validation-IsRequired="true">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRefFieldCaption" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
   
  
   
</telerik:RadInputManager>
<telerik:RadInputManager ID="radAmoun" Enabled="false" runat="server">
     <telerik:RegExpTextBoxSetting BehaviorID="radMaxValue" Validation-IsRequired="False" 
        ErrorMessage="Invalid Max Value" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAmountMaxValue" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="radMinValue" Validation-IsRequired="False" 
        ErrorMessage="Invalid Min Value" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAmountMinValue" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="radXAmount" Validation-IsRequired="false" 
        ErrorMessage="Invalid X Amount" EmptyMessage="" ValidationExpression="^\d{1,15}(\.\d{0,2})?$">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAmountXValue" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyType" runat="server" Text="Company Type" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqCompanyType" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCompanyType" runat="server" 
                CssClass="dropdown">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem>Utility</asp:ListItem>
                <asp:ListItem>Prepaid</asp:ListItem>
                <asp:ListItem>Postpaid</asp:ListItem>
                <asp:ListItem>Internet Service Provider</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvGroup" runat="server" 
                ControlToValidate="ddlCompanyType" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Company Type" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyCode" runat="server" Text="Company Code" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqCompanyCode" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyName" runat="server" Text="Company Name" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqCompanyName" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtUBPSCompanyCode" runat="server" CssClass="txtbox" 
                MaxLength="10"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtCompanyName" runat="server" CssClass="txtbox" 
                MaxLength="150"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblRefFieldMinLength" runat="server" Text="Reference Field Minimum Length" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqRefMinLength" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblRefFieldMaxLength" runat="server" Text="Reference Field Maximum Length" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqRefMaxLength" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtRefMinLength" runat="server" CssClass="txtbox" 
                MaxLength="2"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtRefMaxLength" runat="server" CssClass="txtbox" 
                MaxLength="2"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <%--<asp:RegularExpressionValidator ID="revRefFieldMinLength" runat="server" ControlToValidate="txtRefMinLength" Display="Dynamic" Enabled="True" ErrorMessage="Please enter numbers" SetFocusOnError="True" ToolTip="Required Field" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>--%>
            
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <%--<asp:RegularExpressionValidator ID="revRefFieldMaxLength" runat="server" ControlToValidate="txtRefMaxLength" Display="Dynamic" Enabled="True" ErrorMessage="Please enter numbers" SetFocusOnError="True" ToolTip="Required Field" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>--%>
            
            <asp:CompareValidator ID="cmpMinAndMaxRefernce" runat="server" ControlToCompare="txtRefMinLength" ControlToValidate="txtRefMaxLength" Display="Dynamic" 
                ErrorMessage="Maximum value must be greater than minimum value" Operator="GreaterThanEqual" Type="Integer" SetFocusOnError="True"></asp:CompareValidator>
            
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblReferenceFieldCaption" runat="server" Text="Reference Field Caption" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqRefFieldCaption" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblReferenceFieldFormat" runat="server" Text="Reference Field Format" 
                CssClass="lbl"></asp:Label>
            </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtRefFieldCaption" runat="server" CssClass="txtbox" 
                MaxLength="150"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtReferenceFieldFormat" runat="server" CssClass="txtbox" 
                MaxLength="150" TextMode="MultiLine" Height="70px"  onkeypress="return textboxMultilineMaxNumber(this,150)"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
        </td>
        <td align="left" valign="top" class="style1">
        </td>
        <td align="left" valign="top" class="style1">
        </td>
        <td align="left" valign="top" class="style1">
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblRefRegExprsion" runat="server" Text="Regular Expression for Reference Field" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqRefFieldRegEx" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAmountAcceptanceCriteria" runat="server" Text="Amount Acceptance Criteria" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqAmtAccptCrt" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RadioButtonList ID="rbListRefFieldRegExp" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem>Numeric</asp:ListItem>
                <asp:ListItem>Alpha Numeric</asp:ListItem>
            </asp:RadioButtonList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlAmountAcceptanceCriteria" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem>Only Billed Amount</asp:ListItem>
                <asp:ListItem>Any amount within Min &amp; Max</asp:ListItem>
                <asp:ListItem>Multiple of X within Min &amp; Max</asp:ListItem>
                <asp:ListItem>Any Amount</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:RequiredFieldValidator ID="rfvGroup0" runat="server" 
                ControlToValidate="rbListRefFieldRegExp" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Regular Expression For Reference Field" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue=""></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" class="style1">
            </td>
        <td align="left" valign="top" class="style1">
            <asp:RequiredFieldValidator ID="rfvGroup1" runat="server" 
                ControlToValidate="ddlAmountAcceptanceCriteria" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Amount Acceptance Criteria" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" class="style1">
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblPerformBillInquiry" runat="server" Text="Perform Bill Inquiry" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkPerformBillInquiry" runat="server" Text=" " CssClass="chkBox" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
           <table id="tblMinMax" runat="server" style="width:100%">
               <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <asp:Label ID="lblAmountMinValue" runat="server" Text="Amount Minimum Value" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqAmountMinValue" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <asp:TextBox ID="txtAmountMinValue" runat="server" CssClass="txtbox" 
                MaxLength="7"></asp:TextBox>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <asp:Label ID="lblAmountMaxValue" runat="server" Text="Amount Maximum Value" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqAmountMaxValue" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <asp:TextBox ID="txtAmountMaxValue" runat="server" CssClass="txtbox" 
                MaxLength="7"></asp:TextBox>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <%--<asp:RegularExpressionValidator ID="rfvAmountMaxValue" runat="server" ControlToValidate="txtAmountMaxValue" Display="Dynamic" Enabled="True" ErrorMessage="Please enter numbers" SetFocusOnError="True" ToolTip="Required Field" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>--%>
            <asp:CompareValidator ID="cmpMinAndMax" runat="server" ControlToCompare="txtAmountMinValue" ControlToValidate="txtAmountMaxValue" Display="Dynamic" ErrorMessage="Amount maximum value must be greater than or equal to minimum value" Operator="GreaterThanEqual" SetFocusOnError="True" Type="Currency"></asp:CompareValidator>
        </td>
    </tr>
    <tr id="trAmontXlbl" runat="server" align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <asp:Label ID="lblAmountX" runat="server" Text="Amount X Value" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqAmountXValue" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
    </tr>
    <tr id="trAmontXCntrol" runat="server" align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <asp:TextBox ID="txtAmountXValue" runat="server" CssClass="txtbox" 
                MaxLength="7"></asp:TextBox>
        </td>
    </tr>
           </table></td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIsActive" runat="server" Text="Is Active" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIsApproved" runat="server" Text="Is Approved" CssClass="lbl" Visible="False"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkActive" runat="server" Text=" " CssClass="chkBox" Checked="True" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
            <asp:CheckBox ID="chkApproved" runat="server" Text=" " Visible="False" CssClass="chkBox" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnApproved" runat="server" Text="Approve" CssClass="btn" Visible="False"
                Width="71px" CausesValidation="False" />
            &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px"/>
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False"
                />
            &nbsp;
        </td>
    </tr>
</table>
