﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Text
Imports System.Data
Imports System.Data.OleDb
Imports Telerik.Web.UI

Partial Class DesktopModules_MISReportViewer_MISReportViewer
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Dim dtDa As DataTable
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then

                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing
                btnView.Visible = CBool(htRights("View"))
                LoadddlReport(Me.UserId.ToString)
                pnlrptViewer.Style.Add("display", "none")
            End If



        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Reports")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Private Sub LoadddlReport(ByVal UsersID As String)
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlReport.Items.Clear()
            ddlReport.Items.Add(lit)
            ddlReport.AppendDataBoundItems = True
            ddlReport.DataSource = ICMISReportController.GetReportNameforReportViewer(UsersID.ToString, "Report")
            ddlReport.DataTextField = "ReportName"
            ddlReport.DataValueField = "ReportID"
            ddlReport.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
        If Page.IsValid Then
            Try
                Dim misRPT As New ICMISReports
                Dim objICFiles As New ICFiles
                Dim report As Telerik.Reporting.Report
                If misRPT.LoadByPrimaryKey(ddlReport.SelectedValue.ToString()) Then
                    Dim fl As New System.IO.MemoryStream

                    fl.Write(misRPT.ReportFileData, 0, misRPT.ReportFileSize)
                    fl.Seek(0, SeekOrigin.Begin)
                    Dim settings As New System.Xml.XmlReaderSettings()
                    settings.IgnoreWhitespace = True

                    Dim xmlReader As System.Xml.XmlReader = System.Xml.XmlReader.Create(fl, settings)
                    Dim xmlSerializer As New Telerik.Reporting.XmlSerialization.ReportXmlSerializer()


                    Dim report2 As Telerik.Reporting.Report = DirectCast(xmlSerializer.Deserialize(xmlReader), Telerik.Reporting.Report)
                    report = GetFinalReportWithParameters(report2)
                    ReportViewer1.ReportSource = report
                    ReportViewer1.RefreshReport()
                    pnlrptViewer.Style.Remove("display")
                    ReportViewer1.ShowPrintButton = CBool(htRights("Export"))
                    ReportViewer1.ShowExportGroup = CBool(htRights("Export"))
                    Dim StrAuditTrail As String = Nothing
                    Dim objICUser As New ICUser
                    objICUser.LoadByPrimaryKey(Me.UserId)
                    StrAuditTrail += "MIS Report with ID [ " & misRPT.ReportID & " ] [ " & misRPT.ReportName & " ] is viewed by user [ " & objICUser.UserID & " ] [ " & objICUser.UserName & " ]."
                    StrAuditTrail += " at [ " & Date.Now & " ]."
                    ICUtilities.AddAuditTrail(StrAuditTrail, "MISReport", misRPT.ReportID, Me.UserId.ToString, Me.UserInfo.Username.ToString, "VIEW")
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Private Function GetFinalReportWithParameters(ByVal Report As Telerik.Reporting.Report) As Telerik.Reporting.Report
        Dim FinalReport As Telerik.Reporting.Report
        Dim objICUser As New ICUser
        FinalReport = Report
        objICUser.LoadByPrimaryKey(Me.UserInfo.UserID.ToString)


        If FinalReport.ReportParameters.Count > 0 Then
            If FinalReport.ReportParameters.Contains("BranchCode") Then
                FinalReport.ReportParameters("BranchCode").Value = objICUser.UpToICOfficeByOfficeCode.OfficeCode.ToString
            End If
            If FinalReport.ReportParameters.Contains("UserName") Then
                FinalReport.ReportParameters("UserName").Value = objICUser.UserName.ToString
            End If
        End If

        Return FinalReport

    End Function
    Private Sub reportViewer1_Export(ByVal sender As Object, ByVal args As Telerik.ReportViewer.WinForms.ExportEventArgs)
        Select Case args.Format
            Case "Excel"
                Dim misRPT As New ICMISReports

                misRPT.LoadByPrimaryKey(ddlReport.SelectedValue.ToString())
                Dim StrAuditTrail As String = Nothing
                Dim objICUser As New ICUser
                objICUser.LoadByPrimaryKey(Me.UserId)
                StrAuditTrail += "MIS Report with ID [ " & misRPT.ReportID & " ] [ " & misRPT.ReportName & " ] is Export in Excel by user [ " & objICUser.UserName & " ] [ " & objICUser.UserName & " ]."
                StrAuditTrail += " at [ " & Date.Now & " ]."
                ICUtilities.AddAuditTrail(StrAuditTrail, "MISReport", misRPT.ReportID, Me.UserId.ToString, Me.UserInfo.Username.ToString, "EXPORT EXCEL")
                Exit Select
            Case "PDF"
                Dim misRPT As New ICMISReports

                misRPT.LoadByPrimaryKey(ddlReport.SelectedValue.ToString())
                Dim StrAuditTrail As String = Nothing
                Dim objICUser As New ICUser
                objICUser.LoadByPrimaryKey(Me.UserId)
                StrAuditTrail += "MIS Report with ID [ " & misRPT.ReportID & " ] [ " & misRPT.ReportName & " ] is Export in PDF by user [ " & objICUser.UserName & " ] [ " & objICUser.UserName & " ]."
                StrAuditTrail += " at [ " & Date.Now & " ]."
                ICUtilities.AddAuditTrail(StrAuditTrail, "MISReport", misRPT.ReportID, Me.UserId.ToString, Me.UserInfo.Username.ToString, "EXPORT PDF")

                Exit Select
        End Select
    End Sub

    Protected Sub ddlReport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReport.SelectedIndexChanged
        Try
            'If ddlReport.SelectedValue.ToString = "0" Then
            pnlrptViewer.Style.Add("display", "none")
            'End If
        Catch ex As Exception

        End Try
    End Sub
End Class

