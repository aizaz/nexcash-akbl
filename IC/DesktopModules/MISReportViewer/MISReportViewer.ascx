﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MISReportViewer.ascx.vb" Inherits="DesktopModules_MISReportViewer_MISReportViewer" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register assembly="Telerik.ReportViewer.WebForms,  Version=7.0.13.220, Culture=neutral, PublicKeyToken=A9D7983DFCC261BE" namespace="Telerik.ReportViewer.WebForms" tagprefix="telerik" %>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:TextBoxSetting BehaviorID="reTnxCount" Validation-IsRequired="false" EmptyMessage="Transaction Count"
        ErrorMessage="Transaction Count">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTnxCount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reTotalAmount" Validation-IsRequired="false"
        EmptyMessage="Total Amount" ErrorMessage="Total Amount">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTotalAmount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reCurrency" Validation-IsRequired="false" EmptyMessage="Currency"
        ErrorMessage="Currency">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCurrency" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rePaymentMode" Validation-IsRequired="false"
        EmptyMessage="Payment Mode" ErrorMessage="Payment Mode">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPaymentMode" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reAvailableBalance" Validation-IsRequired="false"
        EmptyMessage="Available Balance" ErrorMessage="Available Balance">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAvailableBalance" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reInstructionNo" Validation-IsRequired="false"
        EmptyMessage="Enter Instruction No." ErrorMessage="Enter Instruction No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstructionNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reInstrumentNo" Validation-IsRequired="false"
        EmptyMessage="Enter Instrument No." ErrorMessage="Enter Instrument No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstrumentNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reReferenceNo" Validation-IsRequired="false"
        EmptyMessage="Enter Reference No." ErrorMessage="Enter Reference No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reRemarks" Validation-IsRequired="true" EmptyMessage="Enter Remarks"
        ErrorMessage="Enter Remarks">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRemarks" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Label ID="lblPageHeader" runat="server" Text="MIS Report Viewer" 
                            CssClass="headingblue"></asp:Label>
                        <br />
                        Note:&nbsp; Fields marked as
                        <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        &nbsp;are required.
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                       <table align="left" valign="top" style="width: 100%">
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 25%">
                                                <asp:Label ID="lblReport" runat="server" Text="Report" CssClass="lbl"></asp:Label>
                        <asp:Label ID="Label5" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="width: 25%">
                                                &nbsp;</td>
                                            <td align="left" valign="top" style="width: 25%">
                                                &nbsp;</td>
                                            <td align="left" valign="top" style="width: 25%">
                                                &nbsp;</td>
                                        </tr>
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 20%" >
                                                <asp:DropDownList ID="ddlReport" runat="server" 
                                                    CssClass="dropdown" AutoPostBack="True">
                                                    <asp:ListItem Value="0">-- Select --</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td align="left" valign="top" style="width: 80%" colspan="3">
                                                <asp:Button ID="btnView" runat="server" Text="View" Width="83px" 
                                                    CssClass="btn" />
                                            </td>
                                        </tr>                                                                        
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 20%" >
            <asp:RequiredFieldValidator ID="rfvddlReport" runat="server" 
                ControlToValidate="ddlReport" Display="Dynamic" 
                ErrorMessage="Please select Report" InitialValue="0" 
                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            </td>
                                            <td align="left" valign="top" style="width: 80%" colspan="3">
                                                &nbsp;</td>
                                        </tr>                                                                        
                                    </table>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                    <asp:Panel ID="pnlrptViewer" runat="server" Width="100%">
                   <%-- <table id="tblRptViewer" runat="server" width="100%">
                    <tr>
                    <td align="left" valign="top">--%>
                    
                    
                    
                        <telerik:ReportViewer ID="ReportViewer1" runat="server" Width="100%" >
                       
                        </telerik:ReportViewer>
                    
                    
                    
                    <%--</td>
                    
                    
                    </tr>
                    
                    
                    </table>--%>

                    </asp:Panel>
                    </td>
                </tr>
                </table>
        </td>
    </tr>
</table>
