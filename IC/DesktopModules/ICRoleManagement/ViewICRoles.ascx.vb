﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_ICRoleManagement_ViewICRoles
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_CustomRoles_ViewCustomRoles_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
             If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadICRoles(Me.gvICRoles.PageSize, 0, True)
                btnAddICRole.Visible = CBool(htRights("Add"))

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
    Private Sub LoadICRoles(ByVal PageSize As Integer, ByVal PageNumber As Integer, ByVal DoDataBind As Boolean)
        Try
            ICRoleController.GetAllICRolesForRadGrid("Bank Role", PageNumber, PageSize, Me.gvICRoles, DoDataBind)
            If gvICRoles.Items.Count > 0 Then
                gvICRoles.Visible = True
                lblRNF.Visible = False
                btnDeleteRoles.Visible = CBool(htRights("Delete"))
                btnAddICRole.Visible = CBool(htRights("Add"))
                gvICRoles.Columns(5).Visible = CBool(htRights("Delete"))
            Else
                gvICRoles.Visible = False
                lblRNF.Visible = True
                btnDeleteRoles.Visible = False


            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Bank Role Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#Region "Button Events"
    Protected Sub btnAddICRole_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddICRole.Click

        Response.Redirect(NavigateURL("SaveICRole", "&mid=" & Me.ModuleId & "&id=0"), False)

    End Sub
#End Region
#Region "Other Functions/Routines"
   
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            ' str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function
#End Region
    Protected Sub btnDeleteRoles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteRoles.Click


        If Page.IsValid Then
            Try
                Dim rowGVICRoles As GridDataItem
                Dim chkSelect As CheckBox
                Dim RoleID As String = ""
                Dim PassToDeleteCount As Integer = 0
                Dim FailToDeleteCount As Integer = 0
                Dim userCtrl As New RoleController
                Dim arrLst As New ArrayList



                If CheckgvCountryForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Bank Roles", "Please select atleast one role.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVICRoles In gvICRoles.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVICRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        RoleID = rowGVICRoles.GetDataKeyValue("RoleID").ToString
                        Dim objICRole As New ICRole
                        objICRole.es.Connection.CommandTimeout = 3600
                        If objICRole.LoadByPrimaryKey(RoleID.ToString()) Then
                            'Dim iRole As RoleInfo = ICRoleController.GetRoleByID(objICRole.RoleID, Me.PortalId)
                            arrLst = userCtrl.GetUsersInRole(Me.PortalId, objICRole.RoleName)
                            If arrLst.Count > 0 Then
                                FailToDeleteCount = FailToDeleteCount + 1
                            Else
                                Try

                                    ICRoleController.DeleteRoleFromIC(objICRole.RoleID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                    ICRoleController.DeleteICRole(objICRole.RoleID.ToString, Me.PortalId)
                                    PassToDeleteCount = PassToDeleteCount + 1
                                Catch ex As Exception
                                    If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                        FailToDeleteCount = FailToDeleteCount + 1
                                        Continue For
                                    End If
                                End Try
                            End If
                        End If
                    End If

                Next

                If Not PassToDeleteCount = 0 And Not FailToDeleteCount = 0 Then
                    LoadICRoles(Me.gvICRoles.PageSize, Me.gvICRoles.CurrentPageIndex + 1, True)
                    UIUtilities.ShowDialog(Me, "Bank Roles", "[ " & PassToDeleteCount.ToString & " ] Role(s) deleted successfuly.<br /> [ " & FailToDeleteCount.ToString & " ] Role(s) can not be deleted due to following reason: <br /> 1. Role is assigned to user", ICBO.IC.Dialogmessagetype.Failure)

                    Exit Sub
                ElseIf Not PassToDeleteCount = 0 And FailToDeleteCount = 0 Then
                    LoadICRoles(Me.gvICRoles.PageSize, Me.gvICRoles.CurrentPageIndex + 1, True)
                    UIUtilities.ShowDialog(Me, "Bank Roles", PassToDeleteCount.ToString & " Role(s) deleted successfuly.", ICBO.IC.Dialogmessagetype.Success)

                    Exit Sub
                ElseIf Not FailToDeleteCount = 0 And PassToDeleteCount = 0 Then
                    LoadICRoles(Me.gvICRoles.PageSize, Me.gvICRoles.CurrentPageIndex + 1, True)
                    UIUtilities.ShowDialog(Me, "Bank Roles", "[ " & FailToDeleteCount.ToString & " ] Role(s) can not be deleted due to following reasons: <br /> 1. Role is assigned to user", ICBO.IC.Dialogmessagetype.Failure)

                    Exit Sub
                End If

            Catch ex As Exception
                If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                    ProcessModuleLoadException(Me, ex, False)
                    UIUtilities.ShowDialog(Me, "Error", "Role can not be deleted. Delete associated record(s) first", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                Else
                    ProcessModuleLoadException(Me, ex, False)
                    UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                End If
            End Try
        End If


    End Sub

    Protected Sub gvICRoles_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvICRoles.ItemCommand
        Try
            Dim userCtrl As New RoleController
            Dim arrLst As New ArrayList
            Dim objICRole As New ICRole
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    objICRole.LoadByPrimaryKey(e.CommandArgument.ToString)
                    'Dim iRole As RoleInfo = ICRoleController.GetRoleByID(e.CommandArgument.ToString(), Me.PortalId)
                    arrLst = userCtrl.GetUsersInRole(Me.PortalId, objICRole.RoleName.ToString())
                    If arrLst.Count > 0 Then
                        UIUtilities.ShowDialog(Me, "Bank Roles", "Role cannot be deleted because users are assigned to this role.", ICBO.IC.Dialogmessagetype.Failure)
                        ICRoleController.GetAllICRolesForRadGrid("Bank Role", Me.gvICRoles.CurrentPageIndex, Me.gvICRoles.PageSize, Me.gvICRoles, True)
                        Exit Sub
                    Else
                        If objICRole.LoadByPrimaryKey(e.CommandArgument.ToString) Then

                            'If objICRole.IsApproved = True Then
                            '    UIUtilities.ShowDialog(Me, "Roles", "Role is approved and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
                            '    ICRoleController.GetAllICRolesForRadGrid("Bank Role", Me.gvICRoles.CurrentPageIndex, Me.gvICRoles.PageSize, Me.gvICRoles, True)
                            '    Exit Sub

                            'Else


                            ICRoleController.DeleteRoleFromIC(e.CommandArgument.ToString(), Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            ICRoleController.DeleteICRole(e.CommandArgument.ToString(), Me.PortalId)
                            'ICUtilities.AddAuditTrail("Role : " & iRole.RoleName.ToString() & " Deleted", "Role", e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                            UIUtilities.ShowDialog(Me, "Bank Roles", "Role deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                            LoadICRoles(Me.gvICRoles.PageSize, Me.gvICRoles.CurrentPageIndex + 1, True)
                            'End If


                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckgvCountryForProcessAll() As Boolean
        Try
            Dim rowGVICRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICRoles In gvICRoles.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub gvICRoles_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvICRoles.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvICRoles.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvICRoles_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvICRoles.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAllRoles"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvICRoles.MasterTableView.ClientID & "','0');"
        End If


    End Sub

    Protected Sub gvICRoles_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvICRoles.NeedDataSource
        Try
            LoadICRoles(Me.gvICRoles.PageSize, Me.gvICRoles.CurrentPageIndex + 1, False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvICRoles_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvICRoles.PageIndexChanged
        Try
            gvICRoles.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
