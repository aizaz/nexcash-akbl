﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Security.Permissions
Imports DotNetNuke.Entities.Portals
Partial Class DesktopModules_CustomRoles_AddEditCustomRoles
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private RoleCode As Integer
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_CustomRoles_AddEditCustomRoles_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            RoleCode = Request.QueryString("id")
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()

                If RoleCode.ToString() = "0" Then
                    Clear()
                    lblPageHeader.Text = "Add Bank Roles"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                Else

                    lblPageHeader.Text = "Edit Bank Roles"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    txtBankRoleName.ReadOnly = True
                    Dim objICRole As New ICRole
                    objICRole.es.Connection.CommandTimeout = 3600
                    objICRole.LoadByPrimaryKey(RoleCode)
                    txtBankRoleName.Text = objICRole.RoleName.ToString()
                    txtBankRoleDescription.Text = objICRole.Description
                    chkActive.Checked = objICRole.IsActive
                    'chkApproved.Checked = objICRole.IsApproved
                End If

            End If
          
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Bank Role Management")
            ViewState("htRights") = htRights
            
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
#Region "Button Events"
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                If RoleCode.ToString() = "0" Then
                    AddICRole(False)
                    Clear()
                Else
                    AddICRole(True)
                    Clear()
                End If

            Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
        End If
    End Sub
#End Region
#Region "Other Functions/Routines"
   
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return Result
        End Try
    End Function
    Private Sub Clear()
        txtBankRoleName.Text = ""
        txtBankRoleDescription.Text = ""
        chkActive.Checked = True
    End Sub

    Private Sub AddICRole(ByVal IsUpDate As Boolean)
        Dim objICRoleController As New RoleController
        Dim objRoleInfo As New RoleInfo
        If IsUpDate = False Then
            Try
                objRoleInfo.RoleName = txtBankRoleName.Text.ToString()
                objRoleInfo.Description = txtBankRoleDescription.Text.ToString()
                objRoleInfo.IsPublic = False
                objRoleInfo.RoleGroupID = -1
                objRoleInfo.AutoAssignment = False
                objRoleInfo.PortalID = Me.PortalId
                objICRoleController.AddRole(objRoleInfo)
            Catch ex As Exception
                If ex.Message.Contains("Cannot insert duplicate key in object") Then

                    UIUtilities.ShowDialog(Me, "Error", "Role already exists.", ICBO.IC.Dialogmessagetype.Failure)
                    Clear()
                    Exit Sub
                End If
            End Try
            If Not objRoleInfo.RoleID = 0 Then
                Try
                    Dim objICRole As New ICRole
                    objICRole.es.Connection.CommandTimeout = 3600
                    objICRole.RoleID = objRoleInfo.RoleID
                    objICRole.RoleName = objRoleInfo.RoleName.ToString()
                    objICRole.Description = txtBankRoleDescription.Text.ToString()
                    objICRole.RoleType = "Bank Role"
                    objICRole.CreateDate = Date.Now
                    objICRole.CreateBy = Me.UserId
                    objICRole.CreationDate = Date.Now
                    objICRole.Creater = Me.UserId
                    objICRole.IsActive = chkActive.Checked
                    ICRoleController.AddICRole(objICRole, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)

                    UIUtilities.ShowDialog(Me, "Save Role", "Role added successfully.", ICBO.IC.Dialogmessagetype.Success)
                Catch ex As Exception
                    ProcessModuleLoadException(Me, ex, False)
                    UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
                End Try
            End If
        Else
            objRoleInfo = objICRoleController.GetRole(RoleCode, Me.PortalId)

            objRoleInfo.RoleName = txtBankRoleName.Text.ToString()
            objRoleInfo.Description = txtBankRoleDescription.Text.ToString()
            objICRoleController.UpdateRole(objRoleInfo)

            Try
                Dim objICRole As New ICRole
                objICRole.es.Connection.CommandTimeout = 3600
                objICRole.LoadByPrimaryKey(RoleCode)
                objICRole.RoleName = txtBankRoleName.Text.ToString()
                objICRole.Description = txtBankRoleDescription.Text.ToString()
                objICRole.RoleType = "Bank Role"
                objICRole.IsActive = chkActive.Checked
                objICRole.CreateDate = Date.Now
                objICRole.CreateBy = Me.UserId
                ICRoleController.AddICRole(objICRole, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                'ICUtilities.AddAuditTrail("Role : " & objICRole.RoleName.ToString & "Updated.", "Role", objICRole.RoleID, Me.UserId, Me.UserInfo.Username.ToString())
                UIUtilities.ShowDialog(Me, "Save Role", "Role updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
#End Region

  
End Class
