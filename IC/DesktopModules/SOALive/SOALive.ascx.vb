﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports Telerik.Reporting
Imports System
Imports System.Data
Imports System.Data.OleDb
Partial Class DesktopModules_SOALive_SOALive
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Dim dtDa As DataTable
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ' txtCurrentDate.Text = Date.Now.ToString("dd-MMM-yyyy")
                Dim objICUser As New ICUser
                objICUser.es.Connection.CommandTimeout = 3600
                objICUser.LoadByPrimaryKey(Me.UserId)
                'raddpToDate.MaxDate = Date.Now
                'raddpFromDate.MaxDate = Date.Now

                'raddpToDate.SelectedDate = Date.Now
                'raddpFromDate.SelectedDate = Nothing


                btnExportExcel.Visible = False
                btnExportPDF.Visible = False

                ddlCompany.Enabled = True

                If objICUser.UserType = "Client User" Then
                    'LoadddlCompany()
                    Dim dtUserCompanies As New DataTable
                    dtUserCompanies = GetAllCompaniesTaggedWithUserByUserID(Me.UserId)
                    If dtUserCompanies.Rows.Count = 1 Then
                        LoadddlCompanyForClientUsers(dtUserCompanies)
                        ddlCompany.SelectedValue = dtUserCompanies.Rows(0)(0)
                        ddlCompany.Enabled = False
                    Else

                        LoadddlCompanyForClientUsers(dtUserCompanies)

                    End If
                    LoadddlAccountNumbers()
                    rvSOA.Visible = False
                    gvSOA.Visible = False
                ElseIf objICUser.UserType = "Bank User" Then
                    Dim dtUserCompanies As New DataTable
                    dtUserCompanies = GetAllCompaniesTaggedWithUserByUserID(Me.UserId)
                    If dtUserCompanies.Rows.Count = 1 Then
                        LoadddlCompanyForClientUsers(dtUserCompanies)
                        ddlCompany.SelectedValue = dtUserCompanies.Rows(0)(0)
                        ddlCompany.Enabled = False
                    Else

                        LoadddlCompanyForClientUsers(dtUserCompanies)

                    End If
                    'LoadddlCompany()
                    LoadddlAccountNumbers()
                    rvSOA.Visible = False
                    gvSOA.Visible = False
                Else
                    UIUtilities.ShowDialog(Me, "Error", "Sorry! You are not authorized", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function GetAllCompaniesTaggedWithUserByUserID(ByVal UsersID As String) As DataTable

        Dim dt As New DataTable
        Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
        Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
        Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

        qryObjICUserRolesAPNature.Select(qryObjICCompany.CompanyCode, qryObjICCompany.CompanyName)
        qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAccounts.Currency)
        qryObjICUserRolesAPNature.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
        qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.UserID = UsersID)
        qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.IsApproved = True)
        qryObjICUserRolesAPNature.es.Distinct = True
        dt = qryObjICUserRolesAPNature.LoadDataTable
        Return dt

    End Function
    Private Sub LoadddlCompanyForClientUsers(ByVal dtUserCompany As DataTable)
        Try
            Dim lit As New ListItem


            Dim dr As DataRow
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)




            For Each dr In dtUserCompany.Rows

                lit = New ListItem
                lit.Value = dr("CompanyCode")
                lit.Text = dr("CompanyName")
                ddlCompany.Items.Add(lit)

            Next

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Statement Of Account Live")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccountNumbers()
        Try
            Dim lit As New ListItem
            lit.Text = "-- Please Select --"
            lit.Value = "0"
            ddlAccountNo.Items.Clear()
            ddlAccountNo.Items.Add(lit)
            lit.Selected = True
            ddlAccountNo.AppendDataBoundItems = True
            ddlAccountNo.DataSource = ICAccountsController.GetAllAccountsTaggedWithUserByUserIDAndCompanyCode(Me.UserId.ToString, ddlCompany.SelectedValue.ToString)
            ddlAccountNo.DataTextField = "MainAccountNumber"
            ddlAccountNo.DataValueField = "AccountNumber"
            ddlAccountNo.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub ddlAccountNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAccountNo.SelectedIndexChanged
        If ddlAccountNo.SelectedValue.ToString() = "0" Then
            txtAccountCurrency.Text = ""
            txtAccountTitle.Text = ""
            txtAvailableBalance.Text = ""
            rvSOA.Visible = False
            
            btnSearch.Visible = True
        Else
            Dim AvailableBalance As Double = 0
            txtAccountCurrency.Text = ""
            txtAccountTitle.Text = ""
            txtAccountTitle.Text = ddlAccountNo.SelectedValue.ToString().Split("-")(3)
            txtAccountCurrency.Text = ddlAccountNo.SelectedValue.ToString().Split("-")(2)

            Try
                AvailableBalance = CBUtilities.BalanceInquiry(ddlAccountNo.SelectedValue.ToString.Split("-")(0), CBUtilities.AccountType.RB, "Statement of Account", ddlAccountNo.SelectedValue.ToString.Split("-")(0), ddlAccountNo.SelectedValue.ToString.Split("-")(1))
                If Not AvailableBalance = -1.0 Then
                    txtAvailableBalance.Text = AvailableBalance.ToString("N2")
                Else
                    txtAvailableBalance.Text = Nothing
                End If
            Catch ex As Exception
                txtAvailableBalance.Text = Nothing
            End Try
            rvSOA.Visible = False

            btnExportExcel.Visible = False
            btnExportPDF.Visible = False
            
            btnSearch.Visible = True
        End If
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetCompanyActiveAndApprove()
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()



        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        If Page.IsValid Then
            Try
                BindSOAGrid(True)
                Dim StrAuditTrail As String = Nothing
                Dim objICUser As New ICUser
                objICUser.LoadByPrimaryKey(Me.UserId)
                StrAuditTrail += "Statement Of Account Live is Viewed by user [ " & objICUser.UserID & " ] [ " & objICUser.UserName & " ]."
                StrAuditTrail += "For Account No [ " & ddlAccountNo.SelectedValue.ToString.Split("-")(0) & " ] , Title [ " & txtAccountTitle.Text.ToString & " ]. "
                'StrAuditTrail += " date range was [ " & raddpFromDate.SelectedDate & " ] and [ " & raddpToDate.SelectedDate & " ]  at [ " & Date.Now & " ]."
                ICUtilities.AddAuditTrail(StrAuditTrail, "Statement Of Account", "SOA", Me.UserId.ToString, Me.UserInfo.Username.ToString, "VIEW")
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
             End If
    End Sub
    Private Sub BindSOAGrid(ByVal IsDataBind As Boolean)
        Try
            ' Generate Cover Note Report and Show Print Dialog
            'If raddpFromDate.SelectedDate Is Nothing Then
            '    UIUtilities.ShowDialog(Me, "Error", "Please select from date", ICBO.IC.Dialogmessagetype.Failure)
            '    Exit Sub
            'End If
            'If raddpToDate.SelectedDate Is Nothing Then
            '    UIUtilities.ShowDialog(Me, "Error", "Please select to date", ICBO.IC.Dialogmessagetype.Failure)
            '    Exit Sub
            'End If
            'If raddpToDate.SelectedDate < raddpFromDate.SelectedDate Then
            '    UIUtilities.ShowDialog(Me, "Error", "To date should be greater than from date.", ICBO.IC.Dialogmessagetype.Failure)
            '    Exit Sub
            'End If
            'Dim FromDate, ToDate As Date
            'FromDate = Nothing
            'ToDate = Nothing
            'FromDate = raddpFromDate.SelectedDate
            'ToDate = raddpToDate.SelectedDate
            Dim dt As New DataTable
            '  CBUtilities.GetAccountStatementForRadGrid(ddlAccountNo.SelectedValue.ToString.Split("-")(0), ddlAccountNo.SelectedValue.ToString.Split("-")(1), ddlAccountNo.SelectedValue.ToString.Split("-")(2), FromDate, ToDate, ddlCompany.SelectedItem.ToString, IsDataBind, gvSOA, gvSOA.CurrentPageIndex + 1)
            GetAccountStatementForRadGrid(ddlAccountNo.SelectedValue.ToString.Split("-")(0), ddlAccountNo.SelectedValue.ToString.Split("-")(1), ddlAccountNo.SelectedValue.ToString.Split("-")(2), Nothing, Nothing, ddlCompany.SelectedItem.ToString, IsDataBind, gvSOA, gvSOA.CurrentPageIndex + 1)



            If gvSOA.Items.Count > 0 Then
                gvSOA.Visible = True
                btnExportExcel.Visible = CBool(htRights("Export Excel"))
                btnExportPDF.Visible = CBool(htRights("Export PDF"))

                lblNRF.Visible = False
            Else
                btnExportExcel.Visible = False
                btnExportPDF.Visible = False

                gvSOA.Visible = False
                lblNRF.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadddlAccountNumbers()
            txtAccountCurrency.Text = ""
            txtAccountTitle.Text = ""
            rvSOA.Visible = False
            btnExportExcel.Visible = False
            btnExportPDF.Visible = False

           
            btnSearch.Visible = True
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnExportPDF_Click(sender As Object, e As System.EventArgs) Handles btnExportPDF.Click

        Try
                Dim FromDate, ToDate As Date
                FromDate = Nothing
                ToDate = Nothing
                'FromDate = raddpFromDate.SelectedDate
                'ToDate = raddpToDate.SelectedDate
                Dim AppPath As String = Request.PhysicalApplicationPath.ToString() & "Reports\"
                Dim dt As New DataTable
                Dim settings As New System.Xml.XmlReaderSettings()
                Dim AccountStatus As String = Nothing
                settings.IgnoreWhitespace = True
                Dim xmlReader As System.Xml.XmlReader = System.Xml.XmlReader.Create(AppPath & "SOAReportLive.trdx", settings)
                Dim xmlSerializer As New Telerik.Reporting.XmlSerialization.ReportXmlSerializer()
                Dim report As Telerik.Reporting.Report = DirectCast(xmlSerializer.Deserialize(xmlReader), Telerik.Reporting.Report)
                xmlReader.Close()

                ' dt = CBUtilities.GetAccountStatement(ddlAccountNo.SelectedValue.ToString.Split("-")(0), ddlAccountNo.SelectedValue.ToString.Split("-")(1), ddlAccountNo.SelectedValue.ToString.Split("-")(2), FromDate, ToDate, ddlCompany.SelectedItem.ToString)
                Dim txtClientName As Telerik.Reporting.TextBox = TryCast(report.Items.Find("textBox33", True)(0), Telerik.Reporting.TextBox)
                Dim txtClientAddress As Telerik.Reporting.TextBox = TryCast(report.Items.Find("textBox4", True)(0), Telerik.Reporting.TextBox)


                'Dim txtFromDate2 As Telerik.Reporting.TextBox = TryCast(report.Items.Find("textBox39", True)(0), Telerik.Reporting.TextBox)
                'Dim txtToDate2 As Telerik.Reporting.TextBox = TryCast(report.Items.Find("textBox41", True)(0), Telerik.Reporting.TextBox)

                Dim txtAccountNo As Telerik.Reporting.TextBox = TryCast(report.Items.Find("textBox45", True)(0), Telerik.Reporting.TextBox)
                Dim txtAccountNo2 As Telerik.Reporting.TextBox = TryCast(report.Items.Find("textBox55", True)(0), Telerik.Reporting.TextBox)

                Dim txtCurrency As Telerik.Reporting.TextBox = TryCast(report.Items.Find("textBox40", True)(0), Telerik.Reporting.TextBox)
                'Dim txtAccountStatus As Telerik.Reporting.TextBox = TryCast(report.Items.Find("textBox42", True)(0), Telerik.Reporting.TextBox)

                'Dim txtAccountType As Telerik.Reporting.TextBox = TryCast(report.Items.Find("textBox43", True)(0), Telerik.Reporting.TextBox)
                'Dim txtAccountType2 As Telerik.Reporting.TextBox = TryCast(report.Items.Find("textBox57", True)(0), Telerik.Reporting.TextBox)

                'Dim txtAccountOpeningDate As Telerik.Reporting.TextBox = TryCast(report.Items.Find("textBox63", True)(0), Telerik.Reporting.TextBox)
                Dim txtAccountBranchName As Telerik.Reporting.TextBox = TryCast(report.Items.Find("textBox36", True)(0), Telerik.Reporting.TextBox)

                dt = GetAccountStatement(ddlAccountNo.SelectedValue.ToString.Split("-")(0), ddlAccountNo.SelectedValue.ToString.Split("-")(1), ddlAccountNo.SelectedValue.ToString.Split("-")(2), FromDate, ToDate, ddlCompany.SelectedValue.ToString, txtAccountTitle.Text)

                If dt.Rows.Count > 0 Then

                    txtClientName.Value = dt.Rows(0)("ClientName")
                    txtClientAddress.Value = dt.Rows(0)("Address")
                    'txtFromDate2.Value = CDate(dt.Rows(0)("FromDate")).ToString("dd-MMM-yyyy")
                    'txtToDate2.Value = CDate(dt.Rows(0)("ToDate")).ToString("dd-MMM-yyyy")
                    txtAccountNo.Value = ddlAccountNo.SelectedValue.ToString.Split("-")(0)
                    txtAccountNo2.Value = ddlAccountNo.SelectedValue.ToString.Split("-")(0)
                    txtCurrency.Value = ddlAccountNo.SelectedValue.ToString.Split("-")(2)
                    'txtAccountStatus.Value = dt.Rows(0)("ACCOUNT_Status")
                    txtAccountBranchName.Value = GetBranchNameByBranchCode(dt.Rows(0)("Unit_ID"))
                    'txtAccountType.Value = dt.Rows(0)("account_type")
                    'txtAccountType2.Value = dt.Rows(0)("account_type")
                    'txtAccountOpeningDate.Value = dt.Rows(0)("AccountOpeningDate")

                    report.DataSource = dt
                    rvSOA.ShowExportGroup = False
                    rvSOA.ReportSource = report
                    rvSOA.RefreshReport()
                    'lblNRF.Visible = False
                    '  rvSOA.Visible = True
                    rvSOA.Visible = False
                    Dim StrAuditTrail As String = Nothing
                    Dim objICUser As New ICUser
                    objICUser.LoadByPrimaryKey(Me.UserId)
                    StrAuditTrail += "Statement Of Account Live is Exported in PDF by user [ " & objICUser.UserID & " ] [ " & objICUser.UserName & " ]."
                    StrAuditTrail += "For Account No [ " & ddlAccountNo.SelectedValue.ToString.Split("-")(0) & " ] , Title [ " & txtAccountTitle.Text.ToString & " ]. "
                    'StrAuditTrail += " date range was [ " & raddpFromDate.SelectedDate & " ] and [ " & raddpToDate.SelectedDate & " ]  at [ " & Date.Now & " ]."
                    ICUtilities.AddAuditTrail(StrAuditTrail, "Statement Of Account", "SOA", Me.UserId.ToString, Me.UserInfo.Username.ToString, "EXPORT PDF")
                Else
                    rvSOA.Visible = False

                    '  lblNRF.Visible = True
                End If



                Dim reportProcessor As New Telerik.Reporting.Processing.ReportProcessor()
                Dim instanceReportSource As New Telerik.Reporting.InstanceReportSource()
                instanceReportSource.ReportDocument = rvSOA.Report
                Dim result As Telerik.Reporting.Processing.RenderingResult = reportProcessor.RenderReport("PDF", instanceReportSource, Nothing)

                Dim fileName As String = result.DocumentName & "." & result.Extension
                Response.Clear()
                Response.ContentType = result.MimeType
                Response.Cache.SetCacheability(HttpCacheability.Private)
                Response.Expires = -1
                Response.Buffer = True
                Response.AddHeader("Content-Disposition", String.Format("{0};FileName=""{1}""", "attachment", fileName))
                Response.BinaryWrite(result.DocumentBytes)

                Response.End()





            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub
    Private Function GetBranchNameByBranchCode(ByVal BranchCode As String) As String
        Dim objICOfficeColl As New ICOfficeCollection

        objICOfficeColl.Query.Where(objICOfficeColl.Query.OfficeCode = BranchCode)
        If objICOfficeColl.Query.Load Then
            Return objICOfficeColl(0).OfficeName
        Else
            Return "N/A"
        End If

    End Function
    Protected Sub btnExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportExcel.Click

        Try

                gvSOA.ExportSettings.ExportOnlyData = True
                gvSOA.ExportSettings.FileName = "SOA Live - " & ddlAccountNo.SelectedItem.Text.ToString()
                gvSOA.ExportSettings.IgnorePaging = True
                gvSOA.ExportSettings.OpenInNewWindow = True
                gvSOA.MasterTableView.ExportToExcel()

                Dim StrAuditTrail As String = Nothing
                Dim objICUser As New ICUser
                objICUser.LoadByPrimaryKey(Me.UserId)
                StrAuditTrail += "Statement Of Account Live is Exported in Excel by user [ " & objICUser.UserID & " ] [ " & objICUser.UserName & " ]."
                StrAuditTrail += "For Account No [ " & ddlAccountNo.SelectedValue.ToString.Split("-")(0) & " ] , Title [ " & txtAccountTitle.Text.ToString & " ]. "
                'StrAuditTrail += " date range was [ " & raddpFromDate.SelectedDate & " ] and [ " & raddpToDate.SelectedDate & " ]  at [ " & Date.Now & " ]."
                ICUtilities.AddAuditTrail(StrAuditTrail, "Statement Of Account", "SOA", Me.UserId.ToString, Me.UserInfo.Username.ToString, "EXPORT EXCEL")

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub

    Protected Sub gvSOA_ExportCellFormatting(ByVal sender As Object, ByVal e As Telerik.Web.UI.ExportCellFormattingEventArgs) Handles gvSOA.ExportCellFormatting
        If e.FormattedColumn.UniqueName.ToString() = "AccountNumber" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If
        If e.FormattedColumn.UniqueName.ToString() = "TransferAccount" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If
        If e.FormattedColumn.UniqueName.ToString() = "Withdrawl" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If
        If e.FormattedColumn.UniqueName.ToString() = "Deposit" Then
            e.Cell.Style("mso-number-format") = "\@"
        End If

    End Sub

    Protected Sub gvSOA_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvSOA.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvSOA.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub



    Protected Sub gvSOA_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvSOA.NeedDataSource
        Try
            BindSOAGrid(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvSOA_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvSOA.PageIndexChanged
        Try
            gvSOA.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    
    Private Function GetMT940TempDataTable() As DataTable
        Try
            Dim dtMT940 As New DataTable

            Return dtMT940
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#Region "BL CODE BY AIZAZ 03-Apr-2014"
    
    Public Shared Sub GetAccountStatementForRadGrid(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal FromDate As Date, ByVal ToDate As Date, ByVal ClientName As String, ByVal DoDataBind As Boolean, ByVal rg As RadGrid, ByVal PageNumber As Integer)
        Dim RunningBalance As Double = 0
        Dim OpeningBalance As Double = 0
        Dim AvailableBalance As Double = 0
        Dim tempOpenBalance As Double = 0
        Dim ClosingBalance As String = ""
        Dim InstrumentNo As String = Nothing
        Dim LastRow As Integer = 0
        Dim dtAccountStatement As New DataTable
        Dim drAccountStatement As DataRow
        Dim cnt As Integer = 0
        Dim ProductCode, SchemeCode As String
        Dim dtSoneriTrnas As New DataTable
        Dim MBMNo As String = Nothing
        Dim collDR As DataRow()
        Dim dtFinal As New DataTable
        Dim drFinal As DataRow

        Try


            dtAccountStatement.Columns.Add(New DataColumn("Date", GetType(System.DateTime)))

            dtAccountStatement.Columns.Add(New DataColumn("TransactionDetails", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("VoucherChequeNo", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Withdrawl", GetType(System.Double)))
            dtAccountStatement.Columns.Add(New DataColumn("Deposit", GetType(System.Double)))
            dtAccountStatement.Columns.Add(New DataColumn("RunningBalance", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("ClientName", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("OpeningBalance", GetType(System.Double)))
            dtAccountStatement.Columns.Add(New DataColumn("AvailableBalance", GetType(System.Double)))
            dtAccountStatement.Columns.Add(New DataColumn("StatementDate", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("FromDate", GetType(System.DateTime)))
            dtAccountStatement.Columns.Add(New DataColumn("ToDate", GetType(System.DateTime)))
            dtAccountStatement.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Currency", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("ClosingBalance", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Unit_ID", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("account_unit_id", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("ValueDate", GetType(System.DateTime)))
            'dtSoneriTrnas = CBUtilities.GetFunctionOFIRIS(BranchCode, ProductCode, SchemeCode, MBMNo, FromDate, Now.Date, "1", 0, 0, "")

            ' Get Current Date's transactions and develop opening balance of current date

            dtSoneriTrnas = New DataTable()
            'dtSoneriTrnas = GetFunctionOFIRIS(BranchCode, ProductCode, SchemeCode, MBMNo, Now.Date, Now.Date, "1", 0, 0, "")
            dtSoneriTrnas = GetFunctionOFIRISForDetailSOA(BranchCode, ProductCode, SchemeCode, AccountNumber, FromDate.Date, ToDate.Date, "1", 0, 0, "")
            If Not dtSoneriTrnas Is Nothing Then
                If dtSoneriTrnas.Rows.Count > 0 Then

                    'OpeningBalance = dtSoneriTrnas.Rows(0)("Balance")
                    ' RunningBalance = OpeningBalance
                    For Each drSoneriTran In dtSoneriTrnas.Rows
                        drAccountStatement = dtAccountStatement.NewRow()
                        cnt = cnt + 1
                        
                            drAccountStatement("Date") = CDate(drSoneriTran("post_date").ToString()).ToString("MM/dd/yyyy")

                            InstrumentNo = Nothing
                            InstrumentNo = drSoneriTran("instrument_no").ToString()
                            'drAccountStatement("TransactionDetails") = GetCustomizedSOADetailByNarration(drSoneriTran("NARRATIONS").ToString(), drSoneriTran("PSY_SYSTCODE").ToString(), InstrumentNo, drSoneriTran("DRCR").ToString())
                            drAccountStatement("TransactionDetails") = drSoneriTran("Narration").ToString()
                            drAccountStatement("VoucherChequeNo") = InstrumentNo
                            drAccountStatement("Unit_ID") = drSoneriTran("Unit_ID").ToString()
                            drAccountStatement("account_unit_id") = drSoneriTran("acc_unit_id").ToString()
                            drAccountStatement("ValueDate") = CDate(drSoneriTran("value_date").ToString()).ToString("MM/dd/yyyy")
                            If drSoneriTran("Amount").ToString().Contains("-") = True Then
                            'RunningBalance = RunningBalance - CDbl(drSoneriTran("Amount").ToString().Replace("-", ""))

                                drAccountStatement("Deposit") = 0
                                drAccountStatement("Withdrawl") = CDbl(drSoneriTran("Amount").ToString().Replace("-", ""))
                            'drAccountStatement("RunningBalance") = RunningBalance.ToString("N2")
                            'If drAccountStatement("RunningBalance").ToString.Contains("-") Then
                            '    drAccountStatement("RunningBalance") = drAccountStatement("RunningBalance").ToString.Replace("-", "") & " " & "DR"
                            'Else
                            '    drAccountStatement("RunningBalance") = drAccountStatement("RunningBalance").ToString & " " & "CR"
                            'End If

                            Else
                            'RunningBalance = RunningBalance + CDbl(drSoneriTran("Amount").ToString())

                                drAccountStatement("Withdrawl") = 0
                            drAccountStatement("Deposit") = CDbl(drSoneriTran("Amount").ToString().Replace("-", ""))
                            'drAccountStatement("RunningBalance") = RunningBalance.ToString("N2")
                            'If drAccountStatement("RunningBalance").ToString.Contains("-") Then
                            '    drAccountStatement("RunningBalance") = drAccountStatement("RunningBalance").ToString.Replace("-", "") & " " & "DR"
                            'Else
                            '    drAccountStatement("RunningBalance") = drAccountStatement("RunningBalance").ToString & " " & "CR"
                            'End If
                            End If
                            drAccountStatement("ClientName") = ClientName.ToString()
                            drAccountStatement("StatementDate") = Date.Now.ToString("dd-MMM-yyyy")
                            drAccountStatement("FromDate") = FromDate
                            drAccountStatement("ToDate") = ToDate
                            drAccountStatement("ClosingBalance") = 0
                        'If cnt = dtSoneriTrnas.Rows.Count Then
                        '    ClosingBalance = drAccountStatement("RunningBalance")
                        'End If

                            dtAccountStatement.Rows.Add(drAccountStatement)
                    Next


                    'For Each drFinal In dtAccountStatement.Rows
                    '    drFinal("ClosingBalance") = ClosingBalance
                    'Next


                End If
            End If


            If Not PageNumber = 0 Then

                rg.DataSource = dtAccountStatement

                If DoDataBind Then
                    rg.DataBind()
                End If

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message.ToString)
        End Try
    End Sub
    Public Shared Function GetCustomizedSOADetailByNarration(ByVal Narration As String, ByVal VoucherType As String, ByRef InstrumentNo As String, ByVal DRCR As String) As String
        Try
            Dim CustMessage As String = Nothing
            Dim ArraySplitedNarration As String()
            Dim InstructionID As String = Nothing
            Dim objICInstruction As ICInstruction
            Dim objICInstructionColl As ICInstructionCollection
            Dim objICSweepActionInstruction As ICSweepActionInstructions
            Dim objICOffice As New ICOffice
            Dim objICBank As New ICBank
            If VoucherType = ICUtilities.GetSettingValue("SystemCode").ToString Then

                If Narration.Contains(":") = True Then
                    ArraySplitedNarration = Narration.Split(":")
                    If ArraySplitedNarration(0).Split(" ")(0).ToString = "Instruction" Then
                        objICInstruction = New ICInstruction
                        If objICInstruction.LoadByPrimaryKey(ArraySplitedNarration(0).Split(" ")(1).ToString) Then
                            CustMessage += "ST-"
                            CustMessage += objICInstruction.ProductTypeCode & "-"
                            CustMessage += "TRAN REF NO:" & objICInstruction.InstructionID & "-"

                            If DRCR = "CR" Then
                                If objICInstruction.ClubID IsNot Nothing Then
                                    CustMessage += "ClubbedInstruction No " & objICInstruction.ClubID & " -"
                                End If
                            End If
                            CustMessage += objICInstruction.BeneficiaryName & "-"
                            If objICInstruction.PaymentMode = "DD" Then
                                If objICOffice.LoadByPrimaryKey(objICInstruction.DrawnOnBranchCode) Then
                                    If objICBank.LoadByPrimaryKey(objICOffice.BankCode) Then
                                        CustMessage += objICBank.BankName & "-"
                                    End If
                                End If
                            End If
                            If objICInstruction.FileBatchNo IsNot Nothing And objICInstruction.FileBatchNo <> "" And objICInstruction.FileBatchNo <> "SI" Then
                                CustMessage += "File Batch No - " & objICInstruction.FileBatchNo & "-"
                            End If
                            If objICInstruction.PaymentMode = "Direct Credit" Or objICInstruction.PaymentMode = "Other Credit" Or objICInstruction.PaymentMode = "COTC" Then
                                InstrumentNo = Nothing
                                InstrumentNo = objICInstruction.ProductTypeCode
                            ElseIf objICInstruction.PaymentMode = "PO" Or objICInstruction.PaymentMode = "DD" Then
                                InstrumentNo = Nothing
                                InstrumentNo += objICInstruction.ProductTypeCode & " " & objICInstruction.InstrumentNo
                            End If
                            CustMessage = CustMessage.Remove(CustMessage.Length - 1, 1)
                        Else
                            CustMessage = Narration
                        End If

                    ElseIf ArraySplitedNarration(0).Split(" ")(0).ToString = "ClubedInstruction" Then
                        objICInstructionColl = New ICInstructionCollection
                        objICInstructionColl.Query.Where(objICInstructionColl.Query.ClubID = ArraySplitedNarration(0).Split(" ")(1).ToString)
                        If objICInstructionColl.Query.Load Then
                            CustMessage += "ST-"
                            objICInstruction = New ICInstruction
                            objICInstruction = objICInstructionColl(0)
                            CustMessage += ArraySplitedNarration(0).Split(" ")(0) & " " & ArraySplitedNarration(0).Split(" ")(1).ToString & "-"
                            CustMessage += objICInstruction.ProductTypeCode & "-"

                            'If objICInstruction.PaymentMode = "Direct Credit" Or objICInstruction.PaymentMode = "Other Credit" Or objICInstruction.PaymentMode = "COTC" Then
                            InstrumentNo = Nothing
                            InstrumentNo = objICInstruction.ProductTypeCode
                            'End If
                            CustMessage = CustMessage.Remove(CustMessage.Length - 1, 1)
                        Else
                            CustMessage = Narration
                        End If
                    ElseIf ArraySplitedNarration(0).Split(" ")(0).ToString = "Sweep" Then
                        objICSweepActionInstruction = New ICSweepActionInstructions
                        objICSweepActionInstruction.LoadByPrimaryKey(ArraySplitedNarration(0).Split(" ")(3).ToString)
                        CustMessage += "ST-"
                        InstrumentNo = Nothing
                        InstrumentNo = ArraySplitedNarration(0).Split(" ")(0).ToString
                        CustMessage += Narration & "-" & " Action ID " & objICSweepActionInstruction.SweepActionID & "-"
                        CustMessage = CustMessage.Remove(CustMessage.Length - 1, 1)
                    Else
                        CustMessage = Narration
                    End If
                Else
                    CustMessage = Narration

                End If
            Else
                CustMessage = Narration
            End If
            Return CustMessage
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    'Public Shared Function GetFunctionOFIRISForDetailSOA(ByVal BranchCode As String, ByVal p As String, ByVal s As String, ByVal m As String, ByVal dateStart As String, ByVal dateEnd As String, ByVal opt As Char, ByVal amt_range_from As Decimal, ByVal amt_range_to As Decimal, ByVal Trans_Desc As String) As DataTable
    '    Try
    '        Dim FilePath As String = ICUtilities.GetSettingValue("PhysicalApplicationPath") & "UploadedFiles/mini.xlsx"
    '        Dim connectionString As String = ""

    '        connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FilePath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""

    '        Dim con As New OleDbConnection(connectionString)
    '        Dim cmd As New OleDbCommand()
    '        cmd.CommandType = System.Data.CommandType.Text
    '        cmd.Connection = con
    '        Dim dAdapter As New OleDbDataAdapter(cmd)
    '        Dim dtExcelRecords As New DataTable()
    '        con.Open()
    '        Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
    '        Dim getExcelSheetName As String = "mini$"
    '        cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
    '        dAdapter.SelectCommand = cmd
    '        dAdapter.Fill(dtExcelRecords)
    '        con.Close()



    '        Return dtExcelRecords

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    Public Shared Function GetFunctionOFIRISForDetailSOA(ByVal BranchCode As String, ByVal p As String, ByVal s As String, ByVal m As String, ByVal dateStart As String, ByVal dateEnd As String, ByVal opt As Char, ByVal amt_range_from As Decimal, ByVal amt_range_to As Decimal, ByVal Trans_Desc As String) As DataTable
        Try
            Dim dt As New DataTable
            Dim constring As String = ConfigurationManager.ConnectionStrings("CBS").ConnectionString.ToString()
            Dim Consql As New SqlConnection(constring)
            Dim da As New SqlDataAdapter
            If Consql.State = ConnectionState.Closed Then
                Consql.Open()
            Else
                Consql.Close()
            End If

            Dim cmd As New SqlCommand("dbo.sp_mini_state_cm", Consql)
            'cmd.CommandText = SqlDataSourceCommandType.StoredProcedure
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@aUnit_ID", BranchCode)
            cmd.Parameters.AddWithValue("@aAccount_No", m)
            'cmd.Parameters.AddWithValue("@from_date", CDate(dateStart).ToString("yyyyMMdd"))
            'cmd.Parameters.AddWithValue("@to_date", CDate(dateEnd).ToString("yyyyMMdd"))

            da.SelectCommand = cmd
            da.Fill(dt)
            Dim st As String = ""

            'While (sqldr.Read)
            '    st = sqldr(0)
            'End While
            cmd.ExecuteReader()

            If Consql.State = ConnectionState.Open Then
                Consql.Close()
            End If
            Return dt

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function GetFunctionOFIRIS(ByVal BranchCode As String, ByVal p As String, ByVal s As String, ByVal m As String, ByVal dateStart As String, ByVal dateEnd As String, ByVal opt As Char, ByVal amt_range_from As Decimal, ByVal amt_range_to As Decimal, ByVal Trans_Desc As String) As DataTable
        Try
            Dim dt As New DataTable
            Dim constring As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString.ToString()
            Dim Consql As New SqlConnection(constring)
            Dim da As New SqlDataAdapter
            If Consql.State = ConnectionState.Closed Then
                Consql.Open()
            Else
                Consql.Close()
            End If

            Dim cmd As New SqlCommand("dbo.SP_CMS_CASH_MANAGEMENT_SOA", Consql)
            'cmd.CommandText = SqlDataSourceCommandType.StoredProcedure
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@Branch_Code", BranchCode)
            cmd.Parameters.AddWithValue("@p", p)
            cmd.Parameters.AddWithValue("@s", s)
            cmd.Parameters.AddWithValue("@m", m)
            cmd.Parameters.AddWithValue("@dateStart", dateStart)
            cmd.Parameters.AddWithValue("@dateEnd", dateEnd)
            cmd.Parameters.AddWithValue("@opt", opt)
            cmd.Parameters.AddWithValue("@Amt_Range_From", amt_range_from)
            cmd.Parameters.AddWithValue("@Amt_Range_to", amt_range_to)
            cmd.Parameters.AddWithValue("@Trans_Desc", Trans_Desc.ToString())

            da.SelectCommand = cmd
            da.Fill(dt)
            Dim st As String = ""

            'While (sqldr.Read)
            '    st = sqldr(0)
            'End While
            cmd.ExecuteReader()

            If Consql.State = ConnectionState.Open Then
                Consql.Close()


            End If
            Return dt
        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Shared Function GetAccountStatement(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal FromDate As Date, ByVal ToDate As Date, ByVal ClientCode As String, ByVal Title As String) As DataTable
        Dim RunningBalance As Double = 0
        Dim OpeningBalance As Double = 0
        Dim AvailableBalance As Double = 0
        Dim tempOpenBalance As Double = 0
        Dim ClosingBalance As String = ""
        Dim LastRow As Integer = 0
        Dim dtAccountStatement As New DataTable
        Dim drAccountStatement As DataRow
        Dim cnt As Integer = 0
        Dim dtSoneriTrnas As New DataTable
        Dim MBMNo As String = Nothing
        Dim DebitCount As Integer = 0
        Dim CreditCount As Integer = 0
        Dim TotalDebitAmount As Double = 0
        Dim TotalCreditAmount As Double = 0
        Dim InstrumentNo As String = ""
        Dim dtFinal As New DataTable
        Dim drFinal As DataRow

        Dim objICCompany As New ICCompany
        objICCompany.LoadByPrimaryKey(ClientCode)

        Try
            dtAccountStatement.Columns.Add(New DataColumn("Date", GetType(System.DateTime)))
            dtAccountStatement.Columns.Add(New DataColumn("TransactionDetails", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("VoucherChequeNo", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Withdrawl", GetType(System.Double)))
            dtAccountStatement.Columns.Add(New DataColumn("Deposit", GetType(System.Double)))
            dtAccountStatement.Columns.Add(New DataColumn("RunningBalance", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("ClientName", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Address", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("StatementDate", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("FromDate", GetType(System.DateTime)))
            dtAccountStatement.Columns.Add(New DataColumn("ToDate", GetType(System.DateTime)))
            dtAccountStatement.Columns.Add(New DataColumn("AccountOpeningDate", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Currency", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("TotalDebitAmount", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("TotalCreditAmount", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("DebitCount", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("CreditCount", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("ClosingBalance", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Unit_ID", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("account_unit_id", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("Title", GetType(System.String)))
            dtAccountStatement.Columns.Add(New DataColumn("ValueDate", GetType(System.DateTime)))

            dtSoneriTrnas = GetFunctionOFIRISForDetailSOA(BranchCode, "", "", AccountNumber, FromDate.Date, ToDate.Date, "1", 0, 0, "")


            If Not dtSoneriTrnas Is Nothing Then
                If dtSoneriTrnas.Rows.Count > 0 Then

                    'OpeningBalance = dtSoneriTrnas.Rows(0)("Balance")
                    'RunningBalance = OpeningBalance
                    For Each drSoneriTran In dtSoneriTrnas.Rows
                        drAccountStatement = dtAccountStatement.NewRow()
                        cnt = cnt + 1
                      

                            drAccountStatement("Date") = CDate(drSoneriTran("post_date").ToString()).ToString("MM/dd/yyyy")
                            InstrumentNo = Nothing
                            InstrumentNo = drSoneriTran("instrument_no").ToString()
                            'drAccountStatement("TransactionDetails") = GetCustomizedSOADetailByNarration(drSoneriTran("NARRATIONS").ToString(), drSoneriTran("PSY_SYSTCODE").ToString(), InstrumentNo, drSoneriTran("DRCR").ToString())
                            drAccountStatement("TransactionDetails") = drSoneriTran("Narration").ToString()
                            drAccountStatement("VoucherChequeNo") = InstrumentNo
                            drAccountStatement("AccountOpeningDate") = CDate(drSoneriTran("post_date")).ToString("dd-MMM-yyyy")
                            drAccountStatement("Unit_ID") = drSoneriTran("Unit_ID").ToString()
                            drAccountStatement("account_unit_id") = drSoneriTran("acc_unit_id").ToString()
                            drAccountStatement("ValueDate") = CDate(drSoneriTran("value_date").ToString()).ToString("MM/dd/yyyy")
                            drAccountStatement("Title") = Title
                            If drSoneriTran("Amount").ToString().Contains("-") = True Then

                                TotalDebitAmount = TotalDebitAmount + CDbl(drSoneriTran("Amount").ToString().Replace("-", ""))
                                DebitCount = DebitCount + 1
                                drAccountStatement("Deposit") = 0
                                drAccountStatement("Withdrawl") = CDbl(drSoneriTran("Amount").ToString().Replace("-", ""))
                            'drAccountStatement("RunningBalance") = RunningBalance.ToString("N2")
                            'If drAccountStatement("RunningBalance").ToString.Contains("-") Then
                            '    drAccountStatement("RunningBalance") = drAccountStatement("RunningBalance").ToString.Replace("-", "") & " " & "DR"
                            'Else
                            '    drAccountStatement("RunningBalance") = drAccountStatement("RunningBalance").ToString & " " & "CR"
                            'End If

                            Else

                                TotalCreditAmount = TotalCreditAmount + CDbl(drSoneriTran("Amount").ToString().Replace("-", ""))
                                CreditCount = CreditCount + 1
                                drAccountStatement("Withdrawl") = 0
                                drAccountStatement("Deposit") = CDbl(drSoneriTran("Amount").ToString().Replace("-", ""))
                            'drAccountStatement("RunningBalance") = RunningBalance.ToString("N2")
                            'If drAccountStatement("RunningBalance").ToString.Contains("-") Then
                            '    drAccountStatement("RunningBalance") = drAccountStatement("RunningBalance").ToString.Replace("-", "") & " " & "DR"
                            'Else
                            '    drAccountStatement("RunningBalance") = drAccountStatement("RunningBalance").ToString & " " & "CR"
                            'End If
                            End If
                            drAccountStatement("ClientName") = objICCompany.CompanyName
                            drAccountStatement("Address") = objICCompany.Address
                            drAccountStatement("StatementDate") = Date.Now.ToString("dd-MMM-yyyy")
                            drAccountStatement("FromDate") = FromDate.ToString("dd-MMM-yyyy")
                            drAccountStatement("ToDate") = ToDate.ToString("dd-MMM-yyyy")
                            'drAccountStatement("ClosingBalance") = 0
                            drAccountStatement("AccountNumber") = AccountNumber
                        'If cnt = dtSoneriTrnas.Rows.Count Then
                        '    ClosingBalance = drAccountStatement("RunningBalance").ToString()
                        'End If

                            dtAccountStatement.Rows.Add(drAccountStatement)
                    Next

                    For Each drFinal In dtAccountStatement.Rows
                        'drFinal("ClosingBalance") = ClosingBalance
                        drFinal("DebitCount") = DebitCount.ToString
                        drFinal("CreditCount") = CreditCount.ToString
                        drFinal("TotalDebitAmount") = TotalDebitAmount.ToString("N2")
                        drFinal("TotalCreditAmount") = TotalCreditAmount.ToString("N2")
                    Next


                End If
            End If

            Return dtAccountStatement
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function

    Private Function GetLogId(ByVal entitytype As String, ByVal relatedid As String, ByVal fromaccountnumber As String, ByVal fromaccounttype As String, ByVal toaccountnumber As String, ByVal toaccounttype As String, ByVal frombankimd As String, ByVal tobankimd As String, ByVal transactionamount As String, ByVal messagetype As String) As Integer
        Dim log As New ICCBLog
        log.es.Connection.CommandTimeout = 3600

        log.LogDateTime = Now
        log.EntityType = entitytype
        log.RelatedId = relatedid
        log.Status = "Pending"

        If Not fromaccountnumber = "" Then
            log.Fromaccountnumber = fromaccountnumber
        End If
        If Not fromaccounttype = "" Then
            log.Fromaccounttype = fromaccounttype
        End If
        If Not toaccountnumber = "" Then
            log.Toaccountnumber = toaccountnumber
        End If
        If Not toaccounttype = "" Then
            log.Toaccounttype = toaccounttype
        End If
        If Not frombankimd = "" Then
            log.FrombankIMD = frombankimd
        End If
        If Not tobankimd = "" Then
            log.TobankIMD = tobankimd
        End If
        If Not transactionamount = "" Then
            log.Transactionamount = transactionamount
        End If
        log.MessageType = messagetype.ToString
        log.Save()
        Return log.LogId
    End Function
    Private Shared Sub UpdateCBLog(ByVal logid As Integer, ByVal status As String, ByVal errordesc As String)
        Dim log As New ICCBLog
        log.es.Connection.CommandTimeout = 3600
        log.LoadByPrimaryKey(logid)
        If Not status = "" Then
            log.Status = status
        End If
        If Not errordesc = "" Then
            log.ErrorDesc = errordesc
        End If
        log.Save()
    End Sub
    Private Function GetCustomerSwiftCodeVIAAccountNo(ByVal AccountNo As String, ByVal BranchCode As String, ByVal Currency As String) As String
        Dim objICAccounts As New ICAccounts

        objICAccounts.Query.Where(objICAccounts.Query.AccountNumber = AccountNo And objICAccounts.Query.BranchCode = BranchCode And objICAccounts.Query.Currency = Currency)
        If objICAccounts.Query.Load Then

            If objICAccounts.CustomerSwiftCode IsNot Nothing Then
                Return objICAccounts.CustomerSwiftCode
            Else
                Return "-"
            End If
        Else
            Return "-"
        End If
    End Function
    
    Public Shared Function RemoveSpecialCharactersFromString(ByVal Data As String) As String
        Data = Data.Replace(".", "")
        Data = Data.Replace("-", "")
        Data = Data.Replace("~", "")
        Data = Data.Replace(":", "")
        Data = Data.Replace("(", "")
        Data = Data.Replace(")", "")
        Data = Data.Replace("%", "")
        Data = Data.Replace("/", "")
        Data = Data.Replace("\", "")
        Data = Data.Replace("]", "")
        Data = Data.Replace("[", "")
        Return Data
    End Function

    
    
    

    
    
#End Region
End Class

