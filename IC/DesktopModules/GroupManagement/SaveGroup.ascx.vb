﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals

Partial Class DesktopModules_GroupManagement_SaveGroup
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private GroupCode As String
    Private htRights As Hashtable

#Region "Page Load"
    Protected Sub DesktopModules_GroupManagement_SaveGroup_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            GroupCode = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing

               
                If GroupCode.ToString() = "0" Then
                    Clear()
                    lblPageHeader.Text = "Add Group"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    lblPageHeader.Text = "Edit Group"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    Dim ICGroup As New ICGroup
                    ICGroup.es.Connection.CommandTimeout = 3600
                    ICGroup.LoadByPrimaryKey(GroupCode)
                    If ICGroup.LoadByPrimaryKey(GroupCode) Then
                       txtGroupName.Text = ICGroup.GroupName.ToString()
                        chkActive.Checked = ICGroup.IsActive
                    End If
                End If
           End If

            If GroupCode.ToString <> "0" Then
                Dim ICGroup As New ICGroup
                ICGroup.es.Connection.CommandTimeout = 3600
                ICGroup.LoadByPrimaryKey(GroupCode)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Group Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        txtGroupName.Text = ""
        chkActive.Checked = True
    End Sub

#End Region

#Region "Buttons"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then

            Try
                Dim ICGroup As New ICGroup
                ICGroup.es.Connection.CommandTimeout = 3600

                If GroupCode.ToString() <> "0" Then
                    ICGroup.LoadByPrimaryKey(GroupCode)
                End If

                ICGroup.GroupName = txtGroupName.Text.ToString()
                ICGroup.IsActive = chkActive.Checked

                If GroupCode = "0" Then
                    ICGroup.CreatedBy = Me.UserId
                    ICGroup.CreatedDate = Date.Now
                    ICGroup.Creater = Me.UserId
                    ICGroup.CreationDate = Date.Now
                    If CheckDuplicate(ICGroup, False) = False Then
                        ICGroupController.AddGroup(ICGroup, False, Me.UserId, Me.UserInfo.Username)
                        UIUtilities.ShowDialog(Me, "Save Group", "Group added successfully.", ICBO.IC.Dialogmessagetype.Success)
                        Clear()
                    Else
                        UIUtilities.ShowDialog(Me, "Save Group", "Can not add duplicate Group.", ICBO.IC.Dialogmessagetype.Failure)
                    End If
                Else
                    ICGroup.CreatedBy = Me.UserId
                    ICGroup.CreatedDate = Date.Now
                    If CheckDuplicate(ICGroup, True) = False Then
                        ICGroupController.AddGroup(ICGroup, True, Me.UserId, Me.UserInfo.Username)
                        UIUtilities.ShowDialog(Me, "Save Group", "Group updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Clear()
                    Else
                        UIUtilities.ShowDialog(Me, "Save Group", "Can not add duplicate Group.", ICBO.IC.Dialogmessagetype.Failure)
                    End If
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try

        End If
    End Sub

    Private Function CheckDuplicate(ByVal CGroup As ICGroup, ByVal isUpdate As Boolean) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim ICGroup As New ICGroup
            Dim collGroup As New ICGroupCollection

            ICGroup.es.Connection.CommandTimeout = 3600
            collGroup.es.Connection.CommandTimeout = 3600

            collGroup.Query.Where(collGroup.Query.GroupName.ToLower.Trim = CGroup.GroupName.ToLower.Trim)

            If isUpdate Then
                collGroup.Query.Where(collGroup.Query.GroupCode.ToLower.Trim <> CGroup.GroupCode)
            End If

            If collGroup.Query.Load() Then
                rslt = True
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

#End Region

End Class
