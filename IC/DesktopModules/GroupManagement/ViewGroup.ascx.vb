﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_GroupManagement_ViewGroup
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Dim dtDa As DataTable
    Private htRights As Hashtable

#Region "Page Load"
    Protected Sub DesktopModules_GroupManagement_ViewGroup_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing
                btnSaveGroup.Visible = CBool(htRights("Add"))
                btnDeleteGroups.Visible = CBool(htRights("Delete"))
                LoadGroupgv(True)
                ViewState("SortExp") = Nothing
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Group Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Grid View"
    Private Sub LoadGroupgv(ByVal IsBind As Boolean)
        Try
            ICGroupController.GetGroupgv(gvGroup.CurrentPageIndex + 1, gvGroup.PageSize, gvGroup, IsBind)

            If gvGroup.Items.Count > 0 Then
                gvGroup.Visible = True
                lblRNF.Visible = False
                btnDeleteGroups.Visible = CBool(htRights("Delete"))
                gvGroup.Columns(5).Visible = CBool(htRights("Delete"))
            Else
                gvGroup.Visible = False
                btnDeleteGroups.Visible = False
                lblRNF.Visible = True
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Protected Sub gvGroup_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvGroup.NeedDataSource
        LoadGroupgv(False)
    End Sub

    Protected Sub gvGroup_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvGroup.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvGroup.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub gvGroup_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvGroup.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvGroup.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvGroup_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvGroup.ItemCommand
        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    Dim ICGroup As New ICGroup

                    ICGroup.es.Connection.CommandTimeout = 3600
                    If ICGroup.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                        ICGroupController.DeleteGroup(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        UIUtilities.ShowDialog(Me, "Delete Group", "Group deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                        LoadGroupgv(True)
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Deleted Group", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvGroup_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvGroup.PageIndexChanged
        gvGroup.CurrentPageIndex = e.NewPageIndex
    End Sub

#End Region

#Region "Buttons"

    Protected Sub btnSaveBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveGroup.Click
        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveGroup", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If
    End Sub

    Protected Sub btnDeleteGroups_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteGroups.Click
        If Page.IsValid Then
            Try
                Dim rowgvGroup As GridDataItem
                Dim chkSelect As CheckBox
                Dim GroupCode As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0

                If CheckgvGroupForProcessAll() = True Then
                    For Each rowgvGroup In gvGroup.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvGroup.Cells(0).FindControl("chkSelectAll"), CheckBox)
                        If chkSelect.Checked = True Then
                            GroupCode = rowgvGroup("GroupCode").Text.ToString()
                            Dim icGroup As New ICGroup
                            icGroup.es.Connection.CommandTimeout = 3600
                            If icGroup.LoadByPrimaryKey(GroupCode.ToString()) Then
                                Try
                                    ICGroupController.DeleteGroup(GroupCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                    PassCount = PassCount + 1
                                Catch ex As Exception
                                    If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                        FailCount = FailCount + 1
                                    End If
                                End Try
                            End If
                        End If

                    Next

                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Group", "[" & FailCount.ToString() & "] Groups can not be deleted due to following reasons : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Group", "[" & PassCount.ToString() & "] Groups deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Delete Group", "[" & PassCount.ToString() & "] Groups deleted successfully. [" & FailCount.ToString() & "] Groups can not be deleted due to following reasons : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Delete Group", "Please select atleast one(1) Group.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Private Function CheckgvGroupForProcessAll() As Boolean
        Try
            Dim rowgvGroup As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvGroup In gvGroup.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvGroup.Cells(0).FindControl("chkSelectAll"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

#End Region

End Class
