﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveBank.ascx.vb" Inherits="DesktopModules_Bank_SaveBank" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .style1
    {
        width: 30%;
    }
</style>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }
    function CustomValidator_TransferType(source, args) {
        if (document.getElementById("<%= chkIBFT.ClientID %>").checked) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
        }
    }

    function CustomValidator_PrincipalType(source, args) {
        if (document.getElementById("<%= rbtnIsPrincipalYes.ClientID %>").checked || document.getElementById("<%= rbtnIsPrincipalNo.ClientID %>").checked) {
            args.IsValid = true;
        }
        else {
            args.IsValid = false;
        }
    }

  


</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior2" EmptyMessage=""
        Type="Number">
    </telerik:NumericTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9]{1,10}$" ErrorMessage="Invalid Bank ID"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBankID" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
  <%--  <telerik:TextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="true"
        EmptyMessage="Enter Bank Name" ErrorMessage="Invalid Bank Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBankName" />
        </TargetControls>
    </telerik:TextBoxSetting>--%>

       <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior2" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9 ]{1,150}$" ErrorMessage="Invalid Bank Name"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBankName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior3" Validation-IsRequired="false"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBankDescription" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior4" Validation-IsRequired="false"
        ValidationExpression="^[a-zA-Z0-9]{1,150}$" ErrorMessage="Invalid Bank IMD" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBankIMD" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <%--    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior5" Validation-IsRequired="false"
        ValidationExpression="^[0-9- ]{5,15}*$" ErrorMessage="Invalid Phone Number" EmptyMessage="Enter Bank Phone No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBankPhoneNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>--%>
    <telerik:RegExpTextBoxSetting BehaviorID="REPhoneNo2" Validation-IsRequired="false"
        ValidationExpression="^\+?[\d- ]{2,15}$" ErrorMessage="Invalid Phone Number" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBankPhoneNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehaviorWebSite" Validation-IsRequired="false"
        ValidationExpression="^((https?|ftp)://|(www|ftp)\.)[a-z0-9-]+(\.[a-z0-9-]+)+([/?].*)?$"
        ErrorMessage="Invalid Website" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBankWebsite" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <%--    <telerik:TextBoxSetting BehaviorID="RagExpBehavior6" Validation-IsRequired="false"
        EmptyMessage="Enter Bank Website" >
        <TargetControls>
            <telerik:TargetInput ControlID="txtBankWebsite" />
        </TargetControls>
    </telerik:TextBoxSetting>--%>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" class="style1">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBankID" runat="server" Text="Bank ID" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label7" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblBankName" runat="server" Text="Bank Name" CssClass="lbl"></asp:Label><asp:Label
                ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtBankID" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" class="style1">
            <asp:TextBox ID="txtBankName" runat="server" CssClass="txtbox" MaxLength="150" 
                TabIndex="1"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBankDescription" runat="server" Text="Bank Description" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblBankIMD" runat="server" Text="Bank IMD" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtBankDescription" runat="server" CssClass="txtbox" MaxLength="200"
                TextMode="MultiLine" onkeypress="return textboxMultilineMaxNumber(this,200)"
                Height="70px" TabIndex="2"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:TextBox ID="txtBankIMD" runat="server" CssClass="txtbox" MaxLength="150" 
                TabIndex="3"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBankPhoneNo" runat="server" Text="Phone No" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblBankWebsite" runat="server" Text="Website" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtBankPhoneNo" runat="server" CssClass="txtbox" 
                MaxLength="15" TabIndex="4"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:TextBox ID="txtBankWebsite" runat="server" CssClass="txtbox" 
                MaxLength="100" TabIndex="5"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblTransferType" runat="server" Text="Transfer Type" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblIsPrincipal" runat="server" Text="Is Principal" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label5" runat="server" Text="*" ForeColor="Red"></asp:Label>
            <br />
            <asp:CustomValidator ID="crfvPrincipalType" runat="server" Display="Dynamic" ErrorMessage="Please select atleast one."
                ClientValidationFunction="CustomValidator_PrincipalType"></asp:CustomValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkIBFT" runat="server" Text=" Is IBFT" CssClass="chkBox" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:RadioButton ID="rbtnIsPrincipalYes" runat="server" GroupName="A" Text=" Yes"
                CssClass="rdbtn" AutoPostBack="True" />
            <br />
            <asp:RadioButton ID="rbtnIsPrincipalNo" runat="server" GroupName="A" Text=" No" 
                CssClass="rdbtn" AutoPostBack="True" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblDDEnabled" runat="server" Text="DD Enabled" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblDDPreferred" runat="server" Text="DD Preferred" CssClass="lbl"></asp:Label><br />
            <asp:Label ID="lblDDPreferredBank" runat="server" 
                Text="Note: Another Bank is already Set as DDPreferred Bank" ForeColor="Gray" Visible="False"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkDDEnabled" runat="server" Text=" " 
                CssClass="chkBox" AutoPostBack="True" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:CheckBox ID="chkDDPreferred" runat="server" Text=" " 
                CssClass="chkBox" AutoPostBack="True" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIsActive" runat="server" Text="Is Active" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblIsApproved" runat="server" Text="Is Approved" CssClass="lbl" Visible="False"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkActive" runat="server" Text=" " CssClass="chkBox" Checked="True" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:CheckBox ID="chkApproved" runat="server" Text=" " Visible="False" CssClass="chkBox" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnApproved" runat="server" Text="Approved" CssClass="btn" Visible="False"
                Width="71px" />
            &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False"
                 />
            &nbsp;
        </td>
    </tr>
</table>
