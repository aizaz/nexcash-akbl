﻿Imports ICBO.IC
Imports ICBO
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_Bank_ViewBank
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable

#Region "Page Load"

    Protected Sub DesktopModules_Bank_ViewBank_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadBank(True)
                ViewState("SortExp") = Nothing
              
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Bank Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Button"

    Protected Sub btnApproveBanks_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveBanks.Click
        If Page.IsValid Then
            Try
                Dim rowgvBank As GridDataItem
                Dim chkSelect As CheckBox
                Dim BankCode As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0
                Dim icBank As New ICBank

                If CheckgvBankForProcessAll() = True Then
                    For Each rowgvBank In gvBank.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvBank.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            BankCode = rowgvBank("BankCode").Text.ToString()
                            icBank.es.Connection.CommandTimeout = 3600
                            If icBank.LoadByPrimaryKey(BankCode.ToString()) Then
                                If Me.UserInfo.IsSuperUser = False Then
                                    If icBank.IsApproved Then
                                        FailCount = FailCount + 1
                                        Continue For
                                    Else
                                        If icBank.CreatedBy = Me.UserId Then
                                            FailCount = FailCount + 1
                                            Continue For
                                        Else
                                            ICBankController.ApproveBank(BankCode.ToString(), chkSelect.Checked, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                            PassCount = PassCount + 1
                                            Continue For
                                        End If
                                    End If
                                Else
                                    ICBankController.ApproveBank(BankCode.ToString(), chkSelect.Checked, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                    PassCount = PassCount + 1
                                    Continue For
                                End If
                            End If
                        End If
                    Next

                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Approve Banks", "[" & FailCount.ToString() & "] Banks can not be approve due to following reasons : <br /> 1. Bank is approve.<br /> 2. Bank must be approve by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Approve Banks", "[" & PassCount.ToString() & "] Banks approve successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Approve Banks", "[" & PassCount.ToString() & "] Banks approve successfully. [" & FailCount.ToString() & "] Banks can not be approve due to following reasons : <br /> 1. Bank is approve.<br /> 2. Bank must be approve by the user other than the maker.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Approve Banks", "Please select atleast one(1) Bank.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try

        End If

    End Sub

    Private Function CheckgvBankForProcessAll() As Boolean
        Try
            Dim rowgvBank As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvBank In gvBank.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvBank.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnDeleteBanks_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteBanks.Click
        If Page.IsValid Then
            Try
                Dim rowgvBank As GridDataItem
                Dim chkSelect As CheckBox
                Dim BankCode As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0

                If CheckgvBankForProcessAll() = True Then
                    For Each rowgvBank In gvBank.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvBank.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            BankCode = rowgvBank("BankCode").Text.ToString()
                            Dim icBank As New ICBank
                            icBank.es.Connection.CommandTimeout = 3600
                            If icBank.LoadByPrimaryKey(BankCode.ToString()) Then
                                If icBank.IsApproved Then
                                    FailCount = FailCount + 1
                                Else
                                    Try
                                        ICBankController.deleteBank(BankCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                        PassCount = PassCount + 1
                                    Catch ex As Exception
                                        If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                            FailCount = FailCount + 1
                                        End If
                                    End Try
                                End If
                            End If
                        End If

                    Next

                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Banks", "[" & FailCount.ToString() & "] Banks can not be deleted due to following reasons : <br /> 1. Bank is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Banks", "[" & PassCount.ToString() & "] Banks deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Delete Banks", "[" & PassCount.ToString() & "] Banks deleted successfully. [" & FailCount.ToString() & "] Banks can not be deleted due to following reasons : <br /> 1. Bank is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Delete Banks", "Please select atleast one(1) Bank.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnSaveBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveBank.Click
        If Page.IsValid Then

            Response.Redirect(NavigateURL("SaveBank", "&mid=" & Me.ModuleId & "&id=0"), False)

        End If
        'GetAllInstructionsForMarkAsStale()
    End Sub
    Private Function GetAllInstructionsForMarkAsStale() As ICInstructionCollection
        Dim StaleDate As Date = Nothing
        Dim arryInstructionID As New ArrayList
        Dim ArryListPaymentMode As New ArrayList
        ArryListPaymentMode.Add("PO")
        ArryListPaymentMode.Add("DD")
        ArryListPaymentMode.Add("Cheque")
        StaleDate = CDate(Now.Date.AddMonths(-6))
        Dim dt As DataTable
        Dim objICInstructionColl As ICInstructionCollection
        objICInstructionColl = New ICInstructionCollection
        objICInstructionColl.Query.Where(objICInstructionColl.Query.Status = 21)
        objICInstructionColl.Query.Where(objICInstructionColl.Query.PaymentMode.In(ArryListPaymentMode))
        objICInstructionColl.Query.OrderBy(objICInstructionColl.Query.InstructionID.Ascending)
        dt = New DataTable
        dt = objICInstructionColl.Query.LoadDataTable

        If objICInstructionColl.Query.Load Then
            For Each objICInstruction In objICInstructionColl
                If Not objICInstruction.PrintDate Is Nothing Then
                    If CDate(objICInstruction.PrintDate) <= StaleDate Then
                        arryInstructionID.Add(objICInstruction.InstructionID)
                    End If
                ElseIf Not objICInstruction.LastPrintDate Is Nothing Then
                    If CDate(objICInstruction.LastPrintDate) <= StaleDate Then
                        arryInstructionID.Add(objICInstruction.InstructionID)
                    End If
                End If
            Next
        End If
        If arryInstructionID.Count > 0 Then
            objICInstructionColl = New ICInstructionCollection
            objICInstructionColl.Query.Where(objICInstructionColl.Query.InstructionID.In(arryInstructionID))
            objICInstructionColl.Query.OrderBy(objICInstructionColl.Query.InstructionID.Ascending)
            dt = New DataTable
            dt = objICInstructionColl.Query.LoadDataTable
        End If
        Return objICInstructionColl
    End Function
#End Region

#Region "Grid View"

    Private Sub LoadBank(ByVal IsBind As Boolean)
        Try
            ICBankController.GetBankgv(gvBank.CurrentPageIndex + 1, gvBank.PageSize, gvBank, IsBind)
            If gvBank.Items.Count > 0 Then
                lblBankListHeader.Visible = True
                lblBankRNF.Visible = False
                gvBank.Visible = True
                btnDeleteBanks.Visible = CBool(htRights("Delete"))
                btnApproveBanks.Visible = CBool(htRights("Approve"))
                gvBank.Columns(6).Visible = CBool(htRights("Delete"))
            Else
                lblBankListHeader.Visible = True
                gvBank.Visible = False
                btnApproveBanks.Visible = False
                btnDeleteBanks.Visible = False
                lblBankRNF.Visible = True
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Protected Sub gvBank_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvBank.ItemCommand
        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    Dim icbank As New ICBank

                    icbank.es.Connection.CommandTimeout = 3600
                    icbank.LoadByPrimaryKey(e.CommandArgument.ToString())
                    If icbank.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                        If icbank.IsApproved Then
                            UIUtilities.ShowDialog(Me, "Warning", "Bank is approved and can not be deleted.", Dialogmessagetype.Failure)
                        Else
                            ICBankController.deleteBank(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                            UIUtilities.ShowDialog(Me, "Deleted Bank", "Bank deleted successfully.", Dialogmessagetype.Success)
                            LoadBank(True)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Deleted Bank", "Cannot delete record. Please delete associated record(s) first.", Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvBank_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvBank.NeedDataSource

        Try
            LoadBank(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvBank_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvBank.PageIndexChanged
        gvBank.CurrentPageIndex = e.NewPageIndex
    End Sub

    Protected Sub gvBank_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvBank.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvBank.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvBank_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvBank.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvBank.MasterTableView.ClientID & "','0');"
        End If

    End Sub

#End Region

End Class
