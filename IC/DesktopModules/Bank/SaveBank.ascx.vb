﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals

Partial Class DesktopModules_Bank_SaveBank
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private BankCode As String
    Private htRights As Hashtable

#Region "Page load"
    Protected Sub DesktopModules_Bank_SaveBank_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            BankCode = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing


                If BankCode.ToString() = "0" Then
                    Clear()
                    lblIsApproved.Visible = False
                    chkApproved.Visible = False
                    btnApproved.Visible = False
                    lblDDEnabled.Visible = False
                    chkDDEnabled.Visible = False
                    chkDDEnabled.Enabled = False
                    lblDDPreferred.Visible = False
                    chkDDPreferred.Visible = False
                    chkDDPreferred.Enabled = False
                    lblTransferType.Enabled = False
                    rbtnIsPrincipalYes.Enabled = False
                    rbtnIsPrincipalNo.Enabled = False
                    chkIBFT.Visible = False
                    chkIBFT.Enabled = False
                    lblPageHeader.Text = "Add Bank"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                    Dim chkBank As New ICBankCollection()
                    chkBank.Query.Where(chkBank.Query.IsPrincipal = True)
                    If chkBank.Query.Load() Then
                        rbtnIsPrincipalNo.Enabled = True
                    Else
                        rbtnIsPrincipalYes.Enabled = True
                        rbtnIsPrincipalNo.Enabled = True
                    End If
                Else
                    lblIsApproved.Visible = CBool(htRights("Approve"))
                    chkApproved.Visible = CBool(htRights("Approve"))
                    btnApproved.Visible = CBool(htRights("Approve"))
                    lblPageHeader.Text = "Edit Bank"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    Dim cBank As New ICBank()
                    cBank.es.Connection.CommandTimeout = 3600
                    cBank.LoadByPrimaryKey(BankCode)
                    If cBank.LoadByPrimaryKey(BankCode) Then
                        txtBankName.Text = cBank.BankName.ToString()
                        txtBankDescription.Text = cBank.Decription.ToString()
                        txtBankIMD.Text = cBank.BankIMD.ToString()
                        txtBankPhoneNo.Text = cBank.Phone.ToString()
                        txtBankWebsite.Text = cBank.WebSite.ToString()
                        txtBankID.Text = cBank.BankID.ToString()


                        If cBank.IsApproved = True Then
                            chkApproved.Enabled = False
                            btnApproved.Visible = False
                        End If
                        If cBank.IsPrincipal = True Then
                            rbtnIsPrincipalYes.Checked = True
                            lblDDEnabled.Visible = False
                            chkDDEnabled.Visible = False
                            lblDDPreferred.Visible = False
                            chkDDPreferred.Visible = False
                            rbtnIsPrincipalNo.Enabled = True
                            rbtnIsPrincipalYes.Enabled = False
                            If cBank.IsIBFT = True Then
                                lblTransferType.Visible = True
                                chkIBFT.Visible = True
                                chkIBFT.Enabled = True
                                chkIBFT.Checked = cBank.IsIBFT
                            Else
                                lblTransferType.Visible = False
                                chkIBFT.Visible = False
                            End If
                        Else
                            rbtnIsPrincipalNo.Checked = True
                            chkIBFT.Checked = cBank.IsIBFT
                            chkDDEnabled.Checked = cBank.IsDDEnabled
                            chkDDPreferred.Checked = cBank.IsDDPreferred
                            rbtnIsPrincipalYes.Enabled = True
                            rbtnIsPrincipalNo.Enabled = False

                        End If



                        chkActive.Checked = cBank.IsActive
                        chkApproved.Checked = cBank.IsApproved
                    End If
                End If

                End If


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Bank Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        txtBankID.Text = ""
        txtBankName.Text = ""
        txtBankDescription.Text = ""
        txtBankIMD.Text = ""
        txtBankPhoneNo.Text = ""
        txtBankWebsite.Text = ""
        chkIBFT.Checked = False
        rbtnIsPrincipalYes.Checked = False
        rbtnIsPrincipalNo.Checked = False
        lblDDPreferred.Visible = False
        chkDDPreferred.Visible = False
        chkDDPreferred.Enabled = False
        lblTransferType.Visible = False
        chkIBFT.Visible = False
        chkIBFT.Enabled = False
        lblDDEnabled.Visible = False
        chkDDEnabled.Visible = False
        chkDDEnabled.Enabled = False
        chkActive.Checked = True
        lblDDPreferredBank.Visible = False

    End Sub
#End Region

#Region "Buttons"

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim cBank As New ICBank()
                Dim chkBank As New ICBank()

                cBank.es.Connection.CommandTimeout = 3600
                chkBank.es.Connection.CommandTimeout = 3600

                Dim isPrinciple As Boolean = False
                cBank.BankName = txtBankName.Text.ToString()
                cBank.Decription = txtBankDescription.Text.ToString()
                cBank.BankIMD = txtBankIMD.Text.ToString()
                cBank.Phone = txtBankPhoneNo.Text.ToString()
                cBank.WebSite = txtBankWebsite.Text.ToString()
                cBank.BankID = txtBankID.Text.ToString()
                cBank.IsIBFT = chkIBFT.Checked
                cBank.IsActive = chkActive.Checked
                cBank.IsDDEnabled = chkDDEnabled.Checked



                If BankCode = "0" Then
                    cBank.BankCode = BankCode.ToString()

                    If rbtnIsPrincipalYes.Checked = True Then
                        If txtBankIMD.Text = "" Then
                            UIUtilities.ShowDialog(Me, "Error", "Bank IMD must required for Principal Bank.", Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                        If ICBankController.CheckIsPrincipal() = True Then
                            UIUtilities.ShowDialog(Me, "Error", "Principle bank already exist.", Dialogmessagetype.Failure)
                            Exit Sub
                        Else
                            isPrinciple = True
                        End If
                    ElseIf rbtnIsPrincipalNo.Checked = True Then
                        isPrinciple = False

                        If chkIBFT.Checked = True Then
                            If txtBankIMD.Text = "" Then
                                UIUtilities.ShowDialog(Me, "Error", "Bank IMD is mandatory for non-principal bank if IBFT is true.", Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        End If
                    End If

                    If chkDDPreferred.Checked = True Then
                        ICBankController.SetAllDDPreferedAsFalse()
                        cBank.IsDDPreferred = True
                    Else
                        cBank.IsDDPreferred = False
                    End If


                    cBank.IsPrincipal = isPrinciple
                    cBank.CreatedBy = Me.UserId
                    cBank.CreateDate = Date.Now
                    cBank.Creater = Me.UserId
                    cBank.CreationDate = Date.Now
                    If CheckDuplicate(cBank, False) = False Then
                        ICBankController.AddBank(cBank, False, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        UIUtilities.ShowDialog(Me, "Save Bank", "Bank added successfully.", Dialogmessagetype.Success)
                    Else
                        UIUtilities.ShowDialog(Me, "Save Bank", "Can not add duplicate Bank.", Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else
                    cBank.BankCode = BankCode.ToString()
                    chkBank.LoadByPrimaryKey(BankCode)
                    cBank.CreatedBy = Me.UserId
                    cBank.CreateDate = Date.Now

                    If chkDDPreferred.Checked = True Then
                        ICBankController.SetAllDDPreferedAsFalse()
                        cBank.IsDDPreferred = True
                    Else
                        cBank.IsDDPreferred = False
                    End If


                    If chkBank.IsPrincipal = False Then
                        If rbtnIsPrincipalYes.Checked = True Then

                            If ICBankController.CheckIsPrincipal() = True Then
                                UIUtilities.ShowDialog(Me, "Error", "Principle bank already exist.", Dialogmessagetype.Failure)
                                Exit Sub
                            Else
                                isPrinciple = True
                            End If
                        End If
                        If rbtnIsPrincipalNo.Checked = True And chkIBFT.Checked = True Then
                            isPrinciple = False
                            If txtBankIMD.Text = "" Then
                                UIUtilities.ShowDialog(Me, "Error", "Bank IMD must required for Non-Principal Bank for IBFT.", Dialogmessagetype.Failure)
                                Exit Sub
                            End If

                        End If
                    Else
                        isPrinciple = True
                        If rbtnIsPrincipalNo.Checked = True Then
                            isPrinciple = False
                        End If
                    End If


                    If CheckDuplicate(cBank, True) = False Then
                        cBank.IsPrincipal = isPrinciple
                        ICBankController.AddBank(cBank, True, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        UIUtilities.ShowDialog(Me, "Save Bank", "Bank updated successfully.", Dialogmessagetype.Success, NavigateURL())
                    Else
                        UIUtilities.ShowDialog(Me, "Save Bank", "Can not update bank successfully.", Dialogmessagetype.Failure, NavigateURL())
                    End If

                End If
                Clear()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Private Function CheckDuplicate(ByVal cBank As ICBank, ByVal IsUpdatee As Boolean) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim collBank As New ICBankCollection

            collBank = New ICBankCollection

            If IsUpdatee = False Then
                collBank.Query.Where(collBank.Query.BankID.ToLower.Trim = cBank.BankID.ToLower.Trim)
            Else
                collBank.Query.Where(collBank.Query.BankID.Like(cBank.BankID) And collBank.Query.BankCode <> cBank.BankCode)
            End If
            If collBank.Query.Load() Then
                rslt = True
            End If

            collBank = New ICBankCollection

            If IsUpdatee = False Then
                collBank.Query.Where(collBank.Query.BankName.ToLower.Trim = cBank.BankName.ToLower.Trim)
            Else
                collBank.Query.Where(collBank.Query.BankName.Like(cBank.BankName) And collBank.Query.BankCode <> cBank.BankCode)
            End If
            If collBank.Query.Load() Then
                rslt = True
            End If

            collBank = New ICBankCollection

            'If cBank.BankIMD = "" Then
            If Not String.IsNullOrEmpty(cBank.BankIMD.Trim()) Then

                If IsUpdatee = False Then
                    collBank.Query.Where(collBank.Query.BankIMD.ToLower.Trim = cBank.BankIMD.ToLower.Trim)
                Else
                    collBank.Query.Where(collBank.Query.BankIMD.Like(cBank.BankIMD) And collBank.Query.BankCode <> cBank.BankCode)
                End If

                If collBank.Query.Load() Then
                    rslt = True
                End If

            End If


            Return rslt

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    Protected Sub btnApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproved.Click
        If Page.IsValid Then


            Dim cBank As New ICBank

            cBank.es.Connection.CommandTimeout = 3600

            Try
                cBank.LoadByPrimaryKey(BankCode.ToString())
                If Me.UserInfo.IsSuperUser = False Then
                    If cBank.CreatedBy = Me.UserId Then
                        UIUtilities.ShowDialog(Me, "Approve Bank", "Bank must be approve by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                If chkApproved.Checked Then
                    ICBankController.ApproveBank(BankCode.ToString(), chkApproved.Checked, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    UIUtilities.ShowDialog(Me, "Approve Bank", "Bank approve.", Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Approve Bank", "Please checked the Approved Check Box.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub rbtnIsPrincipalNo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnIsPrincipalNo.CheckedChanged

        lblDDEnabled.Visible = True
        chkDDEnabled.Visible = True
        chkDDEnabled.Enabled = True
        chkDDEnabled.Checked = False

        lblTransferType.Visible = True
        chkIBFT.Visible = True
        chkIBFT.Enabled = True
        chkIBFT.Checked = False

    End Sub

    Protected Sub rbtnIsPrincipalYes_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnIsPrincipalYes.CheckedChanged
        lblTransferType.Visible = True
        chkIBFT.Visible = True
        chkIBFT.Enabled = True
        chkIBFT.Checked = False

        lblDDEnabled.Visible = False
        chkDDEnabled.Visible = False
        chkDDEnabled.Enabled = False
        lblDDPreferredBank.Visible = False
        lblDDPreferred.Visible = False
        chkDDPreferred.Visible = False
        chkDDPreferred.Enabled = False

    End Sub

    Protected Sub chkDDPreferred_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkDDPreferred.CheckedChanged
        If chkDDPreferred.Checked = True Then
            If ICBankController.ISDDPreferedBankPresent() = True Then
                lblDDPreferredBank.Visible = True
            Else
                lblDDPreferredBank.Visible = False
            End If
        Else
            lblDDPreferredBank.Visible = False
        End If
    End Sub

    Protected Sub chkDDEnabled_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkDDEnabled.CheckedChanged
        If chkDDEnabled.Checked = True Then
            lblDDPreferred.Visible = True
            chkDDPreferred.Visible = True
            chkDDPreferred.Enabled = True
            chkDDPreferred.Checked = False
        Else
            lblDDPreferred.Visible = False
            chkDDPreferred.Visible = False
            chkDDPreferred.Enabled = False
            chkDDPreferred.Checked = False
        End If
    End Sub

#End Region

 
End Class
