﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InstrumentReturnedOrNotReturnedBulk.ascx.vb" Inherits="DesktopModules_CollectionInstrumentReturnedOrNotReturned_InstrumentReturnedOrNotReturnedBulk" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <asp:Label ID="lblHeading" runat="server" Text="Instrument Status Update - Bulk" CssClass="headingblue"></asp:Label>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <asp:HiddenField ID="hfFilePath" runat="server" />
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <asp:FileUpload ID="FileUploadinBulk" runat="server" />
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <asp:RequiredFieldValidator ID="rfvFile" runat="server" ControlToValidate="FileUploadinBulk"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select File" SetFocusOnError="True"
                ToolTip="Required Field"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="Center" valign="top">&nbsp;
            <asp:Button ID="btnBulk" runat="server" CausesValidation="False" CssClass="btn" Text="Update Status in Bulk" ValidationGroup="Bulk" />
            <asp:Button ID="btnProceed" runat="server" CssClass="btn" OnClick="btnProceed_Click" Text="Upload" />
            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="btnCancel" Text="Cancel" Width="75px" />
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <telerik:RadGrid ID="gvInstrumentDetails" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid" Width="1250px">
                <ClientSettings>
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                </ClientSettings>
                <MasterTableView UseAllDataFields="true" TableLayout="Auto">
                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    </ExpandCollapseColumn>
                    <Columns>                      
                        <telerik:GridBoundColumn DataField="InstrumentNo" HeaderText="Instrument No."
                            SortExpression="InstrumentNo">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BankCode" HeaderText="Bank Code"
                            SortExpression="BankCode">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BankName" HeaderText="Bank Name"
                            SortExpression="BankName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="InstrumentStatusCode" HeaderText="Instrument Status Code"
                            SortExpression="InstrumentStatusCode">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ReturnReason" HeaderText="Return Reason"
                            SortExpression="ReturnReason">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                    <PagerStyle AlwaysVisible="True" />
                </MasterTableView>
                <ItemStyle CssClass="rgRow" />
                <PagerStyle AlwaysVisible="True" />
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <telerik:RadGrid ID="gvUnVarified" runat="server" AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True">
                <AlternatingItemStyle CssClass="rgAltRow" />
                <MasterTableView>
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn DataField="RowNo" HeaderText="Line No." SortExpression="RowNo">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ErrorMessage" HeaderText="Error Message" SortExpression="ErrorMessage">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                    <PagerStyle AlwaysVisible="True" />
                </MasterTableView>
                <ItemStyle CssClass="rgRow" />
                <PagerStyle AlwaysVisible="True" />
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
        </td>
    </tr>
</table>
