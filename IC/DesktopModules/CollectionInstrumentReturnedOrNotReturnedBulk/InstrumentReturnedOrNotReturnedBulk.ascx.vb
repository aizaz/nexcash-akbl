﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports System.IO
Imports System.Data
Imports System.Data.OleDb

Partial Class DesktopModules_CollectionInstrumentReturnedOrNotReturned_InstrumentReturnedOrNotReturnedBulk
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnProceed.Attributes.Item("onclick") = "if (Page_ClientValidate('Bulk')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnProceed, Nothing).ToString() & " } "
                lblHeading.Visible = True
                gvInstrumentDetails.Visible = False
                gvUnVarified.Visible = False
                FileUploadinBulk.Visible = True
                btnCancel.Visible = True
                btnProceed.Visible = True
                btnBulk.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Bulk Instrument Status Update")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        gvInstrumentDetails.Visible = False
        gvUnVarified.Visible = False
        btnBulk.Visible = False
        hfFilePath.Value = Nothing
    End Sub

    Protected Sub btnProceed_Click(sender As Object, e As EventArgs) Handles btnProceed.Click
        If Page.IsValid Then
            Try
                Dim str As String = System.IO.Path.GetExtension(FileUploadinBulk.PostedFile.FileName)
                btnProceed.Visible = False
                btnCancel.Visible = True
                If FileUploadinBulk.HasFile Then

                    If str = ".xls" Or str = ".xlsx" Then

                        Dim aFileInfo As IO.FileInfo
                        aFileInfo = New IO.FileInfo(Server.MapPath("~/UploadedFiles/") & FileUploadinBulk.FileName.ToString())
                        'Delete file from HDD. 
                        If aFileInfo.Exists Then
                            aFileInfo.Delete()
                        End If

                        FileUploadinBulk.SaveAs(Server.MapPath("~/UploadedFiles/") & FileUploadinBulk.FileName.ToString())
                        hfFilePath.Value = Server.MapPath("~/UploadedFiles/") & FileUploadinBulk.FileName.ToString()
                        ReadFileShowDetails()
                    Else
                        UIUtilities.ShowDialog(Me, "Instrument Status Update - Bulk", "Please select the .xls or .xlsx file.", ICBO.IC.Dialogmessagetype.Failure)
                    End If
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Private Sub ReadFileShowDetails()
        Try
            Dim FilePath As String = hfFilePath.Value.ToString()
            Dim connectionString As String = ""

            connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FilePath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""

            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = "Sheet1$"
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()

            FilterAndShowDetails(dtExcelRecords)

        Catch ex As Exception
            If ex.Message.Contains("'Sheet1$' is not a valid name") Then
                UIUtilities.ShowDialog(Me, "Instrument Status Update - Bulk", "Invalid sheet name", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            Else
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End If
        End Try
    End Sub

    Private Sub FilterAndShowDetails(ByVal dtExcel As DataTable)
        Try
            If dtExcel.Columns.Count <> 4 Then
                UIUtilities.ShowDialog(Me, "Instrument Status Update - Bulk", "No of columns do not match.", ICBO.IC.Dialogmessagetype.Failure)
                Clear()
                Exit Sub
            End If

            ViewState("dtFiltered") = Nothing
            ViewState("dtUnVarified") = Nothing
            Dim dtFiltered As New DataTable
            Dim dtUnVarified As New DataTable
            Dim drExcel, drFiltered, drUnVarified As DataRow
            Dim IsValid As Boolean = True
            Dim BankName As String = ""
            Dim RowNo As Integer = 0
            Dim ErrorMessage As String = ""

            dtFiltered.Columns.Add(New DataColumn("BankCode", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("InstrumentNo", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("InstrumentStatusCode", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("ReturnReason", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BankName", GetType(System.String)))

            dtUnVarified.Columns.Add(New DataColumn("RowNo", GetType(System.String)))
            dtUnVarified.Columns.Add(New DataColumn("ErrorMessage", GetType(System.String)))
      
            If dtExcel.Rows.Count > 0 Then

                For Each drExcel In dtExcel.Rows

                    RowNo = RowNo + 1
                    ErrorMessage = ""
                    IsValid = True

                    If drExcel(0).ToString() = "" Then
                        IsValid = False
                        ErrorMessage += "Bank Code is empty (" + (drExcel(0).ToString()) + ") ; "
                    ElseIf Regex.IsMatch(drExcel(0).ToString().Trim, "^[0-9]{1,9}$") = False Then
                        IsValid = False
                        ErrorMessage += "Bank Code must be in numbers only (" + (drExcel(0).ToString()) + ") ; "
                    ElseIf drExcel(0).ToString().Length() > 9 Then
                        IsValid = False
                        ErrorMessage += "Invalid Bank Code Length (" + (drExcel(0).ToString()) + ") ; "
                    Else
                        BankName = ICBankController.GetBankNameByBankCode(drExcel(0).ToString()).ToString()
                        If BankName.ToString() = "" Then
                            IsValid = False
                            ErrorMessage += "Invalid Bank Code (" + (drExcel(0).ToString()) + ") ; "
                        End If
                    End If


                    If drExcel(1).ToString() = "" Then
                        IsValid = False
                        ErrorMessage += "Instrument Number is empty (" + (drExcel(1).ToString()) + ") ; "
                    ElseIf drExcel(1).ToString().Length() > 9 Then
                        IsValid = False
                        ErrorMessage += "Instrument Number length exceeded (" + (drExcel(1).ToString()) + ") ; "
                    ElseIf Regex.IsMatch(drExcel(1).ToString().Trim, "^[0-9]{1,9}$") = False Then
                        IsValid = False
                        ErrorMessage += "Instrument Number must be in numbers only (" + (drExcel(1).ToString()) + ") ; "
                    Else
                        Dim objCollections As New ICCollections
                        objCollections.Query.Where(objCollections.Query.InstrumentNo = drExcel(1).ToString())
                        If objCollections.Query.Load() = False Then
                            IsValid = False
                            ErrorMessage += "Invalid Instrument Number (" + (drExcel(1).ToString()) + ") ; "
                        End If
                    End If


                    If drExcel(2).ToString() = "" Then
                        IsValid = False
                        ErrorMessage += "Instrument Status Code is empty (" + (drExcel(2).ToString()) + ") ; "
                    ElseIf drExcel(2).ToString().Length() > 2 Then
                        IsValid = False
                        ErrorMessage += "Instrument Status Code length exceeded (" + (drExcel(2).ToString()) + ") ; "
                    ElseIf Regex.IsMatch(drExcel(2).ToString().Trim, "^[a-zA-Z]{1,2}$") = False Then
                        IsValid = False
                        ErrorMessage += "Instrument Status Code must be in alphabet only (" + (drExcel(2).ToString()) + ") ; "
                    ElseIf drExcel(2).ToString.Trim.ToLower <> "r" And drExcel(2).ToString.Trim.ToLower <> "nr" Then
                        IsValid = False
                        ErrorMessage += "Invalid Instrument Status Code (" + (drExcel(2).ToString()) + ") ; "
                    Else
                        If drExcel(2).ToString.Trim.ToLower = "r" Then
                            If drExcel(3).ToString() = "" Then
                                IsValid = False
                                ErrorMessage += "Return Reason is empty (" + (drExcel(3).ToString()) + ") ; "
                            End If
                        End If
                    End If


                    If IsValid = True Then
                        gvInstrumentDetails.Visible = True
                        btnBulk.Visible = CBool(htRights("Update"))

                        drFiltered = dtFiltered.NewRow()
                        drFiltered(0) = drExcel(0).ToString()               ''BankCode
                        drFiltered(1) = drExcel(1).ToString()               ''InstrumentNo
                        drFiltered(2) = drExcel(2).ToString()               ''InstrumentStatusCode
                        drFiltered(3) = drExcel(3).ToString()               ''ReturnReason
                        drFiltered(4) = BankName.ToString()                 ''BankName
                      

                        dtFiltered.Rows.Add(drFiltered)
                    Else
                        gvUnVarified.Visible = True
                        drUnVarified = dtUnVarified.NewRow()
                        drUnVarified(0) = RowNo.ToString()
                        drUnVarified(1) = ErrorMessage.ToString()

                        dtUnVarified.Rows.Add(drUnVarified)
                    End If
                Next

                ViewState("dtFiltered") = dtFiltered
                ViewState("dtUnVarified") = dtUnVarified

                SetGVInstrumentDetailsGVUnverified(True)

            Else
                UIUtilities.ShowDialog(Me, "Instrument Status Update - Bulk", "Empty File.", ICBO.IC.Dialogmessagetype.Failure)
                Clear()
                Exit Sub
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnBulk_Click(sender As Object, e As EventArgs) Handles btnBulk.Click
        If Page.IsValid Then
            Try
                InstrumentMarkedReturnedOrNotReturned()

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Private Sub InstrumentMarkedReturnedOrNotReturned()
        Try
            Dim dtFiltered As New DataTable
            Dim drFiltered As DataRow
            Dim dtUnsuccessful As New DataTable
            Dim drUnsuccessful As DataRow
            dtUnsuccessful.Columns.Add("Unsuccessful")
            Dim Successful As Integer = 0

            If ViewState("dtFiltered") Is Nothing Then
                UIUtilities.ShowDialog(Me, "Instrument Status Update - Bulk", "Record not Found.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            Else
                dtFiltered = DirectCast(ViewState("dtFiltered"), DataTable)
                For Each drFiltered In dtFiltered.Rows
                    Dim objCollections As New ICCollections
                    objCollections.Query.Where(objCollections.Query.InstrumentNo = drFiltered("InstrumentNo").ToString())
                    objCollections.Query.Where(objCollections.Query.BankCode = drFiltered("BankCode").ToString())
                    objCollections.Query.Where(objCollections.Query.CollectionClearingType <> "Funds Transfer")
                    objCollections.Query.Where(objCollections.Query.Status <> "3") '' Paid
                    If objCollections.Query.Load() Then
                        If drFiltered("InstrumentStatusCode").ToString.ToLower = "r" Then
                            If objCollections.Status = "6" Then       '' Returned
                                'UIUtilities.ShowDialog(Me, "Error", "Instrument status is already Returned.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                                drUnsuccessful = dtUnsuccessful.NewRow()
                                drUnsuccessful("Unsuccessful") = "Instrument No: " & drFiltered("InstrumentNo").ToString() & " and Bank Code: " & drFiltered("BankCode").ToString() & ", status is already Returned. <br/>"
                                dtUnsuccessful.Rows.Add(drUnsuccessful)
                            ElseIf objCollections.Status = "8" Then             '' Pending Clearing
                                Dim FromAccType As CBUtilities.AccountType
                                Dim ToAccType As CBUtilities.AccountType
                                If objCollections.ClearingCBAccountType = "RB" Then
                                    FromAccType = CBUtilities.AccountType.RB
                                Else
                                    FromAccType = CBUtilities.AccountType.GL
                                End If

                                If objCollections.CollectionAccountType = "RB" Then
                                    ToAccType = CBUtilities.AccountType.RB
                                Else
                                    ToAccType = CBUtilities.AccountType.GL
                                End If

                                Try
                                    If CBUtilities.OpenEndedFundsTransfer(objCollections.ClearingAccountNo, objCollections.ClearingAccountBranchCode, objCollections.ClearingAccountCurrency, Nothing, Nothing, Nothing, FromAccType, objCollections.CollectionAccountNo, objCollections.CollectionAccountBranchCode, objCollections.CollectionAccountCurrency, Nothing, Nothing, Nothing, ToAccType, objCollections.CollectionAmount, "Collections", objCollections.CollectionID) = CBUtilities.TransactionStatus.Successfull Then
                                        Dim FrmAccType As CBUtilities.AccountType
                                        Dim ToAcType As CBUtilities.AccountType
                                        If objCollections.CollectionAccountType = "RB" Then
                                            FrmAccType = CBUtilities.AccountType.RB
                                        Else
                                            FrmAccType = CBUtilities.AccountType.GL
                                        End If
                                        If objCollections.ClearingCBAccountType = "RB" Then
                                            ToAcType = CBUtilities.AccountType.RB
                                        Else
                                            ToAcType = CBUtilities.AccountType.GL
                                        End If

                                        Try
                                            CBUtilities.OpenEndedFundsTransfer(objCollections.CollectionAccountNo, objCollections.CollectionAccountBranchCode, objCollections.CollectionAccountCurrency, Nothing, Nothing, Nothing, FrmAccType, objCollections.ClearingAccountNo, objCollections.ClearingAccountBranchCode, objCollections.ClearingAccountCurrency, Nothing, Nothing, Nothing, ToAcType, objCollections.CollectionAmount, "Collections", objCollections.CollectionID)
                                            'objCollections.LastStatus = "6"    ''Returned
                                            'objCollections.Status = "6"    ''Returned

                                            ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objCollections.ReferenceField1, objCollections.ReferenceField2, objCollections.ReferenceField3, objCollections.ReferenceField4, objCollections.ReferenceField5, objCollections.UpToICCollectionsStatusByStatus.CollectionStatusName, "Returned", Me.UserId, Me.UserInfo.Username)
                                            ICCollectionsController.UpdateICCollectionsStatus(objCollections.CollectionID, objCollections.Status, "6", Me.UserId, Me.UserInfo.Username)

                                            objCollections.ReturnReason = drFiltered("ReturnReason").ToString()
                                            objCollections.Save()
                                            Dim ActionStr As String = ""
                                            ActionStr = "Instrument No: " & objCollections.InstrumentNo & " ; Bank Code: " & objCollections.BankCode & " successfully marked as Returned"
                                            ICUtilities.AddAuditTrail(ActionStr, "Collections", objCollections.CollectionID, Me.UserId, Me.UserInfo.Username, "UPDATE")
                                            'UIUtilities.ShowDialog(Me, "Instrument Status Update - Single", "Instrument successfully marked as Returned.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                                            Successful = Successful + 1

                                        Catch ex As Exception
                                            'objCollections.LastStatus = "10"    ''Clearing Error
                                            'objCollections.Status = "10"    ''Clearing Error
                                            'objCollections.Save()
                                            ' UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure, NavigateURL())


                                            ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objCollections.ReferenceField1, objCollections.ReferenceField2, objCollections.ReferenceField3, objCollections.ReferenceField4, objCollections.ReferenceField5, objCollections.UpToICCollectionsStatusByStatus.CollectionStatusName, "Clearing Error", Me.UserId, Me.UserInfo.Username)
                                            ICCollectionsController.UpdateICCollectionsStatus(objCollections.CollectionID, objCollections.Status, "10", Me.UserId, Me.UserInfo.Username)


                                            drUnsuccessful = dtUnsuccessful.NewRow()
                                            drUnsuccessful("Unsuccessful") = "Instrument No: " & drFiltered("InstrumentNo").ToString() & " and Bank Code: " & drFiltered("BankCode").ToString() & ", " & ex.Message.ToString() & ". <br/>"
                                            dtUnsuccessful.Rows.Add(drUnsuccessful)
                                        End Try

                                    End If
                                Catch ex As Exception
                                    'UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                                    drUnsuccessful = dtUnsuccessful.NewRow()
                                    drUnsuccessful("Unsuccessful") = "Instrument No: " & drFiltered("InstrumentNo").ToString() & " and Bank Code: " & drFiltered("BankCode").ToString() & ", " & ex.Message.ToString() & ". <br/>"
                                    dtUnsuccessful.Rows.Add(drUnsuccessful)
                                End Try

                            ElseIf objCollections.Status = "9" Then     '' Cleared
                                Dim FrmAccType As CBUtilities.AccountType
                                Dim ToAcType As CBUtilities.AccountType
                                If objCollections.CollectionAccountType = "RB" Then
                                    FrmAccType = CBUtilities.AccountType.RB
                                Else
                                    FrmAccType = CBUtilities.AccountType.GL
                                End If
                                If objCollections.ClearingCBAccountType = "RB" Then
                                    ToAcType = CBUtilities.AccountType.RB
                                Else
                                    ToAcType = CBUtilities.AccountType.GL
                                End If

                                Try
                                    CBUtilities.OpenEndedFundsTransfer(objCollections.CollectionAccountNo, objCollections.CollectionAccountBranchCode, objCollections.CollectionAccountCurrency, Nothing, Nothing, Nothing, FrmAccType, objCollections.ClearingAccountNo, objCollections.ClearingAccountBranchCode, objCollections.ClearingAccountCurrency, Nothing, Nothing, Nothing, ToAcType, objCollections.CollectionAmount, "Collections", objCollections.CollectionID)
                                    'objCollections.LastStatus = "6"    ''Returned
                                    'objCollections.Status = "6"    ''Returned


                                    ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objCollections.ReferenceField1, objCollections.ReferenceField2, objCollections.ReferenceField3, objCollections.ReferenceField4, objCollections.ReferenceField5, objCollections.UpToICCollectionsStatusByStatus.CollectionStatusName, "Returned", Me.UserId, Me.UserInfo.Username)
                                    ICCollectionsController.UpdateICCollectionsStatus(objCollections.CollectionID, objCollections.Status, "6", Me.UserId, Me.UserInfo.Username)


                                    objCollections.ReturnReason = drFiltered("ReturnReason").ToString()
                                    objCollections.Save()
                                    Dim ActionStr As String = ""
                                    ActionStr = "Instrument No: " & objCollections.InstrumentNo & " ; Bank Code: " & objCollections.BankCode & " successfully marked as Returned"
                                    ICUtilities.AddAuditTrail(ActionStr, "Collections", objCollections.CollectionID, Me.UserId, Me.UserInfo.Username, "UPDATE")
                                    'UIUtilities.ShowDialog(Me, "Instrument Status Update - Single", "Instrument successfully marked as Returned.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                                    Successful = Successful + 1

                                Catch ex As Exception
                                    'objCollections.LastStatus = "10"    ''Clearing Error
                                    'objCollections.Status = "10"    ''Clearing Error
                                    'objCollections.Save()
                                    'UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure, NavigateURL())


                                    ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objCollections.ReferenceField1, objCollections.ReferenceField2, objCollections.ReferenceField3, objCollections.ReferenceField4, objCollections.ReferenceField5, objCollections.UpToICCollectionsStatusByStatus.CollectionStatusName, "Clearing Error", Me.UserId, Me.UserInfo.Username)
                                    ICCollectionsController.UpdateICCollectionsStatus(objCollections.CollectionID, objCollections.Status, "10", Me.UserId, Me.UserInfo.Username)


                                    drUnsuccessful = dtUnsuccessful.NewRow()
                                    drUnsuccessful("Unsuccessful") = "Instrument No: " & drFiltered("InstrumentNo").ToString() & " and Bank Code: " & drFiltered("BankCode").ToString() & ", " & ex.Message.ToString() & ". <br/>"
                                    dtUnsuccessful.Rows.Add(drUnsuccessful)
                                End Try

                            ElseIf objCollections.Status = "10" Then     '' Clearing Error
                                'objCollections.LastStatus = "6"    ''Returned
                                'objCollections.Status = "6"    ''Returned


                                ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objCollections.ReferenceField1, objCollections.ReferenceField2, objCollections.ReferenceField3, objCollections.ReferenceField4, objCollections.ReferenceField5, objCollections.UpToICCollectionsStatusByStatus.CollectionStatusName, "Returned", Me.UserId, Me.UserInfo.Username)
                                ICCollectionsController.UpdateICCollectionsStatus(objCollections.CollectionID, objCollections.Status, "6", Me.UserId, Me.UserInfo.Username)


                                objCollections.ReturnReason = drFiltered("ReturnReason").ToString()
                                objCollections.Save()
                                Dim ActionStr As String = ""
                                ActionStr = "Instrument No: " & objCollections.InstrumentNo & " ; Bank Code: " & objCollections.BankCode & " successfully marked as Returned"
                                ICUtilities.AddAuditTrail(ActionStr, "Collections", objCollections.CollectionID, Me.UserId, Me.UserInfo.Username, "UPDATE")
                                'UIUtilities.ShowDialog(Me, "Instrument Status Update - Single", "Instrument successfully marked as Returned.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                                Successful = Successful + 1
                            End If


                        ElseIf drFiltered("InstrumentStatusCode").ToString.ToLower = "nr" Then
                            If objCollections.Status = "8" Then             '' Pending Clearing
                                'UIUtilities.ShowDialog(Me, "Error", "Instrument is Pending Clearing state and can not be marked as Not Returned.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                                drUnsuccessful = dtUnsuccessful.NewRow()
                                drUnsuccessful("Unsuccessful") = "Instrument No: " & drFiltered("InstrumentNo").ToString() & " and Bank Code: " & drFiltered("BankCode").ToString() & ", is Pending Clearing state and can not be marked as Not Returned. <br/>"
                                dtUnsuccessful.Rows.Add(drUnsuccessful)
                            ElseIf objCollections.Status = "9" Then      '' Cleared
                                'UIUtilities.ShowDialog(Me, "Error", "Instrument is Cleared state and can not be marked as Not Returned.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                                drUnsuccessful = dtUnsuccessful.NewRow()
                                drUnsuccessful("Unsuccessful") = "Instrument No: " & drFiltered("InstrumentNo").ToString() & " and Bank Code: " & drFiltered("BankCode").ToString() & ", is Cleared state and can not be marked as Not Returned. <br/>"
                                dtUnsuccessful.Rows.Add(drUnsuccessful)
                            ElseIf objCollections.Status = "6" Then       '' Returned
                                'objCollections.LastStatus = "8"              '' Pending Clearing
                                'objCollections.Status = "8"              '' Pending Clearing


                                ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objCollections.ReferenceField1, objCollections.ReferenceField2, objCollections.ReferenceField3, objCollections.ReferenceField4, objCollections.ReferenceField5, objCollections.UpToICCollectionsStatusByStatus.CollectionStatusName, "Pending Clearing", Me.UserId, Me.UserInfo.Username)
                                ICCollectionsController.UpdateICCollectionsStatus(objCollections.CollectionID, objCollections.Status, "8", Me.UserId, Me.UserInfo.Username)


                                objCollections.ReturnReason = Nothing
                                objCollections.Save()
                                Dim ActionStr As String = ""
                                ActionStr = "Instrument No: " & objCollections.InstrumentNo & " ; Bank Code: " & objCollections.BankCode & " successfully marked as Not Returned"
                                ICUtilities.AddAuditTrail(ActionStr, "Collections", objCollections.CollectionID, Me.UserId, Me.UserInfo.Username, "UPDATE")
                                'UIUtilities.ShowDialog(Me, "Instrument Status Update - Single", "Instrument successfully marked as Not Returned.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                                Successful = Successful + 1
                            ElseIf objCollections.Status = "10" Then     '' Clearing Error
                                'objCollections.LastStatus = "8"             '' Pending Clearing
                                'objCollections.Status = "8"             '' Pending Clearing


                                ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objCollections.ReferenceField1, objCollections.ReferenceField2, objCollections.ReferenceField3, objCollections.ReferenceField4, objCollections.ReferenceField5, objCollections.UpToICCollectionsStatusByStatus.CollectionStatusName, "Pending Clearing", Me.UserId, Me.UserInfo.Username)
                                ICCollectionsController.UpdateICCollectionsStatus(objCollections.CollectionID, objCollections.Status, "8", Me.UserId, Me.UserInfo.Username)


                                objCollections.ReturnReason = Nothing
                                objCollections.Save()
                                Dim ActionStr As String = ""
                                ActionStr = "Instrument No: " & objCollections.InstrumentNo & " ; Bank Code: " & objCollections.BankCode & " successfully marked as Not Returned"
                                ICUtilities.AddAuditTrail(ActionStr, "Collections", objCollections.CollectionID, Me.UserId, Me.UserInfo.Username, "UPDATE")
                                'UIUtilities.ShowDialog(Me, "Instrument Status Update - Single", "Instrument successfully marked as Not Returned.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                                Successful = Successful + 1
                            End If

                        End If
                    Else
                        drUnsuccessful = dtUnsuccessful.NewRow()
                        drUnsuccessful("Unsuccessful") = "Instrument No: " & drFiltered("InstrumentNo").ToString() & " and Bank Code: " & drFiltered("BankCode").ToString() & ", Record not found. <br/>"
                        dtUnsuccessful.Rows.Add(drUnsuccessful)
                    End If
                Next
            End If

            Dim arrUnsuccessful As String = Nothing
            If dtUnsuccessful.Rows.Count > 0 Then
                For Each dr As DataRow In dtUnsuccessful.Rows
                    arrUnsuccessful += dr("Unsuccessful").ToString()
                Next
            End If
            If Successful <> "0" And dtUnsuccessful.Rows.Count > 0 Then
                UIUtilities.ShowDialog(Me, "Instrument Status Update - Bulk", Successful & " instrument(s) updated successfully.<br/>" & dtUnsuccessful.Rows.Count & " instrument(s) not updated. Details are: <br/>" & arrUnsuccessful, ICBO.IC.Dialogmessagetype.Success, NavigateURL())
            ElseIf Successful <> "0" Then
                UIUtilities.ShowDialog(Me, "Instrument Status Update - Bulk", Successful & " instrument(s) updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
            ElseIf dtUnsuccessful.Rows.Count > 0 Then
                UIUtilities.ShowDialog(Me, "Instrument Status Update - Bulk", dtUnsuccessful.Rows.Count & " instrument(s) not updated. Details are: <br/>" & arrUnsuccessful, ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
            End If

            Dim aFileInfo As IO.FileInfo
            aFileInfo = New IO.FileInfo(hfFilePath.Value.ToString())
            'Delete file from HDD. 
            If aFileInfo.Exists Then
                aFileInfo.Delete()
            End If

            Clear()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub SetGVInstrumentDetailsGVUnverified(ByVal IsBind As Boolean)
        Try
            Dim dtFiltered As New DataTable
            Dim dtUnVarified As New DataTable

            dtFiltered = DirectCast(ViewState("dtFiltered"), DataTable)
            dtUnVarified = DirectCast(ViewState("dtUnVarified"), DataTable)

            gvInstrumentDetails.DataSource = dtFiltered
            gvUnVarified.DataSource = dtUnVarified

            If IsBind = True Then
                gvInstrumentDetails.DataBind()
                gvUnVarified.DataBind()
            End If


            If dtFiltered.Rows.Count > 0 Then
                gvInstrumentDetails.Visible = True
            Else
                gvInstrumentDetails.Visible = False
                btnBulk.Visible = False
            End If
            If dtUnVarified.Rows.Count > 0 Then
                gvUnVarified.Visible = True
            Else
                gvUnVarified.Visible = False
            End If


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If Not hfFilePath.Value Is Nothing And hfFilePath.Value <> "" Then
            Dim aFileInfo As IO.FileInfo
            aFileInfo = New IO.FileInfo(hfFilePath.Value.ToString())
            'Delete file from HDD. 
            If aFileInfo.Exists Then
                aFileInfo.Delete()
            End If
        End If
        Response.Redirect(NavigateURL(), False)
    End Sub

    Protected Sub gvUnVarified_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvUnVarified.NeedDataSource
        SetGVInstrumentDetailsGVUnverified(False)
    End Sub

    Protected Sub gvInstrumentDetails_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles gvInstrumentDetails.NeedDataSource
        SetGVInstrumentDetailsGVUnverified(False)
    End Sub

End Class
