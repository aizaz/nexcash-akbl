﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals

Partial Class DesktopModules_CompanyOfficeManagement_SaveCompanyOffice
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private OfficeCode As String
    Private htRights As Hashtable


#Region "Page Load"
    Protected Sub DesktopModules_CompanyOfficeManagement_SaveCompanyOffice_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            OfficeCode = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlCountry()
                LoadddlProvince()
                LoadddlCity()
                LoadddlGroup()
                LoadddlCompany()
                If OfficeCode.ToString() = "0" Then
                    Clear()

                    lblPageHeader.Text = "Add Company Office"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                Else

                    lblPageHeader.Text = "Edit Company Office"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    Dim objICCompanyOffice As New ICOffice
                    objICCompanyOffice.es.Connection.CommandTimeout = 3600

                    If objICCompanyOffice.LoadByPrimaryKey(OfficeCode) Then
                        If Not objICCompanyOffice.OfficeCode Is Nothing Then
                            txtOfficeCode.Text = objICCompanyOffice.OfficeCode
                        Else
                            txtOfficeCode.Text = Nothing
                        End If
                        txtOfficePhone1.Text = objICCompanyOffice.Phone1.ToString()
                        txtOfficePhone2.Text = objICCompanyOffice.Phone2.ToString()
                        txtOfficeAddress.Text = objICCompanyOffice.Address.ToString()
                        txtOffDescrption.Text = objICCompanyOffice.Description
                        txtOfficeName.Text = objICCompanyOffice.OfficeName.ToString()
                        txtOfficeEmail.Text = objICCompanyOffice.Email.ToString()
                        chkActive.Checked = objICCompanyOffice.IsActive
                        chkPrntLocation.Checked = objICCompanyOffice.IsPrintingLocation
                        Dim objICCompany As New ICCompany
                        Dim objICGroup As New ICGroup
                        objICCompany.es.Connection.CommandTimeout = 3600
                        objICGroup.es.Connection.CommandTimeout = 3600
                        objICCompany.LoadByPrimaryKey(objICCompanyOffice.CompanyCode)
                        objICGroup = objICCompany.UpToICGroupByGroupCode()
                        LoadddlGroup()
                        ddlGroup.SelectedValue = objICGroup.GroupCode.ToString()
                        LoadddlCompany()
                        ddlCompany.SelectedValue = objICCompany.CompanyCode.ToString()
                        ddlGroup.Enabled = False

                        Dim objICCity As New ICCity
                        Dim objICProvince As New ICProvince
                        Dim objICCountry As New ICCountry

                        objICCity.es.Connection.CommandTimeout = 3600
                        objICProvince.es.Connection.CommandTimeout = 3600
                        objICCountry.es.Connection.CommandTimeout = 3600
                        objICCity = objICCompanyOffice.UpToICCityByCityID
                        objICProvince = objICCity.UpToICProvinceByProvinceCode()
                        objICCountry = objICProvince.UpToICCountryByCountryCode()

                        LoadddlCountry()
                        ddlCountry.SelectedValue = objICCountry.CountryCode
                        LoadddlProvince()
                        ddlProvince.SelectedValue = objICProvince.ProvinceCode()
                        LoadddlCity()
                        ddlCity.SelectedValue = objICCity.CityCode
                      

                    End If
                End If
                'btnSave.Visible = ArrRights(1)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client Location Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#Region "Button Events"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then

            Try
                Dim objICCompanyOffice As New ICOffice
                Dim objICCompanyOfficeFrom As New ICOffice
                Dim StrAction As String = ""
                objICCompanyOffice.es.Connection.CommandTimeout = 3600
                objICCompanyOffice.Email = txtOfficeEmail.Text.ToString()
                objICCompanyOffice.Phone1 = txtOfficePhone1.Text.ToString()
                objICCompanyOffice.Phone2 = txtOfficePhone2.Text.ToString()
                objICCompanyOffice.Address = txtOfficeAddress.Text.ToString()
                objICCompanyOffice.Description = txtOffDescrption.Text.ToString()
                objICCompanyOffice.CompanyCode = ddlCompany.SelectedValue.ToString()
                objICCompanyOffice.OfficeName = txtOfficeName.Text.ToString()
                objICCompanyOffice.BankCode = Nothing
                objICCompanyOffice.OfficeCode = Nothing
                objICCompanyOffice.CityID = ddlCity.SelectedValue.ToString
                objICCompanyOffice.IsActive = chkActive.Checked
                objICCompanyOffice.OffceType = "Company Office"
                objICCompanyOffice.OfficeCode = txtOfficeCode.Text
                objICCompanyOffice.IsPrintingLocation = chkPrntLocation.Checked
                objICCompanyOffice.IsApprove = True

                If OfficeCode = "0" Then
                    objICCompanyOffice.CreatedBy = Me.UserId
                    objICCompanyOffice.CreateDate = Date.Now
                    objICCompanyOffice.Creater = Me.UserId
                    objICCompanyOffice.CreationDate = Date.Now
                    If CheckDuplicate(objICCompanyOffice) = False Then
                        ICOfficeController.AddOffice(objICCompanyOffice, False, Me.UserId.ToString, Me.UserInfo.Username.ToString, "")

                        UIUtilities.ShowDialog(Me, "Save Office", "Office added successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Else
                        UIUtilities.ShowDialog(Me, "Save Company Office", "Can not add duplicate Company Office.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else
                    objICCompanyOffice.OfficeID = OfficeCode
                    objICCompanyOffice.CreatedBy = Me.UserId
                    objICCompanyOffice.CreateDate = Date.Now
                    objICCompanyOfficeFrom.LoadByPrimaryKey(OfficeCode.ToString)
                    If CheckDuplicateOnUpdate(objICCompanyOffice) = True Then
                        UIUtilities.ShowDialog(Me, "Save Company Office", "Can not add duplicate Company Office.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    StrAction = "Company Office " & objICCompanyOffice.OfficeName & ": Company Office Name from [ " & objICCompanyOffice.OfficeName & " ] to [ " & objICCompanyOfficeFrom.OfficeName & " ] ; "
                    StrAction += "Office from [ " & objICCompanyOfficeFrom.OfficeCode & " ] to [ " & objICCompanyOffice.OfficeCode & " ]; "
                    StrAction += "Phone 1 from [ " & objICCompanyOfficeFrom.Phone1 & " ] to [ " & objICCompanyOffice.Phone1 & " ]; "
                    StrAction += "Phone 2 from [ " & objICCompanyOfficeFrom.Phone2 & " ] to [ " & objICCompanyOffice.Phone2 & " ]; "
                    StrAction += "Email Address from [ " & objICCompanyOfficeFrom.Email & " ] to [ " & objICCompanyOffice.Email & " ]; "
                    StrAction += "Address from [ " & objICCompanyOfficeFrom.Address & " ] to [" & objICCompanyOffice.Address & " ] ; "
                    StrAction += "Description from [ " & objICCompanyOfficeFrom.Description & " ] to [ " & objICCompanyOffice.Description & " ] ; "
                    StrAction += "City from [ " & objICCompanyOfficeFrom.UpToICCityByCityID.CityName & " ] to [ " & objICCompanyOffice.UpToICCityByCityID.CityName & " ]; "
                    StrAction += "Province from [ " & objICCompanyOfficeFrom.UpToICCityByCityID.UpToICProvinceByProvinceCode.ProvinceName & " ] to [ " & objICCompanyOffice.UpToICCityByCityID.UpToICProvinceByProvinceCode.ProvinceName & " ]; "
                    StrAction += "Country from [ " & objICCompanyOfficeFrom.UpToICCityByCityID.UpToICProvinceByProvinceCode.UpToICCountryByCountryCode.CountryName & " ] to [ " & objICCompanyOffice.UpToICCityByCityID.UpToICProvinceByProvinceCode.UpToICCountryByCountryCode.CountryName & " ]; "
                    StrAction += "Printing Location from [ " & objICCompanyOfficeFrom.IsPrintingLocation & " ] to [ " & objICCompanyOffice.IsPrintingLocation & " ]; "
                    StrAction += "Is Active from [ " & objICCompanyOfficeFrom.IsActive & " ] to [ " & objICCompanyOffice.IsActive & " ] Updated."
                    ICOfficeController.AddOffice(objICCompanyOffice, True, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction)

                    UIUtilities.ShowDialog(Me, "Save Company Office", "Office updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                End If
                Clear()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub
#End Region
#Region "Load Drop Down"
    Private Sub LoadddlCountry()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCountry.Items.Clear()
            ddlCountry.Items.Add(lit)
            ddlCountry.AppendDataBoundItems = True
            ddlCountry.DataSource = ICCountryController.GetCountryActiveAndApproved()
            ddlCountry.DataTextField = "CountryName"
            ddlCountry.DataValueField = "CountryCode"
            ddlCountry.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlProvince()
        Try
            ddlProvince.Items.Clear()
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlProvince.Items.Add(lit)
            ddlProvince.AppendDataBoundItems = True
            ddlProvince.DataSource = ICProvinceController.GetProvinceActiveAndApprove(ddlCountry.SelectedValue.ToString())
            ddlProvince.DataTextField = "ProvinceName"
            ddlProvince.DataValueField = "ProvinceCode"
            ddlProvince.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCity()
        Try
            ddlCity.Items.Clear()
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCity.Items.Add(lit)
            ddlCity.AppendDataBoundItems = True
            ddlCity.DataSource = ICCityController.GetCityActiveAndApprovedByProvinceCode(ddlProvince.SelectedValue.ToString())
            ddlCity.DataTextField = "CityName"
            ddlCity.DataValueField = "CityCode"
            ddlCity.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
   
#End Region
#Region "Drop Down Selected Index Chnaged"
    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        LoadddlProvince()
        LoadddlCity()

    End Sub

    Protected Sub ddlProvince_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProvince.SelectedIndexChanged
        LoadddlCity()


    End Sub

   

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        LoadddlCompany()
    End Sub
#End Region
#Region "Other Functions/Routines"
    Private Function CheckDuplicate(ByVal ICOffice As ICOffice) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim objICOfficeColl As ICOfficeCollection
            'Dim collCompany As New ICCompanyCollection
            objICOfficeColl = New ICOfficeCollection
            objICOfficeColl.es.Connection.CommandTimeout = 3600
            objICOfficeColl.Query.Where(objICOfficeColl.Query.OfficeName = ICOffice.OfficeName.ToString And objICOfficeColl.Query.OffceType = "Company Office")
            objICOfficeColl.Query.Load()
            If objICOfficeColl.Query.Load Then
                rslt = True
            End If
            If rslt = False Then
                objICOfficeColl = New ICOfficeCollection
                objICOfficeColl.es.Connection.CommandTimeout = 3600
                objICOfficeColl.Query.Where(objICOfficeColl.Query.OfficeCode = ICOffice.OfficeCode.ToString And objICOfficeColl.Query.OffceType = "Company Office")
                objICOfficeColl.Query.Load()
                If objICOfficeColl.Query.Load Then
                    rslt = True
                End If
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function CheckDuplicateOnUpdate(ByVal ICOffice As ICOffice) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim objICOfficeColl As ICOfficeCollection
            'Dim collCompany As New ICCompanyCollection
            objICOfficeColl = New ICOfficeCollection
            objICOfficeColl.es.Connection.CommandTimeout = 3600
            objICOfficeColl.Query.Where(objICOfficeColl.Query.OfficeName = ICOffice.OfficeName.ToString And objICOfficeColl.Query.OffceType = "Company Office" And objICOfficeColl.Query.OfficeID <> ICOffice.OfficeID)
            objICOfficeColl.Query.Load()
            If objICOfficeColl.Query.Load Then
                rslt = True
            End If
            If rslt = False Then
                objICOfficeColl = New ICOfficeCollection
                objICOfficeColl.es.Connection.CommandTimeout = 3600
                objICOfficeColl.Query.Where(objICOfficeColl.Query.OfficeCode = ICOffice.OfficeCode.ToString And objICOfficeColl.Query.OffceType = "Company Office" And objICOfficeColl.Query.OfficeID <> ICOffice.OfficeID)
                objICOfficeColl.Query.Load()
                If objICOfficeColl.Query.Load Then
                    rslt = True
                End If
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Sub Clear()

        txtOfficeName.Text = ""
        txtOfficePhone1.Text = ""
        txtOfficePhone2.Text = ""
        txtOfficeAddress.Text = ""
        txtOffDescrption.Text = ""
        txtOfficeName.Text = ""
        txtOfficeEmail.Text = ""
        txtOfficeCode.Text = ""
        LoadddlCountry()
        LoadddlProvince()
        LoadddlCity()
        LoadddlGroup()
        LoadddlCompany()
        chkActive.Checked = True
        chkPrntLocation.Checked = False
    End Sub
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return Result
        End Try
    End Function
   
#End Region

End Class
