﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveCompanyOffice.ascx.vb" Inherits="DesktopModules_CompanyOfficeManagement_SaveCompanyOffice" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .style1
    {
        width: 25%;
        height: 23px;
    }
</style>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }

</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior2" EmptyMessage="type.."
        Type="Number">
    </telerik:NumericTextBoxSetting>
   
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="true"
        EmptyMessage="" ErrorMessage="Invalid Company Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCompanyName" />
        </TargetControls>
    </telerik:TextBoxSetting>
     <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehaviorPhone1" ValidationExpression="^\+?[\d- ]{2,15}$"
         ErrorMessage="Invalid Phone Number" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtOfficePhone1" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior3" ValidationExpression="^\+?[\d- ]{2,15}$"
        ErrorMessage="Invalid Phone Number" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtOfficePhone2" />
        </TargetControls>
        </telerik:RegExpTextBoxSetting>
         <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehaviorEmail" 
        EmptyMessage="" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.%]\w+)*\.\w+([-.]\w+)*$"
        ErrorMessage="Invalid Email">
        <TargetControls>
            <telerik:TargetInput ControlID="txtOfficeEmail" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
     <telerik:TextBoxSetting BehaviorID="RagExpBehaviorAddress" 
        ErrorMessage="Enter Address" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtOfficeAddress" />
        </TargetControls>
    </telerik:TextBoxSetting>
         <telerik:TextBoxSetting BehaviorID="RagExpBehaviorOffName" Validation-IsRequired="True"
        ErrorMessage="Invalid Office Name" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtOfficeName" />
        </TargetControls>
    </telerik:TextBoxSetting>
     <telerik:TextBoxSetting BehaviorID="RagExpBehaviorDescription"
         EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtOffDescrption" />
        </TargetControls>
    </telerik:TextBoxSetting>
     <telerik:RegExpTextBoxSetting BehaviorID="reOfficeCode" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9 -_]{1,10}$" ErrorMessage="Invalid Office Code"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtOfficeCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGrpCode" runat="server" Text="Select Group" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label12" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" 
                CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvGroup" runat="server" 
                ControlToValidate="ddlGroup" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Group" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvCompany" runat="server" 
                ControlToValidate="ddlCompany" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Company" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblOfficeCode" runat="server" Text="Office Code" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqOfficeCode" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompnyName" runat="server" Text="Office Name" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtOfficeCode" runat="server" CssClass="txtbox" 
                MaxLength="10"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtOfficeName" runat="server" CssClass="txtbox" 
                MaxLength="150"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            </td>
        <td align="left" valign="top" class="style1">
            </td>
        <td align="left" valign="top" class="style1">
            </td>
        <td align="left" valign="top" class="style1">
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyPhone1" runat="server" Text="Office Phone 1" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyPhone2" runat="server" Text="Office Phone 2" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtOfficePhone1" runat="server" CssClass="txtbox" 
                MaxLength="15"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtOfficePhone2" runat="server" CssClass="txtbox" 
                MaxLength="15"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyEmail" runat="server" Text="Office Email" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtOfficeEmail" runat="server" CssClass="txtbox" 
                MaxLength="100"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
        </td>
        <td align="left" valign="top" class="style1">
        </td>
        <td align="left" valign="top" class="style1">
        </td>
        <td align="left" valign="top" class="style1">
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblDescrption" runat="server" Text="Description" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" class="style1">
            &nbsp;</td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblOfficeAddress" runat="server" Text="Office Address" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" class="style1">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:TextBox ID="txtOffDescrption" runat="server" CssClass="txtbox" 
                MaxLength="500" TextMode="MultiLine" Height="70px"  
                onkeypress="return textboxMultilineMaxNumber(this,500)"></asp:TextBox>
        </td>
        <td align="left" valign="top" class="style1">
            &nbsp;</td>
        <td align="left" valign="top" class="style1">
            <asp:TextBox ID="txtOfficeAddress" runat="server" CssClass="txtbox" 
                MaxLength="250" TextMode="MultiLine" Height="70px"  
                onkeypress="return textboxMultilineMaxNumber(this,250)"></asp:TextBox>
        </td>
        <td align="left" valign="top" class="style1">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyCountry" runat="server" Text="Select Country" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label9" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyProvince" runat="server" Text="Select Province" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label10" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCountry" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlProvince" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvGroup0" runat="server" 
                ControlToValidate="ddlCountry" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Country" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvGroup1" runat="server" 
                ControlToValidate="ddlProvince" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Province" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyCity" runat="server" Text="Select City" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label11" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblPrintLocation" runat="server" Text="Is Printing Location" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCity" runat="server" 
                CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkPrntLocation" runat="server" Text=" " CssClass="chkBox" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvcity" runat="server" 
                ControlToValidate="ddlCity" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select City" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIsActive" runat="server" Text="Is Active" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkActive" runat="server" Text=" " CssClass="chkBox" Checked="True" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
            </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False"
                />
            &nbsp;
        </td>
    </tr>
</table>
