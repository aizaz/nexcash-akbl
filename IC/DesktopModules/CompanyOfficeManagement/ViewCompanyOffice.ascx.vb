﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_CompanyOfficeManagement_ViewCompanyOffice
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_CompanyOfficeManagement_ViewCompanyOffice_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then

                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnSaveOffice.Visible = CBool(htRights("Add"))
                btnDeleteOffice.Visible = CBool(htRights("Delete"))
                LoadddlGroup()
                LoadddlCompany()
                LoadOffice(0, Me.gvOffice.PageSize, True)

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client Location Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- ALL --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- ALL --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
#Region "Button Events"
    Protected Sub btnSaveBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveOffice.Click
        If Page.IsValid Then


            Response.Redirect(NavigateURL("SaveOffice", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If
    End Sub
#End Region
#Region "Other Functions/Routines"
   
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            ' str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function
   
#End Region
    Private Sub LoadOffice(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean)
        Try

            Dim GroupCode, CompanyCode As String
            If ddlGroup.SelectedValue.ToString <> "0" Then
                GroupCode = ddlGroup.SelectedValue.ToString
            Else
                GroupCode = ""
            End If
            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            Else
                CompanyCode = ""
            End If

            ICOfficeController.GetOfficeByCompanyCodeActiveAndApproveForRadGrid("Company Office", PageNumber, PageSize, DoDataBind, Me.gvOffice, CompanyCode, GroupCode)
            If gvOffice.Items.Count > 0 Then
                lblRNF.Visible = False
                gvOffice.Visible = True
                btnDeleteOffice.Visible = CBool(htRights("Delete"))
                gvOffice.Columns(5).Visible = CBool(htRights("Delete"))
            Else
                lblRNF.Visible = True
                gvOffice.Visible = False
                btnDeleteOffice.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
            LoadOffice(Me.gvOffice.CurrentPageIndex + 1, Me.gvOffice.PageSize, True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadOffice(Me.gvOffice.CurrentPageIndex + 1, Me.gvOffice.PageSize, True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvOffice_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvOffice.ItemCommand
        Try
            Dim objICCompanyOffice As New ICOffice

            objICCompanyOffice.es.Connection.CommandTimeout = 3600
            If e.CommandName.ToString = "del" Then
                If Page.IsValid Then
                    If objICCompanyOffice.LoadByPrimaryKey(e.CommandArgument.ToString) Then
                        ICOfficeController.DeleteOffice(objICCompanyOffice.OfficeID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, objICCompanyOffice.OffceType.ToString)
                        UIUtilities.ShowDialog(Me, "Delete Office", "Office deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                        LoadOffice(Me.gvOffice.CurrentPageIndex + 1, Me.gvOffice.PageSize, True)
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Error ", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                LoadOffice(Me.gvOffice.CurrentPageIndex + 1, Me.gvOffice.PageSize, False)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvOffice_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvOffice.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                ' Dim arrPageSizes() As String = {"1", "2", "5", "10", "50"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvOffice.MasterTableView.ClientID)
                Next
                'If gvAccountsDetails.Items.Count > 0 Then
                '    myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
                'End If
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvOffice_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvOffice.PageIndexChanged
        Try
            gvOffice.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvOffice_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvOffice.NeedDataSource
        Try
            LoadOffice(Me.gvOffice.CurrentPageIndex + 1, Me.gvOffice.PageSize, False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvOffice_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvOffice.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvOffice.MasterTableView.ClientID & "','0');"
        End If
    End Sub
    Private Function CheckGVCompanySelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvCompanyOfficeRow As GridDataItem In gvOffice.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvCompanyOfficeRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnDeleteOffice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteOffice.Click
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim PassToDeleteCount As Integer = 0
                Dim FailToDeleteCount As Integer = 0
                Dim objICCompanyOffice As ICOffice
                Dim OfficeID As String

                If CheckGVCompanySelected() = False Then
                    UIUtilities.ShowDialog(Me, "Delete Office", "Please select atleast one office.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVOffice In gvOffice.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVOffice.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        OfficeID = ""

                        OfficeID = rowGVOffice.GetDataKeyValue("OfficeID").ToString

                        objICCompanyOffice = New ICOffice
                        objICCompanyOffice.es.Connection.CommandTimeout = 3600
                        objICCompanyOffice.LoadByPrimaryKey(OfficeID.ToString)

                        Try
                            ICOfficeController.DeleteOffice(OfficeID, Me.UserId.ToString, Me.UserInfo.Username.ToString, objICCompanyOffice.OffceType.ToString)
                            PassToDeleteCount = PassToDeleteCount + 1
                        Catch ex As Exception
                            FailToDeleteCount = FailToDeleteCount + 1
                            Continue For

                        End Try

                    End If


                Next

                If Not PassToDeleteCount = 0 And Not FailToDeleteCount = 0 Then
                    LoadOffice(Me.gvOffice.CurrentPageIndex + 1, Me.gvOffice.PageSize, True)
                    UIUtilities.ShowDialog(Me, "Delete Office", "[ " & PassToDeleteCount.ToString & " ] Office(s) deleted successfuly.<br /> [ " & FailToDeleteCount.ToString & " ] Office(s) can not be deleted due to following reasons:<br /> 1. Deleted associated record(s) first.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToDeleteCount = 0 And FailToDeleteCount = 0 Then
                    LoadOffice(Me.gvOffice.CurrentPageIndex + 1, Me.gvOffice.PageSize, True)
                    UIUtilities.ShowDialog(Me, "Delete Office", PassToDeleteCount.ToString & " Office(s) deleted successfuly.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToDeleteCount = 0 And PassToDeleteCount = 0 Then
                    LoadOffice(Me.gvOffice.CurrentPageIndex + 1, Me.gvOffice.PageSize, True)
                    UIUtilities.ShowDialog(Me, "Delete Office", "[ " & FailToDeleteCount.ToString & " ] Office(s) can not be deleted due to following reasons: <br /> 1. Deleted associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
End Class
