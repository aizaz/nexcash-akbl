﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data
Imports System.Data.OleDb

Partial Class DesktopModules_BeneficiaryManagement_AddBulkBeneficiary
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
    Private BeneID As String

#Region "Page Load"
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            BeneID = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                If BeneID.ToString() = "0" Then
                    Clear()
                    lblBulkBeneList.Visible = True
                    gvBene.Visible = False
                    gvUnVarified.Visible = False
                    btnCancel.Visible = True
                    btnProceed.Visible = True
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Beneficiary Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        ddlGroup.ClearSelection()
        ddlCompany.ClearSelection()
        ddlPaymentNature.ClearSelection()
        ddlBeneGroup.ClearSelection()
        LoadddlGroup()
        LoadddlCompany()
        LoadPaymentNature()
        LoadBeneGroup()
        gvBene.Visible = False
        gvUnVarified.Visible = False
        btnAddBeneinBulk.Visible = False
        btnProceed.Visible = True
        hfFilePath.Value = Nothing
        ddlGroup.Enabled = True
        ddlCompany.Enabled = True
        ddlPaymentNature.Enabled = True
        ddlBeneGroup.Enabled = True
    End Sub
#End Region

#Region "Load Drop Down"

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
            LoadPaymentNature()
            LoadBeneGroup()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadPaymentNature()
            LoadBeneGroup()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlPaymentNature_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPaymentNature.SelectedIndexChanged
        Try
            LoadBeneGroup()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICBeneficiaryGroupManagementController.GetAllActiveGroupsForBene()
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICBeneficiaryGroupManagementController.GetAllActiveCompaniesByGroupCodeForBene(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadPaymentNature()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlPaymentNature.Items.Clear()
            ddlPaymentNature.Items.Add(lit)
            ddlPaymentNature.AppendDataBoundItems = True
            ddlPaymentNature.DataSource = ICBeneficiaryGroupManagementController.GetPaymentNatureByCompanyCode(ddlCompany.SelectedValue.ToString)
            ddlPaymentNature.DataTextField = "PaymentNatureName"
            ddlPaymentNature.DataValueField = "PaymentNatureCode"
            ddlPaymentNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadBeneGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBeneGroup.Items.Clear()
            ddlBeneGroup.Items.Add(lit)
            ddlBeneGroup.AppendDataBoundItems = True
            ddlBeneGroup.DataSource = ICBeneficiaryGroupManagementController.GetAllActiveandApproveBeneGroupByPNandCompanyCode(ddlPaymentNature.SelectedValue.ToString(), ddlCompany.SelectedValue.ToString())
            ddlBeneGroup.DataTextField = "BeneGroupName"
            ddlBeneGroup.DataValueField = "BeneGroupCode"
            ddlBeneGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Button Events"
    Protected Sub btnProceed_Click(sender As Object, e As EventArgs) Handles btnProceed.Click
        If Page.IsValid Then
            Try
                Dim str As String = System.IO.Path.GetExtension(fuBeneinBulk.PostedFile.FileName)
                Dim FileName As String = ""
                Dim FilePath As String = ""
                btnProceed.Visible = False
                btnCancel.Visible = True
                FilePath = ICUtilities.GetSettingValue("PhysicalApplicationPath") & "UploadedFiles\"
                If fuBeneinBulk.HasFile Then
                    If str = ".xls" Or str = ".xlsx" Then
                        FileName = Date.Now.ToString("ddMMyyyyHHmmss") & fuBeneinBulk.FileName.ToString().Replace(" ", "_")
                        fuBeneinBulk.SaveAs(FilePath.ToString() & "" & FileName.ToString())
                        hfFilePath.Value = FilePath.ToString() & "" & FileName.ToString()

                        ddlGroup.Enabled = False
                        ddlCompany.Enabled = False
                        ddlPaymentNature.Enabled = False
                        ddlBeneGroup.Enabled = False
                        ReadFileShowDetails()
                    Else
                        UIUtilities.ShowDialog(Me, "Save Bulk Beneficiary", "Please select the .xls or .xlsx file.", ICBO.IC.Dialogmessagetype.Failure)
                        btnProceed.Visible = True
                    End If
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Private Sub ReadFileShowDetails()
        Try
            Dim FilePath As String = hfFilePath.Value.ToString()
            Dim str As String = System.IO.Path.GetExtension(FilePath)
            Dim dsExcel As New DataSet
            Dim dtExcelRecords As New DataTable()
            Dim objICUtilities As New ICUtilities

            dsExcel = objICUtilities.ReadExcelFile(str.ToString(), FilePath.ToString(), "Sheet1$")
            If dsExcel.Tables.Count > 0 Then
                dtExcelRecords = dsExcel.Tables(0)
            End If
            FilterAndShowDetails(dtExcelRecords)
        Catch ex As Exception
            If ex.Message.Contains("'Sheet1$' is not a valid name") Then
                UIUtilities.ShowDialog(Me, "Bulk Beneficiary", "Invalid sheet name", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            Else
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End If
        End Try
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    Private Sub FilterAndShowDetails(ByVal dtExcel As DataTable)
        Try
            If dtExcel.Columns.Count <> 21 Then
                UIUtilities.ShowDialog(Me, "Save Bulk Beneficiary", "No of columns do not match.", ICBO.IC.Dialogmessagetype.Failure)
                btnProceed.Visible = True
                Exit Sub
            End If
            ViewState("dtFiltered") = Nothing
            ViewState("dtUnVarified") = Nothing
            Dim dtFiltered As New DataTable
            Dim dtUnVarified As New DataTable
            Dim dtBillInq As New DataTable
            Dim drExcel, drFiltered, drUnVarified As DataRow
            Dim IsValid As Boolean = True
            Dim PaymentNature, BeneficiaryGroupCode, CountryCode, ProvinceCode, CityCode As String
            Dim IBFTAccountTitle, FTAccountTitle, FTAccountBranchCode, FTAccountCurrency As String
            Dim IsCOTCExist As Boolean
            Dim IsBillPayExists As Boolean = False
            Dim OfficeName As String = ""
            Dim BeneCode As String = ""
            Dim BeneIBFTAccNo As String = ""
            Dim BeneFTAccNo As String = ""
            Dim BeneIDNo As String = ""
            Dim BeneName As String = ""
            Dim UBPDetails As String = ""
            Dim BankCode As Boolean = False
            Dim BankName As String = ""
            Dim UBPSCompanyID As String = ""
            Dim srtBeneficiaryCode As String()
            Dim srtProductTypeCode As String()
            Dim strBeneIBFTAccNo As String()
            Dim strBeneFTAccNo As String()
            Dim strBeneIDNo As String()
            Dim strUBPDetails As String()
            Dim drProductTypeCode As DataRow
            Dim dtProductTypeCode As New DataTable
            Dim dtUnTaggedProductTypeCode As New DataTable
            Dim dtValidProductTypeCode As New DataTable
            Dim dtInvalidPT As New DataTable
            Dim drUntaggedPT As DataRow
            Dim drvalidPT As DataRow
            Dim drInvalidPT As DataRow
            Dim RowNo As Integer = 0
            Dim ErrorMessage As String = ""
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryCode", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryName", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("ProductTypeCode", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryIDNo", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryBankCode", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryIBFTAccountNumber", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryFTAccountNumber", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryBankAddress", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryBranchCode", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryBranchName", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryBranchAddress", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryMobile", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryEmail", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryPhone", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryAddress", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryCountry", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryProvince", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryCity", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryBankName", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryIBFTAccountTitle", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryFTAccountTitle", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryFTAccountBranchCode", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryFTAccountCurrency", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("BeneficiaryIDType", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("UBPCompanyID", GetType(System.String)))
            dtFiltered.Columns.Add(New DataColumn("UBPRefNo", GetType(System.String)))

            dtUnVarified.Columns.Add(New DataColumn("RowNo", GetType(System.String)))
            dtUnVarified.Columns.Add(New DataColumn("ErrorMessage", GetType(System.String)))
            
            If dtExcel.Rows.Count > 0 Then
                For Each drExcel In dtExcel.Rows
                    dtProductTypeCode = New DataTable
                    dtUnTaggedProductTypeCode = New DataTable
                    dtValidProductTypeCode = New DataTable
                    dtInvalidPT = New DataTable
                    dtInvalidPT.Columns.Add("InvalidProductType")
                    dtProductTypeCode.Columns.Add("ProductTypeCode")
                    dtUnTaggedProductTypeCode.Columns.Add("ProductTypeCode")
                    dtValidProductTypeCode.Columns.Add("ProductTypeCode")


                    RowNo = RowNo + 1
                    ErrorMessage = ""
                    BeneficiaryGroupCode = ""
                    PaymentNature = ""
                    CountryCode = ""
                    ProvinceCode = ""
                    CityCode = ""
                    BankCode = False
                    BankName = ""
                    IBFTAccountTitle = ""
                    FTAccountTitle = ""
                    FTAccountBranchCode = ""
                    FTAccountCurrency = ""
                    UBPDetails = ""
                    UBPSCompanyID = ""
                    IsCOTCExist = False
                    IsValid = True
                    IsBillPayExists = False
                    dtBillInq = New DataTable


                    For Each dtCol As DataColumn In dtExcel.Columns

                        Dim FieldContent As String = drExcel(dtCol.ColumnName)
                        Dim reg As Regex = New Regex("<[^>]+>")
                        Dim check As Boolean = reg.IsMatch(FieldContent)

                        If check Then
                            ErrorMessage = dtCol.ColumnName & " is containing potentially dangerous value "
                            IsValid = False
                            drExcel(dtCol.ColumnName) = Server.HtmlEncode(drExcel(dtCol.ColumnName))
                        End If
                    Next

                    If IsValid = False Then
                        gvUnVarified.Visible = True
                        drUnVarified = dtUnVarified.NewRow()
                        drUnVarified(0) = RowNo.ToString()
                        drUnVarified(1) = ErrorMessage.ToString()
                        dtUnVarified.Rows.Add(drUnVarified)
                        Continue For
                    End If

                    If drExcel(0).ToString() = "" Then
                        IsValid = False
                        ErrorMessage += "Beneficiary Code is empty (" + (drExcel(0).ToString()) + ") ; "
                    ElseIf drExcel(0).ToString().Length() > 10 Then
                        IsValid = False
                        ErrorMessage += "Beneficiary Code length exceeded (" + (drExcel(0).ToString()) + ") ; "
                    ElseIf Regex.IsMatch(drExcel(0).ToString(), "^[a-zA-Z0-9_\-\/\\]{1,10}$") = False Then
                        IsValid = False
                        ErrorMessage += "Invalid Beneficiary Code (" + (drExcel(0).ToString()) + ") ; "
                    ElseIf drExcel(0).ToString() <> "" Then
                        Dim objBene As New ICBene
                        objBene.BeneficiaryCode = drExcel(0).ToString()
                        objBene.CompanyCode = ddlCompany.selectedvalue.tostring
                        If ICBeneficiaryManagementController.CheckDuplicateBeneForAdd(objBene) <> "OK" Then
                            IsValid = False
                            ErrorMessage += "Duplicate Beneficiary Code (" + (drExcel(0).ToString()) + ") ; "
                        End If

                        If Not ErrorMessage.Contains("Duplicate Beneficiary Code") Then
                            If BeneCode.ToString() <> "" Then
                                srtBeneficiaryCode = BeneCode.ToString.Split(",")
                                For cnt = 0 To srtBeneficiaryCode.Length - 1
                                    If srtBeneficiaryCode(cnt) = drExcel(0).ToString().Trim() Then
                                        IsValid = False
                                        ErrorMessage += "Duplicate Beneficiary Code in file (" + (drExcel(0).ToString()) + ") ; "
                                        BeneCode = BeneCode & "," & drExcel(0).ToString().Trim()
                                        Exit For
                                    End If
                                Next
                            End If
                            BeneCode = BeneCode & "," & drExcel(0).ToString().Trim()
                        End If
                    End If


                    If drExcel(1).ToString() = "" Then
                        IsValid = False
                        ErrorMessage += "Beneficiary Name is empty (" + (drExcel(1).ToString()) + ") ; "
                    ElseIf drExcel(1).ToString().Length() > 250 Then
                        IsValid = False
                        ErrorMessage += "Beneficiary Name length exceeded (" + (drExcel(1).ToString()) + ") ; "
                    ElseIf Regex.IsMatch(drExcel(1).ToString(), "^[a-zA-Z0-9 @._-]{1,250}$") = False Then
                        IsValid = False
                        ErrorMessage += "Invalid Beneficiary Name (" + (drExcel(1).ToString()) + ") ; "
                    End If


                    If drExcel(2).ToString() = "" Then
                        IsValid = False
                        ErrorMessage += "Product Type Code is empty (" + (drExcel(2).ToString()) + ") ; "
                    Else
                        Dim strProductTypeCode As String()
                        Dim PTCode As String = ""

                        If drExcel(2).ToString().Contains("|") Then
                            srtProductTypeCode = drExcel(2).ToString.Split("|")
                            Dim dtDuplicatePT As New DataTable
                            Dim drDuplicatePT As DataRow
                            dtDuplicatePT.Columns.Add("DuplicatePT")
                            Dim ISDuplicatePT As Boolean = False

                            For k As Integer = 0 To srtProductTypeCode.Length - 1
                                Dim DuplicatePT As Boolean = False
                                For Each drPT As DataRow In dtProductTypeCode.Rows
                                    If srtProductTypeCode(k) = drPT("ProductTypeCode") Then
                                        DuplicatePT = True
                                    End If
                                Next
                                If DuplicatePT = False Then
                                    If Regex.IsMatch(srtProductTypeCode(k), "^[a-zA-Z0-9]{1,10}$") = False Then
                                        drInvalidPT = dtInvalidPT.NewRow()
                                        drInvalidPT("InvalidProductType") = srtProductTypeCode(k)
                                        dtInvalidPT.Rows.Add(drInvalidPT)
                                    Else
                                        drProductTypeCode = dtProductTypeCode.NewRow()
                                        drProductTypeCode("ProductTypeCode") = srtProductTypeCode(k)
                                        dtProductTypeCode.Rows.Add(drProductTypeCode)
                                    End If

                                End If


                                '' Check duplicate Product Type in same row

                                For Each drPT As DataRow In dtDuplicatePT.Rows
                                    If srtProductTypeCode(k) = drPT("DuplicatePT") Then
                                        ISDuplicatePT = True
                                    End If
                                Next

                                If ISDuplicatePT = False Then
                                    If PTCode.ToString() <> "" Then
                                        strProductTypeCode = PTCode.ToString.Split(",")
                                        For cnt = 0 To strProductTypeCode.Length - 1
                                            If strProductTypeCode(cnt) = srtProductTypeCode(k) Then
                                                drDuplicatePT = dtDuplicatePT.NewRow()
                                                drDuplicatePT("DuplicatePT") = srtProductTypeCode(k)
                                                dtDuplicatePT.Rows.Add(drDuplicatePT)
                                            End If
                                        Next
                                    End If
                                    PTCode = PTCode & "," & srtProductTypeCode(k)
                                End If
                            Next
                            If dtDuplicatePT.Rows.Count > 0 Then
                                IsValid = False
                                ErrorMessage += "Duplicate Product Type Code in same row ("
                                For Each dr As DataRow In dtDuplicatePT.Rows
                                    ErrorMessage += dr("DuplicatePT") & ","
                                Next
                                ErrorMessage = ErrorMessage.Remove(ErrorMessage.Length - 1, 1) & ") ; "
                            End If

                        Else
                            If Regex.IsMatch(drExcel(2).ToString(), "^[a-zA-Z0-9]{1,10}$") = False Then
                                IsValid = False
                                ErrorMessage += "Invalid Product Type Code (" + (drExcel(2).ToString()) + ") ; "
                            Else
                                drProductTypeCode = dtProductTypeCode.NewRow()
                                drProductTypeCode("ProductTypeCode") = drExcel(2).ToString()
                                dtProductTypeCode.Rows.Add(drProductTypeCode)
                            End If
                        End If

                        If dtInvalidPT.Rows.Count > 0 Then
                            IsValid = False
                            ErrorMessage += "Invalid Product Type Code ("
                            For Each dr As DataRow In dtInvalidPT.Rows
                                ErrorMessage += dr("InvalidProductType") & ","
                            Next
                            ErrorMessage = ErrorMessage.Remove(ErrorMessage.Length - 1, 1) & ") ; "
                        End If

                        Dim dtAPN As New DataTable
                        Dim i As Integer = 0

                        dtAPN = ICAccountPaymentNatureProductTypeController.GetAllAPNProductTypeByCompanyCodeandPNCode(ddlCompany.SelectedValue.ToString(), ddlPaymentNature.SelectedValue.ToString())

                        For Each drPT In dtProductTypeCode.Rows
                            If dtAPN.Rows.Count > 0 Then
                                Dim IsProductTypeAllowed As Boolean = False
                                Dim ProductTypeCode As String = ""
                                For Each dr As DataRow In dtAPN.Rows
                                    ProductTypeCode = drPT("ProductTypeCode")
                                    If dr("ProductTypeCode") = drPT("ProductTypeCode") Then
                                        IsProductTypeAllowed = True
                                    End If
                                Next
                                If IsProductTypeAllowed = False Then
                                    drUntaggedPT = dtUnTaggedProductTypeCode.NewRow()
                                    drUntaggedPT("ProductTypeCode") = ProductTypeCode.ToString()
                                    dtUnTaggedProductTypeCode.Rows.Add(drUntaggedPT)
                                Else
                                    '' Remove duplication in Product Type Code
                                    Dim IsDuplicate As Boolean = False
                                    For Each dr1 As DataRow In dtValidProductTypeCode.Rows
                                        If drPT("ProductTypeCode") = dr1("ProductTypeCode") Then
                                            IsDuplicate = True
                                        End If
                                    Next
                                    If IsDuplicate = False Then
                                        drvalidPT = dtValidProductTypeCode.NewRow()
                                        drvalidPT("ProductTypeCode") = ProductTypeCode.ToString()
                                        dtValidProductTypeCode.Rows.Add(drvalidPT)
                                    End If
                                End If
                            Else
                                IsValid = False
                                ErrorMessage += "Product Type Code is not tagged with selected Company and Payment Nature ("
                                For Each dr As DataRow In dtProductTypeCode.Rows
                                    ErrorMessage += dr("ProductTypeCode") & ","
                                Next
                                ErrorMessage = ErrorMessage.Remove(ErrorMessage.Length - 1, 1) & ") ; "
                            End If
                        Next

                        If dtUnTaggedProductTypeCode.Rows.Count > 0 Then
                            IsValid = False
                            ErrorMessage += "Product Type Code is not tagged with selected Company and Payment Nature ("
                            For Each dr As DataRow In dtUnTaggedProductTypeCode.Rows
                                ErrorMessage += dr("ProductTypeCode") & ","
                            Next
                            ErrorMessage = ErrorMessage.Remove(ErrorMessage.Length - 1, 1) & ") ; "
                        End If

                        If dtValidProductTypeCode.Rows.Count > 0 Then
                            For Each dr As DataRow In dtValidProductTypeCode.Rows
                                Dim objProductType As New ICProductType
                                If objProductType.LoadByPrimaryKey(dr("ProductTypeCode")) Then
                                    If objProductType.DisbursementMode = "Cheque" Or objProductType.DisbursementMode = "PO" Or objProductType.DisbursementMode = "DD" Then

                                        '' Nothing to do

                                    End If


                                    If objProductType.DisbursementMode = "COTC" Then
                                        If drExcel(3).ToString() <> "" Then
                                            If drExcel(3).ToString().Length() > 50 Then
                                                IsValid = False
                                                ErrorMessage += "Beneficiary Identification No. length exceeded (" + (drExcel(3).ToString()) + ") ; "
                                            ElseIf Regex.IsMatch(drExcel(3).ToString().Trim, "^[a-zA-Z0-9-]{1,50}$") = False Then
                                                IsValid = False
                                                ErrorMessage += "Invalid Beneficiary Identification No. (" + (drExcel(3).ToString()) + ") ; "
                                            Else
                                                If BeneIDNo.ToString() <> "" Then
                                                    strBeneIDNo = BeneIDNo.ToString.Split(",")
                                                    For cnt = 0 To strBeneIDNo.Length - 1
                                                        If strBeneIDNo(cnt) = drExcel(3).ToString().Trim() Then
                                                            IsValid = False
                                                            ErrorMessage += "Duplicate Beneficiary Identification Number in file (" + (drExcel(3).ToString()) + ") ; "
                                                            BeneIDNo = BeneIDNo & "," & drExcel(3).ToString().Trim()
                                                            Exit For
                                                        End If
                                                    Next
                                                End If
                                                BeneIDNo = BeneIDNo & "," & drExcel(3).ToString().Trim()
                                            End If
                                        Else
                                            IsValid = False
                                            ErrorMessage += "Beneficiary Identification No. is empty ; "
                                        End If
                                        If drExcel(18).ToString() <> "" Then
                                            If drExcel(18).ToString().Length() > 50 Then
                                                IsValid = False
                                                ErrorMessage += "Beneficiary Identification Type length exceeded (" + (drExcel(9).ToString()) + ") ; "
                                            ElseIf Regex.IsMatch(drExcel(18).ToString().Trim, "^[a-zA-Z0-9-]{1,50}$") = False Then
                                                IsValid = False
                                                ErrorMessage += "Invalid Beneficiary Identification Type (" + (drExcel(18).ToString()) + ") ; "
                                            ElseIf drExcel(18).ToString().ToLower <> "cnic" And drExcel(18).ToString().ToLower <> "passport" And drExcel(18).ToString().ToLower <> "nicop" And drExcel(18).ToString().ToLower <> "driving license" Then
                                                IsValid = False
                                                ErrorMessage += "Invalid value of Beneficiary Identification Type (" + (drExcel(18).ToString()) + ") ; "
                                            Else
                                                Dim objBene As New ICBene
                                                objBene.BeneficiaryIDNo = drExcel(3).ToString()
                                                objBene.BeneficiaryIDType = drExcel(18).ToString()
                                                objBene.CompanyCode = ddlCompany.SelectedValue.ToString
                                                If ICBeneficiaryManagementController.CheckDuplicateBeneForAdd(objBene) <> "OK" Then
                                                    IsValid = False
                                                    ErrorMessage += "Duplicate Beneficiary Identification No. and Identification Type (" + (drExcel(3).ToString()) + ", " + (drExcel(18).ToString()) + ") ; "
                                                End If

                                                IsCOTCExist = True
                                            End If
                                        Else
                                            IsValid = False
                                            ErrorMessage += "Beneficiary Identification Type is empty ; "
                                        End If

                                        '' Pick Up Location

                                        If drExcel(8).ToString() <> "" Then
                                            If drExcel(8).ToString().Length() > 10 Then
                                                IsValid = False
                                                ErrorMessage += "Pick Up Location length exceeded (" + (drExcel(8).ToString()) + ") ; "
                                            ElseIf Regex.IsMatch(drExcel(8).ToString().Trim, "^[a-zA-Z0-9 -_]{1,10}$") = False Then
                                                IsValid = False
                                                ErrorMessage += "Invalid Pick Up Location (" + (drExcel(8).ToString()) + ") ; "
                                            Else
                                                OfficeName = ICOfficeController.GetOfficeNameByOfficeCode(drExcel(8).ToString())

                                                If OfficeName = "" Then
                                                    IsValid = False
                                                    ErrorMessage += "Invalid Pick Up Location  (" + (drExcel(8).ToString()) + ") ; "
                                                End If

                                                IsCOTCExist = True
                                            End If
                                        Else
                                            'IsValid = False
                                            'ErrorMessage += "Pick Up Location is empty ; "
                                        End If

                                    End If
                                    If objProductType.DisbursementMode = "Bill Payment" Then
                                        If drExcel(19).ToString() <> "" Then
                                            If drExcel(19).ToString.Length <= 10 Then
                                                UBPSCompanyID = ICUBPSCompanyController.GetUBPCompanyIDByCompanyCodeor(drExcel(19))
                                                If UBPSCompanyID = "0" Then
                                                    IsValid = False
                                                    ErrorMessage += "Invalid UBPS Company Code (" & drExcel(19) & ") ; "
                                                End If

                                            Else
                                                IsValid = False
                                                ErrorMessage += "Invalid UBPS Company code Length (" & drExcel(19).ToString & ") ; "
                                            End If
                                        Else
                                            IsValid = False
                                            ErrorMessage += "UBPS Company Code is empty ; "
                                        End If
                                        If drExcel(20).ToString() <> "" Then
                                            If drExcel(19).ToString <> "" And IsValid = True Then

                                                Dim objICCompany As New ICUBPSCompany
                                                objICCompany.LoadByPrimaryKey(UBPSCompanyID)
                                                If ICUBPSCompanyController.CheckReferenceFieldLength(drExcel(20).ToString().Length, UBPSCompanyID) = True Then
                                                    If ICUBPSCompanyController.CheckReferenceFieldDataWithRegularExpression(drExcel(20).ToString(), UBPSCompanyID) = False Then
                                                        IsValid = False
                                                        ErrorMessage += "Invalid UBPS Reference No; "
                                                    Else
                                                        If UBPDetails.ToString() <> "" Then
                                                            strUBPDetails = BeneIDNo.ToString.Split(",")
                                                            For cnt = 0 To strUBPDetails.Length - 1
                                                                If strUBPDetails(cnt) = drExcel(19).ToString().Trim() & "-" & drExcel(20).ToString().Trim() Then
                                                                    IsValid = False
                                                                    ErrorMessage += "Duplicate Bill Payment Details (" + (drExcel(19).ToString()) + ")-(" + (drExcel(20).ToString()) + "); "
                                                                    UBPDetails = UBPDetails & "," & drExcel(19).ToString().Trim() & "-" & drExcel(20).ToString().Trim()
                                                                    Exit For
                                                                End If
                                                            Next
                                                        End If
                                                        UBPDetails = UBPDetails & "," & drExcel(19).ToString().Trim() & "-" & drExcel(20).ToString().Trim()
                                                        Dim objBene As New ICBene
                                                        objBene.UBPCompanyID = UBPSCompanyID
                                                        objBene.UBPReferenceNo = drExcel(20).ToString.Trim
                                                        objBene.CompanyCode = ddlCompany.selectedvalue.tostring
                                                        If ICBeneficiaryManagementController.CheckDuplicateBeneForAdd(objBene) <> "OK" Then
                                                            IsValid = False
                                                            ErrorMessage += "Duplicate UBPS Company and Reference No (" + (drExcel(19).ToString()) + ", " + (drExcel(20).ToString()) + ") ; "
                                                        Else


                                                            Dim objICUBPS As New ICUBPSCompany
                                                            objICUBPS.LoadByPrimaryKey(UBPSCompanyID)

                                                            If objICCompany.IsBillingInquiry = True Then
                                                                Try
                                                                    Dim collObjICAccounts As New ICAccountsCollection
                                                                    Dim RRNo As String = Nothing

                                                                    collObjICAccounts.Query.Where(collObjICAccounts.Query.CompanyCode = ddlCompany.SelectedValue.ToString)
                                                                    collObjICAccounts.Query.Where(collObjICAccounts.Query.IsActive = True And collObjICAccounts.Query.IsApproved = True)
                                                                    collObjICAccounts.Query.es.Top = 1
                                                                    collObjICAccounts.Query.Load()


                                                                    If collObjICAccounts.Count = 0 Then
                                                                        IsValid = False
                                                                        ErrorMessage += "Client accounts are not active to perform bill inquiry"
                                                                    Else
                                                                        'Change UBPSCompanyCode to UBPSCompanyID    
                                                                        dtBillInq = CBUtilities.BillInquiryForBene("Beneficiary Bill Inquiry", drExcel(20).ToString.Trim, "", UBPSCompanyID.ToString, drExcel(20).ToString.Trim, "", 0, RRNo, collObjICAccounts(0).BranchCode, collObjICAccounts(0).AccountNumber)
                                                                        If dtBillInq.Rows.Count > 0 Then
                                                                            IsBillPayExists = True
                                                                        Else
                                                                            IsValid = False
                                                                            ErrorMessage += "No billing data found against ( " & drExcel(19).ToString.Trim & " )( " & drExcel(20).ToString.Trim & " )"
                                                                        End If
                                                                    End If



                                                                Catch ex As Exception
                                                                    IsValid = False
                                                                    ErrorMessage += "Fail to Perform bill inquiry"
                                                                End Try
                                                            Else
                                                                IsBillPayExists = True
                                                            End If






                                                        End If

                                                    End If
                                                Else
                                                    IsValid = False
                                                    ErrorMessage += "Invalid UBPS Reference No is Length ; "
                                                End If
                                            Else
                                                IsValid = False
                                            End If
                                        Else
                                            IsValid = False
                                            ErrorMessage += "UBPS Reference No is empty ; "
                                        End If
                                    End If

                                    If objProductType.DisbursementMode = "Other Credit" Then
                                        If drExcel(4).ToString() <> "" Then
                                            If drExcel(4).ToString().Length() > 10 Then
                                                IsValid = False
                                                ErrorMessage += "Beneficiary Bank Code length exceeded (" + (drExcel(4).ToString()) + ") ; "
                                            ElseIf Regex.IsMatch(drExcel(4).ToString().Trim, "^[0-9]{1,10}$") = False Then
                                                IsValid = False
                                                ErrorMessage += "Invalid Beneficiary Bank Code (" + (drExcel(4).ToString()) + ") ; "
                                            Else
                                                BankCode = ICBankController.IsActiveApprovedNonPrincipalAndIBFTEnabledBankExistByBankCode(drExcel(4).ToString()).ToString()
                                                If BankCode = False Then
                                                    IsValid = False
                                                    ErrorMessage += "Invalid Beneficiary Bank Code (" + (drExcel(4).ToString()) + ") for IBFT ; "
                                                Else
                                                    BankName = ICBankController.GetBankNameByBankCode(drExcel(4).ToString())
                                                End If


                                                If drExcel(5).ToString() <> "" Then
                                                    If drExcel(5).ToString().Length() > 100 Then
                                                        IsValid = False
                                                        ErrorMessage += "Beneficiary IBFT Account Number length exceeded (" + (drExcel(5).ToString()) + ") ; "
                                                    ElseIf Regex.IsMatch(drExcel(5).ToString().Trim, "^[a-zA-Z0-9]{2,100}$") = False Then
                                                        IsValid = False
                                                        ErrorMessage += "Invalid Beneficiary IBFT Account Number (" + (drExcel(5).ToString()) + ") ; "
                                                    Else
                                                        Dim objBene As New ICBene
                                                        objBene.BeneficiaryIBFTAccountNo = drExcel(5).ToString()
                                                        objBene.CompanyCode = ddlCompany.selectedvalue.tostring
                                                        If ICBeneficiaryManagementController.CheckDuplicateBeneForAdd(objBene) <> "OK" Then
                                                            IsValid = False
                                                            ErrorMessage += "Duplicate Beneficiary IBFT Account Number (" + (drExcel(5).ToString()) + ") ; "
                                                        End If

                                                        If Not ErrorMessage.Contains("Duplicate Beneficiary IBFT Account Number") Then
                                                            If BeneIBFTAccNo.ToString() <> "" Then
                                                                strBeneIBFTAccNo = BeneIBFTAccNo.ToString.Split(",")
                                                                For cnt = 0 To strBeneIBFTAccNo.Length - 1
                                                                    If strBeneIBFTAccNo(cnt) = drExcel(5).ToString().Trim() Then
                                                                        IsValid = False
                                                                        ErrorMessage += "Duplicate Beneficiary IBFT Account Number in file (" + (drExcel(5).ToString()) + ") ; "
                                                                        BeneIBFTAccNo = BeneIBFTAccNo & "," & drExcel(5).ToString().Trim()
                                                                        Exit For
                                                                    End If
                                                                Next
                                                            End If
                                                            BeneIBFTAccNo = BeneIBFTAccNo & "," & drExcel(5).ToString().Trim()

                                                            If Not ErrorMessage.Contains("Duplicate Beneficiary IBFT Account Number in file") Then
                                                                Try
                                                                    Dim AccTitle As String = Nothing
                                                                    ' Dim Remarks As String = Nothing
                                                                    Dim objBank As New ICBank
                                                                    If drExcel(4).ToString() <> "" Then
                                                                        If objBank.LoadByPrimaryKey(drExcel(4).ToString()) Then
                                                                            Dim BankIMD As String = Nothing
                                                                            BankIMD = objBank.BankIMD
                                                                            If Not BankIMD Is Nothing Then
                                                                                If drExcel(5).ToString() <> "" Then
                                                                                    If CBUtilities.IsNormalAccountNoOrIBAN(drExcel(5).ToString()) Then
                                                                                        If CBUtilities.ValidateIBAN(drExcel(5).ToString()) Then
                                                                                            If BankIMD.ToString <> "" Then
                                                                                                If drExcel(5).ToString() <> "" Then
                                                                                                    Dim collobjICAccounts As New ICAccountsCollection
                                                                                                    collobjICAccounts.Query.Where(collobjICAccounts.Query.CompanyCode = ddlCompany.SelectedValue.ToString)
                                                                                                    collobjICAccounts.Query.Load()
                                                                                                    If collobjICAccounts.Count > 0 Then
                                                                                                        AccTitle = CBUtilities.IBFTTitleFetch(BankIMD.ToString().Trim, drExcel(5).ToString(), ICBO.CBUtilities.AccountType.RB, "Beneficiary Account", drExcel(5).ToString(), "", "", 0, "", "", "", objBank.BankID, objBank.BankName, collobjICAccounts(0).BranchCode & collobjICAccounts(0).AccountNumber, ICBankController.GetPrincipleBank.BankIMD)
                                                                                                        IBFTAccountTitle = AccTitle
                                                                                                    Else
                                                                                                        IsValid = False
                                                                                                        ErrorMessage += "Invalid Client Account Number to fetch IBFT Account Title; "
                                                                                                    End If

                                                                                                    ' Remarks = "-"
                                                                                                Else
                                                                                                    'Remarks = "Invalid Account Number"
                                                                                                    'UIUtilities.ShowDialog(Me, "Error", Remarks, ICBO.IC.Dialogmessagetype.Failure)
                                                                                                    IsValid = False
                                                                                                    ErrorMessage += "Invalid Account Number ; "
                                                                                                End If
                                                                                            Else
                                                                                                'Remarks = "Invalid Bank IMD"
                                                                                                'UIUtilities.ShowDialog(Me, "Error", Remarks, ICBO.IC.Dialogmessagetype.Failure)
                                                                                                IsValid = False
                                                                                                ErrorMessage += "Invalid Bank IMD ; "
                                                                                            End If
                                                                                        Else
                                                                                            'Remarks = "Invalid International Bank Account Number (IBAN)"
                                                                                            'UIUtilities.ShowDialog(Me, "Error", Remarks, ICBO.IC.Dialogmessagetype.Failure)
                                                                                            IsValid = False
                                                                                            ErrorMessage += "Invalid International Bank Account Number (IBAN) ; "
                                                                                        End If
                                                                                    Else
                                                                                        If BankIMD.ToString <> "" Then
                                                                                            Dim collobjICAccounts As New ICAccountsCollection
                                                                                            collobjICAccounts.Query.Where(collobjICAccounts.Query.CompanyCode = ddlCompany.SelectedValue.ToString)
                                                                                            collobjICAccounts.Query.Load()
                                                                                            If collobjICAccounts.Count > 0 Then
                                                                                                AccTitle = CBUtilities.IBFTTitleFetch(BankIMD.ToString().Trim, drExcel(5).ToString(), ICBO.CBUtilities.AccountType.RB, "Beneficiary Account", drExcel(5).ToString(), "", "", 0, "", "", "", objBank.BankID, objBank.BankName, collobjICAccounts(0).BranchCode & collobjICAccounts(0).AccountNumber, ICBankController.GetPrincipleBank.BankIMD)
                                                                                                IBFTAccountTitle = AccTitle
                                                                                            Else
                                                                                                IsValid = False
                                                                                                ErrorMessage += "Invalid Client Account to fetch beneficiary account title ; "
                                                                                            End If
                                                                                            ' Remarks = "-"
                                                                                        Else
                                                                                            'Remarks = "Invalid Bank IMD"
                                                                                            'UIUtilities.ShowDialog(Me, "Error", Remarks, ICBO.IC.Dialogmessagetype.Failure)
                                                                                            IsValid = False
                                                                                            ErrorMessage += "Invalid Bank IMD ; "
                                                                                        End If
                                                                                    End If
                                                                                Else
                                                                                    'Remarks = "Invalid Account Number"
                                                                                    'UIUtilities.ShowDialog(Me, "Error", Remarks, ICBO.IC.Dialogmessagetype.Failure)
                                                                                    IsValid = False
                                                                                    ErrorMessage += "Invalid Account Number ; "
                                                                                End If
                                                                            Else
                                                                                IsValid = False
                                                                                ErrorMessage += "Invalid Bank IMD ; "
                                                                            End If
                                                                        Else
                                                                            IsValid = False
                                                                            ErrorMessage += "Invalid Bank Code ; "
                                                                        End If
                                                                    Else
                                                                        IsValid = False
                                                                        ErrorMessage += "Beneficiary Bank Code is empty; "
                                                                    End If
                                                                Catch ex As Exception
                                                                    'ProcessModuleLoadException(Me, ex, False)
                                                                    'UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                                                                    IsValid = False
                                                                    ErrorMessage += ex.Message + " ; "
                                                                End Try
                                                            End If
                                                        End If
                                                    End If
                                                Else
                                                    IsValid = False
                                                    ErrorMessage += "Beneficiary IBFT Account Number is empty ; "
                                                End If

                                            End If
                                        Else
                                            IsValid = False
                                            ErrorMessage += "Beneficiary Bank Code is empty ; "
                                        End If


                                    End If


                                    If objProductType.DisbursementMode = "Direct Credit" Then
                                        If drExcel(6).ToString() <> "" Then
                                            If drExcel(6).ToString().Length() > 100 Then
                                                IsValid = False
                                                ErrorMessage += "Beneficiary FT Account Number length exceeded (" + (drExcel(6).ToString()) + ") ; "
                                            ElseIf Regex.IsMatch(drExcel(6).ToString().Trim, "^[a-zA-Z0-9]{2,100}$") = False Then
                                                IsValid = False
                                                ErrorMessage += "Invalid Beneficiary FT Account Number (" + (drExcel(6).ToString()) + ") ; "
                                            Else
                                                Dim objBene As New ICBene
                                                objBene.BeneficiaryAccountNo = drExcel(6).ToString()
                                                objBene.CompanyCode = ddlCompany.selectedvalue.tostring
                                                If ICBeneficiaryManagementController.CheckDuplicateBeneForAdd(objBene) <> "OK" Then
                                                    IsValid = False
                                                    ErrorMessage += "Duplicate Beneficiary FT Account Number (" + (drExcel(6).ToString()) + ") ; "
                                                End If

                                                If Not ErrorMessage.Contains("Duplicate Beneficiary FT Account Number") Then
                                                    If BeneFTAccNo.ToString() <> "" Then
                                                        strBeneFTAccNo = BeneFTAccNo.ToString.Split(",")
                                                        For cnt = 0 To strBeneFTAccNo.Length - 1
                                                            If strBeneFTAccNo(cnt) = drExcel(6).ToString().Trim() Then
                                                                IsValid = False
                                                                ErrorMessage += "Duplicate Beneficiary FT Account Number in file (" + (drExcel(6).ToString()) + ") ; "
                                                                BeneFTAccNo = BeneFTAccNo & "," & drExcel(6).ToString().Trim()
                                                                Exit For
                                                            End If
                                                        Next
                                                    End If
                                                    BeneFTAccNo = BeneFTAccNo & "," & drExcel(6).ToString().Trim()

                                                    If Not ErrorMessage.Contains("Duplicate Beneficiary FT Account Number in file") Then
                                                        Try
                                                            Dim StrAccountDetails As String()
                                                            Dim AccountStatus As String = ""
                                                            Dim BBAccNo As String = Nothing
                                                            If CBUtilities.IsNormalAccountNoOrIBAN(drExcel(6).ToString()) = True Then
                                                                If CBUtilities.ValidateIBAN(drExcel(6).ToString()) = True Then
                                                                    BBAccNo = CBUtilities.GetBBANFromIBAN(drExcel(6).ToString())
                                                                    AccountStatus = CBUtilities.AccountStatus(BBAccNo, ICBO.CBUtilities.AccountType.RB, "Beneficiary Account Title", drExcel(6).ToString(), "Add", "Add Beneficiary", "").ToString
                                                                    StrAccountDetails = (CBUtilities.TitleFetch(BBAccNo, ICBO.CBUtilities.AccountType.RB, "Beneficiary Account Title", drExcel(6).ToString()).ToString.Split(";"))
                                                                    If AccountStatus = "Active" Then
                                                                        FTAccountTitle = StrAccountDetails(0).ToString
                                                                        FTAccountBranchCode = StrAccountDetails(1).ToString
                                                                        FTAccountCurrency = StrAccountDetails(2).ToString
                                                                    Else
                                                                        ' UIUtilities.ShowDialog(Me, "Warning", AccountStatus.ToString, ICBO.IC.Dialogmessagetype.Warning)
                                                                        'If StrAccountDetails(0).ToString <> "" And StrAccountDetails(1).ToString <> "" And StrAccountDetails(2).ToString <> "" Then
                                                                        '    FTAccountTitle = StrAccountDetails(0).ToString
                                                                        '    FTAccountBranchCode = StrAccountDetails(1).ToString
                                                                        '    FTAccountCurrency = StrAccountDetails(2).ToString
                                                                        'Else
                                                                        '    IsValid = False
                                                                        '    ErrorMessage += AccountStatus.ToString() + " ; "
                                                                        'End If
                                                                        IsValid = False
                                                                        ErrorMessage += "Beneficiary account is not active ; "
                                                                    End If
                                                                Else
                                                                    IsValid = False
                                                                    ErrorMessage += "Invalid IBAN ; "
                                                                    'UIUtilities.ShowDialog(Me, "Error", "Invalid IBAN", ICBO.IC.Dialogmessagetype.Failure)
                                                                    'Exit Sub
                                                                End If
                                                            Else

                                                                AccountStatus = CBUtilities.AccountStatus(drExcel(6).ToString(), ICBO.CBUtilities.AccountType.RB, "Beneficiary Account Status", drExcel(6).ToString(), "Add", "Add Bene", "").ToString
                                                                StrAccountDetails = (CBUtilities.TitleFetch(drExcel(6).ToString(), ICBO.CBUtilities.AccountType.RB, "IC Client Type", drExcel(6).ToString()).ToString.Split(";"))
                                                                If AccountStatus = "Active" Then
                                                                    FTAccountTitle = StrAccountDetails(0).ToString
                                                                    FTAccountBranchCode = StrAccountDetails(1).ToString
                                                                    FTAccountCurrency = StrAccountDetails(2).ToString
                                                                Else
                                                                    'UIUtilities.ShowDialog(Me, "Warning", AccountStatus.ToString, ICBO.IC.Dialogmessagetype.Warning)
                                                                    'FTAccountTitle = StrAccountDetails(0).ToString
                                                                    'FTAccountBranchCode = StrAccountDetails(1).ToString
                                                                    'FTAccountCurrency = StrAccountDetails(2).ToString


                                                                    'If StrAccountDetails(0).ToString <> "" And StrAccountDetails(1).ToString <> "" And StrAccountDetails(2).ToString <> "" Then
                                                                    '    FTAccountTitle = StrAccountDetails(0).ToString
                                                                    '    FTAccountBranchCode = StrAccountDetails(1).ToString
                                                                    '    FTAccountCurrency = StrAccountDetails(2).ToString
                                                                    'Else
                                                                    '    IsValid = False
                                                                    '    ErrorMessage += AccountStatus.ToString() + " ; "
                                                                    'End If

                                                                    IsValid = False
                                                                    ErrorMessage += "Beneficiary account is not active ; "
                                                                End If
                                                            End If
                                                        Catch ex As Exception
                                                            'ProcessModuleLoadException(Me, ex, False)
                                                            'UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                                                            IsValid = False
                                                            ErrorMessage += ex.Message + " ; "
                                                        End Try
                                                    End If
                                                End If
                                            End If
                                        Else
                                            IsValid = False
                                            ErrorMessage += "Beneficiary FT Account Number is empty ; "
                                        End If
                                    End If
                                End If
                            Next
                        End If
                    End If


                    If drExcel(7).ToString() <> "" Then
                        If drExcel(7).ToString().Length() > 250 Then
                            IsValid = False
                            ErrorMessage += "Beneficiary Bank Address length exceeded (" + (drExcel(7).ToString()) + ") ; "
                        End If
                    End If


                    If drExcel(8).ToString() <> "" Then
                        If drExcel(8).ToString().Length() > 10 Then
                            IsValid = False
                            ErrorMessage += "Beneficiary Branch Code length exceeded (" + (drExcel(8).ToString()) + ") ; "
                        ElseIf Regex.IsMatch(drExcel(8).ToString().Trim, "^[a-zA-Z0-9 -_]{1,10}$") = False Then
                            IsValid = False
                            ErrorMessage += "Invalid Beneficiary Branch Code (" + (drExcel(8).ToString()) + ") ; "
                        End If
                    End If


                    If drExcel(9).ToString() <> "" Then
                        If drExcel(9).ToString().Length() > 250 Then
                            IsValid = False
                            ErrorMessage += "Beneficiary Branch Name length exceeded (" + (drExcel(9).ToString()) + ") ; "
                        ElseIf Regex.IsMatch(drExcel(9).ToString().Trim, "^[a-zA-Z0-9 ]{1,250}$") = False Then
                            IsValid = False
                            ErrorMessage += "Invalid Beneficiary Branch Name (" + (drExcel(9).ToString()) + ") ; "
                        End If
                    End If


                    If drExcel(10).ToString() <> "" Then
                        If drExcel(10).ToString().Length() > 250 Then
                            IsValid = False
                            ErrorMessage += "Beneficiary Branch Address length exceeded (" + (drExcel(10).ToString()) + ") ; "
                        End If
                    End If


                    If drExcel(11).ToString() <> "" Then
                        If drExcel(11).ToString().Length() > 20 Then
                            IsValid = False
                            ErrorMessage += "Beneficiary Mobile length exceeded (" + (drExcel(11).ToString()) + ") ; "
                        ElseIf Regex.IsMatch(drExcel(11).ToString().Trim, "^\+[0-9]{2,20}$") = False Then
                            IsValid = False
                            ErrorMessage += "Invalid Beneficiary Mobile (" + (drExcel(11).ToString()) + ") ; "
                        End If
                    End If


                    If drExcel(12).ToString() <> "" Then
                        If drExcel(12).ToString().Length() > 100 Then
                            IsValid = False
                            ErrorMessage += "Beneficiary Email length exceeded (" + (drExcel(12).ToString()) + ") ; "
                        ElseIf Regex.IsMatch(drExcel(12).ToString().Trim, "^([a-zA-Z0-9_\-\.]+)@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$") = False Then
                            IsValid = False
                            ErrorMessage += "Invalid Beneficiary Email (" + (drExcel(12).ToString()) + ") ; "
                        End If
                    End If


                    If drExcel(13).ToString() <> "" Then
                        If drExcel(13).ToString().Length() > 15 Then
                            IsValid = False
                            ErrorMessage += "Beneficiary Phone length exceeded (" + (drExcel(13).ToString()) + ") ; "
                        ElseIf Regex.IsMatch(drExcel(13).ToString().Trim, "^\+?[\d- ]{2,15}$") = False Then
                            IsValid = False
                            ErrorMessage += "Invalid Beneficiary Phone (" + (drExcel(13).ToString()) + ") ; "
                        End If
                    End If


                    If drExcel(14).ToString() <> "" Then
                        If drExcel(14).ToString().Length() > 250 Then
                            IsValid = False
                            ErrorMessage += "Beneficiary Address length exceeded (" + (drExcel(14).ToString()) + ") ; "
                        End If
                    End If


                    If drExcel(15).ToString() <> "" Then
                        If drExcel(15).ToString().Length() > 125 Then
                            IsValid = False
                            ErrorMessage += "Beneficiary Country length exceeded (" + (drExcel(15).ToString()) + ") ; "
                        ElseIf Regex.IsMatch(drExcel(15).ToString().Trim, "^[a-zA-Z ]{1,125}$") = False Then
                            IsValid = False
                            ErrorMessage += "Invalid Beneficiary Country (" + (drExcel(15).ToString()) + ") ; "
                        Else
                            CountryCode = ICCountryController.GetCountryCodeByCountryName(drExcel(15).ToString()).ToString()
                            If CountryCode.ToString() = "" Then
                                IsValid = False
                                ErrorMessage += "Invalid Beneficiary Country Name (" + (drExcel(15).ToString()) + ") ; "
                            Else
                                'CountryName = drExcel(15).ToString() & " - " & CountryName
                                CountryCode = CountryCode & " - " & drExcel(15).ToString()
                            End If

                            If drExcel(16).ToString() <> "" Then
                                If drExcel(16).ToString().Length() > 125 Then
                                    IsValid = False
                                    ErrorMessage += "Beneficiary Province length exceeded (" + (drExcel(16).ToString()) + ") ; "
                                ElseIf Regex.IsMatch(drExcel(16).ToString().Trim, "^[a-zA-Z ]{1,125}$") = False Then
                                    IsValid = False
                                    ErrorMessage += "Invalid Beneficiary Province (" + (drExcel(16).ToString()) + ") ; "
                                Else
                                    ProvinceCode = ICProvinceController.GetActiveandApproveProvincebyProvinceandCountryName(drExcel(16).ToString(), drExcel(15).ToString()).ToString()
                                    If ProvinceCode.ToString() = "" Then
                                        IsValid = False
                                        ErrorMessage += "Beneficiary Province Name does not belong to Country (" + (drExcel(15).ToString()) + ") ; "
                                    Else
                                        'ProvinceCode = drExcel(16).ToString() & " - " & ProvinceCode
                                        ProvinceCode = ProvinceCode & " - " & drExcel(16).ToString()
                                    End If

                                    If drExcel(17).ToString() <> "" Then
                                        If drExcel(17).ToString().Length() > 125 Then
                                            IsValid = False
                                            ErrorMessage += "Beneficiary City length exceeded (" + (drExcel(17).ToString()) + ") ; "
                                        ElseIf Regex.IsMatch(drExcel(17).ToString().Trim, "^[a-zA-Z ]{1,125}$") = False Then
                                            IsValid = False
                                            ErrorMessage += "Invalid Beneficiary City (" + (drExcel(17).ToString()) + ") ; "
                                        Else
                                            CityCode = ICCityController.GetActiveandApproveCitybyProvinceandCityName(drExcel(16).ToString(), drExcel(17).ToString()).ToString()
                                            If CityCode.ToString() = "" Then
                                                IsValid = False
                                                ErrorMessage += "Beneficiary City Name does not belong to Province (" + (drExcel(16).ToString()) + ") ; "
                                            Else
                                                'CityName = drExcel(17).ToString() & " - " & CityName
                                                CityCode = CityCode & " - " & drExcel(17).ToString()
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If

                    End If

                    If IsValid = True Then
                        gvBene.Visible = True
                        drFiltered = dtFiltered.NewRow()
                        drFiltered(0) = drExcel(0).ToString()                         '' BeneficiaryCode
                        drFiltered(1) = drExcel(1).ToString()                         '' BeneficiaryName
                        drFiltered(2) = drExcel(2).ToString()                          '' ProductTypeCode
                        If IsCOTCExist = True Then
                            drFiltered(3) = drExcel(3).ToString()                          '' BeneficiaryIdentificationNo
                        End If
                        If IBFTAccountTitle.Trim <> "" Then
                            drFiltered(4) = drExcel(4).ToString()                          '' BeneficiaryBankCode
                        End If
                        If IBFTAccountTitle.Trim <> "" Then
                            drFiltered(5) = drExcel(5).ToString()                          '' BeneficiaryIBFTAccountNumber
                        End If
                        If FTAccountTitle.Trim <> "" Then
                            drFiltered(6) = drExcel(6).ToString()                          '' BeneficiaryFTAccountNumber
                        End If
                        drFiltered(7) = drExcel(7).ToString()                          '' BeneficiaryBankAddress
                        If drExcel(8).ToString() <> "" Then
                            drFiltered(8) = drExcel(8).ToString()
                        Else
                            drFiltered(8) = ""
                        End If'' BeneficiaryBranchCode
                        drFiltered(9) = OfficeName.ToString()                          '' BeneficiaryBranchName
                        drFiltered(10) = drExcel(10).ToString()                           '' BeneficiaryBranchAddress
                        drFiltered(11) = drExcel(11).ToString()                           '' BeneficiaryMobile
                        drFiltered(12) = drExcel(12).ToString()                           '' BeneficiaryEmail
                        drFiltered(13) = drExcel(13).ToString()                           '' BeneficiaryPhone
                        drFiltered(14) = drExcel(14).ToString()                           '' BeneficiaryAddress
                        drFiltered(15) = CountryCode                                      '' BeneficiaryCountry
                        drFiltered(16) = ProvinceCode                                     '' BeneficiaryProvince
                        drFiltered(17) = CityCode                                         '' BeneficiaryCity
                        If IBFTAccountTitle.Trim <> "" Then
                            drFiltered(18) = BankName                                        '' BeneficiaryBankName
                        End If
                        drFiltered(19) = IBFTAccountTitle                                '' BeneficiaryIBFTAccountTitle
                        drFiltered(20) = FTAccountTitle                                  '' BeneficiaryFTAccountTitle
                        drFiltered(21) = FTAccountBranchCode                             '' BeneficiaryFTAccountBranchCode
                        drFiltered(22) = FTAccountCurrency                               '' BeneficiaryFTAccountCurrency
                        If IsCOTCExist = True Then
                            drFiltered(23) = drExcel(18).ToString()                          '' BeneficiaryIdentificationType
                        End If
                        If IsBillPayExists = True Then
                            drFiltered(24) = UBPSCompanyID
                            drFiltered(25) = drExcel(20).ToString()
                        End If
                        dtFiltered.Rows.Add(drFiltered)
                    Else
                        gvUnVarified.Visible = True
                        drUnVarified = dtUnVarified.NewRow()
                        drUnVarified(0) = RowNo.ToString()
                        drUnVarified(1) = ErrorMessage.ToString()
                        dtUnVarified.Rows.Add(drUnVarified)
                        ' btnProceed.Visible = True
                        btnProceed.Visible = False
                    End If
                Next
                ViewState("dtFiltered") = dtFiltered
                ViewState("dtUnVarified") = dtUnVarified
                SetgvBenegvUnverified(True)
            Else
                UIUtilities.ShowDialog(Me, "Save Bulk Beneficiary", "Empty File.", ICBO.IC.Dialogmessagetype.Failure)
                btnProceed.Visible = True
                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    'Private Function CheckDuplicateCode(ByVal BeneCode As String) As Boolean
    '    Try
    '        Dim rslt As Boolean = False
    '        Dim collBene As New ICBeneCollection
    '        collBene.Query.Where(collBene.Query.BeneficiaryCode.ToLower.Trim = BeneCode.ToLower.Trim)
    '        If collBene.Query.Load() Then
    '            rslt = True
    '        End If

    '        Return rslt
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
    '    End Try
    'End Function

    'Private Function CheckDuplicateName(ByVal BeneName As String) As Boolean
    '    Try
    '        Dim rslt As Boolean = False
    '        Dim collBene As New ICBeneCollection
    '        collBene.Query.Where(collBene.Query.BeneficiaryName.ToLower.Trim = BeneName.ToLower.Trim)
    '        If collBene.Query.Load() Then
    '            rslt = True
    '        End If

    '        Return rslt
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
    '    End Try
    'End Function

    Private Sub SetgvBenegvUnverified(ByVal IsBind As Boolean)
        Try
            Dim dtFiltered As New DataTable
            Dim dtUnVarified As New DataTable

            dtFiltered = DirectCast(ViewState("dtFiltered"), DataTable)
            dtUnVarified = DirectCast(ViewState("dtUnVarified"), DataTable)
            gvBene.DataSource = dtFiltered
            gvUnVarified.DataSource = dtUnVarified
            If IsBind = True Then
                gvBene.DataBind()
                gvUnVarified.DataBind()
                btnAddBeneinBulk.Visible = CBool(htRights("Add"))
            End If
            If dtFiltered.Rows.Count > 0 Then
                gvBene.Visible = True
            Else
                gvBene.Visible = False
                btnAddBeneinBulk.Visible = False
            End If
            If dtUnVarified.Rows.Count > 0 Then
                gvUnVarified.Visible = True
            Else
                gvUnVarified.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnAddBeneinBulk_Click(sender As Object, e As EventArgs) Handles btnAddBeneinBulk.Click
        If Page.IsValid Then
            Try
                Dim objBene As New ICBene
                Dim dtFiltered As New DataTable
                Dim drFiltered As DataRow
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0
                Dim DuplicateBeneCode As String = Nothing

                If ViewState("dtFiltered") Is Nothing Then
                    UIUtilities.ShowDialog(Me, "Save Bulk Beneficiary", "Record not Found.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                Else
                    dtFiltered = DirectCast(ViewState("dtFiltered"), DataTable)
                    For Each drFiltered In dtFiltered.Rows
                        Try
                            objBene.BeneGroupCode = ddlBeneGroup.SelectedValue.ToString()
                            objBene.PaymentNatureCode = ddlPaymentNature.SelectedValue.ToString()
                            objBene.CompanyCode = ddlCompany.SelectedValue.ToString()
                            objBene.BeneficiaryCode = drFiltered(0).ToString()
                            objBene.BeneficiaryName = drFiltered(1).ToString()
                            objBene.BeneficiaryIDNo = drFiltered(3).ToString()
                            If drFiltered(4).ToString().Trim <> "" Then
                                objBene.BeneficiaryBankCode = CInt(drFiltered(4).ToString())
                            End If
                            objBene.BeneficiaryIBFTAccountNo = drFiltered(5).ToString()
                            objBene.BeneficiaryAccountNo = drFiltered(6).ToString()
                            objBene.BeneficiaryBankAddress = drFiltered(7).ToString()
                            objBene.BeneficiaryBranchCode = drFiltered(8).ToString()
                            objBene.BeneficiaryBranchName = drFiltered(9).ToString()
                            objBene.BeneficiaryBranchAddress = drFiltered(10).ToString()
                            objBene.BeneficiaryMobile = drFiltered(11).ToString()
                            objBene.BeneficiaryEmail = drFiltered(12).ToString()
                            objBene.BeneficiaryPhone = drFiltered(13).ToString()
                            objBene.BeneficiaryAddress = drFiltered(14).ToString()
                            If drFiltered(15).ToString() <> "" Then
                                objBene.BeneficiaryCountry = drFiltered(15).ToString().Split(" - ")(0).ToString()
                            End If
                            If drFiltered(24).ToString() <> "" Then
                                objBene.UBPCompanyID = drFiltered(24).ToString().Trim
                            End If
                            If drFiltered(25).ToString() <> "" Then
                                objBene.UBPReferenceNo = drFiltered(25).ToString().Trim
                            End If
                            If drFiltered(16).ToString() <> "" Then
                                objBene.BeneficiaryProvince = drFiltered(16).ToString().Split(" - ")(0).ToString()
                            End If
                            If drFiltered(17).ToString() <> "" Then
                                objBene.BeneficiaryCity = drFiltered(17).ToString().Split(" - ")(0).ToString()
                            End If
                            objBene.BeneficiaryBankName = drFiltered(18).ToString()
                            objBene.BeneficiaryIBFTAccountTitle = drFiltered(19).ToString()
                            objBene.BeneficiaryAccountTitle = drFiltered(20).ToString()
                            objBene.BeneAccountBranchCode = drFiltered(21).ToString()
                            objBene.BeneAccountCurrency = drFiltered(22).ToString()

                            If drFiltered(23).ToString().ToLower = "cnic" Then
                                objBene.BeneficiaryIDType = "CNIC"
                            ElseIf drFiltered(23).ToString().ToLower = "passport" Then
                                objBene.BeneficiaryIDType = "Passport"
                            ElseIf drFiltered(23).ToString().ToLower = "nicop" Then
                                objBene.BeneficiaryIDType = "NICOP"
                            ElseIf drFiltered(23).ToString().ToLower = "driving license" Then
                                objBene.BeneficiaryIDType = "Driving License"
                            End If

                            objBene.IsApproved = False
                            objBene.ApprovalforDeletionStatus = False
                            objBene.IsActive = True
                            objBene.BeneficiaryAddedVia = "Beneficiary Bulk Management"
                            objBene.BeneficiaryStatus = Nothing
                            objBene.CreateBy = Me.UserId
                            objBene.CreateDate = Date.Now
                            objBene.Creater = Me.UserId
                            objBene.CreationDate = Date.Now

                            If ICBeneficiaryManagementController.CheckDuplicateBeneForAdd(objBene) <> "OK" Then
                                DuplicateBeneCode = objBene.BeneficiaryCode & ", " & DuplicateBeneCode
                                FailCount = FailCount + 1
                            Else
                                objBene.BeneID = ICBeneficiaryManagementController.AddBeneficiaryfromform(objBene, False, Me.UserId, Me.UserInfo.Username)

                                '' Add Bene Product Types
                                Dim strProductTypeCode As String()
                                Dim dtProductTypeCode As New DataTable
                                Dim drProductTypeCode As DataRow
                                dtProductTypeCode.Columns.Add("ProductTypeCode")
                                If drFiltered(2).ToString() <> "" Then                                 '' ProductTypeCode
                                    If drFiltered(2).ToString().Contains("|") Then
                                        strProductTypeCode = drFiltered(2).ToString.Split("|")
                                        For k As Integer = 0 To strProductTypeCode.Length - 1
                                            drProductTypeCode = dtProductTypeCode.NewRow()
                                            drProductTypeCode("ProductTypeCode") = strProductTypeCode(k)
                                            dtProductTypeCode.Rows.Add(drProductTypeCode)
                                        Next
                                    Else
                                        drProductTypeCode = dtProductTypeCode.NewRow()
                                        drProductTypeCode("ProductTypeCode") = drFiltered(2).ToString()
                                        dtProductTypeCode.Rows.Add(drProductTypeCode)
                                    End If
                                End If

                                If dtProductTypeCode.Rows.Count > 0 Then
                                    For Each dr As DataRow In dtProductTypeCode.Rows
                                        ICBeneficiaryManagementController.AddBeneProductTypefromform(objBene, dr("ProductTypeCode"))
                                    Next
                                End If

                                PassCount = PassCount + 1

                            End If


                        Catch ex As Exception
                            ProcessModuleLoadException(Me, ex, False)
                            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                        End Try
                    Next

                    If FailCount = 0 And PassCount > 0 Then
                        UIUtilities.ShowDialog(Me, "Save Bulk Beneficiary", "File saved successfully.", ICBO.IC.Dialogmessagetype.Success)
                        Dim aFileInfo As IO.FileInfo
                        aFileInfo = New IO.FileInfo(hfFilePath.Value.ToString())
                        'Delete file from HDD. 
                        If aFileInfo.Exists Then
                            aFileInfo.Delete()
                        End If
                    ElseIf FailCount > 0 And PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Save Bulk Beneficiary", "Following are duplicate Beneficiary Code. <br/>" & DuplicateBeneCode.Remove(DuplicateBeneCode.Length - 2, 2), ICBO.IC.Dialogmessagetype.Failure)
                    End If

                End If

                Clear()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

#End Region

#Region "Grid View Events"

    Protected Sub gvUnVarified_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvUnVarified.NeedDataSource
        SetgvBenegvUnverified(False)
    End Sub

    Protected Sub gvBene_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvBene.NeedDataSource
        SetgvBenegvUnverified(False)
    End Sub

#End Region

End Class
