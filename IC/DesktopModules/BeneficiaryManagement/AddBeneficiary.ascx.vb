﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals

Partial Class DesktopModules_BeneficiaryManagement_AddBeneficiary
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
    Private BeneID As String

#Region "Page Load"
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            BeneID = Request.QueryString("id").ToString()

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()

                If BeneID.ToString() = "0" Then
                    Clear()
                    ClearBillInfoLabels()
                    lblApproved.Visible = False
                    chkApproved.Visible = False
                    btnApproved.Visible = False
                    lblPageHeader.Text = "Add Beneficiary"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    lblApproved.Visible = CBool(htRights("Approve"))
                    chkApproved.Visible = CBool(htRights("Approve"))
                    btnApproved.Visible = CBool(htRights("Approve"))
                    lblPageHeader.Text = "Edit Beneficiary"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    Dim objBene As New ICBene
                    Dim objBeneGroup As New ICBeneGroup
                    Dim objICCompany As New ICCompany
                    Dim objICGroup As New ICGroup
                    Dim objCountry As New ICCountry
                    Dim objProvince As New ICProvince
                    Dim objCity As New ICCity
                    Dim objICUBPCompany As New ICUBPSCompany

                    If objBene.LoadByPrimaryKey(BeneID) Then
                        txtBeneCode.Text = objBene.BeneficiaryCode
                        txtBeneName.Text = objBene.BeneficiaryName
                        txtBeneBankAddress.Text = objBene.BeneficiaryBankAddress
                        LoadddlBenePickLocation()
                        ddlBenePickLocation.SelectedValue = objBene.BeneficiaryBranchCode
                        txtBeneBranchAddress.Text = objBene.BeneficiaryBranchAddress
                        txtBeneMobile.Text = objBene.BeneficiaryMobile
                        txtBeneEmail.Text = objBene.BeneficiaryEmail
                        txtBenePhone.Text = objBene.BeneficiaryPhone
                        txtBeneAddress.Text = objBene.BeneficiaryAddress
                        chkActive.Checked = objBene.IsActive
                        If Not objBene.IsApproved Is Nothing Then
                            chkApproved.Checked = objBene.IsApproved
                        End If

                        objICCompany.LoadByPrimaryKey(objBene.CompanyCode)
                        objICGroup.LoadByPrimaryKey(objICCompany.GroupCode)
                        LoadddlGroup()
                        ddlGroup.SelectedValue = objICGroup.GroupCode.ToString
                        LoadddlCompany()
                        ddlCompany.SelectedValue = objICCompany.CompanyCode.ToString
                        LoadPaymentNature()
                        ddlPaymentNature.SelectedValue = objBene.PaymentNatureCode
                        LoadBeneGroup()
                        ddlBeneGroup.SelectedValue = objBene.BeneGroupCode
                        LoadddlCountry()
                        ddlBeneCountry.SelectedValue = objBene.BeneficiaryCountry
                        LoadddlProvince()
                        ddlBeneProvince.SelectedValue = objBene.BeneficiaryProvince
                        LoadddlCity()
                        ddlBeneCity.SelectedValue = objBene.BeneficiaryCity

                        LoadAPNProductType()
                        MarkBeneProductType()
                        'SetProductTypeChkboxlstOnUpdate()
                        chklstAPNProductType_SelectedIndexChanged(chklstAPNProductType.SelectedValue.ToString(), Nothing)
                        txtBeneAccNo.Text = objBene.BeneficiaryAccountNo
                        txtBeneAccountTitle.Text = objBene.BeneficiaryAccountTitle
                        txtBeneAccountBranchCode.Text = objBene.BeneAccountBranchCode
                        txtBeneAccountCurrency.Text = objBene.BeneAccountCurrency
                        txtBeneIBFTAccNo.Text = objBene.BeneficiaryIBFTAccountNo
                        txtBeneIBFTAccountTitle.Text = objBene.BeneficiaryIBFTAccountTitle
                        txtBeneCNIC.Text = objBene.BeneficiaryIDNo
                        ddlBeneIDType.SelectedValue = objBene.BeneficiaryIDType
                        LoadddlBeneBank()
                        ddlBeneBank.SelectedValue = objBene.BeneficiaryBankCode

                        If objBene.UBPCompanyID IsNot Nothing Then
                            Dim UBcompanyID As String = ""
                            'UBcompanyID = ICUBPSCompanyController.GetUBPCompanyIDByCompanyCodeor(objBene.UBPCompanyID)
                            'objICUBPCompany.LoadByPrimaryKey(UBcompanyID)

                            objICUBPCompany.Query.Where(objICUBPCompany.Query.CompanyCode = objBene.UBPCompanyID And objICUBPCompany.Query.IsActive = True And objICUBPCompany.Query.IsApproved = True)
                            objICUBPCompany.Query.Load()
                            If objICUBPCompany.Query.Load = False Then
                                Throw New Exception("Unable to load UBP Company")
                            End If

                            ddlCompanyType.SelectedValue = objICUBPCompany.CompanyType
                            LoadddlCompanyName(objICUBPCompany.CompanyType)
                            ddlUBPCompanyName.SelectedValue = objBene.UBPCompanyID

                            ddlUBPCompanyName_SelectedIndexChanged(ddlUBPCompanyName, Nothing)
                            txtUBPRefNo.Text = objBene.UBPReferenceNo
                        End If

                        'If objBene.BeneficiaryStatus = "Ready for" Then
                        If objBene.IsApproved = False Then
                            lblApproved.Visible = True
                            btnApproved.Enabled = True
                            chkApproved.Enabled = True
                        Else
                            lblApproved.Visible = True
                            btnApproved.Enabled = False
                            chkApproved.Enabled = False
                        End If
                        'Else
                        '    btnApproved.Enabled = False
                        '    chkApproved.Enabled = False
                        'End If
                    End If
                    End If
                End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Beneficiary Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        ddlGroup.ClearSelection()
        ddlCompany.ClearSelection()
        ddlBeneGroup.ClearSelection()
        ddlPaymentNature.ClearSelection()
        chklstAPNProductType.ClearSelection()
        ddlBeneCountry.ClearSelection()
        ddlBeneProvince.ClearSelection()
        ddlBeneCity.ClearSelection()
        ddlBeneBank.ClearSelection()
        ddlCompanyType.ClearSelection()
        ddlUBPCompanyName.ClearSelection()
        LoadddlGroup()
        LoadddlCompany()
        LoadPaymentNature()
        LoadAPNProductType()
        LoadBeneGroup()
        LoadddlCountry()
        LoadddlProvince()
        LoadddlCity()
        LoadddlBeneBank()
        LoadddlBenePickLocation()
        LoadddlCompanyName(ddlCompanyType.SelectedValue.ToString)
        txtBeneName.Text = Nothing
        txtBeneCNIC.Text = Nothing
        ddlBeneIDType.SelectedValue = "0"
        txtBeneBranchAddress.Text = Nothing
        txtBeneBankAddress.Text = Nothing
        ddlBenePickLocation.SelectedValue = "0"
        txtBeneCode.Text = Nothing
        txtBeneMobile.Text = Nothing
        txtBeneEmail.Text = Nothing
        txtBenePhone.Text = Nothing
        txtBeneAddress.Text = Nothing
        chkActive.Checked = True
        DisableValidatorandPanelonProductType()
    End Sub

    Private Sub MarkBeneProductType()
        Try
            Dim dtBeneProductType As New DataTable
            dtBeneProductType = ICBeneficiaryManagementController.GetAlltaggedBeneProductTypebyBeneID(BeneID)
            If dtBeneProductType IsNot Nothing Then
                If dtBeneProductType.Rows.Count > 0 Then
                    For Each drBeneProd As DataRow In dtBeneProductType.Rows
                        For Each item As ListItem In chklstAPNProductType.Items
                            If drBeneProd("ProductTypeCode") = item.Value Then
                                item.Selected = True
                            End If
                        Next
                    Next
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    'Private Sub SetProductTypeChkboxlstOnUpdate()
    '    DisableValidatorandPanelonProductType()
    '    For Each item As ListItem In chklstAPNProductType.Items
    '        If item.Selected = True Then
    '            Dim objProductType As New ICProductType
    '            If objProductType.LoadByPrimaryKey(item.Value) Then
    '                If objProductType.DisbursementMode = "Direct Credit" Then
    '                    pnlBeneFTAcc.Visible = True
    '                    tdlblBeneBank.Style.Add("display", "none")
    '                    tdddlBeneBank.Style.Add("display", "none")
    '                ElseIf objProductType.DisbursementMode = "Other Credit" Then
    '                    pnlBeneIBFTAccNoAccTitle.Visible = True
    '                    tdlblBeneBank.Style.Remove("display")
    '                    tdddlBeneBank.Style.Remove("display")
    '                    rfvddlBeneBank.Enabled = True
    '                ElseIf objProductType.DisbursementMode = "COTC" Then
    '                    pnlBeneIDNoType.Visible = True
    '                    rfvBeneIDType.Enabled = True
    '                    rad.GetSettingByBehaviorID("reBeneCNIC").Validation.IsRequired = True
    '                Else
    '                    pnlBeneFTAcc.Visible = False
    '                    pnlBeneIBFTAccNoAccTitle.Visible = False
    '                    tdlblBeneBank.Style.Add("display", "none")
    '                    tdddlBeneBank.Style.Add("display", "none")
    '                    pnlBeneIDNoType.Visible = False
    '                    rfvBeneIDType.Enabled = False
    '                    rad.GetSettingByBehaviorID("reBeneCNIC").Validation.IsRequired = False
    '                End If
    '            End If
    '        End If
    '    Next
    'End Sub

#End Region

#Region "Button Events"
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim objBene As New ICBene
                Dim objBenePrev As New ICBene
                Dim objICUBPSCompany As New ICUBPSCompany
                Dim DuplicateRslt As String = ""
                If pnlBeneFTAcc.Visible = True Then
                    If txtBeneAccountTitle.Text.Trim = "" Or txtBeneAccountBranchCode.Text.Trim = "" Or txtBeneAccountCurrency.Text.Trim = "" Then
                        UIUtilities.ShowDialog(Me, "Save Beneficiary", "Beneficiary FT account details not fetched.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                If pnlBeneBillPament.Visible = True Then

                    'objICUBPSCompany.LoadByPrimaryKey(ddlUBPCompanyName.SelectedValue.ToString)

                    objICUBPSCompany.Query.Where(objICUBPSCompany.Query.CompanyCode = ddlUBPCompanyName.SelectedValue.ToString And objICUBPSCompany.Query.IsActive = True And objICUBPSCompany.Query.IsApproved = True)
                    objICUBPSCompany.Query.Load()
                    If objICUBPSCompany.Query.Load = False Then
                        Throw New Exception("Unable to load UBP Company")
                    End If

                    If BeneID = "0" Then
                        If pnlBillInquiryDetails.Visible = False And objICUBPSCompany.IsBillingInquiry = True Then
                            UIUtilities.ShowDialog(Me, "Save Beneficiary", "Beneficiary bill inquiry is not performed", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        Else
                            If ICUBPSCompanyController.CheckReferenceFieldLength(txtUBPRefNo.Text.Length, objICUBPSCompany.UBPSCompanyID) = False Then
                                UIUtilities.ShowDialog(Me, "Save Beneficiary", "Reference No lenght must be in between [ " & objICUBPSCompany.ReferenceFieldMinLength & " ] and [" & objICUBPSCompany.ReferenceFieldMaxLength & "]", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                            If ICUBPSCompanyController.CheckReferenceFieldDataWithRegularExpression(txtUBPRefNo.Text, objICUBPSCompany.UBPSCompanyID) = False Then
                                UIUtilities.ShowDialog(Me, "Save Beneficiary", "Reference No must be in [ " & objICUBPSCompany.RegularExpressionForReferenceField & " ] format", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        End If
                    Else
                        objBenePrev.LoadByPrimaryKey(BeneID)
                        If pnlBillInquiryDetails.Visible = False And objICUBPSCompany.IsBillingInquiry = True And (objBenePrev.UBPCompanyID <> ddlUBPCompanyName.SelectedValue.ToString Or objBenePrev.UBPReferenceNo <> txtUBPRefNo.Text) Then
                            UIUtilities.ShowDialog(Me, "Save Beneficiary", "Beneficiary bill inquiry is not performed", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        Else
                            If ICUBPSCompanyController.CheckReferenceFieldLength(txtUBPRefNo.Text.Length, objICUBPSCompany.UBPSCompanyID) = False Then
                                UIUtilities.ShowDialog(Me, "Save Beneficiary", "Reference No lenght must be in between [ " & objICUBPSCompany.ReferenceFieldMinLength & " ] and [" & objICUBPSCompany.ReferenceFieldMaxLength & "]", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                            If ICUBPSCompanyController.CheckReferenceFieldDataWithRegularExpression(txtUBPRefNo.Text, objICUBPSCompany.UBPSCompanyID) = False Then
                                UIUtilities.ShowDialog(Me, "Save Beneficiary", "Reference No must be in [ " & objICUBPSCompany.RegularExpressionForReferenceField & " ] format", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        End If
                    End If


                End If
                If pnlBeneIBFTAccNoAccTitle.Visible = True Then
                    If txtBeneIBFTAccountTitle.Text.Trim = "" Then
                        UIUtilities.ShowDialog(Me, "Save Beneficiary", "Beneficiary IBFT Account Title not fetched.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If

                If BeneID <> "0" Then
                    '' Check Bene exist

                    If Not objBene.LoadByPrimaryKey(BeneID) Then
                        UIUtilities.ShowDialog(Me, "Save Beneficiary", "Beneficiary not exist", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If


                objBene.CompanyCode = ddlCompany.SelectedValue.ToString()
                objBene.PaymentNatureCode = ddlPaymentNature.SelectedValue.ToString()
                objBene.BeneGroupCode = ddlBeneGroup.SelectedValue.ToString()
                objBene.BeneficiaryCode = txtBeneCode.Text
                objBene.BeneficiaryName = txtBeneName.Text

                If pnlBeneFTAcc.Visible = True Then
                    objBene.BeneficiaryAccountNo = txtBeneAccNo.Text
                    objBene.BeneficiaryAccountTitle = txtBeneAccountTitle.Text
                    objBene.BeneAccountBranchCode = txtBeneAccountBranchCode.Text
                    objBene.BeneAccountCurrency = txtBeneAccountCurrency.Text
                Else
                    objBene.BeneficiaryAccountNo = Nothing
                    objBene.BeneficiaryAccountTitle = Nothing
                    objBene.BeneAccountBranchCode = Nothing
                    objBene.BeneAccountCurrency = Nothing
                End If

                If pnlBeneIBFTAccNoAccTitle.Visible = True Then
                    objBene.BeneficiaryBankCode = ddlBeneBank.SelectedValue
                    If ddlBeneBank.SelectedValue <> "0" Then
                        objBene.BeneficiaryBankName = ddlBeneBank.SelectedItem.Text
                    End If
                    objBene.BeneficiaryIBFTAccountNo = txtBeneIBFTAccNo.Text
                    objBene.BeneficiaryIBFTAccountTitle = txtBeneIBFTAccountTitle.Text
                Else
                    objBene.BeneficiaryBankCode = "0"
                    objBene.BeneficiaryBankName = Nothing
                    objBene.BeneficiaryIBFTAccountNo = Nothing
                    objBene.BeneficiaryIBFTAccountTitle = Nothing
                End If

                If pnlBeneIDNoType.Visible = True Then
                    objBene.BeneficiaryIDNo = txtBeneCNIC.Text
                    objBene.BeneficiaryIDType = ddlBeneIDType.SelectedValue.ToString()
                Else
                    objBene.BeneficiaryIDNo = Nothing
                    objBene.BeneficiaryIDType = Nothing
                End If
                If pnlBeneBillPament.Visible = True Then

                    '' Save UBPCompanyCode in UBPCompanyID
                    objBene.UBPCompanyID = ddlUBPCompanyName.SelectedValue.ToString
                    objBene.UBPReferenceNo = txtUBPRefNo.Text
                Else
                    objBene.UBPCompanyID = Nothing
                    objBene.UBPReferenceNo = Nothing
                End If
                objBene.BeneficiaryCNIC = "0"
                objBene.BeneficiaryBankAddress = txtBeneBankAddress.Text
                'objBene.BeneficiaryBranchCode = ddlBenePickLocation.SelectedValue.ToString()
                If ddlBenePickLocation.SelectedValue.ToString() <> "0" Then
                    objBene.BeneficiaryBranchName = ddlBenePickLocation.SelectedItem.ToString()
                    objBene.BeneficiaryBranchCode = ddlBenePickLocation.SelectedValue.ToString()
                Else
                    objBene.BeneficiaryBranchName = Nothing
                    objBene.BeneficiaryBranchCode = Nothing
                End If

                objBene.BeneficiaryBranchAddress = txtBeneBranchAddress.Text
                objBene.BeneficiaryMobile = txtBeneMobile.Text
                objBene.BeneficiaryEmail = txtBeneEmail.Text
                objBene.BeneficiaryPhone = txtBenePhone.Text
                objBene.BeneficiaryAddress = txtBeneAddress.Text
                objBene.BeneficiaryCountry = ddlBeneCountry.SelectedValue.ToString()
                objBene.BeneficiaryProvince = ddlBeneProvince.SelectedValue.ToString()
                objBene.BeneficiaryCity = ddlBeneCity.SelectedValue.ToString()
                objBene.IsApproved = False
                objBene.ApprovalforDeletionStatus = False
                objBene.IsActive = chkActive.Checked
                objBene.BeneficiaryAddedVia = "Beneficiary Management"
                objBene.BeneficiaryStatus = Nothing
                If BeneID = "0" Then
                    objBene.CreateBy = Me.UserId
                    objBene.CreateDate = Date.Now
                    objBene.Creater = Me.UserId
                    objBene.CreationDate = Date.Now
                    'DuplicateRslt = ICBeneficiaryManagementController.CheckDuplicateBene(objBene)
                    DuplicateRslt = ICBeneficiaryManagementController.CheckDuplicateBeneForAdd(objBene)

                    If DuplicateRslt = "OK" Then
                        objBene.BeneID = ICBeneficiaryManagementController.AddBeneficiaryfromform(objBene, False, Me.UserId, Me.UserInfo.Username)

                        '' Add Bene Product Types
                        For Each item As ListItem In chklstAPNProductType.Items
                            If item.Selected = True Then
                                ICBeneficiaryManagementController.AddBeneProductTypefromform(objBene, item.Value)
                            End If
                        Next

                        UIUtilities.ShowDialog(Me, "Save Beneficiary", "Beneficiary added successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Else
                        UIUtilities.ShowDialog(Me, "Save Beneficiary", "Can not add duplicate " & DuplicateRslt & ".", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else

                    objBene.BeneID = BeneID
                    objBene.CreateBy = Me.UserId
                    objBene.CreateDate = Date.Now
                    Dim prevobjBene As New ICBene
                    prevobjBene.LoadByPrimaryKey(objBene.BeneID)
                    objBene.BeneficiaryAddedVia = prevobjBene.BeneficiaryAddedVia
                    DuplicateRslt = ICBeneficiaryManagementController.CheckDuplicateBeneForAdd(objBene)
                    If DuplicateRslt = "OK" Then
                        objBene.BeneID = ICBeneficiaryManagementController.AddBeneficiaryfromform(objBene, True, Me.UserId, Me.UserInfo.Username)
                        ICBeneficiaryManagementController.DeleteBeneProductType(objBene.BeneID, Me.UserInfo.UserID, Me.UserInfo.Username)
                        '' Add Bene Product Types
                        For Each item As ListItem In chklstAPNProductType.Items
                            If item.Selected = True Then
                                ICBeneficiaryManagementController.AddBeneProductTypefromform(objBene, item.Value)
                            End If
                        Next
                        UIUtilities.ShowDialog(Me, "Save Beneficiary", "Beneficiary updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    Else
                        UIUtilities.ShowDialog(Me, "Save Beneficiary", "Can not add duplicate " & DuplicateRslt & ".", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                Clear()

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    Protected Sub btnApproved_Click(sender As Object, e As EventArgs) Handles btnApproved.Click
        If Page.IsValid Then
            Try
                Dim objBene As New ICBene
                If objBene.LoadByPrimaryKey(BeneID.ToString()) Then
                    If Me.UserInfo.IsSuperUser = False Then
                        If objBene.CreateBy = Me.UserId Then
                            UIUtilities.ShowDialog(Me, "Approve Beneficiary", "Beneficiary must be approve by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    End If
                    If objBene.BeneficiaryCode = "" Or objBene.BeneficiaryCode Is Nothing Then
                        UIUtilities.ShowDialog(Me, "Approve Beneficiary", "Beneficiary code is not defined.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    If chkApproved.Checked Then
                        ICBeneficiaryManagementController.ApproveBene(BeneID.ToString(), chkApproved.Checked, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        UIUtilities.ShowDialog(Me, "Approve Beneficiary", "Beneficiary approve.", Dialogmessagetype.Success, NavigateURL())
                    Else
                        UIUtilities.ShowDialog(Me, "Approve Beneficiary", "Please checked the Approved Check Box.", ICBO.IC.Dialogmessagetype.Warning)
                    End If
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

#End Region

#Region "Load Drop Down"

    Protected Sub ddlBeneBank_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBeneBank.SelectedIndexChanged
        Try
            If ddlBeneBank.SelectedValue <> "0" Then
                txtBeneIBFTAccNo.ReadOnly = False
            Else
                txtBeneIBFTAccNo.ReadOnly = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
            LoadPaymentNature()
            LoadBeneGroup()
            LoadAPNProductType()
            chklstAPNProductType_SelectedIndexChanged(chklstAPNProductType.SelectedValue.ToString(), Nothing)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadPaymentNature()
            LoadBeneGroup()
            LoadAPNProductType()
            chklstAPNProductType_SelectedIndexChanged(chklstAPNProductType.SelectedValue.ToString(), Nothing)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlPaymentNature_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPaymentNature.SelectedIndexChanged
        Try
            LoadBeneGroup()
            LoadAPNProductType()
            chklstAPNProductType_SelectedIndexChanged(chklstAPNProductType.SelectedValue.ToString(), Nothing)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlBeneCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBeneCountry.SelectedIndexChanged
        Try
            LoadddlProvince()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlBeneProvince_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBeneProvince.SelectedIndexChanged
        Try
            LoadddlCity()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlBeneBank()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBeneBank.Items.Clear()
            ddlBeneBank.Items.Add(lit)
            ddlBeneBank.AppendDataBoundItems = True
            ddlBeneBank.DataSource = ICBankController.GetAllApprovedActiveNonPrincipalBanksAndIBFTEnabledBanks()
            ddlBeneBank.DataTextField = "BankName"
            ddlBeneBank.DataValueField = "BankCode"
            ddlBeneBank.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlCountry()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBeneCountry.Items.Clear()
            ddlBeneCountry.Items.Add(lit)
            ddlBeneCountry.AppendDataBoundItems = True
            ddlBeneCountry.DataSource = ICCountryController.GetCountryActiveAndApproved()
            ddlBeneCountry.DataTextField = "CountryName"
            ddlBeneCountry.DataValueField = "CountryCode"
            ddlBeneCountry.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlProvince()
        Try
            ddlBeneProvince.Items.Clear()
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBeneProvince.Items.Add(lit)
            ddlBeneProvince.AppendDataBoundItems = True
            ddlBeneProvince.DataSource = ICProvinceController.GetProvinceActiveAndApprove(ddlBeneCountry.SelectedValue.ToString())
            ddlBeneProvince.DataTextField = "ProvinceName"
            ddlBeneProvince.DataValueField = "ProvinceCode"
            ddlBeneProvince.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlCity()
        Try
            ddlBeneCity.Items.Clear()
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBeneCity.Items.Add(lit)
            ddlBeneCity.AppendDataBoundItems = True
            ddlBeneCity.DataSource = ICCityController.GetCityActiveAndApprovedByProvinceCode(ddlBeneProvince.SelectedValue.ToString())
            ddlBeneCity.DataTextField = "CityName"
            ddlBeneCity.DataValueField = "CityCode"
            ddlBeneCity.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICBeneficiaryGroupManagementController.GetAllActiveGroupsForBene()
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICBeneficiaryGroupManagementController.GetAllActiveCompaniesByGroupCodeForBene(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadPaymentNature()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlPaymentNature.Items.Clear()
            ddlPaymentNature.Items.Add(lit)
            ddlPaymentNature.AppendDataBoundItems = True
            ddlPaymentNature.DataSource = ICBeneficiaryGroupManagementController.GetPaymentNatureByCompanyCode(ddlCompany.SelectedValue.ToString)
            ddlPaymentNature.DataTextField = "PaymentNatureName"
            ddlPaymentNature.DataValueField = "PaymentNatureCode"
            ddlPaymentNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadBeneGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBeneGroup.Items.Clear()
            ddlBeneGroup.Items.Add(lit)
            ddlBeneGroup.AppendDataBoundItems = True
            'ddlBeneGroup.DataSource = ICBeneficiaryGroupManagementController.GetAllActiveandApproveBeneGroupByPN(ddlPaymentNature.SelectedValue.ToString())
            ddlBeneGroup.DataSource = ICBeneficiaryGroupManagementController.GetAllActiveandApproveBeneGroupByPNandCompanyCode(ddlPaymentNature.SelectedValue.ToString(), ddlCompany.SelectedValue.ToString())
            ddlBeneGroup.DataTextField = "BeneGroupName"
            ddlBeneGroup.DataValueField = "BeneGroupCode"
            ddlBeneGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadAPNProductType()
        Try
            Dim dt As New DataTable
            Dim dr As DataRow
            Dim lit As New ListItem
            If ddlCompany.SelectedValue = "0" And ddlPaymentNature.SelectedValue = "0" Then
                lit = New ListItem
                lit.Value = "0"
                lit.Text = ""
                lit.Enabled = False
                chklstAPNProductType.Items.Clear()
                chklstAPNProductType.Items.Add(lit)
                chklstAPNProductType.AppendDataBoundItems = True
            Else
                chklstAPNProductType.Items.Clear()
            End If


            dt = ICAccountPaymentNatureProductTypeController.GetAllAPNProductTypeByCompanyCodeandPNCode(ddlCompany.SelectedValue.ToString(), ddlPaymentNature.SelectedValue.ToString())

            If dt.Rows.Count > 0 Then
                chklstAPNProductType.Items.Clear()
                For Each dr In dt.Rows
                    lit = New ListItem
                    lit.Text = dr("ProductTypeName").ToString()
                    lit.Value = dr("ProductTypeCode").ToString()
                    ' lit.Attributes("onclick") = "chkCheckChange('" & dr("ProductTypeCode").ToString() & "');"
                    chklstAPNProductType.Items.Add(lit)
                Next
            Else
                lit = New ListItem
                lit.Value = "0"
                lit.Text = ""
                lit.Enabled = False
                chklstAPNProductType.Items.Clear()
                chklstAPNProductType.Items.Add(lit)
                chklstAPNProductType.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub txtBeneAccNo_TextChanged(sender As Object, e As EventArgs) Handles txtBeneAccNo.TextChanged
        Try
            Dim StrAccountDetails As String()
            Dim AccountStatus As String = ""
            Dim BBAccNo As String = Nothing
            If Not txtBeneAccNo.Text.ToString = "" Then
                If CBUtilities.IsNormalAccountNoOrIBAN(txtBeneAccNo.Text.ToString) = True Then
                    If CBUtilities.ValidateIBAN(txtBeneAccNo.Text.ToString) = True Then
                        BBAccNo = CBUtilities.GetBBANFromIBAN(txtBeneAccNo.Text.ToString)
                        AccountStatus = CBUtilities.AccountStatus(BBAccNo, ICBO.CBUtilities.AccountType.RB, "Beneficiary Account Status", txtBeneAccNo.Text.ToString, "Add", "Add Beneficiary", "").ToString
                        StrAccountDetails = (CBUtilities.TitleFetch(BBAccNo, ICBO.CBUtilities.AccountType.RB, "Beneficiary Account Title", txtBeneAccNo.Text.ToString).ToString.Split(";"))
                        If AccountStatus = "Active" Then
                            txtBeneAccountTitle.Text = StrAccountDetails(0).ToString
                            txtBeneAccountCurrency.Text = StrAccountDetails(2).ToString
                            txtBeneAccountBranchCode.Text = StrAccountDetails(1).ToString
                        Else
                            UIUtilities.ShowDialog(Me, "Save Beneficiary", "Beneficiary account is not active", ICBO.IC.Dialogmessagetype.Failure)
                            'UIUtilities.ShowDialog(Me, "Warning", AccountStatus.ToString, ICBO.IC.Dialogmessagetype.Warning)
                            'txtBeneAccountTitle.Text = StrAccountDetails(0).ToString
                            'txtBeneAccountCurrency.Text = StrAccountDetails(2).ToString
                            'txtBeneAccountBranchCode.Text = StrAccountDetails(1).ToString
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Invalid IBAN", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else
                    BBAccNo = txtBeneAccNo.Text.ToString
                    AccountStatus = CBUtilities.AccountStatus(BBAccNo, ICBO.CBUtilities.AccountType.RB, "Beneficiary Account Status", txtBeneAccNo.Text.ToString, "Add", "Add Beneficiary", "").ToString
                    StrAccountDetails = (CBUtilities.TitleFetch(BBAccNo, ICBO.CBUtilities.AccountType.RB, "Beneficiary Account Title", txtBeneAccNo.Text.ToString).ToString.Split(";"))
                    If AccountStatus = "Active" Then
                        txtBeneAccountTitle.Text = StrAccountDetails(0).ToString
                        txtBeneAccountCurrency.Text = StrAccountDetails(2).ToString
                        txtBeneAccountBranchCode.Text = StrAccountDetails(1).ToString
                    Else
                        UIUtilities.ShowDialog(Me, "Save Beneficiary", "Beneficiary account is not active", ICBO.IC.Dialogmessagetype.Failure)
                        'UIUtilities.ShowDialog(Me, "Warning", AccountStatus.ToString, ICBO.IC.Dialogmessagetype.Warning)
                        'txtBeneAccountTitle.Text = StrAccountDetails(0).ToString
                        'txtBeneAccountCurrency.Text = StrAccountDetails(2).ToString
                        'txtBeneAccountBranchCode.Text = StrAccountDetails(1).ToString
                    End If
                End If
            Else
                txtBeneAccNo.Text = ""
                txtBeneAccountCurrency.Text = ""
                txtBeneAccountTitle.Text = ""
                txtBeneAccountBranchCode.Text = ""
                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub txtBeneIBFTAccNo_TextChanged(sender As Object, e As EventArgs) Handles txtBeneIBFTAccNo.TextChanged
        Try
            Dim AccTitle As String = Nothing
            Dim collObjICAccount As New ICAccountsCollection
            Dim Remarks As String = Nothing
            Dim objBank As New ICBank
            Dim objPrincipalBank As New ICBank
            Dim BankIMD As String = Nothing

            If ddlBeneBank.SelectedValue <> 0 Then

                objBank.LoadByPrimaryKey(ddlBeneBank.SelectedValue.ToString())
                objPrincipalBank = ICBankController.GetPrincipleBank

            Else
                UIUtilities.ShowDialog(Me, "Save Beneficiary", "Please select beneficiary bank first", ICBO.IC.Dialogmessagetype.Failure)
                txtBeneIBFTAccNo.Text = Nothing
            End If


            collObjICAccount.Query.Where(collObjICAccount.Query.CompanyCode = ddlCompany.SelectedValue.ToString And collObjICAccount.Query.IsActive = True And collObjICAccount.Query.IsApproved = True)
            collObjICAccount.Query.Load()
            BankIMD = objBank.BankIMD
            If txtBeneIBFTAccNo.Text.Trim.ToString <> "" Then
                If CBUtilities.IsNormalAccountNoOrIBAN(txtBeneIBFTAccNo.Text.Trim.ToString()) Then
                    If CBUtilities.ValidateIBAN(txtBeneIBFTAccNo.Text.Trim.ToString()) Then
                        If BankIMD.ToString <> "" Then
                            If Not objPrincipalBank Is Nothing Then
                                If txtBeneIBFTAccNo.Text.Trim.ToString() <> "" Then
                                    If collObjICAccount.Count > 0 Then
                                        AccTitle = CBUtilities.IBFTTitleFetch(BankIMD.ToString().Trim, txtBeneIBFTAccNo.Text.Trim.ToString(), ICBO.CBUtilities.AccountType.RB, "Beneficiary Account Title", txtBeneIBFTAccNo.Text.Trim.ToString(), "", "", 0, "", "", "", objBank.BankID, objBank.BankName, collObjICAccount(0).BranchCode & collObjICAccount(0).AccountNumber, objPrincipalBank.BankIMD)
                                        txtBeneIBFTAccountTitle.Text = AccTitle
                                        Remarks = "-"
                                    Else
                                        Remarks = "Invalid Client Account Number for IBFT title fetch"
                                        UIUtilities.ShowDialog(Me, "Error", Remarks, ICBO.IC.Dialogmessagetype.Failure)
                                    End If

                                Else
                                    Remarks = "Invalid Account Number"
                                    UIUtilities.ShowDialog(Me, "Error", Remarks, ICBO.IC.Dialogmessagetype.Failure)
                                End If
                            Else
                                Remarks = "Invalid Principal Bank IMD"
                                UIUtilities.ShowDialog(Me, "Error", Remarks, ICBO.IC.Dialogmessagetype.Failure)
                            End If
                        Else
                            Remarks = "Invalid Bank IMD"
                            UIUtilities.ShowDialog(Me, "Error", Remarks, ICBO.IC.Dialogmessagetype.Failure)
                        End If
                    Else
                        Remarks = "Invalid International Bank Account Number (IBAN)"
                        UIUtilities.ShowDialog(Me, "Error", Remarks, ICBO.IC.Dialogmessagetype.Failure)
                    End If
                Else
                    If BankIMD.ToString <> "" Then
                        If Not objPrincipalBank Is Nothing Then
                            If collObjICAccount.Count > 0 Then
                                AccTitle = CBUtilities.IBFTTitleFetch(BankIMD.ToString().Trim, txtBeneIBFTAccNo.Text.Trim.ToString(), ICBO.CBUtilities.AccountType.RB, "Beneficiary Account Title", txtBeneIBFTAccNo.Text.Trim.ToString(), "", "", 0, "", "", "", objBank.BankID, objBank.BankName, collObjICAccount(0).BranchCode & collObjICAccount(0).AccountNumber, objPrincipalBank.BankIMD)
                                txtBeneIBFTAccountTitle.Text = AccTitle
                                Remarks = "-"
                            Else
                                Remarks = "Invalid Client Account Number for IBFT title fetch"
                                UIUtilities.ShowDialog(Me, "Error", Remarks, ICBO.IC.Dialogmessagetype.Failure)
                            End If
                        Else
                            Remarks = "Invalid Bank IMD"
                            UIUtilities.ShowDialog(Me, "Error", Remarks, ICBO.IC.Dialogmessagetype.Failure)
                        End If
                       
                    Else
                        Remarks = "Invalid Bank IMD"
                        UIUtilities.ShowDialog(Me, "Error", Remarks, ICBO.IC.Dialogmessagetype.Failure)
                    End If
                End If
            Else
                Remarks = "Invalid Account Number"
                UIUtilities.ShowDialog(Me, "Error", Remarks, ICBO.IC.Dialogmessagetype.Failure)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

    Private Sub DisableValidatorandPanelonProductType()
        Try

            pnlBeneFTAcc.Visible = False
            txtBeneAccNo.Text = Nothing
            txtBeneAccountTitle.Text = Nothing
            txtBeneAccountBranchCode.Text = Nothing
            txtBeneAccountCurrency.Text = Nothing

            pnlBeneIBFTAccNoAccTitle.Visible = False
            tdlblBeneBank.Style.Add("display", "none")
            tdddlBeneBank.Style.Add("display", "none")
            rfvddlBeneBank.Enabled = False
            LoadddlBeneBank()
            txtBeneIBFTAccNo.Text = Nothing
            txtBeneIBFTAccountTitle.Text = Nothing

            pnlBeneIDNoType.Visible = False
            rfvBeneIDType.Enabled = False
            rad.GetSettingByBehaviorID("reBeneCNIC").Validation.IsRequired = False
            txtBeneCNIC.Text = Nothing
            ddlBeneIDType.SelectedValue = "0"

            pnlBeneBillPament.Visible = False
            pnlBillInquiryDetails.Visible = False

            rfvUBPCompanyType.Enabled = False
            rfvUBPCompany.Enabled = False
            rad.GetSettingByBehaviorID("reUBPRefNo").Validation.IsRequired = False

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    'Protected Sub chklstAPNProductType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chklstAPNProductType.SelectedIndexChanged
    '    Try
    '        Dim objICProductType As New ICProductType
    '        Dim lit As New ListItem
    '        Dim selLI As New ListItem
    '        Dim ProductType As String = Nothing
    '        If objICProductType.LoadByPrimaryKey(hfchkProdCode.Value.ToString()) Then
    '            ProductType = objICProductType.DisbursementMode
    '            For Each lit In chklstAPNProductType.Items
    '                If lit.Value.ToString() = hfchkProdCode.Value.ToString() Then
    '                    selLI = lit
    '                    Exit For
    '                End If
    '            Next

    '            If selLI.Selected = True Then
    '                If ProductType = "Direct Credit" Then
    '                    ShowControlsOnProductType(ProductType, False)
    '                    EnableDisableValidatorsOnProductType(ProductType, False)
    '                ElseIf ProductType = "Other Credit" Then
    '                    ShowControlsOnProductType(ProductType, False)
    '                    EnableDisableValidatorsOnProductType(ProductType, False)
    '                ElseIf ProductType = "COTC" Then
    '                    ShowControlsOnProductType(ProductType, False)
    '                    EnableDisableValidatorsOnProductType(ProductType, False)
    '                Else
    '                    ShowControlsOnProductType(Nothing, True)
    '                    EnableDisableValidatorsOnProductType(Nothing, True)
    '                End If
    '            Else
    '                Select Case (ProductType)
    '                    Case "Direct Credit"
    '                        ClearControlsOnProductType(ProductType)
    '                        ShowControlsOnProductType(ProductType, True)
    '                        EnableDisableValidatorsOnProductType(ProductType, True)
    '                    Case "Other Credit"
    '                        ClearControlsOnProductType(ProductType)
    '                        ShowControlsOnProductType(ProductType, True)
    '                        EnableDisableValidatorsOnProductType(ProductType, True)
    '                    Case "COTC"
    '                        ClearControlsOnProductType(ProductType)
    '                        ShowControlsOnProductType(ProductType, True)
    '                        EnableDisableValidatorsOnProductType(ProductType, True)
    '                    Case Else
    '                        ShowControlsOnProductType(Nothing, True)
    '                        EnableDisableValidatorsOnProductType(Nothing, True)
    '                End Select
    '            End If
    '        End If
    '        '    Dim dtProductType As New DataTable
    '        '    Dim IsCheckedMark As Boolean = False
    '        '    Dim IsCheckedRemove As Boolean = False
    '        '   
    '        '    dtProductType = GetListofAllCheckedProductTypes()
    '        '    For Each item As ListItem In chklstAPNProductType.Items
    '        '        If dtProductType.Rows.Count > 0 Then
    '        '            For Each drProductType As DataRow In dtProductType.Rows
    '        '                If item.Selected = True And drProductType("ProductTypeCode") = item.Value Then
    '        '                    IsCheckedMark = True
    '        '                    Exit For
    '        '                ElseIf item.Selected = False And drProductType("ProductTypeCode") = item.Value Then
    '        '                    IsCheckedRemove = True
    '        '                    ProductType = drProductType("DisbursementMode")
    '        '                    Exit For
    '        '                End If
    '        '            Next
    '        '        Else
    '        '            IsCheckedRemove = True
    '        '        End If

    '        '        If IsCheckedMark = True Then
    '        '            Dim objProductType As New ICProductType
    '        '            If objProductType.LoadByPrimaryKey(item.Value) Then
    '        '                If objProductType.DisbursementMode = "Direct Credit" Then
    '        '                    ShowControlsOnProductType(objProductType.DisbursementMode, False)
    '        '                    EnableDisableValidatorsOnProductType(objProductType.DisbursementMode, False)
    '        '                ElseIf objProductType.DisbursementMode = "Other Credit" Then
    '        '                    ShowControlsOnProductType(objProductType.DisbursementMode, False)
    '        '                    EnableDisableValidatorsOnProductType(objProductType.DisbursementMode, False)
    '        '                ElseIf objProductType.DisbursementMode = "COTC" Then
    '        '                    ShowControlsOnProductType(objProductType.DisbursementMode, False)
    '        '                    EnableDisableValidatorsOnProductType(objProductType.DisbursementMode, False)
    '        '                Else
    '        '                    ShowControlsOnProductType(Nothing, True)
    '        '                    EnableDisableValidatorsOnProductType(Nothing, True)
    '        '                End If
    '        '            End If
    '        '            IsCheckedMark = False

    '        '        ElseIf IsCheckedRemove = True Then
    '        '            Select Case (ProductType)
    '        '                Case "Direct Credit"
    '        '                    ClearControlsOnProductType(ProductType)
    '        '                    ShowControlsOnProductType(ProductType, True)
    '        '                    EnableDisableValidatorsOnProductType(ProductType, True)
    '        '                Case "Other Credit"
    '        '                    ClearControlsOnProductType(ProductType)
    '        '                    ShowControlsOnProductType(ProductType, True)
    '        '                    EnableDisableValidatorsOnProductType(ProductType, True)
    '        '                Case "COTC"
    '        '                    ClearControlsOnProductType(ProductType)
    '        '                    ShowControlsOnProductType(ProductType, True)
    '        '                    EnableDisableValidatorsOnProductType(ProductType, True)
    '        '                Case Else
    '        '                    ShowControlsOnProductType(Nothing, True)
    '        '                    EnableDisableValidatorsOnProductType(Nothing, True)
    '        '            End Select
    '        '            ProductType = Nothing
    '        '            IsCheckedRemove = False
    '        '        End If
    '        '    Next
    '        SetchkProdTypeJS()

    '    Catch ex As Exception
    '        SetchkProdTypeJS()
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

    'Protected Sub chklstAPNProductType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chklstAPNProductType.SelectedIndexChanged
    '    Try
    '        Dim objICProductType As New ICProductType
    '        Dim lit As New ListItem
    '        Dim selLI As New ListItem
    '        Dim ProductType As String = Nothing
    '        If objICProductType.LoadByPrimaryKey(hfchkProdCode.Value.ToString()) Then
    '            ProductType = objICProductType.DisbursementMode
    '            For Each lit In chklstAPNProductType.Items
    '                If lit.Value.ToString() = hfchkProdCode.Value.ToString() Then
    '                    selLI = lit
    '                    Exit For
    '                End If
    '            Next

    '            If selLI.Selected = True Then
    '                If ProductType = "Direct Credit" Then
    '                    ShowControlsOnProductType(ProductType, False)
    '                    EnableDisableValidatorsOnProductType(ProductType, False)
    '                ElseIf ProductType = "Other Credit" Then
    '                    ShowControlsOnProductType(ProductType, False)
    '                    EnableDisableValidatorsOnProductType(ProductType, False)
    '                ElseIf ProductType = "COTC" Then
    '                    ShowControlsOnProductType(ProductType, False)
    '                    EnableDisableValidatorsOnProductType(ProductType, False)
    '                Else
    '                    ShowControlsOnProductType(Nothing, True)
    '                    EnableDisableValidatorsOnProductType(Nothing, True)
    '                End If
    '            Else
    '                Select Case (ProductType)
    '                    Case "Direct Credit"
    '                        ClearControlsOnProductType(ProductType)
    '                        ShowControlsOnProductType(ProductType, True)
    '                        EnableDisableValidatorsOnProductType(ProductType, True)
    '                    Case "Other Credit"
    '                        ClearControlsOnProductType(ProductType)
    '                        ShowControlsOnProductType(ProductType, True)
    '                        EnableDisableValidatorsOnProductType(ProductType, True)
    '                    Case "COTC"
    '                        ClearControlsOnProductType(ProductType)
    '                        ShowControlsOnProductType(ProductType, True)
    '                        EnableDisableValidatorsOnProductType(ProductType, True)
    '                    Case Else
    '                        ShowControlsOnProductType(Nothing, True)
    '                        EnableDisableValidatorsOnProductType(Nothing, True)
    '                End Select
    '            End If
    '        End If

    '        Dim objPT As New ICProductType
    '        For Each lit In chklstAPNProductType.Items
    '            If lit.Selected = True Then
    '                If objPT.LoadByPrimaryKey(lit.Value) Then
    '                    If (lit.Value.ToString() <> hfchkProdCode.Value.ToString() And objPT.DisbursementMode = ProductType) Then

    '                        If ProductType = "Direct Credit" Then
    '                            ShowControlsOnProductType(ProductType, False)
    '                            EnableDisableValidatorsOnProductType(ProductType, False)
    '                        ElseIf ProductType = "Other Credit" Then
    '                            ShowControlsOnProductType(ProductType, False)
    '                            EnableDisableValidatorsOnProductType(ProductType, False)
    '                        ElseIf ProductType = "COTC" Then
    '                            ShowControlsOnProductType(ProductType, False)
    '                            EnableDisableValidatorsOnProductType(ProductType, False)
    '                        Else
    '                            ShowControlsOnProductType(Nothing, True)
    '                            EnableDisableValidatorsOnProductType(Nothing, True)
    '                        End If

    '                    End If
    '                End If
    '            End If
    '        Next


    '        SetchkProdTypeJS()


    '    Catch ex As Exception
    '        SetchkProdTypeJS()
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

    Protected Sub chklstAPNProductType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chklstAPNProductType.SelectedIndexChanged
        Try
            Dim objICProductType As New ICProductType
            Dim lit As New ListItem
            Dim selLI As New ListItem
            Dim ProductType As String = ""
            Dim isControlsVisible As Boolean = False

            Dim FTAlreadyExist As Boolean = False
            Dim IBFTAlreadyExist As Boolean = False
            Dim COTCAlreadyExist As Boolean = False
            Dim BillPayAlreadyExist As Boolean = False


            For Each lit In chklstAPNProductType.Items
              
                If objICProductType.LoadByPrimaryKey(lit.Value) Then
                    If ProductType = objICProductType.DisbursementMode Then
                        If lit.Selected = False Then
                            If isControlsVisible = True Then
                                Continue For
                            End If

                        End If
                    End If
                    ProductType = objICProductType.DisbursementMode
                    If lit.Selected = True Then
                        'ClearControlsOnProductType(ProductType)
                        If ProductType = "Direct Credit" Then
                            ShowControlsOnProductType(ProductType, True)
                            FTAlreadyExist = True
                            isControlsVisible = True
                            '  EnableDisableValidatorsOnProductType(ProductType, False)
                        ElseIf ProductType = "Other Credit" Then
                            IBFTAlreadyExist = True
                            ShowControlsOnProductType(ProductType, True)
                            isControlsVisible = True
                            ' EnableDisableValidatorsOnProductType(ProductType, False)
                        ElseIf ProductType = "COTC" Then
                            COTCAlreadyExist = True
                            ShowControlsOnProductType(ProductType, True)
                            isControlsVisible = True
                            'EnableDisableValidatorsOnProductType(ProductType, False)
                        ElseIf ProductType = "Bill Payment" Then
                            BillPayAlreadyExist = True
                            ShowControlsOnProductType(ProductType, True)
                            isControlsVisible = True
                            'EnableDisableValidatorsOnProductType(ProductType, False)
                        Else
                            ShowControlsOnProductType(Nothing, True)
                            'EnableDisableValidatorsOnProductType(Nothing, True)
                        End If
                    ElseIf lit.Selected = False Then
                        'ClearControlsOnProductType(ProductType)
                        If ProductType = "Direct Credit" Then
                            If FTAlreadyExist = False Then
                                ShowControlsOnProductType(ProductType, False)
                            End If

                        ElseIf ProductType = "Other Credit" Then
                            If IBFTAlreadyExist = False Then
                                ShowControlsOnProductType(ProductType, False)
                            End If

                        ElseIf ProductType = "COTC" Then
                            If COTCAlreadyExist = False Then
                                ShowControlsOnProductType(ProductType, False)
                            End If

                        ElseIf ProductType = "Bill Payment" Then
                            If BillPayAlreadyExist = False Then
                                ShowControlsOnProductType(ProductType, False)
                            
                            End If

                        End If
                    End If
                    ClearControlsOnProductType(ProductType)

                Else
                    ClearControlsOnProductType(Nothing)
                    ShowControlsOnProductType(Nothing, False)
                End If
            Next
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Function GetListofAllCheckedProductTypes() As DataTable
        Dim dtProductType As New DataTable
        Dim drProductType As DataRow
        dtProductType.Columns.Add("ProductTypeCode")
        dtProductType.Columns.Add("DisbursementMode")
        For Each item As ListItem In chklstAPNProductType.Items
            If item.Selected = True Then
                Dim objProductType As New ICProductType
                If objProductType.LoadByPrimaryKey(item.Value) Then
                    drProductType = dtProductType.NewRow()
                    drProductType("ProductTypeCode") = objProductType.ProductTypeCode
                    drProductType("DisbursementMode") = objProductType.DisbursementMode
                    dtProductType.Rows.Add(drProductType)
                End If
            End If
        Next
        Return dtProductType
    End Function

    'Private Sub EnableDisableValidatorsOnProductType(ByVal ProductType As String, ByVal IsDisable As Boolean)
    '    If IsDisable = True Then
    '        If ProductType = "Direct Credit" Then
    '            rad.GetSettingByBehaviorID("reBeneAccNo").Validation.IsRequired = False
    '            rad.GetSettingByBehaviorID("reBeneAccountTitle").Validation.IsRequired = False
    '            rad.GetSettingByBehaviorID("reBeneAccountBranchCode").Validation.IsRequired = False
    '            rad.GetSettingByBehaviorID("reBeneAccountCurrency").Validation.IsRequired = False
    '        ElseIf ProductType = "Other Credit" Then
    '            rad.GetSettingByBehaviorID("reBeneIBFTAccNo").Validation.IsRequired = False
    '            rad.GetSettingByBehaviorID("reBeneIBFTAccountTitle").Validation.IsRequired = False
    '            rfvddlBeneBank.Enabled = False
    '        ElseIf ProductType = "COTC" Then
    '            rad.GetSettingByBehaviorID("reBeneCNIC").Validation.IsRequired = False
    '        Else
    '            rad.GetSettingByBehaviorID("reBeneCNIC").Validation.IsRequired = False
    '            rad.GetSettingByBehaviorID("reBeneAccNo").Validation.IsRequired = False
    '            rad.GetSettingByBehaviorID("reBeneAccountTitle").Validation.IsRequired = False
    '            rad.GetSettingByBehaviorID("reBeneAccountBranchCode").Validation.IsRequired = False
    '            rad.GetSettingByBehaviorID("reBeneAccountCurrency").Validation.IsRequired = False
    '            rad.GetSettingByBehaviorID("reBeneIBFTAccNo").Validation.IsRequired = False
    '            rad.GetSettingByBehaviorID("reBeneIBFTAccountTitle").Validation.IsRequired = False
    '            rfvddlBeneBank.Enabled = False
    '        End If

    '    ElseIf IsDisable = False Then
    '        If ProductType = "Direct Credit" Then
    '            rad.GetSettingByBehaviorID("reBeneAccNo").Validation.IsRequired = True
    '            rad.GetSettingByBehaviorID("reBeneAccountTitle").Validation.IsRequired = True
    '            rad.GetSettingByBehaviorID("reBeneAccountBranchCode").Validation.IsRequired = True
    '            rad.GetSettingByBehaviorID("reBeneAccountCurrency").Validation.IsRequired = True
    '        ElseIf ProductType = "Other Credit" Then
    '            rad.GetSettingByBehaviorID("reBeneIBFTAccNo").Validation.IsRequired = True
    '            rad.GetSettingByBehaviorID("reBeneIBFTAccountTitle").Validation.IsRequired = True
    '            rfvddlBeneBank.Enabled = True
    '        ElseIf ProductType = "COTC" Then
    '            rad.GetSettingByBehaviorID("reBeneCNIC").Validation.IsRequired = True
    '        End If
    '    End If
    'End Sub

    Private Sub ShowControlsOnProductType(ByVal ProductType As String, ByVal IsDisable As Boolean)
        If IsDisable = False Then
            If ProductType = "Direct Credit" Then
                pnlBeneFTAcc.Visible = False

                rad.GetSettingByBehaviorID("reBeneAccNo").Validation.IsRequired = False
                rad.GetSettingByBehaviorID("reBeneAccountTitle").Validation.IsRequired = False
                rad.GetSettingByBehaviorID("reBeneAccountBranchCode").Validation.IsRequired = False
                rad.GetSettingByBehaviorID("reBeneAccountCurrency").Validation.IsRequired = False

            ElseIf ProductType = "Other Credit" Then
                pnlBeneIBFTAccNoAccTitle.Visible = False
                tdlblBeneBank.Style.Add("display", "none")
                tdddlBeneBank.Style.Add("display", "none")

                rad.GetSettingByBehaviorID("reBeneIBFTAccNo").Validation.IsRequired = False
                rad.GetSettingByBehaviorID("reBeneIBFTAccountTitle").Validation.IsRequired = False
                rfvddlBeneBank.Enabled = False
            ElseIf ProductType = "COTC" Then
                pnlBeneIDNoType.Visible = False
                rfvBeneIDType.Enabled = False
                rfvBenePickLocation.Enabled = False
                rad.GetSettingByBehaviorID("reBeneCNIC").Validation.IsRequired = False
            ElseIf ProductType = "Bill Payment" Then
                pnlBeneBillPament.Visible = False
                pnlBillInquiryDetails.Visible = False
                rfvUBPCompany.Enabled = False
                rfvUBPCompanyType.Enabled = False
                rad.GetSettingByBehaviorID("reUBPRefNo").Validation.IsRequired = False
                ddlCompanyType.ClearSelection()
                LoadddlCompanyName(ddlCompanyType.SelectedValue.ToString)
            Else
                pnlBeneFTAcc.Visible = False
                pnlBeneIBFTAccNoAccTitle.Visible = False
                pnlBeneBillPament.Visible = False
                pnlBillInquiryDetails.Visible = False
                tdlblBeneBank.Style.Add("display", "none")
                tdddlBeneBank.Style.Add("display", "none")
                pnlBeneIDNoType.Visible = False
                rfvBeneIDType.Enabled = False
                rfvBenePickLocation.Enabled = False
                rfvUBPCompany.Enabled = False
                rfvUBPCompanyType.Enabled = False
                rad.GetSettingByBehaviorID("reBeneCNIC").Validation.IsRequired = False
                rad.GetSettingByBehaviorID("reBeneAccNo").Validation.IsRequired = False
                rad.GetSettingByBehaviorID("reBeneAccountTitle").Validation.IsRequired = False
                rad.GetSettingByBehaviorID("reBeneAccountBranchCode").Validation.IsRequired = False
                rad.GetSettingByBehaviorID("reBeneAccountCurrency").Validation.IsRequired = False
                rad.GetSettingByBehaviorID("reBeneIBFTAccNo").Validation.IsRequired = False
                rad.GetSettingByBehaviorID("reBeneIBFTAccountTitle").Validation.IsRequired = False
                rad.GetSettingByBehaviorID("reUBPRefNo").Validation.IsRequired = False
                rfvddlBeneBank.Enabled = False
                ddlCompanyType.ClearSelection()
                LoadddlCompanyName(ddlCompanyType.SelectedValue.ToString)
            End If

        ElseIf IsDisable = True Then
            If ProductType = "Direct Credit" Then
                pnlBeneFTAcc.Visible = True

                rad.GetSettingByBehaviorID("reBeneAccNo").Validation.IsRequired = True
                rad.GetSettingByBehaviorID("reBeneAccountTitle").Validation.IsRequired = True
                rad.GetSettingByBehaviorID("reBeneAccountBranchCode").Validation.IsRequired = True
                rad.GetSettingByBehaviorID("reBeneAccountCurrency").Validation.IsRequired = True
            ElseIf ProductType = "Other Credit" Then
                pnlBeneIBFTAccNoAccTitle.Visible = True
                tdlblBeneBank.Style.Remove("display")
                tdddlBeneBank.Style.Remove("display")

                rad.GetSettingByBehaviorID("reBeneIBFTAccNo").Validation.IsRequired = True
                rad.GetSettingByBehaviorID("reBeneIBFTAccountTitle").Validation.IsRequired = True
                rfvddlBeneBank.Enabled = True
            ElseIf ProductType = "COTC" Then
                pnlBeneIDNoType.Visible = True
                rfvBeneIDType.Enabled = True
                rfvBenePickLocation.Enabled = False
                rad.GetSettingByBehaviorID("reBeneCNIC").Validation.IsRequired = True
            ElseIf ProductType = "Bill Payment" Then
                pnlBeneBillPament.Visible = True
                btnBillInquiry.Visible = False
                pnlBillInquiryDetails.Visible = False
                rfvUBPCompany.Enabled = True
                rfvUBPCompanyType.Enabled = True
                txtUBPRefNo.ReadOnly = True
                rad.GetSettingByBehaviorID("reUBPRefNo").Validation.IsRequired = True
                ddlCompanyType.ClearSelection()
                LoadddlCompanyName(ddlCompanyType.SelectedValue.ToString)
            End If
        End If
    End Sub

    Private Sub ClearControlsOnProductType(ByVal ProductType As String)
        Dim dt As New DataTable
        Dim IsControlsClear As Boolean = False
        Dim IsSameModeExist As Boolean = False
        dt = GetListofAllCheckedProductTypes()

        For Each dr As DataRow In dt.Rows
            If dr("DisbursementMode") = ProductType Then
                IsSameModeExist = True
            End If
        Next

        If IsSameModeExist = False Then
            IsControlsClear = True
        End If

        If IsControlsClear = True Then
            If ProductType = "Direct Credit" Then
                If txtBeneAccNo.Visible = False Then
                    txtBeneAccNo.Text = Nothing
                End If
                If txtBeneAccountTitle.Visible = False Then
                    txtBeneAccountTitle.Text = Nothing
                End If
                If txtBeneAccountBranchCode.Visible = False Then
                    txtBeneAccountBranchCode.Text = Nothing
                End If
                If txtBeneAccountCurrency.Visible = False Then
                    txtBeneAccountCurrency.Text = Nothing
                End If
            ElseIf ProductType = "Other Credit" Then
                If txtBeneIBFTAccNo.Visible = False Then
                    txtBeneIBFTAccNo.Text = Nothing
                End If
                If txtBeneIBFTAccountTitle.Visible = False Then
                    txtBeneIBFTAccountTitle.Text = Nothing
                End If
                LoadddlBeneBank()

            ElseIf ProductType = "COTC" Then
                If txtBeneCNIC.Visible = False Then
                    txtBeneCNIC.Text = Nothing
                End If
                If ddlBeneIDType.Visible = False Then
                    ddlBeneIDType.SelectedValue = "0"
                End If
                LoadddlBenePickLocation()
            ElseIf ProductType = "Bill Payment" Then
                If txtUBPRefNo.Visible = False Then
                    txtUBPRefNo.Text = Nothing
                End If
                If ddlCompanyType.Visible = False Then
                    ddlCompanyType.SelectedValue = "0"
                End If
                LoadddlCompanyName(ddlCompanyType.SelectedValue.ToString)
            End If
        End If
    End Sub

    Private Sub LoadddlBenePickLocation()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlBenePickLocation.Items.Clear()
            ddlBenePickLocation.Items.Add(lit)
            ddlBenePickLocation.AppendDataBoundItems = True
            ddlBenePickLocation.DataSource = ICOfficeController.GetPrincipalBankBranchActiveAndApprove
            ddlBenePickLocation.DataTextField = "OfficeName"
            ddlBenePickLocation.DataValueField = "OfficeID"
            ddlBenePickLocation.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCompanyType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCompanyType.SelectedIndexChanged
        Try
            LoadddlCompanyName(ddlCompanyType.SelectedValue.ToString)
            txtUBPRefNo.Text = ""
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCompanyName(ByVal CompanyType As String)
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlUBPCompanyName.Items.Clear()
            ddlUBPCompanyName.Items.Add(lit)
            ddlUBPCompanyName.AppendDataBoundItems = True
            ddlUBPCompanyName.DataSource = ICUBPSCompanyController.GetAllUBPActiveApproveCompanyByCompanyType(CompanyType)
            ddlUBPCompanyName.DataTextField = "CompanyName"
            ddlUBPCompanyName.DataValueField = "CompanyCode"
            ddlUBPCompanyName.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ClearBillInfoLabels()
        lblBillingMonthInfo.Text = ""
        lblBillingStatusInfo.Text = ""
        lblBillAmountInfo.Text = ""
        lblBillDueDateInfo.Text = ""
    End Sub
    Private Sub FillBillInquiryLabels()
        Try
            Dim dt As New DataTable
            Dim objUBPCompany As New ICUBPSCompany
            Dim collObjICAccounts As New ICAccountsCollection

            collObjICAccounts.Query.Where(collObjICAccounts.Query.CompanyCode = ddlCompany.SelectedValue.ToString)
            collObjICAccounts.Query.Where(collObjICAccounts.Query.IsActive = True And collObjICAccounts.Query.IsApproved = True)
            collObjICAccounts.Query.es.Top = 1
            collObjICAccounts.Query.Load()

            If collObjICAccounts.Count = 0 Then
                Throw New Exception("Client accounts are not active to perform bill inquiry")
            End If
            If ddlUBPCompanyName.SelectedValue.ToString = "0" Then
                Throw New Exception("Select UBPS Company")
            End If

            If txtUBPRefNo.Text = "" Or txtUBPRefNo.Text Is Nothing Then
                Throw New Exception("Enter UBPS Reference No")
            End If

            Dim RRefNo As String = Nothing
            objUBPCompany.Query.Where(objUBPCompany.Query.CompanyCode = ddlUBPCompanyName.SelectedValue.ToString And objUBPCompany.Query.IsActive = True And objUBPCompany.Query.IsApproved = True)
            objUBPCompany.Query.Load()
            If objUBPCompany.Query.Load = False Then
                Throw New Exception("Unable to load UBP Company")
            End If

            'objUBPCompany.LoadByPrimaryKey(ddlUBPCompanyName.SelectedValue.ToString)


            If ICUBPSCompanyController.CheckReferenceFieldLength(txtUBPRefNo.Text.ToString.ToString.Length, objUBPCompany.UBPSCompanyID) = False Then
                Throw New Exception("Invalid UBPS Reference No Length")
            End If
            If ICUBPSCompanyController.CheckReferenceFieldDataWithRegularExpression(txtUBPRefNo.Text.ToString.Trim, objUBPCompany.UBPSCompanyID) = False Then
                Throw New Exception("Invalid UBPS Reference No")
            End If

            dt = CBUtilities.BillInquiryForBene("Beneficiary Bill Inquiry", txtUBPRefNo.Text, "", objUBPCompany.UBPSCompanyID, txtUBPRefNo.Text, "", 0, RRefNo, collObjICAccounts(0).BranchCode, collObjICAccounts(0).AccountNumber)

            If dt.Rows.Count > 0 Then
                ClearBillInfoLabels()
                pnlBillInquiryDetails.Visible = True
                If CDate(dt.Rows(0)("DueDate")) >= CDate(Date.Now) Then
                    lblBillAmountInfo.Text = CDbl(dt.Rows(0)("AmountWithInDueDate")).ToString("N2")
                Else
                    lblBillAmountInfo.Text = CDbl(dt.Rows(0)("AmountAfterDueDate")).ToString("N2")
                End If
                lblBillingMonthInfo.Text = dt.Rows(0)("BillingMonth")
                lblBillingStatusInfo.Text = dt.Rows(0)("BillStatus")

                lblBillDueDateInfo.Text = dt.Rows(0)("DueDate")
            Else
                ClearBillInfoLabels()
                pnlBillInquiryDetails.Visible = False
                Throw New Exception("No data found against given information")
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Protected Sub ddlUBPCompanyName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUBPCompanyName.SelectedIndexChanged
        Try
            Dim objICUBPCompany As New ICUBPSCompany
            If ddlUBPCompanyName.SelectedValue.ToString <> "0" Then

                objICUBPCompany.Query.Where(objICUBPCompany.Query.CompanyCode = ddlUBPCompanyName.SelectedValue.ToString And objICUBPCompany.Query.IsActive = True And objICUBPCompany.Query.IsApproved = True)
                objICUBPCompany.Query.Load()
                If objICUBPCompany.Query.Load = False Then
                    Throw New Exception("Unable to load UBP Company")
                End If

                'objICUBPCompany.LoadByPrimaryKey(ddlUBPCompanyName.SelectedValue.ToString)

                If objICUBPCompany.IsBillingInquiry = True Then
                    btnBillInquiry.Visible = True
                Else
                    btnBillInquiry.Visible = False
                End If
                txtUBPRefNo.MaxLength = objICUBPCompany.ReferenceFieldMaxLength
                txtUBPRefNo.Enabled = True
                'txtUBPRefNo.Text = ""
                txtUBPRefNo.ReadOnly = False
            Else
                txtUBPRefNo.MaxLength = 100
                txtUBPRefNo.Text = ""
                txtUBPRefNo.ReadOnly = True
                btnBillInquiry.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnBillInquiry_Click(sender As Object, e As EventArgs) Handles btnBillInquiry.Click
        If Page.IsValid Then
            Try
                FillBillInquiryLabels()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub


End Class
