﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddBulkBeneficiary.ascx.vb" Inherits="DesktopModules_BeneficiaryManagement_AddBulkBeneficiary" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .style1 {
        width: 30%;
    }
</style>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="left" valign="top">
                        <asp:Label ID="lblBulkBeneList" runat="server" Text="Add Beneficiary List" CssClass="headingblue"></asp:Label>
                        <asp:HiddenField ID="hfFilePath" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <table width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" colspan="2">Note:&nbsp; Fields marked as
            <asp:Label ID="lblFieldsReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        &nbsp;are required.
                    </td>
                    <td align="left" valign="top" class="style1">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
                        <asp:Label ID="lblGroupReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" class="style1">
                        <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
                        <asp:Label ID="lblCompanyReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%"></td>
                    <td align="left" valign="top" class="style1">
                        <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:RequiredFieldValidator ID="rfvddlGroup" runat="server"
                            ControlToValidate="ddlGroup" CssClass="lblEror" Display="Dynamic"
                            ErrorMessage="Please select Group" SetFocusOnError="True"
                            ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server"
                            ControlToValidate="ddlCompany" CssClass="lblEror" Display="Dynamic"
                            ErrorMessage="Please select Company" SetFocusOnError="True"
                            ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblPaymentNature" runat="server" Text="Select Payment Nature" CssClass="lbl"></asp:Label>
                        <asp:Label ID="lblPaymentNatureReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" class="style1">
                        <asp:Label ID="lblBeneGroup" runat="server" Text="Select Beneficiary Group" CssClass="lbl"></asp:Label>
                        <asp:Label ID="lblBeneGroupReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlPaymentNature" runat="server" CssClass="dropdown" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" class="style1">
                        <asp:DropDownList ID="ddlBeneGroup" runat="server" CssClass="dropdown">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:RequiredFieldValidator ID="rfvddlPaymentNature" runat="server"
                            ControlToValidate="ddlPaymentNature" CssClass="lblEror" Display="Dynamic"
                            ErrorMessage="Please select Payment Nature" SetFocusOnError="True"
                            ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:RequiredFieldValidator ID="rfvddlBeneGroup" runat="server"
                            ControlToValidate="ddlBeneGroup" CssClass="lblEror" Display="Dynamic"
                            ErrorMessage="Please select Beneficiary Group" SetFocusOnError="True"
                            ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblBeneFileUpload" runat="server" Text="Select Beneficiary File Upload" CssClass="lbl"></asp:Label>
                        <asp:Label ID="lblBeneNameReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" class="style1">&nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%" colspan="2">
                        <asp:FileUpload ID="fuBeneinBulk" runat="server" />
                        &nbsp;
            <asp:HyperLink ID="hlDownloadSampleFile" runat="server" ToolTip="Download" Target="_blank" NavigateUrl="../../Sample Files/Beneficiary Bulk Upload (Sample File).xlsx">Download Sample File</asp:HyperLink>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:RequiredFieldValidator ID="rfvFile" runat="server" ControlToValidate="fuBeneinBulk"
                            CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select File" SetFocusOnError="True"
                            ToolTip="Required Field"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                    <td align="left" valign="top" class="style1">&nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top">

            <telerik:RadGrid ID="gvBene" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                <ClientSettings>
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                </ClientSettings>
                <MasterTableView UseAllDataFields="true" TableLayout="Fixed">
                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn DataField="BeneficiaryCode" HeaderText="Bene Code" SortExpression="BeneficiaryCode">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryName" HeaderText="Bene Name" SortExpression="BeneficiaryName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ProductTypeCode" HeaderText="Product Type Code" SortExpression="ProductTypeCode">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryIDNo" HeaderText="Bene ID No." SortExpression="BeneficiaryIDNo">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryIDType" HeaderText="Bene ID Type" SortExpression="BeneficiaryIDType">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryFTAccountNumber" HeaderText="FT Account Number" SortExpression="BeneficiaryFTAccountNumber">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryFTAccountTitle" HeaderText="FT Account Title" SortExpression="BeneficiaryFTAccountTitle">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryFTAccountBranchCode" HeaderText="FT Account Branch Code" SortExpression="BeneficiaryFTAccountBranchCode">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryFTAccountCurrency" HeaderText="FT Account Currency" SortExpression="BeneficiaryFTAccountCurrency">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryIBFTAccountNumber" HeaderText="IBFT Account Number" SortExpression="BeneficiaryIBFTAccountNumber">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryIBFTAccountTitle" HeaderText="IBFT Account Title" SortExpression="BeneficiaryIBFTAccountTitle">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryBankCode" HeaderText="Bene Bank Code" SortExpression="BeneficiaryBankCode">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryBankName" HeaderText="Bene Bank Name" SortExpression="BeneficiaryBankName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryBankAddress" HeaderText="Bene Bank Address" SortExpression="BeneficiaryBankAddress">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryBranchCode" HeaderText="Bene Branch Code" SortExpression="BeneficiaryBranchCode">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryBranchName" HeaderText="Bene Branch Name" SortExpression="BeneficiaryBranchName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryBranchAddress" HeaderText="Bene Branch Address" SortExpression="BeneficiaryBranchAddress">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryMobile" HeaderText="Bene Mobile" SortExpression="BeneficiaryMobile">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryEmail" HeaderText="Bene Email" SortExpression="BeneficiaryEmail">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryPhone" HeaderText="Bene Phone" SortExpression="BeneficiaryPhone">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryAddress" HeaderText="Bene Address" SortExpression="BeneficiaryAddress">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryCountry" HeaderText="Bene Country" SortExpression="BeneficiaryCountry">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryProvince" HeaderText="Bene Province" SortExpression="BeneficiaryProvince">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryCity" HeaderText="Bene City" SortExpression="BeneficiaryCity">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="UBPCompanyID" HeaderText="UBP Company Code" SortExpression="UBPCompanyID">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="UBPRefNo" HeaderText="UBP Ref. No" SortExpression="UBPRefNo">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                    <PagerStyle AlwaysVisible="True" />
                </MasterTableView>
                <ItemStyle CssClass="rgRow" />
                <PagerStyle AlwaysVisible="True" />
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <asp:Button ID="btnAddBeneinBulk" runat="server" Text="Add Beneficiaries in Bulk"
                CssClass="btn" CausesValidation="False" />
            <asp:Button ID="btnProceed" runat="server" Text="Proceed" CssClass="btn"  />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px"
                CausesValidation="False" />
        </td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <telerik:RadGrid ID="gvUnVarified" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                CellSpacing="0" ShowFooter="True">
                <AlternatingItemStyle CssClass="rgAltRow" />
                <MasterTableView>
                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn DataField="RowNo" HeaderText="Line No." SortExpression="RowNo">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ErrorMessage" HeaderText="ErrorMessage" SortExpression="ErrorMessage">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                    <PagerStyle AlwaysVisible="True" />
                </MasterTableView>
                <ItemStyle CssClass="rgRow" />
                <PagerStyle AlwaysVisible="True" />
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
        </td>
    </tr>
</table>

