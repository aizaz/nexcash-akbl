﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddBeneficiary.ascx.vb" Inherits="DesktopModules_BeneficiaryManagement_AddBeneficiary" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .style1 {
        width: 30%;
    }
</style>

<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }

    function ValidateAPNProductTypeList(source, args) {
        var chkListModules = document.getElementById('<%= chklstAPNProductType.ClientID%>');
        var chkListinputs = chkListModules.getElementsByTagName("input");
        for (var i = 0; i < chkListinputs.length; i++) {
            if (chkListinputs[i].checked) {
                args.IsValid = true;
                return;
            }
        }
        args.IsValid = false;
    }
</script>
<telerik:RadInputManager ID="rad" runat="server">
    <telerik:RegExpTextBoxSetting BehaviorID="reBeneCode" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9_\-\/\\]{1,10}$" ErrorMessage="Invalid Beneficiary Code"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <%--  <telerik:RegExpTextBoxSetting BehaviorID="reBeneName" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9 ]{1,250}$" ErrorMessage="Invalid Beneficiary Name"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>--%>
    <telerik:RegExpTextBoxSetting BehaviorID="reBeneName" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9 @._-]{1,250}$" ErrorMessage="Invalid Beneficiary Name"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reBeneCNIC" Validation-IsRequired="false"
        ValidationExpression="^[a-zA-Z0-9-]{1,50}$" ErrorMessage="Invalid Beneficiary Identification No"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneCNIC" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reBeneAccNo" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Invalid Beneficiary FT Account Number" ValidationExpression="^[a-zA-Z0-9]{1,100}$">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneAccNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reBeneAccountTitle" Validation-IsRequired="false"
        ValidationExpression="^[a-zA-Z0-9 -_/()]{1,500}$" ErrorMessage="Invalid Beneficiary FT Account Title"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneAccountTitle" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reBeneAccountBranchCode" Validation-IsRequired="false"
        ValidationExpression="^[a-zA-Z0-9]{1,20}$" ErrorMessage="Invalid Beneficiary Account Branch Code"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneAccountBranchCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reBeneAccountCurrency" Validation-IsRequired="false"
        ValidationExpression="^[a-zA-Z0-9]{1,50}$" ErrorMessage="Invalid Beneficiary Account Currency"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneAccountCurrency" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reBeneBankAddress" Validation-IsRequired="false"
        ErrorMessage="Invalid Beneficiary Bank Address" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneBankAddress" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reBeneBranchCode" Validation-IsRequired="false"
        ValidationExpression="^[a-zA-Z0-9 -_]{1,10}$" ErrorMessage="Invalid Beneficiary Branch Code"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneBranchCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reBeneBranchName" Validation-IsRequired="false"
        ValidationExpression="^[a-zA-Z0-9 ]{1,250}$" ErrorMessage="Invalid Beneficiary Branch Name"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneBranchName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reBeneBranchAddress" Validation-IsRequired="false"
        ErrorMessage="Invalid Beneficiary Branch Address" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneBranchAddress" />
        </TargetControls>
    </telerik:TextBoxSetting>
   <%-- <telerik:RegExpTextBoxSetting BehaviorID="reBeneMobile" Validation-IsRequired="false"
        ValidationExpression="^\+?[\d- ]{2,11}$" ErrorMessage="Invalid Beneficiary Mobile"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneMobile" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>--%>
     <telerik:RegExpTextBoxSetting BehaviorID="reBeneMobile" Validation-IsRequired="false"
        ValidationExpression="^\+[0-9]{2,20}$" ErrorMessage="Invalid Beneficiary Mobile"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneMobile" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reBeneEmail" Validation-IsRequired="false"
        ValidationExpression="^([a-zA-Z0-9_\-\.]+)@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$"
        ErrorMessage="Invalid Beneficiary Email" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneEmail" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reBenePhone" Validation-IsRequired="false"
        ValidationExpression="^\+?[\d- ]{2,15}$" ErrorMessage="Invalid Beneficiary Phone"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBenePhone" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reBeneAddress" Validation-IsRequired="false"
        ErrorMessage="Invalid Beneficiary Address" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneAddress" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reBeneIBFTAccNo" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Invalid Beneficiary IBFT Account Number" ValidationExpression="^[a-zA-Z0-9]{1,100}$">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneIBFTAccNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reBeneIBFTAccountTitle" Validation-IsRequired="false"
        ValidationExpression="^[a-zA-Z0-9 -_/()]{1,500}$" ErrorMessage="Invalid Beneficiary IBFT Account Title"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneIBFTAccountTitle" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reUBPRefNo" 
        EmptyMessage="" ErrorMessage="Invalid UBP Ref No">
        <TargetControls>
            <telerik:TargetInput ControlID="txtUBPRefNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>

<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">Note:&nbsp; Fields marked as
            <asp:Label ID="lblFieldsReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" class="style1">&nbsp;


        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblGroupReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblCompanyReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="true">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%"></td>
        <td align="left" valign="top" class="style1">
            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="true">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlGroup" runat="server"
                ControlToValidate="ddlGroup" CssClass="lblEror" Display="Dynamic"
                ErrorMessage="Please select Group" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server"
                ControlToValidate="ddlCompany" CssClass="lblEror" Display="Dynamic"
                ErrorMessage="Please select Company" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblPaymentNatureMode" runat="server" Text="Select Payment Nature" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblPaymentNatureModeReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblBeneGroup" runat="server" Text="Select Beneficiary Group" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblBeneGroupReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlPaymentNature" runat="server" CssClass="dropdown" AutoPostBack="true">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:DropDownList ID="ddlBeneGroup" runat="server" CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvPaymentNature" runat="server"
                ControlToValidate="ddlPaymentNature" CssClass="lblEror" Display="Dynamic"
                ErrorMessage="Please select Payment Nature" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvBeneGroup" runat="server"
                ControlToValidate="ddlBeneGroup" CssClass="lblEror" Display="Dynamic"
                ErrorMessage="Please select Beneficiary Group" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAPNProductType" runat="server" Text="Select Product Type" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblAPNProductTypeReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td id="tdlblBeneBank" runat="server" align="left" valign="top" style="width: 25%; display: none">
            <asp:Label ID="lblBeneBank" runat="server" Text="Select Beneficiary Bank" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblBeneBankReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBoxList ID="chklstAPNProductType" runat="server" CssClass="chkBox" Width="90%" AutoPostBack="true">
            </asp:CheckBoxList>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td id="tdddlBeneBank" runat="server" align="left" valign="top" style="width: 25%; display: none">
            <asp:DropDownList ID="ddlBeneBank" runat="server" CssClass="dropdown" AutoPostBack="true">
            </asp:DropDownList>
            <br />
            <br />
            <asp:RequiredFieldValidator ID="rfvddlBeneBank" runat="server" Enabled="false"
                ControlToValidate="ddlBeneBank" CssClass="lblEror" Display="Dynamic"
                ErrorMessage="Please select Beneficiary Bank" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CustomValidator runat="server" ID="cusvalAPNProductType" ClientValidationFunction="ValidateAPNProductTypeList"
                ErrorMessage="Please select atleast one Product Type"></asp:CustomValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBeneCode" runat="server" Text="Beneficiary Code" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblBeneCodeReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblBeneName" runat="server" Text="Beneficiary Name" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblBeneNameReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtBeneCode" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:TextBox ID="txtBeneName" runat="server" CssClass="txtbox" MaxLength="250"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <asp:Panel ID="pnlBeneBillPament" runat="server" Visible="false">
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:Label ID="lblCompanyType" runat="server" Text="Company Type" CssClass="lbl"></asp:Label>
                <asp:Label ID="lblReqCompanyType" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" class="style1">
                <asp:Label ID="lblUBPSCompany" runat="server" Text="UBPS Company" CssClass="lbl"></asp:Label>
                <asp:Label ID="lblReqUBPSCompany" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:DropDownList ID="ddlCompanyType" runat="server"  AutoPostBack="true"
                CssClass="dropdown">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem>Utility</asp:ListItem>
                <asp:ListItem>Prepaid</asp:ListItem>
                <asp:ListItem>Postpaid</asp:ListItem>
                <asp:ListItem>Internet Service Provider</asp:ListItem>
            </asp:DropDownList>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" class="style1">
                <asp:DropDownList ID="ddlUBPCompanyName" runat="server" 
                CssClass="dropdown" AutoPostBack="true">
            </asp:DropDownList>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                 <asp:RequiredFieldValidator ID="rfvUBPCompanyType" runat="server"
                ControlToValidate="ddlCompanyType" CssClass="lblEror" Display="Dynamic"
                ErrorMessage="Please select Company Type" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
            <td align="left" valign="top" style="width: 25%">   <asp:RequiredFieldValidator ID="rfvUBPCompany" runat="server"
                ControlToValidate="ddlUBPCompanyName" CssClass="lblEror" Display="Dynamic"
                ErrorMessage="Please select UBP Company" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator></td>
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:Label ID="lblUBPRefNo" runat="server" Text="Reference No" CssClass="lbl"></asp:Label>
                <asp:Label ID="lblReqRefNo" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" class="style1">
               
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:TextBox ID="txtUBPRefNo" runat="server" CssClass="txtbox"></asp:TextBox>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" class="style1">
                
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
            <td align="left" valign="top" style="width: 25%">

                <asp:Button ID="btnBillInquiry" runat="server" Text="Bill Inquiry" CssClass="btn" Width="71px" CausesValidation="true" ValidationGroup="BillPayment" />

            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        </tr>
    </asp:Panel>
    <asp:Panel ID="pnlBillInquiryDetails" runat="server" Visible="false">
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:Label ID="lblBillingMonth" runat="server" Text="Billing Month" CssClass="lbl"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" class="style1">
                <asp:Label ID="lblBillStatus" runat="server" Text="Bill Status" CssClass="lbl"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBillingMonthInfo" runat="server" Text="" CssClass="lbl"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" class="style1">
               <asp:Label ID="lblBillingStatusInfo" runat="server" Text="" CssClass="lbl"></asp:Label>
            
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
          <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:Label ID="lblBillAmount" runat="server" Text="Bill Amount" CssClass="lbl"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" class="style1">
                <asp:Label ID="lblBillDueDate" runat="server" Text="Due Date" CssClass="lbl"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBillAmountInfo" runat="server" Text="" CssClass="lbl"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" class="style1">
               <asp:Label ID="lblBillDueDateInfo" runat="server" Text="" CssClass="lbl"></asp:Label>
            
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
    </asp:Panel>
    <asp:Panel ID="pnlBeneFTAcc" runat="server" Visible="false">
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:Label ID="lblBeneAccNo" runat="server" Text="Beneficiary FT Account Number" CssClass="lbl"></asp:Label>
                <asp:Label ID="lblBeneAccNoReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" class="style1">
                <asp:Label ID="lblBeneAccountTitle" runat="server" Text="Beneficiary FT Account Title" CssClass="lbl"></asp:Label>
                <asp:Label ID="lblBeneAccountTitleReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:TextBox ID="txtBeneAccNo" runat="server" CssClass="txtbox" MaxLength="100" AutoPostBack="true"></asp:TextBox>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" class="style1">
                <asp:TextBox ID="txtBeneAccountTitle" runat="server" CssClass="txtbox" MaxLength="500" ReadOnly="true"></asp:TextBox>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:Label ID="lblBeneAccountBranchCode" runat="server" Text="Beneficiary Account Branch Code" CssClass="lbl"></asp:Label>
                <asp:Label ID="lblBeneAccountBranchCodeReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" class="style1">
                <asp:Label ID="lblBeneAccountCurrency" runat="server" Text="Beneficiary Account Currency" CssClass="lbl"></asp:Label>
                <asp:Label ID="lblBeneAccountCurrencyReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:TextBox ID="txtBeneAccountBranchCode" runat="server" CssClass="txtbox" MaxLength="20" ReadOnly="true"></asp:TextBox>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" class="style1">
                <asp:TextBox ID="txtBeneAccountCurrency" runat="server" CssClass="txtbox" MaxLength="50" ReadOnly="true"></asp:TextBox>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        </tr>
    </asp:Panel>
    <asp:Panel ID="pnlBeneIBFTAccNoAccTitle" runat="server" Visible="false">
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:Label ID="lblBeneIBFTAccNo" runat="server" Text="Beneficiary IBFT Account Number" CssClass="lbl"></asp:Label>
                <asp:Label ID="lblBeneIBFTAccNoReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" class="style1">
                <asp:Label ID="lblBeneIBFTAccountTitle" runat="server" Text="Beneficiary IBFT Account Title" CssClass="lbl"></asp:Label>
                <asp:Label ID="lblBeneIBFTAccountTitleReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:TextBox ID="txtBeneIBFTAccNo" runat="server" CssClass="txtbox" MaxLength="100" AutoPostBack="true"></asp:TextBox>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" class="style1">
                <asp:TextBox ID="txtBeneIBFTAccountTitle" runat="server" CssClass="txtbox" MaxLength="500" ReadOnly="true"></asp:TextBox>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        </tr>
    </asp:Panel>
    <asp:Panel ID="pnlBeneIDNoType" runat="server" Visible="false">
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:Label ID="lblBeneCNIC" runat="server" Text="Beneficiary Identification No" CssClass="lbl"></asp:Label>
                <asp:Label ID="lblBeneCNICReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" class="style1">
                <asp:Label ID="lblBeneIDType" runat="server" Text="Beneficiary Identification Type" CssClass="lbl"></asp:Label>
                <asp:Label ID="lblBeneIDTypeReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:TextBox ID="txtBeneCNIC" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" class="style1">
                <asp:DropDownList ID="ddlBeneIDType" runat="server" CssClass="dropdown">
                    <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                    <asp:ListItem>CNIC</asp:ListItem>
                    <asp:ListItem>Passport</asp:ListItem>
                    <asp:ListItem>NICOP</asp:ListItem>
                    <asp:ListItem>Driving License</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
            <td align="left" valign="top" style="width: 25%">
                <asp:RequiredFieldValidator ID="rfvBeneIDType" runat="server" ControlToValidate="ddlBeneIDType"
                    Display="Dynamic" Enabled="False" ErrorMessage="Please select Beneficiary Identification Type"
                    InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        </tr>

        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:Label ID="lblBenePickUpLocation" runat="server" CssClass="lbl" Text="Pick Up Location"></asp:Label>
                <%--<asp:Label ID="lblBenePickUpLocationReq" runat="server" Text="*" ForeColor="Red"></asp:Label>--%>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" class="style1"></td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:DropDownList ID="ddlBenePickLocation" runat="server" CssClass="dropdown">
                </asp:DropDownList>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
            <td align="left" valign="top" class="style1"></td>
            <td align="left" valign="top" style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 25%">
                <asp:RequiredFieldValidator ID="rfvBenePickLocation" runat="server" ControlToValidate="ddlBenePickLocation"
                    Display="Dynamic" Enabled="False" ErrorMessage="Please select Pick Up Location"
                    InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
            </td>
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
            <td align="left" valign="top" style="width: 25%"></td>
            <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        </tr>
    </asp:Panel>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBeneBranchAddress" runat="server" Text="Beneficiary Branch Address" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblBeneBankAddress" runat="server" Text="Beneficiary Bank Address" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtBeneBranchAddress" runat="server" CssClass="txtbox" MaxLength="250"
                TextMode="MultiLine" Height="70px" onkeypress="return textboxMultilineMaxNumber(this,250)"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:TextBox ID="txtBeneBankAddress" runat="server" CssClass="txtbox" MaxLength="250"
                TextMode="MultiLine" Height="70px" onkeypress="return textboxMultilineMaxNumber(this,250)"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBeneMobile" runat="server" Text="Beneficiary Mobile" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblBenePhone" runat="server" Text="Beneficiary Phone" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtBeneMobile" runat="server" CssClass="txtbox" MaxLength="20"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:TextBox ID="txtBenePhone" runat="server" CssClass="txtbox" MaxLength="15"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBeneEmail" runat="server" Text="Beneficiary Email" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblBeneAddress" runat="server" Text="Beneficiary Address" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtBeneEmail" runat="server" CssClass="txtbox" MaxLength="100"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:TextBox ID="txtBeneAddress" runat="server" CssClass="txtbox" MaxLength="250"
                TextMode="MultiLine" Height="70px" onkeypress="return textboxMultilineMaxNumber(this,250)"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBeneCountry" runat="server" Text="Beneficiary Country" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblBeneProvince" runat="server" Text="Beneficiary Province" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlBeneCountry" runat="server" CssClass="dropdown" AutoPostBack="true">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:DropDownList ID="ddlBeneProvince" runat="server" CssClass="dropdown" AutoPostBack="true">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBeneCity" runat="server" Text="Beneficiary City" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" class="style1">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlBeneCity" runat="server" CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top"></td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblActive" runat="server" Text="Is Active" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblApproved" runat="server" Text="Is Approved" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkActive" runat="server" Text=" " CssClass="chkBox" Checked="True" />
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkApproved" runat="server" Text=" " Visible="False" CssClass="chkBox" />
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnApproved" runat="server" Text="Approved" CssClass="btn" Visible="False" Width="71px" />
            &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False" />
            &nbsp;
        </td>
    </tr>
</table>
