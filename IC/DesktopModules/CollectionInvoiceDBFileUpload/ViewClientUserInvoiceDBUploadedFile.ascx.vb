﻿Imports ICBO
Imports ICBO.IC
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports Telerik.Web.UI
Imports System.Runtime.Remoting.Messaging
Imports System.Threading
Imports Microsoft.Exchange.WebServices.Data
Imports EntitySpaces.Core
Partial Class DesktopModules_CollectionInvoiceDBFileUpload_ViewClientUserInvoiceDBUploadedFile
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrICAssignUserRolsID As New ArrayList
    Private htRights As Hashtable
    Private Delegate Sub LongRun(ByVal UsersID As String, ByVal FileID As String, ByVal AcqMode As String)
    Protected Sub DesktopModules_CollectionInvoiceDBFileUpload_ViewClientUserInvoiceDBUploadedFile_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then


                'Dim scr As String = "function RedirectToHome() { window.location.href = '" & NavigateURL(PortalSettings.HomeTabId) & "';}"
                'Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "scr", scr, True)

                btnSave.Attributes.Item("onclick") = "if (Page_ClientValidate('save')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnSave, Nothing).ToString() & " } "
                btnProceed.Attributes.Item("onclick") = "if (Page_ClientValidate('Proceed')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnProceed, Nothing).ToString() & " } "

                ViewState("htRights") = Nothing
                GetPageAccessRights()

                Dim dt As New DataTable

                btnSave.Visible = CBool(htRights("Upload"))
                If Me.UserInfo.IsSuperUser = False Then

                    SetArraListRoleID()
                    DesignDts()
                    FillDesignedDtByAPNatureByAssignedRole()
                    LoadddlCollectionAccountCollectionNature()
                    LoadddlInvoiceDBTemplate()
                    'HideGvClientFiles()
                    LoadgvClientFiles(True)
                Else
                    UIUtilities.ShowDialog(Me, "Error", "Sorry! You are not authorized to login.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub DesignDts()
        Dim dtCollAccountNature As New DataTable
        dtCollAccountNature.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
        dtCollAccountNature.Columns.Add(New DataColumn("BranchCode", GetType(System.String)))
        dtCollAccountNature.Columns.Add(New DataColumn("Currency", GetType(System.String)))
        dtCollAccountNature.Columns.Add(New DataColumn("CollectionNature", GetType(System.String)))
        ViewState("CollAccountNature") = dtCollAccountNature
    End Sub
    Private Sub FillDesignedDtByAPNatureByAssignedRole()
        Dim dtAssignedCollANature As New DataTable
        dtAssignedCollANature.Rows.Clear()
        dtAssignedCollANature = ICUserRolesCollectionAccountAndCollectionNatureController.GetCollectionAccountCollectionNatureTaggedWithUser(Me.UserId.ToString, ArrICAssignUserRolsID)
        dtAssignedCollANature.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAssignedCollANature.Rows
            dr("DropDown") = "False"
        Next
        ViewState("CollAccountNature") = Nothing
        ViewState("CollAccountNature") = dtAssignedCollANature
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Invoice Data Upload")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Invoice Data Upload") = True Then
                    If Not ArrICAssignUserRolsID.Contains(RoleID.ToString) Then
                        ArrICAssignUserRolsID.Add(RoleID.ToString)
                    End If
                End If
            Next
        End If
    End Sub
    Private Sub LoadddlCollectionAccountCollectionNature()
        Try
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            'ddlCollectionAccountNature.Enabled = True
            ddlCollectionAccountNature.Items.Clear()
            ddlCollectionAccountNature.Items.Add(lit)
            dt = ICUserRolesCollectionAccountAndCollectionNatureController.GetCollectionNatureTaggedWithUserForDropDown(Me.UserInfo.UserID.ToString, ArrICAssignUserRolsID)
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    ddlCollectionAccountNature.AppendDataBoundItems = True
                    lit = New ListItem
                    lit.Value = dr("Value")
                    lit.Text = dr("CollAccountAndNature")
                    ddlCollectionAccountNature.Items.Add(lit)
                    'lit.Selected = True
                    'ddlCollectionAccountNature.Enabled = False
                Next
                'SetViewStateDtOnAPNatureIndexChnage()
            ElseIf dt.Rows.Count > 1 Then
                ddlCollectionAccountNature.AppendDataBoundItems = True
                ddlCollectionAccountNature.DataSource = dt
                ddlCollectionAccountNature.DataTextField = "CollAccountAndNature"
                ddlCollectionAccountNature.DataValueField = "Value"
                ddlCollectionAccountNature.DataBind()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlInvoiceDBTemplate()
        Try

            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlInvoiceDBTemplate.Items.Clear()
            ddlInvoiceDBTemplate.Items.Add(lit)
            ddlInvoiceDBTemplate.AppendDataBoundItems = True
            ddlInvoiceDBTemplate.Enabled = True
            If ddlCollectionAccountNature.SelectedValue.ToString <> "0" Then
                dt = ICCollectionInvoiceDataTemplateController.GetTemplatesByCollectionNatureAllowedFileUpload(ddlCollectionAccountNature.SelectedValue.ToString)

                If dt.Rows.Count = 1 Then
                    For Each dr As DataRow In dt.Rows
                        lit.Value = dr("CollectionInvoiceTemplateID")
                        lit.Text = dr("TemplateName")
                        ddlInvoiceDBTemplate.Items.Clear()
                        ddlInvoiceDBTemplate.Items.Add(lit)
                        lit.Selected = True
                        ddlInvoiceDBTemplate.Enabled = False
                    Next
                    LoadgvClientFiles(True)
                ElseIf dt.Rows.Count > 1 Then
                    ddlInvoiceDBTemplate.DataSource = dt
                    ddlInvoiceDBTemplate.DataTextField = "TemplateName"
                    ddlInvoiceDBTemplate.DataValueField = "CollectionInvoiceTemplateID"
                    ddlInvoiceDBTemplate.DataBind()
                    ddlInvoiceDBTemplate.Enabled = True
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    
    Private Sub SetViewStateDtOnAPNatureIndexChnage()
        Dim dtAccountNumber As New DataTable
        Dim PaymentNatureCode As String
        Dim drAccountNo As DataRow
        Dim collDR As DataRow()
        PaymentNatureCode = Nothing
        FillDesignedDtByAPNatureByAssignedRole()
        dtAccountNumber = ViewState("CollAccountNature")
        ViewState("CollAccountNature") = Nothing
        PaymentNatureCode = ddlCollectionAccountNature.SelectedValue.ToString
        collDR = dtAccountNumber.Select("CollectionNature='" & PaymentNatureCode & "'")
        dtAccountNumber.Rows.Clear()
     
            drAccountNo = dtAccountNumber.NewRow()

        drAccountNo("CollectionNature") = PaymentNatureCode

            dtAccountNumber.Rows.Add(drAccountNo)

        'dtAccountNumber.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAccountNumber.Rows
            dr("DropDown") = "True"
        Next
        ViewState("CollAccountNature") = dtAccountNumber

    End Sub

    
    Private Sub LoadgvClientFiles(ByVal IsBind As Boolean)
        Try

            Dim GroupCode, CompannyCode, TemplateID As String
            GroupCode = ddlCollectionAccountNature.SelectedValue.ToString()
            CompannyCode = ddlInvoiceDBTemplate.SelectedValue.ToString()
            Dim dtAccountNumber As New DataTable
            dtAccountNumber = ViewState("CollAccountNature")
            If dtAccountNumber.Rows.Count = 0 Then
                UIUtilities.ShowDialog(Me, "Error", "You do not have access to any transactional data. Please contact Soneri Trans@ct Administrator.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                Exit Sub
            End If
            TemplateID = ddlInvoiceDBTemplate.SelectedValue.ToString()

            'If ddlCollectionAccountNature.SelectedValue.ToString <> "0" Then
            GetCollectionClientFilesGV(dtAccountNumber, TemplateID.ToString(), Me.UserId.ToString, gvClientFiles.CurrentPageIndex + 1, gvClientFiles.PageSize, gvClientFiles, IsBind)
            If gvClientFiles.Items.Count > 0 Then
                gvClientFiles.Columns(11).Visible = CBool(htRights("Process"))
                gvClientFiles.Columns(12).Visible = CBool(htRights("Download"))
                gvClientFiles.Columns(13).Visible = CBool(htRights("Delete"))
                btnRefresh.Visible = True
                gvClientFiles.Visible = True
                lblRNF.Visible = False
            Else
                lblRNF.Visible = True
                gvClientFiles.DataSource = Nothing
                btnRefresh.Visible = False
                gvClientFiles.Visible = False

            End If
            'Else
            '    lblRNF.Visible = True
            '    gvClientFiles.DataSource = Nothing
            '    btnRefresh.Visible = False
            '    gvClientFiles.Visible = False
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Public Shared Sub GetCollectionClientFilesGV(ByVal AccountNumber As DataTable, ByVal TemplateID As String, ByVal UsersID As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
        Dim StrQuery As String = Nothing
        Dim WhereClause As String = Nothing
        Dim dt As New DataTable
        WhereClause = " Where "
        StrQuery = "select icf.FileID,icf.FileBatchNo,IC_Company.CompanyName,IC_CollectionNature.CollectionNatureName,icf.OriginalFileName,icf.CreatedDate,"
        StrQuery += "CreatedUser.UserName,icf.ProcessDate as CreatedBy,qryProcessUser.UserName as ProcessBy,icf.TransactionCount,icf.TotalAmount,icf.Status"
        'StrQuery += "convert(varchar,FileBatchNo) + '-' + OriginalFileName as FileBatchNo "
        StrQuery += " from IC_Files as icf "
        StrQuery += "inner join IC_Company on icf.CompanyCode=IC_Company.CompanyCode "
        StrQuery += "inner join IC_CollectionNature on icf.PaymentNatureCode=IC_CollectionNature.CollectionNatureCode "
        StrQuery += "inner join IC_User as CreatedUser on icf.CreatedBy=CreatedUser.UserID "
        StrQuery += "Left join IC_User as qryProcessUser on icf.ProcessBy=qryProcessUser.UserID "
        'StrQuery += "inner join IC_CollectionAccountsCollectionNature as apnft on icf.AccountNumber=apnft.AccountNumber and icf."
        'StrQuery += "BranchCode = apnft.BranchCode And icf.Currency = apnft.Currency And icf.PaymentNatureCode = apnft.CollectionNatureCode "


        WhereClause += " And icf.FileType='Invoice DB File' and icf.AcqMode='File Upload'"

        If TemplateID.ToString() <> "0" Then
            WhereClause += " And icf.FileUploadTemplateID=" & TemplateID
        End If
        WhereClause += "  And ( "
        If AccountNumber.Rows.Count > 0 Then
            WhereClause += " ( "
            WhereClause += " icf.PaymentNatureCode" & " IN ( "
            For Each dr As DataRow In AccountNumber.Rows
                WhereClause += "'" & dr("CollectionNature").ToString & "',"
            Next
            WhereClause = WhereClause.Remove(WhereClause.Length - 1, 1)
            WhereClause += ")"
            WhereClause += " )"
            If AccountNumber(0)(2) = False Then
                WhereClause += " OR (icf.CreatedBy=" & UsersID & " )"
            End If
        Else
            WhereClause += " OR (icf.CreatedBy=" & UsersID & " )"
        End If
        WhereClause += " ) "
        If WhereClause.Contains(" Where  And") = True Then
            WhereClause = WhereClause.Replace(" Where  And", " Where  ")
        End If
        StrQuery += WhereClause
        StrQuery += "order by icf.FileID desc"
        Dim util As New esUtility
        dt = util.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery)
        If Not pagenumber = 0 Then
            rg.DataSource = dt
            If DoDataBind Then
                rg.DataBind()
            End If
        Else

            rg.DataSource = dt
            If DoDataBind Then
                rg.DataBind()
            End If
        End If
    End Sub
    Protected Sub ddlInvoiceDBTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlInvoiceDBTemplate.SelectedIndexChanged
        Try
            SetArraListRoleID()
            LoadgvClientFiles(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCollectionAccountNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCollectionAccountNature.SelectedIndexChanged
        Try
            If ddlCollectionAccountNature.SelectedValue.ToString = "0" Then
                SetArraListRoleID()
                LoadddlInvoiceDBTemplate()
                FillDesignedDtByAPNatureByAssignedRole()
                LoadgvClientFiles(True)
                ' HideGvClientFiles()
            Else
                SetArraListRoleID()
                'FillDesignedDtByAPNatureByAssignedRole()
                LoadddlInvoiceDBTemplate()
                SetViewStateDtOnAPNatureIndexChnage()
                LoadgvClientFiles(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    
    Private Sub HideGvClientFiles()
        lblRNF.Visible = True
        gvClientFiles.DataSource = Nothing
        btnRefresh.Visible = False
        gvClientFiles.Visible = False
        ViewState("AccountNumber") = Nothing
    End Sub
    Protected Sub gvClientFiles_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvClientFiles.ItemCommand
        Try

            If e.CommandName = "ProcessFile" Then
                ' Check Invoice DB Available
                Dim objCollectionNaure As New ICCollectionNature
                Dim objFile As New ICFiles
                objFile.LoadByPrimaryKey(e.CommandArgument.ToString())
                If objCollectionNaure.LoadByPrimaryKey(objFile.PaymentNatureCode) Then
                    If objCollectionNaure.IsInvoiceDBAvailable <> True Then
                        UIUtilities.ShowDialog(Me, "Client Invoice DB File Upload", "Invoice DB not Available for Collection Nature " & objCollectionNaure.CollectionNatureName.ToString() & ".", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            
                hfClientFileID.Value = e.CommandArgument.ToString()
                ReadInstructionsInFile(hfClientFileID.Value.ToString())
                'Response.Clear()
                'Response.Flush()
                '  Response.ClearHeaders()

            ElseIf e.CommandName = "del" Then
                Dim objICFiles As New ICFiles
                Dim aFileInfo As IO.FileInfo
                If objICFiles.LoadByPrimaryKey(e.CommandArgument.ToString) Then
                    If objICFiles.Status = "Pending Read" Then
                        If ICFilesController.DeleteClientUserFilesByFileID(objICFiles.FileID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString) = True Then
                            UIUtilities.ShowDialog(Me, "Client Invoice DB File Upload", "File deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                            aFileInfo = New IO.FileInfo(objICFiles.FileLocation)
                            'Delete file from HDD.
                            If aFileInfo.Exists Then
                                aFileInfo.Delete()
                            End If
                            LoadgvClientFiles(True)
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Client Invoice DB File Upload", "Uploaded File can not be deleted  due to following reasons : <br />1.  File is already parsed.<br />2.  File is in " & "In Process" & " Status. <br />3. File is in " & "Rejected" & " Status.", ICBO.IC.Dialogmessagetype.Failure)
                        LoadgvClientFiles(True)
                    End If
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvClientFiles_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvClientFiles.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvClientFiles.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvClientFiles_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvClientFiles.NeedDataSource
        LoadgvClientFiles(False)
    End Sub

    Protected Sub gvClientFiles_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvClientFiles.PageIndexChanged
        gvClientFiles.CurrentPageIndex = e.NewPageIndex
    End Sub
    Private Sub ReadInstructionsInFile(ByVal FileID As String)
        Try

            Dim collInvoiceDBTemplateFields As New ICCollectionInvoiceDataTemplateFieldsCollection
            Dim HeadercollInvoiceDBTemplateFields As New ICCollectionInvoiceDataTemplateFieldsCollection
            Dim DetailcollInvoiceDBTemplateFields As New ICCollectionInvoiceDataTemplateFieldsCollection
            Dim dtVerifiedHeader As New DataTable
            Dim dtUnverified As New DataTable
            Dim dtDetail As New DataTable


            Dim aFileInfo As IO.FileInfo
            Dim objInvoiceDBTemplate As New ICCollectionInvoiceDataTemplate
            Dim objICFile As New ICFiles
            Dim LineNo As Integer = 0
            Dim LoopCount As Integer = 0
            Dim FilePath As String = ""

            If objICFile.LoadByPrimaryKey(FileID) Then
                objInvoiceDBTemplate.LoadByPrimaryKey(objICFile.FileUploadTemplateID)
                If objInvoiceDBTemplate.TemplateType = "Dual" Then
                    FilePath = objICFile.FileLocation.ToString()
                    HeadercollInvoiceDBTemplateFields = ICCollectionInvoiceDataTemplateFieldsListController.GetTemplateFieldsByInvoiceTemplateOfDualFile(objICFile.FileUploadTemplateID, "Header")
                    DetailcollInvoiceDBTemplateFields = ICCollectionInvoiceDataTemplateFieldsListController.GetTemplateFieldsByInvoiceTemplateOfDualFile(objICFile.FileUploadTemplateID, "Detail")
                    If objInvoiceDBTemplate.TemplateFormat.Trim().ToString() = ".txt" Then
                        aFileInfo = New IO.FileInfo(FilePath.ToString())

                        If aFileInfo.Exists Then
                            Dim mreader As New System.IO.StreamReader(aFileInfo.FullName)
                            Do While mreader.Peek <> -1
                                LineNo = LineNo + 1
                                ICFilesController.GetCollectionDataFromTXTDualFile(mreader.ReadLine(), HeadercollInvoiceDBTemplateFields, DetailcollInvoiceDBTemplateFields, LineNo, dtVerifiedHeader, dtDetail, dtUnverified)
                                LoopCount = LoopCount + 1
                            Loop
                            mreader.Close()
                            mreader.Dispose()
                        End If
                    End If

                    If dtVerifiedHeader.Rows.Count > 0 Then
                        gvVerified.DataSource = dtVerifiedHeader
                        gvVerified.DataBind()

                        'Set FileID, FileName, TnxCount and Total Amount
                        Dim dt As New DataTable
                        Dim dr As DataRow
                        Dim Amount As Double = 0
                        dt = dtVerifiedHeader

                        'For Each dr In dt.Rows
                        '    Amount = Amount + CDbl(dr("Amount-3").ToString())
                        'Next



                        txtFileID.Text = objICFile.FileID.ToString()
                        txtFileName.Text = objICFile.OriginalFileName.ToString()
                        txtTnxCount.Text = dt.Rows.Count.ToString()
                        'txtTotalAmount.Text = Amount.ToString("N2")







                        rimProcessFile.Enabled = True
                        tblProcess.Style.Remove("display")
                        tblView.Style.Add("display", "none")
                        pnlVerified.Visible = True
                        lblVerified.Visible = True
                        lblVerified.Text = "Verified Instruction Header List"
                        btnProceed.Visible = True
                        btnCancel.Visible = True
                    End If


                    If dtDetail.Rows.Count > 0 Then
                        gvVerifiedDetail.DataSource = dtDetail
                        gvVerifiedDetail.DataBind()
                        pnlVerifiedDetail.Visible = True
                        lblVerifiedDetail.Visible = True
                    End If


                    If dtUnverified.Rows.Count > 0 Then
                        'Dim DtForEmail As New DataTable
                        'Dim objICUser As New ICUser
                        'objICUser.LoadByPrimaryKey(Me.UserId.ToString)
                        'Dim APNLocation As String = objICFile.AccountNumber & "-" & objICFile.BranchCode & "-" & objICFile.Currency & "-" & objICFile.PaymentNatureCode & "-" & objICUser.OfficeCode.ToString
                        'Dim TempString As String = ""
                        'Dim ErrorMessageForEmail As String = Nothing
                        'DtForEmail = DirectCast(ViewState("dtUnVerified"), DataTable)
                        'For Each dr As DataRow In DtForEmail.Rows
                        '    If TempString = "" Then
                        '        TempString = dr("Message").ToString
                        '        If Not ErrorMessageForEmail Is Nothing Then
                        '            If ErrorMessageForEmail.Contains(TempString) = False Then
                        '                ErrorMessageForEmail += TempString & "<br/>"
                        '            End If
                        '        Else
                        '            ErrorMessageForEmail += TempString & "<br/>"
                        '        End If
                        '    ElseIf TempString <> dr("Message").ToString Then
                        '        TempString = ""
                        '        TempString = dr("Message").ToString
                        '        If ErrorMessageForEmail.Contains(TempString) = False Then
                        '            ErrorMessageForEmail += TempString & "<br/>"
                        '        End If
                        '    End If
                        'Next


                        gvUnverified.DataSource = dtUnverified
                        gvUnverified.DataBind()

                        rimProcessFile.Enabled = True
                        tblProcess.Style.Remove("display")
                        tblView.Style.Add("display", "none")
                        pnlUnVerified.Visible = True
                        lblUnverified.Visible = True
                        btnProceed.Visible = False
                        btnReject.Visible = True
                        btnCancel.Visible = True
                        'EmailUtilities.Fileprocessedbyuserandfailed(objICFile.OriginalFileName.ToString, ErrorMessageForEmail, Me.UserInfo.Username.ToString, APNLocation)
                        'SMSUtilities.Fileprocessedbyuserandfailed(Me.UserInfo.Username.ToString, APNLocation)
                    End If

                    Exit Sub
                Else
                    collInvoiceDBTemplateFields = ICCollectionInvoiceDataTemplateFieldsListController.GetTemplateFieldsByInvoiceTemplateOfDualFile(objICFile.FileUploadTemplateID, "All")
                   
                    FilePath = objICFile.FileLocation.ToString()

                    If objInvoiceDBTemplate.TemplateFormat.Trim().ToString() = ".txt" Then
                        aFileInfo = New IO.FileInfo(FilePath.ToString())
                        If aFileInfo.Exists Then
                            Dim mreader As New System.IO.StreamReader(aFileInfo.FullName)
                            Do While mreader.Peek <> -1
                                LineNo = LineNo + 1
                                ICFilesController.GetCollectionDataFromTXTFile(mreader.ReadLine(), collInvoiceDBTemplateFields, LineNo, dtVerifiedHeader, dtUnverified)
                                LoopCount = LoopCount + 1

                            Loop
                            mreader.Close()
                            mreader.Dispose()
                        End If
                    Else ' in case of xls or xlsx files

                        Dim ConnStr As String = ""
                        Dim cmdStr As String = ""
                        Dim dsExcel As New DataSet
                        Dim drExcel As DataRow

                        'xlsx
                        If objInvoiceDBTemplate.TemplateFormat.Trim().ToString() = ".xlsx" Then
                            FilePath = objICFile.FileLocation.ToString()
                            ConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FilePath & "; Extended Properties='Excel 12.0 Xml; HDR=Yes; IMEX=1'"

                        ElseIf objInvoiceDBTemplate.TemplateFormat.Trim().ToString() = ".xls" Then
                            FilePath = objICFile.FileLocation.ToString()
                            ConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FilePath & "; Extended Properties='Excel 12.0; HDR=Yes; IMEX=1'"
                        ElseIf objInvoiceDBTemplate.TemplateFormat.Trim().ToString() = ".csv" Then
                            FilePath = Request.PhysicalApplicationPath.ToString() & "UploadedFiles\"
                            ConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FilePath & "; Extended Properties='Text; HDR=Yes; FMT=Delimited'"
                        End If
                        Dim Conn As New OleDb.OleDbConnection(ConnStr)
                        If objInvoiceDBTemplate.TemplateFormat.Trim().ToString() = ".csv" Then
                            cmdStr = "select * from " & objICFile.FileName.ToString & ""
                        Else
                            cmdStr = "select * from [" & objInvoiceDBTemplate.SheetName.Trim.ToString() & "$]"
                        End If


                        Dim cmd As New OleDb.OleDbCommand(cmdStr, Conn)
                        Dim da As New OleDb.OleDbDataAdapter(cmd)

                        da.Fill(dsExcel, "ExcelFile")
                        Conn.Close()
                        Conn.Dispose()






                        If Not dsExcel.Tables(0).Rows.Count = 0 Then
                            For Each drExcel In dsExcel.Tables(0).Rows
                                LineNo = LineNo + 1
                                ICFilesController.GetCollectionDataFromEXCELFile(drExcel, collInvoiceDBTemplateFields, LineNo, dsExcel.Tables(0).Columns.Count, dtVerifiedHeader, dtUnverified)
                                LoopCount = LoopCount + 1
                            Next
                        End If
                    End If
                End If

                txtFileID.Text = objICFile.FileID.ToString()
                txtFileName.Text = objICFile.OriginalFileName.ToString()
                txtTnxCount.Text = Nothing
                'txtTotalAmount.Text = Nothing



                If dtVerifiedHeader.Rows.Count > 0 Then
                    gvVerified.DataSource = dtVerifiedHeader
                    gvVerified.DataBind()

                    'Set FileID, FileName, TnxCount and Total Amount
                    Dim dt As New DataTable
                    Dim dr As DataRow
                    Dim Amount As Double = 0
                    dt = dtVerifiedHeader

                    'For Each dr In dt.Rows
                    '    Amount = Amount + CDbl(dr("Amount-3").ToString())
                    'Next



                    txtFileID.Text = objICFile.FileID.ToString()
                    txtFileName.Text = objICFile.OriginalFileName.ToString()
                    txtTnxCount.Text = dt.Rows.Count.ToString()
                    'txtTotalAmount.Text = Amount.ToString("N2")







                    rimProcessFile.Enabled = True
                    tblProcess.Style.Remove("display")
                    tblView.Style.Add("display", "none")
                    pnlVerified.Visible = True
                    lblVerified.Visible = True
                    btnProceed.Visible = True
                    btnCancel.Visible = True
                End If


                If dtUnverified.Rows.Count > 0 Then
                    'Dim ErrorMessageForEmail As String = Nothing
                    'Dim DtForEmail As New DataTable
                    'DtForEmail = DirectCast(ViewState("dtUnVerified"), DataTable)

                    gvUnverified.DataSource = dtUnverified
                    gvUnverified.DataBind()
                    'Dim objICUser As New ICUser
                    'objICUser.LoadByPrimaryKey(Me.UserId.ToString)
                    'Dim APNLocation As String = objICFile.AccountNumber & "-" & objICFile.BranchCode & "-" & objICFile.Currency & "-" & objICFile.PaymentNatureCode & "-" & objICUser.OfficeCode.ToString
                    'Dim TempString As String = ""
                    'For Each dr As DataRow In DtForEmail.Rows
                    '    If TempString = "" Then
                    '        TempString = dr("Message").ToString
                    '        If Not ErrorMessageForEmail Is Nothing Then
                    '            If ErrorMessageForEmail.Contains(TempString) = False Then
                    '                ErrorMessageForEmail += TempString & "<br/>"
                    '            End If
                    '        Else
                    '            ErrorMessageForEmail += TempString & "<br/>"
                    '        End If
                    '    ElseIf TempString <> dr("Message").ToString Then
                    '        TempString = ""
                    '        TempString = dr("Message").ToString
                    '        If ErrorMessageForEmail.Contains(TempString) = False Then
                    '            ErrorMessageForEmail += TempString & "<br/>"
                    '        End If
                    '    End If
                    'Next




                    rimProcessFile.Enabled = True
                    tblProcess.Style.Remove("display")
                    tblView.Style.Add("display", "none")
                    pnlUnVerified.Visible = True
                    lblUnverified.Visible = True
                    btnProceed.Visible = False
                    btnReject.Visible = True
                    btnCancel.Visible = True
                    'EmailUtilities.Fileprocessedbyuserandfailed(objICFile.OriginalFileName.ToString, ErrorMessageForEmail, Me.UserInfo.Username.ToString, APNLocation)
                    'SMSUtilities.Fileprocessedbyuserandfailed(Me.UserInfo.Username.ToString, APNLocation)
                End If



            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetCollectionDataFromTXTFile(ByVal FileLine As String, ByVal collAccountPaymentNatureTemplateFields As ICCollectionInvoiceDataTemplateFieldsCollection, ByVal LineNumber As Integer, ByVal FileTemplate As ICCollectionInvoiceDataTemplate)
        Dim dtVarified, dtUnVerified As New DataTable
        Dim drVarified, drUnVerified As DataRow
        Dim objAccountPaymentNatureTemplateField As New ICCollectionInvoiceDataTemplateFields
        'Dim FileTemplate As New ICTemplate

        Dim StartIndex As Integer
        Dim ErrorMsg As String = ""
        Dim arrInsruction As String()
        Dim CountColumn As Integer = 0
        Try
            StartIndex = 0
            'FileTemplate.LoadByPrimaryKey(collAccountPaymentNatureTemplateFields(0).TemplateID.ToString())

            If ViewState("dtVarified") Is Nothing Then
                For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                    dtVarified.Columns.Add(New DataColumn(objAccountPaymentNatureTemplateField.FieldName.ToString() & "-" & objAccountPaymentNatureTemplateField.CollectionFieldID.ToString(), GetType(System.String)))
                Next
                dtUnVerified.Columns.Add(New DataColumn("Data", GetType(System.String)))
                dtUnVerified.Columns.Add(New DataColumn("Message", GetType(System.String)))
                dtUnVerified.Columns.Add(New DataColumn("LineNo", GetType(System.String)))
            Else
                dtVarified = DirectCast(ViewState("dtVarified"), DataTable)
                dtUnVerified = DirectCast(ViewState("dtUnVerified"), DataTable)
            End If
            If FileTemplate.IsFixLength = True Then
                ErrorMsg = CheckCollectionDataTXT(FileLine, collAccountPaymentNatureTemplateFields, FileTemplate)
                If ErrorMsg = "" Then
                    drVarified = dtVarified.NewRow()
                    For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                      
                            drVarified(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1) = FileLine.Substring(StartIndex, objAccountPaymentNatureTemplateField.FieldLength).ToString().Trim()

                        StartIndex = StartIndex + objAccountPaymentNatureTemplateField.FieldLength
                    Next
                    dtVarified.Rows.Add(drVarified)
                Else
                    drUnVerified = dtUnVerified.NewRow()
                    drUnVerified("Data") = FileLine.ToString()
                    drUnVerified("Message") = ErrorMsg.ToString()
                    drUnVerified("LineNo") = LineNumber
                    dtUnVerified.Rows.Add(drUnVerified)
                End If

                ViewState("dtVarified") = dtVarified
                ViewState("dtUnVerified") = dtUnVerified
            Else 'CSV File               
                ErrorMsg = CheckCollectionDataTXT(FileLine, collAccountPaymentNatureTemplateFields, FileTemplate)
                arrInsruction = FileLine.Split(FileTemplate.FieldDelimiter.ToString().Trim())
                If ErrorMsg = "" Then
                    drVarified = dtVarified.NewRow()
                    For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                        
                            drVarified(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1) = arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder - 1).ToString().Trim()

                    Next
                    dtVarified.Rows.Add(drVarified)
                Else
                    drUnVerified = dtUnVerified.NewRow()
                    drUnVerified("Data") = FileLine.ToString()
                    drUnVerified("Message") = ErrorMsg.ToString()
                    drUnVerified("LineNo") = LineNumber
                    dtUnVerified.Rows.Add(drUnVerified)
                End If
                ViewState("dtVarified") = dtVarified
                ViewState("dtUnVerified") = dtUnVerified
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetCollectionDataDataFromTXTDualFile(ByVal FileLine As String, ByVal HeadercollAccountPaymentNatureTemplateFields As ICCollectionInvoiceDataTemplateFieldsCollection, ByVal DetailcollAccountPaymentNatureTemplateFields As ICCollectionInvoiceDataTemplateFieldsCollection, ByVal LineNumber As Integer, ByVal FileTemplate As ICCollectionInvoiceDataTemplate)
        Dim dtVarifiedHeader, dtVarifiedDetail, dtUnVerified As New DataTable
        Dim drVarifiedHeader, drVarifiedDetail, drUnVerified As DataRow
        Dim objICInvoiceDBTemplateField As New ICCollectionInvoiceDataTemplateFields
        'Dim FileTemplate As New ICTemplate


        'FileTemplate.es.Connection.CommandTimeout = 3600


        Dim StartIndex As Integer
        Dim ErrorMsg As String = ""
        Dim IdentifierChar As String = ""
        Dim arrInsruction As String()
        Dim CountColumn As Integer = 0
        Dim HeaderDetailRelate As Integer = 0
        Try
            StartIndex = 0
            'FileTemplate.LoadByPrimaryKey(HeadercollAccountPaymentNatureTemplateFields(0).TemplateID.ToString())
            If ViewState("dtVarifiedHeader") Is Nothing Then
                ' Design Header Datatable
                dtVarifiedHeader.Columns.Add(New DataColumn("HeaderDetailRelationNo", GetType(System.String)))
                For Each objICInvoiceDBTemplateField In HeadercollAccountPaymentNatureTemplateFields
                    dtVarifiedHeader.Columns.Add(New DataColumn(objICInvoiceDBTemplateField.FieldName.ToString() & "-" & objICInvoiceDBTemplateField.CollectionFieldID.ToString(), GetType(System.String)))
                Next
                ' Design Detail Datatable
                dtVarifiedDetail.Columns.Add(New DataColumn("HeaderDetailRelationNo", GetType(System.String)))
                For Each objICInvoiceDBTemplateField In DetailcollAccountPaymentNatureTemplateFields
                    dtVarifiedDetail.Columns.Add(New DataColumn(objICInvoiceDBTemplateField.FieldName.ToString() & "-" & objICInvoiceDBTemplateField.CollectionFieldID.ToString(), GetType(System.String)))
                Next

                dtUnVerified.Columns.Add(New DataColumn("Data", GetType(System.String)))
                dtUnVerified.Columns.Add(New DataColumn("Message", GetType(System.String)))
                dtUnVerified.Columns.Add(New DataColumn("LineNo", GetType(System.String)))
            Else
                dtVarifiedHeader = DirectCast(ViewState("dtVarifiedHeader"), DataTable)
                dtVarifiedDetail = DirectCast(ViewState("dtVarifiedDetail"), DataTable)
                dtUnVerified = DirectCast(ViewState("dtUnVerified"), DataTable)
            End If
            If dtVarifiedHeader.Rows.Count > 0 Then
                HeaderDetailRelate = dtVarifiedHeader.Rows.Count
            End If

            arrInsruction = FileLine.Split(FileTemplate.FieldDelimiter.ToString().Trim())
            IdentifierChar = arrInsruction(0).ToString()
            If IdentifierChar.ToString() = FileTemplate.HeaderIdentifier.ToString() Then
                ErrorMsg = CheckCollectionDataTXTDualFile(arrInsruction, HeadercollAccountPaymentNatureTemplateFields, DetailcollAccountPaymentNatureTemplateFields, FileTemplate, "Header")
                If ErrorMsg = "" Then
                    drVarifiedHeader = dtVarifiedHeader.NewRow()
                    drVarifiedHeader(0) = (HeaderDetailRelate + 1).ToString()
                    For Each objICInvoiceDBTemplateField In HeadercollAccountPaymentNatureTemplateFields
                        If objICInvoiceDBTemplateField.FieldDBType.Trim.ToString() = "Decimal" Then
                            drVarifiedHeader(CInt(objICInvoiceDBTemplateField.FieldOrder)) = CDbl(arrInsruction(objICInvoiceDBTemplateField.FieldOrder).ToString().Trim())
                        Else
                            drVarifiedHeader(CInt(objICInvoiceDBTemplateField.FieldOrder)) = arrInsruction(objICInvoiceDBTemplateField.FieldOrder).ToString().Trim()
                        End If
                    Next
                    dtVarifiedHeader.Rows.Add(drVarifiedHeader)
                Else
                    drUnVerified = dtUnVerified.NewRow()
                    drUnVerified("Data") = FileLine.ToString()
                    drUnVerified("Message") = ErrorMsg.ToString()
                    drUnVerified("LineNo") = LineNumber
                    dtUnVerified.Rows.Add(drUnVerified)
                End If
            ElseIf IdentifierChar.ToString() = FileTemplate.DetailIdentifier.ToString() Then
                ErrorMsg = CheckCollectionDataTXTDualFile(arrInsruction, HeadercollAccountPaymentNatureTemplateFields, DetailcollAccountPaymentNatureTemplateFields, FileTemplate, "Detail")
                If ErrorMsg = "" Then
                    drVarifiedDetail = dtVarifiedDetail.NewRow()
                    drVarifiedDetail(0) = HeaderDetailRelate.ToString()
                    For Each objICInvoiceDBTemplateField In DetailcollAccountPaymentNatureTemplateFields
                    
                 
                            drVarifiedDetail(CInt(objICInvoiceDBTemplateField.FieldOrder)) = arrInsruction(objICInvoiceDBTemplateField.FieldOrder).ToString().Trim()

                    Next
                    dtVarifiedDetail.Rows.Add(drVarifiedDetail)
                Else
                    drUnVerified = dtUnVerified.NewRow()
                    drUnVerified("Data") = FileLine.ToString()
                    drUnVerified("Message") = ErrorMsg.ToString()
                    drUnVerified("LineNo") = LineNumber
                    dtUnVerified.Rows.Add(drUnVerified)
                End If
            End If
            ViewState("dtVarifiedHeader") = dtVarifiedHeader
            ViewState("dtVarifiedDetail") = dtVarifiedDetail
            ViewState("dtUnVerified") = dtUnVerified
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckCollectionDataTXT(ByVal FileLine As String, ByVal collAccountPaymentNatureTemplateFields As ICCollectionInvoiceDataTemplateFieldsCollection, ByVal FileTemplate As ICCollectionInvoiceDataTemplate) As String
        'Dim objICCollInvocieDBTemplateField As ICCollectionInvoiceDataTemplateFields

        'Dim Msg As String = ""
        'Dim PaymentMode As String = ""
        'Dim StartIndex As Integer = 0
        'Dim arrInsruction As String()
        'Dim cnt As Integer = 0
        'Dim ProductTypeCode As String = Nothing
        'Dim dtProductType As New DataTable
        'Dim BeneIDType As New ArrayList
        'Try

        '    objICCollInvocieDBTemplateField = New ICCollectionInvoiceDataTemplateFields
        '    If FileTemplate.IsFixLength = True Then
        '        For Each objICCollInvocieDBTemplateField In collAccountPaymentNatureTemplateFields
        '            Dim FieldTypeMessage As String = Nothing
        '            FieldTypeMessage = ICCollectionInvoiceDataTemplateFieldsListController.CheckCollectionFieldDBLength(objICCollInvocieDBTemplateField.CollectionFieldID, FileLine.Substring(StartIndex, objICCollInvocieDBTemplateField.FieldLength).ToString().Trim(), objICCollInvocieDBTemplateField.FieldName, objICCollInvocieDBTemplateField.FieldType)
        '            If Not FieldTypeMessage = "OK" Then
        '                Msg = Msg & " " & objICCollInvocieDBTemplateField.FieldName.ToString() & " " & FieldTypeMessage
        '                Exit For
        '            End If
        '            If objICCollInvocieDBTemplateField.IsReference = True And FileLine.Substring(StartIndex, objICCollInvocieDBTemplateField.FieldLength).ToString().Trim() = "" Then
        '                Msg = Msg & " " & objICCollInvocieDBTemplateField.FieldName.ToString() & " is reference and can not be empty."
        '                Exit For
        '            End If
        '            StartIndex = StartIndex + objICCollInvocieDBTemplateField.FieldLength
        '        Next
        '    Else  'CSV File                
        '        arrInsruction = FileLine.Split(FileTemplate.FieldDelimiter.ToString().Trim())
        '        For Each objICCollInvocieDBTemplateField In collAccountPaymentNatureTemplateFields

        '            Dim FieldTypeMessage As String = Nothing
        '            FieldTypeMessage = ICCollectionInvoiceDataTemplateFieldsListController.CheckCollectionFieldDBType(objICCollInvocieDBTemplateField.CollectionFieldID, arrInsruction(objICCollInvocieDBTemplateField.FieldOrder - 1).ToString().Trim(), objICCollInvocieDBTemplateField.FieldName, objICCollInvocieDBTemplateField.FieldType)
        '            If Not FieldTypeMessage = "OK" Then
        '                Msg = Msg & " " & objICCollInvocieDBTemplateField.FieldName.ToString() & " " & FieldTypeMessage
        '                Exit For
        '            End If
        '            ' Check DB Maximum Length
        '            If ICCollectionInvoiceDataTemplateFieldsListController.CheckCollectionFieldDBLength(objICCollInvocieDBTemplateField.CollectionFieldID, arrInsruction(objICCollInvocieDBTemplateField.FieldOrder - 1).ToString().Trim()) = False Then
        '                Msg = Msg & " Database field length for " & objICCollInvocieDBTemplateField.FieldName.ToString() & " exceeded."
        '                Exit For
        '            End If
        '            If objICCollInvocieDBTemplateField.IsReference = True And arrInsruction(objICCollInvocieDBTemplateField.FieldOrder - 1).ToString().Trim() = "" Then
        '                Msg = Msg & " " & objICCollInvocieDBTemplateField.FieldName.ToString() & " is reference and can not be empty"
        '                Exit For
        '            End If
        '        Next
        '    End If
        '    Return Msg
        'Catch ex As Exception
        '    ProcessModuleLoadException(Me, ex, False)
        '    UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        'End Try
    End Function
    Private Function CheckCollectionDataTXTDualFile(ByVal arrInsruction As String(), ByVal HeadercollAccountPaymentNatureTemplateFields As ICCollectionInvoiceDataTemplateFieldsCollection, ByVal DetailcollAccountPaymentNatureTemplateFields As ICCollectionInvoiceDataTemplateFieldsCollection, ByVal FileTemplate As ICCollectionInvoiceDataTemplate, ByVal LineType As String) As String
        Dim objAccountPaymentNatureTemplateField As ICCollectionInvoiceDataTemplateFields
        Dim Msg As String = ""
        Dim PaymentMode As String = ""
        Dim StartIndex As Integer = 0
        Dim cnt As Integer = 0
        Try
            If LineType.ToString() = "Header" Then
                objAccountPaymentNatureTemplateField = New ICCollectionInvoiceDataTemplateFields
                For Each objAccountPaymentNatureTemplateField In HeadercollAccountPaymentNatureTemplateFields
                    ' Check DB Maximum Length
                    If ICCollectionInvoiceDataTemplateFieldsListController.CheckCollectionFieldDBLength(objAccountPaymentNatureTemplateField.CollectionFieldID, arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) = False Then
                        Msg = Msg & " Database field length for " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " exceeded."
                        Exit For
                    End If
                    Dim FieldTypeMessage As String = Nothing
                    FieldTypeMessage = ICCollectionInvoiceDataTemplateFieldsListController.CheckCollectionFieldDBType(objAccountPaymentNatureTemplateField.CollectionFieldID, arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName.ToString, objAccountPaymentNatureTemplateField.FieldType.ToString)
                    If Not FieldTypeMessage = "OK" Then
                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                        Exit For
                    End If
                    If objAccountPaymentNatureTemplateField.IsReference = True And arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is reference and can not be empty."
                        Exit For
                    End If
                Next
            ElseIf LineType.ToString() = "Detail" Then
               
                objAccountPaymentNatureTemplateField = New ICCollectionInvoiceDataTemplateFields
                For Each objAccountPaymentNatureTemplateField In HeadercollAccountPaymentNatureTemplateFields
                    ' Check DB Maximum Length
                    If ICCollectionInvoiceDataTemplateFieldsListController.CheckCollectionFieldDBLength(objAccountPaymentNatureTemplateField.CollectionFieldID, arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim()) = False Then
                        Msg = Msg & " Database field length for " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " exceeded."
                        Exit For
                    End If
                    Dim FieldTypeMessage As String = Nothing
                    FieldTypeMessage = ICCollectionInvoiceDataTemplateFieldsListController.CheckCollectionFieldDBType(objAccountPaymentNatureTemplateField.CollectionFieldID, arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName.ToString, objAccountPaymentNatureTemplateField.FieldType.ToString)
                    If Not FieldTypeMessage = "OK" Then
                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                        Exit For
                    End If
                    If objAccountPaymentNatureTemplateField.IsReference = True And arrInsruction(objAccountPaymentNatureTemplateField.FieldOrder).ToString().Trim() = "" Then
                        Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is reference and can not be empty."
                        Exit For
                    End If
                Next
            End If
            Return Msg
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Sub GetCollectionDataDataFromEXCELFile(ByVal drExcel As DataRow, ByVal collAccountPaymentNatureTemplateFields As ICCollectionInvoiceDataTemplateFieldsCollection, ByVal LineNumber As Integer, ByVal ColomnCount As Integer, ByVal FileTemplate As ICCollectionInvoiceDataTemplate)
        Dim dtVarified, dtUnVerified As New DataTable
        Dim drVarified, drUnVerified As DataRow
        Dim objAccountPaymentNatureTemplateField As New ICCollectionInvoiceDataTemplateFields
        

        Dim ErrorMsg As String = ""
        Dim CountColumn As Integer = 0
        Dim LineData As String = ""
        Dim i As Integer = 0
        Try
            'FileTemplate.LoadByPrimaryKey(collAccountPaymentNatureTemplateFields(0).TemplateID.ToString())
            If ViewState("dtVarified") Is Nothing Then
                For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                    dtVarified.Columns.Add(New DataColumn(objAccountPaymentNatureTemplateField.FieldName.ToString() & "-" & objAccountPaymentNatureTemplateField.CollectionFieldID.ToString(), GetType(System.String)))
                Next
                dtUnVerified.Columns.Add(New DataColumn("Data", GetType(System.String)))
                dtUnVerified.Columns.Add(New DataColumn("Message", GetType(System.String)))
                dtUnVerified.Columns.Add(New DataColumn("LineNo", GetType(System.String)))
            Else
                dtVarified = DirectCast(ViewState("dtVarified"), DataTable)
                dtUnVerified = DirectCast(ViewState("dtUnVerified"), DataTable)
            End If

            ErrorMsg = CheckCollectionDataEXCEL(drExcel, collAccountPaymentNatureTemplateFields, FileTemplate)
            If ErrorMsg = "" Then
                drVarified = dtVarified.NewRow()
                For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                   
                        drVarified(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1) = drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()

                Next
                dtVarified.Rows.Add(drVarified)
            Else

                For i = 0 To ColomnCount - 1
                    If i = 0 Then
                        LineData = drExcel(i).ToString()
                    Else
                        LineData = LineData & " , " & drExcel(i).ToString()
                    End If

                Next

                drUnVerified = dtUnVerified.NewRow()
                drUnVerified("Data") = LineData.ToString()
                drUnVerified("Message") = ErrorMsg.ToString()
                drUnVerified("LineNo") = LineNumber
                dtUnVerified.Rows.Add(drUnVerified)
            End If

            ViewState("dtVarified") = dtVarified
            ViewState("dtUnVerified") = dtUnVerified
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckCollectionDataEXCEL(ByVal drExcel As DataRow, ByVal collAccountPaymentNatureTemplateFields As ICCollectionInvoiceDataTemplateFieldsCollection, ByVal FileTemplate As ICCollectionInvoiceDataTemplate) As String
        Dim objAccountPaymentNatureTemplateField As ICCollectionInvoiceDataTemplateFields
        Dim Msg As String = ""
        Dim PaymentMode As String = ""
        Dim StartIndex As Integer = 0
        Dim cnt As Integer = 0
        Dim ProductTypeCode As String = Nothing
        Try
            
            objAccountPaymentNatureTemplateField = New ICCollectionInvoiceDataTemplateFields
            For Each objAccountPaymentNatureTemplateField In collAccountPaymentNatureTemplateFields
                ' Check DB Maximum Length
                Dim FieldTypeMessage As String = Nothing
                FieldTypeMessage = ICCollectionInvoiceDataTemplateFieldsListController.CheckCollectionFieldDBType(objAccountPaymentNatureTemplateField.CollectionFieldID, drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim(), objAccountPaymentNatureTemplateField.FieldName, objAccountPaymentNatureTemplateField.FieldType)
                If Not FieldTypeMessage = "OK" Then
                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " " & FieldTypeMessage
                    Exit For
                End If
                If ICCollectionInvoiceDataTemplateFieldsListController.CheckCollectionFieldDBLength(objAccountPaymentNatureTemplateField.CollectionFieldID, drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim()) = False Then
                    Msg = Msg & " Database field length for " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " exceeded."
                    Exit For
                End If
                If objAccountPaymentNatureTemplateField.IsReference And drExcel(CInt(objAccountPaymentNatureTemplateField.FieldOrder) - 1).ToString().Trim() = "" Then
                    Msg = Msg & " " & objAccountPaymentNatureTemplateField.FieldName.ToString() & " is reference and can not be empty."
                    Exit For
                End If
            Next
            Return Msg
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try
    End Function


    Private Function SaveVerifiedInstructions() As Boolean
        Dim dtVerified, dtVerifiedDetail As New DataTable
        Dim drVer As DataRow
        Dim dcVer As DataColumn
        Dim drVerDetail As DataRow
        Dim dcVerDetail As DataColumn
        Dim objInstruction As ICInstruction
        Dim objInstructionDetail As ICInstructionDetail
        Dim objBene As ICBeneficiary
        Dim objUser As New ICUser
        Dim objFile As New ICFiles
        Dim objCompany As New ICCompany
        Dim ActionStr As String = ""
        Dim HeaderDetailRelationNo As Integer = 0
        Dim IsDualFile As Boolean = False
        Dim objICProductType As ICProductType
        Dim StrAccountDetails As String()
        objUser.es.Connection.CommandTimeout = 3600
        objFile.es.Connection.CommandTimeout = 3600
        objCompany.es.Connection.CommandTimeout = 3600

        'ViewState("dtVarifiedHeader") = Nothing
        'ViewState("dtVarifiedDetail") = Nothing


        Dim FieldName, FieldType, FieldID As String
        Dim colName As String()
        FieldName = ""
        FieldType = ""
        FieldID = ""
        Try
            objFile.LoadByPrimaryKey(hfClientFileID.Value.ToString())
            objCompany.LoadByPrimaryKey(objFile.CompanyCode.ToString())
            If Not ViewState("dtVarifiedHeader") Is Nothing Then
                dtVerified = DirectCast(ViewState("dtVarifiedHeader"), DataTable)
                dtVerifiedDetail = DirectCast(ViewState("dtVarifiedDetail"), DataTable)
                IsDualFile = True
            Else
                dtVerified = DirectCast(ViewState("dtVarified"), DataTable)
            End If
            If dtVerified.Rows.Count > 0 Then
                For Each drVer In dtVerified.Rows
                    objUser = New ICUser
                    objUser.es.Connection.CommandTimeout = 3600
                    objUser.LoadByPrimaryKey(Me.UserId)

                    objInstruction = New ICInstruction
                    objBene = New ICBeneficiary
                    objInstructionDetail = New ICInstructionDetail
                    objInstruction.es.Connection.CommandTimeout = 3600
                    objBene.es.Connection.CommandTimeout = 3600
                    objInstructionDetail.es.Connection.CommandTimeout = 3600
                    objICProductType = New ICProductType
                    objICProductType.LoadByPrimaryKey(drVer("ProductTypeCode-65").ToString)
                    Dim IsInstructonDetailForSingleInstruction As Boolean = False
                    Dim CheckDetailCount As Integer = 0
                    Dim EmptyAccountDetails As String = Nothing
                    If objICProductType.DisbursementMode = "Direct Credit" Then
                        Try
                            StrAccountDetails = Nothing
                            StrAccountDetails = (CBUtilities.TitleFetch(drVer("BeneficiaryAccountNo").ToString, ICBO.CBUtilities.AccountType.RB, "IC Client Type", drVer("BeneficiaryAccountNo").ToString).ToString.Split(";"))
                        Catch ex As Exception
                            StrAccountDetails = Nothing
                            EmptyAccountDetails = "-;-;-"
                            StrAccountDetails = EmptyAccountDetails.Split(";")
                        End Try
                    End If

                    For Each dcVer In dtVerified.Columns
                        ActionStr = ""

                        If dcVer.ColumnName.ToString().Contains("-") Then
                            colName = dcVer.ColumnName.ToString().Split("-")
                            FieldName = colName(0).ToString()
                            FieldID = colName(1).ToString()
                            Dim objICField As New ICFieldsList
                            objICField.es.Connection.CommandTimeout = 3600
                            objICField.LoadByPrimaryKey(FieldID)
                            FieldType = ""
                            FieldType = objICField.FieldType.ToString

                            If FieldType.ToString() = "NonFlexi" Then
                                Select Case FieldName.ToString()
                                    Case "Description"
                                        objInstruction.Description = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Remarks"
                                        objInstruction.Remarks = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Amount"
                                        objInstruction.Amount = drVer(dcVer.ColumnName.ToString()).ToString()
                                        objInstruction.AmountInWords = NumToWord.SpellNumber(CDbl(drVer(dcVer.ColumnName.ToString()).ToString())).TrimStart().TrimEnd().Trim().ToString()
                                    Case "AmountInWords"
                                        objInstruction.AmountInWords = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryAccountNo"


                                        objInstruction.BeneficiaryAccountNo = drVer(dcVer.ColumnName.ToString()).ToString()
                                        objBene.BeneAccountNumber = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneAccountBranchCode"
                                        If objICProductType.DisbursementMode = "Direct Credit" Then
                                            If drVer(dcVer.ColumnName.ToString()).ToString() <> "" Then
                                                objInstruction.BeneAccountBranchCode = drVer(dcVer.ColumnName.ToString()).ToString()
                                            Else
                                                objInstruction.BeneAccountBranchCode = StrAccountDetails(1).ToString
                                            End If
                                        Else
                                            objInstruction.BeneAccountBranchCode = drVer(dcVer.ColumnName.ToString()).ToString()

                                        End If
                                    Case "BeneAccountCurrency"
                                        If objICProductType.DisbursementMode = "Direct Credit" Then
                                            If drVer(dcVer.ColumnName.ToString()).ToString() <> "" Then
                                                objInstruction.BeneAccountCurrency = drVer(dcVer.ColumnName.ToString()).ToString()
                                            Else
                                                objInstruction.BeneAccountCurrency = StrAccountDetails(2).ToString
                                            End If
                                        Else
                                            objInstruction.BeneAccountCurrency = drVer(dcVer.ColumnName.ToString()).ToString()

                                        End If

                                    Case "BeneficiaryAccountTitle"
                                        If objICProductType.DisbursementMode = "Direct Credit" Then
                                            If drVer(dcVer.ColumnName.ToString()).ToString() <> "" Then
                                                objInstruction.BeneficiaryAccountTitle = drVer(dcVer.ColumnName.ToString()).ToString()
                                            Else
                                                objInstruction.BeneficiaryAccountTitle = StrAccountDetails(0).ToString
                                            End If
                                        Else
                                            objInstruction.BeneficiaryAccountTitle = drVer(dcVer.ColumnName.ToString()).ToString()

                                        End If

                                    Case "BeneficiaryAddress"
                                        objInstruction.BeneficiaryAddress = drVer(dcVer.ColumnName.ToString()).ToString()
                                        objBene.BeneAddress1 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryBankCode"
                                        objInstruction.BeneficiaryBankCode = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryBankName"
                                        objInstruction.BeneficiaryBankName = drVer(dcVer.ColumnName.ToString()).ToString()
                                        objBene.BankName = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryBankAddress"
                                        objInstruction.BeneficiaryBankAddress = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryBranchCode"
                                        objInstruction.BeneficiaryBranchCode = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryBranchName"
                                        objInstruction.BeneficiaryBranchName = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryCity"
                                        Dim objCity As New ICCity
                                        objCity = ICCityController.GetCityByCityNameOrCode(drVer(dcVer.ColumnName.ToString()).ToString())
                                        If Not objCity Is Nothing Then
                                            objInstruction.BeneficiaryCity = objCity.CityCode
                                            objBene.BeneCity = objCity.CityCode
                                        End If
                                    Case "BeneficiaryCNIC"
                                        objInstruction.BeneficiaryCNIC = drVer(dcVer.ColumnName.ToString()).ToString()
                                        objBene.BeneIDNO = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryCountry"
                                        Dim objCountry As New ICCountry
                                        objCountry = ICCountryController.GetCountryByCountryNameOrCode(drVer(dcVer.ColumnName.ToString()).ToString())
                                        If Not objCountry Is Nothing Then
                                            objInstruction.BeneficiaryCountry = objCountry.CountryCode
                                        End If
                                    Case "BeneficiaryEmail"
                                        objInstruction.BeneficiaryEmail = drVer(dcVer.ColumnName.ToString()).ToString()
                                        objBene.BeneEmail = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryMobile"
                                        objInstruction.BeneficiaryMobile = drVer(dcVer.ColumnName.ToString()).ToString()
                                        objBene.BeneCellNumber = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryName"
                                        objInstruction.BeneficiaryName = drVer(dcVer.ColumnName.ToString()).ToString()
                                        objBene.BeneName = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryPhone"
                                        objInstruction.BeneficiaryPhone = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "BeneficiaryProvince"
                                        Dim objProvince As New ICProvince
                                        objProvince = ICProvinceController.GetProvinceByCountryNameOrCode(drVer(dcVer.ColumnName.ToString()).ToString())
                                        If Not objProvince Is Nothing Then
                                            objInstruction.BeneficiaryProvince = objProvince.ProvinceCode
                                            objBene.BeneProvince = objProvince.ProvinceCode
                                        End If
                                    Case "ClientBankCode"
                                        objInstruction.ClientBankCode = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "ClientBankName"
                                        objInstruction.ClientBankName = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "ClientBranchAddress"
                                        objInstruction.ClientBranchAddress = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "ClientBranchCode"
                                        objInstruction.ClientBranchCode = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "ClientBranchName"
                                        objInstruction.ClientBranchName = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "DetailAmount"
                                        objInstruction.DetailAmount = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "DDPayableLocation"
                                        Dim objCity As New ICCity
                                        objCity = ICCityController.GetCityByCityNameOrCode(drVer(dcVer.ColumnName.ToString()).ToString())
                                        If Not objCity Is Nothing Then
                                            objInstruction.DDDrawnCityCode = objCity.CityCode
                                            objInstruction.DDPayableLocation = objCity.CityName
                                        End If
                                    Case "InstrumentNo"
                                        objInstruction.InstrumentNo = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "PrintLocation"
                                        Dim objOffice As New ICOffice
                                        objOffice = ICOfficeController.GetPrintLocationByPrintLocationNameOrCode(drVer(dcVer.ColumnName.ToString()).ToString())
                                        If Not objOffice Is Nothing Then
                                            objInstruction.PrintLocationCode = objOffice.OfficeID
                                            objInstruction.PrintLocationName = objOffice.OfficeName
                                        End If
                                    Case "ProductTypeCode"
                                        Dim objProductType As New ICProductType
                                        If objProductType.LoadByPrimaryKey(drVer(dcVer.ColumnName.ToString()).Trim.ToString()) Then
                                            objInstruction.ProductTypeCode = objProductType.ProductTypeCode
                                            objInstruction.PaymentMode = objProductType.DisbursementMode
                                        End If
                                    Case "TXN_Code"
                                        objInstruction.TXNCode = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "ReferenceField1"
                                        objInstruction.ReferenceField1 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "ReferenceField2"
                                        objInstruction.ReferenceField2 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "ReferenceField3"
                                        objInstruction.ReferenceField3 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "ValueDate"
                                        If Not drVer(dcVer.ColumnName.ToString()).ToString() = "" Then

                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim ValueDate As Date = Date.ParseExact(drVer(dcVer.ColumnName.ToString()).ToString(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)
                                            objInstruction.ValueDate = ValueDate
                                        End If
                                    Case "PrintDate"
                                        If Not drVer(dcVer.ColumnName.ToString()).ToString() = "" Then
                                            Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy"}
                                            Dim PrintDate As Date = Date.ParseExact(drVer(dcVer.ColumnName.ToString()).ToString(), format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)
                                            objInstruction.PrintDate = PrintDate
                                        End If
                                End Select
                            ElseIf FieldType.ToString() = "DetailField" Then
                                If CheckDetailCount = 0 Then
                                    IsInstructonDetailForSingleInstruction = True
                                    CheckDetailCount = CheckDetailCount + 1
                                End If
                                Select Case FieldName.ToString
                                    Case "Detail_Amount"
                                        objInstructionDetail.DetailAmount = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneAccountNo"
                                        objInstructionDetail.DetailBeneAccountNo = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneAddress"
                                        objInstructionDetail.DetailBeneAddress = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneBankName"
                                        objInstructionDetail.DetailBeneBankName = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneBranchAddress"
                                        objInstructionDetail.DetailBeneBranchAddress = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneBranchCode"
                                        objInstructionDetail.DetailBeneBranchCode = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneBranchName"
                                        objInstructionDetail.DetailBeneBranchName = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneCNIC"
                                        objInstructionDetail.DetailBeneCNIC = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneCountry"
                                        objInstructionDetail.DetailBeneCountry = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneProvince"
                                        objInstructionDetail.DetailBeneProvince = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneCity"
                                        objInstructionDetail.DetailBeneCity = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneEmail"
                                        objInstructionDetail.DetailBeneEmail = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BeneMobile"
                                        objInstructionDetail.DetailBeneMobile = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_BenePhone"
                                        objInstructionDetail.DetailBenePhone = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_InvoiceNo"
                                        objInstructionDetail.DetailInvoiceNo = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_IS_NO"
                                        objInstructionDetail.DetailISNO = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_TXN_CODE"
                                        objInstructionDetail.DetailTXNCODE = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_DetailAmt"
                                        objInstructionDetail.DetailDetailAmt = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_1"
                                        objInstructionDetail.DetailFFText1 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_2"
                                        objInstructionDetail.DetailFFText2 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_3"
                                        objInstructionDetail.DetailFFText3 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_4"
                                        objInstructionDetail.DetailFFText4 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_5"
                                        objInstructionDetail.DetailFFText5 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_6"
                                        objInstructionDetail.DetailFFText6 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_7"
                                        objInstructionDetail.DetailFFText7 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_8"
                                        objInstructionDetail.DetailFFText8 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_9"
                                        objInstructionDetail.DetailFFText9 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_10"
                                        objInstructionDetail.DetailFFText10 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_11"
                                        objInstructionDetail.DetailFFText11 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_12"
                                        objInstructionDetail.DetailFFText12 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_13"
                                        objInstructionDetail.DetailFFText13 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_14"
                                        objInstructionDetail.DetailFFText14 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_15"
                                        objInstructionDetail.DetailFFText15 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_16"
                                        objInstructionDetail.DetailFFText16 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_17"
                                        objInstructionDetail.DetailFFText17 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_18"
                                        objInstructionDetail.DetailFFText18 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_19"
                                        objInstructionDetail.DetailFFText19 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Text_20"
                                        objInstructionDetail.DetailFFText20 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Amt_1"
                                        objInstructionDetail.DetailFFAmt1 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Amt_2"
                                        objInstructionDetail.DetailFFAmt2 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Amt_3"
                                        objInstructionDetail.DetailFFAmt3 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Amt_4"
                                        objInstructionDetail.DetailFFAmt4 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Amt_5"
                                        objInstructionDetail.DetailFFAmt5 = drVer(dcVer.ColumnName.ToString()).ToString()
                                    Case "Detail_FF_Amt_6"
                                        objInstructionDetail.DetailFFAmt6 = drVer(dcVer.ColumnName.ToString()).ToString()
                                End Select
                            End If
                        Else
                            If IsDualFile = True Then
                                HeaderDetailRelationNo = CInt(drVer(0).ToString())
                            End If
                        End If
                    Next


                    objInstruction.ClientName = objCompany.CompanyName
                    objInstruction.ClientEmail = objCompany.EmailAddress
                    objInstruction.ClientMobile = objCompany.PhoneNumber2
                    objInstruction.ClientAddress = objCompany.Address
                    objInstruction.CreatedOfficeCode = objUser.UpToICOfficeByOfficeCode.OfficeID

                    objInstruction.ClientCity = objUser.UpToICOfficeByOfficeCode.UpToICCityByCityID.CityCode.ToString
                    objInstruction.ClientProvince = objUser.UpToICOfficeByOfficeCode.UpToICCityByCityID.UpToICProvinceByProvinceCode.ProvinceCode.ToString
                    objInstruction.ClientCountry = objUser.UpToICOfficeByOfficeCode.UpToICCityByCityID.UpToICProvinceByProvinceCode.UpToICCountryByCountryCode.CountryCode.ToString
                    objInstruction.ClientAccountNo = objFile.AccountNumber.ToString()
                    objInstruction.ClientAccountBranchCode = objFile.BranchCode.ToString()
                    objInstruction.ClientAccountCurrency = objFile.Currency.ToString()
                    objInstruction.PaymentNatureCode = objFile.PaymentNatureCode.ToString()
                    objInstruction.AcquisitionMode = "File Upload"
                    objInstruction.CompanyCode = objCompany.CompanyCode
                    objInstruction.GroupCode = objCompany.GroupCode
                    objInstruction.FileBatchNo = objFile.FileBatchNo
                    objInstruction.FileID = objFile.FileID
                    objInstruction.Status = 1
                    objInstruction.LastStatus = 1
                    objInstruction.CreateBy = Me.UserId
                    objInstruction.CreateDate = Date.Now
                    ActionStr += "Instruction via File Upload for Beneficiary [ " & objInstruction.BeneficiaryName.ToString & " ] of Client [ " & objCompany.CompanyName & " ], "
                    ActionStr += "of Amount [ " & objInstruction.Amount.ToString & "] [ " & objInstruction.AmountInWords.ToString & " ] added. Client Account Number [ " & objInstruction.ClientAccountNo & " ],"
                    ActionStr += " Branch Code [" & objInstruction.ClientAccountBranchCode & " ], Currency [" & objInstruction.ClientAccountCurrency & " ]."
                    ActionStr += " Product Type [ " & objInstruction.ProductTypeCode & " ], Disbursement Mode [ " & objInstruction.PaymentMode & " ], Payment Nature [ " & objInstruction.PaymentNatureCode & " ]."


                    objInstruction.InstructionID = ICInstructionController.AddInstruction(objInstruction, objBene, Me.UserId, Me.UserInfo.Username, ActionStr.ToString())
                    If IsInstructonDetailForSingleInstruction = True Then
                        ' Add Instruction Detail for Single Instruction
                        ICInstructionController.AddInstructionDetail(objInstructionDetail, Me.UserId, Me.UserInfo.Username)
                    End If
                    If objInstruction.InstructionID IsNot Nothing Then
                        If IsDualFile = True Then
                            For Each drVerDetail In dtVerifiedDetail.Rows
                                If CInt(drVerDetail(0).ToString()) = HeaderDetailRelationNo Then
                                    objInstructionDetail = New ICInstructionDetail
                                    objInstructionDetail.InstructionID = objInstruction.InstructionID
                                    For Each dcVerDetail In dtVerifiedDetail.Columns
                                        If dcVerDetail.ColumnName.ToString().Contains("-") Then
                                            colName = dcVerDetail.ColumnName.ToString().Split("-")
                                            Dim objICField As New ICFieldsList
                                            objICField.es.Connection.CommandTimeout = 3600


                                            FieldName = colName(0).ToString()
                                            FieldID = colName(1).ToString()
                                            objICField.LoadByPrimaryKey(FieldID)
                                            FieldType = ""

                                            FieldType = objICField.FieldType.ToString()
                                            If FieldType.ToString() = "FixFormat" Then
                                                Select Case FieldName.ToString()
                                                    Case "FF_Amt_1"
                                                        objInstructionDetail.FFAmt1 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_Amt_2"
                                                        objInstructionDetail.FFAmt2 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_Amt_3"
                                                        objInstructionDetail.FFAmt3 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_Amt_4"
                                                        objInstructionDetail.FFAmt4 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_Amt_5"
                                                        objInstructionDetail.FFAmt5 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_Amt_6"
                                                        objInstructionDetail.FFAmt6 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_1"
                                                        objInstructionDetail.FFTxt1 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_2"
                                                        objInstructionDetail.FFTxt2 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_3"
                                                        objInstructionDetail.FFTxt3 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_4"
                                                        objInstructionDetail.FFTxt4 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_5"
                                                        objInstructionDetail.FFTxt5 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_6"
                                                        objInstructionDetail.FFTxt6 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_7"
                                                        objInstructionDetail.FFTxt7 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_8"
                                                        objInstructionDetail.FFTxt8 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_9"
                                                        objInstructionDetail.FFTxt9 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_10"
                                                        objInstructionDetail.FFTxt10 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_11"
                                                        objInstructionDetail.FFTxt11 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_12"
                                                        objInstructionDetail.FFTxt12 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_13"
                                                        objInstructionDetail.FFTxt13 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_14"
                                                        objInstructionDetail.FFTxt14 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_15"
                                                        objInstructionDetail.FFTxt15 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_16"
                                                        objInstructionDetail.FFTxt16 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_17"
                                                        objInstructionDetail.FFTxt17 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_18"
                                                        objInstructionDetail.FFTxt18 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_19"
                                                        objInstructionDetail.FFTxt19 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                    Case "FF_txt_20"
                                                        objInstructionDetail.FFTxt20 = drVerDetail(dcVerDetail.ColumnName.ToString()).ToString()
                                                End Select
                                            End If
                                        End If
                                    Next
                                    ' Add Instruction Detail
                                    ICInstructionController.AddInstructionDetail(objInstructionDetail, Me.UserId, Me.UserInfo.Username)
                                End If
                            Next
                        End If
                        For Each dcVer In dtVerified.Columns
                            If dcVer.ColumnName.ToString().Contains("-") Then
                                Dim objICField As New ICFieldsList
                                objICField.es.Connection.CommandTimeout = 3600

                                colName = dcVer.ColumnName.ToString().Split("-")
                                FieldName = colName(0).ToString()
                                FieldID = colName(1).ToString()
                                objICField.LoadByPrimaryKey(FieldID)
                                FieldType = ""

                                FieldType = objICField.FieldType.ToString()

                                If FieldType.ToString() = "FlexiField" Then
                                    Dim objFlexiFieldData As New ICFlexiFieldData

                                    objFlexiFieldData.es.Connection.CommandTimeout = 3600

                                    objFlexiFieldData.FieldID = FieldID
                                    objFlexiFieldData.InstructionID = objInstruction.InstructionID
                                    objFlexiFieldData.FieldValue = drVer(dcVer.ColumnName.ToString()).ToString()
                                    ICFlexiFieldDataController.AddFlexiFieldData(objFlexiFieldData)
                                End If
                            End If
                        Next
                    End If


                Next
            End If
            Return True
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        If Page.IsValid Then
            Try
                Dim objFile As New ICFiles
                Dim Action As String = ""
                Dim AuditType As String = ""


                If objFile.LoadByPrimaryKey(hfClientFileID.Value.ToString()) Then
                    objFile.Status = "Rejected"
                    objFile.ProcessDate = Date.Now
                    objFile.ProcessBy = Me.UserId

                    Action = "Client File: File [Name: " & objFile.FileName & " ; ID: " & objFile.FileID & "] is rejected [Status: " & objFile.Status & " ; ProcessedBy: " & objFile.ProcessBy & " ; ProcessDate: " & objFile.ProcessDate & "] by User [ID: " & Me.UserId.ToString() & " ; Name: " & Me.UserInfo.Username.ToString() & "]."
                    AuditType = "Client File"
                    ICFilesController.UpdateClientFile(objFile, Me.UserId, Me.UserInfo.Username, Action.ToString(), AuditType)

                    UIUtilities.ShowDialog(Me, "Client File Upload", "File rejected.", IC.Dialogmessagetype.Success, NavigateURL())
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'Response.Clear()
        'Response.Flush()
        'Response.Redirect(NavigateURL(PortalSettings.HomeTabId), True)
        Response.Redirect(NavigateURL(41), False)
        Exit Sub
    End Sub

    Protected Sub btnProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        If Page.IsValid Then
            Try
                Dim objICFiles As New ICFiles
                objICFiles.LoadByPrimaryKey(hfClientFileID.Value.ToString)
                objICFiles.Status = "In Process"
                Dim ActionStr As String = Nothing
                ActionStr = "Client Invoice DB File [ " & objICFiles.OriginalFileName & " ] of Client [ " & objICFiles.UpToICCompanyByCompanyCode.CompanyName & " ] status updated from"
                ActionStr += " [ " & objICFiles.Status & " ] to [ In Process ]. Action was taken by User [ " & Me.UserId.ToString & " ] [ " & Me.UserInfo.Username.ToString & " ]"
                ICFilesController.UpdateClientFile(objICFiles, Me.UserId.ToString, Me.UserInfo.Username.ToString, ActionStr, "UPDATE")
                Dim res As IAsyncResult = ParseAndUploadInstructionInFileVIAAsynchronusMethod(Me.UserId.ToString, hfClientFileID.Value.ToString, "File Upload")
                Session("FileUploadingResult") = res

                UIUtilities.ShowDialog(Me, "Client File Upload", "File parsing is in progress.", ICBO.IC.Dialogmessagetype.Success, NavigateURL(184))
                'Response.Redirect(NavigateURL("", "&mid=" & Me.ModuleId & "&msg='File parsing is in progress.'"), False)
            Catch ex As Exception
                Dim objICFiles As New ICFiles
                objICFiles.LoadByPrimaryKey(hfClientFileID.Value.ToString)
                objICFiles.Status = "Pending Read"
                objICFiles.ProcessDate = Nothing
                objICFiles.TotalAmount = Nothing
                objICFiles.TransactionCount = Nothing
                objICFiles.ProcessBy = Nothing
                Dim ActionStr As String = Nothing
                ActionStr = "Client File [ " & objICFiles.OriginalFileName & " ] of Client [ " & objICFiles.UpToICCompanyByCompanyCode.CompanyName & " ] status updated from"
                ActionStr += " [ " & objICFiles.Status & " ] to [ Pending Read ] while parsing while faile. Action was taken by User [ " & Me.UserId.ToString & " ] [ " & Me.UserInfo.Username.ToString & " ]"
                ICFilesController.UpdateClientFile(objICFiles, Me.UserId.ToString, Me.UserInfo.Username.ToString, ActionStr, "Client File")
                ICInstructionController.DeleteInstructionByFileIDInError(hfClientFileID.Value.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Sub CallBackMethod(ByVal ar As IAsyncResult)
        Dim result As AsyncResult = CType(ar, AsyncResult)
        Dim longm As LongRun = CType(result.AsyncDelegate, LongRun)

    End Sub
    Private Function ParseAndUploadInstructionInFileVIAAsynchronusMethod(ByVal UsersID As String, ByVal UploadedFileID As String, ByVal AcqMode As String) As IAsyncResult
        Try
            Dim longm As LongRun = New LongRun(AddressOf ICFilesController.AsynChronusMethodForParseAndSaveCollectionInstructions) ' Delegate will call LongRunningMethod

            Dim ar As IAsyncResult = longm.BeginInvoke(Me.UserId.ToString, hfClientFileID.Value.ToString, AcqMode, New AsyncCallback(AddressOf CallBackMethod), Nothing)

            '//”CHECK” will go as function parameter to LongRunningMethod
            Return ar     ' //Once LongRunningMethod get over CallBackMethod method will be invoked. 

        Catch ex As Exception
            Throw ex
        End Try



    End Function



    Protected Sub gvClientFiles_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvClientFiles.ItemDataBound

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim ibtnProcess As New ImageButton
            Dim AppPath As String = "" ' DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()

            Dim hlDownload As New HyperLink
            ibtnProcess = DirectCast(e.Item.Cells(13).FindControl("ibtnProcess"), ImageButton)
            hlDownload = DirectCast(e.Item.Cells(14).FindControl("hlDownload"), HyperLink)
            If e.Item.Cells(12).Text.ToString() = "Pending Read" Then
                ibtnProcess.Enabled = True
            Else
                ibtnProcess.Enabled = False
            End If
            ' hlDownload.NavigateUrl = AppPath & "/DownloadFile.aspx?fileid=" & e.Item.Cells(2).Text.ToString()
            hlDownload.NavigateUrl = "/DownloadFile.aspx?fileid=" & e.Item.Cells(2).Text.ToString().EncryptString()

        End If
    End Sub
#Region "Save File Upload"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim MIMEType As String = ""
                Dim FileName As String = ""
                Dim Action, AuditType As String
                Dim chkCorrectFile As Boolean = False
                Dim objICFile As New ICFiles
                Dim objICCollectionInvoiceDBTemplate As New ICCollectionInvoiceDataTemplate
                Dim objICCollectionNature As New ICCollectionNature
                Dim objICCompany As New ICCompany
                Dim CheckResult As String = ""
                Dim FileLocation As String = ""

                FileLocation = Request.PhysicalApplicationPath.ToString() & "UploadedFiles\"
                objICCollectionNature.LoadByPrimaryKey(ddlCollectionAccountNature.SelectedValue.ToString)
                objICCompany.LoadByPrimaryKey(objICCollectionNature.CompanyCode)
                MIMEType = fuplClientFile.PostedFile.ContentType
                Action = ""
                AuditType = ""
                'Check .txt
                If MIMEType = "text/plain" Then
                    chkCorrectFile = True
                End If
                'Check .xls
                If MIMEType = "application/vnd.ms-excel" Then
                    chkCorrectFile = True
                End If
                'Check .xlsx
                If MIMEType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" Then
                    chkCorrectFile = True
                End If

                If chkCorrectFile = True Then
                    If objICCollectionInvoiceDBTemplate.LoadByPrimaryKey(ddlInvoiceDBTemplate.SelectedValue.ToString()) Then
                        If objICCollectionInvoiceDBTemplate.TemplateFormat.ToString().ToLower.ToString = ".txt" Then
                            If fuplClientFile.FileName.ToString().Split(".")(1).ToString().ToLower.Contains("txt") Or fuplClientFile.FileName.ToString().Split(".")(1).ToString().ToLower.Contains("csv") Then
                                FileName = ddlInvoiceDBTemplate.SelectedValue.ToString() & "" & Date.Now.ToString("ddMMyyyyHHmmss") & fuplClientFile.FileName.Replace(" ", "_")
                                ' Save File on HDD

                                fuplClientFile.SaveAs(FileLocation & FileName.ToString())
                                CheckResult = ICFilesController.CheckCollectionFileBeforeUpload(ddlInvoiceDBTemplate.SelectedValue.ToString, FileLocation & FileName.ToString())
                                If CheckResult.ToString() = "OK" Then
                                    Action = "Invoice DB File: File [Name: " & fuplClientFile.FileName.ToString() & "] of Group [Code: " & objICCompany.UpToICGroupByGroupCode.GroupCode & " ; Name: " & objICCompany.UpToICGroupByGroupCode.GroupName & "], Company [Code: " & objICCompany.CompanyCode & " ; Name: " & objICCompany.CompanyName & "] and Template Name: " & objICCollectionInvoiceDBTemplate.TemplateName.ToString() & " ; Format : " & objICCollectionInvoiceDBTemplate.TemplateFormat.ToString() & "] is uploaded by User [ID: " & Me.UserId.ToString() & " ; Name: " & Me.UserInfo.Username.ToString() & "]."
                                    AuditType = "Invoice DB File Upload"
                                    objICFile.FileID = ICUtilities.UploadFiletoDB(fuplClientFile, "Invoice DB File", "Invoice DB File", FileName, Me.UserId, Me.UserInfo.Username, Action.ToString(), AuditType.ToString())
                                    Dim cICFiles As New ICFiles

                                    cICFiles.es.Connection.CommandTimeout = 3600

                                    cICFiles.LoadByPrimaryKey(objICFile.FileID)
                                    cICFiles.Status = "Pending Read"
                                    cICFiles.AcqMode = "File Upload"
                                    cICFiles.FileBatchNo = ICFilesController.GetFileBatchNo("Invoice DB File")
                                    cICFiles.OriginalFileName = fuplClientFile.FileName
                                    cICFiles.FileLocation = FileLocation & FileName.ToString()
                                    cICFiles.CompanyCode = objICCompany.CompanyCode

                                    'cICFiles.AccountNumber = ddlCollectionAccountNature.SelectedValue.ToString().Split("-")(0).ToString()
                                    'cICFiles.BranchCode = ddlCollectionAccountNature.SelectedValue.ToString().Split("-")(1).ToString()
                                    'cICFiles.Currency = ddlCollectionAccountNature.SelectedValue.ToString().Split("-")(2).ToString()
                                    cICFiles.PaymentNatureCode = ddlCollectionAccountNature.SelectedValue.ToString()
                                    cICFiles.FileUploadTemplateID = ddlInvoiceDBTemplate.SelectedValue.ToString()
                                    cICFiles.CreatedDate = Date.Now

                                    Action = "Invoice DB File: File [Name: " & cICFiles.FileName & " ; ID: " & cICFiles.FileID & "] is updated as [Status: " & cICFiles.Status & " ; AcquisitionMode: " & cICFiles.AcqMode & " ; BatchNo: " & cICFiles.FileBatchNo & " ; OriginalFileName: " & cICFiles.OriginalFileName & " ; FileLocation: " & cICFiles.FileLocation & " ; CompanyCode: " & cICFiles.CompanyCode & " ; AccountNumber: " & cICFiles.AccountNumber & " ; BranchCode: " & cICFiles.BranchCode & " ; Currency: " & cICFiles.Currency & " ; Collection Nature: " & cICFiles.PaymentNatureCode & " ; TemplateID: " & cICFiles.FileUploadTemplateID & "] by User [ID: " & Me.UserId.ToString() & " ; Name: " & Me.UserInfo.Username.ToString() & "]."
                                    AuditType = "Invoice DB File Upload"
                                    ICFilesController.UpdateClientFile(cICFiles, Me.UserId, Me.UserInfo.Username, Action.ToString(), AuditType)
                                Else
                                    Dim aFileInfo As IO.FileInfo
                                    aFileInfo = New IO.FileInfo(FileLocation & FileName.ToString())
                                    'Delete file from HDD.
                                    If aFileInfo.Exists Then
                                        aFileInfo.Delete()
                                    End If
                                    UIUtilities.ShowDialog(Me, "Client File Upload", CheckResult.ToString(), ICBO.IC.Dialogmessagetype.Failure)
                                End If
                            Else
                                UIUtilities.ShowDialog(Me, "Client File Upload", "File Must be in the [" & objICCollectionInvoiceDBTemplate.TemplateFormat.ToString() & " format", ICBO.IC.Dialogmessagetype.Failure)
                            End If
                        End If
                        If objICCollectionInvoiceDBTemplate.TemplateFormat.ToString().ToLower.ToString = ".xls" Then
                            If fuplClientFile.FileName.ToString().Split(".")(1).ToString().ToLower = "xls" Then
                                FileName = ddlInvoiceDBTemplate.SelectedValue.ToString() & "" & Date.Now.ToString("ddMMyyyyHHmmss") & fuplClientFile.FileName.Replace(" ", "_")
                                ' Save File on HDD
                                fuplClientFile.SaveAs(FileLocation & FileName.ToString())
                                CheckResult = ICFilesController.CheckCollectionFileBeforeUpload(ddlInvoiceDBTemplate.SelectedValue.ToString, FileLocation & FileName.ToString()).ToString()
                                If CheckResult.ToString() = "OK" Then
                                    Action = "Invoice DB File: File [Name: " & fuplClientFile.FileName.ToString() & "] of Group [Code: " & objICCompany.UpToICGroupByGroupCode.GroupCode & " ; Name: " & objICCompany.UpToICGroupByGroupCode.GroupName & "], Company [Code: " & objICCompany.CompanyCode & " ; Name: " & objICCompany.CompanyName & "] and Template Name: " & objICCollectionInvoiceDBTemplate.TemplateName.ToString() & " ; Format : " & objICCollectionInvoiceDBTemplate.TemplateFormat.ToString() & "] is uploaded by User [ID: " & Me.UserId.ToString() & " ; Name: " & Me.UserInfo.Username.ToString() & "]."
                                    AuditType = "Invoice DB File Upload"
                                    objICFile.FileID = ICUtilities.UploadFiletoDB(fuplClientFile, "Invoice DB File", "Invoice DB File", FileName, Me.UserId, Me.UserInfo.Username, Action.ToString(), AuditType.ToString())
                                    Dim cICFiles As New ICFiles

                                    cICFiles.es.Connection.CommandTimeout = 3600

                                    cICFiles.LoadByPrimaryKey(objICFile.FileID)
                                    cICFiles.Status = "Pending Read"
                                    cICFiles.AcqMode = "File Upload"
                                    cICFiles.FileBatchNo = ICFilesController.GetFileBatchNo("Invoice DB File")
                                    cICFiles.OriginalFileName = fuplClientFile.FileName
                                    cICFiles.FileLocation = FileLocation & FileName.ToString()
                                    cICFiles.CompanyCode = objICCompany.CompanyCode

                                    'cICFiles.AccountNumber = ddlCollectionAccountNature.SelectedValue.ToString().Split("-")(0).ToString()
                                    'cICFiles.BranchCode = ddlCollectionAccountNature.SelectedValue.ToString().Split("-")(1).ToString()
                                    'cICFiles.Currency = ddlCollectionAccountNature.SelectedValue.ToString().Split("-")(2).ToString()
                                    cICFiles.PaymentNatureCode = ddlCollectionAccountNature.SelectedValue.ToString()
                                    cICFiles.FileUploadTemplateID = ddlInvoiceDBTemplate.SelectedValue.ToString()
                                    cICFiles.CreatedDate = Date.Now

                                    Action = "Invoice DB File: File [Name: " & cICFiles.FileName & " ; ID: " & cICFiles.FileID & "] is updated as [Status: " & cICFiles.Status & " ; AcquisitionMode: " & cICFiles.AcqMode & " ; BatchNo: " & cICFiles.FileBatchNo & " ; OriginalFileName: " & cICFiles.OriginalFileName & " ; FileLocation: " & cICFiles.FileLocation & " ; CompanyCode: " & cICFiles.CompanyCode & " ; AccountNumber: " & cICFiles.AccountNumber & " ; BranchCode: " & cICFiles.BranchCode & " ; Currency: " & cICFiles.Currency & " ; Collection Nature: " & cICFiles.PaymentNatureCode & " ; TemplateID: " & cICFiles.FileUploadTemplateID & "] by User [ID: " & Me.UserId.ToString() & " ; Name: " & Me.UserInfo.Username.ToString() & "]."
                                    AuditType = "Invoice DB File Upload"
                                    ICFilesController.UpdateClientFile(cICFiles, Me.UserId, Me.UserInfo.Username, Action.ToString(), AuditType)
                                Else
                                    Dim aFileInfo As IO.FileInfo
                                    aFileInfo = New IO.FileInfo(FileLocation & FileName.ToString())
                                    'Delete file from HDD.
                                    If aFileInfo.Exists Then
                                        aFileInfo.Delete()
                                    End If
                                    UIUtilities.ShowDialog(Me, "Client File Upload", CheckResult.ToString(), ICBO.IC.Dialogmessagetype.Failure)
                                End If
                            Else
                                UIUtilities.ShowDialog(Me, "Client File Upload", "File Must be in the [" & objICCollectionInvoiceDBTemplate.TemplateFormat.ToString() & "] format", ICBO.IC.Dialogmessagetype.Failure)
                            End If
                        End If
                        If objICCollectionInvoiceDBTemplate.TemplateFormat.ToString().ToLower.ToString = ".csv" Then
                            If fuplClientFile.FileName.ToString().Split(".")(1).ToString().ToLower = "csv" Then
                                FileName = ddlInvoiceDBTemplate.SelectedValue.ToString() & "" & Date.Now.ToString("ddMMyyyyHHmmss") & fuplClientFile.FileName.Replace(" ", "_")
                                ' Save File on HDD
                                fuplClientFile.SaveAs(FileLocation & FileName.ToString())
                                CheckResult = ICFilesController.CheckCollectionFileBeforeUpload(ddlInvoiceDBTemplate.SelectedValue.ToString, FileLocation & FileName.ToString()).ToString()
                                If CheckResult.ToString() = "OK" Then
                                    Action = "Invoice DB File: File [Name: " & fuplClientFile.FileName.ToString() & "] of Group [Code: " & objICCompany.UpToICGroupByGroupCode.GroupCode & " ; Name: " & objICCompany.UpToICGroupByGroupCode.GroupName & "], Company [Code: " & objICCompany.CompanyCode & " ; Name: " & objICCompany.CompanyName & "] and Template Name: " & objICCollectionInvoiceDBTemplate.TemplateName.ToString() & " ; Format : " & objICCollectionInvoiceDBTemplate.TemplateFormat.ToString() & "] is uploaded by User [ID: " & Me.UserId.ToString() & " ; Name: " & Me.UserInfo.Username.ToString() & "]."
                                    AuditType = "Invoice DB File Upload"
                                    objICFile.FileID = ICUtilities.UploadFiletoDB(fuplClientFile, "Invoice DB File", "Invoice DB File", FileName, Me.UserId, Me.UserInfo.Username, Action.ToString(), AuditType.ToString())
                                    Dim cICFiles As New ICFiles

                                    cICFiles.es.Connection.CommandTimeout = 3600

                                    cICFiles.LoadByPrimaryKey(objICFile.FileID)
                                    cICFiles.Status = "Pending Read"
                                    cICFiles.AcqMode = "File Upload"
                                    cICFiles.FileBatchNo = ICFilesController.GetFileBatchNo("Invoice DB File")
                                    cICFiles.OriginalFileName = fuplClientFile.FileName
                                    cICFiles.FileLocation = FileLocation & FileName.ToString()
                                    cICFiles.CompanyCode = objICCompany.CompanyCode

                                    'cICFiles.AccountNumber = ddlCollectionAccountNature.SelectedValue.ToString().Split("-")(0).ToString()
                                    'cICFiles.BranchCode = ddlCollectionAccountNature.SelectedValue.ToString().Split("-")(1).ToString()
                                    'cICFiles.Currency = ddlCollectionAccountNature.SelectedValue.ToString().Split("-")(2).ToString()
                                    cICFiles.PaymentNatureCode = ddlCollectionAccountNature.SelectedValue.ToString()
                                    cICFiles.FileUploadTemplateID = ddlInvoiceDBTemplate.SelectedValue.ToString()
                                    cICFiles.CreatedDate = Date.Now

                                    Action = "Invoice DB File: File [Name: " & cICFiles.FileName & " ; ID: " & cICFiles.FileID & "] is updated as [Status: " & cICFiles.Status & " ; AcquisitionMode: " & cICFiles.AcqMode & " ; BatchNo: " & cICFiles.FileBatchNo & " ; OriginalFileName: " & cICFiles.OriginalFileName & " ; FileLocation: " & cICFiles.FileLocation & " ; CompanyCode: " & cICFiles.CompanyCode & " ; AccountNumber: " & cICFiles.AccountNumber & " ; BranchCode: " & cICFiles.BranchCode & " ; Currency: " & cICFiles.Currency & " ; Collection Nature: " & cICFiles.PaymentNatureCode & " ; TemplateID: " & cICFiles.FileUploadTemplateID & "] by User [ID: " & Me.UserId.ToString() & " ; Name: " & Me.UserInfo.Username.ToString() & "]."
                                    AuditType = "Invoice DB File Upload"
                                    ICFilesController.UpdateClientFile(cICFiles, Me.UserId, Me.UserInfo.Username, Action.ToString(), AuditType)
                                Else
                                    Dim aFileInfo As IO.FileInfo
                                    aFileInfo = New IO.FileInfo(FileLocation & FileName.ToString())
                                    'Delete file from HDD.
                                    If aFileInfo.Exists Then
                                        aFileInfo.Delete()
                                    End If
                                    UIUtilities.ShowDialog(Me, "Client File Upload", CheckResult.ToString(), ICBO.IC.Dialogmessagetype.Failure)
                                End If
                            Else
                                UIUtilities.ShowDialog(Me, "Client File Upload", "File Must be in the [" & objICCollectionInvoiceDBTemplate.TemplateFormat.ToString() & "] format", ICBO.IC.Dialogmessagetype.Failure)
                            End If
                        End If
                        If objICCollectionInvoiceDBTemplate.TemplateFormat.ToString().ToLower.ToString = ".xlsx" Then
                            If fuplClientFile.FileName.ToString().Split(".")(1).ToString().ToLower = "xlsx" Then
                                FileName = ddlInvoiceDBTemplate.SelectedValue.ToString() & "" & Date.Now.ToString("ddMMyyyyHHmmss") & fuplClientFile.FileName.Replace(" ", "_")
                                ' Save File on HDD
                                fuplClientFile.SaveAs(FileLocation & FileName.ToString())
                                CheckResult = ICFilesController.CheckCollectionFileBeforeUpload(ddlInvoiceDBTemplate.SelectedValue.ToString, FileLocation & FileName.ToString()).ToString()
                                If CheckResult.ToString() = "OK" Then
                                    Action = "Invoice DB File: File [Name: " & fuplClientFile.FileName.ToString() & "] of Group [Code: " & objICCompany.UpToICGroupByGroupCode.GroupCode & " ; Name: " & objICCompany.UpToICGroupByGroupCode.GroupName & "], Company [Code: " & objICCompany.CompanyCode & " ; Name: " & objICCompany.CompanyName & "] and Template Name: " & objICCollectionInvoiceDBTemplate.TemplateName.ToString() & " ; Format : " & objICCollectionInvoiceDBTemplate.TemplateFormat.ToString() & "] is uploaded by User [ID: " & Me.UserId.ToString() & " ; Name: " & Me.UserInfo.Username.ToString() & "]."
                                    AuditType = "Invoice DB File Upload"
                                    objICFile.FileID = ICUtilities.UploadFiletoDB(fuplClientFile, "Invoice DB File", "Invoice DB File", FileName, Me.UserId, Me.UserInfo.Username, Action.ToString(), AuditType.ToString())
                                    Dim cICFiles As New ICFiles

                                    cICFiles.es.Connection.CommandTimeout = 3600

                                    cICFiles.LoadByPrimaryKey(objICFile.FileID)
                                    cICFiles.Status = "Pending Read"
                                    cICFiles.AcqMode = "File Upload"
                                    cICFiles.FileBatchNo = ICFilesController.GetFileBatchNo("Invoice DB File")
                                    cICFiles.OriginalFileName = fuplClientFile.FileName
                                    cICFiles.FileLocation = FileLocation & FileName.ToString()
                                    cICFiles.CompanyCode = objICCompany.CompanyCode

                                    'cICFiles.AccountNumber = ddlCollectionAccountNature.SelectedValue.ToString().Split("-")(0).ToString()
                                    'cICFiles.BranchCode = ddlCollectionAccountNature.SelectedValue.ToString().Split("-")(1).ToString()
                                    'cICFiles.Currency = ddlCollectionAccountNature.SelectedValue.ToString().Split("-")(2).ToString()
                                    cICFiles.PaymentNatureCode = ddlCollectionAccountNature.SelectedValue.ToString()
                                    cICFiles.FileUploadTemplateID = ddlInvoiceDBTemplate.SelectedValue.ToString()
                                    cICFiles.CreatedDate = Date.Now

                                    Action = "Invoice DB File: File [Name: " & cICFiles.FileName & " ; ID: " & cICFiles.FileID & "] is updated as [Status: " & cICFiles.Status & " ; AcquisitionMode: " & cICFiles.AcqMode & " ; BatchNo: " & cICFiles.FileBatchNo & " ; OriginalFileName: " & cICFiles.OriginalFileName & " ; FileLocation: " & cICFiles.FileLocation & " ; CompanyCode: " & cICFiles.CompanyCode & " ; AccountNumber: " & cICFiles.AccountNumber & " ; BranchCode: " & cICFiles.BranchCode & " ; Currency: " & cICFiles.Currency & " ; Collection Nature: " & cICFiles.PaymentNatureCode & " ; TemplateID: " & cICFiles.FileUploadTemplateID & "] by User [ID: " & Me.UserId.ToString() & " ; Name: " & Me.UserInfo.Username.ToString() & "]."
                                    AuditType = "Invoice DB File Upload"
                                    ICFilesController.UpdateClientFile(cICFiles, Me.UserId, Me.UserInfo.Username, Action.ToString(), AuditType)
                                Else
                                    Dim aFileInfo As IO.FileInfo
                                    aFileInfo = New IO.FileInfo(FileLocation & FileName.ToString())
                                    'Delete file from HDD.
                                    If aFileInfo.Exists Then
                                        aFileInfo.Delete()
                                    End If
                                    UIUtilities.ShowDialog(Me, "Invoice DB File Upload", CheckResult.ToString(), ICBO.IC.Dialogmessagetype.Failure)
                                End If
                            Else
                                UIUtilities.ShowDialog(Me, "Invoice DB File Upload", "File Must be in the [" & objICCollectionInvoiceDBTemplate.TemplateFormat.ToString() & "] format", ICBO.IC.Dialogmessagetype.Failure)
                            End If
                        End If
                    End If
                Else
                    UIUtilities.ShowDialog(Me, "Invoice DB File Upload", "Invalid file format.", ICBO.IC.Dialogmessagetype.Failure)
                End If
                If objICFile.FileID > 0 Then
                    UIUtilities.ShowDialog(Me, "Invoice DB File Upload", fuplClientFile.FileName.ToString() & " Added Successfully.", ICBO.IC.Dialogmessagetype.Success)
                    'Dim objICFilesNew As New ICFiles
                    'Dim objICUser As New ICUser
                    'Dim APNature As String = Nothing
                    'objICFilesNew.LoadByPrimaryKey(objICFile.FileID)
                    'objICUser.LoadByPrimaryKey(Me.UserId.ToString)
                    'APNature = objICFilesNew.AccountNumber & "-" & objICFilesNew.BranchCode & "-" & objICFilesNew.Currency & "-" & objICFilesNew.PaymentNatureCode & "-" & objICUser.OfficeCode.ToString
                    'EmailUtilities.Fileuploadedbyuser(fuplClientFile.FileName.ToString(), Me.UserInfo.Username.ToString, APNature)
                    'SMSUtilities.Fileuploadedbyuser(Me.UserInfo.Username.ToString, APNature)
                    Clear()

                    LoadgvClientFiles(True)

                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Sub Clear()
        Try
            SetArraListRoleID()
            LoadddlCollectionAccountCollectionNature()
            LoadddlInvoiceDBTemplate()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

    Protected Sub btnCancelMain_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelMain.Click
        Try
            Response.Redirect(NavigateURL(41))
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        If Page.IsValid Then
            Try
                LoadgvClientFiles(True)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
End Class

