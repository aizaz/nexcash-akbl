﻿Imports ICBO
Imports ICBO.IC
Imports System.IO
Imports System.Data
Imports Telerik.Reporting.Processing
Imports Microsoft.VisualBasic
Imports System.Net
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security

Partial Class DesktopModules_test_TestIRIS
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Dim FileRIN As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub btnBalanceInquiry_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBalanceInquiry.Click
        Try
            txtResult.Text = ICBO.CBUtilities.BalanceInquiry(txtAccountNo.Text.ToString().ToUpper(), ICBO.CBUtilities.AccountType.RB, "Test Account Balance", txtAccountNo.Text, txtFromAccountBranchCode.Text).ToString("N2")
        Catch ex As Exception
            'If ex.Message.Contains("IRIS") Then
            txtResult.Text = ex.Message
            'End If
        End Try

    End Sub

    Protected Sub btnFundsTransferDirect_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFundsTransferDirect.Click
        Try

            If CBUtilities.DoTransaction("", "1", "0433", "", "", "0108161191015", "1", "0101", "", "", "0110122397019", "", "", "", "", "", "", CDbl(10.0), "Test Transaction", "N", 100, " Instruction", CBUtilities.AccountType.RB, CBUtilities.AccountType.RB, "0433") = True Then
                txtResult.Text = "Funds Transfer Successfuly."
            Else
                txtResult.Text = "Funds Not Transfer."
            End If
        Catch ex As Exception
            If ex.Message.Contains("IRIS") Then
                txtResult.Text = ex.Message
            End If
        End Try
        
    End Sub

    Protected Sub btnFundsTransferOther_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFundsTransferOther.Click
        Try

            Dim Auth_UserID, Auth_IP, Auth_Password As String
            Auth_UserID = ""
            Auth_IP = ""
            Auth_Password = ""
            CBUtilities.GetAuthenticationCredentials(Auth_UserID, Auth_Password, Auth_IP)
            'PK32UNIL0002000001100111
            '01010110304534031
            Dim switchCl As New IntelligenesSwitch.SwitchClient
            Dim Result As String = ""
            Dim Stan As String = CBUtilities.GetStanNo("615423")
            Result = switchCl.IBFTEuronet("588974", "0002000001100111", "United Bank Limited", "UBL", "01010110122397019", "627688", 1, Stan, "Hex", 1, Auth_UserID, Auth_Password, Auth_IP)

            txtMSG.Text = Result


        Catch ex As Exception
            'If ex.Message.Contains("IRIS") Then
            txtResult.Text = ex.Message
            'End If
        End Try
        
    End Sub

    Protected Sub btnTitleFetch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTitleFetch.Click
        Try
            txtResult.Text = CBUtilities.TitleFetch(txtAccountNo.Text.ToString(), CBUtilities.AccountType.RB, "Test Account Title", txtAccountNo.Text).ToString()
        Catch ex As Exception
            'If ex.Message.Contains("IRIS") Then
            txtResult.Text = ex.Message
            'End If
        End Try

    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtResult.Text = ""
        txtAccountNo.Text = ""
        txtNumber.Text = ""
        txtMSG.Text = ""
        txtFromIMD.Text = ""
        txtFromAccount.Text = ""
        txtToBankIMD.Text = ""
        txtToAccount.Text = ""
    End Sub

   
    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Try
            ICUtilities.SendSMS(txtNumber.Text.ToString(), txtMSG.Text.ToString())
        Catch ex As Exception
            txtResult.Text = ex.Message
        End Try
    End Sub

    'Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click

    '    Dim rpt As New rptBeneDetail
    '    Dim rp As New ReportProcessor
    '    rpt.DataSource = Intelligenes.FRC.FRCReportController.GetBeneficiaryDetails("", "")
    '    ReportViewer1.Report = rpt
    '    ReportViewer1.RefreshReport()

    'End Sub

    Protected Sub btnSendEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendEmail.Click
        Try
            ICUtilities.SendEmail("aizaz@intelligenes.net", "Test", "Test")
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Protected Sub btnEmailCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmailCheck.Click



        'FRCInstructionProcessController.AutoReadEmailsAndFileUpload()
        'FRCInstructionProcessController.AutoReadFTPAndFileUpload()


        ''Try

        ''    Dim frcAcqAgent As New FRCAcqAgent
        ''    Dim collAcqAgent As New FRCAcqAgentCollection
        ''    Dim aFileInfo As FileInfo
        ''    Dim MIMEType As String = ""
        ''    Dim FileName As String = ""
        ''    Dim chkCorrectFile As Boolean = False
        ''    Dim frcFile As New FRC.FRCFiles
        ''    Dim frcFUT As New FRCFileUploadTemplate
        ''    Dim frcAgentFileTemplate As New FRCAcqAgent
        ''    ' Dim FrcUser As New FRC.FrcUser
        ''    Dim NoOfFiles As Integer = 0
        ''    Dim UserName, Password, Server, Domain As String
        ''    Dim Service As New ExchangeService(ExchangeVersion.Exchange2010_SP1)


        ''    UserName = FRCUtilities.GetSettingValue("UserName")
        ''    Password = FRCUtilities.GetSettingValue("Pwd")
        ''    Server = FRCUtilities.GetSettingValue("Server")
        ''    Domain = FRCUtilities.GetSettingValue("Domain")

        ''    Service.Credentials = New NetworkCredential(UserName, Password, Domain)
        ''    Service.Url = New Uri("https://" & Server & "/EWS/Exchange.asmx")
        ''    ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateRemoteCertificate)
        ''    Dim results As FindItemsResults(Of Item)
        ''    results = Service.FindItems(WellKnownFolderName.Inbox, New ItemView(10))



        ''    For Each Item As EmailMessage In results.Items
        ''        collAcqAgent = FRCAcqAgentController.GetEmailAllowedAcqAgentActiveAndApproved()
        ''        Item.Load()
        ''        For Each frcAcqAgent In collAcqAgent
        ''            chkCorrectFile = False
        ''            If Item.Sender.Address.ToString.ToLower.Trim = frcAcqAgent.Email.ToString.ToLower.Trim Then
        ''                chkCorrectFile = True
        ''            End If
        ''            If chkCorrectFile = True Then
        ''                chkCorrectFile = False
        ''                For Each atta As FileAttachment In Item.Attachments
        ''                    MIMEType = atta.ContentType
        ''                    'Check .txt
        ''                    If MIMEType = "text/plain" Then
        ''                        chkCorrectFile = True
        ''                    End If
        ''                    'Check .xls
        ''                    If MIMEType = "application/vnd.ms-excel" Then
        ''                        chkCorrectFile = True
        ''                    End If
        ''                    'Check .xlsx
        ''                    If MIMEType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" Then
        ''                        chkCorrectFile = True
        ''                    End If

        ''                    If chkCorrectFile = True Then
        ''                        If frcFUT.LoadByPrimaryKey(FRCAcqAgentTemplateFieldController.GetTemplateIDByAgentID(frcAcqAgent.Code.ToString())) Then
        ''                            If frcFUT.Format.ToString().ToLower.ToString = ".txt" Then
        ''                                If atta.Name.ToString().Split(".")(1).ToString().ToLower.Contains("txt") Or atta.Name.ToString().Split(".")(1).ToString().ToLower.Contains("csv") Then
        ''                                    FileName = frcAcqAgent.Code.ToString() & Date.Now.ToString("ddMMyyyyHHmmss") & atta.Name
        ''                                    atta.Load(HttpContext.Current.Server.MapPath("~/UploadedFiles/") & FileName.ToString())
        ''                                    frcFile.FRCFileID = FRCUtilities.UploadFiletoDB(HttpContext.Current.Server.MapPath("~/UploadedFiles/") & FileName.ToString(), "AcqAgentFile", frcAcqAgent.Code, Me.UserId)
        ''                                    Dim cfrcFiles As New FRC.FRCFiles
        ''                                    cfrcFiles.LoadByPrimaryKey(frcFile.FRCFileID)
        ''                                    cfrcFiles.FRCAgentID = frcAcqAgent.Code
        ''                                    cfrcFiles.FRCStatus = "Pending Read"
        ''                                    cfrcFiles.AcqMode = "Email"
        ''                                    cfrcFiles.OriginalFileName = atta.Name.ToString()
        ''                                    cfrcFiles.Save()
        ''                                    NoOfFiles = NoOfFiles + 1
        ''                                End If
        ''                            End If
        ''                            If frcFUT.Format.ToString().ToLower = ".xls" Then
        ''                                If atta.Name.ToString().Split(".")(1).ToString().ToLower = "xls" Then
        ''                                    FileName = frcAcqAgent.Code.ToString() & Date.Now.ToString("ddMMyyyyHHmmss") & atta.Name
        ''                                    atta.Load(HttpContext.Current.Server.MapPath("~/UploadedFiles/") & FileName.ToString())
        ''                                    frcFile.FRCFileID = FRCUtilities.UploadFiletoDB(HttpContext.Current.Server.MapPath("~/UploadedFiles/") & FileName.ToString(), "AcqAgentFile", frcAcqAgent.Code, Me.UserId)
        ''                                    Dim cfrcFiles As New FRC.FRCFiles
        ''                                    cfrcFiles.LoadByPrimaryKey(frcFile.FRCFileID)
        ''                                    cfrcFiles.FRCAgentID = frcAcqAgent.Code
        ''                                    cfrcFiles.FRCStatus = "Pending Read"
        ''                                    cfrcFiles.AcqMode = "Email"
        ''                                    cfrcFiles.OriginalFileName = atta.Name.ToString()
        ''                                    cfrcFiles.Save()
        ''                                    NoOfFiles = NoOfFiles + 1
        ''                                End If
        ''                            End If
        ''                            If frcFUT.Format.ToString().ToLower = ".xlsx" Then
        ''                                If atta.Name.ToString().Split(".")(1).ToString().ToLower = "xlsx" Then
        ''                                    FileName = frcAcqAgent.Code.ToString() & Date.Now.ToString("ddMMyyyyHHmmss") & atta.Name
        ''                                    atta.Load(HttpContext.Current.Server.MapPath("~/UploadedFiles/") & FileName.ToString())
        ''                                    frcFile.FRCFileID = FRCUtilities.UploadFiletoDB(HttpContext.Current.Server.MapPath("~/UploadedFiles/") & FileName.ToString(), "AcqAgentFile", frcAcqAgent.Code, Me.UserId)
        ''                                    Dim cfrcFiles As New FRC.FRCFiles
        ''                                    cfrcFiles.LoadByPrimaryKey(frcFile.FRCFileID)
        ''                                    cfrcFiles.FRCAgentID = frcAcqAgent.Code
        ''                                    cfrcFiles.FRCStatus = "Pending Read"
        ''                                    cfrcFiles.AcqMode = "Email"
        ''                                    cfrcFiles.OriginalFileName = atta.Name.ToString()
        ''                                    cfrcFiles.Save()
        ''                                    NoOfFiles = NoOfFiles + 1
        ''                                End If
        ''                            End If
        ''                        End If
        ''                    End If
        ''                Next
        ''                Item.Delete(DeleteMode.HardDelete)
        ''                FRCUtilities.AddAuditTrail("Get " & NoOfFiles.ToString() & " files from Email, Acquisition Agent[Code: " & frcAcqAgent.Code & " ; Name: " & frcAcqAgent.Name & " ]", "Email File Upload", frcAcqAgent.Code.ToString(), Nothing, Nothing)
        ''            End If
        ''        Next
        ''    Next
        ''Catch ex As Exception
        ''    ProcessModuleLoadException(Me, ex, False)
        ''    UIUtilities.ShowDialog(Me, "Error", ex.Message, Intelligenes.FRC.Dialogmessagetype.Failure)
        ''End Try
    End Sub
    'Private Shared Function ValidateRemoteCertificate(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal policyerrors As SslPolicyErrors) As Boolean
    '    Return True
    'End Function
    Protected Sub btnEmailProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmailProcess.Click

        'FRCInstructionProcessController.AutoProcessEmailUploadFiles("Email")
        'FRCInstructionProcessController.AutoProcessEmailUploadFiles("FTP")
        ''Dim frcAcqAgent As New FRCAcqAgent
        ''Dim collAcqAgent As New FRCAcqAgentCollection
        ''Dim collFRCFile As New FRCFilesCollection
        ''Dim cFRCFile As New FRCFiles
        ''Dim UserID As String = ""
        ''Try
        ''    collAcqAgent = FRCAcqAgentController.GetEmailAllowedAcqAgentActiveAndApproved()
        ''    For Each frcAcqAgent In collAcqAgent
        ''        collFRCFile = FRCFileController.GetFileByAgentID("AcqAgentFile", frcAcqAgent.Code, "Email")
        ''        If collFRCFile.Count > 0 Then
        ''            For Each cFRCFile In collFRCFile
        ''                UserID = FRCAcqAgentController.GetAcqAgentBranchActiveAndApprovedUserID(frcAcqAgent.Code, "Email")
        ''                ReadInstructionsInFile(cFRCFile.FRCFileID, frcAcqAgent.Code, UserID)
        ''            Next
        ''        End If
        ''    Next
        ''    Session("dtVarified") = Nothing
        ''    Session("dtUnVerified") = Nothing
        ''Catch ex As Exception
        ''    ProcessModuleLoadException(Me, ex, False)
        ''    UIUtilities.ShowDialog(Me, "Error", ex.Message, Intelligenes.FRC.Dialogmessagetype.Failure)
        ''End Try
    End Sub


    'Private Sub ReadInstructionsInFile(ByVal FileID As String, ByVal AgentID As String, ByVal UserID As String)
    '    Try
    '        Dim AcqAgent As New FRCAcqAgent
    '        Dim collAcqAgnetTemplateFields As New FRCAcqAgentTemplateFieldCollection
    '        Dim aFileInfo As IO.FileInfo
    '        Dim fuTemplate As New FRCFileUploadTemplate
    '        Dim frcFile As New FRC.FRCFiles
    '        Dim TemplateID As String = ""
    '        Dim LineNo As Integer = 0
    '        Dim dtVeri As New DataTable
    '        Dim dtUnVeri As New DataTable
    '        Dim drUnVeri As DataRow
    '        Dim InsText As String = ""


    '        AcqAgent.LoadByPrimaryKey(AgentID)
    '        TemplateID = FRCAcqAgentTemplateFieldController.GetTemplateIDByAgentID(AcqAgent.Code.ToString())
    '        fuTemplate.LoadByPrimaryKey(TemplateID)
    '        collAcqAgnetTemplateFields = FRCAcqAgentTemplateFieldController.GetAgentTemplateFieldsByTemplateIDAndAgentID(TemplateID, AcqAgent.Code)
    '        frcFile.LoadByPrimaryKey(FileID)
    '        Session("dtVarified") = Nothing
    '        Session("dtUnVerified") = Nothing



    '        If fuTemplate.Format.Trim().ToString() = ".txt" Then
    '            aFileInfo = New IO.FileInfo(Server.MapPath("~/UploadedFiles/") & frcFile.FRCFileName.ToString())
    '            If aFileInfo.Exists Then
    '                Dim mreader As New System.IO.StreamReader(aFileInfo.FullName)
    '                Do While mreader.Peek <> -1
    '                    LineNo = LineNo + 1
    '                    GetDataFromTXTFile(mreader.ReadLine(), collAcqAgnetTemplateFields, AcqAgent.Code.ToString(), LineNo)
    '                Loop
    '                mreader.Close()
    '                mreader.Dispose()
    '            End If
    '        Else ' in case of xls or xlsx files

    '            Dim ConnStr As String = ""
    '            Dim FilePath As String = ""
    '            Dim cmdStr As String = ""
    '            Dim dsExcel As New DataSet
    '            Dim drExcel As DataRow

    '            FilePath = Server.MapPath("~/UploadedFiles/") & frcFile.FRCFileName.ToString()


    '            'xlsx
    '            If fuTemplate.Format.Trim().ToString() = ".xlsx" Then
    '                ConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FilePath & "; Extended Properties='Excel 12.0 Xml; HDR=Yes; IMEX=1'"
    '            ElseIf fuTemplate.Format.Trim().ToString() = ".xls" Then
    '                ConnStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FilePath & "; Extended Properties='Excel 12.0; HDR=Yes; IMEX=1'"
    '            End If


    '            Dim Conn As New OleDb.OleDbConnection(ConnStr)
    '            cmdStr = "select * from [" & fuTemplate.SheetName.Trim.ToString() & "$]"
    '            Dim cmd As New OleDb.OleDbCommand(cmdStr, Conn)
    '            Dim da As New OleDb.OleDbDataAdapter(cmd)
    '            da.Fill(dsExcel, "ExcelFile")

    '            If Not dsExcel.Tables(0).Rows.Count = 0 Then
    '                For Each drExcel In dsExcel.Tables(0).Rows
    '                    LineNo = LineNo + 1
    '                    GetDataFromEXCELFile(drExcel, collAcqAgnetTemplateFields, AcqAgent.Code.ToString(), LineNo, dsExcel.Tables(0).Columns.Count)
    '                Next
    '            Else

    '            End If
    '        End If
    '        If Not Session("dtVarified") Is Nothing Then
    '            If DirectCast(Session("dtVarified"), DataTable).Rows.Count > 0 Then
    '                dtVeri = DirectCast(Session("dtVarified"), DataTable)
    '                ' Add verified instructions to DB.
    '                SaveVerifiedInstructions(dtVeri, AgentID, FileID, UserID)
    '                FRCUtilities.AddAuditTrail(dtVeri.Rows.Count - 1.ToString() & " Instruction(s) added via Email File Upload; Acquisition Agent[Code: " & AcqAgent.Code & " ; Name: " & AcqAgent.Name & " ] ; File[ID: " & frcFile.FRCFileID & " ; FileName: " & frcFile.OriginalFileName & "] ; Acq Agen tBranch User[ID: " & UserID & "] ", "Email File Upload", AcqAgent.Code.ToString(), Nothing, Nothing)
    '            End If
    '        End If
    '        If Not Session("dtUnVerified") Is Nothing Then
    '            If DirectCast(Session("dtUnVerified"), DataTable).Rows.Count > 0 Then
    '                dtUnVeri = DirectCast(Session("dtUnVerified"), DataTable)
    '                ' Send email to agent of un verified instruction in a file.
    '                InsText = ""
    '                For Each drUnVeri In dtUnVeri.Rows
    '                    InsText = InsText & "" & drUnVeri(0) & " || " & drUnVeri(1) & " || " & drUnVeri(2) & "<br />"
    '                Next
    '                EmailUtilities.EmailAgentForUnVerifiedInstructions(AcqAgent.Name, frcFile.OriginalFileName, "Email File Upload", InsText, AcqAgent.Email)
    '                FRCUtilities.AddAuditTrail(dtUnVeri.Rows.Count - 1.ToString() & " Instruction(s) are not verified via Email File Upload and email to [EmailID: " & AcqAgent.Email & "]; Acquisition Agent[Code: " & AcqAgent.Code & " ; Name: " & AcqAgent.Name & " ] ; File[ID: " & frcFile.FRCFileID & " ; FileName: " & frcFile.OriginalFileName & "] ; Acq Agen tBranch User[ID: " & UserID & "] ", "Email File Upload", AcqAgent.Code.ToString(), Nothing, Nothing)
    '            End If
    '        End If

    '        FRCFileController.UpdateFileStatus(FileID.ToString(), "Data Read")    'Set File Status
    '        frcFile.LoadByPrimaryKey(FileID.ToString())
    '        aFileInfo = New IO.FileInfo(Server.MapPath("~/UploadedFiles/") & frcFile.FRCFileName.ToString())
    '        'Delete file from HDD.
    '        If aFileInfo.Exists Then
    '            aFileInfo.Delete()
    '        End If


    '    Catch ex As Exception
    '        If ex.Message.Contains("is not a valid name.  Make sure that it does not include") Then
    '            UIUtilities.ShowDialog(Me, "Error", "Sheet Name is not valid.", Intelligenes.FRC.Dialogmessagetype.Failure, NavigateURL())
    '            Exit Sub
    '        End If
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, Intelligenes.FRC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    'Private Sub GetDataFromTXTFile(ByVal FileLine As String, ByVal collfrcAcqAgentFileTemplate As FRCAcqAgentTemplateFieldCollection, ByVal AgentID As String, ByVal LineNumber As Integer)
    '    Dim dtVarified, dtUnVerified As New DataTable
    '    Dim drVarified, drUnVerified As DataRow
    '    Dim AcqAgentTemplateFields As New FRCAcqAgentTemplateField
    '    Dim FileTemplate As New FRCFileUploadTemplate
    '    Dim StartIndex As Integer
    '    Dim ErrorMsg As String = ""
    '    Dim arrInsruction As String()
    '    Dim CountColumn As Integer = 0
    '    Try
    '        StartIndex = 0
    '        FileTemplate.LoadByPrimaryKey(collfrcAcqAgentFileTemplate(0).TemplateID.ToString())

    '        If Session("dtVarified") Is Nothing Then
    '            For Each AcqAgentTemplateFields In collfrcAcqAgentFileTemplate
    '                dtVarified.Columns.Add(New DataColumn(AcqAgentTemplateFields.FieldName.ToString(), GetType(System.String)))
    '            Next
    '            dtUnVerified.Columns.Add(New DataColumn("Data", GetType(System.String)))
    '            dtUnVerified.Columns.Add(New DataColumn("Message", GetType(System.String)))
    '            dtUnVerified.Columns.Add(New DataColumn("LineNo", GetType(System.String)))
    '        Else
    '            dtVarified = DirectCast(Session("dtVarified"), DataTable)
    '            dtUnVerified = DirectCast(Session("dtUnVerified"), DataTable)
    '        End If
    '        If FileTemplate.IsFixLength = True Then
    '            If FileLine.Length = FRCAcqAgentTemplateFieldController.GetAgentTemplateFieldLengthByAgentID(AgentID) Then
    '                ErrorMsg = CheckDataTXT(FileLine, collfrcAcqAgentFileTemplate, FileTemplate)
    '                If ErrorMsg = "" Then
    '                    drVarified = dtVarified.NewRow()
    '                    For Each AcqAgentTemplateFields In collfrcAcqAgentFileTemplate
    '                        If AcqAgentTemplateFields.FieldName.ToString().Trim() = "AmountPKR" Then
    '                            drVarified(CInt(AcqAgentTemplateFields.FieldOrder) - 1) = CDbl(FileLine.Substring(StartIndex, AcqAgentTemplateFields.FixLength).ToString().Trim())
    '                        Else
    '                            drVarified(CInt(AcqAgentTemplateFields.FieldOrder) - 1) = FileLine.Substring(StartIndex, AcqAgentTemplateFields.FixLength).ToString().Trim()
    '                        End If
    '                        StartIndex = StartIndex + AcqAgentTemplateFields.FixLength
    '                    Next
    '                    dtVarified.Rows.Add(drVarified)
    '                Else
    '                    drUnVerified = dtUnVerified.NewRow()
    '                    drUnVerified("Data") = FileLine.ToString()
    '                    drUnVerified("Message") = ErrorMsg.ToString()
    '                    drUnVerified("LineNo") = LineNumber
    '                    dtUnVerified.Rows.Add(drUnVerified)
    '                End If
    '            Else
    '                drUnVerified = dtUnVerified.NewRow()
    '                drUnVerified("Data") = FileLine.ToString()
    '                drUnVerified("Message") = "Length problem."
    '                drUnVerified("LineNo") = LineNumber
    '                dtUnVerified.Rows.Add(drUnVerified)
    '            End If
    '            Session("dtVarified") = dtVarified
    '            Session("dtUnVerified") = dtUnVerified
    '        Else 'CSV File
    '            arrInsruction = FileLine.Split(FileTemplate.FieldDelimiter.ToString().Trim())
    '            If arrInsruction.Length = FRCAcqAgentTemplateFieldController.GetAgentTemplateFieldCountByAgentID(AgentID) Then
    '                ErrorMsg = CheckDataTXT(FileLine, collfrcAcqAgentFileTemplate, FileTemplate)
    '                If ErrorMsg = "" Then
    '                    drVarified = dtVarified.NewRow()
    '                    For Each AcqAgentTemplateFields In collfrcAcqAgentFileTemplate
    '                        If AcqAgentTemplateFields.FieldName.Trim.ToString() = "AmountPKR" Then
    '                            drVarified(CInt(AcqAgentTemplateFields.FieldOrder) - 1) = CDbl(arrInsruction(AcqAgentTemplateFields.FieldOrder - 1).ToString().Trim())
    '                        Else
    '                            drVarified(CInt(AcqAgentTemplateFields.FieldOrder) - 1) = arrInsruction(AcqAgentTemplateFields.FieldOrder - 1).ToString().Trim()
    '                        End If
    '                    Next
    '                    dtVarified.Rows.Add(drVarified)
    '                Else
    '                    drUnVerified = dtUnVerified.NewRow()
    '                    drUnVerified("Data") = FileLine.ToString()
    '                    drUnVerified("Message") = ErrorMsg.ToString()
    '                    drUnVerified("LineNo") = LineNumber
    '                    dtUnVerified.Rows.Add(drUnVerified)
    '                End If
    '            Else ' No of Columns Not match.
    '                drUnVerified = dtUnVerified.NewRow()
    '                drUnVerified("Data") = FileLine.ToString()
    '                drUnVerified("Message") = "No of Columns Not match."
    '                drUnVerified("LineNo") = LineNumber
    '                dtUnVerified.Rows.Add(drUnVerified)
    '            End If
    '            Session("dtVarified") = dtVarified
    '            Session("dtUnVerified") = dtUnVerified
    '        End If
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, Intelligenes.FRC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    'Private Function CheckDataTXT(ByVal FileLine As String, ByVal collfrcAcqAgentFileTemplate As FRCAcqAgentTemplateFieldCollection, ByVal FileTemplate As FRCFileUploadTemplate) As String
    '    Dim AcqAgentTemplateFields As New FRCAcqAgentTemplateField
    '    Dim Msg As String = ""
    '    Dim StartIndex As Integer = 0
    '    Dim result As Integer
    '    Dim arrInsruction As String()
    '    Dim strRIN As String()
    '    Dim cnt As Integer = 0

    '    Try
    '        If FileTemplate.IsFixLength = True Then
    '            For Each AcqAgentTemplateFields In collfrcAcqAgentFileTemplate
    '                If AcqAgentTemplateFields.IsRequired = True Then
    '                    If FileLine.Substring(StartIndex, AcqAgentTemplateFields.FixLength).ToString().Trim() = "" Then
    '                        Msg = Msg & " " & AcqAgentTemplateFields.FieldName.ToString() & " is empty."
    '                        Exit For
    '                    Else
    '                        If AcqAgentTemplateFields.FieldName.Trim.ToString() = "RIN" Then
    '                            If FRCInstructionController.CheckDuplicateRIN(FileLine.Substring(StartIndex, AcqAgentTemplateFields.FixLength).ToString().Trim()) = True Then
    '                                Msg = "Duplicate RIN."
    '                                FileRIN = FileRIN & "," & FileLine.Substring(StartIndex, AcqAgentTemplateFields.FixLength).ToString().Trim()
    '                                Exit For
    '                            Else
    '                                If FileRIN.ToString() <> "" Then
    '                                    strRIN = FileRIN.ToString.Split(",")
    '                                    For cnt = 0 To strRIN.Length - 1
    '                                        If strRIN(cnt) = FileLine.Substring(StartIndex, AcqAgentTemplateFields.FixLength).ToString().Trim() Then
    '                                            Msg = "Duplicate RIN."
    '                                            FileRIN = FileRIN & "," & FileLine.Substring(StartIndex, AcqAgentTemplateFields.FixLength).ToString().Trim()
    '                                            Exit For
    '                                        End If
    '                                    Next
    '                                End If
    '                                FileRIN = FileRIN & "," & FileLine.Substring(StartIndex, AcqAgentTemplateFields.FixLength).ToString().Trim()
    '                            End If
    '                        End If
    '                        If AcqAgentTemplateFields.FieldName.Trim.ToString() = "AmountPKR" Then
    '                            If IsNumeric(FileLine.Substring(StartIndex, AcqAgentTemplateFields.FixLength).ToString().Trim()) = True Then
    '                                If Double.TryParse(FileLine.Substring(StartIndex, AcqAgentTemplateFields.FixLength).ToString().Trim(), result) = False Then
    '                                    Msg = Msg & " " & AcqAgentTemplateFields.FieldName.ToString() & " is not in correct format."
    '                                    Exit For
    '                                End If
    '                            Else
    '                                Msg = Msg & " " & AcqAgentTemplateFields.FieldName.ToString() & " is not in correct format."
    '                                Exit For
    '                            End If

    '                        End If
    '                        If AcqAgentTemplateFields.FieldName.Trim.ToString() = "Amount" Then
    '                            If IsNumeric(FileLine.Substring(StartIndex, AcqAgentTemplateFields.FixLength).ToString().Trim()) = True Then
    '                                'If Double.TryParse(FileLine.Substring(StartIndex, AcqAgentTemplateFields.FixLength).ToString().Trim(), result) = False Then
    '                                '    Msg = Msg & " " & AcqAgentTemplateFields.FieldName.ToString() & " is not in correct format."
    '                                '    Exit For
    '                                'End If
    '                            Else
    '                                Msg = Msg & " " & AcqAgentTemplateFields.FieldName.ToString() & " is not in correct format."
    '                                Exit For
    '                            End If
    '                        End If
    '                        If AcqAgentTemplateFields.FieldName.Trim.ToString() = "BankCode" Then
    '                            If FRCBankController.IsBankExistByBankCode(FileLine.Substring(StartIndex, AcqAgentTemplateFields.FixLength).ToString().Trim()) = False Then
    '                                Msg = "Invalid Bank Code."
    '                                Exit For
    '                            End If
    '                        End If
    '                        If AcqAgentTemplateFields.FieldName.Trim.ToString() = "BankName" Then
    '                            If FRCBankController.IsBankExistByBankName(FileLine.Substring(StartIndex, AcqAgentTemplateFields.FixLength).ToString().Trim().ToLower()) = False Then
    '                                Msg = "Invalid Bank Name."
    '                                Exit For
    '                            End If
    '                        End If
    '                    End If
    '                End If
    '                StartIndex = StartIndex + AcqAgentTemplateFields.FixLength
    '            Next
    '        Else  'CSV File
    '            arrInsruction = FileLine.Split(FileTemplate.FieldDelimiter.ToString().Trim())
    '            For Each AcqAgentTemplateFields In collfrcAcqAgentFileTemplate
    '                If AcqAgentTemplateFields.IsRequired = True Then
    '                    If arrInsruction(AcqAgentTemplateFields.FieldOrder - 1).ToString().Trim() = "" Then
    '                        Msg = Msg & " " & AcqAgentTemplateFields.FieldName.ToString() & " is empty."
    '                        Exit For
    '                    Else
    '                        If AcqAgentTemplateFields.FieldName.ToString().Trim() = "RIN" Then
    '                            If FRCInstructionController.CheckDuplicateRIN(arrInsruction(AcqAgentTemplateFields.FieldOrder - 1).ToString().Trim()) = True Then
    '                                Msg = "Duplicate RIN."
    '                                FileRIN = FileRIN & "," & arrInsruction(AcqAgentTemplateFields.FieldOrder - 1).ToString().Trim()
    '                                Exit For
    '                            Else
    '                                If FileRIN.ToString() <> "" Then
    '                                    strRIN = FileRIN.ToString.Split(",")
    '                                    For cnt = 0 To strRIN.Length - 1
    '                                        If strRIN(cnt) = arrInsruction(AcqAgentTemplateFields.FieldOrder - 1).ToString().Trim() Then
    '                                            Msg = "Duplicate RIN."
    '                                            FileRIN = FileRIN & "," & arrInsruction(AcqAgentTemplateFields.FieldOrder - 1).ToString().Trim()
    '                                            Exit For
    '                                        End If
    '                                    Next
    '                                End If
    '                                FileRIN = FileRIN & "," & arrInsruction(AcqAgentTemplateFields.FieldOrder - 1).ToString().Trim()
    '                            End If
    '                        End If
    '                        If AcqAgentTemplateFields.FieldName.Trim.ToString() = "AmountPKR" Then
    '                            If Double.TryParse(arrInsruction(AcqAgentTemplateFields.FieldOrder - 1).ToString().Trim(), result) = False Then
    '                                Msg = Msg & " " & AcqAgentTemplateFields.FieldName.ToString() & " is not in correct format."
    '                                Exit For
    '                            End If
    '                            If IsNumeric(arrInsruction(AcqAgentTemplateFields.FieldOrder - 1).ToString().Trim()) = True Then
    '                                'If Double.TryParse(FileLine.Substring(StartIndex, AcqAgentTemplateFields.FixLength).ToString().Trim(), result) = False Then
    '                                '    Msg = Msg & " " & AcqAgentTemplateFields.FieldName.ToString() & " is not in correct format."
    '                                '    Exit For
    '                                'End If
    '                            Else
    '                                Msg = Msg & " " & AcqAgentTemplateFields.FieldName.ToString() & " is not in correct format."
    '                                Exit For
    '                            End If
    '                        End If
    '                        If AcqAgentTemplateFields.FieldName.Trim.ToString() = "Amount" Then
    '                            If IsNumeric(arrInsruction(AcqAgentTemplateFields.FieldOrder - 1).ToString().Trim()) = True Then
    '                                'If Double.TryParse(FileLine.Substring(StartIndex, AcqAgentTemplateFields.FixLength).ToString().Trim(), result) = False Then
    '                                '    Msg = Msg & " " & AcqAgentTemplateFields.FieldName.ToString() & " is not in correct format."
    '                                '    Exit For
    '                                'End If
    '                            Else
    '                                Msg = Msg & " " & AcqAgentTemplateFields.FieldName.ToString() & " is not in correct format."
    '                                Exit For
    '                            End If
    '                        End If
    '                        If AcqAgentTemplateFields.FieldName.Trim.ToString() = "BankCode" Then
    '                            If FRCBankController.IsBankExistByBankCode(arrInsruction(AcqAgentTemplateFields.FieldOrder - 1).ToString().Trim()) = False Then
    '                                Msg = "Invalid Bank Code."
    '                                Exit For
    '                            End If
    '                        End If
    '                        If AcqAgentTemplateFields.FieldName.Trim.ToString() = "BankName" Then
    '                            If FRCBankController.IsBankExistByBankName(arrInsruction(AcqAgentTemplateFields.FieldOrder - 1).ToString().Trim().ToLower()) = False Then
    '                                Msg = "Invalid Bank Name."
    '                                Exit For
    '                            End If
    '                        End If
    '                    End If
    '                End If
    '            Next
    '        End If

    '        Return Msg
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, Intelligenes.FRC.Dialogmessagetype.Failure)
    '    End Try
    'End Function
    'Private Sub GetDataFromEXCELFile(ByVal drExcel As DataRow, ByVal collfrcAcqAgentFileTemplate As FRCAcqAgentTemplateFieldCollection, ByVal AgentID As String, ByVal LineNumber As Integer, ByVal ColomnCount As Integer)
    '    Dim dtVarified, dtUnVerified As New DataTable
    '    Dim drVarified, drUnVerified As DataRow
    '    Dim AcqAgentTemplateFields As New FRCAcqAgentTemplateField
    '    Dim FileTemplate As New FRCFileUploadTemplate
    '    Dim ErrorMsg As String = ""
    '    Dim CountColumn As Integer = 0
    '    Dim LineData As String = ""
    '    Dim i As Integer = 0
    '    Try
    '        FileTemplate.LoadByPrimaryKey(collfrcAcqAgentFileTemplate(0).TemplateID.ToString())
    '        If Session("dtVarified") Is Nothing Then
    '            For Each AcqAgentTemplateFields In collfrcAcqAgentFileTemplate
    '                dtVarified.Columns.Add(New DataColumn(AcqAgentTemplateFields.FieldName.ToString(), GetType(System.String)))
    '            Next
    '            dtUnVerified.Columns.Add(New DataColumn("Data", GetType(System.String)))
    '            dtUnVerified.Columns.Add(New DataColumn("Message", GetType(System.String)))
    '            dtUnVerified.Columns.Add(New DataColumn("LineNo", GetType(System.String)))
    '        Else
    '            dtVarified = DirectCast(Session("dtVarified"), DataTable)
    '            dtUnVerified = DirectCast(Session("dtUnVerified"), DataTable)
    '        End If

    '        If ColomnCount = FRCAcqAgentTemplateFieldController.GetAgentTemplateFieldCountByAgentID(AgentID) Then
    '            ErrorMsg = CheckDataEXCEL(drExcel, collfrcAcqAgentFileTemplate, FileTemplate)
    '            If ErrorMsg = "" Then
    '                drVarified = dtVarified.NewRow()
    '                For Each AcqAgentTemplateFields In collfrcAcqAgentFileTemplate
    '                    If AcqAgentTemplateFields.FieldName.ToString().Trim() = "AmountPKR" Then
    '                        drVarified(CInt(AcqAgentTemplateFields.FieldOrder) - 1) = CDbl(drExcel(CInt(AcqAgentTemplateFields.FieldOrder) - 1))
    '                    Else
    '                        drVarified(CInt(AcqAgentTemplateFields.FieldOrder) - 1) = drExcel(CInt(AcqAgentTemplateFields.FieldOrder) - 1).ToString().Trim()
    '                    End If
    '                Next
    '                dtVarified.Rows.Add(drVarified)
    '            Else

    '                For i = 0 To ColomnCount - 1
    '                    If i = 0 Then
    '                        LineData = drExcel(i).ToString()
    '                    Else
    '                        LineData = LineData & " , " & drExcel(i).ToString()
    '                    End If

    '                Next

    '                drUnVerified = dtUnVerified.NewRow()
    '                drUnVerified("Data") = LineData.ToString()
    '                drUnVerified("Message") = ErrorMsg.ToString()
    '                drUnVerified("LineNo") = LineNumber
    '                dtUnVerified.Rows.Add(drUnVerified)
    '            End If
    '        Else ' No of Columns Not match.
    '            For i = 0 To ColomnCount - 1
    '                If i = 0 Then
    '                    LineData = drExcel(i).ToString()
    '                Else
    '                    LineData = LineData & " , " & drExcel(i).ToString()
    '                End If
    '            Next
    '            drUnVerified = dtUnVerified.NewRow()
    '            drUnVerified("Data") = LineData.ToString()
    '            drUnVerified("Message") = "No of Columns Not match."
    '            drUnVerified("LineNo") = LineNumber
    '            dtUnVerified.Rows.Add(drUnVerified)
    '        End If
    '        Session("dtVarified") = dtVarified
    '        Session("dtUnVerified") = dtUnVerified
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, Intelligenes.FRC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    'Private Function CheckDataEXCEL(ByVal drExcel As DataRow, ByVal collfrcAcqAgentFileTemplate As FRCAcqAgentTemplateFieldCollection, ByVal FileTemplate As FRCFileUploadTemplate) As String
    '    Dim AcqAgentTemplateFields As New FRCAcqAgentTemplateField
    '    Dim Msg As String = ""
    '    Dim StartIndex As Integer = 0
    '    Dim result As Integer
    '    Dim arrInsruction As String()
    '    Dim srtRIN As String()
    '    Dim cnt As Integer = 0
    '    Try
    '        For Each AcqAgentTemplateFields In collfrcAcqAgentFileTemplate
    '            If AcqAgentTemplateFields.IsRequired = True Then
    '                If drExcel(CInt(AcqAgentTemplateFields.FieldOrder) - 1).ToString().Trim() = "" Then
    '                    Msg = Msg & " " & AcqAgentTemplateFields.FieldName.ToString().Trim() & " is empty."
    '                    Exit For
    '                Else
    '                    If AcqAgentTemplateFields.FieldName.Trim.ToString() = "RIN" Then
    '                        If FRCInstructionController.CheckDuplicateRIN(drExcel(CInt(AcqAgentTemplateFields.FieldOrder) - 1).ToString().Trim()) = True Then
    '                            Msg = "Duplicate RIN."
    '                            FileRIN = FileRIN & "," & drExcel(CInt(AcqAgentTemplateFields.FieldOrder) - 1).ToString().Trim()
    '                            Exit For
    '                        Else
    '                            If FileRIN.ToString() <> "" Then
    '                                srtRIN = FileRIN.ToString.Split(",")
    '                                For cnt = 0 To srtRIN.Length - 1
    '                                    If srtRIN(cnt) = drExcel(CInt(AcqAgentTemplateFields.FieldOrder) - 1).ToString().Trim() Then
    '                                        Msg = "Duplicate RIN."
    '                                        FileRIN = FileRIN & "," & drExcel(CInt(AcqAgentTemplateFields.FieldOrder) - 1).ToString().Trim()
    '                                        Exit For
    '                                    End If
    '                                Next
    '                            End If
    '                            FileRIN = FileRIN & "," & drExcel(CInt(AcqAgentTemplateFields.FieldOrder) - 1).ToString().Trim()
    '                        End If
    '                    End If
    '                    If AcqAgentTemplateFields.FieldName.Trim.ToString() = "AmountPKR" Then
    '                        If Double.TryParse(drExcel(CInt(AcqAgentTemplateFields.FieldOrder) - 1).ToString().Trim(), result) = False Then
    '                            Msg = Msg & " " & AcqAgentTemplateFields.FieldName.ToString() & " is not in correct format."
    '                            Exit For
    '                        End If
    '                        If IsNumeric(drExcel(CInt(AcqAgentTemplateFields.FieldOrder) - 1).ToString().Trim()) = True Then
    '                            'If Double.TryParse(FileLine.Substring(StartIndex, AcqAgentTemplateFields.FixLength).ToString().Trim(), result) = False Then
    '                            '    Msg = Msg & " " & AcqAgentTemplateFields.FieldName.ToString() & " is not in correct format."
    '                            '    Exit For
    '                            'End If
    '                        Else
    '                            Msg = Msg & " " & AcqAgentTemplateFields.FieldName.ToString() & " is not in correct format."
    '                            Exit For
    '                        End If
    '                    End If
    '                    If AcqAgentTemplateFields.FieldName.Trim.ToString() = "Amount" Then
    '                        If IsNumeric(drExcel(CInt(AcqAgentTemplateFields.FieldOrder) - 1).ToString().Trim()) = True Then
    '                            'If Double.TryParse(FileLine.Substring(StartIndex, AcqAgentTemplateFields.FixLength).ToString().Trim(), result) = False Then
    '                            '    Msg = Msg & " " & AcqAgentTemplateFields.FieldName.ToString() & " is not in correct format."
    '                            '    Exit For
    '                            'End If
    '                        Else
    '                            Msg = Msg & " " & AcqAgentTemplateFields.FieldName.ToString() & " is not in correct format."
    '                            Exit For
    '                        End If
    '                    End If
    '                    If AcqAgentTemplateFields.FieldName.Trim.ToString() = "BankCode" Then
    '                        If FRCBankController.IsBankExistByBankCode(drExcel(CInt(AcqAgentTemplateFields.FieldOrder) - 1).ToString().Trim()) = False Then
    '                            Msg = "Invalid Bank Code."
    '                            Exit For
    '                        End If
    '                    End If
    '                    If AcqAgentTemplateFields.FieldName.Trim.ToString() = "BankName" Then
    '                        If FRCBankController.IsBankExistByBankName(drExcel(CInt(AcqAgentTemplateFields.FieldOrder) - 1).ToString().Trim().ToLower()) = False Then
    '                            Msg = "Invalid Bank Name."
    '                            Exit For
    '                        End If
    '                    End If
    '                End If
    '            End If
    '        Next
    '        Return Msg
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, Intelligenes.FRC.Dialogmessagetype.Failure)
    '    End Try
    'End Function
    'Private Sub SaveVerifiedInstructions(ByVal dtVerified As DataTable, ByVal AgentID As String, ByVal FileID As String, ByVal UserID As String)
    '    Dim drVer As DataRow
    '    Dim dcVer As DataColumn
    '    Dim frcInst As FRCInstruction
    '    Dim cBene As FRCBeneficiary
    '    Dim cRemitter As FRCRemitter
    '    Dim AcqAgent As New FRCAcqAgent
    '    Try
    '        If dtVerified.Rows.Count > 0 Then
    '            For Each drVer In dtVerified.Rows
    '                AcqAgent.LoadByPrimaryKey(AgentID)
    '                frcInst = New FRCInstruction
    '                cBene = New FRCBeneficiary
    '                cRemitter = New FRCRemitter
    '                For Each dcVer In dtVerified.Columns
    '                    Select Case dcVer.ColumnName.Trim.ToString()
    '                        'RIN 
    '                        Case "RIN"
    '                            frcInst.Rin = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            'RemittCode
    '                        Case "RemittCode"
    '                            frcInst.RemittCode = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'RemitterCode 
    '                        Case "RemitterCode"
    '                            frcInst.RemitterCode = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'RemitterName 
    '                        Case "RemitterName"
    '                            frcInst.RemitterName = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cRemitter.RemitterName = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'RemitterCell 
    '                        Case "RemitterCell"
    '                            frcInst.RemitterCell = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cRemitter.RemitterCell = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'RemitterEmail 
    '                        Case "RemitterEmail"
    '                            frcInst.RemitterEmail = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cRemitter.RemitterEmail = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            'RemitterAddress1 
    '                        Case "RemitterAddress1"
    '                            frcInst.RemitterAddress1 = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cRemitter.RemitterAddress1 = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'RemitterAddress2 
    '                        Case "RemitterAddress2"
    '                            frcInst.RemitterAddress2 = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cRemitter.RemitterAddress2 = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'RemitterPostalCode 
    '                        Case "RemitterPostalCode"
    '                            frcInst.RemitterPostalCode = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cRemitter.RemitterPostalCode = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'RemitterCity
    '                        Case "RemitterCity"
    '                            frcInst.RemitterCity = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cRemitter.RemitterCity = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'RemitterProvince 
    '                        Case "RemitterProvince"
    '                            frcInst.RemitterProvince = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cRemitter.RemitterProvince = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'RemitterMessage 
    '                        Case "RemitterMessage"
    '                            frcInst.RemitterMessage = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'RemittType 
    '                        Case "RemittType"
    '                            frcInst.RemittType = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'RemittanceCurrencyCode 
    '                        Case "RemittanceCurrencyCode"
    '                            frcInst.RemittanceCurrencyCode = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'Amount 
    '                        Case "Amount"
    '                            frcInst.Amount = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'AmountPKR 
    '                        Case "AmountPKR"
    '                            frcInst.AmountPKR = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            frcInst.AmountWord = NumToWord.SpellNumber(CDbl(drVer(dcVer.ColumnName.ToString()).ToString())).TrimStart().TrimEnd().Trim().ToString()
    '                            'AmountWord 
    '                        Case "AmountWord"
    '                            frcInst.AmountWord = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'RemittCharges 
    '                        Case "RemittCharges"
    '                            frcInst.RemittCharges = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'RemittPurpose
    '                        Case "RemittPurpose"
    '                            frcInst.RemittPurpose = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'BeneIDNO 
    '                        Case "BeneIDNO"
    '                            frcInst.BeneIDNO = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cBene.BeneIDNO = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'BeneCode 
    '                        Case "BeneCode"
    '                            frcInst.BeneCode = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'BeneName 
    '                        Case "BeneName"
    '                            frcInst.BeneName = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cBene.BeneName = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'BeneCellNo 
    '                        Case "BeneCellNo"
    '                            frcInst.BeneCellNo = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cBene.BeneCellNumber = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'BeneAddress1 
    '                        Case "BeneAddress1"
    '                            frcInst.BeneAddress1 = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cBene.BeneAddress1 = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'BeneAddress2 
    '                        Case "BeneAddress2"
    '                            frcInst.BeneAddress2 = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cBene.BeneAddress2 = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'BeneAddress3 
    '                        Case "BeneAddress3"
    '                            frcInst.BeneAddress3 = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cBene.BeneAddress3 = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'BeneCity 
    '                        Case "BeneCity"
    '                            frcInst.BeneCity = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cBene.BeneCity = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'BenePostalCode 
    '                        Case "BenePostalCode"
    '                            frcInst.BenePostalCode = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cBene.BenePostalCode = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'BeneProvince 
    '                        Case "BeneProvince"
    '                            frcInst.BeneProvince = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cBene.BeneProvince = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'BeneEmail 
    '                        Case "BeneEmail"
    '                            frcInst.BeneEmail = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cBene.BeneEmail = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'BankCode 
    '                        Case "BankCode"
    '                            frcInst.BankCode = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'BankName 
    '                        Case "BankName"
    '                            frcInst.BankName = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'BankAccountNumber 
    '                        Case "BankAccountNumber"
    '                            frcInst.BankAccountNumber = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cBene.BeneAccountNumber = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'BankType 
    '                        Case "BankType"
    '                            frcInst.BankType = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'BranchCode 
    '                        Case "BranchCode"
    '                            frcInst.BranchCode = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'BranchName 
    '                        Case "BranchName"
    '                            frcInst.BranchName = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'BranchCity 
    '                        Case "BranchCity"
    '                            frcInst.BranchCity = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'DisbTYpe 
    '                        Case "DisbTYpe"
    '                            frcInst.DisbTYpe = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'Description 
    '                        Case "Description"
    '                            frcInst.Description = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            cBene.Description = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            'TransferType
    '                        Case "TransferType"
    '                            frcInst.TransferType = drVer(dcVer.ColumnName.ToString()).ToString()

    '                        Case "Remarks"
    '                            frcInst.Remarks = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'PICKUPLOCATION 
    '                        Case "PICKUPLOCATION"
    '                            frcInst.Pickuplocation = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'PICKUPLOCATIONNAME 
    '                        Case "PICKUPLOCATIONNAME"
    '                            frcInst.Pickuplocationname = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'PICKUPLOCATIONADDRESS1 
    '                        Case "PICKUPLOCATIONADDRESS1"
    '                            frcInst.Pickuplocationaddress1 = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'PICKUPLOCATIONADDRESS2 
    '                        Case "PICKUPLOCATIONADDRESS2"
    '                            frcInst.Pickuplocationaddress2 = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'PICKLOCATIONPHONE 
    '                        Case "PICKLOCATIONPHONE"
    '                            frcInst.Picklocationphone = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'PICKUPLOCATIONCITY 
    '                        Case "PICKUPLOCATIONCITY"
    '                            frcInst.Pickuplocationcity = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'SerialNumber 
    '                        Case "SerialNumber"
    '                            frcInst.SerialNumber = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            ''    'RemittDate 
    '                            ''Case "RemittDate"
    '                            ''    frcInst.RemittDate = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'ExchangeRate 
    '                        Case "ExchangeRate"
    '                            'frcInst.ExchangeRate = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            ''    'RemittTime 
    '                            ''Case "RemittTime"
    '                            ''    frcInst.RemittTime = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'LocalCurrencyCode 
    '                        Case "LocalCurrencyCode"
    '                            frcInst.LocalCurrencyCode = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'isVerified 
    '                            'isAuthenticated 
    '                            'RemittRefNo 
    '                        Case "RemittRefNo"
    '                            frcInst.RemittRefNo = drVer(dcVer.ColumnName.ToString()).ToString()

    '                            'BankAccountTitle 
    '                        Case "BankAccountTitle"
    '                            frcInst.BankAccountTitle = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            'BeneBankBranchCode 
    '                        Case "BeneBankBranchCode"
    '                            frcInst.BeneBankBranchCode = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            'BeneBankAddressLine1 
    '                        Case "BeneBankAddressLine1"
    '                            frcInst.BeneBankAddressLine1 = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            'BeneBankAddressLine2 
    '                        Case "BeneBankAddressLine2"
    '                            frcInst.BeneBankAddressLine2 = drVer(dcVer.ColumnName.ToString()).ToString()
    '                            'BeneBankAddressLine3 
    '                        Case "BeneBankAddressLine3"
    '                            frcInst.BeneBankAddressLine3 = drVer(dcVer.ColumnName.ToString()).ToString()
    '                    End Select
    '                Next
    '                If AcqAgent.RINType = "RINApplication" Then
    '                    frcInst.Rin = GenerateRIN()

    '                    If frcInst.Rin = "" Or frcInst.Rin Is Nothing Then
    '                        frcInst.Rin = GenerateRIN()
    '                    End If
    '                    If frcInst.Rin = "" Or frcInst.Rin Is Nothing Then
    '                        frcInst.Rin = GenerateRIN()
    '                    End If
    '                    If frcInst.Rin = "" Or frcInst.Rin Is Nothing Then
    '                        frcInst.Rin = GenerateRIN()
    '                    End If
    '                End If
    '                'If AcqAgent.IsSundryAllow = True Then
    '                frcInst.Status = "27"  'Pending File Processing
    '                'Else
    '                ' frcInst.Status = "23"   'Pending Processing
    '                'End If



    '                cBene.CreateBy = UserID.ToString()
    '                cBene.CreateDate = Date.Now
    '                cBene.IsActive = True
    '                cBene.AcqAgentID = AcqAgent.Code

    '                cRemitter.CreateBy = UserID.ToString()
    '                cRemitter.CreateDate = Date.Now
    '                cRemitter.IsActive = True
    '                cRemitter.AcqAgentID = AcqAgent.Code


    '                frcInst.AgentID = AcqAgent.Code
    '                frcInst.FileID = FileID.ToString()
    '                frcInst.IsActive = True
    '                frcInst.CreateBy = UserID.ToString()
    '                frcInst.CreateDate = Date.Now
    '                frcInst.AcqMode = "Email File Upload"

    '                If frcInst.Rin = "" Or frcInst.Rin Is Nothing Then
    '                    frcInst.Rin = GenerateRIN()
    '                End If

    '                FRCInstructionController.AddTransaction(cBene, cRemitter, frcInst, Me.UserId, Me.UserInfo.Username)
    '            Next
    '        End If
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, Intelligenes.FRC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    'Private Function GenerateRIN() As String
    '    Dim RIN As String = ""
    '    Dim fiveRandom As New Random()
    '    Dim tsFive As New TimeSpan()
    '    Try
    '        tsFive = DateTime.Now.Subtract(Convert.ToDateTime("01/01/1947"))
    '        RIN = fiveRandom.Next(10000, 99999) & tsFive.Days.ToString() & DateTime.Now.Hour.ToString("00") & DateTime.Now.Minute.ToString("00") & DateTime.Now.Second.ToString("00")
    '        If RIN = "" Then
    '            GenerateRIN()
    '        End If
    '        If FRCInstructionController.CheckDuplicateRIN(RIN) = True Then
    '            GenerateRIN()
    '        Else
    '            If RIN = "" Then
    '                GenerateRIN()
    '            End If
    '            Return RIN
    '        End If
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, Intelligenes.FRC.Dialogmessagetype.Failure)
    '        Return Nothing
    '    End Try
    'End Function
    Protected Sub btnCaptcha_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCaptcha.Click
        'Response.Redirect(NavigateURL("Captcha", "&mid=" & Me.ModuleId & ""), False)
    End Sub

    Protected Sub btnGetStatement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetStatement.Click
        Try
            'Dim dt As New DataTable
            'dt = CBUtilities.GetAccountStatement(txtAccountNumber.Text.ToString(), txtBranchCode.Text.ToString(), txtCurrency.Text.ToString(), CDate(txtFromDate.Text.ToString()), CDate(txtToDate.Text.ToString()), "Aizaz Ahmed")

            'gvSOA.DataSource = dt
            'gvSOA.DataBind()

        Catch ex As Exception
            txtResult.Text = ex.Message
        End Try
    End Sub

    Protected Sub btnIBFTTitleFetch_Click(sender As Object, e As System.EventArgs) Handles btnIBFTTitleFetch.Click
        Try
            'Dim cid As String = ICUtilities.GetCustomerIDCountFromICSettings
            txtResult.Text = CBUtilities.IBFTTitleFetch("601373", "0002000001100111", CBUtilities.AccountType.DefaultAccount, "Bene Account Title", "0002000001100111", "", "", 10, "", "", "", "52", "52", "0101PK51AIIN0000110122397019", "627688")
        Catch ex As Exception
            'If ex.Message.Contains("IRIS") Then
            txtResult.Text = ex.Message
            'End If
        End Try
    End Sub

    Protected Sub btnGetPassword_Click(sender As Object, e As EventArgs) Handles btnGetPassword.Click
        Try

            Dim objFromICUser As New ICUser

            objFromICUser.Query.Where(objFromICUser.Query.UserName = txtUserID.Text.ToString.Trim)
            objFromICUser.Query.Load()
            If objFromICUser.Query.Load = True Then
                Dim user As New UserInfo
                user = UserController.GetUserById(Me.PortalId, objFromICUser.UserID)
                Dim password As String = ""
                Dim password2 As String = ""
                password2 = DotNetNuke.Entities.Users.UserController.GetPassword(user, password)
                UIUtilities.ShowDialog(Me, "Get Password", password2, Dialogmessagetype.Success)
                Exit Sub

            Else
                UIUtilities.ShowDialog(Me, "Get Password", "Invalid user id", Dialogmessagetype.Failure)
                Exit Sub
            End If
        Catch ex As Exception
            UIUtilities.ShowDialog(Me, "Get Password", "Invalid user id", Dialogmessagetype.Failure)
            Exit Sub
        End Try
    End Sub

    Protected Sub btnGetPassword0_Click(sender As Object, e As EventArgs) Handles btnAccountStatus.Click
        Try
            txtResult.Text = CBUtilities.AccountStatusForClearing(txtAccountNo.Text.ToString, CBUtilities.AccountType.RB, "Test Account Status", txtAccountNo.Text.ToString, "", "")
        Catch ex As Exception
            txtResult.Text = ex.Message.ToString
        End Try
    End Sub

    Protected Sub btnBillInquiry_Click(sender As Object, e As EventArgs) Handles btnBillInquiry.Click
        Try
            Dim dt As New DataTable
            dt = CBUtilities.BillInquiry("Instruction", "1", "", "3", "1700416100042", "N", "0", "", "0101", "0110122397019")
            gvSOA.DataSource = dt
            gvSOA.DataBind()
            gvSOA.Visible = True
        Catch ex As Exception
            txtResult.Text = ex.Message.ToString
        End Try
    End Sub

    Protected Sub txtDecript_Click(sender As Object, e As EventArgs) Handles txtEncript.Click
        Try
            txtMSG.Text = EncryptDecryptUtilities.Encrypt(txtTextToEncript.Text)
        Catch ex As Exception
            txtResult.Text = ex.Message.ToString
        End Try
    End Sub

   
   
    Protected Sub txtEncript0_Click(sender As Object, e As EventArgs) Handles txtEncript0.Click
        Try
            'Dim con As New System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("SiteSqlServer").ConnectionString)
            'con.Open()
            ''System.Data.DataTable tbl = con.GetSchema("Databases");
            ''ExportExcel(tbl);

            'Dim sb As New System.Text.StringBuilder()
            'sb.Append(" <TABLE BORDER=1>")
            'sb.Append("<tr><td colspan='6' align='center'>")
            'sb.Append("<H3>Table Design for: -" + con.Database.ToString() + "</H3>")

            'sb.Append("</td></tr>")

            'sb.Append("<tr><td colspan='6' align='center'> </td></tr>")
            'con.Close()
            'Dim tblTables As System.Data.DataTable = Nothing

            'con.Open()
            'tblTables = con.GetSchema(System.Data.SqlClient.SqlClientMetaDataCollectionNames.Tables, New String() {Nothing, Nothing, Nothing, "BASE TABLE"})
            'Dim columnsTable As DataTable = Nothing
            'For Each rowDatabase As System.Data.DataRow In tblTables.Rows
            '    columnsTable = con.GetSchema(SqlClientMetaDataCollectionNames.Columns, New String() {Nothing, Nothing, rowDatabase("TABLE_NAME").ToString()})
            '    sb.Append("<tr><td colspan='5' align='center'>")
            '    sb.Append("<b>Table Description</b></td>")
            '    sb.Append("<td align='center'>")
            '    sb.Append("<b>Table Name</b></td></tr>")
            '    sb.Append("<tr><td colspan='5' align='center' >" + rowDatabase("TABLE_NAME").ToString() + "</td><td colspan='1' align='center'>" + rowDatabase("TABLE_NAME").ToString() + "</td></tr>")
            '    sb.Append("<tr><td><b> SL No</b></td> <td><b> Field Name</b></td> <td><b> Data Type</b></td> <td><b> Size</b></td> <td><b> Constraint </b></td><td><b> Explanation </b></td></tr>")
            '    Dim i As Integer = 1
            '    For Each columnsRows As System.Data.DataRow In columnsTable.Rows

            '        sb.Append("<tr><td>" & i & "</td> <td>" & columnsRows("COLUMN_NAME").ToString() & "</td> <td>" & columnsRows("DATA_TYPE").ToString() & "</td> <td>" & columnsRows("CHARACTER_MAXIMUM_LENGTH").ToString() & "</td> <td></td><td></td></tr>")
            '        i += 1
            '    Next
            '    sb.Append("<tr><td colspan=6></td></tr>")
            'Next

            'con.Close()
            'sb.Append(" </TABLE>")

            'Response.Clear()
            'Response.ClearContent()
            'Response.ClearHeaders()
            'Response.ContentType = "application/vnd.ms-excel"
            'Response.AddHeader("Content-Disposition", "attachment; filename=Reports.xls")
            'Response.Write(sb)
            'Response.[End]()
            txtMSG.Text = EncryptDecryptUtilities.Decrypt(txtTextToEncript.Text)
        Catch ex As Exception
            txtResult.Text = ex.Message.ToString
        End Try
    End Sub

    Protected Sub txtEncript1_Click(sender As Object, e As EventArgs) Handles txtEncript1.Click
        Try
            Dim result As Integer = Date.Compare(Date.Now.ToString("HH:mm:ss"), CDate("18:00:00"))
            UIUtilities.ShowDialog(Me, "Date", Date.Now.ToString("HH:mm:ss").ToString & result.ToString, Dialogmessagetype.Success)
        Catch ex As Exception
            UIUtilities.ShowDialog(Me, "Date", ex.Message.ToString, Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub txtEncript2_Click(sender As Object, e As EventArgs) Handles txtEncript2.Click
        Try
            SendDailyAccountBalanceVIAEmailToTaggedUsers("0000000000", "12345", "0001", "PKR")
        Catch ex As Exception
            UIUtilities.ShowDialog(Me, "Date", ex.Message.ToString, Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SendDailyAccountBalanceVIAEmailToTaggedUsers(ByVal AccountNo As String, ByVal Balance As String, ByVal BranchCode As String, ByVal Currency As String)
        Try


            Dim dtUserForEmail As New DataTable


            ''Function to get tagged users
            dtUserForEmail = ICNotificationManagementController.GetAllTaggedClientUserForAccountBalance("71", True, AccountNo, BranchCode, Currency)
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates/TaggedClientUserForAccountBalance.htm")
            Dim txt As String
            Dim txtMsg As String = ""
            Dim dr As DataRow
            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            txtMsg = txt
            'For Each dr In dtUserForEmail.Rows
            txtMsg = ""
            txtMsg = txt

            'txtMsg = txtMsg.Replace("[UserName]", dr("DisplayName").ToString())
            txtMsg = txtMsg.Replace("[Account No]", AccountNo)
            txtMsg = txtMsg.Replace("[Balance]", CDbl(Balance).ToString("N2"))
            txtMsg = txtMsg.Replace("[AppLink]", ICUtilities.GetSettingValue("ApplicationURL"))
            txtMsg = txtMsg.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

            Dim subject As String
            Dim emailto As String

            emailto = dr("Email").ToString()
            subject = "Daily Account Balance"

            ICUtilities.SendEmail(emailto, subject, txtMsg)
            'Next

        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Exit Sub
        End Try
    End Sub
    

    
End Class
