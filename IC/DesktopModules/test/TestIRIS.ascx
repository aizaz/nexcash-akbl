﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="TestIRIS.ascx.vb" Inherits="DesktopModules_test_TestIRIS" %>

<style type="text/css">
    .style1 {
        width: 100%;
    }
</style>

<asp:Button ID="btnBalanceInquiry" runat="server" Text="Get balance" />
&nbsp;<asp:Button ID="btnFundsTransferDirect" runat="server" Text="Direct Transfer" />
&nbsp;<asp:Button ID="btnFundsTransferOther" runat="server" Text="Other Transfer" />
&nbsp;<asp:Button ID="btnTitleFetch" runat="server" Text="Title Fetch" />
&nbsp;<asp:Button ID="btnSend" runat="server" Text="Send Message" />
&nbsp;<asp:Button ID="btnSendEmail" runat="server" Text="Send Email" />
&nbsp;<asp:Button ID="btnEmailCheck" runat="server" Text="Get Agent Email" />
&nbsp;<asp:Button ID="btnEmailProcess" runat="server" Text="Email Files Process" />
&nbsp;<asp:Button ID="btnClear" runat="server" Text="Clear" />
&nbsp;<asp:Button ID="btnCaptcha" runat="server" Text="Test Captcha" />
&nbsp;<asp:Button ID="btnIBFTTitleFetch" runat="server" 
    Text="IBFT Title Fetch" />
<asp:Button ID="btnGetPassword" runat="server" 
    Text="Get Password" />
<asp:Button ID="btnAccountStatus" runat="server" 
    Text="Account Status" />
<asp:Button ID="btnBillInquiry" runat="server" 
    Text="Bill Inquiry" />
<asp:Button ID="txtEncript" runat="server" Text="Encript" />
<asp:Button ID="txtEncript0" runat="server" Text="Decrypt" />
<asp:Button ID="txtEncript1" runat="server" Text="Get Date" />
<asp:Button ID="txtEncript2" runat="server" Text="Send Balance Email" />
<p>
    &nbsp;<asp:Label ID="lblAccountNo" runat="server" 
        Text="Balance / Title Fetch Account No."></asp:Label>
</p>
<p>
    <asp:TextBox ID="txtAccountNo" runat="server" Width="596px"></asp:TextBox>
</p>
<p>
    &nbsp;<asp:Label ID="lblFromAccountNumber" runat="server" Text="From Account No."></asp:Label>
</p>
<p>
    <asp:TextBox ID="txtFromAccount" runat="server" Width="596px"></asp:TextBox>
</p>
<p>
    &nbsp;<asp:Label ID="lblFromAccountBranchCode" runat="server" Text="From Account Branch Code"></asp:Label>
</p>
<p>
    <asp:TextBox ID="txtFromAccountBranchCode" runat="server" Width="596px"></asp:TextBox>
</p>
<p>
    &nbsp;<asp:Label ID="lblToAccount" runat="server" Text="To Account No."></asp:Label>
</p>
<p>
    <asp:TextBox ID="txtToAccount" runat="server" Width="596px"></asp:TextBox>
</p>
<p>
    &nbsp;<asp:Label ID="lblToAccountBranchCode" runat="server" Text="To Account Branch Code"></asp:Label>
</p>
<p>
    <asp:TextBox ID="txtToAccountBranchCode" runat="server" Width="596px"></asp:TextBox>
</p>
<p>
    <asp:Label ID="lblFromBankIMD" runat="server" Text="From BankIMD"></asp:Label>
</p>
<p>
    <asp:TextBox ID="txtFromIMD" runat="server" Width="596px"></asp:TextBox>
</p>
<p>
    <asp:Label ID="lblToBankIMD" runat="server" Text="To BankIMD"></asp:Label>
</p>
<p>
    <asp:TextBox ID="txtToBankIMD" runat="server" Width="596px"></asp:TextBox>
</p>
<p>
    <asp:Label ID="Label1" runat="server" Text="Mobile No"></asp:Label>
</p>
<p>
    <asp:TextBox ID="txtNumber" runat="server" Width="596px"></asp:TextBox>
</p>
<p>
    <asp:Label ID="lblUserID" runat="server" Text="User ID"></asp:Label>
</p>
<p>
    <asp:TextBox ID="txtUserID" runat="server" Width="596px"></asp:TextBox>
</p>
<p>
    <asp:Label ID="Label2" runat="server" Text="Message"></asp:Label>
</p>
<p>
    <asp:TextBox ID="txtMSG" runat="server" Width="596px" TextMode="MultiLine"></asp:TextBox>
</p>
<p>
    <asp:Label ID="lblEncriptText" runat="server" Text="Text to Encript"></asp:Label>
</p>
<p>
    <asp:TextBox ID="txtTextToEncript" runat="server" Width="596px" TextMode="SingleLine"></asp:TextBox>
</p>
<asp:TextBox ID="txtResult" runat="server" Width="596px"></asp:TextBox>
<p>
    <asp:TextBox ID="txtValid" runat="server" Width="596px" ValidationGroup="change"></asp:TextBox>
    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtValid"
        Display="Dynamic" ErrorMessage="Must have at least 1 number, 1 special character, 1 upper case letter, 1 lower case letter,
        and more than 6 characters." ValidationExpression="(?=^.{6,}$)((?=.*\d)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
        ValidationGroup="change"></asp:RegularExpressionValidator>
</p>
    <asp:Label ID="lblAccountNo0" runat="server" Text="Amount"></asp:Label>
<br />
    <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
<br />
<br />
<br />
<br />
<asp:Button ID="Button1" runat="server" Text="Button" />

<p>
    &nbsp&nbsp;</p>

<table align="left" class="style1">
    <tr>
        <td>
    <asp:Label ID="lblAccountNo1" runat="server" Text="Account Number"></asp:Label>
        </td>
        <td>
    <asp:TextBox ID="txtAccountNumber" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
    <asp:Label ID="lblAccountNo2" runat="server" Text="Branch Code"></asp:Label>
        </td>
        <td>
    <asp:TextBox ID="txtBranchCode" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
    <asp:Label ID="lblAccountNo3" runat="server" Text="Currency"></asp:Label>
        </td>
        <td>
    <asp:TextBox ID="txtCurrency" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
    <asp:Label ID="lblAccountNo4" runat="server" Text="From Date"></asp:Label>
        </td>
        <td>
    <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
    <asp:Label ID="lblAccountNo5" runat="server" Text="To Date"></asp:Label>
        </td>
        <td>
    <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            <asp:Button ID="btnGetStatement" runat="server" Text="Account Statement" />
        </td>
    </tr>
     <tr>
        <td colspan="2">
           <telerik:RadGrid ID="gvSOA" runat="server" AllowPaging="True" Width="100%"
                            AllowSorting="True" CellSpacing="0" ShowFooter="True"
                            PageSize="10">
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView>
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                               
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                        </telerik:RadGrid> 
           </td>
       
    </tr>
</table>
 

