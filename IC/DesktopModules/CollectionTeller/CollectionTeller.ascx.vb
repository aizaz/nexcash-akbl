﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_CollectionTeller_CollectionTeller
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
    Private AssignUserRolsID As New ArrayList


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()
            If Page.IsPostBack = False Then
                'btnPay.Attributes.Item("onclick") = "if (Page_ClientValidate('Update')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnPay, Nothing).ToString() & " } "
                If Me.UserInfo.IsSuperUser = False Then
                    Dim objICUser As New ICUser
                    objICUser.LoadByPrimaryKey(Me.UserInfo.UserID.ToString)
                    If objICUser.UserType = "Client User" Then
                        UIUtilities.ShowDialog(Me, "Collection Teller", "Sorry! You are not authorized to login.", Dialogmessagetype.Failure, NavigateURL(41))
                        Exit Sub
                    ElseIf objICUser.UserType = "Bank User" And objICUser.UpToICOfficeByOfficeCode.IsApprove = False Then
                        UIUtilities.ShowDialog(Me, "Collection Teller", "Sorry! You branch is not approved", Dialogmessagetype.Failure, NavigateURL(41))
                        Exit Sub
                    End If
                    ViewState("htRights") = Nothing
                    GetPageAccessRights()
                    LoadddlCompany()
                    LoadddlCollectionNature()
                    tblAmountAndCompanyDetails.Style.Add("display", "none")
                    tblMain.Style.Remove("display")
                    tblReferenceField.Style.Add("display", "none")
                    tblResult.Style.Add("display", "none")
                    tblFundsTransfer.Style.Add("display", "none")
                    tblInstrumentDetails.Style.Add("display", "none")
                    tblPreview.Style.Add("display", "none")
                    rimFundsTransfer.Enabled = False
                    rimInstrumentDetails.Enabled = False
                    radFromBranch.Enabled = False
                    tblFromBranch.Style.Add("display", "none")
                    ClearFundsTransferDetails()
                    rfvddlBank.Enabled = False
                    ClearPreviewLabels()
                    rfvPrintedFrom.Enabled = False
                    rfvddlDrawnOnBranch.Enabled = False
                Else
                    UIUtilities.ShowDialog(Me, "Collection Teller", "Sorry! You are not authorized to login.", Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        AssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Collections Teller") = True Then
                    If Not AssignUserRolsID.Contains(RoleID.ToString) Then
                        AssignUserRolsID.Add(RoleID.ToString)
                    End If
                End If
            Next
        End If
    End Sub
    Private Sub ClearPreviewLabels()
        lblprvReferenceFields.Text = ""
        lblprvCollectionAmount.Text = ""
        lblprvCollPRoduct.Text = ""
        lblPrvClearingType.Text = ""
        lblprvCustomerName.Text = ""
        lblPrvBank.Text = ""
        lblprvInstrumentNo.Text = ""
        lblprvValueDate.Text = ""
        lblprvLateClearing.Text = ""
        lblprvComments.Text = ""
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Collections Teller")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICUserRolesCollectionAccountAndCollectionNatureController.GetCollectionCompanyTaggedWithUserForDropDown(Me.UserId.ToString, AssignUserRolsID)
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlPrincipalBank()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBankFundTransfer.Items.Clear()
            ddlBankFundTransfer.Items.Add(lit)
            ddlBankFundTransfer.AppendDataBoundItems = True
            ddlBankFundTransfer.DataSource = ICBankController.GetAllApprovedActivePrincipalBanks
            ddlBankFundTransfer.DataTextField = "BankName"
            ddlBankFundTransfer.DataValueField = "BankCode"
            ddlBankFundTransfer.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAllBanks()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBankFundTransfer.Items.Clear()
            ddlBankFundTransfer.Items.Add(lit)
            ddlBankFundTransfer.AppendDataBoundItems = True
            ddlBankFundTransfer.DataSource = ICBankController.GetAllApprovedAndActiveBanks
            ddlBankFundTransfer.DataTextField = "BankName"
            ddlBankFundTransfer.DataValueField = "BankCode"
            ddlBankFundTransfer.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlPrincipalBankBranches()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlDrawnOnBranch.Items.Clear()
            ddlDrawnOnBranch.Items.Add(lit)
            ddlDrawnOnBranch.AppendDataBoundItems = True
            ddlDrawnOnBranch.DataSource = ICOfficeController.GetPrincipalBankBranchActiveAndApprove()
            ddlDrawnOnBranch.DataTextField = "OfficeName"
            ddlDrawnOnBranch.DataValueField = "OfficeID"
            ddlDrawnOnBranch.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlProductTypes()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCollProduct.Items.Clear()
            ddlCollProduct.Items.Add(lit)
            ddlCollProduct.AppendDataBoundItems = True
            ddlCollProduct.DataSource = ICCollectionAccountsCollectionNatureCollectionProductTypeController.GetProductTyeByCollAccCollNature(ddlCollNature.SelectedValue.ToString.Split("-")(3), ddlCollNature.SelectedValue.ToString.Split("-")(0), ddlCollNature.SelectedValue.ToString.Split("-")(1), ddlCollNature.SelectedValue.ToString.Split("-")(2))
            ddlCollProduct.DataTextField = "ProductTypeName"
            ddlCollProduct.DataValueField = "ProductTypeCodeAndDisbursementMode"
            ddlCollProduct.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCollectionNature()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCollNature.Items.Clear()
            ddlCollNature.Items.Add(lit)
            ddlCollNature.AppendDataBoundItems = True
            ddlCollNature.DataSource = ICUserRolesCollectionAccountAndCollectionNatureController.GetCollectionAccountCollectionNatureTaggedWithUserForDropDownByCompanyCode(Me.UserId.ToString, AssignUserRolsID, ddlCompany.SelectedValue.ToString())
            ddlCollNature.DataTextField = "CollAccountAndNature"
            ddlCollNature.DataValueField = "Value"
            ddlCollNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub SetLateClearingType(ByVal DisbursementMode As String)
        If Date.Compare(Date.Now.ToString("HH:mm:ss"), CDate(ICBO.ICUtilities.GetCollectionSettingValue("LateClearingTime").ToString())) >= 0 Then
            If DisbursementMode = "Fund Transfer" Then
                ddlLateClearing.SelectedValue = "N"
            Else
                ddlLateClearing.SelectedValue = "Y"
            End If
        Else
            ddlLateClearing.SelectedValue = "N"
        End If
    End Sub
    Private Sub SetPrincipalBank()
        Dim objICBank As New ICBank
        objICBank = ICBankController.GetPrincipleBank()
        ddlBankFundTransfer.SelectedValue = objICBank.BankCode.ToString
        ddlBankFundTransfer.Enabled = False
    End Sub
    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        LoadddlCollectionNature()

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(41))
    End Sub


    Private Sub Clear()

        LoadddlCompany()
        LoadddlCollectionNature()
        ddlCollProduct.ClearSelection()

    End Sub

    Protected Sub ddlCollNature_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCollNature.SelectedIndexChanged
        Try
            If ddlCollNature.SelectedValue.ToString <> "0" Then
                SetReferenceFieldVisibility(ddlCollNature.SelectedValue.ToString.Split("-")(3))
            Else
                DisableReferenceFieldValidation()
                tblReferenceField.Style.Add("display", "none")
                tblResult.Style.Add("display", "none")
                rimFundsTransfer.Enabled = False
                rimInstrumentDetails.Enabled = False
                radFromBranch.Enabled = False
                RadAmountComment.Enabled = False
                rfvddlCollProduct.Enabled = False
                rfvddlBank.Enabled = False
                tblFromBranch.Style.Add("display", "none")
                tblPreview.Style.Add("display", "none")
                rfvPrintedFrom.Enabled = False
                rfvddlDrawnOnBranch.Enabled = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetReferenceFieldVisibility(ByVal CollectionNatureCode As String)
        Dim objICCollectionNature As New ICCollectionNature
        objICCollectionNature.LoadByPrimaryKey(CollectionNatureCode)
        ViewState("CollectionAccounts") = Nothing
        ViewState("CollectionAccounts") = ICCollectionAccountsCollectionNatureController.GetCollectionAccountsByCollectionNature(objICCollectionNature.CollectionNatureCode.ToString)
        If DirectCast(ViewState("CollectionAccounts"), DataTable).Rows.Count = 0 Then
            UIUtilities.ShowDialog(Me, "Direct Pay Maker", "No collection account is tagged with selected collection nature", Dialogmessagetype.Failure, NavigateURL(186))
            Exit Sub
        End If
        If objICCollectionNature.IsInvoiceDBAvailable = True Then
            DisableReferenceFieldValidation()
            tblMain.Style.Add("display", "none")
            tblResult.Style.Add("display", "none")
            tblReferenceField.Style.Remove("display")
            SetReferenceFieldVisibilityByCollectionNature(objICCollectionNature)
            tblAmountAndCompanyDetails.Style.Add("display", "none")
            txtAmount.Text = Nothing
            RadAmountComment.Enabled = False
            btnSearch.Visible = CBool(htRights("Search"))
            btnPay.Visible = False
            btnBack.Visible = True
            btnBack2.Visible = False
            rfvddlBank.Enabled = False
            radFromBranch.Enabled = False
            tblFromBranch.Style.Add("display", "none")
            rfvPrintedFrom.Enabled = False
            rfvddlDrawnOnBranch.Enabled = False
        Else
            tblMain.Style.Add("display", "none")
            DisableReferenceFieldValidation()
            tblReferenceField.Style.Remove("display")
            SetReferenceFieldVisibilityByCollectionNature(objICCollectionNature)
            tblResult.Style.Remove("display")
            gvInvoiceRecords.DataSource = Nothing
            gvInvoiceRecords.DataBind()
            gvInvoiceRecords.Visible = False
            tblAmountAndCompanyDetails.Style.Remove("display")
            txtAmount.Text = Nothing
            RadAmountComment.Enabled = True
            btnBack.Visible = False
            btnBack2.Visible = True
            btnSearch.Visible = False
            btnPay.Visible = CBool(htRights("Pay"))
            LoadddlProductTypes()
            rfvddlCollProduct.Enabled = True
            RadAmountComment.GetSettingByBehaviorID("reAmountPKR").Validation.IsRequired = True
            txtCustomerName.Text = ""
            radFromBranch.Enabled = False
            tblFromBranch.Style.Add("display", "none")
            rfvddlBank.Enabled = False
            rfvPrintedFrom.Enabled = False
            rfvddlDrawnOnBranch.Enabled = False
        End If
    End Sub
    Private Sub DisableReferenceFieldValidation()
        DirectCast(Me.Control.FindControl("trReferenceField1"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField2"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField3"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField4"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField5"), HtmlTableRow).Style.Add("display", "none")
        rfvReferenceField1.Enabled = False
        rfvReferenceField2.Enabled = False
        rfvReferenceField3.Enabled = False
        rfvReferenceField4.Enabled = False
        rfvReferenceField5.Enabled = False
        RadInputReferenceField1.Enabled = False
        RadInputReferenceField2.Enabled = False
        RadInputReferenceField3.Enabled = False
        RadInputReferenceField4.Enabled = False
        RadInputReferenceField5.Enabled = False
        txtReferenceField1.Text = ""
        txtReferenceField2.Text = ""
        txtReferenceField3.Text = ""
        txtReferenceField4.Text = ""
        txtReferenceField5.Text = ""
    End Sub
    Private Sub SetReferenceFieldVisibilityByCollectionNature(ByVal objICCollectionNature As ICCollectionNature)
        Dim objICReferencFieldsCollection As New ICCollectionInvoiceDataTemplateFieldsCollection
        objICReferencFieldsCollection = ICCollectionInvoiceDataTemplateFieldsListController.GetReferenceFieldsCollectionInvoiceTemplateOfDualFile(objICCollectionNature.InvoiceTemplateID.ToString)
        If objICCollectionNature.IsInvoiceDBAvailable = True Then
            If objICCollectionNature.RefField1Caption IsNot Nothing Then
                If objICCollectionNature.RefField1RequiredforSearching = True Then
                    lblReferenceField1.Text = objICCollectionNature.RefField1Caption
                    DirectCast(Me.Control.FindControl("trReferenceField1"), HtmlTableRow).Style.Remove("display")
                    RadInputReferenceField1.Enabled = True
                    rfvReferenceField1.Enabled = True
                    rfvReferenceField1.ErrorMessage = ""
                    'rfvReferenceField1.ValidationGroup = "Search"
                    txtReferenceField1.MaxLength = objICReferencFieldsCollection(0).FieldLength
                    If objICReferencFieldsCollection(0).FieldDBType = "Integer" Then
                        rfvReferenceField1.ValidationExpression = "^[0-9]*$"
                        rfvReferenceField1.ErrorMessage = "Only numbers are allowed."
                    ElseIf objICReferencFieldsCollection(0).FieldDBType = "Decimal" Then
                        rfvReferenceField1.ValidationExpression = "^\d+\.\d{0,2}$"
                        rfvReferenceField1.ErrorMessage = "Only decimal values are allowed."
                    ElseIf objICReferencFieldsCollection(0).FieldDBType = "String" Then
                        rfvReferenceField1.Enabled = False
                    End If
                    If objICCollectionNature.RefField1RequiredforInput = True Then
                        RadInputReferenceField1.GetSettingByBehaviorID("reReferenceFieldOne").Validation.IsRequired = True
                    End If
                    RadInputReferenceField1.GetSettingByBehaviorID("reReferenceFieldOne").Validation.ValidationGroup = "Search"
                End If
            End If
            If objICCollectionNature.RefField2Caption IsNot Nothing Then
                If objICCollectionNature.RefField2RequiredforSearching = True Then
                    lblReferenceField2.Text = objICCollectionNature.RefField2Caption
                    DirectCast(Me.Control.FindControl("trReferenceField2"), HtmlTableRow).Style.Remove("display")
                    RadInputReferenceField2.Enabled = True
                    rfvReferenceField2.Enabled = True
                    rfvReferenceField2.ErrorMessage = ""
                    'rfvReferenceField2.ValidationGroup = "Search"
                    txtReferenceField2.MaxLength = objICReferencFieldsCollection(1).FieldLength
                    If objICReferencFieldsCollection(1).FieldDBType = "Integer" Then
                        rfvReferenceField2.ValidationExpression = "^[0-9]*$"
                        rfvReferenceField2.ErrorMessage = "Only numbers are allowed."
                    ElseIf objICReferencFieldsCollection(1).FieldDBType = "Decimal" Then
                        rfvReferenceField2.ValidationExpression = "^\d+\.\d{0,2}$"
                        rfvReferenceField2.ErrorMessage = "Only decimal values are allowed."
                    ElseIf objICReferencFieldsCollection(1).FieldDBType = "String" Then
                        rfvReferenceField2.Enabled = False
                    End If
                    If objICCollectionNature.RefField2RequiredforInput = True Then
                        RadInputReferenceField2.GetSettingByBehaviorID("reReferenceFieldTwo").Validation.IsRequired = True
                    End If
                    RadInputReferenceField2.GetSettingByBehaviorID("reReferenceFieldTwo").Validation.ValidationGroup = "Search"
                End If
            End If
            If objICCollectionNature.RefField3Caption IsNot Nothing Then
                If objICCollectionNature.RefField3RequiredforSearching = True Then
                    lblReferenceField3.Text = objICCollectionNature.RefField3Caption
                    DirectCast(Me.Control.FindControl("trReferenceField3"), HtmlTableRow).Style.Remove("display")
                    RadInputReferenceField3.Enabled = True
                    rfvReferenceField3.Enabled = True
                    rfvReferenceField3.ErrorMessage = ""
                    'rfvReferenceField3.ValidationGroup = "Search"
                    txtReferenceField3.MaxLength = objICReferencFieldsCollection(2).FieldLength
                    If objICReferencFieldsCollection(2).FieldDBType = "Integer" Then
                        rfvReferenceField3.ValidationExpression = "^[0-9]*$"
                        rfvReferenceField3.ErrorMessage = "Only numbers are allowed."
                    ElseIf objICReferencFieldsCollection(2).FieldDBType = "Decimal" Then
                        rfvReferenceField3.ValidationExpression = "^\d+\.\d{0,2}$"
                        rfvReferenceField3.ErrorMessage = "Only decimal values are allowed."
                    ElseIf objICReferencFieldsCollection(2).FieldDBType = "String" Then
                        rfvReferenceField3.Enabled = False
                    End If
                    If objICCollectionNature.RefField3RequiredforInput = True Then
                        RadInputReferenceField3.GetSettingByBehaviorID("reReferenceFieldThree").Validation.IsRequired = True
                    End If
                    RadInputReferenceField3.GetSettingByBehaviorID("reReferenceFieldThree").Validation.ValidationGroup = "Search"
                End If
            End If
            If objICCollectionNature.RefField4Caption IsNot Nothing Then
                If objICCollectionNature.RefField4RequiredforSearching = True Then
                    lblReferenceField4.Text = objICCollectionNature.RefField4Caption
                    DirectCast(Me.Control.FindControl("trReferenceField4"), HtmlTableRow).Style.Remove("display")
                    RadInputReferenceField4.Enabled = True
                    rfvReferenceField4.Enabled = True
                    rfvReferenceField4.ErrorMessage = ""
                    'rfvReferenceField4.ValidationGroup = "Search"
                    txtReferenceField4.MaxLength = objICReferencFieldsCollection(3).FieldLength
                    If objICReferencFieldsCollection(3).FieldDBType = "Integer" Then
                        rfvReferenceField4.ValidationExpression = "^[0-9]*$"
                        rfvReferenceField4.ErrorMessage = "Only numbers are allowed."
                    ElseIf objICReferencFieldsCollection(3).FieldDBType = "Decimal" Then
                        rfvReferenceField4.ValidationExpression = "^\d+\.\d{0,2}$"
                        rfvReferenceField4.ErrorMessage = "Only decimal values are allowed."
                    ElseIf objICReferencFieldsCollection(3).FieldDBType = "String" Then
                        rfvReferenceField4.Enabled = False
                    End If
                    If objICCollectionNature.RefField4RequiredforInput = True Then
                        RadInputReferenceField4.GetSettingByBehaviorID("reReferenceFieldFour").Validation.IsRequired = True
                    End If
                    RadInputReferenceField4.GetSettingByBehaviorID("reReferenceFieldFour").Validation.ValidationGroup = "Search"
                End If
            End If
            If objICCollectionNature.RefField5Caption IsNot Nothing Then
                If objICCollectionNature.RefField5RequiredforSearching = True Then
                    lblReferenceField5.Text = objICCollectionNature.RefField5Caption
                    DirectCast(Me.Control.FindControl("trReferenceField5"), HtmlTableRow).Style.Remove("display")
                    RadInputReferenceField5.Enabled = True
                    rfvReferenceField5.ErrorMessage = ""
                    rfvReferenceField5.Enabled = True
                    'rfvReferenceField5.ValidationGroup = "Search"
                    txtReferenceField5.MaxLength = objICReferencFieldsCollection(4).FieldLength
                    If objICReferencFieldsCollection(4).FieldDBType = "Integer" Then
                        rfvReferenceField5.ValidationExpression = "^[0-9]*$"
                        rfvReferenceField5.ErrorMessage = "Only numbers are allowed."
                    ElseIf objICReferencFieldsCollection(4).FieldDBType = "Decimal" Then
                        rfvReferenceField5.ValidationExpression = "^\d+\.\d{0,2}$"
                        rfvReferenceField5.ErrorMessage = "Only decimal values are allowed."
                    ElseIf objICReferencFieldsCollection(4).FieldDBType = "String" Then
                        rfvReferenceField5.Enabled = False
                    End If
                    If objICCollectionNature.RefField5RequiredforInput = True Then
                        RadInputReferenceField5.GetSettingByBehaviorID("reReferenceFieldFive").Validation.IsRequired = True
                    End If
                    RadInputReferenceField5.GetSettingByBehaviorID("reReferenceFieldFive").Validation.ValidationGroup = "Search"
                End If
            End If
        Else

            For i = 0 To objICReferencFieldsCollection.Count - 1
                Select Case i.ToString
                    Case "0"
                        lblReferenceField1.Text = objICReferencFieldsCollection(i).FieldName
                        DirectCast(Me.Control.FindControl("trReferenceField1"), HtmlTableRow).Style.Remove("display")
                        RadInputReferenceField1.Enabled = True
                        rfvReferenceField1.Enabled = True
                        'rfvReferenceField1.ValidationGroup = "Pay"
                        txtReferenceField1.MaxLength = objICReferencFieldsCollection(i).FieldLength
                        If objICReferencFieldsCollection(i).FieldDBType = "Integer" Then
                            rfvReferenceField1.ValidationExpression = "^[0-9]*$"
                            rfvReferenceField1.ErrorMessage = "Only numbers are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "Decimal" Then
                            rfvReferenceField1.ValidationExpression = "^\d+\.\d{0,2}$"
                            rfvReferenceField1.ErrorMessage = "Only decimal values are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "String" Then
                            rfvReferenceField1.Enabled = False
                        End If
                        RadInputReferenceField1.GetSettingByBehaviorID("reReferenceFieldOne").Validation.IsRequired = True
                        'RadInputReferenceField1.GetSettingByBehaviorID("reReferenceFieldOne").Validation.ValidationGroup = "Pay"
                    Case "1"
                        lblReferenceField2.Text = objICReferencFieldsCollection(i).FieldName
                        DirectCast(Me.Control.FindControl("trReferenceField2"), HtmlTableRow).Style.Remove("display")
                        RadInputReferenceField2.Enabled = True
                        rfvReferenceField2.Enabled = True
                        'rfvReferenceField2.ValidationGroup = "Pay"
                        txtReferenceField2.MaxLength = objICReferencFieldsCollection(i).FieldLength
                        If objICReferencFieldsCollection(i).FieldDBType = "Integer" Then
                            rfvReferenceField2.ValidationExpression = "^[0-9]*$"
                            rfvReferenceField2.ErrorMessage = "Only numbers are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "Decimal" Then
                            rfvReferenceField2.ValidationExpression = "^\d+\.\d{0,2}$"
                            rfvReferenceField2.ErrorMessage = "Only decimal values are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "String" Then
                            rfvReferenceField2.Enabled = False
                        End If
                        RadInputReferenceField2.GetSettingByBehaviorID("reReferenceFieldTwo").Validation.IsRequired = True
                        'RadInputReferenceField2.GetSettingByBehaviorID("reReferenceFieldTwo").Validation.ValidationGroup = "Pay"
                    Case "2"
                        lblReferenceField3.Text = objICReferencFieldsCollection(i).FieldName
                        DirectCast(Me.Control.FindControl("trReferenceField3"), HtmlTableRow).Style.Remove("display")
                        RadInputReferenceField3.Enabled = True
                        rfvReferenceField3.Enabled = True
                        'rfvReferenceField3.ValidationGroup = "Pay"
                        txtReferenceField3.MaxLength = objICReferencFieldsCollection(i).FieldLength
                        If objICReferencFieldsCollection(i).FieldDBType = "Integer" Then
                            rfvReferenceField3.ValidationExpression = "^[0-9]*$"
                            rfvReferenceField3.ErrorMessage = "Only numbers are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "Decimal" Then
                            rfvReferenceField3.ValidationExpression = "^\d+\.\d{0,2}$"
                            rfvReferenceField3.ErrorMessage = "Only decimal values are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "String" Then
                            rfvReferenceField3.Enabled = False
                        End If
                        RadInputReferenceField3.GetSettingByBehaviorID("reReferenceFieldThree").Validation.IsRequired = True
                        'RadInputReferenceField3.GetSettingByBehaviorID("reReferenceFieldThree").Validation.ValidationGroup = "Pay"
                    Case "3"
                        lblReferenceField4.Text = objICReferencFieldsCollection(i).FieldName
                        DirectCast(Me.Control.FindControl("trReferenceField4"), HtmlTableRow).Style.Remove("display")
                        RadInputReferenceField4.Enabled = True
                        rfvReferenceField4.Enabled = True
                        'rfvReferenceField4.ValidationGroup = "Pay"
                        txtReferenceField4.MaxLength = objICReferencFieldsCollection(i).FieldLength
                        If objICReferencFieldsCollection(i).FieldDBType = "Integer" Then
                            rfvReferenceField4.ValidationExpression = "^[0-9]*$"
                            rfvReferenceField4.ErrorMessage = "Only numbers are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "Decimal" Then
                            rfvReferenceField4.ValidationExpression = "^\d+\.\d{0,2}$"
                            rfvReferenceField4.ErrorMessage = "Only decimal values are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "String" Then
                            rfvReferenceField4.Enabled = False
                        End If
                        RadInputReferenceField4.GetSettingByBehaviorID("reReferenceFieldFour").Validation.IsRequired = True
                        'RadInputReferenceField4.GetSettingByBehaviorID("reReferenceFieldFour").Validation.ValidationGroup = "Pay"
                    Case "4"
                        lblReferenceField5.Text = objICReferencFieldsCollection(i).FieldName
                        DirectCast(Me.Control.FindControl("trReferenceField5"), HtmlTableRow).Style.Remove("display")
                        RadInputReferenceField5.Enabled = True
                        rfvReferenceField5.Enabled = True
                        'rfvReferenceField5.ValidationGroup = "Pay"
                        txtReferenceField5.MaxLength = objICReferencFieldsCollection(i).FieldLength
                        If objICReferencFieldsCollection(i).FieldDBType = "Integer" Then
                            rfvReferenceField5.ValidationExpression = "^[0-9]*$"
                            rfvReferenceField5.ErrorMessage = "Only numbers are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "Decimal" Then
                            rfvReferenceField5.ValidationExpression = "^\d+\.\d{0,2}$"
                            rfvReferenceField5.ErrorMessage = "Only decimal values are allowed."
                        ElseIf objICReferencFieldsCollection(i).FieldDBType = "String" Then
                            rfvReferenceField5.Enabled = False
                        End If
                        RadInputReferenceField5.GetSettingByBehaviorID("reReferenceFieldFive").Validation.IsRequired = True
                        'RadInputReferenceField5.GetSettingByBehaviorID("reReferenceFieldFive").Validation.ValidationGroup = "Pay"
                End Select
            Next
        End If
    End Sub
    'Private Sub SetReferenceFieldVisibilityByCollectionNature(ByVal objICCollectionNature As ICCollectionNature)
    '    Dim objICReferencFieldsCollection As New ICCollectionInvoiceDataTemplateFieldsCollection
    '    objICReferencFieldsCollection = ICCollectionInvoiceDataTemplateFieldsListController.GetReferenceFieldsCollectionInvoiceTemplateOfDualFile(objICCollectionNature.InvoiceTemplateID.ToString)
    '    If objICCollectionNature.RefField1Caption IsNot Nothing Then
    '        If objICCollectionNature.RefField1RequiredforSearching = True Then
    '            lblReferenceField1.Text = objICCollectionNature.RefField1Caption
    '            DirectCast(Me.Control.FindControl("trReferenceField1"), HtmlTableRow).Style.Remove("display")
    '            RadInputReferenceField1.Enabled = True
    '            rfvReferenceField1.Enabled = True
    '            rfvReferenceField1.ErrorMessage = ""
    '            txtReferenceField1.MaxLength = objICReferencFieldsCollection(0).FieldLength
    '            If objICReferencFieldsCollection(0).FieldDBType = "Integer" Then
    '                rfvReferenceField1.ValidationExpression = "^[0-9]*$"
    '                rfvReferenceField1.ErrorMessage = "Only numbers are allowed."
    '            ElseIf objICReferencFieldsCollection(0).FieldDBType = "Decimal" Then
    '                rfvReferenceField1.ValidationExpression = "^\d+\.\d{0,2}$"
    '                rfvReferenceField1.ErrorMessage = "Only decimal values are allowed."
    '            ElseIf objICReferencFieldsCollection(0).FieldDBType = "String" Then
    '                rfvReferenceField1.Enabled = False
    '            End If
    '            If objICCollectionNature.RefField1RequiredforInput = True Then
    '                RadInputReferenceField1.GetSettingByBehaviorID("reReferenceFieldOne").Validation.IsRequired = True
    '            End If
    '        End If
    '    End If
    '    If objICCollectionNature.RefField2Caption IsNot Nothing Then
    '        If objICCollectionNature.RefField2RequiredforSearching = True Then
    '            lblReferenceField2.Text = objICCollectionNature.RefField2Caption
    '            DirectCast(Me.Control.FindControl("trReferenceField2"), HtmlTableRow).Style.Remove("display")
    '            RadInputReferenceField2.Enabled = True
    '            rfvReferenceField2.Enabled = True
    '            rfvReferenceField2.ErrorMessage = ""
    '            txtReferenceField2.MaxLength = objICReferencFieldsCollection(1).FieldLength
    '            If objICReferencFieldsCollection(1).FieldDBType = "Integer" Then
    '                rfvReferenceField2.ValidationExpression = "^[0-9]*$"
    '                rfvReferenceField2.ErrorMessage = "Only numbers are allowed."
    '            ElseIf objICReferencFieldsCollection(1).FieldDBType = "Decimal" Then
    '                rfvReferenceField2.ValidationExpression = "^\d+\.\d{0,2}$"
    '                rfvReferenceField2.ErrorMessage = "Only decimal values are allowed."
    '            ElseIf objICReferencFieldsCollection(1).FieldDBType = "String" Then
    '                rfvReferenceField2.Enabled = False
    '            End If
    '            If objICCollectionNature.RefField2RequiredforInput = True Then
    '                RadInputReferenceField2.GetSettingByBehaviorID("reReferenceFieldTwo").Validation.IsRequired = True
    '            End If
    '        End If
    '    End If
    '    If objICCollectionNature.RefField3Caption IsNot Nothing Then
    '        If objICCollectionNature.RefField3RequiredforSearching = True Then
    '            lblReferenceField3.Text = objICCollectionNature.RefField3Caption
    '            DirectCast(Me.Control.FindControl("trReferenceField3"), HtmlTableRow).Style.Remove("display")
    '            RadInputReferenceField3.Enabled = True
    '            rfvReferenceField3.Enabled = True
    '            rfvReferenceField3.ErrorMessage = ""
    '            txtReferenceField3.MaxLength = objICReferencFieldsCollection(2).FieldLength
    '            If objICReferencFieldsCollection(2).FieldDBType = "Integer" Then
    '                rfvReferenceField3.ValidationExpression = "^[0-9]*$"
    '                rfvReferenceField3.ErrorMessage = "Only numbers are allowed."
    '            ElseIf objICReferencFieldsCollection(2).FieldDBType = "Decimal" Then
    '                rfvReferenceField3.ValidationExpression = "^\d+\.\d{0,2}$"
    '                rfvReferenceField3.ErrorMessage = "Only decimal values are allowed."
    '            ElseIf objICReferencFieldsCollection(2).FieldDBType = "String" Then
    '                rfvReferenceField3.Enabled = False
    '            End If
    '            If objICCollectionNature.RefField3RequiredforInput = True Then
    '                RadInputReferenceField3.GetSettingByBehaviorID("reReferenceFieldThree").Validation.IsRequired = True
    '            End If
    '        End If
    '    End If
    '    If objICCollectionNature.RefField4Caption IsNot Nothing Then
    '        If objICCollectionNature.RefField4RequiredforSearching = True Then
    '            lblReferenceField4.Text = objICCollectionNature.RefField4Caption
    '            DirectCast(Me.Control.FindControl("trReferenceField4"), HtmlTableRow).Style.Remove("display")
    '            RadInputReferenceField4.Enabled = True
    '            rfvReferenceField4.Enabled = True
    '            rfvReferenceField4.ErrorMessage = ""
    '            txtReferenceField4.MaxLength = objICReferencFieldsCollection(3).FieldLength
    '            If objICReferencFieldsCollection(3).FieldDBType = "Integer" Then
    '                rfvReferenceField4.ValidationExpression = "^[0-9]*$"
    '                rfvReferenceField4.ErrorMessage = "Only numbers are allowed."
    '            ElseIf objICReferencFieldsCollection(3).FieldDBType = "Decimal" Then
    '                rfvReferenceField4.ValidationExpression = "^\d+\.\d{0,2}$"
    '                rfvReferenceField4.ErrorMessage = "Only decimal values are allowed."
    '            ElseIf objICReferencFieldsCollection(3).FieldDBType = "String" Then
    '                rfvReferenceField4.Enabled = False
    '            End If
    '            If objICCollectionNature.RefField4RequiredforInput = True Then
    '                RadInputReferenceField4.GetSettingByBehaviorID("reReferenceFieldFour").Validation.IsRequired = True
    '            End If
    '        End If
    '    End If
    '    If objICCollectionNature.RefField5Caption IsNot Nothing Then
    '        If objICCollectionNature.RefField5RequiredforSearching = True Then
    '            lblReferenceField5.Text = objICCollectionNature.RefField5Caption
    '            DirectCast(Me.Control.FindControl("trReferenceField5"), HtmlTableRow).Style.Remove("display")
    '            RadInputReferenceField5.Enabled = True
    '            rfvReferenceField5.ErrorMessage = ""
    '            rfvReferenceField5.Enabled = True
    '            txtReferenceField5.MaxLength = objICReferencFieldsCollection(4).FieldLength
    '            If objICReferencFieldsCollection(4).FieldDBType = "Integer" Then
    '                rfvReferenceField5.ValidationExpression = "^[0-9]*$"
    '                rfvReferenceField5.ErrorMessage = "Only numbers are allowed."
    '            ElseIf objICReferencFieldsCollection(4).FieldDBType = "Decimal" Then
    '                rfvReferenceField5.ValidationExpression = "^\d+\.\d{0,2}$"
    '                rfvReferenceField5.ErrorMessage = "Only decimal values are allowed."
    '            ElseIf objICReferencFieldsCollection(4).FieldDBType = "String" Then
    '                rfvReferenceField5.Enabled = False
    '            End If
    '            If objICCollectionNature.RefField5RequiredforInput = True Then
    '                RadInputReferenceField5.GetSettingByBehaviorID("reReferenceFieldFive").Validation.IsRequired = True
    '            End If
    '        End If
    '        If CBool(htRights("Search")) = True Then
    '            btnSearch.Visible = True
    '        Else
    '            btnSearch.Visible = False
    '        End If
    '    End If
    'End Sub
    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Try
            Response.Redirect(NavigateURL())
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Try

            tblReferenceField.Style.Add("display", "none")
            tblResult.Style.Remove("display")
            Dim objICCollectionNature As New ICCollectionNature
            Dim objICCollectionInvoiceDBTemplate As New ICCollectionInvoiceDataTemplate
            objICCollectionNature.LoadByPrimaryKey(ddlCollNature.SelectedValue.ToString.Split("-")(3))
            objICCollectionInvoiceDBTemplate.LoadByPrimaryKey(objICCollectionNature.InvoiceTemplateID.ToString)
            Dim ReferenceField1, ReferenceField2, ReferenceField3, ReferenceField4, ReferenceField5 As String
            Dim dt As New DataTable
            Dim dtFinal As New DataTable
            Dim drFinal As DataRow
            If txtReferenceField1.Text <> "" Then
                ReferenceField1 = txtReferenceField1.Text
            Else
                ReferenceField1 = Nothing
            End If
            If txtReferenceField2.Text <> "" Then
                ReferenceField2 = txtReferenceField2.Text
            Else
                ReferenceField2 = Nothing
            End If
            If txtReferenceField3.Text <> "" Then
                ReferenceField3 = txtReferenceField3.Text
            Else
                ReferenceField3 = Nothing
            End If
            If txtReferenceField4.Text <> "" Then
                ReferenceField4 = txtReferenceField4.Text
            Else
                ReferenceField4 = Nothing
            End If
            If txtReferenceField5.Text <> "" Then
                ReferenceField5 = txtReferenceField5.Text
            Else
                ReferenceField5 = Nothing

            End If
            dt = ICCollectionSQLController.GetInvoiceRecordsByReferenceFields(objICCollectionInvoiceDBTemplate, ReferenceField1, ReferenceField2, ReferenceField3, ReferenceField4, ReferenceField5)
            If dt.Rows.Count > 0 Then
                If dt.Rows.Count = 1 Then
                    If dt.Rows(0)("Status").ToString = "Paid" Then
                        UIUtilities.ShowDialog(Me, "Collection Teller", "Invoice Record is already paid", Dialogmessagetype.Failure)
                        tblResult.Style.Add("display", "none")
                        gvInvoiceRecords.DataSource = Nothing
                        gvInvoiceRecords.DataBind()
                        tblReferenceField.Style.Remove("display")
                        Exit Sub
                    Else
                        dtFinal = dt.Clone
                        For Each dr As DataRow In dt.Rows
                            If dr("Status") = "Pending" Then
                                drFinal = dtFinal.NewRow
                                For i = 0 To dr.Table.Columns.Count - 1
                                    drFinal(i) = dr(i).ToString
                                Next
                                dtFinal.Rows.Add(drFinal)
                            End If
                        Next
                    End If
                Else
                    dtFinal = dt.Clone
                    For Each dr As DataRow In dt.Rows
                        If dr("Status") = "Pending" Then
                            drFinal = dtFinal.NewRow
                            For i = 0 To dr.Table.Columns.Count - 1
                                drFinal(i) = dr(i).ToString
                            Next
                            dtFinal.Rows.Add(drFinal)
                        End If
                    Next
                End If
                If dtFinal.Rows.Count > 0 Then
                    tblAmountAndCompanyDetails.Style.Remove("display")
                    txtAmount.Text = Nothing
                    RadAmountComment.Enabled = True
                    gvInvoiceRecords.DataSource = Nothing
                    gvInvoiceRecords.DataSource = dtFinal
                    gvInvoiceRecords.DataBind()
                    btnPay.Visible = CBool(htRights("Pay"))
                    btnBack2.Visible = True
                    LoadddlProductTypes()
                    SetAmountTextBoxValue(gvInvoiceRecords.Items(0), objICCollectionNature)
                    RadAmountComment.GetSettingByBehaviorID("reAmountPKR").Validation.IsRequired = True
                Else
                    UIUtilities.ShowDialog(Me, "Collection Teller", "No Record Found<br />1. Invoice Record is already paid <br />2. Invoice record do not exists <br />3. Invoice record is not in Pending status", Dialogmessagetype.Failure)
                    tblResult.Style.Add("display", "none")
                    gvInvoiceRecords.DataSource = Nothing
                    gvInvoiceRecords.DataBind()
                    tblReferenceField.Style.Remove("display")
                    Exit Sub
                End If
            Else
                UIUtilities.ShowDialog(Me, "Collection Teller", "No Record Found", Dialogmessagetype.Failure)
                tblResult.Style.Add("display", "none")
                gvInvoiceRecords.DataSource = Nothing
                gvInvoiceRecords.DataBind()
                tblReferenceField.Style.Remove("display")
                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetAmountTextBoxValue(ByVal gvRow As GridDataItem, ByVal objICCollectionNature As ICCollectionNature)
        If CDate(gvRow.Item(objICCollectionNature.UpToICCollectionInvoiceDataTemplateFieldsByDueDateFieldID.FieldName).Text) >= Now.Date Then
            txtAmount.Text = gvRow.Item(objICCollectionNature.UpToICCollectionInvoiceDataTemplateFieldsByAmountBeforeDueDateFieldID.FieldName).Text
            If objICCollectionNature.IsPartialCollectionAllowed = True Then
                txtAmount.ReadOnly = False
            Else
                txtAmount.ReadOnly = True
            End If
        Else
            txtAmount.Text = gvRow.Item(objICCollectionNature.UpToICCollectionInvoiceDataTemplateFieldsByAmountAfterDueDateFieldID.FieldName).Text
            If objICCollectionNature.IsPartialCollectionAllowed = True Then
                txtAmount.ReadOnly = False
            Else
                txtAmount.ReadOnly = True
            End If
        End If
    End Sub
    Protected Sub gvInvoiceRecords_ItemCreated(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles gvInvoiceRecords.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvInvoiceRecords.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvInvoiceRecords_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles gvInvoiceRecords.ItemDataBound
        'Dim chkProcessAll As CheckBox
        'Dim imgBtn As LinkButton
        'If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
        '    chkProcessAll = New CheckBox
        '    chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
        '    chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvInvoiceRecords.MasterTableView.ClientID & "','0');"
        'End If
        'If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
        '    Dim Ref1, Ref2, Ref3, Ref4, Ref5 As String
        '    Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        '    Ref1 = ""
        '    Ref2 = ""
        '    Ref3 = ""
        '    Ref4 = ""
        '    Ref5 = ""
        '    SetReferenceFieldValuesByTemplateFieldsByGridRow(Ref1, Ref2, Ref3, Ref4, Ref5, item)
        '    imgBtn = New LinkButton
        '    imgBtn = DirectCast(item.Cells(1).FindControl("lbCollDetail"), LinkButton)
        '    imgBtn.OnClientClick = "showurldialog('Collection Details','/ShowCollectionsDetails.aspx?Ref1=" & Ref1 & "-" & Ref2 & "-" & Ref3 & "-" & Ref4 & "-" & Ref5 & "' ,true,700,650);return false;"
        'End If
    End Sub
    Private Function GetCollectionObject(ByVal RefField1 As String, ByVal RefField2 As String, ByVal RefField3 As String, ByVal RefField4 As String, ByVal RefField5 As String, ByVal AmountAfterDueDate As Double, ByVal DueDate As Nullable(Of DateTime), ByVal FileID As String) As ICCollections
        ', ByVal CollNatureCode As String, ByVal CollAccountNo As String, ByVal CollAccountBranch As String, ByVal CollAccountCurrency As String, ByVal CollAccountType As String
        Dim objICCollections As New ICCollections
        Dim objICCollectionProduct As New ICCollectionProductType
        Dim objICCollectionNature As New ICCollectionNature
        Dim objICCinstruction As New ICInstruction
        Dim objICUser As New ICUser
        Dim objICOffice As New ICOffice
        Dim ResultString As String = Nothing
        'objICCompany.LoadByPrimaryKey(CompanyCode)
        objICUser.LoadByPrimaryKey(Me.UserInfo.UserID.ToString)
        objICCollectionProduct.LoadByPrimaryKey(ddlCollProduct.SelectedValue.ToString.Split("-")(0))
        objICCollectionNature.LoadByPrimaryKey(ddlCollNature.SelectedValue.ToString.Split("-")(3))

        If objICCollectionProduct.ClearingType = "Funds Transfer" Then
            If txtCustomerName.Text <> "" Then
                objICCollections.PayersName = txtCustomerName.Text
            Else
                objICCollections.PayersName = txtFromAccountTitle.Text
            End If
        Else
            objICCollections.PayersName = txtCustomerName.Text
        End If

        objICCollections.CollectionDate = Date.Now
        objICCollections.CollectionProductTypeCode = objICCollectionProduct.ProductTypeCode
        If ddlBankFundTransfer.SelectedValue.ToString <> "0" And ddlBankFundTransfer.SelectedValue.ToString <> "" Then
            objICCollections.BankCode = ddlBankFundTransfer.SelectedValue.ToString
        Else
            objICCollections.BankCode = Nothing
        End If


        If txtInstrumentNo.Text <> "" Then
            objICCollections.InstrumentNo = txtInstrumentNo.Text
        End If
        If objICCollectionProduct.DisbursementMode = "Cash" Then
            objICCollections.ClearingAccountNo = objICUser.UpToICOfficeByOfficeCode.BranchCashTillAccountNumber
            objICCollections.ClearingAccountBranchCode = objICUser.UpToICOfficeByOfficeCode.OfficeCode
            objICCollections.ClearingAccountCurrency = objICUser.UpToICOfficeByOfficeCode.BranchCashTillAccountCurrency
            objICCollections.ClearingCBAccountType = objICUser.UpToICOfficeByOfficeCode.BranchCashTillAccountType
            objICCollections.Status = 3
            objICCollections.LastStatus = 3
            objICCollections.ClearingValueDate = Date.Now
        Else
            If objICCollectionProduct.DisbursementMode = "Pay Order" Then
                If objICCollectionProduct.ClearingType = "Funds Transfer" Then
                    If ICUtilities.GetCollectionSettingValue("POPayableAccount") = "Central" Then
                        If ICUtilities.GetCollectionSettingValue("POPayableAccountNumber") <> "" Then
                            objICCollections.ClearingAccountNo = ICUtilities.GetCollectionSettingValue("POPayableAccountNumber")
                            objICCollections.ClearingAccountBranchCode = objICUser.UpToICOfficeByOfficeCode.OfficeCode
                            objICCollections.ClearingAccountCurrency = ICUtilities.GetCollectionSettingValue("POPayableAccountCurrency")
                            objICCollections.ClearingCBAccountType = ICUtilities.GetCollectionSettingValue("POPayableAccountType")
                            objICCollections.Status = 3
                            objICCollections.LastStatus = 3
                            objICCollections.ClearingValueDate = Date.Now
                        Else
                            Throw New Exception("Central PO Payable account is not defined")
                        End If
                    Else
                        If rbPrintedFrom.SelectedValue = "Soneri Trans@ct" Then
                            ResultString = ICInstructionProcessController.GetInstructionForCollectionVerification("PO-" & objICCollections.InstrumentNo, "")
                            If ResultString.ToString().Contains("|") Then
                                Dim collInstruction As New ICInstructionCollection
                                collInstruction = ICInstructionController.GetInstructionForClearing(ResultString.ToString().Split("|")(1).ToString())
                                If collInstruction.Count > 0 Then
                                    objICCinstruction = collInstruction(0)
                                    objICCollections.ClearingAccountNo = objICCinstruction.PayableAccountNumber
                                    objICCollections.ClearingAccountBranchCode = objICUser.UpToICOfficeByOfficeCode.OfficeCode
                                    objICCollections.ClearingAccountCurrency = objICCinstruction.PayableAccountCurrency
                                    objICCollections.ClearingCBAccountType = objICCinstruction.PayableAccountCurrency
                                    objICCollections.Status = 3
                                    objICCollections.LastStatus = 3
                                    objICCollections.ClearingValueDate = Date.Now
                                End If
                            Else
                                Throw New Exception(ResultString)
                                Exit Function
                            End If
                        Else
                            objICOffice.LoadByPrimaryKey(ddlDrawnOnBranch.SelectedValue.ToString)
                            If objICOffice.POPayableAccountNumber IsNot Nothing Then
                                objICCollections.ClearingAccountNo = objICOffice.POPayableAccountNumber
                                objICCollections.ClearingAccountBranchCode = objICOffice.OfficeCode
                                objICCollections.ClearingAccountCurrency = objICOffice.POPayableAccountCurrency
                                objICCollections.ClearingCBAccountType = objICOffice.POPayableAccountType
                                objICCollections.Status = 3
                                objICCollections.LastStatus = 3
                                objICCollections.ClearingValueDate = Date.Now
                            Else
                                Throw New Exception("PO Payable account is not tagged with branch")
                            End If
                        End If
                    End If

                Else
                    If ICUtilities.GetCollectionSettingValue("POPayableAccount") = "Central" Then
                        If ICUtilities.GetCollectionSettingValue("POPayableAccountNumber") <> "" Then
                            objICCollections.ClearingAccountNo = ICUtilities.GetCollectionSettingValue("POPayableAccountNumber")
                            objICCollections.ClearingAccountBranchCode = objICUser.UpToICOfficeByOfficeCode.OfficeCode
                            objICCollections.ClearingAccountCurrency = ICUtilities.GetCollectionSettingValue("POPayableAccountCurrency")
                            objICCollections.ClearingCBAccountType = ICUtilities.GetCollectionSettingValue("POPayableAccountType")
                            objICCollections.Status = 8
                            objICCollections.LastStatus = 8
                            objICCollections.ClearingValueDate = Date.Now
                        Else
                            Throw New Exception("Central PO Payable account is not defined")
                        End If
                    Else
                        If rbPrintedFrom.SelectedValue = "Soneri Trans@ct" Then
                            ResultString = ICInstructionProcessController.GetInstructionForCollectionVerification("PO-" & objICCollections.InstrumentNo, "")
                            If ResultString.ToString().Contains("|") Then
                                Dim collInstruction As New ICInstructionCollection
                                collInstruction = ICInstructionController.GetInstructionForClearing(ResultString.ToString().Split("|")(1).ToString())
                                If collInstruction.Count > 0 Then
                                    objICCinstruction = collInstruction(0)
                                    objICCollections.ClearingAccountNo = objICCinstruction.PayableAccountNumber
                                    objICCollections.ClearingAccountBranchCode = objICUser.UpToICOfficeByOfficeCode.OfficeCode
                                    objICCollections.ClearingAccountCurrency = objICCinstruction.PayableAccountCurrency
                                    objICCollections.ClearingCBAccountType = objICCinstruction.PayableAccountCurrency
                                    objICCollections.Status = 8
                                    objICCollections.LastStatus = 8
                                    objICCollections.ClearingValueDate = Date.Now
                                End If
                            Else
                                Throw New Exception(ResultString)
                                Exit Function
                            End If
                        Else
                            objICOffice.LoadByPrimaryKey(ddlDrawnOnBranch.SelectedValue.ToString)
                            If objICOffice.POPayableAccountNumber IsNot Nothing Then
                                objICCollections.ClearingAccountNo = objICOffice.POPayableAccountNumber
                                objICCollections.ClearingAccountBranchCode = objICOffice.OfficeCode
                                objICCollections.ClearingAccountCurrency = objICOffice.POPayableAccountCurrency
                                objICCollections.ClearingCBAccountType = objICOffice.POPayableAccountType
                                objICCollections.Status = 8
                                objICCollections.LastStatus = 8
                                objICCollections.ClearingValueDate = Date.Now
                            Else
                                Throw New Exception("PO Payable account is not tagged with branch")
                            End If
                        End If
                    End If
                End If
            ElseIf objICCollectionProduct.DisbursementMode = "Demand Draft" Then
                If objICCollectionProduct.ClearingType = "Funds Transfer" Then
                    objICCollections.ClearingAccountNo = objICUser.UpToICOfficeByOfficeCode.DDPayableAccountNumber
                    objICCollections.ClearingAccountBranchCode = objICUser.UpToICOfficeByOfficeCode.OfficeCode
                    objICCollections.ClearingAccountCurrency = objICUser.UpToICOfficeByOfficeCode.DDPayableAccountCurrency.ToString.ToUpper
                    objICCollections.ClearingCBAccountType = objICUser.UpToICOfficeByOfficeCode.DDPayableAccountType
                    objICCollections.Status = 3
                    objICCollections.LastStatus = 3
                    objICCollections.ClearingValueDate = Date.Now
                Else
                    objICCollections.ClearingAccountNo = objICUser.UpToICOfficeByOfficeCode.DDPayableAccountNumber
                    objICCollections.ClearingAccountBranchCode = objICUser.UpToICOfficeByOfficeCode.OfficeCode
                    objICCollections.ClearingAccountCurrency = objICUser.UpToICOfficeByOfficeCode.DDPayableAccountCurrency.ToString.ToUpper
                    objICCollections.ClearingCBAccountType = objICUser.UpToICOfficeByOfficeCode.DDPayableAccountType
                    objICCollections.ClearingValueDate = Date.Now
                    objICCollections.Status = 8
                    objICCollections.LastStatus = 8
                End If
            ElseIf objICCollectionProduct.DisbursementMode = "Cheque" Then
                If objICCollectionProduct.ClearingType = "Funds Transfer" Then
                    objICCollections.PayersAccountNo = txtFromAccountNoFT.Text
                    objICCollections.PayersAccountBranchCode = txtFromAccountBranchCode.Text
                    objICCollections.PayersAccountCurrency = txtFromAccountCurrency.Text
                    objICCollections.PayersAccountType = "RB"
                    objICCollections.Status = 3
                    objICCollections.LastStatus = 3
                    objICCollections.ClearingValueDate = Nothing
                ElseIf objICCollectionProduct.ClearingType = "Inter City" Then
                    objICCollections.ClearingAccountNo = objICUser.UpToICOfficeByOfficeCode.InterCityOutwardClearingAccountNumber
                    objICCollections.ClearingAccountBranchCode = objICUser.UpToICOfficeByOfficeCode.OfficeCode
                    objICCollections.ClearingAccountCurrency = objICUser.UpToICOfficeByOfficeCode.InterCityOutwardClearingAccountBranchCode
                    objICCollections.ClearingCBAccountType = objICUser.UpToICOfficeByOfficeCode.InterCityOutwardClearingAccountCurrency
                    objICCollections.ClearingValueDate = Date.Now
                    objICCollections.Status = 8
                    objICCollections.LastStatus = 8
                ElseIf objICCollectionProduct.ClearingType = "Same Day" Then
                    objICCollections.ClearingAccountNo = objICUser.UpToICOfficeByOfficeCode.SameDayOutwardClearingAccountNumber
                    objICCollections.ClearingAccountBranchCode = objICUser.UpToICOfficeByOfficeCode.OfficeCode
                    objICCollections.ClearingAccountCurrency = objICUser.UpToICOfficeByOfficeCode.SameDayOutwardClearingAccountCurrency
                    objICCollections.ClearingCBAccountType = objICUser.UpToICOfficeByOfficeCode.SameDayOutwardClearingAccountType
                    objICCollections.ClearingValueDate = Date.Now
                    objICCollections.Status = 8
                    objICCollections.LastStatus = 8
                ElseIf objICCollectionProduct.ClearingType = "Normal" Then
                    objICCollections.ClearingAccountNo = objICUser.UpToICOfficeByOfficeCode.NormalOutwardClearingAccountNumber
                    objICCollections.ClearingAccountBranchCode = objICUser.UpToICOfficeByOfficeCode.OfficeCode
                    objICCollections.ClearingAccountCurrency = objICUser.UpToICOfficeByOfficeCode.NormalOutwardClearingAccountCurrency
                    objICCollections.ClearingCBAccountType = objICUser.UpToICOfficeByOfficeCode.NormalOutwardClearingAccountType
                    objICCollections.ClearingValueDate = Date.Now
                    objICCollections.Status = 8
                    objICCollections.LastStatus = 8
                ElseIf objICCollectionProduct.ClearingType = "OBC" Then
                    objICCollections.ClearingAccountNo = objICUser.UpToICOfficeByOfficeCode.OBCOutwardClearingAccountNumber
                    objICCollections.ClearingAccountBranchCode = objICUser.UpToICOfficeByOfficeCode.OfficeCode
                    objICCollections.ClearingAccountCurrency = objICUser.UpToICOfficeByOfficeCode.OBCOutwardClearingAccountCurrency
                    objICCollections.ClearingCBAccountType = objICUser.UpToICOfficeByOfficeCode.OBCOutwardClearingAccountType
                    objICCollections.ClearingValueDate = Date.Now
                    objICCollections.Status = 8
                    objICCollections.LastStatus = 8
                End If
            ElseIf objICCollectionProduct.DisbursementMode = "Fund Transfer" Then
                objICCollections.PayersAccountNo = txtFromAccountNoFT.Text
                objICCollections.PayersAccountBranchCode = txtFromAccountBranchCode.Text
                objICCollections.PayersAccountCurrency = txtFromAccountCurrency.Text
                objICCollections.PayersAccountType = "RB"
                objICCollections.Status = 3
                objICCollections.LastStatus = 3
                objICCollections.ClearingValueDate = Date.Now
            End If
        End If
        objICCollections.CollectionAccountNo = ddlCollNature.SelectedValue.ToString.Split("-")(0)
        objICCollections.CollectionAccountBranchCode = ddlCollNature.SelectedValue.ToString.Split("-")(1)
        objICCollections.CollectionAccountCurrency = ddlCollNature.SelectedValue.ToString.Split("-")(2)
        objICCollections.CollectionAccountType = "RB"
        objICCollections.CollectionAmount = CDbl(txtAmount.Text)
        If txtRemarks.Text <> "" Then
            objICCollections.CollectionRemarks = txtRemarks.Text
        End If

        objICCollections.CollectionLocationCode = objICUser.OfficeCode
        objICCollections.CollectionCreatedBy = UserId
        objICCollections.CollectionMode = objICCollectionProduct.DisbursementMode
        objICCollections.CollectionClientCode = objICCollectionNature.UpToICCompanyByCompanyCode.CompanyCode
        objICCollections.CollectionNatureCode = objICCollectionNature.CollectionNatureCode
        objICCollections.ReferenceField1 = RefField1
        objICCollections.ReferenceField2 = RefField2
        objICCollections.ReferenceField3 = RefField3
        objICCollections.ReferenceField4 = RefField4
        objICCollections.ReferenceField5 = RefField5
        objICCollections.CollectionClearingType = objICCollectionProduct.ClearingType
        If DueDate IsNot Nothing Then
            objICCollections.DueDate = DueDate
        Else
            objICCollections.DueDate = Nothing
        End If
        objICCollections.AmountAfterDueDate = AmountAfterDueDate
        objICCollections.FileID = FileID
        objICCollections.CreatedOfficeCode = objICUser.OfficeCode
        objICCollections.InstrumentDate = dpInstrumentDate.SelectedDate
        If rbPrintedFrom.SelectedValue = "Soneri Trans@ct" Then
            objICCollections.IsPrintedNextCash = True
        Else
            objICCollections.IsPrintedNextCash = False
        End If
        If ddlDrawnOnBranch.SelectedValue.ToString <> "0" And ddlDrawnOnBranch.SelectedValue.ToString <> "" Then
            objICCollections.DrawnOnBranchCode = ddlDrawnOnBranch.SelectedValue.ToString
        Else
            objICCollections.DrawnOnBranchCode = Nothing
        End If

        Return objICCollections
    End Function
    Private Sub SetReferenceFieldValuesByTemplateFieldsByGridRow(ByRef Ref1 As String, ByRef Ref2 As String, ByRef Ref3 As String, ByRef Ref4 As String, ByRef Ref5 As String, ByVal gvRow As GridDataItem)
        Dim objICInvoiceDBTemplateFields As New ICCollectionInvoiceDataTemplateFieldsCollection

        objICInvoiceDBTemplateFields = ICCollectionInvoiceDataTemplateFieldsListController.GetReferenceFieldsCollectionInvoiceTemplateOfDualFile(gvRow.Item("TemplateID").Text.ToString)
        For i = 0 To objICInvoiceDBTemplateFields.Count - 1
            Select Case i.ToString
                Case "0"

                    Ref1 = gvRow.Item(objICInvoiceDBTemplateFields(i).FieldName.ToString).Text.ToString

                Case "1"

                    Ref2 = gvRow.Item(objICInvoiceDBTemplateFields(i).FieldName.ToString).Text.ToString

                Case "2"

                    Ref3 = gvRow.Item(objICInvoiceDBTemplateFields(i).FieldName.ToString).Text.ToString

                Case "3"

                    Ref4 = gvRow.Item(objICInvoiceDBTemplateFields(i).FieldName.ToString).Text.ToString

                Case "4"

                    Ref5 = gvRow.Item(objICInvoiceDBTemplateFields(i).FieldName.ToString).Text.ToString

            End Select
        Next
    End Sub
    Protected Sub btnBack2_Click(sender As Object, e As EventArgs) Handles btnBack2.Click
        Try
            Response.Redirect(NavigateURL())
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckgvBankBranchForProcessAll() As Boolean
        Try
            Dim rowgvBankBranch As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvBankBranch In gvInvoiceRecords.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvBankBranch.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function CheckValidStatus(ByVal ToStatus As String) As String
        Try
            Dim rowgvBankBranch As GridDataItem
            Dim RowCount As Integer = 0
            Dim Result As Boolean = False
            Dim Msg As String = ""
            For Each rowgvBankBranch In gvInvoiceRecords.Items
                RowCount = RowCount + 1
                If rowgvBankBranch.GetDataKeyValue("Status") = ToStatus Then

                    Msg += "From and To Satus are same in row " & RowCount & "<br />"
                    Return Msg
                    Exit Function
                End If
            Next
            If Msg = "" Then
                Msg = "OK"
            End If
            Return Msg
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnUpDate_Click(sender As Object, e As EventArgs) Handles btnPay.Click
        Try
            Dim objICCollectionNature As New ICCollectionNature
            Dim objICInstructions As New ICInstructionCollection
            Dim objICCInstruction As New ICInstruction
            Dim ResultString As String = Nothing
            Dim objICCollections As New ICCollections
            Dim AmountAfterDueDate As Double = 0
            Dim DueDate As Date = Nothing
            Dim FileID As String = Nothing
            objICCollectionNature.LoadByPrimaryKey(ddlCollNature.SelectedValue.ToString.Split("-")(3))
            Dim Ref1, Ref2, Ref3, Ref4, Ref5 As String
            Dim FailErrorMessage2 As String = Nothing
            Dim RowCount As Integer = 1
            If txtAmount.Text = "" Then
                UIUtilities.ShowDialog(Me, "Collection Teller", "Invalid amount", Dialogmessagetype.Failure)
                Exit Sub
            End If
            If ddlCollProduct.SelectedValue.ToString = "0" Then
                UIUtilities.ShowDialog(Me, "Collection Teller", "Please select collection product", Dialogmessagetype.Failure)
                Exit Sub
            End If
            Ref1 = Nothing
            Ref2 = Nothing
            Ref3 = Nothing
            Ref4 = Nothing
            Ref5 = Nothing
            If objICCollectionNature.IsInvoiceDBAvailable = True Then
                SetReferenceFieldValuesByTemplateFieldsByGridRow(Ref1, Ref2, Ref3, Ref4, Ref5, gvInvoiceRecords.Items(0))
                FailErrorMessage2 = ICCollectionsController.VerifyEnteredAmount(objICCollectionNature, Ref1, Ref2, Ref3, Ref4, Ref5, CDbl(txtAmount.Text), RowCount)
                If FailErrorMessage2 <> "OK" Then
                    UIUtilities.ShowDialog(Me, "Collection Teller", "Following Invoices are not valid due to following reason(s):<br />" & FailErrorMessage2, ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                AmountAfterDueDate = CDbl(gvInvoiceRecords.Items(0).Item(objICCollectionNature.UpToICCollectionInvoiceDataTemplateFieldsByAmountAfterDueDateFieldID.FieldName).Text)
                DueDate = CDate(gvInvoiceRecords.Items(0).Item(objICCollectionNature.UpToICCollectionInvoiceDataTemplateFieldsByDueDateFieldID.FieldName).Text)
                FileID = gvInvoiceRecords.Items(0).Item("FileID").Text
                objICCollections = GetCollectionObject(Ref1, Ref2, Ref3, Ref4, Ref5, AmountAfterDueDate, DueDate, FileID)
                If objICCollections.UpToICCollectionProductTypeByCollectionProductTypeCode.DisbursementMode = "Pay Order" And objICCollections.IsPrintedNextCash = True Then
                    ResultString = ICInstructionProcessController.GetInstructionForCollectionVerification("PO-" & objICCollections.InstrumentNo, "")
                    If ResultString.ToString().Contains("|") Then
                        Dim collInstruction As New ICInstructionCollection
                        collInstruction = ICInstructionController.GetInstructionForClearing(ResultString.ToString().Split("|")(1).ToString())
                        If collInstruction(0).Amount <> objICCollections.CollectionAmount Then
                            UIUtilities.ShowDialog(Me, "Collection Teller", "Amount do not match .Instrument issue amount is " & CDbl(collInstruction(0).Amount).ToString("N2"), ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Collection Teller", ResultString.ToString(), ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                If "OK" <> ValidateAccountsVIAProduct(objICCollections) Then
                    UIUtilities.ShowDialog(Me, "Collection Teller", ValidateAccountsVIAProduct(objICCollections), Dialogmessagetype.Success, NavigateURL)
                    Exit Sub
                End If
                ClearPreviewLabels()
                SetPreviewLabelsText(objICCollections)
                ViewState("Collections") = Nothing
                ViewState("Collections") = objICCollections
                tblPreview.Style.Remove("display")
                tblResult.Style.Add("display", "none")
                tblAmountAndCompanyDetails.Style.Add("display", "none")
                RadAmountComment.Enabled = False
                RadAmountComment.GetSettingByBehaviorID("reAmountPKR").Validation.IsRequired = False
                RadAmountComment.GetSettingByBehaviorID("radCSTName").Validation.IsRequired = False
                rfvddlCollProduct.Enabled = False
                tblFromBranch.Style.Add("display", "none")
                rfvddlBank.Enabled = False
                tblInstrumentDetails.Style.Add("display", "none")
                rimInstrumentDetails.Enabled = False
                tblFundsTransfer.Style.Add("display", "none")
                rimFundsTransfer.Enabled = False
                tblReferenceField.Style.Add("display", "none")
                'Try
                '    ICCollectionsController.ReceiveCollections(ddlCollProduct.SelectedValue.ToString.Split("-")(0), Me.UserInfo.UserID.ToString, objICCollections, "Collection", True, gvInvoiceRecords.Items(0).Item("Status").Text.ToString)
                '    UIUtilities.ShowDialog(Me, "Collection Teller", "Collection added successfully", Dialogmessagetype.Success, NavigateURL)
                '    Exit Sub
                'Catch ex As Exception
                '    UIUtilities.ShowDialog(Me, "Collection Teller", "Fail to perform collection due to: <br />" & ex.Message.ToString, Dialogmessagetype.Failure)
                '    Exit Sub
                'End Try
            Else

                SetReferenceFieldValuesByTemplateFieldsByTextBoxes(Ref1, Ref2, Ref3, Ref4, Ref5, objICCollectionNature.InvoiceTemplateID.ToString)
                objICCollections = GetCollectionObject(Ref1, Ref2, Ref3, Ref4, Ref5, Nothing, Nothing, Nothing)
                If objICCollections.UpToICCollectionProductTypeByCollectionProductTypeCode.DisbursementMode = "Pay Order" And objICCollections.IsPrintedNextCash = True Then
                    ResultString = ICInstructionProcessController.GetInstructionForCollectionVerification("PO-" & objICCollections.InstrumentNo, "")
                    If ResultString.ToString().Contains("|") Then
                        Dim collInstruction As New ICInstructionCollection
                        collInstruction = ICInstructionController.GetInstructionForClearing(ResultString.ToString().Split("|")(1).ToString())
                        If collInstruction(0).Amount <> objICCollections.CollectionAmount Then
                            UIUtilities.ShowDialog(Me, "Collection Teller", "Amount do not match. Instrument issue amount is " & CDbl(collInstruction(0).Amount).ToString("N2"), ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Collection Teller", ResultString.ToString(), ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                If "OK" <> ValidateAccountsVIAProduct(objICCollections) Then
                    UIUtilities.ShowDialog(Me, "Collection Teller", ValidateAccountsVIAProduct(objICCollections), Dialogmessagetype.Success, NavigateURL)
                    Exit Sub
                End If
                ClearPreviewLabels()
                SetPreviewLabelsText(objICCollections)
                ViewState("Collections") = Nothing
                ViewState("Collections") = objICCollections
                tblPreview.Style.Remove("display")
                tblAmountAndCompanyDetails.Style.Add("display", "none")
                RadAmountComment.Enabled = False
                RadAmountComment.GetSettingByBehaviorID("reAmountPKR").Validation.IsRequired = False
                RadAmountComment.GetSettingByBehaviorID("radCSTName").Validation.IsRequired = False
                rfvddlCollProduct.Enabled = False
                tblResult.Style.Add("display", "none")
                tblFromBranch.Style.Add("display", "none")
                rfvddlBank.Enabled = False
                tblInstrumentDetails.Style.Add("display", "none")
                rimInstrumentDetails.Enabled = False
                tblFundsTransfer.Style.Add("display", "none")
                rimFundsTransfer.Enabled = False
                tblReferenceField.Style.Add("display", "none")


                'Try
                '    ICCollectionsController.ReceiveCollections(ddlCollProduct.SelectedValue.ToString.Split("-")(0), Me.UserInfo.UserID.ToString, objICCollections, "Collection", False, objICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                '    UIUtilities.ShowDialog(Me, "Collection Teller", "Collection added successfully", Dialogmessagetype.Success, NavigateURL)
                '    Exit Sub
                'Catch ex As Exception
                '    UIUtilities.ShowDialog(Me, "Collection Teller", "Fail to perform collection due to: <br />" & ex.Message.ToString, Dialogmessagetype.Failure)
                '    Exit Sub
                'End Try
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function ValidateAccountsVIAProduct(ByVal objICCollection As ICCollections) As String
        Dim objICCollProduct As New ICCollectionProductType
        Dim objICInstructionColl As New ICInstructionCollection
        Dim objICCinstruction As New ICInstruction
        Dim objICCuser As New ICUser
        Dim objICOffice As New ICOffice
        Dim Result As String = "OK"
        Dim ResultString As String = Nothing
        objICCuser.LoadByPrimaryKey(Me.UserInfo.UserID.ToString)
        objICCollProduct.LoadByPrimaryKey(objICCollection.CollectionProductTypeCode)
        If objICCollProduct.DisbursementMode = "Cash" Then
            If objICCuser.UpToICOfficeByOfficeCode.BranchCashTillAccountNumber Is Nothing Then
                Result = "Branch Cash Till Account is not tagged"
                Return Result
                Exit Function
            End If
        ElseIf objICCollProduct.DisbursementMode = "Pay Order" Then
            If objICCollProduct.ClearingType = "Funds Transfer" Then
                If ICUtilities.GetCollectionSettingValue("POPayableAccount") = "Central" Then
                    If ICUtilities.GetCollectionSettingValue("POPayableAccountNumber") = "" Then
                        Throw New Exception("Central PO Payable account is not defined")
                    End If
                Else
                    If rbPrintedFrom.SelectedValue = "Soneri Trans@ct" Then
                        ResultString = ICInstructionProcessController.GetInstructionForCollectionVerification("PO-" & objICCollection.InstrumentNo, "")
                        If ResultString.ToString().Contains("|") Then
                            Dim collInstruction As New ICInstructionCollection
                            collInstruction = ICInstructionController.GetInstructionForClearing(ResultString.ToString().Split("|")(1).ToString())
                            If collInstruction.Count = 0 Then
                                Result = ""
                                Result = "Invalid Instrument no from Soneri Trans@ct"
                                Return Result
                                Exit Function
                            End If
                        Else
                            Result = ""
                            Result = ResultString
                            Return Result
                            Exit Function
                        End If
                    Else
                        objICOffice.LoadByPrimaryKey(objICCollection.DrawnOnBranchCode.ToString)
                        If objICOffice.POPayableAccountNumber Is Nothing Then
                            Result = ""
                            Result = "PO Payable account is not tagged with branch"
                            Return Result
                            Exit Function
                        End If
                    End If
                End If
            End If
        ElseIf objICCollProduct.DisbursementMode = "Demand Draft" Then
            If objICCuser.UpToICOfficeByOfficeCode.DDPayableAccountNumber Is Nothing Then
                Result = ""
                Result = "DD payable account is not tagged"
                Return Result
                Exit Function
            End If
        ElseIf objICCollProduct.DisbursementMode = "Cheque" Then
          If objICCollProduct.ClearingType = "Inter City" Then
                If objICCuser.UpToICOfficeByOfficeCode.InterCityOutwardClearingAccountNumber Is Nothing Then
                    Result = ""
                    Result = "Inter City Out Ward Clearing account is not tagged"
                    Return Result
                    Exit Function
                End If
            ElseIf objICCollProduct.ClearingType = "Same Day" Then
                If objICCuser.UpToICOfficeByOfficeCode.SameDayOutwardClearingAccountNumber Is Nothing Then
                    Result = ""
                    Result = "Same day Out Ward Clearing account is not tagged"
                    Return Result
                    Exit Function
                End If
            ElseIf objICCollProduct.ClearingType = "OBC" Then
                If objICCuser.UpToICOfficeByOfficeCode.OBCOutwardClearingAccountNumber Is Nothing Then
                    Result = ""
                    Result = "OBC Out Ward Clearing account is not tagged"
                    Return Result
                    Exit Function
                End If
            ElseIf objICCollProduct.ClearingType = "Normal" Then
                If objICCuser.UpToICOfficeByOfficeCode.NormalOutwardClearingAccountNumber Is Nothing Then
                    Result = ""
                    Result = "Normal Out Ward Clearing account is not tagged"
                    Return Result
                    Exit Function
                End If
            End If
        End If
        Return Result
    End Function
    Private Sub SetPreviewLabelsText(ByVal objICCollections As ICCollections)

        If objICCollections.ReferenceField1 IsNot Nothing Then
            lblprvReferenceFields.Text = objICCollections.ReferenceField1 & "-"
        End If
        If objICCollections.ReferenceField2 IsNot Nothing Then
            lblprvReferenceFields.Text += objICCollections.ReferenceField2 & "-"
        End If
        If objICCollections.ReferenceField3 IsNot Nothing Then
            lblprvReferenceFields.Text += objICCollections.ReferenceField3 & "-"
        End If
        If objICCollections.ReferenceField4 IsNot Nothing Then
            lblprvReferenceFields.Text += objICCollections.ReferenceField4 & "-"
        End If
        If objICCollections.ReferenceField5 IsNot Nothing Then
            lblprvReferenceFields.Text += objICCollections.ReferenceField5 & "-"
        End If
        If objICCollections.CollectionAmount IsNot Nothing Then
            lblprvCollectionAmount.Text = CDbl(objICCollections.CollectionAmount).ToString("N2")
        Else
            lblprvCollectionAmount.Text = "-"
        End If
        lblprvCollPRoduct.Text = objICCollections.UpToICCollectionProductTypeByCollectionProductTypeCode.ProductTypeCode & "-" & objICCollections.UpToICCollectionProductTypeByCollectionProductTypeCode.ProductTypeName
        If objICCollections.CollectionClearingType IsNot Nothing Then
            lblPrvClearingType.Text = objICCollections.CollectionClearingType
        Else
            lblPrvClearingType.Text = "-"
        End If
        lblprvCustomerName.Text = objICCollections.PayersName
        If objICCollections.BankCode IsNot Nothing Then
            lblPrvBank.Text = objICCollections.UpToICBankByBankCode.BankName
        Else
            lblPrvBank.Text = "-"
        End If
        If objICCollections.InstrumentNo IsNot Nothing Then
            lblprvInstrumentNo.Text = objICCollections.InstrumentNo
        Else
            lblprvInstrumentNo.Text = ""
        End If
        If objICCollections.ClearingValueDate IsNot Nothing Then
            lblprvValueDate.Text = objICCollections.ClearingValueDate
        Else
            lblprvValueDate.Text = "-"
        End If
        If ddlLateClearing.SelectedValue.ToString <> "0" Then
            lblprvLateClearing.Text = ddlLateClearing.SelectedValue.ToString
        Else
            lblprvLateClearing.Text = "-"
        End If
        lblprvComments.Text = objICCollections.CollectionRemarks
    End Sub
    Private Sub SetReferenceFieldValuesByTemplateFieldsByTextBoxes(ByRef Ref1 As String, ByRef Ref2 As String, ByRef Ref3 As String, ByRef Ref4 As String, ByRef Ref5 As String, ByVal CollectionNatureCode As String)
        Dim objICInvoiceDBTemplateFields As New ICCollectionInvoiceDataTemplateFieldsCollection

        objICInvoiceDBTemplateFields = ICCollectionInvoiceDataTemplateFieldsListController.GetReferenceFieldsCollectionInvoiceTemplateOfDualFile(CollectionNatureCode)
        For i = 0 To objICInvoiceDBTemplateFields.Count - 1
            Select Case i.ToString
                Case "0"
                    If txtReferenceField1.Text <> "" Then
                        Ref1 = txtReferenceField1.Text.ToString.Trim
                    End If
                Case "1"
                    If txtReferenceField2.Text <> "" Then
                        Ref2 = txtReferenceField2.Text.ToString.Trim
                    End If
                Case "2"
                    If txtReferenceField3.Text <> "" Then
                        Ref3 = txtReferenceField3.Text.ToString.Trim
                    End If
                Case "3"
                    If txtReferenceField4.Text <> "" Then
                        Ref4 = txtReferenceField4.Text.ToString.Trim
                    End If
                Case "4"
                    If txtReferenceField5.Text <> "" Then
                        Ref5 = txtReferenceField5.Text.ToString.Trim
                    End If
            End Select
        Next
    End Sub
    Protected Sub txtAmount_TextChanged(sender As Object, e As EventArgs) Handles txtAmount.TextChanged
        Dim result As Double
        If txtAmount.Text.ToString() <> "" Then


            If txtAmount.Text.Contains(".") Then
                Dim str As String()
                Dim strBDeci, strADeci As String
                Dim TotAmt As String = ""
                str = txtAmount.Text.Split(".")
                strBDeci = str(0).ToString()
                strADeci = str(1).ToString()
                TotAmt = strBDeci & strADeci
                If TotAmt.Length > 13 Then
                    txtAmount.Text = Nothing
                    UIUtilities.ShowDialog(Me, "Collection - Teller", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If strBDeci.Length > 11 Then
                    txtAmount.Text = Nothing
                    UIUtilities.ShowDialog(Me, "Collection - Teller", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Else
                If txtAmount.Text.Length > 11 Then
                    txtAmount.Text = Nothing
                    UIUtilities.ShowDialog(Me, "Collection - Teller", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            End If

            Try
                If Double.TryParse(txtAmount.Text.Trim(), result) = False Then
                    txtAmount.Text = Nothing
                    UIUtilities.ShowDialog(Me, "Collection - Teller", "Invalid Amount - Amount cannot be egreatr than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                txtAmount.Text = Nothing
                UIUtilities.ShowDialog(Me, "Collection - Teller", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End Try


            If CDbl(txtAmount.Text.ToString()) < 1 Then
                UIUtilities.ShowDialog(Me, "Collection - Teller", "Amount should be greater than or equal to 1.", ICBO.IC.Dialogmessagetype.Failure)
                txtAmount.Text = ""
                Exit Sub
            End If
            'Standard Format
            'txtAmountWords.Text = NumToWord.AmtInWord(CDbl(txtAmountPKR.Text.ToString())).TrimStart().TrimEnd().Trim().ToString()
            'txtAmountInWords.Text = NumToWord.SpellNumber(CDbl(txtAmountPKR.Text.ToString())).TrimStart().TrimEnd().Trim().ToString()
        End If
    End Sub

    Protected Sub ddlCollProduct_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCollProduct.SelectedIndexChanged
        Try
            SetInstrumentDetailsByProductTypeCode(ddlCollProduct.SelectedValue.ToString)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetInstrumentDetailsByProductTypeCode(ByVal ProductTypeCode As String)
        Dim objICCollProduct As New ICCollectionProductType
        Dim objICCuser As New ICUser
        rimFundsTransfer.Enabled = False
        rimInstrumentDetails.Enabled = False
        tblFundsTransfer.Style.Add("display", "none")
        ClearFundsTransferDetails()

        tblInstrumentDetails.Style.Add("display", "none")
        ddlBankFundTransfer.ClearSelection()
        DirectCast(Me.Control.FindControl("trFromAccount1"), HtmlTableRow).Style.Remove("display")
        DirectCast(Me.Control.FindControl("trFromAccount2"), HtmlTableRow).Style.Remove("display")

        DirectCast(Me.Control.FindControl("trPrintedFrom"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trPrintedFrom2"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trPrintedFrom3"), HtmlTableRow).Style.Add("display", "none")
        rfvPrintedFrom.Enabled = False
        rfvddlDrawnOnBranch.Enabled = False
        rbPrintedFrom.ClearSelection()

        rimInstrumentDetails.GetSettingByBehaviorID("radFromAccountNo").Validation.IsRequired = True
        rfvddlBank.Enabled = False
        ddlBankFundTransfer.Enabled = True
        tblFromBranch.Style.Add("display", "none")
        radFromBranch.Enabled = False
        RadAmountComment.GetSettingByBehaviorID("radCSTName").Validation.IsRequired = True
        lblReqCstName.Visible = True
        If ProductTypeCode <> "0" Then
            objICCuser.LoadByPrimaryKey(Me.UserInfo.UserID.ToString)
            objICCollProduct.LoadByPrimaryKey(ddlCollProduct.SelectedValue.ToString.Split("-")(0))
            If objICCollProduct.DisbursementMode = "Cash" Then
                If objICCuser.UpToICOfficeByOfficeCode.BranchCashTillAccountNumber Is Nothing Then
                    LoadddlProductTypes()
                    UIUtilities.ShowDialog(Me, "Collection Teller", "Branch Cash Till Account is not tagged", Dialogmessagetype.Failure)
                    Exit Sub
                Else
                    rimFundsTransfer.Enabled = False
                    tblFundsTransfer.Style.Add("display", "none")
                    rfvddlBank.Enabled = False
                    radFromBranch.Enabled = False
                    tblFromBranch.Style.Add("display", "none")
                End If
            ElseIf objICCollProduct.DisbursementMode = "Fund Transfer" Then
                rimFundsTransfer.Enabled = True
                tblFundsTransfer.Style.Remove("display")
                tblInstrumentDetails.Style.Add("display", "none")
                tblFromBranch.Style.Remove("display")
                radFromBranch.Enabled = True
                rfvddlBank.Enabled = True
                LoadddlAllBanks()
                rfvddlLateClearing.Enabled = True
                rfvInstrumentDate.Enabled = True
                rfvValueDate.Enabled = True
                SetLateClearingType(objICCollProduct.DisbursementMode.ToString)
                rimInstrumentDetails.Enabled = False
                rimInstrumentDetails.GetSettingByBehaviorID("radFromAccountNo").Validation.IsRequired = False
                DirectCast(Me.Control.FindControl("trFromAccount1"), HtmlTableRow).Style.Add("display", "none")
                DirectCast(Me.Control.FindControl("trFromAccount2"), HtmlTableRow).Style.Add("display", "none")
                RadAmountComment.GetSettingByBehaviorID("radCSTName").Validation.IsRequired = False
                lblReqCstName.Visible = False
                SetPrincipalBank()
            ElseIf objICCollProduct.DisbursementMode = "Pay Order" Then
                DirectCast(Me.Control.FindControl("trPrintedFrom"), HtmlTableRow).Style.Remove("display")
                DirectCast(Me.Control.FindControl("trPrintedFrom2"), HtmlTableRow).Style.Remove("display")
                DirectCast(Me.Control.FindControl("trPrintedFrom3"), HtmlTableRow).Style.Remove("display")
                rfvPrintedFrom.Enabled = True
                rfvddlDrawnOnBranch.Enabled = False
                SetDrawnBranchSelectionFromPOProduct(rbPrintedFrom.SelectedValue.ToString)
                tblInstrumentDetails.Style.Remove("display")
                tblFromBranch.Style.Remove("display")
                radFromBranch.Enabled = True
                rfvddlBank.Enabled = True
                rfvddlLateClearing.Enabled = True
                rfvInstrumentDate.Enabled = True
                rfvValueDate.Enabled = True
                LoadddlAllBanks()
                SetLateClearingType(objICCollProduct.DisbursementMode.ToString)
                rimInstrumentDetails.Enabled = True
                If objICCollProduct.ClearingType = "Funds Transfer" Then
                    SetPrincipalBank()
                    rimInstrumentDetails.GetSettingByBehaviorID("radFromAccountNo").Validation.IsRequired = False
                    DirectCast(Me.Control.FindControl("trFromAccount1"), HtmlTableRow).Style.Add("display", "none")
                    DirectCast(Me.Control.FindControl("trFromAccount2"), HtmlTableRow).Style.Add("display", "none")
                    RadAmountComment.GetSettingByBehaviorID("radCSTName").Validation.IsRequired = True
                    lblReqCstName.Visible = True
                End If
            ElseIf objICCollProduct.DisbursementMode = "Demand Draft" Then
                tblInstrumentDetails.Style.Remove("display")
                tblFromBranch.Style.Remove("display")
                radFromBranch.Enabled = True
                rfvddlBank.Enabled = True
                LoadddlAllBanks()
                rfvddlLateClearing.Enabled = True
                rfvInstrumentDate.Enabled = True
                rfvValueDate.Enabled = True
                SetLateClearingType(objICCollProduct.DisbursementMode.ToString)
                rimInstrumentDetails.Enabled = True
                If objICCollProduct.ClearingType = "Funds Transfer" Then
                    If Not objICCuser.UpToICOfficeByOfficeCode.DDPayableAccountNumber Is Nothing Then
                        SetPrincipalBank()
                        rimInstrumentDetails.GetSettingByBehaviorID("radFromAccountNo").Validation.IsRequired = False
                        DirectCast(Me.Control.FindControl("trFromAccount1"), HtmlTableRow).Style.Add("display", "none")
                        DirectCast(Me.Control.FindControl("trFromAccount2"), HtmlTableRow).Style.Add("display", "none")
                        RadAmountComment.GetSettingByBehaviorID("radCSTName").Validation.IsRequired = True
                        lblReqCstName.Visible = True
                    Else
                        LoadddlProductTypes()
                        UIUtilities.ShowDialog(Me, "Collection Teller", "DD payable account is not tagged", Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else
                    If objICCuser.UpToICOfficeByOfficeCode.DDPayableAccountNumber Is Nothing Then
                        LoadddlProductTypes()
                        UIUtilities.ShowDialog(Me, "Collection Teller", "DD payable account is not tagged", Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            ElseIf objICCollProduct.DisbursementMode = "Cheque" Then
                tblInstrumentDetails.Style.Remove("display")
                tblFromBranch.Style.Remove("display")
                radFromBranch.Enabled = True
                rfvddlBank.Enabled = True
                LoadddlAllBanks()
                rfvddlLateClearing.Enabled = True
                rfvInstrumentDate.Enabled = True
                rfvValueDate.Enabled = True
                SetLateClearingType(objICCollProduct.DisbursementMode.ToString)
                rimInstrumentDetails.Enabled = True
                If objICCollProduct.ClearingType = "Funds Transfer" Then
                    rimFundsTransfer.Enabled = True
                    tblFundsTransfer.Style.Remove("display")
                    ClearFundsTransferDetails()
                    rfvddlBank.Enabled = True
                    SetPrincipalBank()
                    rimInstrumentDetails.GetSettingByBehaviorID("radFromAccountNo").Validation.IsRequired = False
                    DirectCast(Me.Control.FindControl("trFromAccount1"), HtmlTableRow).Style.Add("display", "none")
                    DirectCast(Me.Control.FindControl("trFromAccount2"), HtmlTableRow).Style.Add("display", "none")
                    RadAmountComment.GetSettingByBehaviorID("radCSTName").Validation.IsRequired = False
                    lblReqCstName.Visible = False
                ElseIf objICCollProduct.ClearingType = "Inter City" Then
                    If objICCuser.UpToICOfficeByOfficeCode.InterCityOutwardClearingAccountNumber Is Nothing Then
                        LoadddlProductTypes()
                        UIUtilities.ShowDialog(Me, "Collection Teller", "Inter City Out Ward Clearing account is not tagged", Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                ElseIf objICCollProduct.ClearingType = "Same Day" Then
                    If objICCuser.UpToICOfficeByOfficeCode.SameDayOutwardClearingAccountNumber Is Nothing Then
                        LoadddlProductTypes()
                        UIUtilities.ShowDialog(Me, "Collection Teller", "Same day Out Ward Clearing account is not tagged", Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                ElseIf objICCollProduct.ClearingType = "OBC" Then
                    If objICCuser.UpToICOfficeByOfficeCode.OBCOutwardClearingAccountNumber Is Nothing Then
                        LoadddlProductTypes()
                        UIUtilities.ShowDialog(Me, "Collection Teller", "OBC Out Ward Clearing account is not tagged", Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                ElseIf objICCollProduct.ClearingType = "Normal" Then
                    If objICCuser.UpToICOfficeByOfficeCode.NormalOutwardClearingAccountNumber Is Nothing Then
                        LoadddlProductTypes()
                        UIUtilities.ShowDialog(Me, "Collection Teller", "Normal Out Ward Clearing account is not tagged", Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
        End If
    End Sub
    Private Sub ClearFundsTransferDetails()
        txtFromAccountNoFT.Text = ""
        txtFromAccountBranchCode.Text = ""
        txtFromAccountCurrency.Text = ""
        txtFromAccountTitle.Text = ""
        txtFromAccountNo.Text = ""
        txtFromBranchName.Text = ""
        dpInstrumentDate.SelectedDate = Date.Now
        dpValueDate.SelectedDate = Date.Now
        txtCustomerName.Text = ""
        txtFromBranchName.Text = ""
        rbPrintedFrom.ClearSelection()
        ddlBankFundTransfer.ClearSelection()
        ddlDrawnOnBranch.ClearSelection()
        txtInstrumentNo.Text = ""
        txtRemarks.Text = ""
    End Sub
    Protected Sub txtFromAccountNoFT_TextChanged(sender As Object, e As EventArgs) Handles txtFromAccountNoFT.TextChanged
        Try
            Dim StrAccountDetails As String()
            Dim AccountStatus As String = ""
            If Not txtFromAccountNoFT.Text.ToString = "" And Not txtFromAccountNoFT.Text.ToString = "Enter Beneficiary Account Number" Then
                Try
                    AccountStatus = CBUtilities.AccountStatus(txtFromAccountNoFT.Text.ToString, ICBO.CBUtilities.AccountType.RB, "IC Beneficiary Account", txtFromAccountNoFT.ToString, "Credit", "Collection Teller").ToString
                Catch ex As Exception
                    UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End Try
                If AccountStatus = "Active" Then
                    Try
                        StrAccountDetails = (CBUtilities.TitleFetch(txtFromAccountNoFT.Text.ToString, ICBO.CBUtilities.AccountType.RB, "IC Beneficiary Account", txtFromAccountNoFT.Text.ToString).ToString.Split(";"))
                        txtFromAccountTitle.Text = StrAccountDetails(0).ToString
                        txtFromAccountCurrency.Text = StrAccountDetails(2).ToString
                        txtFromAccountBranchCode.Text = StrAccountDetails(1).ToString
                    Catch ex As Exception
                        UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End Try

                Else
                    UIUtilities.ShowDialog(Me, "Error", AccountStatus.ToString, ICBO.IC.Dialogmessagetype.Failure)
                    txtFromAccountNoFT.Text = ""
                    txtFromAccountCurrency.Text = ""
                    txtFromAccountTitle.Text = ""
                    txtFromAccountBranchCode.Text = ""
                    Exit Sub
                End If
            Else

                'UIUtilities.ShowDialog(Me, "Error", "Please enter valid account number.", ICBO.IC.Dialogmessagetype.Failure)
                txtFromAccountNoFT.Text = ""
                txtFromAccountCurrency.Text = ""
                txtFromAccountTitle.Text = ""
                txtFromAccountBranchCode.Text = ""
                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnProceed_Click(sender As Object, e As EventArgs) Handles btnProceed.Click
        Try
            Dim objICCollectionNature As New ICCollectionNature
            Dim objICCollections As New ICCollections
            objICCollections = DirectCast(ViewState("Collections"), ICCollections)
            objICCollectionNature.LoadByPrimaryKey(objICCollections.CollectionNatureCode.ToString)
            If objICCollectionNature.IsInvoiceDBAvailable = True Then
                Try
                    ICCollectionsController.ReceiveCollections(objICCollections.CollectionProductTypeCode.ToString, Me.UserInfo.UserID.ToString, objICCollections, "Collection", True, gvInvoiceRecords.Items(0).Item("Status").Text.ToString)
                    UIUtilities.ShowDialog(Me, "Collection Teller", "Collection added successfully", Dialogmessagetype.Success, NavigateURL)
                    Exit Sub
                Catch ex As Exception
                    UIUtilities.ShowDialog(Me, "Collection Teller", "Fail to perform collection due to: <br />" & ex.Message.ToString, Dialogmessagetype.Failure)
                    Exit Sub
                End Try
            Else
                Try
                    ICCollectionsController.ReceiveCollections(objICCollections.CollectionProductTypeCode.ToString, Me.UserInfo.UserID.ToString, objICCollections, "Collection", False, objICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString)
                    UIUtilities.ShowDialog(Me, "Collection Teller", "Collection added successfully", Dialogmessagetype.Success, NavigateURL)
                    Exit Sub
                Catch ex As Exception
                    UIUtilities.ShowDialog(Me, "Collection Teller", "Fail to perform collection due to: <br />" & ex.Message.ToString, Dialogmessagetype.Failure)
                    Exit Sub
                End Try
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnCancelProced_Click(sender As Object, e As EventArgs) Handles btnCancelProced.Click
        Try
            Dim objICCollections As New ICCollections
            objICCollections = DirectCast(ViewState("Collections"), ICCollections)
            SetReferenceFieldVisibility(objICCollections.CollectionNatureCode.ToString)
            SetInstrumentDetailsByProductTypeCode(objICCollections.CollectionProductTypeCode.ToString)
            tblPreview.Style.Add("display", "none")
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetDrawnBranchSelectionFromPOProduct(ByVal PrintedFrom As String)
        If rbPrintedFrom.SelectedValue.ToString = "Soneri Trans@ct" Then
            lblDrawnOnBranch.Visible = False
            lblReqDrawnOnBranch.Visible = False
            ddlDrawnOnBranch.Visible = False
            ddlDrawnOnBranch.ClearSelection()
            rfvddlDrawnOnBranch.Enabled = False
        Else
            If ICUtilities.GetCollectionSettingValue("POPayableAccount") <> "Central" Then
                lblDrawnOnBranch.Visible = True
                lblReqDrawnOnBranch.Visible = True
                ddlDrawnOnBranch.Visible = True
                LoadddlPrincipalBankBranches()
                rfvddlDrawnOnBranch.Enabled = True
            Else
                lblDrawnOnBranch.Visible = False
                lblReqDrawnOnBranch.Visible = False
                ddlDrawnOnBranch.Visible = False
                ddlDrawnOnBranch.ClearSelection()
                rfvddlDrawnOnBranch.Enabled = False
            End If
        End If
    End Sub
    Protected Sub rbPrintedFrom_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbPrintedFrom.SelectedIndexChanged
        Try
            SetDrawnBranchSelectionFromPOProduct(rbPrintedFrom.SelectedValue.ToString())
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
