﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CollectionTeller.ascx.vb"
    Inherits="DesktopModules_CollectionTeller_CollectionTeller" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .auto-style1 {
        width: 25%;
    }
    .auto-style2 {
        width: 25%;
        height: 23px;
    }
</style>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }
    function conVerify() {
        if (window.confirm("Are you sure to proceed?") == true) {
            var btnVerify = document.getElementById('<%=btnPay.ClientID%>')
            btnVerify.value = "Processing...";
            btnVerify.disabled = true;

            var a = btnVerify.name;
            __doPostBack(a, '');
            return true;
        }
        else {
            return false;
        }
    }

    function ValidateChkList(source, arguments) {

        arguments.IsValid = IsCheckBoxChecked() ? true : false;



    }


    $(document).ready(function () {

        // Dialog
        $('#urldialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#loading').hide();



            },
            resizable: false
        });
       


    });
    function AdjustWidth(ddl) {
        var maxWidth = 0;
        var Counter = 0;
        for (var i = 0; i < ddl.length; i++) {
            if (ddl.options[i].text.length > maxWidth) {
                Counter = Counter + 1;
                maxWidth = ddl.options[i].text.length;
            }
        }
        if (Counter = 1) { ddl.style.width = "250px"; }
        
        else {
            alert(Counter);
            ddl.style.width = maxWidth + "px";
        }
        //-->
    }
    function showurldialog(title, url, modal, width, height) {
        //  alert(modal);
        $('#urldialog').dialog("option", "title", title);
        $('#urldialog').dialog("option", "modal", modal);
        $('#urldialog').dialog("option", "width", width);
        $('#urldialog').dialog("option", "height", height);
        $('#urldialog').dialog('open');
        $('#dialogiframe').hide();
        $('#loading').show();
        $('#dialogiframe').attr("src", url);
        $('#dialogiframe').load(function () {
            $('#loading').hide();
            $('#dialogiframe').show();
        });
        return false;

    };

    function CloseIframe() {
        $('#urldialog').dialog('close');
    }
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
            }
        }
    }
</script>

<%--<style type="text/css">
.ctrDropDown{
    width:250px;
   
}
.ctrDropDownClick{
    
    width:600px;
 
}
.plainDropDown{
    width:250px;
   
}
    .auto-style1 {
        width: 25%;
        height: 26px;
    }
    .RadPicker{vertical-align:middle}.rdfd_{position:absolute}.RadPicker .rcTable{table-layout:auto}.RadPicker .RadInput{vertical-align:baseline}.RadInput_Default{font:12px "segoe ui",arial,sans-serif}.riSingle{box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;-webkit-box-sizing:border-box;-khtml-box-sizing:border-box}.riSingle{position:relative;display:inline-block;white-space:nowrap;text-align:left}.RadInput{vertical-align:middle}.riSingle .riTextBox{box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;-webkit-box-sizing:border-box;-khtml-box-sizing:border-box}.RadPicker_Default .rcCalPopup{background-position:0 0}.RadPicker_Default .rcCalPopup{background-image:url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Calendar.sprite.gif')}.RadPicker .rcCalPopup{display:block;overflow:hidden;width:22px;height:22px;background-color:transparent;background-repeat:no-repeat;text-indent:-2222px;text-align:center}
    .auto-style2 {
        width: 100%;
        border-style: none;
        border-color: inherit;
        border-width: 0;
        margin: 0;
        padding: 0;
    }
    .auto-style3 {
        width: 100%;
        vertical-align: middle;
    }
    .auto-style4 {
        position: relative;
        z-index: 2;
        text-decoration: none;
        margin: 0 2px;
    }
    </style>--%>
<telerik:RadInputManager ID="RadAmountComment" runat="server" Enabled="false">
    
    <telerik:TextBoxSetting BehaviorID="reComments" Validation-IsRequired="false" Validation-ValidationGroup="Search"
        ErrorMessage="Invalid Remarks" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRemarks" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:NumericTextBoxSetting BehaviorID="reAmountPKR" EmptyMessage="" AllowRounding="false"
                                                            Validation-IsRequired="false" Type="Number" DecimalDigits="2" ErrorMessage="Invalid amount"
                                                            Validation-ValidationGroup="Search">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtAmount" />
                                                            </TargetControls>
                                                        </telerik:NumericTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radCSTName" Validation-IsRequired="true"
        EmptyMessage="" ErrorMessage="Invalid Customer Name" Validation-ValidationGroup="Search">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCustomerName" />
        </TargetControls>
    </telerik:TextBoxSetting>


    
</telerik:RadInputManager>
<telerik:RadInputManager ID="rimFundsTransfer" runat="server" Enabled="False">    
    <telerik:RegExpTextBoxSetting BehaviorID="radAccountNo" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9]+$" ErrorMessage="Invalid Account No."
        EmptyMessage="" Validation-ValidationGroup="Search">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFromAccountNoFT" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radBeneAccountTitle" Validation-IsRequired="true"
        EmptyMessage="" ErrorMessage="Invalid Account Title" Validation-ValidationGroup="Search">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFromAccountTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radBeneAccountBranchCode" Validation-IsRequired="true"
        EmptyMessage="" ErrorMessage="Invalid Account Branch Code" Validation-ValidationGroup="Search">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFromAccountBranchCode" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radBeneAccountCurrency" Validation-IsRequired="true"
        EmptyMessage="" ErrorMessage="Invalid Account Currency" Validation-ValidationGroup="Search">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFromAccountCurrency" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<telerik:RadInputManager ID="rimInstrumentDetails" runat="server" Enabled="False">    
    <telerik:RegExpTextBoxSetting BehaviorID="radFromAccountNo" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9]+$" ErrorMessage="Invalid Account No."
        EmptyMessage="" Validation-ValidationGroup="Search">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFromAccountNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="radInstrumentNo" Validation-IsRequired="true"
        ErrorMessage="Invalid Instrument No." ValidationExpression="[0-9]{1,30}" Validation-ValidationGroup="Search">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstrumentNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
</telerik:RadInputManager>
<telerik:RadInputManager ID="radFromBranch" runat="server" Enabled="False">    
    <telerik:TextBoxSetting BehaviorID="radFromAccountBranchCode" Validation-IsRequired="true"
        EmptyMessage="" ErrorMessage="Invalid Branch Name" Validation-ValidationGroup="Search">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFromBranchName" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
             <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue">Collection Teller</asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
          <table id="tblMain" runat="server" style="width:100%;">
         
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 28%">
            <asp:Label ID="lblCollNature" runat="server" Text="Select Collection Nature" CssClass="lbl"></asp:Label><asp:Label
                ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 22%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
       <asp:DropDownList ID="ddlCollNature" runat="server" class="dropdown" AutoPostBack="true">
            </asp:DropDownList>
            <%--<asp:DropDownList ID="ddlCollNature" runat="server" class="ctrDropDown"  onblur="this.className='ctrDropDown';" onmousedown="this.className='ctrDropDownClick';" onchange="this.className='ctrDropDown';" AutoPostBack="true">
            </asp:DropDownList>--%>
           
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" ControlToValidate="ddlCompany"
                Display="Dynamic" ErrorMessage="Please select Company" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 33%">
            <asp:RequiredFieldValidator ID="rfvddlCollNature" runat="server" ControlToValidate="ddlCollNature"
                Display="Dynamic" ErrorMessage="Please select Collection Nature" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 17%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;&nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" CausesValidation="False"
                Width="75px" />
            &nbsp;
        </td>
    </tr>
          </table></td>
    </tr>
      <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
           <table id="tblReferenceField" runat="server" style="width:100%;">
               <tr  align="left">
                   <td align="left" style="width:25%;">
            <asp:Label ID="lblPageHeader0" runat="server" CssClass="headingblue">Reference Field Detail</asp:Label>
                   </td>
                   <td align="left" style="width:25%;"></td>
                   <td align="left" style="width:25%;"></td>
                   <td align="left" style="width:25%;"></td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr id="trReferenceField1" runat="server" align="left">
                   <td align="center" style="width:25%;">
            <asp:Label ID="lblReferenceField1" runat="server" CssClass="lbl"></asp:Label></td>
                   <td align="left" style="width:25%;">
            <asp:TextBox ID="txtReferenceField1" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
                   </td>
                   <td align="left" style="width:25%;">
                       <asp:RegularExpressionValidator ID="rfvReferenceField1" runat="server" Display="Dynamic" SetFocusOnError="True" ControlToValidate="txtReferenceField1" ValidationGroup="Search"></asp:RegularExpressionValidator>
                   </td>
                   <td align="left" style="width:25%;">
<telerik:RadInputManager ID="RadInputReferenceField1" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="reReferenceFieldOne" Validation-IsRequired="false" ErrorMessage="Invalid Reference Field One" Validation-ValidationGroup="Search"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceField1" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
                   </td>
               </tr>
               <tr id="trReferenceField2" runat="server" align="left">
                   <td align="center"  style="width:25%;">
            <asp:Label ID="lblReferenceField2" runat="server" CssClass="lbl"></asp:Label></td>
                   <td align="left" style="width:25%;">
            <asp:TextBox ID="txtReferenceField2" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
                   </td>
                   <td align="left" style="width:25%;">
                       <asp:RegularExpressionValidator ID="rfvReferenceField2" runat="server" Display="Dynamic" SetFocusOnError="True" ControlToValidate="txtReferenceField2" ValidationGroup="Search"></asp:RegularExpressionValidator>
                   </td>
                   <td align="left" style="width:25%;">
<telerik:RadInputManager ID="RadInputReferenceField2" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="reReferenceFieldTwo" Validation-IsRequired="false" ErrorMessage="Invalid Reference Field Two" Validation-ValidationGroup="Search"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceField2" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
                   </td>
               </tr>
               <tr id="trReferenceField3" runat="server" align="left">
                   <td align="center"  style="width:25%;">
            <asp:Label ID="lblReferenceField3" runat="server" CssClass="lbl"></asp:Label></td>
                   <td align="left" style="width:25%;">
            <asp:TextBox ID="txtReferenceField3" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
                   </td>
                   <td align="left" style="width:25%;">
                       <asp:RegularExpressionValidator ID="rfvReferenceField3" runat="server" Display="Dynamic" SetFocusOnError="True" ControlToValidate="txtReferenceField3" ValidationGroup="Search"></asp:RegularExpressionValidator>
                   </td>
                   <td align="left" style="width:25%;">
<telerik:RadInputManager ID="RadInputReferenceField3" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="reReferenceFieldThree" Validation-IsRequired="false" ErrorMessage="Invalid Reference Field Three" Validation-ValidationGroup="Search"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceField3" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
                   </td>
               </tr>
               <tr id="trReferenceField4" runat="server" align="left">
                   <td align="center" style="width:25%;">
            <asp:Label ID="lblReferenceField4" runat="server" CssClass="lbl"></asp:Label></td>
                   <td align="left" style="width:25%;">
            <asp:TextBox ID="txtReferenceField4" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
                   </td>
                   <td align="left" style="width:25%;">
                       <asp:RegularExpressionValidator ID="rfvReferenceField4" runat="server" Display="Dynamic" SetFocusOnError="True" ControlToValidate="txtReferenceField4" ValidationGroup="Search"></asp:RegularExpressionValidator>
                   </td>
                   <td align="left" style="width:25%;">
<telerik:RadInputManager ID="RadInputReferenceField4" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="reReferenceFieldFour" Validation-IsRequired="false" ErrorMessage="Invalid Reference Field Four" Validation-ValidationGroup="Search"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceField4" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
                   </td>
               </tr>
               <tr  id="trReferenceField5" runat="server" align="left">
                   <td align="center"  style="width:25%;">
            <asp:Label ID="lblReferenceField5" runat="server" CssClass="lbl"></asp:Label></td>
                   <td align="left" style="width:25%;">
            <asp:TextBox ID="txtReferenceField5" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
                   </td>
                   <td align="left" style="width:25%;">
                       <asp:RegularExpressionValidator ID="rfvReferenceField5" runat="server" Display="Dynamic" SetFocusOnError="True" ControlToValidate="txtReferenceField5" ValidationGroup="Search"></asp:RegularExpressionValidator>
                   </td>
                   <td align="left" style="width:25%;">
<telerik:RadInputManager ID="RadInputReferenceField5" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="reReferenceFieldFive" Validation-IsRequired="false" ErrorMessage="Invalid Reference Field Five" Validation-ValidationGroup="Search"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceField" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
                   </td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr  align="left">
                   <td align="center" colspan="4"><asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnCancel"
                Width="75px" ValidationGroup="Search" />
                       <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btnCancel" CausesValidation="False"
                Width="75px" />
                   </td>
               </tr>
               
           </table>
        </td>
    </tr>
    <tr  align="left">
                   <td align="center" colspan="4">
                       <table id="tblAmountAndCompanyDetails" runat="server" style="width:100%;">
                           <tr align="left">
                               <td align="left" style="width:25%;">
                                   <asp:Label ID="Label3" runat="server" CssClass="headingblue">Invoice Details</asp:Label>
                               </td>
                               <td align="left" style="width:25%;">&nbsp;</td>
                               <td align="left" style="width:25%;">&nbsp;</td>
                               <td align="left" style="width:25%;">&nbsp;</td>
                           </tr>
                           <tr align="left">
                               <td align="left" style="width:25%;">&nbsp;</td>
                               <td align="left" style="width:25%;">&nbsp;</td>
                               <td align="left" style="width:25%;">&nbsp;</td>
                               <td align="left" style="width:25%;">&nbsp;</td>
                           </tr>
                           <tr align="left">
                               <td align="left" style="width:25%;">
                                   <asp:Label ID="lblAmount" runat="server" CssClass="lbl" Text="Amount"></asp:Label>
                                   <asp:Label ID="lblReqAmount" runat="server" ForeColor="Red" Text="*"></asp:Label>
                               </td>
                               <td align="left" style="width:25%;">&nbsp;</td>
                               <td align="left" style="width:25%;">
                                   <asp:Label ID="lblCollectionProduct" runat="server" CssClass="lbl" Text="Select Product"></asp:Label>
                                   <asp:Label ID="lblReqCollProduct" runat="server" ForeColor="Red" Text="*"></asp:Label>
                               </td>
                               <td align="left" style="width:25%;">&nbsp;</td>
                           </tr>
                           <tr align="left">
                               <td align="left" style="width:25%;">
                                   <asp:TextBox ID="txtAmount" runat="server" AutoPostBack="True" CssClass="txtbox" MaxLength="14"></asp:TextBox>
                               </td>
                               <td align="left" style="width:25%;">&nbsp;</td>
                               <td align="left" style="width:25%;">
            <asp:DropDownList ID="ddlCollProduct" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
                               </td>
                               <td align="left" style="width:25%;">&nbsp;</td>
                           </tr>
                           <tr align="left">
                               <td align="left" style="width:25%;">&nbsp;</td>
                               <td align="left" style="width:25%;">&nbsp;</td>
                               <td align="left" style="width:25%;">
            <asp:RequiredFieldValidator ID="rfvddlCollProduct" runat="server" ControlToValidate="ddlCollProduct"
                Display="Dynamic" ErrorMessage="Please select Product" InitialValue="0" SetFocusOnError="True" ValidationGroup="Search"></asp:RequiredFieldValidator>
                               </td>
                               <td align="left" style="width:25%;">&nbsp;</td>
                           </tr>
                           <tr align="left">
                               <td align="left" style="width:25%;">
                                   <asp:Label ID="lblCustomerName" runat="server" CssClass="lbl" Text="Customer Name"></asp:Label>
                                   <asp:Label ID="lblReqCstName" runat="server" ForeColor="Red" Text="*"></asp:Label>
                               </td>
                               <td align="left" style="width:25%;">&nbsp;</td>
                               <td align="left" style="width:25%;">
                                   <asp:Label ID="lblRemarks" runat="server" CssClass="lbl" Text="Comment"></asp:Label>
                               </td>
                               <td align="left" style="width:25%;">&nbsp;</td>
                           </tr>
                           <tr align="left">
                               <td align="left" style="width:25%;" valign="top">
                                   <asp:TextBox ID="txtCustomerName" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox>
                               </td>
                               <td align="left" style="width:25%;">&nbsp;</td>
                               <td align="left" style="width:25%;">
                                   <asp:TextBox ID="txtRemarks" runat="server" CssClass="txtbox" Height="60px" MaxLength="150" TextMode="MultiLine"></asp:TextBox>
                               </td>
                               <td align="left" style="width:25%;">&nbsp;</td>
                           </tr>
                           <tr align="left">
                               <td align="center" colspan="4">&nbsp;</td>
                           </tr>
                       </table>
                   </td>
               </tr>
    <tr align="left">
        <td align="left" valign="top" colspan="4">
            <table id="tblFromBranch" runat="server" style="width: 100%">
                 <tr align="left" style="width: 100%" valign="top">
                               <td align="left" style="width: 25%" valign="top">
                                   <asp:Label ID="Label5" runat="server" CssClass="lbl" Text="Select Bank"></asp:Label>
                                   <asp:Label ID="Label7" runat="server" ForeColor="Red" Text="*"></asp:Label>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                               <td align="left" style="width: 25%" valign="top">
                                   <asp:Label ID="lblFrmAccount0" runat="server" CssClass="lbl" Text="Branch Name"></asp:Label>
                                   <asp:Label ID="lblReqFrmBranchName" runat="server" ForeColor="Red" Text="*"></asp:Label>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                           </tr>
                           <tr id="tr3" runat="server" align="left" style="width: 100%" valign="top">
                               <td align="left" style="width: 25%" valign="top">
            <asp:DropDownList ID="ddlBankFundTransfer" runat="server" CssClass="dropdown">
            </asp:DropDownList>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                               <td align="left" style="width: 25%" valign="top">
                                   <asp:TextBox ID="txtFromBranchName" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                           </tr>
                           <tr align="left" style="width: 100%" valign="top">
                               <td align="left" style="width: 25%" valign="top">
            <asp:RequiredFieldValidator ID="rfvddlBank" runat="server" ControlToValidate="ddlBankFundTransfer"
                Display="Dynamic" ErrorMessage="Please select Bank" InitialValue="0" SetFocusOnError="True" ValidationGroup="Search"></asp:RequiredFieldValidator>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp; </td>
                               <td align="left" style="width: 25%" valign="top">
                                   <%--<asp:Label ID="Label10" runat="server" Text="*" ForeColor="Red"></asp:Label>--%>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp; </td>
                           </tr>
            </table>
        </td>
    </tr>
    <tr  align="left">
                   <td align="center" colspan="4">
                       <table id="tblInstrumentDetails" runat="server" style="width: 100%">
                           <tr id="trFromAccount1" runat="server" align="left" style="width: 100%" valign="top">
                               <td align="left" valign="top" class="auto-style1">
                                   <asp:Label ID="lblFrmAccount" runat="server" CssClass="lbl" Text="Drawer Account No."></asp:Label>
                                   <asp:Label ID="lblReqFrmAccount" runat="server" ForeColor="Red" Text="*"></asp:Label>
                               </td>
                               <td align="left" valign="top" class="auto-style1">&nbsp; </td>
                               <td align="left" valign="top" class="auto-style1">
                                   &nbsp;</td>
                               <td align="left" valign="top" class="auto-style1">&nbsp; </td>
                           </tr>
                           <tr id="trFromAccount2" runat="server" align="left" style="width: 100%" valign="top">
                               <td align="left" valign="top" class="auto-style1">
                                   <asp:TextBox ID="txtFromAccountNo" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp; </td>
                               <td align="left" style="width: 25%" valign="top">
                                   <%--<asp:Label ID="Label11" runat="server" Text="*" ForeColor="Red"></asp:Label>--%>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp; </td>
                           </tr>
                           <tr align="left" style="width: 100%" valign="top">
                               <td align="left" valign="top" class="auto-style1">
                                   <asp:Label ID="lblInstrumentNo" runat="server" CssClass="lbl" Text="Instrument No."></asp:Label>
                                   <asp:Label ID="lblReqInstNo" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                   <asp:Label ID="lblHelpText" runat="server" Text="(Please enter ins. number without prefix)" CssClass="lbl" Font-Size="12px"></asp:Label>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                               <td align="left" style="width: 25%" valign="top">
                                   <asp:Label ID="lblInstrumentDate" runat="server" CssClass="lbl" Text="Instrument Date"></asp:Label>
                                   <asp:Label ID="lblReqInstDate" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                   </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                           </tr>
                           <tr align="left" style="width: 100%" valign="top">
                               <td align="left" valign="top" class="auto-style1">
                                   <asp:TextBox ID="txtInstrumentNo" runat="server" CssClass="txtbox"></asp:TextBox>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                               <td align="left" style="width: 25%" valign="top">
                        <telerik:RadDatePicker ID="dpInstrumentDate" runat="server" DateInput-ReadOnly="true"
                            CssClass="dropdown" DateInput-DateFormat="dd-MMM-yyyy" AutoPostBack="true"
                            DateInput-EmptyMessage="" ImagesPath="">
                        </telerik:RadDatePicker>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                           </tr>
                           <tr align="left" style="width: 100%" valign="top">
                               <td align="left" valign="top" class="auto-style1">
                                   &nbsp;</td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                               <td align="left" style="width: 25%" valign="top">
                        <asp:RequiredFieldValidator ID="rfvInstrumentDate" runat="server" ControlToValidate="dpInstrumentDate" Display="Dynamic"
                            Enabled="true" ErrorMessage="Please select Instrument Date" ForeColor="Red" InitialValue="" ValidationGroup="Search" SetFocusOnError="True"
                            ToolTip="Required Field"></asp:RequiredFieldValidator>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                           </tr>
                           <tr align="left" style="width: 100%" valign="top">
                               <td align="left" valign="top" class="auto-style1">
                                   <asp:Label ID="lblLateClearing" runat="server" CssClass="lbl" Text="Late Clearing"></asp:Label>
                                   <asp:Label ID="lblReqLateClearing" runat="server" ForeColor="Red" Text="*"></asp:Label>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                               <td align="left" style="width: 25%" valign="top">
                                   <asp:Label ID="lblValueDate" runat="server" CssClass="lbl" Text="Value Date"></asp:Label>
                                   <asp:Label ID="lblReqValueDate" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                   </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                           </tr>
                           <tr align="left" style="width: 100%" valign="top">
                               <td align="left" valign="top" class="auto-style1">
            <asp:DropDownList ID="ddlLateClearing" runat="server" CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem>N</asp:ListItem>
                <asp:ListItem>Y</asp:ListItem>
            </asp:DropDownList>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                               <td align="left" style="width: 25%" valign="top">
                        <telerik:RadDatePicker ID="dpValueDate" runat="server" DateInput-ReadOnly="true"
                            CssClass="dropdown" DateInput-DateFormat="dd-MMM-yyyy" AutoPostBack="false"
                             ImagesPath="" Enabled="false">
<Calendar UseRowHeadersAsSelectors="False" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"></Calendar>

<DateInput DisplayDateFormat="dd-MMM-yyyy" DateFormat="dd-MMM-yyyy" DisplayText="" ReadOnly="True" LabelWidth="40%" value="" type="text"></DateInput>

<DatePopupButton ImageUrl="" HoverImageUrl=""></DatePopupButton>
                        </telerik:RadDatePicker>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                           </tr>
                           <tr align="left" style="width: 100%" valign="top">
                               <td align="left" valign="top" class="auto-style1">
            <asp:RequiredFieldValidator ID="rfvddlLateClearing" runat="server" ControlToValidate="ddlLateClearing"
                Display="Dynamic" ErrorMessage="Please select Late Clearing" InitialValue="0" SetFocusOnError="True" ValidationGroup="Search"></asp:RequiredFieldValidator>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                               <td align="left" style="width: 25%" valign="top">
                        <asp:RequiredFieldValidator ID="rfvValueDate" runat="server" ControlToValidate="dpValueDate" Display="Dynamic"
                            Enabled="true" ErrorMessage="Please select Value Date" ForeColor="Red" InitialValue="" SetFocusOnError="True"
                            ToolTip="Required Field" ValidationGroup="Search"></asp:RequiredFieldValidator>
                                   </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                           </tr>
                          
                           
                           <tr id="trPrintedFrom" runat="server" align="left" style="width: 100%" valign="top">
                               <td align="left" valign="top" class="auto-style1">
                                   <asp:Label ID="lblPrintedFrom" runat="server" CssClass="lbl" Text="Printed From"></asp:Label>
                                   <asp:Label ID="lblReqPrintedFrom" runat="server" ForeColor="Red" Text="*"></asp:Label>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp; </td>
                               <td align="left" style="width: 25%" valign="top">
                                   <asp:Label ID="lblDrawnOnBranch" runat="server" CssClass="lbl" Text="Drawn on Branch"></asp:Label>
                                   <asp:Label ID="lblReqDrawnOnBranch" runat="server" ForeColor="Red" Text="*"></asp:Label>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp; </td>
                           </tr>
                          
                           
                           <tr id="trPrintedFrom2" runat="server" align="left" style="width: 100%" valign="top">
                               <td align="left" valign="top" class="auto-style1">
                                   <asp:RadioButtonList ID="rbPrintedFrom" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                       <asp:ListItem>Core Banking</asp:ListItem>
                                       <asp:ListItem>Soneri Trans@ct</asp:ListItem>
                                   </asp:RadioButtonList>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                               <td align="left" style="width: 25%" valign="top">
            <asp:DropDownList ID="ddlDrawnOnBranch" runat="server" CssClass="dropdown">
            </asp:DropDownList>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                           </tr>
                          
                           
                           <tr id="trPrintedFrom3" runat="server" align="left" style="width: 100%" valign="top">
                               <td align="left" valign="top" class="auto-style1">
            <asp:RequiredFieldValidator ID="rfvPrintedFrom" runat="server" ControlToValidate="rbPrintedFrom" InitialValue=""
                Display="Dynamic" ErrorMessage="Please select Printed From" SetFocusOnError="True" ValidationGroup="Search"></asp:RequiredFieldValidator>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                               <td align="left" style="width: 25%" valign="top">
            <asp:RequiredFieldValidator ID="rfvddlDrawnOnBranch" runat="server" ControlToValidate="ddlDrawnOnBranch"
                Display="Dynamic" ErrorMessage="Please select Bank Branch" InitialValue="0" SetFocusOnError="True" ValidationGroup="Search"></asp:RequiredFieldValidator>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                           </tr>
                       </table>
                   </td></tr>
     <tr  align="left">
                   <td align="center" colspan="4">
                       <table id="tblFundsTransfer" runat="server" style="width: 100%">
                           <tr align="left" style="width: 100%" valign="top">
                               <td align="left" style="width: 25%" valign="top">
                                   <asp:Label ID="lblFromAccountNo" runat="server" CssClass="lbl" Text="From Account No."></asp:Label>
                                   <asp:Label ID="lblReqFrmAccNoFT" runat="server" ForeColor="Red" Text="*"></asp:Label>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp; </td>
                               <td align="left" style="width: 25%" valign="top">
                                   <asp:Label ID="lblFromAccountTittle" runat="server" CssClass="lbl" Text="From Account Title"></asp:Label>
                                   <%-- <asp:Label ID="Label7" runat="server" Text="*" ForeColor="Red"></asp:Label>--%>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp; </td>
                           </tr>
                           <tr align="left" style="width: 100%" valign="top">
                               <td align="left" style="width: 25%" valign="top">
                                   <asp:TextBox ID="txtFromAccountNoFT" runat="server" AutoPostBack="True" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp; </td>
                               <td align="left" style="width: 25%" valign="top">
                                   <asp:TextBox ID="txtFromAccountTitle" runat="server" CssClass="txtbox" ReadOnly="True"></asp:TextBox>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp; </td>
                           </tr>
                           <tr align="left" style="width: 100%" valign="top">
                               <td align="left" style="width: 25%" valign="top">
                                   <asp:Label ID="lblFromAccountBranchCode" runat="server" CssClass="lbl" Text="From Account Branch Code"></asp:Label>
                                   <%--<asp:Label ID="Label10" runat="server" Text="*" ForeColor="Red"></asp:Label>--%>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp; </td>
                               <td align="left" style="width: 25%" valign="top">
                                   <asp:Label ID="lblFromAccountCurrency" runat="server" CssClass="lbl" Text="From Account Currency"></asp:Label>
                                   <%--<asp:Label ID="Label11" runat="server" Text="*" ForeColor="Red"></asp:Label>--%>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp; </td>
                           </tr>
                           <tr align="left" style="width: 100%" valign="top">
                               <td align="left" style="width: 25%" valign="top">
                                   <asp:TextBox ID="txtFromAccountBranchCode" runat="server" CssClass="txtbox" ReadOnly="True"></asp:TextBox>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp; </td>
                               <td align="left" style="width: 25%" valign="top">
                                   <asp:TextBox ID="txtFromAccountCurrency" runat="server" CssClass="txtbox" ReadOnly="True"></asp:TextBox>
                               </td>
                               <td align="left" style="width: 25%" valign="top">&nbsp; </td>
                           </tr>
                       </table>
                   </td></tr>   
     
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
           <table id="tblResult" runat="server" style="width:100%;">
               <tr  align="left">
                   <td align="left" style="width:25%;">
                       &nbsp;</td>
                   <td align="left" style="width:25%;"></td>
                   <td align="left" style="width:25%;"></td>
                   <td align="left" style="width:25%;"></td>
               </tr>
               <tr id="tr1" runat="server" align="left">
                   <td align="left" colspan="4">
                        <telerik:RadGrid ID="gvInvoiceRecords" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="true" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                            <ClientSettings>
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView TableLayout="Fixed" DataKeyNames="Status,TemplateID">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                     <%--<telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="3%">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Checked="false" Text=""
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Checked="False" Text="" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridTemplateColumn>

                                     <telerik:GridTemplateColumn HeaderText="Detail">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbCollDetail" runat="server" Text="Detail"
                                                ToolTip="Show Detail" ForeColor="Blue" Enabled="false"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridTemplateColumn>--%>

                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">
                        <asp:Label ID="lblRNF" runat="server" Font-Bold="False" Text="No Record Found" Visible="False"
                            CssClass="headingblue"></asp:Label>
                    </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr  align="left">
                   <td align="left" style="width:25%;">
                       &nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
               <tr  align="left">
                   <td align="center" colspan="4"><asp:Button ID="btnPay" runat="server" Text="Next" CssClass="btnCancel"
                Width="75px" ValidationGroup="Search" CausesValidation="true"  />
                       <asp:Button ID="btnBack2" runat="server" Text="Back" CssClass="btnCancel" CausesValidation="False"
                Width="75px" />
                   </td>
               </tr>
           </table>
        </td>
    </tr>
    <tr  align="left">
                   <td align="left" colspan="4">
                           <table id="tblPreview" runat="server" style="width: 100%">
                                <tr  align="left">
                   <td align="left" style="width:25%;">
                            <asp:Label ID="lblCollectionPreview" runat="server" Text="Collection Preview"
                                CssClass="headingblue"></asp:Label>
                        </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="left" style="width:25%;">
                       &nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblReferenceFields" runat="server" CssClass="lbl" Text="Reference Field(s)"></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblCollectionAmount" runat="server" CssClass="lbl" Text="Amount"></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="left" style="width:25%;">
                       &nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblprvReferenceFields" runat="server" CssClass="lblPreview"></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblprvCollectionAmount" runat="server" CssClass="lblPreview"></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="left" style="width:25%;">
                       &nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblCollProduct" runat="server" CssClass="lbl" Text="Collection Product"></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblClearingType" runat="server" CssClass="lbl" Text="Clearing Type"></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="left" style="width:25%;">
                       &nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblprvCollPRoduct" runat="server" CssClass="lblPreview"></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblPrvClearingType" runat="server" CssClass="lblPreview"></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="left" style="width:25%;">
                       &nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblCollProduct0" runat="server" CssClass="lbl" Text="Customer Name"></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblCollBank" runat="server" CssClass="lbl" Text="Bank"></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="left" class="auto-style2">
                       </td>
                   <td align="left" class="auto-style2"></td>
                   <td align="left" class="auto-style2"></td>
                   <td align="left" class="auto-style2"></td>
               </tr>
     <tr  align="left">
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblprvCustomerName" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblPrvBank" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="left" style="width:25%;">
                       &nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblInstrumentNoPrview" runat="server" CssClass="lbl" Text="Instrument No."></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblValueDatePreview" runat="server" CssClass="lbl" Text="Value Date"></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="left" style="width:25%;">
                       &nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblprvInstrumentNo" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblprvValueDate" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="left" style="width:25%;">
                       &nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblLateClearingPreview" runat="server" CssClass="lbl" Text="Late Clearing"></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblCommentsPreview" runat="server" CssClass="lbl" Text="Comments"></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="left" style="width:25%;">
                       &nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblprvLateClearing" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
                   <td align="left" style="width:25%;">
                                                    <asp:Label ID="lblprvComments" runat="server" CssClass="lbl" Text="Instrument No."></asp:Label>
                                                </td>
                   <td align="left" style="width:25%;">&nbsp;</td>
               </tr>
     <tr  align="left">
                   <td align="center" colspan="4">
                       <asp:Button ID="btnProceed" runat="server" Text="Proceed" CssClass="btnCancel"
                Width="75px"  />
                       <asp:Button ID="btnCancelProced" runat="server" Text="Cancel" CssClass="btnCancel" CausesValidation="False"
                Width="75px" />
                   </td>
               </tr>
                           </table>
                        </td>
               </tr>
    
</table>
<div id="urldialog" title="">
    <iframe id='dialogiframe' frameborder="0" allowtransparency="true" src="" style="border-style: none;
        width: 100%; height: 100%; overflow-x: hidden; background-color: transparent;">
    </iframe>
    <div id='loading' style="display: none; margin: 0px auto; text-align: center; width: 100%;
        height: 100%; position: relative">
        <img src='../../../images/progressbar.gif' style="left: 45%; top: 50%; position: absolute;" />
    </div>
</div>
