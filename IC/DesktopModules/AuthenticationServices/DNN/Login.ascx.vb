'
' DotNetNukeŽ - http://www.dotnetnuke.com
' Copyright (c) 2002-2010
' by DotNetNuke Corporation
'
' Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
' documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
' the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and 
' to permit persons to whom the Software is furnished to do so, subject to the following conditions:
'
' The above copyright notice and this permission notice shall be included in all copies or substantial portions 
' of the Software.
'
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
' DEALINGS IN THE SOFTWARE.
'


Imports DotNetNuke.Security.Membership
Imports DotNetNuke.Services.Authentication
Imports ICBO
Imports ICBO.IC
Imports System.DirectoryServices.AccountManagement
Namespace DotNetNuke.Modules.Admin.Authentication

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    ''' The Login AuthenticationLoginBase is used to provide a login for a registered user
    ''' portal.
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[cnurse]	9/24/2004	Updated to reflect design changes for Help, 508 support
    '''                       and localisation
    '''     [cnurse]    08/07/2007  Ported to new Authentication Framework
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Partial Class Login
        Inherits AuthenticationLoginBase

#Region "Protected Properties"

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        ''' Gets whether the Captcha control is used to validate the login
        ''' </summary>
        ''' <history>
        ''' 	[cnurse]	03/17/2006  Created
        '''     [cnurse]    07/03/2007  Moved from Sign.ascx.vb
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Protected ReadOnly Property UseCaptcha() As Boolean
            Get
                Return AuthenticationConfig.GetConfig(PortalId).UseCaptcha
            End Get
        End Property

#End Region

#Region "Public Properties"

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        ''' Check if the Auth System is Enabled (for the Portal)
        ''' </summary>
        ''' <remarks></remarks>
        ''' <history>
        ''' 	[cnurse]	07/04/2007	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Overrides ReadOnly Property Enabled() As Boolean
            Get
                Return AuthenticationConfig.GetConfig(PortalId).Enabled
            End Get
        End Property

#End Region

#Region "Event Handlers"

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        ''' Page_Load runs when the control is loaded
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[cnurse]	9/8/2004	Updated to reflect design changes for Help, 508 support
        '''                       and localisation
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            DotNetNuke.UI.Utilities.ClientAPI.RegisterKeyCapture(Me.Parent, Me.cmdLogin, Asc(vbCr))
            lblError.Visible = False
            If Not Request.IsAuthenticated Then
                If Page.IsPostBack = False Then
                    Try
                        cmdLogin.CssClass = "btn"
                        If Not Request.QueryString("username") Is Nothing Then
                            txtUsername.Text = Request.QueryString("username")
                        End If
                        If Not Request.QueryString("verificationcode") Is Nothing Then
                            If PortalSettings.UserRegistration = PortalRegistrationType.VerifiedRegistration Then
                                'Display Verification Rows 
                                rowVerification1.Visible = True
                                rowVerification2.Visible = True
                                txtVerification.Text = Request.QueryString("verificationcode")
                            End If
                        End If

                    Catch
                        'control not there 
                    End Try
                End If

                Try
                    If String.IsNullOrEmpty(txtUsername.Text) Then
                        SetFormFocus(txtUsername)
                    Else
                        SetFormFocus(txtPassword)
                    End If
                Catch
                    'Not sure why this Try/Catch may be necessary, logic was there in old setFormFocus location stating the following
                    'control not there or error setting focus
                End Try
            End If

            trCaptcha1.Visible = UseCaptcha
            trCaptcha2.Visible = UseCaptcha

        End Sub


        ''' -----------------------------------------------------------------------------
        ''' <summary>
        ''' cmdLogin_Click runs when the login button is clicked
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[cnurse]	9/24/2004	Updated to reflect design changes for Help, 508 support
        '''                       and localisation
        '''     [cnurse]    12/11/2005  Updated to reflect abstraction of Membership
        '''     [cnurse]    07/03/2007  Moved from Sign.ascx.vb
        ''' </history>
        ''' -----------------------------------------------------------------------------
        ''' 
        Private Sub cmdLogin_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdLogin.Click
            If pnl2FA.Visible = True Then
                btnOK2FA_Click(Me.cmdLogin, Nothing)
                Exit Sub
            End If
            Try
                If txtUsername.Text.Trim().ToString() = "" Then
                    UIUtilities.ShowDialog(Me, "Login", "Please enter User Name.", Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If txtPassword.Text.Trim().ToString() = "" Then
                    UIUtilities.ShowDialog(Me, "Login", "Please enter Password.", Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Dim cacheicUser As New ICUser
                cacheicUser.Query.Where(cacheicUser.Query.UserName.ToLower = txtUsername.Text.Trim().ToLower)
                If cacheicUser.Query.Load() Then
                    DataCache.RemoveCache("MembershipUser_" & cacheicUser.UserName.ToString())
                    DataCache.ClearPortalCache(Me.PortalId, True)
                End If
                If (UseCaptcha And ctlCaptcha.IsValid) OrElse (Not UseCaptcha) Then
                    'DataCache.RemoveCache("MembershipUser_" & txtUsername.Text)
                    Dim loginStatus As UserLoginStatus = UserLoginStatus.LOGIN_FAILURE
                    Dim icUser As New ICUser
                    icUser.Query.Where(icUser.Query.UserName = txtUsername.Text.Trim())
                    Dim authenticated As Boolean = False
                    Dim message As String = Null.NullString
                    Dim objICUser As New ICBO.IC.ICUser
                    Dim objUser As New UserInfo

                    If icUser.Query.Load() Then
                        If icUser.AuthenticatedVia Is Nothing Then
                            objUser = UserController.ValidateUser(PortalId, txtUsername.Text, txtPassword.Text, "DNN", txtVerification.Text, PortalSettings.PortalName, IPAddress, loginStatus)
                        ElseIf icUser.AuthenticatedVia.ToString() = "Active Directory" Then
                            objUser = UserController.GetUserByName(PortalId, txtUsername.Text)
                        Else
                            objUser = UserController.ValidateUser(PortalId, txtUsername.Text, txtPassword.Text, "DNN", txtVerification.Text, PortalSettings.PortalName, IPAddress, loginStatus)
                        End If

                        '  objUser = UserController.ValidateUser(PortalId, txtUsername.Text, txtPassword.Text, "DNN", txtVerification.Text, PortalSettings.PortalName, IPAddress, loginStatus)

                        ' Active Directory Integration Work By Aizaz Ahmed [Dated: 11-Mar-2013]

                        If icUser.AuthenticatedVia Is Nothing Then
                            If loginStatus = UserLoginStatus.LOGIN_USERNOTAPPROVED Then
                                'Check if its the first time logging in to a verified site
                                If PortalSettings.UserRegistration = PortalRegistrationType.VerifiedRegistration Then
                                    If Not rowVerification1.Visible Then
                                        'Display Verification Rows so User can enter verification code
                                        rowVerification1.Visible = True
                                        rowVerification2.Visible = True
                                        message = "EnterCode"
                                    Else
                                        If txtVerification.Text <> "" Then
                                            message = "InvalidCode"
                                        Else
                                            message = "EnterCode"
                                        End If
                                    End If
                                Else
                                    message = "UserNotAuthorized"
                                    UIUtilities.ShowDialog(Me, "Login", "Your User ID is in-active. Please contact bank administrator.", Dialogmessagetype.Failure)
                                End If
                            ElseIf loginStatus = UserLoginStatus.LOGIN_USERLOCKEDOUT Then
                                authenticated = False
                                UIUtilities.ShowDialog(Me, "Login", "ID has been deactivated. Please contact bank administrator.", Dialogmessagetype.Failure)
                            Else
                                authenticated = (loginStatus <> UserLoginStatus.LOGIN_FAILURE)
                                UIUtilities.ShowDialog(Me, "Login", "Invalid User Name or Password.", Dialogmessagetype.Failure)
                            End If
                        Else

                            If icUser.UserType = "Client User" Then
                                If icUser.UpToICOfficeByOfficeCode.UpToICCompanyByCompanyCode.IsActive = False Or icUser.UpToICOfficeByOfficeCode.UpToICCompanyByCompanyCode.IsApprove = False Then
                                    UIUtilities.ShowDialog(Me, "Login", "Unable to login. Please contact Al Baraka Administrator", Dialogmessagetype.Failure)
                                    Exit Sub
                                End If
                            End If
                           
                            If icUser.AuthenticatedVia.ToString() = "Active Directory" Then
                                If Not icUser.IsActive = True Or Not icUser.IsApproved = True Then
                                    UIUtilities.ShowDialog(Me, "Login", "Unable to login. Please contact Al Baraka Administrator", Dialogmessagetype.Failure)
                                    Exit Sub
                                End If

                                Dim PC As New PrincipalContext(DirectoryServices.AccountManagement.ContextType.Domain, ICUtilities.GetSettingValue("Domain"))
                                authenticated = PC.ValidateCredentials(txtUsername.Text.ToString(), txtPassword.Text.ToString())
                                If authenticated = True Then
                                    loginStatus = UserLoginStatus.LOGIN_SUCCESS
                                Else
                                    UIUtilities.ShowDialog(Me, "Login", "Unable to login. Please contact your Network Administrator.", Dialogmessagetype.Failure)
                                    Exit Sub
                                End If
                                objUser = New UserInfo
                                objUser = UserController.GetUserByName(Me.PortalId, txtUsername.Text)
                                ''Dim PC As New PrincipalContext(DirectoryServices.AccountManagement.ContextType.Domain, ICUtilities.GetSettingValue("Domain"))
                                ''authenticated = PC.ValidateCredentials(txtUsername.Text.ToString(), txtPassword.Text.ToString())
                                ''If authenticated = True Then
                                ''UserController.ValidateUser(PortalId, txtUsername.Text, txtPassword.Text, "DNN", txtVerification.Text, PortalSettings.PortalName, IPAddress, loginStatus)
                                'If icUser.IsActive = True And icUser.IsApproved = True Then
                                '    loginStatus = UserLoginStatus.LOGIN_SUCCESS
                                'Else
                                '    loginStatus = UserLoginStatus.LOGIN_USERNOTAPPROVED
                                'End If
                                'If loginStatus = UserLoginStatus.LOGIN_SUCCESS Then
                                '    Dim PC As New PrincipalContext(DirectoryServices.AccountManagement.ContextType.Domain, ICUtilities.GetSettingValue("Domain"))
                                '    authenticated = PC.ValidateCredentials(txtUsername.Text.ToString(), txtPassword.Text.ToString())
                                '    If authenticated = True Then
                                '        loginStatus = UserLoginStatus.LOGIN_SUCCESS
                                '    Else
                                '        UIUtilities.ShowDialog(Me, "Login", "Unable to login. Please contact your Network Administrator.", Dialogmessagetype.Failure)
                                '        Exit Sub
                                '    End If

                                'ElseIf loginStatus = UserLoginStatus.LOGIN_USERNOTAPPROVED Then
                                '    'Check if its the first time logging in to a verified site
                                '    If PortalSettings.UserRegistration = PortalRegistrationType.VerifiedRegistration Then
                                '        If Not rowVerification1.Visible Then
                                '            'Display Verification Rows so User can enter verification code
                                '            rowVerification1.Visible = True
                                '            rowVerification2.Visible = True
                                '            message = "EnterCode"
                                '        Else
                                '            If txtVerification.Text <> "" Then
                                '                message = "InvalidCode"
                                '            Else
                                '                message = "EnterCode"
                                '            End If
                                '        End If
                                '    Else
                                '        message = "UserNotAuthorized"
                                '    End If
                                'Else

                                '    authenticated = False
                                'End If
                                ''loginStatus = UserLoginStatus.LOGIN_SUCCESS
                                ''Else
                                ''    UIUtilities.ShowDialog(Me, "Login", "Unable to login. Please contact your Network Administrator.", Dialogmessagetype.Failure)
                                ''    Exit Sub
                                ''End If
                                'objUser = New UserInfo
                                'objUser = UserController.GetUserByName(Me.PortalId, txtUsername.Text)
                            ElseIf icUser.AuthenticatedVia.ToString() = "Application" Then
                                If loginStatus = UserLoginStatus.LOGIN_USERNOTAPPROVED Then
                                    'Check if its the first time logging in to a verified site
                                    If PortalSettings.UserRegistration = PortalRegistrationType.VerifiedRegistration Then
                                        If Not rowVerification1.Visible Then
                                            'Display Verification Rows so User can enter verification code
                                            rowVerification1.Visible = True
                                            rowVerification2.Visible = True
                                            message = "EnterCode"
                                        Else
                                            If txtVerification.Text <> "" Then
                                                message = "InvalidCode"
                                            Else
                                                message = "EnterCode"
                                            End If
                                        End If
                                    Else
                                        message = "UserNotAuthorized"
                                        UIUtilities.ShowDialog(Me, "Login", "Your User ID is in-active. Please send an email at Al Baraka.", Dialogmessagetype.Failure)
                                    End If

                                Else

                                    If loginStatus = UserLoginStatus.LOGIN_FAILURE Then
                                        ICUtilities.AddAuditTrail(txtUsername.Text.ToString() & " login failed, due to incorrect login attempt.", "Login Failed", icUser.UserID, icUser.UserID, txtUsername.Text.ToString(), "LOGIN FAILED")
                                        authenticated = False
                                        UIUtilities.ShowDialog(Me, "Login", "Invalid User Name or Password.", Dialogmessagetype.Failure)
                                        Exit Sub
                                    ElseIf loginStatus = UserLoginStatus.LOGIN_USERLOCKEDOUT Then
                                        authenticated = False
                                        UIUtilities.ShowDialog(Me, "Login", "ID has been deactivated. Please contact bank administrator.", Dialogmessagetype.Failure)
                                        'Exit Sub
                                    ElseIf loginStatus = UserLoginStatus.LOGIN_USERNOTAPPROVED Then
                                        authenticated = False
                                        UIUtilities.ShowDialog(Me, "Login", "Your User ID is in-active. Please send an email at Al Baraka.", Dialogmessagetype.Failure)
                                        'Exit Sub
                                    ElseIf loginStatus = UserLoginStatus.LOGIN_SUCCESS Then
                                        authenticated = True
                                    End If

                                End If
                            End If
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Login", "Invalid User Name or Password.", Dialogmessagetype.Failure)
                        Exit Sub
                    End If

                    If authenticated = True Then
                        objICUser = New ICBO.IC.ICUser

                        objICUser.LoadByPrimaryKey(objUser.UserID)

                        If objICUser.Is2FARequiredOnLogin = True Then
                            ViewState("objUser") = objUser
                            ViewState("loginStatus") = loginStatus
                            Validate2FA(objICUser.UserID)
                        Else
                            'add audit log method here

                            'objUser.UserID
                            If Not objUser Is Nothing Then
                                ICBO.ICUtilities.AddAuditTrail("Log On: " & objUser.Username.ToString() & " Log In.", "Log On", objUser.UserID.ToString(), objUser.UserID.ToString(), objUser.Username.ToString(), "LOG ON")
                                GenerateAuthToken(objUser.Username)
                            End If

                            'Raise UserAuthenticated Event
                            Dim eventArgs As UserAuthenticatedEventArgs = New UserAuthenticatedEventArgs(objUser, txtUsername.Text, loginStatus, "DNN")
                            eventArgs.Authenticated = authenticated
                            eventArgs.Message = message
                            OnUserAuthenticated(eventArgs)
                        End If
                    Else
                        'Raise UserAuthenticated Event
                        Dim eventArgs As UserAuthenticatedEventArgs = New UserAuthenticatedEventArgs(objUser, txtUsername.Text, loginStatus, "DNN")
                        eventArgs.Authenticated = authenticated
                        eventArgs.Message = message
                        OnUserAuthenticated(eventArgs)
                    End If
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Login", ex.Message, Dialogmessagetype.Failure)
            End Try
        End Sub
        Private Sub Validate2FA(ByVal UserID As String)
            If ICBO.IC.IC2FAController.Generate2FACodeAndSend(UserID.ToString(), "Login") Then
                tblLogin.Style.Add("display", "none")
                'pnl2FA.Style.Remove("display")
                ' tblLogin.Visible = False
                pnl2FA.Visible = True
                btnOK2FA.Text = "OK"
                txt2FACode.Text = Nothing
                txt2FACode.Enabled = True
                txt2FACode.Focus()
                rim2FA.Enabled = True
                btnOK2FA.Visible = True
            End If
        End Sub

#End Region

        Protected Sub btnOK2FA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK2FA.Click
            Dim authenticated As Boolean = Null.NullBoolean
            Dim message As String = Null.NullString
            Dim loginStatus As UserLoginStatus = DirectCast(ViewState("loginStatus"), UserLoginStatus)
            Dim objUser As UserInfo = DirectCast(ViewState("objUser"), UserInfo)

            If btnOK2FA.Text = "OK" Then
                If Regex.IsMatch(txt2FACode.Text, "^[a-zA-Z0-9]+$") = False Then
                    ShowError("Invalid Code. Please retry.")
                    Exit Sub
                End If
                If ICBO.IC.IC2FAController.Check2FAExistAndValid(objUser.UserID, txt2FACode.Text.ToString().Trim(), "Login") Then
                    If ICBO.IC.IC2FAController.Update2FACodeStatusAfterUse(objUser.UserID, "Login") Then
                        'add audit log method here

                        'objUser.UserID
                        If Not objUser Is Nothing Then
                            ICBO.ICUtilities.AddAuditTrail("Log On: " & objUser.Username.ToString() & " Log In.", "Log On", objUser.UserID.ToString(), objUser.UserID.ToString(), objUser.Username.ToString(), "LOG ON")
                            GenerateAuthToken(objUser.Username)
                        End If
                        'Raise UserAuthenticated Event
                        Dim eventArgs As UserAuthenticatedEventArgs = New UserAuthenticatedEventArgs(objUser, txtUsername.Text, loginStatus, "DNN")
                        eventArgs.Authenticated = authenticated
                        eventArgs.Message = message
                        OnUserAuthenticated(eventArgs)
                    End If
                Else
                    If ICBO.IC.IC2FAController.CheckAndUpdateLoginAttemts(objUser.UserID, "Login") = False Then
                        btnOK2FA.Text = "Regenerate Code"
                        txt2FACode.Text = Nothing
                        txt2FACode.Enabled = False
                        rim2FA.Enabled = False
                        ShowError("Retries Exhausted. Please regenerate code.")
                    Else
                        txt2FACode.Enabled = True
                        txt2FACode.Text = Nothing
                        ShowError("Invalid Code. Please retry.")
                        rim2FA.Enabled = True
                    End If
                End If
            ElseIf btnOK2FA.Text = "Regenerate Code" Then
                If ICBO.IC.IC2FAController.Generate2FACodeAndSend(objUser.UserID, "Login") Then
                    btnOK2FA.Text = "OK"
                    txt2FACode.Text = Nothing
                    txt2FACode.Enabled = True
                    rim2FA.Enabled = True
                End If
            End If
        End Sub
        Private Sub ShowError(ByVal Msg As String)
            lblError.Text = Msg.ToString()
            lblError.Visible = True
        End Sub

        Protected Sub btnClose2FA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose2FA.Click
            Response.Redirect(NavigateURL())
        End Sub

        Public Sub GenerateAuthToken(ByVal UserName As String)
            Dim guid As String = System.Guid.NewGuid.ToString()
            'Dim authToken As String = (UserName & "_" & DateTime.Now.ToString("yyyyMMddhhmmss")).EncryptString()
            Dim authToken As String = UserName.EncryptString(False)
            Response.Cookies.Add(New HttpCookie("AuthToken", authToken))
            Session("AuthToken") = authToken
        End Sub

    End Class

End Namespace
