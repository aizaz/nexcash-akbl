<%@ Control language="vb" Inherits="DotNetNuke.Modules.Admin.Authentication.Login" CodeFile="Login.ascx.vb" AutoEventWireup="false" Explicit="True"%>
<%@ Register TagPrefix="dnn" Assembly="DotNetNuke" Namespace="DotNetNuke.UI.WebControls"%>
<%@ Register TagPrefix="dnn" TagName="Label" Src="~/controls/LabelControl.ascx" %>

<table cellspacing="0" id="tblLogin" runat="server"  cellpadding="3" border="0" summary="SignIn Design Table" width="160">
	<tr>
		<td class="SubHead" align="left"><asp:Label id="plUsername" runat="server" Text="User Name:" CssClass="lbl"  /></td>
	</tr>
	<tr>
		<td><asp:textbox id="txtUsername" columns="9" width="150" cssclass="NormalTextBox" runat="server" /></td>
	</tr>
	<tr>
		<td class="SubHead" align="left"><asp:Label id="plPassword" runat="server" Text="Password:" CssClass="lbl" /></td>
	</tr>
	<tr>
		<td><asp:textbox id="txtPassword" columns="9" width="150" textmode="Password" cssclass="NormalTextBox" runat="server" /></td>
	</tr>
	<tr id="rowVerification1" runat="server" visible="false">
		<td class="SubHead" align="left"><dnn:label id="plVerification" controlname="txtVerification" runat="server"/></td>
	</tr>
	<tr id="rowVerification2" runat="server" visible="false">
		<td><asp:textbox id="txtVerification" columns="9" width="150" cssclass="NormalTextBox" runat="server" /></td>
	</tr>
    <tr id="trCaptcha1" runat="server">
	    <td class="SubHead" align="left"><dnn:label id="plCaptcha" controlname="ctlCaptcha" runat="server" resourcekey="Captcha" /></td>
    </tr>
    <tr id="trCaptcha2" runat="server">
	    <td><dnn:captchacontrol id="ctlCaptcha" captchawidth="130" captchaheight="40" cssclass="Normal" runat="server" errorstyle-cssclass="NormalRed" textboxstyle-cssclass="NormalTextBox" /></td>
    </tr>
	<tr>
		<td><asp:button id="cmdLogin" resourcekey="cmdLogin" cssclass="StandardButton" text="Login" runat="server" /></td>
	</tr>
</table>
 <asp:Panel ID="pnl2FA" runat="server" Width="670px" BorderWidth="0px" Visible="false" DefaultButton="btnOK2FA">
    <telerik:RadInputManager ID="rim2FA" runat="server" Enabled="false">
    <telerik:RegExpTextBoxSetting BehaviorID="rad2FACode" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9]+$" ErrorMessage="Invalid Code" EmptyMessage="Enter Code"
        Validation-ValidationGroup="2FA">
        <TargetControls>
            <telerik:TargetInput ControlID="txt2FACode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
</telerik:RadInputManager>
        <table style="width: 100%">
            <tr align="left" valign="top" style="width: 100%">
                <td align="left" valign="top" style="width: 50%">
                    <asp:Label ID="lbl2FACode" runat="server" Text="Enter Code" CssClass="lbl"></asp:Label>
                </td>
                <td align="left" valign="top" style="width: 50%">
                    <asp:TextBox ID="txt2FACode" runat="server" CssClass="txtbox" MaxLength="8"></asp:TextBox>
                </td>
            </tr>
            <tr align="left" valign="top" style="width: 100%">
                <td align="left" valign="top" colspan="2">
                    <asp:Label ID="lblError" runat="server" CssClass="lbl" ForeColor="#FF3300" 
                        Visible="False"></asp:Label>
                </td>
            </tr>
            <tr align="left" style="width: 100%" valign="top">
                <td align="left" style="width: 50%" valign="top">
                    &nbsp;</td>
                <td align="left" style="width: 50%" valign="top">
                    <asp:Button ID="btnOK2FA" runat="server" CssClass="btn" Text="Ok" 
                        ValidationGroup="2FA" />
                    <asp:Button ID="btnClose2FA" runat="server" CausesValidation="False" 
                        CssClass="btn" Text="Cancel" />
                </td>
            </tr>
        </table>
    </asp:Panel>