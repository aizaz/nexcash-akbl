﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewPackagesInvoices.ascx.vb" Inherits="DesktopModules_PackageInvoiceManagement_ViewPackagesInvoices" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to remove Record(s)?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }    
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        //      alert(grid)
        //        if ((grid.rows.length -1) != 10 ) {
        //        if ((grid.rows.length) >= 12) {
        //            //            alert('More than 10 rows')
        //            for (i = 0; i < grid.rows.length - 1; i++) {
        //                cell = grid.rows[i].cells[CellNo];
        //                for (j = 0; j < cell.childNodes.length - 1; j++) {
        //                    if (cell.childNodes[j].type == "checkbox") {
        //                        cell.childNodes[j].checked = document.getElementById(idOfControllingCheckbox).checked;
        //                    }
        //                }
        //            }
        //        }
        //        else {
        //                     alert(grid.rows.length)
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    //    }
</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="center" valign="top" colspan="4">
                        <asp:HiddenField ID="hfBankCode" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <asp:Label ID="lblOfficeList" runat="server" Text="Package Invoices" 
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblGroup2" runat="server" Text="Status" CssClass="lbl"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown">
                                    </asp:DropDownList>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:DropDownList ID="ddlstatus" runat="server" CssClass="dropdown">
                                        <asp:ListItem>--All--</asp:ListItem>
                                        <asp:ListItem>Un Paid</asp:ListItem>
                                        <asp:ListItem>Paid</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlGroup" ErrorMessage="Please select group" InitialValue="0"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" valign="top" style="width:25%">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlCompany" ErrorMessage="Please select company" InitialValue="0"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" valign="top" style="width:25%">
                                    &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblGroup0" runat="server" Text="From" CssClass="lbl"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblGroup1" runat="server" Text="To" CssClass="lbl"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
                                                      <telerik:RadDatePicker ID="rdpfrom" runat="server" Width="180px">
                                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                        </Calendar>
                                                        <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                             LabelWidth="40%" type="text" value="">
                                                        </DateInput>
                                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                    </telerik:RadDatePicker>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                                   <telerik:RadDatePicker ID="rdpto" runat="server" Width="180px">
                                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                        </Calendar>
                                                        <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                            LabelWidth="40%" type="text" value="">
                                                        </DateInput>
                                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                    </telerik:RadDatePicker>
                                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Button ID="btnFilter" runat="server" Text="Filter" Width="98px" CssClass="btn" />
                                </td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <asp:Label ID="lblRNF" runat="server" Font-Bold="False" 
                            Text="No Record Found" Visible="False" CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                    <telerik:RadGrid ID="gvOffice" runat="server" AllowPaging="True"
                            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                            PageSize="100" CssClass="RadGrid" GridLines="None">
                            <ClientSettings>
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView NoMasterRecordsText="" DataKeyNames="InvoiceID" TableLayout="Fixed">
                                <NoRecordsTemplate>
                                    No Records found
                                </NoRecordsTemplate>
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                 <%--   <telerik:GridTemplateColumn HeaderText="Approve" DataField="IsApproved">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>
                                    <telerik:GridBoundColumn DataField="InvoiceDate" HeaderText="Date" SortExpression="InvoiceDate" FilterControlAltText="Filter InvoiceDate column" UniqueName="InvoiceDate">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InvoiceAmount" HeaderText="Amount" SortExpression="InvoiceAmount" FilterControlAltText="Filter InvoiceAmount column" UniqueName="InvoiceAmount">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn DataField="IsPaid" HeaderText="Paid"
                                        SortExpression="IsPaid" FilterControlAltText="Filter IsPaid column" UniqueName="IsPaid">
                                    </telerik:GridCheckBoxColumn>
                                 <%--<telerik:GridCheckBoxColumn DataField="IsApprove" HeaderText="Approve"
                                        SortExpression="IsApprove">
                                    </telerik:GridCheckBoxColumn>--%>
                                    <telerik:GridTemplateColumn HeaderText="View">
                                        <ItemTemplate>
                                        <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/view.gif" NavigateUrl='<%# NavigateURL("ViewInvoice", "&mid=" & Me.ModuleId & "&id=" & Eval("InvoiceId"))%>'
                                            ToolTip="View">View</asp:HyperLink>
                                    </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                        </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <%--<telerik:GridCheckBoxColumn DataField="IsApprove" HeaderText="Approve"
                                        SortExpression="IsApprove">
                                    </telerik:GridCheckBoxColumn>--%>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" colspan="4">
                        <%--<telerik:GridCheckBoxColumn DataField="IsApprove" HeaderText="Approve"
                                        SortExpression="IsApprove">
                                    </telerik:GridCheckBoxColumn>--%>
                    </td>
                </tr>
               </table>
</td>
</tr></table>