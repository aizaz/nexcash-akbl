﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewInvoice.ascx.vb" Inherits="DesktopModules_PackageInvoiceManagement_ViewInvoice" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to remove Record(s)?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }    
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        //      alert(grid)
        //        if ((grid.rows.length -1) != 10 ) {
        //        if ((grid.rows.length) >= 12) {
        //            //            alert('More than 10 rows')
        //            for (i = 0; i < grid.rows.length - 1; i++) {
        //                cell = grid.rows[i].cells[CellNo];
        //                for (j = 0; j < cell.childNodes.length - 1; j++) {
        //                    if (cell.childNodes[j].type == "checkbox") {
        //                        cell.childNodes[j].checked = document.getElementById(idOfControllingCheckbox).checked;
        //                    }
        //                }
        //            }
        //        }
        //        else {
        //                     alert(grid.rows.length)
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    //    }
</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="center" valign="top" colspan="4">
                        <asp:HiddenField ID="hfBankCode" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <asp:Label ID="lblOfficeList" runat="server" Text="View Invoice Details" 
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblGroup" runat="server" Text="Group" CssClass="lbl"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblCompany" runat="server" Text="Company" CssClass="lbl"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblGroup2" runat="server" Text="Invoice Date" CssClass="lbl"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblGroup3" runat="server" Text="Invoice Status" CssClass="lbl"></asp:Label>
                                </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblGroupname" runat="server"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblCompanyName" runat="server"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblinvoicedate" runat="server"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblinvoicestatus" runat="server"></asp:Label>
                                &nbsp;<asp:LinkButton ID="lbpaynow" runat="server" CausesValidation="False" Font-Underline="True" Visible="False">Pay Now</asp:LinkButton>
                                </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
                                    &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                                    &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblGroup0" runat="server" Text="Billing Period" CssClass="lbl"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblGroup4" runat="server" Text="Total Amount" CssClass="lbl"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblpaidamounttitle" runat="server" Text="Paid Amount" CssClass="lbl" Visible="False"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblpaymentdatetitle" runat="server" Text="Payment Date" CssClass="lbl" Visible="False"></asp:Label>
                                </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblbillingperiod" runat="server"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lbltotalamount" runat="server"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblpaidamount" runat="server" Visible="False"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblpaymentdate" runat="server" Visible="False"></asp:Label>
                                </td>
                </tr>
                <tr>
                    <td align="center" valign="top" colspan="4">
                                    <asp:Button ID="btnBack" runat="server" Text="Back" Width="98px" CssClass="btn" />
                                </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                                <telerik:RadGrid ID="gvDetails" runat="server" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                            PageSize="100" CssClass="RadGrid" GridLines="None">
                            <ClientSettings>
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView NoMasterRecordsText="" DataKeyNames="" TableLayout="Fixed">
                                <NoRecordsTemplate>
                                    No Records found
                                </NoRecordsTemplate>
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="Account" SortExpression="AccountNumber" FilterControlAltText="Filter AccountNumber column" UniqueName="AccountNumber">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Currency" HeaderText="Currency" SortExpression="Currency" FilterControlAltText="Filter Currency column" UniqueName="Currency">
                                    </telerik:GridBoundColumn>
                                 <%--<telerik:GridCheckBoxColumn DataField="IsApprove" HeaderText="Approve"
                                        SortExpression="IsApprove">
                                    </telerik:GridCheckBoxColumn>--%>
                                    <telerik:GridBoundColumn DataField="BranchCode" FilterControlAltText="Filter BranchCode column" HeaderText="Branch Code" UniqueName="BranchCode">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProductTypeCode" FilterControlAltText="Filter ProductTypeCode column" HeaderText="Product Type Code" UniqueName="ProductTypeCode">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PaymentNatureCode" FilterControlAltText="Filter PaymentNatureCode column" HeaderText="Payment Nature Code" UniqueName="PaymentNatureCode">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ChargeType" FilterControlAltText="Filter ChargeType column" HeaderText="Charge Type" UniqueName="ChargeType">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ChargeAmount" FilterControlAltText="Filter Amount column" HeaderText="Amount" UniqueName="Amount" Aggregate="Sum" DataFormatString="PKR {0}" FooterText="Total: ">
                                        <FooterStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridBoundColumn>
                                </Columns>
                            <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                                    <FooterStyle BackColor="White" />
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <%--<telerik:GridCheckBoxColumn DataField="IsApprove" HeaderText="Approve"
                                        SortExpression="IsApprove">
                                    </telerik:GridCheckBoxColumn>--%>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" colspan="4">
                        <%--<telerik:GridCheckBoxColumn DataField="IsApprove" HeaderText="Approve"
                                        SortExpression="IsApprove">
                                    </telerik:GridCheckBoxColumn>--%>
                    </td>
                </tr>
               </table>
</td>
</tr></table>