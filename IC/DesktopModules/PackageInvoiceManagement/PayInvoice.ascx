﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PayInvoice.ascx.vb" Inherits="DesktopModules_PackageInvoiceManagement_PayInvoice" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<style type="text/css">

.ctrDropDown{
    width:250px;
   
}
    .auto-style1
    {
        width: 25%;
        height: 23px;
    }
</style>

<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to pay the invoice?\nFinancial transaction(s) is in process, please do not refresh the page.") == true) {
            var btnCancel = document.getElementById('<%=btnApprove.ClientID%>')
            btnCancel.value = "Processing...";
            btnCancel.disabled = true;
            var ErrorMessage = document.getElementById('<%=lblMessageForQueue.ClientID%>')
            ErrorMessage.value = "Financial transaction(s) is in process, please do not refresh the page";
            ErrorMessage.style.display = 'inherit';
            var a = btnCancel.name;
            __doPostBack(a, '');
            return true;

        }
        else {
            var ErrorMessage2 = document.getElementById('<%=lblMessageForQueue.ClientID%>')
            ErrorMessage2.value = "";
            ErrorMessage2.style.display = 'none';
            return false;
        }
    }
    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }    
</script>
<script type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }
   </script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        //      alert(grid)
        //        if ((grid.rows.length -1) != 10 ) {
        //        if ((grid.rows.length) >= 12) {
        //            //            alert('More than 10 rows')
        //            for (i = 0; i < grid.rows.length - 1; i++) {
        //                cell = grid.rows[i].cells[CellNo];
        //                for (j = 0; j < cell.childNodes.length - 1; j++) {
        //                    if (cell.childNodes[j].type == "checkbox") {
        //                        cell.childNodes[j].checked = document.getElementById(idOfControllingCheckbox).checked;
        //                    }
        //                }
        //            }
        //        }
        //        else {
        //                     alert(grid.rows.length)
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    //    }
</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="center" valign="top" colspan="4">
                        <asp:HiddenField ID="hfBankCode" runat="server" />
<telerik:RadInputManager ID="RadInputManager2" runat="server">
    <telerik:NumericTextBoxSetting Culture="en-US" DecimalDigits="2" DecimalSeparator="." GroupSeparator="," GroupSizes="3" MaxValue="100" MinValue="0" NegativePattern="-n %" PositivePattern="n %" Type="Percent" ZeroPattern="n %">
        <TargetControls>
            <telerik:TargetInput ControlID="txtdiscountamount" />
        </TargetControls>
    </telerik:NumericTextBoxSetting>


</telerik:RadInputManager>
                        <telerik:RadInputManager ID="RadReject" runat="server" Enabled="false">
                         <telerik:TextBoxSetting BehaviorID="reRemarks" 
        ErrorMessage="Enter Remarks" EmptyMessage="" Validation-IsRequired="true">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRejectRemarks" />
        </TargetControls>
    </telerik:TextBoxSetting>
 
</telerik:RadInputManager>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <asp:Label ID="lblOfficeList" runat="server" Text="Charge Invoice" 
                            CssClass="headingblue"></asp:Label>
                         <br /><br />
                        <asp:Label ID="lblMessageForQueue" runat="server" Text="" CssClass="" ForeColor="Red" ></asp:Label>
                    </td>
                </tr>
                <tr id="trRequiredField" runat="server">
                    <td align="left" valign="top" colspan="4">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblGroup" runat="server" Text="Group" CssClass="lbl"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblCompany" runat="server" Text="Company" CssClass="lbl"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblGroup0" runat="server" Text="Billing Period" CssClass="lbl"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblGroup4" runat="server" Text="Total Amount" CssClass="lbl"></asp:Label>
                                </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblGroupname" runat="server"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblCompanyName" runat="server"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblbillingperiod" runat="server"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lbltotalamount" runat="server"></asp:Label>
                                </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
                                    &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                                    &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblGroup5" runat="server" Text="Customer Account:" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label5" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblGroup6" runat="server" Text="Bank Account:" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label6" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="2">
            <asp:DropDownList ID="ddlAccount" runat="server" class="ctrDropDown"  onblur="this.className='ctrDropDown';" onmousedown="this.className='ctrDropDownClick';" onchange="this.className='ctrDropDown';" CssClass="dropdown">
            </asp:DropDownList>
                                </td>
                    <td align="left" valign="top" colspan="2">
            <asp:DropDownList ID="ddlBankAccount" runat="server" class="ctrDropDown"  onblur="this.className='ctrDropDown';" onmousedown="this.className='ctrDropDownClick';" onchange="this.className='ctrDropDown';" CssClass="dropdown">
            </asp:DropDownList>
                                </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAccount" ErrorMessage="Please select account" InitialValue="0" SetFocusOnError="True" ValidationGroup="save"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" valign="top" style="width:25%">
                                    &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlBankAccount" ErrorMessage="Please select account" InitialValue="0" SetFocusOnError="True" ValidationGroup="save"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" valign="top" style="width:25%">
                                    &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblGroup7" runat="server" Text="Discount:" CssClass="lbl"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblGroup9" runat="server" Text="Amount to be charged:" CssClass="lbl"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width:25%">
            <asp:TextBox ID="txtdiscountamount" runat="server" CssClass="txtbox" 
                MaxLength="3" AutoPostBack="True"></asp:TextBox>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    &nbsp;</td>
                    <td align="left" valign="top" style="width:25%">
                                    <asp:Label ID="lblamounttobecharged" runat="server"></asp:Label>
                                </td>
                    <td align="left" valign="top" style="width:25%">
                                    &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <table id="tblReject" runat="server" style="width:100%">
                            <tr align="left" valign="top" style="width:100%">
                                <td align="left" valign="top" class="auto-style1">
                                    <asp:Label ID="lblRemarks" runat="server" Text="Remarks" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                </td>
                                <td align="left" valign="top" class="auto-style1"></td>
                                <td align="left" valign="top" class="auto-style1"></td>
                                <td align="left" valign="top" class="auto-style1"></td>
                            </tr>

                            <tr align="left" valign="top" style="width:100%">
                                <td align="left" valign="top" style="width:25%">
            <asp:TextBox ID="txtRejectRemarks" runat="server" CssClass="txtbox" 
                MaxLength="250" TextMode="MultiLine" Height="70px"  onkeypress="return textboxMultilineMaxNumber(this,250)"></asp:TextBox>
                                </td>
                                <td align="left" valign="top" style="width:25%">&nbsp;</td>
                                <td align="left" valign="top" style="width:25%">&nbsp;</td>
                                <td align="left" valign="top" style="width:25%">&nbsp;</td>
                            </tr>

                            <tr align="left" valign="top" style="width:100%">
                                <td align="left" valign="top" style="width:25%">&nbsp;</td>
                                <td align="left" valign="top" style="width:25%">&nbsp;</td>
                                <td align="left" valign="top" style="width:25%">&nbsp;</td>
                                <td align="left" valign="top" style="width:25%">&nbsp;</td>
                            </tr>

                        </table> </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4" style="text-align: center">
            <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btn" Visible="False"
                Width="71px" />
            <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn" Visible="False"
                Width="71px" ValidationGroup="save" OnClientClick ="javascript: return conBukDelete();"/>
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px" ValidationGroup="save" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False"
                 />
                        <br />
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="4">
                        <%--<telerik:GridCheckBoxColumn DataField="IsApprove" HeaderText="Approve"
                                        SortExpression="IsApprove">
                                    </telerik:GridCheckBoxColumn>--%>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" colspan="4">
                        <%--<telerik:GridCheckBoxColumn DataField="IsApprove" HeaderText="Approve"
                                        SortExpression="IsApprove">
                                    </telerik:GridCheckBoxColumn>--%>
                    </td>
                </tr>
               </table>
</td>
</tr></table>