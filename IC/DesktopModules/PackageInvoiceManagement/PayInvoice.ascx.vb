﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_PackageInvoiceManagement_PayInvoice
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_CompanyOfficeManagement_ViewCompanyOffice_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            lblMessageForQueue.Style.Add("display", "none")
            If Page.IsPostBack = False Then
                lblMessageForQueue.Text = ICUtilities.GetSettingValue("ErrorMessage")
                btnApprove.Attributes.Item("onclick") = "if (Page_ClientValidate('save')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";document.getElementById('<%=lblMessageForQueue.ClientID%>').style.display = 'block';this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnApprove, Nothing).ToString() & " } "
                Dim objICPackageInvoice As New ICPackageInvoice
                objICPackageInvoice.LoadByPrimaryKey(Request.QueryString("InvoiceID"))
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnApprove.Visible = CBool(htRights("Approve Invoice"))
                btnSave.Visible = CBool(htRights("Pay Invoice"))
                'LoadBankAccounts()
                'LoadddlAccounts(objICPackageInvoice.CompanyCode.ToString)
                ddlAccount.Enabled = True
                ddlBankAccount.Enabled = True
                txtdiscountamount.ReadOnly = False
                tblReject.Style.Add("display", "none")
                RadReject.Enabled = False
                txtRejectRemarks.Text = ""
                If Not Request.QueryString("InvoiceID") = "" Then


                    LoadInvoiceDetails(Request.QueryString("InvoiceID"))
                    If objICPackageInvoice.ChargeTransactionStatus Is Nothing Then
                        btnApprove.Visible = False
                        btnReject.Visible = False
                        btnSave.Visible = CBool(htRights("Pay Invoice"))
                        'DirectCast(Me.Control.FindControl("trRequiredField"), HtmlTableRow).Style.Add("display", "none")
                    ElseIf objICPackageInvoice.ChargeTransactionStatus = "PendingApproval" Or objICPackageInvoice.ChargeTransactionStatus = "Failed" Then
                        Dim AccountNo As String = objICPackageInvoice.ClientAccountNumber & "," & objICPackageInvoice.ClientAccountBranchCode & "," & objICPackageInvoice.ClientAccountCurrency
                        btnApprove.Visible = CBool(htRights("Approve Invoice"))
                        btnReject.Visible = CBool(htRights("Reject Invoice"))
                        If btnReject.Visible = True Then
                            tblReject.Style.Remove("display")
                            RadReject.Enabled = True
                        End If
                        'DirectCast(Me.Control.FindControl("trRequiredField"), HtmlTableRow).Style.Remove("display")
                        ddlAccount.SelectedValue = AccountNo
                        ddlBankAccount.SelectedValue = objICPackageInvoice.BankAccountId.ToString
                        Me.lblamounttobecharged.Text = "PKR " & objICPackageInvoice.PaidAmount.Value.ToString("N3")
                        If objICPackageInvoice.DiscountAmount IsNot Nothing And objICPackageInvoice.DiscountAmount <> 0 Then
                            txtdiscountamount.Text = objICPackageInvoice.DiscountAmount
                        Else
                            txtdiscountamount.Text = Nothing
                        End If

                        txtdiscountamount.ReadOnly = True
                        ddlAccount.Enabled = False
                        ddlBankAccount.Enabled = False
                        btnSave.Visible = False
                    End If




                End If


            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Package Invoice Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


#Region "Other Functions/Routines"


#End Region


    Private Sub LoadddlAccounts(ByVal companycode As Integer)
        Try
            Dim lit As New ListItem
            Dim objAccount As New ICAccounts
            Dim collAccounts As New ICAccountsCollection

            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlAccount.Items.Clear()
            ddlAccount.Items.Add(lit)
            collAccounts = ICAccountsController.GetAllAccountsByCompanyCodeForDropDown(companycode)
            For Each objAccount In collAccounts
                lit = New ListItem
                lit.Text = objAccount.AccountTitle
                lit.Value = objAccount.AccountNumber
                ddlAccount.Items.Add(lit)
            Next
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub



    Private Sub LoadBankAccounts()
        Try
            Dim lit As New ListItem
            Dim objAccount As New ICBankAccounts
            Dim collAccounts As New ICBankAccountsCollection

            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBankAccount.Items.Clear()
            ddlBankAccount.Items.Add(lit)
            collAccounts = ICBankAccountController.GetAllBankAccountsByAccountTypesActiveAndApproveIncludingAccountId("Global Charges Account")
            For Each objAccount In collAccounts
                lit = New ListItem
                lit.Text = objAccount.AccountTitle
                lit.Value = objAccount.PrincipalBankAccountID
                ddlBankAccount.Items.Add(lit)
            Next
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub



    Private Sub LoadInvoiceDetails(ByVal invoiceid As Integer)


        Dim inv As New IC.ICPackageInvoice

        If inv.LoadByPrimaryKey(invoiceid) Then


            Me.lblGroupname.Text = inv.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupName

            Me.lblCompanyName.Text = inv.UpToICCompanyByCompanyCode.CompanyName





            Me.lblbillingperiod.Text = inv.InvoiceDateFrom.Value.ToString("dd MMM yyyy") & " - " & inv.InvoiceDateTo.Value.ToString("dd MMM yyyy")


            Me.lbltotalamount.Text = "PKR " & inv.InvoiceAmount.Value.ToString("N3")


            Me.lblamounttobecharged.Text = "PKR " & inv.InvoiceAmount.Value.ToString("N3")


            LoadddlAccounts(inv.CompanyCode)


            LoadBankAccounts()



        End If




    End Sub


    Protected Sub txtdiscountamount_TextChanged(sender As Object, e As EventArgs) Handles txtdiscountamount.TextChanged
        Try

            Dim disamount As Double
            If Me.txtdiscountamount.Text <> "" Then
                If Not Me.txtdiscountamount.Text = 0 Then

                    disamount = Me.lbltotalamount.Text.Split(" ")(1) * (Me.txtdiscountamount.Text / 100)
                    disamount = Me.lbltotalamount.Text.Split(" ")(1) - disamount

                Else

                    disamount = Me.lbltotalamount.Text.Split(" ")(1)
                    txtdiscountamount.Text = ""
                End If
            Else
                disamount = Me.lbltotalamount.Text.Split(" ")(1)
                txtdiscountamount.Text = ""
            End If








            Me.lblamounttobecharged.Text = "PKR " & disamount.ToString("N3")

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try


                Dim pinv As New IC.ICPackageInvoice


                pinv.InvoiceId = Request.QueryString("InvoiceID")



                pinv.ChargeCreatedBy = Me.UserId
                pinv.ChargeCreatedOn = Now
                pinv.IsApproved = False
                pinv.ChargeTransactionStatus = "PendingApproval"



                Dim str() As String = Me.ddlAccount.SelectedValue.Split(",")

                pinv.ClientAccountNumber = str(0)
                pinv.ClientAccountBranchCode = str(1)
                pinv.ClientAccountCurrency = str(2)


                pinv.BankAccountId = Me.ddlBankAccount.SelectedValue.ToString


                If txtdiscountamount.Text <> "" Then
                    pinv.DiscountAmount = Me.txtdiscountamount.Text
                Else
                    pinv.DiscountAmount = Nothing
                End If


                pinv.PaidAmount = Me.lblamounttobecharged.Text.Split(" ")(1)




                'ICBO.ICPackageInvoiceController.UpdatePackageInvoice(pinv, Me.UserId, Me.UserInfo.Username)


                ICBO.ICPackageInvoiceController.UpdatePackageInvoice(pinv, Me.UserId, Me.UserId)



                UIUtilities.ShowDialog(Me, "Package Invoice", "Transaction saved and is now pending for approval", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        If Page.IsValid Then
            Try
                Try
                    If CBUtilities.GetEODStatus() = "01" Then
                        UIUtilities.ShowDialog(Me, "Error", "EOD Status : " & CBUtilities.GetErrorMessageForTransactionFromResponseCode("01", "EOD"), Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Catch ex As Exception
                    UIUtilities.ShowDialog(Me, "Error", "EOD Status : " & ex.Message.ToString, Dialogmessagetype.Failure)
                    Exit Sub
                End Try

                Dim pinv As New IC.ICPackageInvoice
                pinv.LoadByPrimaryKey(Request.QueryString("InvoiceID"))

                pinv.InvoiceId = Request.QueryString("InvoiceID")


                If pinv.ChargeCreatedBy.ToString = Me.UserId.ToString Then
                    UIUtilities.ShowDialog(Me, "Package Invoice", "Transaction must be approved by other than maker", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    Exit Sub
                End If

                If pinv.InvoiceAmount <= 0 Then
                    UIUtilities.ShowDialog(Me, "Error", "Amount should be greater than zero", Dialogmessagetype.Failure)
                    Exit Sub
                End If


                pinv.ChargeApprovedBy = Me.UserId
                pinv.ChargeApprovedOn = Now
                pinv.IsApproved = True
                pinv.ChargeTransactionStatus = "Pending"
                pinv.BankAccountId = ddlBankAccount.SelectedValue.ToString

                Dim status As Boolean

                status = ICBO.ICPackageInvoiceController.ApprovePackageInvoice(pinv, Me.UserId, Me.UserId)



                If status = True Then


                    UIUtilities.ShowDialog(Me, "Package Invoice", "Transaction has been processed successfully", ICBO.IC.Dialogmessagetype.Success, NavigateURL())


                Else


                    UIUtilities.ShowDialog(Me, "Package Invoice", "Transaction cannot be processed due to an error. Please check logs and try again.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())



                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL(), False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        If Page.IsValid Then
            Try
                Dim pinv As New IC.ICPackageInvoice
                pinv.LoadByPrimaryKey(Request.QueryString("InvoiceID"))



                pinv.ChargeCreatedBy = Nothing
                pinv.ChargeCreatedOn = Nothing
                pinv.IsApproved = False
                pinv.ChargeTransactionStatus = Nothing

                pinv.ClientAccountNumber = Nothing
                pinv.ClientAccountBranchCode = Nothing
                pinv.ClientAccountCurrency = Nothing


                pinv.BankAccountId = Nothing

                pinv.Remarks = txtRejectRemarks.Text

                ICBO.ICPackageInvoiceController.UpdatePackageInvoice(pinv, Me.UserId, Me.UserId)

                txtRejectRemarks.Text = ""

                UIUtilities.ShowDialog(Me, "Package Invoice", "Package invoice rejected successfully", ICBO.IC.Dialogmessagetype.Success, NavigateURL())

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
End Class
