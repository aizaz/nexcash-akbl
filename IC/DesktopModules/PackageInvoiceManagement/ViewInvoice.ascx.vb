﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_PackageInvoiceManagement_ViewInvoice
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_CompanyOfficeManagement_ViewCompanyOffice_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then

                ViewState("htRights") = Nothing
                GetPageAccessRights()

                If Not Request.QueryString("id") = "" Then


                    LoadInvoiceDetails(Request.QueryString("id"))



                End If


            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Package Invoice Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
   
    
#Region "Button Events"
    'Protected Sub btnSaveBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveOffice.Click
    '    Response.Redirect(NavigateURL("SaveOffice", "&mid=" & Me.ModuleId & "&id=0"), False)
    'End Sub
#End Region
#Region "Other Functions/Routines"

    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            ' str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function

#End Region
   

    Private Sub LoadInvoiceDetails(ByVal invoiceid As Integer)


        Dim inv As New IC.ICPackageInvoice

        If inv.LoadByPrimaryKey(invoiceid) Then


            Me.lblGroupname.Text = inv.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupName

            Me.lblCompanyName.Text = inv.UpToICCompanyByCompanyCode.CompanyName

            Me.lblinvoicedate.Text = inv.InvoiceDate.Value.ToString("dd MMM yyyy")


            If inv.IsPaid Then

                Me.lblinvoicestatus.Text = "Paid"

                Me.lbpaynow.Visible = False


            Else

                Me.lblinvoicestatus.Text = "Un Paid"
                If CBool(htRights("Pay Invoice")) = True Then
                    Me.lbpaynow.Visible = True
                ElseIf CBool(htRights("Approve Invoice")) = True Then
                    Me.lbpaynow.Visible = True
                Else
                    Me.lbpaynow.Visible = False
                End If




            End If



            Me.lblbillingperiod.Text = inv.InvoiceDateFrom.Value.ToString("dd MMM yyyy") & " - " & inv.InvoiceDateTo.Value.ToString("dd MMM yyyy")


            Me.lbltotalamount.Text = "PKR " & inv.InvoiceAmount.Value.ToString("N3")



            Dim pccoll As New IC.ICPackageChargesCollection





            pccoll.Query.Select(pccoll.Query.ChargeAmount.Sum, pccoll.Query.AccountNumber, pccoll.Query.BranchCode, pccoll.Query.Currency, pccoll.Query.PaymentNatureCode, pccoll.Query.ProductTypeCode, pccoll.Query.ChargeType)


            pccoll.Query.GroupBy(pccoll.Query.AccountNumber, pccoll.Query.BranchCode, pccoll.Query.Currency, pccoll.Query.PaymentNatureCode, pccoll.Query.ProductTypeCode, pccoll.Query.ChargeType)


            pccoll.Query.Where(pccoll.Query.InvoiceId = invoiceid)

            pccoll.Query.OrderBy(pccoll.Query.ChargeType, EntitySpaces.DynamicQuery.esOrderByDirection.Descending)


            If pccoll.Query.Load Then



                Me.gvDetails.DataSource = pccoll
                Me.gvDetails.DataBind()

                Me.gvDetails.Visible = True
            Else




                Me.gvDetails.DataSource = New Object() {}


                Me.gvDetails.DataBind()
                Me.gvDetails.Visible = False




            End If


           



        End If




    End Sub
  
    Protected Sub lbpaynow_Click(sender As Object, e As EventArgs) Handles lbpaynow.Click


        'Response.Redirect(EditUrl("PayInvoice"), True)

        Response.Redirect(NavigateURL("PayInvoice", "&mid=" & Me.ModuleId & "&InvoiceID=" & Request.QueryString("id").ToString & ""), False)


    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Try
            Response.Redirect(NavigateURL(), False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
