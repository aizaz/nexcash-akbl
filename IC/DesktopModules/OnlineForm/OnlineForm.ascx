﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="OnlineForm.ascx.vb" Inherits="DesktopModules_BankRoleAccountPaymentNature_BankRoleAccountPaymentNatureTagging" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:RegExpTextBoxSetting BehaviorID="REUserName" Validation-IsRequired="true"
        ValidationExpression="\b(\w*)\b(.)*\b(-)*\b(_)*\b" ErrorMessage="Invalid Name"
        EmptyMessage="Enter User Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtUserName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior7" Validation-IsRequired="true"
        ValidationExpression="\b(\w*)\b(.)*\b(-)*\b(_)*\b" ErrorMessage="Invalid Display  Name"
        EmptyMessage="Enter Display Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDisplayName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="false"
        EmptyMessage="Enter Role Type">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRoleType" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>--%><%--<script type="text/javascript">
                                  function textboxMultilineMaxNumber(txt, maxLen) {
                                      try {
                                          if (txt.value.length > (maxLen - 1)) return false;
                                      } catch (e) {
                                      }
                                  }
</script>--%>
<script language="javascript" type="text/javascript">
    function AdjustWidth(ddl) {
        var maxWidth = 0;
        var Counter = 0;
        for (var i = 0; i < ddl.length; i++) {
            if (ddl.options[i].text.length > maxWidth) {
                Counter = Counter + 1;
                maxWidth = ddl.options[i].text.length;
            }
        }
        if (Counter = 1) { ddl.style.width = "250px"; }

        else {
            alert(Counter);
            ddl.style.width = maxWidth + "px";
        }
        //-->
    }
</script>
<style type="text/css">
.ctrDropDown{
    width:250px;
   
}
.ctrDropDownClick{
    
    width:600px;
 
}
.plainDropDown{
    width:250px;
   
}
</style>
<table style="width: 100%">
    <tr>
        <td style="width: 100%" colspan="4">
            <asp:Label ID="lblPageHeader" runat="server" Text="Online Instruction" CssClass="headingblue"></asp:Label>
            <br />
            Note:&nbsp; Fields marked as
            <asp:Label ID="lblReqHeader" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqGroup" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqCompany" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlGroup" runat="server" ControlToValidate="ddlGroup"
                Display="Dynamic" ErrorMessage="Please select Group" InitialValue="0" SetFocusOnError="True"
                ValidationGroup="Preview"></asp:RequiredFieldValidator>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" ControlToValidate="ddlCompany"
                Enabled="true" Display="Dynamic" ErrorMessage="Please select Company" InitialValue="0"
                SetFocusOnError="True" ValidationGroup="Preview"></asp:RequiredFieldValidator>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:Label ID="lblSelectAccountPNature" runat="server" Text="Select Account Payment Nature"
                CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqAccPNature" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:Label ID="lblProductType" runat="server" Text="Select Product Type" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqProductType" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <%--<asp:DropDownList ID="ddlAccPNature" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>--%>
             <div style= "width:250px; overflow:hidden;">
            <asp:DropDownList ID="ddlAccPNature" runat="server" class="ctrDropDown"  onblur="this.className='ctrDropDown';" onmousedown="this.className='ctrDropDownClick';" onchange="this.className='ctrDropDown';" AutoPostBack="true">
            </asp:DropDownList>
            </div>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:DropDownList ID="ddlProductType" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlAccPNature" runat="server" ControlToValidate="ddlAccPNature"
                Display="Dynamic" ErrorMessage="Please select Account Payment Nature" InitialValue="0"
                Enabled="true" SetFocusOnError="True" ValidationGroup="Preview"></asp:RequiredFieldValidator>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlProductType" runat="server" ControlToValidate="ddlProductType"
                Display="Dynamic" ErrorMessage="Please select Product Type" InitialValue="0"
                Enabled="true" SetFocusOnError="True" ValidationGroup="Preview"></asp:RequiredFieldValidator>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <asp:Panel ID="pnlBeneficiary" runat="server" Visible="false">
        <tr>
            <td style="width: 25%">
                <asp:Label ID="lblBeneficiaryGroup" runat="server" Text="Select Beneficiary Group" CssClass="lbl"></asp:Label>
                <asp:Label ID="lblReqBeneficiaryGroup" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td style="width: 25%">&nbsp;
            </td>
            <td style="width: 25%">
                <asp:Label ID="lblBeneficiary" runat="server" Text="Select Beneficiary" CssClass="lbl"></asp:Label>
                <asp:Label ID="lblReqBeneficiary" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 25%">
                <asp:DropDownList ID="ddlBeneficiaryGroup" runat="server" CssClass="dropdown" AutoPostBack="true">
                </asp:DropDownList>
            </td>
            <td style="width: 25%">&nbsp;
            </td>
            <td style="width: 25%">
                <asp:DropDownList ID="ddlBeneficiary" runat="server" CssClass="dropdown" AutoPostBack="true">
                </asp:DropDownList>
            </td>
            <td style="width: 25%">&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 25%">
                <asp:RequiredFieldValidator ID="rfvBeneficiaryGroup" runat="server" ControlToValidate="ddlBeneficiaryGroup" Enabled="false"
                    Display="Dynamic" ErrorMessage="Please select Beneficiary Group" InitialValue="0" SetFocusOnError="True"
                    ValidationGroup="Preview"></asp:RequiredFieldValidator>
            </td>
            <td style="width: 25%">&nbsp;
            </td>
            <td style="width: 25%">
                <asp:RequiredFieldValidator ID="rfvBeneficiary" runat="server" ControlToValidate="ddlBeneficiary" Enabled="false"
                    Display="Dynamic" ErrorMessage="Please select Beneficiary" InitialValue="0" SetFocusOnError="True"
                    ValidationGroup="Preview"></asp:RequiredFieldValidator>
            </td>
            <td style="width: 25%">&nbsp;
            </td>
        </tr>
    </asp:Panel>
    <tr>
        <td align="left" colspan="4" valign="top">
            <asp:Panel ID="pnlOnlineFrom" runat="server" Width="100%">
                <table style="width: 100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="top" style="width: 40%">
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr id="trBeneName" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneName" runat="server" Text="Beneficiary Name" CssClass="lbl"></asp:Label>
                                                    <asp:Label ID="lblReqBeneName" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBeneName" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBeneName" runat="server" Enabled="False">
                                                        <telerik:TextBoxSetting BehaviorID="reBeneName" EmptyMessage=""
                                                             ErrorMessage="Invalid Beneficiary Name"
                                                            Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBeneName" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                    <tr id="trBankAccountNumber" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBankAccountNumber" runat="server" CssClass="lbl" Text="Beneficiary Bank Account Number"></asp:Label>
                                                    <asp:Label ID="lblReqBankAccountNumber" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBankAccountNumber" runat="server" CssClass="txtbox" MaxLength="100"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBankAccountNumber" runat="server" Enabled="False">
                                                        <telerik:RegExpTextBoxSetting BehaviorID="reBankAccountNumber" EmptyMessage=""
                                                            ValidationExpression="^[a-zA-Z0-9 -]+$" ErrorMessage="Invalid Bank Account Number"
                                                            Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBankAccountNumber" />
                                                            </TargetControls>
                                                        </telerik:RegExpTextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneficiaryAccountType" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneficiaryAccountType" runat="server" Text="Beneficiary Account Type" CssClass="lbl"></asp:Label>
                                                    <asp:Label ID="lblReqBeneCBAccountType" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:DropDownList ID="ddlBeneCBAccountType" runat="server" CssClass="dropdown" AutoPostBack="true">
                                                        <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                                                        <asp:ListItem>RB</asp:ListItem>
                                                        <asp:ListItem>GL</asp:ListItem>
                                                    </asp:DropDownList> <br />
                                                    <asp:RequiredFieldValidator ID="rfvCBAccountType" runat="server" 
                                                        ControlToValidate="ddlBeneCBAccountType" Display="Dynamic" Enabled="False" 
                                                        ErrorMessage="Please select beneficiary Account Type" InitialValue="0" 
                                                        SetFocusOnError="True" ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                                   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trGLAccountBranchCode" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblGLAccountBranchCode" runat="server" CssClass="lbl" Text="Beneficiary Account Branch Code"></asp:Label>
                                                                <asp:Label ID="lblReqGLAccountBranchCode" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlGLAccountBranchCode" runat="server" 
                                                                    CssClass="dropdown" AutoPostBack="false">
                                                                </asp:DropDownList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:RequiredFieldValidator ID="rfvGLAccountBranchCode" runat="server" ControlToValidate="ddlGLAccountBranchCode"
                                                                    Display="Dynamic" Enabled="False" ErrorMessage="Please select Beneficiary Account Branch Code"
                                                                    InitialValue="0" SetFocusOnError="True" Validation-ValidationGroup="Preview"
                                                                    ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneAccountCurrency" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneAccountCurrency" runat="server" CssClass="lbl" Text="Beneficiary Account Currency"></asp:Label>
                                                    <asp:Label ID="lblReqBeneAccountCurrency" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBeneAccountCurrency" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBeneAccountCurrency" runat="server" Enabled="False">
                                                        <telerik:RegExpTextBoxSetting BehaviorID="reBeneAccountCurrency" EmptyMessage=""
                                                            ValidationExpression="^[a-zA-Z0-9 -]+$" ErrorMessage="Invalid Account Currency"
                                                            Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBeneAccountCurrency" />
                                                            </TargetControls>
                                                        </telerik:RegExpTextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <%--<tr id="trBeneAccountBranchCode" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneAccountBranchCode" runat="server" CssClass="lbl" Text="Beneficiary Account Branch Code"></asp:Label>
                                                    <asp:Label ID="lblReqBeneAccountBranchCode" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBeneAccountBranchCode" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBeneAccountBranchCode" runat="server" Enabled="False">
                                                        <telerik:RegExpTextBoxSetting BehaviorID="reBeneAccountBranchCode" EmptyMessage=""
                                                            ValidationExpression="^[a-zA-Z0-9 -]+$" ErrorMessage="Invalid Account Branch Code"
                                                            Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBeneAccountBranchCode" />
                                                            </TargetControls>
                                                        </telerik:RegExpTextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                             <%--   <tr id="trAccountTitle" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblAccountTitle" runat="server" CssClass="lbl" Text="Beneficiary Account Title"></asp:Label>
                                                    <asp:Label ID="lblReqAccountTitle" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtAccountTitle" runat="server" CssClass="txtbox" MaxLength="100"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radAccountTitle" runat="server" Enabled="False">
                                                        <telerik:TextBoxSetting BehaviorID="reAccountTitle" EmptyMessage=""
                                                            ErrorMessage="Invalid Account Title" Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtAccountTitle" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <tr id="trPrintLocation" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblPrintLocation" runat="server" Text="Print Location" CssClass="lbl"></asp:Label>
                                                    <asp:Label ID="lblReqPrintLocation" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:DropDownList ID="ddlPrintLocation" runat="server" CssClass="dropdown">
                                                    </asp:DropDownList> <br />
                                                    <asp:RequiredFieldValidator ID="rfvddlPrintLocation" runat="server" 
                                                        ControlToValidate="ddlPrintLocation" Display="Dynamic" Enabled="False" 
                                                        ErrorMessage="Please select Print Location" InitialValue="0" 
                                                        SetFocusOnError="True" ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                                   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>                       
                                <%--<tr id="trBeneBankCode" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneBankCode" runat="server" CssClass="lbl" Text="Beneficiary Bank Code"></asp:Label>
                                                                <asp:Label ID="lblReqBeneBankCode" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:TextBox ID="txtBeneBankCode" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox><br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <telerik:RadInputManager ID="radBeneBankCode" runat="server" Enabled="False">
                                                                    <telerik:TextBoxSetting BehaviorID="reBeneBankCode" EmptyMessage="Enter Beneficiary Bank Code"
                                                                        ErrorMessage="Invalid Beneficiary Bank Code" Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                                        <TargetControls>
                                                                            <telerik:TargetInput ControlID="txtBeneBankCode" />
                                                                        </TargetControls>
                                                                    </telerik:TextBoxSetting>
                                                                </telerik:RadInputManager>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <%--<tr id="trClientBankName" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblClientBankName" runat="server" CssClass="lbl" Text="Client Bank"></asp:Label>
                                                                <asp:Label ID="lblReqClientBankName" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlClientBankName" runat="server" CssClass="dropdown">
                                                                </asp:DropDownList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:RequiredFieldValidator ID="rfvddlClientBankName" runat="server" ControlToValidate="ddlClientBankName"
                                                                    Display="Dynamic" Enabled="False" ErrorMessage="Please select Client Bank" InitialValue="0"
                                                                    SetFocusOnError="True" ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <tr id="trDDPayableLocationCity" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblDDPayableLocationCity" runat="server" CssClass="lbl" Text="DD Drawn City"></asp:Label>
                                                                <asp:Label ID="lblReqDDPayableLocationCity" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlDDPayableLocationCity" runat="server" CssClass="dropdown">
                                                                </asp:DropDownList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:RequiredFieldValidator ID="rfvddlDDPayableLocationCity" runat="server" ControlToValidate="ddlDDPayableLocationCity"
                                                                    Display="Dynamic" Enabled="False" ErrorMessage="Please select DD Payable Location City"
                                                                    InitialValue="0" SetFocusOnError="True" Validation-ValidationGroup="Preview"
                                                                    ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr id="trBeneMobNo" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneMobNo" runat="server" CssClass="lbl" Text="Mobile No."></asp:Label>
                                                                <asp:Label ID="lblReqBeneMobNo" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:TextBox ID="txtBeneMobileNo" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox><br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <telerik:RadInputManager ID="radBeneMobNo" runat="server" Enabled="False">
                                                                    <telerik:RegExpTextBoxSetting BehaviorID="reBeneMobNo" EmptyMessage=""
                                                                        ValidationExpression="^\+?[\d- ]{2,15}$" ErrorMessage="Invalid Mobile No." Validation-IsRequired="false"
                                                                        Validation-ValidationGroup="Preview">
                                                                        <TargetControls>
                                                                            <telerik:TargetInput ControlID="txtBeneMobileNo" />
                                                                        </TargetControls>
                                                                    </telerik:RegExpTextBoxSetting>
                                                                </telerik:RadInputManager>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneIDNo" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneIDNo" runat="server" CssClass="lbl" Text="Beneficiary ID No."></asp:Label>
                                                    <asp:Label ID="lblReqBeneIDNo" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBeneIDNo" runat="server" CssClass="txtbox" MaxLength="30"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBeneIDNo" runat="server" Enabled="False">
                                                        <telerik:RegExpTextBoxSetting BehaviorID="reBeneIDNo" EmptyMessage=""
                                                            ValidationExpression="^[a-zA-Z0-9 -]+$" ErrorMessage="Invalid Beneficiary ID No"
                                                            Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBeneIDNo" />
                                                            </TargetControls>
                                                        </telerik:RegExpTextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneEmail" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblEmail" runat="server" CssClass="lbl" Text="Email"></asp:Label>
                                                                <asp:Label ID="lblReqEmail" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:TextBox ID="txtBeneEmail" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox><br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <telerik:RadInputManager ID="radBeneEmail" runat="server" Enabled="False">
                                                                    <telerik:TextBoxSetting BehaviorID="reBeneEmail" EmptyMessage=""
                                                                        ErrorMessage="Invalid Email Address" Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                                        <TargetControls>
                                                                            <telerik:TargetInput ControlID="txtBeneEmail" />
                                                                        </TargetControls>
                                                                    </telerik:TextBoxSetting>
                                                                </telerik:RadInputManager>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneCity" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneCity" runat="server" CssClass="lbl" Text="Beneficiary City"></asp:Label>
                                                                <asp:Label ID="lblReqBeneficiaryCity" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlBeneficiaryCity" runat="server" CssClass="dropdown">
                                                                </asp:DropDownList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:RequiredFieldValidator ID="rfvddlBeneficiaryCity" runat="server" ControlToValidate="ddlBeneficiaryCity"
                                                                    Display="Dynamic" Enabled="False" ErrorMessage="Please select Beneficiary City"
                                                                    InitialValue="0" SetFocusOnError="True" Validation-ValidationGroup="Preview"
                                                                    ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneCountry" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneCountry" runat="server" CssClass="lbl" Text="Beneficiary Country"></asp:Label>
                                                                <asp:Label ID="lblReqBeneCountry" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlBeneficiaryCountry" runat="server" CssClass="dropdown">
                                                                </asp:DropDownList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:RequiredFieldValidator ID="rfvddlBeneficiaryCountry" runat="server" ControlToValidate="ddlBeneficiaryProvince"
                                                                    Display="Dynamic" Enabled="False" ErrorMessage="Please select Beneficiary Country"
                                                                    InitialValue="0" Validation-ValidationGroup="Preview" SetFocusOnError="True"
                                                                    ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                 <tr id="trUBPCompany" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblUBPCompanyCode" runat="server" CssClass="lbl" Text="UBP Company"></asp:Label>
                                                                <asp:Label ID="lblReqUBPCompany" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlUBPCompany" runat="server" CssClass="dropdown" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:RequiredFieldValidator ID="rfvUBPCompany" runat="server" ControlToValidate="ddlUBPCompany"
                                                                    Display="Dynamic" Enabled="False" ErrorMessage="Please select UBP Company"
                                                                    InitialValue="0" Validation-ValidationGroup="Preview" SetFocusOnError="True"
                                                                    ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trUBPReferenceField" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblUBPReferenceField" runat="server" CssClass="lbl" Text="UBP Reference"></asp:Label>
                                                                <asp:Label ID="lblReqUBPReferenceField" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:TextBox ID="txtUBPReferenceField" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox><br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <telerik:RadInputManager ID="radUBPReferenceField" runat="server" Enabled="False">
                                                                    <telerik:TextBoxSetting BehaviorID="reUBPReferenceField" EmptyMessage=""
                                                                        ErrorMessage="Invalid UBP Reference Field" Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                                        <TargetControls>
                                                                            <telerik:TargetInput ControlID="txtUBPReferenceField" />
                                                                        </TargetControls>
                                                                    </telerik:TextBoxSetting>
                                                                </telerik:RadInputManager>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <%--   <tr id="trClientProvince" runat="server">
                               <td style="width: 100%">
                                   <table style="width: 100%">
                                       <tr>
                                           <td style="width: 100%">
                                               <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                   <tr>
                                                       <td style="width: 100%">
                                                           <asp:Label ID="lblClientProvince" runat="server" CssClass="lbl" Text="Client Province"></asp:Label>
                                                           <asp:Label ID="lblReqClientProvince" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                       </td>
                                                   </tr>
                                                   <tr>
                                                       <td align="left" style="width: 100%">
                                                           <asp:DropDownList ID="ddlClientProvince" runat="server" CssClass="dropdown">
                                                           </asp:DropDownList>
                                                       </td>
                                                   </tr>
                                                   <tr>
                                                       <td style="width: 100%">
                                                           <asp:RequiredFieldValidator ID="rfvddlClientProvince" runat="server" 
                                                               ControlToValidate="ddlClientProvince" Display="Dynamic" Enabled="False" 
                                                               ErrorMessage="Please select Client Province" InitialValue="0" 
                                                               SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                       </td>
                                                   </tr>
                                               </table>
                                           </td>
                                       </tr>
                                   </table>
                               </td>
                           </tr>--%>
                                <%-- <tr id="trBeneAddress2" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneAddress2" runat="server" CssClass="lbl" Text="Address 2"></asp:Label>
                                                    <asp:Label ID="lblReqBeneAddress2" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBeneAddress2" runat="server" CssClass="txtbox" Height="60px"
                                                        MaxLength="150" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBeneAddress2" runat="server" Enabled="False">
                                                        <telerik:TextBoxSetting BehaviorID="reBeneAddress2" EmptyMessage="Enter Address"
                                                            Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBeneAddress2" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <tr id="trAmountPKR" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblAmountPKR" runat="server" Text="Amount" CssClass="lbl"></asp:Label>
                                                    <asp:Label ID="lblReqAmountPKR" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:TextBox ID="txtAmountPKR" runat="server" CssClass="txtbox" MaxLength="14" AutoPostBack="True"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radAmountPKR" runat="server" Enabled="False" >
                                                        <telerik:NumericTextBoxSetting BehaviorID="reAmountPKR" EmptyMessage="" AllowRounding="false"
                                                            Validation-IsRequired="false" Type="Number" DecimalDigits="2" ErrorMessage="Invalid amount"
                                                            Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtAmountPKR" />
                                                            </TargetControls>
                                                        </telerik:NumericTextBoxSetting>
                                                        <%-- <telerik:NumericTextBoxSetting BehaviorID="reAmountPKR" EmptyMessage="Enter Amount in PKR"
                                                            ErrorMessage="Invalid Amount" Validation-IsRequired="true" DecimalDigits="0">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtAmountPKR" />
                                                            </TargetControls>
                                                        </telerik:NumericTextBoxSetting>--%>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trRemarks" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblRemarks" runat="server" CssClass="lbl" Text="Remarks"></asp:Label>
                                                    <asp:Label ID="lblReqRemarks" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="txtbox" Height="60px" MaxLength="150"
                                                        TextMode="MultiLine" ></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radRemarks" runat="server" Enabled="False">
                                                        <telerik:TextBoxSetting BehaviorID="reRemarks" EmptyMessage="" Validation-IsRequired="false"
                                                            Validation-ValidationGroup="Preview" ErrorMessage="Invalid Remarks">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtRemarks" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trReferenceField1" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblReferenceField1" runat="server" CssClass="lbl" Text="Reference Field 1"></asp:Label>
                                                    <asp:Label ID="lblReqReferenceField1" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtReferenceField1" runat="server" CssClass="txtbox" Height="60px"
                                                        MaxLength="150" TextMode="MultiLine" ></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radReferenceField1" runat="server" Enabled="False">
                                                        <telerik:TextBoxSetting BehaviorID="reReferenceField1" EmptyMessage=""
                                                            ErrorMessage="Invalid Remarks" Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtReferenceField1" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trReferenceField3" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblReferenceField3" runat="server" CssClass="lbl" Text="Reference Field 3"></asp:Label>
                                                    <asp:Label ID="lblReqReferenceField3" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtReferenceField3" runat="server" CssClass="txtbox" Height="60px"
                                                        MaxLength="150" TextMode="MultiLine" ></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radReferenceField3" runat="server" Enabled="False">
                                                        <telerik:TextBoxSetting BehaviorID="reReferenceField3" EmptyMessage=""
                                                            ErrorMessage="Invalid Remarks" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtReferenceField3" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trDescription" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblDescription" runat="server" CssClass="lbl" Text="Description"></asp:Label>
                                                    <asp:Label ID="lblReqDescription" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtDescription" runat="server" CssClass="txtbox" Height="60px" MaxLength="150"
                                                        TextMode="MultiLine" ></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radDescription" runat="server" Enabled="False">
                                                        <telerik:TextBoxSetting BehaviorID="reDescription" EmptyMessage=""
                                                            Validation-IsRequired="false" ErrorMessage="Invalid Description" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtDescription" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneBranchAddress" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneBranchAddress" runat="server" CssClass="lbl" Text="Beneficiary Branch Address"></asp:Label>
                                                    <asp:Label ID="lblReqBeneBranchAddress" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBeneficiaryBranchAddress" runat="server" CssClass="txtbox" Height="60px" MaxLength="500"
                                                        TextMode="MultiLine" ></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBeneBranchAddress" runat="server" Enabled="False">
                                                        <telerik:TextBoxSetting BehaviorID="reBeneBranchAddress" EmptyMessage=""
                                                            Validation-IsRequired="false" ErrorMessage="Invalid Beneficiary Branch Address" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBeneficiaryBranchAddress" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="left" valign="top" style="width: 10%">
                        </td>
                        <td align="left" valign="top" style="width: 40%">
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                    <tr id="trBeneBankName" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBankName" runat="server" CssClass="lbl" Text="Beneficiary Bank"></asp:Label>
                                                    <asp:Label ID="lblReqBankName" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:DropDownList ID="ddlBeneBankName" runat="server" CssClass="dropdown">
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:RequiredFieldValidator ID="rfvddlBeneBank" runat="server" ControlToValidate="ddlBeneBankName"
                                                        Display="Dynamic" Enabled="False" ErrorMessage="Please select Beneficiary Bank"
                                                        InitialValue="0" SetFocusOnError="True" ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            <%--    <tr id="trBankAccountNumber" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBankAccountNumber" runat="server" CssClass="lbl" Text="Beneficiary Bank Account Number"></asp:Label>
                                                    <asp:Label ID="lblReqBankAccountNumber" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBankAccountNumber" runat="server" CssClass="txtbox" MaxLength="100"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBankAccountNumber" runat="server" Enabled="False">
                                                        <telerik:RegExpTextBoxSetting BehaviorID="reBankAccountNumber" EmptyMessage=""
                                                            ValidationExpression="^[a-zA-Z0-9 -]+$" ErrorMessage="Invalid Bank Account Number"
                                                            Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBankAccountNumber" />
                                                            </TargetControls>
                                                        </telerik:RegExpTextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                   <tr id="trAccountTitle" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblAccountTitle" runat="server" CssClass="lbl" Text="Beneficiary Account Title"></asp:Label>
                                                    <asp:Label ID="lblReqAccountTitle" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtAccountTitle" runat="server" CssClass="txtbox" MaxLength="100"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radAccountTitle" runat="server" Enabled="False">
                                                        <telerik:TextBoxSetting BehaviorID="reAccountTitle" EmptyMessage=""
                                                            ErrorMessage="Invalid Account Title" Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtAccountTitle" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneAccountBranchCode" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneAccountBranchCode" runat="server" CssClass="lbl" Text="Beneficiary Account Branch Code"></asp:Label>
                                                    <asp:Label ID="lblReqBeneAccountBranchCode" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBeneAccountBranchCode" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBeneAccountBranchCode" runat="server" Enabled="False">
                                                        <telerik:RegExpTextBoxSetting BehaviorID="reBeneAccountBranchCode" EmptyMessage=""
                                                            ValidationExpression="^[a-zA-Z0-9 -]+$" ErrorMessage="Invalid Account Branch Code"
                                                            Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBeneAccountBranchCode" />
                                                            </TargetControls>
                                                        </telerik:RegExpTextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <%--<tr id="trBeneAccountCurrency" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneAccountCurrency" runat="server" CssClass="lbl" Text="Beneficiary Account Currency"></asp:Label>
                                                    <asp:Label ID="lblReqBeneAccountCurrency" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBeneAccountCurrency" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBeneAccountCurrency" runat="server" Enabled="False">
                                                        <telerik:RegExpTextBoxSetting BehaviorID="reBeneAccountCurrency" EmptyMessage=""
                                                            ValidationExpression="^[a-zA-Z0-9 -]+$" ErrorMessage="Invalid Account Currency"
                                                            Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBeneAccountCurrency" />
                                                            </TargetControls>
                                                        </telerik:RegExpTextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                            <%--    <tr id="trBeneBankName" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBankName" runat="server" CssClass="lbl" Text="Beneficiary Bank"></asp:Label>
                                                    <asp:Label ID="lblReqBankName" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:DropDownList ID="ddlBeneBankName" runat="server" CssClass="dropdown">
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:RequiredFieldValidator ID="rfvddlBeneBank" runat="server" ControlToValidate="ddlBeneBankName"
                                                        Display="Dynamic" Enabled="False" ErrorMessage="Please select Beneficiary Bank"
                                                        InitialValue="0" SetFocusOnError="True" ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <%--<tr id="trBankBranchCode" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBankBranchCode" runat="server" CssClass="lbl" Text="Beneficiary Bank Branch Code"></asp:Label>
                                                    <asp:Label ID="lblReqBankBranchCode" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBankBranchCode" runat="server" CssClass="txtbox" MaxLength="15"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBankBranchCode" runat="server" Enabled="False">
                                                        <telerik:TextBoxSetting BehaviorID="reBankCode" EmptyMessage="Enter Bank Branch Code"
                                                            ErrorMessage="Invalid Bank Branch Code" Validation-IsRequired="False" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBankBranchCode" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <%-- <tr id="trBankIMD" runat="server">
                                    <td align="left" valign="top" style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBankIMD" runat="server" CssClass="lbl" Text="Beneficary Bank IMD"></asp:Label>
                                                    <asp:Label ID="lblReqBankIMD" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBankIMD" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBankIMD" runat="server" Enabled="False">
                                                        <telerik:TextBoxSetting BehaviorID="reBankIMD" EmptyMessage="Enter Bank IMD" ErrorMessage="Invalid Bank IMD"
                                                            Validation-IsRequired="true">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBankIMD" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <%--<tr id="trBranchName" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBranchName" runat="server" CssClass="lbl" Text="Beneficiary Bank Branch"></asp:Label>
                                                                <asp:Label ID="lblReqBranchName" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlBeneBankBranch" runat="server" CssClass="dropdown">
                                                                </asp:DropDownList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:RequiredFieldValidator ID="rfvddlBeneBankBranch" runat="server" ControlToValidate="ddlBeneBankBranch"
                                                                    Display="Dynamic" Enabled="False" ErrorMessage="Please select Beneficiary Bank"
                                                                    InitialValue="0" SetFocusOnError="True" ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <%--<tr id="trBeneBankAddress" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBankAddress" runat="server" CssClass="lbl" Text="Beneficiary Bank Address"></asp:Label>
                                                    <asp:Label ID="lblReqBankaddress" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBankAddress" runat="server" CssClass="txtbox" Height="60px" MaxLength="150"
                                                        TextMode="MultiLine"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBankAddress" runat="server" Enabled="False">
                                                        <telerik:TextBoxSetting BehaviorID="reBankAddress" EmptyMessage="Enter Bank Address"
                                                            Validation-IsRequired="false">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBankAddress" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <tr id="trDDPayableLocationProvince" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblDDPayableLocationProvince" runat="server" CssClass="lbl" Text="DD Drawn Location Province"></asp:Label>
                                                                <asp:Label ID="lblReqDDPayableLocationProvince" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlDDPayableLocationProvince" runat="server" 
                                                                    CssClass="dropdown" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:RequiredFieldValidator ID="rfvddlDDPayableLocationProvince" runat="server" ControlToValidate="ddlDDPayableLocationProvince"
                                                                    Display="Dynamic" Enabled="False" ErrorMessage="Please select DD Payable Location Province"
                                                                    InitialValue="0" SetFocusOnError="True" Validation-ValidationGroup="Preview"
                                                                    ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBenePickUpLocation" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBenePickUpLocation" runat="server" CssClass="lbl" Text="Pick Up Location"></asp:Label>
                                                                <asp:Label ID="lblReqBenePickLocation" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlBenePickLocation" runat="server" 
                                                                    CssClass="dropdown" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:RequiredFieldValidator ID="rfvddlBenePickLocation" runat="server" ControlToValidate="ddlBenePickLocation"
                                                                    Display="Dynamic" Enabled="False" ErrorMessage="Please select Beneficiary Pick Location"
                                                                    InitialValue="0" SetFocusOnError="True" Validation-ValidationGroup="Preview"
                                                                    ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneficiaryIdentityType" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneIdentityType" runat="server" CssClass="lbl" Text="Beneficiary ID Type"></asp:Label>
                                                                <asp:Label ID="lblReqBeneIDType" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlBeneIDType" runat="server" 
                                                                    CssClass="dropdown" AutoPostBack="false">
                                                                    <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                                                                    <asp:ListItem>CNIC</asp:ListItem>
                                                                    <asp:ListItem>Passport</asp:ListItem>
                                                                    <asp:ListItem>NICOP</asp:ListItem>
                                                                    <asp:ListItem>Driving License</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:RequiredFieldValidator ID="rfvBeneIDType" runat="server" ControlToValidate="ddlBeneIDType"
                                                                    Display="Dynamic" Enabled="False" ErrorMessage="Please select Beneficiary ID Type"
                                                                    InitialValue="0" SetFocusOnError="True"
                                                                    ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trInstrumentNo" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblInstrumentNo" runat="server" CssClass="lbl" Text="Instrument No."></asp:Label>
                                                                <asp:Label ID="lblReqInstrumentNo" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:TextBox ID="txtInstrumentNo" runat="server" CssClass="txtbox" MaxLength="18"></asp:TextBox><br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <telerik:RadInputManager ID="radInstrumentNo" runat="server" Enabled="False">
                                                                    <telerik:RegExpTextBoxSetting BehaviorID="reInstrumentNo" EmptyMessage=""
                                                                        ValidationExpression="^\d+$" ErrorMessage="Invalid Instrument No." Validation-IsRequired="false"
                                                                        Validation-ValidationGroup="Preview">
                                                                        <TargetControls>
                                                                            <telerik:TargetInput ControlID="txtInstrumentNo" />
                                                                        </TargetControls>
                                                                    </telerik:RegExpTextBoxSetting>
                                                                </telerik:RadInputManager>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBenePhoneNo" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBenePhoneNo" runat="server" CssClass="lbl" Text="Phone No."></asp:Label>
                                                                <asp:Label ID="lblReqPhonNo" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:TextBox ID="txtBenePhoneNo" runat="server" CssClass="txtbox" MaxLength="15"></asp:TextBox><br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <telerik:RadInputManager ID="radBenePhoneNo" runat="server" Enabled="False">
                                                                    <telerik:RegExpTextBoxSetting BehaviorID="reBenePhoneNo" EmptyMessage=""
                                                                        ValidationExpression="^\+?[\d- ]{2,15}$" ErrorMessage="Invalid Phone No." Validation-IsRequired="false"
                                                                        Validation-ValidationGroup="Preview">
                                                                        <TargetControls>
                                                                            <telerik:TargetInput ControlID="txtBenePhoneNo" />
                                                                        </TargetControls>
                                                                    </telerik:RegExpTextBoxSetting>
                                                                </telerik:RadInputManager>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr id="trValueDate" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblValueDate" runat="server" CssClass="lbl" Text="Value Date"></asp:Label>
                                                                <asp:Label ID="lblReqValueDate" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                               
                                                               <%-- <telerik:RadDatePicker ID="txtValueDate" runat="server"  EnableTyping="false"
                                                                    DateInput-DateFormat="dd-MMM-yyyy" DateInput-EmptyMessage="" 
                                                                    ImagesPath="" SharedCalendarID="sharedCalendarValueDate">
                                                                </telerik:RadDatePicker>
                                                                
                                                                <telerik:RadCalendar ID="sharedCalendarValueDate" runat="server"
                                                                    dateinput-dateformat="dd-MMM-yyyy" enablemultiselect="False" 
                                                                    ViewSelectorText="x">
                                                                </telerik:RadCalendar>--%>

                                                                 <telerik:RadDatePicker ID="txtValueDate" runat="server" Width="180px" EnableTyping="false">
                                                                    <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                                    </Calendar>
                                                                    <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                                        Font-Size="10.5px" LabelWidth="40%" type="text" value="">
                                                                    </DateInput>
                                                                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                                </telerik:RadDatePicker>
                                                               
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                               <asp:RequiredFieldValidator ID="rfvValueDate" ControlToValidate="txtValueDate" Display="Dynamic" runat="server" ErrorMessage="Please select Value Date"
                                                                Enabled="false" SetFocusOnError="true" InitialValue="" ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trPrintDate" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblPrintDate" runat="server" CssClass="lbl" Text="Print Date"></asp:Label>
                                                                <asp:Label ID="lblReqPrintDate" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                               
                                                               <%-- <telerik:RadDatePicker ID="txtPrintDate" runat="server"  EnableTyping="false"
                                                                    DateInput-DateFormat="dd-MMM-yyyy" DateInput-EmptyMessage="" 
                                                                    ImagesPath="" SharedCalendarID="sharedCalenderPrintDate">
                                                                </telerik:RadDatePicker>
                                                                
                                                                <telerik:RadCalendar ID="sharedCalenderPrintDate" runat="server"
                                                                    dateinput-dateformat="dd-MMM-yyyy" enablemultiselect="False" 
                                                                    ViewSelectorText="x">
                                                                </telerik:RadCalendar>--%>

                                                                <telerik:RadDatePicker ID="txtPrintDate" runat="server" Width="180px" EnableTyping="false">
                                                                    <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                                    </Calendar>
                                                                    <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                                        Font-Size="10.5px" LabelWidth="40%" type="text" value="">
                                                                    </DateInput>
                                                                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                                </telerik:RadDatePicker>
                                                               
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                               <asp:RequiredFieldValidator ID="rfvPrintDate" ControlToValidate="txtPrintDate" Display="Dynamic" runat="server" ErrorMessage="Please select Print Date"
                                                                Enabled="false" SetFocusOnError="true" InitialValue="" ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneProvince" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneProvince" runat="server" CssClass="lbl" Text="Beneficiary Province"></asp:Label>
                                                                <asp:Label ID="lblReqBeneProvince" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlBeneficiaryProvince" runat="server" CssClass="dropdown">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:RequiredFieldValidator ID="rfvddlBeneficiaryProvince" runat="server" ControlToValidate="ddlBeneficiaryProvince"
                                                                    Display="Dynamic" Enabled="False" ErrorMessage="Please select Beneficiary Province"
                                                                    InitialValue="0" SetFocusOnError="True" ValidationGroup="Preview"></asp:RequiredFieldValidator><br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneCNICNo" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneCNIC" runat="server" CssClass="lbl" Text="CNIC No."></asp:Label>
                                                                <asp:Label ID="lblReqBeneNICNo" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:TextBox ID="txtBeneCNCINo" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox><br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <telerik:RadInputManager ID="radBeneCNICNo" runat="server" Enabled="False">
                                                                    <telerik:RegExpTextBoxSetting BehaviorID="reBeneCNICNo" EmptyMessage=""
                                                                        ErrorMessage="Invalid CNIC No" Validation-IsRequired="false" Validation-ValidationGroup="Preview"
                                                                        ValidationExpression="[0-9-]{1,50}">
                                                                        <TargetControls>
                                                                            <telerik:TargetInput ControlID="txtBeneCNCINo" />
                                                                        </TargetControls>
                                                                    </telerik:RegExpTextBoxSetting>
                                                                </telerik:RadInputManager>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                               
                           <tr id="trAmountInWords" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblAmountInWords" runat="server" CssClass="lbl" Text="Amount In Words"></asp:Label>
                                                    <asp:Label ID="lblReqAmountInWords" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtAmountInWords" runat="server" CssClass="txtbox" Height="60px"
                                                        MaxLength="150" TextMode="MultiLine" ReadOnly="true"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radAmountInWords" runat="server" Enabled="False">
                                                        <telerik:TextBoxSetting BehaviorID="reAmountInWords" EmptyMessage=""
                                                            ErrorMessage="Invalid Amount" Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtAmountInWords" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trReferenceField2" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lbltrReferenceField2" runat="server" CssClass="lbl" Text="Reference Field 2"></asp:Label>
                                                    <asp:Label ID="lblReqtrReferenceField2" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txttrReferenceField2" runat="server" CssClass="txtbox" Height="60px"
                                                        MaxLength="150" TextMode="MultiLine" ></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radtrReferenceField2" runat="server" Enabled="False">
                                                        <telerik:TextBoxSetting BehaviorID="retrReferenceField2" EmptyMessage=""
                                                            ErrorMessage="Invalid Remarks" Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txttrReferenceField2" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneAddress1" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneAddress" runat="server" CssClass="lbl" Text="Address 1"></asp:Label>
                                                    <asp:Label ID="lblReqBeneAddress1" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBeneAddress1" runat="server" CssClass="txtbox" Height="60px"
                                                        MaxLength="150" TextMode="MultiLine" ></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBeneAddress1" runat="server" Enabled="False">
                                                        <telerik:TextBoxSetting BehaviorID="reBeneAddress1" EmptyMessage=""
                                                            ErrorMessage="Invalid Address" Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBeneAddress1" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                
                                
                                
                            </table>
                        </td>
                        <td align="left" valign="top" style="width: 10%">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" colspan="4">
                            <asp:CheckBox ID="rbScheduleTransaction" runat="server" AutoPostBack="True" 
                                Text="Schedule Transaction" />
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4" valign="top">

                        <table id="tblScheduleTxn" runat="server" style="width:100%">
                        <tr>
                        <td align="left" valign="top" style="width:25%;">
                            <asp:Label ID="lblScheduleTitle" runat="server" CssClass="lbl" Text="Title"></asp:Label>
                            <asp:Label ID="lblReqScheduleTitle" runat="server" ForeColor="Red" Text="*"></asp:Label>
                            </td>
                        <td align="left" valign="top" style="width:25%;"></td>
                        <td align="left" valign="top" style="width:25%;">
                            <asp:Label ID="lblScheduleFrequency" runat="server" CssClass="lbl" 
                                Text="Frequency"></asp:Label>
                            <asp:Label ID="lblReqScheduleFrequency" runat="server" ForeColor="Red" Text="*"></asp:Label>
                            </td>
                        <td align="left" valign="top" style="width:25%;"></td>
                        </tr>
                            <tr>
                                <td align="left" style="width:25%;" valign="top">
                                    <asp:TextBox ID="txtScheduleTitle" runat="server" CssClass="txtbox" 
                                        MaxLength="150"></asp:TextBox>
                                </td>
                                <td align="left" style="width:25%;" valign="top">
                                    &nbsp;</td>
                                <td align="left" style="width:25%;" valign="top">
                                    <asp:DropDownList ID="ddlScheduleFrequency" runat="server" CssClass="dropdown" 
                                        AutoPostBack="True">
                                        <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                                        <asp:ListItem Value="Daily">Daily</asp:ListItem>
                                        <asp:ListItem Value="Monthly">Monthly</asp:ListItem>
                                        <asp:ListItem Value="Number Of Days">Number Of Days</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td align="left" style="width:25%;" valign="top">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left" style="width:25%;" valign="top">
                                    <telerik:RadInputManager ID="radScheduleTitle" runat="server" Enabled="False">
                                        <telerik:TextBoxSetting BehaviorID="reScheduleTitle" EmptyMessage="" 
                                            ErrorMessage="Invalid Title" Validation-IsRequired="false" 
                                            Validation-ValidationGroup="Preview">
                                            <TargetControls>
                                                <telerik:TargetInput ControlID="txtScheduleTitle" />
                                            </TargetControls>
                                        </telerik:TextBoxSetting>
                                    </telerik:RadInputManager>
                                </td>
                                <td align="left" style="width:25%;" valign="top">
                                    &nbsp;</td>
                                <td align="left" style="width:25%;" valign="top">
                                    <asp:RequiredFieldValidator ID="rfvddlScheduleFrequency" runat="server" 
                                        ControlToValidate="ddlScheduleFrequency" Display="Dynamic" Enabled="False" 
                                        ErrorMessage="Please select Schedule Frequency" InitialValue="0" 
                                        SetFocusOnError="True" ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="width:25%;" valign="top">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left" style="width:25%;" valign="top">
                                    <asp:Label ID="lblTransactionTime" runat="server" CssClass="lbl" 
                                        Text="Transaction Time"></asp:Label>
                                    <asp:Label ID="lblReqTransactionTime" runat="server" ForeColor="Red" 
                                        Text="*"></asp:Label>
 </td>
                                <td align="left" style="width:25%;" valign="top">
                                    &nbsp;</td>
                                <td align="left" style="width:25%;" valign="top">
                                    &nbsp;</td>
                                <td align="left" style="width:25%;" valign="top">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left" style="width:25%;" valign="top">
                                    
                                    <telerik:radTimePicker ID="radTimePicker" runat="server" InputMode="TimeView">
                                       
                                    </telerik:radTimePicker><br />
                                    <asp:RequiredFieldValidator ID="rfvTransactionTime" runat="server" 
                                        ControlToValidate="radTimePicker" Display="Dynamic" Enabled="false" 
                                        ErrorMessage="Please select Transaction Time" InitialValue="" 
                                        SetFocusOnError="true" ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                    
                                    
                                </td>
                                <td align="left" style="width:25%;" valign="top">
                                    &nbsp;</td>
                                <td align="left" style="width:25%;" valign="top">
                                    <table ID="tblDayType" runat="server" style="width:100%">
                                        <tr>
                                            <td align="left" style="width:100%;" valign="top">
                                                <asp:Label ID="lblDayType" runat="server" CssClass="lbl" Text="Day Type"></asp:Label>
                                                <asp:Label ID="lblReqDayType" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width:100%;" valign="top">
                                                <asp:RadioButtonList ID="rblDaysOfMonth" runat="server" 
                                                    RepeatDirection="Horizontal" AutoPostBack="True">
                                                    <asp:ListItem Value="First Day">First Day</asp:ListItem>
                                                    <asp:ListItem Value="Last Day">Last Day</asp:ListItem>
                                                    <asp:ListItem Value="Specific Day">Specific Day</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width:100%;" valign="top">
                                                <asp:RequiredFieldValidator ID="rfvrblrblDaysOfMonth" runat="server" 
                                                    ControlToValidate="rblDaysOfMonth" Display="Dynamic" Enabled="False" 
                                                    ErrorMessage="Please select Day Type" InitialValue="" 
                                                    SetFocusOnError="True" ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                    <table ID="tblNoOfDays" runat="server" style="width:100%">
                                        <tr>
                                            <td align="left" style="width:100%;" valign="top">
                                                <asp:Label ID="lblNoOfDays" runat="server" CssClass="lbl" Text="No of Days"></asp:Label>
                                                <asp:Label ID="lblReqNoOfDays" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width:100%;" valign="top">
                                                <asp:TextBox ID="txtNoOfDays" runat="server" CssClass="txtbox" MaxLength="2" 
                                                    AutoPostBack="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width:100%;" valign="top">
                                                <telerik:RadInputManager ID="radNoOfDay" runat="server" Enabled="False">
                                                    <telerik:RegExpTextBoxSetting BehaviorID="reNoOfDays" EmptyMessage="" 
                                                        ErrorMessage="Invalid No Of Days" Validation-IsRequired="false" 
                                                        Validation-ValidationGroup="Preview" ValidationExpression="^\+?[\d]{1,2}$">
                                                        <TargetControls>
                                                            <telerik:TargetInput ControlID="txtNoOfDays" />
                                                        </TargetControls>
                                                    </telerik:RegExpTextBoxSetting>
                                                </telerik:RadInputManager>
                                            </td>
                                        </tr>
                                    </table>
                                    <table ID="tblSpecificDay" runat="server" style="width:100%">
                                        <tr>
                                            <td align="left" style="width:100%;" valign="top">
                                                <asp:Label ID="lblSpecificDay" runat="server" CssClass="lbl" 
                                                    Text="Specific Day"></asp:Label>
                                                <asp:Label ID="lblReqSpecificDay" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width:100%;" valign="top">
                                                <asp:DropDownList ID="ddlSpecificDay" runat="server" CssClass="dropdown">
                                                    <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                                                    <asp:ListItem>1</asp:ListItem>
                                                    <asp:ListItem>2</asp:ListItem>
                                                    <asp:ListItem>3</asp:ListItem>
                                                    <asp:ListItem>4</asp:ListItem>
                                                    <asp:ListItem>5</asp:ListItem>
                                                    <asp:ListItem>6</asp:ListItem>
                                                    <asp:ListItem>7</asp:ListItem>
                                                    <asp:ListItem>8</asp:ListItem>
                                                    <asp:ListItem>9</asp:ListItem>
                                                    <asp:ListItem>10</asp:ListItem>
                                                    <asp:ListItem>11</asp:ListItem>
                                                    <asp:ListItem>12</asp:ListItem>
                                                    <asp:ListItem>13</asp:ListItem>
                                                    <asp:ListItem>14</asp:ListItem>
                                                    <asp:ListItem>15</asp:ListItem>
                                                    <asp:ListItem>16</asp:ListItem>
                                                    <asp:ListItem>17</asp:ListItem>
                                                    <asp:ListItem>18</asp:ListItem>
                                                    <asp:ListItem>19</asp:ListItem>
                                                    <asp:ListItem>20</asp:ListItem>
                                                    <asp:ListItem>21</asp:ListItem>
                                                    <asp:ListItem>22</asp:ListItem>
                                                    <asp:ListItem>23</asp:ListItem>
                                                    <asp:ListItem>24</asp:ListItem>
                                                    <asp:ListItem>25</asp:ListItem>
                                                    <asp:ListItem>26</asp:ListItem>
                                                    <asp:ListItem>27</asp:ListItem>
                                                    <asp:ListItem>28</asp:ListItem>
                                                    <asp:ListItem>29</asp:ListItem>
                                                    <asp:ListItem>30</asp:ListItem>
                                                    <asp:ListItem>31</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width:100%;" valign="top">
                                                <asp:RequiredFieldValidator ID="rfvddlSpecificDay" runat="server" 
                                                    ControlToValidate="ddlSpecificDay" Display="Dynamic" Enabled="False" 
                                                    ErrorMessage="Please select Specific Day" InitialValue="0" 
                                                    SetFocusOnError="True" ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="left" style="width:25%;" valign="top">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left" style="width:25%;" valign="top">
                                    &nbsp;</td>
                                <td align="left" style="width:25%;" valign="top">
                                    &nbsp;</td>
                                <td align="left" style="width:25%;" valign="top">
                                    &nbsp;</td>
                                <td align="left" style="width:25%;" valign="top">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left" style="width:25%;" valign="top">
                                    <asp:Label ID="lblStartDate" runat="server" CssClass="lbl" Text="Start Date"></asp:Label>
                                    <asp:Label ID="lblReqStartDate" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                </td>
                                <td align="left" style="width:25%;" valign="top">
                                    &nbsp;</td>
                                <td align="left" style="width:25%;" valign="top">
                                    <asp:Label ID="lblEndDate" runat="server" CssClass="lbl" Text="End Date"></asp:Label>
                                    <asp:Label ID="lblReqStartDate0" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                </td>
                                <td align="left" style="width:25%;" valign="top">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left" style="width:25%;" valign="top">
                             <%--       <telerik:RadDatePicker ID="txtStartDate" runat="server" 
                                        DateInput-DateFormat="dd-MMM-yyyy" DateInput-EmptyMessage="" 
                                        EnableTyping="false" ImagesPath="" SharedCalendarID="" AutoPostBack="True">
                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAssharedCalenderSceduleStartDateSelectors="False" ViewSelectorText="x">
                                        </Calendar>
                                        <DateInput AutoPostBack="True" DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText="" LabelWidth="40%" ReadOnly="True" type="text" value="">
                                        </DateInput>
                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                    </telerik:RadDatePicker>--%>
                                    <telerik:RadDatePicker ID="txtStartDate" runat="server" Width="180px" EnableTyping="false">
                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                        </Calendar>
                                        <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                            Font-Size="10.5px" LabelWidth="40%" type="text" value="">
                                        </DateInput>
                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                    </telerik:RadDatePicker>
                                </td>
                                <td align="left" style="width:25%;" valign="top">
                                    <%--<telerik:RadCalendar ID="sharedCalenderSceduleStartDate" runat="server" 
                                        dateinput-dateformat="dd-MMM-yyyy" enablemultiselect="False" 
                                        ViewSelectorText="x">
                                    </telerik:RadCalendar>--%>
                                </td>
                                <td align="left" style="width:25%;" valign="top">
                                 <%--   <telerik:RadDatePicker ID="txtEndDate" runat="server"
                                        DateInput-DateFormat="dd-MMM-yyyy" DateInput-EmptyMessage="" 
                                        EnableTyping="false" ImagesPath="" SharedCalendarID="sharedCalenderEndScheduleDate" AutoPostBack="True">
                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                        </Calendar>
                                        <DateInput AutoPostBack="True" DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText="" LabelWidth="40%" ReadOnly="True" type="text" value="">
                                        </DateInput>
                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                    </telerik:RadDatePicker>--%>

                                    <telerik:RadDatePicker ID="txtEndDate" runat="server" Width="180px" EnableTyping="false">
                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                        </Calendar>
                                        <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                            Font-Size="10.5px" LabelWidth="40%" type="text" value="">
                                        </DateInput>
                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                    </telerik:RadDatePicker>

                                </td>
                                <%--<td align="left" style="width:25%;" valign="top">
                                    <telerik:RadCalendar ID="sharedCalenderEndScheduleDate" runat="server" 
                                        dateinput-dateformat="dd-MMM-yyyy" enablemultiselect="False" 
                                        ViewSelectorText="x">
                                    </telerik:RadCalendar>
                                </td>--%>
                            </tr>
                            <tr>
                                <td align="left" style="width:25%;" valign="top">
                                    <asp:RequiredFieldValidator ID="rfvScheduleStartDate" runat="server" 
                                        ControlToValidate="txtStartDate" Display="Dynamic" Enabled="false" 
                                        ErrorMessage="Please select Schedule Start Date" InitialValue="" SetFocusOnError="true" 
                                        ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="width:25%;" valign="top">
                                    &nbsp;</td>
                                <td align="left" style="width:25%;" valign="top">
                                    <asp:RequiredFieldValidator ID="rfvScheduleEndDate" runat="server" 
                                        ControlToValidate="txtEndDate" Display="Dynamic" Enabled="false" 
                                        ErrorMessage="Please select Schedule End Date" InitialValue="" 
                                        SetFocusOnError="true" ValidationGroup="Preview"></asp:RequiredFieldValidator><br />
                                        <asp:CompareValidator ID="cmpSheduleDates" runat="server" Enabled="false" ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThan" ErrorMessage="Schedule End Date must be greater than Schedule From Date"
                                        SetFocusOnError="true" Display="Dynamic"
                                        ></asp:CompareValidator>
                                </td>
                                <td align="left" style="width:25%;" valign="top">
                                    &nbsp;</td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="4">
                            <asp:Button ID="btnPreview" runat="server" Text="Preview" Width="75px" CausesValidation="true"
                                CssClass="btn" ValidationGroup="Preview" />
                            <asp:Button ID="btnCancelOnlineForm" runat="server" Text="Cancel" Width="77px" 
                                CssClass="btnCancel" CausesValidation="False" />
                        </td></tr>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="width: 40%">
                            &nbsp;
                        </td>
                        <td align="left" valign="top" style="width: 10%">
                            &nbsp;
                        </td>
                        <td align="left" valign="top" style="width: 40%">
                            &nbsp;
                        </td>
                        <td align="left" valign="top" style="width: 10%">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <asp:Panel ID="pnlOnLineFormPreview" runat="server" Width="100%">
                <table style="width: 100%">
                    <tr>
                        <td align="left" valign="top" style="width: 100%">
                            <asp:Label ID="lblOnlineFormPreview" runat="server" Text="Online Instruction Preview"
                                CssClass="headingblue"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <table cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td align="left" style="width: 40%" valign="top">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr id="trPreviewBeneName" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneNamePreview" runat="server" CssClass="lbl" Text="Beneficiary Name"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvBeneName" runat="server" CssClass="lblPreview"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneAccountBranchCodePreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneAccountBranchCodePreview" runat="server" CssClass="lbl" Text="Beneficiary Account Branch Code"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvBeneAccountBranchCodePreview" runat="server" CssClass="lbl" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trAccountTitlePreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblAccountTitlePreview" runat="server" CssClass="lbl" Text="Account Title"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvAccountTitle" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trPrintLocationPreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblPreviewPrintLocatio" runat="server" CssClass="lbl" Text="Print Location"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvPrintLocation" runat="server" CssClass="lbl" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                
                                
                                <tr id="trDDDrawnLocationCityPreview" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblDDDrawnLocationCityPreview" runat="server" CssClass="lbl" 
                                                        Text="DD Drawn Location City"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblPrvDDDrawnLocationCity" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneMobNoPreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneMobNo0" runat="server" CssClass="lbl" Text="Mobile No."></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:Label ID="lblprvMobNo" runat="server" CssClass="lbl"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneIDTypePreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneIDTypePreview" runat="server" CssClass="lbl" Text="Beneficiary ID Type"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:Label ID="lblPrvBeneIDType" runat="server" CssClass="lbl"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneEmailPreview" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblEmailPreview" runat="server" CssClass="lbl" Text="Email"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:Label ID="lblprvEmail" runat="server" CssClass="lbl"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trPrintDatePreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblPRintDatePreview" runat="server" CssClass="lbl" Text="Print Date"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:Label ID="lblprvPrintDate" runat="server" CssClass="lbl"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneCityPreview" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblCityPreview" runat="server" CssClass="lbl" Text="City"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:Label ID="lblprvCity" runat="server" CssClass="lbl"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBenePickUpLocationPreview" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblPickLocationPreview" runat="server" CssClass="lbl" Text="Pick Up Location"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:Label ID="lblPrvPickLocation" runat="server" CssClass="lbl"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trUBPCompanyPreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblUBPCompanyPreview" runat="server" CssClass="lbl" Text="UBP Company"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblUBPCompanyPrv" runat="server" CssClass="lbl" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneficiaryCountryPreview" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneficiaryCountryPreview" runat="server" CssClass="lbl" Text="Beneficiary Country"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:Label ID="lblprvBeneficiaryCountry" runat="server" CssClass="lbl"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trAmountPKRPreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblAmountPKRPreview" runat="server" CssClass="lbl" Text="Amount"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblprvAmount" runat="server" CssClass="lblPreview"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <%--<tr id="trClientBankNamePreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblClientBankNamePreview" runat="server" CssClass="lbl" Text="Client Bank Name"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvClientBankName" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <tr id="trReferenceFieldPreview" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblReferenceFieldPReview" runat="server" CssClass="lbl" Text="Reference Field 1"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvReferenceField" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trReferenceField3Preview" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblReferenceField3Preview" runat="server" CssClass="lbl" Text="Reference Field 3"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvReferenceField3Preview" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                
                                
                                <%--<tr id="trBeneAddress2Preview" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneAddress2Preview" runat="server" CssClass="lbl" Text="Address 2"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvAddress2" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <tr id="trDescriptionPreview" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblDescriptionPreview" runat="server" CssClass="lbl" Text="Description"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvDescription" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneBranchAddressPreview" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneBranchAddressPreview" runat="server" CssClass="lbl" Text="Beneficiary Branch Address"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvBeneBranchAddress" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                
                                <%--<tr ID="trPreviewBankAddress" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBankAddressPreview" runat="server" CssClass="lbl" Text="Bank Code"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblPrvBankAddress" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <%--<tr ID="trClientProvincePreview" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblClientProvincePreview" runat="server" CssClass="lbl" Text="Client Province"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                &nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:Label ID="lblprvClientProvince" runat="server" CssClass="lbl"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                            </table>
                        </td>
                        <td align="left" style="width: 10%" valign="top">
                        </td>
                        <td align="left" style="width: 40%" valign="top">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr id="trBeneficiaryAccountTypePreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneficiaryAccountNoPreview" runat="server" CssClass="lbl" Text="Beneficiary Account Type"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvBeneficiaryAccountType" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBankAccountNumberPreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBankAccountNumberPreview" runat="server" CssClass="lbl" Text="Beneficiary Bank Account Number"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvBankAccountNo" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneAccountCurrencyPreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneAccountCurrencyPreview" runat="server" CssClass="lbl" Text="Beneficiary Account Currency"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvBeneAccountCurrency" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBankNamePreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBankNamePreview" runat="server" CssClass="lbl" Text="Beneficiary Bank Name"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvBankName" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trDDDrawnLocationProvincePreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblDDDrawnLocationProvincePreview" runat="server" CssClass="lbl" 
                                                        Text="DD Drawn Location Province"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblPrvDDDrawnLocationProvince" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <%-- <tr id="trBankIMDPreview" runat="server">
                                    <td align="left" style="width: 100%" valign="top">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBankIMD0" runat="server" CssClass="lbl" 
                                                        Text="Beneficiary Bank IMD"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvIMDNo" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <tr id="trInstrumentNoPreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblInstrumentNoPreview" runat="server" CssClass="lbl" Text="Instrument No"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:Label ID="lblprvInstrumentNoPreview" runat="server" CssClass="lbl"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBenePhoneNoPreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBenePhoneNoPreview" runat="server" CssClass="lbl" Text="Phone No."></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:Label ID="lblprvPhoneNo" runat="server" CssClass="lbl"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trValueDatePreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblValueDatePreview" runat="server" CssClass="lbl" Text="Value Date"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:Label ID="lblprvValueDate" runat="server" CssClass="lbl"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneCNICNoPreview" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneCNICPreview" runat="server" CssClass="lbl" Text="NIC No."></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:Label ID="lblprvNICNo" runat="server" CssClass="lbl"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneIDNOPreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneIDNoPrview" runat="server" CssClass="lbl" Text="Beneficiary ID No"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:Label ID="lblPrvBeneIDNo" runat="server" CssClass="lbl"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneficiaryProvincePreview" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneficiaryProvincePreview" runat="server" CssClass="lbl" Text="Beneficiary Province"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:Label ID="lblprvBeneficiaryProvince" runat="server" CssClass="lbl"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                
                                <tr id="trBeneBankBranchNamePreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneBankBranchNamePreview" runat="server" CssClass="lbl" Text="Beneficiary Bank Branch Name"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvBeneBankBranchName" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trPaymentModePreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblPaymentModePreview" runat="server" CssClass="lbl" Text="Payment Mode"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvPaymentMode" runat="server" CssClass="lbl" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <%--<tr id="trUBPCompanyPreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblUBPCompanyPreview" runat="server" CssClass="lbl" Text="UBP Company"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblUBPCompanyPrv" runat="server" CssClass="lbl" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <tr id="trUBPReferenceFieldPreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblUBPReferenceFieldPreview" runat="server" CssClass="lbl" Text="UBP Reference"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblUBPReferencePrv" runat="server" CssClass="lbl" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trAmountInWordPreview" runat="server">
                                    <td style="width: 100%">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblAmountInWordPreview" runat="server" CssClass="lbl" Text="Amount In Words"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblprvAmountInWord" runat="server" CssClass="lblPreview"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trReferenceField2Preview" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblReferenceField2Preview" runat="server" CssClass="lbl" Text="Reference Field 2"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvReferenceField2" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneAddressPreview" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneAddressPreview1" runat="server" CssClass="lbl" Text="Address 1"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvAddress1" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trRemarksPreview" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblRemarksPreview" runat="server" CssClass="lbl" Text="Remarks"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:Label ID="lblprvRemarks" runat="server" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                
                            </table>
                        </td>
                        <td align="left" style="width: 10%" valign="top">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" valign="top">
                            <table id="tblSchedulePreview" runat="server" style="display:none;width:100%">
                            <tr>
                            <td align="left" valign="top" style="width:40%">
                                <asp:Label ID="lblOnlineFormSchedulePreview" runat="server" 
                                    CssClass="headingblue" Text="Online Instruction Schedule Preview"></asp:Label>
                                </td>
                            <td align="left" valign="top" style="width:10%"></td>
                            <td align="left" valign="top" style="width:40%"></td>
                            <td align="left" valign="top" style="width:10%"></td>
                            
                            </tr>
                                <tr>
                                    <td align="left" style="width:40%" valign="top">
                                        &nbsp;</td>
                                    <td align="left" style="width:10%" valign="top">
                                        &nbsp;</td>
                                    <td align="left" style="width:40%" valign="top">
                                        &nbsp;</td>
                                    <td align="left" style="width:10%" valign="top">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" style="width:40%" valign="top">
                                        <asp:Label ID="lblScheduleTitlePreview" runat="server" CssClass="lbl" 
                                            Text="Schedule Title"></asp:Label>
                                    </td>
                                    <td align="left" style="width:10%" valign="top">
                                        &nbsp;</td>
                                    <td align="left" style="width:40%" valign="top">
                                        <asp:Label ID="lblScheduleFrequencyPreview" runat="server" CssClass="lbl" 
                                            Text="Schedule Frequency"></asp:Label>
                                    </td>
                                    <td align="left" style="width:10%" valign="top">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" style="width:40%" valign="top">
                                        <asp:Label ID="lblprvScheduleTitle" runat="server" CssClass="lbl"></asp:Label>
                                    </td>
                                    <td align="left" style="width:10%" valign="top">
                                        &nbsp;</td>
                                    <td align="left" style="width:40%" valign="top">
                                        <asp:Label ID="lblprvScheduleFrequency" runat="server" CssClass="lbl"></asp:Label>
                                    </td>
                                    <td align="left" style="width:10%" valign="top">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" style="width:40%" valign="top">
                                        &nbsp;</td>
                                    <td align="left" style="width:10%" valign="top">
                                        &nbsp;</td>
                                    <td align="left" style="width:40%" valign="top">
                                        &nbsp;</td>
                                    <td align="left" style="width:10%" valign="top">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" style="width:40%" valign="top">
                                        <asp:Label ID="lblScheduleTransactionTimePreview" runat="server" CssClass="lbl" 
                                            Text="Transaction Time"></asp:Label>
                                    </td>
                                    <td align="left" style="width:10%" valign="top">
                                        &nbsp;</td>
                                    <td align="left" style="width:40%" valign="top">
                                        <asp:Label ID="lblDayTypePreview" runat="server" CssClass="lbl" Text="Day Type"></asp:Label>
                                      </td>
                                    <td align="left" style="width:10%" valign="top">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" style="width:40%" valign="top">
                                        <asp:Label ID="lblprvTransactionTime" runat="server" CssClass="lbl"></asp:Label>
                                    </td>
                                    <td align="left" style="width:10%" valign="top">
                                        &nbsp;</td>
                                    <td align="left" style="width:40%" valign="top">
                                        <asp:Label ID="lblprvDayType" runat="server" CssClass="lbl"></asp:Label>
                                    </td>
                                    <td align="left" style="width:10%" valign="top">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" style="width:40%" valign="top">
                                        &nbsp;</td>
                                    <td align="left" style="width:10%" valign="top">
                                        &nbsp;</td>
                                    <td align="left" style="width:40%" valign="top">
                                        &nbsp;</td>
                                    <td align="left" style="width:10%" valign="top">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" style="width:40%" valign="top">
                                        <asp:Label ID="lblScheduleStartDatePreview" runat="server" CssClass="lbl" 
                                            Text="Schedule Start Date"></asp:Label>
                                    </td>
                                    <td align="left" style="width:10%" valign="top">
                                        &nbsp;</td>
                                    <td align="left" style="width:40%" valign="top">
                                        <asp:Label ID="lblScheduleEndDatePreview" runat="server" CssClass="lbl" 
                                            Text="Schedule End Date"></asp:Label>
                                    </td>
                                    <td align="left" style="width:10%" valign="top">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="left" style="width:40%" valign="top">
                                        <asp:Label ID="lblprvScheduleStartDate" runat="server" CssClass="lbl"></asp:Label>
                                    </td>
                                    <td align="left" style="width:10%" valign="top">
                                        &nbsp;</td>
                                    <td align="left" style="width:40%" valign="top">
                                        <asp:Label ID="lblprvScheduleEndDate" runat="server" CssClass="lbl"></asp:Label>
                                    </td>
                                    <td align="left" style="width:10%" valign="top">
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" valign="top">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn" Text="Save"
                                Width="75px" ValidationGroup="Save"/>
                            <asp:Button ID="btnCancelPrview" runat="server" CssClass="btnCancel" Text="Cancel"
                                Width="75px" CausesValidation="False" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 40%" valign="top">
                            &nbsp;
                        </td>
                        <td align="left" style="width: 10%" valign="top">
                            &nbsp;
                        </td>
                        <td align="left" style="width: 40%" valign="top">
                            &nbsp;
                        </td>
                        <td align="left" style="width: 10%" valign="top">
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <br />
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%">
            &nbsp;
        </td>
    </tr>
</table>
