﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System
Imports System.IO
Imports System.Data.OleDb
Imports System.Data

Partial Class DesktopModules_BankRoleAccountPaymentNature_BankRoleAccountPaymentNatureTagging
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
    Private ArrICAssignUserRolsID As New ArrayList
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            'txtValueDate.Clear()


            If Page.IsPostBack = False Then



                btnPreview.Attributes.Item("onclick") = "if (Page_ClientValidate('Preview')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnPreview, Nothing).ToString() & " } "
                btnSave.Attributes.Item("onclick") = "if (Page_ClientValidate('Save')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnSave, Nothing).ToString() & " } "

                ''Schedule settings
                tblScheduleTxn.Style.Add("display", "none")
                tblDayType.Style.Add("display", "none")
                tblNoOfDays.Style.Add("display", "none")
                tblSpecificDay.Style.Add("display", "none")
                tblSchedulePreview.Style.Add("display", "none")

                ViewState("htRights") = Nothing

                rbScheduleTransaction.Visible = False
                GetPageAccessRights()
                Dim objICUser As New ICUser
                Dim objICGroup As New ICGroup
                Dim dt As New DataTable

                'sharedCalendarValueDate.SelectedDate = Date.Now
                'sharedCalendarValueDate.RangeMinDate = Date.Now
                'sharedCalenderPrintDate.SelectedDate = Date.Now
                'sharedCalenderPrintDate.RangeMinDate = Date.Now

                txtValueDate.SelectedDate = Date.Now
                txtValueDate.MinDate = Date.Now
                txtPrintDate.SelectedDate = Date.Now
                txtPrintDate.MinDate = Date.Now

                'sharedCalenderSceduleStartDate.RangeMinDate = Date.Now.AddDays(1)
                'sharedCalenderEndScheduleDate.RangeMinDate = Date.Now.AddDays(1)
                'sharedCalenderEndScheduleDate.SelectedDate = Date.Now.AddDays(1)
                'sharedCalenderSceduleStartDate.SelectedDate = Date.Now.AddDays(1)

                txtStartDate.MinDate = Date.Now.AddDays(1)
                txtEndDate.MinDate = Date.Now.AddDays(1)
                txtEndDate.SelectedDate = Date.Now.AddDays(1)
                txtStartDate.SelectedDate = Date.Now.AddDays(1)

                If Me.UserInfo.IsSuperUser = False Then
                    If objICUser.LoadByPrimaryKey(Me.UserId) Then
                        If objICUser.UserType = "Client User" Then
                            ClearOnlineForm()
                            ClearPreview()
                            HideRequiredMarkSign()
                            HideAllOnlineFormRows()
                            HideAllOnlineFormPreviewRows()
                            pnlOnlineFrom.Style.Add("display", "none")
                            pnlOnLineFormPreview.Style.Add("display", "none")


                            objICGroup = objICUser.UpToICOfficeByOfficeCode.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode
                            LoadddlGroup()
                            ddlGroup.SelectedValue = objICGroup.GroupCode.ToString
                            ddlGroup.Enabled = False
                            SetArraListRoleID()
                            LoadddlCompany()
                            ddlCompany_SelectedIndexChanged(ddlCompany.SelectedValue.ToString(), Nothing)
                            'ddlCompany.SelectedValue = objICUser.UpToICOfficeByOfficeCode.UpToICCompanyByCompanyCode.CompanyCode
                            'ddlCompany.Enabled = False

                            LoadddlAccountPaymentNatureByUserID()
                            LoadddlProductTypeByAccountPaymentNature()
                            LoadBeneGroup("0", "0")
                            LoadddlBeneficiary(False, "0")
                            'LoadddlClientOfficeByUserID()
                            LoadddlCountry()
                            LoadddlProvince()
                            LoadddlCity()
                            LoadddlBeneBank()
                            LoadddlUBPCompany()
                        ElseIf objICUser.UserType.ToString = "Bank User" Then
                            ClearOnlineForm()
                            ClearPreview()
                            HideRequiredMarkSign()
                            HideAllOnlineFormRows()
                            HideAllOnlineFormPreviewRows()
                            pnlOnlineFrom.Style.Add("display", "none")
                            pnlOnLineFormPreview.Style.Add("display", "none")
                            SetArraListRoleID()
                            LoadddlGroup()
                            LoadddlCompany()
                            ddlCompany_SelectedIndexChanged(ddlCompany.SelectedValue.ToString(), Nothing)
                            'LoadddlCity()
                            SetArraListRoleID()
                            LoadddlAccountPaymentNatureByUserID()
                            LoadddlProductTypeByAccountPaymentNature()
                            'LoadddlClientOfficeByUserID()
                            LoadBeneGroup("0", "0")
                            LoadddlBeneficiary(False, "0")
                            LoadddlCountry()
                            LoadddlProvince()
                            LoadddlCity()
                            LoadddlBeneBank()
                            LoadddlUBPCompany()

                        End If
                    End If
                Else
                    UIUtilities.ShowDialog(Me, "Error", "Sorry! You are not authorized to login.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Single Instruction Form") = True Then
                    If Not ArrICAssignUserRolsID.Contains(RoleID.ToString) Then
                        ArrICAssignUserRolsID.Add(RoleID.ToString)
                    End If
                End If
            Next
        End If
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Single Instruction Form")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            objICUser.es.Connection.CommandTimeout = 3600
            ddlGroup.Enabled = True
            Dim lit As New ListItem
            lit.Text = Nothing
            lit.Value = Nothing
            If objICUser.LoadByPrimaryKey(Me.UserId) Then
                If objICUser.UserType = "Client User" Then

                    lit.Value = "0"
                    lit.Text = "-- Please Select --"
                    ddlGroup.Items.Clear()
                    ddlGroup.Items.Add(lit)
                    ddlGroup.AppendDataBoundItems = True
                    ddlGroup.DataSource = ICGroupController.GetAllActiveAndApproveGroups
                    ddlGroup.DataTextField = "GroupName"
                    ddlGroup.DataValueField = "GroupCode"
                    ddlGroup.DataBind()
                ElseIf objICUser.UserType = "Bank User" Then

                    ddlGroup.Items.Clear()
                    dt = ICUserRolesPaymentNatureController.GetAllTaggedGroupsByUserID(objICUser.UserID.ToString, ArrICAssignUserRolsID)
                    If dt.Rows.Count = 1 Then
                        For Each dr As DataRow In dt.Rows
                            lit.Value = dr("GroupCode")
                            lit.Text = dr("GroupName")
                            ddlGroup.Items.Add(lit)
                            lit.Selected = True
                            ddlGroup.Enabled = False
                        Next
                    ElseIf dt.Rows.Count > 1 Then
                        lit.Value = "0"
                        lit.Text = "-- Please Select --"
                        ddlGroup.Items.Clear()
                        ddlGroup.Items.Add(lit)
                        ddlGroup.AppendDataBoundItems = True
                        ddlGroup.DataSource = dt
                        ddlGroup.DataTextField = "GroupName"
                        ddlGroup.DataValueField = "GroupCode"
                        ddlGroup.DataBind()
                    Else

                        lit.Value = "0"
                        lit.Text = "-- Please Select --"
                        ddlGroup.Items.Clear()
                        ddlGroup.Items.Add(lit)
                        ddlGroup.AppendDataBoundItems = True

                    End If
                Else

                    lit.Value = "0"
                    lit.Text = "-- Please Select --"
                    ddlGroup.Items.Clear()
                    ddlGroup.Items.Add(lit)
                    ddlGroup.AppendDataBoundItems = True
                End If
            End If




        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, ddlGroup.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlCompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlCompany.Items.Clear()
                    ddlCompany.Items.Add(lit)
                    lit.Selected = True
                    ddlCompany.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- Please Select --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
                ddlCompany.DataSource = dt
                ddlCompany.DataTextField = "CompanyName"
                ddlCompany.DataValueField = "CompanyCode"
                ddlCompany.DataBind()
                ddlCompany.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- Please Select --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    'Private Sub LoadddlClientOfficeByUserID()
    '    Try
    '        Dim lit As New ListItem
    '        Dim dt As New DataTable
    '        Dim objICAPNature As New ICAccountsPaymentNature

    '        objICAPNature.es.Connection.CommandTimeout = 3600
    '        ddlClientOffice.Enabled = True


    '        If ddlAccPNature.SelectedValue <> "0" Then
    '            objICAPNature.LoadByPrimaryKey(ddlAccPNature.SelectedValue.Split("-")(0).ToString, ddlAccPNature.SelectedValue.Split("-")(1).ToString, ddlAccPNature.SelectedValue.Split("-")(2).ToString, ddlAccPNature.SelectedValue.Split("-")(3).ToString)
    '            dt = ICUserRolesPaymentNatureController.GetAllTaggedLocationsWithUserByAPNatureUSerIDAndCompanyCode(objICAPNature, Me.UserId.ToString)
    '            If dt.Rows.Count = 1 Then
    '                For Each dr As DataRow In dt.Rows
    '                    ddlClientOffice.Items.Clear()
    '                    lit.Value = dr("OfficeID")
    '                    lit.Text = dr("OfficeName")
    '                    ddlClientOffice.Items.Add(lit)
    '                    lit.Selected = True
    '                    ddlClientOffice.Enabled = False
    '                Next
    '            ElseIf dt.Rows.Count > 1 Then
    '                ddlClientOffice.Items.Clear()
    '                lit.Value = "0"
    '                lit.Text = "-- Please Select --"
    '                ddlClientOffice.Items.Add(lit)
    '                lit.Selected = True
    '                ddlClientOffice.AppendDataBoundItems = True
    '                ddlClientOffice.DataTextField = "OfficeName"
    '                ddlClientOffice.DataValueField = "OfficeID"
    '                ddlClientOffice.DataBind()
    '            Else
    '                ddlClientOffice.Items.Clear()
    '                lit.Value = "0"
    '                lit.Text = "-- Please Select --"
    '                ddlClientOffice.Items.Add(lit)
    '                lit.Selected = True
    '            End If
    '        Else
    '            ddlClientOffice.Items.Clear()
    '            lit.Value = "0"
    '            lit.Text = "-- Please Select --"
    '            ddlClientOffice.Items.Add(lit)
    '            lit.Selected = True
    '        End If
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    Private Sub LoadddlCity()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBeneficiaryCity.Items.Clear()
            ddlBeneficiaryCity.Items.Add(lit)
            ddlBeneficiaryCity.AppendDataBoundItems = True
            ddlBeneficiaryCity.DataSource = ICCityController.GetAllCityActiveAndApproved
            ddlBeneficiaryCity.DataTextField = "CityName"
            ddlBeneficiaryCity.DataValueField = "CityCode"
            ddlBeneficiaryCity.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlProvince()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBeneficiaryProvince.Items.Clear()
            ddlBeneficiaryProvince.Items.Add(lit)
            ddlBeneficiaryProvince.AppendDataBoundItems = True
            ddlBeneficiaryProvince.DataSource = ICProvinceController.GetAllActiveAndApproveProvinces
            ddlBeneficiaryProvince.DataTextField = "ProvinceName"
            ddlBeneficiaryProvince.DataValueField = "ProvinceCode"
            ddlBeneficiaryProvince.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlDDDrawnLocationProvince()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlDDPayableLocationProvince.Items.Clear()
            ddlDDPayableLocationProvince.Items.Add(lit)
            ddlDDPayableLocationProvince.AppendDataBoundItems = True
            ddlDDPayableLocationProvince.DataSource = ICProvinceController.GetPakistanProvinceActiveAndApprove
            ddlDDPayableLocationProvince.DataTextField = "ProvinceName"
            ddlDDPayableLocationProvince.DataValueField = "ProvinceCode"
            ddlDDPayableLocationProvince.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlDDDrawnLocationCity()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlDDPayableLocationCity.Items.Clear()
            ddlDDPayableLocationCity.Items.Add(lit)
            ddlDDPayableLocationCity.AppendDataBoundItems = True
            ddlDDPayableLocationCity.DataSource = ICCityController.GetCityActiveAndApprovedByProvinceCode(ddlDDPayableLocationProvince.SelectedValue.ToString)
            ddlDDPayableLocationCity.DataTextField = "CityName"
            ddlDDPayableLocationCity.DataValueField = "CityCode"
            ddlDDPayableLocationCity.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlCountry()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBeneficiaryCountry.Items.Clear()
            ddlBeneficiaryCountry.Items.Add(lit)
            ddlBeneficiaryCountry.AppendDataBoundItems = True
            ddlBeneficiaryCountry.DataSource = ICCountryController.GetCountryActiveAndApproved
            ddlBeneficiaryCountry.DataTextField = "CountryName"
            ddlBeneficiaryCountry.DataValueField = "CountryCode"
            ddlBeneficiaryCountry.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    
    Private Sub LoadddlUBPCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlUBPCompany.Items.Clear()
            ddlUBPCompany.Items.Add(lit)
            ddlUBPCompany.AppendDataBoundItems = True
            ddlUBPCompany.DataSource = ICUBPSCompanyController.GetAllUBPActiveApproveCompany
            ddlUBPCompany.DataTextField = "CompanyName"
            ddlUBPCompany.DataValueField = "UBPSCompanyID"
            ddlUBPCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlBenePickLocation()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlBenePickLocation.Items.Clear()
            ddlBenePickLocation.Items.Add(lit)
            ddlBenePickLocation.AppendDataBoundItems = True
            ddlBenePickLocation.DataSource = ICOfficeController.GetPrincipalBankBranchActiveAndApprove
            ddlBenePickLocation.DataTextField = "OfficeName"
            ddlBenePickLocation.DataValueField = "OfficeID"
            ddlBenePickLocation.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlAccountPaymentNatureByUserID()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlAccPNature.Items.Clear()
            ddlAccPNature.Items.Add(lit)
            lit.Selected = True
            ddlAccPNature.AppendDataBoundItems = True
            ddlAccPNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUser(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlAccPNature.DataTextField = "AccountAndPaymentNature"
            ddlAccPNature.DataValueField = "AccountPaymentNature"
            ddlAccPNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlProductTypeByAccountPaymentNature()
        Try
            Dim lit As New ListItem
            Dim objICAPNature As New ICAccountsPaymentNature
            objICAPNature.es.Connection.CommandTimeout = 3600
            ddlProductType.Items.Clear()
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlProductType.Items.Add(lit)
            ddlProductType.AppendDataBoundItems = True
            If ddlAccPNature.SelectedValue.ToString <> "0" Then
                If objICAPNature.LoadByPrimaryKey(ddlAccPNature.SelectedValue.ToString.Split("-")(0), ddlAccPNature.SelectedValue.ToString.Split("-")(1), ddlAccPNature.SelectedValue.ToString.Split("-")(2), ddlAccPNature.SelectedValue.ToString.Split("-")(3)) Then
                    ddlProductType.DataSource = ICAccountPaymentNatureProductTypeController.GetAllProductTypesByAccountAndPaymentNature(objICAPNature)
                    ddlProductType.DataTextField = "ProductTypeName"
                    ddlProductType.DataValueField = "ProductTypeCode"
                    ddlProductType.DataBind()
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlPrintLocations()
        Try
            Dim lit As New ListItem
            Dim objICAPNature As New ICAccountsPaymentNature

            objICAPNature.es.Connection.CommandTimeout = 3600
            ddlPrintLocation.Items.Clear()
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlPrintLocation.Items.Add(lit)
            ddlPrintLocation.AppendDataBoundItems = True
            If ddlAccPNature.SelectedValue.ToString <> "0" And ddlProductType.SelectedValue.ToString <> "0" Then
                If objICAPNature.LoadByPrimaryKey(ddlAccPNature.SelectedValue.ToString.Split("-")(0), ddlAccPNature.SelectedValue.ToString.Split("-")(1), ddlAccPNature.SelectedValue.ToString.Split("-")(2), ddlAccPNature.SelectedValue.ToString.Split("-")(3)) Then
                    SetArraListRoleID()
                    ddlPrintLocation.DataSource = ICInstructionController.GetAllTaggedPrintLocationByAPNatureAndProductType(objICAPNature, ddlProductType.SelectedValue.ToString, ArrICAssignUserRolsID)
                    ddlPrintLocation.DataTextField = "OfficeName"
                    ddlPrintLocation.DataValueField = "OfficeID"
                    ddlPrintLocation.DataBind()
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub HideAllOnlineFormRows()
        DirectCast(Me.Control.FindControl("trBeneName"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trPrintLocation"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trAccountTitle"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trBeneBankCode"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trClientBankName"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneMobNo"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneEmail"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneCity"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneCountry"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trBeneAddress2"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trAmountInWords"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trRemarks"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField1"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField3"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBankAccountNumber"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneAccountBranchCode"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneAccountCurrency"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trBankBranchCode"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trBankIMD"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trBranchName"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneBankName"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trClientBankName"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trValueDate"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBenePhoneNo"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneCNICNo"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneProvince"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneAddress1"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trAmountPKR"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trDescription"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField2"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trDDPayableLocationProvince"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trDDPayableLocationCity"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trInstrumentNo"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trPrintDate"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneBranchAddress"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneIDNo"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneficiaryIdentityType"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBenePickUpLocation"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trUBPCompany"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trUBPReferenceField"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneficiaryAccountType"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trGLAccountBranchCode"), HtmlTableRow).Style.Add("display", "none")

    End Sub
    Private Sub HideAllOnlineFormPreviewRows()

        'DirectCast(Me.Control.FindControl("trBeneBankAddressPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trPreviewBeneName"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trPrintLocationPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trAccountTitlePreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBankNamePreview"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trBankCodePreview"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trClientBankNamePreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneMobNoPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneCNICNoPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneCityPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneficiaryCountryPreview"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trBeneAddress2Preview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trDescriptionPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBankAccountNumberPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneAccountCurrencyPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneAccountBranchCodePreview"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trBeneBankBranchCodePreview"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trBankIMDPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneBankBranchNamePreview"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trClientBankBranchNamePreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBenePhoneNoPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneEmailPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneficiaryProvincePreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneAddressPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trAmountPKRPreview"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trAmountInWordPreview"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trPaymentModePreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trAmountPKRPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trDescriptionPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField2Preview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceFieldPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField3Preview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trRemarksPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trDDDrawnLocationProvincePreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trDDDrawnLocationCityPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trValueDatePreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trInstrumentNoPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trPrintDatePreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneBranchAddressPreview"), HtmlTableRow).Style.Add("display", "none")

        DirectCast(Me.Control.FindControl("trBeneIDNOPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneIDTypePreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBenePickUpLocationPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trUBPCompanyPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trUBPReferenceFieldPreview"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneficiaryAccountTypePreview"), HtmlTableRow).Style.Add("display", "none")
    End Sub
    Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged
        Try
            'txtValueDate.Clear()
            'LoadddlClientOfficeByUserID()
            'sharedCalendarValueDate.SelectedDate = Date.Now
            'sharedCalendarValueDate.RangeMinDate = Date.Now
            'txtValueDate.MinDate = Date.Now
            'txtValueDate.SelectedDate = Date.Now

            tblScheduleTxn.Style.Add("display", "none")
            tblDayType.Style.Add("display", "none")
            tblNoOfDays.Style.Add("display", "none")
            tblSpecificDay.Style.Add("display", "none")

            radScheduleTitle.Enabled = False
            radNoOfDay.Enabled = False
            rfvddlSpecificDay.Enabled = False
            rfvddlScheduleFrequency.Enabled = False
            rbScheduleTransaction.Visible = False
            If ddlProductType.SelectedValue.ToString <> "0" Then
                ClearOnlineForm()
                ClearPreview()
                HideRequiredMarkSign()
                HideAllOnlineFormRows()
                HideAllOnlineFormPreviewRows()
                LoadddlPrintLocations()
                Dim objProductType As New ICProductType
                objProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString())
                LoadddlBeneBank(objProductType.DisbursementMode)
                LoadddlBenePickLocation()
                SetViewFieldsVisibility()
                pnlOnlineFrom.Style.Remove("display")
                pnlOnLineFormPreview.Style.Add("display", "none")
                If CBool(htRights("Allow Schedule")) = True Then
                    Dim objICProductType As New ICProductType
                    objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)
                    If objICProductType.DisbursementMode.ToString <> "Cheque" And objICProductType.DisbursementMode.ToString <> "DD" And objICProductType.DisbursementMode.ToString <> "COTC" Then
                        rbScheduleTransaction.Checked = False

                        rbScheduleTransaction.Visible = True
                    Else
                        rbScheduleTransaction.Visible = False
                        rbScheduleTransaction.Checked = False
                    End If


                End If
                If ddlBeneficiary.SelectedValue = "-1" Or ddlBeneficiary.SelectedValue = "0" Then
                    ' SetViewFieldsVisibilityAndData(False, "0", "0", "0")
                    SetViewFieldsVisibilityAndData(False, "0")
                Else
                    ' SetViewFieldsVisibilityAndData(True, ddlBeneficiary.SelectedValue.ToString(), "0", "0")
                    SetViewFieldsVisibilityAndData(True, ddlBeneficiary.SelectedValue.ToString())
                End If
                pnlOnlineFrom.Style.Remove("display")
                pnlOnLineFormPreview.Style.Add("display", "none")

                Dim objCompany As New ICCompany
                objCompany.LoadByPrimaryKey(ddlCompany.SelectedValue.ToString())
                LoadBeneGroup(ddlAccPNature.SelectedValue.Split("-")(3).ToString(), ddlCompany.SelectedValue.ToString())
                If ddlBeneficiaryGroup.Visible = True Then
                    LoadddlBeneficiary(objCompany.IsAllowOpenPayment, ddlBeneficiaryGroup.SelectedValue.ToString())
                Else
                    LoadddlBeneficiary(objCompany.IsAllowOpenPayment, 0)
                End If
            ElseIf ddlProductType.SelectedValue.ToString = "0" Then
                ClearOnlineForm()
                ClearPreview()
                HideRequiredMarkSign()
                HideAllOnlineFormRows()
                HideAllOnlineFormPreviewRows()
                'SetViewFieldsVisibility()
                SetViewFieldsVisibilityAndData(False, "0")
                pnlOnlineFrom.Style.Add("display", "none")
                pnlOnLineFormPreview.Style.Add("display", "none")
                rbScheduleTransaction.Visible = False
                rbScheduleTransaction.Checked = False
                ddlBeneficiaryGroup.SelectedValue = 0
                ddlBeneficiary.SelectedValue = 0
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetViewFieldsVisibilityAndData(ByVal IsFieldsDisabled As Boolean, ByVal BeneID As String)
        Dim objOnlineFormSettingsColl As New ICOnlineFormSettingsCollection
        Dim objICAPNature As New ICAccountsPaymentNature
        Dim objICProductType As New ICProductType
        Dim objBene As ICBene
        Dim dt As New DataTable
        Dim dr As DataRow
        dt.Columns.Add("BeneName")
        dt.Columns.Add("BankAccountNumber")
        dt.Columns.Add("BeneficiaryBranchAddress")
        dt.Columns.Add("BeneAccountBranchCode")
        dt.Columns.Add("BeneAccountCurrency")
        dt.Columns.Add("AccountTitle")
        dt.Columns.Add("BeneBankName")
        dt.Columns.Add("BeneMobileNo")
        dt.Columns.Add("BenePhoneNo")
        dt.Columns.Add("BeneEmail")
        dt.Columns.Add("BeneIDNo")
        dt.Columns.Add("BeneIDType")
        dt.Columns.Add("BeneficiaryCity")
        dt.Columns.Add("BeneficiaryProvince")
        dt.Columns.Add("BeneficiaryCountry")
        dt.Columns.Add("BeneAddress1")
        dt.Columns.Add("UBPSCompanyCode")
        dt.Columns.Add("UBPSRefNo")
        dt.Columns.Add("BeneBranchCode")
        rfvddlPrintLocation.Enabled = False
        rfvddlBeneficiaryCity.Enabled = False
        rfvddlBeneficiaryProvince.Enabled = False
        rfvddlBeneficiaryCountry.Enabled = False
        rfvddlBeneBank.Enabled = False
        rfvddlDDPayableLocationProvince.Enabled = False
        rfvddlDDPayableLocationCity.Enabled = False

        txtBankAccountNumber.AutoPostBack = False
        txtAccountTitle.ReadOnly = False
        txtBeneAccountCurrency.ReadOnly = False
        txtBeneAccountBranchCode.ReadOnly = False
        ddlBeneBankName.AutoPostBack = False
        ddlDDPayableLocationCity.AutoPostBack = False

        objBene = New ICBene
        If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then

            objBene.LoadByPrimaryKey(ddlBeneficiary.SelectedValue.ToString())

        End If

        If ddlAccPNature.SelectedValue.ToString <> "0" And ddlCompany.SelectedValue.ToString <> "0" And ddlProductType.SelectedValue.ToString <> "0" Then
            objICAPNature.LoadByPrimaryKey(ddlAccPNature.SelectedValue.ToString.Split("-")(0), ddlAccPNature.SelectedValue.ToString.Split("-")(1), ddlAccPNature.SelectedValue.ToString.Split("-")(2), ddlAccPNature.SelectedValue.ToString.Split("-")(3))
            objOnlineFormSettingsColl = ICInstructionController.GetAllOnlineFormSettingFieldsTaggedWithAPNautueAndProductType(objICAPNature, ddlCompany.SelectedValue.ToString, ddlProductType.SelectedValue.ToString)
            objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)
            If objOnlineFormSettingsColl.Count > 0 Then
                DirectCast(Me.Control.FindControl("trPaymentModePreview"), HtmlTableRow).Style.Remove("display")
                DirectCast(Me.Control.FindControl("trAmountInWords"), HtmlTableRow).Style.Remove("display")

                radAmountInWords.Enabled = True
                'sharedCalendarValueDate.SelectedDate = Date.Now
                'sharedCalendarValueDate.RangeMinDate = Date.Now
                'txtValueDate.MinDate = Date.Now
                'txtValueDate.SelectedDate = Date.Now
                dr = dt.NewRow()
                For Each objICOnlineFormSetting As ICOnlineFormSettings In objOnlineFormSettingsColl

                    If objICOnlineFormSetting.FieldID.ToString = "19" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneName"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trPreviewBeneName"), HtmlTableRow).Style.Remove("display")
                            radBeneName.Enabled = True
                            radBeneName.GetSettingByBehaviorID("reBeneName").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            lblReqBeneName.Visible = objICOnlineFormSetting.IsRequired
                            If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then
                                txtBeneName.Text = objBene.BeneficiaryName
                                ViewState("BeneName") = ""
                                ViewState("BeneName") = objBene.BeneficiaryName
                                If IsFieldsDisabled = True Then
                                    txtBeneName.Enabled = False
                                Else
                                    txtBeneName.Text = Nothing  '' For New Beneficiary
                                    ViewState("BeneName") = ""
                                    txtBeneName.Enabled = True
                                End If
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "5" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBankAccountNumber"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBankAccountNumberPreview"), HtmlTableRow).Style.Remove("display")
                            radBankAccountNumber.Enabled = True
                            radBankAccountNumber.GetSettingByBehaviorID("reBankAccountNumber").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            lblReqBankAccountNumber.Visible = objICOnlineFormSetting.IsRequired

                            If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then
                                If objICProductType.DisbursementMode = "Direct Credit" Then
                                    txtBankAccountNumber.Text = objBene.BeneficiaryAccountNo
                                    ViewState("BankAccountNumber") = ""
                                    ViewState("BankAccountNumber") = objBene.BeneficiaryAccountNo
                                ElseIf objICProductType.DisbursementMode = "Other Credit" Then
                                    txtBankAccountNumber.Text = objBene.BeneficiaryIBFTAccountNo
                                    ViewState("BankAccountNumber") = ""
                                    ViewState("BankAccountNumber") = objBene.BeneficiaryIBFTAccountNo
                                End If
                               
                                If txtBankAccountNumber.Text.Trim <> "" Then
                                    If IsFieldsDisabled = True Then
                                        txtBankAccountNumber.ReadOnly = True
                                    Else
                                        txtBankAccountNumber.Text = Nothing  '' For New Beneficiary
                                        ViewState("BankAccountNumber") = ""
                                        txtBankAccountNumber.ReadOnly = False
                                    End If
                                Else
                                    txtBankAccountNumber.Text = Nothing  '' For New Beneficiary
                                    ViewState("BankAccountNumber") = ""
                                    txtBankAccountNumber.ReadOnly = False
                                End If
                            End If


                            If objICProductType.DisbursementMode = "Direct Credit" Then
                                If txtBankAccountNumber.Text.Trim = "" Or txtBankAccountNumber.Text Is Nothing Then
                                    txtBankAccountNumber.AutoPostBack = True

                                    ''Beneficiary Account Title

                                    DirectCast(Me.Control.FindControl("trAccountTitle"), HtmlTableRow).Style.Remove("display")
                                    DirectCast(Me.Control.FindControl("trAccountTitlePreview"), HtmlTableRow).Style.Remove("display")
                                    radAccountTitle.Enabled = True
                                    radAccountTitle.GetSettingByBehaviorID("reAccountTitle").Validation.IsRequired = True
                                    lblReqAccountTitle.Visible = False


                                    ''Beneficiary Branch Code

                                    DirectCast(Me.Control.FindControl("trBeneAccountBranchCode"), HtmlTableRow).Style.Remove("display")
                                    DirectCast(Me.Control.FindControl("trBeneAccountBranchCodePreview"), HtmlTableRow).Style.Remove("display")
                                    radBeneAccountBranchCode.Enabled = True
                                    radBeneAccountBranchCode.GetSettingByBehaviorID("reBeneAccountBranchCode").Validation.IsRequired = True
                                    lblReqBeneAccountBranchCode.Visible = False

                                    ''Beneficiary Account Currency

                                    DirectCast(Me.Control.FindControl("trBeneAccountCurrency"), HtmlTableRow).Style.Remove("display")
                                    DirectCast(Me.Control.FindControl("trBeneAccountCurrencyPreview"), HtmlTableRow).Style.Remove("display")
                                    radBeneAccountCurrency.Enabled = True
                                    radBeneAccountCurrency.GetSettingByBehaviorID("reBeneAccountCurrency").Validation.IsRequired = True
                                    lblReqBeneAccountCurrency.Visible = False
                                    ''Enable False text boxes
                                    txtAccountTitle.ReadOnly = True
                                    txtBeneAccountCurrency.ReadOnly = True
                                    txtBeneAccountBranchCode.ReadOnly = True
                                    ViewState("BeneAccountBranchCode") = ""
                                    ViewState("BeneAccountCurrency") = ""
                                    ViewState("AccountTitle") = ""


                                Else
                                    txtAccountTitle.Text = objBene.BeneficiaryAccountTitle
                                    txtBeneAccountCurrency.Text = objBene.BeneAccountCurrency
                                    txtBeneAccountBranchCode.Text = objBene.BeneAccountBranchCode
                                    txtBeneName.Text = objBene.BeneficiaryAccountTitle
                                    ViewState("BeneName") = ""
                                    ViewState("BeneName") = objBene.BeneficiaryAccountTitle
                                    ViewState("BeneAccountBranchCode") = ""
                                    ViewState("BeneAccountBranchCode") = objBene.BeneAccountBranchCode
                                    ViewState("BeneAccountCurrency") = ""
                                    ViewState("BeneAccountCurrency") = objBene.BeneAccountCurrency
                                    ViewState("AccountTitle") = ""
                                    ViewState("AccountTitle") = objBene.BeneficiaryAccountTitle
                                    txtAccountTitle.ReadOnly = True
                                    txtBeneAccountCurrency.ReadOnly = True
                                    txtBeneAccountBranchCode.ReadOnly = True
                                End If
                            ElseIf objICProductType.DisbursementMode = "Other Credit" Then
                                If txtBankAccountNumber.Text.Trim = "" Or txtBankAccountNumber.Text Is Nothing Then
                                    txtBankAccountNumber.AutoPostBack = True
                                    DirectCast(Me.Control.FindControl("trAccountTitle"), HtmlTableRow).Style.Remove("display")
                                    DirectCast(Me.Control.FindControl("trAccountTitlePreview"), HtmlTableRow).Style.Remove("display")
                                    radAccountTitle.Enabled = True
                                    radAccountTitle.GetSettingByBehaviorID("reAccountTitle").Validation.IsRequired = True
                                    lblReqAccountTitle.Visible = False
                                    txtAccountTitle.ReadOnly = True
                                    ViewState("AccountTitle") = ""

                                Else
                                    txtBankAccountNumber.AutoPostBack = False
                                    DirectCast(Me.Control.FindControl("trAccountTitle"), HtmlTableRow).Style.Remove("display")
                                    DirectCast(Me.Control.FindControl("trAccountTitlePreview"), HtmlTableRow).Style.Remove("display")
                                    radAccountTitle.Enabled = True
                                    radAccountTitle.GetSettingByBehaviorID("reAccountTitle").Validation.IsRequired = True
                                    lblReqAccountTitle.Visible = False
                                    txtAccountTitle.ReadOnly = True
                                    txtAccountTitle.Text = objBene.BeneficiaryIBFTAccountTitle
                                    ViewState("AccountTitle") = ""
                                    ViewState("AccountTitle") = objBene.BeneficiaryIBFTAccountTitle
                                    txtBeneName.Text = objBene.BeneficiaryIBFTAccountTitle
                                    ViewState("BeneName") = ""
                                    ViewState("BeneName") = objBene.BeneficiaryIBFTAccountTitle
                                End If

                               
                                End If

                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "372" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneBranchAddress"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneBranchAddressPreview"), HtmlTableRow).Style.Remove("display")
                            radBeneBranchAddress.Enabled = True
                            radBeneBranchAddress.GetSettingByBehaviorID("reBeneBranchAddress").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            lblReqBeneBranchAddress.Visible = objICOnlineFormSetting.IsRequired
                            If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then
                                txtBeneficiaryBranchAddress.Text = objBene.BeneficiaryBranchAddress
                                ViewState("BeneficiaryBranchAddress") = ""
                                ViewState("BeneficiaryBranchAddress") = objBene.BeneficiaryBranchAddress
                                If IsFieldsDisabled = True Then
                                    txtBeneficiaryBranchAddress.Enabled = False
                                Else
                                    txtBeneficiaryBranchAddress.Text = Nothing  '' For New Beneficiary
                                    ViewState("BeneficiaryBranchAddress") = ""
                                    txtBeneficiaryBranchAddress.Enabled = True
                                End If
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "79" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trPrintDate"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trPrintDatePreview"), HtmlTableRow).Style.Remove("display")
                            lblReqPrintDate.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvPrintDate.Enabled = True
                            Else
                                rfvPrintDate.Enabled = False
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "72" Then
                        If objICProductType.DisbursementMode <> "Direct Credit" Then
                            If objICOnlineFormSetting.IsVisible = True Then
                                DirectCast(Me.Control.FindControl("trBeneAccountBranchCode"), HtmlTableRow).Style.Remove("display")
                                DirectCast(Me.Control.FindControl("trBeneAccountBranchCodePreview"), HtmlTableRow).Style.Remove("display")
                                radBeneAccountBranchCode.Enabled = True
                                radBeneAccountBranchCode.GetSettingByBehaviorID("reBeneAccountBranchCode").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                                lblReqBeneAccountBranchCode.Visible = objICOnlineFormSetting.IsRequired
                                If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then
                                    txtBeneAccountBranchCode.Text = objBene.BeneAccountBranchCode
                                    ViewState("BeneAccountBranchCode") = ""
                                    ViewState("BeneAccountBranchCode") = objBene.BeneAccountBranchCode
                                    If IsFieldsDisabled = True Then
                                        txtBeneAccountBranchCode.Enabled = False
                                    Else
                                        txtBeneAccountBranchCode.Text = Nothing  '' For New Beneficiary
                                        ViewState("BeneAccountBranchCode") = ""
                                        txtBeneAccountBranchCode.Enabled = True
                                    End If
                                End If
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "62" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            If objICProductType.DisbursementMode.ToLower.ToString = "cheque" Then
                                DirectCast(Me.Control.FindControl("trInstrumentNo"), HtmlTableRow).Style.Remove("display")
                                DirectCast(Me.Control.FindControl("trInstrumentNoPreview"), HtmlTableRow).Style.Remove("display")
                                radInstrumentNo.Enabled = True
                                radInstrumentNo.GetSettingByBehaviorID("reInstrumentNo").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                                lblReqInstrumentNo.Visible = objICOnlineFormSetting.IsRequired
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "73" Then
                        If objICProductType.DisbursementMode <> "Direct Credit" Then
                            If objICOnlineFormSetting.IsVisible = True Then
                                DirectCast(Me.Control.FindControl("trBeneAccountCurrency"), HtmlTableRow).Style.Remove("display")
                                DirectCast(Me.Control.FindControl("trBeneAccountCurrencyPreview"), HtmlTableRow).Style.Remove("display")
                                radBeneAccountCurrency.Enabled = True
                                radBeneAccountCurrency.GetSettingByBehaviorID("reBeneAccountCurrency").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                                lblReqBeneAccountCurrency.Visible = objICOnlineFormSetting.IsRequired
                                If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then
                                    txtBeneAccountCurrency.Text = objBene.BeneAccountCurrency
                                    ViewState("BeneAccountCurrency") = ""
                                    ViewState("BeneAccountCurrency") = objBene.BeneAccountCurrency
                                    If IsFieldsDisabled = True Then
                                        txtBeneAccountCurrency.Enabled = False
                                    Else
                                        txtBeneAccountCurrency.Text = Nothing  '' For New Beneficiary
                                        ViewState("BeneAccountCurrency") = ""
                                        txtBeneAccountCurrency.Enabled = True
                                    End If
                                End If
                            End If
                        End If
                       
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "64" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            LoadddlPrintLocations()
                            DirectCast(Me.Control.FindControl("trPrintLocation"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trPrintLocationPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqPrintLocation.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvddlPrintLocation.Enabled = True
                            Else
                                rfvddlPrintLocation.Enabled = False
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "36" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trDDPayableLocationProvince"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trDDDrawnLocationProvincePreview"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trDDPayableLocationCity"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trDDDrawnLocationCityPreview"), HtmlTableRow).Style.Remove("display")
                            LoadddlDDDrawnLocationProvince()
                            LoadddlDDDrawnLocationCity()
                            lblReqDDPayableLocationProvince.Visible = objICOnlineFormSetting.IsRequired
                            lblReqDDPayableLocationCity.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvddlDDPayableLocationProvince.Enabled = True
                                rfvddlDDPayableLocationCity.Enabled = True
                            Else
                                rfvddlDDPayableLocationProvince.Enabled = False
                                rfvddlDDPayableLocationCity.Enabled = False
                            End If
                            If objICProductType.DisbursementMode = "DD" Then
                                ddlDDPayableLocationCity.AutoPostBack = True
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "77" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trValueDate"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trValueDatePreview"), HtmlTableRow).Style.Remove("display")
                            rfvValueDate.Enabled = objICOnlineFormSetting.IsRequired
                            lblReqValueDate.Visible = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "6" Then
                        If objICProductType.DisbursementMode <> "Direct Credit" And objICProductType.DisbursementMode <> "Other Credit" Then
                            If objICOnlineFormSetting.IsVisible = True Then
                                DirectCast(Me.Control.FindControl("trAccountTitle"), HtmlTableRow).Style.Remove("display")
                                DirectCast(Me.Control.FindControl("trAccountTitlePreview"), HtmlTableRow).Style.Remove("display")
                                radAccountTitle.Enabled = True
                                radAccountTitle.GetSettingByBehaviorID("reAccountTitle").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                                lblReqAccountTitle.Visible = objICOnlineFormSetting.IsRequired
                                If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then
                                    txtAccountTitle.Text = objBene.BeneficiaryAccountTitle
                                    ViewState("AccountTitle") = ""
                                    ViewState("AccountTitle") = objBene.BeneficiaryAccountTitle
                                    If IsFieldsDisabled = True Then
                                        txtAccountTitle.Enabled = False
                                    Else
                                        txtAccountTitle.Text = Nothing  '' For New Beneficiary
                                        ViewState("AccountTitle") = ""
                                        txtAccountTitle.Enabled = True
                                    End If
                                End If
                            End If
                        End If

                    ElseIf objICOnlineFormSetting.FieldID.ToString = "9" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneBankName"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBankNamePreview"), HtmlTableRow).Style.Remove("display")
                            ' LoadddlBeneBank()
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvddlBeneBank.Enabled = True
                            Else
                                rfvddlBeneBank.Enabled = False
                            End If
                            lblReqBankName.Visible = objICOnlineFormSetting.IsRequired
                            'If objICProductType.DisbursementMode = "Other Credit" Then
                            '    ddlBeneBankName.AutoPostBack = True
                            'End If
                            If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then
                                ddlBeneBankName.SelectedValue = objBene.BeneficiaryBankCode
                                ViewState("BeneBankName") = ""
                                ViewState("BeneBankName") = objBene.BeneficiaryBankName
                                If ddlBeneBankName.SelectedValue <> "0" Then
                                    If IsFieldsDisabled = True Then
                                        ddlBeneBankName.Enabled = False
                                    Else
                                        ddlBeneBankName.SelectedValue = "0"   '' For New Beneficiary
                                        ViewState("BeneBankName") = ""
                                        ddlBeneBankName.Enabled = True
                                    End If
                                Else
                                    ddlBeneBankName.SelectedValue = "0"   '' For New Beneficiary
                                    ViewState("BeneBankName") = ""
                                    ddlBeneBankName.Enabled = True
                                End If
                            End If
                        End If

                    ElseIf objICOnlineFormSetting.FieldID.ToString = "18" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneMobNo"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneMobNoPreview"), HtmlTableRow).Style.Remove("display")
                            radBeneMobNo.Enabled = True
                            radBeneMobNo.GetSettingByBehaviorID("reBeneMobNo").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            lblReqBeneMobNo.Visible = objICOnlineFormSetting.IsRequired
                            If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then
                                txtBeneMobileNo.Text = objBene.BeneficiaryMobile
                                ViewState("BeneMobileNo") = ""
                                ViewState("BeneMobileNo") = objBene.BeneficiaryMobile
                                If IsFieldsDisabled = True Then
                                    txtBeneMobileNo.Enabled = False
                                Else
                                    txtBeneMobileNo.Text = Nothing  '' For New Beneficiary
                                    ViewState("BeneMobileNo") = ""
                                    txtBeneMobileNo.Enabled = True
                                End If
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "20" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBenePhoneNo"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBenePhoneNoPreview"), HtmlTableRow).Style.Remove("display")
                            radBenePhoneNo.Enabled = True
                            radBenePhoneNo.GetSettingByBehaviorID("reBenePhoneNo").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            lblReqPhonNo.Visible = objICOnlineFormSetting.IsRequired
                            If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then
                                txtBenePhoneNo.Text = objBene.BeneficiaryPhone
                                ViewState("BenePhoneNo") = ""
                                ViewState("BenePhoneNo") = objBene.BeneficiaryPhone
                                If IsFieldsDisabled = True Then
                                    txtBenePhoneNo.Enabled = False
                                Else
                                    txtBenePhoneNo.Text = Nothing  '' For New Beneficiary
                                    ViewState("BenePhoneNo") = ""
                                    txtBenePhoneNo.Enabled = True
                                End If
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "17" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneEmail"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneEmailPreview"), HtmlTableRow).Style.Remove("display")
                            radBeneEmail.Enabled = True
                            radBeneEmail.GetSettingByBehaviorID("reBeneEmail").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            lblReqEmail.Visible = objICOnlineFormSetting.IsRequired
                            If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then
                                txtBeneEmail.Text = objBene.BeneficiaryEmail
                                ViewState("BeneEmail") = ""
                                ViewState("BeneEmail") = objBene.BeneficiaryEmail
                                If IsFieldsDisabled = True Then
                                    txtBeneEmail.Enabled = False
                                Else
                                    txtBeneEmail.Text = Nothing  '' For New Beneficiary
                                    ViewState("BeneEmail") = ""
                                    txtBeneEmail.Enabled = True
                                End If
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "14" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneCNICNo"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneCNICNoPreview"), HtmlTableRow).Style.Remove("display")
                            radBeneCNICNo.Enabled = True
                            radBeneCNICNo.GetSettingByBehaviorID("reBeneCNICNo").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            lblReqBeneNICNo.Visible = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "13" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneCity"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneCityPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqBeneficiaryCity.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvddlBeneficiaryCity.Enabled = True
                            Else
                                rfvddlBeneficiaryCity.Enabled = False
                            End If
                            If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then
                                ddlBeneficiaryCity.SelectedValue = objBene.BeneficiaryCity
                                ViewState("BeneficiaryCity") = ""
                                ViewState("BeneficiaryCity") = objBene.BeneficiaryCity
                                If IsFieldsDisabled = True Then
                                    ddlBeneficiaryCity.Enabled = False
                                Else
                                    ddlBeneficiaryCity.SelectedValue = "0"   '' For New Beneficiary
                                    ViewState("BeneficiaryCity") = ""
                                    ddlBeneficiaryCity.Enabled = True
                                End If
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "21" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneProvince"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneficiaryProvincePreview"), HtmlTableRow).Style.Remove("display")
                            lblReqBeneProvince.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvddlBeneficiaryProvince.Enabled = True
                            Else
                                rfvddlBeneficiaryProvince.Enabled = False
                            End If
                            If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then
                                ddlBeneficiaryProvince.SelectedValue = objBene.BeneficiaryProvince
                                ViewState("BeneficiaryProvince") = ""
                                ViewState("BeneficiaryProvince") = objBene.BeneficiaryProvince
                                If IsFieldsDisabled = True Then
                                    ddlBeneficiaryProvince.Enabled = False
                                Else
                                    ddlBeneficiaryProvince.SelectedValue = "0"   '' For New Beneficiary
                                    ViewState("BeneficiaryProvince") = ""
                                    ddlBeneficiaryProvince.Enabled = True
                                End If
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "16" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneCountry"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneficiaryCountryPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqBeneCountry.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvddlBeneficiaryCountry.Enabled = True
                            Else
                                rfvddlBeneficiaryCountry.Enabled = False
                            End If
                            If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then
                                ddlBeneficiaryCountry.SelectedValue = objBene.BeneficiaryCountry
                                ViewState("BeneficiaryCountry") = ""
                                ViewState("BeneficiaryCountry") = objBene.BeneficiaryCountry
                                If IsFieldsDisabled = True Then
                                    ddlBeneficiaryCountry.Enabled = False
                                Else
                                    ddlBeneficiaryCountry.SelectedValue = "0"   '' For New Beneficiary
                                    ViewState("BeneficiaryCountry") = ""
                                    ddlBeneficiaryCountry.Enabled = True
                                End If
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "7" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneAddress1"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneAddressPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqBeneAddress1.Visible = objICOnlineFormSetting.IsRequired
                            radBeneAddress1.Enabled = True
                            radBeneAddress1.GetSettingByBehaviorID("reBeneAddress1").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then
                                txtBeneAddress1.Text = objBene.BeneficiaryAddress
                                ViewState("BeneAddress1") = ""
                                ViewState("BeneAddress1") = objBene.BeneficiaryAddress
                                If IsFieldsDisabled = True Then
                                    txtBeneAddress1.Enabled = False
                                Else
                                    txtBeneAddress1.Text = Nothing    '' For New Beneficiary
                                    ViewState("BeneAddress1") = ""
                                    txtBeneAddress1.Enabled = True
                                End If
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "3" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trAmountPKR"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trAmountPKRPreview"), HtmlTableRow).Style.Remove("display")
                            radAmountPKR.Enabled = True
                            radAmountPKR.GetSettingByBehaviorID("reAmountPKR").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            lblReqAmountPKR.Visible = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "1" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trDescription"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trDescriptionPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqDescription.Visible = objICOnlineFormSetting.IsRequired
                            radDescription.Enabled = True
                            radDescription.GetSettingByBehaviorID("reDescription").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "2" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trRemarks"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trRemarksPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqRemarks.Visible = objICOnlineFormSetting.IsRequired
                            radRemarks.Enabled = True
                            radRemarks.GetSettingByBehaviorID("reRemarks").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "67" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trReferenceField1"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trReferenceFieldPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqReferenceField1.Visible = objICOnlineFormSetting.IsRequired
                            radReferenceField1.Enabled = True
                            radReferenceField1.GetSettingByBehaviorID("reReferenceField1").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "68" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trReferenceField2"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trReferenceField2Preview"), HtmlTableRow).Style.Remove("display")
                            lblReqtrReferenceField2.Visible = objICOnlineFormSetting.IsRequired
                            radtrReferenceField2.Enabled = True
                            radtrReferenceField2.GetSettingByBehaviorID("retrReferenceField2").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "69" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trReferenceField3"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trReferenceField3Preview"), HtmlTableRow).Style.Remove("display")
                            lblReqReferenceField3.Visible = objICOnlineFormSetting.IsRequired
                            radReferenceField3.Enabled = True
                            radReferenceField3.GetSettingByBehaviorID("reReferenceField3").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "374" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneficiaryIdentityType"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneIDTypePreview"), HtmlTableRow).Style.Remove("display")
                            lblReqBeneIDType.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvBeneIDType.Enabled = True
                            Else
                                rfvBeneIDType.Enabled = False
                            End If
                            If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then
                                ddlBeneIDType.SelectedValue = objBene.BeneficiaryIDType
                                ViewState("BeneIDType") = ""
                                ViewState("BeneIDType") = objBene.BeneficiaryIDType
                                If ddlBeneIDType.SelectedValue <> "0" Then
                                    If IsFieldsDisabled = True Then
                                        ddlBeneIDType.Enabled = False
                                    Else
                                        ddlBeneIDType.SelectedValue = "0"  '' For New Beneficiary
                                        ViewState("BeneIDType") = ""
                                        ddlBeneIDType.Enabled = True
                                    End If
                                Else
                                    ddlBeneIDType.SelectedValue = "0"  '' For New Beneficiary
                                    ViewState("BeneIDType") = ""
                                    ddlBeneIDType.Enabled = True
                                End If
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "375" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneIDNo"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneIDNOPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqBeneIDNo.Visible = objICOnlineFormSetting.IsRequired
                            radBeneIDNo.Enabled = True
                            radBeneIDNo.GetSettingByBehaviorID("reBeneIDNo").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then
                                txtBeneIDNo.Text = objBene.BeneficiaryIDNo
                                ViewState("BeneIDNo") = ""
                                ViewState("BeneIDNo") = objBene.BeneficiaryIDNo
                                If txtBeneIDNo.Text.Trim <> "" Then
                                    If IsFieldsDisabled = True Then
                                        txtBeneIDNo.Enabled = False
                                    Else
                                        txtBeneIDNo.Text = Nothing  '' For New Beneficiary
                                        ViewState("BeneIDNo") = ""
                                        txtBeneIDNo.Enabled = True
                                    End If
                                Else
                                    txtBeneIDNo.Text = Nothing  '' For New Beneficiary
                                    ViewState("BeneIDNo") = ""
                                    txtBeneIDNo.Enabled = True
                                End If
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "11" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            LoadddlBenePickLocation()
                            DirectCast(Me.Control.FindControl("trBenePickUpLocation"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBenePickUpLocationPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqBenePickLocation.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvddlBenePickLocation.Enabled = True
                            Else
                                rfvddlBenePickLocation.Enabled = False
                            End If
                            If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then

                                If objBene.BeneficiaryBranchCode <> "0" And objBene.BeneficiaryBranchCode IsNot Nothing Then
                                    ddlBenePickLocation.selectedvalue = objBene.BeneficiaryBranchCode
                                End If
                                If ddlBenePickLocation.selectedvalue.tostring <> "0" Then
                                    If IsFieldsDisabled = True Then
                                        ddlBenePickLocation.Enabled = False
                                        ViewState("BeneBranchCode") = ddlBenePickLocation.selectedvalue.tostring
                                    Else
                                        'txtBeneIDNo.Text = Nothing  '' For New Beneficiary
                                        ViewState("BeneBranchCode") = ""
                                        ddlBenePickLocation.Enabled = False
                                    End If
                                Else
                                    'txtBeneIDNo.Text = Nothing  '' For New Beneficiary
                                    ViewState("BeneBranchCode") = ""
                                    ddlBenePickLocation.Enabled = True
                                End If
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "376" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            LoadddlUBPCompany()
                            DirectCast(Me.Control.FindControl("trUBPCompany"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trUBPCompanyPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqUBPCompany.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvUBPCompany.Enabled = True
                            Else
                                rfvUBPCompany.Enabled = False
                            End If
                            If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then
                                If Not objBene.UBPCompanyID Is Nothing Then
                                    ddlUBPCompany.SelectedValue = ICUBPSCompanyController.GetUBPCompanyIDByCompanyCodeor(objBene.UBPCompanyID)
                                End If
                                ViewState("UBPSCompanyCode") = ""
                                ViewState("UBPSCompanyCode") = objBene.UBPCompanyID
                                If ddlUBPCompany.SelectedValue.ToString <> "0" Then
                                    If IsFieldsDisabled = True Then
                                        ddlUBPCompany.Enabled = False
                                    Else
                                        ddlUBPCompany.SelectedValue = "0"  '' For New Beneficiary
                                        ViewState("UBPSCompanyCode") = ""
                                        ddlUBPCompany.Enabled = True
                                    End If
                                Else
                                    'txtBeneIDNo.Text = Nothing  '' For New Beneficiary
                                    ViewState("UBPSCompanyCode") = ""
                                    ddlUBPCompany.Enabled = True
                                End If
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "377" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trUBPReferenceField"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trUBPReferenceFieldPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqUBPReferenceField.Visible = objICOnlineFormSetting.IsRequired
                            radUBPReferenceField.Enabled = True
                            radUBPReferenceField.GetSettingByBehaviorID("reUBPReferenceField").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            If ddlBeneficiary.Visible = True And ddlBeneficiary.SelectedValue <> "0" Then
                                txtUBPReferenceField.Text = objBene.UBPReferenceNo
                                ViewState("UBPRefNo") = ""
                                ViewState("UBPRefNo") = objBene.UBPReferenceNo
                                If txtUBPReferenceField.Text.Trim <> "" Then
                                    If IsFieldsDisabled = True Then
                                        txtUBPReferenceField.Enabled = False
                                    Else
                                        txtUBPReferenceField.Text = Nothing  '' For New Beneficiary
                                        ViewState("UBPRefNo") = ""
                                        txtUBPReferenceField.Enabled = True
                                    End If
                                Else
                                    txtUBPReferenceField.Text = Nothing  '' For New Beneficiary
                                    ViewState("UBPRefNo") = ""
                                    txtUBPReferenceField.Enabled = True
                                End If
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "378" Then
                        If objICOnlineFormSetting.IsVisible = True Then

                            DirectCast(Me.Control.FindControl("trBeneficiaryAccountType"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneficiaryAccountTypePreview"), HtmlTableRow).Style.Remove("display")
                            lblReqBeneCBAccountType.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvCBAccountType.Enabled = True
                            Else
                                rfvCBAccountType.Enabled = False
                            End If

                        End If
                    End If

                Next
                dr("BeneName") = ViewState("BeneName")
                dr("BankAccountNumber") = ViewState("BankAccountNumber")
                dr("BeneficiaryBranchAddress") = ViewState("BeneficiaryBranchAddress")
                dr("BeneAccountBranchCode") = ViewState("BeneAccountBranchCode")
                dr("BeneAccountCurrency") = ViewState("BeneAccountCurrency")
                dr("AccountTitle") = ViewState("AccountTitle")
                dr("BeneBankName") = ViewState("BeneBankName")
                dr("BeneMobileNo") = ViewState("BeneMobileNo")
                dr("BenePhoneNo") = ViewState("BenePhoneNo")
                dr("BeneEmail") = ViewState("BeneEmail")
                dr("BeneIDNo") = ViewState("BeneIDNo")
                dr("BeneIDType") = ViewState("BeneIDType")
                dr("BeneficiaryCity") = ViewState("BeneficiaryCity")
                dr("BeneficiaryProvince") = ViewState("BeneficiaryProvince")
                dr("BeneficiaryCountry") = ViewState("BeneficiaryCountry")
                dr("BeneAddress1") = ViewState("BeneAddress1")
                dr("UBPSRefNo") = ViewState("UBPRefNo")
                dr("UBPSCompanyCode") = ViewState("UBPSCompanyCode")
                dt.Rows.Add(dr)
                Session("dtBene") = dt
            Else
                UIUtilities.ShowDialog(Me, "Error", "Please set online form settings for selected product type", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                Exit Sub
            End If
            'Else
            '    UIUtilities.ShowDialog(Me, "Error", "Please select account, payment nature, product type and company.", ICBO.IC.Dialogmessagetype.Failure)
            '    Exit Sub
        End If
    End Sub
    Private Sub SetViewFieldsVisibility()
        Dim objOnlineFormSettingsColl As New ICOnlineFormSettingsCollection
        Dim objICAPNature As New ICAccountsPaymentNature
        Dim objICProductType As New ICProductType
        objICProductType.es.Connection.CommandTimeout = 3600
        objOnlineFormSettingsColl.es.Connection.CommandTimeout = 3600
        objICAPNature.es.Connection.CommandTimeout = 3600

        rfvddlPrintLocation.Enabled = False
        rfvddlBeneficiaryCity.Enabled = False
        rfvddlBeneficiaryProvince.Enabled = False
        rfvddlBeneficiaryCountry.Enabled = False
        rfvddlBeneBank.Enabled = False
        rfvddlDDPayableLocationProvince.Enabled = False
        rfvddlDDPayableLocationCity.Enabled = False

        txtBankAccountNumber.AutoPostBack = False
        txtAccountTitle.ReadOnly = False
        txtBeneAccountCurrency.ReadOnly = False
        txtBeneAccountBranchCode.ReadOnly = False
        ddlBeneBankName.AutoPostBack = False
        ddlDDPayableLocationCity.AutoPostBack = False
        If ddlAccPNature.SelectedValue.ToString <> "0" And ddlCompany.SelectedValue.ToString <> "0" And ddlProductType.SelectedValue.ToString <> "0" Then
            objICAPNature.LoadByPrimaryKey(ddlAccPNature.SelectedValue.ToString.Split("-")(0), ddlAccPNature.SelectedValue.ToString.Split("-")(1), ddlAccPNature.SelectedValue.ToString.Split("-")(2), ddlAccPNature.SelectedValue.ToString.Split("-")(3))
            objOnlineFormSettingsColl = ICInstructionController.GetAllOnlineFormSettingFieldsTaggedWithAPNautueAndProductType(objICAPNature, ddlCompany.SelectedValue.ToString, ddlProductType.SelectedValue.ToString)
            objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)
            If objOnlineFormSettingsColl.Count > 0 Then
                DirectCast(Me.Control.FindControl("trPaymentModePreview"), HtmlTableRow).Style.Remove("display")



                radAmountInWords.Enabled = True
                'sharedCalendarValueDate.SelectedDate = Date.Now
                'sharedCalendarValueDate.RangeMinDate = Date.Now
                'txtValueDate.MinDate = Date.Now
                'txtValueDate.SelectedDate = Date.Now
                For Each objICOnlineFormSetting As ICOnlineFormSettings In objOnlineFormSettingsColl
                    If objICOnlineFormSetting.FieldID.ToString = "19" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneName"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trPreviewBeneName"), HtmlTableRow).Style.Remove("display")
                            radBeneName.Enabled = True
                            radBeneName.GetSettingByBehaviorID("reBeneName").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            lblReqBeneName.Visible = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "5" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBankAccountNumber"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBankAccountNumberPreview"), HtmlTableRow).Style.Remove("display")
                            radBankAccountNumber.Enabled = True
                            radBankAccountNumber.GetSettingByBehaviorID("reBankAccountNumber").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            lblReqBankAccountNumber.Visible = objICOnlineFormSetting.IsRequired
                            If objICProductType.DisbursementMode = "Direct Credit" Then
                                txtBankAccountNumber.AutoPostBack = True
                                ''Beneficiary Account Title

                                DirectCast(Me.Control.FindControl("trAccountTitle"), HtmlTableRow).Style.Remove("display")
                                DirectCast(Me.Control.FindControl("trAccountTitlePreview"), HtmlTableRow).Style.Remove("display")
                                radAccountTitle.Enabled = True
                                radAccountTitle.GetSettingByBehaviorID("reAccountTitle").Validation.IsRequired = True
                                lblReqAccountTitle.Visible = False


                                ''Beneficiary Branch Code

                                DirectCast(Me.Control.FindControl("trBeneAccountBranchCode"), HtmlTableRow).Style.Remove("display")
                                DirectCast(Me.Control.FindControl("trBeneAccountBranchCodePreview"), HtmlTableRow).Style.Remove("display")
                                radBeneAccountBranchCode.Enabled = True
                                radBeneAccountBranchCode.GetSettingByBehaviorID("reBeneAccountBranchCode").Validation.IsRequired = True
                                lblReqBeneAccountBranchCode.Visible = False

                                ''Beneficiary Account Currency

                                DirectCast(Me.Control.FindControl("trBeneAccountCurrency"), HtmlTableRow).Style.Remove("display")
                                DirectCast(Me.Control.FindControl("trBeneAccountCurrencyPreview"), HtmlTableRow).Style.Remove("display")
                                radBeneAccountCurrency.Enabled = True
                                radBeneAccountCurrency.GetSettingByBehaviorID("reBeneAccountCurrency").Validation.IsRequired = True
                                lblReqBeneAccountCurrency.Visible = False



                                ''Enable False text boxes
                                txtAccountTitle.ReadOnly = True
                                txtBeneAccountCurrency.ReadOnly = True
                                txtBeneAccountBranchCode.ReadOnly = True


                            ElseIf objICProductType.DisbursementMode = "Other Credit" Then
                                txtBankAccountNumber.AutoPostBack = True
                                DirectCast(Me.Control.FindControl("trAccountTitle"), HtmlTableRow).Style.Remove("display")
                                DirectCast(Me.Control.FindControl("trAccountTitlePreview"), HtmlTableRow).Style.Remove("display")
                                radAccountTitle.Enabled = True
                                radAccountTitle.GetSettingByBehaviorID("reAccountTitle").Validation.IsRequired = True
                                lblReqAccountTitle.Visible = False
                                txtAccountTitle.ReadOnly = True
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "372" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneBranchAddress"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneBranchAddressPreview"), HtmlTableRow).Style.Remove("display")
                            radBeneBranchAddress.Enabled = True
                            radBeneBranchAddress.GetSettingByBehaviorID("reBeneBranchAddress").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            lblReqBeneBranchAddress.Visible = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "79" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trPrintDate"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trPrintDatePreview"), HtmlTableRow).Style.Remove("display")
                            lblReqPrintDate.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvPrintDate.Enabled = True
                            Else
                                rfvPrintDate.Enabled = False
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "72" Then
                        If objICProductType.DisbursementMode <> "Direct Credit" Then
                            If objICOnlineFormSetting.IsVisible = True Then
                                DirectCast(Me.Control.FindControl("trBeneAccountBranchCode"), HtmlTableRow).Style.Remove("display")
                                DirectCast(Me.Control.FindControl("trBeneAccountBranchCodePreview"), HtmlTableRow).Style.Remove("display")
                                radBeneAccountBranchCode.Enabled = True
                                radBeneAccountBranchCode.GetSettingByBehaviorID("reBeneAccountBranchCode").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                                lblReqBeneAccountBranchCode.Visible = objICOnlineFormSetting.IsRequired
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "62" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            If objICProductType.DisbursementMode.ToLower.ToString = "cheque" Then
                                DirectCast(Me.Control.FindControl("trInstrumentNo"), HtmlTableRow).Style.Remove("display")
                                DirectCast(Me.Control.FindControl("trInstrumentNoPreview"), HtmlTableRow).Style.Remove("display")
                                radInstrumentNo.Enabled = True
                                radInstrumentNo.GetSettingByBehaviorID("reInstrumentNo").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                                lblReqInstrumentNo.Visible = objICOnlineFormSetting.IsRequired
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "73" Then
                        If objICProductType.DisbursementMode <> "Direct Credit" Then
                            If objICOnlineFormSetting.IsVisible = True Then
                                DirectCast(Me.Control.FindControl("trBeneAccountCurrency"), HtmlTableRow).Style.Remove("display")
                                DirectCast(Me.Control.FindControl("trBeneAccountCurrencyPreview"), HtmlTableRow).Style.Remove("display")
                                radBeneAccountCurrency.Enabled = True
                                radBeneAccountCurrency.GetSettingByBehaviorID("reBeneAccountCurrency").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                                lblReqBeneAccountCurrency.Visible = objICOnlineFormSetting.IsRequired
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "64" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            LoadddlPrintLocations()
                            DirectCast(Me.Control.FindControl("trPrintLocation"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trPrintLocationPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqPrintLocation.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvddlPrintLocation.Enabled = True
                            Else
                                rfvddlPrintLocation.Enabled = False
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "36" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trDDPayableLocationProvince"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trDDDrawnLocationProvincePreview"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trDDPayableLocationCity"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trDDDrawnLocationCityPreview"), HtmlTableRow).Style.Remove("display")
                            LoadddlDDDrawnLocationProvince()
                            LoadddlDDDrawnLocationCity()
                            lblReqDDPayableLocationProvince.Visible = objICOnlineFormSetting.IsRequired
                            lblReqDDPayableLocationCity.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvddlDDPayableLocationProvince.Enabled = True
                                rfvddlDDPayableLocationCity.Enabled = True
                            Else
                                rfvddlDDPayableLocationProvince.Enabled = False
                                rfvddlDDPayableLocationCity.Enabled = False
                            End If
                            If objICProductType.DisbursementMode = "DD" Then
                                ddlDDPayableLocationCity.AutoPostBack = True
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "77" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trValueDate"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trValueDatePreview"), HtmlTableRow).Style.Remove("display")
                            rfvValueDate.Enabled = objICOnlineFormSetting.IsRequired
                            lblReqValueDate.Visible = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "6" Then
                        If objICProductType.DisbursementMode <> "Direct Credit" And objICProductType.DisbursementMode <> "Other Credit" Then
                            If objICOnlineFormSetting.IsVisible = True Then
                                DirectCast(Me.Control.FindControl("trAccountTitle"), HtmlTableRow).Style.Remove("display")
                                DirectCast(Me.Control.FindControl("trAccountTitlePreview"), HtmlTableRow).Style.Remove("display")
                                radAccountTitle.Enabled = True
                                radAccountTitle.GetSettingByBehaviorID("reAccountTitle").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                                lblReqAccountTitle.Visible = objICOnlineFormSetting.IsRequired
                            End If
                        End If

                    ElseIf objICOnlineFormSetting.FieldID.ToString = "9" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneBankName"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBankNamePreview"), HtmlTableRow).Style.Remove("display")
                            LoadddlBeneBank()
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvddlBeneBank.Enabled = True
                            Else
                                rfvddlBeneBank.Enabled = False
                            End If
                            lblReqBankName.Visible = objICOnlineFormSetting.IsRequired
                            If objICProductType.DisbursementMode = "Other Credit" Then
                                ddlBeneBankName.AutoPostBack = True
                            End If
                        End If

                    ElseIf objICOnlineFormSetting.FieldID.ToString = "18" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneMobNo"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneMobNoPreview"), HtmlTableRow).Style.Remove("display")
                            radBeneMobNo.Enabled = True
                            radBeneMobNo.GetSettingByBehaviorID("reBeneMobNo").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            lblReqBeneMobNo.Visible = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "20" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBenePhoneNo"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBenePhoneNoPreview"), HtmlTableRow).Style.Remove("display")
                            radBenePhoneNo.Enabled = True
                            radBenePhoneNo.GetSettingByBehaviorID("reBenePhoneNo").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            lblReqPhonNo.Visible = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "17" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneEmail"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneEmailPreview"), HtmlTableRow).Style.Remove("display")
                            radBeneEmail.Enabled = True
                            radBeneEmail.GetSettingByBehaviorID("reBeneEmail").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            lblReqEmail.Visible = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "14" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneCNICNo"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneCNICNoPreview"), HtmlTableRow).Style.Remove("display")
                            radBeneCNICNo.Enabled = True
                            radBeneCNICNo.GetSettingByBehaviorID("reBeneCNICNo").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            lblReqBeneNICNo.Visible = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "13" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneCity"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneCityPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqBeneficiaryCity.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvddlBeneficiaryCity.Enabled = True
                            Else
                                rfvddlBeneficiaryCity.Enabled = False
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "21" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneProvince"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneficiaryProvincePreview"), HtmlTableRow).Style.Remove("display")
                            lblReqBeneProvince.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvddlBeneficiaryProvince.Enabled = True
                            Else
                                rfvddlBeneficiaryProvince.Enabled = False
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "16" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneCountry"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneficiaryCountryPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqBeneCountry.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvddlBeneficiaryCountry.Enabled = True
                            Else
                                rfvddlBeneficiaryCountry.Enabled = False
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "7" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneAddress1"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneAddressPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqBeneAddress1.Visible = objICOnlineFormSetting.IsRequired
                            radBeneAddress1.Enabled = True
                            radBeneAddress1.GetSettingByBehaviorID("reBeneAddress1").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "3" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trAmountPKR"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trAmountPKRPreview"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trAmountInWords"), HtmlTableRow).Style.Remove("display")
                            radAmountPKR.Enabled = True
                            radAmountPKR.GetSettingByBehaviorID("reAmountPKR").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            lblReqAmountPKR.Visible = objICOnlineFormSetting.IsRequired
                            lblReqAmountInWords.Visible = objICOnlineFormSetting.IsRequired
                            radAmountInWords.Enabled = True
                            radAmountInWords.GetSettingByBehaviorID("reAmountInWords").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "1" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trDescription"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trDescriptionPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqDescription.Visible = objICOnlineFormSetting.IsRequired
                            radDescription.Enabled = True
                            radDescription.GetSettingByBehaviorID("reDescription").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "2" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trRemarks"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trRemarksPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqRemarks.Visible = objICOnlineFormSetting.IsRequired
                            radRemarks.Enabled = True
                            radRemarks.GetSettingByBehaviorID("reRemarks").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "67" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trReferenceField1"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trReferenceFieldPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqReferenceField1.Visible = objICOnlineFormSetting.IsRequired
                            radReferenceField1.Enabled = True
                            radReferenceField1.GetSettingByBehaviorID("reReferenceField1").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "68" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trReferenceField2"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trReferenceField2Preview"), HtmlTableRow).Style.Remove("display")
                            lblReqtrReferenceField2.Visible = objICOnlineFormSetting.IsRequired
                            radtrReferenceField2.Enabled = True
                            radtrReferenceField2.GetSettingByBehaviorID("retrReferenceField2").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "69" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trReferenceField3"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trReferenceField3Preview"), HtmlTableRow).Style.Remove("display")
                            lblReqReferenceField3.Visible = objICOnlineFormSetting.IsRequired
                            radReferenceField3.Enabled = True
                            radReferenceField3.GetSettingByBehaviorID("reReferenceField3").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "374" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneficiaryIdentityType"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneIDTypePreview"), HtmlTableRow).Style.Remove("display")
                            lblReqBeneIDType.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvBeneIDType.Enabled = True
                            Else
                                rfvBeneIDType.Enabled = False
                            End If
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "375" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBeneIDNo"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneIDNOPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqBeneIDNo.Visible = objICOnlineFormSetting.IsRequired
                            radBeneIDNo.Enabled = True
                            radBeneIDNo.GetSettingByBehaviorID("reBeneIDNo").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "11" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            LoadddlBenePickLocation()
                            DirectCast(Me.Control.FindControl("trBenePickUpLocation"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBenePickUpLocationPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqBenePickLocation.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvddlBenePickLocation.Enabled = True
                            Else
                                rfvddlBenePickLocation.Enabled = False
                            End If

                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "376" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            LoadddlUBPCompany()
                            DirectCast(Me.Control.FindControl("trUBPCompany"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trUBPCompanyPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqUBPCompany.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvUBPCompany.Enabled = True
                            Else
                                rfvUBPCompany.Enabled = False
                            End If

                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "377" Then
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trUBPReferenceField"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trUBPReferenceFieldPreview"), HtmlTableRow).Style.Remove("display")
                            lblReqUBPReferenceField.Visible = objICOnlineFormSetting.IsRequired
                            radUBPReferenceField.Enabled = True
                            radUBPReferenceField.GetSettingByBehaviorID("reUBPReferenceField").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                        End If
                    ElseIf objICOnlineFormSetting.FieldID.ToString = "378" Then
                        If objICOnlineFormSetting.IsVisible = True Then

                            DirectCast(Me.Control.FindControl("trBeneficiaryAccountType"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneficiaryAccountTypePreview"), HtmlTableRow).Style.Remove("display")
                            lblReqBeneCBAccountType.Visible = objICOnlineFormSetting.IsRequired
                            If objICOnlineFormSetting.IsRequired = True Then
                                rfvCBAccountType.Enabled = True
                            Else
                                rfvCBAccountType.Enabled = False
                            End If

                        End If
                    End If

                Next
            Else
                UIUtilities.ShowDialog(Me, "Error", "Please set online form settings for selected product type", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                Exit Sub
            End If
            'Else
            '    UIUtilities.ShowDialog(Me, "Error", "Please select account, payment nature, product type and company.", ICBO.IC.Dialogmessagetype.Failure)
            '    Exit Sub
        End If
    End Sub
    Private Sub HideRequiredMarkSign()
        lblReqBeneName.Visible = False
        lblReqBankAccountNumber.Visible = False
        lblReqPrintLocation.Visible = False
        'lblReqBankBranchCode.Visible = False
        lblReqAccountTitle.Visible = False
        'lblReqBranchName.Visible = False
        'lblReqBranchName.Visible = False
        'lblReqBankaddress.Visible = False
        'lblReqClientBankName.Visible = False
        lblReqDDPayableLocationCity.Visible = False
        lblReqDDPayableLocationProvince.Visible = False
        lblReqPhonNo.Visible = False
        lblReqBeneMobNo.Visible = False
        lblReqEmail.Visible = False
        lblReqBeneNICNo.Visible = False
        lblReqBeneficiaryCity.Visible = False
        lblReqBeneProvince.Visible = False
        lblReqBeneCountry.Visible = False
        lblReqAmountPKR.Visible = False
        lblReqAmountInWords.Visible = False
        lblReqDescription.Visible = False
        lblReqRemarks.Visible = False
        lblReqReferenceField1.Visible = False
        lblReqtrReferenceField2.Visible = False
        lblReqReferenceField3.Visible = False
        lblReqValueDate.Visible = False
        lblReqInstrumentNo.Visible = False
        lblReqPrintDate.Visible = False
        lblReqBeneBranchAddress.Visible = False
        lblReqBeneIDType.Visible = False
        lblReqBeneIDNo.Visible = False
        lblReqBenePickLocation.Visible = False
        ''B.P
        lblReqUBPCompany.Visible = False
        lblReqUBPReferenceField.Visible = False

        lblReqBeneCBAccountType.Visible = False
        lblReqGLAccountBranchCode.Visible = False
    End Sub
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            SetArraListRoleID()
            LoadddlCompany()
            LoadddlAccountPaymentNatureByUserID()
            LoadddlProductTypeByAccountPaymentNature()
            'LoadddlClientOfficeByUserID()
            ClearOnlineForm()
            ClearPreview()
            HideRequiredMarkSign()
            HideAllOnlineFormRows()

            'sharedCalendarValueDate.SelectedDate = Date.Now
            'sharedCalendarValueDate.RangeMinDate = Date.Now
            'txtValueDate.MinDate = Date.Now
            'txtValueDate.SelectedDate = Date.Now


            HideAllOnlineFormPreviewRows()
            pnlOnlineFrom.Style.Add("display", "none")
            pnlOnLineFormPreview.Style.Add("display", "none")
            ddlCompany_SelectedIndexChanged(ddlCompany.SelectedValue.ToString(), Nothing)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub ddlAccPNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAccPNature.SelectedIndexChanged
        Try
            SetArraListRoleID()
            LoadddlProductTypeByAccountPaymentNature()
            'LoadddlClientOfficeByUserID()
            ClearOnlineForm()
            ClearPreview()
            HideRequiredMarkSign()
            HideAllOnlineFormRows()
            'sharedCalendarValueDate.SelectedDate = Date.Now
            'sharedCalendarValueDate.RangeMinDate = Date.Now
            'txtValueDate.MinDate = Date.Now
            'txtValueDate.SelectedDate = Date.Now
            HideAllOnlineFormPreviewRows()
            pnlOnlineFrom.Style.Add("display", "none")
            pnlOnLineFormPreview.Style.Add("display", "none")


            If ddlAccPNature.SelectedValue <> "0" Then
                Dim objCompany As New ICCompany
                objCompany.LoadByPrimaryKey(ddlCompany.SelectedValue.ToString())
                If objCompany.IsBeneRequired = True Then
                    LoadBeneGroup(ddlAccPNature.SelectedValue.ToString().Split("-")(3).ToString(), ddlCompany.SelectedValue.ToString())
                    rfvBeneficiaryGroup.Enabled = True
                    rfvBeneficiary.Enabled = True
                    pnlBeneficiary.Visible = True
                Else
                    rfvBeneficiary.Enabled = False
                    rfvBeneficiaryGroup.Enabled = False
                    pnlBeneficiary.Visible = False
                End If
            Else
                ddlCompany_SelectedIndexChanged(ddlCompany.SelectedValue.ToString(), Nothing)
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadBeneGroup(ByVal PaymentNatureCode As String, ByVal CompanyCode As String)
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBeneficiaryGroup.Items.Clear()
            ddlBeneficiaryGroup.Items.Add(lit)
            ddlBeneficiaryGroup.AppendDataBoundItems = True
            ddlBeneficiaryGroup.DataSource = ICBeneficiaryGroupManagementController.GetAllActiveandApproveBeneGroupByPNandCompanyCode(PaymentNatureCode, CompanyCode)
            ddlBeneficiaryGroup.DataTextField = "BeneGroupName"
            ddlBeneficiaryGroup.DataValueField = "BeneGroupCode"
            ddlBeneficiaryGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetPreviewforNewBene()
        Dim objICProduct As New ICProductType
        Dim objICCompany As New ICCompany
        objICProduct.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)
        objICCompany.LoadByPrimaryKey(ddlCompany.SelectedValue.ToString)
        lblprvPaymentMode.Text = objICProduct.DisbursementMode.ToString

        Dim objBene As New ICBene
        Dim rslt As String = Nothing
        Dim DuplicateField As String = Nothing
        Dim DuplicateFieldValue As String = Nothing
        'If objICCompany.IsBeneRequired = True Then
        objBene.BeneficiaryAccountNo = txtBankAccountNumber.Text.ToString()
        objBene.BeneficiaryIBFTAccountNo = txtBankAccountNumber.Text.ToString()
        objBene.BeneficiaryIDNo = txtBeneIDNo.Text.ToString
        objBene.BeneficiaryIDType = ddlBeneIDType.SelectedValue.ToString()

        If ddlUBPCompany.SelectedValue.ToString() <> "0" Then
            objBene.UBPCompanyID = ddlUBPCompany.SelectedValue.ToString()
        End If
        objBene.UBPReferenceNo = txtUBPReferenceField.text
        rslt = ICBeneficiaryManagementController.CheckDuplicateBeneForSingleInput(objBene, objICCompany.CompanyCode)
        'Else
        '    rslt = "OK"
        'End If


        If rslt <> "OK" Then

            If rslt = "Beneficiary Code" Then
                DuplicateField = "BeneficiaryCode"
                DuplicateFieldValue = objBene.BeneficiaryCode
            ElseIf rslt = "Beneficiary FT Account Number" Then
                DuplicateField = "BeneficiaryAccountNo"
                DuplicateFieldValue = objBene.BeneficiaryAccountNo
            ElseIf rslt = "Beneficiary IBFT Account Number" Then
                DuplicateField = "BeneficiaryIBFTAccountNo"
                DuplicateFieldValue = objBene.BeneficiaryIBFTAccountNo
            ElseIf rslt = "Beneficiary Identification No. and Beneficiary Identification Type" Then
                DuplicateField = "BeneficiaryIdentificationNo"
                DuplicateFieldValue = objBene.BeneficiaryIDNo
            ElseIf rslt = "UBP Reference No" Then
                DuplicateField = "UBP Reference No"
                DuplicateFieldValue = objBene.UBPCompanyID & "-" & objBene.UBPReferenceNo
            End If

            Dim BeneID As String = Nothing
            BeneID = ICBeneficiaryManagementController.GetBeneIDbyBeneFields(DuplicateField, DuplicateFieldValue, objICCompany.CompanyCode, objBene.BeneficiaryIDType)

            objBene.LoadByPrimaryKey(BeneID)

            If txtBeneName.Text.ToString <> "" And txtBeneName.Text.ToString <> "Enter Beneficiary Name" Then
                lblprvBeneName.Text = objBene.BeneficiaryName.ToString()
            Else
                lblprvBeneName.Text = "N/A"
            End If
            If txtBankAccountNumber.Text.ToString <> "" And txtBankAccountNumber.Text.ToString <> "Enter Bank Account Number" Then
                If objICProduct.DisbursementMode = "Direct Credit" Then
                    lblprvBankAccountNo.Text = objBene.BeneficiaryAccountNo.ToString()
                ElseIf objICProduct.DisbursementMode = "Other Credit" Then
                    lblprvBankAccountNo.Text = objBene.BeneficiaryIBFTAccountNo.ToString()
                End If
            Else
                lblprvBankAccountNo.Text = "N/A"
            End If
            If txtAccountTitle.Text.ToString <> "" And txtAccountTitle.Text.ToString <> "Enter Account Title" Then
                If objICProduct.DisbursementMode = "Direct Credit" Then
                    lblprvAccountTitle.Text = objBene.BeneficiaryAccountTitle.ToString()
                ElseIf objICProduct.DisbursementMode = "Other Credit" Then
                    lblprvAccountTitle.Text = objBene.BeneficiaryIBFTAccountTitle.ToString()
                End If
            Else
                lblprvAccountTitle.Text = "N/A"
            End If
            If txtBeneAccountBranchCode.ToString <> "" And txtBeneAccountBranchCode.Text.ToString <> "Enter Account Branch Code" Then
                If objICProduct.DisbursementMode = "Direct Credit" Then
                    lblprvBeneAccountBranchCodePreview.Text = objBene.BeneAccountBranchCode.ToString.ToUpper()
                End If
            Else
                lblprvBeneAccountBranchCodePreview.Text = "N/A"
            End If
            If txtBeneAccountCurrency.Text.ToString <> "" And txtBeneAccountCurrency.Text.ToString <> "Enter Account Currency" Then
                If objICProduct.DisbursementMode = "Direct Credit" Then
                    lblprvBeneAccountCurrency.Text = objBene.BeneAccountCurrency.ToString()
                End If
            Else
                lblprvBeneAccountCurrency.Text = "N/A"
            End If
            If ddlBeneBankName.SelectedValue.ToString <> "0" Then
                If Not objBene.BeneficiaryBankCode Is Nothing Then
                    Dim objBank As New ICBank
                    If objBank.LoadByPrimaryKey(objBene.BeneficiaryBankCode) Then
                        lblprvBankName.Text = objBank.BankName.ToString()
                    End If
                End If
            Else
                lblprvBankName.Text = "N/A"
            End If
            If txtBeneficiaryBranchAddress.Text.ToString <> "" Then
                lblprvBeneBranchAddress.Text = objBene.BeneficiaryBranchAddress.ToString()
            Else
                lblprvBeneBranchAddress.Text = "N/A"
            End If
            If txtBeneMobileNo.Text.ToString <> "" And txtBeneMobileNo.Text.ToString <> "Enter Beneficiary Mobile No." Then
                lblprvMobNo.Text = objBene.BeneficiaryMobile.ToString()
            Else
                lblprvMobNo.Text = "N/A"
            End If
            If txtBenePhoneNo.Text.ToString <> "" And txtBenePhoneNo.Text.ToString <> "Enter Beneficiary Phone No." Then
                lblprvPhoneNo.Text = objBene.BeneficiaryPhone.ToString()
            Else
                lblprvPhoneNo.Text = "N/A"
            End If
            If txtBeneEmail.Text.ToString <> "" And txtBeneEmail.Text.ToString <> "Enter Email Address" Then
                lblprvEmail.Text = objBene.BeneficiaryEmail.ToString()
            Else
                lblprvEmail.Text = "N/A"
            End If
            If txtBeneCNCINo.Text.ToString <> "" And txtBeneCNCINo.Text.ToString <> "Enter CNIC No" Then
                lblprvNICNo.Text = objBene.BeneficiaryCNIC.ToString()
            Else
                lblprvNICNo.Text = "N/A"
            End If
            If ddlBeneficiaryCity.SelectedValue.ToString <> "0" Then
                Dim objCity As New ICCity
                objCity.LoadByPrimaryKey(objBene.BeneficiaryCity)
                lblprvCity.Text = objCity.CityName.ToString()
            Else
                lblprvCity.Text = "N/A"
            End If
            If ddlBeneficiaryProvince.SelectedValue.ToString <> "0" Then
                Dim objProvince As New ICProvince
                objProvince.LoadByPrimaryKey(objBene.BeneficiaryProvince)
                lblprvBeneficiaryProvince.Text = objProvince.ProvinceName.ToString()
            Else
                lblprvBeneficiaryProvince.Text = "N/A"
            End If
            If ddlBeneficiaryCountry.SelectedValue.ToString <> "0" Then
                Dim objCountry As New ICCountry
                objCountry.LoadByPrimaryKey(objBene.BeneficiaryCountry)
                lblprvBeneficiaryCountry.Text = objCountry.CountryName.ToString()
            Else
                lblprvBeneficiaryCountry.Text = "N/A"
            End If
            If txtBeneAddress1.Text.ToString <> "" And txtBeneAddress1.Text.ToString <> "Enter Address" Then
                lblprvAddress1.Text = objBene.BeneficiaryAddress.ToString()
            Else
                lblprvAddress1.Text = "N/A"
            End If
            If txtBeneIDNo.Text.ToString <> "" Then
                lblPrvBeneIDNo.Text = objBene.BeneficiaryIDNo.ToString()
            Else
                lblPrvBeneIDNo.Text = "N/A"
            End If
            If ddlBeneIDType.SelectedValue <> "0" Then
                lblPrvBeneIDType.Text = objBene.BeneficiaryIDType.ToString()
            Else
                lblPrvBeneIDType.Text = "N/A"
            End If
            If txtUBPReferenceField.Text.ToString <> "" Then
                lblUBPReferencePrv.Text = objBene.UBPReferenceNo.ToString()
            Else
                lblUBPReferencePrv.Text = "N/A"
            End If
            If ddlUBPCompany.SelectedValue <> "0" Then
                lblUBPCompanyPrv.Text = ddlUBPCompany.SelectedItem.ToString
            Else
                lblUBPCompanyPrv.Text = "N/A"
            End If
        Else

            If txtBeneName.Text.ToString <> "" And txtBeneName.Text.ToString <> "Enter Beneficiary Name" Then
                lblprvBeneName.Text = txtBeneName.Text.ToString
            Else
                lblprvBeneName.Text = "N/A"
            End If
            If txtBankAccountNumber.Text.ToString <> "" And txtBankAccountNumber.Text.ToString <> "Enter Bank Account Number" Then
                lblprvBankAccountNo.Text = txtBankAccountNumber.Text.ToString
            Else
                lblprvBankAccountNo.Text = "N/A"
            End If
            If txtBeneAccountBranchCode.ToString <> "" And txtBeneAccountBranchCode.Text.ToString <> "Enter Account Branch Code" Then
                lblprvBeneAccountBranchCodePreview.Text = txtBeneAccountBranchCode.Text.ToString.ToUpper
            Else
                lblprvBeneAccountBranchCodePreview.Text = "N/A"
            End If
            If txtAccountTitle.Text.ToString <> "" And txtAccountTitle.Text.ToString <> "Enter Account Title" Then
                lblprvAccountTitle.Text = txtAccountTitle.Text.ToString
            Else
                lblprvAccountTitle.Text = "N/A"
            End If
            If txtBeneficiaryBranchAddress.Text.ToString <> "" Then
                lblprvBeneBranchAddress.Text = txtBeneficiaryBranchAddress.Text.ToString
            Else
                lblprvBeneBranchAddress.Text = "N/A"
            End If
            If ddlBeneBankName.SelectedValue.ToString <> "0" Then
                lblprvBankName.Text = ddlBeneBankName.SelectedItem.ToString
            Else
                lblprvBankName.Text = "N/A"
            End If
            If txtBeneAccountCurrency.Text.ToString <> "" And txtBeneAccountCurrency.Text.ToString <> "Enter Account Currency" Then
                lblprvBeneAccountCurrency.Text = txtBeneAccountCurrency.Text.ToString
            Else
                lblprvBeneAccountCurrency.Text = "N/A"
            End If
            If txtBeneMobileNo.Text.ToString <> "" And txtBeneMobileNo.Text.ToString <> "Enter Beneficiary Mobile No." Then
                lblprvMobNo.Text = txtBeneMobileNo.Text.ToString
            Else
                lblprvMobNo.Text = "N/A"
            End If
            If txtBenePhoneNo.Text.ToString <> "" And txtBenePhoneNo.Text.ToString <> "Enter Beneficiary Phone No." Then
                lblprvPhoneNo.Text = txtBenePhoneNo.Text.ToString
            Else
                lblprvPhoneNo.Text = "N/A"
            End If
            If txtBeneEmail.Text.ToString <> "" And txtBeneEmail.Text.ToString <> "Enter Email Address" Then
                lblprvEmail.Text = txtBeneEmail.Text.ToString
            Else
                lblprvEmail.Text = "N/A"
            End If
            If txtBeneCNCINo.Text.ToString <> "" And txtBeneCNCINo.Text.ToString <> "Enter CNIC No" Then
                lblprvNICNo.Text = txtBeneCNCINo.Text.ToString
            Else
                lblprvNICNo.Text = "N/A"
            End If
            If ddlBeneficiaryCity.SelectedValue.ToString <> "0" Then
                lblprvCity.Text = ddlBeneficiaryCity.SelectedItem.ToString
            Else
                lblprvCity.Text = "N/A"
            End If
            If ddlBeneficiaryProvince.SelectedValue.ToString <> "0" Then
                lblprvBeneficiaryProvince.Text = ddlBeneficiaryProvince.SelectedItem.ToString
            Else
                lblprvBeneficiaryProvince.Text = "N/A"
            End If
            If ddlBeneficiaryCountry.SelectedValue.ToString <> "0" Then
                lblprvBeneficiaryCountry.Text = ddlBeneficiaryCountry.SelectedItem.ToString
            Else
                lblprvBeneficiaryCountry.Text = "N/A"
            End If
            If txtBeneAddress1.Text.ToString <> "" And txtBeneAddress1.Text.ToString <> "Enter Address" Then
                lblprvAddress1.Text = txtBeneAddress1.Text.ToString
            Else
                lblprvAddress1.Text = "N/A"
            End If
            If txtBeneIDNo.Text.ToString <> "" Then
                lblPrvBeneIDNo.Text = txtBeneIDNo.Text.ToString
            Else
                lblPrvBeneIDNo.Text = "N/A"
            End If
            If ddlBeneIDType.SelectedValue <> "0" Then
                lblPrvBeneIDType.Text = ddlBeneIDType.SelectedValue.ToString()
            Else
                lblPrvBeneIDType.Text = "N/A"
            End If
            If txtUBPReferenceField.Text.ToString <> "" Then
                lblUBPReferencePrv.Text = txtUBPReferenceField.Text
            Else
                lblUBPReferencePrv.Text = "N/A"
            End If
            If ddlUBPCompany.SelectedValue <> "0" Then
                lblUBPCompanyPrv.Text = ddlUBPCompany.SelectedItem.ToString
            Else
                lblUBPCompanyPrv.Text = "N/A"
            End If
        End If


        If txtAmountPKR.Text.ToString <> "" And txtAmountPKR.Text.ToString <> "Enter Amount in PKR" Then
            lblprvAmount.Text = CDbl(txtAmountPKR.Text).ToString("N2")
        Else
            lblprvAmount.Text = "N/A"
        End If
        If txtAmountInWords.Text.ToString <> "" And txtAmountInWords.Text.ToString <> "Amount In Words" Then
            lblprvAmountInWord.Text = txtAmountInWords.Text.ToString
        Else
            lblprvAmountInWord.Text = "N/A"
        End If
        If ddlDDPayableLocationCity.SelectedValue.ToString <> "0" Then
            lblPrvDDDrawnLocationCity.Text = ddlDDPayableLocationCity.SelectedItem.ToString
        Else
            lblPrvDDDrawnLocationCity.Text = "N/A"
        End If
        If ddlPrintLocation.SelectedValue.ToString <> "0" Then
            lblprvPrintLocation.Text = ddlPrintLocation.SelectedItem.ToString
        Else
            lblprvPrintLocation.Text = "N/A"
        End If
        If txtInstrumentNo.Text.ToString <> "" And txtInstrumentNo.Text.ToString <> "Enter Instrument No." Then
            lblprvInstrumentNoPreview.Text = txtInstrumentNo.Text.ToString
        Else
            lblprvInstrumentNoPreview.Text = "N/A"
        End If
        If ddlDDPayableLocationProvince.SelectedValue.ToString <> "0" Then
            lblPrvDDDrawnLocationProvince.Text = ddlDDPayableLocationProvince.SelectedItem.ToString
        Else
            lblPrvDDDrawnLocationProvince.Text = "N/A"
        End If
        If txtDescription.Text.ToString <> "" And txtDescription.Text.ToString <> "Enter Description" Then
            ' lblDescription.Text = txtDescription.Text.ToString
            lblprvDescription.Text = txtDescription.Text.ToString
        Else
            ' lblDescription.Text = "N/A"
            lblprvDescription.Text = "N/A"
        End If
        If txtRemarks.Text.ToString <> "" And txtRemarks.Text.ToString <> "Enter Remarks" Then
            lblprvRemarks.Text = txtRemarks.Text.ToString
        Else
            lblprvRemarks.Text = "N/A"
        End If
        If txtReferenceField1.Text.ToString <> "" And txtReferenceField1.Text.ToString <> "Enter Reference Remarks" Then
            lblprvReferenceField.Text = txtReferenceField1.Text.ToString
        Else
            lblprvReferenceField.Text = "N/A"
        End If
        If txttrReferenceField2.Text.ToString <> "" And txttrReferenceField2.Text.ToString <> "Enter Reference Remarks" Then
            lblprvReferenceField2.Text = txttrReferenceField2.Text.ToString
        Else
            lblprvReferenceField2.Text = "N/A"
        End If
        If txtReferenceField3.Text.ToString <> "" And txtReferenceField3.Text.ToString <> "Enter Reference Remarks" Then
            lblprvReferenceField3Preview.Text = txtReferenceField3.Text.ToString
        Else
            lblprvReferenceField3Preview.Text = "N/A"
        End If
        If Not txtValueDate.SelectedDate Is Nothing Then
            lblprvValueDate.Text = txtValueDate.SelectedDate
        Else
            lblprvValueDate.Text = "N/A"
        End If
        If Not txtPrintDate.SelectedDate Is Nothing Then
            lblprvPrintDate.Text = txtPrintDate.SelectedDate
        Else
            lblprvPrintDate.Text = "N/A"
        End If

        btnSave.Visible = CBool(htRights("Add"))
    End Sub
    Private Sub SetPreviewforOldBene()
        Dim objICProduct As New ICProductType
        objICProduct.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)

        lblprvPaymentMode.Text = objICProduct.DisbursementMode.ToString

        If Not Session("dtBene") Is Nothing Then
            Dim dtBene As New DataTable
            dtBene = Session("dtBene")
            For Each dr As DataRow In dtBene.Rows

                If Not dr("BeneName") Is Nothing Then
                    lblprvBeneName.Text = dr("BeneName")
                Else
                    lblprvBeneName.Text = "N/A"
                End If
                If Not dr("BeneBranchCode") Is Nothing And Not dr("BeneBranchCode").ToString = "" Then
                    lblPrvPickLocation.Text = ddlBenePickLocation.selecteditem.text
                Else
                    lblPrvPickLocation.Text = "N/A"
                End If
                'If ddlBenePickLocation.SelectedValue.ToString <> "0" Then
                '    lblPrvPickLocation.Text = ddlBenePickLocation.SelectedItem.ToString
                'Else
                '    lblPrvPickLocation.Text = "N/A"
                'End If
                If Not dr("BankAccountNumber") Is Nothing Then
                    ' lblprvBankAccountNo.Text = dr("BankAccountNumber").ToString()
                    lblprvBankAccountNo.Text = txtBankAccountNumber.Text.ToString()
                Else
                    lblprvBankAccountNo.Text = "N/A"
                End If
                If ddlPrintLocation.SelectedValue.ToString <> "0" Then
                    lblprvPrintLocation.Text = ddlPrintLocation.SelectedItem.ToString
                Else
                    lblprvPrintLocation.Text = "N/A"
                End If
                If Not dr("BeneAccountBranchCode") Is Nothing Then
                    '  lblprvBeneAccountBranchCodePreview.Text = dr("BeneAccountBranchCode").ToString.ToUpper
                    lblprvBeneAccountBranchCodePreview.Text = txtBeneAccountBranchCode.Text.ToString.ToUpper
                Else
                    lblprvBeneAccountBranchCodePreview.Text = "N/A"
                End If
                If Not dr("BeneAccountCurrency") Is Nothing Then
                    ' lblprvBeneAccountCurrency.Text = dr("BeneAccountCurrency").ToString()
                    lblprvBeneAccountCurrency.Text = txtBeneAccountCurrency.Text.ToString()
                Else
                    lblprvBeneAccountCurrency.Text = "N/A"
                End If
                If Not dr("AccountTitle") Is Nothing Then
                    ' lblprvAccountTitle.Text = dr("AccountTitle").ToString()
                    lblprvAccountTitle.Text = txtAccountTitle.Text.ToString()
                Else
                    lblprvAccountTitle.Text = "N/A"
                End If
                If txtInstrumentNo.Text.ToString <> "" And txtInstrumentNo.Text.ToString <> "Enter Instrument No." Then
                    lblprvInstrumentNoPreview.Text = txtInstrumentNo.Text.ToString
                Else
                    lblprvInstrumentNoPreview.Text = "N/A"
                End If
                If Not dr("BeneficiaryBranchAddress") Is Nothing Then
                    lblprvBeneBranchAddress.Text = dr("BeneficiaryBranchAddress").ToString()
                Else
                    lblprvBeneBranchAddress.Text = "N/A"
                End If
                'If txtBankIMD.Text.ToString <> "" And txtBankIMD.Text.ToString <> "Enter Bank IMD" Then
                '    lblprvIMDNo.Text = txtBankIMD.Text.ToString
                'Else
                '    lblprvIMDNo.Text = "N/A"
                'End If
                If Not dr("BeneBankName") Is Nothing Then
                    lblprvBankName.Text = dr("BeneBankName").ToString()
                Else
                    lblprvBankName.Text = "N/A"
                End If
                If ddlDDPayableLocationCity.SelectedValue.ToString <> "0" Then
                    lblPrvDDDrawnLocationCity.Text = ddlDDPayableLocationCity.SelectedItem.ToString
                Else
                    lblPrvDDDrawnLocationCity.Text = "N/A"
                End If
                If ddlDDPayableLocationProvince.SelectedValue.ToString <> "0" Then
                    lblPrvDDDrawnLocationProvince.Text = ddlDDPayableLocationProvince.SelectedItem.ToString
                Else
                    lblPrvDDDrawnLocationProvince.Text = "N/A"
                End If

                'If txtBankAddress.Text.ToString <> "" And txtBankAddress.Text.ToString <> "Enter Bank Address" Then
                '    lblprvBeneBranchAddress.Text = txtBankAddress.Text.ToString
                'Else
                '    lblprvBeneBranchAddress.Text = "N/A"
                'End If
                If Not dr("BeneMobileNo") Is Nothing Then
                    lblprvMobNo.Text = dr("BeneMobileNo").ToString()
                Else
                    lblprvMobNo.Text = "N/A"
                End If
                If Not dr("BenePhoneNo") Is Nothing Then
                    lblprvPhoneNo.Text = dr("BenePhoneNo").ToString()
                Else
                    lblprvPhoneNo.Text = "N/A"
                End If
                If Not dr("BeneEmail") Is Nothing Then
                    lblprvEmail.Text = dr("BeneEmail").ToString()
                Else
                    lblprvEmail.Text = "N/A"
                End If
                If Not dr("BeneficiaryCity") Is Nothing Then
                    lblprvCity.Text = dr("BeneficiaryCity").ToString()
                Else
                    lblprvCity.Text = "N/A"
                End If
                If Not dr("BeneficiaryProvince") Is Nothing Then
                    lblprvBeneficiaryProvince.Text = dr("BeneficiaryProvince").ToString()
                Else
                    lblprvBeneficiaryProvince.Text = "N/A"
                End If
                If Not dr("BeneficiaryCountry") Is Nothing Then
                    lblprvBeneficiaryCountry.Text = dr("BeneficiaryCountry").ToString()
                Else
                    lblprvBeneficiaryCountry.Text = "N/A"
                End If
                If Not dr("BeneAddress1") Is Nothing Then
                    lblprvAddress1.Text = dr("BeneAddress1").ToString()
                Else
                    lblprvAddress1.Text = "N/A"
                End If
                'If txtBeneAddress2.Text.ToString <> "" And txtBeneAddress2.Text.ToString <> "Enter Address" Then
                '    lblprvAddress2.Text = txtBeneAddress2.Text.ToString
                'Else
                '    lblprvAddress2.Text = "N/A"
                'End If
                If txtAmountPKR.Text.ToString <> "" And txtAmountPKR.Text.ToString <> "Enter Amount in PKR" Then
                    lblprvAmount.Text = CDbl(txtAmountPKR.Text).ToString("N2")
                Else
                    lblprvAmount.Text = "N/A"
                End If
                If txtAmountInWords.Text.ToString <> "" And txtAmountInWords.Text.ToString <> "Amount In Words" Then
                    lblprvAmountInWord.Text = txtAmountInWords.Text.ToString
                Else
                    lblprvAmountInWord.Text = "N/A"
                End If
                If txtDescription.Text.ToString <> "" And txtDescription.Text.ToString <> "Enter Description" Then
                    lblprvDescription.Text = txtDescription.Text.ToString
                Else
                    lblprvDescription.Text = "N/A"
                End If
                If txtRemarks.Text.ToString <> "" And txtRemarks.Text.ToString <> "Enter Remarks" Then
                    lblprvRemarks.Text = txtRemarks.Text.ToString
                Else
                    lblprvRemarks.Text = "N/A"
                End If
                If txtReferenceField1.Text.ToString <> "" And txtReferenceField1.Text.ToString <> "Enter Reference Remarks" Then
                    lblprvReferenceField.Text = txtReferenceField1.Text.ToString
                Else
                    lblprvReferenceField.Text = "N/A"
                End If
                If txttrReferenceField2.Text.ToString <> "" And txttrReferenceField2.Text.ToString <> "Enter Reference Remarks" Then
                    lblprvReferenceField2.Text = txttrReferenceField2.Text.ToString
                Else
                    lblprvReferenceField2.Text = "N/A"
                End If
                If txtReferenceField3.Text.ToString <> "" And txtReferenceField3.Text.ToString <> "Enter Reference Remarks" Then
                    lblprvReferenceField3Preview.Text = txtReferenceField3.Text.ToString
                Else
                    lblprvReferenceField3Preview.Text = "N/A"
                End If
                If Not txtValueDate.SelectedDate Is Nothing Then
                    lblprvValueDate.Text = txtValueDate.SelectedDate
                Else
                    lblprvValueDate.Text = "N/A"
                End If
                If Not txtPrintDate.SelectedDate Is Nothing Then
                    lblprvPrintDate.Text = txtPrintDate.SelectedDate
                Else
                    lblprvPrintDate.Text = "N/A"
                End If
                If txtBeneIDNo.Text.ToString <> "" Then
                    lblPrvBeneIDNo.Text = dr("BeneIDNo").ToString()
                Else
                    lblPrvBeneIDNo.Text = "N/A"
                End If
                If ddlBeneIDType.SelectedValue <> "0" Then
                    lblPrvBeneIDType.Text = dr("BeneIDType").ToString()
                Else
                    lblPrvBeneIDType.Text = "N/A"
                End If
                If ddlUBPCompany.SelectedValue.ToString <> "0" Then
                    lblUBPCompanyPrv.Text = ddlUBPCompany.SelectedItem.ToString
                Else
                    lblUBPCompanyPrv.Text = "N/A"
                End If
                If txtUBPReferenceField.Text.ToString <> "" Then
                    lblUBPReferencePrv.Text = dr("UBPSRefNo")
                Else
                    lblUBPReferencePrv.Text = "N/A"
                End If
            Next
        End If
        btnSave.Visible = CBool(htRights("Add"))
    End Sub
    Private Sub LoadddlBeneficiary(ByVal IsOpenPaymentAllowed As Boolean, ByVal BeneGroupCode As String)
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBeneficiary.Items.Clear()
            ddlBeneficiary.Items.Add(lit)
            ddlBeneficiary.AppendDataBoundItems = True

            Dim collBene As New ICBeneCollection
            Dim dtBene As New DataTable

            dtBene = ICBeneficiaryManagementController.GetAllActiveandApprovedBeneByBeneGroupCodeandTaggedProductType(BeneGroupCode, ddlProductType.SelectedValue.ToString())

            If IsOpenPaymentAllowed = True Then
                Dim dt As New DataTable
                Dim dr As DataRow
                dt.Columns.Add("BeneficiaryName")
                dt.Columns.Add("BeneID")
                For i As Integer = 0 To 1
                    If i = 0 Then
                        dr = dt.NewRow()
                        dr("BeneficiaryName") = "New Beneficiary"
                        dr("BeneID") = "-1"          '' BeneID may be 1 that's why 01 for Unique 
                        dt.Rows.Add(dr)
                    Else
                        For Each dr1 As DataRow In dtBene.Rows
                            dr = dt.NewRow()
                            dr("BeneficiaryName") = dr1("BeneficiaryName") & " - " & dr1("BeneficiaryCode")
                            dr("BeneID") = dr1("BeneID")
                            dt.Rows.Add(dr)
                        Next
                    End If
                Next
                dtBene = New DataTable
                dtBene = dt.Copy
            End If

            ddlBeneficiary.DataSource = dtBene
            ddlBeneficiary.DataTextField = "BeneficiaryName"
            ddlBeneficiary.DataValueField = "BeneID"
            ddlBeneficiary.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub ClearOnlineForm()
        txtBeneName.Text = Nothing
        txtBankAccountNumber.Text = Nothing
        ddlPrintLocation.ClearSelection()
        txtAccountTitle.Text = Nothing
        txtBenePhoneNo.Text = Nothing
        txtBeneMobileNo.Text = Nothing
        txtBeneEmail.Text = Nothing
        txtBeneCNCINo.Text = Nothing
        txtBeneIDNo.Text = Nothing
        txtBeneficiaryBranchAddress.Text = Nothing
        ddlBeneficiaryCity.ClearSelection()
        ddlBeneficiaryProvince.ClearSelection()
        ddlBeneficiaryCountry.ClearSelection()
        ddlBenePickLocation.ClearSelection()
        ddlBeneIDType.ClearSelection()
        ddlUBPCompany.ClearSelection()
        LoadddlDDDrawnLocationProvince()
        LoadddlDDDrawnLocationCity()
        LoadddlBeneBank()
        ''B.P
        LoadddlUBPCompany()
        'LoadddlClientOfficeByUserID()
        txtValueDate.SelectedDate = Nothing
        txtBeneAddress1.Text = Nothing
        txtBeneAccountBranchCode.Text = Nothing
        txtBeneAccountCurrency.Text = Nothing
        'txtBeneAddress2.Text = ""
        txtAmountPKR.Text = Nothing
        txtAmountInWords.Text = Nothing
        txtDescription.Text = Nothing
        txtDescription.Text = Nothing
        txtRemarks.Text = Nothing
        txtReferenceField1.Text = Nothing
        txttrReferenceField2.Text = Nothing
        txtReferenceField3.Text = Nothing
        txtInstrumentNo.Text = Nothing
        txtPrintDate.SelectedDate = Nothing
        ''B.P
        txtUBPReferenceField.Text = Nothing
        radBeneName.Enabled = False
        radBeneIDNo.Enabled = False
        ''schedule settings
        radScheduleTitle.Enabled = False

        radNoOfDay.Enabled = False
        rfvScheduleEndDate.Enabled = False
        rfvScheduleStartDate.Enabled = False
        rfvTransactionTime.Enabled = False
        rfvrblrblDaysOfMonth.Enabled = False
        rfvBeneIDType.Enabled = False
        cmpSheduleDates.Enabled = False


        radBankAccountNumber.Enabled = False
        radBeneAccountBranchCode.Enabled = False
        radBeneAccountCurrency.Enabled = False
        radAccountTitle.Enabled = False
        radBenePhoneNo.Enabled = False
        radBeneMobNo.Enabled = False
        radBeneEmail.Enabled = False
        radBeneCNICNo.Enabled = False
        radAmountPKR.Enabled = False
        radAmountInWords.Enabled = False
        radDescription.Enabled = False
        radRemarks.Enabled = False
        radReferenceField1.Enabled = False
        radReferenceField3.Enabled = False
        radtrReferenceField2.Enabled = False
        radBeneBranchAddress.Enabled = False
        radBeneAddress1.Enabled = False
        radInstrumentNo.Enabled = False
        ''B.P
        radUBPReferenceField.Enabled = False
        rfvddlPrintLocation.Enabled = False
        rfvddlDDPayableLocationCity.Enabled = False
        rfvddlDDPayableLocationProvince.Enabled = False
        rfvPrintDate.Enabled = False
        rfvValueDate.Enabled = False

        rfvddlBeneficiaryCity.Enabled = False
        rfvddlBeneficiaryCountry.Enabled = False
        rfvddlBeneficiaryProvince.Enabled = False
        radBeneAddress1.Enabled = False
        rfvddlBeneBank.Enabled = False
        rfvddlScheduleFrequency.Enabled = False
        rfvddlSpecificDay.Enabled = False
        rfvddlBenePickLocation.Enabled = False

        ''Bill Payment Work
        rfvUBPCompany.Enabled = False




        'radBankBranchCode.Enabled = False

        'radBeneBankCode.Enabled = False
        'radBankAddress.Enabled = False
        rfvGLAccountBranchCode.Enabled = False
        'radBankBranchCode.Enabled = False
        DirectCast(Me.Control.FindControl("trGLAccountBranchCode"), HtmlTableRow).Style.Add("display", "none")

        lblReqGLAccountBranchCode.Visible = False



    End Sub
    Private Sub ClearPreview()
        lblprvBeneName.Text = ""
        lblprvBankAccountNo.Text = ""
        lblprvPrintLocation.Text = ""
        lblPrvDDDrawnLocationProvince.Text = ""
        lblprvAccountTitle.Text = ""
        'lblprvIMDNo.Text = ""
        lblprvBankName.Text = ""
        lblprvBeneBranchAddress.Text = ""
        lblprvBeneBankBranchName.Text = ""
        lblPrvDDDrawnLocationCity.Text = ""
        'lblprvClientBankBranchName.Text = ""
        'lblprvClientBankName.Text = ""
        lblprvMobNo.Text = ""
        lblprvPhoneNo.Text = ""
        lblprvEmail.Text = ""
        lblprvAmount.Text = ""
        lblprvNICNo.Text = ""
        lblprvBeneficiaryProvince.Text = ""
        lblprvBeneficiaryCountry.Text = ""
        lblprvCity.Text = ""
        lblprvAddress1.Text = ""
        lblPrvDDDrawnLocationCity.Text = ""
        lblPrvDDDrawnLocationProvince.Text = ""
        'lblprvAddress2.Text = ""
        lblprvAmount.Text = ""
        lblprvAmountInWord.Text = ""
        lblprvPaymentMode.Text = ""
        lblprvDescription.Text = ""
        lblprvReferenceField.Text = ""
        lblprvReferenceField2.Text = ""
        lblprvReferenceField3Preview.Text = ""
        lblprvValueDate.Text = ""
        lblprvInstrumentNoPreview.Text = ""
        lblprvRemarks.Text = ""
        lblPrvBeneIDType.Text = ""
        lblPrvBeneIDNo.Text = ""

        lblprvScheduleTitle.Text = ""
        lblprvScheduleFrequency.Text = ""
        lblprvTransactionTime.Text = ""
        lblprvDayType.Text = ""
        lblprvScheduleStartDate.Text = ""
        lblprvScheduleEndDate.Text = ""
        lblPrvPickLocation.Text = ""
        lblUBPCompanyPrv.Text = ""
        lblUBPReferencePrv.Text = ""
        lblUBPReferenceField.Text = "UBP Reference"
        lblprvBeneficiaryAccountType.Text = ""
    End Sub
    Private Sub SetPreview()
        Dim objICProduct As New ICProductType
        Dim objICUBPCompany As New ICUBPSCompany
        objICProduct.es.Connection.CommandTimeout = 3600


        objICProduct.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)


        lblprvPaymentMode.Text = objICProduct.DisbursementMode.ToString

        If txtBeneName.Text.ToString <> "" And txtBeneName.Text.ToString <> "Enter Beneficiary Name" Then
            lblprvBeneName.Text = txtBeneName.Text.ToString
        Else
            lblprvBeneName.Text = "N/A"
        End If
        If txtBankAccountNumber.Text.ToString <> "" And txtBankAccountNumber.Text.ToString <> "Enter Bank Account Number" Then
            lblprvBankAccountNo.Text = txtBankAccountNumber.Text.ToString
        Else
            lblprvBankAccountNo.Text = "N/A"
        End If
        If ddlPrintLocation.SelectedValue.ToString <> "0" Then
            lblprvPrintLocation.Text = ddlPrintLocation.SelectedItem.ToString
        Else
            lblprvPrintLocation.Text = "N/A"
        End If
        If txtBeneAccountBranchCode.Text.ToString <> "" And txtBeneAccountBranchCode.Text.ToString <> "Enter Account Branch Code" Then
            lblprvBeneAccountBranchCodePreview.Text = txtBeneAccountBranchCode.Text.ToString.ToUpper
        ElseIf ddlGLAccountBranchCode.SelectedValue.ToString <> "0" Then
            lblprvBeneAccountBranchCodePreview.Text = ddlGLAccountBranchCode.SelectedValue.ToString.ToUpper
        Else
            lblprvBeneAccountBranchCodePreview.Text = "N/A"
        End If
        If txtAccountTitle.Text.ToString <> "" And txtAccountTitle.Text.ToString <> "Enter Account Title" Then
            lblprvAccountTitle.Text = txtAccountTitle.Text.ToString
        Else
            lblprvAccountTitle.Text = "N/A"
        End If
        If txtInstrumentNo.Text.ToString <> "" And txtInstrumentNo.Text.ToString <> "Enter Instrument No." Then
            lblprvInstrumentNoPreview.Text = txtInstrumentNo.Text.ToString
        Else
            lblprvInstrumentNoPreview.Text = "N/A"
        End If
        If txtBeneficiaryBranchAddress.Text.ToString <> "" Then
            lblprvBeneBranchAddress.Text = txtBeneficiaryBranchAddress.Text.ToString
        Else
            lblprvBeneBranchAddress.Text = "N/A"
        End If
        'If txtBankIMD.Text.ToString <> "" And txtBankIMD.Text.ToString <> "Enter Bank IMD" Then
        '    lblprvIMDNo.Text = txtBankIMD.Text.ToString
        'Else
        '    lblprvIMDNo.Text = "N/A"
        'End If
        If ddlBeneBankName.SelectedValue.ToString <> "0" Then
            lblprvBankName.Text = ddlBeneBankName.SelectedItem.ToString
        Else
            lblprvBankName.Text = "N/A"
        End If
        If ddlBeneIDType.SelectedValue.ToString <> "0" Then
            lblPrvBeneIDType.Text = ddlBeneIDType.SelectedItem.ToString
        Else
            lblPrvBeneIDType.Text = "N/A"
        End If
        If ddlBenePickLocation.SelectedValue.ToString <> "0" Then
            lblPrvPickLocation.Text = ddlBenePickLocation.SelectedItem.ToString
        Else
            lblPrvPickLocation.Text = "N/A"
        End If
        If ddlDDPayableLocationCity.SelectedValue.ToString <> "0" Then
            lblPrvDDDrawnLocationCity.Text = ddlDDPayableLocationCity.SelectedItem.ToString
        Else
            lblPrvDDDrawnLocationCity.Text = "N/A"
        End If
        If ddlDDPayableLocationProvince.SelectedValue.ToString <> "0" Then
            lblPrvDDDrawnLocationProvince.Text = ddlDDPayableLocationProvince.SelectedItem.ToString
        Else
            lblPrvDDDrawnLocationProvince.Text = "N/A"
        End If
        If txtBeneAccountCurrency.Text.ToString <> "" And txtBeneAccountCurrency.Text.ToString <> "Enter Account Currency" Then
            lblprvBeneAccountCurrency.Text = txtBeneAccountCurrency.Text.ToString
        Else
            lblprvBeneAccountCurrency.Text = "N/A"
        End If
        If ddlBeneCBAccountType.SelectedValue.ToString = "0" Then
            lblprvBeneficiaryAccountType.Text = "RB"
        ElseIf ddlBeneCBAccountType.SelectedValue.ToString <> "0" Then
            lblprvBeneficiaryAccountType.Text = ddlBeneCBAccountType.SelectedValue.ToString
        Else
            lblprvBeneficiaryAccountType.Text = "N/A"
        End If
        'If txtBankAddress.Text.ToString <> "" And txtBankAddress.Text.ToString <> "Enter Bank Address" Then
        '    lblprvBeneBranchAddress.Text = txtBankAddress.Text.ToString
        'Else
        '    lblprvBeneBranchAddress.Text = "N/A"
        'End If
        If txtBeneIDNo.Text <> "" Then
            lblPrvBeneIDNo.Text = txtBeneIDNo.Text
        Else
            lblPrvBeneIDNo.Text = "N/A"
        End If
        If txtBeneMobileNo.Text.ToString <> "" And txtBeneMobileNo.Text.ToString <> "Enter Beneficiary Mobile No." Then
            lblprvMobNo.Text = txtBeneMobileNo.Text.ToString
        Else
            lblprvMobNo.Text = "N/A"
        End If
        If txtBenePhoneNo.Text.ToString <> "" And txtBenePhoneNo.Text.ToString <> "Enter Beneficiary Phone No." Then
            lblprvPhoneNo.Text = txtBenePhoneNo.Text.ToString
        Else
            lblprvPhoneNo.Text = "N/A"
        End If
        If txtBeneEmail.Text.ToString <> "" And txtBeneEmail.Text.ToString <> "Enter Email Address" Then
            lblprvEmail.Text = txtBeneEmail.Text.ToString
        Else
            lblprvEmail.Text = "N/A"
        End If
        If txtBeneCNCINo.Text.ToString <> "" And txtBeneCNCINo.Text.ToString <> "Enter CNIC No" Then
            lblprvNICNo.Text = txtBeneCNCINo.Text.ToString
        Else
            lblprvNICNo.Text = "N/A"
        End If
        If ddlBeneficiaryCity.SelectedValue.ToString <> "0" Then
            lblprvCity.Text = ddlBeneficiaryCity.SelectedItem.ToString
        Else
            lblprvCity.Text = "N/A"
        End If
        If ddlBeneficiaryProvince.SelectedValue.ToString <> "0" Then
            lblprvBeneficiaryProvince.Text = ddlBeneficiaryProvince.SelectedItem.ToString
        Else
            lblprvBeneficiaryProvince.Text = "N/A"
        End If
        If ddlBeneficiaryCountry.SelectedValue.ToString <> "0" Then
            lblprvBeneficiaryCountry.Text = ddlBeneficiaryCountry.SelectedItem.ToString
        Else
            lblprvBeneficiaryCountry.Text = "N/A"
        End If
        If txtBeneAddress1.Text.ToString <> "" And txtBeneAddress1.Text.ToString <> "Enter Address" Then
            lblprvAddress1.Text = txtBeneAddress1.Text.ToString
        Else
            lblprvAddress1.Text = "N/A"
        End If
        'If txtBeneAddress2.Text.ToString <> "" And txtBeneAddress2.Text.ToString <> "Enter Address" Then
        '    lblprvAddress2.Text = txtBeneAddress2.Text.ToString
        'Else
        '    lblprvAddress2.Text = "N/A"
        'End If
        If txtAmountPKR.Text.ToString <> "" And txtAmountPKR.Text.ToString <> "Enter Amount in PKR" Then
            lblprvAmount.Text = CDbl(txtAmountPKR.Text).ToString("N2")

        Else
            lblprvAmount.Text = "N/A"
        End If
        If txtAmountInWords.Text.ToString <> "" And txtAmountInWords.Text.ToString <> "Amount In Words" Then
            lblprvAmountInWord.Text = txtAmountInWords.Text.ToString
        Else
            lblprvAmountInWord.Text = "N/A"
        End If
        If txtDescription.Text.ToString <> "" And txtDescription.Text.ToString <> "Enter Description" Then
            lblprvDescription.Text = txtDescription.Text.ToString
        Else
            lblprvDescription.Text = "N/A"
        End If
        If txtRemarks.Text.ToString <> "" And txtRemarks.Text.ToString <> "Enter Remarks" Then
            lblprvRemarks.Text = txtRemarks.Text.ToString
        Else
            lblprvRemarks.Text = "N/A"
        End If
        If txtReferenceField1.Text.ToString <> "" And txtReferenceField1.Text.ToString <> "Enter Reference Remarks" Then
            lblprvReferenceField.Text = txtReferenceField1.Text.ToString
        Else
            lblprvReferenceField.Text = "N/A"
        End If
        If txttrReferenceField2.Text.ToString <> "" And txttrReferenceField2.Text.ToString <> "Enter Reference Remarks" Then
            lblprvReferenceField2.Text = txttrReferenceField2.Text.ToString
        Else
            lblprvReferenceField2.Text = "N/A"
        End If
        If txtReferenceField3.Text.ToString <> "" And txtReferenceField3.Text.ToString <> "Enter Reference Remarks" Then
            lblprvReferenceField3Preview.Text = txtReferenceField3.Text.ToString
        Else
            lblprvReferenceField3Preview.Text = "N/A"
        End If
        If Not txtPrintDate.SelectedDate Is Nothing Then
            lblprvPrintDate.Text = txtPrintDate.SelectedDate
        Else
            lblprvPrintDate.Text = "N/A"
        End If
        If rbScheduleTransaction.Checked = True Then
            tblSchedulePreview.Style.Remove("display")
            If txtScheduleTitle.Text <> "" Then
                lblprvScheduleTitle.Text = txtScheduleTitle.Text
            Else
                lblprvScheduleTitle.Text = "N/A"
            End If

            If ddlScheduleFrequency.SelectedValue.ToString = "Daily" Then
                lblprvScheduleFrequency.Text = "Daily"
                lblprvDayType.Text = ""
            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Monthly" Then
                lblprvScheduleFrequency.Text = "Monthly"
                If rblDaysOfMonth.SelectedValue.ToString = "Specific Day" Then
                    lblprvDayType.Text = "Specific Day : " & ddlSpecificDay.SelectedValue.ToString
                ElseIf rblDaysOfMonth.SelectedValue.ToString = "First Day" Then
                    lblprvDayType.Text = "First Day"
                ElseIf rblDaysOfMonth.SelectedValue.ToString = "Last Day" Then
                    lblprvDayType.Text = "Last Day"
                End If

            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Number Of Days" Then
                lblprvScheduleFrequency.Text = "Number Of Days"
                lblprvDayType.Text = "Number Of Days : " & txtNoOfDays.Text
            End If
            If Not radTimePicker.SelectedDate Is Nothing Then
                lblprvTransactionTime.Text = CDate(radTimePicker.SelectedDate).ToString("hh:mm tt")
            Else
                lblprvTransactionTime.Text = "N/A"
            End If
            If Not txtStartDate.SelectedDate Is Nothing Then
                lblprvScheduleStartDate.Text = CDate(txtStartDate.SelectedDate).ToString("dd-MM-yyyy")
            Else
                lblprvScheduleStartDate.Text = "N/A"
            End If
            If Not txtEndDate.SelectedDate Is Nothing Then
                lblprvScheduleEndDate.Text = CDate(txtEndDate.SelectedDate).ToString("dd-MM-yyyy")
            Else
                lblprvScheduleEndDate.Text = "N/A"
            End If
            If DirectCast(Me.Control.FindControl("trValueDatePreview"), HtmlTableRow).Visible = True Then
                DirectCast(Me.Control.FindControl("trValueDatePreview"), HtmlTableRow).Style.Add("display", "none")

                lblprvValueDate.Text = "N/A"

            End If

        Else
            tblSchedulePreview.Style.Add("display", "none")
            lblprvScheduleTitle.Text = ""
            lblprvScheduleFrequency.Text = ""
            lblprvTransactionTime.Text = ""
            lblprvDayType.Text = ""
            lblprvScheduleStartDate.Text = ""
            lblprvScheduleEndDate.Text = ""
            If Not txtValueDate.SelectedDate Is Nothing Then
                lblprvValueDate.Text = txtValueDate.SelectedDate
            Else
                lblprvValueDate.Text = "N/A"
            End If
        End If
        If ddlUBPCompany.SelectedValue.ToString = "0" Then
            lblUBPCompanyPrv.Text = "N/A"
        Else
            lblUBPCompanyPrv.Text = ddlUBPCompany.SelectedItem.ToString
        End If
        If txtUBPReferenceField.Text <> "" Then
            lblUBPReferencePrv.Text = txtUBPReferenceField.Text
        Else
            lblUBPReferencePrv.Text = "N/A"
        End If
        lblUBPReferenceFieldPreview.Text = lblUBPReferenceField.Text
        btnSave.Visible = CBool(htRights("Add"))
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try

                Dim objICInstruction As New ICInstruction
                Dim objICScheduleInstruction As New ICScheduleTransactions
                Dim dtBestPossibleDate As New DataTable
                Dim StrBestPossible As String
                Dim ScheduleID As Integer = 0
                Dim objICCity As New ICCity
                Dim objICProvince As New ICProvince
                Dim objICCountry As New ICCountry
                Dim objICUser As New ICUser
                Dim objICProductType As New ICProductType
                Dim ActionStr As String = ""
                Dim objICBeneficiary As New ICBeneficiary
                Dim objICCompany As New ICCompany
                Dim StrAccountDetails As String()
                Dim EmptyAccountDetails As String = Nothing
                Dim Count As Integer


                objICUser.LoadByPrimaryKey(Me.UserId.ToString)
                objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)
                objICCompany.LoadByPrimaryKey(ddlCompany.SelectedValue.ToString)
                If objICProductType.DisbursementMode = "Direct Credit" Or objICProductType.DisbursementMode = "Other Credit" Then
                    If txtBeneName.Text <> "" Then
                        objICInstruction.BeneficiaryName = txtBeneName.Text
                    Else
                        If txtAccountTitle.Text <> "" Then
                            objICInstruction.BeneficiaryName = txtAccountTitle.Text
                        Else
                            objICInstruction.BeneficiaryName = txtBeneName.Text
                        End If
                    End If
                    If ddlBeneCBAccountType.SelectedValue.ToString <> "0" And ddlBeneCBAccountType.SelectedValue.ToString <> "" Then
                        objICInstruction.BeneficaryAccountType = ddlBeneCBAccountType.SelectedValue.ToString.ToUpper

                    Else
                        objICInstruction.BeneficaryAccountType = "RB"
                    End If
                Else
                    objICInstruction.BeneficiaryName = txtBeneName.Text
                    If ddlBeneCBAccountType.SelectedValue.ToString <> "0" And ddlBeneCBAccountType.SelectedValue.ToString <> "" Then
                        objICInstruction.BeneficaryAccountType = ddlBeneCBAccountType.SelectedValue.ToString.ToUpper
                    Else
                        objICInstruction.BeneficaryAccountType = Nothing
                    End If
                End If


                objICInstruction.BeneficiaryAccountNo = txtBankAccountNumber.Text
                objICInstruction.BeneAccountBranchCode = txtBeneAccountBranchCode.Text
                objICInstruction.BeneAccountCurrency = txtBeneAccountCurrency.Text
                objICInstruction.BeneficiaryAccountTitle = txtAccountTitle.Text


                ''COTC Work

                If ddlBenePickLocation.SelectedValue.ToString <> "0" Then
                    objICInstruction.BeneficiaryBranchCode = ddlBenePickLocation.SelectedValue.ToString
                End If
                If ddlBeneIDType.SelectedValue.ToString <> "0" Then
                    objICInstruction.BeneficiaryIDType = ddlBeneIDType.SelectedValue.ToString
                End If
                objICInstruction.BeneficiaryIDNo = txtBeneIDNo.Text


                If ddlDDPayableLocationCity.SelectedValue.ToString <> "0" Then
                    objICInstruction.DDPayableLocation = ddlDDPayableLocationCity.SelectedItem.ToString
                    objICInstruction.DDDrawnCityCode = ddlDDPayableLocationCity.SelectedValue.ToString
                End If
                If ddlPrintLocation.SelectedValue.ToString <> "0" Then
                    objICInstruction.PrintLocationName = ddlPrintLocation.SelectedItem.ToString.Split(" - ")(1)
                    objICInstruction.PrintLocationCode = ddlPrintLocation.SelectedValue.ToString
                End If
                'objICInstruction.BeneficiaryBranchCode = txtBankBranchCode.Text.ToString

                'objICInstruction.BeneficiaryBankCode = txtBeneBankCode.Text.ToString
                'objICInstruction.BeneficiaryBankAddress = txtBankAddress.Text.ToString
                objICInstruction.BeneficiaryPhone = txtBenePhoneNo.Text
                objICInstruction.BeneficiaryMobile = txtBeneMobileNo.Text
                objICInstruction.BeneficiaryCNIC = txtBeneCNCINo.Text
                objICInstruction.BeneficiaryEmail = txtBeneEmail.Text
                objICInstruction.ClientName = objICCompany.CompanyName
                objICInstruction.ClientEmail = objICCompany.EmailAddress
                objICInstruction.ClientMobile = objICCompany.PhoneNumber2
                objICInstruction.ClientAddress = objICCompany.Address
                objICInstruction.CreatedOfficeCode = objICUser.UpToICOfficeByOfficeCode.OfficeID
                If ddlBeneBankName.SelectedValue.ToString <> "0" Then
                    objICInstruction.BeneficiaryBankName = ddlBeneBankName.SelectedItem.ToString
                    objICInstruction.BeneficiaryBankCode = ddlBeneBankName.SelectedValue.ToString
                    objICBeneficiary.BankName = ddlBeneBankName.SelectedItem.ToString
                End If
                If ddlBeneficiaryCity.SelectedValue.ToString <> "0" Then
                    objICInstruction.BeneficiaryCity = ddlBeneficiaryCity.SelectedValue.ToString
                    objICBeneficiary.BeneCity = ddlBeneficiaryCity.SelectedItem.Text.ToString
                End If
                If ddlBeneficiaryProvince.SelectedValue.ToString <> "0" Then
                    objICInstruction.BeneficiaryProvince = ddlBeneficiaryProvince.SelectedValue.ToString
                    objICBeneficiary.BeneProvince = ddlBeneficiaryProvince.SelectedItem.Text.ToString

                End If
                If ddlBeneficiaryCountry.SelectedValue.ToString <> "0" Then
                    objICInstruction.BeneficiaryCountry = ddlBeneficiaryCountry.SelectedValue.ToString

                End If
                objICInstruction.BeneficiaryAddress = txtBeneAddress1.Text
                objICInstruction.BeneBranchAddress = txtBeneficiaryBranchAddress.Text
                If txtAmountPKR.Text <> "" Then
                    objICInstruction.Amount = CDbl(txtAmountPKR.Text)
                    objICInstruction.AmountInWords = txtAmountInWords.Text
                Else
                    objICInstruction.Amount = 0
                    objICInstruction.AmountInWords = NumToWord.SpellNumber(0).TrimStart().TrimEnd().Trim().ToString()
                End If


                objICInstruction.Description = txtDescription.Text
                objICInstruction.Remarks = txtRemarks.Text
                objICInstruction.ReferenceField1 = txtReferenceField1.Text
                objICInstruction.ReferenceField2 = txttrReferenceField2.Text
                objICInstruction.ReferenceField3 = txtReferenceField3.Text
                objICInstruction.ClientCity = objICUser.UpToICOfficeByOfficeCode.UpToICCityByCityID.CityCode.ToString
                objICInstruction.ClientProvince = objICUser.UpToICOfficeByOfficeCode.UpToICCityByCityID.UpToICProvinceByProvinceCode.ProvinceCode.ToString
                objICInstruction.ClientCountry = objICUser.UpToICOfficeByOfficeCode.UpToICCityByCityID.UpToICProvinceByProvinceCode.UpToICCountryByCountryCode.CountryCode.ToString
                objICInstruction.ClientAccountNo = ddlAccPNature.SelectedValue.ToString.Split("-")(0).ToString
                objICInstruction.ClientAccountBranchCode = ddlAccPNature.SelectedValue.ToString.Split("-")(1).ToString
                objICInstruction.ClientAccountCurrency = ddlAccPNature.SelectedValue.ToString.Split("-")(2).ToString
                objICInstruction.PaymentNatureCode = ddlAccPNature.SelectedValue.ToString.Split("-")(3).ToString
                objICInstruction.ProductTypeCode = ddlProductType.SelectedValue
                objICInstruction.PaymentMode = objICProductType.DisbursementMode
                objICInstruction.AcquisitionMode = "Online Form"
                objICInstruction.CompanyCode = ddlCompany.SelectedValue
                objICInstruction.GroupCode = ddlGroup.SelectedValue
                objICInstruction.InstrumentNo = txtInstrumentNo.Text
                objICInstruction.UBPCompanyID = ddlUBPCompany.SelectedValue
                objICInstruction.UBPReferenceField = txtUBPReferenceField.Text

                If Not txtValueDate.SelectedDate Is Nothing Then
                    objICInstruction.ValueDate = txtValueDate.SelectedDate
                End If
                If Not txtPrintDate.SelectedDate Is Nothing Then
                    objICInstruction.PrintDate = txtPrintDate.SelectedDate
                End If
                objICInstruction.FileBatchNo = "SI"
                objICInstruction.Status = 1
                objICInstruction.LastStatus = 1
                objICInstruction.CreateBy = Me.UserId
                objICInstruction.CreateDate = Date.Now
                ActionStr += "Instruction  via online form for beneficiary [ " & objICInstruction.BeneficiaryName.ToString & " ] of client [ " & ddlCompany.SelectedItem.ToString & " ], "
                ActionStr += "of amount [ " & objICInstruction.Amount.ToString & "] [ " & objICInstruction.AmountInWords.ToString & " ] added. Client account number was [ " & ddlAccPNature.SelectedValue.ToString.Split("-")(0).ToString & " ],"
                ActionStr += " branch code [" & ddlAccPNature.SelectedValue.ToString.Split("-")(1).ToString & " ], currency [" & ddlAccPNature.SelectedValue.ToString.Split("-")(2) & " ]."
                ActionStr += " Product type [ " & objICProductType.ProductTypeName & " ], Disbursement mode [ " & objICProductType.DisbursementMode & " ], Payment nature [ " & ddlAccPNature.SelectedValue.Split("-")(3).ToString & " ]."

                ''Beneficiary Region 

                objICBeneficiary.BeneName = txtBeneName.Text
                objICBeneficiary.BeneEmail = txtBeneEmail.Text
                objICBeneficiary.BeneCellNumber = txtBeneMobileNo.Text
                objICBeneficiary.BeneIDNO = txtBeneCNCINo.Text
                objICBeneficiary.BeneAddress1 = txtBeneAddress1.Text
                objICBeneficiary.BeneAccountNumber = txtBankAccountNumber.Text
                objICBeneficiary.IsActive = True
                objICBeneficiary.CreateBy = Me.UserId
                objICBeneficiary.CreateDate = Date.Now


                '' Add Beneficiary and Instruction


                If rbScheduleTransaction.Checked = True Then
                    If ddlScheduleFrequency.SelectedValue.ToString = "Monthly" Then
                        If rblDaysOfMonth.SelectedValue.ToString = "First Day" Then
                            StrBestPossible = ICScheduleInstructionController.GetBestPossibleDay(CDate(txtStartDate.SelectedDate), "First Day", "")
                            dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, StrBestPossible, "First Day")
                        ElseIf rblDaysOfMonth.SelectedValue.ToString = "Last Day" Then
                            Dim MonthInterval As Integer = DateDiff(DateInterval.Month, CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate))
                            StrBestPossible = MonthInterval.ToString
                            dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, StrBestPossible, "Last Day")
                        ElseIf rblDaysOfMonth.SelectedValue.ToString = "Specific Day" Then
                            StrBestPossible = ICScheduleInstructionController.GetBestPossibleDay(CDate(txtStartDate.SelectedDate), "Specific Day", ddlSpecificDay.SelectedValue.ToString)
                            dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, StrBestPossible, "Specific Day")
                        End If
                    ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Daily" Then
                        dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, "", "")
                    ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Number Of Days" Then
                        StrBestPossible = ICScheduleInstructionController.GetBestPossibleDay(CDate(txtStartDate.SelectedDate), "Number Of Days", txtNoOfDays.Text.ToString)
                        dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, StrBestPossible, "")
                    End If

                    If dtBestPossibleDate.Rows.Count > 0 Then
                        objICScheduleInstruction = GetScheduleTransaction(objICInstruction, dtBestPossibleDate.Rows.Count)
                        ScheduleID = ICScheduleInstructionController.AddScheduleInstruction(objICScheduleInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        For Each drScheduleInstruction As DataRow In dtBestPossibleDate.Rows
                            objICInstruction.ScheduleTransactionID = ScheduleID
                            objICInstruction.ScheduleTransactionTime = objICScheduleInstruction.TransactionTime
                            objICInstruction.IsCheduleTransactionProcessed = False
                            objICInstruction.ValueDate = CDate(drScheduleInstruction("ValueDates"))

                            '' Add Beneficiary and Instruction
                            Dim objCompany As New ICCompany
                            Dim objICbenedata As New ICBene

                            If objCompany.LoadByPrimaryKey(objICInstruction.CompanyCode) Then
                                If objCompany.IsBeneRequired = True And objCompany.IsAllowOpenPayment = True Then
                                    Dim BeneDuplicateRslt As String = ""
                                    Dim objBene As New ICBene
                                    Dim objProductType As New ICProductType

                                    If ddlBeneficiary.SelectedValue.ToString <> "0" And ddlBeneficiary.SelectedValue.ToString <> "-1" Then

                                        objICbenedata.LoadByPrimaryKey(ddlBeneficiary.SelectedValue)
                                        objBene.BeneficiaryCode = objICbenedata.BeneficiaryCode

                                    Else
                                        objBene.BeneficiaryCode = Nothing
                                    End If

                                    objProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString())
                                    objBene.CompanyCode = objICInstruction.CompanyCode
                                    objBene.PaymentNatureCode = objICInstruction.PaymentNatureCode
                                    objBene.BeneGroupCode = ddlBeneficiaryGroup.SelectedValue.ToString()

                                    objBene.BeneficiaryName = objICInstruction.BeneficiaryName

                                    If objProductType.DisbursementMode = "Direct Credit" Then
                                        objBene.BeneficiaryAccountNo = objICInstruction.BeneficiaryAccountNo
                                        objBene.BeneficiaryAccountTitle = objICInstruction.BeneficiaryAccountTitle
                                        objBene.BeneAccountBranchCode = objICInstruction.BeneAccountBranchCode
                                        objBene.BeneAccountCurrency = objICInstruction.BeneAccountCurrency
                                    ElseIf objProductType.DisbursementMode = "Other Credit" Then
                                        objBene.BeneficiaryIBFTAccountNo = objICInstruction.BeneficiaryAccountNo
                                        objBene.BeneficiaryIBFTAccountTitle = objICInstruction.BeneficiaryAccountTitle
                                        objBene.BeneAccountBranchCode = Nothing
                                        objBene.BeneAccountCurrency = Nothing
                                    ElseIf objProductType.DisbursementMode = "Bill Payment" Then
                                        objBene.UBPCompanyID = ICUBPSCompanyController.GetUBPCompanyCodeByCompanyNameOrCodeorID(objICInstruction.UBPCompanyID)
                                        objBene.UBPReferenceNo = objICInstruction.UBPReferenceField
                                    End If

                                    If objICInstruction.BeneficiaryBankCode Is Nothing Then
                                        objBene.BeneficiaryBankCode = "0"
                                    Else
                                        objBene.BeneficiaryBankCode = objICInstruction.BeneficiaryBankCode
                                    End If

                                    objBene.BeneficiaryBankName = objICInstruction.BeneficiaryBankName
                                    objBene.BeneficiaryCNIC = objICInstruction.BeneficiaryCNIC
                                    objBene.BeneficiaryBankAddress = objICInstruction.BeneficiaryBankAddress
                                    objBene.BeneficiaryBranchCode = objICInstruction.BeneficiaryBranchCode
                                    objBene.BeneficiaryBranchName = objICInstruction.BeneficiaryBranchName
                                    objBene.BeneficiaryBranchAddress = objICInstruction.BeneBranchAddress
                                    objBene.BeneficiaryMobile = objICInstruction.BeneficiaryMobile
                                    objBene.BeneficiaryEmail = objICInstruction.BeneficiaryEmail
                                    objBene.BeneficiaryPhone = objICInstruction.BeneficiaryPhone
                                    objBene.BeneficiaryAddress = objICInstruction.BeneficiaryAddress
                                    objBene.BeneficiaryCountry = objICInstruction.BeneficiaryCountry
                                    objBene.BeneficiaryProvince = objICInstruction.BeneficiaryProvince
                                    objBene.BeneficiaryCity = objICInstruction.BeneficiaryCity
                                    objBene.BeneficiaryIDNo = objICInstruction.BeneficiaryIDNo
                                    objBene.BeneficiaryIDType = objICInstruction.BeneficiaryIDType
                                    objBene.IsApproved = False
                                    objBene.ApprovalforDeletionStatus = False
                                    objBene.IsActive = True
                                    objBene.BeneficiaryAddedVia = "Single Input"
                                    objBene.BeneficiaryStatus = Nothing
                                    objBene.CreateBy = Me.UserId
                                    objBene.CreateDate = Date.Now
                                    objBene.Creater = Me.UserId
                                    objBene.CreationDate = Date.Now

                                    BeneDuplicateRslt = ICBeneficiaryManagementController.CheckDuplicateBene(objBene, objBene.CompanyCode)

                                    If BeneDuplicateRslt = "OK" Then
                                        If objBene.BeneGroupCode = "0" Then
                                            Dim collBeneGroup As New ICBeneGroupCollection
                                            collBeneGroup = ICBeneficiaryGroupManagementController.GetAllActiveandApproveBeneGroupByPNandCompanyCode(objICInstruction.PaymentNatureCode, objBene.CompanyCode)
                                            objBene.BeneGroupCode = collBeneGroup(0).BeneGroupCode
                                        End If
                                        objBene.BeneID = ICBeneficiaryManagementController.AddBeneficiary(objBene, False, Me.UserId, Me.UserInfo.Username)
                                        ICBeneficiaryManagementController.AddBeneProductType(objBene, ddlProductType.SelectedValue.ToString())
                                    Else
                                        Dim BeneIDBeneCode As String = Nothing
                                        If objICInstruction.PaymentMode = "Other Credit" Then
                                            BeneIDBeneCode = ICBeneficiaryManagementController.GetBeneCodeandBeneIDByBeneAccNoandCNIC(objICInstruction.PaymentMode, objBene.BeneficiaryIBFTAccountNo, objICInstruction.CompanyCode)
                                            If Not BeneIDBeneCode Is Nothing Then
                                                objICInstruction.BeneID = BeneIDBeneCode.Split("-")(1).ToString()
                                                objICInstruction.BeneficiaryCode = BeneIDBeneCode.Split("-")(0).ToString()
                                            End If
                                        ElseIf objICInstruction.PaymentMode = "Direct Credit" Then
                                            BeneIDBeneCode = ICBeneficiaryManagementController.GetBeneCodeandBeneIDByBeneAccNoandCNIC(objICInstruction.PaymentMode, objBene.BeneficiaryAccountNo, objICInstruction.CompanyCode)
                                            If Not BeneIDBeneCode Is Nothing Then
                                                objICInstruction.BeneID = BeneIDBeneCode.Split("-")(1).ToString()
                                                objICInstruction.BeneficiaryCode = BeneIDBeneCode.Split("-")(0).ToString()
                                            End If
                                        ElseIf objICInstruction.PaymentMode = "COTC" Then
                                            BeneIDBeneCode = ICBeneficiaryManagementController.GetBeneCodeandBeneIDByBeneAccNoandCNIC(objICInstruction.PaymentMode, (objBene.BeneficiaryIDNo & "-" & objBene.BeneficiaryIDType), objICInstruction.CompanyCode)
                                            If Not BeneIDBeneCode Is Nothing Then
                                                objICInstruction.BeneID = BeneIDBeneCode.Split("-")(1).ToString()
                                                objICInstruction.BeneficiaryCode = BeneIDBeneCode.Split("-")(0).ToString()
                                            End If
                                        ElseIf objICInstruction.PaymentMode = "Bill Payment" Then
                                            BeneIDBeneCode = ICBeneficiaryManagementController.GetBeneCodeandBeneIDByBeneAccNoandCNIC(objICInstruction.PaymentMode, (objBene.UBPCompanyID & "-" & objBene.UBPReferenceNo), objICInstruction.CompanyCode)
                                            If Not BeneIDBeneCode Is Nothing Then
                                                objICInstruction.BeneID = BeneIDBeneCode.Split("-")(1).ToString()
                                                objICInstruction.BeneficiaryCode = BeneIDBeneCode.Split("-")(0).ToString()
                                            End If
                                        Else
                                            BeneIDBeneCode = ICBeneficiaryManagementController.GetBeneCodeandBeneIDByBeneAccNoandCNIC(objICInstruction.PaymentMode, objBene.BeneficiaryCode, objICInstruction.CompanyCode)
                                            If Not BeneIDBeneCode Is Nothing Then
                                                objICInstruction.BeneID = BeneIDBeneCode.Split("-")(1).ToString()
                                                objICInstruction.BeneficiaryCode = BeneIDBeneCode.Split("-")(0).ToString()
                                            End If
                                        End If
                                    End If

                                    '' Beneficiary details update in Instruction
                                    Dim objICBene As New ICBene
                                    If Not (objICInstruction.BeneID.ToString() Is Nothing Or objICInstruction.BeneID.ToString() = "") Then
                                        If objICBene.LoadByPrimaryKey(objICInstruction.BeneID) Then

                                            If objICInstruction.PaymentMode = "Direct Credit" Then
                                                objICInstruction.BeneficiaryAccountNo = objICBene.BeneficiaryAccountNo
                                                objICInstruction.BeneAccountBranchCode = objICBene.BeneAccountBranchCode
                                                objICInstruction.BeneAccountCurrency = objICBene.BeneAccountCurrency
                                                objICInstruction.BeneficiaryAccountTitle = objICBene.BeneficiaryAccountTitle
                                            ElseIf objICInstruction.PaymentMode = "Other Credit" Then
                                                objICInstruction.BeneficiaryAccountNo = objICBene.BeneficiaryIBFTAccountNo
                                                objICInstruction.BeneficiaryAccountTitle = objICBene.BeneficiaryIBFTAccountTitle
                                            ElseIf objICInstruction.PaymentMode = "Bill Payment" Then
                                                objICInstruction.UBPCompanyID = ICUBPSCompanyController.GetUBPCompanyIDByCompanyCodeor(objICBene.UBPCompanyID)
                                                objICInstruction.UBPReferenceField = objICBene.UBPReferenceNo
                                            ElseIf objICInstruction.PaymentMode = "COTC" Then
                                                objICInstruction.BeneficiaryIDNo = objICBene.BeneficiaryIDNo
                                                objICInstruction.BeneficiaryIDType = objICBene.BeneficiaryIDType
                                            End If
                                            objICInstruction.BeneficiaryAddress = objICBene.BeneficiaryAddress
                                            objICInstruction.BeneficiaryBankCode = objICBene.BeneficiaryBankCode
                                            objICInstruction.BeneficiaryBankName = objICBene.BeneficiaryBankName
                                            objICInstruction.BeneficiaryBankAddress = objICBene.BeneficiaryBankAddress
                                            objICInstruction.BeneficiaryBranchCode = objICBene.BeneficiaryBranchCode
                                            objICInstruction.BeneficiaryBranchName = objICBene.BeneficiaryBranchName
                                            objICInstruction.BeneBranchAddress = objICBene.BeneficiaryBranchAddress
                                            objICInstruction.BeneficiaryCity = objICBene.BeneficiaryCity
                                            objICInstruction.BeneficiaryCNIC = objICBene.BeneficiaryCNIC
                                            objICInstruction.BeneficiaryCountry = objICBene.BeneficiaryCountry
                                            objICInstruction.BeneficiaryEmail = objICBene.BeneficiaryEmail
                                            objICInstruction.BeneficiaryMobile = objICBene.BeneficiaryMobile
                                            objICInstruction.BeneficiaryName = objICBene.BeneficiaryName
                                            objICInstruction.BeneficiaryPhone = objICBene.BeneficiaryPhone
                                            objICInstruction.BeneficiaryProvince = objICBene.BeneficiaryProvince
                                            objICInstruction.BeneficiaryIDNo = objICBene.BeneficiaryIDNo
                                            objICInstruction.BeneficiaryIDType = objICBene.BeneficiaryIDType
                                            objICInstruction.BeneficiaryCode = objICBene.BeneficiaryCode
                                        End If
                                    End If

                                    '' If BeneID Present in instruction then save only relevant fields based on Disbursement Mode
                                    '' If other fields present in instruction but not depend on selected Disbursment mode then it will be ignore

                                    If objICInstruction.PaymentMode = "Other Credit" Then
                                        objICInstruction.BeneficiaryIDNo = Nothing
                                        objICInstruction.BeneficiaryIDType = Nothing
                                        objICInstruction.BeneAccountBranchCode = Nothing
                                        objICInstruction.BeneAccountCurrency = Nothing
                                        objICInstruction.UBPCompanyID = Nothing
                                        objICInstruction.UBPReferenceField = Nothing
                                        objICInstruction.BeneficiaryBranchCode = Nothing
                                    ElseIf objICInstruction.PaymentMode = "Direct Credit" Then
                                        objICInstruction.BeneficiaryIDNo = Nothing
                                        objICInstruction.BeneficiaryIDType = Nothing
                                        objICInstruction.UBPCompanyID = Nothing
                                        objICInstruction.UBPReferenceField = Nothing
                                        objICInstruction.BeneficiaryBankCode = Nothing
                                        objICInstruction.BeneficiaryBankName = Nothing
                                        objICInstruction.BeneAccountBranchCode = Nothing
                                        objICInstruction.BeneficiaryBranchCode = Nothing
                                    ElseIf objICInstruction.PaymentMode = "COTC" Then
                                        objICInstruction.BeneficiaryAccountNo = Nothing
                                        objICInstruction.BeneficiaryAccountTitle = Nothing
                                        objICInstruction.BeneAccountBranchCode = Nothing
                                        objICInstruction.BeneAccountCurrency = Nothing
                                        objICInstruction.UBPCompanyID = Nothing
                                        objICInstruction.UBPReferenceField = Nothing
                                        objICInstruction.BeneficiaryBankCode = Nothing
                                        objICInstruction.BeneficiaryBankName = Nothing
                                        objICInstruction.BeneAccountBranchCode = Nothing

                                    ElseIf objICInstruction.PaymentMode = "Bill Payment" Then
                                        objICInstruction.BeneficiaryAccountNo = Nothing
                                        objICInstruction.BeneficiaryAccountTitle = Nothing
                                        objICInstruction.BeneAccountBranchCode = Nothing
                                        objICInstruction.BeneAccountCurrency = Nothing
                                        objICInstruction.BeneficiaryIDNo = Nothing
                                        objICInstruction.BeneficiaryIDType = Nothing
                                        objICInstruction.BeneficiaryBankCode = Nothing
                                        objICInstruction.BeneficiaryBankName = Nothing
                                        objICInstruction.BeneAccountBranchCode = Nothing
                                        objICInstruction.BeneficiaryBranchCode = Nothing
                                    Else            '' PO, DD, CHQ,COTC,Bill Payment
                                        objICInstruction.BeneficiaryIDNo = Nothing
                                        objICInstruction.BeneficiaryIDType = Nothing
                                        objICInstruction.BeneficiaryAccountNo = Nothing
                                        objICInstruction.BeneficiaryAccountTitle = Nothing
                                        objICInstruction.BeneAccountBranchCode = Nothing
                                        objICInstruction.BeneAccountCurrency = Nothing
                                        objICInstruction.UBPCompanyID = Nothing
                                        objICInstruction.UBPReferenceField = Nothing
                                        objICInstruction.BeneficiaryBankCode = Nothing
                                        objICInstruction.BeneficiaryBankName = Nothing
                                        objICInstruction.BeneAccountBranchCode = Nothing
                                        objICInstruction.BeneficiaryBranchCode = Nothing

                                    End If

                                End If
                            End If


                            objICInstruction.InstructionID = ICInstructionController.AddInstruction(objICInstruction, objICBeneficiary, Me.UserId.ToString, Me.UserInfo.Username.ToString, ActionStr.ToString)
                            Count = Count + 1
                            Dim APNLocation As String = Nothing
                            APNLocation = objICInstruction.ClientAccountNo + "-" + objICInstruction.ClientAccountBranchCode + "-" + objICInstruction.ClientAccountCurrency + "-" + objICInstruction.PaymentNatureCode + "-" + objICInstruction.CreatedOfficeCode.ToString
                            EmailUtilities.SingleInstructioninputtedbyUser(objICInstruction.InstructionID.ToString, Me.UserInfo.Username.ToString, APNLocation)
                            SMSUtilities.SingleInstructioninputtedbyUser(objICInstruction.InstructionID.ToString, APNLocation)
                        Next
                        UIUtilities.ShowDialog(Me, "Online Form", " [ " & Count & " ] Schedule Instruction(s) added successfully.", ICBO.IC.Dialogmessagetype.Success)
                        ClearOnlineForm()
                        ClearPreview()
                        ClearScheduleTxn()
                        HideAllOnlineFormRows()
                        HideAllOnlineFormPreviewRows()
                        HideRequiredMarkSign()
                        'SetViewFieldsVisibility()
                        If ddlBeneficiary.SelectedValue = "01" Or ddlBeneficiary.SelectedValue = "0" Then
                            SetViewFieldsVisibilityAndData(False, "0")
                        Else
                            SetViewFieldsVisibilityAndData(True, ddlBeneficiary.SelectedValue.ToString())
                        End If
                        SetScheduleTransactionSection()

                        ''Schedule settings
                        tblScheduleTxn.Style.Add("display", "none")
                        tblDayType.Style.Add("display", "none")
                        tblNoOfDays.Style.Add("display", "none")
                        tblSpecificDay.Style.Add("display", "none")
                        tblSchedulePreview.Style.Add("display", "none")
                        rbScheduleTransaction.Checked = False
                        txtBeneName.Enabled = True


                        pnlOnlineFrom.Style.Remove("display")
                        pnlOnLineFormPreview.Style.Add("display", "none")

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Invalid possible dates", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else


                    '' Add Beneficiary and Instruction
                    Dim objCompany As New ICCompany
                    If objCompany.LoadByPrimaryKey(objICInstruction.CompanyCode) Then
                        If objCompany.IsBeneRequired = True And objCompany.IsAllowOpenPayment = True Then

                            Dim objICbenedata As New ICBene

                            Dim BeneDuplicateRslt As String = ""
                            Dim objBene As New ICBene
                            Dim objProductType As New ICProductType

                            If ddlBeneficiary.SelectedValue.ToString <> "0" And ddlBeneficiary.SelectedValue.ToString <> "-1" Then

                                objICbenedata.LoadByPrimaryKey(ddlBeneficiary.SelectedValue)
                                objBene.BeneficiaryCode = objICbenedata.BeneficiaryCode

                            Else
                                objBene.BeneficiaryCode = Nothing
                            End If

                            objProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString())
                            objBene.CompanyCode = objICInstruction.CompanyCode
                            objBene.PaymentNatureCode = objICInstruction.PaymentNatureCode
                            objBene.BeneGroupCode = ddlBeneficiaryGroup.SelectedValue.ToString()
                            'objBene.BeneficiaryCode = objICInstruction.BeneficiaryCode
                            objBene.BeneficiaryName = objICInstruction.BeneficiaryName

                            If objProductType.DisbursementMode = "Direct Credit" Then
                                objBene.BeneficiaryAccountNo = objICInstruction.BeneficiaryAccountNo
                                objBene.BeneficiaryAccountTitle = objICInstruction.BeneficiaryAccountTitle
                                objBene.BeneAccountBranchCode = objICInstruction.BeneAccountBranchCode
                                objBene.BeneAccountCurrency = objICInstruction.BeneAccountCurrency
                            ElseIf objProductType.DisbursementMode = "Other Credit" Then
                                objBene.BeneficiaryIBFTAccountNo = objICInstruction.BeneficiaryAccountNo
                                objBene.BeneficiaryIBFTAccountTitle = objICInstruction.BeneficiaryAccountTitle
                                objBene.BeneAccountBranchCode = Nothing
                                objBene.BeneAccountCurrency = Nothing
                            ElseIf objProductType.DisbursementMode = "Bill Payment" Then
                                objBene.UBPCompanyID = ICUBPSCompanyController.GetUBPCompanyCodeByCompanyNameOrCodeorID(objICInstruction.UBPCompanyID)
                                objBene.UBPReferenceNo = objICInstruction.UBPReferenceField

                            End If

                            If objICInstruction.BeneficiaryBankCode Is Nothing Then
                                objBene.BeneficiaryBankCode = "0"
                            Else
                                objBene.BeneficiaryBankCode = objICInstruction.BeneficiaryBankCode
                            End If

                            objBene.BeneficiaryBankName = objICInstruction.BeneficiaryBankName
                            objBene.BeneficiaryCNIC = objICInstruction.BeneficiaryCNIC
                            objBene.BeneficiaryBankAddress = objICInstruction.BeneficiaryBankAddress
                            objBene.BeneficiaryBranchCode = objICInstruction.BeneficiaryBranchCode
                            objBene.BeneficiaryBranchName = objICInstruction.BeneficiaryBranchName
                            objBene.BeneficiaryBranchAddress = objICInstruction.BeneBranchAddress
                            objBene.BeneficiaryMobile = objICInstruction.BeneficiaryMobile
                            objBene.BeneficiaryEmail = objICInstruction.BeneficiaryEmail
                            objBene.BeneficiaryPhone = objICInstruction.BeneficiaryPhone
                            objBene.BeneficiaryAddress = objICInstruction.BeneficiaryAddress
                            objBene.BeneficiaryCountry = objICInstruction.BeneficiaryCountry
                            objBene.BeneficiaryProvince = objICInstruction.BeneficiaryProvince
                            objBene.BeneficiaryCity = objICInstruction.BeneficiaryCity
                            objBene.BeneficiaryIDNo = objICInstruction.BeneficiaryIDNo
                            objBene.BeneficiaryIDType = objICInstruction.BeneficiaryIDType
                            objBene.IsApproved = False
                            objBene.ApprovalforDeletionStatus = False
                            objBene.IsActive = True
                            objBene.BeneficiaryAddedVia = "Single Input"
                            objBene.BeneficiaryStatus = Nothing
                            objBene.CreateBy = Me.UserId
                            objBene.CreateDate = Date.Now
                            objBene.Creater = Me.UserId
                            objBene.CreationDate = Date.Now

                            BeneDuplicateRslt = ICBeneficiaryManagementController.CheckDuplicateBene(objBene, objBene.CompanyCode)

                            If BeneDuplicateRslt = "OK" Then
                                If objBene.BeneGroupCode = "0" Then
                                    Dim collBeneGroup As New ICBeneGroupCollection
                                    collBeneGroup = ICBeneficiaryGroupManagementController.GetAllActiveandApproveBeneGroupByPNandCompanyCode(objICInstruction.PaymentNatureCode, objBene.CompanyCode)
                                    objBene.BeneGroupCode = collBeneGroup(0).BeneGroupCode
                                End If
                                objBene.BeneID = ICBeneficiaryManagementController.AddBeneficiary(objBene, False, Me.UserId, Me.UserInfo.Username)
                                ICBeneficiaryManagementController.AddBeneProductType(objBene, ddlProductType.SelectedValue.ToString())
                            Else
                                Dim BeneIDBeneCode As String = Nothing
                                If objICInstruction.PaymentMode = "Other Credit" Then
                                    BeneIDBeneCode = ICBeneficiaryManagementController.GetBeneCodeandBeneIDByBeneAccNoandCNIC(objICInstruction.PaymentMode, objBene.BeneficiaryIBFTAccountNo, objICInstruction.CompanyCode)
                                    If Not BeneIDBeneCode Is Nothing Then
                                        objICInstruction.BeneID = BeneIDBeneCode.Split("-")(1).ToString()
                                        objICInstruction.BeneficiaryCode = BeneIDBeneCode.Split("-")(0).ToString()
                                    End If
                                ElseIf objICInstruction.PaymentMode = "Direct Credit" Then
                                    BeneIDBeneCode = ICBeneficiaryManagementController.GetBeneCodeandBeneIDByBeneAccNoandCNIC(objICInstruction.PaymentMode, objBene.BeneficiaryAccountNo, objICInstruction.CompanyCode)
                                    If Not BeneIDBeneCode Is Nothing Then
                                        objICInstruction.BeneID = BeneIDBeneCode.Split("-")(1).ToString()
                                        objICInstruction.BeneficiaryCode = BeneIDBeneCode.Split("-")(0).ToString()
                                    End If
                                ElseIf objICInstruction.PaymentMode = "COTC" Then
                                    BeneIDBeneCode = ICBeneficiaryManagementController.GetBeneCodeandBeneIDByBeneAccNoandCNIC(objICInstruction.PaymentMode, (objBene.BeneficiaryIDNo & "-" & objBene.BeneficiaryIDType), objICInstruction.CompanyCode)
                                    If Not BeneIDBeneCode Is Nothing Then
                                        objICInstruction.BeneID = BeneIDBeneCode.Split("-")(1).ToString()
                                        objICInstruction.BeneficiaryCode = BeneIDBeneCode.Split("-")(0).ToString()
                                    End If
                                ElseIf objICInstruction.PaymentMode = "Bill Payment" Then
                                    BeneIDBeneCode = ICBeneficiaryManagementController.GetBeneCodeandBeneIDByBeneAccNoandCNIC(objICInstruction.PaymentMode, (objBene.UBPCompanyID & "-" & objBene.UBPReferenceNo), objICInstruction.CompanyCode)
                                    If Not BeneIDBeneCode Is Nothing Then
                                        objICInstruction.BeneID = BeneIDBeneCode.Split("-")(1).ToString()
                                        objICInstruction.BeneficiaryCode = BeneIDBeneCode.Split("-")(0).ToString()
                                    End If
                                Else
                                    BeneIDBeneCode = ICBeneficiaryManagementController.GetBeneCodeandBeneIDByBeneAccNoandCNIC(objICInstruction.PaymentMode, objBene.BeneficiaryCode, objICInstruction.CompanyCode)
                                    If Not BeneIDBeneCode Is Nothing Then
                                        objICInstruction.BeneID = BeneIDBeneCode.Split("-")(1).ToString()
                                        objICInstruction.BeneficiaryCode = BeneIDBeneCode.Split("-")(0).ToString()
                                    End If
                                End If
                            End If


                            '' Beneficiary details update in Instruction
                            Dim objICBene As New ICBene
                            If Not (objICInstruction.BeneID Is Nothing Or objICInstruction.BeneID.ToString() = "") Then
                                If objICBene.LoadByPrimaryKey(objICInstruction.BeneID) Then

                                    If objICInstruction.PaymentMode = "Direct Credit" Then
                                        objICInstruction.BeneficiaryAccountNo = objICBene.BeneficiaryAccountNo
                                        objICInstruction.BeneAccountBranchCode = objICBene.BeneAccountBranchCode
                                        objICInstruction.BeneAccountCurrency = objICBene.BeneAccountCurrency
                                        objICInstruction.BeneficiaryAccountTitle = objICBene.BeneficiaryAccountTitle
                                    ElseIf objICInstruction.PaymentMode = "Other Credit" Then
                                        objICInstruction.BeneficiaryAccountNo = objICBene.BeneficiaryIBFTAccountNo
                                        objICInstruction.BeneficiaryAccountTitle = objICBene.BeneficiaryIBFTAccountTitle
                                    ElseIf objICInstruction.PaymentMode = "Bill Payment" Then
                                        objICInstruction.UBPCompanyID = ICUBPSCompanyController.GetUBPCompanyIDByCompanyCodeor(objICBene.UBPCompanyID)
                                        objICInstruction.UBPReferenceField = objICBene.UBPReferenceNo
                                    End If
                                    objICInstruction.BeneficiaryAddress = objICBene.BeneficiaryAddress
                                    objICInstruction.BeneficiaryBankCode = objICBene.BeneficiaryBankCode
                                    objICInstruction.BeneficiaryBankName = objICBene.BeneficiaryBankName
                                    objICInstruction.BeneficiaryBankAddress = objICBene.BeneficiaryBankAddress
                                    objICInstruction.BeneficiaryBranchCode = objICBene.BeneficiaryBranchCode
                                    objICInstruction.BeneficiaryBranchName = objICBene.BeneficiaryBranchName
                                    objICInstruction.BeneBranchAddress = objICBene.BeneficiaryBranchAddress
                                    objICInstruction.BeneficiaryCity = objICBene.BeneficiaryCity
                                    objICInstruction.BeneficiaryCNIC = objICBene.BeneficiaryCNIC
                                    objICInstruction.BeneficiaryCountry = objICBene.BeneficiaryCountry
                                    objICInstruction.BeneficiaryEmail = objICBene.BeneficiaryEmail
                                    objICInstruction.BeneficiaryMobile = objICBene.BeneficiaryMobile
                                    objICInstruction.BeneficiaryName = objICBene.BeneficiaryName
                                    objICInstruction.BeneficiaryPhone = objICBene.BeneficiaryPhone
                                    objICInstruction.BeneficiaryProvince = objICBene.BeneficiaryProvince
                                    objICInstruction.BeneficiaryIDNo = objICBene.BeneficiaryIDNo
                                    objICInstruction.BeneficiaryIDType = objICBene.BeneficiaryIDType
                                    objICInstruction.BeneficiaryCode = objICBene.BeneficiaryCode


                                End If
                            End If

                            '' If BeneID Present in instruction then save only relevant fields based on Disbursement Mode
                            '' If other fields present in instruction but not depend on selected Disbursment mode then it will be ignore


                            If objICInstruction.PaymentMode = "Other Credit" Then
                                objICInstruction.BeneficiaryIDNo = Nothing
                                objICInstruction.BeneficiaryIDType = Nothing
                                objICInstruction.BeneAccountBranchCode = Nothing
                                objICInstruction.BeneAccountCurrency = Nothing
                                objICInstruction.UBPCompanyID = Nothing
                                objICInstruction.UBPReferenceField = Nothing
                                objICInstruction.BeneficiaryBranchCode = Nothing
                            ElseIf objICInstruction.PaymentMode = "Direct Credit" Then
                                objICInstruction.BeneficiaryIDNo = Nothing
                                objICInstruction.BeneficiaryIDType = Nothing
                                objICInstruction.UBPCompanyID = Nothing
                                objICInstruction.UBPReferenceField = Nothing
                                objICInstruction.BeneficiaryBankCode = Nothing
                                objICInstruction.BeneficiaryBankName = Nothing
                                objICInstruction.BeneAccountBranchCode = Nothing
                                objICInstruction.BeneficiaryBranchCode = Nothing
                            ElseIf objICInstruction.PaymentMode = "COTC" Then
                                objICInstruction.BeneficiaryAccountNo = Nothing
                                objICInstruction.BeneficiaryAccountTitle = Nothing
                                objICInstruction.BeneAccountBranchCode = Nothing
                                objICInstruction.BeneAccountCurrency = Nothing
                                objICInstruction.UBPCompanyID = Nothing
                                objICInstruction.UBPReferenceField = Nothing
                                objICInstruction.BeneficiaryBankCode = Nothing
                                objICInstruction.BeneficiaryBankName = Nothing
                                objICInstruction.BeneAccountBranchCode = Nothing


                            ElseIf objICInstruction.PaymentMode = "Bill Payment" Then
                                objICInstruction.BeneficiaryAccountNo = Nothing
                                objICInstruction.BeneficiaryAccountTitle = Nothing
                                objICInstruction.BeneAccountBranchCode = Nothing
                                objICInstruction.BeneAccountCurrency = Nothing
                                objICInstruction.BeneficiaryIDNo = Nothing
                                objICInstruction.BeneficiaryIDType = Nothing
                                objICInstruction.BeneficiaryBankCode = Nothing
                                objICInstruction.BeneficiaryBankName = Nothing
                                objICInstruction.BeneAccountBranchCode = Nothing

                                objICInstruction.BeneficiaryBranchCode = Nothing
                            Else            '' PO, DD, CHQ
                                objICInstruction.BeneficiaryIDNo = Nothing
                                objICInstruction.BeneficiaryIDType = Nothing
                                objICInstruction.BeneficiaryAccountNo = Nothing
                                objICInstruction.BeneficiaryAccountTitle = Nothing
                                objICInstruction.BeneAccountBranchCode = Nothing
                                objICInstruction.BeneAccountCurrency = Nothing
                                objICInstruction.UBPCompanyID = Nothing
                                objICInstruction.UBPReferenceField = Nothing
                                objICInstruction.BeneficiaryBankCode = Nothing
                                objICInstruction.BeneficiaryBankName = Nothing
                                objICInstruction.BeneAccountBranchCode = Nothing

                                objICInstruction.BeneficiaryBranchCode = Nothing
                            End If

                        End If
                    End If
                    objICInstruction.InstructionID = ICInstructionController.AddInstruction(objICInstruction, objICBeneficiary, Me.UserId.ToString, Me.UserInfo.Username.ToString, ActionStr.ToString)
                    Dim APNLocation As String = Nothing
                    APNLocation = objICInstruction.ClientAccountNo + "-" + objICInstruction.ClientAccountBranchCode + "-" + objICInstruction.ClientAccountCurrency + "-" + objICInstruction.PaymentNatureCode + "-" + objICInstruction.CreatedOfficeCode.ToString
                    EmailUtilities.SingleInstructioninputtedbyUser(objICInstruction.InstructionID.ToString, Me.UserInfo.Username.ToString, APNLocation)
                    SMSUtilities.SingleInstructioninputtedbyUser(objICInstruction.InstructionID.ToString, APNLocation)
                    UIUtilities.ShowDialog(Me, "Online Form", "[ " & objICInstruction.InstructionID & " ] Instruction added successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    ClearOnlineForm()
                    ClearPreview()
                    HideAllOnlineFormRows()
                    HideAllOnlineFormPreviewRows()
                    HideRequiredMarkSign()
                    'SetViewFieldsVisibility()

                    pnlOnlineFrom.Style.Remove("display")
                    pnlOnLineFormPreview.Style.Add("display", "none")
                    If ddlBeneficiary.SelectedValue = "01" Or ddlBeneficiary.SelectedValue = "0" Then
                        SetViewFieldsVisibilityAndData(False, "0")
                    Else
                        SetViewFieldsVisibilityAndData(True, ddlBeneficiary.SelectedValue.ToString())
                    End If
                End If



            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Function GetScheduleTransaction(ByVal objICInstruction As ICInstruction, ByVal TransactionCount As Integer) As ICScheduleTransactions
        Dim objICScheduleTransaction As New ICScheduleTransactions

        Dim StrTimeArray As String() = radTimePicker.SelectedDate.Value.ToString.Split(" ")
        Dim idATEtIME As DateTime = StrTimeArray(1).Split(":")(0) & ":" & StrTimeArray(1).Split(":")(1)
        objICScheduleTransaction.CompanyCode = objICInstruction.CompanyCode
        objICScheduleTransaction.ClientAccountNo = objICInstruction.ClientAccountNo
        objICScheduleTransaction.ClientAccountBranchCode = objICInstruction.ClientAccountBranchCode
        objICScheduleTransaction.ClientAccountCurrency = objICInstruction.ClientAccountCurrency
        objICScheduleTransaction.PaymentNatureCode = objICInstruction.PaymentNatureCode
        objICScheduleTransaction.ProductTypeCode = objICInstruction.ProductTypeCode
        objICScheduleTransaction.BeneficaryName = objICInstruction.BeneficiaryName
        objICScheduleTransaction.BeneficiaryAccountNo = objICInstruction.BeneficiaryAccountNo
        objICScheduleTransaction.BeneficiaryAccountBranchCode = objICInstruction.BeneAccountBranchCode
        objICScheduleTransaction.BeneficiaryAccountCurrency = objICInstruction.BeneAccountCurrency
        objICScheduleTransaction.CreatedDate = Date.Now
        objICScheduleTransaction.CreatedBy = Me.UserId
        objICScheduleTransaction.ScheduleTransactionFromDate = txtStartDate.SelectedDate
        objICScheduleTransaction.ScheduleTransactionToDate = txtEndDate.SelectedDate
        objICScheduleTransaction.TrnsactionFrequency = ddlScheduleFrequency.SelectedValue.ToString
        objICScheduleTransaction.DayType = rblDaysOfMonth.SelectedValue.ToString
        objICScheduleTransaction.AmountPerInstruction = CDbl(objICInstruction.Amount)
        objICScheduleTransaction.TransactionCount = TransactionCount
        objICScheduleTransaction.TransactionTime = CDate(radTimePicker.SelectedDate)

        Return objICScheduleTransaction
    End Function
    Protected Sub btnPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        'Page.Validate()

        'For Each validator As BaseValidator In Page.Validators
        '    If validator.Enabled = True And validator.IsValid = False Then
        '        Dim clientID As String
        '        clientID = validator.ClientID
        '    End If
        'Next

        'If Page.IsValid Then
        Try
                Dim objICProductType As New ICProductType
                Dim objICUBPCompany As New ICUBPSCompany
                Dim dtBestPossibleDate As New DataTable
                Dim StrBestPossible As String




                If objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString) Then
                    If objICProductType.DisbursementMode = "Direct Credit" Then
                        If (txtAccountTitle.Text = "" Or txtAccountTitle.Text Is Nothing) Or ((txtBeneAccountBranchCode.Text = "" Or txtBeneAccountBranchCode.Text Is Nothing) And ddlGLAccountBranchCode.SelectedValue.ToString = "0") Or (txtAccountTitle.Text = "" Or txtAccountTitle.Text Is Nothing) Then
                            UIUtilities.ShowDialog(Me, "Error", "Invalid beneficiary account", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                        If ddlBeneCBAccountType.SelectedValue.ToString = "GL" Then
                            If txtBankAccountNumber.Text.Trim.Length <> 8 Then
                                UIUtilities.ShowDialog(Me, "Error", "Invalid beneficiary GL account length", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        End If
                    ElseIf objICProductType.DisbursementMode = "Other Credit" Then
                        If txtAccountTitle.Text = "" Or txtAccountTitle.Text Is Nothing Then
                            UIUtilities.ShowDialog(Me, "Error", "Invalid beneficiary account", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    ElseIf objICProductType.DisbursementMode = "DD" Then
                        If ddlDDPayableLocationCity.SelectedValue.ToString <> "0" Then
                            Dim dtBankBranches As New DataTable
                            dtBankBranches = ICOfficeController.GetAllPrinciaplBankBranchesByCityCode(ddlDDPayableLocationCity.SelectedValue.ToString)
                            If dtBankBranches.Rows.Count > 0 Then
                                UIUtilities.ShowDialog(Me, "Error", "Principal Bank branch exists in specified city. DD cannot be made", ICBO.IC.Dialogmessagetype.Failure)
                                LoadddlDDDrawnLocationCity()
                                Exit Sub
                            End If

                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Please select DD payable location", ICBO.IC.Dialogmessagetype.Failure)
                            LoadddlDDDrawnLocationCity()
                            Exit Sub
                        End If
                    End If

                End If


                If objICProductType.DisbursementMode <> "Bill Payment" Then
                    If txtAmountPKR.Text = "" Or txtAmountPKR.Text Is Nothing Then
                        UIUtilities.ShowDialog(Me, "Error", "Invalid amount.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf CDbl(txtAmountPKR.Text) <= 0 Then
                        UIUtilities.ShowDialog(Me, "Error", "Invalid amount.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else
                    If objICUBPCompany.LoadByPrimaryKey(ddlUBPCompany.SelectedValue.ToString) Then
                        If objICUBPCompany.MultiplesOfAmount <> "Only Billed Amount" Then
                            If txtAmountPKR.Text = "" Or txtAmountPKR.Text Is Nothing Then
                                UIUtilities.ShowDialog(Me, "Error", "Invalid amount.", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            ElseIf CDbl(txtAmountPKR.Text) <= 0 Then
                                UIUtilities.ShowDialog(Me, "Error", "Invalid amount.", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If

                            If txtUBPReferenceField.Text <> "" Then
                                If ICUBPSCompanyController.CheckReferenceFieldLength(txtUBPReferenceField.Text.Length.ToString, ddlUBPCompany.SelectedValue.ToString) = False Then
                                    UIUtilities.ShowDialog(Me, "Error", "Invalid UBP reference field length", ICBO.IC.Dialogmessagetype.Failure)
                                    Exit Sub
                                End If
                            End If
                            If txtUBPReferenceField.Text <> "" Then
                                If ICUBPSCompanyController.CheckReferenceFieldDataWithRegularExpression(txtUBPReferenceField.Text.ToString, ddlUBPCompany.SelectedValue.ToString) = False Then
                                    UIUtilities.ShowDialog(Me, "Error", "Invalid UBP reference field data", ICBO.IC.Dialogmessagetype.Failure)
                                    Exit Sub
                                End If
                            End If
                            Dim EnteredAmount As String = ""
                            EnteredAmount = ICUBPSCompanyController.CheckUBPCompanyAmountAcceptance(CDbl(txtAmountPKR.Text), ddlUBPCompany.SelectedValue.ToString)
                            If EnteredAmount <> "OK" Then
                                UIUtilities.ShowDialog(Me, "Error", "Invalid amount due to " & EnteredAmount, ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        Else
                            If txtUBPReferenceField.Text <> "" Then
                                If ICUBPSCompanyController.CheckReferenceFieldLength(txtUBPReferenceField.Text.Length.ToString, ddlUBPCompany.SelectedValue.ToString) = False Then
                                    UIUtilities.ShowDialog(Me, "Error", "Invalid UBP reference field length", ICBO.IC.Dialogmessagetype.Failure)
                                    Exit Sub
                                End If
                            End If
                            If txtUBPReferenceField.Text <> "" Then
                                If ICUBPSCompanyController.CheckReferenceFieldDataWithRegularExpression(txtUBPReferenceField.Text.ToString, ddlUBPCompany.SelectedValue.ToString) = False Then
                                    UIUtilities.ShowDialog(Me, "Error", "Invalid UBP reference field data", ICBO.IC.Dialogmessagetype.Failure)
                                    Exit Sub
                                End If
                            End If
                        End If

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Invalid UBP Company", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If

                If txtBankAccountNumber.Text = ddlAccPNature.SelectedValue.ToString.Split("-")(0).ToString Then
                    UIUtilities.ShowDialog(Me, "Error", "Beneficiary account shoud be other than customer account", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If rbScheduleTransaction.Checked = True Then
                    If txtStartDate.SelectedDate Is Nothing Then
                        UIUtilities.ShowDialog(Me, "Error", "Please select Schedule Start Date", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    If txtEndDate.SelectedDate Is Nothing Then
                        UIUtilities.ShowDialog(Me, "Error", "Please select Schedule End Date", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    If txtStartDate.SelectedDate > txtEndDate.SelectedDate Then
                        UIUtilities.ShowDialog(Me, "Error", "Schedule start must be greater than or equal to Schedule To Date", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    If ddlScheduleFrequency.SelectedValue.ToString = "Monthly" Then
                        Dim MonthInterval As Integer = DateDiff(DateInterval.Month, CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate))
                        If MonthInterval <= 0 Then
                            UIUtilities.ShowDialog(Me, "Error", "Schedule End Date must be greater than or equal to one month from Schedule Start Date", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                        If rblDaysOfMonth.SelectedValue.ToString = "Specific Day" Then
                            Dim DayInterval As Integer = DateDiff(DateInterval.Day, CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate))
                            If DayInterval < CInt(ddlSpecificDay.SelectedValue) Then
                                UIUtilities.ShowDialog(Me, "Error", "Schedule End Date must be greater than or equal to Specific Day", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        End If
                    ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Number Of Days" Then
                        Dim MonthInterval As Integer = DateDiff(DateInterval.Day, CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate))
                        If MonthInterval < CInt(txtNoOfDays.Text) Then
                            UIUtilities.ShowDialog(Me, "Error", "Schedule End Date must be greater than or equal to No of Days", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    End If


                    If ddlScheduleFrequency.SelectedValue.ToString = "Monthly" Then
                        If rblDaysOfMonth.SelectedValue.ToString = "First Day" Then
                            StrBestPossible = ICScheduleInstructionController.GetBestPossibleDay(CDate(txtStartDate.SelectedDate), "First Day", "")
                            dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, StrBestPossible, "First Day")
                        ElseIf rblDaysOfMonth.SelectedValue.ToString = "Last Day" Then
                            Dim MonthInterval As Integer = DateDiff(DateInterval.Month, CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate))
                            StrBestPossible = MonthInterval.ToString
                            dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, StrBestPossible, "Last Day")
                        ElseIf rblDaysOfMonth.SelectedValue.ToString = "Specific Day" Then
                            StrBestPossible = ICScheduleInstructionController.GetBestPossibleDay(CDate(txtStartDate.SelectedDate), "Specific Day", ddlSpecificDay.SelectedValue.ToString)
                            dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, StrBestPossible, "Specific Day")
                        End If
                    ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Daily" Then
                        dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, "", "")
                    ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Number Of Days" Then
                        StrBestPossible = ICScheduleInstructionController.GetBestPossibleDay(CDate(txtStartDate.SelectedDate), "Number Of Days", txtNoOfDays.Text.ToString)
                        dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, StrBestPossible, "")
                    End If

                    If dtBestPossibleDate.Rows.Count = 0 Then

                        UIUtilities.ShowDialog(Me, "Error", "Invalid possible dates", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If



                End If



                If ddlBeneficiary.SelectedValue = "-1" Or ddlBeneficiary.SelectedValue = "0" Then
                    SetPreviewforNewBene()
                Else
                    SetPreviewforOldBene()
                End If





                pnlOnlineFrom.Style.Add("display", "none")
                'SetPreview()
                pnlOnLineFormPreview.Style.Remove("display")
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        'End If

    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            SetArraListRoleID()
            LoadddlAccountPaymentNatureByUserID()
            LoadddlProductTypeByAccountPaymentNature()
            'LoadddlClientOfficeByUserID()
            ClearOnlineForm()
            ClearPreview()
            HideAllOnlineFormRows()
            HideAllOnlineFormPreviewRows()
            'sharedCalendarValueDate.SelectedDate = Date.Now
            'sharedCalendarValueDate.RangeMinDate = Date.Now
            'txtValueDate.MinDate = Date.Now
            'txtValueDate.SelectedDate = Date.Now
            pnlOnLineFormPreview.Style.Add("display", "none")
            pnlOnLineFormPreview.Style.Add("display", "none")
            If ddlCompany.SelectedValue <> "0" Then
                Dim objCompany As New ICCompany
                objCompany.LoadByPrimaryKey(ddlCompany.SelectedValue.ToString())
                If objCompany.IsBeneRequired = True Then
                    pnlBeneficiary.Visible = True
                Else
                    pnlBeneficiary.Visible = False
                End If

                LoadBeneGroup(0, 0)
                If ddlBeneficiaryGroup.Visible = True Then
                    LoadddlBeneficiary(objCompany.IsAllowOpenPayment, ddlBeneficiaryGroup.SelectedValue.ToString())
                Else
                    LoadddlBeneficiary(objCompany.IsAllowOpenPayment, 0)
                End If
            Else
                pnlBeneficiary.Visible = False

                LoadBeneGroup(0, 0)
                LoadddlBeneficiary(0, 0)

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnCancelOnlineForm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelOnlineForm.Click
        Try
            Response.Redirect(NavigateURL(41))
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub ddlBeneficiaryGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBeneficiaryGroup.SelectedIndexChanged
        Try

            Dim objCompany As New ICCompany
            objCompany.LoadByPrimaryKey(ddlCompany.SelectedValue.ToString())
            If objCompany.IsBeneRequired = True And objCompany.IsAllowOpenPayment = False Then
                LoadddlBeneficiary(False, ddlBeneficiaryGroup.SelectedValue.ToString())
            ElseIf objCompany.IsBeneRequired = True And objCompany.IsAllowOpenPayment = True Then
                LoadddlBeneficiary(True, ddlBeneficiaryGroup.SelectedValue.ToString())
            End If

            ddlBeneficiary_SelectedIndexChanged(ddlBeneficiary.SelectedValue, Nothing)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlBeneBank(Optional DisbursementMode As String = "")
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBeneBankName.Items.Clear()
            ddlBeneBankName.Items.Add(lit)
            ddlBeneBankName.AppendDataBoundItems = True
            If DisbursementMode <> "Direct Credit" Then
                ddlBeneBankName.DataSource = ICBankController.GetAllApprovedActiveNonPrincipalBanksAndIBFTEnabledBanks()
            Else
                ddlBeneBankName.DataSource = ICBankController.GetAllApprovedActivePrincipalBanks()
            End If
            ddlBeneBankName.DataTextField = "BankName"
            ddlBeneBankName.DataValueField = "BankCode"
            ddlBeneBankName.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Protected Sub ddlBeneficiary_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBeneficiary.SelectedIndexChanged
        Try
            If ddlBeneficiary.SelectedValue.ToString() <> "0" Then
                If ddlProductType.SelectedValue.ToString <> "0" Then
                    Dim objProductType As New ICProductType
                    objProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString())
                    LoadddlBeneBank(objProductType.DisbursementMode)
                    If ddlBeneficiary.SelectedValue = "-1" Or ddlBeneficiary.SelectedValue = "0" Then
                        ' SetViewFieldsVisibilityAndData(False, "0", "0", "0")
                        SetViewFieldsVisibilityAndData(False, "0")
                    Else
                        ' SetViewFieldsVisibilityAndData(True, ddlBeneficiary.SelectedValue.ToString(), "0", "0")
                        SetViewFieldsVisibilityAndData(True, ddlBeneficiary.SelectedValue.ToString())
                    End If
                End If
            Else
                ClearOnlineForm()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub txtAmountPKR_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAmountPKR.TextChanged
        Dim result As Double
        If txtAmountPKR.Text.ToString() <> "" Then
            txtAmountInWords.Text = ""

            If txtAmountPKR.Text.Contains(".") Then
                Dim str As String()
                Dim strBDeci, strADeci As String
                Dim TotAmt As String = ""
                str = txtAmountPKR.Text.Split(".")
                strBDeci = str(0).ToString()
                strADeci = str(1).ToString()
                TotAmt = strBDeci & strADeci
                If TotAmt.Length > 13 Then
                    txtAmountPKR.Text = Nothing
                    UIUtilities.ShowDialog(Me, "Online Instruction", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If strBDeci.Length > 11 Then
                    txtAmountPKR.Text = Nothing
                    UIUtilities.ShowDialog(Me, "Online Instruction", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Else
                If txtAmountPKR.Text.Length > 11 Then
                    txtAmountPKR.Text = Nothing
                    UIUtilities.ShowDialog(Me, "Online Instruction", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            End If

            Try
                If Double.TryParse(txtAmountPKR.Text.Trim(), result) = False Then
                    txtAmountPKR.Text = Nothing
                    UIUtilities.ShowDialog(Me, "Online Instruction", "Invalid Amount - Amount cannot be egreatr than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                txtAmountPKR.Text = Nothing
                UIUtilities.ShowDialog(Me, "Online Instruction", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End Try


            If CDbl(txtAmountPKR.Text.ToString()) < 1 Then
                UIUtilities.ShowDialog(Me, "Online Instruction", "Amount should be greater than or equal to 1.", ICBO.IC.Dialogmessagetype.Failure)
                txtAmountPKR.Text = ""
                Exit Sub
            End If
            'Standard Format
            'txtAmountWords.Text = NumToWord.AmtInWord(CDbl(txtAmountPKR.Text.ToString())).TrimStart().TrimEnd().Trim().ToString()
            txtAmountInWords.Text = NumToWord.SpellNumber(CDbl(txtAmountPKR.Text.ToString())).TrimStart().TrimEnd().Trim().ToString()
        End If
    End Sub

    Protected Sub btnCancelPrview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelPrview.Click
        Try
            ClearPreview()
            pnlOnLineFormPreview.Style.Add("display", "none")
            pnlOnlineFrom.Style.Remove("display")
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlDDPayableLocationProvince_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDDPayableLocationProvince.SelectedIndexChanged
        Try
            LoadddlDDDrawnLocationCity()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub rbScheduleTransaction_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbScheduleTransaction.CheckedChanged
        Try
            If rbScheduleTransaction.Checked = True Then
                tblScheduleTxn.Style.Remove("display")
                radScheduleTitle.Enabled = True
                rfvddlScheduleFrequency.Enabled = True
                cmpSheduleDates.Enabled = True
                rfvTransactionTime.Enabled = True

                radNoOfDay.Enabled = False
                radScheduleTitle.GetSettingByBehaviorID("reScheduleTitle").Validation.IsRequired = True
                radNoOfDay.GetSettingByBehaviorID("reNoOfDays").Validation.IsRequired = False
                ClearScheduleTxn()
                rfvScheduleEndDate.Enabled = True
                rfvScheduleStartDate.Enabled = True
                SetScheduleTransactionSection()

                txtStartDate.SelectedDate = Date.Now.AddDays(1)
                txtEndDate.SelectedDate = Date.Now.AddDays(1)



            Else
                txtStartDate.SelectedDate = Nothing
                txtEndDate.SelectedDate = Nothing
                tblScheduleTxn.Style.Add("display", "none")
                radScheduleTitle.Enabled = False
                rfvddlSpecificDay.Enabled = False
                radNoOfDay.Enabled = False
                cmpSheduleDates.Enabled = False
                rfvTransactionTime.Enabled = False
                rfvddlScheduleFrequency.Enabled = False
                radScheduleTitle.GetSettingByBehaviorID("reScheduleTitle").Validation.IsRequired = False
                radNoOfDay.GetSettingByBehaviorID("reNoOfDays").Validation.IsRequired = False
                rfvScheduleEndDate.Enabled = False
                rfvScheduleStartDate.Enabled = False
                ClearScheduleTxn()
                SetViewFieldsVisibility()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetScheduleTransactionSection()
        Try
            If rbScheduleTransaction.Checked = True Then
                Dim objICProductType As New ICProductType
                objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)
                If objICProductType.DisbursementMode.ToString <> "Cheque" And objICProductType.DisbursementMode.ToString <> "DD" Then
                    If DirectCast(Me.Control.FindControl("trValueDate"), HtmlTableRow).Visible = True Then
                        DirectCast(Me.Control.FindControl("trValueDate"), HtmlTableRow).Style.Add("display", "none")
                        If rfvValueDate.Enabled = True Then
                            rfvValueDate.Enabled = False
                            txtValueDate.SelectedDate = Nothing
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub ClearScheduleTxn()
        txtScheduleTitle.Text = ""
        radTimePicker.SelectedDate = Nothing
        ddlScheduleFrequency.ClearSelection()
        rblDaysOfMonth.ClearSelection()
        txtNoOfDays.Text = ""
        ddlSpecificDay.ClearSelection()
        txtStartDate.SelectedDate = Date.Now.AddDays(1)
        txtEndDate.SelectedDate = Date.Now.AddDays(1)
        tblDayType.Style.Add("display", "none")
        tblNoOfDays.Style.Add("display", "none")
        tblSpecificDay.Style.Add("display", "none")
    End Sub

    Protected Sub ddlScheduleFrequency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlScheduleFrequency.SelectedIndexChanged
        Try
            tblDayType.Style.Add("display", "none")
            tblNoOfDays.Style.Add("display", "none")
            tblSpecificDay.Style.Add("display", "none")
            rfvddlSpecificDay.Enabled = False
            radNoOfDay.Enabled = False
            txtNoOfDays.Text = ""
            ddlSpecificDay.ClearSelection()
            rfvrblrblDaysOfMonth.Enabled = False
            radScheduleTitle.GetSettingByBehaviorID("reScheduleTitle").Validation.IsRequired = True
            radNoOfDay.GetSettingByBehaviorID("reNoOfDays").Validation.IsRequired = False


            'sharedCalenderSceduleStartDate.RangeMinDate t= Date.Now
            'sharedCalenderEndScheduleDate.RangeMinDate = Date.Now
            'sharedCalenderEndScheduleDate.SelectedDate = Date.Now
            'sharedCalenderSceduleStartDate.SelectedDate = Date.Now
            txtStartDate.SelectedDate = Date.Now.AddDays(1)
            txtEndDate.SelectedDate = Date.Now.AddDays(1)

            If ddlScheduleFrequency.SelectedValue.ToString = "Daily" Then
                txtEndDate.SelectedDate = Nothing
                txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate)
                'sharedCalenderEndScheduleDate.RangeMinDate = CDate(txtStartDate.SelectedDate)
                txtEndDate.MinDate = CDate(txtStartDate.SelectedDate)

            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Monthly" Then
                txtEndDate.MinDate = Date.Now.AddDays(1).AddMonths(1)
                txtEndDate.SelectedDate = Date.Now.AddDays(1).AddMonths(1)
                txtEndDate.SelectedDate = Nothing
                txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddMonths(1).Date
                tblDayType.Style.Remove("display")
                rfvrblrblDaysOfMonth.Enabled = True
            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Number Of Days" Then
                tblNoOfDays.Style.Remove("display")
                radNoOfDay.Enabled = True
                radNoOfDay.GetSettingByBehaviorID("reNoOfDays").Validation.IsRequired = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub rblDaysOfMonth_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblDaysOfMonth.SelectedIndexChanged
        Try
            tblSpecificDay.Style.Add("display", "none")
            rfvddlSpecificDay.Enabled = False
            If rblDaysOfMonth.SelectedValue.ToString = "Specific Day" Then
                tblSpecificDay.Style.Remove("display")
                rfvddlSpecificDay.Enabled = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    'Protected Sub ddlSpecificDay_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSpecificDay.SelectedIndexChanged
    '    Try
    '        sharedCalenderEndScheduleDate.RangeMinDate = Date.Now
    '        sharedCalenderEndScheduleDate.SelectedDate = Date.Now

    '        If ddlSpecificDay.SelectedValue.ToString <> "0" Then
    '            sharedCalenderEndScheduleDate.RangeMinDate = Date.Now.AddDays(CInt(ddlSpecificDay.SelectedValue)).Date
    '            sharedCalenderEndScheduleDate.SelectedDate = Date.Now.AddDays(CInt(ddlSpecificDay.SelectedValue)).Date

    '        End If

    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

    Protected Sub txtNoOfDays_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNoOfDays.TextChanged
        Try
            'sharedCalenderEndScheduleDate.RangeMinDate = Date.Now.AddDays(1)
            'sharedCalenderEndScheduleDate.SelectedDate = Date.Now.AddDays(1)
            txtEndDate.MinDate = Date.Now.AddDays(1)
            txtEndDate.SelectedDate = Date.Now.AddDays(1)
            If txtNoOfDays.Text <> "" Then
                'sharedCalenderEndScheduleDate.RangeMinDate = Date.Now.AddDays(CInt(txtNoOfDays.Text)).Date
                'sharedCalenderEndScheduleDate.SelectedDate = Date.Now.AddDays(CInt(txtNoOfDays.Text)).Date

                txtEndDate.MinDate = Date.Now.AddDays(CInt(txtNoOfDays.Text)).Date
                txtEndDate.SelectedDate = Date.Now.AddDays(CInt(txtNoOfDays.Text)).Date
                txtEndDate.SelectedDate = Nothing
                txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddDays(CInt(txtNoOfDays.Text))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub txtStartDate_SelectedDateChanged(sender As Object, e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles txtStartDate.SelectedDateChanged
        Try
          

            If ddlScheduleFrequency.SelectedValue.ToString = "Daily" Then

                'sharedCalenderEndScheduleDate.RangeMinDate = CDate(Now.Date.AddDays(1))
                txtEndDate.MinDate = CDate(Now.Date.AddDays(1))
            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Monthly" Then
                'sharedCalenderEndScheduleDate.RangeMinDate = CDate(txtStartDate.SelectedDate).AddMonths(1).Date
                'sharedCalenderEndScheduleDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddMonths(1).Date
                txtEndDate.MinDate = CDate(txtStartDate.SelectedDate).AddMonths(1).Date
                txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddMonths(1).Date
            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Number Of Days" Then

                'sharedCalenderEndScheduleDate.RangeMinDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date
                'sharedCalenderEndScheduleDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date
                txtEndDate.MinDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date
                txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date

                If Not txtStartDate.SelectedDate Is Nothing Then
                    txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date
                End If
            Else
                'sharedCalenderEndScheduleDate.RangeMinDate = Date.Now.AddDays(1)
                'sharedCalenderEndScheduleDate.SelectedDate = Date.Now.AddDays(1)
                txtEndDate.MinDate = Date.Now.AddDays(1)
                txtEndDate.SelectedDate = Date.Now.AddDays(1)

                txtStartDate.SelectedDate = Date.Now.AddDays(1)
                txtEndDate.SelectedDate = Date.Now.AddDays(1)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub txtEndDate_SelectedDateChanged(sender As Object, e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles txtEndDate.SelectedDateChanged
        Try
            

            If ddlScheduleFrequency.SelectedValue.ToString = "Daily" Then

                'sharedCalenderEndScheduleDate.RangeMinDate = CDate(Now.Date.AddDays(1))
                txtEndDate.MinDate = CDate(Now.Date.AddDays(1))

            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Monthly" Then
                'sharedCalenderEndScheduleDate.RangeMinDate = CDate(txtStartDate.SelectedDate).AddMonths(1).Date
                'sharedCalenderEndScheduleDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddMonths(1).Date
                txtEndDate.MinDate = CDate(txtStartDate.SelectedDate).AddMonths(1).Date
                txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddMonths(1).Date
            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Number Of Days" Then
                'sharedCalenderEndScheduleDate.RangeMinDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date
                'sharedCalenderEndScheduleDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date
                txtEndDate.MinDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date
                txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date
                txtEndDate.SelectedDate = Nothing
                If Not txtStartDate.SelectedDate Is Nothing Then
                    txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date
                End If
            Else
                'sharedCalenderEndScheduleDate.RangeMinDate = Date.Now.AddDays(1)
                'sharedCalenderEndScheduleDate.SelectedDate = Date.Now.AddDays(1)
                txtEndDate.MinDate = Date.Now.AddDays(1)
                txtEndDate.SelectedDate = Date.Now.AddDays(1)
                txtStartDate.SelectedDate = Date.Now.AddDays(1)
                txtEndDate.SelectedDate = Date.Now.AddDays(1)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub txtBankAccountNumber_TextChanged(sender As Object, e As EventArgs) Handles txtBankAccountNumber.TextChanged
        Try
            Dim StrAccountDetails As String()
            Dim objICProductType As New ICProductType
            Dim objICNonPrincipalBank As New ICBank
            Dim objICPrincipalBank As New ICBank
            Dim AccountStatus As String = ""
            Dim RestraintsDetails As String = ""
            Dim BBAccNo As String = Nothing
            Dim PrincipalBankIMD As String = ""
            Dim NonPrincipalBankIMD As String = ""
            Dim CustomerID As String = Nothing
            Dim IBFTRRNo As String = Nothing
            If Not txtBankAccountNumber.Text.ToString = "" And txtBankAccountNumber.Text IsNot Nothing Then
                objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)
                If ddlBeneCBAccountType.SelectedValue.ToString = "GL" Then
                    Exit Sub
                End If
                If objICProductType.DisbursementMode = "Direct Credit" Then
                    If CBUtilities.IsNormalAccountNoOrIBAN(txtBankAccountNumber.Text.ToString) = True Then
                        If CBUtilities.ValidateIBAN(txtBankAccountNumber.Text.ToString) = True Then
                            BBAccNo = CBUtilities.GetBBANFromIBAN(txtBankAccountNumber.Text.ToString)
                            Try
                                StrAccountDetails = (CBUtilities.TitleFetch(BBAccNo, ICBO.CBUtilities.AccountType.RB, "Beneficiary Account Title", txtBankAccountNumber.Text.ToString).ToString.Split(";"))

                                txtAccountTitle.Text = StrAccountDetails(0).ToString
                                txtBeneAccountCurrency.Text = StrAccountDetails(2).ToString
                                txtBeneAccountBranchCode.Text = StrAccountDetails(1).ToString

                                ViewState("BeneAccountBranchCode") = ""
                                ViewState("BeneAccountCurrency") = ""
                                ViewState("AccountTitle") = ""
                                ViewState("BankAccountNumber") = ""
                                ViewState("BankAccountNumber") = txtBankAccountNumber.Text
                                ViewState("BeneAccountBranchCode") = txtBeneAccountBranchCode.Text
                                ViewState("BeneAccountCurrency") = txtBeneAccountCurrency.Text
                                ViewState("AccountTitle") = txtAccountTitle.Text



                            Catch ex As Exception
                                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                                txtAccountTitle.Text = ""
                                txtBeneAccountCurrency.Text = ""
                                txtBeneAccountBranchCode.Text = ""
                                Exit Sub
                            End Try
                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Invalid IBAN", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    Else
                        Try
                            StrAccountDetails = (CBUtilities.TitleFetch(txtBankAccountNumber.Text.ToString, ICBO.CBUtilities.AccountType.RB, "Beneficiary Account Title", txtBankAccountNumber.Text.ToString).ToString.Split(";"))
                            txtAccountTitle.Text = StrAccountDetails(0).ToString
                            txtBeneAccountCurrency.Text = StrAccountDetails(2).ToString
                            txtBeneAccountBranchCode.Text = StrAccountDetails(1).ToString
                        Catch ex As Exception
                            txtAccountTitle.Text = ""
                            txtBeneAccountCurrency.Text = ""
                            txtBeneAccountBranchCode.Text = ""
                            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End Try
                    End If
                ElseIf objICProductType.DisbursementMode = "Other Credit" Then
                    Try
                        CustomerID = ICUtilities.GetCustomerIDCountFromICSettings
                        If ddlBeneBankName.SelectedValue <> "0" Then
                            If ICBankController.GetBankByBankCode(ddlBeneBankName.SelectedValue.ToString) IsNot Nothing Then
                                objICNonPrincipalBank = ICBankController.GetBankByBankCode(ddlBeneBankName.SelectedValue.ToString)
                                NonPrincipalBankIMD = objICNonPrincipalBank.BankIMD
                            End If

                        End If

                        If ICBankController.GetPrincipleBank() IsNot Nothing Then
                            objICPrincipalBank = ICBankController.GetPrincipleBank()
                            PrincipalBankIMD = objICPrincipalBank.BankIMD
                        End If
                        If NonPrincipalBankIMD = "" Then
                            UIUtilities.ShowDialog(Me, "Error", "Please select Non-Principal bank", Dialogmessagetype.Failure)
                            txtBankAccountNumber.Text = ""
                            txtAccountTitle.Text = ""
                            Exit Sub
                        End If
                        If PrincipalBankIMD = "" Then
                            UIUtilities.ShowDialog(Me, "Error", "Invalid Principal Bank IMD", Dialogmessagetype.Failure)
                            txtAccountTitle.Text = ""
                            Exit Sub
                        End If
                        txtAccountTitle.Text = ""
                        txtAccountTitle.Text = CBUtilities.IBFTTitleFetch(NonPrincipalBankIMD, txtBankAccountNumber.Text, CBUtilities.AccountType.RB, "BeneAccountStatus", txtBankAccountNumber.Text.ToString, "", "", 0, "", IBFTRRNo, CustomerID, objICNonPrincipalBank.BankID, objICNonPrincipalBank.BankName, ddlAccPNature.SelectedValue.ToString.Split("-")(1) & ddlAccPNature.SelectedValue.ToString.Split("-")(0), PrincipalBankIMD)

                        ViewState("AccountTitle") = ""
                        ViewState("BankAccountNumber") = ""
                        ViewState("BankAccountNumber") = txtBankAccountNumber.Text
                       





                    Catch ex As Exception
                        txtAccountTitle.Text = ""
                        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End Try
                End If

            Else
                'UIUtilities.ShowDialog(Me, "Error", "Please enter valid account number.", ICBO.IC.Dialogmessagetype.Failure)
                txtAccountTitle.Text = ""
                txtBeneAccountCurrency.Text = ""
                txtBeneAccountBranchCode.Text = ""
                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlBeneBankName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBeneBankName.SelectedIndexChanged

        Try
            Dim objICProductType As New ICProductType
            Dim objICNonPrincipalBank As New ICBank
            Dim objICPrincipalBank As New ICBank
            Dim CustomerID As String = Nothing
            Dim NonPrincipalBankIMD As String = Nothing
            Dim PrincipalBankIMD As String = Nothing
            Dim IBFTRRNo As String = Nothing
            objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)
            If objICProductType.DisbursementMode = "Other Credit" Then
                If ddlBeneBankName.SelectedValue.ToString <> "0" Then

                    CustomerID = ICUtilities.GetCustomerIDCountFromICSettings
                    If ddlBeneBankName.SelectedValue <> "0" Then
                        If ICBankController.GetBankByBankCode(ddlBeneBankName.SelectedValue.ToString) IsNot Nothing Then
                            objICNonPrincipalBank = ICBankController.GetBankByBankCode(ddlBeneBankName.SelectedValue.ToString)
                            NonPrincipalBankIMD = objICNonPrincipalBank.BankIMD
                        End If

                    End If

                    If ICBankController.GetPrincipleBank() IsNot Nothing Then
                        objICPrincipalBank = ICBankController.GetPrincipleBank()
                        PrincipalBankIMD = objICPrincipalBank.BankIMD
                    End If
                    If NonPrincipalBankIMD = "" Then
                        UIUtilities.ShowDialog(Me, "Error", "Invalid Non-Principal Bank IMD", Dialogmessagetype.Failure)
                        txtAccountTitle.Text = ""
                        Exit Sub
                    End If
                    If PrincipalBankIMD = "" Then
                        txtAccountTitle.Text = ""
                        UIUtilities.ShowDialog(Me, "Error", "Invalid Principal Bank IMD", Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    txtAccountTitle.Text = ""
                    txtAccountTitle.Text = CBUtilities.IBFTTitleFetch(NonPrincipalBankIMD, txtBankAccountNumber.Text, CBUtilities.AccountType.RB, "BeneAccountStatus", txtBankAccountNumber.Text.ToString, "", "", 0, "", IBFTRRNo, CustomerID, objICNonPrincipalBank.BankID, objICNonPrincipalBank.BankName, ddlAccPNature.SelectedValue.ToString.Split("-")(1) & ddlAccPNature.SelectedValue.ToString.Split("-")(0), PrincipalBankIMD)

                Else
                    txtAccountTitle.Text = ""
                End If
            End If

        Catch ex As Exception
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Exit Sub
        End Try
    End Sub

    Protected Sub ddlDDPayableLocationCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDDPayableLocationCity.SelectedIndexChanged
        Try
            If ddlDDPayableLocationCity.SelectedValue.ToString <> "0" Then
                Dim dtBankBranches As New DataTable
                dtBankBranches = ICOfficeController.GetAllPrinciaplBankBranchesByCityCode(ddlDDPayableLocationCity.SelectedValue.ToString)
                If dtBankBranches.Rows.Count > 0 Then
                    UIUtilities.ShowDialog(Me, "Error", "Principal Bank branch exists in specified city. DD cannot be made", ICBO.IC.Dialogmessagetype.Failure)
                    LoadddlDDDrawnLocationCity()
                    Exit Sub
                End If
            Else
                LoadddlDDDrawnLocationCity()
                Exit Sub
            End If
        Catch ex As Exception
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Exit Sub
        End Try
    End Sub

    Protected Sub ddlUBPCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUBPCompany.SelectedIndexChanged
        Try
            Dim objICCompany As New ICUBPSCompany

            If ddlUBPCompany.SelectedValue.ToString = "0" Then
                lblUBPReferenceField.Text = "UBP Reference"
                SetOnlineFormForBillPaymentOnIndexChange()
            Else
                objICCompany.LoadByPrimaryKey(ddlUBPCompany.SelectedValue.ToString)
                lblUBPReferenceField.Text = objICCompany.ReferenceFieldCaption
                If objICCompany.MultiplesOfAmount = "Only Billed Amount" Then
                    DirectCast(Me.Control.FindControl("trAmountPKR"), HtmlTableRow).Style.Add("display", "none")
                    DirectCast(Me.Control.FindControl("trAmountPKRPreview"), HtmlTableRow).Style.Add("display", "none")
                    DirectCast(Me.Control.FindControl("trAmountInWords"), HtmlTableRow).Style.Add("display", "none")
                    DirectCast(Me.Control.FindControl("trAmountInWordPreview"), HtmlTableRow).Style.Add("display", "none")
                    radAmountPKR.Enabled = False
                    radAmountPKR.GetSettingByBehaviorID("reAmountPKR").Validation.IsRequired = False
                    lblReqAmountPKR.Visible = False
                    radAmountInWords.Enabled = False
                    radAmountInWords.GetSettingByBehaviorID("reAmountInWords").Validation.IsRequired = False
                    lblReqAmountInWords.Visible = False
                    txtAmountPKR.Text = ""
                    txtAmountInWords.Text = ""
                Else

                    SetOnlineFormForBillPaymentOnIndexChange()
                End If
            End If
        Catch ex As Exception
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Exit Sub
        End Try
    End Sub
    Private Sub SetOnlineFormForBillPaymentOnIndexChange()
        Dim objOnlineFormSettingsColl As New ICOnlineFormSettingsCollection
        Dim objICAPNature As New ICAccountsPaymentNature
        Dim objICProductType As New ICProductType
        objICAPNature.LoadByPrimaryKey(ddlAccPNature.SelectedValue.ToString.Split("-")(0), ddlAccPNature.SelectedValue.ToString.Split("-")(1), ddlAccPNature.SelectedValue.ToString.Split("-")(2), ddlAccPNature.SelectedValue.ToString.Split("-")(3))
        objOnlineFormSettingsColl = ICInstructionController.GetAllOnlineFormSettingFieldsTaggedWithAPNautueAndProductType(objICAPNature, ddlCompany.SelectedValue.ToString, ddlProductType.SelectedValue.ToString)
        objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)


        For Each objICOnlineFormSetting As ICOnlineFormSettings In objOnlineFormSettingsColl
            Select Case objICOnlineFormSetting.FieldID.ToString
                Case "3"
                    DirectCast(Me.Control.FindControl("trAmountPKR"), HtmlTableRow).Style.Remove("display")
                    DirectCast(Me.Control.FindControl("trAmountPKRPreview"), HtmlTableRow).Style.Remove("display")
                    DirectCast(Me.Control.FindControl("trAmountInWords"), HtmlTableRow).Style.Remove("display")
                    DirectCast(Me.Control.FindControl("trAmountInWordPreview"), HtmlTableRow).Style.Remove("display")
                    radAmountPKR.Enabled = True
                    radAmountPKR.GetSettingByBehaviorID("reAmountPKR").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                    lblReqAmountPKR.Visible = objICOnlineFormSetting.IsRequired
                    lblReqAmountInWords.Visible = objICOnlineFormSetting.IsRequired
                    radAmountInWords.Enabled = True
                    radAmountInWords.GetSettingByBehaviorID("reAmountInWords").Validation.IsRequired = True

                    txtAmountPKR.Text = ""
                    txtAmountInWords.Text = ""
            End Select
        Next
    End Sub

    Protected Sub ddlBeneCBAccountType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBeneCBAccountType.SelectedIndexChanged
        Try
            SetViewFieldsByBeneAccountType(ddlBeneCBAccountType.SelectedValue.ToString)
            'Dim objICProductType As New ICProductType
            'objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)
            'txtBankAccountNumber.AutoPostBack = False
            'If objICProductType.DisbursementMode = "Direct Credit" Or objICProductType.DisbursementMode = "Other Credit" Then
            '    If ddlBeneCBAccountType.SelectedValue.ToString = "0" Or ddlBeneCBAccountType.SelectedValue.ToString = "RB" Then
            '        txtBankAccountNumber.AutoPostBack = True
            '        txtAccountTitle.ReadOnly = True
            '        txtBeneAccountCurrency.ReadOnly = True
            '        txtBeneAccountBranchCode.ReadOnly = True

            '    Else
            '        txtAccountTitle.Text = ""
            '        txtBeneAccountBranchCode.Text = ""
            '        txtBeneAccountCurrency.Text = ""

            '        txtAccountTitle.ReadOnly = False
            '        txtBeneAccountCurrency.ReadOnly = False
            '        txtBeneAccountBranchCode.ReadOnly = False
            '    End If
            'End If
        Catch ex As Exception
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Exit Sub
        End Try
    End Sub
    Private Sub LoadddlGLAccountBranchCode()
        Try
            Dim lit As New ListItem
            Dim dt As New DataTable
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlGLAccountBranchCode.Items.Clear()
            ddlGLAccountBranchCode.Items.Add(lit)
            ddlGLAccountBranchCode.AppendDataBoundItems = True
            dt = ICOfficeController.GetPrincipalBankBranchActiveAndApprove
            For Each dr As DataRow In dt.Rows
                lit = New ListItem
                lit.Text = dr("OfficeCode") & "-" & dr("OfficeName")
                lit.Value = dr("OfficeCode")
                ddlGLAccountBranchCode.Items.Add(lit)
            Next
            ddlGLAccountBranchCode.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub SetViewFieldsByBeneAccountType(ByVal AccountType As String)
        txtBankAccountNumber.AutoPostBack = False
        txtAccountTitle.ReadOnly = False
        txtBeneAccountCurrency.ReadOnly = False
        txtBeneAccountBranchCode.ReadOnly = False
        txtBeneAccountBranchCode.Text = ""
        Dim objICProductType As New ICProductType
        Dim objOnlineFormSettingsColl As New ICOnlineFormSettingsCollection
        Dim objICAPNature As New ICAccountsPaymentNature
        objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)
        objICAPNature.LoadByPrimaryKey(ddlAccPNature.SelectedValue.ToString.Split("-")(0), ddlAccPNature.SelectedValue.ToString.Split("-")(1), ddlAccPNature.SelectedValue.ToString.Split("-")(2), ddlAccPNature.SelectedValue.ToString.Split("-")(3))
        DirectCast(Me.Control.FindControl("trGLAccountBranchCode"), HtmlTableRow).Style.Add("display", "none")
        rfvGLAccountBranchCode.Enabled = False
        lblReqGLAccountBranchCode.Visible = False
        ddlGLAccountBranchCode.ClearSelection()
        objOnlineFormSettingsColl = ICInstructionController.GetAllOnlineFormSettingFieldsTaggedWithAPNautueAndProductType(objICAPNature, ddlCompany.SelectedValue.ToString, ddlProductType.SelectedValue.ToString)

        If AccountType = "RB" Or AccountType = "0" Then
            For Each objICOnlineFormSetting As ICOnlineFormSettings In objOnlineFormSettingsColl
                Select Case objICOnlineFormSetting.FieldID.ToString
                    Case "5"
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBankAccountNumber"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBankAccountNumberPreview"), HtmlTableRow).Style.Remove("display")
                            radBankAccountNumber.Enabled = True
                            radBankAccountNumber.GetSettingByBehaviorID("reBankAccountNumber").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            lblReqBankAccountNumber.Visible = objICOnlineFormSetting.IsRequired
                            If objICProductType.DisbursementMode = "Direct Credit" Then
                                txtBankAccountNumber.AutoPostBack = True
                                ''Beneficiary Account Title

                                DirectCast(Me.Control.FindControl("trAccountTitle"), HtmlTableRow).Style.Remove("display")
                                DirectCast(Me.Control.FindControl("trAccountTitlePreview"), HtmlTableRow).Style.Remove("display")
                                radAccountTitle.Enabled = True
                                radAccountTitle.GetSettingByBehaviorID("reAccountTitle").Validation.IsRequired = True
                                lblReqAccountTitle.Visible = False


                                ''Beneficiary Branch Code

                                DirectCast(Me.Control.FindControl("trBeneAccountBranchCode"), HtmlTableRow).Style.Remove("display")
                                DirectCast(Me.Control.FindControl("trBeneAccountBranchCodePreview"), HtmlTableRow).Style.Remove("display")
                                radBeneAccountBranchCode.Enabled = True
                                radBeneAccountBranchCode.GetSettingByBehaviorID("reBeneAccountBranchCode").Validation.IsRequired = True
                                lblReqBeneAccountBranchCode.Visible = False

                                ''Beneficiary Account Currency

                                DirectCast(Me.Control.FindControl("trBeneAccountCurrency"), HtmlTableRow).Style.Remove("display")
                                DirectCast(Me.Control.FindControl("trBeneAccountCurrencyPreview"), HtmlTableRow).Style.Remove("display")
                                radBeneAccountCurrency.Enabled = True
                                radBeneAccountCurrency.GetSettingByBehaviorID("reBeneAccountCurrency").Validation.IsRequired = True
                                lblReqBeneAccountCurrency.Visible = False



                                ''Enable False text boxes
                                txtAccountTitle.ReadOnly = True
                                txtBeneAccountCurrency.ReadOnly = True
                                txtBeneAccountBranchCode.ReadOnly = True

                                txtBeneAccountCurrency.Text = ""

                            End If
                        End If

                End Select
            Next
        ElseIf AccountType = "GL" Then
            DirectCast(Me.Control.FindControl("trGLAccountBranchCode"), HtmlTableRow).Style.Remove("display")
            rfvGLAccountBranchCode.Enabled = True
            lblReqGLAccountBranchCode.Visible = True
            LoadddlGLAccountBranchCode()

            For Each objICOnlineFormSetting As ICOnlineFormSettings In objOnlineFormSettingsColl
                Select Case objICOnlineFormSetting.FieldID.ToString
                    Case "5"
                        If objICOnlineFormSetting.IsVisible = True Then
                            DirectCast(Me.Control.FindControl("trBankAccountNumber"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBankAccountNumberPreview"), HtmlTableRow).Style.Remove("display")
                            radBankAccountNumber.Enabled = True
                            radBankAccountNumber.GetSettingByBehaviorID("reBankAccountNumber").Validation.IsRequired = objICOnlineFormSetting.IsRequired
                            lblReqBankAccountNumber.Visible = objICOnlineFormSetting.IsRequired
                            If objICProductType.DisbursementMode = "Direct Credit" Then
                                txtBankAccountNumber.AutoPostBack = False
                                txtBeneAccountCurrency.ReadOnly = True

                                ''Beneficiary Account Title

                                DirectCast(Me.Control.FindControl("trAccountTitle"), HtmlTableRow).Style.Remove("display")
                                DirectCast(Me.Control.FindControl("trAccountTitlePreview"), HtmlTableRow).Style.Remove("display")
                                radAccountTitle.Enabled = True
                                radAccountTitle.GetSettingByBehaviorID("reAccountTitle").Validation.IsRequired = True
                                lblReqAccountTitle.Visible = objICOnlineFormSetting.IsRequired


                                ''Beneficiary Branch Code

                                DirectCast(Me.Control.FindControl("trBeneAccountBranchCode"), HtmlTableRow).Style.Add("display", "none")
                                DirectCast(Me.Control.FindControl("trBeneAccountBranchCodePreview"), HtmlTableRow).Style.Remove("display")
                                radBeneAccountBranchCode.Enabled = False
                                radBeneAccountBranchCode.GetSettingByBehaviorID("reBeneAccountBranchCode").Validation.IsRequired = False
                                lblReqBeneAccountBranchCode.Visible = False



                                ''Beneficiary Account Currency

                                DirectCast(Me.Control.FindControl("trBeneAccountCurrency"), HtmlTableRow).Style.Remove("display")
                                DirectCast(Me.Control.FindControl("trBeneAccountCurrencyPreview"), HtmlTableRow).Style.Remove("display")
                                radBeneAccountCurrency.Enabled = True
                                radBeneAccountCurrency.GetSettingByBehaviorID("reBeneAccountCurrency").Validation.IsRequired = True
                                lblReqBeneAccountCurrency.Visible = objICOnlineFormSetting.IsRequired

                                txtBeneAccountCurrency.Text = ""
                                txtBeneAccountCurrency.Text = "PKR"






                            End If
                        End If

                End Select
            Next
        End If
    End Sub
End Class
