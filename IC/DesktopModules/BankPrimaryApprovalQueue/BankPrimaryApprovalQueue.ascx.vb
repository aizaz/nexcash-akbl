﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_BankPrimaryApprovalQueue_BankPrimaryApprovalQueue
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrICAssignUserRolsID As New ArrayList
    Private ArrICAssignedOfficeID As New ArrayList
    Private ArrICAssignedStatus As New ArrayList
    Private ArrICAssignedPaymentModes As New ArrayList
    Private htRights As Hashtable

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init

        'Register AJAX components if available
        If DotNetNuke.Framework.AJAX.IsInstalled Then
            'Register the script manager
            DotNetNuke.Framework.AJAX.RegisterScriptManager()

            'Wrap the pnlAjaxUpdate within an update panel with progress control
            'DotNetNuke.Framework.AJAX.WrapUpdatePanelControl(Panel1, True)




            'Register the btnPostbackTimeUpdate button as a postback control
            ' DotNetNuke.Framework.AJAX.RegisterPostBackControl(Me.btnsubmit2)


        End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()
            lblError.Visible = False
            lblMessageForQueue.Style.Add("display", "none")
            If Page.IsPostBack = False Then

                lblMessageForQueue.Text = ICUtilities.GetSettingValue("ErrorMessage")
                pnl2FACode.Style.Add("display", "none")

                btnApprove.Attributes.Item("onclick") = "if (Page_ClientValidate('Verify')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnApprove, Nothing).ToString() & " } "

                btnCancelInstructions.Attributes.Item("onclick") = "if (Page_ClientValidate('Verify')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";document.getElementById('<%=lblMessageForQueue.ClientID%>').style.display = 'block';this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnCancelInstructions, Nothing).ToString() & " } "

               
                btnClose2FA.OnClientClick = "CloseDialog2FA('" & NavigateURL() & "');return false;"
                DesignDts()
                ViewState("htRights") = Nothing
                GetPageAccessRights()

                'SetArraListAssignedOfficeIDs()
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
                rbtnlstDateSelection.SelectedValue = "Creation Date"
                RefreshPage()
               
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub RefreshPage()
        LoadddlCompany()

        LoadddlAccountNumbers()
        LoadddlAccountPaymentNatureByUserID()
        LoadddlProductTypeByAccountPaymentNature()
        ClearAllTextBoxes()
        FillDesignedDtByAPNatureByAssignedRole()


        Dim dtPageLoad As New DataTable
        dtPageLoad = DirectCast(ViewState("AccountNumber"), DataTable)
        If dtPageLoad.Rows.Count > 0 Then
            LoadddlBatcNumber()
            LoadgvAccountPaymentNatureProductType(True)
        Else
            UIUtilities.ShowDialog(Me, "Error", "You do not have access to any transactional data. Please contact Al Baraka Administrator.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
            Exit Sub
        End If
    End Sub
    Private Sub DesignDts()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim dtOfficeID As New DataTable
        dtAccountNumber.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("BranchCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("Currency", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("PaymentNatureCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("OfficeID", GetType(System.String)))
        ViewState("AccountNumber") = dtAccountNumber
    End Sub
    Private Sub FillDesignedDtByAPNatureByAssignedRole()
      
        Dim dtAssignedAPNatureOfficeID As New DataTable
        dtAssignedAPNatureOfficeID.Rows.Clear()
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserSecond(Me.UserId.ToString, ArrICAssignUserRolsID)
        dtAssignedAPNatureOfficeID.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAssignedAPNatureOfficeID.Rows
            dr("DropDown") = "False"
        Next
        ViewState("AccountNumber") = Nothing
        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID

        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Bank Approval Queue")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Bank Approval Queue") = True Then
                    ArrICAssignUserRolsID.Add(RoleID.ToString)
                End If
            Next
        End If
    End Sub
    Private Sub SetArraListAssignedOfficeIDs()
        Dim dt As New DataTable
        Dim objICUser As New ICUser
        objICUser.es.Connection.CommandTimeout = 3600
        objICUser.LoadByPrimaryKey(Me.UserId)
        ArrICAssignedOfficeID.Clear()
        dt = ICUserRolesPaymentNatureController.GetAllTaggedLocationsWithUserByUSerIDAndRoleID(Me.UserId.ToString, ArrICAssignUserRolsID)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("OfficeID").ToString
                ArrICAssignedOfficeID.Add(RoleID.ToString)
            Next
        End If
        ArrICAssignedOfficeID.Add(objICUser.OfficeCode)
    End Sub
    Private Sub SetArraListAssignedStatus()
        Dim dt As New DataTable
        ArrICAssignedStatus.Clear()
        If CBool(htRights("Primary Approval")) = True Then
            ArrICAssignedStatus.Add(16)
        End If
        If CBool(htRights("Secondary Approval")) = True Then
            ArrICAssignedStatus.Add(18)
        End If
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, "", ArrICAssignUserRolsID)
            ddlCompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlCompany.Items.Clear()
                    ddlCompany.Items.Add(lit)
                    lit.Selected = True
                    ddlCompany.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
                ddlCompany.DataSource = dt
                ddlCompany.DataTextField = "CompanyName"
                ddlCompany.DataValueField = "CompanyCode"
                ddlCompany.DataBind()
                ddlCompany.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccountPaymentNatureByUserID()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlAcccountPaymentNature.Items.Clear()
            ddlAcccountPaymentNature.Items.Add(lit)
            lit.Selected = True
            ddlAcccountPaymentNature.AppendDataBoundItems = True
            ddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForDropDown(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
            ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
            ddlAcccountPaymentNature.DataBind()
            'Dim lit As New ListItem
            'lit.Value = "0"
            'lit.Text = "-- All --"
            'ddlAcccountPaymentNature.Items.Clear()
            'ddlAcccountPaymentNature.Items.Add(lit)
            'lit.Selected = True
            'ddlAcccountPaymentNature.AppendDataBoundItems = True
            'ddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUser(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
            'ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
            'ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
            'ddlAcccountPaymentNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlAccountNumbers()
        Try
            Dim lit As New ListItem
            lit.Text = "-- Please Select --"
            lit.Value = "0"
            ddlAccount.Items.Clear()
            ddlAccount.Items.Add(lit)
            lit.Selected = True
            ddlAccount.AppendDataBoundItems = True
            ddlAccount.DataSource = ICInstructionController.GetAccountNumbersTaggedWithUserSecond(Me.UserId.ToString, ArrICAssignUserRolsID, ddlCompany.SelectedValue.ToString)
            ddlAccount.DataTextField = "MainAccountNo"
            ddlAccount.DataValueField = "AccountNumber"
            ddlAccount.DataBind()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub LoadddlBatcNumber()
        Try
           
            Dim dtAccountNumber As New DataTable
            dtAccountNumber = ViewState("AccountNumber")

            SetArraListAssignedStatus()
            Dim lit As New ListItem
            lit.Text = "-- All --"
            lit.Value = "0"
            ddlBatch.Items.Clear()
            ddlBatch.Items.Add(lit)
            lit.Selected = True
            ddlBatch.AppendDataBoundItems = True
            ddlBatch.DataSource = ICInstructionController.GetAllTaggedBatchNumbersByCompanyCodeWithStatusForPrinting(ArrICAssignedPaymentModes, dtAccountNumber, ddlCompany.SelectedValue.ToString, ArrICAssignedStatus, Me.UserId.ToString)
            ddlBatch.DataTextField = "FileBatchNoName"
            ddlBatch.DataValueField = "FileBatchNo"
            ddlBatch.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlProductTypeByAccountPaymentNature()
        Try
            Dim lit As New ListItem
            Dim objICAPNature As New ICAccountsPaymentNature
            objICAPNature.es.Connection.CommandTimeout = 3600
            ddlProductType.Items.Clear()
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlProductType.Items.Add(lit)
            ddlProductType.AppendDataBoundItems = True
            If ddlAcccountPaymentNature.SelectedValue.ToString <> "0" Then
                If objICAPNature.LoadByPrimaryKey(ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)) Then
                    ddlProductType.DataSource = ICAccountPaymentNatureProductTypeController.GetAllProductTypesByAccountAndPaymentNature(objICAPNature)
                    ddlProductType.DataTextField = "ProductTypeName"
                    ddlProductType.DataValueField = "ProductTypeCode"
                    ddlProductType.DataBind()
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadgvAccountPaymentNatureProductType(ByVal DoDataBind As Boolean)
        Try
           
            Dim DateType, AccountNumber, BranchCode, Currency, PaymentNatureCode, CompanyCode, ProductTypeCode, InstrumentNo, InstructionNo, BatchCode, ReferenceNo As String
            Dim FromDate, ToDate As String
            Dim dtAccountNumber As New DataTable
            dtAccountNumber = ViewState("AccountNumber")
            DateType = ""
            AccountNumber = ""
            BranchCode = ""
            Currency = ""
            Currency = ""
            PaymentNatureCode = ""
            CompanyCode = "0"
            ProductTypeCode = ""
            InstructionNo = ""
            InstrumentNo = ""
            BatchCode = ""
            ReferenceNo = ""
            FromDate = Nothing
            ToDate = Nothing
            If rbtnlstDateSelection.SelectedValue = "Creation Date" Then
                DateType = "Creation Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Value Date" Then
                DateType = "Value Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Verfication Date" Then
                DateType = "Verfication Date"
            End If

            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            End If
            If ddlProductType.SelectedValue.ToString <> "0" Then
                ProductTypeCode = ddlProductType.SelectedValue.ToString
            End If
            If txtInstrumentNo.Text.ToString <> "" Then
                InstrumentNo = txtInstrumentNo.Text.ToString
            End If
            If txtInstructionNo.Text.ToString <> "" Then
                InstructionNo = txtInstructionNo.Text.ToString
            End If
            If txtReferenceNo.Text.ToString <> "" Then
                ReferenceNo = txtReferenceNo.Text.ToString
            End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                FromDate = raddpCreationFromDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If Not raddpCreationToDate.SelectedDate Is Nothing Then
                ToDate = raddpCreationToDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If ddlBatch.SelectedValue <> "0" Then
                BatchCode = ddlBatch.SelectedValue.ToString
            End If
            SetArraListAssignedStatus()
            SetArraListAssignedOfficeIDs()

            ICInstructionController.GetAllInstructionsForRadGridForPrintingWithUserLocation(DateType, FromDate, ToDate, dtAccountNumber, BatchCode, ProductTypeCode, InstructionNo, InstrumentNo, ReferenceNo, Me.gvAuthorization.CurrentPageIndex + 1, Me.gvAuthorization.PageSize, Me.gvAuthorization, DoDataBind, ArrICAssignedStatus, Me.UserId.ToString, 0, "", ArrICAssignedPaymentModes, CompanyCode)
            ClearSummaryAndAccountBalance()
            btnApprove.Visible = False
            btnCancelInstructions.Visible = False
            If gvAuthorization.Items.Count > 0 Then
                gvAuthorization.Visible = True
                lblNRF.Visible = False
                gvAuthorization.Columns(0).Visible = False
                gvAuthorization.Columns(1).Visible = False
                gvAuthorization.Columns(2).Visible = False

                If CBool(htRights("Primary Approval")) = True Then
                    btnApprove.Visible = True
                    gvAuthorization.Columns(0).Visible = CBool(htRights("Primary Approval"))
                End If
                If CBool(htRights("Secondary Approval")) = True Then
                    btnApprove.Visible = True
                    gvAuthorization.Columns(1).Visible = CBool(htRights("Secondary Approval"))
                End If
                If CBool(htRights("Cancel")) = True Then

                    gvAuthorization.Columns(2).Visible = CBool(htRights("Cancel"))
                    btnCancelInstructions.Visible = CBool(htRights("Cancel"))
                End If

                'SetJavaScript()
            Else
                lblNRF.Visible = True
                gvAuthorization.Visible = False
                btnApprove.Visible = False
                btnCancelInstructions.Visible = False
            End If
            pnl2FACode.Style.Add("display", "none")
            rim2FA.Enabled = False
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ClearSummaryAndAccountBalance()
        txtTnxCount.Text = ""
        txtTotalAmount.Text = ""
        txtAvailableBalance.Text = ""
        LoadddlAccountNumbers()
    End Sub
    'Private Sub SetJavaScript()
    '    Dim imgBtn As ImageButton
    '    'If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Then
    '    Dim AppPath As String = DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
    '    For Each gvAuthorizationRow As GridDataItem In gvAuthorization.Items
    '        imgBtn = New ImageButton
    '        imgBtn = DirectCast(gvAuthorizationRow.Cells(11).FindControl("ibButton"), ImageButton)
    '        imgBtn.OnClientClick = "showurldialog('Instruction Details','" & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & imgBtn.CommandArgument.ToString() & "' ,true,700,650);return false;"
    '    Next
    '    'End If
    'End Sub
    Private Sub ClearAllTextBoxes()
        txtRemarks.Text = ""
        txtAvailableBalance.Text = ""
        txtInstructionNo.Text = ""
        txtInstrumentNo.Text = ""
        txtReferenceNo.Text = ""
        txtAvailableBalance.Text = ""
        pnl2FACode.Style.Add("display", "none")
        txt2FACode.Text = Nothing
        txtTnxCount.Text = ""
        txtTotalAmount.Text = ""
    End Sub
    Private Function CheckgvInstructionAuthentication(ByVal ForApproval As Boolean) As Boolean
        Try

            Dim chkSelectAllPrimary As CheckBox
            Dim chkSelectAllSecondary As CheckBox
            Dim chkSelectCancel As CheckBox
            Dim Result As Boolean = False
            For Each rowGVInstruction In gvAuthorization.Items
                chkSelectAllPrimary = New CheckBox
                chkSelectAllSecondary = New CheckBox
                chkSelectCancel = New CheckBox
                chkSelectAllPrimary = DirectCast(rowGVInstruction.Cells(0).FindControl("chkSelectPrimary"), CheckBox)
                chkSelectAllSecondary = DirectCast(rowGVInstruction.Cells(1).FindControl("chkSelectSecondary"), CheckBox)
                chkSelectCancel = DirectCast(rowGVInstruction.Cells(2).FindControl("chkSelectCancel"), CheckBox)

                If ((chkSelectAllPrimary.Checked = True And chkSelectAllPrimary.Enabled = True) Or (chkSelectAllSecondary.Checked = True And chkSelectAllSecondary.Enabled = True)) Or (chkSelectCancel.Checked = True And chkSelectCancel.Visible = True And chkSelectCancel.Enabled = True) Then
                    Result = True
                    Exit For
                End If


            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try

            LoadddlAccountNumbers()
            LoadddlAccountPaymentNatureByUserID()
            LoadddlBatcNumber()
            LoadddlProductTypeByAccountPaymentNature()

            LoadgvAccountPaymentNatureProductType(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlAcccountPaymentNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcccountPaymentNature.SelectedIndexChanged
        If ddlAcccountPaymentNature.SelectedValue.ToString = "0" Then
            SetArraListRoleID()

            LoadddlAccountNumbers()
            LoadddlProductTypeByAccountPaymentNature()

            FillDesignedDtByAPNatureByAssignedRole()
            LoadddlBatcNumber()
            LoadgvAccountPaymentNatureProductType(True)
        Else
FillDesignedDtByAPNatureByAssignedRole()
            LoadddlAccountNumbers()
            LoadddlProductTypeByAccountPaymentNature()

            SetViewStateDtOnAPNatureIndexChnage()
            LoadddlBatcNumber()
            LoadgvAccountPaymentNatureProductType(True)
        End If
    End Sub
    Private Sub SetViewStateDtOnAPNatureIndexChnage()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim AccountNumber, BranchCode, Currency, PaymentNatureCode As String
        Dim ArrayListOfficeID As New ArrayList
        Dim drAccountNo As DataRow
        AccountNumber = Nothing
        BranchCode = Nothing
        Currency = Nothing
        PaymentNatureCode = Nothing
        dtAccountNumber = ViewState("AccountNumber")
        ViewState("AccountNumber") = Nothing
        AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0)
        BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1)
        Currency = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2)
        PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)
        For Each dr As DataRow In dtAccountNumber.Rows
            If dr("AccountNumber").ToString.Split("-")(0) = AccountNumber And dr("AccountNumber").ToString.Split("-")(1) = BranchCode And dr("AccountNumber").ToString.Split("-")(2) = Currency And dr("PaymentNature").ToString = PaymentNatureCode Then
                ArrayListOfficeID.Add(dr("OfficeID").ToString)
            End If
        Next
        dtAccountNumber.Rows.Clear()
        If ArrayListOfficeID.Count > 0 Then
            For Each OfficeID In ArrayListOfficeID
                drAccountNo = dtAccountNumber.NewRow()
                drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
                drAccountNo("PaymentNature") = PaymentNatureCode
                drAccountNo("OfficeID") = OfficeID
                dtAccountNumber.Rows.Add(drAccountNo)
            Next
        Else
            drAccountNo = dtAccountNumber.NewRow()
            drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
            drAccountNo("PaymentNature") = PaymentNatureCode
            drAccountNo("OfficeID") = "0"
            dtAccountNumber.Rows.Add(drAccountNo)
        End If
        'dtAccountNumber.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAccountNumber.Rows
            dr("DropDown") = "True"
        Next
        ViewState("AccountNumber") = dtAccountNumber





    End Sub
    Protected Sub ddlBatch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBatch.SelectedIndexChanged
        Try
            SetArraListRoleID()
            LoadddlAccountNumbers()
            LoadddlProductTypeByAccountPaymentNature()

            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged
        Try
            SetArraListRoleID()



            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAuthorization_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAuthorization.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAuthorization_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemDataBound


        Dim chkProcessPrimary As CheckBox
        Dim chkProcessSecondary As CheckBox
        Dim chkProcessAllCancel As CheckBox
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            If CBool(htRights("Primary Approval")) = True And CBool(htRights("Secondary Approval")) = True And CBool(htRights("Cancel")) = True Then
                Dim chkProcessAllPrimary As New CheckBox
                Dim chkProcessAllSecondary As New CheckBox
                chkProcessAllCancel = New CheckBox
                chkProcessAllPrimary = DirectCast(e.Item.Cells(0).FindControl("chkSelectAllPrimary"), CheckBox)
                chkProcessAllSecondary = DirectCast(e.Item.Cells(1).FindControl("chkSelectAllSecondary"), CheckBox)
                chkProcessAllCancel = DirectCast(e.Item.Cells(2).FindControl("chkSelectAllCancel"), CheckBox)
                chkProcessAllPrimary.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAllPrimary.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','0');"
                chkProcessAllSecondary.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAllSecondary.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','1');"
                chkProcessAllCancel.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAllCancel.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','2');"
            ElseIf CBool(htRights("Primary Approval")) = True And CBool(htRights("Secondary Approval")) = False And CBool(htRights("Cancel")) = False Then
                Dim chkProcessAllPrimary As New CheckBox
                chkProcessAllPrimary = DirectCast(e.Item.Cells(0).FindControl("chkSelectAllPrimary"), CheckBox)
                chkProcessAllPrimary.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAllPrimary.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','0');"
            ElseIf CBool(htRights("Primary Approval")) = False And CBool(htRights("Secondary Approval")) = True And CBool(htRights("Cancel")) = False Then

                Dim chkProcessAllSecondary As New CheckBox
                chkProcessAllSecondary = DirectCast(e.Item.Cells(1).FindControl("chkSelectAllSecondary"), CheckBox)
                chkProcessAllSecondary.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAllSecondary.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','0');"
            ElseIf CBool(htRights("Primary Approval")) = True And CBool(htRights("Secondary Approval")) = False And CBool(htRights("Cancel")) = True Then
                Dim chkProcessAllPrimary As New CheckBox
                chkProcessAllCancel = New CheckBox
                chkProcessAllPrimary = DirectCast(e.Item.Cells(0).FindControl("chkSelectAllPrimary"), CheckBox)
                chkProcessAllPrimary.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAllPrimary.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','0');"

                chkProcessAllCancel = DirectCast(e.Item.Cells(2).FindControl("chkSelectAllCancel"), CheckBox)
                chkProcessAllCancel.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAllCancel.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','1');"
            ElseIf CBool(htRights("Primary Approval")) = False And CBool(htRights("Secondary Approval")) = True And CBool(htRights("Cancel")) = True Then

                chkProcessAllCancel = New CheckBox
                Dim chkProcessAllSecondary As New CheckBox
                chkProcessAllSecondary = DirectCast(e.Item.Cells(1).FindControl("chkSelectAllSecondary"), CheckBox)
                chkProcessAllSecondary.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAllSecondary.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','0');"

                chkProcessAllCancel = DirectCast(e.Item.Cells(2).FindControl("chkSelectAllCancel"), CheckBox)
                chkProcessAllCancel.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAllCancel.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','1');"
            ElseIf CBool(htRights("Primary Approval")) = False And CBool(htRights("Secondary Approval")) = False And CBool(htRights("Cancel")) = True Then

                chkProcessAllCancel = New CheckBox


                chkProcessAllCancel = DirectCast(e.Item.Cells(2).FindControl("chkSelectAllCancel"), CheckBox)
                chkProcessAllCancel.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAllCancel.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','0');"
            End If

        End If
        Dim imgBtn As LinkButton
        Dim AppPath As String = "" ' DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            chkProcessPrimary = New CheckBox
            chkProcessSecondary = New CheckBox
            chkProcessPrimary = DirectCast(e.Item.Cells(0).FindControl("chkSelectPrimary"), CheckBox)
            chkProcessSecondary = DirectCast(e.Item.Cells(1).FindControl("chkSelectSecondary"), CheckBox)
            imgBtn = New LinkButton
            imgBtn = DirectCast(item.Cells(3).FindControl("lbInstructionID"), LinkButton)
            imgBtn.OnClientClick = "showurldialog('Instruction Details',' " & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & item.GetDataKeyValue("InstructionID").ToString().EncryptString() & "' ,true,700,650);return false;"

            If CBool(htRights("Primary Approval")) = True And CBool(htRights("Secondary Approval")) = True Then
                If item.GetDataKeyValue("Status").ToString = "16" Then
                    chkProcessPrimary.Enabled = True
                    chkProcessPrimary.Checked = False
                    chkProcessSecondary.Enabled = False
                    chkProcessSecondary.Checked = False
                ElseIf item.GetDataKeyValue("Status").ToString = "18" Then
                    chkProcessPrimary.Enabled = False
                    chkProcessPrimary.Checked = True
                    chkProcessSecondary.Enabled = True
                    chkProcessSecondary.Checked = False
                End If

            End If
            If item.GetDataKeyValue("IsAmendmentComplete") = True Then

                gvAuthorization.AlternatingItemStyle.CssClass = "GridItemComplete"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItemComplete"


            Else
                gvAuthorization.AlternatingItemStyle.CssClass = "GridItem"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItem"


            End If
        End If

    End Sub

    Protected Sub btnSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSummary.Click

        Try

                Dim InstructionCount As Double = 0
                Dim TotalAmount As Double = 0
                Dim TotalAmountInInstruction As Double
                Dim TotalCurrencyUsed, PassesCurrencyName As String
                Dim TotalPaymentModeUsed, PassesPaymentModeName As String
                Dim chkSelectPrimary As CheckBox
                Dim chkSelectSecondary As CheckBox
                Dim chkSelectCancel As CheckBox
                If gvAuthorization.Items.Count > 0 Then
                    If CheckgvInstructionAuthentication(True) = False Then
                        UIUtilities.ShowDialog(Me, "Instruction Verification", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                        txtTnxCount.Text = ""
                        txtTotalAmount.Text = ""
                        Exit Sub
                    End If
                    TotalCurrencyUsed = ""
                    TotalPaymentModeUsed = ""
                    PassesCurrencyName = ""
                    PassesPaymentModeName = ""
                    txtTnxCount.Text = ""
                    txtTotalAmount.Text = ""
                    For Each gvInsctructionAuthenticationRow As GridDataItem In gvAuthorization.Items
                        chkSelectPrimary = New CheckBox
                        chkSelectSecondary = New CheckBox
                        chkSelectCancel = New CheckBox
                        chkSelectPrimary = DirectCast(gvInsctructionAuthenticationRow.Cells(0).FindControl("chkSelectPrimary"), CheckBox)
                        chkSelectSecondary = DirectCast(gvInsctructionAuthenticationRow.Cells(1).FindControl("chkSelectSecondary"), CheckBox)
                        chkSelectCancel = DirectCast(gvInsctructionAuthenticationRow.Cells(2).FindControl("chkSelectCancel"), CheckBox)
                        If chkSelectPrimary.Checked = True Or chkSelectSecondary.Checked = True Or chkSelectCancel.Checked = True Then
                            TotalAmountInInstruction = 0

                            InstructionCount = InstructionCount + 1
                            TotalAmountInInstruction = CDbl(gvInsctructionAuthenticationRow.Item("Amount").Text)
                            TotalAmount = TotalAmount + TotalAmountInInstruction
                        End If
                    Next

                    If Not TotalAmount = 0 Then
                        txtTotalAmount.Text = CDbl(TotalAmount).ToString("N2")
                    End If
                    If Not InstructionCount = 0 Then
                        txtTnxCount.Text = CInt(InstructionCount)
                    End If
                    If txtTnxCount.Text = "" And txtTotalAmount.Text = "" Then
                        txtTnxCount.Text = ""
                        txtTotalAmount.Text = ""
                        UIUtilities.ShowDialog(Me, "Instruction Verification", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    InstructionCount = 0
                    TotalAmount = 0
                    TotalCurrencyUsed = ""
                    TotalPaymentModeUsed = ""
                    PassesPaymentModeName = ""
                    PassesCurrencyName = ""
                Else
                    UIUtilities.ShowDialog(Me, "Instruction Verification", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try


    End Sub

    Protected Sub gvAuthorization_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAuthorization.NeedDataSource
        Try
            ClearAllTextBoxes()
            SetArraListRoleID()
            LoadgvAccountPaymentNatureProductType(False)
            'SetJavaScript()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckgvClientRoleCheckedForProcessAll() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelectPrimaryApproval As CheckBox
            Dim chkSelectSecondaryApproval As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvAuthorization.Items
                chkSelectPrimaryApproval = New CheckBox
                chkSelectSecondaryApproval = New CheckBox
                chkSelectPrimaryApproval = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelectPrimary"), CheckBox)
                chkSelectSecondaryApproval = DirectCast(rowGVICClientRoles.Cells(1).FindControl("chkSelectSecondary"), CheckBox)
                If (chkSelectPrimaryApproval.Checked = True And chkSelectPrimaryApproval.Enabled = True) Or (chkSelectSecondaryApproval.Checked = True And chkSelectSecondaryApproval.Enabled = True) Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function CheckgvClientRoleCheckedForProcessAllForCancel() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelectCancel As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvAuthorization.Items

                chkSelectCancel = New CheckBox
                chkSelectCancel = DirectCast(rowGVICClientRoles.Cells(2).FindControl("chkSelectCancel"), CheckBox)
                If chkSelectCancel.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnCancelInstructions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelInstructions.Click
        If Page.IsValid Then
            Try
                Dim chkSelectCancel As CheckBox
                Dim ArrayListInstID As New ArrayList
                Dim objICInstruction As ICInstruction
                Dim StrInstCount As String = Nothing
                Dim StrArr As String() = Nothing

                If CheckgvClientRoleCheckedForProcessAllForCancel() = False Then
                    UIUtilities.ShowDialog(Me, "Instruction Verification", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                Dim dt As New DataTable
                dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Message", GetType(System.String)))

                For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                    chkSelectCancel = New CheckBox
                    chkSelectCancel = DirectCast(gvVerificationRow.Cells(2).FindControl("chkSelectCancel"), CheckBox)
                    If chkSelectCancel.Checked = True Then
                        objICInstruction = New ICInstruction
                        objICInstruction.LoadByPrimaryKey(gvVerificationRow.GetDataKeyValue("InstructionID"))
                        If objICInstruction.Status.ToString = "16" Or objICInstruction.Status.ToString = "18" Then
                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "52", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE")
                            ArrayListInstID.Add(objICInstruction.InstructionID)
                        End If
                    End If
                Next
                Dim Remarks As String = ""
                If txtRemarks.Text <> "" And txtRemarks.Text <> "Enter Remarks" Then
                    Remarks = txtRemarks.Text.ToString
                End If
                If ArrayListInstID.Count > 0 Then
                    StrInstCount = ICInstructionProcessController.CancelBankApprovalQueueTransactions(ArrayListInstID, Me.UserId.ToString, Me.UserInfo.Username.ToString, Remarks, dt)
                Else
                    UIUtilities.ShowDialog(Me, "Instruction Verification", "Invalid selected instruction(s) status.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If
                RefreshPage()
                If dt.Rows.Count > 0 Then
                    gvShowSummary.DataSource = Nothing
                    gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                    gvShowSummary.DataBind()
                    pnlShowSummary.Style.Remove("display")
                    Dim scr As String
                    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,600,650);};"
                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                Else
                    gvShowSummary.DataSource = Nothing
                    pnlShowSummary.Style.Add("display", "none")
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Function GetFinalDTForSummary(ByVal dt As DataTable) As DataTable
        Dim CurrentStatus As String = Nothing
        Dim Message As String = Nothing
        Dim InstructionID As String = Nothing
        Dim dtFinalForSummary As New DataTable
        Dim StatusFound As Boolean = False
        Dim drForFinaldt As DataRow

        Dim i As Integer = 0
        dtFinalForSummary.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
        dtFinalForSummary.Columns.Add(New DataColumn("Message", GetType(System.String)))
        dt.DefaultView.Sort = "Status ASC"
        For Each dr As DataRow In dt.Rows
            i = i + 1
            StatusFound = False
            If CurrentStatus = dr("Status").ToString Then
                InstructionID += dr("InstructionID").ToString & " ; "

                For j As Integer = i To dt.Rows.Count - 1
                    If dt(j)(1).ToString = CurrentStatus Then
                        StatusFound = True
                    End If
                Next
                If StatusFound = False Then
                    drForFinaldt = dtFinalForSummary.NewRow
                    drForFinaldt("InstructionID") = InstructionID.Remove(InstructionID.Length - 2, 2)
                    drForFinaldt("Message") = Message.ToString
                    dtFinalForSummary.Rows.Add(drForFinaldt)
                    CurrentStatus = Nothing
                    InstructionID = Nothing
                    Message = Nothing
                End If

            Else
                If dt.Rows.Count = 1 Then
                    drForFinaldt = dtFinalForSummary.NewRow
                    drForFinaldt("InstructionID") = dr("InstructionID").ToString
                    drForFinaldt("Message") = dr("Message").ToString
                    dtFinalForSummary.Rows.Add(drForFinaldt)
                    CurrentStatus = Nothing
                    InstructionID = Nothing
                    Message = Nothing
                Else
                    If Not InstructionID Is Nothing And Not Message Is Nothing Then
                        drForFinaldt = dtFinalForSummary.NewRow
                        drForFinaldt("InstructionID") = InstructionID.Remove(InstructionID.Length - 2, 2)
                        drForFinaldt("Message") = Message.ToString
                        dtFinalForSummary.Rows.Add(drForFinaldt)
                    End If
                    CurrentStatus = Nothing
                    InstructionID = Nothing
                    Message = Nothing
                    CurrentStatus = dr("Status")
                    InstructionID = dr("InstructionID").ToString & " ; "
                    Message = dr("Message").ToString
                    For j As Integer = i To dt.Rows.Count - 1
                        If dt(j)(1).ToString = CurrentStatus Then
                            StatusFound = True
                        End If
                    Next
                    If StatusFound = False Then
                        drForFinaldt = dtFinalForSummary.NewRow
                        drForFinaldt("InstructionID") = InstructionID.Remove(InstructionID.Length - 2, 2)
                        drForFinaldt("Message") = Message.ToString
                        dtFinalForSummary.Rows.Add(drForFinaldt)
                    End If
                End If

            End If

        Next
        Return dtFinalForSummary
    End Function
    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        If Page.IsValid Then
            Dim objUser As New ICUser
            Try

                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Instruction Verification", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If objUser.LoadByPrimaryKey(Me.UserId) Then
                    If objUser.Is2FARequiredOnApproval = True Then
                        If IC2FAController.Generate2FACodeAndSend(Me.UserId, "Approval") Then
                            btnOK2FA.Text = "OK"
                            txt2FACode.Text = Nothing
                            txt2FACode.Enabled = True
                            rim2FA.Enabled = True
                            btnOK2FA.Visible = True
                            pnl2FACode.Style.Remove("display")
                            btnApprove.Visible = False

                            btnCancelInstructions.Visible = False
                            'Dim scr As String = ""
                            'scr = "window.onload = function () {$('#dialog2FA').dialog('open');return false;}"
                            'Me.Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                        End If
                    Else
                        Dim chkSelectPrimaryApproval As CheckBox
                        Dim chkSelectSecondaryApproval As CheckBox
                        Dim ArrayListInstID As New ArrayList
                        Dim StrInstCount As String = Nothing
                        Dim StrArr As String() = Nothing
                        Dim dt As New DataTable
                        dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                        dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                        dt.Columns.Add(New DataColumn("Message", GetType(System.String)))

                        For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                            chkSelectPrimaryApproval = New CheckBox
                            chkSelectSecondaryApproval = New CheckBox
                            chkSelectPrimaryApproval = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelectPrimary"), CheckBox)
                            chkSelectSecondaryApproval = DirectCast(gvVerificationRow.Cells(1).FindControl("chkSelectSecondary"), CheckBox)
                            If (chkSelectPrimaryApproval.Checked = True And chkSelectPrimaryApproval.Enabled = True) Or (chkSelectSecondaryApproval.Checked = True And chkSelectSecondaryApproval.Enabled = True) Then
                                ArrayListInstID.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                            End If
                        Next
                        Dim Remarks As String = ""
                        If txtRemarks.Text <> "" And txtRemarks.Text <> "Enter Remarks" Then
                            Remarks = txtRemarks.Text.ToString
                        End If
                        If ArrayListInstID.Count > 0 Then
                            Dim objPOICInstructionColl As New ICInstructionCollection
                            Dim objDDICInstructionColl As New ICInstructionCollection

                            objPOICInstructionColl = ICInstructionController.GetInstructionByPaymentModeAndArrayListID("PO", ArrayListInstID, "18")
                            objDDICInstructionColl = ICInstructionController.GetInstructionByPaymentModeAndArrayListID("DD", ArrayListInstID, "18")
                            If objPOICInstructionColl.Count > 0 Then
                                If CInt(GetRequiredInstrumentNosForPrint(objPOICInstructionColl)) > 0 Then
                                    If CInt(ICPOInstrumentsController.GetTotalUnUSedPOInstrumentCountByPrintLocationCode(objUser.OfficeCode.ToString)) < CInt(GetRequiredInstrumentNosForPrint(objPOICInstructionColl)) Then
                                        UIUtilities.ShowDialog(Me, "Error", "Selected instruction can not be approved due to insufficient available PO stationary on printing location", ICBO.IC.Dialogmessagetype.Failure)
                                        Exit Sub
                                    End If
                                End If
                            End If
                            If objDDICInstructionColl.Count > 0 Then
                                If CInt(GetRequiredInstrumentNosForPrint(objDDICInstructionColl)) > 0 Then
                                    If CInt(ICDDInstrumentsController.GetTotalUnUSedDDInstrumentCountByPrintLocationCode(objUser.OfficeCode.ToString)) < CInt(GetRequiredInstrumentNosForPrint(objDDICInstructionColl)) Then
                                        UIUtilities.ShowDialog(Me, "Error", "Selected instruction can not be approved due to insufficient available DD stationary on printing location", ICBO.IC.Dialogmessagetype.Failure)
                                        Exit Sub
                                    End If
                                End If
                            End If
                            ArrayListInstID.Sort()


                            StrInstCount = ICInstructionProcessController.PerformBankPrimaryOrSecondaryApproval(ArrayListInstID, Me.UserId.ToString, Me.UserInfo.Username.ToString, Remarks, dt)
                        Else
                            UIUtilities.ShowDialog(Me, "Instruction Verification", "No instruction selected.", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                        RefreshPage()
                        If dt.Rows.Count > 0 Then
                            gvShowSummary.DataSource = Nothing
                            gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                            gvShowSummary.DataBind()
                            pnlShowSummary.Style.Remove("display")
                            Dim scr As String
                            scr = "window.onload = function () {showsumdialog('Transaction Summary',true,600,650);};"
                            Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                        Else
                            gvShowSummary.DataSource = Nothing
                            pnlShowSummary.Style.Add("display", "none")
                        End If

                    End If
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Private Function GetRequiredInstrumentNosForPrint(ByVal objICInstructionColl As ICInstructionCollection) As Integer
        Dim InstrumentNoCount As Integer = 0
        For Each objICInstructions As ICInstruction In objICInstructionColl
            If objICInstructions.InstrumentNo Is Nothing Or objICInstructions.InstrumentNo = "-" Or objICInstructions.InstrumentNo = "" Then
                InstrumentNoCount = InstrumentNoCount + 1
            End If


        Next
        Return InstrumentNoCount
    End Function

    
#Region "2FA Code"
    Protected Sub btnOK2FA_Click(sender As Object, e As System.EventArgs) Handles btnOK2FA.Click
        If Page.IsValid Then
            Try
                If btnOK2FA.Text = "OK" Then
                    If IC2FAController.Check2FAExistAndValid(Me.UserId, txt2FACode.Text.ToString().Trim(), "Approval") Then
                        If ICBO.IC.IC2FAController.Update2FACodeStatusAfterUse(Me.UserId, "Approval") Then
                            Dim chkSelectPrimaryApproval As CheckBox
                            Dim chkSelectSecondaryApproval As CheckBox
                            Dim ArrayListInstID As New ArrayList
                            Dim StrInstCount As String = Nothing
                            Dim StrArr As String() = Nothing
                            Dim dt As New DataTable
                            dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                            dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                            dt.Columns.Add(New DataColumn("Message", GetType(System.String)))
                            pnl2FACode.Style.Add("display", "none")
                            For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                                chkSelectPrimaryApproval = New CheckBox
                                chkSelectSecondaryApproval = New CheckBox
                                chkSelectPrimaryApproval = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelectPrimary"), CheckBox)
                                chkSelectSecondaryApproval = DirectCast(gvVerificationRow.Cells(1).FindControl("chkSelectSecondary"), CheckBox)
                                If (chkSelectPrimaryApproval.Checked = True And chkSelectPrimaryApproval.Enabled = True) Or (chkSelectSecondaryApproval.Checked = True And chkSelectSecondaryApproval.Enabled = True) Then
                                    ArrayListInstID.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                                End If
                            Next
                            If ArrayListInstID.Count > 0 Then
                                Dim objUser As New ICUser
                                objUser.LoadByPrimaryKey(Me.UserId.ToString)
                                Dim objPOICInstructionColl As New ICInstructionCollection
                                Dim objDDICInstructionColl As New ICInstructionCollection

                                objPOICInstructionColl = ICInstructionController.GetInstructionByPaymentModeAndArrayListID("PO", ArrayListInstID, "18")
                                objDDICInstructionColl = ICInstructionController.GetInstructionByPaymentModeAndArrayListID("DD", ArrayListInstID, "18")
                                If objPOICInstructionColl.Count > 0 Then
                                    If CInt(GetRequiredInstrumentNosForPrint(objPOICInstructionColl)) > 0 Then
                                        If CInt(ICPOInstrumentsController.GetTotalUnUSedPOInstrumentCountByPrintLocationCode(objUser.OfficeCode.ToString)) < CInt(GetRequiredInstrumentNosForPrint(objPOICInstructionColl)) Then
                                            UIUtilities.ShowDialog(Me, "Error", "Selected instruction can not be approved due to insufficient available PO stationary on printing location", ICBO.IC.Dialogmessagetype.Failure)
                                            RefreshPage()
                                            Exit Sub
                                        End If
                                    End If
                                End If
                                If objDDICInstructionColl.Count > 0 Then
                                    If CInt(GetRequiredInstrumentNosForPrint(objDDICInstructionColl)) > 0 Then
                                        If CInt(ICDDInstrumentsController.GetTotalUnUSedDDInstrumentCountByPrintLocationCode(objUser.OfficeCode.ToString)) < CInt(GetRequiredInstrumentNosForPrint(objDDICInstructionColl)) Then
                                            UIUtilities.ShowDialog(Me, "Error", "Selected instruction can not be approved due to insufficient available DD stationary on printing location", ICBO.IC.Dialogmessagetype.Failure)
                                            RefreshPage()
                                            Exit Sub
                                        End If
                                    End If
                                End If
                                ArrayListInstID.Sort()
                                StrInstCount = ICInstructionProcessController.PerformBankPrimaryOrSecondaryApproval(ArrayListInstID, Me.UserId.ToString, Me.UserInfo.Username.ToString, txtRemarks.Text.ToString, dt)
                            Else
                                UIUtilities.ShowDialog(Me, "Instruction Verification", "No instruction selected.", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                            RefreshPage()
                            btnOK2FA.Visible = False
                            If dt.Rows.Count > 0 Then
                                gvShowSummary.DataSource = Nothing
                                gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                                gvShowSummary.DataBind()
                                pnlShowSummary.Style.Remove("display")
                                Dim scr As String
                                scr = "window.onload = function () {showsumdialog('Transaction Summary',true,600,650);};"
                                Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                            Else
                                gvShowSummary.DataSource = Nothing
                                pnlShowSummary.Style.Add("display", "none")
                            End If



                        End If
                    Else
                        If IC2FAController.CheckAndUpdateLoginAttemts(Me.UserId, "Approval") = False Then
                            btnOK2FA.Text = "Regenerate Code"
                            txt2FACode.Text = Nothing
                            txt2FACode.Enabled = False
                            rim2FA.Enabled = False
                            ShowError("Retries Exhausted. Please regenerate code.")
                            btnApprove.Visible = False

                            btnCancelInstructions.Visible = False
                        Else
                            btnApprove.Visible = False
                            btnCancelInstructions.Visible = False
                            txt2FACode.Enabled = True
                            txt2FACode.Text = Nothing
                            ShowError("Invalid Code. Please retry.")
                            rim2FA.Enabled = True
                        End If
                    End If
                ElseIf btnOK2FA.Text = "Regenerate Code" Then
                    If IC2FAController.Generate2FACodeAndSend(Me.UserId, "Approval") Then
                        btnOK2FA.Text = "OK"
                        txt2FACode.Text = Nothing
                        txt2FACode.Enabled = True
                        rim2FA.Enabled = True
                        btnApprove.Visible = False
                        btnCancelInstructions.Visible = False
                    End If
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                ShowError(ex.Message.ToString())
            End Try
        End If
    End Sub
    Private Sub ShowError(ByVal Msg As String)
        lblError.Text = Msg.ToString()
        lblError.Visible = True
    End Sub
#End Region

    Protected Sub raddpCreationToDate_SelectedDateChanged(sender As Object, e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationToDate.SelectedDateChanged
        Try
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Verfication Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Verfication from date should be less than verfication to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub raddpCreationFromDate_SelectedDateChanged(sender As Object, e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationFromDate.SelectedDateChanged
        Try

            'If raddpCreationFromDate.SelectedDate Is Nothing Then
            '    UIUtilities.ShowDialog(Me, "Error", "Please select creation from date.", ICBO.IC.Dialogmessagetype.Failure)
            '    Exit Sub
            'End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Verfication Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Verfication from date should be less than verfication to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlAccount_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlAccount.SelectedIndexChanged
        Try
            If ddlAccount.SelectedValue.ToString <> "0" Then
                txtAvailableBalance.Text = ""
                txtAvailableBalance.Text = CDbl(CBUtilities.BalanceInquiry(ddlAccount.SelectedValue.Split("-")(0).ToString, ICBO.CBUtilities.AccountType.RB, "Client Account Balance", ddlAccount.SelectedValue.Split("-")(0).ToString, ddlAccount.SelectedValue.Split("-")(1).ToString)).ToString("N2")
            Else
                txtAvailableBalance.Text = ""
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click

        Try
                LoadgvAccountPaymentNatureProductType(True)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub

    Protected Sub rbtnlstDateSelection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnlstDateSelection.SelectedIndexChanged
        Try
            If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now.AddYears(2)
            Else
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
            End If

            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Verfication Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Verfication from date should be less than verfication to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnClose2FA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose2FA.Click
        If Page.IsValid Then
            Try
                RefreshPage()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub


End Class

