﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data.OleDb
Imports System.Data

Partial Class DesktopModules_BankRoleAccountPaymentNature_BankRoleAccountPaymentNatureTagging
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
    Private ArrICAssignUserRolsID As New ArrayList
    Private InstructionID As String
    Private CompanyCode As String
    Private ProductTypeCode As String
    Private InstructionIDForPrintLocations As String
    Private IsBulkAmend As Boolean
    Private ArrayListInstID As String()



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            InstructionID = Request.QueryString("InstructionID").ToString().DecryptString2()

            Dim objICInstruction As New ICInstruction
            objICInstruction.LoadByPrimaryKey(InstructionID)

            'CompanyCode = Request.QueryString("CompanyCode").ToString
            CompanyCode = objICInstruction.CompanyCode
            ProductTypeCode = Request.QueryString("ProductTypeCode").ToString
            If Not Request.QueryString("IsBulkAmend") Is Nothing Then
                If Request.QueryString("IsBulkAmend") = "True" Then
                    IsBulkAmend = True
                Else
                    IsBulkAmend = False
                End If
            Else
                IsBulkAmend = False
            End If
            If InstructionID.Contains(";") = True Then
                ArrayListInstID = InstructionID.Split(";")
                InstructionIDForPrintLocations = ArrayListInstID(0)
            End If
            If Not InstructionID.Contains(";") Then
                InstructionIDForPrintLocations = InstructionID
            End If
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            'txtValueDate.Clear()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                'btnUpdate.Visible = CBool(htRights(""))

                Dim objICUser As New ICUser
                Dim objICGroup As New ICGroup

                Dim dt As New DataTable

                sharedCalendarValueDate.SelectedDate = Date.Now
                sharedCalendarValueDate.RangeMinDate = Date.Now
                sharedCalenderPrintDate.SelectedDate = Date.Now
                sharedCalenderPrintDate.RangeMinDate = Date.Now


                objICUser.es.Connection.CommandTimeout = 3600
                objICGroup.es.Connection.CommandTimeout = 3600
                'txtValueDate.SelectedDate = Date.Now

                If Me.UserInfo.IsSuperUser = False Then
                    If objICUser.LoadByPrimaryKey(Me.UserId) Then

                        HideAllOnlineFormRows()
                        HideRequiredMarkSign()
                        ClearOnlineForm()

                        SetArraListRoleID()
                        LoadddlCompany()
                        LoadddlAccountPaymentNatureByUserID()
                        LoadddlProductTypeByAccountPaymentNature()

                        LoadddlCountry()
                        LoadddlProvince()
                        LoadddlCity()
                        LoadddlBeneBank()
                        LoadddlDDDrawnLocationProvince()
                        LoadddlDDDrawnLocationCity()
                        LoadddlPrintLocations()
                        LoadddlBenePickLocation()
                        LoadddlGLAccountBranchCode()
                        SetViewFieldsVisibility()
                        If IsBulkAmend = False Then
                            tblHeader.Style.Remove("display")

                            ddlCompany.SelectedValue = objICInstruction.CompanyCode
                            ddlAccPNature.SelectedValue = objICInstruction.ClientAccountNo + "-" + objICInstruction.ClientAccountBranchCode + "-" + objICInstruction.ClientAccountCurrency + "-" + objICInstruction.PaymentNatureCode
                            ddlProductType.SelectedValue = objICInstruction.ProductTypeCode
                            ddlCompany.Enabled = False
                            ddlAccPNature.Enabled = False
                            ddlProductType.Enabled = False
                            lblInstIDForAmend.Text = "Instruction No <br/>" & InstructionID
                            lblInstIDForAmend.Visible = True
                            SetTextBoxesValues(objICInstruction)
                            lblPageHeader.Text = "Single Amend"
                            btnCancel.Visible = False

                        Else
                            tblHeader.Style.Add("display", "none")
                            lblInstIDForAmend.Text = "Instruction No <br/>" & InstructionID
                            lblInstIDForAmend.Visible = True
                            lblPageHeader.Text = "Bulk Amend"
                            btnUpdate.Text = "Amend"
                            btnCancel.Text = "Cancel Bulk Amend"
                            btnCancelSingle.Visible = False

                        End If
                        'End If
                    End If
                Else
                    UIUtilities.ShowDialog(Me, "Error", "Sorry! You are not authorized to login.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Amendment Queue") = True Then
                    If Not ArrICAssignUserRolsID.Contains(RoleID.ToString) Then
                        ArrICAssignUserRolsID.Add(RoleID.ToString)
                    End If
                End If
            Next
        End If
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Amendment Queue")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlBenePickLocation()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBenePickUpLocation.Items.Clear()
            ddlBenePickUpLocation.Items.Add(lit)
            ddlBenePickUpLocation.AppendDataBoundItems = True
            ddlBenePickUpLocation.DataSource = ICOfficeController.GetPrincipalBankBranchActiveAndApprove
            ddlBenePickUpLocation.DataTextField = "OfficeName"
            ddlBenePickUpLocation.DataValueField = "OfficeID"
            ddlBenePickUpLocation.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetCompanyActiveAndApprove()
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCity()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBeneficiaryCity.Items.Clear()
            ddlBeneficiaryCity.Items.Add(lit)
            ddlBeneficiaryCity.AppendDataBoundItems = True
            ddlBeneficiaryCity.DataSource = ICCityController.GetAllCityActiveAndApproved
            ddlBeneficiaryCity.DataTextField = "CityName"
            ddlBeneficiaryCity.DataValueField = "CityCode"
            ddlBeneficiaryCity.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlGLAccountBranchCode()
        Try
            Dim lit As New ListItem
            Dim dt As New DataTable
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlGLAccountBranchCode.Items.Clear()
            ddlGLAccountBranchCode.Items.Add(lit)
            ddlGLAccountBranchCode.AppendDataBoundItems = True
            dt = ICOfficeController.GetPrincipalBankBranchActiveAndApprove
            For Each dr As DataRow In dt.Rows
                lit = New ListItem
                lit.Text = dr("OfficeCode") & "-" & dr("OfficeName")
                lit.Value = dr("OfficeCode")
                ddlGLAccountBranchCode.Items.Add(lit)
            Next
            ddlGLAccountBranchCode.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlProvince()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBeneficiaryProvince.Items.Clear()
            ddlBeneficiaryProvince.Items.Add(lit)
            ddlBeneficiaryProvince.AppendDataBoundItems = True
            ddlBeneficiaryProvince.DataSource = ICProvinceController.GetAllActiveAndApproveProvinces
            ddlBeneficiaryProvince.DataTextField = "ProvinceName"
            ddlBeneficiaryProvince.DataValueField = "ProvinceCode"
            ddlBeneficiaryProvince.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlDDDrawnLocationProvince()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlDDPayableLocationProvince.Items.Clear()
            ddlDDPayableLocationProvince.Items.Add(lit)
            ddlDDPayableLocationProvince.AppendDataBoundItems = True
            ddlDDPayableLocationProvince.DataSource = ICProvinceController.GetPakistanProvinceActiveAndApprove
            ddlDDPayableLocationProvince.DataTextField = "ProvinceName"
            ddlDDPayableLocationProvince.DataValueField = "ProvinceCode"
            ddlDDPayableLocationProvince.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlDDDrawnLocationCity()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlDDPayableLocationCity.Items.Clear()
            ddlDDPayableLocationCity.Items.Add(lit)
            ddlDDPayableLocationCity.AppendDataBoundItems = True
            ddlDDPayableLocationCity.DataSource = ICCityController.GetCityActiveAndApprovedByProvinceCode(ddlDDPayableLocationProvince.SelectedValue.ToString)
            ddlDDPayableLocationCity.DataTextField = "CityName"
            ddlDDPayableLocationCity.DataValueField = "CityCode"
            ddlDDPayableLocationCity.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlCountry()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBeneficiaryCountry.Items.Clear()
            ddlBeneficiaryCountry.Items.Add(lit)
            ddlBeneficiaryCountry.AppendDataBoundItems = True
            ddlBeneficiaryCountry.DataSource = ICCountryController.GetCountryActiveAndApproved
            ddlBeneficiaryCountry.DataTextField = "CountryName"
            ddlBeneficiaryCountry.DataValueField = "CountryCode"
            ddlBeneficiaryCountry.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlBeneBank()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBeneBankName.Items.Clear()
            ddlBeneBankName.Items.Add(lit)
            ddlBeneBankName.AppendDataBoundItems = True
            ddlBeneBankName.DataSource = ICBankController.GetBank
            ddlBeneBankName.DataTextField = "BankName"
            ddlBeneBankName.DataValueField = "BankCode"
            ddlBeneBankName.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlAccountPaymentNatureByUserID()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlAccPNature.Items.Clear()
            ddlAccPNature.Items.Add(lit)
            lit.Selected = True
            ddlAccPNature.AppendDataBoundItems = True
            ddlAccPNature.DataSource = ICAccountPaymentNatureController.GetAllAccountPaymentNature
            ddlAccPNature.DataTextField = "AccountAndPaymentNature"
            ddlAccPNature.DataValueField = "AccountPaymentNature"
            ddlAccPNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlProductTypeByAccountPaymentNature()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlProductType.Items.Clear()
            ddlProductType.Items.Add(lit)
            ddlProductType.AppendDataBoundItems = True
            ddlProductType.DataSource = ICProductTypeController.GetAllProductTypesActiveAndApprove
            ddlProductType.DataTextField = "ProductTypeName"
            ddlProductType.DataValueField = "ProductTypeCode"
            ddlProductType.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlPrintLocations()
        Try
            Dim objICInstructions As New ICInstruction
            objICInstructions.LoadByPrimaryKey(InstructionIDForPrintLocations)

            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlPrintLocation.Items.Clear()
            ddlPrintLocation.Items.Add(lit)

            ddlPrintLocation.AppendDataBoundItems = True
            ddlPrintLocation.DataSource = ICAccountsPaymentNatureProductTypePrintLocationsController.GetAllTaggedLocationsWithAPNPType(objICInstructions.ProductTypeCode.ToString, objICInstructions.ClientAccountNo.ToString, objICInstructions.ClientAccountBranchCode.ToString, objICInstructions.ClientAccountCurrency.ToString, objICInstructions.PaymentNatureCode.ToString)
            ddlPrintLocation.DataTextField = "OfficeName"
            ddlPrintLocation.DataValueField = "OfficeID"
            ddlPrintLocation.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub HideAllOnlineFormRows()
        DirectCast(Me.Control.FindControl("trBeneName"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trPrintLocation"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trAccountTitle"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trBeneBankCode"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trClientBankName"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneMobNo"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneEmail"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneCity"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneCountry"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trBeneAddress2"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trAmountInWords"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trRemarks"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField1"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField3"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBankAccountNumber"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneAccountBranchCode"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneAccountCurrency"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trBankBranchCode"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trBankIMD"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trBranchName"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneBankName"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trClientBankName"), HtmlTableRow).Style.Add("display", "none")
        'DirectCast(Me.Control.FindControl("trBeneBankAddress"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBenePhoneNo"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneCNICNo"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneProvince"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneAddress1"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trAmountPKR"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trDescription"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trReferenceField2"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trDDPayableLocationProvince"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trDDPayableLocationCity"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trValueDate"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trAmountInWords"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trInstrumentNo"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trPrintDate"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneBranchAddress"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneIDType"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBenePickUpLocation"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneIDNo"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trBeneficiaryAccountType"), HtmlTableRow).Style.Add("display", "none")
        DirectCast(Me.Control.FindControl("trGLAccountBranchCode"), HtmlTableRow).Style.Add("display", "none")


    End Sub
    Private Sub SetViewFieldsVisibiltyByBeneAccountType(ByVal CBAccountType As String)
        Dim objAmendmentRightsColl As New ICAmendmentsRightsManagementCollection
        Dim objICProductType As New ICProductType
        objICProductType.LoadByPrimaryKey(ProductTypeCode)
        If objICProductType.DisbursementMode = "Direct Credit" Then
            DirectCast(Me.Control.FindControl("trGLAccountBranchCode"), HtmlTableRow).Style.Add("display", "none")
            txtBeneAccountBranchCode.Text = ""

            ddlGLAccountBranchCode.ClearSelection()
            objAmendmentRightsColl = ICAmendmentRightsController.GetAllAmendmentAllowedFieldsByCompanyCodeAndProductTypeCode(CompanyCode, ProductTypeCode, IsBulkAmend)
            For Each objICAmendmentRights As ICAmendmentsRightsManagement In objAmendmentRightsColl
                Select Case objICAmendmentRights.FieldID.ToString
                    Case "5"
                        txtBeneAccountBranchCode.ReadOnly = False
                        txtBeneAccountCurrency.ReadOnly = False
                        txtAccountTitle.ReadOnly = False
                        txtBankAccountNumber.AutoPostBack = False
                        txtBankAccountNumber.Enabled = True


                        txtBeneAccountBranchCode.Enabled = False
                        txtBeneAccountCurrency.Enabled = False
                        txtAccountTitle.Enabled = False


                        If CBAccountType = "RB" Or CBAccountType = "0" Then
                            If objICProductType.DisbursementMode = "Direct Credit" Then
                                DirectCast(Me.Control.FindControl("trBeneAccountBranchCode"), HtmlTableRow).Style.Remove("display")
                                DirectCast(Me.Control.FindControl("trBeneAccountCurrency"), HtmlTableRow).Style.Remove("display")
                                DirectCast(Me.Control.FindControl("trAccountTitle"), HtmlTableRow).Style.Remove("display")
                                txtBeneAccountCurrency.Text = ""
                                txtBeneAccountBranchCode.ReadOnly = True
                                txtBeneAccountCurrency.ReadOnly = True
                                txtAccountTitle.ReadOnly = True
                                txtBankAccountNumber.AutoPostBack = True
                            End If
                        ElseIf CBAccountType = "GL" Then
                            DirectCast(Me.Control.FindControl("trGLAccountBranchCode"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneAccountBranchCode"), HtmlTableRow).Style.Add("display", "none")
                            LoadddlGLAccountBranchCode()

                            DirectCast(Me.Control.FindControl("trBeneAccountCurrency"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trAccountTitle"), HtmlTableRow).Style.Remove("display")
                            txtBeneAccountBranchCode.Enabled = True
                            txtBeneAccountCurrency.ReadOnly = True
                            txtBeneAccountCurrency.Text = ""
                            txtBeneAccountCurrency.Text = "PKR"
                            txtAccountTitle.Enabled = True
                            txtBankAccountNumber.AutoPostBack = False
                        End If
                End Select
            Next
        End If

    End Sub
    Private Sub SetViewFieldsVisibility()
        Dim objAmendmentRightsColl As New ICAmendmentsRightsManagementCollection
        Dim objICProductType As New ICProductType
        Dim dtBankBranches As New DataTable
        objAmendmentRightsColl.es.Connection.CommandTimeout = 3600

        objICProductType.LoadByPrimaryKey(ProductTypeCode)
        objAmendmentRightsColl = ICAmendmentRightsController.GetAllAmendmentAllowedFieldsByCompanyCodeAndProductTypeCode(CompanyCode, ProductTypeCode, IsBulkAmend)
        If objAmendmentRightsColl.Count > 0 Then
            txtBeneAccountBranchCode.ReadOnly = False
            txtBeneAccountCurrency.ReadOnly = False
            txtAccountTitle.ReadOnly = False
            txtBankAccountNumber.AutoPostBack = False
            ddlBeneBankName.AutoPostBack = False
            ddlDDPayableLocationCity.AutoPostBack = False
            For Each objICAmendmentRights As ICAmendmentsRightsManagement In objAmendmentRightsColl
                If objICAmendmentRights.FieldID.ToString = "19" And objICAmendmentRights.CanView = True Then
                    DirectCast(Me.Control.FindControl("trBeneName"), HtmlTableRow).Style.Remove("display")

                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        txtBeneName.Enabled = True

                    End If

                ElseIf objICAmendmentRights.FieldID.ToString = "62" And objICAmendmentRights.CanView = True Then
                    DirectCast(Me.Control.FindControl("trInstrumentNo"), HtmlTableRow).Style.Remove("display")

                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        txtInstrumentNo.Enabled = True
                    End If
                ElseIf objICAmendmentRights.FieldID.ToString = "372" And objICAmendmentRights.CanView = True Then
                    DirectCast(Me.Control.FindControl("trBeneBranchAddress"), HtmlTableRow).Style.Remove("display")

                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        txtBeneBranchAddress.Enabled = True
                    End If
                ElseIf objICAmendmentRights.FieldID.ToString = "79" And objICAmendmentRights.CanView = True Then
                    DirectCast(Me.Control.FindControl("trPrintDate"), HtmlTableRow).Style.Remove("display")

                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        txtPrintDate.Enabled = True
                        sharedCalenderPrintDate.Enabled = True
                    End If
                ElseIf objICAmendmentRights.FieldID.ToString = "5" And objICAmendmentRights.CanView = True Then
                    DirectCast(Me.Control.FindControl("trBankAccountNumber"), HtmlTableRow).Style.Remove("display")
                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        txtBankAccountNumber.Enabled = True
                        If objICProductType.DisbursementMode = "Direct Credit" Then
                            DirectCast(Me.Control.FindControl("trBeneAccountBranchCode"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trBeneAccountCurrency"), HtmlTableRow).Style.Remove("display")
                            DirectCast(Me.Control.FindControl("trAccountTitle"), HtmlTableRow).Style.Remove("display")
                            txtBeneAccountBranchCode.ReadOnly = True
                            txtBeneAccountCurrency.ReadOnly = True
                            txtAccountTitle.ReadOnly = True
                            txtBankAccountNumber.AutoPostBack = True
                        ElseIf objICProductType.DisbursementMode = "Other Credit" Then
                            DirectCast(Me.Control.FindControl("trAccountTitle"), HtmlTableRow).Style.Remove("display")
                            txtAccountTitle.ReadOnly = True
                            txtBankAccountNumber.AutoPostBack = True
                        End If
                    End If

                ElseIf objICAmendmentRights.FieldID.ToString = "72" And objICAmendmentRights.CanView Then


                    If objICProductType.DisbursementMode <> "Direct Credit" Then
                        DirectCast(Me.Control.FindControl("trBeneAccountBranchCode"), HtmlTableRow).Style.Remove("display")
                        If objICAmendmentRights.IsAmendmentAllow = True Then
                            txtBeneAccountBranchCode.Enabled = True
                        End If
                    End If

                ElseIf objICAmendmentRights.FieldID.ToString = "73" And objICAmendmentRights.CanView = True Then
                    If objICProductType.DisbursementMode <> "Direct Credit" Then
                        DirectCast(Me.Control.FindControl("trBeneAccountCurrency"), HtmlTableRow).Style.Remove("display")
                        If objICAmendmentRights.IsAmendmentAllow = True Then
                            txtBeneAccountCurrency.Enabled = True
                        End If
                    End If

                ElseIf objICAmendmentRights.FieldID.ToString = "64" And objICAmendmentRights.CanView = True Then

                    LoadddlPrintLocations()

                    DirectCast(Me.Control.FindControl("trPrintLocation"), HtmlTableRow).Style.Remove("display")

                    If objICAmendmentRights.IsAmendmentAllow = True Then

                        ddlPrintLocation.Enabled = True
                    End If
                ElseIf objICAmendmentRights.FieldID.ToString = "36" And objICAmendmentRights.CanView = True Then

                    DirectCast(Me.Control.FindControl("trDDPayableLocationProvince"), HtmlTableRow).Style.Remove("display")
                    DirectCast(Me.Control.FindControl("trDDPayableLocationCity"), HtmlTableRow).Style.Remove("display")
                    LoadddlDDDrawnLocationProvince()
                    LoadddlDDDrawnLocationCity()

                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        ddlDDPayableLocationProvince.Enabled = True
                        ddlDDPayableLocationCity.Enabled = True
                        If objICProductType.DisbursementMode = "DD" Then
                            ddlDDPayableLocationCity.AutoPostBack = True
                        End If
                    End If
                ElseIf objICAmendmentRights.FieldID.ToString = "6" And objICAmendmentRights.CanView = True Then
                    If objICProductType.DisbursementMode <> "Direct Credit" And objICProductType.DisbursementMode <> "Other Credit" Then
                        DirectCast(Me.Control.FindControl("trAccountTitle"), HtmlTableRow).Style.Remove("display")
                        If objICAmendmentRights.IsAmendmentAllow = True Then
                            txtAccountTitle.Enabled = True
                        End If
                    End If

                ElseIf objICAmendmentRights.FieldID.ToString = "9" And objICAmendmentRights.CanView = True Then
                    DirectCast(Me.Control.FindControl("trBeneBankName"), HtmlTableRow).Style.Remove("display")
                    LoadddlBeneBank()
                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        ddlBeneBankName.Enabled = True
                        If objICProductType.DisbursementMode = "Other Credit" Then
                            ddlBeneBankName.AutoPostBack = True
                        End If
                    End If

                ElseIf objICAmendmentRights.FieldID.ToString = "18" And objICAmendmentRights.CanView = True Then

                    DirectCast(Me.Control.FindControl("trBeneMobNo"), HtmlTableRow).Style.Remove("display")
                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        txtBeneMobileNo.Enabled = True
                    End If


                ElseIf objICAmendmentRights.FieldID.ToString = "20" And objICAmendmentRights.CanView = True Then

                    DirectCast(Me.Control.FindControl("trBenePhoneNo"), HtmlTableRow).Style.Remove("display")
                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        txtBenePhoneNo.Enabled = True
                    End If
                ElseIf objICAmendmentRights.FieldID.ToString = "17" And objICAmendmentRights.CanView = True Then

                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        txtBeneEmail.Enabled = True
                    End If
                    DirectCast(Me.Control.FindControl("trBeneEmail"), HtmlTableRow).Style.Remove("display")

                ElseIf objICAmendmentRights.FieldID.ToString = "14" And objICAmendmentRights.CanView = True Then

                    DirectCast(Me.Control.FindControl("trBeneCNICNo"), HtmlTableRow).Style.Remove("display")

                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        txtBeneCNCINo.Enabled = True
                    End If
                ElseIf objICAmendmentRights.FieldID.ToString = "13" And objICAmendmentRights.CanView = True Then

                    DirectCast(Me.Control.FindControl("trBeneCity"), HtmlTableRow).Style.Remove("display")
                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        ddlBeneficiaryCity.Enabled = True
                    End If
                ElseIf objICAmendmentRights.FieldID.ToString = "21" And objICAmendmentRights.CanView = True Then

                    DirectCast(Me.Control.FindControl("trBeneProvince"), HtmlTableRow).Style.Remove("display")
                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        ddlBeneficiaryProvince.Enabled = True
                    End If

                ElseIf objICAmendmentRights.FieldID.ToString = "16" And objICAmendmentRights.CanView = True Then
                    DirectCast(Me.Control.FindControl("trBeneCountry"), HtmlTableRow).Style.Remove("display")
                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        ddlBeneficiaryCountry.Enabled = True
                    End If
                ElseIf objICAmendmentRights.FieldID.ToString = "7" And objICAmendmentRights.CanView = True Then

                    DirectCast(Me.Control.FindControl("trBeneAddress1"), HtmlTableRow).Style.Remove("display")
                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        txtBeneAddress1.Enabled = True
                    End If
                ElseIf objICAmendmentRights.FieldID.ToString = "3" And objICAmendmentRights.CanView = True Then

                    DirectCast(Me.Control.FindControl("trAmountPKR"), HtmlTableRow).Style.Remove("display")
                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        txtAmountPKR.Enabled = True
                    End If

                ElseIf objICAmendmentRights.FieldID.ToString = "1" And objICAmendmentRights.CanView = True Then

                    DirectCast(Me.Control.FindControl("trDescription"), HtmlTableRow).Style.Remove("display")
                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        txtDescription.Enabled = True
                    End If
                ElseIf objICAmendmentRights.FieldID.ToString = "2" And objICAmendmentRights.CanView = True Then

                    DirectCast(Me.Control.FindControl("trRemarks"), HtmlTableRow).Style.Remove("display")
                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        txtRemarks.Enabled = True
                    End If

                ElseIf objICAmendmentRights.FieldID.ToString = "67" And objICAmendmentRights.CanView = True Then

                    DirectCast(Me.Control.FindControl("trReferenceField1"), HtmlTableRow).Style.Remove("display")
                    txtReferenceField1.Enabled = True
                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        txtReferenceField1.Enabled = True
                    End If
                ElseIf objICAmendmentRights.FieldID.ToString = "68" And objICAmendmentRights.CanView = True Then

                    DirectCast(Me.Control.FindControl("trReferenceField2"), HtmlTableRow).Style.Remove("display")
                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        txttrReferenceField2.Enabled = True
                    End If
                ElseIf objICAmendmentRights.FieldID.ToString = "69" And objICAmendmentRights.CanView = True Then

                    DirectCast(Me.Control.FindControl("trReferenceField3"), HtmlTableRow).Style.Remove("display")
                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        txtReferenceField3.Enabled = True
                    End If
                ElseIf objICAmendmentRights.FieldID.ToString = "4" And objICAmendmentRights.CanView = True Then

                    DirectCast(Me.Control.FindControl("trAmountInWords"), HtmlTableRow).Style.Remove("display")
                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        txtAmountInWords.Enabled = True
                    End If
                ElseIf objICAmendmentRights.FieldID.ToString = "77" And objICAmendmentRights.CanView = True Then

                    DirectCast(Me.Control.FindControl("trValueDate"), HtmlTableRow).Style.Remove("display")
                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        txtValueDate.Enabled = True
                        sharedCalendarValueDate.Enabled = True
                    End If
                ElseIf objICAmendmentRights.FieldID.ToString = "374" And objICAmendmentRights.CanView = True Then

                    DirectCast(Me.Control.FindControl("trBeneIDType"), HtmlTableRow).Style.Remove("display")
                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        ddlBeneIDType.Enabled = True
                    End If

                ElseIf objICAmendmentRights.FieldID.ToString = "375" And objICAmendmentRights.CanView = True Then

                    DirectCast(Me.Control.FindControl("trBeneIDNo"), HtmlTableRow).Style.Remove("display")

                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        txtBeneIDNo.Enabled = True
                    End If

                ElseIf objICAmendmentRights.FieldID.ToString = "11" And objICAmendmentRights.CanView = True Then

                    LoadddlBenePickLocation()
                    DirectCast(Me.Control.FindControl("trBenePickUpLocation"), HtmlTableRow).Style.Remove("display")
                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        ddlBenePickUpLocation.Enabled = True
                    End If
                ElseIf objICAmendmentRights.FieldID.ToString = "378" And objICAmendmentRights.CanView = True Then


                    DirectCast(Me.Control.FindControl("trBeneficiaryAccountType"), HtmlTableRow).Style.Remove("display")
                    If objICAmendmentRights.IsAmendmentAllow = True Then
                        ddlBeneficiaryAccountType.Enabled = True
                    End If

                End If

            Next
        Else
            UIUtilities.ShowDialog(Me, "Instruction Amendment Queue", "Amendment settings are not defined against selected instruction", ICBO.IC.Dialogmessagetype.Failure, NavigateURL)
            Exit Sub
        End If

    End Sub
    Private Sub HideRequiredMarkSign()
        lblReqBeneName.Visible = False
        lblReqBankAccountNumber.Visible = False


        lblReqAccountTitle.Visible = False
        lblReqValueDate.Visible = False
        lblReqBeneAddress1.Visible = False

        lblReqBankAccountNumber.Visible = False
        lblReqBeneAccountBranchCode.Visible = False
        lblReqBeneAccountCurrency.Visible = False

        lblReqBeneficiaryBranchAddress.Visible = False
        lblReqPhonNo.Visible = False
        lblReqBeneMobNo.Visible = False
        lblReqEmail.Visible = False
        lblReqBeneNICNo.Visible = False
        lblReqPrintDate.Visible = False
        lblReqAmountPKR.Visible = False
        lblReqAmountInWords.Visible = False
        lblReqDescription.Visible = False
        lblReqRemarks.Visible = False
        lblReqReferenceField1.Visible = False
        lblReqtrReferenceField2.Visible = False
        lblReqReferenceField3.Visible = False
        lblReqInstrumentNo.Visible = False

        lblReqBeneIDNo.Visible = False
    End Sub
    Private Sub ClearOnlineForm()
        txtBeneName.Text = Nothing
        txtBankAccountNumber.Text = Nothing
        ddlPrintLocation.ClearSelection()
        ddlBenePickUpLocation.ClearSelection()
        txtBeneIDNo.Text = Nothing
        ddlBeneIDType.ClearSelection()
        txtAccountTitle.Text = Nothing
        txtBeneBranchAddress.Text = Nothing
        txtBenePhoneNo.Text = Nothing
        txtBeneMobileNo.Text = Nothing
        txtBeneEmail.Text = Nothing
        txtBeneCNCINo.Text = Nothing
        ddlBeneficiaryCity.ClearSelection()
        ddlBeneficiaryProvince.ClearSelection()
        ddlBeneficiaryCountry.ClearSelection()
        ddlBeneficiaryAccountType.ClearSelection()
        ddlGLAccountBranchCode.ClearSelection()
        LoadddlDDDrawnLocationProvince()
        LoadddlDDDrawnLocationCity()
        txtBeneAddress1.Text = Nothing
        txtBeneAccountBranchCode.Text = Nothing
        txtBeneAccountCurrency.Text = Nothing

        txtAmountPKR.Text = Nothing
        txtAmountInWords.Text = Nothing
        txtDescription.Text = Nothing
        txtDescription.Text = Nothing
        txtRemarks.Text = Nothing
        txtReferenceField1.Text = Nothing
        txttrReferenceField2.Text = Nothing
        txtReferenceField3.Text = Nothing
        txtValueDate.SelectedDate = Nothing
        txtPrintDate.SelectedDate = Nothing
        txtInstrumentNo.Text = Nothing
        'radBeneAddress1.Enabled = False


    End Sub



    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        If Page.IsValid Then


            Try

                Dim objICInstruction As ICInstruction
                Dim objICCity As New ICCity
                Dim objICProvince As New ICProvince
                Dim objICCountry As New ICCountry
                Dim objICUser As New ICUser
                Dim objICProductType As New ICProductType
                Dim ActionStr As String = ""
                Dim objICBeneficiary As New ICBeneficiary
                Dim objICCompany As New ICCompany
                Dim dtInstructionInfo As New DataTable
                Dim ArryListStatus As ArrayList
                Dim ArrayListInstructionID As ArrayList
                Dim StrAccountDetails As String()
                objICCity.es.Connection.CommandTimeout = 3600
                objICProvince.es.Connection.CommandTimeout = 3600
                objICCountry.es.Connection.CommandTimeout = 3600
                objICUser.es.Connection.CommandTimeout = 3600
                objICProductType.es.Connection.CommandTimeout = 3600
                objICBeneficiary.es.Connection.CommandTimeout = 3600
                objICCompany.es.Connection.CommandTimeout = 3600



                objICUser.LoadByPrimaryKey(Me.UserId.ToString)

                objICCompany.LoadByPrimaryKey(ddlCompany.SelectedValue.ToString)
                If IsBulkAmend = True Then
                    If Not ArrayListInstID Is Nothing Then
                        For i = 0 To ArrayListInstID.Length - 1
                            objICInstruction = New ICInstruction
                            objICInstruction.LoadByPrimaryKey(ArrayListInstID(i))
                            ddlCompany.SelectedValue = objICInstruction.CompanyCode
                            ddlAccPNature.SelectedValue = objICInstruction.ClientAccountNo + "-" + objICInstruction.ClientAccountBranchCode + "-" + objICInstruction.ClientAccountCurrency + "-" + objICInstruction.PaymentNatureCode
                            ddlProductType.SelectedValue = objICInstruction.ProductTypeCode
                            objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)

                            'If txtBeneName.Text <> "" And txtBeneName.Text <> "Enter Beneficiary Name" Then
                            '    objICInstruction.BeneficiaryName = txtBeneName.Text.ToString
                            'End If

                            ''Account Verification In case of FT and IBFT
                            If txtBankAccountNumber.Text <> "" And txtBankAccountNumber.Text <> "Enter Bank Account Number" Then
                                objICInstruction.BeneficiaryAccountNo = txtBankAccountNumber.Text.ToString
                            Else
                                If objICProductType.DisbursementMode = "Direct Credit" Or objICProductType.DisbursementMode = "Other Credit" Then
                                    If DirectCast(Me.Control.FindControl("trBankAccountNumber"), HtmlTableRow).Visible = True Then
                                        UIUtilities.ShowDialog(Me, "Error", "Please enter beneficiary account number", Dialogmessagetype.Failure)
                                        Exit Sub
                                    End If
                                End If
                            End If
                            If txtBeneAccountBranchCode.Text <> "" And txtBeneAccountBranchCode.Text <> "Enter Account Branch Code" Then
                                objICInstruction.BeneAccountBranchCode = txtBeneAccountBranchCode.Text.ToString
                            Else
                                If objICProductType.DisbursementMode = "Direct Credit" Then
                                    If ddlGLAccountBranchCode.SelectedValue.ToString <> "0" And ddlGLAccountBranchCode.SelectedValue.ToString <> "" Then
                                        objICInstruction.BeneAccountBranchCode = ddlGLAccountBranchCode.SelectedValue.ToString
                                    Else
                                        If DirectCast(Me.Control.FindControl("trBeneAccountBranchCode"), HtmlTableRow).Visible = True Then
                                            UIUtilities.ShowDialog(Me, "Error", "Please enter beneficiary account branch code", Dialogmessagetype.Failure)
                                            Exit Sub
                                        ElseIf DirectCast(Me.Control.FindControl("trGLAccountBranchCode"), HtmlTableRow).Visible = True And (ddlGLAccountBranchCode.SelectedValue.ToString = "" Or ddlGLAccountBranchCode.SelectedValue.ToString = "0") Then
                                            UIUtilities.ShowDialog(Me, "Error", "Please enter beneficiary account branch code", Dialogmessagetype.Failure)
                                            Exit Sub
                                        End If
                                    End If
                                End If

                            End If

                            If txtBeneAccountCurrency.Text <> "" And txtBeneAccountCurrency.Text <> "Enter Account Currency" Then

                                objICInstruction.BeneAccountCurrency = txtBeneAccountCurrency.Text.ToString
                            Else
                                If objICProductType.DisbursementMode = "Direct Credit" Then
                                    If DirectCast(Me.Control.FindControl("trBeneAccountCurrency"), HtmlTableRow).Visible = True Then
                                        UIUtilities.ShowDialog(Me, "Error", "Please enter beneficiary account Currency", Dialogmessagetype.Failure)
                                        Exit Sub
                                    End If
                                End If
                            End If
                            If txtAccountTitle.Text <> "" And txtAccountTitle.Text <> "Enter Account Title" Then
                                objICInstruction.BeneficiaryAccountTitle = txtAccountTitle.Text.ToString
                            Else
                                If objICProductType.DisbursementMode = "Direct Credit" Or objICProductType.DisbursementMode = "Other Credit" Then
                                    If DirectCast(Me.Control.FindControl("trAccountTitle"), HtmlTableRow).Visible = True Then
                                        UIUtilities.ShowDialog(Me, "Error", "Please enter beneficiary account title", Dialogmessagetype.Failure)
                                        Exit Sub
                                    End If
                                End If
                            End If
                            If ddlBeneBankName.SelectedValue.ToString <> "0" Then
                                objICInstruction.BeneficiaryBankName = ddlBeneBankName.SelectedItem.ToString
                                objICInstruction.BeneficiaryBankCode = ddlBeneBankName.SelectedValue.ToString
                                objICBeneficiary.BankName = ddlBeneBankName.SelectedItem.ToString
                            Else
                                If objICProductType.DisbursementMode = "Other Credit" Then
                                    If DirectCast(Me.Control.FindControl("trBeneBankName"), HtmlTableRow).Visible = True Then
                                        UIUtilities.ShowDialog(Me, "Error", "Please enter beneficiary Bank", Dialogmessagetype.Failure)
                                        Exit Sub
                                    End If
                                End If
                            End If


                            If objICProductType.DisbursementMode = "Direct Credit" Or objICProductType.DisbursementMode = "Other Credit" Then
                                If txtBeneName.Text <> "" Then
                                    objICInstruction.BeneficiaryName = txtBeneName.Text
                                Else
                                    If txtAccountTitle.Text <> "" Then
                                        objICInstruction.BeneficiaryName = txtAccountTitle.Text
                                    Else
                                        objICInstruction.BeneficiaryName = txtBeneName.Text
                                    End If
                                End If
                            Else
                                objICInstruction.BeneficiaryName = txtBeneName.Text
                            End If
                            If objICProductType.DisbursementMode = "Direct Credit" Then
                                If ddlBeneficiaryAccountType.SelectedValue.ToString <> "0" Then
                                    objICInstruction.BeneficaryAccountType = ddlBeneficiaryAccountType.SelectedValue.ToString
                                    If ddlBeneficiaryAccountType.SelectedValue.ToString = "GL" Then
                                        If txtBankAccountNumber.Text.Trim.Length <> 17 Then
                                            UIUtilities.ShowDialog(Me, "Error", "Invalid beneficiary GL account length", ICBO.IC.Dialogmessagetype.Failure)
                                            Exit Sub
                                        End If
                                    End If
                                Else
                                    objICInstruction.BeneficaryAccountType = "RB"
                                End If
                            Else
                                If ddlBeneficiaryAccountType.SelectedValue.ToString <> "0" Then
                                    objICInstruction.BeneficaryAccountType = ddlBeneficiaryAccountType.SelectedValue.ToString
                                Else
                                    objICInstruction.BeneficaryAccountType = Nothing
                                End If
                            End If

                            If ddlDDPayableLocationCity.SelectedValue.ToString <> "0" Then
                                'objICInstruction.DDPayableLocation = ddlDDPayableLocationCity.SelectedValue.ToString
                                objICInstruction.DDDrawnCityCode = ddlDDPayableLocationCity.SelectedValue.ToString
                            End If
                            If ddlPrintLocation.SelectedValue.ToString <> "0" Then
                                objICInstruction.PrintLocationName = ddlPrintLocation.SelectedItem.ToString
                                objICInstruction.PrintLocationCode = ddlPrintLocation.SelectedValue.ToString
                            End If
                            'objICInstruction.BeneficiaryBranchCode = txtBankBranchCode.Text.ToString


                            If txtBenePhoneNo.Text <> "" And txtBenePhoneNo.Text <> "Enter Beneficiary Phone No." Then
                                objICInstruction.BeneficiaryPhone = txtBenePhoneNo.Text.ToString
                            End If
                            If txtBeneMobileNo.Text <> "" And txtBeneMobileNo.Text <> "Enter Beneficiary Mobile No." Then
                                objICInstruction.BeneficiaryMobile = txtBeneMobileNo.Text.ToString
                            End If
                            If txtBeneCNCINo.Text <> "" And txtBeneCNCINo.Text <> "Enter CNIC No" Then
                                objICInstruction.BeneficiaryCNIC = txtBeneCNCINo.Text.ToString
                            End If
                            If txtBeneEmail.Text <> "" And txtBeneEmail.Text <> "Enter Email Address" Then
                                objICInstruction.BeneficiaryEmail = txtBeneEmail.Text.ToString
                            End If

                            If ddlBeneficiaryCity.SelectedValue.ToString <> "0" Then
                                objICInstruction.BeneficiaryCity = ddlBeneficiaryCity.SelectedValue.ToString
                                objICBeneficiary.BeneCity = ddlBeneficiaryCity.SelectedItem.Text.ToString
                            End If
                            If ddlBeneficiaryProvince.SelectedValue.ToString <> "0" Then
                                objICInstruction.BeneficiaryProvince = ddlBeneficiaryProvince.SelectedValue.ToString
                                objICBeneficiary.BeneProvince = ddlBeneficiaryProvince.SelectedItem.Text.ToString

                            End If
                            If ddlBeneficiaryCountry.SelectedValue.ToString <> "0" Then
                                objICInstruction.BeneficiaryCountry = ddlBeneficiaryCountry.SelectedValue.ToString

                            End If
                            If txtBeneAddress1.Text <> "" And txtBeneAddress1.Text <> "Enter Address" Then
                                objICInstruction.BeneficiaryAddress = txtBeneAddress1.Text.ToString
                            End If

                            If txtAmountPKR.Text <> "" And txtAmountPKR.Text <> "Amount (PKR) " Then
                                objICInstruction.Amount = CDbl(txtAmountPKR.Text)
                            End If
                            If txtAmountInWords.Text <> "" And txtAmountInWords.Text <> "Amount In Words" Then
                                objICInstruction.AmountInWords = txtAmountInWords.Text.ToString
                            End If
                            If txtBeneBranchAddress.Text <> "" Then
                                objICInstruction.BeneBranchAddress = txtBeneBranchAddress.Text.ToString
                            End If
                            If txtDescription.Text <> "" And txtDescription.Text <> "Enter Description" Then
                                objICInstruction.Description = txtDescription.Text.ToString
                            End If
                            If txtRemarks.Text <> "" And txtRemarks.Text <> "Enter Remarks" Then
                                objICInstruction.Remarks = txtRemarks.Text.ToString
                            End If
                            If txtReferenceField1.Text <> "" And txtReferenceField1.Text <> "Enter Reference Remarks" Then
                                objICInstruction.ReferenceField1 = txtReferenceField1.Text.ToString
                            End If
                            If txttrReferenceField2.Text <> "" And txttrReferenceField2.Text <> "Enter Reference Remarks" Then
                                objICInstruction.ReferenceField2 = txttrReferenceField2.Text.ToString
                            End If
                            If txtReferenceField3.Text <> "" And txtReferenceField3.Text <> "Enter Reference Remarks" Then
                                objICInstruction.ReferenceField3 = txtReferenceField3.Text.ToString
                            End If
                            If Not txtValueDate.SelectedDate Is Nothing Then
                                objICInstruction.ValueDate = txtValueDate.SelectedDate
                            End If
                            If Not txtPrintDate.SelectedDate Is Nothing Then
                                objICInstruction.PrintDate = txtPrintDate.SelectedDate
                            End If
                            If txtInstrumentNo.Text <> "" And txtInstrumentNo.Text <> "Enter Reference Remarks" Then
                                If ICMasterSeriesController.CheckIsInstrumentNumberIsUsedByInstrumentNumber(txtInstrumentNo.Text.ToString) = True Then
                                    UIUtilities.ShowDialog(Me, "Amend Instruction", "Instrument number is already assigned or used", ICBO.IC.Dialogmessagetype.Failure)
                                    Exit Sub
                                End If
                                If objICInstruction.InstrumentNo <> "" Then
                                    ICInstrumentController.MarkInstrumentNumberIsCancelledByInstructionIDAndInstrumentNumberAndOfficeCode(objICInstruction.InstrumentNo.ToString, objICInstruction.PrintLocationCode.ToString, objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                End If
                                objICInstruction.InstrumentNo = txtInstrumentNo.Text.ToString
                            End If

                            ''COTC

                            If ddlBenePickUpLocation.SelectedValue <> "0" Then
                                objICInstruction.BeneficiaryBranchCode = ddlBenePickUpLocation.SelectedValue.ToString
                            End If
                            If ddlBeneIDType.SelectedValue <> "0" Then
                                objICInstruction.BeneficiaryIDType = ddlBeneIDType.SelectedValue.ToString
                            End If
                            If txtBeneIDNo.Text <> "" Then
                                objICInstruction.BeneficiaryIDNo = txtBeneIDNo.Text
                            End If

                            objICInstruction.Save()
                            ActionStr += "Instruction with id [ " & objICInstruction.InstructionID.ToString & " ] of client [ " & ddlCompany.SelectedItem.ToString & " ], "
                            ActionStr += "of amount [ " & objICInstruction.Amount.ToString & "] [ " & objICInstruction.AmountInWords.ToString & " ] amended. Client account number was [ " & ddlAccPNature.SelectedValue.ToString.Split("-")(0).ToString & " ],"
                            ActionStr += " branch code [" & ddlAccPNature.SelectedValue.ToString.Split("-")(1).ToString & " ], currency [" & ddlAccPNature.SelectedValue.ToString.Split("-")(2) & " ]."
                            ActionStr += " Product type [ " & objICProductType.ProductTypeName & " ], Disbursement mode [ " & objICProductType.DisbursementMode & " ], Payment nature [ " & ddlAccPNature.SelectedValue.Split("-")(3).ToString & " ]."
                            ICInstructionController.UpdateInstructionStatus(ArrayListInstID(i).ToString, objICInstruction.Status, "9", Me.UserId.ToString, Me.UserInfo.Username.ToString, "Remarks", "Amend", ActionStr)
                        Next
                        UIUtilities.ShowDialog(Me, "Amend Instruction", ArrayListInstID.Count & " Instruction amended successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        '' For Email Javed


                        ArryListStatus = New ArrayList
                        ArrayListInstructionID = New ArrayList
                        ArryListStatus.Clear()
                        ArryListStatus.Add(9)
                        ArrayListInstructionID.Clear()
                        For i = 0 To ArrayListInstID.Length - 1
                            ArrayListInstructionID.Add(ArrayListInstID(i))
                        Next
                        dtInstructionInfo = New DataTable
                        dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArrayListInstructionID, ArryListStatus, False)
                        EmailUtilities.Txnamended(dtInstructionInfo, Me.UserInfo.Username.ToString)
                        SMSUtilities.Txnamended(Me.UserInfo.Username.ToString, dtInstructionInfo, ArryListStatus, ArrayListInstructionID)


                    Else
                        objICInstruction = New ICInstruction
                        objICInstruction.LoadByPrimaryKey(InstructionID)
                        ddlCompany.SelectedValue = objICInstruction.CompanyCode
                        ddlAccPNature.SelectedValue = objICInstruction.ClientAccountNo + "-" + objICInstruction.ClientAccountBranchCode + "-" + objICInstruction.ClientAccountCurrency + "-" + objICInstruction.PaymentNatureCode
                        ddlProductType.SelectedValue = objICInstruction.ProductTypeCode
                        objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)

                        If txtBeneName.Text <> "" And txtBeneName.Text <> "Enter Beneficiary Name" Then
                            objICInstruction.BeneficiaryName = txtBeneName.Text.ToString
                        End If
                        ''Account Verification In case of FT and IBFT
                        If txtBankAccountNumber.Text <> "" And txtBankAccountNumber.Text <> "Enter Bank Account Number" Then
                            objICInstruction.BeneficiaryAccountNo = txtBankAccountNumber.Text.ToString
                        Else
                            If objICProductType.DisbursementMode = "Direct Credit" Or objICProductType.DisbursementMode = "Other Credit" Then
                                If DirectCast(Me.Control.FindControl("trBankAccountNumber"), HtmlTableRow).Visible = True Then
                                    UIUtilities.ShowDialog(Me, "Error", "Please enter beneficiary account number", Dialogmessagetype.Failure)
                                    Exit Sub
                                End If
                            End If
                        End If
                        If txtBeneAccountBranchCode.Text <> "" And txtBeneAccountBranchCode.Text <> "Enter Account Branch Code" Then
                            objICInstruction.BeneAccountBranchCode = txtBeneAccountBranchCode.Text.ToString
                        Else
                            If objICProductType.DisbursementMode = "Direct Credit" Then
                                If ddlGLAccountBranchCode.SelectedValue.ToString <> "0" And ddlGLAccountBranchCode.SelectedValue.ToString <> "" Then
                                    objICInstruction.BeneAccountBranchCode = ddlGLAccountBranchCode.SelectedValue.ToString
                                Else
                                    If DirectCast(Me.Control.FindControl("trBeneAccountBranchCode"), HtmlTableRow).Visible = True Then
                                        UIUtilities.ShowDialog(Me, "Error", "Please enter beneficiary account branch code", Dialogmessagetype.Failure)
                                        Exit Sub
                                    ElseIf DirectCast(Me.Control.FindControl("trGLAccountBranchCode"), HtmlTableRow).Visible = True And (ddlGLAccountBranchCode.SelectedValue.ToString = "" Or ddlGLAccountBranchCode.SelectedValue.ToString = "0") Then
                                        UIUtilities.ShowDialog(Me, "Error", "Please enter beneficiary account branch code", Dialogmessagetype.Failure)
                                        Exit Sub
                                    End If
                                End If
                            End If

                        End If
                        If txtBeneAccountCurrency.Text <> "" And txtBeneAccountCurrency.Text <> "Enter Account Currency" Then

                            objICInstruction.BeneAccountCurrency = txtBeneAccountCurrency.Text.ToString
                        Else
                            If objICProductType.DisbursementMode = "Direct Credit" Then
                                If DirectCast(Me.Control.FindControl("trBeneAccountCurrency"), HtmlTableRow).Visible = True Then
                                    UIUtilities.ShowDialog(Me, "Error", "Please enter beneficiary account Currency", Dialogmessagetype.Failure)
                                    Exit Sub
                                End If
                            End If
                        End If
                        If txtAccountTitle.Text <> "" And txtAccountTitle.Text <> "Enter Account Title" Then
                            objICInstruction.BeneficiaryAccountTitle = txtAccountTitle.Text.ToString
                        Else
                            If objICProductType.DisbursementMode = "Direct Credit" Or objICProductType.DisbursementMode = "Other Credit" Then
                                If DirectCast(Me.Control.FindControl("trAccountTitle"), HtmlTableRow).Visible = True Then
                                    UIUtilities.ShowDialog(Me, "Error", "Please enter beneficiary account title", Dialogmessagetype.Failure)
                                    Exit Sub
                                End If
                            End If
                        End If
                        If ddlBeneBankName.SelectedValue.ToString <> "0" Then
                            objICInstruction.BeneficiaryBankName = ddlBeneBankName.SelectedItem.ToString
                            objICInstruction.BeneficiaryBankCode = ddlBeneBankName.SelectedValue.ToString
                            objICBeneficiary.BankName = ddlBeneBankName.SelectedItem.ToString
                        Else
                            If objICProductType.DisbursementMode = "Other Credit" Then
                                If DirectCast(Me.Control.FindControl("trBeneBankName"), HtmlTableRow).Visible = True Then
                                    UIUtilities.ShowDialog(Me, "Error", "Please enter beneficiary Bank", Dialogmessagetype.Failure)
                                    Exit Sub
                                End If
                            End If
                        End If
                        If objICProductType.DisbursementMode = "Direct Credit" Then
                            If ddlBeneficiaryAccountType.SelectedValue.ToString <> "0" Then
                                objICInstruction.BeneficaryAccountType = ddlBeneficiaryAccountType.SelectedValue.ToString
                                If ddlBeneficiaryAccountType.SelectedValue.ToString = "GL" Then
                                    If txtBankAccountNumber.Text.Trim.Length <> 17 Then
                                        UIUtilities.ShowDialog(Me, "Error", "Invalid beneficiary GL account length", ICBO.IC.Dialogmessagetype.Failure)
                                        Exit Sub
                                    End If
                                End If
                            Else
                                objICInstruction.BeneficaryAccountType = Nothing
                            End If
                        Else
                            If ddlBeneficiaryAccountType.SelectedValue.ToString <> "0" Then
                                objICInstruction.BeneficaryAccountType = ddlBeneficiaryAccountType.SelectedValue.ToString
                            Else
                                objICInstruction.BeneficaryAccountType = Nothing
                            End If
                        End If
                        If ddlDDPayableLocationCity.SelectedValue.ToString <> "0" Then
                            'objICInstruction.DDPayableLocation = ddlDDPayableLocationCity.SelectedValue.ToString
                            objICInstruction.DDDrawnCityCode = ddlDDPayableLocationCity.SelectedValue.ToString
                        End If
                        If ddlPrintLocation.SelectedValue.ToString <> "0" Then
                            objICInstruction.PrintLocationName = ddlPrintLocation.SelectedItem.ToString
                            objICInstruction.PrintLocationCode = ddlPrintLocation.SelectedValue.ToString
                        End If
                        'objICInstruction.BeneficiaryBranchCode = txtBankBranchCode.Text.ToString

                        If txtBeneBranchAddress.Text <> "" Then
                            objICInstruction.BeneBranchAddress = txtBeneBranchAddress.Text.ToString
                        End If
                        If txtBenePhoneNo.Text <> "" And txtBenePhoneNo.Text <> "Enter Beneficiary Phone No." Then
                            objICInstruction.BeneficiaryPhone = txtBenePhoneNo.Text.ToString
                        End If
                        If txtBeneMobileNo.Text <> "" And txtBeneMobileNo.Text <> "Enter Beneficiary Mobile No." Then
                            objICInstruction.BeneficiaryMobile = txtBeneMobileNo.Text.ToString
                        End If
                        If txtBeneCNCINo.Text <> "" And txtBeneCNCINo.Text <> "Enter CNIC No" Then
                            objICInstruction.BeneficiaryCNIC = txtBeneCNCINo.Text.ToString
                        End If
                        If txtBeneEmail.Text <> "" And txtBeneEmail.Text <> "Enter Email Address" Then
                            objICInstruction.BeneficiaryEmail = txtBeneEmail.Text.ToString
                        End If
                        If ddlBeneficiaryCity.SelectedValue.ToString <> "0" Then
                            objICInstruction.BeneficiaryCity = ddlBeneficiaryCity.SelectedValue.ToString
                            objICBeneficiary.BeneCity = ddlBeneficiaryCity.SelectedItem.Text.ToString
                        End If
                        If ddlBeneficiaryProvince.SelectedValue.ToString <> "0" Then
                            objICInstruction.BeneficiaryProvince = ddlBeneficiaryProvince.SelectedValue.ToString
                            objICBeneficiary.BeneProvince = ddlBeneficiaryProvince.SelectedItem.Text.ToString

                        End If
                        If ddlBeneficiaryCountry.SelectedValue.ToString <> "0" Then
                            objICInstruction.BeneficiaryCountry = ddlBeneficiaryCountry.SelectedValue.ToString

                        End If
                        If txtBeneAddress1.Text <> "" And txtBeneAddress1.Text <> "Enter Address" Then
                            objICInstruction.BeneficiaryAddress = txtBeneAddress1.Text.ToString
                        End If

                        If txtAmountPKR.Text <> "" And txtAmountPKR.Text <> "Amount (PKR) " Then
                            objICInstruction.Amount = CDbl(txtAmountPKR.Text)
                        End If
                        If txtAmountInWords.Text <> "" And txtAmountInWords.Text <> "Amount In Words" Then
                            objICInstruction.AmountInWords = txtAmountInWords.Text.ToString
                        End If
                        If txtDescription.Text <> "" And txtDescription.Text <> "Enter Description" Then
                            objICInstruction.Description = txtDescription.Text.ToString
                        End If
                        If txtRemarks.Text <> "" And txtRemarks.Text <> "Enter Remarks" Then
                            objICInstruction.Remarks = txtRemarks.Text.ToString
                        End If
                        If txtReferenceField1.Text <> "" And txtReferenceField1.Text <> "Enter Reference Remarks" Then
                            objICInstruction.ReferenceField1 = txtReferenceField1.Text.ToString
                        End If
                        If txttrReferenceField2.Text <> "" And txttrReferenceField2.Text <> "Enter Reference Remarks" Then
                            objICInstruction.ReferenceField2 = txttrReferenceField2.Text.ToString
                        End If
                        If txtReferenceField3.Text <> "" And txtReferenceField3.Text <> "Enter Reference Remarks" Then
                            objICInstruction.ReferenceField3 = txtReferenceField3.Text.ToString
                        End If
                        If Not txtValueDate.SelectedDate Is Nothing Then
                            objICInstruction.ValueDate = txtValueDate.SelectedDate
                        End If
                        If txtInstrumentNo.Text <> "" And txtInstrumentNo.Text <> "Enter Reference Remarks" Then
                            If ICMasterSeriesController.CheckIsInstrumentNumberIsUsedByInstrumentNumber(txtInstrumentNo.Text.ToString) = True Then
                                UIUtilities.ShowDialog(Me, "Amend Instruction", "Instrument number is already assigned or used", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                            If objICInstruction.InstrumentNo <> "" Then
                                ICInstrumentController.MarkInstrumentNumberIsCancelledByInstructionIDAndInstrumentNumberAndOfficeCode(objICInstruction.InstrumentNo.ToString, objICInstruction.PrintLocationCode.ToString, objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            End If
                            objICInstruction.InstrumentNo = txtInstrumentNo.Text.ToString
                        End If
                        If Not txtPrintDate.SelectedDate Is Nothing Then
                            objICInstruction.PrintDate = txtPrintDate.SelectedDate
                        End If



                        ''COTC

                        If ddlBenePickUpLocation.SelectedValue <> "0" Then
                            objICInstruction.BeneficiaryBranchCode = ddlBenePickUpLocation.SelectedValue.ToString
                        End If
                        If ddlBeneIDType.SelectedValue <> "0" Then
                            objICInstruction.BeneficiaryIDType = ddlBeneIDType.SelectedValue.ToString
                        End If
                        If txtBeneIDNo.Text <> "" Then
                            objICInstruction.BeneficiaryIDNo = txtBeneIDNo.Text
                        End If

                        objICInstruction.Save()
                        ActionStr += "Instruction with id [ " & objICInstruction.InstructionID.ToString & " ] of client [ " & ddlCompany.SelectedItem.ToString & " ], "
                        ActionStr += "of amount [ " & objICInstruction.Amount.ToString & "] [ " & objICInstruction.AmountInWords.ToString & " ] amended. Client account number was [ " & ddlAccPNature.SelectedValue.ToString.Split("-")(0).ToString & " ],"
                        ActionStr += " branch code [" & ddlAccPNature.SelectedValue.ToString.Split("-")(1).ToString & " ], currency [" & ddlAccPNature.SelectedValue.ToString.Split("-")(2) & " ]."
                        ActionStr += " Product type [ " & objICProductType.ProductTypeName & " ], Disbursement mode [ " & objICProductType.DisbursementMode & " ], Payment nature [ " & ddlAccPNature.SelectedValue.Split("-")(3).ToString & " ]."
                        ICInstructionController.UpdateInstructionStatus(InstructionID, objICInstruction.Status, "9", Me.UserId.ToString, Me.UserInfo.Username.ToString, "Remarks", "Amend", ActionStr)
                        UIUtilities.ShowDialog(Me, "Amend Instruction", "[ 1 ] Instruction amended successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        ''For Email Javed


                        ArryListStatus = New ArrayList
                        ArrayListInstructionID = New ArrayList
                        ArryListStatus.Clear()
                        ArryListStatus.Add(9)
                        ArrayListInstructionID.Clear()
                        ArrayListInstructionID.Add(InstructionID)
                        dtInstructionInfo = New DataTable
                        dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArrayListInstructionID, ArryListStatus, False)
                        EmailUtilities.Txnamended(dtInstructionInfo, Me.UserInfo.Username.ToString)
                        SMSUtilities.Txnamended(Me.UserInfo.Username.ToString, dtInstructionInfo, ArryListStatus, ArrayListInstructionID)




                    End If



                    Exit Sub
                Else

                    objICInstruction = New ICInstruction
                    objICInstruction.LoadByPrimaryKey(InstructionID)
                    ddlCompany.SelectedValue = objICInstruction.CompanyCode
                    ddlAccPNature.SelectedValue = objICInstruction.ClientAccountNo + "-" + objICInstruction.ClientAccountBranchCode + "-" + objICInstruction.ClientAccountCurrency + "-" + objICInstruction.PaymentNatureCode
                    ddlProductType.SelectedValue = objICInstruction.ProductTypeCode
                    objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)

                    If txtBeneName.Text <> "" And txtBeneName.Text <> "Enter Beneficiary Name" Then
                        objICInstruction.BeneficiaryName = txtBeneName.Text.ToString
                    End If
                    ''Account Verification In case of FT and IBFT
                    If txtBankAccountNumber.Text <> "" And txtBankAccountNumber.Text <> "Enter Bank Account Number" Then
                        objICInstruction.BeneficiaryAccountNo = txtBankAccountNumber.Text.ToString
                    Else
                        If objICProductType.DisbursementMode = "Direct Credit" Or objICProductType.DisbursementMode = "Other Credit" Then
                            If DirectCast(Me.Control.FindControl("trBankAccountNumber"), HtmlTableRow).Visible = True Then
                                UIUtilities.ShowDialog(Me, "Error", "Please enter beneficiary account number", Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        End If
                    End If
                    If txtBeneAccountBranchCode.Text <> "" And txtBeneAccountBranchCode.Text <> "Enter Account Branch Code" Then
                        objICInstruction.BeneAccountBranchCode = txtBeneAccountBranchCode.Text.ToString
                    Else
                        If objICProductType.DisbursementMode = "Direct Credit" Then
                            If ddlGLAccountBranchCode.SelectedValue.ToString <> "0" And ddlGLAccountBranchCode.SelectedValue.ToString <> "" Then
                                objICInstruction.BeneAccountBranchCode = ddlGLAccountBranchCode.SelectedValue.ToString
                            Else
                                If DirectCast(Me.Control.FindControl("trBeneAccountBranchCode"), HtmlTableRow).Visible = True Then
                                    UIUtilities.ShowDialog(Me, "Error", "Please enter beneficiary account branch code", Dialogmessagetype.Failure)
                                    Exit Sub
                                ElseIf DirectCast(Me.Control.FindControl("trGLAccountBranchCode"), HtmlTableRow).Visible = True And (ddlGLAccountBranchCode.SelectedValue.ToString = "" Or ddlGLAccountBranchCode.SelectedValue.ToString = "0") Then
                                    UIUtilities.ShowDialog(Me, "Error", "Please enter beneficiary account branch code", Dialogmessagetype.Failure)
                                    Exit Sub
                                End If
                            End If
                        End If

                    End If
                    If txtBeneAccountCurrency.Text <> "" And txtBeneAccountCurrency.Text <> "Enter Account Currency" Then

                        objICInstruction.BeneAccountCurrency = txtBeneAccountCurrency.Text.ToString
                    Else
                        If objICProductType.DisbursementMode = "Direct Credit" Then
                            If DirectCast(Me.Control.FindControl("trBeneAccountCurrency"), HtmlTableRow).Visible = True Then
                                UIUtilities.ShowDialog(Me, "Error", "Please enter beneficiary account Currency", Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        End If
                    End If
                    If txtAccountTitle.Text <> "" And txtAccountTitle.Text <> "Enter Account Title" Then
                        objICInstruction.BeneficiaryAccountTitle = txtAccountTitle.Text.ToString
                    Else
                        If objICProductType.DisbursementMode = "Direct Credit" Or objICProductType.DisbursementMode = "Other Credit" Then
                            If DirectCast(Me.Control.FindControl("trAccountTitle"), HtmlTableRow).Visible = True Then
                                UIUtilities.ShowDialog(Me, "Error", "Please enter beneficiary account title", Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        End If
                    End If
                    If ddlBeneBankName.SelectedValue.ToString <> "0" Then
                        objICInstruction.BeneficiaryBankName = ddlBeneBankName.SelectedItem.ToString
                        objICInstruction.BeneficiaryBankCode = ddlBeneBankName.SelectedValue.ToString
                        objICBeneficiary.BankName = ddlBeneBankName.SelectedItem.ToString
                    Else
                        If objICProductType.DisbursementMode = "Other Credit" Then
                            If DirectCast(Me.Control.FindControl("trBeneBankName"), HtmlTableRow).Visible = True Then
                                UIUtilities.ShowDialog(Me, "Error", "Please enter beneficiary Bank", Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        End If
                    End If
                    If objICProductType.DisbursementMode = "Direct Credit" Then
                        If ddlBeneficiaryAccountType.SelectedValue.ToString <> "0" Then
                            objICInstruction.BeneficaryAccountType = ddlBeneficiaryAccountType.SelectedValue.ToString
                            If ddlBeneficiaryAccountType.SelectedValue.ToString = "GL" Then
                                If txtBankAccountNumber.Text.Trim.Length <> 17 Then
                                    UIUtilities.ShowDialog(Me, "Error", "Invalid beneficiary GL account length", ICBO.IC.Dialogmessagetype.Failure)
                                    Exit Sub
                                End If
                            End If
                        Else
                            objICInstruction.BeneficaryAccountType = Nothing
                        End If
                    Else
                        If ddlBeneficiaryAccountType.SelectedValue.ToString <> "0" Then
                            objICInstruction.BeneficaryAccountType = ddlBeneficiaryAccountType.SelectedValue.ToString
                        Else
                            objICInstruction.BeneficaryAccountType = Nothing
                        End If
                    End If
                    If ddlDDPayableLocationCity.SelectedValue.ToString <> "0" Then
                        'objICInstruction.DDPayableLocation = ddlDDPayableLocationCity.SelectedValue.ToString
                        objICInstruction.DDDrawnCityCode = ddlDDPayableLocationCity.SelectedValue.ToString
                    End If
                    If ddlPrintLocation.SelectedValue.ToString <> "0" Then
                        objICInstruction.PrintLocationName = ddlPrintLocation.SelectedItem.ToString
                        objICInstruction.PrintLocationCode = ddlPrintLocation.SelectedValue.ToString
                    End If
                    'objICInstruction.BeneficiaryBranchCode = txtBankBranchCode.Text.ToString

                    If txtBeneBranchAddress.Text <> "" Then
                        objICInstruction.BeneBranchAddress = txtBeneBranchAddress.Text.ToString
                    End If
                    If txtBenePhoneNo.Text <> "" And txtBenePhoneNo.Text <> "Enter Beneficiary Phone No." Then
                        objICInstruction.BeneficiaryPhone = txtBenePhoneNo.Text.ToString
                    End If
                    If txtBeneMobileNo.Text <> "" And txtBeneMobileNo.Text <> "Enter Beneficiary Mobile No." Then
                        objICInstruction.BeneficiaryMobile = txtBeneMobileNo.Text.ToString
                    End If
                    If txtBeneCNCINo.Text <> "" And txtBeneCNCINo.Text <> "Enter CNIC No" Then
                        objICInstruction.BeneficiaryCNIC = txtBeneCNCINo.Text.ToString
                    End If
                    If txtBeneEmail.Text <> "" And txtBeneEmail.Text <> "Enter Email Address" Then
                        objICInstruction.BeneficiaryEmail = txtBeneEmail.Text.ToString
                    End If

                    If ddlBeneficiaryCity.SelectedValue.ToString <> "0" Then
                        objICInstruction.BeneficiaryCity = ddlBeneficiaryCity.SelectedValue.ToString
                        objICBeneficiary.BeneCity = ddlBeneficiaryCity.SelectedItem.Text.ToString
                    End If
                    If ddlBeneficiaryProvince.SelectedValue.ToString <> "0" Then
                        objICInstruction.BeneficiaryProvince = ddlBeneficiaryProvince.SelectedValue.ToString
                        objICBeneficiary.BeneProvince = ddlBeneficiaryProvince.SelectedItem.Text.ToString

                    End If
                    If ddlBeneficiaryCountry.SelectedValue.ToString <> "0" Then
                        objICInstruction.BeneficiaryCountry = ddlBeneficiaryCountry.SelectedValue.ToString

                    End If
                    If txtBeneAddress1.Text <> "" And txtBeneAddress1.Text <> "Enter Address" Then
                        objICInstruction.BeneficiaryAddress = txtBeneAddress1.Text.ToString
                    End If

                    If txtAmountPKR.Text <> "" And txtAmountPKR.Text <> "Amount (PKR) " Then
                        objICInstruction.Amount = CDbl(txtAmountPKR.Text)
                    End If
                    If txtAmountInWords.Text <> "" And txtAmountInWords.Text <> "Amount In Words" Then
                        objICInstruction.AmountInWords = txtAmountInWords.Text.ToString
                    End If
                    If txtDescription.Text <> "" And txtDescription.Text <> "Enter Description" Then
                        objICInstruction.Description = txtDescription.Text.ToString
                    End If
                    If txtRemarks.Text <> "" And txtRemarks.Text <> "Enter Remarks" Then
                        objICInstruction.Remarks = txtRemarks.Text.ToString
                    End If
                    If txtReferenceField1.Text <> "" And txtReferenceField1.Text <> "Enter Reference Remarks" Then
                        objICInstruction.ReferenceField1 = txtReferenceField1.Text.ToString
                    End If
                    If txttrReferenceField2.Text <> "" And txttrReferenceField2.Text <> "Enter Reference Remarks" Then
                        objICInstruction.ReferenceField2 = txttrReferenceField2.Text.ToString
                    End If
                    If txtReferenceField3.Text <> "" And txtReferenceField3.Text <> "Enter Reference Remarks" Then
                        objICInstruction.ReferenceField3 = txtReferenceField3.Text.ToString
                    End If
                    If Not txtValueDate.SelectedDate Is Nothing Then
                        objICInstruction.ValueDate = txtValueDate.SelectedDate
                    End If
                    If txtInstrumentNo.Text <> "" And txtInstrumentNo.Text <> "Enter Reference Remarks" Then
                        If ICMasterSeriesController.CheckIsInstrumentNumberIsUsedByInstrumentNumber(txtInstrumentNo.Text.ToString) = True Then
                            UIUtilities.ShowDialog(Me, "Amend Instruction", "Instrument number is already assigned or used", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                        If objICInstruction.InstrumentNo <> "" Then
                            ICInstrumentController.MarkInstrumentNumberIsCancelledByInstructionIDAndInstrumentNumberAndOfficeCode(objICInstruction.InstrumentNo.ToString, objICInstruction.PrintLocationCode.ToString, objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        End If
                        objICInstruction.InstrumentNo = txtInstrumentNo.Text.ToString
                    End If
                    If Not txtPrintDate.SelectedDate Is Nothing Then
                        objICInstruction.PrintDate = txtPrintDate.SelectedDate
                    End If


                    ''COTC

                    If ddlBenePickUpLocation.SelectedValue <> "0" Then
                        objICInstruction.BeneficiaryBranchCode = ddlBenePickUpLocation.SelectedValue.ToString
                    End If
                    If ddlBeneIDType.SelectedValue <> "0" Then
                        objICInstruction.BeneficiaryIDType = ddlBeneIDType.SelectedValue.ToString
                    End If
                    If txtBeneIDNo.Text <> "" Then
                        objICInstruction.BeneficiaryIDNo = txtBeneIDNo.Text
                    End If


                    objICInstruction.Save()
                    ActionStr += "Instruction with id [ " & objICInstruction.InstructionID.ToString & " ] of client [ " & ddlCompany.SelectedItem.ToString & " ], "
                    ActionStr += "of amount [ " & objICInstruction.Amount.ToString & "] [ " & objICInstruction.AmountInWords.ToString & " ] amended. Client account number was [ " & ddlAccPNature.SelectedValue.ToString.Split("-")(0).ToString & " ],"
                    ActionStr += " branch code [" & ddlAccPNature.SelectedValue.ToString.Split("-")(1).ToString & " ], currency [" & ddlAccPNature.SelectedValue.ToString.Split("-")(2) & " ]."
                    ActionStr += " Product type [ " & objICProductType.ProductTypeName & " ], Disbursement mode [ " & objICProductType.DisbursementMode & " ], Payment nature [ " & ddlAccPNature.SelectedValue.Split("-")(3).ToString & " ]."
                    ICInstructionController.UpdateInstructionStatus(InstructionID, objICInstruction.Status, "9", Me.UserId.ToString, Me.UserInfo.Username.ToString, "Remarks", "Amend", ActionStr)

                    UIUtilities.ShowDialog(Me, "Amend Instruction", "Instruction amended successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())

                    ''For Email Javed
                    ArryListStatus = New ArrayList
                    ArrayListInstructionID = New ArrayList
                    ArryListStatus.Clear()
                    ArryListStatus.Add(9)
                    ArrayListInstructionID.Clear()
                    ArrayListInstructionID.Add(InstructionID)
                    dtInstructionInfo = New DataTable
                    dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArrayListInstructionID, ArryListStatus, False)
                    EmailUtilities.Txnamended(dtInstructionInfo, Me.UserInfo.Username.ToString)
                    SMSUtilities.Txnamended(Me.UserInfo.Username.ToString, dtInstructionInfo, ArryListStatus, ArrayListInstructionID)


                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub
    Protected Sub btnCancelOnlineForm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL())
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub txtAmountPKR_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAmountPKR.TextChanged
        Dim result As Double
        If txtAmountPKR.Text.ToString() <> "" Then
            txtAmountInWords.Text = ""

            If txtAmountPKR.Text.Contains(".") Then
                Dim str As String()
                Dim strBDeci, strADeci As String
                Dim TotAmt As String = ""
                str = txtAmountPKR.Text.Split(".")
                strBDeci = str(0).ToString()
                strADeci = str(1).ToString()
                TotAmt = strBDeci & strADeci
                If TotAmt.Length > 13 Then
                    txtAmountPKR.Text = Nothing
                    UIUtilities.ShowDialog(Me, "Amend Instruction", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If strBDeci.Length > 11 Then
                    txtAmountPKR.Text = Nothing
                    UIUtilities.ShowDialog(Me, "Amend Instruction", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Else
                If txtAmountPKR.Text.Length > 11 Then
                    txtAmountPKR.Text = Nothing
                    UIUtilities.ShowDialog(Me, "Amend Instruction", "Invalid Amount - Amount cannot be greater than 99,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            End If

            Try
                If Double.TryParse(txtAmountPKR.Text.Trim(), result) = False Then
                    txtAmountPKR.Text = Nothing
                    UIUtilities.ShowDialog(Me, "Amend Instruction", "Invalid Amount - Amount cannot be greater than 9,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                txtAmountPKR.Text = Nothing
                UIUtilities.ShowDialog(Me, "Amend Instruction", "Invalid Amount - Amount cannot be greater than 9,999,999,999.99", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End Try


            If CDbl(txtAmountPKR.Text.ToString()) < 1 Then
                UIUtilities.ShowDialog(Me, "Amend Instruction", "Amount should be greater than or equal to 1.", ICBO.IC.Dialogmessagetype.Failure)
                txtAmountPKR.Text = ""
                Exit Sub
            End If
            'Standard Format
            'txtAmountWords.Text = NumToWord.AmtInWord(CDbl(txtAmountPKR.Text.ToString())).TrimStart().TrimEnd().Trim().ToString()
            txtAmountInWords.Text = NumToWord.SpellNumber(CDbl(txtAmountPKR.Text.ToString())).TrimStart().TrimEnd().Trim().ToString()
        End If

    End Sub

    'Protected Sub btnCancelPrview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelPrview.Click
    '    Try
    '        ClearPreview()
    '        pnlOnLineFormPreview.Style.Add("display", "none")
    '        pnlOnlineFrom.Style.Remove("display")
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

    Protected Sub ddlDDPayableLocationProvince_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDDPayableLocationProvince.SelectedIndexChanged
        Try
            LoadddlDDDrawnLocationCity()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetTextBoxesValues(ByVal objICInstruction As ICInstruction)
        Dim objICProvince As New ICProvince
        Dim objICCity As New ICCity
        Dim objICOffice As New ICOffice
        If Not objICInstruction.DDDrawnCityCode Is Nothing Then
            objICCity.LoadByPrimaryKey(objICInstruction.DDDrawnCityCode)
            objICProvince.LoadByPrimaryKey(objICCity.ProvinceCode)
        End If
        If Not objICInstruction.PrintLocationCode Is Nothing Then
            objICOffice.LoadByPrimaryKey(objICInstruction.PrintLocationCode)
        End If
        txtBeneName.Text = objICInstruction.BeneficiaryName
        txtBankAccountNumber.Text = objICInstruction.BeneficiaryAccountNo


        If objICInstruction.BeneficiaryBankCode <> "" And Not objICInstruction.BeneficiaryBankCode Is Nothing Then
            ddlBeneBankName.SelectedValue = objICInstruction.BeneficiaryBankCode
        End If
        If Not objICInstruction.PrintLocationCode Is Nothing Then
            ddlPrintLocation.SelectedValue = objICInstruction.PrintLocationCode
        End If
        If Not objICProvince.ProvinceCode Is Nothing Then
            ddlDDPayableLocationProvince.SelectedValue = objICProvince.ProvinceCode
            LoadddlDDDrawnLocationCity()
        End If
        If Not objICCity.CityCode Is Nothing Then
            ddlDDPayableLocationCity.SelectedValue = objICCity.CityCode.ToString
        End If
        If objICInstruction.BeneficiaryIDType IsNot Nothing Then
            ddlBeneIDType.SelectedValue = objICInstruction.BeneficiaryIDType
        End If
        If objICInstruction.BeneficiaryBranchCode IsNot Nothing Then
            ddlBenePickUpLocation.SelectedValue = objICInstruction.BeneficiaryBranchCode
        End If
        If objICInstruction.BeneficaryAccountType IsNot Nothing And objICInstruction.BeneficaryAccountType <> "" Then
            ddlBeneficiaryAccountType.SelectedValue = objICInstruction.BeneficaryAccountType.ToString
            SetViewFieldsVisibiltyByBeneAccountType(objICInstruction.BeneficaryAccountType.ToString)
            If objICInstruction.BeneficaryAccountType = "GL" Then
                ddlGLAccountBranchCode.SelectedValue = objICInstruction.BeneAccountBranchCode.ToString
            Else
                txtBeneAccountBranchCode.Text = objICInstruction.BeneAccountBranchCode
            End If
        Else
            txtBeneAccountBranchCode.Text = objICInstruction.BeneAccountBranchCode
        End If
        txtBeneAccountCurrency.Text = objICInstruction.BeneAccountCurrency
        txtBenePhoneNo.Text = objICInstruction.BeneficiaryPhone
        txtBeneMobileNo.Text = objICInstruction.BeneficiaryMobile
        txtBeneBranchAddress.Text = objICInstruction.BeneBranchAddress
        'sharedCalendarValueDate.SelectedDate = CDate(objICInstruction.ValueDate)
        'txtValueDate.SelectedDate = sharedCalendarValueDate.SelectedDate
        'txtValueDate.SelectedDate = objICInstruction.ValueDate
        txtBeneEmail.Text = objICInstruction.BeneficiaryEmail
        ddlBeneficiaryCity.SelectedValue = objICInstruction.BeneficiaryCity
        ddlBeneficiaryProvince.SelectedValue = objICInstruction.BeneficiaryProvince
        ddlBeneficiaryCountry.SelectedValue = objICInstruction.BeneficiaryName
        txtAmountPKR.Text = CDbl(objICInstruction.Amount)
        txtBeneCNCINo.Text = objICInstruction.BeneficiaryCNIC
        txtRemarks.Text = objICInstruction.Remarks
        txtReferenceField1.Text = objICInstruction.ReferenceField1
        txttrReferenceField2.Text = objICInstruction.ReferenceField2
        txtReferenceField3.Text = objICInstruction.ReferenceField3
        txtDescription.Text = objICInstruction.Description
        txtAmountInWords.Text = objICInstruction.AmountInWords
        txtAccountTitle.Text = objICInstruction.BeneficiaryAccountTitle
        txtInstrumentNo.Text = objICInstruction.InstrumentNo
        txtBeneIDNo.Text = objICInstruction.BeneficiaryIDNo

    End Sub

    Protected Sub btnCancelSingle_Click(sender As Object, e As System.EventArgs) Handles btnCancelSingle.Click
        'If Page.IsValid Then
        Try
                Response.Redirect(NavigateURL())
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        'End If

    End Sub

    Protected Sub txtBankAccountNumber_TextChanged(sender As Object, e As EventArgs) Handles txtBankAccountNumber.TextChanged
        Try
            Dim StrAccountDetails As String()
            Dim ArrayListInstID As String() = Nothing
            Dim InstID As String
            Dim objICProductType As New ICProductType
            Dim objICInstruction As New ICInstruction
            Dim AccountStatus As String = ""
            Dim RestraintsDetails As String = ""
            Dim BBAccNo As String = Nothing
            Dim PrincipalBankIMD As String = ""
            Dim objICPrincipalBank As ICBank
            Dim objICNonPRincipalBank As ICBank
            Dim NonPrincipalBankIMD As String = ""
            Dim CustomerID As String = Nothing
            Dim IBFTRRNo As String = Nothing
            If Not txtBankAccountNumber.Text.ToString = "" And txtBankAccountNumber.Text IsNot Nothing Then
                objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)
                If objICProductType.DisbursementMode = "Direct Credit" Then
                    If CBUtilities.IsNormalAccountNoOrIBAN(txtBankAccountNumber.Text.ToString) = True Then
                        If CBUtilities.ValidateIBAN(txtBankAccountNumber.Text.ToString) = True Then
                            BBAccNo = CBUtilities.GetBBANFromIBAN(txtBankAccountNumber.Text.ToString)
                            Try
                                StrAccountDetails = (CBUtilities.TitleFetch(BBAccNo, ICBO.CBUtilities.AccountType.RB, "Beneficiary Account Title", txtBankAccountNumber.Text.ToString).ToString.Split(";"))

                                txtAccountTitle.Text = StrAccountDetails(0).ToString
                                txtBeneAccountCurrency.Text = StrAccountDetails(2).ToString
                                txtBeneAccountBranchCode.Text = StrAccountDetails(1).ToString

                            Catch ex As Exception
                                txtAccountTitle.Text = ""
                                txtBeneAccountCurrency.Text = ""
                                txtBeneAccountBranchCode.Text = ""
                                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End Try
                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Invalid IBAN", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    Else
                        Try
                            StrAccountDetails = (CBUtilities.TitleFetch(txtBankAccountNumber.Text.ToString, ICBO.CBUtilities.AccountType.RB, "Beneficiary Account Title", txtBankAccountNumber.Text.ToString).ToString.Split(";"))
                            txtAccountTitle.Text = StrAccountDetails(0).ToString
                            txtBeneAccountCurrency.Text = StrAccountDetails(2).ToString
                            txtBeneAccountBranchCode.Text = StrAccountDetails(1).ToString
                        Catch ex As Exception
                            txtAccountTitle.Text = ""
                            txtBeneAccountCurrency.Text = ""
                            txtBeneAccountBranchCode.Text = ""
                            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End Try
                    End If
                ElseIf objICProductType.DisbursementMode = "Other Credit" Then
                    Try
                        CustomerID = ICUtilities.GetCustomerIDCountFromICSettings
                        If ddlBeneBankName.SelectedValue <> "0" Then
                            If ICBankController.GetBankByBankCode(ddlBeneBankName.SelectedValue.ToString) IsNot Nothing Then
                                objICNonPRincipalBank = New ICBank
                                objICNonPRincipalBank = ICBankController.GetBankByBankCode(ddlBeneBankName.SelectedValue.ToString)
                                NonPrincipalBankIMD = objICNonPRincipalBank.BankIMD
                            End If
                        End If

                        If ICBankController.GetPrincipleBank() IsNot Nothing Then
                            objICPrincipalBank = New ICBank
                            objICPrincipalBank = ICBankController.GetPrincipleBank()
                            PrincipalBankIMD = objICPrincipalBank.BankIMD.ToString
                        End If
                        If NonPrincipalBankIMD = "" Then
                            UIUtilities.ShowDialog(Me, "Error", "Invalid Non-Principal Bank IMD", Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                        If PrincipalBankIMD = "" Then
                            UIUtilities.ShowDialog(Me, "Error", "Invalid Principal Bank IMD", Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                        InstID = Request.QueryString("InstructionID").ToString().DecryptString2()
                        If InstID.ToString.Contains(";") = True Then
                            ArrayListInstID = InstID.ToString.Split(";")
                        Else
                            ArrayListInstID(0) = InstID
                        End If
                        objICInstruction.LoadByPrimaryKey(ArrayListInstID(0).ToString)
                        txtAccountTitle.Text = CBUtilities.IBFTTitleFetch(NonPrincipalBankIMD, txtBankAccountNumber.Text, CBUtilities.AccountType.RB, "Beneficiary Account Title", txtBankAccountNumber.Text.ToString, "", "", 0, "", IBFTRRNo, CustomerID, objICNonPRincipalBank.BankID, objICNonPRincipalBank.BankName, objICInstruction.ClientAccountBranchCode & objICInstruction.ClientAccountNo, PrincipalBankIMD)
                    Catch ex As Exception
                        txtAccountTitle.Text = ""
                        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End Try
                End If

            Else
                'UIUtilities.ShowDialog(Me, "Error", "Please enter valid account number.", ICBO.IC.Dialogmessagetype.Failure)
                txtAccountTitle.Text = ""
                txtBeneAccountCurrency.Text = ""
                txtBeneAccountBranchCode.Text = ""
                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub ddlBeneBankName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBeneBankName.SelectedIndexChanged

        Try
            Dim objICProductType As New ICProductType
            Dim objICPrincipalBank As ICBank
            Dim objICNonPRincipalBank As ICBank
            Dim objICInstruction As New ICInstruction
            Dim ArrayListInstID As String() = Nothing
            Dim InstID As String
            Dim CustomerID As String = Nothing
            Dim NonPrincipalBankIMD As String = Nothing
            Dim PrincipalBankIMD As String = Nothing
            Dim IBFTRRNo As String = Nothing
            objICProductType.LoadByPrimaryKey(ddlProductType.SelectedValue.ToString)
            If objICProductType.DisbursementMode = "Other Credit" Then
                If ddlBeneBankName.SelectedValue.ToString <> "0" Then
                    If txtAccountTitle.Text = "" Or txtAccountTitle.Text Is Nothing Then
                        CustomerID = ICUtilities.GetCustomerIDCountFromICSettings
                        If ddlBeneBankName.SelectedValue <> "0" Then
                            If ICBankController.GetBankByBankCode(ddlBeneBankName.SelectedValue.ToString) IsNot Nothing Then
                                objICNonPRincipalBank = New ICBank
                                objICNonPRincipalBank = ICBankController.GetBankByBankCode(ddlBeneBankName.SelectedValue.ToString)
                                NonPrincipalBankIMD = objICNonPRincipalBank.BankIMD
                            End If

                        End If

                        If ICBankController.GetPrincipleBank() IsNot Nothing Then
                            objICPrincipalBank = New ICBank
                            objICPrincipalBank = ICBankController.GetPrincipleBank()
                            PrincipalBankIMD = objICPrincipalBank.BankIMD
                        End If
                        If NonPrincipalBankIMD = "" Then
                            UIUtilities.ShowDialog(Me, "Error", "Invalid Non-Principal Bank IMD", Dialogmessagetype.Failure)
                            txtAccountTitle.Text = ""
                            Exit Sub
                        End If
                        If PrincipalBankIMD = "" Then
                            txtAccountTitle.Text = ""
                            UIUtilities.ShowDialog(Me, "Error", "Invalid Principal Bank IMD", Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                        InstID = Request.QueryString("InstructionID").ToString().DecryptString2()
                        If InstID.ToString.Contains(";") = True Then
                            ArrayListInstID = InstID.ToString.Split(";")
                        Else
                            ArrayListInstID(0) = InstID
                        End If
                        objICInstruction.LoadByPrimaryKey(ArrayListInstID(0).ToString)


                        txtAccountTitle.Text = CBUtilities.IBFTTitleFetch(NonPrincipalBankIMD, txtBankAccountNumber.Text, CBUtilities.AccountType.RB, "Beneficiary Account Title", txtBankAccountNumber.Text.ToString, "", "", 0, "", IBFTRRNo, CustomerID, objICNonPRincipalBank.BankID, objICNonPRincipalBank.BankName, objICInstruction.ClientAccountBranchCode & objICInstruction.ClientAccountNo, PrincipalBankIMD)
                    End If
                Else
                    txtAccountTitle.Text = ""
                End If
            End If

        Catch ex As Exception
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Exit Sub
        End Try
    End Sub

    Protected Sub ddlDDPayableLocationCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDDPayableLocationCity.SelectedIndexChanged
        Try
            Dim dtBankBranches As New DataTable
            Dim objICInstruction As New ICInstruction
            If ddlDDPayableLocationCity.SelectedValue.ToString <> "0" Then

                dtBankBranches = ICOfficeController.GetAllPrinciaplBankBranchesByCityCode(ddlDDPayableLocationCity.SelectedValue.ToString)
                If dtBankBranches.Rows.Count > 0 Then
                    UIUtilities.ShowDialog(Me, "Error", "Principal Bank branch exists in specified city. DD cannot be made", ICBO.IC.Dialogmessagetype.Failure)
                    LoadddlDDDrawnLocationCity()
                    If IsBulkAmend = False Then
                        objICInstruction.LoadByPrimaryKey(InstructionID)
                        ddlDDPayableLocationCity.SelectedValue = objICInstruction.DDDrawnCityCode
                    End If
                    Exit Sub
                End If
            Else
                LoadddlDDDrawnLocationCity()
                If IsBulkAmend = False Then
                    objICInstruction.LoadByPrimaryKey(InstructionID)
                    ddlDDPayableLocationCity.SelectedValue = objICInstruction.DDDrawnCityCode
                End If
                Exit Sub
            End If
        Catch ex As Exception
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Exit Sub
        End Try
    End Sub

    Protected Sub ddlBeneficiaryAccountType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBeneficiaryAccountType.SelectedIndexChanged
        Try
            SetViewFieldsVisibiltyByBeneAccountType(ddlBeneficiaryAccountType.SelectedValue.ToString)

        Catch ex As Exception
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Exit Sub
        End Try
    End Sub
End Class
