﻿Imports ICBO
Imports ICBO.IC

Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports System.Drawing
Partial Class DesktopModules_AmendmentQueue_AmendmentQueue
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrICAssignUserRolsID As New ArrayList
    Private ArrICAssignedOfficeID As New ArrayList
    Private ArrICAssignedStatus As New ArrayList
    Private ArrICAssignedPaymentModes As New ArrayList
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()
            If Page.IsPostBack = False Then
                btnBulkAmend.Attributes.Item("onclick") = "if (Page_ClientValidate('BulkAmend')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnBulkAmend, Nothing).ToString() & " }"
                btnComplete.Attributes.Item("onclick") = "if (Page_ClientValidate('Remarks')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnComplete, Nothing).ToString() & " }"
                DesignDts()
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                'SetArraListAssignedOfficeIDs()
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
                rbtnlstDateSelection.SelectedValue = "Creation Date"
                RefreshPage()

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub RefreshPage()

        LoadddlCompany()
        LoadddlAccountNumbers()
        LoadddlAccountPaymentNatureByUserID()
        FillDesignedDtByAPNatureByAssignedRole()
        Dim dtPageLoad As New DataTable
        dtPageLoad = DirectCast(ViewState("AccountNumber"), DataTable)
        If dtPageLoad.Rows.Count > 0 Then
            LoadddlBatcNumber()
            LoadddlProductTypeByAccountPaymentNature()
            ClearAllTextBoxes()

            LoadgvAccountPaymentNatureProductType(True)
        Else
            UIUtilities.ShowDialog(Me, "Error", "You do not have access to any transactional data. Please contact Al Baraka Administrator.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
            Exit Sub
        End If
    End Sub
    Private Sub DesignDts()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim dtOfficeID As New DataTable
        dtAccountNumber.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("BranchCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("Currency", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("PaymentNatureCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("OfficeID", GetType(System.String)))
        ViewState("AccountNumber") = dtAccountNumber
    End Sub
    Private Sub FillDesignedDtByAPNatureByAssignedRole()
      Dim dtAssignedAPNatureOfficeID As New DataTable
        dtAssignedAPNatureOfficeID.Rows.Clear()
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserSecond(Me.UserId.ToString, ArrICAssignUserRolsID)
        dtAssignedAPNatureOfficeID.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAssignedAPNatureOfficeID.Rows
            dr("DropDown") = "False"
        Next
        ViewState("AccountNumber") = Nothing
        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Amendment Queue")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Amendment Queue") = True Then
                    If Not ArrICAssignUserRolsID.Contains(RoleID.ToString) Then
                        ArrICAssignUserRolsID.Add(RoleID.ToString)
                    End If
                End If
            Next
        End If
    End Sub
    Private Sub SetArraListAssignedOfficeIDs()
        Dim dt As New DataTable
        Dim objICUser As New ICUser
        objICUser.LoadByPrimaryKey(Me.UserId)
        ArrICAssignedOfficeID.Clear()
        dt = ICUserRolesPaymentNatureController.GetAllTaggedLocationsWithUserByUSerIDAndRoleID(Me.UserId.ToString, ArrICAssignUserRolsID)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("OfficeID").ToString
                ArrICAssignedOfficeID.Add(RoleID.ToString)
            Next
            ArrICAssignedOfficeID.Add(objICUser.UpToICOfficeByOfficeCode.OfficeID)
        End If
    End Sub
    Private Sub SetArraListAssignedStatus()
        Dim dt As New DataTable
        ArrICAssignedStatus.Clear()
        ArrICAssignedStatus.Add(6)
        ArrICAssignedStatus.Add(9)
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, "", ArrICAssignUserRolsID)
            ddlCompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlCompany.Items.Clear()
                    ddlCompany.Items.Add(lit)
                    lit.Selected = True
                    ddlCompany.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
                ddlCompany.DataSource = dt
                ddlCompany.DataTextField = "CompanyName"
                ddlCompany.DataValueField = "CompanyCode"
                ddlCompany.DataBind()
                ddlCompany.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccountPaymentNatureByUserID()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlAcccountPaymentNature.Items.Clear()
            ddlAcccountPaymentNature.Items.Add(lit)
            lit.Selected = True
            ddlAcccountPaymentNature.AppendDataBoundItems = True
            ddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForDropDown(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
            ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
            ddlAcccountPaymentNature.DataBind()
            'Dim lit As New ListItem
            'lit.Value = "0"
            'lit.Text = "-- All --"
            'ddlAcccountPaymentNature.Items.Clear()
            'ddlAcccountPaymentNature.Items.Add(lit)
            'lit.Selected = True
            'ddlAcccountPaymentNature.AppendDataBoundItems = True
            'ddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUser(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
            'ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
            'ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
            'ddlAcccountPaymentNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlAccountNumbers()
        Try
            Dim lit As New ListItem
            lit.Text = "-- Please Select --"
            lit.Value = "0"
            ddlAccount.Items.Clear()
            ddlAccount.Items.Add(lit)
            lit.Selected = True
            ddlAccount.AppendDataBoundItems = True
            ddlAccount.DataSource = ICInstructionController.GetAccountNumbersTaggedWithUserSecond(Me.UserId.ToString, ArrICAssignUserRolsID, ddlCompany.SelectedValue.ToString)
            ddlAccount.DataTextField = "AccountNumber"
            ddlAccount.DataValueField = "AccountNumber"
            ddlAccount.DataBind()
            'Dim lit As New ListItem
            'lit.Text = "-- Please Select --"
            'lit.Value = "0"
            'ddlAccount.Items.Clear()
            'ddlAccount.Items.Add(lit)
            'lit.Selected = True
            'ddlAccount.AppendDataBoundItems = True
            'ddlAccount.DataSource = ICInstructionController.GetAccountNumbersTaggedWithUser(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
            'ddlAccount.DataTextField = "AccountNumber"
            'ddlAccount.DataValueField = "AccountNumber"
            'ddlAccount.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlBatcNumber()
        Try
            
            Dim dtAccountNumber As New DataTable


            dtAccountNumber = ViewState("AccountNumber")

            SetArraListAssignedStatus()
            Dim lit As New ListItem
            lit.Text = "-- All --"
            lit.Value = "0"
            ddlBatch.Items.Clear()
            ddlBatch.Items.Add(lit)
            lit.Selected = True
            ddlBatch.AppendDataBoundItems = True
            ddlBatch.DataSource = ICInstructionController.GetAllTaggedBatchNumbersByCompanyCodeWithStatus(ArrICAssignedPaymentModes, dtAccountNumber, ddlCompany.SelectedValue.ToString, ArrICAssignedStatus, Me.UserId.ToString)
            ddlBatch.DataTextField = "FileBatchNoName"
            ddlBatch.DataValueField = "FileBatchNo"
            ddlBatch.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlProductTypeByAccountPaymentNature()
        Try
            Dim lit As New ListItem
            Dim objICAPNature As New ICAccountsPaymentNature
            objICAPNature.es.Connection.CommandTimeout = 3600
            ddlProductType.Items.Clear()
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlProductType.Items.Add(lit)
            ddlProductType.AppendDataBoundItems = True
            If ddlAcccountPaymentNature.SelectedValue.ToString <> "0" Then
                If objICAPNature.LoadByPrimaryKey(ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)) Then
                    ddlProductType.DataSource = ICAccountPaymentNatureProductTypeController.GetAllProductTypesByAccountAndPaymentNature(objICAPNature)
                    ddlProductType.DataTextField = "ProductTypeName"
                    ddlProductType.DataValueField = "ProductTypeCode"
                    ddlProductType.DataBind()
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadgvAccountPaymentNatureProductType(ByVal DoDataBind As Boolean)
        Try
            Dim objICUser As New ICUser
            objICUser.LoadByPrimaryKey(Me.UserId.ToString)
            Dim DateType, AccountNumber, BranchCode, Currency, PaymentNatureCode, CompanyCode, ProductTypeCode, InstrumentNo, InstructionNo, BatchCode, ReferenceNo As String
            Dim FromDate, ToDate As String
            DateType = ""
            AccountNumber = ""
            BranchCode = ""
            Currency = ""
            Currency = ""
            PaymentNatureCode = ""
            CompanyCode = "0"
            ProductTypeCode = ""
            InstructionNo = ""
            InstrumentNo = ""
            BatchCode = ""
            ReferenceNo = ""
            FromDate = Nothing
            ToDate = Nothing
            Dim dtAccountNumber As New DataTable
            dtAccountNumber = ViewState("AccountNumber")
            If rbtnlstDateSelection.SelectedValue = "Creation Date" Then
                DateType = "Creation Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Value Date" Then
                DateType = "Value Date"
            End If
            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            End If
            If ddlProductType.SelectedValue.ToString <> "0" Then
                ProductTypeCode = ddlProductType.SelectedValue.ToString
            End If
            If txtInstrumentNo.Text.ToString <> "" And txtInstrumentNo.Text.ToString <> "Enter Instrument No." Then
                InstrumentNo = txtInstrumentNo.Text.ToString
            End If
            If txtInstructionNo.Text.ToString <> "" And txtInstructionNo.Text.ToString <> "Enter Instruction No." Then
                InstructionNo = txtInstructionNo.Text.ToString
            End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                FromDate = raddpCreationFromDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If Not raddpCreationToDate.SelectedDate Is Nothing Then
                ToDate = raddpCreationToDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If ddlBatch.SelectedValue <> "0" Then
                BatchCode = ddlBatch.SelectedValue.ToString
            End If
            SetArraListAssignedStatus()
            SetArraListAssignedOfficeIDs()
            ICInstructionController.GetAllInstructionsForRadGridSecond(DateType, FromDate, ToDate, dtAccountNumber, BatchCode, ProductTypeCode, InstructionNo, InstrumentNo, ReferenceNo, Me.gvAmendment.CurrentPageIndex + 1, Me.gvAmendment.PageSize, Me.gvAmendment, DoDataBind, ArrICAssignedStatus, Me.UserId.ToString, 0, "", ArrICAssignedPaymentModes, CompanyCode)
            ClearSummaryAndAccountBalance()
            If gvAmendment.Items.Count > 0 Then
                gvAmendment.Visible = True
                lblNRF.Visible = False
                gvAmendment.Columns(12).Visible = CBool(htRights("Single Amend"))

                btnBulkAmend.Visible = CBool(htRights("Bulk Amend"))
                btnComplete.Visible = CBool(htRights("Complete"))

                'SetJavaScript()
            Else
                lblNRF.Visible = True
                gvAmendment.Visible = False
                btnBulkAmend.Visible = False
                btnComplete.Visible = False
                btnSummary.Visible = False
            End If



        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ClearSummaryAndAccountBalance()
        txtTnxCount.Text = ""
        txtTotalAmount.Text = ""
        txtAvailableBalance.Text = ""
        LoadddlAccountNumbers()
    End Sub
    Private Function CheckgvClientRoleCheckedForProcessAll() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvAmendment.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnBulkAmend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBulkAmend.Click
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim objICInstruction As ICInstruction
                Dim StrInstructionID As String = Nothing
                Dim StrArr As String() = Nothing
                If Page.IsValid = True Then
                    If CheckgvClientRoleCheckedForProcessAll() = False Then
                        UIUtilities.ShowDialog(Me, "Instruction Verification", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    For Each gvVerificationRow As GridDataItem In gvAmendment.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            objICInstruction = New ICInstruction
                            objICInstruction.es.Connection.CommandTimeout = 3600
                            If objICInstruction.LoadByPrimaryKey(gvVerificationRow.GetDataKeyValue("InstructionID")) Then
                                If objICInstruction.Status = "6" Then
                                    StrInstructionID = StrInstructionID & objICInstruction.InstructionID.ToString & ";"
                                End If
                            End If
                        End If
                    Next
                    If Not StrInstructionID Is Nothing Then
                        If StrInstructionID.ToString <> "" Then
                            StrInstructionID = StrInstructionID.Remove(StrInstructionID.Length - 1, 1)
                            Response.Redirect(NavigateURL("AmendInstruction", "&mid=" & Me.ModuleId & "&InstructionID=" & StrInstructionID & "&CompanyCode=" & ddlCompany.SelectedValue & "&ProductTypeCode=" & ddlProductType.SelectedValue & "&IsBulkAmend=True"), False)
                        Else
                            StrInstructionID = Nothing
                            LoadgvAccountPaymentNatureProductType(True)
                            UIUtilities.ShowDialog(Me, "Instruction Amendment", "Selected instruction's status was updated", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    Else
                        StrInstructionID = Nothing
                        LoadgvAccountPaymentNatureProductType(True)
                        UIUtilities.ShowDialog(Me, "Instruction Amendment", "Selected instruction status is updated.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If


    End Sub

    Private Sub ClearAllTextBoxes()
        txtRemarks.Text = ""
        txtAvailableBalance.Text = ""
        txtInstructionNo.Text = ""
        txtInstrumentNo.Text = ""
        txtReferenceNo.Text = ""
        txtAvailableBalance.Text = ""
      
        txtTnxCount.Text = ""
        txtTotalAmount.Text = ""
    End Sub
    Private Function CheckgvInstructionAuthentication() As Boolean
        Try

            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVInstruction In gvAmendment.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVInstruction.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSummary.Click

        Try

                Dim InstructionCount As Double = 0
                Dim TotalAmount As Double = 0
                Dim TotalAmountInInstruction As Double

                Dim chkSelect As CheckBox

                If CheckgvInstructionAuthentication() = False Then
                    UIUtilities.ShowDialog(Me, "Instruction Amendment", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                For Each gvInsctructionAuthenticationRow As GridDataItem In gvAmendment.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvInsctructionAuthenticationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        TotalAmountInInstruction = 0

                        InstructionCount = InstructionCount + 1
                        TotalAmountInInstruction = CDbl(gvInsctructionAuthenticationRow.Item("Amount").Text)
                        TotalAmount = TotalAmount + TotalAmountInInstruction



                    End If
                Next

                If Not TotalAmount = 0 Then
                    txtTotalAmount.Text = CDbl(TotalAmount).ToString("N2")
                End If
                If Not InstructionCount = 0 Then
                    txtTnxCount.Text = CInt(InstructionCount)
                End If

                InstructionCount = 0
                TotalAmount = 0

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            'SetArraListRoleID()

            LoadddlAccountNumbers()
            LoadddlAccountPaymentNatureByUserID()
            LoadddlBatcNumber()
            LoadddlProductTypeByAccountPaymentNature()

            LoadgvAccountPaymentNatureProductType(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlAcccountPaymentNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcccountPaymentNature.SelectedIndexChanged
        Try
            If ddlAcccountPaymentNature.SelectedValue.ToString = "0" Then
                SetArraListRoleID()

                LoadddlAccountNumbers()
                FillDesignedDtByAPNatureByAssignedRole()
                LoadddlBatcNumber()
                LoadddlProductTypeByAccountPaymentNature()

                
                LoadgvAccountPaymentNatureProductType(True)
            Else
FillDesignedDtByAPNatureByAssignedRole()
                LoadddlAccountNumbers()
                SetViewStateDtOnAPNatureIndexChnage()
                LoadddlBatcNumber()
                LoadddlProductTypeByAccountPaymentNature()

                
                LoadgvAccountPaymentNatureProductType(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetViewStateDtOnAPNatureIndexChnage()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim AccountNumber, BranchCode, Currency, PaymentNatureCode As String
        Dim ArrayListOfficeID As New ArrayList
        Dim drAccountNo As DataRow
        AccountNumber = Nothing
        BranchCode = Nothing
        Currency = Nothing
        PaymentNatureCode = Nothing
        dtAccountNumber = ViewState("AccountNumber")
        ViewState("AccountNumber") = Nothing
        AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0)
        BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1)
        Currency = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2)
        PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)
        For Each dr As DataRow In dtAccountNumber.Rows
            If dr("AccountNumber").ToString.Split("-")(0) = AccountNumber And dr("AccountNumber").ToString.Split("-")(1) = BranchCode And dr("AccountNumber").ToString.Split("-")(2) = Currency And dr("PaymentNature").ToString = PaymentNatureCode Then
                ArrayListOfficeID.Add(dr("OfficeID").ToString)
            End If
        Next
        dtAccountNumber.Rows.Clear()
        If ArrayListOfficeID.Count > 0 Then
            For Each OfficeID In ArrayListOfficeID
                drAccountNo = dtAccountNumber.NewRow()
                drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
                drAccountNo("PaymentNature") = PaymentNatureCode
                drAccountNo("OfficeID") = OfficeID
                dtAccountNumber.Rows.Add(drAccountNo)
            Next
        Else
            drAccountNo = dtAccountNumber.NewRow()
            drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
            drAccountNo("PaymentNature") = PaymentNatureCode
            drAccountNo("OfficeID") = "0"
            dtAccountNumber.Rows.Add(drAccountNo)
        End If
        'dtAccountNumber.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAccountNumber.Rows
            dr("DropDown") = "True"
        Next
        ViewState("AccountNumber") = dtAccountNumber



    End Sub
    Protected Sub ddlBatch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBatch.SelectedIndexChanged
        Try
            SetArraListRoleID()
            LoadddlAccountNumbers()
            LoadddlProductTypeByAccountPaymentNature()

            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged
        Try
            SetArraListRoleID()



            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    

    

    Protected Sub gvAmendment_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAmendment.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAmendment.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAmendment_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAmendment.ItemDataBound
        Dim chkProcessAll As CheckBox
        Dim chkEdit As HyperLink
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAmendment.MasterTableView.ClientID & "','0');"
        End If
       

        Dim imgBtn As LinkButton
        Dim AppPath As String = "" ' DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            chkEdit = New HyperLink
           
            chkEdit = DirectCast(e.Item.Cells(12).FindControl("hlEdit"), HyperLink)
            imgBtn = New LinkButton
            imgBtn = DirectCast(item.Cells(1).FindControl("lbInstructionID"), LinkButton)
            imgBtn.OnClientClick = "showurldialog('Instruction Details','/ShowInstructionDetails.aspx?InstructionID=" & item.GetDataKeyValue("InstructionID").ToString().EncryptString() & "' ,true,700,650);return false;"
            If CBool(htRights("View Instruction Details")) = True Then
                imgBtn.Enabled = True
            End If
            If item.GetDataKeyValue("Status") = 9 Then
                chkEdit.Enabled = False
                gvAmendment.AlternatingItemStyle.CssClass = "GridItemComplete"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItemComplete"
            Else
                gvAmendment.AlternatingItemStyle.CssClass = "GridItem"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItem"
                 


            End If
        End If

    End Sub
    'Private Sub ChangeRowColor()
    '    For Each gvAmendmentRow As GridDataItem In gvAmendment.Items
    '        If gvAmendmentRow.GetDataKeyValue("Status").ToString = "9" Then
    '            gvAmendmentRow.BackColor = Color.Red
    '        End If
    '    Next
    'End Sub
    Protected Sub gvAmendment_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAmendment.NeedDataSource
        Try
            ClearAllTextBoxes()
            SetArraListRoleID()
            LoadgvAccountPaymentNatureProductType(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnComplete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnComplete.Click
        If Page.IsValid Then


            Try
                Dim chkSelect As CheckBox
                Dim objICInstruction As ICInstruction
                Dim StrInstructionID As String = Nothing
                Dim PassToCompleteCount As Integer = 0
                Dim FailToCompleteCount As Integer = 0
                Dim StrArr As String() = Nothing
                Dim dr As DataRow
                Dim dt As New DataTable
                Dim StrAction As String = ""
                dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Message", GetType(System.String)))

                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Instruction Verification", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each gvVerificationRow As GridDataItem In gvAmendment.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        objICInstruction = New ICInstruction
                        objICInstruction.es.Connection.CommandTimeout = 3600
                        If objICInstruction.LoadByPrimaryKey(gvVerificationRow.GetDataKeyValue("InstructionID")) Then
                            dr = dt.NewRow
                            If gvVerificationRow.GetDataKeyValue("Status").ToString = "9" Then
                                Try
                                    StrAction = ""
                                    StrAction += "Instruction with ID [ " & objICInstruction.InstructionID & " ] marked as amendment complete action was taken by user [ " & Me.UserId & " ] [ " & Me.UserInfo.Username & " ]"
                                    objICInstruction.IsAmendmentComplete = True
                                    ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.ToString, StrAction, "UPDATE")
                                    ICInstructionController.UpdateInstructionStatus(gvVerificationRow.GetDataKeyValue("InstructionID").ToString, gvVerificationRow.GetDataKeyValue("Status").ToString, "1", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", txtRemarks.Text)
                                    dr("InstructionID") = objICInstruction.InstructionID.ToString
                                    dr("Message") = "Instruction(s) amendment completed successfully"
                                    dr("Status") = "1"
                                    dt.Rows.Add(dr)
                                    PassToCompleteCount = PassToCompleteCount + 1
                                Catch ex As Exception
                                    objICInstruction.SkipReason = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped,  on amendment queue fails to complete due to " & ex.Message.ToString & ". Action was taken by [ " & Me.UserId & " ] [ " & Me.UserInfo.Username & " ]"
                                    ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                    'ICUtilities.AddAuditTrail("Instruction Skip: Instruction with ID [ " & gvVerificationRow.GetDataKeyValue("InstructionID").ToString & " ] on amendment queue fails to complete due to " & ex.Message.ToString, "Instruction", gvVerificationRow.GetDataKeyValue("InstructionID").ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                    dr("InstructionID") = objICInstruction.InstructionID.ToString
                                    dr("Message") = "Instruction(s) skipped"
                                    dr("Status") = objICInstruction.Status.ToString
                                    dt.Rows.Add(dr)
                                    FailToCompleteCount = FailToCompleteCount + 1
                                    Continue For
                                End Try
                            Else
                                objICInstruction.LoadByPrimaryKey(gvVerificationRow.GetDataKeyValue("InstructionID").ToString)
                                objICInstruction.SkipReason = "Instruction Skip: Instruction with ID [ " & gvVerificationRow.GetDataKeyValue("InstructionID").ToString & " ] skipped,  on amendment queue fails to complete due to its status is [ " & objICInstruction.UpToICInstructionStatusByStatus.StatusName & " ]. Action was taken by [ " & Me.UserId & " ] [ " & Me.UserInfo.Username & " ]"
                                ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                'ICUtilities.AddAuditTrail("Instruction Skip: Instruction with ID [ " & gvVerificationRow.GetDataKeyValue("InstructionID").ToString & " ] on amendment queue fails to complete due to its status is [ " & objICInstruction.UpToICInstructionStatusByStatus.StatusName & " ]", "Instruction", gvVerificationRow.GetDataKeyValue("InstructionID").ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                dr("InstructionID") = objICInstruction.InstructionID.ToString
                                'dr("Message") = "Instruction Skip: Instruction's amendment not completed successfully due to invalid current status."
                                dr("Message") = "Instruction(s) skipped"
                                dr("Status") = objICInstruction.Status.ToString
                                dt.Rows.Add(dr)
                                FailToCompleteCount = FailToCompleteCount + 1
                                Continue For
                            End If
                        End If
                    End If
                Next
                RefreshPage()
                If dt.Rows.Count > 0 Then
                    gvShowSummary.DataSource = Nothing
                    gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                    gvShowSummary.DataBind()
                    pnlShowSummary.Style.Remove("display")
                    Dim scr As String
                    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                Else
                    gvShowSummary.DataSource = Nothing
                    pnlShowSummary.Style.Add("display", "none")
                End If


            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub


    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click

        Try
                LoadgvAccountPaymentNatureProductType(True)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub

    Protected Sub ddlAccount_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlAccount.SelectedIndexChanged
        Try

            Try
                If ddlAccount.SelectedValue.ToString <> "0" Then
                    txtAvailableBalance.Text = ""
                    txtAvailableBalance.Text = CDbl(CBUtilities.BalanceInquiry(ddlAccount.SelectedValue.Split("-")(0).ToString, ICBO.CBUtilities.AccountType.RB, "Client Account Balance", ddlAccount.SelectedValue.Split("-")(0).ToString, ddlAccount.SelectedValue.Split("-")(1).ToString)).ToString("N2")
                Else
                    txtAvailableBalance.Text = ""
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub raddpCreationToDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationToDate.SelectedDateChanged
        Try

            'If raddpCreationFromDate.SelectedDate Is Nothing Then
            '    UIUtilities.ShowDialog(Me, "Error", "Please select creation from date.", ICBO.IC.Dialogmessagetype.Failure)
            '    Exit Sub
            'End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub raddpCreationFromDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationFromDate.SelectedDateChanged
        Try

            'If raddpCreationFromDate.SelectedDate Is Nothing Then
            '    UIUtilities.ShowDialog(Me, "Error", "Please select creation from date.", ICBO.IC.Dialogmessagetype.Failure)
            '    Exit Sub
            'End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub rbtnlstDateSelection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnlstDateSelection.SelectedIndexChanged
        Try
            If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now.AddYears(2)
            Else
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
            End If

            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class

