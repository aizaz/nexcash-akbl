﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AmendInstruction.ascx.vb" Inherits="DesktopModules_BankRoleAccountPaymentNature_BankRoleAccountPaymentNatureTagging" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:RegExpTextBoxSetting BehaviorID="REUserName" Validation-IsRequired="true"
        ValidationExpression="\b(\w*)\b(.)*\b(-)*\b(_)*\b" ErrorMessage="Invalid Name"
        EmptyMessage="Enter User Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtUserName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior7" Validation-IsRequired="true"
        ValidationExpression="\b(\w*)\b(.)*\b(-)*\b(_)*\b" ErrorMessage="Invalid Display  Name"
        EmptyMessage="Enter Display Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDisplayName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="false"
        EmptyMessage="Enter Role Type">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRoleType" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>--%><%--<script type="text/javascript">
                                  function textboxMultilineMaxNumber(txt, maxLen) {
                                      try {
                                          if (txt.value.length > (maxLen - 1)) return false;
                                      } catch (e) {
                                      }
                                  }
</script>--%>
<table style="width: 100%">
    <tr>
        <td style="width: 100%" colspan="4">
            <asp:Label ID="lblPageHeader" runat="server" Text="Single Amend" 
                CssClass="headingblue"></asp:Label>
            <br />
            <%--   Note:&nbsp; Fields marked as
            <asp:Label ID="lblReqHeader" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.--%>
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
                                                    <asp:Label ID="lblInstIDForAmend" runat="server" 
                                                        CssClass="lbl" Visible="False"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
    <td align="left" valign="top" colspan="4">
    <table id="tblHeader" runat="server" style="width:100% ; display:none">
     <tr>
        <td style="width: 25%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:Label ID="lblSelectAccountPNature" runat="server" Text="Select Account Payment Nature"
                CssClass="lbl"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            <asp:DropDownList ID="ddlAccPNature" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            &nbsp;</td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;</td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:Label ID="lblProductType" runat="server" Text="Select Product Type" CssClass="lbl"></asp:Label>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;</td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            <asp:DropDownList ID="ddlProductType" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;</td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            &nbsp;</td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;</td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    
    </table>
    
    
    </td>
    
    </tr>
   
    <tr>
        <td align="left" colspan="4" valign="top">
            <asp:Panel ID="pnlOnlineFrom" runat="server" Width="100%">
                <table style="width: 100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="left" valign="top" style="width: 40%">
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr id="trBeneName" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneName" runat="server" Text="Beneficiary Name" CssClass="lbl"></asp:Label>
                                                    <asp:Label ID="lblReqBeneName" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBeneName" runat="server" CssClass="txtbox" MaxLength="150" 
                                                        Enabled="False"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBeneName" runat="server">
                                                        <telerik:TextBoxSetting BehaviorID="reBeneName" EmptyMessage=""
                                                             ErrorMessage="Invalid Beneficiary Name"
                                                            Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBeneName" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                 <tr id="trBeneficiaryAccountType" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneficiaryAccountType" runat="server" Text="Beneficiary Account Type"  CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:DropDownList ID="ddlBeneficiaryAccountType" runat="server" CssClass="dropdown"  AutoPostBack="true"
                                                        Enabled="False">
                                                        <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                                                        <asp:ListItem>RB</asp:ListItem>
                                                        <asp:ListItem>GL</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>   
                                <tr id="trGLAccountBranchCode" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblGLAccountBranchCode" runat="server" CssClass="lbl" Text="Beneficiary Account Branch Code"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlGLAccountBranchCode" runat="server" CssClass="dropdown">
                                                                </asp:DropDownList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr> 
                                <tr id="trBeneAccountBranchCode" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneAccountBranchCode" runat="server" CssClass="lbl" Text="Beneficiary Account Branch Code"></asp:Label>
                                                    <asp:Label ID="lblReqBeneAccountBranchCode" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBeneAccountBranchCode" runat="server" CssClass="txtbox" 
                                                        MaxLength="10" Enabled="False"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBeneAccountBranchCode" runat="server">
                                                        <telerik:RegExpTextBoxSetting BehaviorID="reBeneAccountBranchCode" EmptyMessage=""
                                                            ValidationExpression="^[a-zA-Z0-9 -]+$" ErrorMessage="Invalid Account Branch Code"
                                                            Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBeneAccountBranchCode" />
                                                            </TargetControls>
                                                        </telerik:RegExpTextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trAccountTitle" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblAccountTitle" runat="server" CssClass="lbl" Text="Beneficiary Account Title"></asp:Label>
                                                    <asp:Label ID="lblReqAccountTitle" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtAccountTitle" runat="server" CssClass="txtbox" 
                                                        MaxLength="100" Enabled="False"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radAccountTitle" runat="server">
                                                        <telerik:TextBoxSetting BehaviorID="reAccountTitle" EmptyMessage=""
                                                            ErrorMessage="Invalid Enter Account Title" Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtAccountTitle" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trPrintLocation" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblPrintLocation" runat="server" Text="Print Location" CssClass="lbl"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:DropDownList ID="ddlPrintLocation" runat="server" CssClass="dropdown" 
                                                        Enabled="False">
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>                       
                                <%--<tr id="trBeneBankCode" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneBankCode" runat="server" CssClass="lbl" Text="Beneficiary Bank Code"></asp:Label>
                                                                <asp:Label ID="lblReqBeneBankCode" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:TextBox ID="txtBeneBankCode" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox><br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <telerik:RadInputManager ID="radBeneBankCode" runat="server" Enabled="False">
                                                                    <telerik:TextBoxSetting BehaviorID="reBeneBankCode" EmptyMessage="Enter Beneficiary Bank Code"
                                                                        ErrorMessage="Invalid Beneficiary Bank Code" Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                                        <TargetControls>
                                                                            <telerik:TargetInput ControlID="txtBeneBankCode" />
                                                                        </TargetControls>
                                                                    </telerik:TextBoxSetting>
                                                                </telerik:RadInputManager>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <%--<tr id="trClientBankName" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblClientBankName" runat="server" CssClass="lbl" Text="Client Bank"></asp:Label>
                                                                <asp:Label ID="lblReqClientBankName" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlClientBankName" runat="server" CssClass="dropdown">
                                                                </asp:DropDownList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:RequiredFieldValidator ID="rfvddlClientBankName" runat="server" ControlToValidate="ddlClientBankName"
                                                                    Display="Dynamic" Enabled="False" ErrorMessage="Please select Client Bank" InitialValue="0"
                                                                    SetFocusOnError="True" ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <tr id="trDDPayableLocationCity" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblDDPayableLocationCity" runat="server" CssClass="lbl" Text="DD Drawn City"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlDDPayableLocationCity" runat="server" 
                                                                    CssClass="dropdown" Enabled="False">
                                                                </asp:DropDownList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneMobNo" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneMobNo" runat="server" CssClass="lbl" Text="Mobile No."></asp:Label>
                                                                <asp:Label ID="lblReqBeneMobNo" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:TextBox ID="txtBeneMobileNo" runat="server" CssClass="txtbox" 
                                                                    MaxLength="150" Enabled="False"></asp:TextBox><br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <telerik:RadInputManager ID="radBeneMobNo" runat="server">
                                                                    <telerik:RegExpTextBoxSetting BehaviorID="reBeneMobNo" EmptyMessage=""
                                                                        ValidationExpression="[0-9 -]{2,15}" ErrorMessage="Invalid Mobile No." Validation-IsRequired="false"
                                                                        Validation-ValidationGroup="Preview">
                                                                        <TargetControls>
                                                                            <telerik:TargetInput ControlID="txtBeneMobileNo" />
                                                                        </TargetControls>
                                                                    </telerik:RegExpTextBoxSetting>
                                                                </telerik:RadInputManager>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneEmail" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblEmail" runat="server" CssClass="lbl" Text="Email"></asp:Label>
                                                                <asp:Label ID="lblReqEmail" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:TextBox ID="txtBeneEmail" runat="server" CssClass="txtbox" MaxLength="150" 
                                                                    Enabled="False"></asp:TextBox><br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <telerik:RadInputManager ID="radBeneEmail" runat="server">
                                                                    <telerik:TextBoxSetting BehaviorID="reBeneEmail" EmptyMessage=""
                                                                        ErrorMessage="Invalid Email Address" Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                                        <TargetControls>
                                                                            <telerik:TargetInput ControlID="txtBeneEmail" />
                                                                        </TargetControls>
                                                                    </telerik:TextBoxSetting>
                                                                </telerik:RadInputManager>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneCity" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneCity" runat="server" CssClass="lbl" Text="Beneficiary City"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlBeneficiaryCity" runat="server" CssClass="dropdown" 
                                                                    Enabled="False">
                                                                </asp:DropDownList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneCountry" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneCountry" runat="server" CssClass="lbl" Text="Beneficiary Country"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlBeneficiaryCountry" runat="server" CssClass="dropdown" 
                                                                    Enabled="False">
                                                                </asp:DropDownList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBenePickUpLocation" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBenePickUpLocation" runat="server" CssClass="lbl" Text="Beneficiary PickUp Location"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlBenePickUpLocation" runat="server" CssClass="dropdown" 
                                                                    Enabled="False">
                                                                </asp:DropDownList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <%--   <tr id="trClientProvince" runat="server">
                               <td style="width: 100%">
                                   <table style="width: 100%">
                                       <tr>
                                           <td style="width: 100%">
                                               <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                   <tr>
                                                       <td style="width: 100%">
                                                           <asp:Label ID="lblClientProvince" runat="server" CssClass="lbl" Text="Client Province"></asp:Label>
                                                           <asp:Label ID="lblReqClientProvince" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                       </td>
                                                   </tr>
                                                   <tr>
                                                       <td align="left" style="width: 100%">
                                                           <asp:DropDownList ID="ddlClientProvince" runat="server" CssClass="dropdown">
                                                           </asp:DropDownList>
                                                       </td>
                                                   </tr>
                                                   <tr>
                                                       <td style="width: 100%">
                                                           <asp:RequiredFieldValidator ID="rfvddlClientProvince" runat="server" 
                                                               ControlToValidate="ddlClientProvince" Display="Dynamic" Enabled="False" 
                                                               ErrorMessage="Please select Client Province" InitialValue="0" 
                                                               SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                       </td>
                                                   </tr>
                                               </table>
                                           </td>
                                       </tr>
                                   </table>
                               </td>
                           </tr>--%>
                                <%-- <tr id="trBeneAddress2" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneAddress2" runat="server" CssClass="lbl" Text="Address 2"></asp:Label>
                                                    <asp:Label ID="lblReqBeneAddress2" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBeneAddress2" runat="server" CssClass="txtbox" Height="60px"
                                                        MaxLength="150" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBeneAddress2" runat="server" Enabled="False">
                                                        <telerik:TextBoxSetting BehaviorID="reBeneAddress2" EmptyMessage="Enter Address"
                                                            Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBeneAddress2" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <tr id="trAmountPKR" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblAmountPKR" runat="server" Text="Amount" CssClass="lbl"></asp:Label>
                                                    <asp:Label ID="lblReqAmountPKR" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:TextBox ID="txtAmountPKR" runat="server" CssClass="txtbox" MaxLength="14" 
                                                        AutoPostBack="True" Enabled="False"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radAmountPKR" runat="server">
                                                        <telerik:NumericTextBoxSetting BehaviorID="reAmountPKR" EmptyMessage=""
                                                            Validation-IsRequired="false" Type="Number" ErrorMessage="Amount is missing"
                                                            Validation-ValidationGroup="Preview" DecimalDigits="2" AllowRounding="false">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtAmountPKR" />
                                                            </TargetControls>
                                                        </telerik:NumericTextBoxSetting>
                                                        <%-- <telerik:NumericTextBoxSetting BehaviorID="reAmountPKR" EmptyMessage="Enter Amount in PKR"
                                                            ErrorMessage="Invalid Amount" Validation-IsRequired="true" DecimalDigits="0">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtAmountPKR" />
                                                            </TargetControls>
                                                        </telerik:NumericTextBoxSetting>--%>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trRemarks" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblRemarks" runat="server" CssClass="lbl" Text="Remarks"></asp:Label>
                                                    <asp:Label ID="lblReqRemarks" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="txtbox" Height="60px" MaxLength="150"
                                                        TextMode="MultiLine" Enabled="False" ></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radRemarks" runat="server">
                                                        <telerik:TextBoxSetting BehaviorID="reRemarks" EmptyMessage="" Validation-IsRequired="false"
                                                            Validation-ValidationGroup="Preview" ErrorMessage="Invalid Remarks">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtRemarks" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trReferenceField1" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblReferenceField1" runat="server" CssClass="lbl" Text="Reference Field 1"></asp:Label>
                                                    <asp:Label ID="lblReqReferenceField1" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtReferenceField1" runat="server" CssClass="txtbox" Height="60px"
                                                        MaxLength="150" TextMode="MultiLine" Enabled="False" ></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radReferenceField1" runat="server">
                                                        <telerik:TextBoxSetting BehaviorID="reReferenceField1" EmptyMessage=""
                                                            ErrorMessage="Invalid Remarks" Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtReferenceField1" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trReferenceField3" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblReferenceField3" runat="server" CssClass="lbl" Text="Reference Field 3"></asp:Label>
                                                    <asp:Label ID="lblReqReferenceField3" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtReferenceField3" runat="server" CssClass="txtbox" Height="60px"
                                                        MaxLength="150" TextMode="MultiLine" Enabled="False" ></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radReferenceField3" runat="server">
                                                        <telerik:TextBoxSetting BehaviorID="reReferenceField3" EmptyMessage=""
                                                            ErrorMessage="Invalid Remarks" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtReferenceField3" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trDescription" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblDescription" runat="server" CssClass="lbl" Text="Description"></asp:Label>
                                                    <asp:Label ID="lblReqDescription" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtDescription" runat="server" CssClass="txtbox" Height="60px" MaxLength="150"
                                                        TextMode="MultiLine" Enabled="False" ></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radDescription" runat="server">
                                                        <telerik:TextBoxSetting BehaviorID="reDescription" EmptyMessage=""
                                                            Validation-IsRequired="false" ErrorMessage="Invalid Description" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtDescription" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneBranchAddress" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneBranchAddress" runat="server" CssClass="lbl" Text="Beneficiary Branch Address"></asp:Label>
                                                    <asp:Label ID="lblReqBeneficiaryBranchAddress" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBeneBranchAddress" runat="server" CssClass="txtbox" Height="60px" MaxLength="500"
                                                        TextMode="MultiLine" Enabled="False" ></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBeneBranchAddress" runat="server">
                                                        <telerik:TextBoxSetting BehaviorID="reBeneBranchAddress" EmptyMessage=""
                                                            Validation-IsRequired="false" ErrorMessage="Invalid Beneficiary Branch Address" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBeneBranchAddress" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="left" valign="top" style="width: 10%">
                        </td>
                        <td align="left" valign="top" style="width: 40%">
                            <table style="width: 100%" cellpadding="0" cellspacing="0">
                                <tr id="trBankAccountNumber" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBankAccountNumber" runat="server" CssClass="lbl" Text="Beneficiary Bank Account Number"></asp:Label>
                                                    <asp:Label ID="lblReqBankAccountNumber" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBankAccountNumber" runat="server" CssClass="txtbox" 
                                                        MaxLength="100" Enabled="False"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBankAccountNumber" runat="server">
                                                        <telerik:RegExpTextBoxSetting BehaviorID="reBankAccountNumber" EmptyMessage=""
                                                            ValidationExpression="^[0-9]+$" ErrorMessage="Invalid Bank Account Number"
                                                            Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBankAccountNumber" />
                                                            </TargetControls>
                                                        </telerik:RegExpTextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneAccountCurrency" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneAccountCurrency" runat="server" CssClass="lbl" Text="Beneficiary Account Currency"></asp:Label>
                                                    <asp:Label ID="lblReqBeneAccountCurrency" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBeneAccountCurrency" runat="server" CssClass="txtbox" 
                                                        MaxLength="10" Enabled="False"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBeneAccountCurrency" runat="server">
                                                        <telerik:RegExpTextBoxSetting BehaviorID="reBeneAccountCurrency" EmptyMessage=""
                                                            ValidationExpression="^[a-zA-Z0-9 -]+$" ErrorMessage="Invalid Account Currency"
                                                            Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBeneAccountCurrency" />
                                                            </TargetControls>
                                                        </telerik:RegExpTextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trInstrumentNo" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblInstrumentNo" runat="server" CssClass="lbl" Text="Instrument No."></asp:Label>
                                                                <asp:Label ID="lblReqInstrumentNo" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:TextBox ID="txtInstrumentNo" runat="server" CssClass="txtbox" MaxLength="18" 
                                                                    Enabled="False"></asp:TextBox><br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <telerik:RadInputManager ID="radInstrumentNo" runat="server">
                                                                    <telerik:RegExpTextBoxSetting BehaviorID="reInstrumentNo" EmptyMessage=""
                                                                        ValidationExpression="^\d+$" ErrorMessage="Invalid Instrument No." Validation-IsRequired="false"
                                                                        Validation-ValidationGroup="Preview">
                                                                        <TargetControls>
                                                                            <telerik:TargetInput ControlID="txtInstrumentNo" />
                                                                        </TargetControls>
                                                                    </telerik:RegExpTextBoxSetting>
                                                                </telerik:RadInputManager>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneBankName" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBankName" runat="server" CssClass="lbl" Text="Beneficiary Bank"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:DropDownList ID="ddlBeneBankName" runat="server" CssClass="dropdown" 
                                                        Enabled="False">
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    &nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <%--<tr id="trBankBranchCode" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBankBranchCode" runat="server" CssClass="lbl" Text="Beneficiary Bank Branch Code"></asp:Label>
                                                    <asp:Label ID="lblReqBankBranchCode" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBankBranchCode" runat="server" CssClass="txtbox" MaxLength="15"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBankBranchCode" runat="server" Enabled="False">
                                                        <telerik:TextBoxSetting BehaviorID="reBankCode" EmptyMessage="Enter Bank Branch Code"
                                                            ErrorMessage="Invalid Bank Branch Code" Validation-IsRequired="False" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBankBranchCode" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <%-- <tr id="trBankIMD" runat="server">
                                    <td align="left" valign="top" style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBankIMD" runat="server" CssClass="lbl" Text="Beneficary Bank IMD"></asp:Label>
                                                    <asp:Label ID="lblReqBankIMD" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBankIMD" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBankIMD" runat="server" Enabled="False">
                                                        <telerik:TextBoxSetting BehaviorID="reBankIMD" EmptyMessage="Enter Bank IMD" ErrorMessage="Invalid Bank IMD"
                                                            Validation-IsRequired="true">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBankIMD" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <%--<tr id="trBranchName" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBranchName" runat="server" CssClass="lbl" Text="Beneficiary Bank Branch"></asp:Label>
                                                                <asp:Label ID="lblReqBranchName" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlBeneBankBranch" runat="server" CssClass="dropdown">
                                                                </asp:DropDownList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:RequiredFieldValidator ID="rfvddlBeneBankBranch" runat="server" ControlToValidate="ddlBeneBankBranch"
                                                                    Display="Dynamic" Enabled="False" ErrorMessage="Please select Beneficiary Bank"
                                                                    InitialValue="0" SetFocusOnError="True" ValidationGroup="Preview"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <%--<tr id="trBeneBankAddress" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBankAddress" runat="server" CssClass="lbl" Text="Beneficiary Bank Address"></asp:Label>
                                                    <asp:Label ID="lblReqBankaddress" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBankAddress" runat="server" CssClass="txtbox" Height="60px" MaxLength="150"
                                                        TextMode="MultiLine"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBankAddress" runat="server" Enabled="False">
                                                        <telerik:TextBoxSetting BehaviorID="reBankAddress" EmptyMessage="Enter Bank Address"
                                                            Validation-IsRequired="false">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBankAddress" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>--%>
                                <tr id="trDDPayableLocationProvince" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblDDPayableLocationProvince" runat="server" CssClass="lbl" Text="DD Drawn Location Province"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlDDPayableLocationProvince" runat="server" 
                                                                    CssClass="dropdown" AutoPostBack="True" Enabled="False">
                                                                </asp:DropDownList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                
                                <tr id="trBenePhoneNo" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBenePhoneNo" runat="server" CssClass="lbl" Text="Phone No."></asp:Label>
                                                                <asp:Label ID="lblReqPhonNo" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:TextBox ID="txtBenePhoneNo" runat="server" CssClass="txtbox" 
                                                                    MaxLength="15" Enabled="False"></asp:TextBox><br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <telerik:RadInputManager ID="radBenePhoneNo" runat="server">
                                                                    <telerik:RegExpTextBoxSetting BehaviorID="reBenePhoneNo" EmptyMessage=""
                                                                        ValidationExpression="[0-9 -]{2,15}" ErrorMessage="Invalid Phone No." Validation-IsRequired="false"
                                                                        Validation-ValidationGroup="Preview">
                                                                        <TargetControls>
                                                                            <telerik:TargetInput ControlID="txtBenePhoneNo" />
                                                                        </TargetControls>
                                                                    </telerik:RegExpTextBoxSetting>
                                                                </telerik:RadInputManager>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneIDType" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneIDType" runat="server" CssClass="lbl" Text="Beneficiary ID Type"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlBeneIDType" runat="server" CssClass="dropdown" AutoPostBack="false">
                                                                    <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                                                                    <asp:ListItem>CNIC</asp:ListItem>
                                                                    <asp:ListItem>Passport</asp:ListItem>
                                                                    <asp:ListItem>NICOP</asp:ListItem>
                                                                    <asp:ListItem>Driving License</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneIDNo" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneIDNo" runat="server" CssClass="lbl" Text="Beneficiary ID No."></asp:Label>
                                                                <asp:Label ID="lblReqBeneIDNo" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:TextBox ID="txtBeneIDNo" runat="server" CssClass="txtbox" 
                                                                    MaxLength="15" Enabled="False"></asp:TextBox><br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <telerik:RadInputManager ID="radBeneIDNo" runat="server">
                                                                    <telerik:TextBoxSetting BehaviorID="reBeneIDNo" EmptyMessage=""
                                                                      ErrorMessage="Invalid Bene ID No" Validation-IsRequired="false"
                                                                        Validation-ValidationGroup="Preview">
                                                                        <TargetControls>
                                                                            <telerik:TargetInput ControlID="txtBeneIDNo" />
                                                                        </TargetControls>
                                                                    </telerik:TextBoxSetting>
                                                                </telerik:RadInputManager>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trValueDate" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblValueDate" runat="server" CssClass="lbl" Text="Value Date"></asp:Label>
                                                                <asp:Label ID="lblReqValueDate" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                               
                                                                <telerik:RadDatePicker ID="txtValueDate" runat="server"  EnableTyping="false"
                                                                    DateInput-DateFormat="dd-MMM-yyyy" DateInput-EmptyMessage="" 
                                                                    ImagesPath="" SharedCalendarID="sharedCalendarValueDate" Enabled="False">
                                                                </telerik:RadDatePicker>
                                                                <telerik:RadCalendar ID="sharedCalendarValueDate" runat="server" 
                                                                    dateinput-dateformat="dd-MMM-yyyy" enablemultiselect="False" 
                                                                    ViewSelectorText="x">
                                                                </telerik:RadCalendar>
                                                               
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trPrintDate" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblPrintDate" runat="server" CssClass="lbl" Text="Print Date"></asp:Label>
                                                                <asp:Label ID="lblReqPrintDate" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                               
                                                                <telerik:RadDatePicker ID="txtPrintDate" runat="server"  EnableTyping="false"
                                                                    DateInput-DateFormat="dd-MMM-yyyy" DateInput-EmptyMessage="" 
                                                                    ImagesPath="" SharedCalendarID="sharedCalenderPrintDate" Enabled="False">
                                                                </telerik:RadDatePicker>
                                                                <telerik:RadCalendar ID="sharedCalenderPrintDate" runat="server" 
                                                                    dateinput-dateformat="dd-MMM-yyyy" enablemultiselect="False" 
                                                                    ViewSelectorText="x">
                                                                </telerik:RadCalendar>
                                                               
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneProvince" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneProvince" runat="server" CssClass="lbl" Text="Beneficiary Province"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:DropDownList ID="ddlBeneficiaryProvince" runat="server" 
                                                                    CssClass="dropdown" Enabled="False">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneCNICNo" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblBeneCNIC" runat="server" CssClass="lbl" Text="NIC No."></asp:Label>
                                                                <asp:Label ID="lblReqBeneNICNo" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" style="width: 100%">
                                                                <asp:TextBox ID="txtBeneCNCINo" runat="server" CssClass="txtbox" 
                                                                    MaxLength="25" Enabled="False"></asp:TextBox><br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%">
                                                                <telerik:RadInputManager ID="radBeneCNICNo" runat="server">
                                                                    <telerik:RegExpTextBoxSetting BehaviorID="reBeneCNICNo" EmptyMessage=""
                                                                        ErrorMessage="Invalid CNIC No" Validation-IsRequired="false" Validation-ValidationGroup="Preview"
                                                                        ValidationExpression="[0-9-]{13,15}">
                                                                        <TargetControls>
                                                                            <telerik:TargetInput ControlID="txtBeneCNCINo" />
                                                                        </TargetControls>
                                                                    </telerik:RegExpTextBoxSetting>
                                                                </telerik:RadInputManager>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                
                                <%--<tr id="trClientCity" runat="server">
                               <td style="width: 100%">
                                   <table style="width: 100%">
                                       <tr>
                                           <td style="width: 100%">
                                               <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                   <tr>
                                                       <td style="width: 100%">
                                                           <asp:Label ID="lblClientCity" runat="server" CssClass="lbl" Text="Client City"></asp:Label>
                                                           <asp:Label ID="lblReqClientCity" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                       </td>
                                                   </tr>
                                                   <tr>
                                                       <td align="left" style="width: 100%">
                                                           <asp:DropDownList ID="ddlClientCity" runat="server" CssClass="dropdown">
                                                           </asp:DropDownList>
                                                       </td>
                                                   </tr>
                                                   <tr>
                                                       <td style="width: 100%">
                                                           <asp:RequiredFieldValidator ID="rfvddlBeneficiaryCity0" runat="server" 
                                                               ControlToValidate="ddlClientCity" Display="Dynamic" Enabled="False" 
                                                               ErrorMessage="Please select Client City" InitialValue="0" 
                                                               SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                       </td>
                                                   </tr>
                                               </table>
                                           </td>
                                       </tr>
                                   </table>
                               </td>
                           </tr>--%>
                                <%--<tr id="trClientCountry" runat="server">
                               <td style="width: 100%">
                                   <table style="width: 100%">
                                       <tr>
                                           <td style="width: 100%">
                                               <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                   <tr>
                                                       <td style="width: 100%">
                                                           <asp:Label ID="lblClientCountry" runat="server" CssClass="lbl" Text="Client Country"></asp:Label>
                                                           <asp:Label ID="lblReqClientCountry" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                       </td>
                                                   </tr>
                                                   <tr>
                                                       <td align="left" style="width: 100%">
                                                           <asp:DropDownList ID="ddlClientCountry" runat="server" CssClass="dropdown">
                                                           </asp:DropDownList>
                                                       </td>
                                                   </tr>
                                                   <tr>
                                                       <td style="width: 100%">
                                                           <asp:RequiredFieldValidator ID="rfvddlClientCountry" runat="server" 
                                                               ControlToValidate="ddlClientCountry" Display="Dynamic" Enabled="False" 
                                                               ErrorMessage="Please select Client Country" InitialValue="0" 
                                                               SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                       </td>
                                                   </tr>
                                               </table>
                                           </td>
                                       </tr>
                                   </table>
                               </td>
                           </tr>--%>
                           <tr id="trAmountInWords" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblAmountInWords" runat="server" CssClass="lbl" Text="Amount In Words"></asp:Label>
                                                    <asp:Label ID="lblReqAmountInWords" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtAmountInWords" runat="server" CssClass="txtbox" Height="60px"
                                                        MaxLength="150" TextMode="MultiLine" ReadOnly="true" Enabled="False"></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radAmountInWords" runat="server">
                                                        <telerik:TextBoxSetting BehaviorID="reAmountInWords" EmptyMessage=""
                                                            ErrorMessage="Invalid Amount" Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtAmountInWords" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trReferenceField2" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lbltrReferenceField2" runat="server" CssClass="lbl" Text="Reference Field 2"></asp:Label>
                                                    <asp:Label ID="lblReqtrReferenceField2" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txttrReferenceField2" runat="server" CssClass="txtbox" Height="60px"
                                                        MaxLength="150" TextMode="MultiLine" Enabled="False" ></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radtrReferenceField2" runat="server">
                                                        <telerik:TextBoxSetting BehaviorID="retrReferenceField2" EmptyMessage=""
                                                            ErrorMessage="Invalid Remarks" Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txttrReferenceField2" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trBeneAddress1" runat="server">
                                    <td style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblBeneAddress" runat="server" CssClass="lbl" Text="Address 1"></asp:Label>
                                                    <asp:Label ID="lblReqBeneAddress1" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 100%">
                                                    <asp:TextBox ID="txtBeneAddress1" runat="server" CssClass="txtbox" Height="60px"
                                                        MaxLength="150" TextMode="MultiLine" Enabled="False" ></asp:TextBox><br />
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%">
                                                    <telerik:RadInputManager ID="radBeneAddress1" runat="server">
                                                        <telerik:TextBoxSetting BehaviorID="reBeneAddress1" EmptyMessage=""
                                                            ErrorMessage="Invalid Address" Validation-IsRequired="false" Validation-ValidationGroup="Preview">
                                                            <TargetControls>
                                                                <telerik:TargetInput ControlID="txtBeneAddress1" />
                                                            </TargetControls>
                                                        </telerik:TextBoxSetting>
                                                    </telerik:RadInputManager>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                
                                
                                
                            </table>
                        </td>
                        <td align="left" valign="top" style="width: 10%">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="4">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top" colspan="4">
                            <asp:Button ID="btnUpdate" runat="server" Text="Update" Width="77px" CssClass="btn"
                                Height="25px" ValidationGroup="Preview" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="157px" CssClass="btnCancel"
                                Height="25px" CausesValidation="False" />
                            <asp:Button ID="btnCancelSingle" runat="server" CausesValidation="False" 
                                CssClass="btnCancel" Height="25px" Text="Cancel" Width="77px" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" style="width: 40%">
                            &nbsp;
                        </td>
                        <td align="left" valign="top" style="width: 10%">
                            &nbsp;
                        </td>
                        <td align="left" valign="top" style="width: 40%">
                            &nbsp;
                        </td>
                        <td align="left" valign="top" style="width: 10%">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <%--<tr id="trClientCountry" runat="server">
                               <td style="width: 100%">
                                   <table style="width: 100%">
                                       <tr>
                                           <td style="width: 100%">
                                               <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                   <tr>
                                                       <td style="width: 100%">
                                                           <asp:Label ID="lblClientCountry" runat="server" CssClass="lbl" Text="Client Country"></asp:Label>
                                                           <asp:Label ID="lblReqClientCountry" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                                       </td>
                                                   </tr>
                                                   <tr>
                                                       <td align="left" style="width: 100%">
                                                           <asp:DropDownList ID="ddlClientCountry" runat="server" CssClass="dropdown">
                                                           </asp:DropDownList>
                                                       </td>
                                                   </tr>
                                                   <tr>
                                                       <td style="width: 100%">
                                                           <asp:RequiredFieldValidator ID="rfvddlClientCountry" runat="server" 
                                                               ControlToValidate="ddlClientCountry" Display="Dynamic" Enabled="False" 
                                                               ErrorMessage="Please select Client Country" InitialValue="0" 
                                                               SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                       </td>
                                                   </tr>
                                               </table>
                                           </td>
                                       </tr>
                                   </table>
                               </td>
                           </tr>--%>
        </td>
    </tr>
    <tr>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
        <td style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%">
            &nbsp;
        </td>
    </tr>
</table>
