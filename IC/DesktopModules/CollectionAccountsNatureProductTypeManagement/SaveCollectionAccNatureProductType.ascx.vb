﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals

Partial Class DesktopModules_CollectionAccountsNatureProductTypeManagement_SaveCollectionAccNatureProductType
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private AccountNumber As String
    Private BranchCode As String
    Private Currency As String
    Private CollectionNatureCode As String
    Private ProductTypeCode As String
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
           
            AccountNumber = Request.QueryString("AccountNumber").ToString()
            BranchCode = Request.QueryString("BranchCode").ToString()
            Currency = Request.QueryString("Currency").ToString()
            CollectionNatureCode = Request.QueryString("CollectionNatureCode").ToString()
            
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlCompany()
                LoadddlAccounts()
                lblCollectionProductType.Visible = False
                lblCollectionProdTypeReq.Visible = False
                pnlCollectionNature.Visible = False
                If AccountNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" And CollectionNatureCode.ToString() = "0" Then
                    lblPageHeader.Text = "Add Collection Account, Nature Product Type"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))

                Else

                    lblPageHeader.Text = "Edit Collection Account, Nature Product Type"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    Dim objCollAccNatureProdType As New ICCollectionAccountsCollectionNatureProductTypeCollection
                    Dim objICGroup As New ICGroup
                    Dim objICCompany As New ICCompany
                    Dim objICCollAccounts As New ICCollectionAccounts
                    Dim objICCollAccCollNature As New ICCollectionAccountsCollectionNature
                    objCollAccNatureProdType.es.Connection.CommandTimeout = 3600
                    objICGroup.es.Connection.CommandTimeout = 3600
                    objICCompany.es.Connection.CommandTimeout = 3600
                    objICCollAccounts.es.Connection.CommandTimeout = 3600
                    objICCollAccCollNature.es.Connection.CommandTimeout = 3600

                    objICCollAccCollNature.LoadByPrimaryKey(AccountNumber, BranchCode, Currency, CollectionNatureCode)
                    objICCollAccounts.LoadByPrimaryKey(objICCollAccCollNature.AccountNumber, objICCollAccCollNature.BranchCode, objICCollAccCollNature.Currency)

                    objICCompany.LoadByPrimaryKey(objICCollAccounts.CompanyCode)

                    ddlGroup.SelectedValue = objICCompany.GroupCode
                    LoadddlCompany()
                    ddlCompany.SelectedValue = objICCompany.CompanyCode
                    LoadddlAccounts()

                    ddlAccount.SelectedValue = objICCollAccCollNature.AccountNumber + "-" + objICCollAccCollNature.BranchCode + "-" + objICCollAccCollNature.Currency + "-" + objICCollAccCollNature.CollectionNatureCode

                    LoadchklstCollectionProductType()
               
                    Dim item As New ListItem

                    For Each item In chklstCollectionProductType.Items
                        item.Selected = ICCollectionAccountsCollectionNatureCollectionProductTypeController.GetTaggedProductType(item.Value, ddlAccount.SelectedValue.Split("-")(3).ToString(), ddlAccount.SelectedValue.Split("-")(0).ToString(), ddlAccount.SelectedValue.Split("-")(1).ToString(), ddlAccount.SelectedValue.Split("-")(2).ToString())
                    Next

                    ddlGroup.Enabled = False
                    ddlCompany.Enabled = False
                    ddlAccount.Enabled = False

                End If

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Collection Accounts Collection Nature Collection ProductType Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            Dim dt As DataTable
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            dt = ICGroupController.GetAllActiveGroupsIsCollectionAllowonCompany()
            ddlGroup.DataSource = dt
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlcompany.Items.Clear()
            ddlcompany.Items.Add(lit)
            ddlcompany.AppendDataBoundItems = True
            ddlcompany.DataSource = ICCompanyController.GetAllActiveandApproveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString)
            ddlcompany.DataTextField = "CompanyName"
            ddlcompany.DataValueField = "CompanyCode"
            ddlcompany.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccounts()
        Try
            Dim lit As New ListItem
            Dim dt As DataTable

            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlAccount.Items.Clear()
            ddlAccount.Items.Add(lit)
            ddlAccount.AppendDataBoundItems = True
            dt = ICCollectionAccountsCollectionNatureController.GetCollectionAccountCollectionNatureByCompanyCode(ddlCompany.SelectedValue.ToString())
            ddlAccount.DataSource = dt
            ddlAccount.DataTextField = "CollectionAccountAndCollectionNature"
            ddlAccount.DataValueField = "CollectionAccountCollectionNature"
            ddlAccount.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadchklstCollectionProductType()
        Try

            Dim collProdType As New ICCollectionProductTypeCollection
            Dim dt As New DataTable
         
            If AccountNumber = "0" And BranchCode = "0" And Currency = "0" And CollectionNatureCode = "0" Then
                dt = ICCollectionProductTypeController.GetAllCollectionProductTypesActiveAndApproveUnTagged(ddlAccount.SelectedValue.Split("-")(0).ToString(), ddlAccount.SelectedValue.Split("-")(1).ToString(), ddlAccount.SelectedValue.Split("-")(2).ToString(), ddlAccount.SelectedValue.Split("-")(3).ToString())

                If dt.Rows.Count = 0 Then
                    collProdType = ICCollectionProductTypeController.GetAllCollectionProductTypesActiveAndApprove()
                    If collProdType.Count = 0 Then
                        UIUtilities.ShowDialog(Me, "Save Collection Account Collection Nature Collection Product Type", "Product Type not defined.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If

                chklstCollectionProductType.DataSource = dt
                chklstCollectionProductType.DataTextField = "ProductTypeName"
                chklstCollectionProductType.DataValueField = "ProductTypeCode"
                chklstCollectionProductType.DataBind()

                If chklstCollectionProductType.Items.Count = 0 Then
                    UIUtilities.ShowDialog(Me, "Save Collection Account Collection Nature Collection Product Type", "All Product Types are already tagged.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL())
                End If

            Else
                chklstCollectionProductType.DataSource = ICCollectionProductTypeController.GetAllCollectionProductTypesActiveAndApprove()
                chklstCollectionProductType.DataTextField = "ProductTypeName"
                chklstCollectionProductType.DataValueField = "ProductTypeCode"
                chklstCollectionProductType.DataBind()

            End If

            If chklstCollectionProductType.Items.Count > 0 Then
                pnlCollectionNature.Visible = True
                chklstCollectionProductType.Visible = True
                lblCollectionProductType.Visible = True
                lblCollectionProdTypeReq.Visible = True
                regCollNature.Enabled = True

            Else
                pnlCollectionNature.Visible = False
                regCollNature.Enabled = False
                chklstCollectionProductType.Visible = False
                lblCollectionProductType.Visible = False
                lblCollectionProdTypeReq.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
            LoadddlAccounts()
            lblCollectionProductType.Visible = False
            lblCollectionProdTypeReq.Visible = False
            pnlCollectionNature.Visible = False
       Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadddlAccounts()
            lblCollectionProductType.Visible = False
            lblCollectionProdTypeReq.Visible = False
            pnlCollectionNature.Visible = False
       Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlAccount_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlAccount.SelectedIndexChanged
        Try
            If ddlAccount.SelectedValue <> "0" Then
                lblCollectionProductType.Visible = True
                lblCollectionProdTypeReq.Visible = True
                pnlCollectionNature.Visible = True
                LoadchklstCollectionProductType()

            Else

                lblCollectionProductType.Visible = False
                lblCollectionProdTypeReq.Visible = False
                pnlCollectionNature.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim lit As New ListItem
                Dim objAccountCollectionNature As New ICCollectionAccountsCollectionNatureProductType
                Dim objchkAccountCollectionNature As New ICCollectionAccountsCollectionNatureProductType
                Dim objICCompany As New ICCompany
                Dim objCollNature As New ICCollectionNature
                objICCompany.es.Connection.CommandTimeout = 3600
                objCollNature.es.Connection.CommandTimeout = 3600

                If ddlGroup.SelectedValue <> "0" And ddlCompany.SelectedValue <> "0" And ddlAccount.SelectedValue <> "0" Then

                    objAccountCollectionNature.AccountNumber = ddlAccount.SelectedValue.Split("-")(0).ToString()
                    objAccountCollectionNature.BranchCode = ddlAccount.SelectedValue.Split("-")(1).ToString()
                    objAccountCollectionNature.Currency = ddlAccount.SelectedValue.Split("-")(2).ToString()
                    objAccountCollectionNature.CollectionNatureCode = ddlAccount.SelectedValue.Split("-")(3).ToString()

                    If AccountNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" And CollectionNatureCode.ToString() = "0" Then
                        objAccountCollectionNature.CreateBy = Me.UserId
                        objAccountCollectionNature.CreateDate = Date.Now

                        For Each lit In chklstCollectionProductType.Items
                            If lit.Selected = True Then
                                objAccountCollectionNature.ProductTypeCode = lit.Value
                                ICCollectionAccountsCollectionNatureCollectionProductTypeController.AddCollectionAccountCollectionNatureCollectionProductType(objAccountCollectionNature, Me.UserId.ToString(), Me.UserInfo.Username.ToString(), False, Nothing)
                            End If
                        Next
                        UIUtilities.ShowDialog(Me, "Save Collection Account, Nature and Product Type Tagging", "Collection Account, Nature and Product Type Tagging added successfully.", ICBO.IC.Dialogmessagetype.Success)

                    Else
                        objAccountCollectionNature.CreateBy = Me.UserId
                        objAccountCollectionNature.CreateDate = Date.Now

                        DeletePreviousProductTypes(ddlAccount.SelectedValue.Split("-")(3).ToString(), ddlAccount.SelectedValue.Split("-")(0).ToString(), ddlAccount.SelectedValue.Split("-")(1).ToString(), ddlAccount.SelectedValue.Split("-")(2).ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())

                        For Each lit In chklstCollectionProductType.Items
                            If lit.Selected = True Then
                                objAccountCollectionNature.ProductTypeCode = lit.Value
                                Dim OldValues As String = ""
                                OldValues = "<br />Collection Account, Nature and Product Type Tagging : Previous Values [Account Number : " & objAccountCollectionNature.AccountNumber & " ; Branch Code : " & objAccountCollectionNature.BranchCode & " ; Currency : " & objAccountCollectionNature.Currency & " ; Collection Nature Code : " & objAccountCollectionNature.CollectionNatureCode & " ; Product Type Code : " & objAccountCollectionNature.ProductTypeCode & "] "
                                ICCollectionAccountsCollectionNatureCollectionProductTypeController.AddCollectionAccountCollectionNatureCollectionProductType(objAccountCollectionNature, Me.UserId.ToString(), Me.UserInfo.Username.ToString(), True, OldValues)
                            End If
                        Next

                        UIUtilities.ShowDialog(Me, "Save Collection Account, Nature and Product Type Tagging", "Collection Account, Nature and Product Type Tagging updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())

                    End If

                    Clear()
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Private Sub Clear()
        LoadddlGroup()
        LoadddlCompany()
        LoadddlAccounts()
        lblCollectionProductType.Visible = False
        lblCollectionProdTypeReq.Visible = False
        pnlCollectionNature.Visible = False
        pnlCollectionNature.Visible = False
        lblCollectionProductType.Visible = False
        lblCollectionProdTypeReq.Visible = False
    End Sub

    Private Sub DeletePreviousProductTypes(ByVal CollNatureCode As String, ByVal AccNo As String, ByVal BranchCode As String, ByVal Currency As String, ByVal UserId As String, ByVal UserName As String)
        Dim collCollAccNatProdType As New ICCollectionAccountsCollectionNatureProductTypeCollection
        Dim objCollAccNatProdType As New ICCollectionAccountsCollectionNatureProductType
        Dim CurrentAt As String

        collCollAccNatProdType.es.Connection.CommandTimeout = 3600

        collCollAccNatProdType.Query.Where(collCollAccNatProdType.Query.CollectionNatureCode = CollNatureCode)
        collCollAccNatProdType.Query.Where(collCollAccNatProdType.Query.AccountNumber = AccNo)
        collCollAccNatProdType.Query.Where(collCollAccNatProdType.Query.BranchCode = BranchCode)
        collCollAccNatProdType.Query.Where(collCollAccNatProdType.Query.Currency = Currency)

        collCollAccNatProdType.Query.Load()

        Dim i As Integer = 0

        For i = 0 To collCollAccNatProdType.Count - 1
            objCollAccNatProdType = New ICCollectionAccountsCollectionNatureProductType
            objCollAccNatProdType.LoadByPrimaryKey(collCollAccNatProdType(i).AccountNumber, collCollAccNatProdType(i).BranchCode, collCollAccNatProdType(i).Currency, collCollAccNatProdType(i).CollectionNatureCode, collCollAccNatProdType(i).ProductTypeCode)

            CurrentAt = "Collection Accounts Collection Nature Product Type: [Collection Nature Code:  " & CollNatureCode & " ; Collection Product Type Code: " & collCollAccNatProdType(i).ProductTypeCode & " ; Account Number:  " & AccNo & " ; Branch Code: " & BranchCode & " ; Currency: " & Currency & "] "

            If AccountNumber <> "0" And BranchCode <> "0" And Currency <> "0" And CollectionNatureCode <> "0" And ProductTypeCode <> "0" Then
                ICUtilities.AddAuditTrail(CurrentAt & " Deleted on Edit ", "Collection Account, Nature and Product Type Tagging", AccNo & ";" & BranchCode & ";" & Currency & ";" & CollectionNatureCode, UserId.ToString(), UserName.ToString(), "DELETE")
            Else
                ICUtilities.AddAuditTrail(CurrentAt & " Deleted on Save ", "Collection Account, Nature and Product Type Tagging", AccNo & ";" & BranchCode & ";" & Currency & ";" & CollectionNatureCode, UserId.ToString(), UserName.ToString(), "DELETE")
            End If

            objCollAccNatProdType.MarkAsDeleted()
            objCollAccNatProdType.Save()
        Next i


    End Sub


End Class
