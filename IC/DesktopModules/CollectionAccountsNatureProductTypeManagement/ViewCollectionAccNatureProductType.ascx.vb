﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.Configuration
Imports System.Web.Configuration
Imports Telerik.Web.UI

Partial Class DesktopModules_CollectionAccountsNatureProductTypeManagement_ViewCollectionAccNatureProductType
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub DesktopModules_AccountPaymentNatureProductTypeManagement_ViewAccountPaymentNatureProductType_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlCompany()
                LoadgvAccountPaymentNature(True)
                btnSaveAccPayNatProTyp.Visible = CBool(htRights("Add"))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Collection Accounts Collection Nature Collection ProductType Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

#Region "Button Events"
    Protected Sub btnSaveAccPayNatProTyp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAccPayNatProTyp.Click
        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveCollectionAccountsNatureProductType", "&mid=" & Me.ModuleId & "&AccountNumber=0" & "&BranchCode=0" & "&Currency=0" & "&CollectionNatureCode=0" & "&ProductTypeCode=0"), False)
        End If
    End Sub
#End Region
#Region "DropDown Events"
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            Dim dt As DataTable
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            dt = ICGroupController.GetAllActiveGroupsIsCollectionAllowonCompany()
            ddlGroup.DataSource = dt
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlcompany.Items.Clear()
            ddlcompany.Items.Add(lit)
            ddlcompany.AppendDataBoundItems = True
            ddlcompany.DataSource = ICCompanyController.GetAllActiveandApproveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString)
            ddlcompany.DataTextField = "CompanyName"
            ddlcompany.DataValueField = "CompanyCode"
            ddlcompany.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        LoadddlCompany()
        LoadgvAccountPaymentNature(True)
    End Sub
    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        LoadgvAccountPaymentNature(True)
    End Sub
#End Region
#Region "Other Functions/Routines"

    Private Sub LoadgvAccountPaymentNature(ByVal IsBind As Boolean)
        Try
            ICCollectionAccountsCollectionNatureCollectionProductTypeController.SetgvCollectionAccountCollectionNaturesProductType(ddlGroup.SelectedValue.ToString(), ddlCompany.SelectedValue.ToString(), gvAccountCollectionNature.CurrentPageIndex + 1, gvAccountCollectionNature.PageSize, gvAccountCollectionNature, IsBind)

            If gvAccountCollectionNature.Items.Count > 0 Then
                lblBankRNF.Visible = False
                gvAccountCollectionNature.Visible = True
                btnBulkDeleteAccountPNature.Visible = CBool(htRights("Delete"))
                gvAccountCollectionNature.Columns(8).Visible = CBool(htRights("Delete"))
            Else
                lblBankRNF.Visible = True
                gvAccountCollectionNature.Visible = False
                btnBulkDeleteAccountPNature.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
     
#End Region
    Protected Sub gvAccountPaymentNature_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAccountCollectionNature.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAccountCollectionNature.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAccountPaymentNature_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAccountCollectionNature.ItemDataBound
        Dim chkProcessAll As CheckBox
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAccountCollectionNature.MasterTableView.ClientID & "','0');"
        End If
         
    End Sub

    Protected Sub gvAccountPaymentNature_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvAccountCollectionNature.PageIndexChanged
        gvAccountCollectionNature.CurrentPageIndex = e.NewPageIndex
    End Sub

    Protected Sub gvAccountPaymentNature_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAccountCollectionNature.NeedDataSource
        Try
            LoadgvAccountPaymentNature(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
     
    Protected Sub gvAccountPaymentNature_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvAccountCollectionNature.ItemCommand
        Try
            Dim objCollAccCollNature As New ICCollectionAccountsCollectionNatureProductType
            objCollAccCollNature.es.Connection.CommandTimeout = 3600

            If e.CommandName = "del" Then
                Dim AccountNumber, BranchCode, Currency, CollectionNatureCode As String
                AccountNumber = e.CommandArgument.ToString().Split(";")(0).ToString()
                BranchCode = e.CommandArgument.ToString().Split(";")(1).ToString()
                Currency = e.CommandArgument.ToString().Split(";")(2).ToString()
                CollectionNatureCode = e.CommandArgument.ToString().Split(";")(3).ToString()
                '  ProductTypeCode = e.CommandArgument.ToString().Split(";")(4).ToString()

                'objCollAccCollNature.Query.Select(objCollAccCollNature.Query.ProductTypeCode)
                'objCollAccCollNature.Query.Where(objCollAccCollNature.Query.AccountNumber = AccountNumber And objCollAccCollNature.Query.BranchCode = BranchCode And objCollAccCollNature.Query.Currency = Currency And objCollAccCollNature.Query.CollectionNatureCode = CollectionNatureCode)
                'objCollAccCollNature.LoadByPrimaryKey(AccountNumber, BranchCode, Currency, CollectionNatureCode, objCollAccCollNature.ProductTypeCode)

                ICCollectionAccountsCollectionNatureCollectionProductTypeController.DeleteCollectionAccountCollectionNatureProductType(AccountNumber, BranchCode, Currency, CollectionNatureCode, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                UIUtilities.ShowDialog(Me, "Delete Collection Account, Nature and Product Type Tagging", "Collection Account, Nature and Product Type Tagging deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())

                LoadgvAccountPaymentNature(True)
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Delete Collection Account Collection Nature", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckgvCollectionAccountCollectionNatureForProcessAll() As Boolean
        Try
            Dim rowgvAccountPaymentNature As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvAccountPaymentNature In gvAccountCollectionNature.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvAccountPaymentNature.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnBulkDeleteAccountPNature_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBulkDeleteAccountPNature.Click
        If Page.IsValid Then
            Try
                Dim rowgvAccountCollectionNature As GridDataItem
                Dim chkSelect As CheckBox
                Dim AccountNumber, BranchCode, Currency, CollectionNatureCode, ProductTypeCode As String
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0

                If CheckgvCollectionAccountCollectionNatureForProcessAll() = True Then
                    For Each rowgvAccountCollectionNature In gvAccountCollectionNature.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvAccountCollectionNature.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            Dim objCollAccCollNature As New ICCollectionAccountsCollectionNatureProductType
                            Dim collCollAccCollNatureForDelProdType As New ICCollectionAccountsCollectionNatureProductTypeCollection
                            objCollAccCollNature.es.Connection.CommandTimeout = 3600

                            AccountNumber = rowgvAccountCollectionNature.GetDataKeyValue("AccountNumber").ToString()
                            BranchCode = rowgvAccountCollectionNature.GetDataKeyValue("BranchCode").ToString()
                            Currency = rowgvAccountCollectionNature.GetDataKeyValue("Currency").ToString()
                            CollectionNatureCode = rowgvAccountCollectionNature.GetDataKeyValue("CollectionNatureCode").ToString()
                            '     ProductTypeCode = rowgvAccountCollectionNature.GetDataKeyValue("ProductTypeCode").ToString()

                            'collCollAccCollNatureForDelProdType.Query.Select(collCollAccCollNatureForDelProdType(0).Query.ProductTypeCode)
                            'objCollAccCollNature.Query.Where(objCollAccCollNature.Query.AccountNumber = AccountNumber And objCollAccCollNature.Query.BranchCode = BranchCode And objCollAccCollNature.Query.Currency = Currency And objCollAccCollNature.Query.CollectionNatureCode = CollectionNatureCode)


                            '  objCollAccCollNature.LoadByPrimaryKey(AccountNumber, BranchCode, Currency, CollectionNatureCode, collCollAccCollNatureForDelProdType(0).Query.ProductTypeCode)
                            'ActionText = "Collection Account Collection Nature Collection Product Type: [Code:  " & objCollAccCollNature.CollectionNatureCode.ToString() & " ; Account Number:  " & objCollAccCollNature.AccountNumber.ToString() & " ; Branch Code: " & objCollAccCollNature.BranchCode.ToString() & " ; Currency: " & objCollAccCollNature.Currency.ToString() & " ; Collection Nature: " & objCollAccCollNature.CollectionNatureCode.ToString() & " ; IsActive:  " & objCollAccCollNature.IsActive.ToString() & "] " & "Deleted"

                            Try
                                ICCollectionAccountsCollectionNatureCollectionProductTypeController.DeleteCollectionAccountCollectionNatureProductType(AccountNumber, BranchCode, Currency, CollectionNatureCode, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                PassCount = PassCount + 1
                            Catch ex As Exception
                                If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                    FailCount = FailCount + 1
                                End If
                            End Try
                        End If
                    Next
                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Collection Account, Nature and Product Type Tagging", "[" & FailCount.ToString() & "] Collection Account, Nature and Product Type Tagging can not be deleted due to following reasons : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Collection Account, Nature and Product Type Tagging", "[" & PassCount.ToString() & "] Collection Account, Nature and Product Type Tagging deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Delete Collection Account, Nature and Product Type Tagging", "[" & PassCount.ToString() & "] Collection Account, Nature and Product Type Tagging deleted successfully. [" & FailCount.ToString() & "] Collection Account, Nature and Product Type Tagging can not be deleted due to following reasons : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Delete Collection Account, Nature and Product Type Tagging", "Please select atleast one(1) Collection Account, Nature and Product Type Tagging.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub


End Class
