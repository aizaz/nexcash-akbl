﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Partial Class DesktopModules_DDStationaryManagement_SaveDDMasterSeries
     Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private DDMasterSeriesID As String
    Private TextSeriesStartFromChange As Boolean = False
    Private TextNoOfLeavesChange As Boolean = False
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DDMasterSeriesID = Request.QueryString("id").ToString()

            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                If DDMasterSeriesID.ToString = "0" Then
                    lblPageHeader.Text = "Add DD Master Series"
                    btnSaveMasterSeries.Text = "Save"
                    btnSaveMasterSeries.Visible = CBool(htRights("Add"))
                Else
                    Dim objICDDMasterSeries As New ICDDMasterSeries
                    objICDDMasterSeries.es.Connection.CommandTimeout = 3600
                    objICDDMasterSeries.LoadByPrimaryKey(DDMasterSeriesID)
                   lblPageHeader.Text = "Edit DD Master Series"
                    btnSaveMasterSeries.Text = "Update"
                    btnSaveMasterSeries.Visible = CBool(htRights("Update"))
                    txtStartsFrom.ReadOnly = True
                    txtEndsAt.ReadOnly = True
                    txtStartsFrom.Text = objICDDMasterSeries.StartFrom
                    txtEndsAt.Text = objICDDMasterSeries.EndsAt
                    txtNoOfLeaves.Text = CInt(txtEndsAt.Text - txtStartsFrom.Text) + 1


                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "DD Stationary Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSaveMasterSeries_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveMasterSeries.Click
        If Page.IsValid Then
            Try
                Dim objICDDMasterSeries As New ICDDMasterSeries
                Dim objICDDMasterSeriesUpdate As New ICDDMasterSeries
                Dim objICDDInstrument As New ICDDInstruments
                Dim SavedDDMasterSeriesID As Integer = 0
                objICDDMasterSeries.es.Connection.CommandTimeout = 3600
                objICDDInstrument.es.Connection.CommandTimeout = 3600

                objICDDMasterSeries.StartFrom = CLng(txtStartsFrom.Text)
                objICDDMasterSeries.EndsAt = CLng(txtEndsAt.Text)
                objICDDMasterSeries.PreFix = "DD"

                If DDMasterSeriesID.ToString = "0" Then
                    If CheckDuplicateDDMasterSeries(objICDDMasterSeries) = False Then
                        objICDDMasterSeries.CreatedBy = Me.UserId
                        objICDDMasterSeries.CreatedDate = Date.Now
                        objICDDMasterSeries.Creater = Me.UserId
                        objICDDMasterSeries.CreationDate = Date.Now
                        SavedDDMasterSeriesID = ICDDMasterSeriesController.AddDDMasterSeries(objICDDMasterSeries, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        If Not SavedDDMasterSeriesID = 0 Then
                            Dim i As Long = Nothing
                            Dim SeriesStarFrom As Long = CLng(txtStartsFrom.Text)
                            Dim NoOfLeaves As Long = CLng(txtEndsAt.Text)
                            Dim DDMasterSeriesEndsAt As Long = CLng(txtEndsAt.Text)
                            Dim k As Long = SeriesStarFrom
                            For i = SeriesStarFrom To DDMasterSeriesEndsAt

                                objICDDInstrument.DDMasterSeriesID = SavedDDMasterSeriesID
                                objICDDInstrument.DDInstrumentNumber = CLng(i)
                                objICDDInstrument.PreFix = objICDDMasterSeries.PreFix
                                objICDDInstrument.IsUsed = Nothing
                                objICDDInstrument.CreatedDate = Date.Now
                                objICDDInstrument.CreatedBy = Me.UserId
                                objICDDInstrument.CreationDate = Date.Now
                                objICDDInstrument.Creater = Me.UserId
                                ICDDInstrumentsController.AddDDInstrumentSeriesFromDDMasterSeries(objICDDInstrument, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            Next
                        End If
                        UIUtilities.ShowDialog(Me, "Add DD Master Series", "DD Master series added successfully", ICBO.IC.Dialogmessagetype.Success)
                        clear()
                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Sorry! Duplicate DD Master Series is not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                        clear()
                        Exit Sub
                    End If
                Else

                    If objICDDMasterSeriesUpdate.LoadByPrimaryKey(DDMasterSeriesID) Then
                        objICDDMasterSeries.DDMasterSeriesID = objICDDMasterSeriesUpdate.DDMasterSeriesID
                        objICDDMasterSeries.CreatedBy = Me.UserId
                        objICDDMasterSeries.CreatedDate = Date.Now
                        Dim TopOrder As Long = Nothing
                        Dim AvailableLeaves As Integer = Nothing
                        Dim i As Integer = 0
                        TopOrder = CLng(CLng(objICDDMasterSeriesUpdate.EndsAt - objICDDMasterSeriesUpdate.StartFrom) + 1 - CLng(txtNoOfLeaves.Text))
                        If CLng(txtNoOfLeaves.Text) < (CLng(CLng(objICDDMasterSeriesUpdate.EndsAt) - CLng(objICDDMasterSeriesUpdate.StartFrom) + 1)) Then
                            If CheckDuplicateDDMasterSeriesOnAddingLeaves(objICDDMasterSeries, TopOrder, False) = True Then
                                If GetTopAssignedDDInstrumentNumbersToPrintLocationsByDDMSID(DDMasterSeriesID, TopOrder) = True Then
                                    ICDDMasterSeriesController.AddDDMasterSeries(objICDDMasterSeries, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                    ICDDInstrumentsController.DeleteUpdatedDDInstrumentSeriesByDDMasterSeriesID(objICDDMasterSeries.DDMasterSeriesID, Me.UserId.ToString, Me.UserInfo.Username.ToString, TopOrder)
                                    UIUtilities.ShowDialog(Me, "Add DD Master Series", "DD Master series updated successfully", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                                    clear()
                                Else
                                    UIUtilities.ShowDialog(Me, "Error", "Sorry! required number of series is assigned to printed location.", ICBO.IC.Dialogmessagetype.Failure)
                                    Exit Sub
                                End If

                            Else
                                UIUtilities.ShowDialog(Me, "Error", "Sorry! required number of leaves are printed or assigned to instructions.", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If

                        ElseIf CLng(txtNoOfLeaves.Text) > (CLng(CLng(objICDDMasterSeriesUpdate.EndsAt) - CLng(objICDDMasterSeriesUpdate.StartFrom) + 1)) Then
                            TopOrder = TopOrder * (-1)
                            Dim LastDDInstrumentNo As Integer = Nothing
                            If CheckDuplicateDDMasterSeriesOnUpdate(objICDDMasterSeries) = False Then
                                ICDDMasterSeriesController.AddDDMasterSeries(objICDDMasterSeries, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                LastDDInstrumentNo = (ICDDInstrumentsController.GetLastDDInstrumentOfDDMasterSeries(objICDDMasterSeriesUpdate.DDMasterSeriesID) + 1)
                                For i = LastDDInstrumentNo To CInt(txtEndsAt.Text)
                                    objICDDInstrument.DDMasterSeriesID = objICDDMasterSeries.DDMasterSeriesID

                                    objICDDInstrument.DDInstrumentNumber = CLng(i)
                                    objICDDInstrument.PreFix = objICDDMasterSeries.PreFix
                                    objICDDInstrument.IsUsed = Nothing
                                    objICDDInstrument.CreatedDate = Date.Now
                                    objICDDInstrument.CreatedBy = Me.UserId
                                    objICDDInstrument.CreationDate = Date.Now
                                    objICDDInstrument.Creater = Me.UserId
                                    ICDDInstrumentsController.AddDDInstrumentSeriesFromDDMasterSeries(objICDDInstrument, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                Next
                                UIUtilities.ShowDialog(Me, "Add DD Master Series", "DD Master series updated successfully", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                                clear()
                            Else

                                UIUtilities.ShowDialog(Me, "Error", "Sorry! Duplicate DD Master Series is not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Equal no of leaves", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    End If

                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub
    Public Shared Function GetTopAssignedDDInstrumentNumbersToPrintLocationsByDDMSID(ByVal DDMasterSeriesID As String, ByVal TopOrder As Integer) As Boolean
        Dim qryObjICDDInstruments As New ICDDInstrumentsQuery("qryObjICDDInstruments")
        Dim Result As Boolean = False
        Dim dt As New DataTable
        Dim dtFinal As New DataTable
        Dim drFinal As DataRow()
        qryObjICDDInstruments.Select(qryObjICDDInstruments.DDInstrumentNumber, qryObjICDDInstruments.OfficeCode)
        qryObjICDDInstruments.Where(qryObjICDDInstruments.DDMasterSeriesID = DDMasterSeriesID)
        qryObjICDDInstruments.OrderBy(qryObjICDDInstruments.DDInstrumentNumber.Descending)
        qryObjICDDInstruments.es.Top = TopOrder
        dt = qryObjICDDInstruments.LoadDataTable()
        If dt.Rows.Count > 0 Then
            drFinal = dt.Select("OfficeCode IS NULL")
            dtFinal = dt.Clone
            For Each drFinal2 As DataRow In drFinal
                dtFinal.ImportRow(drFinal2)
            Next
            If dtFinal.Rows.Count >= TopOrder Then
                Result = True
            End If

        End If
        Return Result
    End Function
    Public Shared Function IsUpDatedDDMasterSeriesInstrumentsAssignedToLocation(ByVal DDMasterSeriesID As String, ByVal DDSeriesStartFrom As Long, ByVal DDSeriesEndsAt As Long) As Boolean
        Dim objICPOInstrumentColl As New ICDDInstrumentsCollection
        Dim Result As Boolean = False
        objICPOInstrumentColl.Query.Where(objICPOInstrumentColl.Query.DDInstrumentNumber.Between(DDSeriesStartFrom, DDSeriesEndsAt) And objICPOInstrumentColl.Query.OfficeCode.IsNull)
        objICPOInstrumentColl.Query.OrderBy(objICPOInstrumentColl.Query.DDInstrumentNumber.Ascending)
        objICPOInstrumentColl.Query.Load()
        If objICPOInstrumentColl.Query.Load Then
            Result = True
        End If
        Return Result
    End Function
    Private Function CheckDuplicateDDMasterSeries(ByVal objICDDMasterSeries As ICDDMasterSeries) As Boolean
        Dim Result As Boolean = False

        Dim objICDDMSeriesColl As New ICDDMasterSeriesCollection
        Dim objICDDInstrumentColl As New ICDDInstrumentsCollection
        objICDDInstrumentColl.es.Connection.CommandTimeout = 3600
        objICDDMSeriesColl.es.Connection.CommandTimeout = 3600

        'objICDDMSeriesColl.LoadAll()
        objICDDMSeriesColl.Query.Where(objICDDMSeriesColl.Query.StartFrom = objICDDMasterSeries.StartFrom And objICDDMSeriesColl.Query.EndsAt = objICDDMasterSeries.EndsAt And objICDDMSeriesColl.Query.PreFix = objICDDMasterSeries.PreFix)
        objICDDMSeriesColl.Query.Load()
        If objICDDMSeriesColl.Query.Load Then
            Result = True
        End If
        If Result = False Then
            objICDDInstrumentColl.LoadAll()

            objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.DDInstrumentNumber >= objICDDMasterSeries.StartFrom And objICDDInstrumentColl.Query.DDInstrumentNumber <= objICDDMasterSeries.EndsAt And objICDDInstrumentColl.Query.PreFix = objICDDMasterSeries.PreFix)
            objICDDInstrumentColl.Query.Load()
            If objICDDInstrumentColl.Query.Load Then
                Result = True
            End If
        End If
        Return Result
    End Function
    Private Function CheckDuplicateDDMasterSeriesOnUpdate(ByVal objICDDMasterSeries As ICDDMasterSeries) As Boolean
        Dim Result As Boolean = False

        Dim objICDDMSeriesColl As New ICDDMasterSeriesCollection
        Dim objICDDInstrumentColl As New ICDDInstrumentsCollection
        objICDDInstrumentColl.es.Connection.CommandTimeout = 3600
        objICDDMSeriesColl.es.Connection.CommandTimeout = 3600

        'objICDDMSeriesColl.LoadAll()
        objICDDMSeriesColl.Query.Where(objICDDMSeriesColl.Query.StartFrom = objICDDMasterSeries.StartFrom And objICDDMSeriesColl.Query.EndsAt = objICDDMasterSeries.EndsAt And objICDDMSeriesColl.Query.PreFix = objICDDMasterSeries.PreFix And objICDDMSeriesColl.Query.DDMasterSeriesID <> objICDDMasterSeries.DDMasterSeriesID)
        objICDDMSeriesColl.Query.Load()
        If objICDDMSeriesColl.Query.Load Then
            Result = True
        End If
        If Result = False Then
            objICDDInstrumentColl.LoadAll()

            objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.DDInstrumentNumber >= objICDDMasterSeries.StartFrom And objICDDInstrumentColl.Query.DDInstrumentNumber <= objICDDMasterSeries.EndsAt And objICDDInstrumentColl.Query.PreFix = objICDDMasterSeries.PreFix And objICDDMSeriesColl.Query.DDMasterSeriesID <> objICDDMasterSeries.DDMasterSeriesID)
            objICDDInstrumentColl.Query.Load()
            If objICDDInstrumentColl.Query.Load Then
                Result = True
            End If
        End If
        Return Result
    End Function
    Private Function CheckDuplicateDDMasterSeriesOnAddingLeaves(ByVal objICMasterSeries As ICDDMasterSeries, ByVal TopOrder As Double, ByVal IsExtends As Boolean) As Boolean
        Dim Result As Boolean = False
        Dim qryObjICDDInstrument As New ICDDInstrumentsQuery
        Dim dt As New DataTable
        If IsExtends = False Then
            qryObjICDDInstrument.Select(qryObjICDDInstrument.DDInstrumentNumber.Count)
            qryObjICDDInstrument.Where(qryObjICDDInstrument.DDMasterSeriesID = objICMasterSeries.DDMasterSeriesID And qryObjICDDInstrument.IsUsed = False And qryObjICDDInstrument.IsAssigned.IsNull)
            qryObjICDDInstrument.OrderBy(qryObjICDDInstrument.DDInstrumentNumber.Descending)
            qryObjICDDInstrument.es.Top = TopOrder
            dt = qryObjICDDInstrument.LoadDataTable
          
            If CLng(dt.Rows(0)(0)) >= TopOrder Then
                Result = True
            End If
        End If
        Return Result
    End Function
    'Private Function CheckDuplicateDDMasterSeriesOnAddingLeaves(ByVal objICMasterSeries As ICDDMasterSeries) As Boolean
    '    Dim Result As Boolean = False

    '    Dim objICMSeriesColl As New ICDDMasterSeriesCollection
    '    Dim objICDDInstrumentColl As New ICDDInstrumentsCollection
    '    objICDDInstrumentColl.es.Connection.CommandTimeout = 3600

    '    objICDDInstrumentColl.LoadAll()

    '    objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.DDInstrumentNumber.In(objICMasterSeries.StartFrom, objICMasterSeries.EndsAt) And objICDDInstrumentColl.Query.PreFix = objICMasterSeries.PreFix)
    '    objICDDInstrumentColl.Query.Where(objICDDInstrumentColl.Query.DDMasterSeriesID <> objICMasterSeries.DDMasterSeriesID)
    '    objICDDInstrumentColl.Query.Load()
    '    If objICDDInstrumentColl.Query.Load Then
    '        Result = True
    '    End If

    '    Return Result
    'End Function

    Private Sub clear()
        txtStartsFrom.Text = ""
        txtEndsAt.Text = ""
        txtNoOfLeaves.Text = ""
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
 
    End Sub

    Protected Sub txtNoOfLeaves_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNoOfLeaves.TextChanged
      
        Try
            'TextNoOfLeavesChange = True
            'If TextSeriesStartFromChange = False Then
            If txtNoOfLeaves.Text.ToString() <> "" And txtNoOfLeaves.Text.ToString() <> "Enter No Of Leaves" Then
               

                If txtStartsFrom.Text.ToString <> "" And txtStartsFrom.Text.ToString <> "Enter Start Series" Then
                    Dim SeriesStartfrom As Long = Nothing
                    Dim LeavesOfSeries As Integer = Nothing
                    Dim SeriesEndAt As Long = Nothing
                    SeriesStartfrom = CLng(txtStartsFrom.Text)
                    LeavesOfSeries = CInt(txtNoOfLeaves.Text)
                    SeriesEndAt = (SeriesStartfrom + LeavesOfSeries) - 1
                    txtEndsAt.Text = SeriesEndAt
                Else

                End If

            Else
                txtEndsAt.Text = ""
            End If
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Protected Sub txtStartsFrom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStartsFrom.TextChanged
      
        Try

            'If TextNoOfLeavesChange = False Then
            If txtStartsFrom.Text.ToString() <> "" And txtStartsFrom.Text.ToString() <> "Enter Start Series" Then
                If txtNoOfLeaves.Text.ToString() <> "" And txtNoOfLeaves.Text.ToString() <> "Enter No Of Leaves" Then
                    Dim SeriesStartfrom As Long = Nothing
                    Dim LeavesOfSeries As Integer = Nothing
                    Dim SeriesEndAt As Long = Nothing
                    SeriesStartfrom = CLng(txtStartsFrom.Text)
                    LeavesOfSeries = CInt(txtNoOfLeaves.Text)
                    SeriesEndAt = (SeriesStartfrom + LeavesOfSeries) - 1
                    txtEndsAt.Text = SeriesEndAt
                Else
                    txtEndsAt.Text = ""
                End If
            Else
                txtEndsAt.Text = ""
            End If
            'End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
End Class
