﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewUserLimits.ascx.vb"
    Inherits="DesktopModules_User_Limits_ViewUserLimits" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .btn
    {
        margin-left: 0px;
    }
</style>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {

                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
                else {

                    if (cell.childNodes[j].type == "checkbox") {
                        cell.childNodes[j].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }

                }
            }
        }
    }


    function conBukDelete() {
        if (window.confirm("Are you sure you wish to remove Record(s)?") == true) {
            return true;
        }
        else {
            return false;
        }
    }  

</script>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2" style="width: 50%">
            <asp:Label ID="lblPageHeader" runat="server" Text="Approval Limits List" CssClass="headingblue"></asp:Label>
            <br />
            <%-- Note:&nbsp; Fields marked as
            <asp:Label ID="lblReqHeader" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.--%>
        </td>
        <td align="left" valign="top" colspan="2" style="width: 50%">
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2" style="width: 50%">
            &nbsp;
        </td>
        <td align="left" valign="top" colspan="2" style="width: 50%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnAddUserLimits" runat="server" Text="Add User Limits" CssClass="btn"
                Width="115px" />
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblUserType" runat="server" Text="User Type" CssClass="lbl"></asp:Label>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlUserType" runat="server" AutoPostBack="True" CssClass="dropdown">
                <asp:ListItem Value="0">-- All --</asp:ListItem>
                <asp:ListItem>Bank User</asp:ListItem>
                <asp:ListItem>Client User</asp:ListItem>
            </asp:DropDownList>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblRNF" runat="server" Font-Bold="False" Text="No Record Found" Visible="False"
                CssClass="headingblue"></asp:Label>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <telerik:RadGrid ID="gvUserLimit" runat="server" AllowPaging="True" Width="100%"
                AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                PageSize="100" CssClass="RadGrid">
                <ClientSettings>
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                </ClientSettings>
                <AlternatingItemStyle CssClass="rgAltRow" />
                <MasterTableView DataKeyNames="LimitsForApprovalID" TableLayout="Fixed">
                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridTemplateColumn>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select" TextAlign="Right" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text="" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="UserName" HeaderText="User Name" SortExpression="UserName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="UserType" HeaderText="User Type" SortExpression="UserType">
                        </telerik:GridBoundColumn>
                       <%-- <telerik:GridBoundColumn DataField="CompanyName" HeaderText="Company Name" SortExpression="CompanyName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="Account No." SortExpression="AccountNumber">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LimitsForPrimaryApproval" HeaderText="Client Primary Limit"
                            SortExpression="LimitsForPrimaryApproval">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LimitsForSecondaryApproval" HeaderText="Client Secondary Limit"
                            SortExpression="LimitsForSecondaryApproval">
                        </telerik:GridBoundColumn>--%>

                         <telerik:GridBoundColumn DataField="BankPrimaryApprovalLimit" HeaderText="Bank Primary Limit"
                            SortExpression="BankPrimaryApprovalLimit" HtmlEncode="false" DataFormatString="{0:N2}">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BankSecondaryApprovalLimit" HeaderText="Bank Secondary Limit"
                            SortExpression="BankSecondaryApprovalLimit" HtmlEncode="false" DataFormatString="{0:N2}">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn HeaderText="Edit">
                            <ItemTemplate>
                                <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveUserLimits", "&mid=" & Me.ModuleId & "&UserCode="& Eval("UserID") & "&LimitsForApprovalID="& Eval("LimitsForApprovalID"))%>'
                                    ToolTip="Edit">Edit</asp:HyperLink>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                    <PagerStyle AlwaysVisible="True" />
                </MasterTableView>
                <ItemStyle CssClass="rgRow" />
                <PagerStyle AlwaysVisible="True" />
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
            <%--<telerik:RadGrid ID="gvUserLimit" runat="server" AllowPaging="True" AllowSorting="True"
                AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="10">
                <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView DataKeyNames="LimitsForApprovalID">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridTemplateColumn>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select"
                                    TextAlign="Right" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="UserName" HeaderText="User Name" SortExpression="UserName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CompanyName" HeaderText="Company Name" SortExpression="CompanyName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="Account No." SortExpression="AccountNumber">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LimitsForPrimaryApproval" HeaderText="Primary Approval Limit"
                            SortExpression="LimitsForPrimaryApproval">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LimitsForSecondaryApproval" HeaderText="Secondary Approval Limit"
                            SortExpression="LimitsForSecondaryApproval">
                        </telerik:GridBoundColumn>
                          <telerik:GridTemplateColumn HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveUserLimits", "&mid=" & Me.ModuleId & "&UserCode="& Eval("UserID"))%>'
                                                ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
          
                    </Columns>
                   <EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />

<FilterMenu EnableImageSprites="False"></FilterMenu>
            </telerik:RadGrid>--%>
            <%--<telerik:RadGrid ID="gvUserLimit" runat="server" AllowPaging="True" AllowSorting="True"
                AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="10">
                <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView DataKeyNames="LimitsForApprovalID">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridTemplateColumn>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select"
                                    TextAlign="Right" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="UserName" HeaderText="User Name" SortExpression="UserName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CompanyName" HeaderText="Company Name" SortExpression="CompanyName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="Account No." SortExpression="AccountNumber">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LimitsForPrimaryApproval" HeaderText="Primary Approval Limit"
                            SortExpression="LimitsForPrimaryApproval">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="LimitsForSecondaryApproval" HeaderText="Secondary Approval Limit"
                            SortExpression="LimitsForSecondaryApproval">
                        </telerik:GridBoundColumn>
                          <telerik:GridTemplateColumn HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveUserLimits", "&mid=" & Me.ModuleId & "&UserCode="& Eval("UserID"))%>'
                                                ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
          
                    </Columns>
                 <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
            </telerik:RadGrid>--%>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnDeleteLimits" runat="server" Text="Delete Limits" OnClientClick="javascript: return conBukDelete();"
                CssClass="btn" Width="97px" />
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
        &nbsp;</tr>
</table>
<table width="100%">
    <tr align="center" valign="top" style="width: 100%">
        <td align="center" valign="top">
            &nbsp; &nbsp;
        </td>
    </tr>
</table>
