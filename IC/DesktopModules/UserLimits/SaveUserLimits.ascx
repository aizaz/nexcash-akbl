﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveUserLimits.ascx.vb"
    Inherits="DesktopModules_User_Limits_SaveUserLimits" %>
<style type="text/css">
    .btn
    {
        margin-left: 0px;
    }
    
    </style>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {

                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
                else {

                    if (cell.childNodes[j].type == "checkbox") {
                        cell.childNodes[j].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }

                }
            }
        }
    }
</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:NumericTextBoxSetting BehaviorID="rePrimLimit" Validation-IsRequired="false"
        EmptyMessage="" DecimalDigits="2" AllowRounding="false" ErrorMessage="Invalid Limit">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBankPrimaryApprovalLimit" />
        </TargetControls>
    </telerik:NumericTextBoxSetting>
    <telerik:NumericTextBoxSetting BehaviorID="reSecondaryLimit"
        Validation-IsRequired="false"  DecimalDigits="2" AllowRounding="false"
        ErrorMessage="Invalid Limit">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBankSecondaryApprovalLimit" />
        </TargetControls>
    </telerik:NumericTextBoxSetting>
    <telerik:NumericTextBoxSetting BehaviorID="reClientPrimLimit" Validation-IsRequired="false"
        EmptyMessage="" DecimalDigits="2" AllowRounding="false" ErrorMessage="Invalid Limit">
        <TargetControls>
            <telerik:TargetInput ControlID="PrimaryRightstxtbox" />
        </TargetControls>
    </telerik:NumericTextBoxSetting>
    <telerik:NumericTextBoxSetting BehaviorID="reClientSecondaryLimit"
        Validation-IsRequired="false"  DecimalDigits="2" AllowRounding="false"
        ErrorMessage="Invalid Limit">
        <TargetControls>
            <telerik:TargetInput ControlID="SecondaryRightstxtbox" />
        </TargetControls>
    </telerik:NumericTextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2" style="width: 50%">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
            <br />
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" colspan="2" style="width: 50%">
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2" style="width: 50%">
            &nbsp;
        </td>
        <td align="left" valign="top" colspan="2" style="width: 50%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblUserType" runat="server" Text="Select User Type" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqUserType" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqGroup" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlUserType" runat="server" AutoPostBack="True" CssClass="dropdown">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem>Bank User</asp:ListItem>
                <asp:ListItem>Client User</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True" CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlUserType" runat="server" ControlToValidate="ddlUserType"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select User Type" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                <asp:RequiredFieldValidator ID="rfvddlGroup" runat="server" ControlToValidate="ddlGroup"
                    CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Group" SetFocusOnError="True"
                    ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblUser" runat="server" Text="Select User" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqUser" runat="server" Text="*" ForeColor="Red"></asp:Label>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                <asp:Label ID="lblSignature" runat="server" CssClass="lbl" Text="Select Signature Image"></asp:Label>
                <asp:Label ID="lblReqSignature" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlUser" runat="server" AutoPostBack="True" CssClass="dropdown">
            </asp:DropDownList>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                <asp:FileUpload ID="fupSignature" runat="server" Style="margin-bottom: 0px" />
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlUser" runat="server" ControlToValidate="ddlUser"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select User" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                <asp:Label ID="lblLogo" runat="server" CssClass="lbl"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" width="100%" colspan="4">
            <table id="tblApprovalLimit" runat="server" width="100%" style="display:none">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 22%">
                        <asp:Label ID="lblPrimaryApprovalLimit" runat="server" Text="Bank Primary Approval Limit"
                            CssClass="lbl"></asp:Label><%--</asp:Label><asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>--%>
                    </td>
                    <td align="left" valign="top" style="width: 23%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblSecondaryApprovalLimit" runat="server" Text="Bank Secondary Approval Limit"
                            CssClass="lbl"></asp:Label><%--<asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>--%>
                    </td>
                    <td align="left" valign="top" style="width: 20%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 22%">
                        <asp:TextBox ID="txtBankPrimaryApprovalLimit" runat="server" CssClass="txt" MaxLength="13"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 23%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtBankSecondaryApprovalLimit" runat="server" CssClass="txt" MaxLength="13"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 20%">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnSave" runat="server" CssClass="btn" Text="Save" Width="74px" />
            <asp:Button ID="btnCancel" runat="server" CssClass="btnCancel" Text="Cancel" Width="75px"
                CausesValidation="False" />
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;<td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;<td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;<td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
        &nbsp;
    </tr>
</table>
<table width="100%">
    <tr align="center" valign="top" style="width: 100%">
        <td align="center" valign="top">
            &nbsp; &nbsp;
        </td>
    </tr>
</table>
