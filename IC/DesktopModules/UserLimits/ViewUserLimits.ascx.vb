﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Security.Permissions
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_User_Limits_ViewUserLimits
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable



    Protected Sub DesktopModules_User_Limits_ViewUserLimits_Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnAddUserLimits.Visible = CBool(htRights("Add"))
                'LoadddlGroup()
                'LoadddlCompany()
                ddlUserType.ClearSelection()
                LoadgvUserLimit(True)

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "User Limits Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnAddUserLimits_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddUserLimits.Click
        Page.Validate()

        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveUserLimits", "&mid=" & Me.ModuleId & "&UserCode=0" & "&LimitsForApprovalID=0"))
        End If
    End Sub

    Private Sub LoadddlGroup()
        Try
            'Dim lit As New ListItem
            'lit.Value = "0"
            'lit.Text = "-- All --"
            'ddlGroup.Items.Clear()
            'ddlGroup.Items.Add(lit)
            'ddlGroup.AppendDataBoundItems = True
            'ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            'ddlGroup.DataTextField = "GroupName"
            'ddlGroup.DataValueField = "GroupCode"
            'ddlGroup.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlCompany()
        Try
            'Dim lit As New ListItem
            'lit.Value = "0"
            'lit.Text = "-- All --"
            'ddlCompany.Items.Clear()
            'ddlCompany.Items.Add(lit)
            'ddlCompany.AppendDataBoundItems = True
            'If ddlGroup.SelectedValue.ToString <> "0" Then
            '    ddlCompany.DataSource = ICCompanyController.GetCompanyActiveAndApproveByGroupCode(ddlGroup.SelectedValue.ToString)
            '    ddlCompany.DataTextField = "CompanyName"
            '    ddlCompany.DataValueField = "CompanyCode"
            '    ddlCompany.DataBind()
            'Else
            '    ddlCompany.DataSource = ICCompanyController.GetCompanyActiveAndApprove
            '    ddlCompany.DataTextField = "CompanyName"
            '    ddlCompany.DataValueField = "CompanyCode"
            '    ddlCompany.DataBind()
            'End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadgvUserLimit(ByVal DoDataBind As Boolean)
        Try
            Dim UserType As String
            'If ddlGroup.SelectedValue.ToString <> "0" Then
            '    GroupCode = ddlGroup.SelectedValue.ToString
            'Else
            '    GroupCode = ""
            'End If
            If ddlUserType.SelectedValue.ToString <> "0" Then
                UserType = ddlUserType.SelectedValue.ToString
            Else
                UserType = ""
            End If
            'If ddlCompany.SelectedValue.ToString <> "0" Then
            '    CompanyCode = ddlCompany.SelectedValue.ToString
            'Else
            '    CompanyCode = ""
            'End If

            ICUserLimitsController.GetAllUSerLimitsDefinedAgainstUSerForRadGrid(Me.gvUserLimit.CurrentPageIndex + 1, Me.gvUserLimit.PageSize, DoDataBind, Me.gvUserLimit, "", "", UserType)

            If gvUserLimit.Items.Count > 0 Then
                btnDeleteLimits.Visible = CBool(htRights("Delete"))
                gvUserLimit.Visible = True
                lblRNF.Visible = False
            Else
                gvUserLimit.Visible = False
                btnDeleteLimits.Visible = False
                lblRNF.Visible = True

            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    'Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
    '    Try
    '        LoadddlCompany()
    '        ddlUserType.ClearSelection()
    '        LoadgvUserLimit(True)
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub


    Private Function CheckgvUserLimitsForProcessAll() As Boolean
        Try

            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVUserLimits In gvUserLimit.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVUserLimits.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub savebtn1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteLimits.Click
        Page.Validate()

        If Page.IsValid Then
            Try
                Dim rowGVICRoles As GridDataItem
                Dim chkSelect As CheckBox
                Dim ApprovalLimitID As String = ""
                Dim PassToDeleteCount As Integer = 0
                Dim FailToDeleteCount As Integer = 0
                Dim objICLimitForApp As ICLimitForApproval
                Dim StrAction As String = Nothing
                Dim objICUser As ICUser
                If CheckgvUserLimitsForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Limits For Approval", "Please select atleast one limit to delete.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVICRoles In gvUserLimit.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVICRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        ApprovalLimitID = Nothing
                        ApprovalLimitID = rowGVICRoles.GetDataKeyValue("LimitsForApprovalID").ToString
                        objICLimitForApp = New ICLimitForApproval
                        objICLimitForApp.es.Connection.CommandTimeout = 3600
                        If objICLimitForApp.LoadByPrimaryKey(ApprovalLimitID.ToString()) Then
                            Try
                                StrAction = Nothing
                                objICUser = New ICUser
                                objICUser.LoadByPrimaryKey(objICLimitForApp.UserID.ToString)
                                StrAction = "Approval limit for user [ " & objICUser.UserID & " ] [ " & objICUser.UserName & " ]"

                                If Not objICLimitForApp.BankPrimaryApprovalLimit Is Nothing And Not objICLimitForApp.BankSecondaryApprovalLimit Is Nothing Then
                                    StrAction += " [ Bank Primary ] of amount [ " & objICLimitForApp.BankPrimaryApprovalLimit & " ] and [ Bank Secondary ]"
                                    StrAction += " of amount [ " & objICLimitForApp.BankSecondaryApprovalLimit & " ]"
                                ElseIf Not objICLimitForApp.BankPrimaryApprovalLimit Is Nothing And objICLimitForApp.BankSecondaryApprovalLimit Is Nothing Then
                                    StrAction += " [ Bank Primary ] of amount [ " & objICLimitForApp.BankPrimaryApprovalLimit & " ]"
                                ElseIf objICLimitForApp.BankPrimaryApprovalLimit Is Nothing And Not objICLimitForApp.BankSecondaryApprovalLimit Is Nothing Then
                                    StrAction += " [ Bank Secondary ]"
                                    StrAction += " of amount [ " & objICLimitForApp.BankSecondaryApprovalLimit & " ]"
                                End If
                                StrAction += " deleted. Action was taken by user [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]"

                                ICUserLimitsController.DeleteICUserLimt(objICLimitForApp, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction)
                                PassToDeleteCount = PassToDeleteCount + 1
                            Catch ex As Exception

                                FailToDeleteCount = FailToDeleteCount + 1
                                Continue For

                            End Try
                        End If
                    End If


                Next

                If Not PassToDeleteCount = 0 And Not FailToDeleteCount = 0 Then
                    LoadgvUserLimit(True)
                    UIUtilities.ShowDialog(Me, "Limits For Approval", "[ " & PassToDeleteCount.ToString & " ] Approval limit(s) deleted successfuly.<br /> [ " & FailToDeleteCount.ToString & " ] Approval limit(s) can not be deleted due to following reason: <br /> 1. Deleted associated record(s) first", ICBO.IC.Dialogmessagetype.Failure)

                    Exit Sub
                ElseIf Not PassToDeleteCount = 0 And FailToDeleteCount = 0 Then
                    LoadgvUserLimit(True)
                    UIUtilities.ShowDialog(Me, "Limits For Approval", PassToDeleteCount.ToString & " Approval limit(s) deleted successfuly.", ICBO.IC.Dialogmessagetype.Success)

                    Exit Sub
                ElseIf Not FailToDeleteCount = 0 And PassToDeleteCount = 0 Then
                    LoadgvUserLimit(True)
                    UIUtilities.ShowDialog(Me, "Limits For Approval", "[ " & FailToDeleteCount.ToString & " ] Approval limit(s) can not be deleted due to following reasons: <br /> 1. Deleted associated record(s) first", ICBO.IC.Dialogmessagetype.Failure)

                    Exit Sub
                End If

            Catch ex As Exception
                If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                    ProcessModuleLoadException(Me, ex, False)
                    UIUtilities.ShowDialog(Me, "Error", "Role can not be deleted. Delete associated record(s) first", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                Else
                    ProcessModuleLoadException(Me, ex, False)
                    UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                End If
            End Try
        End If
    End Sub

    Protected Sub gvUserLimit_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvUserLimit.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvUserLimit.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvUserLimit_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvUserLimit.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvUserLimit.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub gvUserLimit_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvUserLimit.NeedDataSource
        Try
            LoadgvUserLimit(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlUserType_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlUserType.SelectedIndexChanged
        Try
            LoadgvUserLimit(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    'Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
    '    Try
    '        LoadgvUserLimit(True)
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
End Class
