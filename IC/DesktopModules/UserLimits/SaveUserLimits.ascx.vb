﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Security.Permissions
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_User_Limits_SaveUserLimits
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private UserCode As String
    Private LimitsForApprovalID As String
    Private objICLimitForApprovalOnUpdate As New ICLimitForApproval
    Private htRights As Hashtable


    Protected Sub DesktopModules_User_Limits_SaveUserLimits_Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            UserCode = Request.QueryString("UserCode").ToString
            LimitsForApprovalID = Request.QueryString("LimitsForApprovalID").ToString
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlGroup()
                LoadddlUser(ddlUserType.SelectedValue.ToString)
                'pnlUserLimits.Style.Add("display", "none")

                If UserCode.ToString = "0" Then
                    lblPageHeader.Text = "Add User Approval Limit"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                    'pnlUserLimits.Style.Add("display", "none")

                    HideAllDropDowns()
                Else
                    ClearBankLimitTextboxes()
                    Dim objICUser As New ICUser
                    Dim objICGroup As New ICGroup
                    Dim objICLimitsForAppGroup As New ICLimitForApproval


                    objICLimitsForAppGroup.LoadByPrimaryKey(LimitsForApprovalID)
                    lblPageHeader.Text = "Update User Approval Limit"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    objICUser.LoadByPrimaryKey(UserCode.ToString)
                    If objICUser.UserType = "Client User" Then
                        'objICLimitForApprovalOnUpdate = ICUserLimitsController.GetSingleObjectLimitsForApprovalByUserID(objICUser.UserID.ToString)
                        ddlUserType.SelectedValue = "Client User"
                        objICGroup = objICUser.UpToICOfficeByOfficeCode.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode
                        LoadddlGroup()
                        ddlGroup.SelectedValue = objICGroup.GroupCode
                        LoadddlUser(ddlUserType.SelectedValue.ToString)
                        ddlUser.SelectedValue = objICUser.UserID.ToString
                        ddlGroup.Enabled = False
                        ddlUser.Enabled = False
                        ddlUserType.Enabled = False
                        LoadgvUserLimit()
                        'CheckMarkGridOnUpDate(UserCode.ToString)
                        tblApprovalLimit.Style.Add("display", "none")
                        If Not objICLimitsForAppGroup.UserSignatureImage Is Nothing Then
                            lblLogo.Text = ICUtilities.showpicturefromdb("Approval Limits User", objICLimitsForAppGroup.LimitsForApprovalID, 100, 100)
                        End If
                    ElseIf objICUser.UserType = "Bank User" Then
                        lblGroup.Visible = False
                        lblReqGroup.Visible = False
                        ddlGroup.Visible = False
                        ddlGroup.DataSource = Nothing
                        rfvddlGroup.Enabled = False
                        'objICLimitForApprovalOnUpdate = ICUserLimitsController.GetSingleObjectLimitsForApprovalByUserID(objICUser.UserID.ToString)
                        ddlUserType.SelectedValue = "Bank User"
                        lblUser.Visible = True
                        lblReqUser.Visible = True
                        ddlUser.Visible = True
                        ddlUser.Enabled = True
                        LoadddlUser("Bank User")
                        ddlUser.SelectedValue = objICUser.UserID.ToString
                        ddlUser.Enabled = False
                        ddlUserType.Enabled = False
                        tblApprovalLimit.Style.Remove("display")
                        'GetAllAccountsTaggedWithPrincipalBankUsers()
                        'CheckMarkGridOnUpDate(UserCode.ToString)


                        If Not objICLimitsForAppGroup.BankPrimaryApprovalLimit Is Nothing Then
                            txtBankPrimaryApprovalLimit.Text = objICLimitsForAppGroup.BankPrimaryApprovalLimit
                        Else
                            txtBankPrimaryApprovalLimit.Text = ""
                        End If

                        If Not objICLimitsForAppGroup.BankSecondaryApprovalLimit Is Nothing Then
                            txtBankSecondaryApprovalLimit.Text = objICLimitsForAppGroup.BankSecondaryApprovalLimit
                        Else
                            txtBankSecondaryApprovalLimit.Text = ""
                        End If

                        If Not objICLimitsForAppGroup.UserSignatureImage Is Nothing Then
                            lblLogo.Text = ICUtilities.showpicturefromdb("Approval Limits User", objICLimitsForAppGroup.LimitsForApprovalID, 100, 100)
                        End If
                    End If
                End If



            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub ClearBankLimitTextboxes()

        txtBankPrimaryApprovalLimit.Text = ""
        txtBankSecondaryApprovalLimit.Text = ""

    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "User Limits Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckFileType(ByVal fuImage As FileUpload) As Boolean
        Dim strExt As String = System.IO.Path.GetExtension(fuImage.PostedFile.FileName)
        Dim rslt As Boolean = False
        'img.FromStream(fuImage.PostedFile.InputStream)
        If strExt.ToString().ToLower = ".jpeg" Then
            rslt = True
        End If
        If strExt.ToString().ToLower = ".jpg" Then
            rslt = True
        End If
        If strExt.ToString().ToLower = ".png" Then
            rslt = True
        End If
        If strExt.ToString().ToLower = ".gif" Then
            rslt = True
        End If
        If strExt.ToString().ToLower = ".bmp" Then
            rslt = True
        End If
        If strExt.ToString().ToLower = ".dib" Then
            rslt = True
        End If
        If strExt.ToString().ToLower = ".jpe" Then
            rslt = True
        End If
        If strExt.ToString().ToLower = ".jfif" Then
            rslt = True
        End If
        If strExt.ToString().ToLower = ".tif" Then
            rslt = True
        End If
        If strExt.ToString().ToLower = ".tiff" Then
            rslt = True
        End If
        Return rslt
    End Function
    Private Function CheckUserApprovalLimitIsValid(ByVal TextBoxForVerify As TextBox) As String
        Dim ResultString As String = "OK"
        Dim Result As Double
        If TextBoxForVerify.Text.ToString() <> "" Then

            If TextBoxForVerify.Text.Contains(".") Then
                Dim str As String()
                Dim strBDeci, strADeci As String
                Dim TotAmt As String = ""
                str = TextBoxForVerify.Text.Split(".")
                strBDeci = str(0).ToString()
                strADeci = str(1).ToString()
                TotAmt = strBDeci & strADeci
                If TotAmt.Length > 13 Then
                    ResultString = ""
                    ResultString = "Invalid approval amount."
                    Return Result
                    Exit Function
                End If
                If strBDeci.Length > 11 Then
                    ResultString = ""
                    ResultString = "Invalid approval amount."
                    Return Result
                    Exit Function
                End If
            Else
                If TextBoxForVerify.Text.Length > 11 Then
                    ResultString = ""
                    ResultString = "Invalid approval amount."
                    Return Result
                    Exit Function
                End If
            End If

            Try
                If Double.TryParse(TextBoxForVerify.Text.Trim(), result) = False Then
                    ResultString = ""
                    ResultString = "Invalid approval amount."
                    Return Result
                    Exit Function
                End If
            Catch ex As Exception
                ResultString = ""
                ResultString = "Invalid approval amount."
                Return Result
                Exit Function
            End Try


            If CDbl(TextBoxForVerify.Text.ToString()) <= 0 Then
                ResultString = ""
                ResultString = "Invalid approval amount."
                Return Result
                Exit Function
            End If
        End If
        Return ResultString
    End Function
    Private Function ValidateApprovalLimits() As Boolean
        Dim Result As Boolean = True
        If txtBankPrimaryApprovalLimit.Text.ToString <> "" And Not txtBankPrimaryApprovalLimit.Text Is Nothing Then
            If CheckUserApprovalLimitIsValid(txtBankPrimaryApprovalLimit) <> "OK" Then
                Result = False
                Return Result
                Exit Function
            End If
        End If
        If txtBankSecondaryApprovalLimit.Text.ToString <> "" And Not txtBankSecondaryApprovalLimit.Text Is Nothing Then
            If CheckUserApprovalLimitIsValid(txtBankSecondaryApprovalLimit) <> "OK" Then
                Result = False
                Return Result
                Exit Function
            End If
        End If
       
        Return Result
    End Function
    Protected Sub savebtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try

                Dim PassToSaveCount As Integer = 0
                Dim FailToSaveCount As Integer = 0
                Dim RecordCount As Integer = 0
                Dim objICLimitsForApp As New ICLimitForApproval


                'Verify entered values according to user type 

                If ddlUserType.SelectedValue.ToString() = "Bank User" Then
                    If txtBankPrimaryApprovalLimit.Text = "" And txtBankSecondaryApprovalLimit.Text = "" Then
                        UIUtilities.ShowDialog(Me, "Save User Limits", "Please select atleast one(1) limit for user.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    If ValidateApprovalLimits() = False Then
                        UIUtilities.ShowDialog(Me, "Save User Limits", "Invalid approval limit entered.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If

                '' Verify Signature image on new record or update
                If UserCode = "0" Then
                    '' New Entry
                    If fupSignature.HasFile = True Then
                        If CheckFileType(fupSignature) = True Then
                            Dim imageBytes(fupSignature.PostedFile.InputStream.Length) As Byte
                            fupSignature.PostedFile.InputStream.Read(imageBytes, 0, imageBytes.Length)
                            objICLimitForApprovalOnUpdate.UserSignatureImage = imageBytes
                            objICLimitForApprovalOnUpdate.FileSize = fupSignature.PostedFile.ContentLength
                            objICLimitForApprovalOnUpdate.FileType = System.IO.Path.GetExtension(fupSignature.FileName)
                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Invalid image format.", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Please Select  a file for signature.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else
                    '' on update
                    If fupSignature.HasFile = True Then
                        If CheckFileType(fupSignature) = True Then
                            Dim imageBytes(fupSignature.PostedFile.InputStream.Length) As Byte
                            fupSignature.PostedFile.InputStream.Read(imageBytes, 0, imageBytes.Length)
                            objICLimitForApprovalOnUpdate.UserSignatureImage = imageBytes
                            objICLimitForApprovalOnUpdate.FileSize = fupSignature.PostedFile.ContentLength
                            objICLimitForApprovalOnUpdate.FileType = System.IO.Path.GetExtension(fupSignature.FileName)
                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Invalid image format.", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    Else
                        objICLimitsForApp.LoadByPrimaryKey(LimitsForApprovalID)
                        objICLimitForApprovalOnUpdate = objICLimitsForApp
                    End If
                End If


                ''Add new entries



                If UserCode.ToString = "0" Then
                    AddUserLimits(UserCode, PassToSaveCount, FailToSaveCount, False)
                    If Not PassToSaveCount = 0 And Not FailToSaveCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Limits For Approval", "[ " & PassToSaveCount.ToString & " ] Approval limit(s) saved successfully.<br /> [ " & FailToSaveCount.ToString & " ] Approval limit(s) can not be saved due to following reason: <br /> 1. Limit is entered and not check the check box for primary or secondary approver and vice versa<br /> 2. Entered limit is duplicate<br /> 3. Delete associated records first", ICBO.IC.Dialogmessagetype.Failure, NavigateURL)
                        Clear()
                        Exit Sub
                    ElseIf Not PassToSaveCount = 0 And FailToSaveCount = 0 Then

                        UIUtilities.ShowDialog(Me, "Limits For Approval", PassToSaveCount.ToString & " Approval limit(s) saved successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL)

                        Exit Sub
                    ElseIf Not FailToSaveCount = 0 And PassToSaveCount = 0 Then

                        UIUtilities.ShowDialog(Me, "Limits For Approval", "[ " & FailToSaveCount.ToString & " ] Approval limit(s) can not be saved due to following reasons:<br /> 1. Limit is entered and not check the check box for primary or secondary approver and vice versa<br /> 2. Entered limit is duplicate<br /> 3. Delete associated records first", ICBO.IC.Dialogmessagetype.Failure, NavigateURL)

                        Exit Sub
                    End If

                Else
                    AddUserLimits(UserCode, PassToSaveCount, FailToSaveCount, True)
                    If Not PassToSaveCount = 0 And Not FailToSaveCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Limits For Approval", "[ " & PassToSaveCount.ToString & " ] Approval limit(s) updated successfully.<br /> [ " & FailToSaveCount.ToString & " ] Approval limit(s) can not be updated due to following reason: <br /> 1. Limit is entered and not check the check box for primary or secondary approver and vice versa<br /> 2. Entered limit is duplicate<br /> 3. Delete associated records first", ICBO.IC.Dialogmessagetype.Failure, NavigateURL)

                        Exit Sub
                    ElseIf Not PassToSaveCount = 0 And FailToSaveCount = 0 Then

                        UIUtilities.ShowDialog(Me, "Limits For Approval", PassToSaveCount.ToString & " Approval limit(s) updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL)

                        Exit Sub
                    ElseIf Not FailToSaveCount = 0 And PassToSaveCount = 0 Then

                        UIUtilities.ShowDialog(Me, "Limits For Approval", "[ " & FailToSaveCount.ToString & " ] Approval limit(s) can not be updated due to following reasons:<br /> 1. Limit is entered and not check the check box for primary or secondary approver and vice versa<br /> 2. Entered limit is duplicate<br /> 3. Delete associated records first", ICBO.IC.Dialogmessagetype.Failure, NavigateURL)

                        Exit Sub
                    End If
                End If

                ClearBankLimitTextboxes()

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Sub AddUserLimits(ByVal UserID As String, ByRef PassToAddCount As Integer, ByRef FailToAddCount As Integer, ByVal IsUpdate As Boolean)
        Dim objICApprovalLimits As New ICLimitForApproval
        Dim objICUser As ICUser
        Dim objICLimitsForAppCount As Integer = 0
        Dim ActionStr As String = Nothing
        If ddlUserType.SelectedValue.ToString = "Client User" Then
            objICUser = New ICUser
            objICUser.LoadByPrimaryKey(ddlUser.SelectedValue.ToString)


            objICApprovalLimits.BankPrimaryApprovalLimit = Nothing



            objICApprovalLimits.BankSecondaryApprovalLimit = Nothing


            objICApprovalLimits.LimitsForPrimaryApproval = Nothing
            objICApprovalLimits.IsPrimaryApprover = False
            objICApprovalLimits.LimitsForSecondaryApproval = Nothing
            objICApprovalLimits.IsSecondaryApprover = False
            objICApprovalLimits.UserID = ddlUser.SelectedValue.ToString

            objICApprovalLimits.UserSignatureImage = objICLimitForApprovalOnUpdate.UserSignatureImage
            objICApprovalLimits.FileSize = objICLimitForApprovalOnUpdate.FileSize
            objICApprovalLimits.FileType = objICLimitForApprovalOnUpdate.FileType


            objICApprovalLimits.CreatedBy = Me.UserId
            objICApprovalLimits.CreatedDate = Date.Now
            ActionStr = Nothing
            ActionStr = "Signatures for user [ " & objICUser.UserID & " ] [ " & objICUser.UserName & " ]"
            If IsUpdate = False Then
                ActionStr += " added."
            Else
                ActionStr += " updated."
            End If
            ActionStr += " Action was taken by user [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
            If UserID = 0 Then
                If CheckDuplicatLimitLimitForUser(objICApprovalLimits) = False Then
                    Try
                        ICUserLimitsController.AddICLimitsForApproval(objICApprovalLimits, Me.UserId.ToString, Me.UserInfo.Username.ToString, IsUpdate, ActionStr)
                        PassToAddCount = PassToAddCount + 1
                    Catch ex As Exception
                        FailToAddCount = FailToAddCount + 1
                        Throw New Exception(ex.Message.ToString)
                    End Try
                Else
                    FailToAddCount = FailToAddCount + 1
                    Throw New Exception("Duplicate approval limits are not allowed")
                End If
            Else
                Try
                    objICApprovalLimits.LimitsForApprovalID = LimitsForApprovalID
                    ICUserLimitsController.AddICLimitsForApproval(objICApprovalLimits, Me.UserId.ToString, Me.UserInfo.Username.ToString, IsUpdate, ActionStr)
                    PassToAddCount = PassToAddCount + 1
                Catch ex As Exception
                    FailToAddCount = FailToAddCount + 1
                    Throw New Exception(ex.Message.ToString)
                End Try
            End If
        ElseIf ddlUserType.SelectedValue.ToString = "Bank User" Then


            objICUser = New ICUser
            objICUser.LoadByPrimaryKey(ddlUser.SelectedValue.ToString)


            objICApprovalLimits.LimitsForSecondaryApproval = Nothing
            objICApprovalLimits.IsSecondaryApprover = False
            objICApprovalLimits.LimitsForPrimaryApproval = Nothing
            objICApprovalLimits.IsPrimaryApprover = False


            If txtBankPrimaryApprovalLimit.Text <> "" And Not txtBankPrimaryApprovalLimit.Text Is Nothing Then
                If CheckUserApprovalLimitIsValid(txtBankPrimaryApprovalLimit) = "OK" Then
                    objICApprovalLimits.BankPrimaryApprovalLimit = CDbl(txtBankPrimaryApprovalLimit.Text)
                Else
                    FailToAddCount = FailToAddCount + 1
                    Throw New Exception("Invalid Bank Primary Approval Limit")
                End If
            Else
                objICApprovalLimits.BankPrimaryApprovalLimit = Nothing
            End If

            If txtBankSecondaryApprovalLimit.Text <> "" And Not txtBankSecondaryApprovalLimit.Text Is Nothing Then
                If CheckUserApprovalLimitIsValid(txtBankSecondaryApprovalLimit) = "OK" Then
                    objICApprovalLimits.BankSecondaryApprovalLimit = CDbl(txtBankSecondaryApprovalLimit.Text)
                Else
                    FailToAddCount = FailToAddCount + 1
                    Throw New Exception("Invalid Bank Secondary Approval Limit")
                End If
            Else
                objICApprovalLimits.BankSecondaryApprovalLimit = Nothing
            End If
            objICApprovalLimits.UserID = ddlUser.SelectedValue.ToString

            objICApprovalLimits.UserSignatureImage = objICLimitForApprovalOnUpdate.UserSignatureImage
            objICApprovalLimits.FileSize = objICLimitForApprovalOnUpdate.FileSize
            objICApprovalLimits.FileType = objICLimitForApprovalOnUpdate.FileType


            objICApprovalLimits.CreatedBy = Me.UserId
            objICApprovalLimits.CreatedDate = Date.Now
            ActionStr = Nothing
            ActionStr = "Bank Approval limit for user [ " & objICUser.UserID & " ] [ " & objICUser.UserName & " ]"
            If IsUpdate = False Then
                ActionStr += " added."
            Else
                ActionStr += " updated."
            End If
            ActionStr += " Action was taken by user [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]"
            If UserID = 0 Then
                If CheckDuplicatLimitLimitForUser(objICApprovalLimits) = False Then
                    Try
                        ICUserLimitsController.AddICLimitsForApproval(objICApprovalLimits, Me.UserId.ToString, Me.UserInfo.Username.ToString, IsUpdate, ActionStr)
                        PassToAddCount = PassToAddCount + 1
                    Catch ex As Exception
                        FailToAddCount = FailToAddCount + 1
                        Throw New Exception(ex.Message.ToString)
                    End Try
                Else
                    FailToAddCount = FailToAddCount + 1
                    Throw New Exception("Dupplicate Approval Limits are not allowed")
                End If
            Else
                Try
                    objICApprovalLimits.LimitsForApprovalID = LimitsForApprovalID
                    ICUserLimitsController.AddICLimitsForApproval(objICApprovalLimits, Me.UserId.ToString, Me.UserInfo.Username.ToString, IsUpdate, ActionStr)
                    PassToAddCount = PassToAddCount + 1
                Catch ex As Exception
                    FailToAddCount = FailToAddCount + 1
                    Throw New Exception(ex.Message.ToString)
                End Try
            End If


        End If
    End Sub
    Private Sub Clear()
        LoadddlGroup()
        LoadddlUser(ddlUserType.SelectedValue.ToString)
        LoadgvUserLimit()
        lblLogo.Text = ""
    End Sub
    Protected Sub cancelbtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlUser(ByVal UserType As String)
        Try
            Dim lit As New ListItem
            Dim IsUpdate As Boolean = False
            If UserCode <> "0" Then
                IsUpdate = True
            End If
            lit.Value = "0"
            lit.Text = "-- Please Select --"

            If UserType = "Client User" Then
                ddlUser.Items.Clear()
                ddlUser.Items.Add(lit)
                ddlUser.AppendDataBoundItems = True
                ddlUser.DataSource = ICUserController.GetAllActiveAndApproveUsersByGroupCodeForUserLimit(ddlGroup.SelectedValue.ToString(), IsUpdate)
                ddlUser.DataTextField = "UserName"
                ddlUser.DataValueField = "UserID"
                ddlUser.DataBind()
            ElseIf UserType = "Bank User" Then
                ddlUser.Items.Clear()
                ddlUser.Items.Add(lit)
                ddlUser.AppendDataBoundItems = True
                ddlUser.DataSource = ICUserController.GetAllActiveAndApproveUsersOfPrincipalBankWithUserAndDisplayNameForUserLimit(IsUpdate)
                ddlUser.DataTextField = "UserName"
                ddlUser.DataValueField = "UserID"
                ddlUser.DataBind()
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckDuplicatLimitLimitForUser(ByVal objICUserLimits As ICLimitForApproval) As Boolean
        Dim Result As Boolean = False
        Dim objICApprovalLimitColl As New ICLimitForApprovalCollection
      
        objICApprovalLimitColl.Query.Where(objICApprovalLimitColl.Query.UserID = objICUserLimits.UserID)

        If objICApprovalLimitColl.Query.Load Then
            Result = True
        End If
        Return Result
    End Function
    Private Function CheckDuplicatLimitLimitForBankUserWithNoAccount(ByVal objICUserLimits As ICLimitForApproval) As Boolean
        Dim Result As Boolean = False
        Dim objICApprovalLimitColl As ICLimitForApprovalCollection
        objICApprovalLimitColl = New ICLimitForApprovalCollection
        objICApprovalLimitColl.es.Connection.CommandTimeout = 3600


        objICApprovalLimitColl.Query.Where(objICApprovalLimitColl.Query.AccountNumber.IsNull)
        objICApprovalLimitColl.Query.Where(objICApprovalLimitColl.Query.Currency.IsNull)
        objICApprovalLimitColl.Query.Where(objICApprovalLimitColl.Query.UserID = objICUserLimits.UserID And objICApprovalLimitColl.Query.BranchCode.IsNull)
        If objICApprovalLimitColl.Query.Load Then
            Result = True
            Return Result
            Exit Function
        End If
        'If Result = False Then
        '    objICApprovalLimitColl = New ICLimitForApprovalCollection
        '    objICApprovalLimitColl.es.Connection.CommandTimeout = 3600
        '    objICApprovalLimitColl.Query.Where(objICApprovalLimitColl.Query.BankPrimaryApprovalLimit = objICUserLimits.BankPrimaryApprovalLimit Or objICApprovalLimitColl.Query.BankSecondaryApprovalLimit = objICUserLimits.BankSecondaryApprovalLimit)
        '    objICApprovalLimitColl.Query.Where(objICApprovalLimitColl.Query.UserID = objICUserLimits.UserID)
        '    If objICApprovalLimitColl.Query.Load Then
        '        Result = True
        '        Return Result
        '        Exit Function
        '    End If
        'End If
        Return Result
    End Function
    Private Sub LoadgvUserLimit()
        'Try
        '    gvUserLimit.DataSource = ICAccountsController.GetAllActiveAndApproveAccountByGroupCode(ddlGroup.SelectedValue.ToString())
        '    gvUserLimit.DataBind()
        '    If gvUserLimit.Items.Count > 0 Then
        '        'pnlUserLimits.Style.Remove("display")
        '        gvUserLimit.Visible = True
        '        lblRNF.Visible = False
        '    Else
        '        'pnlUserLimits.Style.Add("display", "none")
        '        gvUserLimit.Visible = False
        '        lblRNF.Visible = True
        '    End If

        'Catch ex As Exception
        '    ProcessModuleLoadException(Me, ex, False)
        '    UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        'End Try
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            If ddlUserType.SelectedValue.ToString = "Client User" Then
                LoadddlUser(ddlUserType.SelectedValue.ToString)
                LoadgvUserLimit()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    

    Protected Sub ddlUser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUser.SelectedIndexChanged
        Try
            If ddlUserType.SelectedValue.ToString = "0" Then
                HideAllDropDowns()
            ElseIf ddlUserType.SelectedValue.ToString = "Client User" Then
                'GetAllAccountsByGroupCode()
                'CheckMarkGridOnUpDate(ddlUser.SelectedValue.ToString)
            ElseIf ddlUserType.SelectedValue.ToString = "Bank User" Then
                'GetAllAccountsTaggedWithPrincipalBankUsers()
                'CheckMarkGridOnUpDate(ddlUser.SelectedValue.ToString)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    'Private Sub GetAllAccountsByGroupCode()
    '    Try
    '        Dim GroupCode As String
    '        If ddlGroup.SelectedValue.ToString <> "0" Then
    '            GroupCode = ddlGroup.SelectedValue.ToString
    '        Else
    '            GroupCode = ""
    '        End If
    '        ICUserLimitsController.BindRadGridForAccountsByGroupCode(GroupCode, Me.gvUserLimit.CurrentPageIndex + 1, Me.gvUserLimit.PageSize, True, Me.gvUserLimit)
    '        If gvUserLimit.Items.Count > 0 Then
    '            gvUserLimit.Visible = True
    '            If UserCode.ToString = "0" Then
    '                btnSave.Visible = CBool(htRights("Add"))
    '            Else
    '                btnSave.Visible = CBool(htRights("Update"))
    '            End If
    '        Else

    '            gvUserLimit.Visible = False
    '            btnSave.Visible = False
    '        End If
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    'Private Sub GetAllAccountsTaggedWithPrincipalBankUsers()
    '    Try
    '        Dim GroupCode As String
    '        If ddlGroup.SelectedValue.ToString <> "0" Then
    '            GroupCode = ddlGroup.SelectedValue.ToString
    '        Else
    '            GroupCode = ""
    '        End If
    '        gvUserLimit.DataSource = Nothing

    '        ICInstructionController.GetAccountNumbersTaggedWithPrincipalBankUSer(ddlUser.SelectedValue.ToString, Me.gvUserLimit.CurrentPageIndex + 1, Me.gvUserLimit.PageSize, True, Me.gvUserLimit)
    '        If gvUserLimit.Items.Count > 0 Then
    '            gvUserLimit.Visible = True
    '            lblRNF.Visible = False
    '            If UserCode.ToString = "0" Then
    '                btnSave.Visible = CBool(htRights("Add"))
    '            Else
    '                btnSave.Visible = CBool(htRights("Update"))
    '            End If
    '        Else
    '            lblRNF.Visible = True
    '            gvUserLimit.Visible = False
    '            btnSave.Visible = False
    '        End If
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    Protected Sub ddlUserType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUserType.SelectedIndexChanged
        Try
            If ddlUserType.SelectedValue.ToString = "0" Then
                HideAllDropDowns()
                'gvUserLimit.DataSource = Nothing
                'pnlUserLimits.Style.Add("display", "none")
                'gvUserLimit.Visible = False
                tblApprovalLimit.Style.Add("display", "none")
                'lblRNF.Visible = False
            ElseIf ddlUserType.SelectedValue.ToString = "Bank User" Then
                tblApprovalLimit.Style.Remove("display")
                lblGroup.Visible = False
                ddlGroup.Visible = False
                rfvddlGroup.Enabled = False
                ddlGroup.DataSource = Nothing
                LoadddlUser("Bank User")
                lblSignature.Visible = True
                lblReqSignature.Visible = True
                fupSignature.Visible = True
                'gvUserLimit.DataSource = Nothing
                'pnlUserLimits.Style.Add("display", "none")
                'gvUserLimit.Visible = False
                lblUser.Visible = True
                lblReqUser.Visible = True
                ddlUser.Visible = True
                rfvddlUser.Enabled = True
                lblReqGroup.Visible = False

           
                'lblRNF.Visible = False
            ElseIf ddlUserType.SelectedValue.ToString = "Client User" Then
                tblApprovalLimit.Style.Add("display", "none")
                lblGroup.Visible = True
                ddlGroup.Visible = True
                rfvddlGroup.Enabled = True
                lblReqUser.Visible = True
                LoadddlGroup()
                LoadddlUser("Client User")
                lblSignature.Visible = True
                lblReqSignature.Visible = True
                fupSignature.Visible = True
                'gvUserLimit.DataSource = Nothing
                'gvUserLimit.Visible = False
                'pnlUserLimits.Style.Add("display", "none")
                lblUser.Visible = True
                lblReqGroup.Visible = True
                ddlUser.Visible = True
                rfvddlUser.Enabled = True
              
                'lblRNF.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub HideAllDropDowns()
        lblGroup.Visible = False
        lblReqGroup.Visible = False
        ddlGroup.DataSource = Nothing
        ddlGroup.Visible = False
        rfvddlGroup.Enabled = True
        lblUser.Visible = False
        lblReqUser.Visible = False
        ddlUser.DataSource = Nothing
        ddlUser.Visible = False
        lblSignature.Visible = False
        lblReqSignature.Visible = False
        fupSignature.Visible = False
       
    End Sub
    Protected Sub download(ByVal objICUserLimit As ICLimitForApproval)
        Dim bytes() As Byte = CType(objICUserLimit.UserSignatureImage, Byte())
        Response.Buffer = True
        Response.Charset = ""
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = objICUserLimit.FileType
        Response.AddHeader("content-disposition", "attachment;filename=UserSignature" & objICUserLimit.FileType)
        Response.BinaryWrite(bytes)
        Response.Flush()
        Response.End()
    End Sub

   
End Class
