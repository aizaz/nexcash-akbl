﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InstructionClearing.ascx.vb"
    Inherits="DesktopModules_InstructionClearing_InstructionClearing" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .auto-style1
    {
        width: 25%;
        height: 31px;
    }
    .auto-style2
    {
        width: 25%;
        height: 23px;
    }
</style>
<script language="javascript" type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;

        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to clear selected instruction(s)?\nFinancial transaction(s) is in process, please do not refresh the page.") == true) {
            var btnCancel = document.getElementById('<%=btnClear.ClientID%>')
            btnCancel.value = "Processing...";
            btnCancel.disabled = true;
            var ErrorMessage = document.getElementById('<%=lblMessageForQueue.ClientID%>')
            ErrorMessage.value = "Financial transaction(s) is in process, please do not refresh the page";
            ErrorMessage.style.display = 'inherit';
            var a = btnCancel.name;
            __doPostBack(a, '');
            return true;
        }
        else {
            var ErrorMessage2 = document.getElementById('<%=lblMessageForQueue.ClientID%>')
            ErrorMessage2.value = "";
            ErrorMessage2.style.display = 'none';
            return false;
        }
       
    }
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }

    $(document).ready(function () {

        // Dialog
        $('#urldialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#loading').hide();



            },
            resizable: false
        });
        // Summary Dialog
        $('#sumdialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#sumdialog').hide();
                //        window.location.reload();
                //                window.location.reload();sa
            },
            resizable: false
        });
        $('#sumdialog1').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#sumdialog1').hide();
                //                window.location.reload();
                //                window.location.reload();sa
            },

            resizable: false
        });

        //$("#dnn_ctr487_InstructionClearing_btnClose").click(function () { $('#sumdialog').dialog('close'); });

    });
    function showurldialog(title, url, modal, width, height) {
        //  alert(modal);
        $('#urldialog').dialog("option", "title", title);
        $('#urldialog').dialog("option", "modal", modal);
        $('#urldialog').dialog("option", "width", width);
        $('#urldialog').dialog("option", "height", height);
        $('#urldialog').dialog('open');
        $('#dialogiframe').hide();
        $('#loading').show();
        $('#dialogiframe').attr("src", url);
        $('#dialogiframe').load(function () {
            $('#loading').hide();
            $('#dialogiframe').show();
        });
        return false;

    };

    function CloseIframe() {
        $('#urldialog').dialog('close');
    }
    function showsumdialog1(title, modal, width, height) {
        //  alert(modal);
        $('#sumdialog1').dialog("option", "title", title);
        $('#sumdialog1').dialog("option", "modal", modal);
        $('#sumdialog1').dialog("option", "width", width);
        $('#sumdialog1').dialog("option", "height", height);
        $('#sumdialog1').dialog('open');
        $('#sumdialog1').dialog();
        return false;

    };
    function showsumdialog(title, modal, width, height) {
        //  alert(modal);
        $('#sumdialog').dialog("option", "title", title);
        $('#sumdialog').dialog("option", "modal", modal);
        $('#sumdialog').dialog("option", "width", width);
        $('#sumdialog').dialog("option", "height", height);
        $('#sumdialog').dialog('open');
        $('#sumdialog').dialog();
        return false;

    };
    function CloseSummaryDialogBox() {
        $('#sumdialog').dialog('close');
    }
</script>
<telerik:RadInputManager ID="rimDefault" runat="server">
<telerik:RegExpTextBoxSetting BehaviorID="reInstructionNo" Validation-IsRequired="true"
        ErrorMessage="Enter Instruction/Instrument No." ValidationExpression="[0-9]{1,30}" Validation-ValidationGroup="Details">
        <TargetControls>
            <telerik:TargetInput ControlID="txtNumber" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
   <%-- <telerik:TextBoxSetting BehaviorID="radNumber" Validation-IsRequired="true" ErrorMessage="Invalid No."
        EmptyMessage="" Validation-ValidationGroup="Details">
        <TargetControls>
            <telerik:TargetInput ControlID="txtNumber" />
        </TargetControls>
    </telerik:TextBoxSetting>--%>
</telerik:RadInputManager>
<telerik:RadInputManager ID="rimFundsTransfer" runat="server" Enabled="False">    
    <telerik:RegExpTextBoxSetting BehaviorID="radAccountNo" Validation-IsRequired="false"
        ValidationExpression="^[a-zA-Z0-9]+$" ErrorMessage="Invalid Account No."
        EmptyMessage="" Validation-ValidationGroup="Clear">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneAccountNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radBeneAccountTitle" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Invalid Account Title" Validation-ValidationGroup="Clear">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneAccountTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radBeneAccountBranchCode" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Invalid Account Branch Code" Validation-ValidationGroup="Clear">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneAccountBranchCode" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="radBeneAccountCurrency" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Invalid Account Currency" Validation-ValidationGroup="Clear">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneAccountCurrency" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<telerik:RadInputManager ID="rimDetail" runat="server" Enabled="False">
    <telerik:TextBoxSetting BehaviorID="radPhysicalInstrumentNumber" Validation-IsRequired="true"
        EmptyMessage="" ErrorMessage="Invalid Physical Instrument Number" Validation-ValidationGroup="Clear">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPhysicalInstrumentNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<telerik:RadInputManager ID="radChqReturn" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="REDepartment" Validation-IsRequired="true" EmptyMessage="" Validation-ValidationGroup="Return">
        <TargetControls>
            <telerik:TargetInput ControlID="txtChqRetReason" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue">Instruction Clearing</asp:Label>
             <br /><br />
                        <asp:Label ID="lblMessageForQueue" runat="server" Text="" CssClass="" ForeColor="Red" >
                        </asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
            <asp:Label ID="lblClearingBranchCode" runat="server" CssClass="lbl"></asp:Label>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="auto-style2">
            <asp:Label ID="lblProductType" runat="server" Text="Select Product Type" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label5" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" class="auto-style2">
            &nbsp;
        </td>
        <td align="left" valign="top" class="auto-style2">
            <asp:Label ID="lblTransferType" runat="server" Text="Select Transfer Type" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label8" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" class="auto-style2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RadioButtonList ID="rbtnlstProductType" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
            </asp:RadioButtonList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RadioButtonList ID="rbtnlstTransferType" runat="server" 
                RepeatDirection="Horizontal" AutoPostBack="True">
                <%--<asp:ListItem>Funds Transfer</asp:ListItem>--%>
                <%--<asp:ListItem >Clearing</asp:ListItem>--%>
            </asp:RadioButtonList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvrbtnlstProductType" runat="server" ControlToValidate="rbtnlstProductType" ValidationGroup="Details"
                Display="Dynamic" ErrorMessage="Please select Product Type" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvrbtnlstTransferType" runat="server" ControlToValidate="rbtnlstTransferType" ValidationGroup="Details"
                Display="Dynamic" ErrorMessage="Please select Transfer Type" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblNumberType" runat="server" Text="Select Number Type" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblNumber" runat="server" Text="Number" CssClass="lbl"></asp:Label><asp:Label
                ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label><asp:Label ID="lblHelpText" runat="server" Text="(Please enter ins. number without prefix)" CssClass="lbl" Font-Size="12px"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="auto-style1">
            <asp:RadioButtonList ID="rbtnlstNumberType" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Value="Instruction ID">Instruction No</asp:ListItem>
                <asp:ListItem>Instrument Number</asp:ListItem>
            </asp:RadioButtonList>
        </td>
        <td align="left" valign="top" class="auto-style1">
        </td>
        <td align="left" valign="top" class="auto-style1">
            <asp:TextBox ID="txtNumber" runat="server" CssClass="txtbox" MaxLength="30"></asp:TextBox>
        </td>
        <td align="left" valign="top" class="auto-style1">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvrbtnlstNumberType" runat="server" ControlToValidate="rbtnlstNumberType"
                Display="Dynamic" ErrorMessage="Please select Number Type" SetFocusOnError="True" ValidationGroup="Details"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" style="width: 100%" colspan="4">
            <asp:Button ID="btnShowDetails" runat="server" Text="Show Details" CssClass="btn" CausesValidation="true" ValidationGroup="Details" />
        &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Back" CssClass="btnCancel" Width="75px" CausesValidation="False" />
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" style="width: 100%" colspan="4">
            <telerik:RadGrid ID="gvClearing" runat="server" AllowPaging="false" Width="100%"
                AllowSorting="false" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="false"
                PageSize="1" Visible="False">
                <AlternatingItemStyle CssClass="rgAltRow" />
                <MasterTableView DataKeyNames="ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,InstructionID,IsAmendmentComplete,PaymentMode">
                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    </ExpandCollapseColumn>
                    <Columns>                        
                        <telerik:GridBoundColumn DataField="InstructionID" HeaderText="Instruction No." SortExpression="InstructionID">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                        </telerik:GridBoundColumn>
                       <telerik:GridBoundColumn  HeaderText="Batch No." >
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CreateDate" HeaderText="Issue Date" SortExpression="CreateDate"
                            HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryName" HeaderText="BeneficiaryName"
                            SortExpression="BeneficiaryName">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BeneficiaryAddress" HeaderText="Bene Address"
                            SortExpression="BeneficiaryAddress">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Amount" HeaderText="Amount" SortExpression="Amount"
                            HtmlEncode="false" DataFormatString="{0:N2}">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PaymentMode" HeaderText="Payment Mode" SortExpression="PaymentMode">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="InstrumentNo" HeaderText="Instrument No." SortExpression="InstrumentNo">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                        </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn DataField="ClientPrimaryApprovedDate" HeaderText="Primary Approval" SortExpression="ClientPrimaryApprovedDate"
                            HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy hh:mm:ss tt}">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ClientSecondaryApprovalDateTime" HeaderText="Secondary Approval" SortExpression="ClientSecondaryApprovalDateTime"
                            HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy hh:mm:ss tt}">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                    <PagerStyle AlwaysVisible="True" />
                </MasterTableView>
                <ItemStyle CssClass="rgRow" />
                <PagerStyle AlwaysVisible="True" />
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" style="width: 100%" colspan="4">
            <table id="tblDetail" runat="server" width="100%" style="display:none;">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Button ID="btnVerifySignatures" runat="server" Text="Verify Signatures" 
                            CssClass="btn" CausesValidation="false" />
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <table id="tblFundsTransfer" runat="server" width="100%" style="display:none;">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblBeneAccountNumber" runat="server" Text="Benificiary Account No."
                            CssClass="lbl"></asp:Label>
                        <asp:Label ID="Label6" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblBeneAccountTitle" runat="server" Text="Benificiary Account Title"
                            CssClass="lbl"></asp:Label>
                       <%-- <asp:Label ID="Label7" runat="server" Text="*" ForeColor="Red"></asp:Label>--%>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtBeneAccountNo" runat="server" CssClass="txtbox" 
                            MaxLength="50" AutoPostBack="True"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtBeneAccountTitle" runat="server" CssClass="txtbox" ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblBeneAccountBranchCode" runat="server" Text="Benificiary Account Branch Code"
                            CssClass="lbl"></asp:Label>
                        <%--<asp:Label ID="Label10" runat="server" Text="*" ForeColor="Red"></asp:Label>--%>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblBeneAccountCurrency" runat="server" Text="Benificiary Account Currency"
                            CssClass="lbl"></asp:Label>
                        <%--<asp:Label ID="Label11" runat="server" Text="*" ForeColor="Red"></asp:Label>--%>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtBeneAccountBranchCode" runat="server" CssClass="txtbox" ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtBeneAccountCurrency" runat="server" CssClass="txtbox" ReadOnly="True"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <table id="tblClearing" runat="server" width="100%" style="display:none;">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblClearingType" runat="server" Text="Clearing Type" CssClass="lbl"></asp:Label>
                        <asp:Label ID="Label12" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:RadioButtonList ID="rbtnlstClearingType" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem>Normal</asp:ListItem>
                            <asp:ListItem>Intercity</asp:ListItem>
                            <asp:ListItem>Same Day</asp:ListItem>
                        </asp:RadioButtonList>

                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:RequiredFieldValidator ID="rfvrbtnlstClearingType" runat="server" ControlToValidate="rbtnlstClearingType"
                            Display="Dynamic" ErrorMessage="Please select Clearing Type" 
                            SetFocusOnError="True" Enabled="False" ValidationGroup="Clear"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>    
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnClear" runat="server" Text="Save" CssClass="btn"  CausesValidation="true" ValidationGroup="Clear" OnClientClick="javascript: return conBukDelete();"
                Visible="False"  />
            &nbsp;</td>
    </tr>
        <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
             <table id="pnlShowSummary1" runat="server" style="display:none;width:100%" >
            <tr>
                <td align="left" valign="top"  colspan="4">
                    <telerik:RadGrid ID="gvChequeReturned" runat="server" CssClass="RadGrid" AllowSorting="false"
                        AllowPaging="false" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                        PageSize="1">
                        <%--<ClientSettings>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>--%>
                        <MasterTableView DataKeyNames="InstructionID,IsChequeReturned">
                            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridTemplateColumn  HeaderStyle-HorizontalAlign="Left">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Checked="true" Text=""
                                            TextAlign="Right" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Checked="true" Text="" />
                                    </ItemTemplate>
                                    
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="InstructionID" HeaderText="Instruction No." SortExpression="InstructionID">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CreateDate" HeaderText="Creation Date" SortExpression="CreateDate"
                                    HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ValueDate" HeaderText="Value Date" SortExpression="ValueDate"
                                    HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="BeneficiaryName" HeaderText="Bene Name" SortExpression="BeneficiaryName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Amount" HeaderText="Amount" SortExpression="Amount"
                                    HtmlEncode="false" DataFormatString="{0:N2}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ProductTypeCode" HeaderText="Payment Mode" SortExpression="ProductTypeCode">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="InstrumentNo" HeaderText="Instrument No." SortExpression="InstrumentNo">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="OfficeName" HeaderText="Print Location" SortExpression="OfficeName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="StatusName" HeaderText="Status" SortExpression="StatusName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="BeneficiaryAccountNo" HeaderText="Account No."
                                    SortExpression="BeneficiaryAccountNo">
                                </telerik:GridBoundColumn>
                                 <%--<telerik:GridTemplateColumn HeaderText="Cheque Returned Reason">
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlChequeReturnedReason" runat="server" Width="70%" AutoPostBack="true">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>--%>
                            </Columns>
                            <EditFormSettings>
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                </EditColumn>
                            </EditFormSettings>
                            <PagerStyle AlwaysVisible="True" />
                        </MasterTableView>
                        <HeaderStyle CssClass="GridHeader" />
                        <ItemStyle CssClass="GridItem" />
                        <AlternatingItemStyle CssClass="GridItem" />
                        <PagerStyle AlwaysVisible="True" />
                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>
                    </telerik:RadGrid>
                </td>
            </tr>
                 <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <table id="tblChqReturn" runat="server" style="width:100%;display:none" >
                <tr>
                    <td style="width:100%">     <asp:Label ID="lblChqReturn" runat="server" Text="Select Return Reason" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqChqReturn" runat="server" Text="*" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width:100%"><asp:DropDownList ID="ddlChequeReturnedReason" runat="server" Width="70%" AutoPostBack="true">
                                    </asp:DropDownList></td>
                </tr>
                <tr>
                    <td style="width:100%"><asp:RequiredFieldValidator ID="rfvddlChequeReturnedReason" runat="server" 
                ControlToValidate="ddlChequeReturnedReason" Display="Dynamic" 
                ErrorMessage="Please select Reason" InitialValue="0" 
                SetFocusOnError="True" Enabled="false" ValidationGroup="Return"></asp:RequiredFieldValidator></td>
                </tr>
            </table>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <table id="tblOtherReason" runat="server" style="width:100%;display:none">
                <tr>
                    <td style="width:100%">     <asp:Label ID="lblOtherReason" runat="server" Text="Enter Reason" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqOtherReason" runat="server" Text="*" ForeColor="Red"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width:100%">            <asp:TextBox ID="txtChqRetReason" runat="server" CssClass="txtbox" 
                MaxLength="250" Height="70px"
                TextMode="MultiLine" 
                onkeypress="return textboxMultilineMaxNumber(this,250)"></asp:TextBox>
        </td>
                </tr>
            </table>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
            <tr>
                <td align="center" valign="top" colspan="4">
                   <asp:Button ID="btnChequeReturned" runat="server" Text="Cheque Returned" CssClass="btn" ValidationGroup="Return"/>
                    <asp:Button ID="btnCancelGrid" runat="server" Text="Cancel" CssClass="btnCancel" CausesValidation="false"/>
                </td>
            </tr>
        </table>
            </td>
    </tr>
       
</table>
<div id="sumdialog" title="" style="text-align: center">
    <asp:Panel ID="pnlShowSummary" runat="server" Style="display: none; width: 100%"
        ScrollBars="Vertical" Height="200px">
        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnCancel" OnClientClick="CloseSummaryDialogBox();return false;"/><br />
        <telerik:RadGrid ID="gvShowSummary" runat="server" AllowPaging="false" Width="100%" CssClass="RadGrid"
            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
            PageSize="1">
            <AlternatingItemStyle CssClass="rgAltRow" />
            <MasterTableView>
                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                </RowIndicatorColumn>
                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="InstructionID" HeaderText="Instruction No." SortExpression="InstructionID">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                    <%-- <telerik:GridBoundColumn DataField="ReferenceNo" HeaderText="Reference No." SortExpression="ReferenceNo">
                                    </telerik:GridBoundColumn>--%>
                    <telerik:GridBoundColumn DataField="Message" HeaderText="Message" SortExpression="Message">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
                <PagerStyle AlwaysVisible="True" />
            </MasterTableView>
            <ItemStyle CssClass="rgRow" />
            <PagerStyle AlwaysVisible="True" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
    </asp:Panel>
</div>
<div id="urldialog" title="">
    <iframe id='dialogiframe' frameborder="0" allowtransparency="true" src="" style="border-style: none;
        width: 100%; height: 100%; overflow-x: hidden; background-color: transparent;">
    </iframe>
    <div id='loading' style="display: none; margin: 0px auto; text-align: center; width: 100%;
        height: 100%; position: relative">
        <img src='../../../../../images/progressbar.gif' style="left: 45%; top: 50%; position: absolute;" />
    </div>
</div>