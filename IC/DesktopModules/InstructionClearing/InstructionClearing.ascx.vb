﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_InstructionClearing_InstructionClearing
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private arrStatues As New ArrayList
    Private arrUserRoles As New ArrayList
    Private htRights As Hashtable

#Region "Page Load"
    Protected Sub DesktopModules_InstructionClearing_InstructionClearing_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ' btnShowDetails.Attributes.Item("onclick") = "this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnShowDetails, Nothing).ToString()


            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            lblMessageForQueue.Style.Add("display", "none")
            If Page.IsPostBack = False Then
                lblMessageForQueue.Text = ICUtilities.GetSettingValue("ErrorMessage")
                btnShowDetails.Attributes.Item("onclick") = "if (Page_ClientValidate('Details')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnShowDetails, Nothing).ToString() & " } "
                btnClear.Attributes.Item("onclick") = "if (Page_ClientValidate('Clear')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";document.getElementById('<%=lblMessageForQueue.ClientID%>').style.display = 'block';this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnClear, Nothing).ToString() & " } "
                If Me.UserInfo.IsSuperUser = False Then
                    Dim objUser As New ICUser
                    objUser.LoadByPrimaryKey(Me.UserId.ToString())
                    lblClearingBranchCode.Text = "Clearing Branch Code: " & objUser.ClearingBranchCode
                    If objUser.UserType.ToString() <> "Bank User" Then
                        UIUtilities.ShowDialog(Me, "Instruction Clearing", "Only authorized Bank Users allowed", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                        Exit Sub
                    Else
                        Dim objUserBranch As New ICOffice
                        If objUserBranch.LoadByPrimaryKey(objUser.OfficeCode) Then
                            If Not (objUserBranch.IsActive = True And objUserBranch.IsApprove = True) Then
                                UIUtilities.ShowDialog(Me, "Instruction Clearing", "User Branch is not approved.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                                Exit Sub
                            End If
                        End If

                    End If
                    ViewState("htRights") = Nothing
                    GetPageAccessRights()
                    PageLoad()
                End If

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub PageLoad()

        SetArraListRoleID()
        SetdtAPNatureByAssignedRoles()
        SetProductType()
        SetControls()
        FillTransferType()
        gvClearing.DataSource = Nothing
        gvClearing.DataBind()
        gvClearing.Visible = False
        btnClear.Visible = False
        txtNumber.Text = ""
        rbtnlstNumberType.ClearSelection()
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Clearing")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        arrUserRoles.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Clearing") = True Then
                    arrUserRoles.Add(RoleID.ToString)
                End If
            Next
        End If
    End Sub

    Private Sub SetdtAPNatureByAssignedRoles()
        Try
            Dim dtAssignedAPNatureOfficeID As New DataTable
            dtAssignedAPNatureOfficeID.Rows.Clear()
            dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserSecond(Me.UserId.ToString, arrUserRoles)
            ViewState("AccountNumber") = Nothing
            ViewState("AccountNumber") = dtAssignedAPNatureOfficeID
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetProductType()
        Try
            Dim lit As ListItem
            rbtnlstProductType.Items.Clear()
            rbtnlstProductType.ClearSelection()
            If htRights("PO Instructions") = True Then
                lit = New ListItem
                lit.Text = "PO"
                lit.Value = "PO"
                rbtnlstProductType.Items.Add(lit)
            End If
            If htRights("Cheque Instructions") = True Then
                lit = New ListItem
                lit.Text = "Cheque"
                lit.Value = "Cheque"
                rbtnlstProductType.Items.Add(lit)
            End If
            If rbtnlstProductType.Items.Count = 1 Then
                rbtnlstProductType.Items(0).Selected = True
                rbtnlstProductType.Enabled = False
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetControls()
        btnShowDetails.Visible = True

        gvClearing.DataSource = Nothing
        gvClearing.DataBind()
        gvClearing.Visible = False
        rimDetail.Enabled = False
        tblDetail.Style.Add("display", "none")

        txtBeneAccountNo.Text = Nothing
        txtBeneAccountTitle.Text = Nothing
        txtBeneAccountBranchCode.Text = Nothing
        txtBeneAccountCurrency.Text = Nothing
        rimFundsTransfer.Enabled = False

        tblFundsTransfer.Style.Add("display", "none")
        'rimFundsTransfer.GetSettingByBehaviorID("radAccountno").Validation.IsRequired = "false"
        'rimFundsTransfer.GetSettingByBehaviorID("radbeneaccounttitle").Validation.IsRequired = "false"
        'rimFundsTransfer.GetSettingByBehaviorID("radbeneaccountbranchcode").Validation.IsRequired = "false"
        'rimFundsTransfer.GetSettingByBehaviorID("radbeneaccountcurrency").Validation.IsRequired = "false"
        rbtnlstClearingType.ClearSelection()
        rfvrbtnlstClearingType.Enabled = False
        tblClearing.Style.Add("display", "none")

        btnClear.Visible = False
    End Sub
    Private Sub FillTransferType()
        rbtnlstTransferType.Items.Clear()

        Dim lit As ListItem

        'lit = New ListItem
        'lit.Text = "Clearing"
        'lit.Value = "Clearing"
        'rbtnlstTransferType.Items.Add(lit)
        rbtnlstTransferType.Enabled = True
        If CBool(htRights("Funds Transfer")) = True And CBool(htRights("Clearing")) = True Then
            lit = New ListItem
            lit.Text = "Funds Transfer"
            lit.Value = "Funds Transfer"
            rbtnlstTransferType.Items.Add(lit)
            lit = New ListItem
            lit.Text = "Clearing"
            lit.Value = "Clearing"
            rbtnlstTransferType.Items.Add(lit)
        ElseIf CBool(htRights("Funds Transfer")) = False And CBool(htRights("Clearing")) = True Then
            lit = New ListItem
            lit.Text = "Clearing"
            lit.Value = "Clearing"
            rbtnlstTransferType.Items.Add(lit)
            lit.Selected = True
            rbtnlstTransferType.Enabled = False
        ElseIf CBool(htRights("Funds Transfer")) = True And CBool(htRights("Clearing")) = False Then
            lit = New ListItem
            lit.Text = "Funds Transfer"
            lit.Value = "Funds Transfer"
            rbtnlstTransferType.Items.Add(lit)
            lit.Selected = True
            rbtnlstTransferType.Enabled = False
        Else
        End If
    End Sub
    'Private Sub FillTransferType()
    '    rbtnlstTransferType.Items.Clear()
    '    Dim lit As ListItem
    '    If CBool(htRights("Funds Transfer")) = True Then
    '        lit = New ListItem
    '        lit.Text = "Funds Transfer"
    '        lit.Value = "Funds Transfer"
    '        rbtnlstTransferType.Items.Add(lit)
    '        lit.Selected = True
    '        rbtnlstTransferType.Enabled = False
    '    Else
    '        lit = New ListItem
    '        lit.Text = "Clearing"
    '        lit.Value = "Clearing"
    '        rbtnlstTransferType.Items.Add(lit)
    '        rbtnlstTransferType.Enabled = False
    '        lit.Selected = True
    '    End If
    'End Sub
#End Region
    Protected Sub btnShowDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowDetails.Click
        If Page.IsValid Then
            Try
                Dim ResultString As String = ""
                Dim dt As New DataTable
                Dim chkAmount As Integer = 0
                Dim IDNO As String = Nothing
                dt = DirectCast(ViewState("AccountNumber"), DataTable)
                If rbtnlstNumberType.SelectedValue.ToString() = "Instruction ID" Then
                    If Integer.TryParse(txtNumber.Text.ToString().Trim(), chkAmount) = False Then
                        txtNumber.Text = Nothing
                        UIUtilities.ShowDialog(Me, "Instruction Clearing", "Invalid Instruction ID", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    Else
                        IDNO = txtNumber.Text.ToString
                    End If
                ElseIf rbtnlstNumberType.SelectedValue.ToString() = "Instrument Number" Then
                    If rbtnlstProductType.SelectedValue.ToString = "PO" Then
                        IDNO = "PO-" & txtNumber.Text.ToString
                    Else
                        IDNO = txtNumber.Text.ToString
                    End If
                End If

                ResultString = ICInstructionProcessController.GetInstructionForClearing(rbtnlstNumberType.SelectedValue.ToString(), IDNO, dt, rbtnlstProductType.SelectedValue.ToString)

                If ResultString.ToString().Contains("|") Then
                    Dim collInstruction As New ICInstructionCollection
                    collInstruction = ICInstructionController.GetInstructionForClearing(ResultString.ToString().Split("|")(1).ToString())

                    gvClearing.DataSource = collInstruction
                    gvClearing.DataBind()
                    gvClearing.Visible = True

                    btnShowDetails.Visible = False

                    tblDetail.Style.Remove("display")
                    rimDetail.Enabled = True

                    If rbtnlstTransferType.SelectedValue.ToString() = "Funds Transfer" Then
                        tblFundsTransfer.Style.Remove("display")
                        rimFundsTransfer.Enabled = True
                        'rimFundsTransfer.GetSettingByBehaviorID("radAccountno").Validation.IsRequired = True
                        'rimFundsTransfer.GetSettingByBehaviorID("radbeneaccounttitle").Validation.IsRequired = True
                        'rimFundsTransfer.GetSettingByBehaviorID("radbeneaccountbranchcode").Validation.IsRequired = True
                        'rimFundsTransfer.GetSettingByBehaviorID("radbeneaccountcurrency").Validation.IsRequired = True
                        rfvrbtnlstClearingType.Enabled = False
                    ElseIf rbtnlstTransferType.SelectedValue.ToString() = "Clearing" Then
                        tblClearing.Style.Remove("display")
                        'If rbtnlstProductType.SelectedValue.ToString = "PO" Then
                        '    rbtnlstClearingType.Items.Remove("Intercity")
                        'End If
                        rfvrbtnlstClearingType.Enabled = True
                        rimFundsTransfer.Enabled = False
                        'rimFundsTransfer.GetSettingByBehaviorID("radAccountno").Validation.IsRequired = False
                        'rimFundsTransfer.GetSettingByBehaviorID("radbeneaccounttitle").Validation.IsRequired = False
                        'rimFundsTransfer.GetSettingByBehaviorID("radbeneaccountbranchcode").Validation.IsRequired = False
                        'rimFundsTransfer.GetSettingByBehaviorID("radbeneaccountcurrency").Validation.IsRequired = False
                    End If
                    btnClear.Visible = htRights("Clear")


                Else
                    UIUtilities.ShowDialog(Me, "Instruction Clearing", ResultString.ToString(), ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(41), False)
    End Sub

    Protected Sub rbtnlstTransferType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnlstTransferType.SelectedIndexChanged
        Dim objICUser As New ICUser
        pnlShowSummary1.Style.Add("display", "none")
        If rbtnlstTransferType.SelectedValue.ToString = "Clearing" Then
            objICUser.LoadByPrimaryKey(Me.UserId.ToString)
            If objICUser.ClearingBranchCode Is Nothing Or objICUser.ClearingBranchCode = "" Then
                UIUtilities.ShowDialog(Me, "Error", "Clearing branch code is not tagged with user.", Dialogmessagetype.Failure, NavigateURL(41))
                Exit Sub
            End If

        End If
        SetControls()
    End Sub

    Protected Sub btnVerifySignatures_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVerifySignatures.Click
        'If Page.IsValid Then
        Try
            Dim AppPath As String = "" ' = DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
            Dim scr As String = ""
            'Dim AccountNumber As String = gvClearing.Items(0).GetDataKeyValue("ClientAccountNo").ToString()
            'Dim BranchCode As String = gvClearing.Items(0).GetDataKeyValue("ClientAccountBranchCode").ToString()
            'Dim Currency As String = gvClearing.Items(0).GetDataKeyValue("ClientAccountCurrency").ToString()
            Dim InstructionID As String = gvClearing.Items(0).GetDataKeyValue("InstructionID").ToString()
            scr = "window.onload = function () {showurldialog('Account Signatures','/ShowSiignatures.aspx?an=" & InstructionID.ToString().EncryptString() & "' ,true,700,650);return false;}"
            Me.Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
        'End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        'For Each validator As BaseValidator In Page.Validators
        '    If validator.Enabled = True And validator.IsValid = False Then
        '        Dim clientID As String
        '        clientID = validator.ClientID
        '    End If
        'Next


        'If Page.IsValid Then
        Dim objUser As New ICUser
        Dim objUserBranch As New ICOffice
        Dim ToAccountNumber As String = ""
        Dim ToAccountBranchCode As String = ""
        Dim ToAccountCurrency As String = ""
        Dim ToSeqNo As String = ""
        Dim ToProfitCentre As String = ""
        Dim ToClientNo As String = ""
        Dim ToAccountType As String = ""
        Dim PhysicalInstrumentNumber As String = ""
        Dim InstructionID As String = ""
        Dim Result As String = ""
        Dim objICInstruction As New ICInstruction

        Dim ArrayListInstructionID As New ArrayList
        Dim dtVerifiedInstruction As DataTable
        Dim ClearingType As String = Nothing
        Dim TransferType As String = Nothing
        Dim dr As DataRow
        Dim objICInstructionCleared As ICInstruction
        DesignDtForSummary()
        Dim dtSummary As DataTable
        dtSummary = New DataTable
        dtSummary = DirectCast(ViewState("Summary"), DataTable)
        Dim LastStatus As String = Nothing

        Try
            If CBUtilities.GetEODStatus() = "01" Then
                UIUtilities.ShowDialog(Me, "Error", "EOD Status : " & CBUtilities.GetErrorMessageForTransactionFromResponseCode("01", "EOD"), Dialogmessagetype.Failure, NavigateURL(41))
                Exit Sub
            End If
        Catch ex As Exception
            UIUtilities.ShowDialog(Me, "Error", "EOD Status : " & ex.Message.ToString, Dialogmessagetype.Failure, NavigateURL(41))
            Exit Sub
        End Try


        Try


            If gvClearing.Items.Count = 0 Then
                UIUtilities.ShowDialog(Me, "Instruction Clearing", "No instruction selected", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                Exit Sub
            End If



            InstructionID = gvClearing.Items(0).GetDataKeyValue("InstructionID").ToString()
            PhysicalInstrumentNumber = Nothing


            ' Check Instruction Status before clear
            Dim objchkICInstruction As New ICInstruction
            If objchkICInstruction.LoadByPrimaryKey(InstructionID) Then
                If objchkICInstruction.Status = "30" Then
                    UIUtilities.ShowDialog(Me, "Instruction Clearing", "Instruction already cleared.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    Exit Sub
                ElseIf objchkICInstruction.Status = "38" Then
                    UIUtilities.ShowDialog(Me, "Instruction Clearing", "Instruction already Pending for Clearing Approval.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    Exit Sub
                ElseIf objchkICInstruction.Status = "5" Then
                    UIUtilities.ShowDialog(Me, "Instruction Clearing", "Instrument has been cancelled.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    Exit Sub
                ElseIf objchkICInstruction.Status = "32" Then
                    UIUtilities.ShowDialog(Me, "Instruction Clearing", "Instrument has been stopped.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    Exit Sub
                ElseIf objchkICInstruction.Status = "52" Or objchkICInstruction.Status = "40" Or objchkICInstruction.Status = "39" Then
                    UIUtilities.ShowDialog(Me, "Instruction Clearing", "Instruction already under process.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    Exit Sub
                End If
            End If

            If rbtnlstTransferType.SelectedValue.ToString() = "Funds Transfer" Then
                If (txtBeneAccountNo.Text = "" Or txtBeneAccountNo.Text Is Nothing) Or (txtBeneAccountBranchCode.Text = "" Or txtBeneAccountBranchCode.Text Is Nothing) Or (txtBeneAccountCurrency.Text = "" Or txtBeneAccountCurrency.Text Is Nothing) Or (txtBeneAccountTitle.Text = "" Or txtBeneAccountTitle.Text Is Nothing) Then
                    UIUtilities.ShowDialog(Me, "Instruction Clearing", "Invalid beneficiary account number", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            ElseIf rbtnlstTransferType.SelectedValue.ToString() = "Clearing" Then
                If rbtnlstClearingType.SelectedValue.ToString() = "" Or rbtnlstClearingType.SelectedValue.ToString() = "0" Then
                    UIUtilities.ShowDialog(Me, "Instruction Clearing", "Please select Clearing Type.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            End If



            LastStatus = objchkICInstruction.Status
            ICInstructionController.UpdateInstructionStatus(objchkICInstruction.InstructionID.ToString, objchkICInstruction.Status.ToString, "52", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE")
            objchkICInstruction = New ICInstruction
            objchkICInstruction.LoadByPrimaryKey(InstructionID)


            objUser.LoadByPrimaryKey(Me.UserId.ToString())
            If objUserBranch.LoadByPrimaryKey(objUser.OfficeCode) Then
                If objUserBranch.IsActive = True And objUserBranch.IsApprove = True Then
                    If rbtnlstTransferType.SelectedValue.ToString() = "Funds Transfer" Then
                        ToAccountNumber = txtBeneAccountNo.Text.ToString()
                        ToAccountBranchCode = txtBeneAccountBranchCode.Text.ToString()
                        ToAccountCurrency = txtBeneAccountCurrency.Text.ToString()
                        ToSeqNo = ""
                        ToClientNo = ""
                        ToProfitCentre = ""
                        ToAccountType = "RB"
                        ClearingType = "Funds Transfer"
                    ElseIf rbtnlstTransferType.SelectedValue.ToString() = "Clearing" Then

                        ClearingType = "Clearing"
                        If rbtnlstClearingType.SelectedValue.ToString() = "Normal" Then
                            TransferType = "Normal"
                            If objUserBranch.ClearingNormalAccountNumber Is Nothing Then
                                objchkICInstruction = New ICInstruction
                                objchkICInstruction.LoadByPrimaryKey(InstructionID)
                                ICInstructionController.UpdateInstructionStatus(objchkICInstruction.InstructionID.ToString, objchkICInstruction.Status.ToString, objchkICInstruction.LastStatus.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE")
                                UIUtilities.ShowDialog(Me, "Instruction Clearing", "Clearing Account not defined.", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                            If objUserBranch.ClearingNormalAccountType.ToString = "GL" Then
                                ToAccountNumber = objUserBranch.ClearingNormalAccountNumber.ToString()
                                ToAccountBranchCode = objUser.ClearingBranchCode
                                'ToAccountBranchCode = objUserBranch.ClearingNormalAccountBranchCode.ToString
                                ToAccountCurrency = objUserBranch.ClearingNormalAccountCurrency.ToString()
                                ToSeqNo = ""
                                ToClientNo = ""
                                ToProfitCentre = ""
                                ToAccountType = objUserBranch.ClearingNormalAccountType
                            Else
                                ToAccountNumber = objUserBranch.ClearingNormalAccountNumber.ToString()
                                ToAccountBranchCode = objUser.ClearingBranchCode
                                'ToAccountBranchCode = objUserBranch.ClearingNormalAccountBranchCode.ToString
                                ToAccountCurrency = objUserBranch.ClearingNormalAccountCurrency.ToString()
                                ToSeqNo = ""
                                ToClientNo = ""
                                ToProfitCentre = ""
                                ToAccountType = objUserBranch.ClearingNormalAccountType
                            End If


                        ElseIf rbtnlstClearingType.SelectedValue.ToString() = "Intercity" Then
                            TransferType = "InterCity"
                            If objUserBranch.InterCityAccountNumber Is Nothing Then
                                objchkICInstruction = New ICInstruction
                                objchkICInstruction.LoadByPrimaryKey(InstructionID)
                                ICInstructionController.UpdateInstructionStatus(objchkICInstruction.InstructionID.ToString, objchkICInstruction.Status.ToString, objchkICInstruction.LastStatus.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE")
                                UIUtilities.ShowDialog(Me, "Instruction Clearing", "Clearing Account not defined.", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                            If objUserBranch.InterCityAccountType.ToString = "GL" Then
                                ToAccountNumber = objUserBranch.InterCityAccountNumber.ToString()
                                ToAccountBranchCode = objUser.ClearingBranchCode
                                'ToAccountBranchCode = objUserBranch.InterCityAccountBranchCode.ToString
                                ToAccountCurrency = objUserBranch.InterCityAccountCurrency.ToString()
                                ToSeqNo = ""
                                ToClientNo = ""
                                ToProfitCentre = ""
                                ToAccountType = objUserBranch.InterCityAccountType
                            Else
                                ToAccountNumber = objUserBranch.InterCityAccountNumber.ToString()
                                'ToAccountBranchCode = objUserBranch.InterCityAccountBranchCode.ToString
                                ToAccountBranchCode = objUser.ClearingBranchCode
                                ToAccountCurrency = objUserBranch.InterCityAccountCurrency.ToString()
                                ToSeqNo = ""
                                ToClientNo = ""
                                ToProfitCentre = ""
                                ToAccountType = objUserBranch.InterCityAccountType
                            End If
                        ElseIf rbtnlstClearingType.SelectedValue.ToString() = "Same Day" Then
                            TransferType = "SameDay"
                            If objUserBranch.SameDayAccountNumber Is Nothing Then
                                objchkICInstruction = New ICInstruction
                                objchkICInstruction.LoadByPrimaryKey(InstructionID)
                                ICInstructionController.UpdateInstructionStatus(objchkICInstruction.InstructionID.ToString, objchkICInstruction.Status.ToString, objchkICInstruction.LastStatus.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE")
                                UIUtilities.ShowDialog(Me, "Instruction Clearing", "Clearing Account not defined.", ICBO.IC.Dialogmessagetype.Failure)
                                Exit Sub
                            End If
                            If objUserBranch.SameDayAccountType.ToString = "GL" Then
                                ToAccountNumber = objUserBranch.SameDayAccountNumber.ToString()
                                ToAccountBranchCode = objUser.ClearingBranchCode
                                'ToAccountBranchCode = objUserBranch.SameDayAccountBranchCode.ToString()
                                ToAccountCurrency = objUserBranch.SameDayAccountCurrency.ToString()
                                ToSeqNo = ""
                                ToClientNo = ""
                                ToProfitCentre = ""
                                ToAccountType = objUserBranch.SameDayAccountType
                            Else
                                ToAccountNumber = objUserBranch.SameDayAccountNumber.ToString()
                                ToAccountBranchCode = objUser.ClearingBranchCode
                                'ToAccountBranchCode = objUserBranch.SameDayAccountBranchCode.ToString()
                                ToAccountCurrency = objUserBranch.SameDayAccountCurrency.ToString()
                                ToSeqNo = ""
                                ToClientNo = ""
                                ToProfitCentre = ""
                                ToAccountType = objUserBranch.SameDayAccountType
                            End If
                        End If
                    End If

                    If objchkICInstruction.Status.ToString = "52" Then
                        Result = ICInstructionProcessController.InstructionClearing(InstructionID.ToString(), ToAccountNumber.ToString(), ToAccountBranchCode.ToString(), ToAccountCurrency.ToString(), ToSeqNo.ToString(), ToClientNo.ToString(), ToProfitCentre.ToString, ToAccountType.ToString, Me.UserId, Me.UserInfo.Username, PhysicalInstrumentNumber, objUserBranch.OfficeID.ToString, "30", objchkICInstruction.Status.ToString, ClearingType, TransferType)
                    Else
                        UIUtilities.ShowDialog(Me, "Instruction Clearing", "Selected instruction is already in processing.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If

                    objICInstruction = New ICInstruction
                    objICInstruction.LoadByPrimaryKey(InstructionID)

                    If (Result.ToString() = "OK" Or Result.ToString() = "Pending For Approval") Then

                        'UIUtilities.ShowDialog(Me, "Instruction Clearing", "Instruction processed successfully. [Status : " & Result.ToString() & "]", ICBO.IC.Dialogmessagetype.Success, NavigateURL())

                        '' For SMS and Email

                        If objICInstruction.Status.ToString = "52" Then
                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, LastStatus, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE")
                            UIUtilities.ShowDialog(Me, "Instruction Clearing", "Instruction not processed successfully. Please try again.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                            Exit Sub
                        End If


                        ArrayListInstructionID.Clear()
                        Dim ArryListPassStatus As ArrayList
                        ArrayListInstructionID.Add(InstructionID.ToString)
                        dtVerifiedInstruction = New DataTable
                        dtVerifiedInstruction = ICInstructionController.GetAllInstructionAccountAmountAndProductTypeWiseByArrayListInstructionIDAndStatusWithInstrumentNo(ArrayListInstructionID, "30")
                        If dtVerifiedInstruction.Rows.Count > 0 Then
                            ArryListPassStatus = New ArrayList
                            ArryListPassStatus.Add(30)
                            EmailUtilities.TransactionsCleared(dtVerifiedInstruction)
                            SMSUtilities.Transactionscleared(dtVerifiedInstruction, ArrayListInstructionID, ArryListPassStatus)

                        End If
                        dtVerifiedInstruction = New DataTable
                        dtVerifiedInstruction = ICInstructionController.GetAllInstructionAccountAmountAndProductTypeWiseByArrayListInstructionIDAndStatusWithInstrumentNoForClearingEmail(ArrayListInstructionID, "38")
                        If dtVerifiedInstruction.Rows.Count > 0 Then
                            ArryListPassStatus = New ArrayList
                            ArryListPassStatus.Add(38)
                            EmailUtilities.TransactionsAreReadyForClearing(dtVerifiedInstruction)
                            SMSUtilities.TransactionsreadyforClearing(dtVerifiedInstruction, ArrayListInstructionID, ArryListPassStatus)

                        End If

                        ''For summary dialog

                        objICInstructionCleared = New ICInstruction
                        objICInstructionCleared.LoadByPrimaryKey(InstructionID.ToString)

                        dr = dtSummary.NewRow
                        dr("InstructionID") = InstructionID.ToString
                        dr("Status") = objICInstructionCleared.Status
                        dr("Message") = "Instruction processed successfully. [Status : " & Result.ToString() & "]"
                        dtSummary.Rows.Add(dr)

                        If rbtnlstTransferType.SelectedValue.ToString() = "Funds Transfer" Then
                            objICInstructionCleared.ClearingType = "Funds Transfer"
                            objICInstructionCleared.BeneAccountBranchCode = txtBeneAccountBranchCode.Text
                            objICInstructionCleared.BeneAccountCurrency = txtBeneAccountCurrency.Text
                            objICInstructionCleared.BeneficiaryAccountNo = txtBeneAccountNo.Text
                            objICInstructionCleared.BeneficiaryAccountTitle = txtBeneAccountTitle.Text
                            'objICInstructionCleared.BeneficiaryName = txtBeneAccountTitle.Text
                            objICInstructionCleared.Save()
                        ElseIf rbtnlstTransferType.SelectedValue.ToString() = "Clearing" Then
                            If rbtnlstClearingType.SelectedValue.ToString() = "Normal" Then
                                objICInstructionCleared.ClearingType = "Normal"
                                objICInstructionCleared.Save()
                            ElseIf rbtnlstClearingType.SelectedValue.ToString() = "Intercity" Then
                                objICInstructionCleared.ClearingType = "Inter City"
                                objICInstructionCleared.Save()
                            ElseIf rbtnlstClearingType.SelectedValue.ToString() = "Same Day" Then
                                objICInstructionCleared.ClearingType = "Same Day"
                                objICInstructionCleared.Save()
                            End If
                        End If
                        PageLoad()

                        If dtSummary.Rows.Count > 0 Then
                            gvShowSummary.DataSource = Nothing
                            gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dtSummary)
                            gvShowSummary.DataBind()
                            pnlShowSummary.Style.Remove("display")
                            Dim scr As String
                            scr = "window.onload = function () {showsumdialog('Transaction Summary',true,850,200);};"
                            Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                            'Response.Redirect(NavigateURL(118))
                        Else
                            gvShowSummary.DataSource = Nothing
                            pnlShowSummary.Style.Add("display", "none")
                        End If

                    ElseIf Result = "Funds Not Available" Then
                        Dim dtChequeReturned As New DataTable
                        gvChequeReturned.DataSource = Nothing
                        ' gvChequeReturned.DataSource = ICInstructionController.GetInstructionDetailsByInstructionIDForChequeReturnd(InstructionID.ToString)
                        dtChequeReturned = ICInstructionController.GetInstructionDetailsByInstructionIDForChequeReturnd(InstructionID.ToString)

                        If dtChequeReturned.Rows.Count > 0 Then
                            Dim dt As New DataTable
                            dt = dtChequeReturned.Clone()
                            dt.Columns.Add("IsChequeReturned")

                            For Each drCHQ As DataRow In dtChequeReturned.Rows
                                dr = dt.NewRow
                                dr("InstructionID") = drCHQ("InstructionID")
                                dr("StatusName") = drCHQ("StatusName")
                                dr("CreateDate") = drCHQ("CreateDate")
                                dr("ValueDate") = drCHQ("ValueDate")
                                dr("BeneficiaryName") = drCHQ("BeneficiaryName")
                                dr("Amount") = drCHQ("Amount")
                                dr("ProductTypeCode") = drCHQ("ProductTypeCode")
                                dr("InstrumentNo") = drCHQ("InstrumentNo")
                                dr("OfficeName") = drCHQ("OfficeName")
                                dr("BeneficiaryAccountNo") = drCHQ("BeneficiaryAccountNo")
                                dr("IsChequeReturned") = True
                                dt.Rows.Add(dr)
                            Next
                            gvChequeReturned.DataSource = dt
                            gvChequeReturned.DataBind()
                        End If

                        If gvChequeReturned.Items.Count > 0 Then
                            btnChequeReturned.Visible = htRights("Clear")
                        Else
                            btnChequeReturned.Visible = False
                        End If
                        gvChequeReturned.DataBind()
                        pnlShowSummary1.Style.Remove("display")
                        tblOtherReason.Style.Add("display", "none")
                        tblChqReturn.Style.Remove("display")
                        PageLoad()


                        gvClearing.DataSource = Nothing
                        gvClearing.DataBind()
                        gvClearing.Visible = False

                        btnShowDetails.Visible = False
                        rfvddlChequeReturnedReason.Enabled = True
                        tblDetail.Style.Add("display", "none")
                        tblFundsTransfer.Style.Add("display", "none")
                        tblClearing.Style.Add("display", "none")
                        rimFundsTransfer.Enabled = False
                        'rimFundsTransfer.GetSettingByBehaviorID("radAccountno").Validation.IsRequired = False
                        'rimFundsTransfer.GetSettingByBehaviorID("radbeneaccounttitle").Validation.IsRequired = False
                        'rimFundsTransfer.GetSettingByBehaviorID("radbeneaccountbranchcode").Validation.IsRequired = False
                        'rimFundsTransfer.GetSettingByBehaviorID("radbeneaccountcurrency").Validation.IsRequired = False
                        rfvrbtnlstClearingType.Enabled = False
                        rimDetail.Enabled = False
                        rfvrbtnlstClearingType.Enabled = False
                        rimFundsTransfer.Enabled = False

                        btnClear.Visible = False
                        LoadddlChqReturn()
                    Else
                        objICInstruction = New ICInstruction
                        objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                        objICInstruction.ErrorMessage = Result
                        ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString(), "", "UPDATE")

                        'UIUtilities.ShowDialog(Me, "Instruction Clearing", "Instruction not processed successfully. [Status : " & Result.ToString() & "]", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        'objICInstructionCleared = New ICInstruction
                        'objICInstructionCleared.LoadByPrimaryKey(InstructionID.ToString)
                        dr = dtSummary.NewRow
                        dr("InstructionID") = InstructionID.ToString
                        dr("Status") = objICInstruction.Status
                        dr("Message") = "Instruction not processed successfully. [Status : " & Result.ToString() & "]"
                        dtSummary.Rows.Add(dr)
                        PageLoad()
                        If dtSummary.Rows.Count > 0 Then
                            gvShowSummary.DataSource = Nothing
                            gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dtSummary)
                            gvShowSummary.DataBind()
                            pnlShowSummary.Style.Remove("display")
                            Dim scr As String
                            scr = "window.onload = function () {showsumdialog('Transaction Summary',true,850,200);};"
                            Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                            'Response.Redirect(NavigateURL(118))
                        Else
                            gvShowSummary.DataSource = Nothing
                            pnlShowSummary.Style.Add("display", "none")
                        End If
                    End If
                Else
                    UIUtilities.ShowDialog(Me, "Instruction Clearing", "User Branch is not approved.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                End If
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub SetVisibilityOfCancelAndChequeReturned(ByVal dt As DataTable)
        Dim Result As Boolean = False
        For Each dr As DataRow In dt.Rows
            If dr("IsChequeReturned") = True Then
                Result = True
                'btnChequeReturned.Visible = CBool(htRights("Clear"))
                Exit For
            End If
        Next
        If Result = False Then
            'btnChequeReturned.Visible = False
        End If
    End Sub


    Protected Sub gvClearing_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvClearing.ItemDataBound

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim FileBatchNo As String = Nothing
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            If Not item.GetDataKeyValue("IsAmendmentComplete") Is Nothing Then
                If item.GetDataKeyValue("IsAmendmentComplete") = True Then

                    gvClearing.AlternatingItemStyle.CssClass = "GridItemComplete"
                    'gvAmendment.ItemStyle.BackColor = Color.Red
                    e.Item.CssClass = "GridItemComplete"


                Else
                    gvClearing.AlternatingItemStyle.CssClass = "GridItem"
                    'gvAmendment.ItemStyle.BackColor = Color.Red
                    e.Item.CssClass = "GridItem"


                End If
            Else
                gvClearing.AlternatingItemStyle.CssClass = "GridItem"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItem"


            End If
            FileBatchNo = GetFileBatchNoByInstructionID(item.GetDataKeyValue("InstructionID").ToString).ToString
            item.Cells(3).Text = FileBatchNo

        End If

    End Sub
    Public Shared Function GetFileBatchNoByInstructionID(ByVal InstructionID As String) As String
        Dim objICInstructionQuery As New ICInstructionQuery("objICInstructionQuery")
        Dim objICFilesQuery As New ICFilesQuery("objICFilesQuery")
        objICInstructionQuery.Select(objICInstructionQuery.FileBatchNo.Case.When(objICInstructionQuery.FileBatchNo = "SI").Then("SI").Else((objICInstructionQuery.FileBatchNo + " - " + objICFilesQuery.OriginalFileName)).End().As("FileBatchNo"))
        objICInstructionQuery.LeftJoin(objICFilesQuery).On(objICInstructionQuery.FileID = objICFilesQuery.FileID)
        objICInstructionQuery.Where(objICInstructionQuery.InstructionID = InstructionID)
        Return objICInstructionQuery.LoadDataTable.Rows(0)(0).ToString
    End Function
    Private Sub DesignDtForSummary()
        ViewState("Summary") = Nothing
        Dim dtSummary As New DataTable

        dtSummary.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
        dtSummary.Columns.Add(New DataColumn("Status", GetType(System.String)))
        dtSummary.Columns.Add(New DataColumn("Message", GetType(System.String)))
        ViewState("Summary") = dtSummary
    End Sub
    Private Function CheckGVChequeReturnedCheckedForProcessAll() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvChequeReturned.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    'Protected Sub btnChequeReturned_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChequeReturned.Click
    '    Try
    '        Dim objICInstruction As ICInstruction
    '        Dim chkSelect As CheckBox
    '        Dim ArryListInstructionId As New ArrayList
    '        Dim InstructionCount As Integer = 0
    '        ArryListInstructionId.Clear()
    '        Dim dtVerification As New DataTable
    '        Dim dtSummary As DataTable
    '        dtVerification.Rows.Clear()
    '        Dim StatusArray As String() = Nothing
    '        Dim StrAuditTrailAction As String = Nothing
    '        Dim dr As DataRow
    '        DesignDtForSummary()
    '        dtSummary = New DataTable
    '        dtSummary = DirectCast(ViewState("Summary"), DataTable)

    '        If CheckGVChequeReturnedCheckedForProcessAll() = False Then
    '            UIUtilities.ShowDialog(Me, "Instruction Clearing", "No instruction was selected", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(118))
    '            Exit Sub
    '        End If
    '        For Each gvVerificationRow As GridDataItem In gvChequeReturned.Items
    '            chkSelect = New CheckBox
    '            chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
    '            If chkSelect.Checked = True Then
    '                InstructionCount = InstructionCount + 1
    '                ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
    '            End If
    '        Next
    '        If ArryListInstructionId.Count > 0 Then

    '            objICInstruction = New ICInstruction
    '            objICInstruction.LoadByPrimaryKey(ArryListInstructionId(0).ToString)
    '            StrAuditTrailAction = Nothing
    '            StrAuditTrailAction += "Instruction with ID [ " & ArryListInstructionId(0) & " ] status update from [ " & objICInstruction.Status & " ] [ " & objICInstruction.UpToICInstructionStatusByStatus.StatusName & " ]"
    '            StrAuditTrailAction += " to [ 43 ] [ Cheque Returned ]. Action was taken by [ " & Me.UserInfo.UserID & " ] [ " & Me.UserInfo.Username & " ]."
    '            ICInstructionController.UpdateInstructionStatus(ArryListInstructionId(0).ToString, objICInstruction.Status.ToString, "43", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", StrAuditTrailAction)
    '            dr = dtSummary.NewRow
    '            dr("InstructionID") = ArryListInstructionId(0).ToString
    '            dr("Status") = "43"
    '            dr("Message") = "Instruction cheque returned successfully."
    '            dtSummary.Rows.Add(dr)

    '        End If
    '        If dtSummary.Rows.Count > 0 Then

    '            gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dtSummary)
    '            gvShowSummary.DataBind()
    '            pnlShowSummary.Style.Remove("display")
    '            Dim scr As String
    '            scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
    '            Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
    '            'Response.Redirect(NavigateURL(118))
    '        Else
    '            gvShowSummary.DataSource = Nothing
    '            pnlShowSummary.Style.Add("display", "none")
    '        End If


    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

    Protected Sub gvChequeReturned_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvChequeReturned.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvChequeReturned.MasterTableView.ClientID & "','0');"
        End If
        'If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
        '    Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        '    Dim chkSelect As New CheckBox
        '    chkSelect = DirectCast(item.Cells(0).FindControl("chkSelect"), CheckBox)
        '    If item.GetDataKeyValue("IsChequeReturned") = True Then
        '        chkSelect.Enabled = True

        '        Dim ddl As GridDataItem = DirectCast(e.Item, GridDataItem)
        '        Dim ddlChequeReturnedReason As DropDownList = DirectCast(ddl.FindControl("ddlChequeReturnedReason"), DropDownList)
        '        Dim lit As New ListItem
        '        lit.Value = "0"
        '        lit.Text = "-- Please Select --"
        '        ddlChequeReturnedReason.Items.Clear()
        '        ddlChequeReturnedReason.Items.Add(lit)
        '        ddlChequeReturnedReason.AppendDataBoundItems = True
        '        ddlChequeReturnedReason.DataSource = ICInstructionController.GetAllChequeReturnedReasons()
        '        ddlChequeReturnedReason.DataTextField = "ReturnReason"
        '        ddlChequeReturnedReason.DataValueField = "ReturnReason"
        '        ddlChequeReturnedReason.DataBind()
        '    Else
        '        Dim ddl As GridDataItem = DirectCast(e.Item, GridDataItem)
        '        Dim ddlChequeReturnedReason As DropDownList = DirectCast(ddl.FindControl("ddlChequeReturnedReason"), DropDownList)
        '        ddlChequeReturnedReason.Visible = False
        '    End If
        'End If
    End Sub
    Private Sub LoadddlChqReturn()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlChequeReturnedReason.Items.Clear()
            ddlChequeReturnedReason.Items.Add(lit)
            ddlChequeReturnedReason.AppendDataBoundItems = True
            ddlChequeReturnedReason.DataSource = ICInstructionController.GetAllChequeReturnedReasons()
            ddlChequeReturnedReason.DataTextField = "ReturnReason"
            ddlChequeReturnedReason.DataValueField = "ReturnReason"
            ddlChequeReturnedReason.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnCancelGrid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelGrid.Click
        If Page.IsValid Then

            Try

                Dim objICInstruction As ICInstruction
                Dim chkSelect As CheckBox
                Dim ArryListInstructionId As New ArrayList
                ArryListInstructionId.Clear()
                Dim StrAuditTrailAction As String = Nothing

                For Each gvVerificationRow As GridDataItem In gvChequeReturned.Items
                    objICInstruction = New ICInstruction
                    objICInstruction.LoadByPrimaryKey(gvVerificationRow.GetDataKeyValue("InstructionID").ToString)
                    StrAuditTrailAction = Nothing
                    StrAuditTrailAction += "Instruction with ID [ " & objICInstruction.InstructionID & " ] not cleared succesfully due to insufficient funds in client account [ " & objICInstruction.ClientAccountNo & " ]."
                    StrAuditTrailAction += " Action was taken by [ " & Me.UserInfo.UserID & " ] [ " & Me.UserInfo.Username & " ]."
                    ICUtilities.AddAuditTrail(StrAuditTrailAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "SKIP")
                Next
                Response.Redirect(NavigateURL(118), False)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try

        End If
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(NavigateURL(118), False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub txtBeneAccountNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBeneAccountNo.TextChanged
        Try
            Dim StrAccountDetails As String()
            Dim AccountStatus As String = ""
            If Not txtBeneAccountNo.Text.ToString = "" And Not txtBeneAccountNo.Text.ToString = "Enter Beneficiary Account Number" Then
                Try
                    AccountStatus = CBUtilities.AccountStatus(txtBeneAccountNo.Text.ToString, ICBO.CBUtilities.AccountType.RB, "Beneficiary Account Status", txtBeneAccountNo.ToString, "Credit", "Clearing", "").ToString
                Catch ex As Exception
                    UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End Try
                If AccountStatus = "Active" Then
                    Try
                        StrAccountDetails = (CBUtilities.TitleFetch(txtBeneAccountNo.Text.ToString, ICBO.CBUtilities.AccountType.RB, "Beneficiary Account Title", txtBeneAccountNo.Text.ToString).ToString.Split(";"))
                        txtBeneAccountTitle.Text = StrAccountDetails(0).ToString
                        txtBeneAccountCurrency.Text = StrAccountDetails(2).ToString
                        txtBeneAccountBranchCode.Text = StrAccountDetails(1).ToString
                    Catch ex As Exception
                        UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End Try

                Else
                    UIUtilities.ShowDialog(Me, "Error", AccountStatus.ToString, ICBO.IC.Dialogmessagetype.Failure)
                    txtBeneAccountNo.Text = ""
                    txtBeneAccountCurrency.Text = ""
                    txtBeneAccountTitle.Text = ""
                    txtBeneAccountBranchCode.Text = ""
                    Exit Sub
                End If
            Else

                'UIUtilities.ShowDialog(Me, "Error", "Please enter valid account number.", ICBO.IC.Dialogmessagetype.Failure)
                txtBeneAccountNo.Text = ""
                txtBeneAccountCurrency.Text = ""
                txtBeneAccountTitle.Text = ""
                txtBeneAccountBranchCode.Text = ""
                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub



    Protected Sub rbtnlstProductType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbtnlstProductType.SelectedIndexChanged
        Try
            rbtnlstClearingType.ClearSelection()
            rbtnlstNumberType.ClearSelection()
            pnlShowSummary1.Style.Add("display", "none")
            tblDetail.Style.Add("display", "none")
            tblFundsTransfer.Style.Add("display", "none")
            tblClearing.Style.Add("display", "none")
            rimFundsTransfer.Enabled = True
            rimDetail.Enabled = False
            'rimFundsTransfer.GetSettingByBehaviorID("radAccountno").Validation.IsRequired = False
            'rimFundsTransfer.GetSettingByBehaviorID("radbeneaccounttitle").Validation.IsRequired = False
            'rimFundsTransfer.GetSettingByBehaviorID("radbeneaccountbranchcode").Validation.IsRequired = False
            'rimFundsTransfer.GetSettingByBehaviorID("radbeneaccountcurrency").Validation.IsRequired = False
            rfvrbtnlstClearingType.Enabled = False
            rbtnlstClearingType.ClearSelection()
            txtNumber.Text = ""
            gvClearing.DataSource = Nothing
            gvClearing.DataBind()
            gvClearing.Visible = False
            btnClear.Visible = False
            btnShowDetails.Visible = True
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub ddlChequeReturnedReason_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlChequeReturnedReason.SelectedIndexChanged
        Try
            If ddlChequeReturnedReason.SelectedValue.ToString = "Other" Then
                tblOtherReason.Style.Remove("display")
                radChqReturn.Enabled = True
                txtChqRetReason.Text = ""
            Else
                tblOtherReason.Style.Add("display", "none")
                radChqReturn.Enabled = False
                txtChqRetReason.Text = ""
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnChequeReturned_Click(sender As Object, e As EventArgs) Handles btnChequeReturned.Click
        If Page.IsValid Then
            Try

                Dim objICInstruction As ICInstruction
                Dim chkSelect As CheckBox
                Dim ArryListInstructionId As New ArrayList
                Dim InstructionCount As Integer = 0
                ArryListInstructionId.Clear()
                Dim dtVerification As New DataTable
                Dim dtSummary As DataTable
                dtVerification.Rows.Clear()
                Dim StatusArray As String() = Nothing
                Dim StrAuditTrailAction As String = Nothing
                Dim dr As DataRow
                DesignDtForSummary()
                dtSummary = New DataTable
                dtSummary = DirectCast(ViewState("Summary"), DataTable)

                If CheckGVChequeReturnedCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Instruction Clearing", "No instruction was selected", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(118))
                    Exit Sub
                End If
                For Each gvVerificationRow As GridDataItem In gvChequeReturned.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        InstructionCount = InstructionCount + 1
                        ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                    End If
                Next
                If ArryListInstructionId.Count > 0 Then

                    objICInstruction = New ICInstruction
                    objICInstruction.LoadByPrimaryKey(ArryListInstructionId(0).ToString)



                    If ddlChequeReturnedReason.SelectedValue.ToString <> "Other" And ddlChequeReturnedReason.SelectedValue.ToString <> "0" Then
                        objICInstruction.ReturnReason = ddlChequeReturnedReason.SelectedValue.ToString
                    ElseIf ddlChequeReturnedReason.SelectedValue.ToString = "Other" Then
                        objICInstruction.ReturnReason = txtChqRetReason.Text
                    Else
                        objICInstruction.ReturnReason = Nothing
                    End If
                    StrAuditTrailAction = Nothing
                    StrAuditTrailAction += "Instruction with ID [ " & ArryListInstructionId(0) & " ] status update from [ " & objICInstruction.Status & " ] [ " & objICInstruction.UpToICInstructionStatusByStatus.StatusName & " ]"
                    StrAuditTrailAction += " to [ 43 ] [ Cheque Returned ]. Action was taken by [ " & Me.UserInfo.UserID & " ] [ " & Me.UserInfo.Username & " ]."
                    ICInstructionController.UpDateInstruction(objICInstruction, Me.UserInfo.UserID, Me.UserInfo.Username, StrAuditTrailAction, "Update")

                    ICInstructionController.UpdateInstructionStatus(ArryListInstructionId(0).ToString, objICInstruction.Status.ToString, "43", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", StrAuditTrailAction)

                    UIUtilities.ShowDialog(Me, "Instruction Clearing", "Instruction marked as cheque returned successfully", Dialogmessagetype.Success, NavigateURL())
                    Exit Sub
                    'dr = dtSummary.NewRow
                    'dr("InstructionID") = ArryListInstructionId(0).ToString
                    'dr("Status") = "43"
                    'dr("Message") = "Instruction cheque returned successfully."
                    'dtSummary.Rows.Add(dr)

                End If
                'If dtSummary.Rows.Count > 0 Then

                '    gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dtSummary)
                '    gvShowSummary.DataBind()
                '    pnlShowSummary.Style.Remove("display")
                '    Dim scr As String
                '    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                '    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                '    'Response.Redirect(NavigateURL(118))
                'Else
                '    gvShowSummary.DataSource = Nothing
                '    pnlShowSummary.Style.Add("display", "none")
                'End If


            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub


End Class
