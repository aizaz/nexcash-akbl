﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_CollectionValueDateClearingProcessManual_CollectionValueDateClearingProcessManual
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable

#Region "Page Load"
    Protected Sub DesktopModules_CompanyOfficeManagement_ViewCompanyOffice_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'If Not ViewState("htRights") Is Nothing Then
            '    htRights = DirectCast(ViewState("htRights"), Hashtable)
            'End If

            If Page.IsPostBack = False Then
                'ViewState("htRights") = Nothing
                'GetPageAccessRights()
                If ICUtilities.GetCollectionSettingValue("ValueDateClearingType") = "Auto" Then
                    UIUtilities.ShowDialog(Me, "Warning", "Clearing process is set to auto in settings.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
                End If
                txtValueDate.Enabled = True
                DirectCast(Me.Control.FindControl("trMain"), HtmlTableRow).Style.Remove("display")
                DirectCast(Me.Control.FindControl("trProceed"), HtmlTableRow).Style.Add("display", "none")
                'btnExecute.Visible = CBool(htRights("Execute"))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Value Date Clearing")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                txtValueDate.Enabled = False
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))

                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Drop Down"
#End Region

#Region "Button Events"
#End Region
    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Try
            Response.Redirect(NavigateURL(41))
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnBack0_Click(sender As Object, e As EventArgs) Handles btnBack2.Click
        Try
            Response.Redirect(NavigateURL())
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnExecute_Click(sender As Object, e As EventArgs) Handles btnExecute.Click
        Try
            Dim dt As New DataTable
            If ICValueDateClearingHistoryController.IsClearingProcessExecutedByDate(txtValueDate.SelectedDate) = True Then
                UIUtilities.ShowDialog(Me, "Warning", "Value Date for selected date [ " & txtValueDate.SelectedDate & " ] is already executed", ICBO.IC.Dialogmessagetype.Warning)
                DirectCast(Me.Control.FindControl("trProceed"), HtmlTableRow).Style.Remove("display")
                DirectCast(Me.Control.FindControl("trMain"), HtmlTableRow).Style.Add("display", "none")
                txtValueDate.Enabled = False
                Exit Sub
            End If
            ICValueDateClearingHistoryController.ProcessValueDateClearing(Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Manual", dt)


            If dt.Rows.Count > 0 Then
                gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummaryForValueDateClearing(dt)
                gvShowSummary.DataBind()
                pnlShowSummary.Style.Remove("display")
                Dim scr As String
                scr = "window.onload = function () {showsumdialog('Value Date Clearing Summary',true,500,300);};"
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                txtValueDate.Enabled = True
                DirectCast(Me.Control.FindControl("trMain"), HtmlTableRow).Style.Remove("display")
                DirectCast(Me.Control.FindControl("trProceed"), HtmlTableRow).Style.Add("display", "none")
            Else
                gvShowSummary.DataSource = Nothing
                pnlShowSummary.Style.Add("display", "none")
                txtValueDate.Enabled = True
                DirectCast(Me.Control.FindControl("trMain"), HtmlTableRow).Style.Remove("display")
                DirectCast(Me.Control.FindControl("trProceed"), HtmlTableRow).Style.Add("display", "none")
            End If
           

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnProceed_Click(sender As Object, e As EventArgs) Handles btnProceed.Click
        Try
            Dim dt As New DataTable
            ICValueDateClearingHistoryController.ProcessValueDateClearing(Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Manual", dt)


            If dt.Rows.Count > 0 Then
                gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummaryForValueDateClearing(dt)
                gvShowSummary.DataBind()
                pnlShowSummary.Style.Remove("display")
                Dim scr As String
                scr = "window.onload = function () {showsumdialog('Value Date Clearing Summary',true,500,300);};"
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                txtValueDate.Enabled = True
                DirectCast(Me.Control.FindControl("trMain"), HtmlTableRow).Style.Remove("display")
                DirectCast(Me.Control.FindControl("trProceed"), HtmlTableRow).Style.Add("display", "none")
            Else
                gvShowSummary.DataSource = Nothing
                pnlShowSummary.Style.Add("display", "none")
                txtValueDate.Enabled = True
                DirectCast(Me.Control.FindControl("trMain"), HtmlTableRow).Style.Remove("display")
                DirectCast(Me.Control.FindControl("trProceed"), HtmlTableRow).Style.Add("display", "none")
            End If
            'Response.Redirect(NavigateURL())
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Try
            'txtValueDate.Enabled = True
            'DirectCast(Me.Control.FindControl("trMain"), HtmlTableRow).Style.Remove("display")
            'DirectCast(Me.Control.FindControl("trProceed"), HtmlTableRow).Style.Add("display", "none")
            Response.Redirect(NavigateURL())
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
