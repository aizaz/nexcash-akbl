﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Partial Class DesktopModules_DDSubSetManagement_AssignDDSubSetToOffice
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase

    Private DDMasterSeriesID, OfficeCode, SubSetFrom, SubSetTo As String
    Private htRights As Hashtable
    Protected Sub DesktopModules_POSubSetManagement_AssignPOSubSetToOffice_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            DDMasterSeriesID = Request.QueryString("DDMasterSeriesID").ToString
            OfficeCode = Request.QueryString("Officecode").ToString
            SubSetFrom = Request.QueryString("SubSetFrom").ToString
            SubSetTo = Request.QueryString("SubSetTo").ToString
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlBankBranch()
                LoadddlMasterSeries()

                If DDMasterSeriesID.ToString = "0" And OfficeCode.ToString = "0" And SubSetFrom.ToString = "0" And SubSetTo.ToString = "0" Then
                    lblPageHeader.Text = "Add DD Sub Set"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    btnSave.Visible = CBool(htRights("Update"))
                    lblPageHeader.Text = "Update DD Sub Set"
                    btnSave.Text = "Update"
                    Dim objICDDInstrument As New ICDDInstruments
                    Dim objICOffice As New ICOffice
                    Dim objICDDMasterSeries As New ICDDMasterSeries


                    objICDDInstrument.es.Connection.CommandTimeout = 3600
                    objICOffice.es.Connection.CommandTimeout = 3600
                    objICDDMasterSeries.es.Connection.CommandTimeout = 3600

                    ddlOffice.Enabled = False

                    ddlMasterSeries.Enabled = False
                    txtSubSeriesFrom.ReadOnly = True
                    objICDDInstrument = ICDDInstrumentsController.GetSingleDDInstrumentObjectByPOIDOFFiceCodeAndsubSetRange(DDMasterSeriesID, SubSetFrom, SubSetTo, OfficeCode)
                    objICOffice.LoadByPrimaryKey(objICDDInstrument.OfficeCode.ToString)
                    LoadddlBankBranch()
                    ddlOffice.SelectedValue = objICOffice.OfficeID
                    LoadddlMasterSeries()
                    ddlMasterSeries.SelectedValue = objICDDInstrument.DDMasterSeriesID
                    objICDDMasterSeries.LoadByPrimaryKey(ddlMasterSeries.SelectedValue.ToString)
                    RangeValidator1.MinimumValue = CLng(objICDDMasterSeries.StartFrom)
                    RangeValidator1.MaximumValue = CLng(objICDDMasterSeries.EndsAt - 1)
                    rvSubSetTo.MinimumValue = CLng(objICDDMasterSeries.StartFrom + 1)
                    rvSubSetTo.MaximumValue = CLng(objICDDMasterSeries.EndsAt)
                    txtSubSeriesFrom.Text = objICDDInstrument.SubsetFrom
                    txtSubSeriesTo.Text = objICDDInstrument.SubsetTo
                End If


            End If


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "DD Sub Set Management")
            ViewState("htRights") = htRights

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlBankBranch()
        Try
            Dim lit As New ListItem

            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlOffice.Items.Clear()
            ddlOffice.Items.Add(lit)
            ddlOffice.AppendDataBoundItems = True

            ddlOffice.DataSource = ICOfficeController.GetPrincipalBankBranchActiveAndApprovePrintingLocations()
            ddlOffice.DataTextField = "OfficeName"
            ddlOffice.DataValueField = "OfficeID"
            ddlOffice.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlMasterSeries()
        Try

            Dim lit As New ListItem
            Dim dt As New DataTable

            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlMasterSeries.Items.Clear()
            ddlMasterSeries.Items.Add(lit)
            ddlMasterSeries.AppendDataBoundItems = True
            For Each MSItem In ICDDMasterSeriesController.GetAllDDMasterSeries()
                ddlMasterSeries.Items.Add(New ListItem(MSItem.StartFrom & "-" & MSItem.EndsAt, MSItem.DDMasterSeriesID))
            Next

            ddlMasterSeries.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL(), False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim objICDDInstrument As New ICDDInstruments
                Dim objICDDUInstrumentColl As New ICDDInstrumentsCollection
                Dim objICDDUInstrumentCollForSave As New ICDDInstrumentsCollection
                Dim objICOFficeTo As New ICOffice
                Dim objICDDInstrumentFrom As New ICDDInstruments
                Dim ArrStrForAccount As String()
                Dim SavedSubSetID As Integer = 0
                Dim AssignedLeaves As Long = 0
                Dim UpDatedLeaves As Integer = 0
                Dim TopOrder As Integer = 0
                Dim StrAuditTrail As String = Nothing
                Dim objICAT As New ICAuditTrail
                ArrStrForAccount = Nothing

                'If IsSubSetExistsInMasterSeries(ddlMasterSeries.SelectedValue.ToString, txtSubSeriesFrom.Text.ToString, txtSubSeriesTo.Text.ToString) = False Then
                '    UIUtilities.ShowDialog(Me, "Error", "Sub set not exists", ICBO.IC.Dialogmessagetype.Failure)
                '    Exit Sub
                'End If
                objICDDInstrument.es.Connection.CommandTimeout = 3600
                objICDDInstrumentFrom.es.Connection.CommandTimeout = 3600


                objICDDInstrument.SubsetFrom = CLng(txtSubSeriesFrom.Text)
                objICDDInstrument.SubsetTo = CLng(txtSubSeriesTo.Text)
                objICDDInstrument.OfficeCode = ddlOffice.SelectedValue.ToString
                objICDDInstrument.DDMasterSeriesID = ddlMasterSeries.SelectedValue.ToString

                If DDMasterSeriesID.ToString = "0" And OfficeCode.ToString = "0" And SubSetFrom.ToString = "0" And SubSetTo.ToString = "0" Then
                    If ICDDInstrumentsController.IsDDInstrumentSeriesIsAssignedOrPrintedForSubSet(objICDDInstrument.SubsetFrom, objICDDInstrument.SubsetTo, objICDDInstrument.DDMasterSeriesID) = False Then
                        objICDDInstrument.CreatedDate = Date.Now
                        objICDDInstrument.CreatedBy = Me.UserId
                        objICDDInstrument.CreationDate = Date.Now
                        objICDDInstrument.Creater = Me.UserId
                        objICDDUInstrumentColl = ICDDInstrumentsController.GetDDInstrumentsForSubSetByDDMasterSeriesIDAndSubSetRange(ddlMasterSeries.SelectedValue.ToString, objICDDInstrument.SubsetFrom, objICDDInstrument.SubsetTo)
                        For Each objICDDInstruments As ICDDInstruments In objICDDUInstrumentColl
                            objICDDInstruments.SubsetFrom = objICDDInstrument.SubsetFrom
                            objICDDInstruments.SubsetTo = objICDDInstrument.SubsetTo
                            objICDDInstruments.OfficeCode = objICDDInstrument.OfficeCode
                            objICDDUInstrumentCollForSave.Add(objICDDInstruments)
                        Next
                        If objICDDUInstrumentCollForSave.Count > 0 Then
                            objICDDUInstrumentCollForSave.Save()
                            objICOFficeTo.LoadByPrimaryKey(ddlOffice.SelectedValue.ToString)
                            StrAuditTrail = Nothing
                            StrAuditTrail = "DD Sub set is assigned to branch [ " & objICOFficeTo.OfficeCode & " ] [ " & objICOFficeTo.OfficeName & " ]"
                            StrAuditTrail += " from [ " & objICDDInstrument.SubsetFrom & " ] to [ " & objICDDInstrument.SubsetTo & " ] of DD series [ " & ddlMasterSeries.SelectedValue & " ] ."
                            ICUtilities.AddAuditTrail(StrAuditTrail, "DD Sub Set", ddlOffice.SelectedValue.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ASSIGN")
                        End If
                        UIUtilities.ShowDialog(Me, "Save Sub Set", "Sub set assigned to office successfully.", ICBO.IC.Dialogmessagetype.Success)
                        clear()

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Part of subset is already assigned to another branch", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else
                    objICDDInstrumentFrom = ICDDInstrumentsController.GetSingleDDInstrumentObjectByPOIDOFFiceCodeAndsubSetRange(DDMasterSeriesID, SubSetFrom, SubSetTo, OfficeCode)
                    objICDDInstrument.CreatedBy = Me.UserId
                    objICDDInstrument.CreationDate = Date.Now
                    AssignedLeaves = CLng(CLng(objICDDInstrumentFrom.SubsetTo) - CLng(objICDDInstrumentFrom.SubsetFrom))
                    UpDatedLeaves = CLng(CLng(objICDDInstrument.SubsetTo) - CLng(objICDDInstrument.SubsetFrom))
                    If UpDatedLeaves < AssignedLeaves Then
                        TopOrder = AssignedLeaves - UpDatedLeaves
                        If ICDDInstrumentsController.IsDDInstrumentsExistForUpdateOfDDSubSet(TopOrder, objICDDInstrument, objICDDInstrumentFrom.SubsetTo.ToString, False) = True Then
                            ICDDInstrumentsController.UpDateDDInstrumentsForUpdateOfSubSet(TopOrder, objICDDInstrument, objICDDInstrumentFrom.SubsetTo, Me.UserId.ToString, Me.UserInfo.Username.ToString, False)
                            objICDDInstrumentFrom = New ICDDInstruments
                            objICDDInstrumentFrom = ICDDInstrumentsController.GetSingleDDInstrumentObjectByPOIDOFFiceCodeAndsubSetRange(DDMasterSeriesID, txtSubSeriesFrom.Text.ToString, txtSubSeriesTo.Text.ToString, OfficeCode)
                            ICDDInstrumentsController.UpDateSubSetRangeOnUpDate(objICDDInstrumentFrom.DDMasterSeriesID, CLng(txtSubSeriesFrom.Text), CLng(txtSubSeriesTo.Text.ToString), objICDDInstrumentFrom.OfficeCode.ToString, objICDDInstrument)

                            UIUtilities.ShowDialog(Me, "Save Sub Set", "Sub set updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Part of subset is already assigned to another location or sub set is already used.", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    ElseIf UpDatedLeaves > AssignedLeaves Then
                        TopOrder = ((AssignedLeaves - UpDatedLeaves) * (-1))
                        If ICDDInstrumentsController.IsDDInstrumentsExistForUpdateOfDDSubSet(TopOrder, objICDDInstrument, objICDDInstrumentFrom.SubsetTo.ToString, True) = True Then
                            ICDDInstrumentsController.UpDateDDInstrumentsForUpdateOfSubSet(TopOrder, objICDDInstrument, objICDDInstrumentFrom.SubsetTo.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, True)
                            objICDDInstrumentFrom = New ICDDInstruments
                            objICDDInstrumentFrom = ICDDInstrumentsController.GetSingleDDInstrumentObjectByPOIDOFFiceCodeAndsubSetRange(DDMasterSeriesID, txtSubSeriesFrom.Text.ToString, txtSubSeriesTo.Text.ToString, OfficeCode)
                            ICDDInstrumentsController.UpDateSubSetRangeOnUpDate(objICDDInstrumentFrom.DDMasterSeriesID, CLng(txtSubSeriesFrom.Text), CLng(txtSubSeriesTo.Text.ToString), objICDDInstrumentFrom.OfficeCode.ToString, objICDDInstrumentFrom)
                            UIUtilities.ShowDialog(Me, "Save Sub Set", "Sub set updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Part of subset is already assigned to another location", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                            Exit Sub
                        End If
                    ElseIf UpDatedLeaves = AssignedLeaves Then
                        UIUtilities.ShowDialog(Me, "Save Sub Set", "No changes made in sub set", ICBO.IC.Dialogmessagetype.Failure, NavigateURL)
                        Exit Sub

                    End If
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub
    Private Sub clear()
        LoadddlMasterSeries()
        LoadddlBankBranch()
        txtSubSeriesFrom.Text = ""
        txtSubSeriesTo.Text = ""

    End Sub

    Protected Sub ddlMasterSeries_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMasterSeries.SelectedIndexChanged
        If ddlMasterSeries.SelectedValue.ToString <> "0" Then
            Dim objICMasterSeries As New ICDDMasterSeries

            objICMasterSeries.LoadByPrimaryKey(ddlMasterSeries.SelectedValue.ToString)

            RangeValidator1.MinimumValue = CLng(objICMasterSeries.StartFrom)
            RangeValidator1.MaximumValue = CLng(objICMasterSeries.EndsAt - 1)
            rvSubSetTo.MinimumValue = CLng(objICMasterSeries.StartFrom + 1)
            rvSubSetTo.MaximumValue = CLng(objICMasterSeries.EndsAt)
        ElseIf ddlMasterSeries.SelectedValue.ToString = "0" Then
            RangeValidator1.MinimumValue = 0
            RangeValidator1.MaximumValue = 0
            rvSubSetTo.MinimumValue = 0
            rvSubSetTo.MaximumValue = 0
        End If
    End Sub
End Class
