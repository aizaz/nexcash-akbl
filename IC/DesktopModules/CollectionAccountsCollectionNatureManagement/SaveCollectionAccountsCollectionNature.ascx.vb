﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals

Partial Class DesktopModules_CollectionAccountsCollectionNatureManagement_SaveCollectionAccountsCollectionNature
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private AccountPaymentNatureProductTypeCode As String
    Private htRights As Hashtable


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            AccountPaymentNatureProductTypeCode = Request.QueryString("APNPTCode").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                If AccountPaymentNatureProductTypeCode.ToString() = "0" Then
                    lblPageHeader.Text = "Add Collection Account, Collection Nature"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                End If
                LoadddlGroup()
                LoadddlCompany()
                LoadddlAccounts()
                LoadchklstCollectionNature()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Collection Accounts Collection Nature Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            Dim dt As DataTable
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            dt = ICGroupController.GetAllActiveGroupsIsCollectionAllowonCompany()
            ddlGroup.DataSource = dt
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlcompany.Items.Clear()
            ddlcompany.Items.Add(lit)
            ddlcompany.AppendDataBoundItems = True
            ddlcompany.DataSource = ICCompanyController.GetAllActiveandApproveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString)
            ddlcompany.DataTextField = "CompanyName"
            ddlcompany.DataValueField = "CompanyCode"
            ddlcompany.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccounts()
        Try
            Dim lit As New ListItem
            Dim objCollectionAccount As New ICCollectionAccounts
            Dim collCollectionAccounts As New ICCollectionAccountsCollection

            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlAccount.Items.Clear()
            ddlAccount.Items.Add(lit)
            collCollectionAccounts = ICCollectionAccountsController.GetAllCollectionAccountsByCompanyCode(ddlCompany.SelectedValue.ToString())

            For Each objCollectionAccount In collCollectionAccounts
                lit = New ListItem
                lit.Text = objCollectionAccount.AccountTitle.ToString() & " - " & objCollectionAccount.AccountNumber.ToString()
                lit.Value = objCollectionAccount.AccountNumber.ToString() & ";" & objCollectionAccount.BranchCode.ToString() & ";" & objCollectionAccount.Currency.ToString()
                ddlAccount.Items.Add(lit)
            Next
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadchklstCollectionNature()
        Try
            chklstPaymentNature.DataSource = ICCollectionNatureController.GetAllActiveCollectionNatures(ddlCompany.SelectedValue.ToString())
            chklstPaymentNature.DataTextField = "CollectionNatureName"
            chklstPaymentNature.DataValueField = "CollectionNatureCode"
            chklstPaymentNature.DataBind()

            If chklstPaymentNature.Items.Count > 0 Then
                pnlPaymentNature.Visible = True
                chklstPaymentNature.Visible = True
                lblPaymentNatue.Visible = True
                lblPaymentNatueReq.Visible = True
                regCollNature.Enabled = True
            Else
                pnlPaymentNature.Visible = False
                regCollNature.Enabled = False
                chklstPaymentNature.Visible = False
                lblPaymentNatue.Visible = False
                lblPaymentNatueReq.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        LoadddlCompany()
        LoadddlAccounts()
        LoadchklstCollectionNature()
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        LoadddlAccounts()
        LoadchklstCollectionNature()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim lit As New ListItem
                Dim objAccountCollectionNature As ICCollectionAccountsCollectionNature
                Dim objchkAccountCollectionNature As ICCollectionAccountsCollectionNature
                Dim objICCompany As New ICCompany
                Dim StrActionForCompany As String = Nothing
                objICCompany.es.Connection.CommandTimeout = 3600
                Dim ActionText As String = ""

                For Each lit In chklstPaymentNature.Items
                    If lit.Selected = True Then
                        objAccountCollectionNature = New ICCollectionAccountsCollectionNature
                        objchkAccountCollectionNature = New ICCollectionAccountsCollectionNature
                        objAccountCollectionNature.AccountNumber = ddlAccount.SelectedValue.ToString().Split(";")(0).ToString()
                        objAccountCollectionNature.BranchCode = ddlAccount.SelectedValue.ToString().Split(";")(1).ToString()
                        objAccountCollectionNature.Currency = ddlAccount.SelectedValue.ToString().Split(";")(2).ToString()
                        objAccountCollectionNature.CollectionNatureCode = lit.Value.ToString()
                        objAccountCollectionNature.CreateBy = Me.UserId
                        objAccountCollectionNature.CreateDate = Date.Now
                        ActionText = "Collection Account [Account Number : " & ddlAccount.SelectedValue.ToString().Split(";")(0).ToString() & " ; Branch Code : " & ddlAccount.SelectedValue.ToString().Split(";")(1).ToString() & " ; Currency : " & ddlAccount.SelectedValue.ToString().Split(";")(2).ToString() & "] and Collection Nature [Code : " & lit.Value.ToString() & " ; Name : " & lit.Text.ToString() & "] tagged successfully."
                        If objchkAccountCollectionNature.LoadByPrimaryKey(ddlAccount.SelectedValue.ToString().Split(";")(0).ToString(), ddlAccount.SelectedValue.ToString().Split(";")(1).ToString(), ddlAccount.SelectedValue.ToString().Split(";")(2).ToString(), lit.Value.ToString()) = False Then
                            ICCollectionAccountsCollectionNatureController.AddCollectionAccountCollectionNature(objAccountCollectionNature, ActionText.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        End If
                    End If
                Next
                If Not AccountPaymentNatureProductTypeCode.ToString() = "0" Then
                    objICCompany.LoadByPrimaryKey(ddlCompany.SelectedValue.ToString)
                    objICCompany.IsApprove = False
                    StrActionForCompany = Nothing
                    StrActionForCompany += "Compnay [ " & objICCompany.CompanyName & " ] is unapproved on updating collection account, collection nature. Action was taken by user [ " & Me.UserId.ToString & " ] [ " & Me.UserInfo.Username.ToString & " ]"
                    ICCompanyController.AddCompany(objICCompany, True, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrActionForCompany)
                End If
                UIUtilities.ShowDialog(Me, "Save Collection Account Collection Nature", "Collection Account and Collection Natures are tagged successfully.", ICBO.IC.Dialogmessagetype.Success)
                Clear()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Sub Clear()
        LoadddlGroup()
        LoadddlCompany()
        LoadddlAccounts()
        LoadchklstCollectionNature()
    End Sub


End Class
