﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveCollectionAccountsCollectionNature.ascx.vb" 
Inherits="DesktopModules_CollectionAccountsCollectionNatureManagement_SaveCollectionAccountsCollectionNature" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }


    function ValidateChkList(source, arguments) {

        arguments.IsValid = IsCheckBoxChecked() ? true : false;



    }

    function ValidatePaymentNatureList(source, args) {
        var chkListModules = document.getElementById('<%= chklstPaymentNature.ClientID %>');
        var chkListinputs = chkListModules.getElementsByTagName("input");
        for (var i = 0; i < chkListinputs.length; i++) {
            if (chkListinputs[i].checked) {
                args.IsValid = true;
                return;
            }
        }
        args.IsValid = false;
    }



  


</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:TextBoxSetting BehaviorID="reName" Validation-IsRequired="true" ErrorMessage="Invalid Account Title"
        EmptyMessage="Enter Account Title">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior3" Validation-IsRequired="true"
        ErrorMessage="Invalid Account No." EmptyMessage="Enter Account Number">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountNumber" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehaviorCurrency" Validation-IsRequired="true"
        ErrorMessage="Invalid Currency" EmptyMessage="Enter Currency">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountCurrency" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior2" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9 .-_/]{2,20}$" ErrorMessage="Invalid Area Code"
        EmptyMessage="Enter Area Code">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBranchCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label8" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 28%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 22%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlGroup" runat="server" ControlToValidate="ddlGroup"
                Display="Dynamic" ErrorMessage="Please select Group" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 33%">
            <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" ControlToValidate="ddlCompany"
                Display="Dynamic" ErrorMessage="Please select Company" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 17%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAccount" runat="server" Text="Select Account" CssClass="lbl"></asp:Label><asp:Label
                ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblPaymentNatue" runat="server" Text="Select Collection Nature" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblPaymentNatueReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlAccount" runat="server" CssClass="dropdown">
            </asp:DropDownList>
            <br />
            <asp:RequiredFieldValidator ID="rfvddlAccount" runat="server" ControlToValidate="ddlAccount"
                Display="Dynamic" ErrorMessage="Please select Collection Account" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Panel ID="pnlPaymentNature" runat="server" Width="100%" ScrollBars="Vertical"
                Height="100px">
                <asp:CheckBoxList ID="chklstPaymentNature" runat="server" CssClass="chkBox" 
                    Width="90%" AutoPostBack="True">
                </asp:CheckBoxList>
            </asp:Panel>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:CustomValidator runat="server" ID="regCollNature" ClientValidationFunction="ValidatePaymentNatureList"
               ErrorMessage="Please Select Atleast one Collection Nature"></asp:CustomValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" CausesValidation="False"
                Width="75px" />
            &nbsp;
        </td>
    </tr>
</table>
