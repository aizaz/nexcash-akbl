﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewCollectionAccountsCollectionNature.ascx.vb"
 Inherits="DesktopModules_CollectionAccountsCollectionNatureManagement_ViewCollectionAccountsCollectionNature" %>

<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure to remove this record ?") == true) {

            return true;
        }
        else {
            return false;
        }
    }
    function delcon(item) {


        if (window.confirm("Are you sure to remove this " + item + "?") == true) {

            return true;

        }
        else {

            return false;

        }
    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to remove Record(s)?") == true) {
            return true;
        }
        else {
            return false;
        }
    }  
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
            }
        }
    }
</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table width="100%">
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnSaveAccPayNatProTyp" runat="server" Text="Add Collection Account Collection Nature"
                            CssClass="btn" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle">
                        <asp:Label ID="lblAccountPaymentNatureProductTypeList" runat="server" Text="Collection Account, Collection Nature List"
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle">
                        <table width="100%">
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 28%">
                                    <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 22%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="left">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="left">
                        <asp:Label ID="lblBankRNF" runat="server" Text="Record Not Found." CssClass="headingblue"
                            Visible="false"></asp:Label>
                        <br />
                        <telerik:RadGrid ID="gvAccountPaymentNature" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="10">
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView DataKeyNames="AccountNumber, BranchCode, Currency, CollectionNatureCode">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Select">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                     <telerik:GridBoundColumn DataField="GroupName" HeaderText="Group" SortExpression="GroupName">
                                    </telerik:GridBoundColumn>
                                     <telerik:GridBoundColumn DataField="CompanyName" HeaderText="Company" SortExpression="CompanyName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="AccountTitle" HeaderText="Account Title" SortExpression="AccountTitle">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="Account Number" SortExpression="AccountNumber">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CollectionNatureName" HeaderText="Collection Nature"
                                        SortExpression="CollectionNatureName">
                                    </telerik:GridBoundColumn>
                                   <telerik:GridTemplateColumn HeaderText="Delete"> 
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("AccountNumber") + ";" +Eval("BranchCode") + ";" +Eval("Currency") + ";" +Eval("CollectionNatureCode") %>'
                                            CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                            ToolTip="Delete" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="center">
                        <asp:Button ID="btnBulkDeleteAccountPNature" runat="server" Text="Delete Account Payment Nature" OnClientClick="javascript: return conBukDelete();"
                            CssClass="btn" Visible="False" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

