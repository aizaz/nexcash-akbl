﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals

Partial Class DesktopModules_CollectionNatureManagement_SaveCollectionNature
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private CollectionNatureCode As String
    Private htRights As Hashtable

#Region "Page Load"

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            CollectionNatureCode = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing
                btnSave.Visible = CBool(htRights("Add"))


                If CollectionNatureCode.ToString() = "0" Then
                    Clear()
                    lblPageHeader.Text = "Add Collection Nature"
                    btnSave.Text = "Save"
                    LoadddlGroupCollection()
                Else
                    lblPageHeader.Text = "Edit Collection Nature"
                    btnSave.Visible = CBool(htRights("Update"))
                    btnSave.Text = "Update"
                    txtCollectionNatureCode.ReadOnly = True
                    txtCollectionNatureName.Enabled = True
                    LoadddlGroupCollection()

                    Dim ICCollectionNature As New ICCollectionNature
                    Dim ICGroup As New ICGroup
                    Dim ICCompany As New ICCompany
                    ICGroup.es.Connection.CommandTimeout = 3600
                    ICCompany.es.Connection.CommandTimeout = 3600
                    ICCollectionNature.es.Connection.CommandTimeout = 3600
                    ICCollectionNature.LoadByPrimaryKey(CollectionNatureCode)
                    ICCompany.LoadByPrimaryKey(ICCollectionNature.CompanyCode)
                    ICGroup.LoadByPrimaryKey(ICCompany.GroupCode)
                    If ICCollectionNature.LoadByPrimaryKey(CollectionNatureCode) Then
                        txtCollectionNatureCode.Text = ICCollectionNature.CollectionNatureCode.ToString()
                        txtCollectionNatureName.Text = ICCollectionNature.CollectionNatureName.ToString()
                        ddlGroupCollection.SelectedValue = ICGroup.GroupCode
                        LoadddlCompanyCollection()
                        ddlCompanyCollection.SelectedValue = ICCollectionNature.CompanyCode
                        chkActive.Checked = ICCollectionNature.IsActive
                    End If

                End If

            End If
            If CollectionNatureCode.ToString <> "0" Then
                Dim ICCollectionNature As New ICCollectionNature
                ICCollectionNature.es.Connection.CommandTimeout = 3600
                ICCollectionNature.LoadByPrimaryKey(CollectionNatureCode)

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        LoadddlGroupCollection()
        LoadddlCompanyCollection()
        txtCollectionNatureCode.Text = ""
        txtCollectionNatureName.Text = ""
        chkActive.Checked = True
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Collection Nature Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

#Region "Button"

    Protected Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim ICCollectionNature As New ICCollectionNature
                ICCollectionNature.es.Connection.CommandTimeout = 3600

                ICCollectionNature.CollectionNatureCode = txtCollectionNatureCode.Text.ToString()
                ICCollectionNature.CollectionNatureName = txtCollectionNatureName.Text.ToString()
                ICCollectionNature.CompanyCode = ddlCompanyCollection.SelectedValue.ToString()
                ICCollectionNature.IsActive = chkActive.Checked

                If CollectionNatureCode = "0" Then
                    ICCollectionNature.CreatedBy = Me.UserId
                    ICCollectionNature.CreatedDate = Date.Now
                    If CheckDuplicate(ICCollectionNature) = False Then
                        ICCollectionNatureController.AddCollectionNature(ICCollectionNature, False, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        UIUtilities.ShowDialog(Me, "Save Collection Nature", "Collection nature added successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Else
                        UIUtilities.ShowDialog(Me, "Save Collection Nature", "Can not add duplicate Collection Nature.", ICBO.IC.Dialogmessagetype.Failure)
                    End If
                Else
                    If CheckDuplicateonEdit(ICCollectionNature) = False Then
                        ICCollectionNatureController.AddCollectionNature(ICCollectionNature, True, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        UIUtilities.ShowDialog(Me, "Save Collection Nature", "Collection Nature updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    Else
                        UIUtilities.ShowDialog(Me, "Save Collection Nature", "Can not add duplicate Collection Nature Name.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                Clear()

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Private Function CheckDuplicate(ByVal ICCollectionNature As ICCollectionNature) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim objCollectionNature As New ICCollectionNature
            Dim collCollectionNature As New ICCollectionNatureCollection

            objCollectionNature.es.Connection.CommandTimeout = 3600
            collCollectionNature.es.Connection.CommandTimeout = 3600

            If CollectionNatureCode = "0" Then
                If objCollectionNature.LoadByPrimaryKey(ICCollectionNature.CollectionNatureCode) Then
                    rslt = True
                End If
                collCollectionNature.LoadAll()
                collCollectionNature.Query.Where(collCollectionNature.Query.CollectionNatureName.ToLower.Trim = ICCollectionNature.CollectionNatureName.ToLower.Trim)
                If collCollectionNature.Query.Load() Then
                    rslt = True
                End If

            End If

            Return rslt

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Private Function CheckDuplicateonEdit(ByVal ICCollectionNature As ICCollectionNature) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim objCollectionNature As New ICCollectionNature
            Dim collCollectionNature As New ICCollectionNatureCollection

            objCollectionNature.es.Connection.CommandTimeout = 3600
            collCollectionNature.es.Connection.CommandTimeout = 3600

            collCollectionNature.Query.Where(collCollectionNature.Query.CollectionNatureCode <> ICCollectionNature.CollectionNatureCode And collCollectionNature.Query.CollectionNatureName.ToLower.Trim = ICCollectionNature.CollectionNatureName.ToLower.Trim)
            If collCollectionNature.Query.Load() Then
                rslt = True
            Else
                rslt = False
            End If

            Return rslt

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

#End Region

#Region "Drop Down"

    Protected Sub ddlGroupCollection_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlGroupCollection.SelectedIndexChanged
        Try
            If ddlGroupCollection.SelectedValue = "0" Then
                LoadddlCompanyCollection()
            Else
                LoadddlCompanyCollection()
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Private Sub LoadddlGroupCollection()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            Dim dt As DataTable
            ddlGroupCollection.Items.Clear()
            ddlGroupCollection.Items.Add(lit)
            ddlGroupCollection.AppendDataBoundItems = True
            dt = ICGroupController.GetAllActiveGroupsIsCollectionAllowonCompany()
            ddlGroupCollection.DataSource = dt
            ddlGroupCollection.DataValueField = "GroupCode"
            ddlGroupCollection.DataTextField = "GroupName"
            ddlGroupCollection.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlCompanyCollection()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompanyCollection.Items.Clear()
            ddlCompanyCollection.Items.Add(lit)
            ddlCompanyCollection.AppendDataBoundItems = True
            ddlCompanyCollection.DataSource = ICCompanyController.GetAllActiveandApproveCompaniesByGroupCode(ddlGroupCollection.SelectedValue.ToString)
            ddlCompanyCollection.DataTextField = "CompanyName"
            ddlCompanyCollection.DataValueField = "CompanyCode"
            ddlCompanyCollection.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

End Class
