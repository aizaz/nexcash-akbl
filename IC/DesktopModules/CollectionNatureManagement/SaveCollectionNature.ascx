﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveCollectionNature.ascx.vb" Inherits="DesktopModules_CollectionNatureManagement_SaveCollectionNature" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }

</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior2" EmptyMessage="type.."
        Type="Number">
    </telerik:NumericTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RegExpCollectionNatureCode" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9.-_/]{1,20}$" ErrorMessage="Invalid Collection Nature Code"
        EmptyMessage="Enter Collection Nature Code">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCollectionNatureCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RegExpCollectionNatureName" ValidationExpression="^[a-zA-Z0-9 -_()&{}]{1,150}$"
        Validation-IsRequired="true" ErrorMessage="Invalid Collection Nature Name" EmptyMessage="Enter Collection Nature Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCollectionNatureName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGroupCollection" runat="server" Text="Group" CssClass="lbl"></asp:Label><asp:Label
                ID="ReqGroupCollection" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompanyCollection" runat="server" Text="Company" CssClass="lbl"></asp:Label>
            <asp:Label ID="ReqCompanyCollection" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            
            <asp:DropDownList ID="ddlGroupCollection" runat="server" 
                CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
            </asp:DropDownList>
            
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            
            <asp:DropDownList ID="ddlCompanyCollection" runat="server" 
                CssClass="dropdown">
                 <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
            </asp:DropDownList>
            
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
       <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlGroupCollection" runat="server" 
                ControlToValidate="ddlGroupCollection" Display="Dynamic" 
                ErrorMessage="Please select Group Collection" InitialValue="0" 
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlCompanyCollection" runat="server" 
                ControlToValidate="ddlCompanyCollection" Display="Dynamic" 
                ErrorMessage="Please select Company Collection" InitialValue="0" 
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            
            <asp:Label ID="lblCollectionNatureCode" runat="server" Text="Collection Nature Code" CssClass="lbl"></asp:Label><asp:Label
                ID="ReqCollectionNatureCode" runat="server" Text="*" ForeColor="Red"></asp:Label>
            
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            
            <asp:Label ID="lblCollectionNatureName" runat="server" Text="Collection Nature Name" CssClass="lbl"></asp:Label>
            <asp:Label ID="ReqCollectionNatureName" runat="server" Text="*" ForeColor="Red"></asp:Label>
            
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtCollectionNatureCode" runat="server" CssClass="txtbox" MaxLength="20"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtCollectionNatureName" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIsActive" runat="server" Text="Is Active" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkActive" runat="server" Text=" " CssClass="chkBox" Checked="True" />
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" CausesValidation="False"
                Width="71px" />
            &nbsp;
        </td>
    </tr>
</table>
