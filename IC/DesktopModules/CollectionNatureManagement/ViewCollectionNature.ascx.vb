﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_CollectionNatureManagement_ViewCollectionNature
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable

#Region "Page Load"

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try

            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing
                btnAddCollectionNature.Visible = CBool(htRights("Add"))
                btnDeleteCollectionNatures.Visible = CBool(htRights("Delete"))
                LoadCollectionNature(True)

                ViewState("SortExp") = Nothing
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Collection Nature Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Button"

    Protected Sub btnAddCollectionNature_Click(sender As Object, e As System.EventArgs) Handles btnAddCollectionNature.Click
        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveCollectionNature", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If
    End Sub

    Protected Sub btnDeleteCollectionNatures_Click(sender As Object, e As System.EventArgs) Handles btnDeleteCollectionNatures.Click
        Try
            Dim rowgvCollectionNatures As GridDataItem
            Dim chkSelect As CheckBox
            Dim CollectionNaturesCode As String = ""
            Dim PassCount As Integer = 0
            Dim FailCount As Integer = 0

            If CheckgvCollectionNatureForProcessAll() = True Then
                For Each rowgvCollectionNatures In gvCollectionNature.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowgvCollectionNatures.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        CollectionNaturesCode = rowgvCollectionNatures("CollectionNatureCode").Text.ToString()
                        Dim icCollectionNatures As New ICCollectionNature
                        icCollectionNatures.es.Connection.CommandTimeout = 3600
                        If icCollectionNatures.LoadByPrimaryKey(CollectionNaturesCode.ToString()) Then
                            Try
                                ICCollectionNatureController.DeleteCollectionNature(CollectionNaturesCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                PassCount = PassCount + 1
                            Catch ex As Exception
                                If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                    FailCount = FailCount + 1
                                End If
                            End Try
                            ' End If
                        End If
                    End If

                Next

                If PassCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Delete Collection Natures", "[" & FailCount.ToString() & "] Collection Natures can not be deleted due to following reasones : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    Exit Sub
                End If
                If FailCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Delete Collection Natures", "[" & PassCount.ToString() & "] Collection Natures deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    Exit Sub
                End If
                UIUtilities.ShowDialog(Me, "Delete Collection Natures", "[" & PassCount.ToString() & "] Collection Natures deleted successfully. [" & FailCount.ToString() & "] Collection Natures can not be deleted due to following reasons : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
            Else
                UIUtilities.ShowDialog(Me, "Delete Collection Natures", "Please select atleast one(1) Collection Nature.", ICBO.IC.Dialogmessagetype.Warning)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Grid View"

    Private Function CheckgvCollectionNatureForProcessAll() As Boolean
        Try
            Dim rowgvCollectionNatures As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvCollectionNatures In gvCollectionNature.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvCollectionNatures.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Private Sub LoadCollectionNature(ByVal IsBind As Boolean)
        Try
            ICCollectionNatureController.GetCollectionNaturesgv(gvCollectionNature.CurrentPageIndex + 1, gvCollectionNature.PageSize, gvCollectionNature, IsBind)

            If gvCollectionNature.Items.Count > 0 Then
                gvCollectionNature.Visible = True
                lblCollectionNaturesRNF.Visible = False
                btnDeleteCollectionNatures.Visible = CBool(htRights("Delete"))
                gvCollectionNature.Columns(5).Visible = CBool(htRights("Delete"))
            Else
                gvCollectionNature.Visible = False
                btnDeleteCollectionNatures.Visible = False
                lblCollectionNaturesRNF.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvCollectionNature_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvCollectionNature.NeedDataSource
        LoadCollectionNature(False)
    End Sub

    Protected Sub gvCollectionNature_ItemCreated(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles gvCollectionNature.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvCollectionNature.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvCollectionNature_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles gvCollectionNature.ItemCommand
        Try
            If e.CommandName = "del" Then
                ICCollectionNatureController.DeleteCollectionNature(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                UIUtilities.ShowDialog(Me, "Delete Collection Nature", "Collection Nature deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                LoadCollectionNature(True)
            End If

        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Deleted  Collection Nature", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvCollectionNature_PageIndexChanged(sender As Object, e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvCollectionNature.PageIndexChanged
        gvCollectionNature.CurrentPageIndex = e.NewPageIndex
    End Sub

    Protected Sub gvCollectionNature_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles gvCollectionNature.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvCollectionNature.MasterTableView.ClientID & "','0');"
        End If
    End Sub

#End Region

End Class
