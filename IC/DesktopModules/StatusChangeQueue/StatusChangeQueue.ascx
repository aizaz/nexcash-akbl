﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="StatusChangeQueue.ascx.vb"
    Inherits="DesktopModules_StatusChangeQueue_StatusChangeQueue" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">

    function conBukDelete() {
        if (window.confirm("Are you sure you wish to cancel selected instruction(s)?") == true) {
            var btnCancel = document.getElementById('<%=btnCancelInstructions.ClientID%>')
            btnCancel.value = "Processing...";
            btnCancel.disabled = true;

            var a = btnCancel.name;
            __doPostBack(a, '');
            return true;
        }
        else {
            return false;
        }
    }
    function ReloadPage() {

        location.reload(true);
    }
    function DisAbleCancelButton() {
        var btnCancel = document.getElementById('<%=btnCancelInstructions.ClientID%>')

        btnCancel.value = "Processing...";
        btnCancel.disabled = true;


    }
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;

        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }

    $(document).ready(function () {

        // Dialog
        $('#urldialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#loading').hide();



            },
            resizable: false
        });
        // Summary Dialog
        $('#sumdialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#sumdialog').hide();
                //  location.reload(false);
                //                window.location.reload();
                //                window.location.reload();sa
            },
            resizable: false
        });




        //        $.msgBox({
        //            title: "Are You Sure",
        //            content: "Would you like a cup of coffee?",
        //            type: "confirm",
        //            buttons: [{ value: "Yes" }, { value: "No" }, { value: "Cancel"}],
        //            success: function (result) {
        //                if (result == "Yes") {
        //                    alert("One cup of coffee coming right up!");
        //                }
        //            }
        //        });
        //           

    });
    function showsumdialog(title, modal, width, height) {
        //  alert(modal);
        $('#sumdialog').dialog("option", "title", title);
        $('#sumdialog').dialog("option", "modal", modal);
        $('#sumdialog').dialog("option", "width", width);
        $('#sumdialog').dialog("option", "height", height);
        $('#sumdialog').dialog('open');

        return false;

    };


    function showurldialog(title, url, modal, width, height) {
        //  alert(modal);
        $('#urldialog').dialog("option", "title", title);
        $('#urldialog').dialog("option", "modal", modal);
        $('#urldialog').dialog("option", "width", width);
        $('#urldialog').dialog("option", "height", height);
        $('#urldialog').dialog('open');
        $('#dialogiframe').hide();
        $('#loading').show();
        $('#dialogiframe').attr("src", url);
        $('#dialogiframe').load(function () {
            $('#loading').hide();
            $('#dialogiframe').show();
        });
        return false;

    };

    function CloseIframe() {
        $('#urldialog').dialog('close');
    }




</script>
<style type="text/css">
    .ctrDropDown {
        width: 290px;
    }

    .ctrDropDownClick {
        width: 430px;
    }

    .plainDropDown {
        width: 290px;
    }
</style>
<telerik:RadInputManager ID="RadInputManager3" runat="server" EnableEmbeddedSkins="false">
    <telerik:TextBoxSetting BehaviorID="reTnxCount" Validation-IsRequired="false" ErrorMessage="Transaction Count">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTnxCount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reTotalAmount" Validation-IsRequired="false"
        ErrorMessage="Total Amount">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTotalAmount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reCurrency" Validation-IsRequired="false" ErrorMessage="Currency">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCurrency" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rePaymentMode" Validation-IsRequired="false"
        ErrorMessage="Payment Mode">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPaymentMode" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reAvailableBalance" Validation-IsRequired="false"
        ErrorMessage="Available Balance">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAvailableBalance" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reInstructionNo" Validation-IsRequired="false"
        ErrorMessage="Enter Instruction No." ValidationExpression="[0-9]{0,8}">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstructionNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reInstrumentNo" Validation-IsRequired="false"
        ErrorMessage="Enter Instrument No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstrumentNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reReferenceNo" Validation-IsRequired="false"
        ErrorMessage="Enter Reference No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reRemarks" ErrorMessage="Enter Remarks">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRemarks" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reRemarksApproval" ErrorMessage="Enter Remarks">
        <TargetControls>
            <telerik:TargetInput ControlID="txtApprovalRemarks" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<telerik:RadInputManager ID="radCreditAccountNumber" runat="server" EnableEmbeddedSkins="false"
    Enabled="false">
    <telerik:RegExpTextBoxSetting BehaviorID="radAccountNo" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9 ]+$" ErrorMessage="Invalid Account No." EmptyMessage="" Validation-ValidationGroup="Proceed">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCreditAccountNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="radCurrency" Validation-IsRequired="true" Validation-ValidationGroup="Proceed"
        ValidationExpression="^[a-zA-Z0-9 ]+$" ErrorMessage="Invalid Branch Code">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCreditAccountBrCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="radBranchCode" Validation-IsRequired="true" Validation-ValidationGroup="Proceed"
        ValidationExpression="^[a-zA-Z0-9 ]+$" ErrorMessage="Invalid Currency">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCreditAccountCurrency" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <%-- <telerik:TextBoxSetting BehaviorID="radClientNo" Validation-IsRequired="false"
        ErrorMessage="Invalid Client Number" EmptyMessage="" Validation-ValidationGroup="Proceed">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCreditAccountClientNo" />
        </TargetControls>
    </telerik:TextBoxSetting>

    <telerik:RegExpTextBoxSetting BehaviorID="radSeqNo" Validation-IsRequired="false" ValidationExpression="^[0-9]{1,9}$"
        ErrorMessage="Invalid Sequence Number" EmptyMessage="" Validation-ValidationGroup="Proceed">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCreditAccountSequenceNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>

    <telerik:TextBoxSetting BehaviorID="radProfitCentre" Validation-IsRequired="false"
        ErrorMessage="Invalid Profit Centre" EmptyMessage="" Validation-ValidationGroup="Proceed">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCreditAccountProfitCentre" />
        </TargetControls>
    </telerik:TextBoxSetting>--%>
</telerik:RadInputManager>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {

                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
                else {

                    if (cell.childNodes[j].type == "checkbox") {
                        cell.childNodes[j].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }

                }
            }
        }
    }
</script>
<table align="left" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Status Change Queue" CssClass="lblHeadingForQueue"></asp:Label>
                        <br />
                        Note:&nbsp; Fields marked as
                        <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        &nbsp;are required.
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>--%>
                        <asp:Panel ID="pnlHeader" runat="server" DefaultButton="btnSearch">
                            <table align="left" style="width: 100%">
                                <tr align="left" valign="top" style="width: 100%">
                                    <td align="left" valign="top" style="width: 43%">
                                        <table align="left" style="width: 100%" cellpadding="0" cellspacing="0">

                                            <tr align="left" style="width: 100%" valign="top">
                                                <td align="left" style="width: 30%" valign="top">&nbsp;
                                                </td>
                                                <td align="left" style="width: 40%" valign="top">
                                                    <asp:RadioButtonList ID="rbtnlstDateSelection" runat="server" AutoPostBack="True"
                                                        Font-Size="10px" RepeatDirection="Horizontal">
                                                        <asp:ListItem Value="Creation Date"> Creation Date</asp:ListItem>
                                                        <asp:ListItem Value="Value Date"> Value Date</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td align="left" style="width: 30%" valign="top">&nbsp;
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblDateFrom" runat="server" Text="Date From" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <telerik:RadDatePicker ID="raddpCreationFromDate" runat="server" Width="180px" AutoPostBack="true">
                                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                        </Calendar>
                                                        <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                            Font-Size="10.5px" LabelWidth="40%" type="text" value="">
                                                        </DateInput>
                                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                    </telerik:RadDatePicker>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">&nbsp;
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px"></td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px"></td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px"></td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%; height: 25%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblDateTo" runat="server" Text="Date To" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <telerik:RadDatePicker ID="raddpCreationToDate" runat="server" Width="180px" AutoPostBack="true">
                                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                        </Calendar>
                                                        <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                            LabelWidth="40%" type="text" value="" Font-Size="10.5px">
                                                        </DateInput>
                                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                    </telerik:RadDatePicker>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%"></td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px"></td>
                                                <td align="left" valign="top" style="width: 45%; height: 1px"></td>
                                                <td align="left" valign="top" style="width: 25%; height: 1px"></td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblCompany" runat="server" Text="Company" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdownForQueue" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%"></td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px"></td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px"></td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px"></td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblAccountPaymentNature" runat="server" Text="Account Payment Nature"
                                                        CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <div style="width: 290px; overflow: hidden;">
                                                        <%--<asp:DropDownList ID="ddlAcccountPaymentNature" runat="server" class="ctrDropDown"
                                                            Font-Size="11px" Height="19px" onblur="this.className='ctrDropDown';" onmousedown="this.className='ctrDropDownClick';"
                                                            onchange="this.className='ctrDropDown';" AutoPostBack="true">
                                                        </asp:DropDownList>--%>
                                                    </div>
                                                    <asp:DropDownList ID="ddlAcccountPaymentNature" runat="server" CssClass="dropdownForQueue"
                                                        AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%"></td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px"></td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px"></td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px"></td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblBatch" runat="server" Text="Batch" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:DropDownList ID="ddlBatch" runat="server" CssClass="dropdownForQueue" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%"></td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px"></td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px"></td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px"></td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblProductType" runat="server" Text="Product Type" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:DropDownList ID="ddlProductType" runat="server" CssClass="dropdownForQueue"
                                                        AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%"></td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px"></td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px"></td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px"></td>
                                            </tr>
                                            <tr align="left" style="width: 100%" valign="top">
                                                <td align="left" style="width: 30%; height: 1px" valign="top">
                                                    <%-- <asp:Label ID="lblStatus" runat="server" CssClass="lblForDropDown"
                                                        Text="Status"></asp:Label>--%>
                                                    <asp:Label ID="lblAction" runat="server" CssClass="lblForDropDown"
                                                        Text="Action"></asp:Label>
                                                    <asp:Label ID="lblReqAction" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 40%; height: 1px" valign="top">
                                                    <%--<asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="True"
                                                        CssClass="dropdownForQueue">
                                                    </asp:DropDownList>--%>
                                                    <asp:DropDownList ID="ddlAction" runat="server" AutoPostBack="True" CssClass="dropdownForQueue">
                                                    </asp:DropDownList>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="rfvAction" runat="server" ControlToValidate="ddlAction" ValidationGroup="View"
                                                        CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Action" SetFocusOnError="True"
                                                        ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
                                                </td>
                                                <td align="left" style="width: 30%; height: 1px" valign="top">&nbsp;</td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblRemarks" runat="server" Text="Remarks" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:TextBox ID="txtRemarks" runat="server" Width="288px" CssClass="txtboxForQueue"
                                                        TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%"></td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px"></td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px"></td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px"></td>
                                            </tr>
                                            <tr align="left" style="width: 100%" valign="top">
                                                <td align="left" style="width: 30%; height: 1px" valign="top">
                                                    <asp:Label ID="lblRevertToStatus" runat="server" CssClass="lblForDropDown" Visible="false"
                                                        Text="To Status"></asp:Label>
                                                    <asp:Label ID="lblReqRevertToStatus" runat="server" Text="*" ForeColor="Red" Visible="false"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 40%; height: 1px" valign="top">
                                                    <asp:DropDownList ID="ddlRevertToStatus" runat="server" AutoPostBack="True" CssClass="dropdownForQueue" Visible="false">
                                                    </asp:DropDownList>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="rfvddlRevertToStatus" runat="server" ControlToValidate="ddlRevertToStatus" ValidationGroup="View"
                                                        CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select To Status" SetFocusOnError="True" Enabled="false"
                                                        ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
                                                </td>
                                                <td align="left" style="width: 30%; height: 1px" valign="top">&nbsp;</td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px"></td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px"></td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px"></td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">&nbsp;
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:Button ID="btnView" runat="server" Text="View" CssClass="btn2" ValidationGroup="View" />
                                                    <asp:Button ID="btnReverse" runat="server" Text="Reverse" CssClass="btn2" ValidationGroup="Verify" />
                                                    <asp:Button ID="btnSetteled" runat="server" Text="Settle" CssClass="btn2" ValidationGroup="Verify" />
                                                    <asp:Button ID="btnCancelInstructions" runat="server" Text="Cancel Instructions"
                                                        OnClientClick="javascript: return conBukDelete();" CssClass="btnCancel2" ValidationGroup="Verify"
                                                        UseSubmitBehavior="False" />
                                                    <asp:Button ID="btnHold" runat="server" Text="Hold" CssClass="btn2" ValidationGroup="Verify" />
                                                    <asp:Button ID="btnUnHold" runat="server" Text="UnHold" CssClass="btn2" ValidationGroup="Verify" />
                                                    <asp:Button ID="btnRevert" runat="server" Text="Revert" CssClass="btn2" ValidationGroup="Verify" />
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="right" valign="top" style="width: 57%">
                                        <table align="right" style="width: 100%">
                                            <tr>
                                                <td align="right" valign="top" style="width: 30%">
                                                    <fieldset class="fieldsetForIE7LeftTD">
                                                        <legend class="legendForIE7LeftTD">Transaction Summary</legend>
                                                        <table align="right" style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td align="center" valign="top" style="width: 100%">&nbsp;
                                                                    <asp:Label ID="lblTnxCount" runat="server" Text="Transaction Count" CssClass="lblForFieldSetLabels"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr align="left" valign="top" style="width: 100%">
                                                                <td align="center" valign="top" style="width: 100%">
                                                                    <asp:TextBox ID="txtTnxCount" runat="server" MaxLength="150" Width="120px" Font-Size="10px"
                                                                        ReadOnly="True"></asp:TextBox>
                                                                    <br />
                                                                </td>
                                                            </tr>
                                                            <tr valign="top" style="width: 100%">
                                                                <td align="center" valign="top" style="width: 100%">
                                                                    <asp:Label ID="lblTotalAmount" runat="server" Text="Total Amount" CssClass="lblForFieldSetLabels"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr align="center" valign="top" style="width: 100%">
                                                                <td align="center" valign="top" style="width: 100%">
                                                                    <asp:TextBox ID="txtTotalAmount" runat="server" MaxLength="150" Width="120px" Font-Size="10px"
                                                                        HtmlEncode="false" DataFormatString="{0:N2}" ReadOnly="true"></asp:TextBox><br />
                                                                </td>
                                                            </tr>
                                                            <tr align="center" valign="top" style="width: 100%">
                                                                <td align="center" valign="top">
                                                                    <asp:Button ID="btnSummary" runat="server" Text="Summary" CssClass="btn2" CausesValidation="False" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                                <td align="left" valign="top" style="width: 70%">
                                                    <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="left" valign="top" style="width: 100%">
                                                                <fieldset class="fieldsetForIE7RightTopTD">
                                                                    <legend class="legendForIE7RightTopTD">Search</legend>
                                                                    <table align="left" style="width: 100%">
                                                                        <tr align="left" valign="top" style="width: 100%">
                                                                            <td align="left" valign="top" style="width: 25%">
                                                                                <asp:Label ID="lblInstructionNo" runat="server" Text="Instruction No." CssClass="lblForFieldSetLabels"></asp:Label>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 25%">
                                                                                <asp:Label ID="lblInstrumentNo" runat="server" Text="Instrument No." CssClass="lblForFieldSetLabels"></asp:Label>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 25%">
                                                                                <asp:Label ID="Label2" runat="server" Text="Reference No." CssClass="lblForFieldSetLabels"></asp:Label>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 25%">&nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr align="left" valign="top" style="width: 100%">
                                                                            <td align="left" valign="top" style="width: 28%">
                                                                                <asp:TextBox ID="txtInstructionNo" runat="server" Width="120px" Font-Size="10px"
                                                                                    MaxLength="8"></asp:TextBox>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 28%">
                                                                                <asp:TextBox ID="txtInstrumentNo" runat="server" Width="120px" Font-Size="10px" MaxLength="18"></asp:TextBox>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 28%">
                                                                                <asp:TextBox ID="txtReferenceNo" runat="server" Width="120px" Font-Size="10px" MaxLength="18"></asp:TextBox>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 16%">
                                                                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn2" ValidationGroup="View" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <%--                <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 45%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 25%; height: 1px">
                                                </td>
                                            </tr>--%>
                                                        <tr>
                                                            <td align="left" valign="top" style="width: 100%">
                                                                <fieldset class="fieldsetForIE7RightBottomTD">
                                                                    <legend class="legendForIE7RightBottomTD">Check Balance</legend>
                                                                    <table align="left" style="width: 100%">
                                                                        <tr align="left" valign="top" style="width: 100%">
                                                                            <td align="left" valign="top" style="width: 60%">
                                                                                <asp:Label ID="Label4" runat="server" Text="Select Account" CssClass="lblForFieldSetLabels"></asp:Label>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 40%">
                                                                                <asp:Label ID="lblAvailableBalance" runat="server" Text="Available Balance" CssClass="lblForFieldSetLabels"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr align="left" valign="top" style="width: 100%">
                                                                            <td align="left" valign="top" style="width: 60%">
                                                                                <asp:DropDownList ID="ddlAccount" runat="server" CssClass="dropdownForQueue2" AutoPostBack="True">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 40%">
                                                                                <asp:TextBox ID="txtAvailableBalance" runat="server" MaxLength="150" Width="120px"
                                                                                    Font-Size="10px" ReadOnly="true" HtmlEncode="false" DataFormatString="{0:N2}"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <%--  </ContentTemplate>
                        </asp:UpdatePanel>--%>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <table id="tblCBAccountType" runat="server" style="width: 100%; display: none">
                            <tr>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblCBAccountType" runat="server" CssClass="lblForDropDown"
                                        Text="Account Type"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%"></td>
                                <td align="left" valign="top" style="width: 25%"></td>
                                <td align="left" valign="top" style="width: 25%"></td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:DropDownList ID="ddlCBAccountType" runat="server" AutoPostBack="True"
                                        CssClass="dropdownForQueue" Width="120px">
                                        <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                                        <asp:ListItem>RB</asp:ListItem>
                                        <asp:ListItem>GL</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td align="left" valign="top" style="width: 25%"></td>
                                <td align="left" valign="top" style="width: 25%"></td>
                                <td align="left" valign="top" style="width: 25%"></td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Button ID="btnCancelCB" runat="server" Text="Cancel" CssClass="btnCancel2" />
                                </td>
                                <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                                <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                                <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <table id="tblCreditAccountNumber" runat="server" style="width: 100%; display: none">
                            <tr>
                                <td align="left" valign="top" colspan="2">&nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%"></td>
                                <td align="left" valign="top" style="width: 25%"></td>

                            </tr>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblCreditAccountNo0" runat="server" Text="Credit Account No" CssClass="lblForDropDown"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblCreditAccountBrCode" runat="server" Text="Credit Account Branch Code"
                            CssClass="lblForDropDown"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtCreditAccountNo" runat="server" MaxLength="30" Width="130px"
                            Font-Size="10px"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtCreditAccountBrCode" runat="server" MaxLength="150" Width="130px"
                            Font-Size="10px"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblCreditAccountCurency" runat="server"
                            Text="Credit Account Currency" CssClass="lblForDropDown"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <%--   <asp:Label ID="lblCreditAccountProfitCentre" runat="server"
                            Text="Credit Account Profit Centre" CssClass="lblForDropDown"></asp:Label>--%>
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtCreditAccountCurrency" runat="server" MaxLength="150" Width="130px"
                            Font-Size="10px"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <%--  <asp:TextBox ID="txtCreditAccountProfitCentre" runat="server" MaxLength="150" Width="130px"
                            Font-Size="10px"></asp:TextBox>--%>
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                </tr>
                <%--  <tr>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblCreditAccountClientNo" runat="server"
                            Text="Credit Account Client No" CssClass="lblForDropDown"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblCreditAccountSeqNo" runat="server"
                            Text="Credit Account Sequence No" CssClass="lblForDropDown"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtCreditAccountClientNo" runat="server" MaxLength="150" Width="130px"
                            Font-Size="10px"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtCreditAccountSequenceNo" runat="server" MaxLength="9" Width="130px"
                            Font-Size="10px"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                </tr>--%>
                <tr>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Button ID="btnProceed" runat="server" Text="Proceed" CssClass="btn2" ValidationGroup="Proceed" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel2" />
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">

            <table id="tblStatusChangeQueue" runat="server" style="width: 100%; display: none">
                <tr>
                    <td align="left" valign="top" style="width: 100%">


                        <telerik:RadGrid ID="gvStatusChangeQueue" runat="server" CssClass="RadGrid" AllowPaging="True"
                            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                            PageSize="100">
                            <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="true" />
                            </ClientSettings>
                            <%-- <AlternatingItemStyle CssClass="rgAltRow" />--%>
                            <MasterTableView DataKeyNames="InstructionID,IsAmendmentComplete,Status,ClearingOfficeCode,PaymentMode" TableLayout="Fixed"
                                NoMasterRecordsText="">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Checked="true" Text=""
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Checked="true" Text="" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Instruction No." HeaderStyle-Width="8%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbInstructionID" runat="server" Text='<%#Eval("InstructionID") %>'
                                                ToolTip="Instruction No." ForeColor="Blue" Enabled="false"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />

                                    </telerik:GridTemplateColumn>
                                    <%--<telerik:GridBoundColumn DataField="InstructionID" HeaderText="Instruction No." SortExpression="InstructionID">
                                    </telerik:GridBoundColumn>--%>
                                    <%-- <telerik:GridBoundColumn DataField="ReferenceNo" HeaderText="Reference No." SortExpression="ReferenceNo">
                                    </telerik:GridBoundColumn>--%>
                                    <telerik:GridBoundColumn DataField="CreateDate" HeaderText="Creation Date" SortExpression="CreateDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}" HeaderStyle-Width="8%">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ValueDate" HeaderText="Value Date" SortExpression="ValueDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}" HeaderStyle-Width="8%">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneficiaryName" HeaderText="Bene Name" SortExpression="BeneficiaryName">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="13%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="13%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Amount" HeaderText="Amount" SortExpression="Amount"
                                        HtmlEncode="false" DataFormatString="{0:N2}">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProductTypeCode" HeaderText="Payment Mode" SortExpression="ProductTypeCode">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="10%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="10%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InstrumentNo" HeaderText="Instrument No." SortExpression="InstrumentNo">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="OfficeName" HeaderText="Print Location" SortExpression="OfficeName">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="13%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="13%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneficiaryAccountNo" HeaderText="Account No."
                                        SortExpression="BeneficiaryAccountNo">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="9%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="9%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneAccountBranchCode" HeaderText="Branch Code"
                                        SortExpression="BeneAccountBranchCode">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneAccountCurrency" HeaderText="Currency" SortExpression="BeneAccountCurrency">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridTemplateColumn HeaderText="View Details">
                                     
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlDetails" runat="server" ImageUrl="~/images/view.gif" NavigateUrl='<%#NavigateURL("ViewInstructionDetails", "&mid=" & Me.ModuleId & "&InstructionID="& Eval("InstructionID"))%>'
                                                ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>
                                    <%-- <telerik:GridTemplateColumn HeaderText="View Details">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ibButton" runat="server" ImageUrl="~/images/view.gif" ToolTip="Instruction Details"
                                                CausesValidation="false" CommandArgument='<%#Eval("InstructionID") %>'></asp:ImageButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridTemplateColumn>--%>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <%--     <ItemStyle CssClass="rgRow" />--%>
                            <PagerStyle AlwaysVisible="True" />
                            <HeaderStyle CssClass="GridHeader" />
                            <ItemStyle CssClass="GridItem" />
                            <AlternatingItemStyle CssClass="GridItem" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>


                    </td>

                </tr>


            </table>


            <br />
            <asp:Label ID="lblNRF" runat="server" Text="No Record Found." CssClass="headingblue" Visible="false"></asp:Label>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table id="tblApprovalQueue" runat="server" style="width: 100%; display: none">
                <tr>
                    <td align="left" valign="top" style="width: 100%">

                        <asp:Label ID="lblPageHeadeApproval" runat="server"
                            Text="Pending For Status Change Approval" CssClass="lblHeadingForQueue"></asp:Label>



                    </td>


                </tr>

                <tr>
                    <td align="left" valign="top" style="width: 100%">

                        <telerik:RadGrid ID="gvStatusChangeApprovalQueue" runat="server" CssClass="RadGrid"
                            AllowPaging="True"
                            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                            PageSize="100">
                            <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="true" />
                            </ClientSettings>
                            <%-- <AlternatingItemStyle CssClass="rgAltRow" />--%>
                            <MasterTableView DataKeyNames="InstructionID,IsAmendmentComplete,Status,ClearingOfficeCode,PaymentMode" TableLayout="Fixed"
                                NoMasterRecordsText="">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderStyle-Width="3%" HeaderStyle-HorizontalAlign="Left">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAllApproval" runat="server" CssClass="chkBox"
                                                Checked="true" Text=""
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelectApproval" runat="server" CssClass="chkBox" Checked="true"
                                                Text="" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Instruction No." HeaderStyle-Width="8%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbInstructionIDApproval" runat="server" Text='<%#Eval("InstructionID") %>'
                                                ToolTip="Instruction No." ForeColor="Blue" Enabled="false"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridTemplateColumn>
                                    <%--<telerik:GridBoundColumn DataField="InstructionID" HeaderText="Instruction No." SortExpression="InstructionID">
                                    </telerik:GridBoundColumn>--%>
                                    <%-- <telerik:GridBoundColumn DataField="ReferenceNo" HeaderText="Reference No." SortExpression="ReferenceNo">
                                    </telerik:GridBoundColumn>--%>
                                    <telerik:GridBoundColumn DataField="CreateDate" HeaderText="Creation Date" SortExpression="CreateDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}" HeaderStyle-Width="8%">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ValueDate" HeaderText="Value Date" SortExpression="ValueDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}" HeaderStyle-Width="8%">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneficiaryName" HeaderText="Bene Name" SortExpression="BeneficiaryName">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="13%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="13%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Amount" HeaderText="Amount" SortExpression="Amount"
                                        HtmlEncode="false" DataFormatString="{0:N2}">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProductTypeCode" HeaderText="Payment Mode" SortExpression="ProductTypeCode">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="10%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="10%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InstrumentNo" HeaderText="Instrument No." SortExpression="InstrumentNo">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="OfficeName" HeaderText="Print Location" SortExpression="OfficeName">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="13%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="13%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneficiaryAccountNo" HeaderText="Account No."
                                        SortExpression="BeneficiaryAccountNo">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="9%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="9%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneAccountBranchCode" HeaderText="Branch Code"
                                        SortExpression="BeneAccountBranchCode">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneAccountCurrency" HeaderText="Currency" SortExpression="BeneAccountCurrency">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridTemplateColumn HeaderText="View Details">
                                     
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlDetails" runat="server" ImageUrl="~/images/view.gif" NavigateUrl='<%#NavigateURL("ViewInstructionDetails", "&mid=" & Me.ModuleId & "&InstructionID="& Eval("InstructionID"))%>'
                                                ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>
                                    <%-- <telerik:GridTemplateColumn HeaderText="View Details">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ibButton" runat="server" ImageUrl="~/images/view.gif" ToolTip="Instruction Details"
                                                CausesValidation="false" CommandArgument='<%#Eval("InstructionID") %>'></asp:ImageButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                    </telerik:GridTemplateColumn>--%>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <%--     <ItemStyle CssClass="rgRow" />--%>
                            <PagerStyle AlwaysVisible="True" />
                            <HeaderStyle CssClass="GridHeader" />
                            <ItemStyle CssClass="GridItem" />
                            <AlternatingItemStyle CssClass="GridItem" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid><br />
                        <asp:Label ID="lblNRFApprovalGrid" runat="server" Text="No Record Found." Visible="false"
                            CssClass="headingblue"></asp:Label>



                    </td>


                </tr>

                <tr>
                    <td align="left" valign="top" style="width: 100%">

                        <asp:Label ID="lblApprovalRemarks"
                            runat="server" Text="Approval Remarks"
                            CssClass="lblForDropDown"></asp:Label>



                    </td>


                </tr>

                <tr>
                    <td align="left" valign="top" style="width: 100%">

                        <asp:TextBox ID="txtApprovalRemarks"
                            runat="server" Width="288px" CssClass="txtboxForQueue"
                            TextMode="MultiLine"></asp:TextBox>



                    </td>


                </tr>

                <tr>
                    <td align="center" valign="top" style="width: 100%">

                        <asp:Button ID="btnApproveSettlement"
                            runat="server" Text="Approve Settlement" CssClass="btn2"
                            ValidationGroup="Approval" />
                        <asp:Button ID="btnApproveCancellation"
                            runat="server" Text="Approve Cancellation" CssClass="btn2"
                            ValidationGroup="Approval" />
                        <asp:Button ID="btnApproveReversal"
                            runat="server" Text="Approve Reversal" CssClass="btn2"
                            ValidationGroup="Approval" />

                        <asp:Button ID="btnApproveHold"
                            runat="server" Text="Approve Hold" CssClass="btn2"
                            ValidationGroup="Approval" />
                        <asp:Button ID="btnApproveRevert"
                            runat="server" Text="Approve Revert" CssClass="btn2"
                            ValidationGroup="Approval" />
                        <asp:Button ID="btnApproveUnHold"
                            runat="server" Text="Approve UnHold" CssClass="btn2"
                            ValidationGroup="Approval" />

                        <asp:Button ID="btnCancelApproval"
                            runat="server" Text="Reject Request" CssClass="btnCancel2"
                            ValidationGroup="Approval" />



                    </td>


                </tr>

            </table>

        </td>
    </tr>
</table>
</td> </tr> </table>
<div id="sumdialog" title="" style="text-align: center">
    <asp:Panel ID="pnlShowSummary" runat="server" Style="display: none; width: 100%"
        ScrollBars="Vertical" Height="600px">
        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnCancel" /><br />
        <telerik:RadGrid ID="gvShowSummary" runat="server" AllowPaging="false" Width="100%"
            CssClass="RadGrid" AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0"
            ShowFooter="True" PageSize="100">
            <AlternatingItemStyle CssClass="rgAltRow" />
            <MasterTableView>
                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                </RowIndicatorColumn>
                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="InstructionID" HeaderText="Instruction No." SortExpression="InstructionID">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                    <%-- <telerik:GridBoundColumn DataField="ReferenceNo" HeaderText="Reference No." SortExpression="ReferenceNo">
                                    </telerik:GridBoundColumn>--%>
                    <telerik:GridBoundColumn DataField="Message" HeaderText="Message" SortExpression="Message">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
                <PagerStyle AlwaysVisible="True" />
            </MasterTableView>
            <%--  <ItemStyle CssClass="rgRow" />--%>
            <PagerStyle AlwaysVisible="True" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
    </asp:Panel>
</div>
<div id="urldialog" title="">
    <iframe id='dialogiframe' frameborder="0" allowtransparency="true" src="" style="border-style: none; width: 100%; height: 100%; overflow-x: hidden; background-color: transparent;"></iframe>
    <div id='loading' style="display: none; margin: 0px auto; text-align: center; width: 100%; height: 100%; position: relative">
        <img src='../../../../images/progressbar.gif' style="left: 45%; top: 50%; position: absolute;" />
    </div>
</div>
