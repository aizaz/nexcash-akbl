﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_StatusChangeQueue_StatusChangeQueue
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrICAssignUserRolsID As New ArrayList
    Private ArrICAssignedOfficeID As New ArrayList
    Private ArrICAssignedStatus As New ArrayList
    Private ArrICAssignedApprovalStatus As New ArrayList
    Private ArrICAssignedStatusForstatusDropDown As New ArrayList
    Private ArrICAssignedPaymentModes As New ArrayList
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()
            If Page.IsPostBack = False Then
                Dim objICUser As New ICUser
                objICUser.LoadByPrimaryKey(Me.UserId.ToString)
                btnReverse.Attributes.Item("onclick") = "if (Page_ClientValidate('Verify')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnReverse, Nothing).ToString() & " } "
                btnSetteled.Attributes.Item("onclick") = "if (Page_ClientValidate('Verify')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnSetteled, Nothing).ToString() & " } "
                btnProceed.Attributes.Item("onclick") = "if (Page_ClientValidate('Proceed')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnProceed, Nothing).ToString() & " } "

                btnApproveReversal.Attributes.Item("onclick") = "if (Page_ClientValidate('Approval')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnApproveReversal, Nothing).ToString() & " } "
                btnApproveSettlement.Attributes.Item("onclick") = "if (Page_ClientValidate('Approval')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnApproveSettlement, Nothing).ToString() & " } "
                btnApproveCancellation.Attributes.Item("onclick") = "if (Page_ClientValidate('Approval')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnApproveCancellation, Nothing).ToString() & " } "
                btnCancelApproval.Attributes.Item("onclick") = "if (Page_ClientValidate('Approval')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnCancelApproval, Nothing).ToString() & " } "

                'btnCancelInstructions.Attributes.Item("onclick") = "if (Page_ClientValidate('Verify')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnCancelInstructions, Nothing).ToString() & " } "
                tblCBAccountType.Style.Add("display", "none")
                btnCancelCB.Visible = True
                ddlCBAccountType.ClearSelection()
                tblCreditAccountNumber.Style.Add("display", "none")
                tblApprovalQueue.Style.Add("display", "none")
                tblStatusChangeQueue.Style.Add("display", "none")
                radCreditAccountNumber.Enabled = False
                DesignDts()
                DesignDtForSummary()
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
                rbtnlstDateSelection.SelectedValue = "Creation Date"
                RefreshPage()

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub RefreshPage()
        LoadddlCompany()
        LoadddlAccountNumbers()
        LoadddlAccountPaymentNatureByUserID()
        LoadddlProductTypeByAccountPaymentNature()
        ClearAllTextBoxes()
        FillDesignedDtByAPNatureByAssignedRole()
        tblCreditAccountNumber.Style.Add("display", "none")
        tblCBAccountType.Style.Add("display", "none")
        btnCancelCB.Visible = True
        ddlCBAccountType.ClearSelection()
        radCreditAccountNumber.Enabled = False
        ClaerCreditAccountNoTextBoxes()

        Dim dtPageLoad As New DataTable
        dtPageLoad = DirectCast(ViewState("AccountNumber"), DataTable)
        If dtPageLoad.Rows.Count > 0 Then
            LoadddlBatcNumber()
            LoadddlAction()
            LoadgvStatusChangeApprovalQueue(True)
            LoadgvStatusChangeQueue(True)
            HideAllButtons()
            LoadddlRevertToStatus()
            lblRevertToStatus.Visible = False
            lblReqRevertToStatus.Visible = False
            ddlRevertToStatus.Visible = False
            rfvddlRevertToStatus.Enabled = False
        Else
            UIUtilities.ShowDialog(Me, "Error", "You do not have access to any transactional data. Please contact DIB Connect Administrator.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
            Exit Sub
        End If
    End Sub

    Private Sub ClearSummaryAndAccountBalance()
        txtTnxCount.Text = ""
        txtTotalAmount.Text = ""
        txtAvailableBalance.Text = ""
        LoadddlAccountNumbers()
    End Sub

    Private Sub DesignDts()
        Dim dtAccountNumber As New DataTable
        dtAccountNumber.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("BranchCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("Currency", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("PaymentNatureCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("OfficeID", GetType(System.String)))
        ViewState("AccountNumber") = dtAccountNumber
    End Sub

    Private Sub DesignDtForSummary()
        ViewState("Summary") = Nothing
        Dim dtSummary As New DataTable

        dtSummary.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
        dtSummary.Columns.Add(New DataColumn("Status", GetType(System.String)))
        dtSummary.Columns.Add(New DataColumn("Message", GetType(System.String)))
        ViewState("Summary") = dtSummary
    End Sub

    Private Sub FillDesignedDtByAPNatureByAssignedRole()
        Dim dtAssignedAPNatureOfficeID As New DataTable
        dtAssignedAPNatureOfficeID.Rows.Clear()
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserSecond(Me.UserId.ToString, ArrICAssignUserRolsID)
        dtAssignedAPNatureOfficeID.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAssignedAPNatureOfficeID.Rows
            dr("DropDown") = "False"
        Next
        ViewState("AccountNumber") = Nothing
        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID

    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Status Change Queue")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Status Change Queue") = True Then
                    If Not ArrICAssignUserRolsID.Contains(RoleID.ToString) Then
                        ArrICAssignUserRolsID.Add(RoleID.ToString)
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub SetArraListAssignedOfficeIDs()
        Dim dt As New DataTable
        Dim objICUser As New ICUser
        objICUser.LoadByPrimaryKey(Me.UserId)
        ArrICAssignedOfficeID.Clear()
        dt = ICUserRolesPaymentNatureController.GetAllTaggedLocationsWithUserByUSerIDAndRoleID(Me.UserId.ToString, ArrICAssignUserRolsID)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("OfficeID").ToString
                ArrICAssignedOfficeID.Add(RoleID.ToString)
            Next
            ArrICAssignedOfficeID.Add(objICUser.OfficeCode)
        End If
    End Sub

    Private Sub SetArraListExceptedStatus()
        Dim dt As New DataTable
        ArrICAssignedStatus.Clear()
        ArrICAssignedStatus.Add(5)
        ArrICAssignedStatus.Add(19)
        ArrICAssignedStatus.Add(20)
        ArrICAssignedStatus.Add(24)
        ArrICAssignedStatus.Add(28)
        ArrICAssignedStatus.Add(32)
        ArrICAssignedStatus.Add(43)
        ArrICAssignedStatus.Add(34)
        ArrICAssignedStatus.Add(35)
        ArrICAssignedStatus.Add(38)
        ArrICAssignedStatus.Add(33)
        ArrICAssignedStatus.Add(22)
        ArrICAssignedStatus.Add(8)
        ArrICAssignedStatus.Add(10)
        ArrICAssignedStatus.Add(44)
        ArrICAssignedStatus.Add(45)
        ArrICAssignedStatus.Add(46)
        ArrICAssignedStatus.Add(39)
        ArrICAssignedStatus.Add(40)
        ArrICAssignedStatus.Add(9)
        ArrICAssignedStatus.Add(37)
        ArrICAssignedStatus.Add(14)
        ArrICAssignedStatus.Add(7)
        ArrICAssignedStatus.Add(1)
        ArrICAssignedStatus.Add(26)
    End Sub

    Private Sub SetArraListExceptedStatusForStatusDropDown()
        Dim dt As New DataTable
        ArrICAssignedStatusForstatusDropDown.Clear()
        ArrICAssignedStatusForstatusDropDown.Add(5)
        ArrICAssignedStatusForstatusDropDown.Add(19)
        ArrICAssignedStatusForstatusDropDown.Add(20)
        ArrICAssignedStatusForstatusDropDown.Add(24)
        ArrICAssignedStatusForstatusDropDown.Add(28)
        ArrICAssignedStatusForstatusDropDown.Add(32)
        ArrICAssignedStatusForstatusDropDown.Add(43)
        ArrICAssignedStatusForstatusDropDown.Add(38)
        ArrICAssignedStatusForstatusDropDown.Add(33)
        ArrICAssignedStatusForstatusDropDown.Add(34)
        ArrICAssignedStatusForstatusDropDown.Add(35)
        ArrICAssignedStatusForstatusDropDown.Add(22)
        ArrICAssignedStatusForstatusDropDown.Add(8)
        ArrICAssignedStatusForstatusDropDown.Add(10)
        ArrICAssignedStatusForstatusDropDown.Add(39)
        ArrICAssignedStatusForstatusDropDown.Add(40)
        ArrICAssignedStatusForstatusDropDown.Add(9)
        ArrICAssignedStatusForstatusDropDown.Add(37)
        ArrICAssignedStatusForstatusDropDown.Add(14)
        ArrICAssignedStatusForstatusDropDown.Add(7)
        ArrICAssignedStatusForstatusDropDown.Add(1)
        ArrICAssignedStatusForstatusDropDown.Add(2)
        ArrICAssignedStatusForstatusDropDown.Add(3)
        ArrICAssignedStatusForstatusDropDown.Add(4)
        ArrICAssignedStatusForstatusDropDown.Add(13)
        ArrICAssignedStatusForstatusDropDown.Add(36)

    End Sub

    Private Sub SetArraListAssignedApprovalStatus()
        ArrICAssignedApprovalStatus.Clear()
        ArrICAssignedApprovalStatus.Add(46)     ' Approve Cancellation
        ArrICAssignedApprovalStatus.Add(45)     ' Approve Reversal
        ArrICAssignedApprovalStatus.Add(44)     ' Approve Settlement
        ArrICAssignedApprovalStatus.Add(54)     ' Approve Hold
        ArrICAssignedApprovalStatus.Add(56)     ' Approve Revert
        ArrICAssignedApprovalStatus.Add(55)     ' Approve UnHold
    End Sub

    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, "", ArrICAssignUserRolsID)
            ddlCompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlCompany.Items.Clear()
                    ddlCompany.Items.Add(lit)
                    lit.Selected = True
                    ddlCompany.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- Please Select --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
                ddlCompany.DataSource = dt
                ddlCompany.DataTextField = "CompanyName"
                ddlCompany.DataValueField = "CompanyCode"
                ddlCompany.DataBind()
                ddlCompany.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- Please Select --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlAccountPaymentNatureByUserID()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlAcccountPaymentNature.Items.Clear()
            ddlAcccountPaymentNature.Items.Add(lit)
            lit.Selected = True
            ddlAcccountPaymentNature.AppendDataBoundItems = True
            ddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForDropDown(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
            ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
            ddlAcccountPaymentNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub LoadddlAccountNumbers()
        Try
            Dim lit As New ListItem
            lit.Text = "-- Please Select --"
            lit.Value = "0"
            ddlAccount.Items.Clear()
            ddlAccount.Items.Add(lit)
            lit.Selected = True
            ddlAccount.AppendDataBoundItems = True
            ddlAccount.DataSource = ICInstructionController.GetAccountNumbersTaggedWithUserSecond(Me.UserId.ToString, ArrICAssignUserRolsID, ddlCompany.SelectedValue.ToString)
            ddlAccount.DataTextField = "MainAccountNo"
            ddlAccount.DataValueField = "AccountNumber"
            ddlAccount.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlBatcNumber()
        Try

            Dim dtAccountNumber As New DataTable
            dtAccountNumber = ViewState("AccountNumber")
            SetArraListExceptedStatus()
            Dim lit As New ListItem
            lit.Text = "-- Please Select --"
            lit.Value = "0"
            ddlBatch.Items.Clear()
            ddlBatch.Items.Add(lit)
            lit.Selected = True
            ddlBatch.AppendDataBoundItems = True
            ddlBatch.DataSource = ICInstructionController.GetAllTaggedBatchNumbersByCompanyCodeWithStatusForStatusChangeQueue(dtAccountNumber, ddlCompany.SelectedValue.ToString, ArrICAssignedStatus)
            ddlBatch.DataTextField = "FileBatchNoName"
            ddlBatch.DataValueField = "FileBatchNo"
            ddlBatch.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlProductTypeByAccountPaymentNature()
        Try
            Dim lit As New ListItem
            Dim objICAPNature As New ICAccountsPaymentNature
            ddlProductType.Items.Clear()
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlProductType.Items.Add(lit)
            ddlProductType.AppendDataBoundItems = True
            If ddlAcccountPaymentNature.SelectedValue.ToString <> "0" Then
                If objICAPNature.LoadByPrimaryKey(ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)) Then
                    ddlProductType.DataSource = ICAccountPaymentNatureProductTypeController.GetAllProductTypesByAccountAndPaymentNature(objICAPNature)
                    ddlProductType.DataTextField = "ProductTypeName"
                    ddlProductType.DataValueField = "ProductTypeCode"
                    ddlProductType.DataBind()
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadgvStatusChangeQueue(ByVal DoDataBind As Boolean)
        Try

            Dim DateType, AccountNumber, PaymentNatureCode, CompanyCode, ProductTypeCode, InstrumentNo, InstructionNo, BatchCode, ReferenceNo, status As String
            Dim AccountNmbr, BranchCode, Currency, PaymentNature, OfficeIDStr As ArrayList
            AccountNmbr = New ArrayList
            BranchCode = New ArrayList
            Currency = New ArrayList
            PaymentNature = New ArrayList
            Dim FromDate, ToDate As String

            Dim dtPaymentNature As New DataTable
            Dim dtOfficeCode As New DataTable
            OfficeIDStr = New ArrayList
            Dim dtAccountNumber As New DataTable
            dtAccountNumber = ViewState("AccountNumber")
            DateType = ""
            PaymentNatureCode = ""
            CompanyCode = "0"
            ProductTypeCode = ""
            InstructionNo = ""
            InstrumentNo = ""
            BatchCode = ""
            ReferenceNo = ""
            FromDate = Nothing
            ToDate = Nothing
            status = ""

            If rbtnlstDateSelection.SelectedValue = "Creation Date" Then
                DateType = "Creation Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Value Date" Then
                DateType = "Value Date"
            End If
            If ddlProductType.SelectedValue.ToString <> "0" Then
                ProductTypeCode = ddlProductType.SelectedValue.ToString
            End If
            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            End If
            If txtInstrumentNo.Text.ToString <> "" And txtInstrumentNo.Text.ToString <> "Enter Instrument No." Then
                InstrumentNo = txtInstrumentNo.Text.ToString
            End If
            If txtInstructionNo.Text.ToString <> "" And txtInstructionNo.Text.ToString <> "Enter Instruction No." Then
                InstructionNo = txtInstructionNo.Text.ToString
            End If
            If txtReferenceNo.Text.ToString <> "" And txtReferenceNo.Text.ToString <> "Enter Reference No." Then
                ReferenceNo = txtReferenceNo.Text.ToString
            End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                FromDate = raddpCreationFromDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If Not raddpCreationToDate.SelectedDate Is Nothing Then
                ToDate = raddpCreationToDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If ddlBatch.SelectedValue <> "0" Then
                BatchCode = ddlBatch.SelectedValue.ToString
            End If
            If ddlAction.SelectedValue = "Reverse" Or ddlAction.SelectedValue = "Settle" Or ddlAction.SelectedValue = "Cancel Instruction" Or ddlAction.SelectedValue = "Hold" Or ddlAction.SelectedValue = "Revert" Or ddlAction.SelectedValue = "UnHold" Or txtReferenceNo.Text.ToString <> "" Or txtInstrumentNo.Text.ToString <> "" Or txtInstructionNo.Text.ToString <> "" Then

                Dim arrInstStatus As New ArrayList
                arrInstStatus = GetAllInstructionStatusOnActionDropDown()

                ICInstructionController.GetAllInstructionsForStatusChangeQueue(DateType, FromDate, ToDate, dtAccountNumber, BatchCode, ProductTypeCode, InstructionNo, InstrumentNo, ReferenceNo, Me.gvStatusChangeQueue.CurrentPageIndex + 1, Me.gvStatusChangeQueue.PageSize, Me.gvStatusChangeQueue, DoDataBind, arrInstStatus, 0, "", CompanyCode, status)

                ClearSummaryAndAccountBalance()

                If DoDataBind = True Then
                    tblStatusChangeQueue.Style.Remove("display")
                    If gvStatusChangeQueue.Items.Count > 0 Then
                        gvStatusChangeQueue.Visible = True
                        lblNRF.Visible = False
                        HideAllButtons()
                        ButtonVisibleOnActionDropDown()
                    Else

                        lblNRF.Visible = True
                        gvStatusChangeQueue.Visible = False
                        btnReverse.Visible = False
                        btnSetteled.Visible = False
                        btnCancelInstructions.Visible = False
                    End If
                Else
                    lblNRF.Visible = False
                End If
            Else
                tblStatusChangeQueue.Style.Add("display", "none")
                gvStatusChangeQueue.DataSource = Nothing
                btnReverse.Visible = False
                btnSetteled.Visible = False
                btnCancelInstructions.Visible = False
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadgvStatusChangeApprovalQueue(ByVal DoDataBind As Boolean)
        Try

            Dim DateType, AccountNumber, PaymentNatureCode, CompanyCode, ProductTypeCode, InstrumentNo, InstructionNo, BatchCode, ReferenceNo, Status As String
            Dim AccountNmbr, BranchCode, Currency, PaymentNature, OfficeIDStr As ArrayList
            AccountNmbr = New ArrayList
            BranchCode = New ArrayList
            Currency = New ArrayList
            PaymentNature = New ArrayList
            Dim FromDate, ToDate As String

            Dim dtPaymentNature As New DataTable
            Dim dtOfficeCode As New DataTable
            OfficeIDStr = New ArrayList
            Dim dtAccountNumber As New DataTable
            dtAccountNumber = ViewState("AccountNumber")
            DateType = ""
            PaymentNatureCode = ""
            CompanyCode = "0"
            ProductTypeCode = ""
            InstructionNo = ""
            InstrumentNo = ""
            BatchCode = ""
            ReferenceNo = ""
            Status = ""
            FromDate = Nothing
            ToDate = Nothing

            If rbtnlstDateSelection.SelectedValue = "Creation Date" Then
                DateType = "Creation Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Value Date" Then
                DateType = "Value Date"
            End If
            If ddlProductType.SelectedValue.ToString <> "0" Then
                ProductTypeCode = ddlProductType.SelectedValue.ToString
            End If
            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            End If
            If txtInstrumentNo.Text.ToString <> "" And txtInstrumentNo.Text.ToString <> "Enter Instrument No." Then
                InstrumentNo = txtInstrumentNo.Text.ToString
            End If
            If txtInstructionNo.Text.ToString <> "" And txtInstructionNo.Text.ToString <> "Enter Instruction No." Then
                InstructionNo = txtInstructionNo.Text.ToString
            End If
            If txtReferenceNo.Text.ToString <> "" And txtReferenceNo.Text.ToString <> "Enter Reference No." Then
                ReferenceNo = txtReferenceNo.Text.ToString
            End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                FromDate = raddpCreationFromDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If Not raddpCreationToDate.SelectedDate Is Nothing Then
                ToDate = raddpCreationToDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If ddlBatch.SelectedValue <> "0" Then
                BatchCode = ddlBatch.SelectedValue.ToString
            End If

            If ddlAction.SelectedValue = "Approve Reversal" Or ddlAction.SelectedValue = "Approve Settlement" Or ddlAction.SelectedValue = "Approve Cancellation" Or ddlAction.SelectedValue = "Approve Hold" Or ddlAction.SelectedValue = "Approve Revert" Or ddlAction.SelectedValue = "Approve UnHold" Or ddlAction.SelectedValue = "Cancel Approval" Or txtReferenceNo.Text.ToString <> "" Or txtInstrumentNo.Text.ToString <> "" Or txtInstructionNo.Text.ToString <> "" Then
                SetArraListAssignedApprovalStatus()

                Dim arrInstStatus As New ArrayList
                arrInstStatus = GetAllInstructionStatusOnActionDropDown()

                ICInstructionController.GetAllInstructionsForStatusChangeApprovalQueue(DateType, FromDate, ToDate, dtAccountNumber, BatchCode, ProductTypeCode, InstructionNo, InstrumentNo, ReferenceNo, Me.gvStatusChangeApprovalQueue.CurrentPageIndex + 1, Me.gvStatusChangeApprovalQueue.PageSize, Me.gvStatusChangeApprovalQueue, DoDataBind, arrInstStatus, 0, "", CompanyCode, Status)

                ClearSummaryAndAccountBalance()

                If DoDataBind = True Then
                    tblApprovalQueue.Style.Remove("display")
                    If gvStatusChangeApprovalQueue.Items.Count > 0 Then
                        gvStatusChangeApprovalQueue.Visible = True
                        lblNRFApprovalGrid.Visible = False
                        lblApprovalRemarks.Visible = True
                        txtApprovalRemarks.Visible = True
                        HideAllButtons()
                        ButtonVisibleOnActionDropDown()
                    Else

                        lblNRFApprovalGrid.Visible = True
                        gvStatusChangeApprovalQueue.Visible = False
                        btnApproveReversal.Visible = False
                        btnApproveSettlement.Visible = False
                        btnCancelApproval.Visible = False
                        btnApproveCancellation.Visible = False
                        lblApprovalRemarks.Visible = False
                        txtApprovalRemarks.Visible = False
                    End If
                Else
                    lblNRFApprovalGrid.Visible = False
                End If
            Else
                tblApprovalQueue.Style.Add("display", "none")
                gvStatusChangeApprovalQueue.DataSource = Nothing
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Function IsApprovalRihtsAssigned() As Boolean
        Dim Result As Boolean = False
        If CBool(htRights("Approve Reversal")) = True Or CBool(htRights("Approve Settlement")) = True Or CBool(htRights("Approve Cancellation")) = True Or CBool(htRights("Approve Hold")) = True Or CBool(htRights("Approve Revert")) = True Or CBool(htRights("Approve UnHold")) = True Or CBool(htRights("Cancel Approval")) = True Then
            Result = True
        End If
        Return Result
    End Function

    Private Function GetAllInstructionStatusOnActionDropDown() As ArrayList
        Dim ArryListRequiredStatus As New ArrayList

        If ddlAction.SelectedValue = "Reverse" Then
            ArryListRequiredStatus.Add(30)
            ArryListRequiredStatus.Add(49)

        ElseIf ddlAction.SelectedValue = "Settle" Then
            ArryListRequiredStatus.Add(16)
            ArryListRequiredStatus.Add(18)
            ArryListRequiredStatus.Add(21)
            ArryListRequiredStatus.Add(12)
            ArryListRequiredStatus.Add(23)
            ArryListRequiredStatus.Add(29)
            ArryListRequiredStatus.Add(31)

        ElseIf ddlAction.SelectedValue = "Cancel Instruction" Then
            ArryListRequiredStatus.Add(16)
            ArryListRequiredStatus.Add(18)
            ArryListRequiredStatus.Add(12)
            ArryListRequiredStatus.Add(21)
            ArryListRequiredStatus.Add(23)
            ArryListRequiredStatus.Add(29)
            ArryListRequiredStatus.Add(31)
            ArryListRequiredStatus.Add(48)
        ElseIf ddlAction.SelectedValue = "Hold" Then
            ArryListRequiredStatus.Add(21)
            ArryListRequiredStatus.Add(48)
        ElseIf ddlAction.SelectedValue = "Revert" Then
            ArryListRequiredStatus.Add(39)
            'ArryListRequiredStatus.Add(1)
            'ArryListRequiredStatus.Add(32)
            ArryListRequiredStatus.Add(37)
            ArryListRequiredStatus.Add(52)

        ElseIf ddlAction.SelectedValue = "UnHold" Then
            ArryListRequiredStatus.Add(53)

        ElseIf ddlAction.SelectedValue = "Cancel Approval" Then
            ArryListRequiredStatus.Add(44)
            ArryListRequiredStatus.Add(45)
            ArryListRequiredStatus.Add(46)

        ElseIf ddlAction.SelectedValue = "Approve Cancellation" Then
            ArryListRequiredStatus.Add(46)

        ElseIf ddlAction.SelectedValue = "Approve Hold" Then
            ArryListRequiredStatus.Add(54)

        ElseIf ddlAction.SelectedValue = "Approve Reversal" Then
            ArryListRequiredStatus.Add(45)

        ElseIf ddlAction.SelectedValue = "Approve Revert" Then
            ArryListRequiredStatus.Add(56)

        ElseIf ddlAction.SelectedValue = "Approve Settlement" Then
            ArryListRequiredStatus.Add(44)

        ElseIf ddlAction.SelectedValue = "Approve UnHold" Then
            ArryListRequiredStatus.Add(55)

        End If

        Return ArryListRequiredStatus
    End Function

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Try
                txtRemarks.Text = ""
                txtApprovalRemarks.Text = ""

                If txtReferenceNo.Text.ToString <> "" Or txtInstrumentNo.Text.ToString <> "" Or txtInstructionNo.Text.ToString <> "" Then
                    If ddlAction.SelectedValue = "Reverse" Or ddlAction.SelectedValue = "Settle" Or ddlAction.SelectedValue = "Cancel Instruction" Or ddlAction.SelectedValue = "Hold" Or ddlAction.SelectedValue = "Revert" Or ddlAction.SelectedValue = "UnHold" Then

                        LoadgvStatusChangeQueue(True)

                    End If
                    If ddlAction.SelectedValue = "Approve Reversal" Or ddlAction.SelectedValue = "Approve Settlement" Or ddlAction.SelectedValue = "Approve Cancellation" Or ddlAction.SelectedValue = "Approve Hold" Or ddlAction.SelectedValue = "Approve Revert" Or ddlAction.SelectedValue = "Approve UnHold" Then

                        LoadgvStatusChangeApprovalQueue(True)

                    End If

                Else
                    LoadgvStatusChangeQueue(False)
                    LoadgvStatusChangeApprovalQueue(False)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Please select filter for searching.", ICBO.IC.Dialogmessagetype.Failure)
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub
    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadddlAccountNumbers()
            LoadddlAccountPaymentNatureByUserID()
            LoadddlBatcNumber()
            LoadddlAction()
            LoadddlProductTypeByAccountPaymentNature()
            ClaerCreditAccountNoTextBoxes()
            ddlCBAccountType.ClearSelection()
            tblCreditAccountNumber.Style.Add("display", "none")
            tblCBAccountType.Style.Add("display", "none")

            ddlAction_SelectedIndexChanged(ddlAction.SelectedValue, Nothing)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub ddlAcccountPaymentNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcccountPaymentNature.SelectedIndexChanged
        Try
            If ddlAcccountPaymentNature.SelectedValue.ToString = "0" Then
                SetArraListRoleID()
                LoadddlAccountNumbers()
                LoadddlAction()
                LoadddlProductTypeByAccountPaymentNature()
                FillDesignedDtByAPNatureByAssignedRole()
                LoadddlBatcNumber()
            Else
                LoadddlAccountNumbers()
                FillDesignedDtByAPNatureByAssignedRole()
                LoadddlAction()
                LoadddlProductTypeByAccountPaymentNature()
                SetViewStateDtOnAPNatureIndexChnage()
                LoadddlBatcNumber()

            End If
            ClaerCreditAccountNoTextBoxes()
            ddlCBAccountType.ClearSelection()
            tblCreditAccountNumber.Style.Add("display", "none")
            tblCBAccountType.Style.Add("display", "none")

            ddlAction_SelectedIndexChanged(ddlAction.SelectedValue, Nothing)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub SetViewStateDtOnAPNatureIndexChnage()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim AccountNumber, BranchCode, Currency, PaymentNatureCode As String
        Dim ArrayListOfficeID As New ArrayList
        Dim drAccountNo As DataRow
        AccountNumber = Nothing
        BranchCode = Nothing
        Currency = Nothing
        PaymentNatureCode = Nothing
        dtAccountNumber = ViewState("AccountNumber")
        ViewState("AccountNumber") = Nothing
        AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0)
        BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1)
        Currency = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2)
        PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)
        For Each dr As DataRow In dtAccountNumber.Rows
            If dr("AccountNumber").ToString.Split("-")(0) = AccountNumber And dr("AccountNumber").ToString.Split("-")(1) = BranchCode And dr("AccountNumber").ToString.Split("-")(2) = Currency And dr("PaymentNature").ToString = PaymentNatureCode Then
                ArrayListOfficeID.Add(dr("OfficeID").ToString)
            End If
        Next
        dtAccountNumber.Rows.Clear()
        If ArrayListOfficeID.Count > 0 Then
            For Each OfficeID In ArrayListOfficeID
                drAccountNo = dtAccountNumber.NewRow()
                drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
                drAccountNo("PaymentNature") = PaymentNatureCode
                drAccountNo("OfficeID") = OfficeID
                dtAccountNumber.Rows.Add(drAccountNo)
            Next
        Else
            drAccountNo = dtAccountNumber.NewRow()
            drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
            drAccountNo("PaymentNature") = PaymentNatureCode
            drAccountNo("OfficeID") = "0"
            dtAccountNumber.Rows.Add(drAccountNo)
        End If

        For Each dr As DataRow In dtAccountNumber.Rows
            dr("DropDown") = "True"
        Next
        ViewState("AccountNumber") = dtAccountNumber

    End Sub

    Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged
        Try
            SetArraListRoleID()
            ClaerCreditAccountNoTextBoxes()
            LoadddlAction()
            ddlCBAccountType.ClearSelection()
            tblCreditAccountNumber.Style.Add("display", "none")
            tblCBAccountType.Style.Add("display", "none")

            ddlAction_SelectedIndexChanged(ddlAction.SelectedValue, Nothing)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAuthorization_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvStatusChangeQueue.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvStatusChangeQueue.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvStatusChangeApprovalQueue_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvStatusChangeApprovalQueue.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvStatusChangeApprovalQueue.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAuthorization_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvStatusChangeQueue.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvStatusChangeQueue.MasterTableView.ClientID & "','0');"
        End If
        Dim imgBtn As LinkButton
        Dim AppPath As String = "" ' DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim objICUser As New ICUser
            objICUser.LoadByPrimaryKey(Me.UserId.ToString)
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            imgBtn = New LinkButton
            imgBtn = DirectCast(item.Cells(1).FindControl("lbInstructionID"), LinkButton)

            If CBool(htRights("View Instruction Details")) = True Then
                imgBtn.Enabled = True
                imgBtn.OnClientClick = "showurldialog('Instruction Details',' " & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & item.GetDataKeyValue("InstructionID").ToString().EncryptString() & "' ,true,700,650);return false;"
            End If
            If item.GetDataKeyValue("Status") = "30" And (item.GetDataKeyValue("PaymentMode") = "PO" Or item.GetDataKeyValue("PaymentMode") = "DD" Or item.GetDataKeyValue("PaymentMode") = "Cheque") Then
                If Not item.GetDataKeyValue("ClearingOfficeCode") Is Nothing Then
                    If (item.GetDataKeyValue("ClearingOfficeCode") = objICUser.OfficeCode.ToString) Then
                        If item.GetDataKeyValue("IsAmendmentComplete") = True Then
                            gvStatusChangeQueue.AlternatingItemStyle.CssClass = Nothing
                            e.Item.CssClass = Nothing
                            gvStatusChangeQueue.AlternatingItemStyle.CssClass = "GridItemAmendedAndClearingReversal"
                            'gvAmendment.ItemStyle.BackColor = Color.Red
                            e.Item.CssClass = "GridItemAmendedAndClearingReversal"
                        Else
                            gvStatusChangeQueue.AlternatingItemStyle.CssClass = Nothing
                            e.Item.CssClass = Nothing
                            gvStatusChangeQueue.AlternatingItemStyle.CssClass = "GridItemClearingReversal"
                            'gvAmendment.ItemStyle.BackColor = Color.Red
                            e.Item.CssClass = "GridItemClearingReversal"
                        End If
                    End If
                Else
                    If item.GetDataKeyValue("IsAmendmentComplete") = True Then
                        gvStatusChangeQueue.AlternatingItemStyle.CssClass = Nothing
                        e.Item.CssClass = Nothing
                        gvStatusChangeQueue.AlternatingItemStyle.CssClass = "GridItemComplete"
                        'gvAmendment.ItemStyle.BackColor = Color.Red
                        e.Item.CssClass = "GridItemComplete"
                    Else
                        gvStatusChangeQueue.AlternatingItemStyle.CssClass = Nothing
                        e.Item.CssClass = Nothing
                        gvStatusChangeQueue.AlternatingItemStyle.CssClass = "GridItem"
                        'gvAmendment.ItemStyle.BackColor = Color.Red
                        e.Item.CssClass = "GridItem"
                    End If
                End If
            Else
                If item.GetDataKeyValue("IsAmendmentComplete") = True Then
                    gvStatusChangeQueue.AlternatingItemStyle.CssClass = Nothing
                    e.Item.CssClass = Nothing
                    gvStatusChangeQueue.AlternatingItemStyle.CssClass = "GridItemComplete"
                    'gvAmendment.ItemStyle.BackColor = Color.Red
                    e.Item.CssClass = "GridItemComplete"
                Else
                    gvStatusChangeQueue.AlternatingItemStyle.CssClass = Nothing
                    e.Item.CssClass = Nothing
                    gvStatusChangeQueue.AlternatingItemStyle.CssClass = "GridItem"
                    'gvAmendment.ItemStyle.BackColor = Color.Red
                    e.Item.CssClass = "GridItem"
                End If
            End If
        End If
    End Sub

    Protected Sub gvStatusChangeApprovalQueue_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvStatusChangeApprovalQueue.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAllApproval"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvStatusChangeApprovalQueue.MasterTableView.ClientID & "','0');"
        End If
        Dim imgBtn As LinkButton
        Dim AppPath As String = "" ' DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim objICUser As New ICUser
            objICUser.LoadByPrimaryKey(Me.UserId.ToString)
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            imgBtn = New LinkButton
            imgBtn = DirectCast(item.Cells(1).FindControl("lbInstructionIDApproval"), LinkButton)

            If CBool(htRights("View Instruction Details")) = True Then
                imgBtn.Enabled = True
                imgBtn.OnClientClick = "showurldialog('Instruction Details',' " & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & item.GetDataKeyValue("InstructionID").ToString().EncryptString() & "' ,true,700,650);return false;"
            End If
            If item.GetDataKeyValue("Status") = "45" And (item.GetDataKeyValue("PaymentMode") = "PO" Or item.GetDataKeyValue("PaymentMode") = "DD" Or item.GetDataKeyValue("PaymentMode") = "Cheque") Then
                If Not item.GetDataKeyValue("ClearingOfficeCode") Is Nothing Then
                    If (item.GetDataKeyValue("ClearingOfficeCode") = objICUser.OfficeCode.ToString) Then
                        If item.GetDataKeyValue("IsAmendmentComplete") = True Then
                            gvStatusChangeQueue.AlternatingItemStyle.CssClass = ""
                            e.Item.CssClass = ""
                            gvStatusChangeQueue.AlternatingItemStyle.CssClass = "GridItemAmendedAndClearingReversal"
                            'gvAmendment.ItemStyle.BackColor = Color.Red
                            e.Item.CssClass = "GridItemAmendedAndClearingReversal"
                        Else
                            gvStatusChangeQueue.AlternatingItemStyle.CssClass = ""
                            e.Item.CssClass = ""
                            gvStatusChangeQueue.AlternatingItemStyle.CssClass = "GridItemClearingReversal"
                            'gvAmendment.ItemStyle.BackColor = Color.Red
                            e.Item.CssClass = "GridItemClearingReversal"
                        End If
                    Else
                        If item.GetDataKeyValue("IsAmendmentComplete") = True Then
                            gvStatusChangeQueue.AlternatingItemStyle.CssClass = ""
                            e.Item.CssClass = ""

                            gvStatusChangeQueue.AlternatingItemStyle.CssClass = "GridItemComplete"
                            'gvAmendment.ItemStyle.BackColor = Color.Red
                            e.Item.CssClass = "GridItemComplete"
                        Else
                            gvStatusChangeQueue.AlternatingItemStyle.CssClass = ""
                            e.Item.CssClass = ""
                            gvStatusChangeQueue.AlternatingItemStyle.CssClass = "GridItem"
                            'gvAmendment.ItemStyle.BackColor = Color.Red
                            e.Item.CssClass = "GridItem"
                        End If
                    End If
                Else
                    If item.GetDataKeyValue("IsAmendmentComplete") = True Then
                        gvStatusChangeQueue.AlternatingItemStyle.CssClass = ""
                        e.Item.CssClass = ""

                        gvStatusChangeQueue.AlternatingItemStyle.CssClass = "GridItemComplete"
                        'gvAmendment.ItemStyle.BackColor = Color.Red
                        e.Item.CssClass = "GridItemComplete"
                    Else
                        gvStatusChangeQueue.AlternatingItemStyle.CssClass = ""
                        e.Item.CssClass = ""
                        gvStatusChangeQueue.AlternatingItemStyle.CssClass = "GridItem"
                        'gvAmendment.ItemStyle.BackColor = Color.Red
                        e.Item.CssClass = "GridItem"
                    End If
                End If
            Else
                If item.GetDataKeyValue("IsAmendmentComplete") = True Then
                    gvStatusChangeQueue.AlternatingItemStyle.CssClass = ""
                    e.Item.CssClass = ""
                    gvStatusChangeQueue.AlternatingItemStyle.CssClass = "GridItemComplete"
                    'gvAmendment.ItemStyle.BackColor = Color.Red
                    e.Item.CssClass = "GridItemComplete"
                Else
                    gvStatusChangeQueue.AlternatingItemStyle.CssClass = Nothing
                    e.Item.CssClass = Nothing
                    gvStatusChangeQueue.AlternatingItemStyle.CssClass = "GridItem"
                    'gvAmendment.ItemStyle.BackColor = Color.Red
                    e.Item.CssClass = "GridItem"
                End If
            End If
        End If
    End Sub

    Protected Sub gvAuthorization_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvStatusChangeQueue.NeedDataSource
        Try
            'ClearAllTextBoxes()
            SetArraListRoleID()
            LoadgvStatusChangeQueue(False)
            'SetJavaScript()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub gvStatusChangeApprovalQueue_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvStatusChangeApprovalQueue.NeedDataSource
        Try
            'ClearAllTextBoxes()
            SetArraListRoleID()
            LoadgvStatusChangeApprovalQueue(False)
            'SetJavaScript()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub ddlBatch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBatch.SelectedIndexChanged
        Try
            SetArraListRoleID()
            LoadddlAccountNumbers()
            LoadddlProductTypeByAccountPaymentNature()
            LoadddlAction()
            ClaerCreditAccountNoTextBoxes()
            ddlCBAccountType.ClearSelection()
            tblCreditAccountNumber.Style.Add("display", "none")
            tblCBAccountType.Style.Add("display", "none")

            ddlAction_SelectedIndexChanged(ddlAction.SelectedValue, Nothing)

            'If ddlBatch.SelectedValue.ToString() <> "0" Then
            '    LoadgvStatusChangeQueue(True)
            '    LoadgvStatusChangeApprovalQueue(True)
            'Else
            '    LoadgvStatusChangeQueue(False)
            '    LoadgvStatusChangeApprovalQueue(False)
            'End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ClearAllTextBoxes()
        txtRemarks.Text = ""
        txtAvailableBalance.Text = ""
        txtInstructionNo.Text = ""
        txtInstrumentNo.Text = ""
        txtReferenceNo.Text = ""
        txtAvailableBalance.Text = ""
        'txtCurrency.Text = ""
        'txtPaymentMode.Text = ""
        txtTnxCount.Text = ""
        txtTotalAmount.Text = ""
        txtApprovalRemarks.Text = ""

    End Sub
    Private Function CheckgvInstructionAuthentication() As Boolean
        Try

            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVInstruction In gvStatusChangeQueue.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVInstruction.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function CheckgvInstructionApprovalStatusChangeQueue() As Boolean
        Try

            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVInstruction In gvStatusChangeApprovalQueue.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVInstruction.Cells(0).FindControl("chkSelectApproval"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSummary.Click

        Try

                Dim InstructionCount As Double = 0
                Dim TotalAmount As Double = 0
                Dim TotalAmountInInstruction As Double
                Dim TotalCurrencyUsed, PassesCurrencyName, CurrencyInRow As String
                Dim TotalPaymentModeUsed, PassesPaymentModeName As String
                Dim chkSelect As CheckBox

                If gvStatusChangeQueue.Items.Count > 0 And gvStatusChangeApprovalQueue.Items.Count > 0 Then
                    If CheckgvInstructionAuthentication() = False Then
                        UIUtilities.ShowDialog(Me, "Instruction Verification", "Please select at least one instruction to view summary", ICBO.IC.Dialogmessagetype.Failure)
                        txtTnxCount.Text = ""
                        txtTotalAmount.Text = ""
                        Exit Sub
                    End If
                    txtTnxCount.Text = ""
                    txtTotalAmount.Text = ""
                    TotalCurrencyUsed = ""
                    TotalPaymentModeUsed = ""
                    PassesCurrencyName = ""
                    PassesPaymentModeName = ""
                    For Each gvInsctructionAuthenticationRow As GridDataItem In gvStatusChangeQueue.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(gvInsctructionAuthenticationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            TotalAmountInInstruction = 0
                            CurrencyInRow = ""

                            InstructionCount = InstructionCount + 1
                            TotalAmountInInstruction = CDbl(gvInsctructionAuthenticationRow.Item("Amount").Text)
                            TotalAmount = TotalAmount + TotalAmountInInstruction


                        End If
                    Next

                    If Not TotalAmount = 0 Then
                        txtTotalAmount.Text = CDbl(TotalAmount).ToString("N2")
                    End If
                    If Not InstructionCount = 0 Then
                        txtTnxCount.Text = CInt(InstructionCount)
                    End If

                    InstructionCount = 0
                    TotalAmount = 0
                    TotalCurrencyUsed = ""
                    TotalPaymentModeUsed = ""
                    PassesPaymentModeName = ""
                    PassesCurrencyName = ""
                ElseIf gvStatusChangeQueue.Items.Count > 0 And gvStatusChangeApprovalQueue.Items.Count = 0 Then
                    If CheckgvInstructionAuthentication() = False Then
                        UIUtilities.ShowDialog(Me, "Instruction Verification", "Please select at least one instruction to view summary", ICBO.IC.Dialogmessagetype.Failure)
                        txtTnxCount.Text = ""
                        txtTotalAmount.Text = ""
                        Exit Sub
                    End If
                    txtTnxCount.Text = ""
                    txtTotalAmount.Text = ""
                    TotalCurrencyUsed = ""
                    TotalPaymentModeUsed = ""
                    PassesCurrencyName = ""
                    PassesPaymentModeName = ""
                    For Each gvInsctructionAuthenticationRow As GridDataItem In gvStatusChangeQueue.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(gvInsctructionAuthenticationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            TotalAmountInInstruction = 0
                            CurrencyInRow = ""

                            InstructionCount = InstructionCount + 1
                            TotalAmountInInstruction = CDbl(gvInsctructionAuthenticationRow.Item("Amount").Text)
                            TotalAmount = TotalAmount + TotalAmountInInstruction


                        End If
                    Next

                    If Not TotalAmount = 0 Then
                        txtTotalAmount.Text = CDbl(TotalAmount).ToString("N2")
                    End If
                    If Not InstructionCount = 0 Then
                        txtTnxCount.Text = CInt(InstructionCount)
                    End If

                    InstructionCount = 0
                    TotalAmount = 0
                    TotalCurrencyUsed = ""
                    TotalPaymentModeUsed = ""
                    PassesPaymentModeName = ""
                    PassesCurrencyName = ""
                ElseIf gvStatusChangeQueue.Items.Count = 0 And gvStatusChangeApprovalQueue.Items.Count > 0 Then
                    If CheckgvInstructionApprovalStatusChangeQueue() = False Then
                        UIUtilities.ShowDialog(Me, "Instruction Verification", "Please select at least one instruction to view summary", ICBO.IC.Dialogmessagetype.Failure)
                        txtTnxCount.Text = ""
                        txtTotalAmount.Text = ""
                        Exit Sub
                    End If
                    txtTnxCount.Text = ""
                    txtTotalAmount.Text = ""
                    TotalCurrencyUsed = ""
                    TotalPaymentModeUsed = ""
                    PassesCurrencyName = ""
                    PassesPaymentModeName = ""
                    For Each gvInsctructionAuthenticationRow As GridDataItem In gvStatusChangeApprovalQueue.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(gvInsctructionAuthenticationRow.Cells(0).FindControl("chkSelectApproval"), CheckBox)
                        If chkSelect.Checked = True Then
                            TotalAmountInInstruction = 0
                            CurrencyInRow = ""

                            InstructionCount = InstructionCount + 1
                            TotalAmountInInstruction = CDbl(gvInsctructionAuthenticationRow.Item("Amount").Text)
                            TotalAmount = TotalAmount + TotalAmountInInstruction


                        End If
                    Next

                    If Not TotalAmount = 0 Then
                        txtTotalAmount.Text = CDbl(TotalAmount).ToString("N2")
                    End If
                    If Not InstructionCount = 0 Then
                        txtTnxCount.Text = CInt(InstructionCount)
                    End If

                    InstructionCount = 0
                    TotalAmount = 0
                    TotalCurrencyUsed = ""
                    TotalPaymentModeUsed = ""
                    PassesPaymentModeName = ""
                    PassesCurrencyName = ""
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub
    Private Function CheckInstructionRequiredStatus(ByVal ArrayListRequiredStatus As ArrayList) As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim objICInstructionColl As New ICInstructionCollection
            Dim ArraListInstID As New ArrayList
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvStatusChangeQueue.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    ArraListInstID.Add(rowGVICClientRoles.GetDataKeyValue("InstructionID").ToString)
                Else
                    Result = False
                    Return Result
                    Exit For
                    Exit Function
                End If
            Next
            objICInstructionColl.Query.Where(objICInstructionColl.Query.InstructionID.In(ArraListInstID) And objICInstructionColl.Query.Status.In(ArrayListRequiredStatus))
            objICInstructionColl.Query.Load()
            If objICInstructionColl.Count > 0 Then
                Result = True
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function CheckInstructionRequiredStatusSecondForReversal(ByVal ArrayListRequiredStatus As ArrayList) As String
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim objICInstructionColl As New ICInstructionCollection
            Dim ArraListInstID As New ArrayList
            Dim chkSelect As CheckBox
            Dim ResultString As String = ""
            For Each rowGVICClientRoles In gvStatusChangeApprovalQueue.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelectApproval"), CheckBox)
                If chkSelect.Checked = True Then
                    If ArrayListRequiredStatus.Contains(rowGVICClientRoles.GetDataKeyValue("Status")) = True Then
                        ArraListInstID.Add(rowGVICClientRoles.GetDataKeyValue("InstructionID").ToString)
                    Else
                        ResultString += rowGVICClientRoles.GetDataKeyValue("InstructionID").ToString & ","
                    End If
                End If
            Next
            If ResultString = "" Then
                objICInstructionColl.Query.Where(objICInstructionColl.Query.InstructionID.In(ArraListInstID) And objICInstructionColl.Query.Status.In(ArrayListRequiredStatus))
                objICInstructionColl.Query.Load()
                If objICInstructionColl.Count > 0 Then
                    ResultString = "OK"
                End If
            Else
                ResultString = ResultString.Remove(ResultString.Length - 1, 1)
            End If
            Return ResultString
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function CheckInstructionRequiredStatusSecond(ByVal ArrayListRequiredStatus As ArrayList) As String
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim objICInstructionColl As New ICInstructionCollection
            Dim ArraListInstID As New ArrayList
            Dim chkSelect As CheckBox
            Dim ResultString As String = ""
            For Each rowGVICClientRoles In gvStatusChangeQueue.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    If ArrayListRequiredStatus.Contains(rowGVICClientRoles.GetDataKeyValue("Status")) = True Then
                        ArraListInstID.Add(rowGVICClientRoles.GetDataKeyValue("InstructionID").ToString)
                    Else
                        ResultString += rowGVICClientRoles.GetDataKeyValue("InstructionID").ToString & ","
                    End If
                End If
            Next
            If ResultString = "" Then
                objICInstructionColl.Query.Where(objICInstructionColl.Query.InstructionID.In(ArraListInstID) And objICInstructionColl.Query.Status.In(ArrayListRequiredStatus))
                objICInstructionColl.Query.Load()
                If objICInstructionColl.Count > 0 Then
                    ResultString = "OK"
                End If
            Else
                ResultString = ResultString.Remove(ResultString.Length - 1, 1)
            End If
            Return ResultString
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function GetInstructionCollWithRequiredStatus(ByVal ArrayListRequiredStatus As ArrayList) As ICInstructionCollection
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim objICInstructionColl As New ICInstructionCollection
            Dim ArraListInstID As New ArrayList
            Dim chkSelect As CheckBox
            Dim objICUser As New ICUser
            objICUser.LoadByPrimaryKey(Me.UserId.ToString)
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvStatusChangeQueue.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    ArraListInstID.Add(rowGVICClientRoles.GetDataKeyValue("InstructionID").ToString)
                End If
            Next
            If ArraListInstID.Count > 0 Then
                objICInstructionColl.Query.Where(objICInstructionColl.Query.InstructionID.In(ArraListInstID) And objICInstructionColl.Query.Status.In(ArrayListRequiredStatus))
                objICInstructionColl.Query.Load()
            End If
            Return objICInstructionColl
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function GetInstructionCollWithRequiredStatusForReversal(ByVal ArrayListRequiredStatus As ArrayList) As ICInstructionCollection
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim objICInstructionColl As New ICInstructionCollection
            Dim objICInstructionCollFinal As New ICInstructionCollection
            Dim objICUser As New ICUser
            objICUser.LoadByPrimaryKey(Me.UserId.ToString)
            Dim ArraListInstID As New ArrayList
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvStatusChangeQueue.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    ArraListInstID.Add(rowGVICClientRoles.GetDataKeyValue("InstructionID").ToString)
                End If
            Next
            If ArraListInstID.Count > 0 Then
                objICInstructionColl.Query.Where(objICInstructionColl.Query.InstructionID.In(ArraListInstID) And objICInstructionColl.Query.Status.In(ArrayListRequiredStatus))
                objICInstructionColl.Query.Where(objICInstructionColl.Query.PaymentMode <> "Direct Credit" And objICInstructionColl.Query.PaymentMode <> "Other Credit")


                objICInstructionColl.Query.Load()
            End If

            If objICInstructionColl.Count > 0 Then
                For Each objICInstruction As ICInstruction In objICInstructionColl
                    objICInstructionCollFinal.Add(objICInstruction)
                    'If objICInstruction.PaymentMode <> "COTC" Then
                    '    If Me.UserInfo.IsSuperUser = False Then
                    '        If objICInstruction.ClearingOfficeCode = objICUser.OfficeCode.ToString Then
                    '            objICInstructionCollFinal.Add(objICInstruction)
                    '        End If

                    '        'objICInstructionColl.Query.Where(objICInstructionColl.Query.ClearingOfficeCode = objICUser.OfficeCode.ToString)

                    '    End If
                    'Else
                    '    objICInstructionCollFinal.Add(objICInstruction)
                    'End If
                Next
            End If


            Return objICInstructionCollFinal
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function GetInstructionCollWithRequiredStatusForReversalCOTC(ByVal ArrayListRequiredStatus As ArrayList) As ICInstructionCollection
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim objICInstructionColl As New ICInstructionCollection
            Dim objICUser As New ICUser
            objICUser.LoadByPrimaryKey(Me.UserId.ToString)
            Dim ArraListInstID As New ArrayList
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvStatusChangeQueue.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    ArraListInstID.Add(rowGVICClientRoles.GetDataKeyValue("InstructionID").ToString)
                End If
            Next
            If ArraListInstID.Count > 0 Then
                objICInstructionColl.Query.Where(objICInstructionColl.Query.InstructionID.In(ArraListInstID) And objICInstructionColl.Query.Status.In(ArrayListRequiredStatus))
                objICInstructionColl.Query.Where(objICInstructionColl.Query.PaymentMode <> "Direct Credit" And objICInstructionColl.Query.PaymentMode <> "Other Credit")
                'If Me.UserInfo.IsSuperUser = False Then

                '    objICInstructionColl.Query.Where(objICInstructionColl.Query.ClearingOfficeCode = objICUser.OfficeCode.ToString)

                'End If

                objICInstructionColl.Query.Load()
            End If
            Return objICInstructionColl
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function GetInstructionCollWithRequiredStatusForHold(ByVal ArrayListRequiredStatus As ArrayList) As ICInstructionCollection
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim objICInstructionColl As New ICInstructionCollection
            Dim objICUser As New ICUser
            objICUser.LoadByPrimaryKey(Me.UserId.ToString)
            Dim ArraListInstID As New ArrayList
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvStatusChangeQueue.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    ArraListInstID.Add(rowGVICClientRoles.GetDataKeyValue("InstructionID").ToString)
                End If
            Next
            If ArraListInstID.Count > 0 Then
                objICInstructionColl.Query.Where(objICInstructionColl.Query.InstructionID.In(ArraListInstID) And objICInstructionColl.Query.Status.In(ArrayListRequiredStatus))
                'objICInstructionColl.Query.Where(objICInstructionColl.Query.PaymentMode <> "Direct Credit" And objICInstructionColl.Query.PaymentMode <> "Other Credit")
                'If Me.UserInfo.IsSuperUser = False Then

                '    objICInstructionColl.Query.Where(objICInstructionColl.Query.ClearingOfficeCode = objICUser.OfficeCode.ToString)

                'End If

                objICInstructionColl.Query.Load()
            End If
            Return objICInstructionColl
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Private Function GetInstructionCollWithRequiredStatusForRevert(ByVal ArrayListRequiredStatus As ArrayList) As ICInstructionCollection
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim objICInstructionColl As New ICInstructionCollection
            Dim objICUser As New ICUser
            objICUser.LoadByPrimaryKey(Me.UserId.ToString)
            Dim ArraListInstID As New ArrayList
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvStatusChangeQueue.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    ArraListInstID.Add(rowGVICClientRoles.GetDataKeyValue("InstructionID").ToString)
                End If
            Next
            If ArraListInstID.Count > 0 Then
                objICInstructionColl.Query.Where(objICInstructionColl.Query.InstructionID.In(ArraListInstID) And objICInstructionColl.Query.Status.In(ArrayListRequiredStatus))
                'objICInstructionColl.Query.Where(objICInstructionColl.Query.PaymentMode <> "Direct Credit" And objICInstructionColl.Query.PaymentMode <> "Other Credit")
                'If Me.UserInfo.IsSuperUser = False Then

                '    objICInstructionColl.Query.Where(objICInstructionColl.Query.ClearingOfficeCode = objICUser.OfficeCode.ToString)

                'End If

                objICInstructionColl.Query.Load()
            End If
            Return objICInstructionColl
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Private Function GetInstructionCollWithRequiredStatusForUnHold(ByVal ArrayListRequiredStatus As ArrayList) As ICInstructionCollection
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim objICInstructionColl As New ICInstructionCollection
            Dim objICUser As New ICUser
            objICUser.LoadByPrimaryKey(Me.UserId.ToString)
            Dim ArraListInstID As New ArrayList
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvStatusChangeQueue.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    ArraListInstID.Add(rowGVICClientRoles.GetDataKeyValue("InstructionID").ToString)
                End If
            Next
            If ArraListInstID.Count > 0 Then
                objICInstructionColl.Query.Where(objICInstructionColl.Query.InstructionID.In(ArraListInstID) And objICInstructionColl.Query.Status.In(ArrayListRequiredStatus))
                'objICInstructionColl.Query.Where(objICInstructionColl.Query.PaymentMode <> "Direct Credit" And objICInstructionColl.Query.PaymentMode <> "Other Credit")
                'If Me.UserInfo.IsSuperUser = False Then

                '    objICInstructionColl.Query.Where(objICInstructionColl.Query.ClearingOfficeCode = objICUser.OfficeCode.ToString)

                'End If

                objICInstructionColl.Query.Load()
            End If
            Return objICInstructionColl
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Private Function GetInstructionCollWithRequiredStatusForReversalApproval(ByVal ArrayListRequiredStatus As ArrayList) As ICInstructionCollection
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim objICInstructionColl As New ICInstructionCollection
            Dim objICInstructionCollFinal As New ICInstructionCollection
            Dim objICUser As New ICUser
            objICUser.LoadByPrimaryKey(Me.UserId.ToString)
            Dim ArraListInstID As New ArrayList
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvStatusChangeApprovalQueue.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelectApproval"), CheckBox)
                If chkSelect.Checked = True Then
                    ArraListInstID.Add(rowGVICClientRoles.GetDataKeyValue("InstructionID").ToString)
                End If
            Next
            If ArraListInstID.Count > 0 Then
                objICInstructionColl.Query.Where(objICInstructionColl.Query.InstructionID.In(ArraListInstID) And objICInstructionColl.Query.Status.In(ArrayListRequiredStatus))
                objICInstructionColl.Query.Where(objICInstructionColl.Query.PaymentMode <> "Direct Credit" And objICInstructionColl.Query.PaymentMode <> "Other Credit")
                'If Me.UserInfo.IsSuperUser = False Then

                '    objICInstructionColl.Query.Where(objICInstructionColl.Query.ClearingOfficeCode = objICUser.OfficeCode.ToString)

                'End If

                objICInstructionColl.Query.Load()
            End If
            If objICInstructionColl.Count > 0 Then
                For Each objICInstruction As ICInstruction In objICInstructionColl
                    objICInstructionCollFinal.Add(objICInstruction)
                    'If objICInstruction.PaymentMode <> "COTC" Then
                    '    If Me.UserInfo.IsSuperUser = False Then
                    '        If objICInstruction.ClearingOfficeCode = objICUser.OfficeCode.ToString Then
                    '            objICInstructionCollFinal.Add(objICInstruction)
                    '        End If

                    '        'objICInstructionColl.Query.Where(objICInstructionColl.Query.ClearingOfficeCode = objICUser.OfficeCode.ToString)

                    '    End If
                    'Else
                    '    objICInstructionCollFinal.Add(objICInstruction)
                    'End If
                Next
            End If
            Return objICInstructionCollFinal
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function GetInstructionCollWithRequiredStatusForApproval(ByVal ArrayListRequiredStatus As ArrayList) As ICInstructionCollection
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim objICInstructionColl As New ICInstructionCollection
            Dim ArraListInstID As New ArrayList
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvStatusChangeApprovalQueue.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelectApproval"), CheckBox)
                If chkSelect.Checked = True Then
                    ArraListInstID.Add(rowGVICClientRoles.GetDataKeyValue("InstructionID").ToString)
                End If
            Next
            If ArraListInstID.Count > 0 Then
                objICInstructionColl.Query.Where(objICInstructionColl.Query.InstructionID.In(ArraListInstID) And objICInstructionColl.Query.Status.In(ArrayListRequiredStatus))
                objICInstructionColl.Query.Load()
            End If

            Return objICInstructionColl
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Private Function GetInstructionActivityCollForLastStatus(ByVal InstructionID As String, ByVal Status As String) As String
        Try
            Dim collICInstructionActivity As New ICInstructionActivityCollection
            collICInstructionActivity.Query.Where(collICInstructionActivity.Query.InstructionID = InstructionID)
            collICInstructionActivity.Query.OrderBy(collICInstructionActivity.Query.InstructionActivityID.Descending)
            collICInstructionActivity.Query.Load()

            For Each objICInstActivity As ICInstructionActivity In collICInstructionActivity
                If objICInstActivity.FromStatus <> Status Then
                    Return objICInstActivity.FromStatus
                End If
            Next
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    'Private Function GetCrediAccountTypeAsString() As String
    '    Try
    '        Dim AccountType As String = Nothing
    '        If txtCreditAccountNo.Text <> "" And txtCreditAccountBrCode.Text <> "" And txtCreditAccountCurrency.Text <> "" And txtCreditAccountClientNo.Text <> "" And txtCreditAccountProfitCentre.Text <> "" And txtCreditAccountSequenceNo.Text <> "" Then
    '            AccountType = "GL"
    '        ElseIf txtCreditAccountNo.Text <> "" And txtCreditAccountBrCode.Text <> "" And txtCreditAccountCurrency.Text <> "" And txtCreditAccountClientNo.Text = "" And txtCreditAccountProfitCentre.Text = "" And txtCreditAccountSequenceNo.Text = "" Then
    '            AccountType = "RB"
    '        End If
    '        Return AccountType
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Function
    Private Function GetCrediAccountTypeAsString() As String
        Try
            Dim AccountType As String = Nothing
            If txtCreditAccountNo.Text <> "" And txtCreditAccountBrCode.Text <> "" And txtCreditAccountCurrency.Text <> "" Then
                AccountType = "GL"
            ElseIf txtCreditAccountNo.Text <> "" And txtCreditAccountBrCode.Text <> "" And txtCreditAccountCurrency.Text <> "" Then
                AccountType = "RB"
            End If
            Return AccountType
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function CheckgvClientRoleCheckedForProcessAll() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvStatusChangeQueue.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnReverse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReverse.Click
        If Page.IsValid Then
            Try
                Dim ArryListRequiredStatus As New ArrayList
                Dim ArrayListInstructionID As New ArrayList
                Dim objICInstructionColl As New ICInstructionCollection
                Dim StrAction As String = Nothing
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim dtInstructionInfo As New DataTable
                Dim ArrayListStatus As New ArrayList
                Dim Remarks As String = Nothing

                radCreditAccountNumber.Enabled = False
                ClaerCreditAccountNoTextBoxes()
                'If gvStatusChangeQueue.Items.Count > 0 Then
                '    btnSetteled.Visible = CBool(htRights("Settle"))
                'Else
                '    btnSetteled.Visible = False
                'End If
                ArryListRequiredStatus.Add(30)
                ArryListRequiredStatus.Add(49)
                'ArryListRequiredStatus.Add(15)

                ''ok
                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    ClearAllTextBoxes()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                ''ok
                Dim ResultStr As String = ""
                ResultStr = CheckInstructionRequiredStatusSecond(ArryListRequiredStatus)
                If ResultStr <> "OK" Then
                    ClearAllTextBoxes()
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Following selected Instruction(s) are invalid :<br />" & ResultStr, ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If


                objICInstructionColl = GetInstructionCollWithRequiredStatusForReversal(ArryListRequiredStatus)
                If objICInstructionColl.Count = 0 Then
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Invalid selected Instruction(s) status.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If txtRemarks.Text <> "" And Not txtRemarks.Text Is Nothing Then
                    Remarks = txtRemarks.Text.ToString
                Else
                    Remarks = ""
                End If
                If objICInstructionColl.Count > 0 Then
                    For Each objICInstruction As ICInstruction In objICInstructionColl
                        ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
                        objICInstruction.StatusReversedBy = Me.UserId
                        objICInstruction.StatusReversedDate = Date.Now
                        objICInstruction.StatusChangeRemarks = Remarks
                        StrAction = Nothing
                        StrAction = "Instruction with ID [ " & objICInstruction.InstructionID & " ] mark as reversed and pending for reverse approval from status change queue."
                        StrAction += " Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                        Try
                            ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "45", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                            PassToCancelCount = PassToCancelCount + 1
                        Catch ex As Exception
                            StrAction = Nothing
                            StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as reversed from status change queue due to " & ex.Message.ToString
                            StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                            ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For
                        End Try
                    Next
                Else
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "No instruction reversed.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If


                ArrayListStatus.Add(45)
                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArrayListInstructionID, ArrayListStatus, False)
                If dtInstructionInfo.Rows.Count > 0 Then
                    EmailUtilities.TxnMarkAsReversalApprovalFromStatusChnagequeue(dtInstructionInfo, ArrayListInstructionID, ArrayListStatus, Me.UserInfo.Username.ToString)
                    SMSUtilities.TxnavailableForReversalApprovalFromStatusChangequeue(dtInstructionInfo, ArrayListStatus, ArrayListInstructionID, "APNatureAndLocation")
                End If


                If Not PassToCancelCount = 0 And Not FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) sent for reversal approval successfully.<br /> [ " & FailToCancelCount.ToString & " ] Instruction(s) can not be sent to reversal approval successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToCancelCount = 0 And FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) sent for reversal approval successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToCancelCount = 0 And PassToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & FailToCancelCount.ToString & " ] Instruction(s) can not be sent to reversal approval successfully.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Protected Sub btnCancelInstructions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelInstructions.Click
        If Page.IsValid Then
            Try
                Dim ArryListRequiredStatus As New ArrayList
                Dim ArrayListInstructionID As New ArrayList
                Dim objICInstructionColl As New ICInstructionCollection
                Dim StrAction As String = Nothing
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim dtInstructionInfo As New DataTable
                Dim ArrayListStatus As New ArrayList
                Dim ArrylistInstID As New ArrayList
                Dim Remarks As String = Nothing
                Dim dtVerifiedInstruction As New DataTable
                radCreditAccountNumber.Enabled = False
                ClaerCreditAccountNoTextBoxes()
                'If gvStatusChangeQueue.Items.Count > 0 Then
                '    btnSetteled.Visible = CBool(htRights("Settle"))
                'Else
                '    btnSetteled.Visible = False
                'End If
                ArryListRequiredStatus.Add(16)
                ArryListRequiredStatus.Add(18)
                ArryListRequiredStatus.Add(12)
                ArryListRequiredStatus.Add(21)
                ArryListRequiredStatus.Add(23)
                ArryListRequiredStatus.Add(29)
                ArryListRequiredStatus.Add(31)
                ArryListRequiredStatus.Add(48)

                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    ClearAllTextBoxes()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Dim ResultStr As String = ""
                ResultStr = CheckInstructionRequiredStatusSecond(ArryListRequiredStatus)
                If ResultStr <> "OK" Then
                    ClearAllTextBoxes()
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Following selected Instruction(s) are invalid :<br />" & ResultStr, ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                objICInstructionColl = GetInstructionCollWithRequiredStatus(ArryListRequiredStatus)
                If objICInstructionColl.Count = 0 Then
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Invalid selected Instruction(s) status.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If txtRemarks.Text <> "" And Not txtRemarks.Text Is Nothing Then
                    Remarks = txtRemarks.Text.ToString
                Else
                    Remarks = ""
                End If
                For Each objICInstruction As ICInstruction In objICInstructionColl
                    ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
                    objICInstruction.StatusCancelledBy = Me.UserId
                    objICInstruction.StatusCancelledDate = Date.Now
                    objICInstruction.StatusChangeRemarks = Remarks
                    StrAction = Nothing
                    StrAction = "Instruction with ID [ " & objICInstruction.InstructionID & " ] mark as cancelled and pending for cancellation approval from status change queue."
                    StrAction += " Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                    Try
                        ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                        ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "46", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                        PassToCancelCount = PassToCancelCount + 1
                    Catch ex As Exception
                        StrAction = Nothing
                        StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as cancel from status change queue due to " & ex.Message.ToString
                        StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                        ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                        FailToCancelCount = FailToCancelCount + 1
                        Continue For
                    End Try
                Next
                ArrayListStatus.Add(46)
                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArrayListInstructionID, ArrayListStatus, False)
                If dtInstructionInfo.Rows.Count > 0 Then
                    EmailUtilities.TxnMarkAsCancelledFromStatusChnagequeue(dtInstructionInfo, ArrayListInstructionID, ArrayListStatus, Me.UserInfo.Username.ToString)
                    SMSUtilities.TxnavailableForCancellationFromStatusChnageQueue(dtInstructionInfo, ArrayListStatus, ArrayListInstructionID, "APNatureAndLocation")
                End If
                If Not PassToCancelCount = 0 And Not FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) sent for cancellation approval successfully.<br /> [ " & FailToCancelCount.ToString & " ] Instruction(s) can not be sent to cancellation approval successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToCancelCount = 0 And FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) sent for cancellation approval successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToCancelCount = 0 And PassToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & FailToCancelCount.ToString & " ] Instruction(s) can not be sent to cancellation approval successfully.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Protected Sub btnSetteled_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSetteled.Click
        If Page.IsValid Then
            Try
                Dim ArryListRequiredStatsu As New ArrayList
                Dim StrAction As String = Nothing
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim dtInstructionInfo As New DataTable
                Dim ArrayListStatus As New ArrayList

                ArryListRequiredStatsu.Add(16)
                ArryListRequiredStatsu.Add(18)
                ArryListRequiredStatsu.Add(21)
                ArryListRequiredStatsu.Add(12)
                ArryListRequiredStatsu.Add(23)
                ArryListRequiredStatsu.Add(29)
                ArryListRequiredStatsu.Add(31)
                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    ClearAllTextBoxes()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Dim ResultStr As String = ""
                ResultStr = CheckInstructionRequiredStatusSecond(ArryListRequiredStatsu)
                If ResultStr <> "OK" Then
                    ClearAllTextBoxes()
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Following selected Instruction(s) are invalid :<br />" & ResultStr, ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                tblCBAccountType.Style.Remove("display")
                ddlCBAccountType.ClearSelection()
                btnCancelCB.Visible = True
                tblCreditAccountNumber.Style.Add("display", "none")
                btnSetteled.Visible = False
                btnReverse.Visible = False
                btnCancelInstructions.Visible = False

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Protected Sub ddlAccount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAccount.SelectedIndexChanged
        Try
            Try
                If ddlAccount.SelectedValue.ToString <> "0" Then
                    txtAvailableBalance.Text = ""

                    txtAvailableBalance.Text = CDbl(CBUtilities.BalanceInquiry(ddlAccount.SelectedValue.Split("-")(0).ToString, ICBO.CBUtilities.AccountType.RB, "Client Account Balance", ddlAccount.SelectedValue.Split("-")(0).ToString, ddlAccount.SelectedValue.Split("-")(1).ToString)).ToString("N2")


                Else
                    txtAvailableBalance.Text = ""
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccountNumberAndClearTextBoxes()
        LoadddlAccountNumbers()
        txtTnxCount.Text = ""
        txtTotalAmount.Text = ""
        txtAvailableBalance.Text = ""
    End Sub
    Protected Sub raddpCreationFromDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationFromDate.SelectedDateChanged
        Try

            'If raddpCreationFromDate.SelectedDate Is Nothing Then
            '    UIUtilities.ShowDialog(Me, "Error", "Please select creation from date.", ICBO.IC.Dialogmessagetype.Failure)
            '    Exit Sub
            'End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadddlAccountNumberAndClearTextBoxes()
            'LoadgvStatusChangeQueue(True)
            'LoadgvStatusChangeApprovalQueue(True)
            ClaerCreditAccountNoTextBoxes()
            ddlCBAccountType.ClearSelection()
            tblCreditAccountNumber.Style.Add("display", "none")
            tblCBAccountType.Style.Add("display", "none")
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub raddpCreationToDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationToDate.SelectedDateChanged
        Try

            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadddlAccountNumberAndClearTextBoxes()
            ClaerCreditAccountNoTextBoxes()
            ddlCBAccountType.ClearSelection()
            tblCreditAccountNumber.Style.Add("display", "none")
            'LoadgvStatusChangeQueue(True)
            'LoadgvStatusChangeApprovalQueue(True)
            tblCBAccountType.Style.Add("display", "none")
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub rbtnlstDateSelection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnlstDateSelection.SelectedIndexChanged
        Try
            If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now.AddYears(2)
            Else
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
            End If

            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadddlAccountNumberAndClearTextBoxes()
            ClaerCreditAccountNoTextBoxes()
            ddlCBAccountType.ClearSelection()
            tblCreditAccountNumber.Style.Add("display", "none")
            'LoadgvStatusChangeQueue(True)
            'LoadgvStatusChangeApprovalQueue(True)
            tblCBAccountType.Style.Add("display", "none")

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            RefreshPage()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnProceed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        If Page.IsValid Then
            Try
                Dim ArryListRequiredStatsu As New ArrayList
                Dim dtVerifiedInstruction As New DataTable
                Dim ArrayListInstructionID As New ArrayList
                Dim objICInstructionColl As New ICInstructionCollection
                Dim StrAction As String = Nothing
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim dtInstructionInfo As New DataTable
                Dim ArrayListStatus As New ArrayList
                Dim Remarks As String = Nothing
                Dim CreditAccountType As String = Nothing
                ArryListRequiredStatsu.Add(16)
                ArryListRequiredStatsu.Add(18)
                ArryListRequiredStatsu.Add(21)
                ArryListRequiredStatsu.Add(12)
                ArryListRequiredStatsu.Add(23)
                ArryListRequiredStatsu.Add(29)
                ArryListRequiredStatsu.Add(31)
                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    ClearAllTextBoxes()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Dim ResultStr As String = ""
                ResultStr = CheckInstructionRequiredStatusSecond(ArryListRequiredStatsu)
                If ResultStr <> "OK" Then
                    ClearAllTextBoxes()
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Following selected Instruction(s) are invalid :<br />" & ResultStr, ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                objICInstructionColl = GetInstructionCollWithRequiredStatus(ArryListRequiredStatsu)
                'If objICInstructionColl.Count = 0 Then
                '    UIUtilities.ShowDialog(Me, "Status Change Queue", "Invalid selected Instruction(s) status.", ICBO.IC.Dialogmessagetype.Failure)
                '    Exit Sub
                'End If
                If txtRemarks.Text <> "" And Not txtRemarks.Text Is Nothing Then
                    Remarks = txtRemarks.Text.ToString
                Else
                    Remarks = ""
                End If
                If ddlCBAccountType.SelectedValue.ToString = "GL" Then
                    For Each objICInstruction As ICInstruction In objICInstructionColl
                        ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
                        objICInstruction.ClearingAccountNo = txtCreditAccountNo.Text
                        objICInstruction.ClearingAccountBranchCode = txtCreditAccountBrCode.Text
                        objICInstruction.ClearingAccountCurrency = txtCreditAccountCurrency.Text
                        'objICInstruction.ClearingProfitCentre = txtCreditAccountProfitCentre.Text
                        'objICInstruction.ClearingClientNo = txtCreditAccountClientNo.Text
                        'objICInstruction.ClearingSeqNo = txtCreditAccountSequenceNo.Text
                        objICInstruction.ClearingProfitCentre = Nothing
                        objICInstruction.ClearingClientNo = Nothing
                        objICInstruction.ClearingSeqNo = Nothing
                        objICInstruction.ClearingAccountType = ddlCBAccountType.SelectedValue.ToString
                        objICInstruction.StatusSettledBy = Me.UserId
                        objICInstruction.StatusSettledDate = Date.Now
                        objICInstruction.StatusChangeRemarks = Remarks
                        StrAction = Nothing
                        StrAction = "Instruction with ID [ " & objICInstruction.InstructionID & " ] mark as settled and pending for settlment approval from status change queue. Clearing Account Updated with Account No [ " & txtCreditAccountNo.Text & " ] Branch Code [ " & txtCreditAccountBrCode.Text & " ]"
                        'StrAction += " Currency [ " & txtCreditAccountCurrency.Text & " ] Profit Centre [ " & txtCreditAccountProfitCentre.Text & " ] Sequence No [ " & txtCreditAccountSequenceNo.Text & " ] Client No [ " & txtCreditAccountClientNo.Text & " ]."
                        StrAction += " Currency [ " & txtCreditAccountCurrency.Text & " ]."
                        StrAction += " Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                        Try
                            ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "44", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                            PassToCancelCount = PassToCancelCount + 1
                        Catch ex As Exception
                            StrAction = Nothing
                            StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as settled from status change queue due to " & ex.Message.ToString
                            StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                            ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For
                        End Try
                    Next
                ElseIf ddlCBAccountType.SelectedValue.ToString = "RB" Then
                    For Each objICInstruction As ICInstruction In objICInstructionColl
                        ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
                        objICInstruction.ClearingAccountNo = txtCreditAccountNo.Text
                        objICInstruction.ClearingAccountBranchCode = txtCreditAccountBrCode.Text
                        objICInstruction.ClearingAccountCurrency = txtCreditAccountCurrency.Text
                        objICInstruction.ClearingAccountType = ddlCBAccountType.SelectedValue.ToString
                        objICInstruction.StatusSettledBy = Me.UserId
                        objICInstruction.StatusSettledDate = Date.Now
                        objICInstruction.StatusChangeRemarks = Remarks
                        StrAction = Nothing
                        StrAction = "Instruction with ID [ " & objICInstruction.InstructionID & " ] mark as settled and pending for settlment approval from status change queue. Clearing Account Updated with Account No [ " & txtCreditAccountNo.Text & " ] Branch Code [ " & txtCreditAccountBrCode.Text & " ]"
                        StrAction += " Currency [ " & txtCreditAccountCurrency.Text & " ]"
                        StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                        Try
                            ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "44", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                            PassToCancelCount = PassToCancelCount + 1
                        Catch ex As Exception
                            StrAction = Nothing
                            StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and  fail to mark as settled from status change queue due to " & ex.Message.ToString
                            StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                            ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For
                        End Try
                    Next
                End If
                ArrayListStatus.Add(44)
                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArrayListInstructionID, ArrayListStatus, False)
                If dtInstructionInfo.Rows.Count > 0 Then
                    EmailUtilities.TxnMarkAsSettlementApprovalFromStatusChnagequeue(dtInstructionInfo, ArrayListInstructionID, ArrayListStatus, Me.UserInfo.Username.ToString)
                    SMSUtilities.TxnaAvailableForSettlementApprovalFromStatusChnageQueue(dtInstructionInfo, ArrayListStatus, ArrayListInstructionID, "APNatureAndLocation")
                End If
                If Not PassToCancelCount = 0 And Not FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) sent for settlement approval successfully.<br /> [ " & FailToCancelCount.ToString & " ] Instruction(s) can not be settlement approval successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToCancelCount = 0 And FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) sent for settlement approval successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToCancelCount = 0 And PassToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & FailToCancelCount.ToString & " ] Instruction(s) can not be settlement approval successfully.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Sub ClaerCreditAccountNoTextBoxes()
        txtCreditAccountNo.Text = ""
        txtCreditAccountBrCode.Text = ""
        txtCreditAccountCurrency.Text = ""
        'txtCreditAccountClientNo.Text = ""
        'txtCreditAccountProfitCentre.Text = ""
        'txtCreditAccountSequenceNo.Text = ""
    End Sub

    Protected Sub btnApproveCancellation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveCancellation.Click
        If Page.IsValid Then
            Try
                Dim ArryListRequiredStatus As New ArrayList
                Dim ArrayListInstructionID As New ArrayList
                Dim objICInstructionColl As New ICInstructionCollection
                Dim StrAction As String = Nothing
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim dtInstructionInfo As New DataTable
                Dim dtInstructionInfoPO As New DataTable
                Dim ArrayListStatus As New ArrayList
                Dim Remarks As String = Nothing
                radCreditAccountNumber.Enabled = False
                ClaerCreditAccountNoTextBoxes()
                ArryListRequiredStatus.Add(46)
                If CheckgvInstructionApprovalStatusChangeQueue() = False Then
                    ClearAllTextBoxes()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Dim ResultStr As String = ""
                ResultStr = CheckInstructionRequiredStatusSecondForReversal(ArryListRequiredStatus)
                If ResultStr <> "OK" Then
                    ClearAllTextBoxes()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Following selected Instruction(s) are invalid :<br />" & ResultStr, ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                objICInstructionColl = GetInstructionCollWithRequiredStatusForApproval(ArryListRequiredStatus)

                'If objICInstructionColl.Count = 0 Then
                '    UIUtilities.ShowDialog(Me, "Status Change Queue", "Invalid selected Instruction(s) status.", ICBO.IC.Dialogmessagetype.Failure)
                '    Exit Sub
                'End If
                If txtApprovalRemarks.Text <> "" And Not txtApprovalRemarks.Text Is Nothing Then
                    Remarks = txtRemarks.Text.ToString
                Else
                    Remarks = ""
                End If
                For Each objICInstruction As ICInstruction In objICInstructionColl
                    ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
                    If objICInstruction.Status.ToString = "46" Then
                        If objICInstruction.StatusCancelledBy.ToString <> Me.UserId.ToString Then
                            If objICInstruction.LastStatus.ToString <> "30" Then

                                If PerformCancelApprovalFormStatusChnageQueue(objICInstruction.InstructionID.ToString, "5", objICInstruction.Status, Me.UserInfo.UserID.ToString) = True Then
                                    objICInstruction.StatusCancelledApprovedBy = Me.UserId
                                    objICInstruction.StatusCancelledApprovedDate = Date.Now
                                    objICInstruction.StatusApproveRemarks = Remarks
                                    StrAction = Nothing
                                    StrAction = "Instruction with ID [ " & objICInstruction.InstructionID & " ] mark as cancelled from status change queue."

                                    If objICInstruction.PaymentMode = "COTC" And objICInstruction.UpToICProductTypeByProductTypeCode.IsSundaryAllow = True Then
                                        StrAction += " Funds transfered from " & objICInstruction.PayableAccountNumber & ", branch code " & objICInstruction.PayableAccountBranchCode
                                        StrAction += " to account " & objICInstruction.ClientAccountNo & ", branch code " & objICInstruction.ClientAccountBranchCode & "."
                                    ElseIf objICInstruction.PaymentMode = "PO" Or objICInstruction.PaymentMode = "DD" Then
                                        StrAction += " Funds transfered from " & objICInstruction.PayableAccountNumber & ", branch code " & objICInstruction.PayableAccountBranchCode
                                        StrAction += " to account " & objICInstruction.ClientAccountNo & ", branch code " & objICInstruction.ClientAccountBranchCode & "."
                                    End If
                                    StrAction += " Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                    Try

                                        objICInstruction.PayableAccountBranchCode = Nothing
                                        objICInstruction.PayableAccountCurrency = Nothing
                                        objICInstruction.PayableAccountNumber = Nothing
                                        objICInstruction.PayableAccountType = Nothing

                                        ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                                        ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "5", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                                        PassToCancelCount = PassToCancelCount + 1
                                    Catch ex As Exception
                                        StrAction = Nothing
                                        StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as cancelled from status change queue due to " & ex.Message.ToString
                                        StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                        ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                        FailToCancelCount = FailToCancelCount + 1
                                        Continue For
                                    End Try
                                Else
                                    StrAction = Nothing
                                    StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as cancelled from status change queue on approval."
                                    StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                    ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                    FailToCancelCount = FailToCancelCount + 1
                                    Continue For

                                End If
                            ElseIf objICInstruction.LastStatus.ToString = "30" Then
                                Try
                                    If PerformCancelApprovalFormStatusChnageQueue(objICInstruction.InstructionID.ToString, "5", objICInstruction.Status, Me.UserInfo.UserID.ToString) = True Then

                                        objICInstruction.StatusCancelledApprovedBy = Me.UserId
                                        objICInstruction.StatusCancelledApprovedDate = Date.Now
                                        objICInstruction.StatusApproveRemarks = Remarks
                                        StrAction = Nothing
                                        StrAction = "Instruction with ID [ " & objICInstruction.InstructionID & " ] mark as cancelled and approved from status change queue."
                                        StrAction += " Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                        ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                                        ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "5", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                                        PassToCancelCount = PassToCancelCount + 1
                                        Continue For
                                    Else
                                        StrAction = Nothing
                                        StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as cancelled from status change queue on approval."
                                        StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                        ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                        FailToCancelCount = FailToCancelCount + 1
                                        Continue For

                                    End If
                                Catch ex As Exception
                                    StrAction = Nothing
                                    StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as cancelled on approval from status change queue due to " & ex.Message.ToString
                                    StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                    ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                    FailToCancelCount = FailToCancelCount + 1
                                    Continue For
                                End Try

                            End If
                        Else
                            ''audit trail entry
                            StrAction = Nothing
                            StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as cancelled on approval from status change queue due to instruction must be approved by other than maker."
                            StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                            ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For

                        End If
                    Else
                        ''audit trail entry
                        StrAction = Nothing
                        StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as cancelled on approval from status change queue due invalid current status."
                        StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                        ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                        FailToCancelCount = FailToCancelCount + 1
                        Continue For

                    End If
                Next


                ArrayListStatus.Add(5)
                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArrayListInstructionID, ArrayListStatus, False)
                dtInstructionInfoPO = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArrayListInstructionID, ArrayListStatus, True)
                If dtInstructionInfo.Rows.Count > 0 Or dtInstructionInfoPO.Rows.Count > 0 Then
                    EmailUtilities.TxnMarkAsCancelledApprovedFromStatusChangequeue(dtInstructionInfo, dtInstructionInfoPO, ArrayListInstructionID, ArrayListStatus, Me.UserInfo.Username.ToString)
                    SMSUtilities.TxnaCancellationApprovedFromStatusChnageQueue(dtInstructionInfo, dtInstructionInfoPO, ArrayListStatus, ArrayListInstructionID, "APNatureAndLocation")
                End If


                If Not PassToCancelCount = 0 And Not FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) approved successfully.<br /> [ " & FailToCancelCount.ToString & " ] Instruction(s) can not approved successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToCancelCount = 0 And FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) approved successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToCancelCount = 0 And PassToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & FailToCancelCount.ToString & " ] Instruction(s) can not approved successfully.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Public Shared Function PerformCancelApprovalFormStatusChnageQueue(ByVal InstructionID As String, ByVal FinalStatus As String, ByVal FailureStatus As String, ByVal UsersID As String) As Boolean
        Try
            Dim Result As Boolean = False
            Dim CurrentStatus, LastStatus As String
            CurrentStatus = Nothing
            LastStatus = Nothing
            Dim objICInstruction As ICInstruction
            Dim objICoffice As New ICOffice
            objICInstruction = New ICInstruction
            objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
            CurrentStatus = objICInstruction.Status.ToString
            LastStatus = objICInstruction.LastStatus.ToString
            If objICInstruction.PaymentMode.ToLower = "po" Then
                If objICInstruction.Status = "30" Then
                    If objICInstruction.ClearingAccountType.ToString.ToLower.Trim = "rb" Then
                        If CBUtilities.Reversal(objICInstruction.ClearingAccountNo.ToString, objICInstruction.ClearingAccountBranchCode.ToString, objICInstruction.ClearingAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.ClientAccountNo.ToString, objICInstruction.ClientAccountBranchCode.ToString, objICInstruction.ClientAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailureStatus, UsersID) = True Then
                            Result = True
                            Return Result
                            Exit Function
                        Else
                            objICInstruction = New ICInstruction
                            objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                            objICInstruction.Status = CurrentStatus
                            objICInstruction.LastStatus = LastStatus
                            objICInstruction.Save()
                            Result = False
                            Return Result
                            Exit Function
                        End If
                    ElseIf objICInstruction.ClearingAccountType.ToString.ToLower.Trim = "gl" Then
                        If CBUtilities.Reversal(objICInstruction.ClearingAccountNo.ToString, objICInstruction.ClearingAccountBranchCode.ToString, objICInstruction.ClearingAccountCurrency.ToString, objICInstruction.ClearingClientNo.ToString, objICInstruction.ClearingSeqNo.ToString, objICInstruction.ClearingProfitCentre.ToString, ICBO.CBUtilities.AccountType.GL, objICInstruction.ClientAccountNo.ToString, objICInstruction.ClientAccountBranchCode.ToString, objICInstruction.ClientAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailureStatus, UsersID) = True Then
                            Result = True
                            Return Result
                            Exit Function
                        Else
                            objICInstruction = New ICInstruction
                            objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                            objICInstruction.Status = CurrentStatus
                            objICInstruction.LastStatus = LastStatus
                            objICInstruction.Save()
                            Result = False
                            Return Result
                            Exit Function
                        End If
                    End If
                Else
                    If objICInstruction.PayableAccountType.ToString.ToLower.Trim = "rb" Then
                        If CBUtilities.Reversal(objICInstruction.PayableAccountNumber.ToString, objICInstruction.PayableAccountBranchCode.ToString, objICInstruction.PayableAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.ClientAccountNo.ToString, objICInstruction.ClientAccountBranchCode.ToString, objICInstruction.ClientAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailureStatus, UsersID) = True Then
                            Result = True
                            Return Result
                            Exit Function
                        Else
                            objICInstruction = New ICInstruction
                            objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                            objICInstruction.Status = CurrentStatus
                            objICInstruction.LastStatus = LastStatus
                            objICInstruction.Save()
                            Result = False
                            Return Result
                            Exit Function
                        End If
                    ElseIf objICInstruction.PayableAccountType.ToString.ToLower.Trim = "gl" Then
                        If CBUtilities.Reversal(objICInstruction.PayableAccountNumber.ToString, objICInstruction.PayableAccountBranchCode.ToString, objICInstruction.PayableAccountCurrency.ToString, objICInstruction.PayableClientNo.ToString, objICInstruction.PayableSeqNo.ToString, objICInstruction.PayableProfitCentre.ToString, ICBO.CBUtilities.AccountType.GL, objICInstruction.ClientAccountNo.ToString, objICInstruction.ClientAccountBranchCode.ToString, objICInstruction.ClientAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailureStatus, UsersID) = True Then
                            Result = True
                            Return Result
                            Exit Function
                        Else
                            objICInstruction = New ICInstruction
                            objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                            objICInstruction.Status = CurrentStatus
                            objICInstruction.LastStatus = LastStatus
                            objICInstruction.Save()
                            Result = False
                            Return Result
                            Exit Function
                        End If
                    End If

                End If
            ElseIf objICInstruction.PaymentMode.ToLower = "dd" Then

                If objICInstruction.PayableAccountType.ToString.ToLower.Trim = "rb" Then
                    If CBUtilities.Reversal(objICInstruction.PayableAccountNumber.ToString, objICInstruction.PayableAccountBranchCode.ToString, objICInstruction.PayableAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.ClientAccountNo.ToString, objICInstruction.ClientAccountBranchCode.ToString, objICInstruction.ClientAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailureStatus, UsersID) = True Then
                        Result = True
                        Return Result
                        Exit Function
                    Else
                        objICInstruction = New ICInstruction
                        objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                        objICInstruction.Status = CurrentStatus
                        objICInstruction.LastStatus = LastStatus
                        objICInstruction.Save()
                        Result = False
                        Return Result
                        Exit Function
                    End If
                ElseIf objICInstruction.PayableAccountType.ToString.ToLower.Trim = "gl" Then
                    If CBUtilities.Reversal(objICInstruction.PayableAccountNumber.ToString, objICInstruction.PayableAccountBranchCode.ToString, objICInstruction.PayableAccountCurrency.ToString, objICInstruction.PayableClientNo.ToString, objICInstruction.PayableSeqNo.ToString, objICInstruction.PayableProfitCentre.ToString, ICBO.CBUtilities.AccountType.GL, objICInstruction.ClientAccountNo.ToString, objICInstruction.ClientAccountBranchCode.ToString, objICInstruction.ClientAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailureStatus, UsersID) = True Then
                        Result = True
                        Return Result
                        Exit Function
                    Else
                        objICInstruction = New ICInstruction
                        objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                        objICInstruction.Status = CurrentStatus
                        objICInstruction.LastStatus = LastStatus
                        objICInstruction.Save()
                        Result = False
                        Return Result
                        Exit Function
                    End If
                End If
            Else
                If objICInstruction.PaymentMode = "COTC" Then
                    If objICInstruction.UpToICProductTypeByProductTypeCode.IsSundaryAllow = True Then
                        If objICInstruction.PayableAccountType = "gl" Then
                            If CBUtilities.Reversal(objICInstruction.PayableAccountNumber, objICInstruction.PayableAccountBranchCode.ToString, objICInstruction.PayableAccountCurrency.ToString, objICInstruction.PayableClientNo, objICInstruction.PayableSeqNo, objICInstruction.PayableProfitCentre, ICBO.CBUtilities.AccountType.GL, objICInstruction.ClientAccountNo.ToString, objICInstruction.ClientAccountBranchCode.ToString, objICInstruction.ClientAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailureStatus, UsersID) = True Then
                                Result = True
                                Return Result
                                Exit Function
                            Else
                                objICInstruction = New ICInstruction
                                objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                                objICInstruction.Status = CurrentStatus
                                objICInstruction.LastStatus = LastStatus
                                objICInstruction.Save()
                                Result = False
                                Return Result
                                Exit Function
                            End If

                        Else
                            If CBUtilities.Reversal(objICInstruction.PayableAccountNumber.ToString, objICInstruction.PayableAccountBranchCode.ToString, objICInstruction.PayableAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.ClientAccountNo.ToString, objICInstruction.ClientAccountBranchCode.ToString, objICInstruction.ClientAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailureStatus, UsersID) = True Then
                                Result = True
                                Return Result
                                Exit Function
                            Else
                                objICInstruction = New ICInstruction
                                objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                                objICInstruction.Status = CurrentStatus
                                objICInstruction.LastStatus = LastStatus
                                objICInstruction.Save()
                                Result = False
                                Return Result
                                Exit Function
                            End If
                        End If
                    Else
                        Result = True
                        Return Result
                        Exit Function
                    End If
                Else
                    Result = True
                    Return Result
                    Exit Function
                End If
            End If
            Return Result
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Function PerformReversalApprovalFormStatusChnageQueue(ByVal InstructionID As String, ByVal FinalStatus As String, ByVal FailStatus As String, ByVal UsersID As String) As Boolean
        Try
            Dim Result As Boolean = False
            Dim CurrentStatus, LastStatus As String
            Dim StanNo As String = Nothing
            CurrentStatus = Nothing
            LastStatus = Nothing
            Dim objICInstruction As ICInstruction
            objICInstruction = New ICInstruction
            Dim objICOffice As New ICOffice
            objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
            CurrentStatus = objICInstruction.Status.ToString
            LastStatus = objICInstruction.LastStatus.ToString
            If objICInstruction.LastStatus.ToString = "30" Then
                If objICInstruction.ClearingAccountType.ToString.ToLower.Trim = "rb" Then
                    If CBUtilities.Reversal(objICInstruction.ClearingAccountNo.ToString, objICInstruction.ClearingAccountBranchCode.ToString, objICInstruction.ClearingAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.ClientAccountNo.ToString, objICInstruction.ClientAccountBranchCode.ToString, objICInstruction.ClientAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailStatus, UsersID) = True Then
                        Result = True
                        Return Result
                        Exit Function
                    Else
                        objICInstruction = New ICInstruction
                        objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                        objICInstruction.Status = CurrentStatus
                        objICInstruction.LastStatus = LastStatus
                        objICInstruction.Save()
                        Result = False
                        Return Result
                        Exit Function
                    End If
                ElseIf objICInstruction.ClearingAccountType.ToString.ToLower.Trim = "gl" Then
                    If CBUtilities.FundsTransfer(StanNo, objICInstruction.ClearingAccountNo.ToString, objICInstruction.ClearingAccountBranchCode.ToString, objICInstruction.ClearingAccountCurrency.ToString, objICInstruction.ClearingClientNo.ToString, objICInstruction.ClearingSeqNo.ToString, objICInstruction.ClearingProfitCentre.ToString, ICBO.CBUtilities.AccountType.GL, objICInstruction.ClientAccountNo.ToString, objICInstruction.ClientAccountBranchCode.ToString, objICInstruction.ClientAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailStatus, UsersID) = True Then
                        Result = True
                        Return Result
                        Exit Function
                    Else
                        objICInstruction = New ICInstruction
                        objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                        objICInstruction.Status = CurrentStatus
                        objICInstruction.LastStatus = LastStatus
                        objICInstruction.Save()
                        Result = False
                        Return Result
                        Exit Function
                    End If
                End If
            ElseIf objICInstruction.LastStatus.ToString = "15" Then
                If CBUtilities.Reversal(objICInstruction.BeneficiaryAccountNo.ToString, objICInstruction.BeneAccountBranchCode.ToString, objICInstruction.BeneAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.ClientAccountNo.ToString, objICInstruction.ClientAccountBranchCode.ToString, objICInstruction.ClientAccountCurrency.ToString, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailStatus, UsersID) = True Then
                    Result = True
                    Return Result
                    Exit Function
                Else
                    objICInstruction = New ICInstruction
                    objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                    objICInstruction.Status = CurrentStatus
                    objICInstruction.LastStatus = LastStatus
                    objICInstruction.Save()
                    Result = False
                    Return Result
                    Exit Function
                End If
            ElseIf objICInstruction.LastStatus.ToString = "49" Then
                objICOffice.LoadByPrimaryKey(objICInstruction.DisbursedBranchCode)
                If objICInstruction.UpToICProductTypeByProductTypeCode.IsSundaryAllow = True Then
                    If objICInstruction.PayableAccountType = "RB" Then
                        If objICOffice.BranchCashTillAccountType = "RB" Then
                            If CBUtilities.Reversal(objICOffice.BranchCashTillAccountNumber, objICOffice.OfficeCode, objICOffice.BranchCashTillAccountCurrency, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.PayableAccountNumber, objICInstruction.PayableAccountBranchCode, objICInstruction.PayableAccountCurrency, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailStatus, UsersID) = True Then
                                Result = True
                                Return Result
                                Exit Function
                            Else
                                objICInstruction = New ICInstruction
                                objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                                objICInstruction.Status = CurrentStatus
                                objICInstruction.LastStatus = LastStatus
                                objICInstruction.Save()
                                Result = False
                                Return Result
                                Exit Function
                            End If
                        Else
                            If CBUtilities.Reversal(objICOffice.BranchCashTillAccountNumber, objICOffice.OfficeCode, objICOffice.BranchCashTillAccountCurrency, "", "", "", ICBO.CBUtilities.AccountType.GL, objICInstruction.PayableAccountNumber, objICInstruction.PayableAccountBranchCode, objICInstruction.PayableAccountCurrency, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailStatus, UsersID) = True Then
                                Result = True
                                Return Result
                                Exit Function
                            Else
                                objICInstruction = New ICInstruction
                                objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                                objICInstruction.Status = CurrentStatus
                                objICInstruction.LastStatus = LastStatus
                                objICInstruction.Save()
                                Result = False
                                Return Result
                                Exit Function
                            End If
                        End If
                    Else
                        If objICOffice.BranchCashTillAccountType = "RB" Then
                            If CBUtilities.Reversal(objICOffice.BranchCashTillAccountNumber, objICOffice.OfficeCode, objICOffice.BranchCashTillAccountCurrency, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.PayableAccountNumber, objICInstruction.PayableAccountBranchCode, objICInstruction.PayableAccountCurrency, "", "", "", ICBO.CBUtilities.AccountType.GL, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailStatus, UsersID) = True Then
                                Result = True
                                Return Result
                                Exit Function
                            Else
                                objICInstruction = New ICInstruction
                                objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                                objICInstruction.Status = CurrentStatus
                                objICInstruction.LastStatus = LastStatus
                                objICInstruction.Save()
                                Result = False
                                Return Result
                                Exit Function
                            End If
                        Else
                            If CBUtilities.Reversal(objICOffice.BranchCashTillAccountNumber, objICOffice.OfficeCode, objICOffice.BranchCashTillAccountCurrency, "", "", "", ICBO.CBUtilities.AccountType.GL, objICInstruction.PayableAccountNumber, objICInstruction.PayableAccountBranchCode, objICInstruction.PayableAccountCurrency, "", "", "", ICBO.CBUtilities.AccountType.GL, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailStatus, UsersID) = True Then
                                Result = True
                                Return Result
                                Exit Function
                            Else
                                objICInstruction = New ICInstruction
                                objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                                objICInstruction.Status = CurrentStatus
                                objICInstruction.LastStatus = LastStatus
                                objICInstruction.Save()
                                Result = False
                                Return Result
                                Exit Function
                            End If
                        End If
                    End If

                Else
                    If objICOffice.BranchCashTillAccountType = "RB" Then
                        If CBUtilities.Reversal(objICOffice.BranchCashTillAccountNumber, objICOffice.OfficeCode, objICOffice.BranchCashTillAccountCurrency, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.ClientAccountNo, objICInstruction.ClientAccountBranchCode, objICInstruction.ClientAccountCurrency, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailStatus, UsersID) = True Then
                            Result = True
                            Return Result
                            Exit Function
                        Else
                            objICInstruction = New ICInstruction
                            objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                            objICInstruction.Status = CurrentStatus
                            objICInstruction.LastStatus = LastStatus
                            objICInstruction.Save()
                            Result = False
                            Return Result
                            Exit Function
                        End If
                    Else
                        If CBUtilities.Reversal(objICOffice.BranchCashTillAccountNumber, objICOffice.OfficeCode, objICOffice.BranchCashTillAccountCurrency, "", "", "", ICBO.CBUtilities.AccountType.GL, objICInstruction.ClientAccountNo, objICInstruction.ClientAccountBranchCode, objICInstruction.ClientAccountCurrency, "", "", "", ICBO.CBUtilities.AccountType.RB, objICInstruction.Amount.ToString, "Instruction", objICInstruction.InstructionID.ToString, FinalStatus, FailStatus, UsersID) = True Then
                            Result = True
                            Return Result
                            Exit Function
                        Else
                            objICInstruction = New ICInstruction
                            objICInstruction.LoadByPrimaryKey(InstructionID.ToString)
                            objICInstruction.Status = CurrentStatus
                            objICInstruction.LastStatus = LastStatus
                            objICInstruction.Save()
                            Result = False
                            Return Result
                            Exit Function
                        End If
                    End If
                End If

            End If

            Return Result
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Protected Sub btnApproveSettlement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveSettlement.Click
        If Page.IsValid Then
            Try
                Dim ArryListRequiredStatus As New ArrayList
                Dim ArrayListInstructionID As New ArrayList
                Dim objICInstructionColl As New ICInstructionCollection
                Dim StrAction As String = Nothing
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim dtInstructionInfo As New DataTable
                Dim ArrayListStatus As New ArrayList
                Dim Remarks As String = Nothing
                radCreditAccountNumber.Enabled = False
                ClaerCreditAccountNoTextBoxes()
                ArryListRequiredStatus.Add(44)
                If CheckgvInstructionApprovalStatusChangeQueue() = False Then
                    ClearAllTextBoxes()
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Dim ResultStr As String = ""
                ResultStr = CheckInstructionRequiredStatusSecondForReversal(ArryListRequiredStatus)
                If ResultStr <> "OK" Then
                    ClearAllTextBoxes()
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Following selected Instruction(s) are invalid :<br />" & ResultStr, ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                objICInstructionColl = GetInstructionCollWithRequiredStatusForApproval(ArryListRequiredStatus)
                'If objICInstructionColl.Count = 0 Then
                '    UIUtilities.ShowDialog(Me, "Status Change Queue", "Invalid selected Instruction(s) status.", ICBO.IC.Dialogmessagetype.Failure)
                '    Exit Sub
                'End If
                If txtApprovalRemarks.Text <> "" And Not txtApprovalRemarks.Text Is Nothing Then
                    Remarks = txtRemarks.Text.ToString
                Else
                    Remarks = ""
                End If
                For Each objICInstruction As ICInstruction In objICInstructionColl
                    If objICInstruction.Status.ToString = "44" Then
                        If objICInstruction.StatusSettledBy.ToString <> Me.UserId.ToString Then
                            ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
                            objICInstruction.StatusSettledApprovedBy = Me.UserId
                            objICInstruction.StatusSettledApprovedDate = Date.Now
                            objICInstruction.StatusApproveRemarks = Remarks
                            StrAction = Nothing
                            StrAction = "Instruction with ID [ " & objICInstruction.InstructionID & " ] mark as settled and approved from status change queue."
                            StrAction += " Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                            Try
                                ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                                ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "15", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                                PassToCancelCount = PassToCancelCount + 1
                            Catch ex As Exception
                                StrAction = Nothing
                                StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as settled from status change queue due to " & ex.Message.ToString
                                StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                FailToCancelCount = FailToCancelCount + 1
                                Continue For
                            End Try
                        Else
                            ''audit trail entry
                            StrAction = Nothing
                            StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as settled on approval from status change queue due to instruction must be approved by other than maker."
                            StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                            ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For

                        End If
                    Else
                        ''audit trail entry
                        StrAction = Nothing
                        StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as cancelled on approval from status change queue due invalid current status."
                        StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                        ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "ERROR")
                        FailToCancelCount = FailToCancelCount + 1
                        Continue For

                    End If
                Next
                ArrayListStatus.Add(15)
                If Not (ArrayListInstructionID Is Nothing Or ArrayListInstructionID.Count = 0) Then

                    dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArrayListInstructionID, ArrayListStatus, False)

                    If dtInstructionInfo.Rows.Count > 0 Then
                        EmailUtilities.TxnMarkAsSettledApprovedFromStatusChangequeue(dtInstructionInfo, ArrayListInstructionID, ArrayListStatus, Me.UserInfo.Username.ToString)
                        SMSUtilities.TxnSettlementApprovedFromStatusChnageQueue(dtInstructionInfo, ArrayListStatus, ArrayListStatus, "APNatureAndLocation")
                    End If

                End If

                If Not PassToCancelCount = 0 And Not FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) approved successfully.<br /> [ " & FailToCancelCount.ToString & " ] Instruction(s) can not approved successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToCancelCount = 0 And FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) approved successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToCancelCount = 0 And PassToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & FailToCancelCount.ToString & " ] Instruction(s) can not approved successfully.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Protected Sub btnApproveReversal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveReversal.Click
        If Page.IsValid Then
            Try
                Dim ArryListRequiredStatus As New ArrayList
                Dim ArrayListInstructionID As New ArrayList
                Dim objICInstructionColl As New ICInstructionCollection
                Dim StrAction As String = Nothing
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim dtInstructionInfo As New DataTable
                Dim dtInstructionInfoPO As New DataTable
                Dim ArrayListStatus As New ArrayList
                Dim Remarks As String = Nothing
                radCreditAccountNumber.Enabled = False
                ClaerCreditAccountNoTextBoxes()
                ArryListRequiredStatus.Add(45)
                If CheckgvInstructionApprovalStatusChangeQueue() = False Then
                    ClearAllTextBoxes()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Dim ResultStr As String = ""
                ResultStr = CheckInstructionRequiredStatusSecondForReversal(ArryListRequiredStatus)
                If ResultStr <> "OK" Then
                    ClearAllTextBoxes()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Following selected Instruction(s) are invalid :<br />" & ResultStr, ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                objICInstructionColl = GetInstructionCollWithRequiredStatusForReversalApproval(ArryListRequiredStatus)

                If objICInstructionColl.Count = 0 Then
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Invalid selected Instruction(s) status.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If txtApprovalRemarks.Text <> "" And Not txtApprovalRemarks.Text Is Nothing Then
                    Remarks = txtRemarks.Text.ToString
                Else
                    Remarks = ""
                End If
                For Each objICInstruction As ICInstruction In objICInstructionColl
                    ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
                    If objICInstruction.Status.ToString = "45" Then
                        If objICInstruction.StatusReversedBy.ToString <> Me.UserId.ToString Then
                            If objICInstruction.LastStatus.ToString = "30" Or objICInstruction.LastStatus.ToString = "15" Then
                                If objICInstruction.LastStatus.ToString = "30" Then
                                    Try
                                        If PerformReversalApprovalFormStatusChnageQueue(objICInstruction.InstructionID.ToString, "21", objICInstruction.Status.ToString, Me.UserId.ToString) = True Then
                                            objICInstruction.StatusReversedBy = Me.UserId
                                            objICInstruction.StatusReversedDate = Date.Now
                                            objICInstruction.StatusApproveRemarks = Remarks
                                            StrAction = Nothing
                                            StrAction = "Instruction with ID [ " & objICInstruction.InstructionID & " ] mark as reversed and approved from status change queue."
                                            StrAction += " Funds moved from account " & objICInstruction.ClearingAccountNo & ", branch code " & objICInstruction.ClearingAccountBranchCode & "."
                                            StrAction += " Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."

                                            objICInstruction.BeneficiaryAccountNo = Nothing
                                            objICInstruction.BeneAccountBranchCode = Nothing
                                            objICInstruction.BeneAccountCurrency = Nothing
                                            objICInstruction.BeneficiaryAccountTitle = Nothing
                                            objICInstruction.ClearingAccountNo = Nothing
                                            objICInstruction.ClearingAccountBranchCode = Nothing
                                            objICInstruction.ClearingAccountCurrency = Nothing
                                            objICInstruction.ClearingAccountType = Nothing

                                            ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "21", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                                            PassToCancelCount = PassToCancelCount + 1
                                            Continue For
                                        Else
                                            StrAction = Nothing
                                            StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as reversed from status change queue on approval."
                                            StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                            ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                            FailToCancelCount = FailToCancelCount + 1
                                            Continue For
                                        End If
                                    Catch ex As Exception
                                        StrAction = Nothing
                                        StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as reversed on approval from status change queue due to " & ex.Message.ToString
                                        StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                        ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                        FailToCancelCount = FailToCancelCount + 1
                                        Continue For
                                    End Try
                                ElseIf objICInstruction.LastStatus.ToString = "15" Then
                                    Try
                                        If PerformReversalApprovalFormStatusChnageQueue(objICInstruction.InstructionID.ToString, "14", objICInstruction.Status.ToString, Me.UserId.ToString) = True Then
                                            objICInstruction.StatusReversedBy = Me.UserId
                                            objICInstruction.StatusReversedDate = Date.Now
                                            objICInstruction.StatusApproveRemarks = Remarks
                                            StrAction = Nothing
                                            StrAction = "Instruction with ID [ " & objICInstruction.InstructionID & " ] mark as reversed and approved from status change queue."
                                            StrAction += " Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                            ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")

                                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "14", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                                            PassToCancelCount = PassToCancelCount + 1

                                            Continue For
                                        Else
                                            StrAction = Nothing
                                            StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as reversed from status change queue on approval."
                                            StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                            ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                            FailToCancelCount = FailToCancelCount + 1
                                            Continue For
                                        End If
                                    Catch ex As Exception
                                        StrAction = Nothing
                                        StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as cancelled on approval from status change queue due to " & ex.Message.ToString
                                        StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                        ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                        FailToCancelCount = FailToCancelCount + 1
                                        Continue For
                                    End Try

                                End If
                            ElseIf objICInstruction.LastStatus.ToString = "49" Then
                                Try
                                    If PerformReversalApprovalFormStatusChnageQueue(objICInstruction.InstructionID.ToString, "48", objICInstruction.Status.ToString, Me.UserId.ToString) = True Then
                                        Dim objICoffice As New ICOffice
                                        objICoffice.LoadByPrimaryKey(objICInstruction.DisbursedBranchCode)
                                        objICInstruction.StatusReversedBy = Me.UserId
                                        objICInstruction.StatusReversedDate = Date.Now
                                        objICInstruction.StatusApproveRemarks = Remarks
                                        StrAction = Nothing
                                        StrAction = "Instruction with ID [ " & objICInstruction.InstructionID & " ] mark as reversed and approved from status change queue."


                                        If objICInstruction.UpToICProductTypeByProductTypeCode.IsSundaryAllow = True Then
                                            StrAction += "Funds transfered from account number " & objICoffice.BranchCashTillAccountNumber & ",  branch code " & objICoffice.OfficeCode & ""
                                            StrAction += "to account number " & objICInstruction.PayableAccountNumber & ",  branch code " & objICInstruction.PayableAccountBranchCode & "."
                                        Else
                                            StrAction += "Funds transfered from account number " & objICoffice.BranchCashTillAccountNumber & ",  branch code " & objICoffice.OfficeCode & ""
                                            StrAction += "to account number " & objICInstruction.ClientAccountNo & ",  branch code " & objICInstruction.ClientAccountBranchCode & "."
                                        End If


                                        StrAction += " Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                        ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")

                                        ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "48", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                                        PassToCancelCount = PassToCancelCount + 1

                                        Continue For
                                    Else
                                        StrAction = Nothing
                                        StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as reversed from status change queue on approval."
                                        StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                        ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                        FailToCancelCount = FailToCancelCount + 1
                                        Continue For
                                    End If
                                Catch ex As Exception
                                    StrAction = Nothing
                                    StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as cancelled on approval from status change queue due to " & ex.Message.ToString
                                    StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                    ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                    FailToCancelCount = FailToCancelCount + 1
                                    Continue For
                                End Try
                            Else
                                ''audit trail entry
                                StrAction = Nothing
                                StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped fail to mark as cancelled on approval from status change queue due to invalid instruction status for approve reversal."
                                StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                FailToCancelCount = FailToCancelCount + 1
                                Continue For

                            End If
                        Else
                            ''audit trail entry
                            StrAction = Nothing
                            StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as cancelled on approval from status change queue due to approver must be other than maker."
                            StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                            ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For

                        End If
                    End If
                Next
                ArrayListStatus.Add(21)
                ArrayListStatus.Add(10)
                ArrayListStatus.Add(14)
                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArrayListInstructionID, ArrayListStatus, False, "Cheque")
                dtInstructionInfoPO = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArrayListInstructionID, ArrayListStatus, True)
                If dtInstructionInfo.Rows.Count > 0 Then
                    EmailUtilities.TxnMarkAsReversalApprovedFromStatusChangequeue(dtInstructionInfo, dtInstructionInfoPO, ArrayListInstructionID, ArrayListStatus, Me.UserInfo.Username.ToString)
                    SMSUtilities.TxnaReversalApprovedFromStatusChangequeue(dtInstructionInfo, ArrayListStatus, ArrayListInstructionID, "APNatureAndLocation")
                End If
                If Not PassToCancelCount = 0 And Not FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) approved successfully.<br /> [ " & FailToCancelCount.ToString & " ] Instruction(s) can not approved successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToCancelCount = 0 And FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) approved successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToCancelCount = 0 And PassToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & FailToCancelCount.ToString & " ] Instruction(s) can not approved successfully.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Protected Sub btnCancelApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelApproval.Click
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim objICInstruction As ICInstruction
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0
                Dim ActionStr As String = Nothing
                Dim objICInstructionLastStatus As ICInstructionStatus
                Dim Remarks As String = ""
                Dim StrAction As String = Nothing
                Dim IsMakerCheckerSame As Boolean = False
                If CheckgvInstructionApprovalStatusChangeQueue() = False Then
                    ClearAllTextBoxes()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If txtApprovalRemarks.Text <> "" And Not txtApprovalRemarks.Text Is Nothing Then
                    Remarks = txtApprovalRemarks.Text.ToString
                End If
                For Each gvStatusChnageApprovalRow As GridDataItem In gvStatusChangeApprovalQueue.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvStatusChnageApprovalRow.Cells(0).FindControl("chkSelectApproval"), CheckBox)
                    If chkSelect.Checked = True Then
                        objICInstruction = New ICInstruction
                        objICInstructionLastStatus = New ICInstructionStatus
                        objICInstruction.LoadByPrimaryKey(gvStatusChnageApprovalRow.GetDataKeyValue("InstructionID"))
                        objICInstructionLastStatus.LoadByPrimaryKey(objICInstruction.LastStatus.ToString)
                        objICInstruction.StatusChangeApprovalCancelledby = Me.UserId
                        objICInstruction.StatusChangeApprovalCancelledDate = Date.Now
                        objICInstruction.StatusApproveRemarks = Remarks
                        ActionStr = Nothing
                        ActionStr = "Instruction with ID [ " & objICInstruction.InstructionID & " ] approval cancelled from "
                        If objICInstruction.Status.ToString = "44" Then
                            ActionStr += "[44] [ Approve Settlement ]"
                        ElseIf objICInstruction.Status.ToString = "45" Then
                            ActionStr += "[45] [ Approve Reversal ]"
                        ElseIf objICInstruction.Status.ToString = "46" Then
                            ActionStr += "[46] [ Approve Cancellation ] "
                        ElseIf objICInstruction.Status.ToString = "54" Then
                            ActionStr += "[54] [ Approve Hold ]"
                        ElseIf objICInstruction.Status.ToString = "55" Then
                            ActionStr += "[55] [ Approve UnHold ] "
                        ElseIf objICInstruction.Status.ToString = "56" Then
                            ActionStr += "[56] [ Approve Revert ]"
                        End If
                        ActionStr += " to [ " & objICInstructionLastStatus.StatusID & " ] [ " & objICInstructionLastStatus.StatusName & " ]. Action was taken by [ " & Me.UserId & " ] [ " & Me.UserInfo.Username & " ]."
                        Try
                            If objICInstruction.Status.ToString = "54" Then
                                If objICInstruction.StatusHoldBy.ToString <> Me.UserId.ToString Then
                                    ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, ActionStr, "UPDATE")
                                    ICInstructionController.UpdateInstructionCurrentStatus(objICInstruction.InstructionID.ToString, objICInstruction.LastStatus.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                                    PassCount = PassCount + 1
                                Else
                                    ''audit trail entry
                                    StrAction = Nothing
                                    StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as rejected on approval from status change queue due to instruction must be rejected by other than maker."
                                    StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                    ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                    FailCount = FailCount + 1
                                    Continue For
                                End If

                            ElseIf objICInstruction.Status.ToString = "55" Then
                                If objICInstruction.StatusUnHoldBy.ToString <> Me.UserId.ToString Then
                                    ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, ActionStr, "UPDATE")
                                    ICInstructionController.UpdateInstructionCurrentStatus(objICInstruction.InstructionID.ToString, "50", Me.UserId.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                                    PassCount = PassCount + 1
                                Else
                                    ''audit trail entry
                                    StrAction = Nothing
                                    StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as rejected on approval from status change queue due to instruction must be rejected by other than maker."
                                    StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                    ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                    FailCount = FailCount + 1
                                    Continue For
                                End If

                            Else
                                If objICInstruction.Status.ToString = "44" Then
                                    If objICInstruction.StatusSettledBy.ToString = Me.UserId.ToString Then
                                        IsMakerCheckerSame = True
                                    End If
                                ElseIf objICInstruction.Status.ToString = "45" Then
                                    If objICInstruction.StatusReversedBy.ToString = Me.UserId.ToString Then
                                        IsMakerCheckerSame = True
                                    End If
                                ElseIf objICInstruction.Status.ToString = "46" Then
                                    If objICInstruction.StatusCancelledBy.ToString = Me.UserId.ToString Then
                                        IsMakerCheckerSame = True
                                    End If
                                ElseIf objICInstruction.Status.ToString = "56" Then
                                    If objICInstruction.StatusRevertBy.ToString = Me.UserId.ToString Then
                                        IsMakerCheckerSame = True
                                    End If
                                End If

                                If IsMakerCheckerSame = False Then
                                    ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, ActionStr, "UPDATE")
                                    ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, objICInstruction.LastStatus.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                                    PassCount = PassCount + 1
                                Else
                                    ''audit trail entry
                                    StrAction = Nothing
                                    StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as rejected on approval from status change queue due to instruction must be rejected by other than maker."
                                    StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                    ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                    FailCount = FailCount + 1
                                    Continue For
                                End If
                            End If

                        Catch ex As Exception
                            FailCount = FailCount + 1
                            Continue For
                        End Try
                    End If
                Next
                If Not PassCount = 0 And Not FailCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassCount.ToString & " ] Instruction(s) approval rejected successfully.<br /> [ " & FailCount.ToString & " ] Instruction(s) approval not rejected successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassCount = 0 And FailCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassCount.ToString & " ] Instruction(s) approval rejected successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailCount = 0 And PassCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & FailCount.ToString & " ] Instruction(s) approval not rejected successfully.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    'Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStatus.SelectedIndexChanged
    '    Try
    '        SetArraListRoleID()
    '        LoadddlProductTypeByAccountPaymentNature()
    '        LoadddlProductTypeByAccountPaymentNature()
    '        LoadgvStatusChangeQueue(True)
    '        LoadgvStatusChangeApprovalQueue(True)
    '        ClaerCreditAccountNoTextBoxes()
    '        radCreditAccountNumber.Enabled = False
    '        tblCreditAccountNumber.Style.Add("display", "none")
    '        tblCBAccountType.Style.Add("display", "none")
    '        ddlCBAccountType.ClearSelection()
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

    Protected Sub ddlCBAccountType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCBAccountType.SelectedIndexChanged
        Try
            btnCancelCB.Visible = True
            If ddlCBAccountType.SelectedValue.ToString <> "0" Then
                If ddlCBAccountType.SelectedValue.ToString = "RB" Then
                    SetGLAccountTypeValues()
                    btnCancelCB.Visible = False
                ElseIf ddlCBAccountType.SelectedValue.ToString = "GL" Then
                    SetGLAccountTypeValues()
                    btnCancelCB.Visible = False
                End If
            Else
                ClaerCreditAccountNoTextBoxes()
                radCreditAccountNumber.Enabled = False
                tblCreditAccountNumber.Style.Add("display", "none")

            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub SetGLAccountTypeValues()
        If ddlCBAccountType.SelectedValue = "GL" Then
            ClaerCreditAccountNoTextBoxes()
            radCreditAccountNumber.Enabled = True
            tblCreditAccountNumber.Style.Remove("display")

            'lblCreditAccountProfitCentre.Visible = True
            'lblCreditAccountClientNo.Visible = True
            'lblCreditAccountSeqNo.Visible = True
            txtCreditAccountNo.AutoPostBack = False
            txtCreditAccountBrCode.ReadOnly = False
            txtCreditAccountCurrency.ReadOnly = False


            'txtCreditAccountProfitCentre.Visible = True
            'txtCreditAccountClientNo.Visible = True
            'txtCreditAccountSequenceNo.Visible = True


            'radCreditAccountNumber.GetSettingByBehaviorID("radClientNo").Validation.IsRequired = True
            'radCreditAccountNumber.GetSettingByBehaviorID("radSeqNo").Validation.IsRequired = True
            'radCreditAccountNumber.GetSettingByBehaviorID("radProfitCentre").Validation.IsRequired = True
            'radCreditAccountNumber.GetSettingByBehaviorID("radClientNo").EmptyMessage = ""
            'radCreditAccountNumber.GetSettingByBehaviorID("radSeqNo").EmptyMessage = ""
            'radCreditAccountNumber.GetSettingByBehaviorID("radProfitCentre").EmptyMessage = ""
            'radCreditAccountNumber.GetSettingByBehaviorID("radClientNo").ErrorMessage = "Invalid Client No"
            'radCreditAccountNumber.GetSettingByBehaviorID("radSeqNo").ErrorMessage = "Invalid Seq No"
            'radCreditAccountNumber.GetSettingByBehaviorID("radProfitCentre").ErrorMessage = "Invalid Profit Centre"

        Else
            ClaerCreditAccountNoTextBoxes()
            radCreditAccountNumber.Enabled = True
            tblCreditAccountNumber.Style.Remove("display")

            'lblCreditAccountProfitCentre.Visible = False
            'lblCreditAccountClientNo.Visible = False
            'lblCreditAccountSeqNo.Visible = False
            txtCreditAccountNo.AutoPostBack = True
            txtCreditAccountBrCode.ReadOnly = True
            txtCreditAccountCurrency.ReadOnly = True
            'txtCreditAccountProfitCentre.Visible = False
            'txtCreditAccountClientNo.Visible = False
            'txtCreditAccountSequenceNo.Visible = False


            'radCreditAccountNumber.GetSettingByBehaviorID("radClientNo").Validation.IsRequired = False
            'radCreditAccountNumber.GetSettingByBehaviorID("radSeqNo").Validation.IsRequired = False
            'radCreditAccountNumber.GetSettingByBehaviorID("radProfitCentre").Validation.IsRequired = False
            'radCreditAccountNumber.GetSettingByBehaviorID("radClientNo").EmptyMessage = ""
            'radCreditAccountNumber.GetSettingByBehaviorID("radSeqNo").EmptyMessage = ""
            'radCreditAccountNumber.GetSettingByBehaviorID("radProfitCentre").EmptyMessage = ""
            'radCreditAccountNumber.GetSettingByBehaviorID("radClientNo").ErrorMessage = ""
            'radCreditAccountNumber.GetSettingByBehaviorID("radSeqNo").ErrorMessage = ""
            'radCreditAccountNumber.GetSettingByBehaviorID("radProfitCentre").ErrorMessage = ""
        End If
    End Sub

    Protected Sub btnCancelCB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelCB.Click
        If Page.IsValid Then
            Try
                ddlCBAccountType.ClearSelection()
                RefreshPage()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub txtCreditAccountNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCreditAccountNo.TextChanged
        Try
            If ddlCBAccountType.SelectedValue.ToString = "RB" Then
                Dim StrAccountDetails As String()
                Dim AccountStatus As String = ""
                Dim BBAccNo As String = Nothing
                If Not txtCreditAccountNo.Text.ToString = "" And Not txtCreditAccountNo.Text.ToString = "Enter Account Number" Then
                    If CBUtilities.IsNormalAccountNoOrIBAN(txtCreditAccountNo.Text.ToString) = True Then
                        If CBUtilities.ValidateIBAN(txtCreditAccountNo.Text.ToString) = True Then
                            BBAccNo = CBUtilities.GetBBANFromIBAN(txtCreditAccountNo.Text.ToString)
                            AccountStatus = CBUtilities.AccountStatus(BBAccNo, ICBO.CBUtilities.AccountType.RB, "Account Status", txtCreditAccountNo.ToString, "Credit", "Status Change Queue", txtCreditAccountBrCode.Text).ToString
                            StrAccountDetails = (CBUtilities.TitleFetch(BBAccNo, ICBO.CBUtilities.AccountType.RB, "IC Client Type", txtCreditAccountNo.Text.ToString).ToString.Split(";"))
                            If AccountStatus = "Active" Then
                                txtCreditAccountCurrency.Text = StrAccountDetails(2).ToString
                                txtCreditAccountBrCode.Text = StrAccountDetails(1).ToString
                            Else
                                UIUtilities.ShowDialog(Me, "Warning", AccountStatus.ToString, ICBO.IC.Dialogmessagetype.Warning)
                                txtCreditAccountCurrency.Text = StrAccountDetails(2).ToString
                                txtCreditAccountBrCode.Text = StrAccountDetails(1).ToString
                            End If
                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Invalid IBAN", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    Else
                        AccountStatus = CBUtilities.AccountStatus(txtCreditAccountNo.Text.ToString, ICBO.CBUtilities.AccountType.RB, "Account Status", txtCreditAccountNo.ToString, "Credit", "Status Change Queue", txtCreditAccountBrCode.Text).ToString
                        StrAccountDetails = (CBUtilities.TitleFetch(txtCreditAccountNo.Text.ToString, ICBO.CBUtilities.AccountType.RB, "IC Client Type", txtCreditAccountNo.Text.ToString).ToString.Split(";"))
                        If AccountStatus = "Active" Then
                            txtCreditAccountCurrency.Text = StrAccountDetails(2).ToString
                            txtCreditAccountBrCode.Text = StrAccountDetails(1).ToString
                        Else
                            UIUtilities.ShowDialog(Me, "Warning", AccountStatus.ToString, ICBO.IC.Dialogmessagetype.Warning)
                            txtCreditAccountCurrency.Text = StrAccountDetails(2).ToString
                            txtCreditAccountBrCode.Text = StrAccountDetails(1).ToString
                        End If
                    End If
                Else
                    ClaerCreditAccountNoTextBoxes()
                    'UIUtilities.ShowDialog(Me, "Error", "Please enter valid account number.", ICBO.IC.Dialogmessagetype.Failure)

                    Exit Sub
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlAction()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlAction.Items.Clear()
            ddlAction.Items.Add(lit)
            ddlAction.AppendDataBoundItems = True

            Dim arrAction As New ArrayList
            Dim dt As New DataTable
            Dim dr As DataRow
            dt.Columns.Add("Text")
            dt.Columns.Add("Value")

            If CBool(htRights("Hold")) = True Then
                arrAction.Add("Hold")
            End If
            If CBool(htRights("Reverse")) = True Then
                arrAction.Add("Reverse")
            End If
            If CBool(htRights("Revert")) = True Then
                arrAction.Add("Revert")
            End If
            If CBool(htRights("Settle")) = True Then
                arrAction.Add("Settle")
            End If
            If CBool(htRights("UnHold")) = True Then
                arrAction.Add("UnHold")
            End If
            If CBool(htRights("Cancel Instruction")) = True Then
                arrAction.Add("Cancel Instruction")
            End If


            If CBool(htRights("Approve Cancellation")) = True Then
                arrAction.Add("Approve Cancellation")
            End If
            If CBool(htRights("Approve Hold")) = True Then
                arrAction.Add("Approve Hold")
            End If
            If CBool(htRights("Approve Reversal")) = True Then
                arrAction.Add("Approve Reversal")
            End If
            If CBool(htRights("Approve Revert")) = True Then
                arrAction.Add("Approve Revert")
            End If
            If CBool(htRights("Approve Settlement")) = True Then
                arrAction.Add("Approve Settlement")
            End If
            If CBool(htRights("Approve UnHold")) = True Then
                arrAction.Add("Approve UnHold")
            End If
            'If CBool(htRights("Cancel Approval")) = True Then
            '    arrAction.Add("Cancel Approval")
            'End If


            If Not arrAction Is Nothing Then
                For i As Integer = 0 To arrAction.Count - 1
                    dr = dt.NewRow()
                    dr("Text") = arrAction(i).ToString()
                    dr("Value") = arrAction(i).ToString()
                    dt.Rows.Add(dr)
                Next
            End If

            ddlAction.DataSource = dt
            ddlAction.DataTextField = "Text"
            ddlAction.DataValueField = "Value"
            ddlAction.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlAction_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAction.SelectedIndexChanged
        Try
            SetArraListRoleID()
            ClaerCreditAccountNoTextBoxes()
            radCreditAccountNumber.Enabled = False
            tblCreditAccountNumber.Style.Add("display", "none")
            tblCBAccountType.Style.Add("display", "none")
            ddlCBAccountType.ClearSelection()
            tblApprovalQueue.Style.Add("display", "none")
            lblApprovalRemarks.Visible = False
            txtApprovalRemarks.Visible = False
            txtApprovalRemarks.Text = Nothing

            btnView.Visible = True
            HideAllButtons()
            gvStatusChangeQueue.DataSource = Nothing
            gvStatusChangeQueue.DataBind()
            gvStatusChangeQueue.Visible = False

            gvStatusChangeApprovalQueue.DataSource = Nothing
            gvStatusChangeApprovalQueue.DataBind()
            gvStatusChangeApprovalQueue.Visible = False

            lblNRF.Visible = False
            lblNRFApprovalGrid.Visible = False

            If ddlAction.SelectedValue = "Revert" Then
                lblRevertToStatus.Visible = True
                lblReqRevertToStatus.Visible = True
                ddlRevertToStatus.Visible = True
                rfvddlRevertToStatus.Enabled = True
                LoadddlRevertToStatus()
            Else
                LoadddlRevertToStatus()
                lblRevertToStatus.Visible = False
                lblReqRevertToStatus.Visible = False
                ddlRevertToStatus.Visible = False
                rfvddlRevertToStatus.Enabled = False
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub HideAllButtons()
        btnReverse.Visible = False
        btnSetteled.Visible = False
        btnCancelInstructions.Visible = False
        btnRevert.Visible = False
        btnHold.Visible = False
        btnUnHold.Visible = False
        btnApproveReversal.Visible = False
        btnApproveSettlement.Visible = False
        btnApproveCancellation.Visible = False
        btnCancelApproval.Visible = False
        btnApproveHold.Visible = False
        btnApproveRevert.Visible = False
        btnApproveUnHold.Visible = False
    End Sub

    Private Sub ButtonVisibleOnActionDropDown()
        If ddlAction.SelectedValue = "Reverse" Then
            If gvStatusChangeQueue.Visible = True Then
                btnReverse.Visible = True
                btnView.Visible = False
            End If
        ElseIf ddlAction.SelectedValue = "Settle" Then
            If gvStatusChangeQueue.Visible = True Then
                btnSetteled.Visible = True
                btnView.Visible = False
            End If
        ElseIf ddlAction.SelectedValue = "Cancel Instruction" Then
            If gvStatusChangeQueue.Visible = True Then
                btnCancelInstructions.Visible = True
                btnView.Visible = False
            End If
        ElseIf ddlAction.SelectedValue = "Revert" Then
            If gvStatusChangeQueue.Visible = True Then
                btnRevert.Visible = True
                btnView.Visible = False
            End If
        ElseIf ddlAction.SelectedValue = "Hold" Then
            If gvStatusChangeQueue.Visible = True Then
                btnHold.Visible = True
                btnView.Visible = False
            End If
        ElseIf ddlAction.SelectedValue = "UnHold" Then
            If gvStatusChangeQueue.Visible = True Then
                btnUnHold.Visible = True
                btnView.Visible = False
            End If
        ElseIf ddlAction.SelectedValue = "Approve Cancellation" Then
            If gvStatusChangeApprovalQueue.Visible = True Then
                btnApproveCancellation.Visible = True
                btnView.Visible = False
                If CBool(htRights("Cancel Approval")) = True Then
                    btnCancelApproval.Visible = True
                End If
            End If
        ElseIf ddlAction.SelectedValue = "Approve Hold" Then
            If gvStatusChangeApprovalQueue.Visible = True Then
                btnApproveHold.Visible = True
                btnView.Visible = False
                If CBool(htRights("Cancel Approval")) = True Then
                    btnCancelApproval.Visible = True
                End If
            End If
        ElseIf ddlAction.SelectedValue = "Approve Reversal" Then
            If gvStatusChangeApprovalQueue.Visible = True Then
                btnApproveReversal.Visible = True
                btnView.Visible = False
                If CBool(htRights("Cancel Approval")) = True Then
                    btnCancelApproval.Visible = True
                End If
            End If
        ElseIf ddlAction.SelectedValue = "Approve Revert" Then
            If gvStatusChangeApprovalQueue.Visible = True Then
                btnApproveRevert.Visible = True
                btnView.Visible = False
                If CBool(htRights("Cancel Approval")) = True Then
                    btnCancelApproval.Visible = True
                End If
            End If
        ElseIf ddlAction.SelectedValue = "Approve Settlement" Then
            If gvStatusChangeApprovalQueue.Visible = True Then
                btnApproveSettlement.Visible = True
                btnView.Visible = False
                If CBool(htRights("Cancel Approval")) = True Then
                    btnCancelApproval.Visible = True
                End If
            End If
        ElseIf ddlAction.SelectedValue = "Approve UnHold" Then
            If gvStatusChangeApprovalQueue.Visible = True Then
                btnApproveUnHold.Visible = True
                btnView.Visible = False
                If CBool(htRights("Cancel Approval")) = True Then
                    btnCancelApproval.Visible = True
                End If
            End If
            'ElseIf ddlAction.SelectedValue = "Cancel Approval" Then
            '    If gvStatusChangeApprovalQueue.Visible = True Then
            '        btnCancelApproval.Visible = True
            '    End If
        Else
            HideAllButtons()
        End If
    End Sub

    Protected Sub btnView_Click(sender As Object, e As EventArgs) Handles btnView.Click
        If Page.IsValid Then
            Try
                If ddlAction.SelectedValue = "Reverse" Or ddlAction.SelectedValue = "Settle" Or ddlAction.SelectedValue = "Cancel Instruction" Or ddlAction.SelectedValue = "Hold" Or ddlAction.SelectedValue = "Revert" Or ddlAction.SelectedValue = "UnHold" Then
                    LoadgvStatusChangeQueue(True)
                End If
                If ddlAction.SelectedValue = "Approve Reversal" Or ddlAction.SelectedValue = "Approve Settlement" Or ddlAction.SelectedValue = "Approve Cancellation" Or ddlAction.SelectedValue = "Approve Hold" Or ddlAction.SelectedValue = "Approve Revert" Or ddlAction.SelectedValue = "Approve UnHold" Then
                    LoadgvStatusChangeApprovalQueue(True)
                End If

                HideAllButtons()
                ButtonVisibleOnActionDropDown()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnHold_Click(sender As Object, e As EventArgs) Handles btnHold.Click
        If Page.IsValid Then
            Try
                Dim ArryListRequiredStatsu As New ArrayList
                Dim StrAction As String = Nothing
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim dtInstructionInfo As New DataTable
                Dim ArrayListStatus As New ArrayList
                Dim ArrayListInstructionID As New ArrayList
                Dim objICInstructionColl As New ICInstructionCollection
                Dim Remarks As String = Nothing

                ArryListRequiredStatsu.Add(21)     ' Printed
                ArryListRequiredStatsu.Add(48)  'Ready to disburse
                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    ClearAllTextBoxes()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Dim ResultStr As String = ""
                ResultStr = CheckInstructionRequiredStatusSecond(ArryListRequiredStatsu)
                If ResultStr <> "OK" Then
                    ClearAllTextBoxes()
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Following selected Instruction(s) are invalid :<br />" & ResultStr, ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                objICInstructionColl = GetInstructionCollWithRequiredStatusForHold(ArryListRequiredStatsu)
                If objICInstructionColl.Count = 0 Then
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Invalid selected Instruction(s) status.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If txtRemarks.Text <> "" And Not txtRemarks.Text Is Nothing Then
                    Remarks = txtRemarks.Text.ToString
                Else
                    Remarks = ""
                End If
                If objICInstructionColl.Count > 0 Then
                    For Each objICInstruction As ICInstruction In objICInstructionColl
                        ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
                        objICInstruction.StatusHoldBy = Me.UserId
                        objICInstruction.StatusHoldDate = Date.Now
                        objICInstruction.StatusChangeRemarks = Remarks
                        StrAction = Nothing
                        StrAction = "Instruction with ID [ " & objICInstruction.InstructionID & " ] mark as hold and pending for hold approval from status change queue."
                        StrAction += " Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                        Try
                            ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "54", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                            PassToCancelCount = PassToCancelCount + 1
                        Catch ex As Exception
                            StrAction = Nothing
                            StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as hold from status change queue due to " & ex.Message.ToString
                            StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                            ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For
                        End Try
                    Next
                Else
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "No instruction hold.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                ArrayListStatus.Add(54)
                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArrayListInstructionID, ArrayListStatus, False)
                If dtInstructionInfo.Rows.Count > 0 Then
                    EmailUtilities.TxnMarkAsHoldApprovalFromStatusChnagequeue(dtInstructionInfo, ArrayListInstructionID, ArrayListStatus, Me.UserInfo.Username.ToString)
                    SMSUtilities.TxnaAvailableForHoldApprovalFromStatusChnageQueue(dtInstructionInfo, ArrayListStatus, ArrayListInstructionID, "APNatureAndLocation")
                End If

                If Not PassToCancelCount = 0 And Not FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) sent for hold approval successfully.<br /> [ " & FailToCancelCount.ToString & " ] Instruction(s) can not be sent to hold approval successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToCancelCount = 0 And FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) sent for hold approval successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToCancelCount = 0 And PassToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & FailToCancelCount.ToString & " ] Instruction(s) can not be sent to hold approval successfully.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If


            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnRevert_Click(sender As Object, e As EventArgs) Handles btnRevert.Click
        If Page.IsValid Then
            Try
                Dim ArryListRequiredStatsu As New ArrayList
                Dim StrAction As String = Nothing
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim dtInstructionInfo As New DataTable
                Dim ArrayListStatus As New ArrayList
                Dim ArrayListInstructionID As New ArrayList
                Dim objICInstructionColl As New ICInstructionCollection
                Dim Remarks As String = Nothing

                ArryListRequiredStatsu.Add(39)    ' Pending CB
                'ArryListRequiredStatsu.Add(1)     ' In Process
                ArryListRequiredStatsu.Add(32)    ' Instrument Stopped
                ArryListRequiredStatsu.Add(37)    ' Error

                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    ClearAllTextBoxes()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Dim ResultStr As String = ""
                ResultStr = CheckInstructionRequiredStatusSecond(ArryListRequiredStatsu)
                If ResultStr <> "OK" Then
                    ClearAllTextBoxes()
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Following selected Instruction(s) are invalid :<br />" & ResultStr, ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                objICInstructionColl = GetInstructionCollWithRequiredStatusForRevert(ArryListRequiredStatsu)
                If objICInstructionColl.Count = 0 Then
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Invalid selected Instruction(s) status.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If txtRemarks.Text <> "" And Not txtRemarks.Text Is Nothing Then
                    Remarks = txtRemarks.Text.ToString
                Else
                    Remarks = ""
                End If
                If objICInstructionColl.Count > 0 Then
                    For Each objICInstruction As ICInstruction In objICInstructionColl
                        ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
                        objICInstruction.StatusRevertBy = Me.UserId
                        objICInstruction.StatusRevertDate = Date.Now
                        objICInstruction.StatusChangeRemarks = Remarks
                        objICInstruction.FinalStatus = ddlRevertToStatus.SelectedValue
                        StrAction = Nothing
                        StrAction = "Instruction with ID [ " & objICInstruction.InstructionID & " ] mark as Revert and pending for Revert approval from status change queue."
                        StrAction += " Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                        Try
                            ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "56", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                            PassToCancelCount = PassToCancelCount + 1
                        Catch ex As Exception
                            StrAction = Nothing
                            StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as Revert from status change queue due to " & ex.Message.ToString
                            StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                            ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For
                        End Try
                    Next
                Else
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "No instruction Revert.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                ArrayListStatus.Add(53)
                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArrayListInstructionID, ArrayListStatus, False)
                If dtInstructionInfo.Rows.Count > 0 Then
                    EmailUtilities.TxnMarkAsRevertApprovalFromStatusChnagequeue(dtInstructionInfo, ArrayListInstructionID, ArrayListStatus, Me.UserInfo.Username.ToString)
                    SMSUtilities.TxnaAvailableForRevertApprovalFromStatusChnageQueue(dtInstructionInfo, ArrayListStatus, ArrayListInstructionID, "APNatureAndLocation")
                End If

                If Not PassToCancelCount = 0 And Not FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) sent for Revert approval successfully.<br /> [ " & FailToCancelCount.ToString & " ] Instruction(s) can not be sent to Revert approval successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToCancelCount = 0 And FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) sent for Revert approval successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToCancelCount = 0 And PassToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & FailToCancelCount.ToString & " ] Instruction(s) can not be sent to Revert approval successfully.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If


            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnApproveHold_Click(sender As Object, e As EventArgs) Handles btnApproveHold.Click
        If Page.IsValid Then
            Try
                Dim ArryListRequiredStatus As New ArrayList
                Dim ArrayListInstructionID As New ArrayList
                Dim objICInstructionColl As New ICInstructionCollection
                Dim StrAction As String = Nothing
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim dtInstructionInfo As New DataTable
                Dim ArrayListStatus As New ArrayList
                Dim Remarks As String = Nothing
                radCreditAccountNumber.Enabled = False
                ClaerCreditAccountNoTextBoxes()
                ArryListRequiredStatus.Add(54)     ' Approve Hold
                If CheckgvInstructionApprovalStatusChangeQueue() = False Then
                    ClearAllTextBoxes()
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                objICInstructionColl = GetInstructionCollWithRequiredStatusForApproval(ArryListRequiredStatus)

                If txtApprovalRemarks.Text <> "" And Not txtApprovalRemarks.Text Is Nothing Then
                    Remarks = txtRemarks.Text.ToString
                Else
                    Remarks = ""
                End If
                For Each objICInstruction As ICInstruction In objICInstructionColl
                    If objICInstruction.Status.ToString = "54" Then
                        If objICInstruction.StatusHoldBy.ToString <> Me.UserId.ToString Then
                            ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
                            objICInstruction.StatusHoldApprovedBy = Me.UserId
                            objICInstruction.StatusHoldApprovedDate = Date.Now
                            objICInstruction.StatusApproveRemarks = Remarks
                            StrAction = Nothing
                            StrAction = "Instruction with ID [ " & objICInstruction.InstructionID & " ] mark as hold and approved from status change queue."
                            StrAction += " Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                            Try
                                ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                                ICInstructionController.UpdateInstructionCurrentStatus(objICInstruction.InstructionID.ToString, "53", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                                PassToCancelCount = PassToCancelCount + 1
                            Catch ex As Exception
                                StrAction = Nothing
                                StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as hold from status change queue due to " & ex.Message.ToString
                                StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                FailToCancelCount = FailToCancelCount + 1
                                Continue For
                            End Try
                        Else
                            ''audit trail entry
                            StrAction = Nothing
                            StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as hold on approval from status change queue due to instruction must be approved by other than maker."
                            StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                            ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For

                        End If
                    Else
                        ''audit trail entry
                        StrAction = Nothing
                        StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as cancelled on approval from status change queue due invalid current status."
                        StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                        ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "ERROR")
                        FailToCancelCount = FailToCancelCount + 1
                        Continue For

                    End If
                Next
                ArrayListStatus.Add(53)

                If Not (ArrayListInstructionID Is Nothing Or ArrayListInstructionID.Count = 0) Then

                    dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArrayListInstructionID, ArrayListStatus, False)
                    If dtInstructionInfo.Rows.Count > 0 Then
                        Dim dtInstructionInfoPO As New DataTable
                        EmailUtilities.TxnMarkAsHoldApprovedFromStatusChangequeue(dtInstructionInfo, dtInstructionInfoPO, ArrayListInstructionID, ArrayListStatus, Me.UserInfo.Username.ToString)
                        SMSUtilities.TxnHoldApprovedFromStatusChangequeue(dtInstructionInfo, ArrayListStatus, ArrayListStatus, "APNatureAndLocation")
                    End If

                End If

                If Not PassToCancelCount = 0 And Not FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) approved successfully.<br /> [ " & FailToCancelCount.ToString & " ] Instruction(s) can not approved successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToCancelCount = 0 And FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) approved successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToCancelCount = 0 And PassToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & FailToCancelCount.ToString & " ] Instruction(s) can not approved successfully.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnApproveRevert_Click(sender As Object, e As EventArgs) Handles btnApproveRevert.Click
        If Page.IsValid Then
            Try
                Dim ArryListRequiredStatus As New ArrayList
                Dim ArrayListInstructionID As New ArrayList
                Dim objICInstructionColl As New ICInstructionCollection
                Dim StrAction As String = Nothing
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim dtInstructionInfo As New DataTable
                Dim ArrayListStatus As New ArrayList
                Dim Remarks As String = Nothing
                radCreditAccountNumber.Enabled = False
                ClaerCreditAccountNoTextBoxes()
                ArryListRequiredStatus.Add(56)     ' Approve Revert
                If CheckgvInstructionApprovalStatusChangeQueue() = False Then
                    ClearAllTextBoxes()
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                objICInstructionColl = GetInstructionCollWithRequiredStatusForApproval(ArryListRequiredStatus)

                If txtApprovalRemarks.Text <> "" And Not txtApprovalRemarks.Text Is Nothing Then
                    Remarks = txtRemarks.Text.ToString
                Else
                    Remarks = ""
                End If
                For Each objICInstruction As ICInstruction In objICInstructionColl
                    If objICInstruction.Status.ToString = "56" Then
                        If objICInstruction.StatusRevertBy.ToString <> Me.UserId.ToString Then
                            ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
                            objICInstruction.StatusRevertApprovedBy = Me.UserId
                            objICInstruction.StatusRevertApprovedDate = Date.Now
                            objICInstruction.StatusApproveRemarks = Remarks
                            StrAction = Nothing
                            StrAction = "Instruction with ID [ " & objICInstruction.InstructionID & " ] mark as revert and approved from status change queue."
                            StrAction += " Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                            Try
                                ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                                ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, objICInstruction.FinalStatus.ToString(), Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                                Dim Status As String = Nothing
                                Status = GetInstructionActivityCollForLastStatus(objICInstruction.InstructionID, objICInstruction.Status)
                                ArrayListStatus.Add(Status)
                                'ICInstructionController.UpdateInstructionCurrentStatus(objICInstruction.InstructionID.ToString, Status, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                                PassToCancelCount = PassToCancelCount + 1

                            Catch ex As Exception
                                StrAction = Nothing
                                StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as revert from status change queue due to " & ex.Message.ToString
                                StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                FailToCancelCount = FailToCancelCount + 1
                                Continue For
                            End Try
                        Else
                            ''audit trail entry
                            StrAction = Nothing
                            StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as revert on approval from status change queue due to instruction must be approved by other than maker."
                            StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                            ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For

                        End If
                    Else
                        ''audit trail entry
                        StrAction = Nothing
                        StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as cancelled on approval from status change queue due invalid current status."
                        StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                        ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "ERROR")
                        FailToCancelCount = FailToCancelCount + 1
                        Continue For

                    End If
                Next
                'ArrayListStatus.Add(53)

                If Not (ArrayListInstructionID Is Nothing Or ArrayListInstructionID.Count = 0) Then

                    dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArrayListInstructionID, ArrayListStatus, False)
                    If dtInstructionInfo.Rows.Count > 0 Then
                        Dim dtInstructionInfoPO As New DataTable
                        EmailUtilities.TxnMarkAsRevertApprovedFromStatusChangequeue(dtInstructionInfo, dtInstructionInfoPO, ArrayListInstructionID, ArrayListStatus, Me.UserInfo.Username.ToString)
                        SMSUtilities.TxnRevertApprovedFromStatusChangequeue(dtInstructionInfo, ArrayListStatus, ArrayListStatus, "APNatureAndLocation")
                    End If
                End If


                If Not PassToCancelCount = 0 And Not FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) approved successfully.<br /> [ " & FailToCancelCount.ToString & " ] Instruction(s) can not approved successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToCancelCount = 0 And FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) approved successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToCancelCount = 0 And PassToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & FailToCancelCount.ToString & " ] Instruction(s) can not approved successfully.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnUnHold_Click(sender As Object, e As EventArgs) Handles btnUnHold.Click


        If Page.IsValid Then
            Try
                Dim ArryListRequiredStatsu As New ArrayList
                Dim StrAction As String = Nothing
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim dtInstructionInfo As New DataTable
                Dim ArrayListStatus As New ArrayList
                Dim ArrayListInstructionID As New ArrayList
                Dim objICInstructionColl As New ICInstructionCollection
                Dim Remarks As String = Nothing

                ArryListRequiredStatsu.Add(53)     ' Hold

                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    ClearAllTextBoxes()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Dim ResultStr As String = ""
                ResultStr = CheckInstructionRequiredStatusSecond(ArryListRequiredStatsu)
                If ResultStr <> "OK" Then
                    ClearAllTextBoxes()
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Following selected Instruction(s) are invalid :<br />" & ResultStr, ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                objICInstructionColl = GetInstructionCollWithRequiredStatusForUnHold(ArryListRequiredStatsu)
                If objICInstructionColl.Count = 0 Then
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Invalid selected Instruction(s) status.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If txtRemarks.Text <> "" And Not txtRemarks.Text Is Nothing Then
                    Remarks = txtRemarks.Text.ToString
                Else
                    Remarks = ""
                End If
                If objICInstructionColl.Count > 0 Then
                    For Each objICInstruction As ICInstruction In objICInstructionColl
                        ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
                        objICInstruction.StatusUnHoldBy = Me.UserId
                        objICInstruction.StatusUnHoldDate = Date.Now
                        objICInstruction.StatusChangeRemarks = Remarks
                        StrAction = Nothing
                        StrAction = "Instruction with ID [ " & objICInstruction.InstructionID & " ] mark as unhold and pending for unhold approval from status change queue."
                        StrAction += " Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                        Try
                            ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                            ICInstructionController.UpdateInstructionCurrentStatus(objICInstruction.InstructionID.ToString, "55", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                            PassToCancelCount = PassToCancelCount + 1
                        Catch ex As Exception
                            StrAction = Nothing
                            StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as unhold from status change queue due to " & ex.Message.ToString
                            StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                            ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For
                        End Try
                    Next
                Else
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "No instruction Unhold.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                ArrayListStatus.Add(55)
                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArrayListInstructionID, ArrayListStatus, False)
                If dtInstructionInfo.Rows.Count > 0 Then
                    EmailUtilities.TxnMarkAsUnholdApprovalFromStatusChnagequeue(dtInstructionInfo, ArrayListInstructionID, ArrayListStatus, Me.UserInfo.Username.ToString)
                    SMSUtilities.TxnaAvailableForUnholdApprovalFromStatusChnageQueue(dtInstructionInfo, ArrayListStatus, ArrayListInstructionID, "APNatureAndLocation")
                End If

                If Not PassToCancelCount = 0 And Not FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) sent for Unhold approval successfully.<br /> [ " & FailToCancelCount.ToString & " ] Instruction(s) can not be sent to Unhold approval successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToCancelCount = 0 And FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) sent for Unhold approval successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToCancelCount = 0 And PassToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & FailToCancelCount.ToString & " ] Instruction(s) can not be sent to Unhold approval successfully.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnApproveUnHold_Click(sender As Object, e As EventArgs) Handles btnApproveUnHold.Click
        If Page.IsValid Then
            Try
                Dim ArryListRequiredStatus As New ArrayList
                Dim ArrayListInstructionID As New ArrayList
                Dim objICInstructionColl As New ICInstructionCollection
                Dim StrAction As String = Nothing
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim dtInstructionInfo As New DataTable
                Dim ArrayListStatus As New ArrayList
                Dim Remarks As String = Nothing
                radCreditAccountNumber.Enabled = False
                ClaerCreditAccountNoTextBoxes()
                ArryListRequiredStatus.Add(55)     ' Approve UnHold
                If CheckgvInstructionApprovalStatusChangeQueue() = False Then
                    ClearAllTextBoxes()
                    LoadgvStatusChangeQueue(True)
                    LoadgvStatusChangeApprovalQueue(True)
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                objICInstructionColl = GetInstructionCollWithRequiredStatusForApproval(ArryListRequiredStatus)

                If txtApprovalRemarks.Text <> "" And Not txtApprovalRemarks.Text Is Nothing Then
                    Remarks = txtRemarks.Text.ToString
                Else
                    Remarks = ""
                End If
                For Each objICInstruction As ICInstruction In objICInstructionColl
                    If objICInstruction.Status.ToString = "55" Then
                        If objICInstruction.StatusUnHoldBy.ToString <> Me.UserId.ToString Then
                            ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
                            objICInstruction.StatusUnHoldApprovedBy = Me.UserId
                            objICInstruction.StatusUnHoldApprovedDate = Date.Now
                            objICInstruction.StatusApproveRemarks = Remarks
                            StrAction = Nothing
                            StrAction = "Instruction with ID [ " & objICInstruction.InstructionID & " ] mark as unhold and approved from status change queue."
                            StrAction += " Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                            Try
                                ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAction, "UPDATE")
                                If objICInstruction.PaymentMode = "COTC" Then

                                    ICInstructionController.UpdateInstructionCurrentStatus(objICInstruction.InstructionID.ToString, "48", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                                Else
                                    ICInstructionController.UpdateInstructionCurrentStatus(objICInstruction.InstructionID.ToString, "21", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                                End If
                                PassToCancelCount = PassToCancelCount + 1
                            Catch ex As Exception
                                StrAction = Nothing
                                StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as unhold from status change queue due to " & ex.Message.ToString
                                StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                                ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                FailToCancelCount = FailToCancelCount + 1
                                Continue For
                            End Try
                        Else
                            ''audit trail entry
                            StrAction = Nothing
                            StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as unhold on approval from status change queue due to instruction must be approved by other than maker."
                            StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                            ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For

                        End If
                    Else
                        ''audit trail entry
                        StrAction = Nothing
                        StrAction = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped and fail to mark as cancelled on approval from status change queue due invalid current status."
                        StrAction += ". Action was taken by [ " & Me.UserId & " ][ " & Me.UserInfo.Username & " ]."
                        ICUtilities.AddAuditTrail(StrAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "ERROR")
                        FailToCancelCount = FailToCancelCount + 1
                        Continue For

                    End If
                Next
                ArrayListStatus.Add(21)
                ArrayListStatus.Add(48)
                If Not (ArrayListInstructionID Is Nothing Or ArrayListInstructionID.Count = 0) Then

                    dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArrayListInstructionID, ArrayListStatus, False)
                    If dtInstructionInfo.Rows.Count > 0 Then
                        Dim dtInstructionInfoPO As New DataTable
                        EmailUtilities.TxnMarkAsUnholdApprovedFromStatusChangequeue(dtInstructionInfo, dtInstructionInfoPO, ArrayListInstructionID, ArrayListStatus, Me.UserInfo.Username.ToString)
                        SMSUtilities.TxnUnholdApprovedFromStatusChangequeue(dtInstructionInfo, ArrayListStatus, ArrayListStatus, "APNatureAndLocation")
                    End If
                End If


                If Not PassToCancelCount = 0 And Not FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) approved successfully.<br /> [ " & FailToCancelCount.ToString & " ] Instruction(s) can not approved successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToCancelCount = 0 And FailToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & PassToCancelCount.ToString & " ] Instruction(s) approved successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToCancelCount = 0 And PassToCancelCount = 0 Then
                    RefreshPage()
                    UIUtilities.ShowDialog(Me, "Status Change Queue", "[ " & FailToCancelCount.ToString & " ] Instruction(s) can not approved successfully.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Private Sub LoadddlRevertToStatus()
        Try
            Dim lit As New ListItem
            lit.Text = "-- Please Select --"
            lit.Value = "0"
            ddlRevertToStatus.Items.Clear()
            ddlRevertToStatus.Items.Add(lit)
            lit.Selected = True
            ddlRevertToStatus.AppendDataBoundItems = True
            ddlRevertToStatus.DataSource = GetRevertToStatus(GetAllInstructionStatusOnActionDropDown())
            ddlRevertToStatus.DataTextField = "StatusName"
            ddlRevertToStatus.DataValueField = "StatusID"
            ddlRevertToStatus.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Function GetRevertToStatus(ByVal ArrInst As ArrayList) As DataTable
        Dim dt As New DataTable
        Dim collInstStatus As New ICInstructionStatusCollection

        If Not ArrInst Is Nothing Then
            If ArrInst.Count > 0 Then
                collInstStatus.Query.Where(collInstStatus.Query.StatusID.NotIn(ArrInst))
                collInstStatus.Query.OrderBy(collInstStatus.Query.StatusName.Ascending)

                If collInstStatus.Query.Load() Then
                    dt = collInstStatus.Query.LoadDataTable()
                End If
            End If
        End If

        Return dt

    End Function

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click

    End Sub
End Class

