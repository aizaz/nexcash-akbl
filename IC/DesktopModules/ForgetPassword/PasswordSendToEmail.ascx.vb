﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals

Partial Class DesktopModules_PasswordSendToEmail_PasswordSendToEmail
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private GroupCode As String
    Private htRights As Hashtable

#Region "Page Load"
    Protected Sub DesktopModules_PasswordSendToEmail_PasswordSendToEmail_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                lblPageHeader.Text = "Forget Password"
                txtUserNameOrEmail.Text = ""
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    

    Private Sub Clear()
      
    End Sub

#End Region


    Protected Sub btnSendPassword_Click(sender As Object, e As EventArgs) Handles btnSendPassword.Click
        If Page.IsValid Then

            Try
                GetActiveandApprovedUsersbyUserNameOrEmail(txtUserNameOrEmail.Text)
                UIUtilities.ShowDialog(Me, "Password", "Password sent successfully", Dialogmessagetype.Success, NavigateURL(41))
                txtUserNameOrEmail.Text = ""
                Exit Sub
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
            End Try
        End If
    End Sub



    Public Sub GetActiveandApprovedUsersbyUserNameOrEmail(ByVal UserNameOrEmail As String)

        Dim objICUsers As ICUserCollection
       
        objICUsers = New ICUserCollection
        If Not txtUserNameOrEmail.Text = "" Then


            '' IF User Name is Entered

            objICUsers.Query.Select(objICUsers.Query.UserID, objICUsers.Query.UserName, objICUsers.Query.Email)
            objICUsers.Query.Where(objICUsers.Query.UserName = UserNameOrEmail.ToString())
            objICUsers.Query.Where(objICUsers.Query.IsActive = True And objICUsers.Query.IsApproved = True)
            objICUsers.Query.Load()



            If objICUsers.Query.Load() Then
                '' IF User Name is Entered
                Try

                    Dim userInfo As DotNetNuke.Entities.Users.UserInfo = DotNetNuke.Entities.Users.UserController.GetUserByName(Me.PortalId, objICUsers(0).UserName.ToString)
                    Dim password As String = ""
                    Dim password2 As String = ""
                    password2 = DotNetNuke.Entities.Users.UserController.GetPassword(userInfo, password)
                    If Not password2 Is Nothing Then
                        SendPassword(objICUsers(0).Email, password2, objICUsers(0).UserName)
                    End If

                Catch ex As Exception
                    Throw New Exception(ex.Message.ToString)
                End Try
            Else
                '' If Email ID is Entered

                objICUsers = New ICUserCollection
                objICUsers.Query.Select(objICUsers.Query.UserID, objICUsers.Query.UserName, objICUsers.Query.Email)
                objICUsers.Query.Where(objICUsers.Query.Email = UserNameOrEmail.ToString())
                objICUsers.Query.Where(objICUsers.Query.IsActive = True And objICUsers.Query.IsApproved = True)

                If objICUsers.Query.Load() Then
                    Try

                        Dim userInfo As DotNetNuke.Entities.Users.UserInfo = DotNetNuke.Entities.Users.UserController.GetUserByName(Me.PortalId, objICUsers(0).UserName.ToString)
                        Dim password As String = ""
                        Dim password2 As String = ""
                        password2 = DotNetNuke.Entities.Users.UserController.GetPassword(userInfo, password)
                        If Not password2 Is Nothing Then
                            SendPassword(objICUsers(0).Email, password2, objICUsers(0).UserName)
                        End If


                    Catch ex As Exception
                        Throw New Exception(ex.Message.ToString)

                    End Try
                Else
                    Throw New Exception("Invalid user id or email address")
                End If
            End If

        Else
            Throw New Exception("Please Enter User ID or Email ID")
        End If

    End Sub

    Public Shared Sub SendPassword(ByVal Eamil As String, ByVal Password As String, ByVal UserName As String)


        Try
            Dim reader As New System.IO.StreamReader(ICUtilities.GetSettingValue("PhysicalApplicationPath") & "EmailTemplates\SendPasswordOnEmail.htm")
            Dim txt As String

            txt = reader.ReadToEnd
            reader.Close()
            reader.Dispose()
            Dim DisplayName As String = ""


            txt = txt.Replace("[UserName]", UserName)
            txt = txt.Replace("[Password]", Password.ToString)
            txt = txt.Replace("[EmailSignature]", ICUtilities.GetSettingValue("EmailSignature"))

            Dim subject As String
            Dim emailto As String

            emailto = Eamil
            subject = "User Login Password"

            ICUtilities.SendEmail(emailto, subject, txt)

        Catch ex As Exception
            ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
            Throw New Exception("Fail to send email. Please contact Al baraka administrator")
        End Try
    End Sub

    'Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
    '    Try
    '        'Response.Redirect(NavigateURL(False))

    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL(41))

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
