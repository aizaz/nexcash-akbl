﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NotificationManagement.ascx.vb"
    Inherits="DesktopModules_FixUsers_AddEditFixRolesUser" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .RadGrid_Default
    {
        border: 1px solid #828282;
        background: #fff;
        color: #333;
    }
    .RadGrid_Default
    {
        font: 12px/16px "segoe ui" ,arial,sans-serif;
    }
    .RadGrid .rgMasterTable
    {
        border-collapse: separate;
        border-spacing: 0;
    }
    .RadGrid_Default .rgMasterTable
    {
        font: 12px/16px "segoe ui" ,arial,sans-serif;
    }
    .RadGrid .rgHeader
    {
        cursor: default;
    }
    .RadGrid .rgHeader
    {
        padding-left: 7px;
        padding-right: 7px;
    }
    .RadGrid .rgHeader
    {
        padding-top: 5px;
        padding-bottom: 4px;
        text-align: left;
        font-weight: normal;
    }
    .RadGrid_Default .rgHeader
    {
        border: 0;
        border-bottom: 1px solid #828282;
        background: #eaeaea 0 -2300px repeat-x url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
    }
    .RadGrid_Default .rgHeader
    {
        color: #333;
    }
    .RadGrid .rgHeader a
    {
        text-decoration: none;
    }
    .RadGrid_Default .rgHeader a
    {
        color: #333;
    }
    .RadGrid_Default .rgFooter
    {
        background: #eee;
    }
    .style1
    {
        height: 23px;
    }
</style>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }
    function ValidateChkList(source, arguments) {

        arguments.IsValid = IsCheckBoxChecked() ? true : false;



    }


    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;

        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }

</script>
<script type="text/javascript">
    function CheckInGridAll(chkView, chkAmend, chkAmendBulk) {

        if (document.getElementById(chkAmend).checked == true && document.getElementById(chkAmendBulk).checked == false) {
            document.getElementById(chkView).checked = true;
            document.getElementById(chkAmend).checked = true;
        }

        if (document.getElementById(chkAmendBulk).checked == true && document.getElementById(chkAmend).checked == false) {
            document.getElementById(chkView).checked = true;
            document.getElementById(chkAmendBulk).checked = true;
            document.getElementById(chkAmend).checked = false;
        }
        if (document.getElementById(chkAmend).checked == false && document.getElementById(chkAmendBulk).checked == false) {
            document.getElementById(chkView).checked = false;
            document.getElementById(chkAmendBulk).checked = false;
            document.getElementById(chkAmend).checked = false;
        }
    }
</script>
<script type="text/javascript">
    function CheckInGridView(chkView, chkAmend, chkAmendBulk) {

        if (document.getElementById(chkView).checked == true) {
            document.getElementById(chkAmend).checked = true;
            document.getElementById(chkAmendBulk).checked = true;
        }
        else {
            document.getElementById(chkView).checked = false;
            document.getElementById(chkAmend).checked = false;
            document.getElementById(chkAmendBulk).checked = false;
        }
    }
</script>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:TextBoxSetting BehaviorID="reName" Validation-IsRequired="false" ErrorMessage="Invalid Account Title"
        EmptyMessage="Enter Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue">Notification Management</asp:Label>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*"></asp:Label>
            &nbsp;are required.
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top">
            &nbsp;
        </td>
    </tr>
</table>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblUserType" runat="server" Text="Select User Type" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqUserType" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblSelectGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqddlGroup" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlUserType" runat="server" AutoPostBack="True" CssClass="dropdown">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem>Client Users</asp:ListItem>
                <asp:ListItem>Bank Users</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroupType" runat="server" AutoPostBack="True" CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvUserType" runat="server" ControlToValidate="ddlUserType"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select User Type" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlGroup" runat="server" ControlToValidate="ddlGroupType"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Group" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblSelectUser" runat="server" Text="Select User" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqBank" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblUserRoles" runat="server" Text="User Roles" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlUser" runat="server" AutoPostBack="True" CssClass="dropdown">
            </asp:DropDownList>
            <br />
            <asp:RequiredFieldValidator ID="rfvddlUser" runat="server" ControlToValidate="ddlUser"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select User" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0" Enabled="False"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:ListBox ID="lstboxUserRoles" runat="server" Height="75px" Width="310px"></asp:ListBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td colspan="4" style="width: 100%">
            <table id="tblSlctCompanyOffice" runat="server" style="width: 100%; display: none">
                <tr>
                    <td style="width: 25%">
                        <asp:Label ID="lblGroup" runat="server" CssClass="lbl" Text="Select Group"></asp:Label>
                        <asp:Label ID="lblreqGroup" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%">
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 25%">
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                    <td style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <asp:Label ID="lblEventsList" Visible="false" runat="server" CssClass="headingblue">Events List</asp:Label><br />
           <%-- <asp:Panel ID="pnlEventsList" runat="server" Height="250px" ScrollBars="Vertical"
                Style="display: none">--%>
                <br />
                <telerik:RadGrid ID="gvEnetsList" runat="server" AllowPaging="True" AllowSorting="True" CssClass="RadGrid"
                    AutoGenerateColumns="False" CellSpacing="0" PageSize="100" ShowFooter="True">
                    <ClientSettings>
                            
                            <Scrolling UseStaticHeaders="true" AllowScroll="true" />
                            </ClientSettings>
                    <AlternatingItemStyle CssClass="rgAltRow" />
                    <MasterTableView DataKeyNames="EventID" TableLayout="Fixed">
                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <%-- <telerik:GridTemplateColumn HeaderText="Select">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" Text="" /></ItemTemplate>
                        </telerik:GridTemplateColumn>--%>
                            <telerik:GridTemplateColumn>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Checked="true" Text=" Select"
                                        TextAlign="Right" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Checked="true" Text=" " />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="EventName" HeaderText="Un-Assigned Event" SortExpression="EventName">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkVIASMS" runat="server" CssClass="chkBox" Text="VIA SMS"
                                        TextAlign="Right" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSMS" runat="server" CssClass="chkBox"  Text=" " />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkVIAEmail" runat="server" CssClass="chkBox" Text="VIA Email"
                                        TextAlign="Right" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkEmail" runat="server" CssClass="chkBox"  Text=" " />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                           <%-- <telerik:GridTemplateColumn>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkVIAEmail" runat="server" CssClass="chkBox" Text="VIA Email"
                                        TextAlign="Right" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkEmail" runat="server" Text="" />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>--%>
                        </Columns>
                    </MasterTableView>
                    <ItemStyle CssClass="rgRow" />
                </telerik:RadGrid>
            <%--</asp:Panel>--%>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px"
                CausesValidation="False" />
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
           <%-- <asp:Panel ID="pnlAssignedEvents" runat="server" Height="250px" ScrollBars="Vertical"
                Style="display: none">--%>
                <br />
                <asp:Label ID="lblAssignedEventsList" runat="server" CssClass="headingblue"></asp:Label>
                <telerik:RadGrid ID="gvAssignedEventsList" runat="server" AllowPaging="True" AllowSorting="True" CssClass="RadGrid"
                    AutoGenerateColumns="False" CellSpacing="0" PageSize="100" ShowFooter="True">
                    <ClientSettings>
                            
                            <Scrolling UseStaticHeaders="true" AllowScroll="true" />
                            </ClientSettings>
                    <AlternatingItemStyle CssClass="rgAltRow" />
                    <MasterTableView DataKeyNames="NotificationID,IsApproved" TableLayout="Fixed">
                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn DataField="EventName" HeaderText="Event Name" SortExpression="EventName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="VIASMS" HeaderText="VIA SMS" SortExpression="EventName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="VIAEmail" HeaderText="VIA Email" SortExpression="EventName">
                            </telerik:GridBoundColumn>
                            <telerik:GridCheckBoxColumn HeaderText="Approve" DataField="IsApproved">
                            </telerik:GridCheckBoxColumn>
                            <telerik:GridTemplateColumn HeaderText="Delete">
                                <ItemTemplate>
                                    <asp:ImageButton ID="IbtnDelete" runat="server" CommandName="del" ImageUrl="~/images/delete.gif"
                                        CommandArgument='<%#Eval("NotificationID") %>' OnClientClick="javascript: return con();"
                                        ToolTip="Delete" />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ItemStyle CssClass="rgRow" />
                </telerik:RadGrid>
           <%-- </asp:Panel>--%>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <asp:Label ID="lblIsApproved" runat="server" Text="Is Approved" CssClass="lbl"></asp:Label>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <asp:CheckBox ID="chkApprove" runat="server" Checked="false" />
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnApprove" runat="server" Text="Approve" Width="77px" CssClass="btn"
                Height="25px" CausesValidation="False" />
        </td>
    </tr>
</table>
