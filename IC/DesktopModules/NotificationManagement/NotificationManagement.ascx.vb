﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI


Partial Class DesktopModules_FixUsers_AddEditFixRolesUser
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase

    Private htRights As Hashtable
    
#Region "Page Load"
    Protected Sub DesktopModules_Users_SaveUser_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                Clear()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Notification Management")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlGroupType.Items.Clear()
            ddlGroupType.Items.Add(lit)
            ddlGroupType.AppendDataBoundItems = True
            ddlGroupType.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroupType.DataValueField = "GroupCode"
            ddlGroupType.DataTextField = "GroupName"
            ddlGroupType.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
  
    Private Sub LoadddlUserByGroupCodeOrPrincipalBank(ByVal UserType As String)
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlUser.Items.Clear()
            ddlUser.Items.Add(lit)
            ddlUser.AppendDataBoundItems = True
            If UserType = "Group Users" Then
                ddlUser.DataSource = ICUserController.GetAllActiveAndApproveUsersByGroupCode(ddlGroupType.SelectedValue.ToString)
                ddlUser.DataValueField = "UserID"
                ddlUser.DataTextField = "UserName"
                ddlUser.DataBind()
               
            ElseIf UserType = "Bank User" Then
                ddlUser.DataSource = ICUserController.GetAllActiveAndApproveUsersOfPrincipalBank()
                ddlUser.DataValueField = "UserID"
                ddlUser.DataTextField = "UserName"
                ddlUser.DataBind()
            End If
            lstboxUserRoles.ClearSelection()
            lblUserRoles.Visible = False
            lstboxUserRoles.Visible = False
            gvAssignedEventsList.Style.Add("display", "none")
            gvEnetsList.Style.Add("display", "none")
            lblIsApproved.Visible = False
            chkApprove.Visible = False
            btnApprove.Visible = False
            btnSave.Visible = False
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Protected Sub ddlUserType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUserType.SelectedIndexChanged
        If ddlUserType.SelectedValue.ToString = "Client Users" Then
            lblSelectGroup.Visible = True
            lblReqddlGroup.Visible = True
            rfvddlGroup.Enabled = True
            ddlGroupType.Visible = True
            lblUserRoles.Visible = False
            lstboxUserRoles.Visible = False
            LoadddlGroup()
            LoadddlUserByGroupCodeOrPrincipalBank("Group Users")
            lblEventsList.Visible = False
            lblAssignedEventsList.Visible = False
        ElseIf ddlUserType.SelectedValue.ToString = "Bank Users" Then
            lblSelectGroup.Visible = False
            lblReqddlGroup.Visible = False
            rfvddlGroup.Enabled = False
            ddlGroupType.ClearSelection()
            ddlGroupType.Visible = False
            LoadddlUserByGroupCodeOrPrincipalBank("Bank User")
            lblUserRoles.Visible = False
            lstboxUserRoles.Visible = False
            lblEventsList.Visible = False
            lblAssignedEventsList.Visible = False
        Else
            lblAssignedEventsList.Visible = False
            lblEventsList.Visible = False
            Clear()

        End If
    End Sub

    Protected Sub ddlGroupType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroupType.SelectedIndexChanged
        Try
            LoadddlUserByGroupCodeOrPrincipalBank("Group Users")
            lblAssignedEventsList.Visible = False
            lblEventsList.Visible = False
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlUser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUser.SelectedIndexChanged
        LoadListBoxUserRolesByUserID(ddlUser.SelectedValue.ToString)
        LoadEventsList(True)
        LoadEventsListAssignedToUser(True)
        CheckMarkEventsListByAssignedEventsToUser()

    End Sub
    Private Sub CheckMarkEventsListByAssignedEventsToUser()
        Dim objICNotificationManagementColl As New ICNotificationManagementCollection
        Dim chkIsSMSAllow, chkIsEmaillow As CheckBox
        objICNotificationManagementColl.es.Connection.CommandTimeout = 3600



        objICNotificationManagementColl = ICNotificationManagementController.GetAllNotificationAssignedToUser(ddlUser.SelectedValue.ToString)
        If objICNotificationManagementColl.Count > 0 Then
            For Each objICNotificationMangement As ICNotificationManagement In objICNotificationManagementColl
                For Each gvEventListRow As GridDataItem In gvEnetsList.Items
                    If objICNotificationMangement.EventID = gvEventListRow.GetDataKeyValue("EventID") Then
                        chkIsSMSAllow = New CheckBox
                        chkIsSMSAllow = New CheckBox
                        chkIsEmaillow = DirectCast(gvEventListRow.Cells(2).FindControl("chkEmail"), CheckBox)
                        chkIsSMSAllow = DirectCast(gvEventListRow.Cells(1).FindControl("chkSMS"), CheckBox)
                        chkIsEmaillow.Checked = objICNotificationMangement.IsEmailAllow
                        chkIsSMSAllow.Checked = objICNotificationMangement.IsSMSAllow
                    End If
                Next
            Next
        End If
    End Sub
    Private Sub LoadListBoxUserRolesByUserID(ByVal UserID As String)
        Dim dt As New DataTable
        lstboxUserRoles.Items.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(UserID.ToString)
        If dt.Rows.Count > 0 Then
            lblUserRoles.Visible = True
            lstboxUserRoles.Visible = True
            For Each dr As DataRow In dt.Rows
                Dim lit As New ListItem
                lit.Value = dr("RoleID")
                lit.Text = dr("RoleName")
                lstboxUserRoles.Items.Add(lit)
            Next
        Else
            lblUserRoles.Visible = False
            lstboxUserRoles.Items.Clear()
            lstboxUserRoles.Visible = False
        End If

    End Sub
    Private Sub LoadEventsList(ByVal DoDataBind As Boolean)
        ICEventsListController.GetAllEventsListForRadGrid(Me.gvEnetsList.CurrentPageIndex + 1, Me.gvEnetsList.PageSize, DoDataBind, Me.gvEnetsList)
        If gvEnetsList.Items.Count > 0 Then
            gvEnetsList.Style.Remove("display")
            btnSave.Visible = CBool(htRights("Add"))
            lblEventsList.Visible = True
        Else
            lblEventsList.Visible = False
            btnSave.Visible = False
            gvEnetsList.Style.Add("display", "none")
        End If
    End Sub
    Private Sub LoadEventsListAssignedToUser(ByVal DoDataBind As Boolean)
        ICNotificationManagementController.GetAllEventsListAssignedToUserForRadGrid(ddlUser.SelectedValue.ToString, Me.gvAssignedEventsList.CurrentPageIndex + 1, Me.gvAssignedEventsList.PageSize, DoDataBind, Me.gvAssignedEventsList)
        If gvAssignedEventsList.Items.Count > 0 Then
            Dim objICUser As New ICUser
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(ddlUser.SelectedValue.ToString)
            gvAssignedEventsList.Style.Remove("display")
            lblIsApproved.Visible = CBool(htRights("Approve"))
            chkApprove.Visible = CBool(htRights("Approve"))
            btnApprove.Visible = CBool(htRights("Approve"))
            gvAssignedEventsList.Columns(4).Visible = CBool(htRights("Delete"))
            lblAssignedEventsList.Text = "Assigned Events List to " & objICUser.DisplayName
            lblAssignedEventsList.Visible = True
        Else
            lblAssignedEventsList.Text = ""
            lblAssignedEventsList.Visible = False
            gvAssignedEventsList.Style.Add("display", "none")
            lblIsApproved.Visible = False
            chkApprove.Visible = False
            btnApprove.Visible = False
            chkApprove.Checked = False
        End If
    End Sub
    Private Function CheckgvEventsListColl() As Boolean
        Try

            Dim chkVIASMS, chkVIAEmail As CheckBox
            Dim Result As Boolean = False
            For Each rowGVEventListRow In gvEnetsList.Items
                chkVIASMS = New CheckBox
                chkVIAEmail = New CheckBox

                chkVIASMS = DirectCast(rowGVEventListRow.Cells(1).FindControl("chkSMS"), CheckBox)
                chkVIAEmail = DirectCast(rowGVEventListRow.Cells(2).FindControl("chkEmail"), CheckBox)

                If chkVIASMS.Checked = True Or chkVIAEmail.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Page.Validate()
        If Page.IsValid Then
            Try
                Dim objICNotificationManagement As ICNotificationManagement
                Dim chkEmail, chkVIASMS, chkSelect As CheckBox
                Dim RecordCount As Integer = 0
                If CheckgvEventsListColl() = False Then
                    UIUtilities.ShowDialog(Me, "Error", "Please select at least one event to assign to user.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each gvEventListRow As GridDataItem In gvEnetsList.Items
                    chkEmail = New CheckBox
                    chkVIASMS = New CheckBox
                    chkSelect = New CheckBox
                    chkVIASMS = DirectCast(gvEventListRow.Cells(1).FindControl("chkSMS"), CheckBox)
                    chkEmail = DirectCast(gvEventListRow.Cells(2).FindControl("chkEmail"), CheckBox)
                    chkSelect = DirectCast(gvEventListRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        If chkEmail.Checked = True Or chkVIASMS.Checked = True Then
                            If RecordCount = 0 Then
                                DeleteEventsAssignedToUser()
                            End If
                            objICNotificationManagement = New ICNotificationManagement
                            objICNotificationManagement.es.Connection.CommandTimeout = 3600
                            objICNotificationManagement.EventID = gvEventListRow.GetDataKeyValue("EventID")
                            objICNotificationManagement.UserID = ddlUser.SelectedValue
                            objICNotificationManagement.IsEmailAllow = chkEmail.Checked
                            objICNotificationManagement.IsSMSAllow = chkVIASMS.Checked
                            objICNotificationManagement.CreatedDate = Date.Now
                            objICNotificationManagement.CreatedBy = Me.UserId
                            objICNotificationManagement.CreationDate = Date.Now
                            objICNotificationManagement.Creater = Me.UserId
                            ICNotificationManagementController.AddICNotificationForUser(objICNotificationManagement, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            RecordCount = 1
                        End If
                    End If

                Next

                UIUtilities.ShowDialog(Me, "Notification Management", "Notification management settings for user added successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL(117))
                'LoadEventsListAssignedToUser(True)
                'LoadEventsList(True)
                'CheckMarkEventsListByAssignedEventsToUser()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Sub Clear()
        ddlUserType.SelectedValue = "0"
        lblSelectGroup.Visible = False
        lblReqddlGroup.Visible = False
        rfvddlGroup.Enabled = False
        ddlGroupType.Visible = False
        ddlGroupType.Items.Clear()
        lblUserRoles.Visible = False
        lstboxUserRoles.Items.Clear()
        lstboxUserRoles.Visible = False
        gvEnetsList.Style.Add("display", "none")
        gvAssignedEventsList.Style.Add("display", "none")
        btnSave.Visible = False
        lblIsApproved.Visible = False
        chkApprove.Visible = False
        btnApprove.Visible = False
        lblAssignedEventsList.Visible = False
        lblEventsList.Visible = False
        LoadddlGroup()
        LoadddlUserByGroupCodeOrPrincipalBank("")
        LoadListBoxUserRolesByUserID(ddlUser.SelectedValue.ToString)

    End Sub
    Private Sub DeleteEventsAssignedToUser()
        Dim objICNotificationManagementColl As New ICNotificationManagementCollection
        objICNotificationManagementColl.es.Connection.CommandTimeout = 3600
        objICNotificationManagementColl = ICNotificationManagementController.GetAllNotificationAssignedToUser(ddlUser.SelectedValue.ToString)
        If objICNotificationManagementColl.Count > 0 Then
            For Each objICNotificationManagment As ICNotificationManagement In objICNotificationManagementColl
                ICNotificationManagementController.DeleteNotificationSettingForUSer(objICNotificationManagment, Me.UserId.ToString, Me.UserInfo.Username.ToString)
            Next
        End If

    End Sub
    
    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Page.Validate()
        If Page.IsValid Then
            Try
                Dim objICNotification As ICNotificationManagement
                Dim RecordCount As Integer = 0
                If chkApprove.Checked = False Then
                    UIUtilities.ShowDialog(Me, "Error", "Please select approve check box.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                For Each gvAssignedNotificationRow As GridDataItem In gvAssignedEventsList.Items
                    objICNotification = New ICNotificationManagement
                    objICNotification.es.Connection.CommandTimeout = 3600
                    objICNotification.LoadByPrimaryKey(gvAssignedNotificationRow.GetDataKeyValue("NotificationID"))

                    If RecordCount = 0 Then
                        If objICNotification.CreatedBy = Me.UserId Then
                            UIUtilities.ShowDialog(Me, "Error", "Notification events must be approved by user other than maker.", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    End If

                    ICNotificationManagementController.ApproveNotificationSettingForUSer(chkApprove.Checked, gvAssignedNotificationRow.GetDataKeyValue("NotificationID").ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)


                    RecordCount = 1
                Next
                UIUtilities.ShowDialog(Me, "Notification Management", "Notification events approved successfully", ICBO.IC.Dialogmessagetype.Success)
                LoadEventsListAssignedToUser(True)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub gvAssignedEventsList_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvAssignedEventsList.ItemCommand
        Try
            Dim objICNotificationManagement As New ICNotificationManagement
            
            objICNotificationManagement.es.Connection.CommandTimeout = 3600
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    If objICNotificationManagement.LoadByPrimaryKey(e.CommandArgument.ToString) Then
                        If objICNotificationManagement.IsApproved = True Then
                            UIUtilities.ShowDialog(Me, "Error", "Notification event is approved and can not delete record.", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        Else
                            ICNotificationManagementController.DeleteNotificationSettingForUSer(objICNotificationManagement, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            UIUtilities.ShowDialog(Me, "Notification Management", "Notification event deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                            LoadEventsList(True)
                            LoadEventsListAssignedToUser(True)
                            CheckMarkEventsListByAssignedEventsToUser()
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

  
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL(41))
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvEnetsList_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles gvEnetsList.ItemDataBound
        Dim chkProcessAll As CheckBox
        Dim chkVIASMS As CheckBox
        Dim chkVIAEmal As CheckBox
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkVIAEmal = New CheckBox
            chkVIASMS = New CheckBox

            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkVIAEmal = DirectCast(e.Item.Cells(3).FindControl("chkVIAEmail"), CheckBox)
            chkVIASMS = DirectCast(e.Item.Cells(2).FindControl("chkVIASMS"), CheckBox)

            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvEnetsList.MasterTableView.ClientID & "','0');"
            chkVIAEmal.Attributes("onclick") = "checkAllCheckboxes('" & chkVIAEmal.ClientID & "','" & gvEnetsList.MasterTableView.ClientID & "','3');"
            chkVIASMS.Attributes("onclick") = "checkAllCheckboxes('" & chkVIASMS.ClientID & "','" & gvEnetsList.MasterTableView.ClientID & "','2');"
        End If


        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim chkSelect, chkVIASMS2, chkVIAEMAIL2 As CheckBox
            chkSelect = New CheckBox
            chkVIASMS2 = New CheckBox
            chkVIAEMAIL2 = New CheckBox
            chkSelect = DirectCast(e.Item.Cells(0).FindControl("chkSelect"), CheckBox)
            chkVIASMS2 = DirectCast(e.Item.Cells(2).FindControl("chkSMS"), CheckBox)
            chkVIAEMAIL2 = DirectCast(e.Item.Cells(3).FindControl("chkEmail"), CheckBox)
            chkSelect.Attributes("onclick") = "CheckInGridView('" & chkSelect.ClientID & "','" & chkVIASMS2.ClientID & "','" & chkVIAEMAIL2.ClientID & "');"
            chkVIASMS2.Attributes("onclick") = "CheckInGridAll('" & chkSelect.ClientID & "','" & chkVIASMS2.ClientID & "','" & chkVIAEMAIL2.ClientID & "');"
            chkVIAEMAIL2.Attributes("onclick") = "CheckInGridAll('" & chkSelect.ClientID & "','" & chkVIASMS2.ClientID & "','" & chkVIAEMAIL2.ClientID & "');"
        End If


    End Sub

  
    Protected Sub gvEnetsList_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvEnetsList.NeedDataSource
        LoadEventsList(False)
    End Sub

    Protected Sub gvAssignedEventsList_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles gvAssignedEventsList.ItemDataBound
        Try
            If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
                Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)

                If item.GetDataKeyValue("IsApproved") = True Then
                    chkApprove.Checked = True
                Else
                    chkApprove.Checked = False
                End If
            

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAssignedEventsList_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAssignedEventsList.NeedDataSource
        LoadEventsListAssignedToUser(False)
    End Sub

  

End Class
