﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_AuditTrails_ViewAuditTrails
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Dim dtDa As DataTable
    Private htRights As Hashtable
    Protected Sub DesktopModules_AuditTrails_ViewAuditTrails_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Result As Boolean = False
        Dim userCtrl As New RoleController
        Dim str As String()
        Dim i As Integer
        If Not ViewState("htRights") Is Nothing Then
            htRights = DirectCast(ViewState("htRights"), Hashtable)
        End If
  btnSearch.Focus()
        If Page.IsPostBack = False Then
            ViewState("htRights") = Nothing
            GetPageAccessRights()
            ViewState("SortExp") = Nothing

            gvAuditTrail.Visible = False
            LoadDropDown()
            btnExport.Visible = False
        End If




    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Audit Trail")
            ViewState("htRights") = htRights

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadDropDown()

        Dim userCtrl As New RoleController
        Dim str As String()
        Dim i As Integer
        Dim atcoll As New ICAuditTrailCollection
        atcoll.es.Connection.CommandTimeout = 3600
        atcoll.Query.es.Distinct = True
        atcoll.Query.Select(atcoll.Query.AuditType)


        str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
        str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
       
     
            Dim cUser As New ICUser
            cUser.es.Connection.CommandTimeout = 3600
            cUser.LoadByPrimaryKey(Me.UserId)
            Dim dt As New DataTable
            'Dim drDt As DataRow
            Dim userID As New ArrayList

        If cUser.LoadByPrimaryKey(Me.UserId) Then
            atcoll.Query.es.Distinct = True
            atcoll.Query.Load()
        End If


            atcoll.Query.OrderBy(atcoll.Query.AuditType.Ascending)
            If atcoll.Query.Load Then
                Me.listtype.DataTextField = atcoll.Query.AuditType
                Me.listtype.DataValueField = atcoll.Query.AuditType

                Me.listtype.DataSource = atcoll
                Me.listtype.DataBind()

            End If
    End Sub

    Protected Sub listfilters_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles listfilters.SelectedIndexChanged

        If Me.listfilters.SelectedIndex = -1 Then
            gvAuditTrail.Visible = False
            btnExport.Visible = False
        Else
            gvAuditTrail.Visible = True
            btnExport.Visible = True
        End If


        If Me.listfilters.Items(0).Selected Then
            Me.pnldate.Visible = True
            LoadDropDown()
            gvAuditTrail.Visible = False
            btnExport.Visible = False
        Else
            Me.pnldate.Visible = False
        End If

        If Me.listfilters.Items(1).Selected Then
            Me.pnlact.Visible = True
            LoadDropDown()
            gvAuditTrail.Visible = False
            btnExport.Visible = False
        Else
            Me.pnlact.Visible = False
        End If

        If Me.listfilters.Items(2).Selected Then
            Me.pnltyp.Visible = True
            LoadDropDown()
            gvAuditTrail.Visible = False
            btnExport.Visible = False
        Else
            Me.pnltyp.Visible = False
        End If

        If Me.listfilters.Items(3).Selected Then
            Me.pnlid.Visible = True
            LoadDropDown()
            gvAuditTrail.Visible = False
            btnExport.Visible = False
        Else
            Me.pnlid.Visible = False
        End If

        If Me.listfilters.Items(4).Selected Then
            Me.pnlipaddress.Visible = True
            LoadDropDown()
            gvAuditTrail.Visible = False
            btnExport.Visible = False
        Else
            Me.pnlipaddress.Visible = False
        End If

        If Me.listfilters.Items(5).Selected Then
            Me.pnlusername.Visible = True
            LoadDropDown()
            gvAuditTrail.Visible = False
            btnExport.Visible = False
        Else
            Me.pnlusername.Visible = False
        End If
    End Sub


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Me.gvAuditTrail.CurrentPageIndex = 0

            search(True)







    End Sub

    'Private Sub search()

    Private Sub search(ByVal DoDataBind As Boolean)
        Dim IDManagementuserArray As New ArrayList
        Dim IDApprovaluserArray As New ArrayList
        Dim RoleManagementuserArray As New ArrayList
        Dim cRole As New RoleController
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim i As Integer


        Dim pagenumber As Integer = gvAuditTrail.CurrentPageIndex + 1
        Dim pagesize As Integer = gvAuditTrail.PageSize
        Dim rg As RadGrid = gvAuditTrail
        Dim dt1 As DataTable



        Try

            If Me.listfilters.SelectedIndex = -1 Then
                UIUtilities.ShowDialog(Me, "Audit Trail", "Please select a filter", ICBO.IC.Dialogmessagetype.Warning)
                Exit Sub
            End If


            Dim auditq As New ICAuditTrailQuery("qryAT")
            Dim icuser As New ICUserQuery("qryusr")

            'auditq.es.Connection.CommandTimeout = 3600

            If Not pagenumber = 0 Then
                auditq.Select(auditq.AuditID, auditq.AuditDate, auditq.AuditDate.As("Time"), auditq.AuditAction, auditq.AuditType, auditq.IPAddress, auditq.Username, auditq.RelatedID, icuser.DisplayName, auditq.ActionType.Coalesce("'-'"))
                auditq.LeftJoin(icuser).On(auditq.UserID = icuser.UserID)


                If Me.listfilters.Items(0).Selected Then
                    If Me.dtdatefrom.SelectedDate.HasValue And Me.dtdateto.SelectedDate.HasValue Then
                        auditq.Where(auditq.AuditDate.Date.Between(dtdatefrom.SelectedDate, dtdateto.SelectedDate))
                    Else
                        UIUtilities.ShowDialog(Me, "Audit Trail", "Please select dates", ICBO.IC.Dialogmessagetype.Warning)
                        Exit Sub
                    End If
                End If
                If Me.listfilters.Items(1).Selected Then
                    If Not Me.txtaction.Text = "" Then
                        auditq.Where(auditq.AuditAction.Like("%" & Me.txtaction.Text & "%"))
                    Else
                        UIUtilities.ShowDialog(Me, "Audit Trail", "Please enter action", ICBO.IC.Dialogmessagetype.Warning)
                        Exit Sub
                    End If
                End If
                If Me.listfilters.Items(2).Selected Then
                    auditq.Where(auditq.AuditType = Me.listtype.SelectedValue.ToString())
                End If
                If Me.listfilters.Items(3).Selected Then
                    If Not Me.txtid.Text = "" Then
                        auditq.Where(auditq.RelatedID = Me.txtid.Text.ToString())
                    Else
                        UIUtilities.ShowDialog(Me, "Audit Trail", "Please enter id", ICBO.IC.Dialogmessagetype.Warning)
                        Exit Sub
                    End If
                End If
                If Me.listfilters.Items(4).Selected Then
                    If Not Me.txtipaddress.Text = "" Then
                        auditq.Where(auditq.IPAddress = Me.txtipaddress.TextWithLiterals.ToString())
                    Else
                        UIUtilities.ShowDialog(Me, "Audit Trail", "Please enter ip address", ICBO.IC.Dialogmessagetype.Warning)
                        Exit Sub
                    End If
                End If
                If Me.listfilters.Items(5).Selected Then
                    If Not Me.txtusername.Text = "" Then
                        auditq.Where(auditq.Username = Me.txtusername.Text.ToString())

                    Else
                        UIUtilities.ShowDialog(Me, "Audit Trail", "Please enter user name", ICBO.IC.Dialogmessagetype.Warning)
                        Exit Sub
                    End If
                End If

               




              

               
                'Dim orderbyitems(1) As EntitySpaces.Interfaces.esOrderByItem
                'orderbyitems(0) = auditq.Query.AuditID.Ascending
                'orderbyitems(1) = auditq.Query.AuditDate.Ascending
                'orderbyitems(2) = auditq.Query.RelatedID.Ascending
                auditq.OrderBy(auditq.AuditID.Descending)



                dt1 = auditq.LoadDataTable
                rg.DataSource = dt1

                If DoDataBind Then
                    rg.DataBind()
                End If

             
            Else

                auditq.es.PageNumber = 1
                auditq.es.PageSize = pagesize
                dt1 = auditq.LoadDataTable
                rg.DataSource = dt1

                If DoDataBind Then
                    rg.DataBind()
                End If
              
            End If

            If gvAuditTrail.Items.Count > 0 Then
                gvAuditTrail.Visible = True
                btnExport.Visible = CBool(htRights("Export"))

            Else
                gvAuditTrail.Visible = False
                btnExport.Visible = False
            End If




            'If auditq.Load Then
            '    Me.gvAuditTrail.DataSource = auditq
            '    Me.gvAuditTrail.DataBind()
            '    btnExport.Visible = True

            'Else
            '    Me.gvAuditTrail.DataSource = Nothing
            '    Me.gvAuditTrail.DataBind()
            '    btnExport.Visible = False
            'End If


            'If Not pagenumber = 0 Then
            '    auditq.Select(icuser.DisplayName)
            '    auditq.InnerJoin(icuser).On(auditq.UserID = icuser.UserID)
            '    dt1 = auditq.LoadDataTable
            '    rg.DataSource = dt1

            '    If DoDataBind Then
            '        rg.DataBind()
            '    End If

            '    btnExport.Visible = True

            'Else

            '    auditq.es.PageNumber = 1
            '    auditq.es.PageSize = pagesize
            '    dt1 = auditq.LoadDataTable
            '    rg.DataSource = dt1

            '    If DoDataBind Then
            '        rg.DataBind()
            '    End If

            'End If


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click

        '   Me.gvAuditTrail.AllowPaging = False
        'search(True)
        '  GridViewExportUtil.Export("AuditTrail" & Date.Now.ToString("dd-MMM-yyyy") & ".xls", Me.gvAuditTrail)
        '   Me.gvAuditTrail.AllowPaging = True
        '  search()






        'Dim pagenumber As Integer = gvAuditTrail.CurrentPageIndex + 1
        'Dim pagesize As Integer = gvAuditTrail.PageSize

        'search(True)
        gvAuditTrail.ExportSettings.ExportOnlyData = True
            gvAuditTrail.MasterTableView.AllowPaging = False
            gvAuditTrail.ExportSettings.IgnorePaging = True
            gvAuditTrail.PageSize = 500
            '    gvAuditTrail.PageSize = gvAuditTrail.Items.Count + 1

            gvAuditTrail.Rebind()


            gvAuditTrail.ExportSettings.OpenInNewWindow = True
            gvAuditTrail.ExportSettings.FileName = "Al Baraka Audit Trail " & Date.Now.ToString("dd-MMM-yyyy")
            gvAuditTrail.MasterTableView.ExportToExcel()


            ' gvAuditTrail.MasterTableView.AllowPaging = True
            'gvAuditTrail.CurrentPageIndex = pagenumber
            'gvAuditTrail.PageSize = pagesize
            ' gvAuditTrail.Rebind()
            search(True)





        'gvAuditTrail.ExportSettings.ExportOnlyData = True
        'gvAuditTrail.ExportSettings.IgnorePaging = False
        'gvAuditTrail.ExportSettings.OpenInNewWindow = False
        'gvAuditTrail.MasterTableView.AllowPaging = False
        'gvAuditTrail.MasterTableView.ExportToExcel()


    End Sub

    Protected Sub gvAuditTrail_ItemCreated(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuditTrail.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAuditTrail.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAuditTrail_PageIndexChanged(sender As Object, e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvAuditTrail.PageIndexChanged
        gvAuditTrail.CurrentPageIndex = e.NewPageIndex
    End Sub

    Protected Sub gvAuditTrail_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAuditTrail.NeedDataSource
        '  search()
        If Page.IsPostBack = False Then
            Exit Sub
        Else
            search(False)
        End If


    End Sub

End Class


