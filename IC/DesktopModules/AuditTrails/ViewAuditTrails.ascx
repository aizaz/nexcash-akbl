﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewAuditTrails.ascx.vb"
    Inherits="DesktopModules_AuditTrails_ViewAuditTrails" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<table id="Table7" align="left" border="0" cellpadding="0" cellspacing="0" class="txmaingrey" width="100%">
<tr style="width:100%">
<td align="left" valign="top" style="width:100%">
<asp:Panel ID="pnlAuditTrail" DefaultButton="btnSearch" runat="server" Width="100%">
<table>
    <tr>
        <td align="left" width="30%" class="style2">
            <asp:Label ID="lblAuditTrail" runat="server" CssClass="headingblue" Text="Audit Trail"></asp:Label>
        </td>
    </tr>
    <tr>
        <td style="height: 17px" align="center">
            <table>
                <tr>
                    <td align="left">
                        <asp:CheckBoxList ID="listfilters" runat="server" AutoPostBack="True" RepeatColumns="2"
                            Width="323px">
                            <asp:ListItem>By Date</asp:ListItem>
                            <asp:ListItem>By Action</asp:ListItem>
                            <asp:ListItem>By Type</asp:ListItem>
                            <asp:ListItem>By ID</asp:ListItem>
                            <asp:ListItem>By IP Address</asp:ListItem>
                            <asp:ListItem>By Username</asp:ListItem>
                        </asp:CheckBoxList>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" style="height: 17px">
            &nbsp;&nbsp;
        </td>
    </tr>
    <tr>
        <td align="center" style="height: 17px">
            <asp:Panel ID="pnldate" runat="server" Visible="False" HorizontalAlign="Center" Width="500px">
                <table style="width: 500px">
                    <tr>
                        <td>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 200px">
                            <asp:Label ID="Label19" runat="server" Text="Date"></asp:Label>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label20" runat="server" Text="From"></asp:Label>
                        </td>
                        <td align="left">
                            <telerik:RadDatePicker ID="dtdatefrom" runat="server" Width="200px" Culture="English (United Kingdom)" AutoPostBack="true">
                                <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                </Calendar>
                                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadDatePicker>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            &nbsp;&nbsp;
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label31" runat="server" Text="To"></asp:Label>
                        </td>
                        <td align="left">
                            <telerik:RadDatePicker ID="dtdateto" runat="server" Culture="English (United Kingdom)" AutoPostBack="true"
                                Width="200px">
                                <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                </Calendar>
                                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadDatePicker>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnlact" runat="server" Visible="False" HorizontalAlign="Center" Width="500px">
                <table width="500">
                    <tr>
                        <td align="left" style="width: 200px">
                            &nbsp;
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 200px">
                            <asp:Label ID="Label16" runat="server" Text="Action"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtaction" runat="server" CssClass="frinputbx"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnltyp" runat="server" Visible="False" HorizontalAlign="Center" Width="500px">
                <table style="width: 500px">
                    <tr>
                        <td align="left" style="width: 200px">
                            &nbsp;
                        </td>
                        <td align="left">
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label17" runat="server" Text="Type"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="listtype" runat="server" CssClass="frinputbx" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnlid" runat="server" Visible="False" HorizontalAlign="Center" Width="500px">
                <table width="500">
                    <tr>
                        <td align="left" style="width: 200px">
                            &nbsp;
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 200px">
                            <asp:Label ID="Label32" runat="server" Text="ID"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtid" runat="server" CssClass="frinputbx"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnlipaddress" runat="server" Visible="False" HorizontalAlign="Center"
                Width="500px">
                <table width="500">
                    <tr>
                        <td align="left">
                            &nbsp;
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 200px">
                            <asp:Label ID="Label33" runat="server" Text="IP Address"></asp:Label>
                        </td>
                        <td align="left">
                            <telerik:RadMaskedTextBox ID="txtipaddress" runat="server" CssClass="inline_override"
                                Mask="###.###.###.###" PromptChar=" " TextWithLiterals="..." Width="125px">
                            </telerik:RadMaskedTextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnlusername" runat="server" Visible="False" HorizontalAlign="Center"
                Width="500px">
                <table width="500">
                    <tr>
                        <td align="left" style="width: 200px">
                            &nbsp;
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 200px">
                            <asp:Label ID="Label34" runat="server" Text="Username"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtusername" runat="server" CssClass="frinputbx"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
        </td>
    </tr>
    <tr>
        <td align="left" style="\: :;">
            <table style="width: 100%;">
                <tr>
                    <td width="43%">
                        &nbsp;
                    </td>
                    <td>
                        <asp:Button ID="btnSearch" runat="server" CssClass="btn" Text="Search" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>


</table>


</asp:Panel>
</td>
</tr>
    <tr>
        <td style="height: 17px">
            <telerik:RadGrid ID="gvAuditTrail" runat="server" AllowPaging="True" AllowSorting="True"
                AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100"
                CssClass="RadGrid">
                <ClientSettings>
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                </ClientSettings>
                <AlternatingItemStyle CssClass="rgAltRow" />
                <ExportSettings HideStructureColumns="true">
                </ExportSettings>
                <MasterTableView TableLayout="Fixed">
                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn DataField="AuditID" HeaderText="Audit ID" SortExpression="AuditID">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AuditDate" HeaderText="Date" SortExpression="AuditDate"
                            DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="false">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Time" HeaderText="Time" SortExpression="Time"
                            DataFormatString="{0:hh:mm:ss}" HtmlEncode="false">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AuditAction" HeaderText="Action" SortExpression="AuditAction"
                            HtmlEncode="false">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20%" />
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AuditType" HeaderText="Type" SortExpression="AuditType">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ActionType" HeaderText="Action Type" SortExpression="ActionType">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="RelatedID" HeaderText="Related ID" SortExpression="RelatedID">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="IPAddress" HeaderText="IP Address" SortExpression="IPAddress">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="username" HeaderText="User Name" SortExpression="username">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="DisplayName" HeaderText="Display Name" SortExpression="DisplayName">
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                    <PagerStyle AlwaysVisible="True" />
                </MasterTableView>
                <ItemStyle CssClass="rgRow" />
                <PagerStyle AlwaysVisible="True" />
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
        </td>
    </tr>
    <tr>
        <td align="left">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td class="txmaingrey" align="center">
            <%-- &nbsp;&nbsp;&nbsp;<telerik:messagepanel id="MessagePanel1" runat="server" />
            <telerik:loader id="Loader1" runat="server" />--%>
            <asp:Button ID="btnExport" runat="server" CssClass="btn" Text="Export" Visible="False" />
        </td>
    </tr>
    <tr>
        <td align="left">
            &nbsp;
        </td>
    </tr>
</table>
