﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports EntitySpaces.Core
Partial Class DesktopModules_StaleInstructionsQueue_StaleInstructionsQueue
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrICAssignUserRolsID As New ArrayList
    Private ArrICAssignedOfficeID As New ArrayList
    Private ArrICAssignedStatus As New ArrayList
    Private ArrICAssignedPaymentModes As New ArrayList
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()
            lblMessageForQueue.Style.Add("display", "none")
            If Page.IsPostBack = False Then
                lblMessageForQueue.Text = ICUtilities.GetSettingValue("ErrorMessage")
                btnRevalidate.Attributes.Item("onclick") = "if (Page_ClientValidate('Remarks')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnRevalidate, Nothing).ToString() & " } "
                btnCancelInstructions.Attributes.Item("onclick") = "if (Page_ClientValidate('Remarks')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";document.getElementById('<%=lblMessageForQueue.ClientID%>').style.display = 'block';this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnCancelInstructions, Nothing).ToString() & " } "

                DesignDts()
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                SetArraListAssignedPaymentModes()
                'SetArraListAssignedOfficeIDs()
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
                rbtnlstDateSelection.SelectedValue = "Creation Date"
                RefreshPage()
               
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub RefreshPage()
        LoadddlCompany()
        LoadddlAccountPaymentNatureByUserID()
        LoadddlProductTypeByAccountPaymentNature()
        ClearAllTextBoxes()
        FillDesignedDtByAPNatureByAssignedRole()

        Dim dtPageLoad As New DataTable
        dtPageLoad = DirectCast(ViewState("AccountNumber"), DataTable)
        If dtPageLoad.Rows.Count > 0 Then
            LoadddlBatcNumber()
            LoadgvAccountPaymentNatureProductType(True)

        Else
            UIUtilities.ShowDialog(Me, "Error", "You do not have access to any transactional data. Please contact Al Baraka Administrator.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
            Exit Sub
        End If
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Stale Instructions Queue")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Stale Instructions Queue") = True Then
                    ArrICAssignUserRolsID.Add(RoleID.ToString)
                End If
            Next
        End If
    End Sub
    Private Sub SetArraListAssignedOfficeIDs()
        Dim dt As New DataTable
        Dim objICUser As New ICUser
        ArrICAssignedOfficeID.Clear()
        dt = ICUserRolesPaymentNatureController.GetAllTaggedLocationsWithUserByUSerIDAndRoleID(Me.UserId.ToString, ArrICAssignUserRolsID)
        If dt.Rows.Count > 0 Then
            objICUser.LoadByPrimaryKey(Me.UserId)
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("OfficeID").ToString
                ArrICAssignedOfficeID.Add(RoleID.ToString)
            Next
            ArrICAssignedOfficeID.Add(objICUser.OfficeCode)
        End If
    End Sub
    Private Sub DesignDts()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim dtOfficeID As New DataTable
        dtAccountNumber.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("BranchCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("Currency", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("PaymentNatureCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("OfficeID", GetType(System.String)))
        ViewState("AccountNumber") = dtAccountNumber
    End Sub
    Private Sub FillDesignedDtByAPNatureByAssignedRole()
         Dim dtAssignedAPNatureOfficeID As New DataTable
        dtAssignedAPNatureOfficeID.Rows.Clear()
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForAPNature(Me.UserId.ToString, ArrICAssignUserRolsID)
        ViewState("AccountNumber") = Nothing
        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID
       
    End Sub
    Private Sub SetArraListAssignedStatus()
        ArrICAssignedStatus.Clear()
        ArrICAssignedStatus.Add(31)
    End Sub
    Private Sub SetArraListAssignedPaymentModes()
        ArrICAssignedPaymentModes.Clear()
        If CBool(htRights("Cheque Instructions")) = True Then
            ArrICAssignedPaymentModes.Add("Cheque")
        End If
        If CBool(htRights("DD Instructions")) = True Then
            ArrICAssignedPaymentModes.Add("DD")
        End If
        If CBool(htRights("PO Instructions")) = True Then
            ArrICAssignedPaymentModes.Add("PO")
        End If
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, "", ArrICAssignUserRolsID)
            ddlCompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlCompany.Items.Clear()
                    ddlCompany.Items.Add(lit)
                    lit.Selected = True
                    ddlCompany.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
                ddlCompany.DataSource = dt
                ddlCompany.DataTextField = "CompanyName"
                ddlCompany.DataValueField = "CompanyCode"
                ddlCompany.DataBind()
                ddlCompany.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccountPaymentNatureByUserID()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlAcccountPaymentNature.Items.Clear()
            ddlAcccountPaymentNature.Items.Add(lit)
            lit.Selected = True
            ddlAcccountPaymentNature.AppendDataBoundItems = True
            ddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForDropDown(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
            ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
            ddlAcccountPaymentNature.DataBind()
           
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlBatcNumber()
        Try

            Dim dtAccountNumber As New DataTable

           
            dtAccountNumber = ViewState("AccountNumber")
            SetArraListAssignedPaymentModes()
            SetArraListAssignedStatus()
            Dim lit As New ListItem
            lit.Text = "-- All --"
            lit.Value = "0"
            ddlBatch.Items.Clear()
            ddlBatch.Items.Add(lit)
            lit.Selected = True
            ddlBatch.AppendDataBoundItems = True
            ddlBatch.DataSource = ICInstructionController.GetAllTaggedBatchNumbersByCompanyAccountPaymentNature(ArrICAssignedPaymentModes, dtAccountNumber, ddlCompany.SelectedValue.ToString, ArrICAssignedStatus, Me.UserId.ToString)
            ddlBatch.DataTextField = "FileBatchNoName"
            ddlBatch.DataValueField = "FileBatchNo"
            ddlBatch.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Public Shared Function GetAllTaggedBatchNumbersByCompanyAccountPaymentNature(ByVal AssignedPaymentModes As ArrayList, ByVal dtAccountNo As DataTable, ByVal CompanyCode As String, ByVal ArrayListStatus As ArrayList, ByVal UserIDs As String) As DataTable

        Dim StrQuery As String = Nothing
        Dim WhereClause As String = Nothing
        WhereClause = " Where "
        Dim dt As New DataTable
        StrQuery = "select distinct(IC_Instruction.FileBatchNo), "
        StrQuery += "case ic_instruction.FileBatchNo when 'SI' "
        StrQuery += " then 'SI' else ic_instruction.FileBatchNo + '-' + IC_Files.OriginalFileName end as FileBatchNoName from IC_Instruction  "
        StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID  Where IC_Instruction.InstructionID IN "
        StrQuery += "(Select ins2.InstructionID from IC_Instruction as ins2 "
        If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
            WhereClause += " And ins2.PaymentMode in ("
            For i = 0 To AssignedPaymentModes.Count - 1
                WhereClause += "'" & AssignedPaymentModes(i) & "',"
            Next
            WhereClause = WhereClause.Remove(WhereClause.Length - 1)
            WhereClause += ")"
        End If
        If ArrayListStatus.Count > 0 Then
            WhereClause += " And ins2.Status in ( "
            For i = 0 To ArrayListStatus.Count - 1
                WhereClause += ArrayListStatus(i) & ","
            Next
            WhereClause = WhereClause.Remove(WhereClause.Length - 1)
            WhereClause += ")"
        End If
        If CompanyCode.ToString <> "0" Then
            WhereClause += " And ins2.CompanyCode=" & CompanyCode
        Else
            WhereClause += " And ins2.CompanyCode=0"
        End If
        If dtAccountNo.Rows.Count > 0 Then
            WhereClause += " AND ("
            WhereClause += " ins2.ClientAccountNo +" & "'-'" & "+ ins2.ClientAccountBranchCode +" & "'-'" & "+ ins2.ClientAccountCurrency +" & "'-'" & "+ ins2.PaymentNatureCode IN ("
            For Each dr As DataRow In dtAccountNo.Rows
                WhereClause += "'" & dr("AccountPaymentNature").ToString.Split("-")(0) & "-" & dr("AccountPaymentNature").ToString.Split("-")(1) & "-" & dr("AccountPaymentNature").ToString.Split("-")(2) & "-" & dr("AccountPaymentNature").ToString.Split("-")(3) & "',"
            Next
            WhereClause = WhereClause.Remove(WhereClause.Length - 1, 1)
            WhereClause += ")"
        End If
        WhereClause += ")"
        If WhereClause.Contains(" Where  And") Then
            WhereClause = WhereClause.Replace(" Where  And", " Where ")
        End If
        StrQuery += WhereClause
        StrQuery += " ) Order By FileBatchNoName Asc"

        Dim util As New esUtility
        dt = util.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery)
        Return dt

    End Function
    Private Sub LoadddlProductTypeByAccountPaymentNature()
        Try
            Dim lit As New ListItem
            Dim objICAPNature As New ICAccountsPaymentNature
            objICAPNature.es.Connection.CommandTimeout = 3600
            ddlProductType.Items.Clear()
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlProductType.Items.Add(lit)
            ddlProductType.AppendDataBoundItems = True
            If ddlAcccountPaymentNature.SelectedValue.ToString <> "0" Then
                If objICAPNature.LoadByPrimaryKey(ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)) Then
                    ddlProductType.DataSource = ICAccountPaymentNatureProductTypeController.GetAllProductTypesByAccountAndPaymentNature(objICAPNature)
                    ddlProductType.DataTextField = "ProductTypeName"
                    ddlProductType.DataValueField = "ProductTypeCode"
                    ddlProductType.DataBind()
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadgvAccountPaymentNatureProductType(ByVal DoDataBind As Boolean)
        Try
            
            Dim DateType, AccountNumber, BranchCode, Currency, PaymentNatureCode, CompanyCode, ProductTypeCode, InstrumentNo, InstructionNo, BatchCode, ReferenceNo As String
            Dim FromDate, ToDate As String
            Dim Amount As Double = 0
            Dim dtAccountNumber As New DataTable
            dtAccountNumber = ViewState("AccountNumber")
            DateType = ""
            AccountNumber = ""
            BranchCode = ""
            Currency = ""
            Currency = ""
            PaymentNatureCode = ""
            CompanyCode = "0"
            ProductTypeCode = ""
            InstructionNo = ""
            InstrumentNo = ""
            BatchCode = ""
            ReferenceNo = ""
            FromDate = Nothing
            ToDate = Nothing
            
            If txtAmount.Text <> "" And txtAmount.Text <> "Enter Amount" Then
                Amount = CDbl(txtAmount.Text)
                If Amount < 1 Then
                    UIUtilities.ShowDialog(Me, "Instruction Tracking", "Amount must be greater than zero.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            End If

            If rbtnlstDateSelection.SelectedValue = "Creation Date" Then
                DateType = "Creation Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Value Date" Then
                DateType = "Value Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Stale Date" Then
                DateType = "Stale Date"
            End If
            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            End If
            If txtReferenceNo.Text.ToString <> "" And txtReferenceNo.Text.ToString <> "Enter Reference No." Then
                ReferenceNo = txtReferenceNo.Text.ToString
            End If
            If ddlProductType.SelectedValue.ToString <> "0" Then
                ProductTypeCode = ddlProductType.SelectedValue.ToString
            End If
            If txtInstrumentNo.Text.ToString <> "" And txtInstrumentNo.Text.ToString <> "Enter Instrument No." Then
                InstrumentNo = txtInstrumentNo.Text.ToString
            End If
            If txtInstructionNo.Text.ToString <> "" And txtInstructionNo.Text.ToString <> "Enter Instruction No." Then
                InstructionNo = txtInstructionNo.Text.ToString
            End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                FromDate = raddpCreationFromDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If Not raddpCreationToDate.SelectedDate Is Nothing Then
                ToDate = raddpCreationToDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If ddlBatch.SelectedValue <> "0" Then
                BatchCode = ddlBatch.SelectedValue.ToString
            End If
            SetArraListAssignedStatus()

            SetArraListAssignedPaymentModes()
            ICInstructionController.GetAllInstructionsForRadGridByAccountAndPaymentNature(DateType, FromDate, ToDate, dtAccountNumber, ProductTypeCode, InstructionNo, InstrumentNo, ReferenceNo, Me.gvAuthorization.CurrentPageIndex + 1, Me.gvAuthorization.PageSize, Me.gvAuthorization, DoDataBind, Me.UserId.ToString, Amount, ddlAmountOp.SelectedValue.ToString, CompanyCode, "", "31", BatchCode)
            If gvAuthorization.Items.Count > 0 Then
                gvAuthorization.Visible = True
                lblNRF.Visible = False
                btnRevalidate.Visible = CBool(htRights("Re-Validate"))
                btnCancelInstructions.Visible = CBool(htRights("Cancel"))
                'SetJavaScript()
            Else
                lblNRF.Visible = True
                gvAuthorization.Visible = False
                btnRevalidate.Visible = False
                btnCancelInstructions.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Public Shared Sub GetAllInstructionsForRadGridByAccountAndPaymentNature(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal RefeRenceNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal UsersID As String, ByVal Amount As Double, ByVal AmountOperator As String, ByVal CompanyCode As String, ByVal GroupCode As String, ByVal Status As String)
        Dim dt As DataTable
        Dim StrQuery As String = Nothing
        Dim WhereClasue As String = Nothing
        WhereClasue = " Where "
        StrQuery = "Select InstructionID,convert(date,IC_Instruction.CreateDate) as CreateDate,convert(date,ValueDate) as ValueDate,Amount,StatusName, "
        StrQuery += "isnull(BeneficiaryName,'-') as BeneficiaryName,isnull(BeneficiaryAddress,'-') as BeneficiaryAddress,PaymentMode,ISNULL(InstrumentNo,'-') as InstrumentNo,"
        StrQuery += "ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,ISNULL(IC_Office.OfficeCode + '-' + IC_Office.OfficeName,'-') as OfficeName,"
        StrQuery += "FileBatchNo,ISNULL(ReferenceNo,'-') as ReferenceNo,IC_Bank.BankName,isnull(PreviousPrintLocationCode,'-') as PreviousPrintLocationCode,"
        StrQuery += "ISNULL(PreviousPrintLocationName,'-') as PreviousPrintLocationName,CONVERT(date,VerificationDate) as VerificationDate,"
        StrQuery += "CONVERT(date,ApprovalDate) as ApprovalDate,CONVERT(date,StaleDate) as staledate,CONVERT(date,CancellationDate) as CancellationDate,"
        StrQuery += "isnull(BeneficiaryAccountNo,'-') as BeneficiaryAccountNo,isnull(BeneAccountBranchCode,'-') as BeneAccountBranchCode,"
        StrQuery += "isnull(BeneAccountCurrency,'-') as BeneAccountCurrency,LastPrintDate,BeneficiaryAccountNo,BeneAccountBranchCode,BeneAccountCurrency,"
        StrQuery += "CONVERT(date,RevalidationDate) as revalidationdate,IC_Instruction.ProductTypeCode,IC_Instruction.CompanyCode,status,IC_Company.CompanyName,"
        StrQuery += "IC_ProductType.ProductTypeName,"
        StrQuery += "case isnull(convert(varchar,IsAmendmentComplete),CONVERT(varchar,'false')) "
        StrQuery += "when 'false' then "
        StrQuery += "CONVERT(varchar,'false') "
        StrQuery += "else "
        StrQuery += "IsAmendmentComplete "
        StrQuery += " end "
        StrQuery += "as IsAmendmentComplete "
        StrQuery += "from IC_Instruction "
        StrQuery += "left join IC_Office on IC_Instruction.PrintLocationCode=IC_Office.OfficeID "
        StrQuery += "left join IC_Bank on IC_Instruction.BeneficiaryBankCode=IC_Bank.BankCode "
        StrQuery += "left join IC_Company on IC_Instruction.CompanyCode=IC_Company.CompanyCode "
        StrQuery += "left join IC_ProductType on IC_Instruction.ProductTypeCode=IC_ProductType.ProductTypeCode "
        StrQuery += "left join IC_InstructionStatus on IC_Instruction.Status=IC_InstructionStatus.StatusID "
        If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And Amount = 0 Then
            If DateType.ToString = "Creation Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.CreateDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.CreateDate) <= CONVERT(date,'" & ToDate & "')"
                End If
            ElseIf DateType.ToString = "Value Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.ValueDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.ValueDate) <= CONVERT(date,'" & ToDate & "')"
                End If
            ElseIf DateType.ToString = "Approval Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) between CONVERT(date,'" & FromDate & ")' and CONVERT(date,'" & ToDate & "')"
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) <= CONVERT(date,'" & ToDate & "')"
                End If
            ElseIf DateType.ToString = "Stale Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.StaleDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.StaleDate) <= CONVERT(date,'" & ToDate & "')"
                End If
            ElseIf DateType.ToString = "Cancellation Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) <= CONVERT(date,'" & ToDate & "')"
                End If
            ElseIf DateType.ToString = "Last Print Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) <= CONVERT(date,'" & ToDate & "')"
                End If
            ElseIf DateType.ToString = "Revalidation Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) <= CONVERT(date,'" & ToDate & "')"
                End If
            ElseIf DateType.ToString = "Verfication Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) between CONVERT(date,'" & FromDate & "') and CONVERT(date,'" & ToDate & "')"
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) <= CONVERT(date,'" & ToDate & "')"
                End If
            End If
            If ProductTypeCode.ToString <> "" Then
                WhereClasue += " AND IC_Instruction.ProductTypeCode='" & ProductTypeCode & "'"
            End If
            If CompanyCode.ToString <> "0" Then
                WhereClasue += " AND IC_Instruction.CompanyCode='" & CompanyCode & "'"
            End If
            If Status.ToString <> "" Then
                WhereClasue += " AND IC_Instruction.Status=" & Status & " "
            End If
        Else
            If InstructionNo.ToString <> "" Then
                WhereClasue += " AND InstructionID=" & InstructionNo & ""
            End If
            If InstrumentNo.ToString <> "" Then
                WhereClasue += " AND InstrumentNo='" & InstrumentNo & "'"
            End If

            If CompanyCode.ToString <> "0" Then
                WhereClasue += " AND IC_Instruction.CompanyCode='" & CompanyCode & "'"
            End If
            If Not Amount = -1 Then
                If AmountOperator.ToString = "=" Then
                    WhereClasue += " AND Amount = " & Amount & ""
                ElseIf AmountOperator.ToString = "<" Then
                    WhereClasue += " AND Amount < " & Amount & ""
                ElseIf AmountOperator.ToString = ">" Then
                    WhereClasue += " AND Amount > " & Amount & ""
                ElseIf AmountOperator.ToString = "<=" Then
                    WhereClasue += " AND Amount <= " & Amount & ""
                ElseIf AmountOperator.ToString = ">=" Then
                    WhereClasue += " AND Amount >= " & Amount & ""
                ElseIf AmountOperator.ToString = "<>" Then
                    WhereClasue += " AND Amount <> " & Amount & ""
                End If
            End If
        End If
        If AccountNumber.Rows.Count > 0 Then
            WhereClasue += " And "
            WhereClasue += "ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + PaymentNatureCode  IN ("
            For Each dr As DataRow In AccountNumber.Rows
                WhereClasue += "'" & dr("AccountPaymentNature").ToString.Split("-")(0) & "-" & dr("AccountPaymentNature").ToString.Split("-")(1) & "-" & dr("AccountPaymentNature").ToString.Split("-")(2) & "-" & dr("AccountPaymentNature").ToString.Split("-")(3) & "',"
            Next
            WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
            WhereClasue += ")"
        End If


        If WhereClasue.Contains(" Where  AND ") = True Then
            WhereClasue = WhereClasue.Replace(" Where  AND ", "Where ")
        End If
        StrQuery += WhereClasue
        StrQuery += " Order By InstructionID Desc"
        Dim utill As New esUtility()
        dt = New DataTable
        dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery)
        If Not CurrentPage = 0 Then

            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        Else
            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        End If

    End Sub
  
    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            'SetArraListRoleID()

            LoadddlAccountPaymentNatureByUserID()
            LoadddlBatcNumber()
            LoadddlProductTypeByAccountPaymentNature()

            LoadgvAccountPaymentNatureProductType(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlAcccountPaymentNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcccountPaymentNature.SelectedIndexChanged
        Try
            If ddlAcccountPaymentNature.SelectedValue.ToString = "0" Then
                SetArraListRoleID()

                FillDesignedDtByAPNatureByAssignedRole()
                LoadddlBatcNumber()
                LoadddlProductTypeByAccountPaymentNature()


                LoadgvAccountPaymentNatureProductType(True)
            Else
                FillDesignedDtByAPNatureByAssignedRole()
                SetViewStateDtOnAPNatureIndexChnage()
                LoadddlBatcNumber()
                LoadddlProductTypeByAccountPaymentNature()


                LoadgvAccountPaymentNatureProductType(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetViewStateDtOnAPNatureIndexChnage()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim AccountNumber, BranchCode, Currency, PaymentNatureCode As String
        Dim ArrayListOfficeID As New ArrayList
        Dim drAccountNo As DataRow
        AccountNumber = Nothing
        BranchCode = Nothing
        Currency = Nothing
        PaymentNatureCode = Nothing
        dtAccountNumber = ViewState("AccountNumber")
        ViewState("AccountNumber") = Nothing
        AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0)
        BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1)
        Currency = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2)
        PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)
        dtAccountNumber.Rows.Clear()

        drAccountNo = dtAccountNumber.NewRow()
        drAccountNo("AccountAndPaymentNature") = AccountNumber & "-" & PaymentNatureCode
        drAccountNo("AccountPaymentNature") = AccountNumber & "-" & BranchCode & "-" & Currency & "-" & PaymentNatureCode
        dtAccountNumber.Rows.Add(drAccountNo)

        'dtAccountNumber.Columns.Add(New DataColumn("DropDown", GetType(System.String)))

        ViewState("AccountNumber") = dtAccountNumber
    End Sub

    Protected Sub ddlBatch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBatch.SelectedIndexChanged
        Try
            SetArraListRoleID()
            LoadddlProductTypeByAccountPaymentNature()

            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged
        Try
            SetArraListRoleID()



            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ClearAllTextBoxes()
        txtRemarks.Text = ""
        txtInstructionNo.Text = ""
        txtInstrumentNo.Text = ""
        txtReferenceNo.Text = ""
        txtAmount.Text = ""
        ddlAmountOp.SelectedValue = "="
    End Sub
    Private Function CheckgvInstructionAuthentication() As Boolean
        Try

            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVInstruction In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVInstruction.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Page.Validate()
        If Page.IsValid Then
            Try
                LoadgvAccountPaymentNatureProductType(True)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub gvAuthorization_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAuthorization.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAuthorization_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','0');"
        End If
        Dim imgBtn As LinkButton
        Dim AppPath As String = "" ' DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            imgBtn = New LinkButton
            imgBtn = DirectCast(item.Cells(1).FindControl("lbInstructionID"), LinkButton)
            imgBtn.OnClientClick = "showurldialog('Instruction Details',' " & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & item.GetDataKeyValue("InstructionID").ToString().EncryptString() & "' ,true,700,650);return false;"
            If CBool(htRights("View Instruction Details")) = True Then
                imgBtn.Enabled = True

            End If
            If item.GetDataKeyValue("IsAmendmentComplete") = True Then

                gvAuthorization.AlternatingItemStyle.CssClass = "GridItemComplete"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItemComplete"


            Else
                gvAuthorization.AlternatingItemStyle.CssClass = "GridItem"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItem"


            End If
        End If
    End Sub

    Protected Sub gvAuthorization_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAuthorization.NeedDataSource
        Try
            ClearAllTextBoxes()
            SetArraListRoleID()
            LoadgvAccountPaymentNatureProductType(False)
            'SetJavaScript()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Function CheckgvClientRoleCheckedForProcessAll() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnCancelInstructions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelInstructions.Click
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim ArryListInstructionId As New ArrayList
                Dim ArraListStatus As New ArrayList
                Dim StrInstCount As String = Nothing
                Dim StrArr As String() = Nothing
                ArryListInstructionId.Clear()
                ArraListStatus.Clear()
                Dim dtInstructionInfo As New DataTable
                Dim dtPODD As New DataTable
                Dim objICInstruction As ICInstruction
                Dim Remarks As String = Nothing
                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Stale Instruction Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If txtRemarks.Text <> "" Then
                    Remarks = txtRemarks.Text
                End If

                Dim dt As New DataTable
                dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Message", GetType(System.String)))
                For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        objICInstruction = New ICInstruction
                        objICInstruction.LoadByPrimaryKey(gvVerificationRow.GetDataKeyValue("InstructionID"))
                        If (objICInstruction.PaymentMode = "PO" Or objICInstruction.PaymentMode = "DD") And objICInstruction.Status = "31" Then
                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "52", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                            ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                        ElseIf objICInstruction.PaymentMode = "Cheque" And objICInstruction.Status = "31" Then
                            ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                        End If

                    End If
                Next
                If ArryListInstructionId.Count > 0 Then
                    StrInstCount = ICInstructionProcessController.CancelPrintOfInstructionFromPrintedQueue(ArryListInstructionId, Me.UserId, Me.UserInfo.Username.ToString, txtRemarks.Text.ToString, "32", dt, "INSTRUMENT STOP")
                Else
                    UIUtilities.ShowDialog(Me, "Error", "Invalid selected instruction(s) status", Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If
                If dt.Rows.Count > 0 Then
                    gvShowSummary.DataSource = Nothing
                    gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                    gvShowSummary.DataBind()
                    pnlShowSummary.Style.Remove("display")
                    Dim scr As String
                    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                Else
                    gvShowSummary.DataSource = Nothing
                    pnlShowSummary.Style.Add("display", "none")
                End If
                RefreshPage()
                ArraListStatus.Add(32)
                dtPODD = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueByAPNature(ArryListInstructionId, ArraListStatus, True)
                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueByAPNature(ArryListInstructionId, ArraListStatus, False)
                EmailUtilities.RevalidationRejectedbyApprover(dtInstructionInfo, dtPODD, Me.UserInfo.Username.ToString)

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnRevalidate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRevalidate.Click
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim ArryListInstructionId As New ArrayList
                Dim objICInstruction As ICInstruction
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim StrAuditTrailAction As String
                Dim dtInstructionInfo As New DataTable
                Dim ArrayListInstructionStatus As New ArrayList
                ArryListInstructionId.Clear()
                ArrayListInstructionStatus.Clear()

                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Stale Instruction Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Dim dr As DataRow
                Dim dt As New DataTable
                dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Message", GetType(System.String)))

                For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                    End If
                Next
                If ArryListInstructionId.Count > 0 Then
                    For Each InstructionID In ArryListInstructionId
                        objICInstruction = New ICInstruction
                        objICInstruction.es.Connection.CommandTimeout = 3600
                        objICInstruction.LoadByPrimaryKey(InstructionID)
                        dr = dt.NewRow
                        Try
                            objICInstruction.IsReValiDationAllowed = True
                            objICInstruction.ReValiDatedDate = Date.Now
                            objICInstruction.ReValiDationAllowedBy = Me.UserId
                            StrAuditTrailAction = Nothing
                            StrAuditTrailAction += "Instruction with ID [ " & InstructionID & " ] allowed re-validation."
                            StrAuditTrailAction += "Action was taken by user [ " & Me.UserInfo.UserID & " ] [ " & Me.UserInfo.Username & " ]"
                            ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAuditTrailAction, "UPDATE")
                            ICInstructionController.UpdateInstructionStatus(InstructionID, objICInstruction.Status, "33", Me.UserId.ToString, Me.UserInfo.Username.ToString, "Remarks", "PENDING REVALIDATION APPROVE", txtRemarks.Text.ToString)
                            dr("InstructionID") = objICInstruction.InstructionID.ToString()
                            dr("Message") = "Instruction(s) Re-validation allowed successfully"
                            dr("Status") = "33"
                            dt.Rows.Add(dr)
                            PassToCancelCount = PassToCancelCount + 1
                            Continue For
                        Catch ex As Exception
                            objICInstruction.SkipReason = "Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped. Revalidation fail due to " & ex.Message.ToString
                            ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            'ICUtilities.AddAuditTrail("Instruction Skip: Instruction with ID [ " & objICInstruction.InstructionID & " ] skip revalidation due to " & ex.Message.ToString, "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            dr("InstructionID") = objICInstruction.InstructionID.ToString()
                            'dr("Message") = "Instruction(s) Skipped: Re-validation not allowed successfully due to " & ex.Message.ToString
                            dr("Message") = "Instruction(s) Skipped"
                            dr("Status") = objICInstruction.Status.ToString
                            dt.Rows.Add(dr)
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For
                        End Try
                    Next
                End If
                RefreshPage()
                If dt.Rows.Count > 0 Then
                    gvShowSummary.DataSource = Nothing
                    gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                    gvShowSummary.DataBind()
                    pnlShowSummary.Style.Remove("display")
                    Dim scr As String
                    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                Else
                    gvShowSummary.DataSource = Nothing
                    pnlShowSummary.Style.Add("display", "none")
                End If
                If Not PassToCancelCount = 0 Then
                    ArrayListInstructionStatus.Add(33)
                    dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueByAPNature(ArryListInstructionId, ArrayListInstructionStatus, False)
                    EmailUtilities.TxnRevalidation(dtInstructionInfo)
                    SMSUtilities.TxnRevalidation(dtInstructionInfo, ArrayListInstructionStatus, ArryListInstructionId)
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub


    Protected Sub raddpCreationFromDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationFromDate.SelectedDateChanged
        Try


            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Stale Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Stale from date should be less than stale to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub raddpCreationToDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationToDate.SelectedDateChanged
        Try


            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Stale Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Stale from date should be less than stale to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub rbtnlstDateSelection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnlstDateSelection.SelectedIndexChanged
        Try
            If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now.AddYears(2)
            Else
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
            End If

            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Stale Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Stale from date should be less than stale to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class

