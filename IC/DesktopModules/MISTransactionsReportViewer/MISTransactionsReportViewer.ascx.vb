﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports Telerik.Reporting
Imports EntitySpaces.Core
Partial Class DesktopModules_MISTransactionsReportViewer_MISTransactionsReportViewer
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Dim dtDa As DataTable
    Private ArrICAssignUserRolsID As New ArrayList
    Private htRights As Hashtable
    Private ArrICAssignedPaymentModes As New ArrayList
    Private ArrICAssignedStatus As New ArrayList

    Private Sub DesignDts()
        Dim dtAccountNumber As New DataTable
        dtAccountNumber.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("BranchCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("Currency", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("PaymentNatureCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("OfficeID", GetType(System.String)))
        ViewState("AccountNumber") = dtAccountNumber
    End Sub
    Private Sub FillDesignedDtByAPNatureByAssignedRole()
        Dim dtAssignedAPNatureOfficeID As New DataTable
        dtAssignedAPNatureOfficeID.Rows.Clear()
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForAPNature(Me.UserId.ToString, ArrICAssignUserRolsID)
        ViewState("AccountNumber") = Nothing
        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID
        If dtAssignedAPNatureOfficeID.Rows.Count = 0 Then
            UIUtilities.ShowDialog(Me, "Error", "You do not have access to any transactional data. Please contact Al Baraka Administrator.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
            Exit Sub
        End If
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
         Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()
            If Page.IsPostBack = False Then

                btnExport.Visible = False
                ViewState("htRights") = Nothing
                GetPageAccessRights()

                LoadddlReport(Me.UserId.ToString())
                LoadddlCompany()
                LoadddlAccountPaymentNatureByUserID()
                LoadddlProductTypeByAccountPaymentNature()
                LoadStatus()
                DesignDts()
                FillDesignedDtByAPNatureByAssignedRole()
                LoadDLL_Batch()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    

    Private Sub LoadddlReport(ByVal UsersID As String)
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlReport.Items.Clear()
            ddlReport.Items.Add(lit)
            ddlReport.AppendDataBoundItems = True
            ddlReport.DataSource = ICMISReportController.GetReportNameforReportViewer(UsersID.ToString, "Transaction Report")
            ddlReport.DataTextField = "ReportName"
            ddlReport.DataValueField = "ReportID"
            ddlReport.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Private Sub GenerateReportByTransaction()
        'ICInstructionProcessController.ReportViewerByTransactionType(ddlCompany.SelectedValue, "", "", RptTransationReport.Report)
        ICInstructionProcessController.PrintInstruction("331", "", RptTransationReport.Report)
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try

            LoadddlAccountPaymentNatureByUserID()
            LoadddlProductTypeByAccountPaymentNature()
            LoadStatus()
            LoadDLL_Batch()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click
        If Page.IsValid Then
            Try
                Dim misRPT As New ICMISReports
                Dim objICFiles As New ICFiles
                If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                    If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                        UIUtilities.ShowDialog(Me, "Error", "From Date should be less than or equal To Date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                If misRPT.LoadByPrimaryKey(ddlReport.SelectedValue.ToString()) Then
                    Dim fl As New System.IO.MemoryStream

                    fl.Write(misRPT.ReportFileData, 0, misRPT.ReportFileSize)
                    fl.Seek(0, SeekOrigin.Begin)
                    Dim settings As New System.Xml.XmlReaderSettings()
                    settings.IgnoreWhitespace = True

                    'Dim xmlReader As System.Xml.XmlReader = System.Xml.XmlReader.Create(fl, settings)
                    'Dim xmlSerializer As New Telerik.Reporting.XmlSerialization.ReportXmlSerializer()


                    'Dim report As Telerik.Reporting.Report = DirectCast(xmlSerializer.Deserialize(xmlReader), Telerik.Reporting.Report)

                    Dim xmlReader As System.Xml.XmlReader = System.Xml.XmlReader.Create(fl, settings)
                    Dim xmlSerializer As New Telerik.Reporting.XmlSerialization.ReportXmlSerializer()

                    Dim report As Telerik.Reporting.Report = DirectCast(xmlSerializer.Deserialize(xmlReader), Telerik.Reporting.Report)


                    Dim AccountPaymentNature As String = String.Empty
                    Dim dt As New DataTable
                    dt = ViewState("AccountNumber")



                    Dim lstOf_PaymentNatureCollection As New ArrayList



                    For Each dr As DataRow In dt.DefaultView().ToTable(True, "AccountPaymentNature").Rows
                        'AccountPaymentNature += dr("AccountPaymentNature") + "-" + dr("PaymentNature") + Environment.NewLine + ","
                        lstOf_PaymentNatureCollection.Add(dr("AccountPaymentNature").ToString.Split("-")(0) + "-" + dr("AccountPaymentNature").ToString.Split("-")(1) + "-" + dr("AccountPaymentNature").ToString.Split("-")(2) + "-" + dr("AccountPaymentNature").ToString.Split("-")(3))
                    Next


                    'AccountPaymentNature = AccountPaymentNature.Remove(AccountPaymentNature.Length - 1, 1)

                    If ddlCompany.SelectedValue = "0" Then
                        report.ReportParameters("CompanyCode").Value = "%"
                    Else
                        report.ReportParameters("CompanyCode").Value = ddlCompany.SelectedValue
                    End If

                    If ddlProductType.SelectedValue = "0" Then
                        report.ReportParameters("ProductTypeCode").Value = "%"
                    Else
                        report.ReportParameters("ProductTypeCode").Value = ddlProductType.SelectedValue
                    End If




                    report.ReportParameters("GroupCode").Value = "%"


                    If ddlStatus.SelectedValue = "0" Then
                        report.ReportParameters("Status").Value = "%"
                    Else
                        report.ReportParameters("Status").Value = ddlStatus.SelectedValue
                    End If
                    If ddlBatch.SelectedValue = "0" Then
                        report.ReportParameters("FileBatchNo").Value = "%"
                    Else
                        report.ReportParameters("FileBatchNo").Value = ddlBatch.SelectedValue
                    End If



                    If rbtnlstDateSelection.SelectedIndex = 0 Then
                        If Not raddpCreationToDate.SelectedDate Is Nothing Then
                            report.ReportParameters("CreateDateTo").Value = raddpCreationToDate.SelectedDate
                        Else
                            report.ReportParameters("CreateDateTo").Value = DateTime.Now.AddYears(50)
                        End If
                        If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                            report.ReportParameters("CreateDateFrom").Value = raddpCreationFromDate.SelectedDate
                        Else
                            report.ReportParameters("CreateDateFrom").Value = DateTime.Now.AddYears(-215)
                        End If

                    Else

                        report.ReportParameters("CreateDateTo").Value = DateTime.Now.AddYears(50)
                        report.ReportParameters("CreateDateFrom").Value = DateTime.Now.AddYears(-215)
                    End If

                    If rbtnlstDateSelection.SelectedIndex = 1 Then

                        If Not raddpCreationToDate.SelectedDate Is Nothing Then
                            report.ReportParameters("ValueDateTo").Value = raddpCreationToDate.SelectedDate
                        Else
                            report.ReportParameters("ValueDateTo").Value = DateTime.Now.AddYears(50)
                        End If
                        If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                            report.ReportParameters("ValueDateFrom").Value = raddpCreationFromDate.SelectedDate
                        Else
                            report.ReportParameters("ValueDateFrom").Value = DateTime.Now.AddYears(-215)
                        End If

                    Else

                        report.ReportParameters("ValueDateTo").Value = DateTime.Now.AddYears(50)
                        report.ReportParameters("ValueDateFrom").Value = DateTime.Now.AddYears(-215)
                    End If

                    If rbtnlstDateSelection.SelectedIndex = 2 Then
                        If Not raddpCreationToDate.SelectedDate Is Nothing Then
                            report.ReportParameters("PrintDateTo").Value = raddpCreationToDate.SelectedDate
                        Else
                            report.ReportParameters("PrintDateTo").Value = DateTime.Now.AddYears(50)
                        End If
                        If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                            report.ReportParameters("PrintDateFrom").Value = raddpCreationFromDate.SelectedDate

                        Else
                            report.ReportParameters("PrintDateFrom").Value = DateTime.Now.AddYears(-215)
                        End If
                    Else
                        report.ReportParameters("PrintDateTo").Value = DateTime.Now.AddYears(50)
                        report.ReportParameters("PrintDateFrom").Value = DateTime.Now.AddYears(-215)
                    End If

                    If rbtnlstDateSelection.SelectedIndex = 3 Then
                        If Not raddpCreationToDate.SelectedDate Is Nothing Then
                            report.ReportParameters("ClearingDateTo").Value = raddpCreationToDate.SelectedDate
                        Else
                            report.ReportParameters("ClearingDateTo").Value = DateTime.Now.AddYears(50)
                        End If
                        If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                            report.ReportParameters("ClearingDateFrom").Value = raddpCreationFromDate.SelectedDate

                        Else
                            report.ReportParameters("ClearingDateFrom").Value = DateTime.Now.AddYears(-215)
                        End If
                    Else

                        report.ReportParameters("ClearingDateTo").Value = DateTime.Now.AddYears(50)
                        report.ReportParameters("ClearingDateFrom").Value = DateTime.Now.AddYears(-215)

                    End If
                    If ddlAcccountPaymentNature.SelectedValue = "0" Then
                        report.ReportParameters("PaymentNatureCode").Value = lstOf_PaymentNatureCollection 'AccountPaymentNature.Split(",")
                    Else
                        report.ReportParameters("PaymentNatureCode").Value = ddlAcccountPaymentNature.SelectedValue
                    End If

                    'Dim t As Telerik.Reporting.FilterCollection
                    'report.Filters.Add(new Telerik.Reporting.Data.Filter("StartTime", Telerik.Reporting.Data.FilterOperator.GreaterOrEqual, RadDatePicker1.SelectedDate.ToString())); 
                    'report.ReportParameters("PaymentNatureCode").MultiValue = True
                    report.ReportParameters("PaymentNaturecode").Mergeable = True


                    btnExport.Visible = CBool(htRights("Export"))


                    RptTransationReport.Width = Unit.Percentage(100)
                    RptTransationReport.ParametersAreaVisible = False
                    RptTransationReport.ReportSource = report
                    RptTransationReport.RefreshReport()
                    RptTransationReport.RefreshData()

                    ' RptTransationReport.Style.Remove("display")



                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Sub SetArraListAssignedPaymentModes()
        ArrICAssignedPaymentModes.Clear()
    End Sub
    Private Sub SetArraListAssignedStatus()
        ArrICAssignedStatus.Clear()
        If ddlStatus.SelectedValue.ToString <> "0" Then
            ArrICAssignedStatus.Add(ddlStatus.SelectedValue.ToString)
        End If
    End Sub

    Private Sub LoadDLL_Batch()
        Try
            Dim lit As New ListItem
            Dim DateType, FromDate, ToDate As String
            If ddlAcccountPaymentNature.SelectedValue.ToString = "0" Then
                lit.Text = "-- All --"
                lit.Value = "0"
                ddlBatch.Items.Clear()
                ddlBatch.Items.Add(lit)
            Else
                DateType = ""
                FromDate = ""
                ToDate = ""
                If rbtnlstDateSelection.SelectedIndex = 0 Then
                    DateType = "CreationDate"
                    If Not raddpCreationToDate.SelectedDate Is Nothing Then
                        ToDate = raddpCreationToDate.SelectedDate.Value.ToString("yyyyMMdd")
                    Else
                        ToDate = DateTime.Now.AddYears(50).ToString("yyyyMMdd")
                    End If
                    If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                        FromDate = raddpCreationFromDate.SelectedDate.Value.ToString("yyyyMMdd")
                    Else
                        FromDate = DateTime.Now.AddYears(-215).ToString("yyyyMMdd")
                    End If
                Else
                    ToDate = DateTime.Now.AddYears(50).ToString("yyyyMMdd")
                    FromDate = DateTime.Now.AddYears(-215).ToString("yyyyMMdd")
                End If
                Dim dtAccountNumber As New DataTable
                dtAccountNumber = ViewState("AccountNumber")
                SetArraListAssignedPaymentModes()
                SetArraListAssignedStatus()
                lit = New ListItem
                lit.Text = "-- All --"
                lit.Value = "0"
                ddlBatch.Items.Clear()
                ddlBatch.Items.Add(lit)
                lit.Selected = True
                ddlBatch.AppendDataBoundItems = True
                ddlBatch.DataSource = GetAllTaggedBatchNumbersByCompanyAccountPaymentNatureTxnReports(ArrICAssignedPaymentModes, dtAccountNumber, ddlCompany.SelectedValue.ToString, ArrICAssignedStatus, Me.UserId.ToString, DateType, FromDate, ToDate)
                ddlBatch.DataTextField = "FileBatchNoName"
                ddlBatch.DataValueField = "FileBatchNo"
                ddlBatch.DataBind()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Public Shared Function GetAllTaggedBatchNumbersByCompanyAccountPaymentNatureTxnReports(ByVal AssignedPaymentModes As ArrayList, ByVal dtAccountNo As DataTable, ByVal CompanyCode As String, ByVal ArrayListStatus As ArrayList, ByVal UserIDs As String, ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String) As DataTable

        Dim StrQuery As String = Nothing
        Dim WhereClause As String = Nothing
        WhereClause = " Where "
        Dim dt As New DataTable
        Dim params As New EntitySpaces.Interfaces.esParameters

        StrQuery = "select distinct(IC_Instruction.FileBatchNo), "
        StrQuery += "case ic_instruction.FileBatchNo when 'SI' "
        StrQuery += " then 'SI' else ic_instruction.FileBatchNo + '-' + IC_Files.OriginalFileName end as FileBatchNoName from IC_Instruction  "
        StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID  Where IC_Instruction.InstructionID IN "
        StrQuery += "(Select ins2.InstructionID from IC_Instruction as ins2 "
        If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
            WhereClause += " And ins2.PaymentMode in ("
            For i = 0 To AssignedPaymentModes.Count - 1
                WhereClause += "'" & AssignedPaymentModes(i) & "',"
            Next
            WhereClause = WhereClause.Remove(WhereClause.Length - 1)
            WhereClause += ")"
        End If
        If ArrayListStatus.Count > 0 Then
            WhereClause += " And ins2.Status in ( "
            For i = 0 To ArrayListStatus.Count - 1
                WhereClause += ArrayListStatus(i) & ","
            Next
            WhereClause = WhereClause.Remove(WhereClause.Length - 1)
            WhereClause += ")"
        End If
        If CompanyCode.ToString <> "0" Then
            WhereClause += " And ins2.CompanyCode=@CompanyCode"
            params.Add("CompanyCode", CompanyCode)
        Else
            WhereClause += " And ins2.CompanyCode=0"
        End If
        If FromDate <> "" Or ToDate <> "" Then
            If DateType = "CreationDate" Then
                If FromDate <> "" And ToDate <> "" Then
                    WhereClause += " And convert(date,isnull(ins2.CreateDate,'1900-01-01'))>=Convert(Date,@FromDate) And convert(date,isnull(ins2.CreateDate,'1900-01-01'))<=Convert(Date,@ToDate) "
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate <> "" And ToDate = "" Then
                    WhereClause += " And convert(date,isnull(ins2.CreateDate,'1900-01-01'))>=Convert(Date,@FromDate) "
                    params.Add("FromDate", FromDate)
                ElseIf FromDate = "" And ToDate <> "" Then
                    WhereClause += " And convert(date,isnull(ins2.CreateDate,'1900-01-01'))<=Convert(Date,@ToDate) "
                    params.Add("ToDate", ToDate)
                End If
            End If
        End If
        If dtAccountNo.Rows.Count > 0 Then
            WhereClause += " AND ("
            WhereClause += " ins2.ClientAccountNo +" & "'-'" & "+ ins2.ClientAccountBranchCode +" & "'-'" & "+ ins2.ClientAccountCurrency +" & "'-'" & "+ ins2.PaymentNatureCode IN ("
            For Each dr As DataRow In dtAccountNo.Rows
                WhereClause += "'" & dr("AccountPaymentNature").ToString.Split("-")(0) & "-" & dr("AccountPaymentNature").ToString.Split("-")(1) & "-" & dr("AccountPaymentNature").ToString.Split("-")(2) & "-" & dr("AccountPaymentNature").ToString.Split("-")(3) & "',"
            Next
            WhereClause = WhereClause.Remove(WhereClause.Length - 1, 1)
            WhereClause += ")"
        End If

        WhereClause += ")"
        If WhereClause.Contains(" Where  And") Then
            WhereClause = WhereClause.Replace(" Where  And", " Where ")
        End If
        StrQuery += WhereClause
        StrQuery += " ) Order By FileBatchNoName Asc"

        Dim util As New esUtility
        dt = util.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)
        Return dt

    End Function

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Transaction Reports")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Private Sub LoadStatus()
        ddlStatus.AppendDataBoundItems = True
        ddlStatus.Items.Clear()
        Dim lit As New ListItem("--All--", 0)
        
        ddlStatus.Items.Add(lit)
        ddlStatus.DataTextField = "StatusName"
        ddlStatus.DataValueField = "StatusID"
        ddlStatus.DataSource = ICInstructionStatusController.GetAllDistinctStatus()
        ddlStatus.DataBind()

    End Sub



    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, "", ArrICAssignUserRolsID)
            ddlCompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlCompany.Items.Clear()
                    ddlCompany.Items.Add(lit)
                    lit.Selected = True
                    ddlCompany.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
                ddlCompany.DataSource = dt
                ddlCompany.DataTextField = "CompanyName"
                ddlCompany.DataValueField = "CompanyCode"
                ddlCompany.DataBind()
                ddlCompany.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Transaction Reports") = True Then
                    If Not ArrICAssignUserRolsID.Contains(RoleID.ToString) Then
                        ArrICAssignUserRolsID.Add(RoleID.ToString)
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub LoadddlAccountPaymentNatureByUserID()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlAcccountPaymentNature.Items.Clear()
            ddlAcccountPaymentNature.Items.Add(lit)
            lit.Selected = True
            ddlAcccountPaymentNature.AppendDataBoundItems = True
            ddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForDropDown(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
            ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
            ddlAcccountPaymentNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub LoadddlProductTypeByAccountPaymentNature()
        Try
            Dim lit As New ListItem
            Dim objICAPNature As New ICAccountsPaymentNature
            objICAPNature.es.Connection.CommandTimeout = 3600
            ddlProductType.Items.Clear()
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlProductType.Items.Add(lit)
            ddlProductType.AppendDataBoundItems = True
            If ddlAcccountPaymentNature.SelectedValue.ToString <> "0" Then
                If objICAPNature.LoadByPrimaryKey(ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)) Then
                    ddlProductType.DataSource = ICAccountPaymentNatureProductTypeController.GetAllProductTypesByAccountAndPaymentNature(objICAPNature)
                    ddlProductType.DataTextField = "ProductTypeName"
                    ddlProductType.DataValueField = "ProductTypeCode"
                    ddlProductType.DataBind()
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub ddlAcccountPaymentNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcccountPaymentNature.SelectedIndexChanged

        Try
            If ddlAcccountPaymentNature.SelectedValue.ToString = "0" Then
                FillDesignedDtByAPNatureByAssignedRole()
                LoadddlProductTypeByAccountPaymentNature()
                LoadDLL_Batch()
               
            Else
                FillDesignedDtByAPNatureByAssignedRole()
                SetViewStateDtOnAPNatureIndexChnage()
                LoadddlProductTypeByAccountPaymentNature()
                LoadStatus()
                LoadDLL_Batch()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetViewStateDtOnAPNatureIndexChnage()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim AccountNumber, BranchCode, Currency, PaymentNatureCode As String
        Dim ArrayListOfficeID As New ArrayList
        Dim drAccountNo As DataRow
        AccountNumber = Nothing
        BranchCode = Nothing
        Currency = Nothing
        PaymentNatureCode = Nothing
        dtAccountNumber = ViewState("AccountNumber")
        ViewState("AccountNumber") = Nothing
        AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0)
        BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1)
        Currency = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2)
        PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)
        dtAccountNumber.Rows.Clear()

        drAccountNo = dtAccountNumber.NewRow()
        drAccountNo("AccountAndPaymentNature") = AccountNumber & "-" & PaymentNatureCode
        drAccountNo("AccountPaymentNature") = AccountNumber & "-" & BranchCode & "-" & Currency & "-" & PaymentNatureCode
        dtAccountNumber.Rows.Add(drAccountNo)

        'dtAccountNumber.Columns.Add(New DataColumn("DropDown", GetType(System.String)))

        ViewState("AccountNumber") = dtAccountNumber
    End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click

        Try
                'Dim reportProcessor As New Telerik.Reporting.Processing.ReportProcessor()

                ''set any deviceInfo settings if necessary
                'Dim deviceInfo As New System.Collections.Hashtable()

                'Dim instanceReportSource As New Telerik.Reporting.InstanceReportSource()
                'instanceReportSource.ReportDocument = rvSOA.Report

                'Dim result As Telerik.Reporting.Processing.RenderingResult = reportProcessor.RenderReport("PDF", instanceReportSource, deviceInfo)
                'Dim fileName As String = result.DocumentName + "." + result.Extension
                'Dim path As String = System.IO.Path.GetTempPath()
                'Dim filePath As String = System.IO.Path.Combine(path, fileName)

                'Using fs As New System.IO.FileStream(filePath, System.IO.FileMode.Create)
                '    fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length)
                'End Using



                Dim reportProcessor As New Telerik.Reporting.Processing.ReportProcessor()
                Dim instanceReportSource As New Telerik.Reporting.InstanceReportSource()
                instanceReportSource.ReportDocument = RptTransationReport.Report
                Dim result As Telerik.Reporting.Processing.RenderingResult = reportProcessor.RenderReport("XLS", instanceReportSource, Nothing)

                Dim fileName As String = result.DocumentName & "." '"& result.Extension
                Response.Clear()
                Response.ContentType = result.MimeType
                Response.Cache.SetCacheability(HttpCacheability.Private)
                Response.Expires = -1
                Response.Buffer = True
                Response.AddHeader("Content-Disposition", String.Format("{0};FileName=""{1}""", "attachment", fileName))
                Response.BinaryWrite(result.DocumentBytes)
                Response.End()





            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub

    Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged

        Try
            LoadStatus()
            LoadDLL_Batch()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStatus.SelectedIndexChanged
        Try
            LoadDLL_Batch()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub raddpCreationFromDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationFromDate.SelectedDateChanged
        Try
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    raddpCreationFromDate.SelectedDate = Nothing
                    UIUtilities.ShowDialog(Me, "Error", "From date should be less than To date.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub raddpCreationToDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationToDate.SelectedDateChanged
        Try

            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    raddpCreationToDate.SelectedDate = Nothing
                    UIUtilities.ShowDialog(Me, "Error", "From date should be less than To date.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class

