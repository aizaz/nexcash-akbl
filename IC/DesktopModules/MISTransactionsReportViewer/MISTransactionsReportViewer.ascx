﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MISTransactionsReportViewer.ascx.vb"
    Inherits="DesktopModules_MISTransactionsReportViewer_MISTransactionsReportViewer" %>
<%@ Register Assembly="Telerik.ReportViewer.WebForms,  Version=7.0.13.220, Culture=neutral, PublicKeyToken=A9D7983DFCC261BE"
    Namespace="Telerik.ReportViewer.WebForms" TagPrefix="telerik" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:TextBoxSetting BehaviorID="reTnxCount" Validation-IsRequired="false" EmptyMessage="Transaction Count"
        ErrorMessage="Transaction Count">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTnxCount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reTotalAmount" Validation-IsRequired="false"
        EmptyMessage="Total Amount" ErrorMessage="Total Amount">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTotalAmount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reCurrency" Validation-IsRequired="false" EmptyMessage="Currency"
        ErrorMessage="Currency">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCurrency" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rePaymentMode" Validation-IsRequired="false"
        EmptyMessage="Payment Mode" ErrorMessage="Payment Mode">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPaymentMode" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reAvailableBalance" Validation-IsRequired="false"
        EmptyMessage="Available Balance" ErrorMessage="Available Balance">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAvailableBalance" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reInstructionNo" Validation-IsRequired="false"
        EmptyMessage="Enter Instruction No." ErrorMessage="Enter Instruction No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstructionNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reInstrumentNo" Validation-IsRequired="false"
        EmptyMessage="Enter Instrument No." ErrorMessage="Enter Instrument No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstrumentNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reReferenceNo" Validation-IsRequired="false"
        EmptyMessage="Enter Reference No." ErrorMessage="Enter Reference No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reRemarks" Validation-IsRequired="true" EmptyMessage="Enter Remarks"
        ErrorMessage="Enter Remarks">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRemarks" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Label ID="lblPageHeader" runat="server" Text="MIS Transactions Report Viewer"
                            CssClass="headingblue"></asp:Label>
                        <br />
                        Note:&nbsp; Fields marked as
                        <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        &nbsp;are required.
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <table align="left" valign="top" style="width: 100%">
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblReport" runat="server" Text="Report" CssClass="lbl"></asp:Label>
                                    <asp:Label ID="Label5" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:DropDownList ID="ddlReport" runat="server" CssClass="dropdown">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlReport" runat="server" ControlToValidate="ddlReport" Enabled="true"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Report" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%" colspan="2">
                                    <asp:RadioButtonList ID="rbtnlstDateSelection" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem> Creation Date</asp:ListItem>
                                        <asp:ListItem> Value Date</asp:ListItem>
                                        <asp:ListItem>Print Date</asp:ListItem>
                                        <asp:ListItem>Settled / Cleared Date</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td align="left" valign="top" style="width: 50%" colspan="2">
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblDateFrom" runat="server" Text="Date From" CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblCreationDateTo" runat="server" Text="Date To" CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                                <td align="left" valign="top" style="width: 25%">
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <telerik:RadDatePicker ID="raddpCreationFromDate" runat="server" 
                                        CssClass="txtbox">
                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                        </Calendar>
                                        <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                            LabelWidth="40%" type="text" value="">
                                        </DateInput>
                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                    </telerik:RadDatePicker>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <telerik:RadDatePicker ID="raddpCreationToDate" runat="server" 
                                        CssClass="txtbox">
                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                        </Calendar>
                                        <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                            LabelWidth="40%" type="text" value="">
                                        </DateInput>
                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                    </telerik:RadDatePicker>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;</td>
                                <td align="left" valign="top" style="width: 25%">
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblAccountPaymentNature1" runat="server" Text="Company" CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblAccountPaymentNature" runat="server" Text="Account Payment Nature"
                                        CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:DropDownList ID="ddlCompany" AutoPostBack="true" runat="server" 
                                        CssClass="dropdown">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:DropDownList ID="ddlAcccountPaymentNature" runat="server" 
                                        CssClass="dropdown" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblAccountPaymentNature2" runat="server" Text="Product Type" CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblBatch0" runat="server" Text="Status" CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:DropDownList ID="ddlProductType" runat="server" CssClass="dropdown" 
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:DropDownList ID="ddlStatus" runat="server" CssClass="dropdown" 
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblBatch" runat="server" Text="Batch" CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:DropDownList ID="ddlBatch" runat="server" CssClass="dropdown" 
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                </td>
                            </tr>
                            <tr align="center" valign="top" style="width: 100%">
                                <td align="center" valign="top" style="width: 100%" colspan="4">
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn" CausesValidation="true" />
                                    <asp:Button ID="btnExport" runat="server" CssClass="btn" CausesValidation="False"  Text="Export" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr align="center" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <telerik:ReportViewer ID="RptTransationReport" DocumentMapVisible="true"  runat="server" Width="100%" >
                        
                        </telerik:ReportViewer>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
