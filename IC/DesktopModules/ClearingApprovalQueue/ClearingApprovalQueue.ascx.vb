﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_ClearingApprovalQueue_ClearingApprovalQueue
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrICAssignUserRolsID As New ArrayList
    Private ArrICAssignedOfficeID As New ArrayList
    Private ArrICAssignedStatus As New ArrayList
    Private ArrICAssignedPaymentModes As New ArrayList
    Private htRights As Hashtable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()
            lblMessageForQueue.Style.Add("display", "none")
            If Page.IsPostBack = False Then
                lblMessageForQueue.Text = ICUtilities.GetSettingValue("ErrorMessage")
                btnClearingReject.Attributes.Item("onclick") = "if (Page_ClientValidate('Verify')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnClearingReject, Nothing).ToString() & " } "
                btnApproveClearing.Attributes.Item("onclick") = "if (Page_ClientValidate('Verify')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";document.getElementById('<%=lblMessageForQueue.ClientID%>').style.display = 'block';this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnApproveClearing, Nothing).ToString() & " } "
                btnCancelInstructions.Attributes.Item("onclick") = "if (Page_ClientValidate('Verify')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnCancelInstructions, Nothing).ToString() & " } "


                Dim objICUser As New ICUser
                objICUser.es.Connection.CommandTimeout = 3600
                objICUser.LoadByPrimaryKey(Me.UserId.ToString)
                If Not objICUser.UserType = "Bank User" Then
                    UIUtilities.ShowDialog(Me, "Error", "Only authorized Bank Users allowed", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                'SetArraListAssignedOfficeIDs()
                rbtnlstDateSelection.SelectedValue = "Creation Date"
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
                LoadddlGroup()
                LoadddlCompany()
                LoadddlAccountPaymentNatureByUserID()
                LoadddlProductTypeByAccountPaymentNature()
                FillDesignedDtByAPNatureByAssignedRole()

                Dim dtPageLoad As New DataTable
                dtPageLoad = DirectCast(ViewState("AccountNumber"), DataTable)
                If dtPageLoad.Rows.Count > 0 Then
                    ClearAllTextBoxes()
                    LoadgvAccountPaymentNatureProductType(True)
                Else
                    UIUtilities.ShowDialog(Me, "Error", "You do not have access to any transactional data. Please contact Al Baraka Administrator.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Clearing Approval") = True Then
                    If Not ArrICAssignUserRolsID.Contains(RoleID.ToString) Then
                        ArrICAssignUserRolsID.Add(RoleID.ToString)
                    End If
                End If
            Next
        End If
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Clearing Approval")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, ddlGroup.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlCompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlCompany.Items.Clear()
                    ddlCompany.Items.Add(lit)
                    lit.Selected = True
                    ddlCompany.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
                ddlCompany.DataSource = dt
                ddlCompany.DataTextField = "CompanyName"
                ddlCompany.DataValueField = "CompanyCode"
                ddlCompany.DataBind()
                ddlCompany.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedGroupsByUserID(Me.UserId.ToString, ArrICAssignUserRolsID)
            ddlGroup.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("GroupCode")
                    lit.Text = dr("GroupName")
                    ddlGroup.Items.Clear()
                    ddlGroup.Items.Add(lit)
                    lit.Selected = True
                    ddlGroup.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlGroup.Items.Clear()
                ddlGroup.Items.Add(lit)
                ddlGroup.AppendDataBoundItems = True
                ddlGroup.DataSource = dt
                ddlGroup.DataTextField = "GroupName"
                ddlGroup.DataValueField = "GroupCode"
                ddlGroup.DataBind()
                ddlGroup.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlGroup.Items.Clear()
                ddlGroup.Items.Add(lit)
                ddlGroup.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetViewStateDtOnAPNatureIndexChnage()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim AccountNumber, BranchCode, Currency, PaymentNatureCode As String
        Dim ArrayListOfficeID As New ArrayList
        Dim drAccountNo As DataRow
        AccountNumber = Nothing
        BranchCode = Nothing
        Currency = Nothing
        PaymentNatureCode = Nothing
        dtAccountNumber = ViewState("AccountNumber")
        ViewState("AccountNumber") = Nothing
        AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0)
        BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1)
        Currency = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2)
        PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)
        dtAccountNumber.Rows.Clear()
       
        drAccountNo = dtAccountNumber.NewRow()
        drAccountNo("AccountAndPaymentNature") = AccountNumber & "-" & PaymentNatureCode
        drAccountNo("AccountPaymentNature") = AccountNumber & "-" & BranchCode & "-" & Currency & "-" & PaymentNatureCode
            dtAccountNumber.Rows.Add(drAccountNo)

        'dtAccountNumber.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
      
        ViewState("AccountNumber") = dtAccountNumber
    End Sub
    Private Sub LoadddlAccountPaymentNatureByUserID()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlAcccountPaymentNature.Items.Clear()
            ddlAcccountPaymentNature.Items.Add(lit)
            lit.Selected = True
            ddlAcccountPaymentNature.AppendDataBoundItems = True
            ddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForDropDown(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
            ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
            ddlAcccountPaymentNature.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub FillDesignedDtByAPNatureByAssignedRole()
        Dim dtAssignedAPNatureOfficeID As New DataTable
        dtAssignedAPNatureOfficeID.Rows.Clear()
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForAPNature(Me.UserId.ToString, ArrICAssignUserRolsID)
        ViewState("AccountNumber") = Nothing
        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID

    End Sub
    Private Sub LoadddlProductTypeByAccountPaymentNature()
        Try
            Try
                Dim lit As New ListItem
                Dim objICAPNature As New ICAccountsPaymentNature
                objICAPNature.es.Connection.CommandTimeout = 3600
                ddlProductType.Items.Clear()
                lit.Value = "0"
                lit.Text = "-- All --"
                ddlProductType.Items.Add(lit)
                ddlProductType.AppendDataBoundItems = True
                If ddlAcccountPaymentNature.SelectedValue.ToString <> "0" Then
                    If objICAPNature.LoadByPrimaryKey(ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)) Then
                        ddlProductType.DataSource = ICAccountPaymentNatureProductTypeController.GetAllProductTypesByAccountAndPaymentNature(objICAPNature)
                        ddlProductType.DataTextField = "ProductTypeName"
                        ddlProductType.DataValueField = "ProductTypeCode"
                        ddlProductType.DataBind()
                    End If
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadgvAccountPaymentNatureProductType(ByVal DoDataBind As Boolean)
        Try
            Dim AccountNumber, BranchCode, Currency, PaymentNatureCode, GroupCode, CompanyCode, ProductTypeCode, InstrumentNo, InstructionNo As String
            Dim FromDate, ToDate, DateType As String
            Dim Amount As Double = 0
            AccountNumber = ""
            BranchCode = ""
            Currency = ""
            Currency = ""
            PaymentNatureCode = ""
            CompanyCode = "0"
            ProductTypeCode = ""
            InstructionNo = ""
            InstrumentNo = ""
            GroupCode = ""
            FromDate = Nothing
            ToDate = Nothing
            DateType = ""
            If rbtnlstDateSelection.SelectedValue = "Creation Date" Then
                DateType = "Creation Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Value Date" Then
                DateType = "Value Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Stale Date" Then
                DateType = "Stale Date"
            End If

            If ddlGroup.SelectedValue.ToString <> "0" Then
                GroupCode = ddlGroup.SelectedValue.ToString
            End If
            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            End If
            If ddlProductType.SelectedValue.ToString <> "0" Then
                ProductTypeCode = ddlProductType.SelectedValue.ToString
            End If
            If txtInstrumentNo.Text.ToString <> "" And txtInstrumentNo.Text.ToString <> "Enter Instrument No." Then
                InstrumentNo = txtInstrumentNo.Text.ToString
            End If
            If txtInstructionNo.Text.ToString <> "" And txtInstructionNo.Text.ToString <> "Enter Instruction No." Then
                InstructionNo = txtInstructionNo.Text.ToString
            End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                FromDate = raddpCreationFromDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If Not raddpCreationToDate.SelectedDate Is Nothing Then
                ToDate = raddpCreationToDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If

            Dim dtAccountNumber As New DataTable
            dtAccountNumber = ViewState("AccountNumber")
            ICInstructionController.GetAllInstructionsForRadGridByAccountAndPaymentNatureForClearingApproval(DateType, FromDate, ToDate, dtAccountNumber, ProductTypeCode, InstructionNo, InstrumentNo, "", Me.gvAuthorization.CurrentPageIndex + 1, Me.gvAuthorization.PageSize, Me.gvAuthorization, DoDataBind, Me.UserId.ToString, Amount, "", CompanyCode, GroupCode, "38", Me.UserId.ToString)
            If gvAuthorization.Items.Count > 0 Then
                gvAuthorization.Visible = True
                lblNRF.Visible = False
                btnApproveClearing.Visible = CBool(htRights("Clearing Approve"))
                btnCancelInstructions.Visible = CBool(htRights("Cancel Instructions"))
                btnClearingReject.Visible = CBool(htRights("Clearing Reject"))

            Else
                btnApproveClearing.Visible = False
                btnCancelInstructions.Visible = False
                btnClearingReject.Visible = False
                lblNRF.Visible = True
                gvAuthorization.Visible = False

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    'Private Sub SetJavaScript()
    '    Dim imgBtn As ImageButton
    '    'If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Then
    '    Dim AppPath As String = DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
    '    For Each gvAuthorizationRow As GridDataItem In gvAuthorization.Items
    '        imgBtn = New ImageButton
    '        imgBtn = DirectCast(gvAuthorizationRow.Cells(11).FindControl("ibButton"), ImageButton)
    '        imgBtn.OnClientClick = "showurldialog('Instruction Details','" & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & imgBtn.CommandArgument.ToString() & "' ,true,700,650);return false;"
    '    Next
    '    'End If
    'End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            'SetArraListRoleID()
            FillDesignedDtByAPNatureByAssignedRole()
            LoadddlAccountPaymentNatureByUserID()
            LoadddlProductTypeByAccountPaymentNature()
            LoadgvAccountPaymentNatureProductType(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlAcccountPaymentNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcccountPaymentNature.SelectedIndexChanged
        Try
            If ddlAcccountPaymentNature.SelectedValue.ToString = "0" Then
                SetArraListRoleID()
                LoadddlProductTypeByAccountPaymentNature()
                FillDesignedDtByAPNatureByAssignedRole()
                LoadgvAccountPaymentNatureProductType(True)
            Else
                FillDesignedDtByAPNatureByAssignedRole()
                LoadddlProductTypeByAccountPaymentNature()
                SetViewStateDtOnAPNatureIndexChnage()
                LoadgvAccountPaymentNatureProductType(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged
        Try

            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ClearAllTextBoxes()

        txtInstructionNo.Text = ""
        txtInstrumentNo.Text = ""

    End Sub
    Private Function CheckgvInstructionAuthentication() As Boolean
        Try

            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVInstruction In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVInstruction.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub gvAuthorization_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles gvAuthorization.ItemCommand
        Try
            If e.CommandName = "ShowBalance" Then
                Dim AccountBalance As Double = 0
                Dim AccountNo As String = Nothing
                Dim BranchCode As String = Nothing
                Try
                    AccountNo = e.CommandArgument.ToString.Split(";")(0)
                    BranchCode = e.CommandArgument.ToString.Split(";")(1)
                    AccountBalance = CBUtilities.BalanceInquiry(AccountNo, CBUtilities.AccountType.RB, "Client Account Balance", AccountNo, BranchCode)
                    If AccountBalance <> -1 Then
                        UIUtilities.ShowDialog(Me, "Client Account Balance", "Available account balance is [ " & AccountBalance.ToString("N2") & "]", ICBO.IC.Dialogmessagetype.Success)
                        Exit Sub
                    Else
                        UIUtilities.ShowDialog(Me, "Client Account Balance", "There is an issue in balance inquiry. Please contact Al Baraka administrator", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Catch ex As Exception
                    UIUtilities.ShowDialog(Me, "Client Account Balance", "There is an issue in balance inquiry. Please contact Al Baraka administrator", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End Try
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAuthorization_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAuthorization.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAuthorization_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','0');"
        End If
        Dim imgBtn As LinkButton
        Dim AppPath As String = "" ' DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            imgBtn = New LinkButton
            imgBtn = DirectCast(item.Cells(1).FindControl("lbInstructionID"), LinkButton)
            imgBtn.OnClientClick = "showurldialog('Instruction Details','/ShowInstructionDetails.aspx?InstructionID=" & item.GetDataKeyValue("InstructionID").ToString().EncryptString() & "' ,true,700,650);return false;"
            If CBool(htRights("View Instructions Detail")) = True Then
                imgBtn.Enabled = True
            End If
            imgBtn = New LinkButton
            imgBtn = DirectCast(item.Cells(12).FindControl("lbSignatures"), LinkButton)
            imgBtn.OnClientClick = "showurldialog('Account Signatures','/ShowSiignatures.aspx?an=" & item.GetDataKeyValue("InstructionID").ToString().EncryptString() & "' ,true,700,650);return false;"



            If item.GetDataKeyValue("IsAmendmentComplete") = True Then
                gvAuthorization.AlternatingItemStyle.CssClass = "GridItemComplete"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItemComplete"
            Else
                gvAuthorization.AlternatingItemStyle.CssClass = "GridItem"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItem"
            End If
        End If
    End Sub
    Private Function CheckgvClientRoleCheckedForProcessAll() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub gvAuthorization_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAuthorization.NeedDataSource
        Try
            LoadgvAccountPaymentNatureProductType(False)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click

        Try
                Response.Redirect(NavigateURL(41))
                ' UIUtilities.ShowDialog(Me, "Response", GetBeforeClearingApprovalStatus("11669").ToString, Dialogmessagetype.Success)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click

        Try
                LoadgvAccountPaymentNatureProductType(True)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            FillDesignedDtByAPNatureByAssignedRole()
            LoadddlCompany()
            LoadddlAccountPaymentNatureByUserID()
            LoadddlProductTypeByAccountPaymentNature()
            LoadgvAccountPaymentNatureProductType(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnRetry_Click(sender As Object, e As System.EventArgs) Handles btnCancelInstructions.Click
        Page.Validate()
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim ArryListInstructionId As New ArrayList
                Dim objICInstruction As ICInstruction
                Dim objICInstructionStatus As ICInstructionStatus
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim StrInstCount As String = Nothing
                Dim StrAuditTrailAction As String
                ArryListInstructionId.Clear()
                Dim dtInstructionInfo As New DataTable
                Dim ArrListPassStatus As New ArrayList

                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Clearing Approval Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                Dim dt As New DataTable
                dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Message", GetType(System.String)))
                Dim Remarks As String = ""

                For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        'ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                        objICInstruction = New ICInstruction
                        objICInstruction.LoadByPrimaryKey(gvVerificationRow.GetDataKeyValue("InstructionID"))
                        If (objICInstruction.PaymentMode = "PO" Or objICInstruction.PaymentMode = "DD") And (objICInstruction.Status = "38") Then
                            ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "52", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", Remarks)
                            ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                        ElseIf objICInstruction.PaymentMode = "Cheque" And (objICInstruction.Status = "38") Then
                            ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                        End If
                    End If
                Next
                If ArryListInstructionId.Count > 0 Then

                    StrInstCount = ICInstructionProcessController.CancelPrintOfInstructionFromPrintedQueue(ArryListInstructionId, Me.UserId, Me.UserInfo.Username.ToString, Remarks, "32", dt, "INSTRUMENT STOP")
                Else
                    UIUtilities.ShowDialog(Me, "Error", "Invalid selected instruction(s) status", Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If

                PassToCancelCount = StrInstCount.Split(",")(0)
                FailToCancelCount = StrInstCount.Split(",")(1)

                If FailToCancelCount = 0 And PassToCancelCount <> 0 Then
                    UIUtilities.ShowDialog(Me, "Clearing Approval Queue", "[ " & PassToCancelCount & " ] Instruction(s) cancelled successfully", ICBO.IC.Dialogmessagetype.Success)
                    LoadgvAccountPaymentNatureProductType(True)
                    ClearAllTextBoxes()
                ElseIf FailToCancelCount <> 0 And PassToCancelCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Clearing Approval Queue", "[ " & FailToCancelCount & " ] Instruction(s) not cancelled successfully", ICBO.IC.Dialogmessagetype.Failure)
                    LoadgvAccountPaymentNatureProductType(True)
                    ClearAllTextBoxes()
                ElseIf FailToCancelCount <> 0 And PassToCancelCount <> 0 Then
                    UIUtilities.ShowDialog(Me, "Clearing Approval Queue", "[ " & PassToCancelCount & " ] Instruction(s) cancelled successfully <br/ > [ " & FailToCancelCount & " ] Instruction(s) not cancelled successfully", ICBO.IC.Dialogmessagetype.Failure)
                    LoadgvAccountPaymentNatureProductType(True)
                    ClearAllTextBoxes()
                End If
                ArrListPassStatus.Add(5)
                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueByAPNature(ArryListInstructionId, ArrListPassStatus, False)
                EmailUtilities.Cancelledtransactions(dtInstructionInfo, Me.UserInfo.Username.ToString, "APNature")
                SMSUtilities.Cancelledtransactions(Me.UserInfo.Username.ToString, dtInstructionInfo, ArrListPassStatus, ArryListInstructionId, "APNature")
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Protected Sub btnClearingReject_Click(sender As Object, e As System.EventArgs) Handles btnClearingReject.Click
        Page.Validate()
        If Page.IsValid Then
            Try
                Dim chkSelect As CheckBox
                Dim ArryListInstructionId As New ArrayList
                Dim objICInstruction As ICInstruction
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim StrAuditTrailAction As String = Nothing
                Dim LastStatus As String = Nothing
                ArryListInstructionId.Clear()


                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Clearing Approval Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                    End If
                Next
                If ArryListInstructionId.Count > 0 Then
                    For Each InstructionID In ArryListInstructionId
                        objICInstruction = New ICInstruction
                        objICInstruction.es.Connection.CommandTimeout = 3600
                        objICInstruction.LoadByPrimaryKey(InstructionID)
                        objICInstruction.BeneficiaryAccountTitle = "-"
                        objICInstruction.BeneficiaryAccountNo = "-"
                        objICInstruction.BeneAccountBranchCode = "-"
                        objICInstruction.BeneAccountCurrency = "-"
                        objICInstruction.Save()

                        Try
                            StrAuditTrailAction = Nothing
                            StrAuditTrailAction += "Instruction with ID [ " & InstructionID & " ]  rejected from clearing approval."
                            StrAuditTrailAction += "Status updated from  [ " & objICInstruction.Status & " ][ " & objICInstruction.UpToICInstructionStatusByStatus.StatusName & " ], to status [ " & objICInstruction.LastStatus & "] [ " & objICInstruction.UpToICInstructionStatusByLastStatus.StatusName & " ]."
                            StrAuditTrailAction += "Action was taken by user [ " & Me.UserInfo.UserID & " ] [ " & Me.UserInfo.Username & " ]."



                            LastStatus = GetBeforeClearingApprovalStatus(objICInstruction.InstructionID.ToString)
                            If objICInstruction.LastStatus.ToString = LastStatus Then
                                ICInstructionController.UpdateInstructionStatus(InstructionID, objICInstruction.Status, objICInstruction.LastStatus.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", StrAuditTrailAction)
                            Else
                                ICInstructionController.UpdateInstructionStatus(InstructionID, objICInstruction.Status, LastStatus, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", StrAuditTrailAction)
                            End If

                            PassToCancelCount = PassToCancelCount + 1
                            Continue For
                        Catch ex As Exception
                            StrAuditTrailAction = Nothing
                            StrAuditTrailAction += "Instruction Skip: Instruction with ID [ " & InstructionID & " ] skipped due to " & ex.Message.ToString & " . Action was taken by [ " & Me.UserId & " ] [ " & Me.UserInfo.Username & " ]"
                            ICUtilities.AddAuditTrail(StrAuditTrailAction, "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            'ICInstructionController.UpdateInstructionStatus(InstructionID, objICInstruction.Status, "37", Me.UserId.ToString, Me.UserInfo.Username.ToString, "Error", ex.Message.ToString)
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For
                        End Try
                    Next
                End If

                If FailToCancelCount = 0 And PassToCancelCount <> 0 Then
                    UIUtilities.ShowDialog(Me, "Clearing Approval Queue", "[ " & PassToCancelCount & " ] Instruction(s) rejected successfully", ICBO.IC.Dialogmessagetype.Success)
                    LoadgvAccountPaymentNatureProductType(True)
                    ClearAllTextBoxes()
                ElseIf FailToCancelCount <> 0 And PassToCancelCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Clearing Approval Queue", "[ " & FailToCancelCount & " ] Instruction(s) not rejected successfully", ICBO.IC.Dialogmessagetype.Failure)
                    LoadgvAccountPaymentNatureProductType(True)
                    ClearAllTextBoxes()
                ElseIf FailToCancelCount <> 0 And PassToCancelCount <> 0 Then
                    UIUtilities.ShowDialog(Me, "Clearing Approval Queue", "[ " & PassToCancelCount & " ] Instruction(s) rejected successfully <br/ > [ " & FailToCancelCount & " ] Instruction(s) not rejected successfully", ICBO.IC.Dialogmessagetype.Failure)
                    LoadgvAccountPaymentNatureProductType(True)
                    ClearAllTextBoxes()
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Protected Sub btnApproveClearing_Click(sender As Object, e As System.EventArgs) Handles btnApproveClearing.Click
        Page.Validate()
        If Page.IsValid Then
            Try

                Dim chkSelect As CheckBox
                Dim ArryListInstructionId As New ArrayList
                Dim objICUser As New ICUser
                Dim StrInstCount As String = Nothing
                Dim StrArr As String() = Nothing
                Dim ArrayListInstructionID As New ArrayList
                Dim dtVerifiedInstruction As DataTable
                Dim InstCount As Integer = 0
                Dim objchkICInstruction As ICInstruction


                objICUser.LoadByPrimaryKey(Me.UserId.ToString)
                ArryListInstructionId.Clear()


                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Instruction Verification", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Try
                    If CBUtilities.GetEODStatus() = "01" Then
                        UIUtilities.ShowDialog(Me, "Error", "EOD Status : " & CBUtilities.GetErrorMessageForTransactionFromResponseCode("01", "EOD"), Dialogmessagetype.Failure, NavigateURL(41))
                        Exit Sub
                    End If
                Catch ex As Exception
                    UIUtilities.ShowDialog(Me, "Error", "EOD Status : " & ex.Message.ToString, Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End Try

                For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        objchkICInstruction = New ICInstruction
                        objchkICInstruction.LoadByPrimaryKey(gvVerificationRow.GetDataKeyValue("InstructionID"))
                        If objchkICInstruction.Status.ToString = "38" Then
                            ICInstructionController.UpdateInstructionStatus(objchkICInstruction.InstructionID.ToString, objchkICInstruction.Status.ToString, "52", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE")
                            ArryListInstructionId.Add(objchkICInstruction.InstructionID.ToString)
                        End If
                    End If
                Next

                If ArryListInstructionId.Count > 0 Then

                    StrInstCount = ICInstructionProcessController.InstructionClearingApproval(ArryListInstructionId, objICUser.OfficeCode.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "30", "38")
                Else
                    UIUtilities.ShowDialog(Me, "Clearing Approval Queue", "Instruction(s) already processed.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If
                StrArr = StrInstCount.Split(",")
                If StrArr(1).ToString = "0" And StrArr(0).ToString <> "0" Then
                    UIUtilities.ShowDialog(Me, "Clearing Approval Queue", "[ " & StrArr(0).ToString & " ] Instruction(s) processed successfully.", ICBO.IC.Dialogmessagetype.Success)
                    LoadgvAccountPaymentNatureProductType(True)
                    ClearAllTextBoxes()
                ElseIf StrArr(1).ToString <> "0" And StrArr(0).ToString = "0" Then
                    UIUtilities.ShowDialog(Me, "Clearing Approval Queue", "[ " & StrArr(1).ToString & " ] Instruction(s) not processed successfully.", ICBO.IC.Dialogmessagetype.Failure)
                    LoadgvAccountPaymentNatureProductType(True)
                    ClearAllTextBoxes()
                ElseIf StrArr(1).ToString <> "0" And StrArr(0).ToString <> "0" Then
                    UIUtilities.ShowDialog(Me, "Clearing Approval Queue", "[ " & StrArr(0).ToString & " ] Instruction(s) processed successfully <br/ > [ " & StrArr(1).ToString & " ] Instruction(s) not processed successfully.", ICBO.IC.Dialogmessagetype.Failure)
                    LoadgvAccountPaymentNatureProductType(True)
                    ClearAllTextBoxes()
                ElseIf StrArr(1).ToString = "0" And StrArr(0).ToString = "0" Then
                    UIUtilities.ShowDialog(Me, "Clearing Approval Queue", "No instruction(s) processed successfully.", ICBO.IC.Dialogmessagetype.Failure)
                    LoadgvAccountPaymentNatureProductType(True)
                    ClearAllTextBoxes()
                End If

                If ArryListInstructionId.Count > 0 Then
                    dtVerifiedInstruction = New DataTable
                    Dim ArryListPassStatus As New ArrayList
                    ArryListPassStatus.Add(30)
                    dtVerifiedInstruction = ICInstructionController.GetAllInstructionAccountAmountAndProductTypeWiseByArrayListInstructionIDAndStatusWithInstrumentNo(ArryListInstructionId, "30")
                    If dtVerifiedInstruction.Rows.Count > 0 Then


                        EmailUtilities.TransactionsCleared(dtVerifiedInstruction)
                        SMSUtilities.Transactionscleared(dtVerifiedInstruction, ArryListInstructionId, ArryListPassStatus)

                    End If
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub rbtnlstDateSelection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnlstDateSelection.SelectedIndexChanged
        Try
            If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now.AddYears(2)
            Else
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
            End If

            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Stale Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Stale from date should be less than stale to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub raddpCreationFromDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationFromDate.SelectedDateChanged
        Try
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Stale Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Stale from date should be less than stale to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub raddpCreationToDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationToDate.SelectedDateChanged
        Try
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Stale Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Stale from date should be less than stale to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function GetBeforeClearingApprovalStatus(ByVal InstructionID As String) As String
        Dim LastStatus As String = Nothing

        Dim objICInstActivity As New ICInstructionActivityQuery("objICInstActivity")
        'Dim qryObjICInstActivity As New ICInstructionActivityQuery("qryObjICInstActivity")

        objICInstActivity.Select(objICInstActivity.FromStatus)

        objICInstActivity.Where(objICInstActivity.ToStatus = "38" And objICInstActivity.FromStatus <> "39" And objICInstActivity.FromStatus <> "40" And objICInstActivity.FromStatus <> "52")
        objICInstActivity.OrderBy(objICInstActivity.ActionDate.Descending)
        objICInstActivity.es.Top = 1
        LastStatus = objICInstActivity.LoadDataTable.Rows(0)(0).ToString


        Return LastStatus

    End Function
End Class
