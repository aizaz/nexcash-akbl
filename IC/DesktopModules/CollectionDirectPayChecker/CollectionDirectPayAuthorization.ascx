﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CollectionDirectPayAuthorization.ascx.vb" Inherits="DesktopModules_CollectionDirectPayChecker_CollectionDirectPayAuthorization" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:TextBoxSetting BehaviorID="reTnxCount" Validation-IsRequired="false" EmptyMessage="Transaction Count"
        ErrorMessage="Transaction Count">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTnxCount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reTotalAmount" Validation-IsRequired="false"
        EmptyMessage="Total Amount" ErrorMessage="Total Amount">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTotalAmount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reCurrency" Validation-IsRequired="false" EmptyMessage="Currency"
        ErrorMessage="Currency">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCurrency" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rePaymentMode" Validation-IsRequired="false"
        EmptyMessage="Payment Mode" ErrorMessage="Payment Mode">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPaymentMode" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reAvailableBalance" Validation-IsRequired="false"
        EmptyMessage="Available Balance" ErrorMessage="Available Balance">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAvailableBalance" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reInstructionNo" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Enter Instruction No." ValidationExpression="[0-9]{0,8}">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstructionNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reInstrumentNo" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Enter Instrument No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstrumentNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reReferenceNo" Validation-IsRequired="false"
        EmptyMessage="Enter Reference No." ErrorMessage="Enter Reference No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reBaneName" Validation-IsRequired="false" EmptyMessage="Enter Baneficiary Name"
        ErrorMessage="Enter Baneficiary Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneName" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reAmount" Validation-IsRequired="false" EmptyMessage="Enter Amount"
        ErrorMessage="Enter Amount">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAmount" />
        </TargetControls>
    </telerik:TextBoxSetting>
       <telerik:TextBoxSetting BehaviorID="reRemarks" Validation-IsRequired="true" EmptyMessage="Enter Remarks"
        ErrorMessage="Enter Remarks" Validation-ValidationGroup="Remarks">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRemarks" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<script type="text/javascript">

    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    

       
    $(document).ready(function () {

        // Dialog
        $('#urldialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#loading').hide();



            },
            resizable: false
        });

    });
    function showurldialog(title, url, modal, width, height) {
        //  alert(modal);
        $('#urldialog').dialog("option", "title", title);
        $('#urldialog').dialog("option", "modal", modal);
        $('#urldialog').dialog("option", "width", width);
        $('#urldialog').dialog("option", "height", height);
        $('#urldialog').dialog('open');
        $('#dialogiframe').hide();
        $('#loading').show();
        $('#dialogiframe').attr("src", url);
        $('#dialogiframe').load(function () {
            $('#loading').hide();
            $('#dialogiframe').show();
        });
        return false;

    };

    function CloseIframe() {
        $('#urldialog').dialog('close');
    }
</script>
<script language="javascript" type="text/javascript">
    function AdjustWidth(ddl) {
        var maxWidth = 0;
        var Counter = 0;
        for (var i = 0; i < ddl.length; i++) {
            if (ddl.options[i].text.length > maxWidth) {
                Counter = Counter + 1;
                maxWidth = ddl.options[i].text.length;
            }
        }
        if (Counter = 1) { ddl.style.width = "250px"; }

        else {
            alert(Counter);
            ddl.style.width = maxWidth + "px";
        }
        //-->
    }
    function conApprove() {
        if (window.confirm("Are you sure you wish to approve selected invoice(s)?") == true) {
            var btnVerify = document.getElementById('<%=btnApprove.ClientID%>')
            btnVerify.value = "Processing...";
            btnVerify.disabled = true;

            var a = btnVerify.name;
            __doPostBack(a, '');
            return true;
        }
        else {
            return false;
        }
    }


    function conReject() {
        if (window.confirm("Are you sure you wish to reject selected invoice(s)?") == true) {
            var btnSendForAmend = document.getElementById('<%=btnReject.ClientID%>')
            btnSendForAmend.value = "Processing...";
            btnSendForAmend.disabled = true;

            var a = btnSendForAmend.name;
            __doPostBack(a, '');
            return true;
        }
        else {
            return false;
        }
    }
</script>
<style type="text/css">
.ctrDropDown{
    width:250px;
   
}
.ctrDropDownClick{
    
    width:600px;
 
}
.plainDropDown{
    width:250px;
   
}
</style>
<%--<asp:Label ID="lblscript" runat="server" Text=""></asp:Label>--%>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Direct Pay Authorization Queue" 
                            CssClass="headingblue"></asp:Label>
                           <br /><br />
                        <asp:Label ID="lblMessageForQueue" runat="server" Text="" CssClass="" ForeColor="Red" >
                        </asp:Label>
                        Note:&nbsp; Fields marked as
                        <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        &nbsp;are required.
                    </td>
                </tr>
                 <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:HiddenField ID="hfCollectionID" runat="server" />
                    </td>
                </tr>
                  <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <table id="tblMain" runat="server" style="width:100%">
                            <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%">
                        &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Back" CssClass="btnCancel" Width="75px" CausesValidation="False"
                 />
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Label ID="lblNRF" runat="server" Text="No Record Found." CssClass="headingblue"
                            Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <telerik:RadGrid ID="gvAuthorization" runat="server" AllowPaging="True" Width="100%" CssClass="RadGrid"
                            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                            PageSize="100">
                             <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="true" />
                            </ClientSettings>
                            <MasterTableView TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                     <%--<telerik:GridTemplateColumn HeaderText="Collection No.">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbCollectionID" runat="server" Text='<%#Eval("CollectionID") %>'
                                                ToolTip="Collection No." ForeColor="Blue" Enabled="false"></asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>
                                     <telerik:GridBoundColumn DataField="CollectionID" HeaderText="Collection No" SortExpression="CollectionID">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CollectionDate" HeaderText="Collection Date" SortExpression="CollectionDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                    </telerik:GridBoundColumn>
                           
                                    <%--<telerik:GridBoundColumn DataField="CollectionAccountNo" HeaderText="Collection AccountNo" SortExpression="CollectionAccountNo">
                                    </telerik:GridBoundColumn>--%>
                                <%--     <telerik:GridBoundColumn DataField="BeneficiaryAddress" HeaderText="Bene Address" SortExpression="BeneficiaryAddress">
                                    </telerik:GridBoundColumn>--%>
                                    <telerik:GridBoundColumn DataField="CollectionAmount" HeaderText="Amount" SortExpression="CollectionAmount"
                                        HtmlEncode="false" DataFormatString="{0:N2}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="AmountAfterDueDate" HeaderText="Amount After DueDate" SortExpression="AmountAfterDueDate"
                                        HtmlEncode="false" DataFormatString="{0:N2}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PayersAccountNo" HeaderText="Debit Account No" SortExpression="PayersAccountNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="UserName" HeaderText="Created By" SortExpression="UserName">
                                    </telerik:GridBoundColumn>  
                                             <telerik:GridBoundColumn DataField="DueDate" HeaderText="Due Date" SortExpression="DueDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                    </telerik:GridBoundColumn> 
                                           <%--<telerik:GridBoundColumn DataField="CollectionDirectPayCreatedDate" HeaderText="Created Date" SortExpression="CollectionDirectPayCreatedDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                    </telerik:GridBoundColumn> --%>
                                   <telerik:GridBoundColumn DataField="ReferenceField1" HeaderText="ReferenceField1" SortExpression="ReferenceField1">
                                    </telerik:GridBoundColumn>  
                                    <telerik:GridBoundColumn DataField="ReferenceField2" HeaderText="ReferenceField2" SortExpression="ReferenceField2">
                                    </telerik:GridBoundColumn> 
                                      <telerik:GridBoundColumn DataField="ReferenceField3" HeaderText="ReferenceField3" SortExpression="ReferenceField3">
                                    </telerik:GridBoundColumn> 
                                    <telerik:GridBoundColumn DataField="ReferenceField4" HeaderText="ReferenceField4" SortExpression="ReferenceField4">
                                    </telerik:GridBoundColumn> 
                                      <telerik:GridBoundColumn DataField="ReferenceField5" HeaderText="ReferenceField5" SortExpression="ReferenceField5">
                                    </telerik:GridBoundColumn> 
                                    <telerik:GridTemplateColumn HeaderText="View Details">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ibButton" runat="server" ImageUrl="~/images/view.gif" ToolTip="Payment Details"
                                                CausesValidation="false" CommandArgument='<%#Eval("CollectionID")%>' CommandName="ShowDetail"></asp:ImageButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <%--<ItemStyle CssClass="rgRow" />--%>
                               <HeaderStyle CssClass="GridHeader" />
                            <ItemStyle CssClass="GridItem" />
                            <AlternatingItemStyle CssClass="GridItem" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                        <%--<telerik:GridTemplateColumn HeaderText="Collection No.">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbCollectionID" runat="server" Text='<%#Eval("CollectionID") %>'
                                                ToolTip="Collection No." ForeColor="Blue" Enabled="false"></asp:LinkButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>
                    </td>
                </tr>
                        </table>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <table id="tblPaymaentDetail" runat="server" style="width:100%">
                            <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%">
                        &nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        &nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                       <table id="tblDetail" runat="server" style="width:100%">
                           <tr align="left" valign="top" style="width: 100%">
                               <td align="left" valign="top" style="width: 25%">
                            <asp:Label ID="lblOnlineFormPreview" runat="server" Text="Payment Details"
                                CssClass="headingblue"></asp:Label>
                               </td>
                               <td align="left" valign="top" style="width: 25%"></td>
                               <td align="left" valign="top" style="width: 25%"></td>
                               <td align="left" valign="top" style="width: 25%"></td>
                           </tr>
                           <tr align="left" valign="top" style="width: 100%">
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                           </tr>
                           <tr align="left" valign="top" style="width: 100%">
                               <td align="left" valign="top" style="width: 25%">
                                                    <asp:Label ID="lblCollectionAccount" runat="server" CssClass="lbl" Text="Collection Account"></asp:Label>
                                                </td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                               <td align="left" valign="top" style="width: 25%">
                                                    <asp:Label ID="lblCollectionCompany" runat="server" CssClass="lbl" Text="Collection Company"></asp:Label>
                                                </td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                           </tr>
                           <tr align="left" valign="top" style="width: 100%">
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                           </tr>
                           <tr align="left" valign="top" style="width: 100%">
                               <td align="left" valign="top" style="width: 25%">
                                                    <asp:Label ID="lblprvCollectionAccount" runat="server" CssClass="lblPreview"></asp:Label>
                                                </td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                               <td align="left" valign="top" style="width: 25%">
                                                    <asp:Label ID="lblprvCollectionCompany" runat="server" CssClass="lblPreview"></asp:Label>
                                                </td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                           </tr>
                           <tr align="left" valign="top" style="width: 100%">
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                           </tr>
                           <tr align="left" valign="top" style="width: 100%">
                               <td align="left" valign="top" style="width: 25%">
                                                    <asp:Label ID="lblDebitAccount" runat="server" CssClass="lbl" Text="Debit Account"></asp:Label>
                                                </td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                               <td align="left" valign="top" style="width: 25%">
                                                    <asp:Label ID="lblAmount" runat="server" CssClass="lbl" Text="Amount"></asp:Label>
                                                </td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                           </tr>
                           <tr align="left" valign="top" style="width: 100%">
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                           </tr>
                           <tr align="left" valign="top" style="width: 100%">
                               <td align="left" valign="top" style="width: 25%">
                                                    <asp:Label ID="lblprvDebitAccount" runat="server" CssClass="lblPreview"></asp:Label>
                                                </td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                               <td align="left" valign="top" style="width: 25%">
                                                    <asp:Label ID="lblprvAmount" runat="server" CssClass="lblPreview"></asp:Label>
                                                </td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                           </tr>
                           <tr align="left" valign="top" style="width: 100%">
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                               <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                           </tr>
                       </table>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%">
                        <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btnCancel" Width="75px" ValidationGroup="Approve" OnClientClick ="javascript: return conApprove();"
                 />
                        <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btnCancel" Width="75px" OnClientClick ="javascript: return conReject();" ValidationGroup="Reject"
                 />
                        <asp:Button ID="btnBackDetail" runat="server" Text="Back" CssClass="btnCancel" Width="75px" CausesValidation="False"
                 />
                    </td>
                </tr>
                        </table>
                    </td>
                </tr>
                 </table>
        </td>
    </tr>
</table>
<div id="urldialog" title="">
    <iframe id='dialogiframe' frameborder="0" allowtransparency="true" src="" style="border-style: none;
        width: 100%; height: 100%; overflow-x: hidden; background-color: transparent;">
    </iframe>
    <div id='loading' style="display: none; margin: 0px auto; text-align: center; width: 100%;
        height: 100%; position: relative">
        <img src='../../../../images/progressbar.gif' style="left: 45%; top: 50%; position: absolute;" />
    </div>
</div>
