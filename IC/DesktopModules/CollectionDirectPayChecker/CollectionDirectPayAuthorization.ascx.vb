﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports EntitySpaces.Core
Partial Class DesktopModules_CollectionDirectPayChecker_CollectionDirectPayAuthorization
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrICAssignUserRolsID As New ArrayList
    Private ArrICAssignedOfficeID As New ArrayList
    Private ArrICAssignedStatus As New ArrayList
    Private ArrICAssignedPaymentModes As New ArrayList
    Private htRights As Hashtable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()
            lblMessageForQueue.Style.Add("display", "none")
            If Page.IsPostBack = False Then
                btnApprove.Attributes.Item("onclick") = "if (Page_ClientValidate('Approve')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnApprove, Nothing).ToString() & " } "
                btnReject.Attributes.Item("onclick") = "if (Page_ClientValidate('Reject')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnReject, Nothing).ToString() & " } "
                hfCollectionID.Value = Nothing
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                FillDesignedDtByAPNatureByAssignedRole()
                tblMain.Style.Remove("display")
                tblPaymaentDetail.Style.Add("display", "none")
                ClearPaymentPreview()
                Dim dtPageLoad As New DataTable
                dtPageLoad = DirectCast(ViewState("AccountNumber"), DataTable)
                If dtPageLoad.Rows.Count > 0 Then
                    LoadgvAccountPaymentNatureProductType(True)
                Else
                    UIUtilities.ShowDialog(Me, "Error", "You do not have access to any transactional data. Please contact Soneri Trans@ct Administrator.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ClearPaymentPreview()
        lblprvAmount.Text = ""
        lblprvCollectionAccount.Text = ""
        lblprvCollectionCompany.Text = ""
        lblprvDebitAccount.Text = ""
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Direct Pay Authorization") = True Then
                    If Not ArrICAssignUserRolsID.Contains(RoleID.ToString) Then
                        ArrICAssignUserRolsID.Add(RoleID.ToString)
                    End If
                End If
            Next
        End If
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Direct Pay Authorization")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub




    Private Sub FillDesignedDtByAPNatureByAssignedRole()
        Dim dtAssignedAPNatureOfficeID As New DataTable
        dtAssignedAPNatureOfficeID.Rows.Clear()
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserSecond(Me.UserId.ToString, ArrICAssignUserRolsID)
        ViewState("AccountNumber") = Nothing
        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID

    End Sub

    Private Sub LoadgvAccountPaymentNatureProductType(ByVal DoDataBind As Boolean)
        Try
            Dim dtAccountNumber As New DataTable
            dtAccountNumber = ViewState("AccountNumber")
            ICCollectionsController.GetAllCollectionForRadGridForAuthorizationDirectPay(dtAccountNumber, Me.gvAuthorization.CurrentPageIndex + 1, Me.gvAuthorization.PageSize, Me.gvAuthorization, DoDataBind, Me.UserId.ToString, "2")
            If gvAuthorization.Items.Count > 0 Then
                gvAuthorization.Visible = True
                lblNRF.Visible = False
                If CBool(htRights("Approve")) = True Or CBool(htRights("Reject")) = True Then
                    gvAuthorization.Columns(10).Visible = True
                Else
                    gvAuthorization.Columns(10).Visible = False
                End If
            Else
                lblNRF.Visible = True
                gvAuthorization.Visible = False

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    'Private Sub SetJavaScript()
    '    Dim imgBtn As ImageButton
    '    'If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Then
    '    Dim AppPath As String = DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
    '    For Each gvAuthorizationRow As GridDataItem In gvAuthorization.Items
    '        imgBtn = New ImageButton
    '        imgBtn = DirectCast(gvAuthorizationRow.Cells(11).FindControl("ibButton"), ImageButton)
    '        imgBtn.OnClientClick = "showurldialog('Instruction Details','" & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & imgBtn.CommandArgument.ToString() & "' ,true,700,650);return false;"
    '    Next
    '    'End If
    'End Sub



    Private Function CheckgvInstructionAuthentication() As Boolean
        Try

            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVInstruction In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVInstruction.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub gvAuthorization_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles gvAuthorization.ItemCommand
        Try
            If e.CommandName = "ShowDetail" Then
                hfCollectionID.Value = Nothing
                hfCollectionID.Value = e.CommandArgument.ToString
                tblMain.Style.Add("display", "none")
                tblPaymaentDetail.Style.Remove("display")
                btnApprove.Visible = CBool(htRights("Approve"))
                btnReject.Visible = CBool(htRights("Reject"))
                SetPaymentDetails(e.CommandArgument.ToString)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetPaymentDetails(ByVal CollectionID As String)
        Dim objICCollections As New ICCollections
        objICCollections.LoadByPrimaryKey(CollectionID)
        lblprvCollectionAccount.Text = objICCollections.CollectionAccountNo
        lblprvCollectionCompany.Text = objICCollections.UpToICCollectionNatureByCollectionNatureCode.UpToICCompanyByCompanyCode.CompanyName
        lblprvDebitAccount.Text = objICCollections.PayersAccountNo
        If objICCollections.DueDate IsNot Nothing Then
            If CDate(objICCollections.DueDate) >= Date.Now Then
                lblprvAmount.Text = objICCollections.CollectionAmount.ToString
            Else
                If objICCollections.AmountAfterDueDate IsNot Nothing Then
                    lblprvAmount.Text = objICCollections.AmountAfterDueDate.ToString
                Else
                    lblprvAmount.Text = objICCollections.CollectionAmount.ToString
                End If
            End If
        Else
            lblprvAmount.Text = objICCollections.CollectionAmount.ToString
        End If
        'If objICCollections.UpToICCollectionNatureByCollectionNatureCode.IsPartialCollectionAllowed = True Then
        '    If objICCollections.DueDate IsNot Nothing Then
        '        If CDate(objICCollections.DueDate) >= Date.Now Then
        '            If objICCollections.UpToICCollectionNatureByCollectionNatureCode.AllowedPartialCollectionType = "Over" Then
        '            ElseIf objICCollections.UpToICCollectionNatureByCollectionNatureCode.AllowedPartialCollectionType = "Over" Then

        '            Else
        '                lblprvAmount.Text = objICCollections.CollectionAmount.ToString("N2")
        '            End If

        '        Else
        '            If objICCollections.AmountAfterDueDate IsNot Nothing Then
        '                lblprvAmount.Text = objICCollections.AmountAfterDueDate.ToString("N2")
        '            Else
        '                lblprvAmount.Text = objICCollections.CollectionAmount.ToString("N2")
        '            End If
        '        End If
        '    Else
        '        lblprvAmount.Text = objICCollections.CollectionAmount.ToString("N2")
        '    End If
        'Else

        'End If
    End Sub
    Protected Sub gvAuthorization_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAuthorization.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAuthorization_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemDataBound
        'Dim chkProcessAll As CheckBox

        'If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
        '    chkProcessAll = New CheckBox
        '    chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
        '    chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','0');"
        'End If
        'Dim imgBtn As LinkButton
        'Dim AppPath As String = DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
        'If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
        '    Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        '    imgBtn = New LinkButton
        '    imgBtn = DirectCast(item.Cells(1).FindControl("lbInstructionID"), LinkButton)
        '    imgBtn.OnClientClick = "showurldialog('Instruction Details',' " & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & item.GetDataKeyValue("InstructionID").ToString() & "' ,true,700,650);return false;"
        '    If CBool(htRights("View Instructions Detail")) = True Then
        '        imgBtn.Enabled = True
        '    End If
        '    imgBtn = New LinkButton
        '    imgBtn = DirectCast(item.Cells(12).FindControl("lbSignatures"), LinkButton)
        '    imgBtn.OnClientClick = "showurldialog('Account Signatures',' " & AppPath & "/ShowSiignatures.aspx?an=" & item.GetDataKeyValue("InstructionID").ToString() & "' ,true,700,650);return false;"



        '    If item.GetDataKeyValue("IsAmendmentComplete") = True Then
        '        gvAuthorization.AlternatingItemStyle.CssClass = "GridItemComplete"
        '        'gvAmendment.ItemStyle.BackColor = Color.Red
        '        e.Item.CssClass = "GridItemComplete"
        '    Else
        '        gvAuthorization.AlternatingItemStyle.CssClass = "GridItem"
        '        'gvAmendment.ItemStyle.BackColor = Color.Red
        '        e.Item.CssClass = "GridItem"
        '    End If
        'End If
    End Sub
    Private Function CheckgvClientRoleCheckedForProcessAll() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub gvAuthorization_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAuthorization.NeedDataSource
        Try
            LoadgvAccountPaymentNatureProductType(False)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL(41))
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnBackDetail_Click(sender As Object, e As EventArgs) Handles btnBackDetail.Click
        If Page.IsValid Then
            Try
                tblPaymaentDetail.Style.Add("display", "none")
                ClearPaymentPreview()
                tblMain.Style.Remove("display")
                FillDesignedDtByAPNatureByAssignedRole()
                LoadgvAccountPaymentNatureProductType(True)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        If Page.IsValid Then
            Try
                Dim objICCollections As New ICCollections
                Dim ReqSqlStr As String = Nothing
                Dim dt As New DataTable
                ''Update Status
                objICCollections.LoadByPrimaryKey(hfCollectionID.Value.ToString)
                ICCollectionsController.UpdateICCollectionsStatus(hfCollectionID.Value.ToString, objICCollections.Status.ToString, "11", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                ''update rejected authority credential
                objICCollections = New ICCollections
                objICCollections.LoadByPrimaryKey(hfCollectionID.Value.ToString)
                objICCollections.DirectPayRejectedDateTime = Date.Now
                objICCollections.DirectPayRejectedBy = Me.UserInfo.UserID.ToString
                ICCollectionsController.UpdateICCollections(objICCollections, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)

                ''update invoice db record status if exists validate by file id in main collection object


                ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlStr, Me.UserInfo.UserID.ToString, objICCollections.FileID.ToString, "Select", "Master", objICCollections.ReferenceField1, objICCollections.ReferenceField2, objICCollections.ReferenceField3, objICCollections.ReferenceField4, objICCollections.ReferenceField5, Nothing, Nothing)
                objICCollections = New ICCollections
                objICCollections.LoadByPrimaryKey(hfCollectionID.Value.ToString)
                dt = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlStr)
                ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollections.ReferenceField1, objICCollections.ReferenceField2, objICCollections.ReferenceField3, objICCollections.ReferenceField4, objICCollections.ReferenceField5, dt.Rows(0)("Status").ToString, objICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)

                UIUtilities.ShowDialog(Me, "Direct Pay Autorization", "Invoice rejected successfully.", Dialogmessagetype.Success)
                ClearPaymentPreview()
                tblPaymaentDetail.Style.Add("display", "none")
                tblMain.Style.Remove("display")
                FillDesignedDtByAPNatureByAssignedRole()
                LoadgvAccountPaymentNatureProductType(True)
                Exit Sub
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        If Page.IsValid Then
            Try

                Dim objICCollections As New ICCollections
                Dim OrigTranType, RespondingTranType, VoucherType, InstrumentType, ReversalTranType As String
                Dim TranStat As CBUtilities.TransactionStatus
                Dim ReqSqlStr As String = Nothing
                Dim dt As New DataTable
                objICCollections.LoadByPrimaryKey(hfCollectionID.Value.ToString)
                If objICCollections.CollectionDirectPayCreatedBy = Me.UserInfo.UserID.ToString Then
                    UIUtilities.ShowDialog(Me, "Direct Pay Authorization", "Invoice must be approved by other than maker", Dialogmessagetype.Failure)
                    ClearPaymentPreview()
                    tblPaymaentDetail.Style.Add("display", "none")
                    tblMain.Style.Remove("display")
                    FillDesignedDtByAPNatureByAssignedRole()
                    LoadgvAccountPaymentNatureProductType(True)
                    Exit Sub
                End If
                ''Set TranTypes
                OrigTranType = ICUtilities.GetCollectionSettingValue("OriginatingTranType")
                RespondingTranType = ICUtilities.GetCollectionSettingValue("RespondingTranType")
                InstrumentType = ICUtilities.GetCollectionSettingValue("InstrumentType")
                VoucherType = ICUtilities.GetCollectionSettingValue("VoucherType")
                ReversalTranType = ICUtilities.GetCollectionSettingValue("ReversalTranType")
                objICCollections = New ICCollections
                objICCollections.LoadByPrimaryKey(hfCollectionID.Value.ToString)


                TranStat = CBUtilities.MoveCollectionFunds(objICCollections.CollectionID.ToString, objICCollections.PayersAccountNo, objICCollections.PayersAccountBranchCode, objICCollections.PayersAccountCurrency, Nothing, Nothing, Nothing, objICCollections.PayersAccountType, objICCollections.CollectionAccountNo, objICCollections.CollectionAccountBranchCode, objICCollections.CollectionAccountCurrency, Nothing, Nothing, Nothing, objICCollections.CollectionAccountType, OrigTranType, RespondingTranType, VoucherType, InstrumentType, ReversalTranType, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, CDbl(lblprvAmount.Text), "Collection")
                If TranStat = CBUtilities.TransactionStatus.Successfull Then
                    objICCollections = New ICCollections
                    objICCollections.LoadByPrimaryKey(hfCollectionID.Value.ToString)
                    objICCollections.CollectionDirectPayApprovedBy = Me.UserInfo.UserID.ToString
                    objICCollections.CollectionDirectPayApproveDateTime = Date.Now
                    objICCollections.CollectionTransferedDate = Date.Now
                    ICCollectionsController.UpdateICCollections(objICCollections, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                    ''Update Status
                    objICCollections = New ICCollections
                    objICCollections.LoadByPrimaryKey(hfCollectionID.Value.ToString)
                    ICCollectionsController.UpdateICCollectionsStatus(hfCollectionID.Value.ToString, objICCollections.Status.ToString, "3", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                    ''update invoice db record status if exists validate by file id in main collection object


                    ICCollectionSQLController.GetSQLCommandFromReferenceFieldsByCommandType(objICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID.ToString, ReqSqlStr, Me.UserInfo.UserID.ToString, objICCollections.FileID.ToString, "Select", "Master", objICCollections.ReferenceField1, objICCollections.ReferenceField2, objICCollections.ReferenceField3, objICCollections.ReferenceField4, objICCollections.ReferenceField5, Nothing, Nothing)

                    dt = New DataTable
                    objICCollections = New ICCollections
                    objICCollections.LoadByPrimaryKey(hfCollectionID.Value.ToString)
                    dt = ICCollectionSQLController.GetInvoiceRecordBySQLStatement(ReqSqlStr)
                    If dt.Rows.Count > 0 Then
                        ICCollectionSQLController.UpdateInvoiceRecordsStatusByReferenceFields(objICCollections.UpToICCollectionNatureByCollectionNatureCode.InvoiceTemplateID, objICCollections.ReferenceField1, objICCollections.ReferenceField2, objICCollections.ReferenceField3, objICCollections.ReferenceField4, objICCollections.ReferenceField5, dt.Rows(0)("Status").ToString, objICCollections.UpToICCollectionsStatusByStatus.CollectionStatusName.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                    End If


                    UIUtilities.ShowDialog(Me, "Direct Pay Autorization", "Invoice approved successfully.", Dialogmessagetype.Success)
                    ClearPaymentPreview()
                    tblPaymaentDetail.Style.Add("display", "none")
                    tblMain.Style.Remove("display")
                    FillDesignedDtByAPNatureByAssignedRole()
                    LoadgvAccountPaymentNatureProductType(True)
                Else

                    UIUtilities.ShowDialog(Me, "Direct Pay Autorization", "Invoice not approved successfully due to " & CBUtilities.GetErrorMessageFromOpenEndedFundsTransferResponseCode(TranStat), Dialogmessagetype.Success)
                    Exit Sub

                End If


                Exit Sub
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
End Class
