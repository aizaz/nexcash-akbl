﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_InstructionTrackingQueue_ScheduleTransaction
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private InstructionID As String
    Private htRights As Hashtable

#Region "Page load"
    Protected Sub DesktopModules_Bank_SaveBank_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            InstructionID = Request.QueryString("InstructionID").ToString().DecryptString2()


            If Page.IsPostBack = False Then
                txtStartDate.MinDate = Date.Now.AddDays(1)
                txtStartDate.SelectedDate = Date.Now.AddDays(1)
                txtEndDate.MinDate = Date.Now.AddDays(1)
                txtEndDate.SelectedDate = Date.Now.AddDays(1)

                ViewState("htRights") = Nothing
                GetPageAccessRights()
                If InstructionID.ToString() <> "0" Then
                    Clear()
                    gvInstructionDetail.DataSource = Nothing
                    gvInstructionDetail.DataSource = ICInstructionController.GetInstructionDetailByInstructionID(InstructionID)
                    gvInstructionDetail.DataBind()
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Allow Schedule"))
                End If

            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Instruction Tracking")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        txtScheduleTitle.Text = ""
        txtNoOfDays.Text = ""
        ddlSpecificDay.ClearSelection()
        ddlScheduleFrequency.ClearSelection()
        radTimePicker.SelectedDate = Nothing
        tblDayType.Style.Add("display", "none")
        tblNoOfDays.Style.Add("display", "none")
        tblSpecificDay.Style.Add("display", "none")
        txtStartDate.SelectedDate = Now.Date.AddDays(1)
        txtEndDate.SelectedDate = Now.Date.AddDays(1)
    End Sub
#End Region

#Region "Buttons"

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim objICInstruction As New ICInstruction
                Dim objICBeneficiary As New ICBeneficiary
                Dim objICScheduleInstruction As New ICScheduleTransactions
                Dim dtBestPossibleDate As New DataTable
                Dim StrBestPossible As String
                Dim ScheduleID As Integer = 0
                Dim ActionStr As String = ""
                Dim Count As Integer = 0
                If txtStartDate.SelectedDate Is Nothing Then
                    UIUtilities.ShowDialog(Me, "Error", "Please select Schedule Start Date", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If txtEndDate.SelectedDate Is Nothing Then
                    UIUtilities.ShowDialog(Me, "Error", "Please select Schedule End Date", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If txtStartDate.SelectedDate > txtEndDate.SelectedDate Then
                    UIUtilities.ShowDialog(Me, "Error", "Schedule start must be greater than or equal to Schedule To Date", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If ddlScheduleFrequency.SelectedValue.ToString = "Monthly" Then
                    Dim MonthInterval As Integer = DateDiff(DateInterval.Month, CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate))
                    If MonthInterval <= 0 Then
                        UIUtilities.ShowDialog(Me, "Error", "Schedule End Date must be greater than or equal to one month from Schedule Start Date", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    If rblDaysOfMonth.SelectedValue.ToString = "Specific Day" Then
                        Dim DayInterval As Integer = DateDiff(DateInterval.Day, CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate))
                        If DayInterval < CInt(ddlSpecificDay.SelectedValue) Then
                            UIUtilities.ShowDialog(Me, "Error", "Schedule End Date must be greater than or equal to Specific Day", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    End If
                ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Number Of Days" Then
                    Dim MonthInterval As Integer = DateDiff(DateInterval.Day, CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate))
                    If MonthInterval < CInt(txtNoOfDays.Text) Then
                        UIUtilities.ShowDialog(Me, "Error", "Schedule End Date must be greater than or equal to No of Days", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If




                objICInstruction.LoadByPrimaryKey(InstructionID)
                objICBeneficiary.LoadByPrimaryKey(objICInstruction.BeneficiaryCode)

                ActionStr += "Instruction  via [ " & objICInstruction.AcquisitionMode & " ] for beneficiary [ " & objICInstruction.BeneficiaryName.ToString & " ] of client [ " & objICInstruction.UpToICCompanyByCompanyCode.CompanyName & " ], "
                ActionStr += "of amount [ " & objICInstruction.Amount.ToString & "] [ " & objICInstruction.AmountInWords.ToString & " ] added. Client account number was [ " & objICInstruction.ClientAccountNo & " ],"
                ActionStr += " branch code [" & objICInstruction.ClientAccountBranchCode & " ], currency [" & objICInstruction.ClientAccountCurrency & " ]."
                ActionStr += " Product type [ " & objICInstruction.ProductTypeCode & " ], Disbursement mode [ " & objICInstruction.PaymentMode & " ], Payment nature [ " & objICInstruction.PaymentNatureCode & " ]."




                If ddlScheduleFrequency.SelectedValue.ToString = "Monthly" Then
                    If rblDaysOfMonth.SelectedValue.ToString = "First Day" Then
                        StrBestPossible = ICScheduleInstructionController.GetBestPossibleDay(CDate(txtStartDate.SelectedDate), "First Day", "")
                        dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, StrBestPossible, "First Day")
                    ElseIf rblDaysOfMonth.SelectedValue.ToString = "Last Day" Then
                        Dim MonthInterval As Integer = DateDiff(DateInterval.Month, CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate))
                        StrBestPossible = MonthInterval.ToString
                        dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, StrBestPossible, "Last Day")
                    ElseIf rblDaysOfMonth.SelectedValue.ToString = "Specific Day" Then
                        StrBestPossible = ICScheduleInstructionController.GetBestPossibleDay(CDate(txtStartDate.SelectedDate), "Specific Day", ddlSpecificDay.SelectedValue.ToString)
                        dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, StrBestPossible, "Specific Day")
                    End If
                ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Daily" Then
                    dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, "", "")
                ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Number Of Days" Then
                    StrBestPossible = ICScheduleInstructionController.GetBestPossibleDay(CDate(txtStartDate.SelectedDate), "Number Of Days", txtNoOfDays.Text.ToString)
                    dtBestPossibleDate = ICScheduleInstructionController.GetValueDateforRecurring(CDate(txtStartDate.SelectedDate), CDate(txtEndDate.SelectedDate), ddlScheduleFrequency.SelectedValue.ToString, "", "")

                End If

                If dtBestPossibleDate.Rows.Count > 0 Then
                    objICScheduleInstruction = GetScheduleTransaction(objICInstruction, dtBestPossibleDate.Rows.Count)
                    ScheduleID = ICScheduleInstructionController.AddScheduleInstruction(objICScheduleInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    For Each drScheduleInstruction As DataRow In dtBestPossibleDate.Rows
                        objICInstruction.ScheduleTransactionID = ScheduleID
                        objICInstruction.ScheduleTransactionTime = objICScheduleInstruction.TransactionTime
                        objICInstruction.IsCheduleTransactionProcessed = False
                        objICInstruction.ValueDate = CDate(drScheduleInstruction("ValueDates"))
                        objICInstruction.Status = 1
                        objICInstruction.LastStatus = 1
                        objICInstruction.InstructionID = ICInstructionController.AddInstruction(objICInstruction, objICBeneficiary, Me.UserId.ToString, Me.UserInfo.Username.ToString, ActionStr.ToString)
                        Count = Count + 1
                        Dim APNLocation As String = Nothing
                        APNLocation = objICInstruction.ClientAccountNo + "-" + objICInstruction.ClientAccountBranchCode + "-" + objICInstruction.ClientAccountCurrency + "-" + objICInstruction.PaymentNatureCode + "-" + objICInstruction.CreatedOfficeCode.ToString
                        EmailUtilities.SingleInstructioninputtedbyUser(objICInstruction.InstructionID.ToString, Me.UserInfo.Username.ToString, APNLocation)
                        SMSUtilities.SingleInstructioninputtedbyUser(objICInstruction.InstructionID.ToString, APNLocation)
                    Next
                    UIUtilities.ShowDialog(Me, "Schedule Transactions", "[ " & Count & " ] Schedule Instruction added successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else

                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

 

#End Region


    Protected Sub ddlScheduleFrequency_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlScheduleFrequency.SelectedIndexChanged
        Try
            tblDayType.Style.Add("display", "none")
            tblNoOfDays.Style.Add("display", "none")
            tblSpecificDay.Style.Add("display", "none")
            txtNoOfDays.Text = ""
            ddlSpecificDay.ClearSelection()
            rfvddlSpecificDay.Enabled = False
            radNoOfDay.Enabled = False

            rfvrblrblDaysOfMonth.Enabled = False
            radNoOfDay.GetSettingByBehaviorID("reNoOfDays").Validation.IsRequired = False
            txtStartDate.SelectedDate = Date.Now.AddDays(1)
            txtEndDate.SelectedDate = Date.Now.AddDays(1)

            If ddlScheduleFrequency.SelectedValue.ToString = "Daily" Then
                txtEndDate.SelectedDate = Nothing
                txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate)
            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Monthly" Then
           
                txtEndDate.SelectedDate = Nothing
                txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddMonths(1).Date
                tblDayType.Style.Remove("display")
                rfvrblrblDaysOfMonth.Enabled = True
            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Number Of Days" Then
                tblNoOfDays.Style.Remove("display")
                radNoOfDay.Enabled = True
                radNoOfDay.GetSettingByBehaviorID("reNoOfDays").Validation.IsRequired = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub rblDaysOfMonth_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblDaysOfMonth.SelectedIndexChanged
        Try
            tblSpecificDay.Style.Add("display", "none")
            rfvddlSpecificDay.Enabled = False
            If rblDaysOfMonth.SelectedValue.ToString = "Specific Day" Then
                tblSpecificDay.Style.Remove("display")
                rfvddlSpecificDay.Enabled = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function GetScheduleTransaction(ByVal objICInstruction As ICInstruction, ByVal TransactionCount As Integer) As ICScheduleTransactions
        Dim objICScheduleTransaction As New ICScheduleTransactions
        objICScheduleTransaction.CompanyCode = objICInstruction.CompanyCode
        objICScheduleTransaction.ClientAccountNo = objICInstruction.ClientAccountNo
        objICScheduleTransaction.ClientAccountBranchCode = objICInstruction.ClientAccountBranchCode
        objICScheduleTransaction.ClientAccountCurrency = objICInstruction.ClientAccountCurrency
        objICScheduleTransaction.PaymentNatureCode = objICInstruction.PaymentNatureCode
        objICScheduleTransaction.ProductTypeCode = objICInstruction.ProductTypeCode
        objICScheduleTransaction.BeneficaryName = objICInstruction.BeneficiaryName
        objICScheduleTransaction.BeneficiaryAccountNo = objICInstruction.BeneficiaryAccountNo
        objICScheduleTransaction.BeneficiaryAccountBranchCode = objICInstruction.BeneAccountBranchCode
        objICScheduleTransaction.BeneficiaryAccountCurrency = objICInstruction.BeneAccountCurrency
        objICScheduleTransaction.CreatedDate = Date.Now
        objICScheduleTransaction.CreatedBy = Me.UserId
        objICScheduleTransaction.ScheduleTransactionFromDate = txtStartDate.SelectedDate
        objICScheduleTransaction.ScheduleTransactionToDate = txtEndDate.SelectedDate
        objICScheduleTransaction.TrnsactionFrequency = ddlScheduleFrequency.SelectedValue.ToString
        objICScheduleTransaction.DayType = rblDaysOfMonth.SelectedValue.ToString
        objICScheduleTransaction.AmountPerInstruction = CDbl(objICInstruction.Amount)
        objICScheduleTransaction.TransactionCount = TransactionCount
        objICScheduleTransaction.TransactionTime = CDate(radTimePicker.SelectedDate)

        Return objICScheduleTransaction
    End Function

    Protected Sub txtNoOfDays_TextChanged(sender As Object, e As EventArgs) Handles txtNoOfDays.TextChanged
        Try
            txtEndDate.MinDate = Date.Now.AddDays(1)
            txtEndDate.SelectedDate = Date.Now.AddDays(1)

            If txtNoOfDays.Text <> "" Then
                txtEndDate.MinDate = Date.Now.AddDays(CInt(txtNoOfDays.Text)).Date
                txtEndDate.SelectedDate = Nothing
                txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddDays(CInt(txtNoOfDays.Text))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvInstructionDetail_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles gvInstructionDetail.ItemDataBound
        Dim imgBtn As LinkButton
        Dim AppPath As String = "" ' DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            imgBtn = New LinkButton
            imgBtn = DirectCast(item.Cells(0).FindControl("lbInstructionID"), LinkButton)
            imgBtn.OnClientClick = "showurldialog('Instruction Details',' " & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & item.GetDataKeyValue("InstructionID").ToString().EncryptString() & "' ,true,700,650);return false;"

            imgBtn.Enabled = True

            If item.GetDataKeyValue("IsAmendmentComplete") = True Then

                gvInstructionDetail.AlternatingItemStyle.CssClass = "GridItemComplete"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItemComplete"
            Else
                gvInstructionDetail.AlternatingItemStyle.CssClass = "GridItem"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItem"


            End If

        End If
    End Sub
    Protected Sub txtStartDate_SelectedDateChanged(sender As Object, e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles txtStartDate.SelectedDateChanged
        Try


            If ddlScheduleFrequency.SelectedValue.ToString = "Daily" Then
                
                txtEndDate.MinDate = CDate(Now.Date.AddDays(1))
            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Monthly" Then
                txtEndDate.MinDate = CDate(txtStartDate.SelectedDate).AddMonths(1).Date
                txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddMonths(1).Date
               
            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Number Of Days" Then
                txtEndDate.MinDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date
                txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date
                If Not txtStartDate.SelectedDate Is Nothing Then
                    txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date
                End If
            Else
                txtEndDate.MinDate = Date.Now.AddDays(1)
                txtEndDate.SelectedDate = Date.Now.AddDays(1)
                txtStartDate.SelectedDate = Date.Now.AddDays(1)
                txtEndDate.SelectedDate = Date.Now.AddDays(1)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub txtEndDate_SelectedDateChanged(sender As Object, e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles txtEndDate.SelectedDateChanged
        Try


            If ddlScheduleFrequency.SelectedValue.ToString = "Daily" Then
               
                txtEndDate.MinDate = CDate(Now.Date.AddDays(1))
            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Monthly" Then
                txtEndDate.MinDate = CDate(txtStartDate.SelectedDate).AddMonths(1).Date
                txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddMonths(1).Date

            ElseIf ddlScheduleFrequency.SelectedValue.ToString = "Number Of Days" Then
                txtEndDate.MinDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date
                txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date
                If Not txtStartDate.SelectedDate Is Nothing Then
                    txtEndDate.SelectedDate = CDate(txtStartDate.SelectedDate).AddDays(txtNoOfDays.Text).Date
                End If

            Else
                
                txtStartDate.SelectedDate = Date.Now.AddDays(1)
                txtEndDate.SelectedDate = Date.Now.AddDays(1)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
