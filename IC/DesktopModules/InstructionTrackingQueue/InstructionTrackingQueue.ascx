﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InstructionTrackingQueue.ascx.vb"
    Inherits="DesktopModules_InstructionTrackingQueue_InstructionTrackingQueue" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="ICBO" %>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:TextBoxSetting BehaviorID="reTnxCount" Validation-IsRequired="false" EmptyMessage=""
        ErrorMessage="Transaction Count">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTnxCount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reTotalAmount" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Total Amount">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTotalAmount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reCurrency" Validation-IsRequired="false" EmptyMessage=""
        ErrorMessage="Currency">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCurrency" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rePaymentMode" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Payment Mode">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPaymentMode" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reAvailableBalance" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Available Balance">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAvailableBalance" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reInstructionNo" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Enter Instruction No." ValidationExpression="[0-9]{0,8}">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstructionNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reInstrumentNo" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Enter Instrument No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstrumentNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reReferenceNo" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Enter Reference No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reBaneName" Validation-IsRequired="false" EmptyMessage="Enter Baneficiary Name"
        ErrorMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneName" />
        </TargetControls>
    </telerik:TextBoxSetting>
       <telerik:NumericTextBoxSetting BehaviorID="reAmount" Validation-IsRequired="false" EmptyMessage=""
        ErrorMessage="Invalid Amount" Type="Number" DecimalDigits="2" AllowRounding="false" >
        <TargetControls>
            <telerik:TargetInput ControlID="txtAmount" />
        </TargetControls>
    </telerik:NumericTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reRemarks" Validation-IsRequired="true" EmptyMessage="Enter Remarks"
        ErrorMessage="" Validation-ValidationGroup="Remarks">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRemarks" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<script type="text/javascript">

    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }

    $(document).ready(function () {

        // Dialog
        $('#urldialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#loading').hide();



            },
            resizable: false
        });
        // Summary Dialog
        $('#sumdialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#sumdialog').hide();
                //                window.location.reload();
                //                window.location.reload();sa
            },
            resizable: false
        });
    });
    function showsumdialog(title, modal, width, height) {
        //  alert(modal);
        $('#sumdialog').dialog("option", "title", title);
        $('#sumdialog').dialog("option", "modal", modal);
        $('#sumdialog').dialog("option", "width", width);
        $('#sumdialog').dialog("option", "height", height);
        $('#sumdialog').dialog('open');

        return false;

    };


    function showurldialog(title, url, modal, width, height) {
        //  alert(modal);
        $('#urldialog').dialog("option", "title", title);
        $('#urldialog').dialog("option", "modal", modal);
        $('#urldialog').dialog("option", "width", width);
        $('#urldialog').dialog("option", "height", height);
        $('#urldialog').dialog('open');
        $('#dialogiframe').hide();
        $('#loading').show();
        $('#dialogiframe').attr("src", url);
        $('#dialogiframe').load(function () {
            $('#loading').hide();
            $('#dialogiframe').show();
        });
        return false;

    };

    function CloseIframe() {
        $('#urldialog').dialog('close');
    }
</script>
<script language="javascript" type="text/javascript">
    function AdjustWidth(ddl) {
        var maxWidth = 0;
        var Counter = 0;
        for (var i = 0; i < ddl.length; i++) {
            if (ddl.options[i].text.length > maxWidth) {
                Counter = Counter + 1;
                maxWidth = ddl.options[i].text.length;
            }
        }
        if (Counter = 1) { ddl.style.width = "250px"; }

        else {
            alert(Counter);
            ddl.style.width = maxWidth + "px";
        }
        //-->
    }
</script>
<style type="text/css">
.ctrDropDown{
    width:250px;
   
}
.ctrDropDownClick{
    
    width:600px;
 
}
.plainDropDown{
    width:250px;
   
}
</style>
<%--<asp:Label ID="lblscript" runat="server" Text=""></asp:Label>--%>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%" colspan="4">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Instruction Tracking" CssClass="lblHeadingForQueue"></asp:Label>
                        <br />
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%" colspan="4">
                        <table align="left" valign="top" style="width: 100%">
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%">
                                    <table align="left" valign="top" style="width: 100%">
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 25%">
                                                <asp:Label ID="lblDateFrom" runat="server" Text="Date From" CssClass="lbl"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="width: 25%">
                                                <telerik:RadDatePicker ID="raddpCreationFromDate" runat="server" CssClass="txtbox" AutoPostBack="true">
                                                    <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                    </Calendar>
                                                    <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                        LabelWidth="40%" type="text" value="">
                                                    </DateInput>
                                                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                </telerik:RadDatePicker>
                                            </td>
                                            <td align="left" valign="top" style="width: 25%">
                                                <asp:Label ID="lblCreationDateTo" runat="server" Text="Date To" CssClass="lbl"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="width: 25%">
                                                <telerik:RadDatePicker ID="raddpCreationToDate" runat="server" CssClass="txtbox">
                                                    <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                    </Calendar>
                                                    <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                        LabelWidth="40%" type="text" value="">
                                                    </DateInput>
                                                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="left" valign="top" style="width: 50%">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
              <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" colspan="4">
                        <asp:Panel ID="pnlHeader" runat="server" DefaultButton="btnSearch">
                        
                        <table width="100%">
                                        <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lbl" runat="server" Text="Group" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblAccountPaymentNature0" runat="server" Text="Company" CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblAccountPaymentNature" runat="server" Text="Account Payment Nature"
                            CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblBatchNo" runat="server" CssClass="lbl" Text="Batch No."></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                       <%-- <asp:DropDownList ID="ddlAcccountPaymentNature" runat="server" CssClass="dropdown"
                            AutoPostBack="True">
                        </asp:DropDownList>--%>
                          <div style= "width:250px; overflow:hidden;">
            <asp:DropDownList ID="ddlAcccountPaymentNature" runat="server" class="ctrDropDown"  onblur="this.className='ctrDropDown';" onmousedown="this.className='ctrDropDownClick';" onchange="this.className='ctrDropDown';" AutoPostBack="true">
            </asp:DropDownList>
            </div>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlFileBatchNo" runat="server" AutoPostBack="True" 
                            CssClass="dropdown">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblProductType0" runat="server" CssClass="lbl" 
                            Text="Product Type"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblStatus0" runat="server" CssClass="lbl" Text="Status"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlProductType" runat="server" AutoPostBack="True" 
                            CssClass="dropdown">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="True" 
                            CssClass="dropdown">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                                        <tr align="left" style="width: 100%" valign="top">
                                            <td align="left" style="width: 25%" valign="top">
                                                <asp:Label ID="lblInstructionNo" runat="server" CssClass="lbl" 
                                                    Text="Instruction No."></asp:Label>
                                            </td>
                                            <td align="left" style="width: 25%" valign="top">
                                                &nbsp;</td>
                                            <td align="left" style="width: 25%" valign="top">
                                                <asp:Label ID="lblInstrumentNo0" runat="server" CssClass="lbl" 
                                                    Text="Instrument No."></asp:Label>
                                            </td>
                                            <td align="left" style="width: 25%" valign="top">
                                                &nbsp;</td>
                                        </tr>
                                        <tr align="left" style="width: 100%" valign="top">
                                            <td align="left" style="width: 25%" valign="top">
                                                <asp:TextBox ID="txtInstructionNo" runat="server" CssClass="txtbox" 
                                                    MaxLength="8"></asp:TextBox>
                                            </td>
                                            <td align="left" style="width: 25%" valign="top">
                                                &nbsp;</td>
                                            <td align="left" style="width: 25%" valign="top">
                                                <asp:TextBox ID="txtInstrumentNo" runat="server" CssClass="txtbox" 
                                                    MaxLength="150"></asp:TextBox>
                                            </td>
                                            <td align="left" style="width: 25%" valign="top">
                                                &nbsp;</td>
                                        </tr>
                                        <tr align="left" style="width: 100%" valign="top">
                                            <td align="left" style="width: 25%" valign="top">
                                                &nbsp;</td>
                                            <td align="left" style="width: 25%" valign="top">
                                                &nbsp;</td>
                                            <td align="left" style="width: 25%" valign="top">
                                                &nbsp;</td>
                                            <td align="left" style="width: 25%" valign="top">
                                                &nbsp;</td>
                                        </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%; margin-left: 120px;">
                        <asp:Label ID="lblAmount0" runat="server" CssClass="lbl" Text="Amount"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtAmount" runat="server" Font-Size="10px" MaxLength="14" 
                            Width="120px"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                                        <tr align="left" style="width: 100%" valign="top">
                                            <td align="left" style="width: 25%" valign="top">
                                                <asp:DropDownList ID="ddlAmountOp" runat="server" 
                                                    CssClass="dropdownForQueueForAmountOperator">
                                                    <asp:ListItem>=</asp:ListItem>
                                                    <asp:ListItem>&gt;</asp:ListItem>
                                                    <asp:ListItem>&lt;</asp:ListItem>
                                                    <asp:ListItem>&gt;=</asp:ListItem>
                                                    <asp:ListItem>&lt;=</asp:ListItem>
                                                    <asp:ListItem>&lt;&gt;</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td align="left" style="width: 25%" valign="top">
                                                &nbsp;</td>
                                            <td align="left" style="width: 25%" valign="top">
                                                &nbsp;</td>
                                            <td align="left" style="width: 25%" valign="top">
                                                &nbsp;</td>
                                        </tr>
                                        <tr align="left" style="width: 100%" valign="top">
                                            <td align="left" style="width: 25%" valign="top">
                                                <asp:Button ID="btnSearch" runat="server" CausesValidation="False" 
                                                    CssClass="btn" Text="Search" Width="71px" />
                                            </td>
                                            <td align="left" style="width: 25%" valign="top">
                                                &nbsp;</td>
                                            <td align="left" style="width: 25%" valign="top">
                                                &nbsp;</td>
                                            <td align="left" style="width: 25%" valign="top">
                                                &nbsp;</td>
                                        </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%" colspan="4">
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px"
                            CausesValidation="False" />
                    </td>
                </tr>
                        </table>
                        </asp:Panel>
                    </td>
                    
                </tr>

                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%" colspan="4">
                        <asp:Label ID="lblNRF" runat="server" Text="No Record Found." CssClass="headingblue"
                            Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%" colspan="4">
                        <telerik:RadGrid ID="gvAuthorization" runat="server" AllowPaging="True" CssClass="RadGrid"
                            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                            PageSize="100">
                            <ClientSettings>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                
                            </ClientSettings>
                            <MasterTableView DataKeyNames="ClientAccountCurrency,InstructionID,IsAmendmentComplete,PaymentMode,Status" TableLayout="Auto" >
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Ins No." HeaderStyle-Width="6%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbInstructionID" runat="server" Text='<%#Eval("InstructionID") %>'
                                                ToolTip="Instruction No." ForeColor="Blue" Enabled="false"></asp:LinkButton>
                                        </ItemTemplate>
                                        <%--<ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="4%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="4%" />--%>
                                    </telerik:GridTemplateColumn>
                                     <telerik:GridBoundColumn DataField="FileBatchNoName" HeaderText="Batch No" SortExpression="FileBatchNoName">
                                 <%--       <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="9%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="9%" />--%>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CreateDate" HeaderText="Creation Date" SortExpression="CreateDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                     <%--   <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />--%>
                                    </telerik:GridBoundColumn>
                                   <%-- <telerik:GridBoundColumn DataField="ValueDate" HeaderText="Value Date" SortExpression="ValueDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}" HeaderStyle-Width="8%">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>--%>
                                    <telerik:GridBoundColumn DataField="BeneficiaryName" HeaderText="Bene Name" SortExpression="BeneficiaryName">
                                       <%-- <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="13%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="13%" />--%>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Amount" HeaderText="Amount" SortExpression="Amount"
                                        HtmlEncode="false" DataFormatString="{0:N2}">
                                       <%-- <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />--%>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProductTypeCode" HeaderText="Payment Mode" SortExpression="ProductTypeCode">
                                    <%--    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="5%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="5%" />--%>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InstrumentNo" HeaderText="Instrument No." SortExpression="InstrumentNo">
                                     <%--   <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />--%>
                                    </telerik:GridBoundColumn>
                                  
                                    <telerik:GridBoundColumn DataField="OfficeName" HeaderText="Print Location" SortExpression="OfficeName">
                                      <%--  <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="13%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="13%" />--%>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="StatusName" HeaderText="Status" SortExpression="StatusName">
                                     <%--   <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="9%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="9%" />--%>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneficiaryAccountNo" HeaderText="Account No."
                                        SortExpression="BeneficiaryAccountNo">
                                       <%-- <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="9%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="9%" />--%>
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridBoundColumn DataField="BeneAccountBranchCode" HeaderText="Branch Code"
                                        SortExpression="BeneAccountBranchCode">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>--%>
                                    <%--<telerik:GridBoundColumn DataField="BeneAccountCurrency" HeaderText="Currency" SortExpression="BeneAccountCurrency">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="5%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="5%" />
                                    </telerik:GridBoundColumn>--%>
                                     <telerik:GridBoundColumn DataField="UBPReferenceField" HeaderText="UBPS Reference No" SortExpression="UBPReferenceField">
                                     <%--   <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />--%>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="UBPSCompanyName" HeaderText="UBPS Company Name" SortExpression="UBPSCompanyName"
                                        >
                                      <%--  <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />--%>
                                    </telerik:GridBoundColumn>
                                     <telerik:GridTemplateColumn HeaderText="Schedule">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlSchedule" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("ScheduleTransaction", "&mid=" & Me.ModuleId & "&InstructionID=" & Eval("InstructionID").ToString().EncryptString2())%>'
                                                ToolTip="Edit">Schedule Instruction</asp:HyperLink>
                                        </ItemTemplate>


<%--                                         <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="9%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="9%" />--%>
                                    </telerik:GridTemplateColumn>
                                     <%-- Changes Made by Farhan 15-04-15--%>
                                     <telerik:GridTemplateColumn HeaderText="Resend Rin">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgBtnResendRin" runat="server" ImageUrl="~/images/restore.gif" CommandArgument='<%#Eval("InstructionID")%>' CommandName="Resend RIN"
                                                ToolTip="Resend RIN" />
                                        </ItemTemplate>


<%--                                         <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />--%>

                                       
                                    </telerik:GridTemplateColumn>
                                   
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <HeaderStyle CssClass="GridHeader" />
                            <ItemStyle CssClass="GridItem" />
                            <AlternatingItemStyle CssClass="GridItem" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div id="sumdialog" title="" style="text-align: center">
    <asp:Panel ID="pnlShowSummary" runat="server" Style="display: none; width: 100%"
        ScrollBars="Vertical" Height="600px">
        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn" /><br />
        <telerik:RadGrid ID="gvShowSummary" runat="server" AllowPaging="false" Width="100%"
            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" CssClass="RadGrid"
            PageSize="10">
            <AlternatingItemStyle CssClass="rgAltRow" />
            <MasterTableView>
                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                </RowIndicatorColumn>
                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="InstructionID" HeaderText="Instruction No." SortExpression="InstructionID">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                    <%-- <telerik:GridBoundColumn DataField="ReferenceNo" HeaderText="Reference No." SortExpression="ReferenceNo">
                                    </telerik:GridBoundColumn>--%>
                    <telerik:GridBoundColumn DataField="Message" HeaderText="Message" SortExpression="Message">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
                <PagerStyle AlwaysVisible="True" />
            </MasterTableView>
            <ItemStyle CssClass="rgRow" />
            <PagerStyle AlwaysVisible="True" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
    </asp:Panel>
</div>
<div id="urldialog" title="">
    <iframe id='dialogiframe' frameborder="0" allowtransparency="true" src="" style="border-style: none;
        width: 100%; height: 100%; overflow-x: hidden; background-color: transparent;">
    </iframe>
     <div id='loading' style="display: none; margin: 0px auto; text-align: center; width: 100%;
        height: 100%; position: relative">
        <img src='~/images/progressbar.gif' style="left: 45%; top: 50%; position: absolute;" />
    </div>
</div>
