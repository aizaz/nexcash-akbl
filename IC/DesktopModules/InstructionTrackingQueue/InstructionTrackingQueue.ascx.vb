﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports EntitySpaces.Core
Partial Class DesktopModules_InstructionTrackingQueue_InstructionTrackingQueue
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrICAssignUserRolsID As New ArrayList
    Private ArrICAssignedOfficeID As New ArrayList
    Private ArrICAssignedStatus As New ArrayList
    Private ArrICAssignedPaymentModes As New ArrayList
    Private htRights As Hashtable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            SetArraListRoleID()
            If Page.IsPostBack = False Then


                ViewState("htRights") = Nothing
                GetPageAccessRights()
                SetArraListAssignedPaymentModes()
                'SetArraListAssignedOfficeIDs()
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
                LoadddlGroup()
                LoadddlCompany()
                LoadddlAccountPaymentNatureByUserID()
                LoadddlProductTypeByAccountPaymentNature()
                LoadddlStatus()
                ClearAllTextBoxes()
                FillDesignedDtByAPNatureByAssignedRole()
                Dim dtPageLoad As New DataTable
                dtPageLoad = DirectCast(ViewState("AccountNumber"), DataTable)


                If dtPageLoad.Rows.Count > 0 Then
                    LoadddlBatcNumber()
                    LoadgvAccountPaymentNatureProductType(True)
                Else
                    UIUtilities.ShowDialog(Me, "Error", "You do not have access to any transactional data. Please contact Al Baraka Administrator.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                    Exit Sub
                End If

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Instruction Tracking") = True Then
                    If Not ArrICAssignUserRolsID.Contains(RoleID.ToString) Then
                        ArrICAssignUserRolsID.Add(RoleID.ToString)
                    End If
                End If
            Next
        End If
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Instruction Tracking")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub DesignDts()
        Dim dtAccountNumber As New DataTable
        dtAccountNumber.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("BranchCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("Currency", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("PaymentNatureCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("OfficeID", GetType(System.String)))
        ViewState("AccountNumber") = dtAccountNumber
    End Sub
    Private Sub FillDesignedDtByAPNatureByAssignedRole()
        Dim dtAssignedAPNatureOfficeID As New DataTable
        dtAssignedAPNatureOfficeID.Rows.Clear()
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserSecond(Me.UserId.ToString, ArrICAssignUserRolsID)
        dtAssignedAPNatureOfficeID.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAssignedAPNatureOfficeID.Rows
            dr("DropDown") = "False"
        Next
        ViewState("AccountNumber") = Nothing
        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID

    End Sub
    Private Sub SetArraListAssignedOfficeIDs()
        Dim dt As New DataTable
        Dim objICUser As New ICUser
        ArrICAssignedOfficeID.Clear()
        dt = ICUserRolesPaymentNatureController.GetAllTaggedLocationsWithUserByUSerIDAndRoleID(Me.UserId.ToString, ArrICAssignUserRolsID)
        If dt.Rows.Count > 0 Then
            objICUser.LoadByPrimaryKey(Me.UserId)
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("OfficeID").ToString
                ArrICAssignedOfficeID.Add(RoleID.ToString)
            Next
            ArrICAssignedOfficeID.Add(objICUser.OfficeCode)
        End If
    End Sub
    Private Sub SetArraListAssignedStatus()
        ArrICAssignedStatus.Clear()

    End Sub
    Private Sub SetArraListAssignedPaymentModes()
        ArrICAssignedPaymentModes.Clear()
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, ddlGroup.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlCompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlCompany.Items.Clear()
                    ddlCompany.Items.Add(lit)
                    lit.Selected = True
                    ddlCompany.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
                ddlCompany.DataSource = dt
                ddlCompany.DataTextField = "CompanyName"
                ddlCompany.DataValueField = "CompanyCode"
                ddlCompany.DataBind()
                ddlCompany.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedGroupsByUserID(Me.UserId.ToString, ArrICAssignUserRolsID)
            ddlGroup.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("GroupCode")
                    lit.Text = dr("GroupName")
                    ddlGroup.Items.Clear()
                    ddlGroup.Items.Add(lit)
                    lit.Selected = True
                    ddlGroup.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlGroup.Items.Clear()
                ddlGroup.Items.Add(lit)
                ddlGroup.AppendDataBoundItems = True
                ddlGroup.DataSource = dt
                ddlGroup.DataTextField = "GroupName"
                ddlGroup.DataValueField = "GroupCode"
                ddlGroup.DataBind()
                ddlGroup.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlGroup.Items.Clear()
                ddlGroup.Items.Add(lit)
                ddlGroup.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetViewStateDtOnAPNatureIndexChnage()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim AccountNumber, BranchCode, Currency, PaymentNatureCode As String
        Dim ArrayListOfficeID As New ArrayList
        Dim drAccountNo As DataRow
        AccountNumber = Nothing
        BranchCode = Nothing
        Currency = Nothing
        PaymentNatureCode = Nothing
        dtAccountNumber = ViewState("AccountNumber")
        ViewState("AccountNumber") = Nothing
        AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0)
        BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1)
        Currency = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2)
        PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)
        For Each dr As DataRow In dtAccountNumber.Rows
            If dr("AccountNumber").ToString.Split("-")(0) = AccountNumber And dr("AccountNumber").ToString.Split("-")(1) = BranchCode And dr("AccountNumber").ToString.Split("-")(2) = Currency And dr("PaymentNature").ToString = PaymentNatureCode Then
                ArrayListOfficeID.Add(dr("OfficeID").ToString)
            End If
        Next
        dtAccountNumber.Rows.Clear()
        If ArrayListOfficeID.Count > 0 Then
            For Each OfficeID In ArrayListOfficeID
                drAccountNo = dtAccountNumber.NewRow()
                drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
                drAccountNo("PaymentNature") = PaymentNatureCode
                drAccountNo("OfficeID") = OfficeID
                dtAccountNumber.Rows.Add(drAccountNo)
            Next
        Else
            drAccountNo = dtAccountNumber.NewRow()
            drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
            drAccountNo("PaymentNature") = PaymentNatureCode
            drAccountNo("OfficeID") = "0"
            dtAccountNumber.Rows.Add(drAccountNo)
        End If
        'dtAccountNumber.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAccountNumber.Rows
            dr("DropDown") = "True"
        Next
        ViewState("AccountNumber") = dtAccountNumber
    End Sub
    Private Sub LoadddlStatus()
        Try
            Dim lit As New ListItem
            Dim AL_ExceptStatus As New ArrayList
            AL_ExceptStatus.Add(35)
            AL_ExceptStatus.Add(10)
            AL_ExceptStatus.Add(2)
            AL_ExceptStatus.Add(3)
            AL_ExceptStatus.Add(4)
            AL_ExceptStatus.Add(13)
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlStatus.Items.Clear()
            ddlStatus.Items.Add(lit)
            ddlStatus.AppendDataBoundItems = True
            ddlStatus.DataSource = ICInstructionStatusController.GetAllDistinctStatusByExceptedStatus(AL_ExceptStatus)
            ddlStatus.DataTextField = "StatusName"
            ddlStatus.DataValueField = "StatusID"
            ddlStatus.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccountPaymentNatureByUserID()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlAcccountPaymentNature.Items.Clear()
            ddlAcccountPaymentNature.Items.Add(lit)
            lit.Selected = True
            ddlAcccountPaymentNature.AppendDataBoundItems = True
            ddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForDropDown(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
            ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
            ddlAcccountPaymentNature.DataBind()
           
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub LoadddlProductTypeByAccountPaymentNature()
        Try
            Try
                Dim lit As New ListItem
                Dim objICAPNature As New ICAccountsPaymentNature
                objICAPNature.es.Connection.CommandTimeout = 3600
                ddlProductType.Items.Clear()
                lit.Value = "0"
                lit.Text = "-- All --"
                ddlProductType.Items.Add(lit)
                ddlProductType.AppendDataBoundItems = True
                If ddlAcccountPaymentNature.SelectedValue.ToString <> "0" Then
                    If objICAPNature.LoadByPrimaryKey(ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)) Then
                        ddlProductType.DataSource = ICAccountPaymentNatureProductTypeController.GetAllProductTypesByAccountAndPaymentNature(objICAPNature)
                        ddlProductType.DataTextField = "ProductTypeName"
                        ddlProductType.DataValueField = "ProductTypeCode"
                        ddlProductType.DataBind()
                    End If
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlBatcNumber()
        Try


            Dim dtAccountNumber As New DataTable


            dtAccountNumber = ViewState("AccountNumber")
            SetArraListAssignedStatus()
            Dim lit As New ListItem
            lit.Text = "-- All --"
            lit.Value = "0"
            ddlFileBatchNo.Items.Clear()
            ddlFileBatchNo.Items.Add(lit)
            lit.Selected = True
            ddlFileBatchNo.AppendDataBoundItems = True

           
            ddlFileBatchNo.DataSource = GetAllTaggedBatchNumbersByCompanyCodeWithStatus(ArrICAssignedPaymentModes, dtAccountNumber, ddlCompany.SelectedValue.ToString, ArrICAssignedStatus, Me.UserId.ToString)
            ddlFileBatchNo.DataTextField = "FileBatchNoName"
            ddlFileBatchNo.DataValueField = "FileBatchNo"
            ddlFileBatchNo.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Public Shared Function GetAllTaggedBatchNumbersByCompanyCodeWithStatus(ByVal AssignedPaymentModes As ArrayList, ByVal dtAccountNo As DataTable, ByVal CompanyCode As String, ByVal ArrayListStatus As ArrayList, ByVal UserIDs As String) As DataTable
        Dim StrQuery As String = Nothing
        Dim WhereClause As String = Nothing
        WhereClause = " Where "
        Dim dt As New DataTable
        Dim params As New EntitySpaces.Interfaces.esParameters
        StrQuery = "select distinct(IC_Instruction.FileBatchNo), "
        StrQuery += "case ic_instruction.FileBatchNo when 'SI' "
        StrQuery += " then 'SI' else ic_instruction.FileBatchNo + '-' + IC_Files.OriginalFileName end as FileBatchNoName from IC_Instruction  "
        StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID  Where IC_Instruction.InstructionID IN "
        StrQuery += "(Select ins2.InstructionID from IC_Instruction as ins2 "
        If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
            WhereClause += " And ins2.PaymentMode in ( '"
            For i = 0 To AssignedPaymentModes.Count - 1
                WhereClause += AssignedPaymentModes(i) & "',"
            Next
            WhereClause = WhereClause.Remove(WhereClause.Length - 1)
            WhereClause += ")"
        End If
        If ArrayListStatus.Count > 0 Then
            WhereClause += " And ins2.Status in ( "
            For i = 0 To ArrayListStatus.Count - 1
                WhereClause += ArrayListStatus(i) & ","
            Next
            WhereClause = WhereClause.Remove(WhereClause.Length - 1)
            WhereClause += ")"
        End If
        If CompanyCode.ToString <> "0" Then
            WhereClause += " And ins2.CompanyCode=@CompanyCode"
            params.Add("CompanyCode", CompanyCode)
        End If
        If dtAccountNo.Rows.Count > 0 Then
            WhereClause += " And ( "
            WhereClause += " ( "
            WhereClause += " ins2.ClientAccountNo +" & "'-'" & "+ ins2.ClientAccountBranchCode +" & "'-'" & "+ ins2.ClientAccountCurrency +" & "'-'" & "+ ins2.PaymentNatureCode +" & "'-'" & "+ Convert(Varchar,ins2.CreatedOfficeCode) IN ("
            For Each dr As DataRow In dtAccountNo.Rows
                WhereClause += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "-" & dr("OfficeID").ToString & "',"
            Next
            WhereClause = WhereClause.Remove(WhereClause.Length - 1, 1)
            WhereClause += ")"
            WhereClause += " )"
            If dtAccountNo.Rows(0)(3).ToString = False Then
                WhereClause += " OR (IC_Instruction.CreateBy=@UserIDs)"
                params.Add("UserIDs", UserIDs)
            End If

        Else
            WhereClause += " OR (IC_Instruction.CreateBy=@UserIDs)"
            params.Add("UserIDs", UserIDs)
            'ArrAND.Add(qryObjICInstruction.CreateBy = UserIDs)
        End If

        If WhereClause.Contains(" Where  And") Then
            WhereClause = WhereClause.Replace(" Where  And", " Where ")
        End If
        StrQuery += WhereClause
        StrQuery += " ))"

        Dim util As New esUtility
        dt = util.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)
        Return dt
    End Function
    Private Sub LoadgvAccountPaymentNatureProductType(ByVal DoDataBind As Boolean)
        Try
            Dim AccountNumber, BranchCode, Status, Currency, PaymentNatureCode, GroupCode, CompanyCode, ProductTypeCode, InstrumentNo, InstructionNo As String
            Dim FromDate, ToDate As String
            Dim Amount As Double = 0
            AccountNumber = ""
            BranchCode = ""
            Currency = ""
            Currency = ""
            PaymentNatureCode = ""
            CompanyCode = "0"
            ProductTypeCode = ""
            InstructionNo = ""
            InstrumentNo = ""
            GroupCode = ""
            Status = ""
            FromDate = Nothing
            ToDate = Nothing

            Dim dtAccountNumber As New DataTable
            dtAccountNumber = ViewState("AccountNumber")
            If txtAmount.Text <> "" And txtAmount.Text <> "Enter Amount" Then
                Amount = CDbl(txtAmount.Text)
                If Amount < 1 Then
                    UIUtilities.ShowDialog(Me, "Instruction Tracking", "Amount must be greater than or equal to 1 PKR.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            End If
            If ddlGroup.SelectedValue.ToString <> "0" Then
                GroupCode = ddlGroup.SelectedValue.ToString
            End If
            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            End If
            If ddlProductType.SelectedValue.ToString <> "0" Then
                ProductTypeCode = ddlProductType.SelectedValue.ToString
            End If
            If ddlStatus.SelectedValue.ToString <> "0" Then
                Status = ddlStatus.SelectedValue.ToString
            End If
            If txtInstrumentNo.Text.ToString <> "" And txtInstrumentNo.Text.ToString <> "Enter Instrument No." Then
                InstrumentNo = txtInstrumentNo.Text.ToString
            End If
            If txtInstructionNo.Text.ToString <> "" And txtInstructionNo.Text.ToString <> "Enter Instruction No." Then
                InstructionNo = txtInstructionNo.Text.ToString
            End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                FromDate = raddpCreationFromDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If Not raddpCreationToDate.SelectedDate Is Nothing Then
                ToDate = raddpCreationToDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            GetAllInstructionsForRadGridSecondForInstructionTrackingAndErrorQueue("Creation Date", FromDate, ToDate, dtAccountNumber, ProductTypeCode, InstructionNo, InstrumentNo, Me.gvAuthorization.CurrentPageIndex + 1, Me.gvAuthorization.PageSize, Me.gvAuthorization, DoDataBind, Me.UserId.ToString, Amount, ddlAmountOp.SelectedValue.ToString, CompanyCode, GroupCode, Status, ddlFileBatchNo.SelectedValue.ToString)
            If gvAuthorization.Items.Count > 0 Then
                gvAuthorization.Visible = True
                gvAuthorization.Columns(11).Visible = CBool(htRights("Allow Schedule"))
                lblNRF.Visible = False
                'SetJavaScript()
            Else
                lblNRF.Visible = True
                gvAuthorization.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
   
    Public Shared Sub GetAllInstructionsForRadGridSecondForInstructionTrackingAndErrorQueue(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal UsersID As String, ByVal Amount As Double, ByVal AmountOperator As String, ByVal CompanyCode As String, ByVal GroupCode As String, ByVal Status As String, ByVal FileBatchNo As String)
        Dim dt As DataTable
        Dim params As New EntitySpaces.Interfaces.esParameters

        Dim StrQuery As String = Nothing
        Dim WhereClasue As String = Nothing
        WhereClasue = " Where "
        StrQuery = "Select InstructionID,convert(date,IC_Instruction.CreateDate) as CreateDate,convert(date,ValueDate) as ValueDate,Amount,StatusName, "
        StrQuery += "isnull(BeneficiaryName,'-') as BeneficiaryName,isnull(BeneficiaryAddress,'-') as BeneficiaryAddress,PaymentMode,ISNULL(InstrumentNo,'-') as InstrumentNo,"
        StrQuery += "ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,ISNULL(IC_Office.OfficeCode + '-' + IC_Office.OfficeName,'-') as OfficeName,"
        StrQuery += "ISNULL(ReferenceNo,'-') as ReferenceNo,IC_Bank.BankName,isnull(PreviousPrintLocationCode,'-') as PreviousPrintLocationCode,"
        StrQuery += "ISNULL(PreviousPrintLocationName,'-') as PreviousPrintLocationName,CONVERT(date,VerificationDate) as VerificationDate,"
        StrQuery += "CONVERT(date,ApprovalDate) as ApprovalDate,CONVERT(date,StaleDate) as staledate,CONVERT(date,CancellationDate) as CancellationDate,"
        StrQuery += "isnull(BeneficiaryAccountNo,'-') as BeneficiaryAccountNo,isnull(BeneAccountBranchCode,'-') as BeneAccountBranchCode,"
        StrQuery += "isnull(BeneAccountCurrency,'-') as BeneAccountCurrency,LastPrintDate,BeneficiaryAccountNo,BeneAccountBranchCode,BeneAccountCurrency,"
        StrQuery += "CONVERT(date,RevalidationDate) as revalidationdate,IC_Instruction.ProductTypeCode,IC_Instruction.CompanyCode,ic_instruction.status,IC_Company.CompanyName,"
        StrQuery += "IC_ProductType.ProductTypeName,"
        StrQuery += "case isnull(convert(varchar,IsAmendmentComplete),CONVERT(varchar,'false')) "
        StrQuery += "when 'false' then "
        StrQuery += "CONVERT(varchar,'false') "
        StrQuery += "else "
        StrQuery += "IsAmendmentComplete "
        StrQuery += " end "
        StrQuery += "as IsAmendmentComplete, "
        StrQuery += "case ic_instruction.FileBatchNo when 'SI' "
        StrQuery += " then 'SI' else ic_instruction.FileBatchNo + '-' + IC_Files.OriginalFileName end as FileBatchNoName,isnull(IC_Instruction.UBPReferenceField,'-') as UBPReferenceField,isnull(IC_UBPSCompany.CompanyName,'-') as UBPSCompanyName "
        StrQuery += "from IC_Instruction "
        StrQuery += "left join IC_Office on IC_Instruction.PrintLocationCode=IC_Office.OfficeID "
        StrQuery += "left join IC_Bank on IC_Instruction.BeneficiaryBankCode=IC_Bank.BankCode "
        StrQuery += "left join IC_Company on IC_Instruction.CompanyCode=IC_Company.CompanyCode "
        StrQuery += "left join IC_ProductType on IC_Instruction.ProductTypeCode=IC_ProductType.ProductTypeCode "
        StrQuery += "left join IC_InstructionStatus on IC_Instruction.Status=IC_InstructionStatus.StatusID "
        StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID "
        StrQuery += "left join IC_UBPSCompany on IC_Instruction.UBPCompanyID=IC_UBPSCompany.UBPSCompanyID "
        StrQuery += "Left JOIN IC_AccountsPaymentNatureProductType on IC_Instruction.ClientAccountNo=IC_AccountsPaymentNatureProductType.AccountNumber AND IC_Instruction.ClientAccountBranchCode=IC_AccountsPaymentNatureProductType.BranchCode AND IC_Instruction.ClientAccountCurrency=IC_AccountsPaymentNatureProductType.Currency AND IC_Instruction.PaymentNatureCode=IC_AccountsPaymentNatureProductType.PaymentNatureCode AND IC_Instruction.ProductTypeCode=IC_AccountsPaymentNatureProductType.ProductTypeCode "
        WhereClasue += " AND IC_AccountsPaymentNatureProductType.IsApproved=1 "
        If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And Amount = 0 Then
            If DateType.ToString = "Creation Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.CreateDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.CreateDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Value Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.ValueDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.ValueDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Approval Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Stale Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.StaleDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.StaleDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Cancellation Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Last Print Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Revalidation Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Verfication Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            End If
            If ProductTypeCode.ToString <> "" Then
                WhereClasue += " AND IC_Instruction.ProductTypeCode=@ProductTypeCode"
                params.Add("ProductTypeCode", ProductTypeCode)
            End If
            If CompanyCode.ToString <> "0" Then
                WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                params.Add("CompanyCode", CompanyCode)
            End If
            If Status.ToString <> "" Then
                WhereClasue += " AND IC_Instruction.Status=@Status"
                params.Add("Status", Status)
            End If
            If FileBatchNo <> "0" Then
                WhereClasue += " AND IC_Instruction.FileBatchNo=@FileBatchNo"
                params.Add("FileBatchNo", FileBatchNo)
            End If
        Else
            If InstructionNo.ToString <> "" Then
                WhereClasue += " AND InstructionID=@InstructionNo"
                params.Add("InstructionNo", InstructionNo)
            End If
            If InstrumentNo.ToString <> "" Then
                WhereClasue += " AND InstrumentNo=@InstrumentNo"
                params.Add("InstrumentNo", InstrumentNo)
            End If
            If Status.ToString <> "" Then
                WhereClasue += " AND IC_Instruction.Status=@Status"
                params.Add("Status", Status)
            End If
            If CompanyCode.ToString <> "0" Then
                WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                params.Add("CompanyCode", CompanyCode)
            End If
            If Not Amount < 1 Then
                If AmountOperator.ToString = "=" Then
                    WhereClasue += " AND Amount = @Amount"
                    params.Add("Amount", Amount)
                ElseIf AmountOperator.ToString = "<" Then
                    WhereClasue += " AND Amount < @Amount"
                    params.Add("Amount", Amount)
                ElseIf AmountOperator.ToString = ">" Then
                    WhereClasue += " AND Amount > @Amount"
                    params.Add("Amount", Amount)
                ElseIf AmountOperator.ToString = "<=" Then
                    WhereClasue += " AND Amount <= @Amount"
                    params.Add("Amount", Amount)
                ElseIf AmountOperator.ToString = ">=" Then
                    WhereClasue += " AND Amount >= @Amount"
                    params.Add("Amount", Amount)
                ElseIf AmountOperator.ToString = "<>" Then
                    WhereClasue += " AND Amount <> @Amount"
                    params.Add("Amount", Amount)
                End If
            End If
        End If

        WhereClasue += " AND ( "

        If AccountNumber.Rows.Count > 0 Then
            WhereClasue += " ( "
            WhereClasue += "ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + ic_instruction.PaymentNatureCode + '" & "-" & "' + Convert(Varchar,CreatedOfficeCode) IN ("
            For Each dr As DataRow In AccountNumber.Rows

                WhereClasue += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "-" & dr("OfficeID").ToString & "',"

            Next
            WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
            WhereClasue += ")"
            WhereClasue += " )"
            If AccountNumber(0)(3) = False Then
                WhereClasue += " OR (IC_Instruction.CreateBy=@UsersID)"
                params.Add("UsersID", UsersID)
            End If
        Else
            WhereClasue += " OR (IC_Instruction.CreateBy=@UsersID)"
            params.Add("UsersID", UsersID)
        End If
        WhereClasue += " ) "

        If WhereClasue.Contains(" Where  AND ") = True Then
            WhereClasue = WhereClasue.Replace(" Where  AND ", "Where ")
        End If
        StrQuery += WhereClasue
        StrQuery += " Order By InstructionID Desc"
        Dim utill As New esUtility()
        dt = New DataTable
        dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)
        If Not CurrentPage = 0 Then

            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        Else
            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        End If

    End Sub
    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            'SetArraListRoleID()
            FillDesignedDtByAPNatureByAssignedRole()
            LoadddlAccountPaymentNatureByUserID()
            LoadddlBatcNumber()
            LoadddlProductTypeByAccountPaymentNature()
            LoadddlStatus()
            LoadgvAccountPaymentNatureProductType(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlAcccountPaymentNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcccountPaymentNature.SelectedIndexChanged
        Try
            If ddlAcccountPaymentNature.SelectedValue.ToString = "0" Then
                SetArraListRoleID()

                LoadddlProductTypeByAccountPaymentNature()
                FillDesignedDtByAPNatureByAssignedRole()
                LoadddlBatcNumber()
                LoadgvAccountPaymentNatureProductType(True)
            Else
                FillDesignedDtByAPNatureByAssignedRole()
                LoadddlBatcNumber()
                LoadddlProductTypeByAccountPaymentNature()
                SetViewStateDtOnAPNatureIndexChnage()
                LoadgvAccountPaymentNatureProductType(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged
        Try
            LoadddlStatus()
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ClearAllTextBoxes()

        txtInstructionNo.Text = ""
        txtInstrumentNo.Text = ""
        txtAmount.Text = ""
        ddlAmountOp.SelectedValue = "="

    End Sub

    Protected Sub gvAuthorization_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles gvAuthorization.ItemCommand

        Try
            '  Dim InstructionID As Integer

            If e.CommandName.ToString = "Resend RIN" Then

                Dim objICInstruction As New ICInstruction
                Dim IsRINSent As Boolean = False
                Dim FailureMessage As String = Nothing


                objICInstruction.LoadByPrimaryKey(e.CommandArgument.ToString)


                Try
                    If Not objICInstruction.BeneficiaryEmail = "" And Not objICInstruction.BeneficiaryEmail Is Nothing Then
                        EmailUtilities.SendRINToBeneficiaryForCOTCVIAEmail(objICInstruction)
                        IsRINSent = True
                    Else
                        FailureMessage = "Invalid beneficiary email </br >"
                    End If
                    If Not objICInstruction.BeneficiaryMobile = "" And Not objICInstruction.BeneficiaryMobile Is Nothing Then
                        SMSUtilities.SendRINToBeneficiaryForCOTCVIASMS(objICInstruction)
                        IsRINSent = True
                    Else
                        FailureMessage += "Invalid beneficiary mobile no."
                    End If
                Catch ex As Exception
                    FailureMessage = ex.Message.ToString & " </br >"
                End Try
                If IsRINSent = False Then
                    UIUtilities.ShowDialog(Me, "Instruction Tracking", "RIN not sent successfully due to following reasond: </br >" & FailureMessage, Dialogmessagetype.Failure)
                    Exit Sub
                Else
                    UIUtilities.ShowDialog(Me, "Instruction Tracking", "RIN sent successfully", Dialogmessagetype.Success)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
 

    Protected Sub gvAuthorization_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAuthorization.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAuthorization_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemDataBound


      
        Dim imgBtn As LinkButton
        Dim AppPath As String = "" ' DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            imgBtn = New LinkButton
            imgBtn = DirectCast(item.Cells(0).FindControl("lbInstructionID"), LinkButton)
            imgBtn.OnClientClick = "showurldialog('Instruction Details',' " & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & item.GetDataKeyValue("InstructionID").ToString().EncryptString() & "' ,true,700,650);return false;"

            imgBtn.Enabled = True
            Dim hlSchedule As HyperLink = DirectCast(item.Cells(12).FindControl("hlSchedule"), HyperLink)
            hlSchedule.Visible = True
            If item.GetDataKeyValue("PaymentMode") = "Direct Credit" Or item.GetDataKeyValue("PaymentMode") = "Other Credit" Or item.GetDataKeyValue("PaymentMode") = "PO" Then
                hlSchedule.Visible = True
            Else
                hlSchedule.Visible = False

            End If

            Dim ImgBtnResendRin As ImageButton = DirectCast(item.Cells(13).FindControl("ImgBtnResendRin"), ImageButton)

            'Dim IsTrue As Boolean = False
            'IsTrue = CBool(htRights("Resend Rin"))

            If item.GetDataKeyValue("PaymentMode") = "COTC" And CBool(htRights("Resend Rin")) = True And item.GetDataKeyValue("Status") = "48" Then
                ImgBtnResendRin.Visible = True
            Else
                ImgBtnResendRin.Visible = False

            End If
            If item.GetDataKeyValue("IsAmendmentComplete") = True Then

                gvAuthorization.AlternatingItemStyle.CssClass = "GridItemComplete"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItemComplete"


            Else
                gvAuthorization.AlternatingItemStyle.CssClass = "GridItem"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItem"


            End If

        End If
    End Sub

    'Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
    '    Try
    '        LoadgvAccountPaymentNatureProductType(True)
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    Private Function CheckgvClientRoleCheckedForProcessAll() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvAuthorization.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    

    Protected Sub gvAuthorization_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAuthorization.NeedDataSource
        Try
            LoadgvAccountPaymentNatureProductType(False)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL(41))
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As System.EventArgs) Handles btnSearch.Click

        Try
                LoadgvAccountPaymentNatureProductType(True)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
            FillDesignedDtByAPNatureByAssignedRole()
            LoadddlAccountPaymentNatureByUserID()
            LoadddlBatcNumber()
            LoadddlProductTypeByAccountPaymentNature()
            LoadddlStatus()
            LoadgvAccountPaymentNatureProductType(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlStatus_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlStatus.SelectedIndexChanged
        Try
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub raddpCreationFromDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationFromDate.SelectedDateChanged
        Try
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then

                    UIUtilities.ShowDialog(Me, "Error", "From date should be less than to date.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub

                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub raddpCreationToDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationToDate.SelectedDateChanged
        Try
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then

                    UIUtilities.ShowDialog(Me, "Error", "From date should be less than to date.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub

                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlFileBatchNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFileBatchNo.SelectedIndexChanged
        Try
            LoadddlProductTypeByAccountPaymentNature()
            LoadddlStatus()
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class

