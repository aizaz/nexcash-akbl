﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ScheduleTransaction.ascx.vb" Inherits="DesktopModules_InstructionTrackingQueue_ScheduleTransaction" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<script type="text/javascript">


    $(document).ready(function () {

        // Dialog
        $('#urldialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#loading').hide();



            },
            resizable: false
        });
        // Summary Dialog
        $('#sumdialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#sumdialog').hide();
                //  location.reload(false);
                //                window.location.reload();
                //                window.location.reload();sa
            },
            resizable: false
        });



    });
    function showsumdialog(title, modal, width, height) {
        //  alert(modal);
        $('#sumdialog').dialog("option", "title", title);
        $('#sumdialog').dialog("option", "modal", modal);
        $('#sumdialog').dialog("option", "width", width);
        $('#sumdialog').dialog("option", "height", height);
        $('#sumdialog').dialog('open');
        $('#sumdialog').dialog();
        return false;

    };
    function CloseSummaryDialogBox() {
        $('#sumdialog').dialog('close');
    }

    function showurldialog(title, url, modal, width, height) {
        //  alert(modal);
        $('#urldialog').dialog("option", "title", title);
        $('#urldialog').dialog("option", "modal", modal);
        $('#urldialog').dialog("option", "width", width);
        $('#urldialog').dialog("option", "height", height);
        $('#urldialog').dialog('open');
        $('#dialogiframe').hide();
        $('#loading').show();
        $('#dialogiframe').attr("src", url);
        $('#dialogiframe').load(function () {
            $('#loading').hide();
            $('#dialogiframe').show();
        });
        return false;

    };

    function CloseIframe() {
        $('#urldialog').dialog('close');
    }




</script>


<telerik:RadInputManager ID="RadInputManager1" runat="server">

    <telerik:TextBoxSetting BehaviorID="reScheduleTitle" EmptyMessage="" Validation-ValidationGroup="Schedule"
        ErrorMessage="Invalid Title" Validation-IsRequired="true">
        <TargetControls>
            <telerik:TargetInput ControlID="txtScheduleTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>

</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue">Schedule Transaction</asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" class="style1">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" class="style1">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblPageHeader0" runat="server" CssClass="headingblue">Instruction Detail</asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" class="style1">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
                        <telerik:RadGrid ID="gvInstructionDetail" runat="server" CssClass="RadGrid" AllowPaging="True"
                            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                            PageSize="1">
                           
                            <%-- <AlternatingItemStyle CssClass="rgAltRow" />--%>
                            <MasterTableView DataKeyNames="InstructionID,IsAmendmentComplete" TableLayout="Fixed"
                                NoMasterRecordsText="">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                  
                                    <telerik:GridTemplateColumn HeaderText="Instruction No." HeaderStyle-Width="8%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbInstructionID" runat="server" Text='<%#Eval("InstructionID") %>'
                                                ToolTip="Instruction No." ForeColor="Blue" Enabled="false"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridTemplateColumn>
                                    
                                    <telerik:GridBoundColumn DataField="CreateDate" HeaderText="Creation Date" SortExpression="CreateDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}" HeaderStyle-Width="8%">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ValueDate" HeaderText="Value Date" SortExpression="ValueDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}" HeaderStyle-Width="8%">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneficiaryName" HeaderText="Bene Name" SortExpression="BeneficiaryName">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="13%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="13%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Amount" HeaderText="Amount" SortExpression="Amount"
                                        HtmlEncode="false" DataFormatString="{0:N2}">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProductTypeCode" HeaderText="Payment Mode" SortExpression="ProductTypeCode">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="10%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="10%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InstrumentNo" HeaderText="Instrument No." SortExpression="InstrumentNo">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="OfficeName" HeaderText="Print Location" SortExpression="OfficeName">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="13%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="13%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneficiaryAccountNo" HeaderText="Account No."
                                        SortExpression="BeneficiaryAccountNo">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="9%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="9%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneAccountBranchCode" HeaderText="Branch Code"
                                        SortExpression="BeneAccountBranchCode">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneAccountCurrency" HeaderText="Currency" SortExpression="BeneAccountCurrency">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                    </telerik:GridBoundColumn>
                                   
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <%--     <ItemStyle CssClass="rgRow" />--%>
                            <PagerStyle AlwaysVisible="True" />
                            <HeaderStyle CssClass="GridHeader" />
                            <ItemStyle CssClass="GridItem" />
                            <AlternatingItemStyle CssClass="GridItem" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" class="style1">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblScheduleTitle" runat="server" CssClass="lbl" Text="Title"></asp:Label>
            <asp:Label ID="lblReqScheduleTitle" runat="server" ForeColor="Red" Text="*"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblScheduleFrequency" runat="server" CssClass="lbl"
                Text="Frequency"></asp:Label>
            <asp:Label ID="lblReqScheduleFrequency" runat="server" ForeColor="Red" Text="*"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtScheduleTitle" runat="server" CssClass="txtbox"
                MaxLength="150"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" class="style1">
            <asp:DropDownList ID="ddlScheduleFrequency" runat="server" CssClass="dropdown"
                AutoPostBack="True">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem Value="Daily">Daily</asp:ListItem>
                <asp:ListItem Value="Monthly">Monthly</asp:ListItem>
                <asp:ListItem Value="Number Of Days">Number Of Days</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" class="style1">
            <asp:RequiredFieldValidator ID="rfvddlScheduleFrequency" runat="server"
                ControlToValidate="ddlScheduleFrequency" Display="Dynamic" Enabled="true"
                ErrorMessage="Please select Schedule Frequency" InitialValue="0"
                SetFocusOnError="True" ValidationGroup="Schedule"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblTransactionTime" runat="server" CssClass="lbl"
                Text="Transaction Time"></asp:Label>
            <asp:Label ID="lblReqTransactionTime" runat="server" ForeColor="Red"
                Text="*"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" class="style1">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">

            <telerik:RadTimePicker ID="radTimePicker" runat="server" InputMode="TimeView" TimePopupButton-Visible="true">
            </telerik:RadTimePicker>
            <br />
            <asp:RequiredFieldValidator ID="rfvTransactionTime" runat="server"
                ControlToValidate="radTimePicker" Display="Dynamic"
                ErrorMessage="Please select Transaction Time" InitialValue=""
                SetFocusOnError="true" ValidationGroup="Schedule"></asp:RequiredFieldValidator>


        </td>
        <td align="left" valign="top" style="width: 25%">
        <td align="left" valign="top" style="width: 30%">
            <table id="tblDayType" runat="server" style="width: 100%">
                <tr>
                    <td align="left" style="width: 100%;" valign="top">
                        <asp:Label ID="lblDayType" runat="server" CssClass="lbl" Text="Day Type"></asp:Label>
                        <asp:Label ID="lblReqDayType" runat="server" ForeColor="Red" Text="*"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 100%;" valign="top">
                        <asp:RadioButtonList ID="rblDaysOfMonth" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                            <asp:ListItem Value="First Day">First Day</asp:ListItem>
                            <asp:ListItem Value="Last Day">Last Day</asp:ListItem>
                            <asp:ListItem Value="Specific Day">Specific Day</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 100%;" valign="top">
                        <asp:RequiredFieldValidator ID="rfvrblrblDaysOfMonth" runat="server" ControlToValidate="rblDaysOfMonth" Display="Dynamic" Enabled="False" ErrorMessage="Please select Day Type" InitialValue="" SetFocusOnError="True" ValidationGroup="Preview"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
            <table id="tblNoOfDays" runat="server" style="width: 100%">
                <tr>
                    <td align="left" style="width: 100%;" valign="top">
                        <asp:Label ID="lblNoOfDays" runat="server" CssClass="lbl" Text="No of Days"></asp:Label>
                        <asp:Label ID="lblReqNoOfDays" runat="server" ForeColor="Red" Text="*"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 100%;" valign="top">
                        <asp:TextBox ID="txtNoOfDays" runat="server" AutoPostBack="True" CssClass="txtbox" MaxLength="2"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 100%;" valign="top">
                        <telerik:RadInputManager ID="radNoOfDay" runat="server" Enabled="False">
                            <telerik:RegExpTextBoxSetting BehaviorID="reNoOfDays" EmptyMessage="" ErrorMessage="Invalid No Of Days" Validation-IsRequired="false" Validation-ValidationGroup="Preview" ValidationExpression="^\+?[\d]{1,2}$">
                                <TargetControls>
                                    <telerik:TargetInput ControlID="txtNoOfDays" />
                                </TargetControls>
                            </telerik:RegExpTextBoxSetting>
                        </telerik:RadInputManager>
                    </td>
                </tr>
            </table>
            <table id="tblSpecificDay" runat="server" style="width: 100%">
                <tr>
                    <td align="left" style="width: 100%;" valign="top">
                        <asp:Label ID="lblSpecificDay" runat="server" CssClass="lbl" Text="Specific Day"></asp:Label>
                        <asp:Label ID="lblReqSpecificDay" runat="server" ForeColor="Red" Text="*"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 100%;" valign="top">
                        <asp:DropDownList ID="ddlSpecificDay" runat="server" CssClass="dropdown">
                            <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>21</asp:ListItem>
                            <asp:ListItem>22</asp:ListItem>
                            <asp:ListItem>23</asp:ListItem>
                            <asp:ListItem>24</asp:ListItem>
                            <asp:ListItem>25</asp:ListItem>
                            <asp:ListItem>26</asp:ListItem>
                            <asp:ListItem>27</asp:ListItem>
                            <asp:ListItem>28</asp:ListItem>
                            <asp:ListItem>29</asp:ListItem>
                            <asp:ListItem>30</asp:ListItem>
                            <asp:ListItem>31</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="left" style="width: 100%;" valign="top">
                        <asp:RequiredFieldValidator ID="rfvddlSpecificDay" runat="server" ControlToValidate="ddlSpecificDay" Display="Dynamic" Enabled="False" ErrorMessage="Please select Specific Day" InitialValue="0" SetFocusOnError="True" ValidationGroup="Schedule"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
        </td>
        <td align="left" valign="top" style="width: 20%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblStartDate" runat="server" CssClass="lbl" Text="Start Date"></asp:Label>
            <asp:Label ID="lblReqStartDate" runat="server" ForeColor="Red" Text="*"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblEndDate" runat="server" CssClass="lbl" Text="End Date"></asp:Label>
            <asp:Label ID="lblReqStartDate0" runat="server" ForeColor="Red" Text="*"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <telerik:RadDatePicker ID="txtStartDate" runat="server"
                DateInput-DateFormat="dd-MMM-yyyy" DateInput-EmptyMessage=""
                EnableTyping="false" ImagesPath="" AutoPostBack="True">
<Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                                    </Calendar>
                                                                    <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                                        Font-Size="10.5px" LabelWidth="40%" type="text" value="">
                                                                    </DateInput>
                                                                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
            </telerik:RadDatePicker>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" class="style1">
            <telerik:RadDatePicker ID="txtEndDate" runat="server"
                DateInput-DateFormat="dd-MMM-yyyy" DateInput-EmptyMessage=""
                EnableTyping="false" ImagesPath="" AutoPostBack="True">
<Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                                    </Calendar>
                                                                    <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                                        Font-Size="10.5px" LabelWidth="40%" type="text" value="">
                                                                    </DateInput>
                                                                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
            </telerik:RadDatePicker>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
                                    <asp:RequiredFieldValidator ID="rfvScheduleStartDate" runat="server" 
                                        ControlToValidate="txtStartDate" Display="Dynamic" 
                                        ErrorMessage="Please select Schedule Start Date" InitialValue="" SetFocusOnError="true" 
                                        ValidationGroup="Schedule"></asp:RequiredFieldValidator>
                                </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" class="style1">
            <asp:RequiredFieldValidator ID="rfvScheduleEndDate" runat="server"
                ControlToValidate="txtEndDate" Display="Dynamic"
                ErrorMessage="Please select Schedule End Date" InitialValue=""
                SetFocusOnError="true" ValidationGroup="Schedule"></asp:RequiredFieldValidator><br />
            <asp:CompareValidator ID="cmpSheduleDates" runat="server" ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThan" ErrorMessage="Schedule End Date must be greater than Schedule From Date"
                SetFocusOnError="true" Display="Dynamic" ValidationGroup="Schedule"></asp:CompareValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">&nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px" ValidationGroup="Schedule" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False" />
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
           
        </td>
    </tr>
</table>
 
<div id="urldialog" title="">
    <iframe id='dialogiframe' frameborder="0" allowtransparency="true" src="" style="border-style: none;
        width: 100%; height: 100%; overflow-x: hidden; background-color: transparent;">
    </iframe>
    <div id='loading' style="display: none; margin: 0px auto; text-align: center; width: 100%;
        height: 100%; position: relative">
        <img src='../../../../../../../../../../images/progressbar.gif' style="left: 45%; top: 50%; position: absolute;" />
    </div>
</div>