﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Security.Permissions
Imports DotNetNuke.Entities.Portals
Partial Class DesktopModules_ClientRoleManagement_SaveClientRole
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private RoleCode As Integer
    Private htRights As Hashtable
#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            RoleCode = Request.QueryString("id")
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                If RoleCode.ToString() = "0" Then
                    Clear()
                    lblPageHeader.Text = "Add Client Roles"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    lblPageHeader.Text = "Edit Client Roles"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    txtICClientRoleName.ReadOnly = True
                    Dim objICClientRole As New ICRole
                    objICClientRole.es.Connection.CommandTimeout = 3600
                    objICClientRole.LoadByPrimaryKey(RoleCode)
                    txtICClientRoleName.Text = objICClientRole.RoleName.ToString()
                    txtICClinetRoleDescription.Text = objICClientRole.Description
                    chkActive.Checked = objICClientRole.IsActive
                End If
            End If
           
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client Roles Management")
            ViewState("htRights") = htRights

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
#Region "Button Events"
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        Try
                Response.Redirect(NavigateURL(), False)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                If RoleCode.ToString() = "0" Then
                    AddICRole(False)
                    Clear()
                Else
                    AddICRole(True)
                    Clear()
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub
#End Region
#Region "Other Functions/Routines"
   
    Private Sub Clear()
        txtICClientRoleName.Text = ""
        txtICClinetRoleDescription.Text = ""
        chkActive.Checked = True
    End Sub

    Private Sub AddICRole(ByVal IsUpDate As Boolean)
        Dim objICRoleController As New RoleController
        Dim objRoleInfo As New RoleInfo
        Dim objICClientRole As New ICRole
        If IsUpDate = False Then
            Try
                objRoleInfo.RoleName = txtICClientRoleName.Text.ToString()
                objRoleInfo.Description = txtICClinetRoleDescription.Text.ToString()
                objRoleInfo.IsPublic = False
                objRoleInfo.RoleGroupID = -1
                objRoleInfo.AutoAssignment = False
                objRoleInfo.PortalID = Me.PortalId
                objICRoleController.AddRole(objRoleInfo)
            Catch ex As Exception
                If ex.Message.Contains("Cannot insert duplicate key in object") Then

                    UIUtilities.ShowDialog(Me, "Error", "Role already exists.", ICBO.IC.Dialogmessagetype.Failure)
                    Clear()
                    Exit Sub
                End If
            End Try
            If Not objRoleInfo.RoleID = 0 Then
                Try

                    objICClientRole.es.Connection.CommandTimeout = 3600
                    objICClientRole.RoleID = objRoleInfo.RoleID
                    objICClientRole.RoleName = objRoleInfo.RoleName.ToString()
                    objICClientRole.Description = txtICClinetRoleDescription.Text.ToString()
                    objICClientRole.RoleType = "Company Role"
                    objICClientRole.CreateDate = Date.Now
                    objICClientRole.CreateBy = Me.UserId
                    objICClientRole.CreateDate = Date.Now
                    objICClientRole.CreateBy = Me.UserId
                    objICClientRole.IsActive = chkActive.Checked
                    ICRoleController.AddICRole(objICClientRole, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    'ICUtilities.AddAuditTrail("Role : " & objICClientRole.RoleName.ToString & "Added.", "Role", objICClientRole.RoleID, Me.UserId, Me.UserInfo.Username.ToString())
                    UIUtilities.ShowDialog(Me, "Save Role", "Role added successfully.", ICBO.IC.Dialogmessagetype.Success)
                Catch ex As Exception
                    ProcessModuleLoadException(Me, ex, False)
                    UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
                End Try
            End If
        Else
            objICClientRole.LoadByPrimaryKey(RoleCode)

            'If CheckDuplicateRole(objICClientRole) = True Then
            '    UIUtilities.ShowDialog(Me, "Error", "Role already exists.", ICBO.IC.Dialogmessagetype.Failure)
            '    Exit Sub
            'End If


            'objRoleInfo = objICRoleController.GetRoleByName(Me.PortalId, objICClientRole.RoleName.ToString)
            objRoleInfo = objICRoleController.GetRole(RoleCode, Me.PortalId)
            objRoleInfo.RoleName = txtICClientRoleName.Text.ToString()
            objRoleInfo.Description = txtICClinetRoleDescription.Text.ToString()
            objICRoleController.UpdateRole(objRoleInfo)

            Try
                'Dim objICRole As New ICRole
                objICClientRole.es.Connection.CommandTimeout = 3600
                'objICClientRole.LoadByPrimaryKey(RoleCode)
                objICClientRole.RoleName = txtICClientRoleName.Text.ToString()
                objICClientRole.Description = txtICClinetRoleDescription.Text.ToString()
                objICClientRole.RoleType = "Company Role"
                objICClientRole.IsActive = chkActive.Checked
                objICClientRole.CreateDate = Date.Now
                objICClientRole.CreateBy = Me.UserId
                ICRoleController.AddICRole(objICClientRole, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                'ICUtilities.AddAuditTrail("Role : " & objICRole.RoleName.ToString & "Updated.", "Role", objICRole.RoleID, Me.UserId, Me.UserInfo.Username.ToString())
                UIUtilities.ShowDialog(Me, "Save Role", "Role updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    'Private Function CheckDuplicateRole(ByVal objICRole As ICRole) As Boolean
    '    Dim CollObjICRole As New ICRoleCollection



    '    CollObjICRole.Query.Where(CollObjICRole.Query.RoleName.ToLower.Trim = objICRole.RoleName.ToLower.Trim And CollObjICRole.Query.RoleID <> objICRole.RoleID)
    '    CollObjICRole.Query.Load()

    '    If CollObjICRole.Count > 0 Then

    '        Return True
    '    Else
    '        Return False
    '    End If


    'End Function
#End Region
End Class

