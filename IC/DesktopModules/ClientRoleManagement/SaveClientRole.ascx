﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveClientRole.ascx.vb"
    Inherits="DesktopModules_ClientRoleManagement_SaveClientRole" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
               <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior2" EmptyMessage="type.."
            Type="Number">
                    </telerik:NumericTextBoxSetting>                           
      <%--  <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="true"
            ValidationExpression="^[ a-zA-Z][ a-zA-Z\\s]+$" ErrorMessage="Invalid Role Name" EmptyMessage="">
            <TargetControls>
                <telerik:TargetInput ControlID="txtICClientRoleName" />
            </TargetControls>
        </telerik:RegExpTextBoxSetting>--%>

          <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="true"
            ValidationExpression="^[0-9a-zA-Z ]+$" ErrorMessage="Invalid Role Name" EmptyMessage="">
            <TargetControls>
                <telerik:TargetInput ControlID="txtICClientRoleName" />
            </TargetControls>
        </telerik:RegExpTextBoxSetting>



        
        <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior4" Validation-IsRequired="false"
            EmptyMessage="">
            <TargetControls>
                <telerik:TargetInput ControlID="txtICClinetRoleDescription" />
            </TargetControls>
        </telerik:RegExpTextBoxSetting>        
    </telerik:RadInputManager>
    <script language="javascript" type="text/javascript">
        function textboxMultilineMaxNumber(txt, maxLen) {
            try {
                if (txt.value.length > (maxLen - 1)) return false;
            } catch (e) {
            }
        }
    </script>
<table width="100%">
<tr align="left" valign="top" style="width: 100%">
        <td align="left"  valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>            
            <br />
               Note:&nbsp; 
            Fields marked as        <asp:Label ID="Label1" runat="server" Text="*"  ForeColor="Red"></asp:Label>   
         
                    
        &nbsp;are required.
                </td>
        <td align="left" valign="top" colspan="2">           
         </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
    <td align="left"  valign="top" colspan="2">
         
        </td>
        <td align="left" valign="top" colspan="2">           
         </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblICRoleName" runat="server" Text="Role Name" CssClass="lbl"></asp:Label><asp:Label ID="Label2" runat="server" Text="*"  ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblICDescription" runat="server" Text="Description" 
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtICClientRoleName" runat="server" CssClass="txtbox" 
                MaxLength="50"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtICClinetRoleDescription" runat="server" CssClass="txtbox" 
                MaxLength="200" TextMode="MultiLine" Height="70px" 
                onkeypress="return textboxMultilineMaxNumber(this,200)"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>    
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top"  width="25%">
            <asp:Label ID="lblIsActive" runat="server" Text="Is Active" CssClass="lbl"></asp:Label>
            </td>
            <td align="left" valign="top"  width="25%">
                &nbsp;</td>
            <td align="left" valign="top"  width="25%">
                &nbsp;</td>
            <td align="left" valign="top" width="25%">
                &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top"  width="25%">
            <asp:CheckBox ID="chkActive" runat="server" Text=" " 
                CssClass="chkBox" Checked="True" />
        </td>
            <td align="left" valign="top"  width="25%">
                &nbsp;</td>
            <td align="left" valign="top"  width="25%">
                &nbsp;</td>
            <td align="left" valign="top" width="25%">
                &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top"  width="25%">
            &nbsp;</td>
            <td align="left" valign="top"  width="25%">
                &nbsp;</td>
            <td align="left" valign="top"  width="25%">
            &nbsp;</td>
            <td align="left" valign="top" width="25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
        &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" 
                Width="67px" />
        &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" 
                CausesValidation="False" />
            &nbsp;
        </td>
    </tr>
</table>