﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_ClientRoleManagement_ViewClientRole
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
    
#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnAddICRole.Visible = CBool(htRights("Add"))
                btnDeleteRoles.Visible = CBool(htRights("Delete"))
                LoadICClientRoles(0, Me.gvICClientRoles.PageSize, Me.gvICClientRoles, True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client Roles Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
#Region "Button Events"
    Protected Sub btnAddICRole_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddICRole.Click
        Response.Redirect(NavigateURL("SaveClientRole", "&mid=" & Me.ModuleId & "&id=0"), False)
    End Sub
#End Region
#Region "Grid View Events"
    Protected Sub gvICRoles_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvICClientRoles.PageIndexChanged
        Try
            gvICClientRoles.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub gvICRoles_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvICClientRoles.ItemCommand
        Try
            Dim userCtrl As New RoleController
            Dim arrLst As New ArrayList
            Dim objICClientRole As New ICRole
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    'Dim iRole As RoleInfo = ICRoleController.GetRoleByID(e.CommandArgument.ToString(), Me.PortalId)
                    objICClientRole.LoadByPrimaryKey(e.CommandArgument.ToString)
                    arrLst = userCtrl.GetUsersInRole(Me.PortalId, objICClientRole.RoleName.ToString())
                    If arrLst.Count > 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Roles", "Role cannot be deleted because users are assigned to this role.", ICBO.IC.Dialogmessagetype.Failure)
                        LoadICClientRoles(Me.gvICClientRoles.CurrentPageIndex + 1, Me.gvICClientRoles.PageSize, Me.gvICClientRoles, False)
                        Exit Sub
                    Else
                        If objICClientRole.LoadByPrimaryKey(e.CommandArgument.ToString) Then

                            'If objICRole.IsApproved = True Then
                            '    UIUtilities.ShowDialog(Me, "Roles", "Role is approved and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
                            '    ICRoleController.GetAllICRolesForRadGrid("Bank Role", Me.gvICRoles.CurrentPageIndex, Me.gvICRoles.PageSize, Me.gvICRoles, True)
                            '    Exit Sub

                            'Else
                            ICRoleController.DeleteRoleFromIC(e.CommandArgument.ToString(), Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            ICRoleController.DeleteICRole(e.CommandArgument.ToString(), Me.PortalId)

                            'ICUtilities.AddAuditTrail("Role : " & iRole.RoleName.ToString() & " Deleted", "Role", e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                            UIUtilities.ShowDialog(Me, "Delete Roles", "Role deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                            ICRoleController.GetAllICRolesForRadGrid("Company Role", Me.gvICClientRoles.CurrentPageIndex, Me.gvICClientRoles.PageSize, Me.gvICClientRoles, True)
                            'End If


                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
            Else
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End If
           
        End Try
    End Sub

#End Region
#Region "Other Functions/Routines"
    
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            ' str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function
    Private Sub LoadICClientRoles(ByVal PageIndex As Integer, ByVal PageSize As Integer, ByVal RadGrid As radgrid, ByVal DoDataBind As Boolean)
        Try
            ICRoleController.GetAllICRolesForRadGrid("Company Role", PageIndex, PageSize, RadGrid, DoDataBind)
            If gvICClientRoles.Items.Count > 0 Then
                gvICClientRoles.Visible = True
                lblRNF.Visible = False
                btnDeleteRoles.Visible = CBool(htRights("Delete"))

                gvICClientRoles.Columns(5).Visible = CBool(htRights("Delete"))
            Else
                gvICClientRoles.Visible = False
                lblRNF.Visible = True

                btnDeleteRoles.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

    Protected Sub btnDeleteRoles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteRoles.Click
        If Page.IsValid Then
            Try
                Dim rowGVICClientRoles As GridDataItem
                Dim chkSelect As CheckBox
                Dim RoleID As String = ""
                Dim PassToDeleteCount As Integer = 0
                Dim FailToDeleteCount As Integer = 0
                Dim userCtrl As New RoleController
                Dim arrLst As New ArrayList
                Dim objICClientRole As ICRole


                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Client Roles", "Please select atleast one role.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVICClientRoles In gvICClientRoles.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        RoleID = ""
                        RoleID = rowGVICClientRoles.GetDataKeyValue("RoleID").ToString
                        objICClientRole = New ICRole
                        objICClientRole.es.Connection.CommandTimeout = 3600
                        If objICClientRole.LoadByPrimaryKey(RoleID.ToString()) Then
                            'Dim iRole As RoleInfo = ICRoleController.GetRoleByID(objICClientRole.RoleID, Me.PortalId)
                            arrLst = userCtrl.GetUsersInRole(Me.PortalId, objICClientRole.RoleName.ToString())
                            If arrLst.Count > 0 Then
                                FailToDeleteCount = FailToDeleteCount + 1
                            Else
                                Try

                                    ICRoleController.DeleteRoleFromIC(objICClientRole.RoleID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                    ICRoleController.DeleteICRole(objICClientRole.RoleID.ToString, Me.PortalId)
                                    PassToDeleteCount = PassToDeleteCount + 1
                                Catch ex As Exception
                                    If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                        FailToDeleteCount = FailToDeleteCount + 1
                                        Continue For
                                    Else
                                        FailToDeleteCount = FailToDeleteCount + 1
                                        Continue For
                                    End If
                                End Try
                            End If
                        End If
                    End If

                Next

                If Not PassToDeleteCount = 0 And Not FailToDeleteCount = 0 Then
                    'ICRoleController.GetAllICRolesForRadGrid("Bank Role", Me.gvICRoles.CurrentPageIndex + 1, Me.gvICRoles.PageSize, Me.gvICRoles, True)
                    LoadICClientRoles(Me.gvICClientRoles.CurrentPageIndex + 1, Me.gvICClientRoles.PageSize, Me.gvICClientRoles, True)
                    UIUtilities.ShowDialog(Me, "Client Roles", "[ " & PassToDeleteCount.ToString & " ] Role(s) deleted successfuly.<br /> [ " & FailToDeleteCount.ToString & " ] Role(s) can not be deleted due to following reason: <br /> 1. Role is assigned to user<br /> 2. Delete associated records", ICBO.IC.Dialogmessagetype.Failure)

                    Exit Sub
                ElseIf Not PassToDeleteCount = 0 And FailToDeleteCount = 0 Then
                    LoadICClientRoles(Me.gvICClientRoles.CurrentPageIndex + 1, Me.gvICClientRoles.PageSize, Me.gvICClientRoles, True)
                    UIUtilities.ShowDialog(Me, "Client Roles", PassToDeleteCount.ToString & " Role(s) deleted successfuly.", ICBO.IC.Dialogmessagetype.Success)

                    Exit Sub
                ElseIf Not FailToDeleteCount = 0 And PassToDeleteCount = 0 Then
                    LoadICClientRoles(Me.gvICClientRoles.CurrentPageIndex + 1, Me.gvICClientRoles.PageSize, Me.gvICClientRoles, True)
                    UIUtilities.ShowDialog(Me, "Client Roles", "[ " & FailToDeleteCount.ToString & " ] Role(s) can not be deleted due to following reasons: <br /> 1. Role is assigned to user<br /> 2. Delete associated records", ICBO.IC.Dialogmessagetype.Failure)

                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Function CheckgvClientRoleCheckedForProcessAll() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvICClientRoles.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub gvICRoles_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvICClientRoles.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvICClientRoles.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvICRoles_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvICClientRoles.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAllClientRoles"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvICClientRoles.MasterTableView.ClientID & "','0');"
        End If


    End Sub

    Protected Sub gvICRoles_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvICClientRoles.NeedDataSource
        Try

            LoadICClientRoles(Me.gvICClientRoles.CurrentPageIndex + 1, Me.gvICClientRoles.PageSize, Me.gvICClientRoles, False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
