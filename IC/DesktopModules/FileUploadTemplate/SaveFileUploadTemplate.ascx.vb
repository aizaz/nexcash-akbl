﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_FileUploadTemplate_SaveFileUploadTemplate
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private FileUploadTemplateCode As String
    Private htRights As Hashtable

    Protected Sub DesktopModules_FileUploadTemplate_SaveFileUploadTemplate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            FileUploadTemplateCode = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                'LoadTemplateFieldsList()
                lblAssignedSingleTemplateFieldsFields.Visible = False
                lblAssignedDetailField.Visible = False
                lblDetailFields.Visible = False
                lblTemplateFieldHeader.Visible = False
                If FileUploadTemplateCode.ToString() = "0" Then
                    Clear()
                    lblPageHeader.Text = "Add File Upload Template"
                    btnSave.Text = "Save"
                    tblAssignedDetailFields.Style.Add("display", "none")
                    tblAssignedFieldsForSingleTemplate.Style.Add("display", "none")
                    tblFieldList.Style.Add("display", "none")
                    tblDetail.Style.Add("display", "none")
                    pnlHeaderIdentifier.Style.Add("display", "none")
                    'pnlDelimeter.Style.Add("display", "none")
                    'pnlDetail.Style.Add("display", "none")
                    rimDelemiter.Enabled = False
                    rimHeaderAndDetail.Enabled = False
                    rimSheet.Enabled = False
                    btnSave.Visible = False

                Else
                    'If ICTemplateFieldListController.CheckFileStatusAgainstTemplate(FileUploadTemplateCode) = True Then
                    '    UIUtilities.ShowDialog(Me, "Error", "File upload template can not updated due to following reason: <br /> 1. Files exists in pending read status", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    '    Exit Sub
                    'End If
                    lblPageHeader.Text = "Edit File Upload Template"
                    btnSave.Text = "Update"
                    Clear()
                    Dim objICTemplate As New ICTemplate
                    objICTemplate.es.Connection.CommandTimeout = 3600
                    If objICTemplate.LoadByPrimaryKey(FileUploadTemplateCode) Then
                        txtFileUploadTemplateName.Text = objICTemplate.TemplateName.ToString()
                        txtFileUploadTemplateCode.Text = objICTemplate.FileUploadTemplateCode.ToString
                        If objICTemplate.TemplateFormat.Equals(".txt") Then
                            RbFormat.SelectedIndex = 0
                        End If
                        If objICTemplate.TemplateFormat.Equals(".xls") Then
                            RbFormat.SelectedIndex = 2
                        End If
                        If objICTemplate.TemplateFormat.Equals(".xlsx") Then
                            RbFormat.SelectedIndex = 3
                        End If
                        If objICTemplate.TemplateFormat.Equals(".csv") Then
                            RbFormat.SelectedIndex = 1
                        End If
                        SetFileFormat()
                        SetFieldFormat()
                        If objICTemplate.TemplateFormat.Equals(".xls") Then
                            txtSheetName.Text = objICTemplate.SheetName.ToString()
                        End If
                        If objICTemplate.TemplateFormat.Equals(".xlsx") Then
                            txtSheetName.Text = objICTemplate.SheetName.ToString()
                        End If
                        If objICTemplate.TemplateFormat.Equals(".csv") Then
                            txtSheetName.Text = objICTemplate.SheetName.ToString()
                        End If
                        If objICTemplate.TemplateFormat.Equals(".xls") Or objICTemplate.TemplateFormat.Equals(".xlsx") Or objICTemplate.TemplateFormat.Equals(".csv") Then
                            LoadAssignedFieldsListForTemplate(Me.gvAssignedTemplateFieldForSingleTemplate.CurrentPageIndex + 1, Me.gvAssignedTemplateFieldForSingleTemplate.PageSize, True, "TemplateHeader", "NonFlexi")
                            LoadFieldsList(Me.gvFieldsList.CurrentPageIndex + 1, Me.gvFieldsList.PageSize, True)
                            CheckSelectCheckBoxOnUpDateTemplate("Header")
                            lblAssignedDetailField.Visible = False
                            lblDetailFields.Visible = False
                            tblAssignedDetailFields.Style.Add("display", "none")
                            tblDetail.Style.Add("display", "none")
                            'pnlAssignedDetailFields.Visible = False
                            'pnlDetail.Visible = False
                        End If
                        If objICTemplate.TemplateFormat.Equals(".txt") Then
                            If Not objICTemplate.IsFixLength Is Nothing Then
                                If objICTemplate.IsFixLength = True Then
                                    rbFieldFormate.SelectedValue = 0
                                    LoadAssignedFieldsListForTemplate(Me.gvAssignedTemplateFieldForSingleTemplate.CurrentPageIndex + 1, Me.gvAssignedTemplateFieldForSingleTemplate.PageSize, True, "TemplateHeader", "NonFlexi")
                                    LoadFieldsList(Me.gvFieldsList.CurrentPageIndex + 1, Me.gvFieldsList.PageSize, True)
                                    CheckSelectCheckBoxOnUpDateTemplate("Header")
                                    tblAssignedDetailFields.Style.Add("display", "none")
                                    tblDetail.Style.Add("display", "none")
                                    lblAssignedDetailField.Visible = False
                                    lblDetailFields.Visible = False
                                End If
                                If objICTemplate.IsFixLength = False Then
                                    rbFieldFormate.SelectedValue = 1
                                End If
                                SetFieldFormat()
                                CheckSelectCheckBoxOnUpDateTemplate("Header")
                                If objICTemplate.TemplateType = "Single" Then
                                    rbTemplateType.SelectedValue = "Single"
                                    SetDelimitAndHeaderAndDetail()
                                    txtdelimiter.Text = objICTemplate.FieldDelimiter
                                    LoadAssignedFieldsListForTemplate(Me.gvAssignedTemplateFieldForSingleTemplate.CurrentPageIndex + 1, Me.gvAssignedTemplateFieldForSingleTemplate.PageSize, True, "TemplateHeader", "NonFlexi")
                                    LoadFieldsList(Me.gvFieldsList.CurrentPageIndex + 1, Me.gvFieldsList.PageSize, True)
                                    CheckSelectCheckBoxOnUpDateTemplate("Header")

                                ElseIf objICTemplate.TemplateType = "Dual" Then
                                    rbTemplateType.SelectedValue = "Dual"
                                    SetDelimitAndHeaderAndDetail()

                                    txtdelimiter.Text = objICTemplate.FieldDelimiter
                                    txtHeaderIdentifier.Text = objICTemplate.HeaderIdentifier
                                    txtDetailIdentifier.Text = objICTemplate.DetailIdentifier
                                    LoadAssignedFieldsListForTemplate(Me.gvAssignedTemplateFieldForSingleTemplate.CurrentPageIndex + 1, Me.gvAssignedTemplateFieldForSingleTemplate.PageSize, True, "TemplateHeader", "NonFlexi")
                                    LoadFieldsList(Me.gvFieldsList.CurrentPageIndex + 1, Me.gvFieldsList.PageSize, True)
                                    CheckSelectCheckBoxOnUpDateTemplate("Header")
                                    LoadAssignedFieldsListForTemplate(Me.gvAssignedDetailFields.CurrentPageIndex + 1, Me.gvAssignedDetailFields.PageSize, True, "Detail", "FixFormat")
                                    LoadDetailFieldListForDualTemplateType(Me.gvDetailFields.CurrentPageIndex + 1, Me.gvDetailFields.PageSize, True)
                                    CheckSelectCheckBoxOnUpDateTemplate("Detail")



                                End If
                            End If
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckHeaderDetailAndDelimiterIDentifier() As Boolean
        Dim Result As Boolean = False
        If (txtdelimiter.Text = txtHeaderIdentifier.Text) Or (txtdelimiter.Text = txtDetailIdentifier.Text) Then
            Result = True
            Return Result
            Exit Function
        ElseIf (txtHeaderIdentifier.Text = txtdelimiter.Text) Or (txtHeaderIdentifier.Text = txtDetailIdentifier.Text) Then
            Result = True
            Return Result
            Exit Function
        ElseIf (txtDetailIdentifier.Text = txtdelimiter.Text) Or (txtDetailIdentifier.Text = txtHeaderIdentifier.Text) Then
            Result = True
            Return Result
            Exit Function
        End If
        Return Result
    End Function
    Private Sub CheckSelectCheckBoxOnUpDateTemplate(ByVal GridType As String)

        Dim chkPOAss, chkDDAss, chkIBFTAss, chkFTAss, chkCQAss, chkCOTC, chkBillPayment As CheckBox
        Dim chkSelectField, chkPOField, chkDDField, chkIBFTField, chkFTField, chkCQField, chkCOTCField, chkBillPaymentField As CheckBox
        Dim txtFieldLengthAss, txtFieldLengthField As TextBox
        If GridType = "Detail" Then
            Dim AssignedFieldID As Integer = 0
            Dim DetailFieldID As Integer = 0
            For Each gvAssignedDetailRow As GridDataItem In gvAssignedDetailFields.Items
                AssignedFieldID = gvAssignedDetailRow.GetDataKeyValue("FieldID")
                For Each gvDetailFieldsRow As GridDataItem In gvDetailFields.Items
                    DetailFieldID = gvDetailFieldsRow.GetDataKeyValue("FieldID")
                    If AssignedFieldID = DetailFieldID Then

                        chkPOAss = New CheckBox
                        chkDDAss = New CheckBox
                        chkIBFTAss = New CheckBox
                        chkFTAss = New CheckBox
                        chkCQAss = New CheckBox
                        chkCOTC = New CheckBox
                        chkBillPayment = New CheckBox
                        chkCQAss = DirectCast(gvAssignedDetailRow.Cells(2).FindControl("chkRequiredCheque"), CheckBox)
                        chkPOAss = DirectCast(gvAssignedDetailRow.Cells(3).FindControl("chkRequiredPO"), CheckBox)
                        chkDDAss = DirectCast(gvAssignedDetailRow.Cells(4).FindControl("chkRequiredDD"), CheckBox)
                        chkIBFTAss = DirectCast(gvAssignedDetailRow.Cells(5).FindControl("chkRequiredIBFT"), CheckBox)
                        chkFTAss = DirectCast(gvAssignedDetailRow.Cells(6).FindControl("chkRequiredFT"), CheckBox)
                        chkCOTC = DirectCast(gvAssignedDetailRow.Cells(7).FindControl("chkRequiredCOTC"), CheckBox)
                        chkBillPayment = DirectCast(gvAssignedDetailRow.Cells(8).FindControl("chkRequiredBillPayment"), CheckBox)

                        chkSelectField = New CheckBox
                        chkPOField = New CheckBox
                        chkDDField = New CheckBox
                        chkIBFTField = New CheckBox
                        chkFTField = New CheckBox
                        chkCQField = New CheckBox
                        chkCOTCField = New CheckBox
                        chkBillPaymentField = New CheckBox

                        chkSelectField = DirectCast(gvDetailFieldsRow.Cells(0).FindControl("chkSelect"), CheckBox)
                        chkPOField = DirectCast(gvDetailFieldsRow.Cells(3).FindControl("chkRequiredPO"), CheckBox)
                        chkDDField = DirectCast(gvDetailFieldsRow.Cells(4).FindControl("chkRequiredDD"), CheckBox)
                        chkIBFTField = DirectCast(gvDetailFieldsRow.Cells(5).FindControl("chkRequiredIBFT"), CheckBox)
                        chkFTField = DirectCast(gvDetailFieldsRow.Cells(6).FindControl("chkRequiredFT"), CheckBox)
                        chkCQField = DirectCast(gvDetailFieldsRow.Cells(2).FindControl("chkRequiredCheque"), CheckBox)
                        chkCOTCField = DirectCast(gvDetailFieldsRow.Cells(7).FindControl("chkRequiredCOTC"), CheckBox)
                        chkBillPaymentField = DirectCast(gvDetailFieldsRow.Cells(8).FindControl("chkRequiredBillPayment"), CheckBox)


                        chkSelectField.Checked = True
                        chkPOField.Checked = chkPOAss.Checked
                        chkDDField.Checked = chkDDAss.Checked
                        chkIBFTField.Checked = chkIBFTAss.Checked
                        chkFTField.Checked = chkFTAss.Checked
                        chkCQField.Checked = chkCQAss.Checked
                        chkCOTCField.Checked = chkCOTC.Checked
                        chkBillPaymentField.Checked = chkBillPayment.Checked
                    End If
                Next
            Next
        ElseIf GridType = "Header" Then
            Dim AssignedHeaderFieldID As Integer = 0
            Dim FieldListID As Integer = 0
            For Each gvAssignedHeaderRow As GridDataItem In gvAssignedTemplateFieldForSingleTemplate.Items
                AssignedHeaderFieldID = gvAssignedHeaderRow.GetDataKeyValue("FieldID")
                For Each gvHeaderFieldsRow As GridDataItem In gvFieldsList.Items
                    FieldListID = gvHeaderFieldsRow.GetDataKeyValue("FieldID")
                    If AssignedHeaderFieldID = FieldListID Then

                        chkPOAss = New CheckBox
                        chkDDAss = New CheckBox
                        chkIBFTAss = New CheckBox
                        chkFTAss = New CheckBox
                        chkCQAss = New CheckBox
                        chkCOTC = New CheckBox
                        chkBillPayment = New CheckBox
                        chkCQAss = DirectCast(gvAssignedHeaderRow.Cells(2).FindControl("chkRequiredCheque"), CheckBox)
                        chkPOAss = DirectCast(gvAssignedHeaderRow.Cells(3).FindControl("chkRequiredPO"), CheckBox)
                        chkDDAss = DirectCast(gvAssignedHeaderRow.Cells(4).FindControl("chkRequiredDD"), CheckBox)
                        chkIBFTAss = DirectCast(gvAssignedHeaderRow.Cells(5).FindControl("chkRequiredIBFT"), CheckBox)
                        chkFTAss = DirectCast(gvAssignedHeaderRow.Cells(6).FindControl("chkRequiredFT"), CheckBox)
                        chkCOTC = DirectCast(gvAssignedHeaderRow.Cells(7).FindControl("chkRequiredCOTC"), CheckBox)
                        chkBillPayment = DirectCast(gvAssignedHeaderRow.Cells(8).FindControl("chkRequiredBillPayment"), CheckBox)
                        txtFieldLengthAss = DirectCast(gvAssignedHeaderRow.Cells(1).FindControl("txtFieldDBLength"), TextBox)

                        chkSelectField = New CheckBox
                        chkPOField = New CheckBox
                        chkDDField = New CheckBox
                        chkIBFTField = New CheckBox
                        chkFTField = New CheckBox
                        chkCQField = New CheckBox
                        chkCOTCField = New CheckBox
                        chkBillPaymentField = New CheckBox


                        chkSelectField = DirectCast(gvHeaderFieldsRow.Cells(0).FindControl("chkSelect"), CheckBox)
                        chkPOField = DirectCast(gvHeaderFieldsRow.Cells(4).FindControl("chkRequiredPO"), CheckBox)
                        chkDDField = DirectCast(gvHeaderFieldsRow.Cells(5).FindControl("chkRequiredDD"), CheckBox)
                        chkIBFTField = DirectCast(gvHeaderFieldsRow.Cells(6).FindControl("chkRequiredIBFT"), CheckBox)
                        chkFTField = DirectCast(gvHeaderFieldsRow.Cells(7).FindControl("chkRequiredFT"), CheckBox)
                        chkCQField = DirectCast(gvHeaderFieldsRow.Cells(3).FindControl("chkRequiredCheque"), CheckBox)
                        chkCOTCField = DirectCast(gvHeaderFieldsRow.Cells(8).FindControl("chkRequiredCOTC"), CheckBox)
                        chkBillPaymentField = DirectCast(gvHeaderFieldsRow.Cells(9).FindControl("chkRequiredBillPayment"), CheckBox)
                        txtFieldLengthField = DirectCast(gvHeaderFieldsRow.Cells(2).FindControl("txtFieldDBLength"), TextBox)


                        chkSelectField.Checked = True
                        chkPOField.Checked = chkPOAss.Checked
                        chkDDField.Checked = chkDDAss.Checked
                        chkIBFTField.Checked = chkIBFTAss.Checked
                        chkFTField.Checked = chkFTAss.Checked
                        chkCQField.Checked = chkCQAss.Checked
                        chkCOTCField.Checked = chkCOTC.Checked
                        chkBillPaymentField.Checked = chkBillPayment.Checked
                        txtFieldLengthField.Text = txtFieldLengthAss.Text
                    End If
                Next
            Next
        End If
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client Bulk Upload Template Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadFieldsList(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean)
        Try
            ICTemplateFieldListController.GetFieldListForRadGridByFieldType(Me.gvFieldsList, PageNumber, PageSize, DoDataBind, "NonFlexi", "FileUpload")

            gvFieldsList.Columns(2).Visible = False
            If gvFieldsList.Items.Count > 0 Then
                tblFieldList.Style.Remove("display")
                lblTemplateFieldHeader.Visible = True
                If rbFieldFormate.SelectedValue.ToString = "0" Then
                    gvFieldsList.Columns(2).Visible = True
                End If
                If rbTemplateType.SelectedValue.ToString = "Dual" Then
                    lblTemplateFieldHeader.Text = "Header"
                Else
                    lblTemplateFieldHeader.Text = "Fields List"
                End If
                If FileUploadTemplateCode.ToString = "0" Then
                    btnSave.Visible = CBool(htRights("Add"))
                ElseIf Not FileUploadTemplateCode.ToString = "0" Then
                    btnSave.Visible = CBool(htRights("Update"))
                End If
            Else
                tblFieldList.Style.Add("display", "none")
                lblTemplateFieldHeader.Visible = False
                If Not rbTemplateType.SelectedValue.ToString = "Dual" Then
                    lblTemplateFieldHeader.Text = "Fields List"
                End If
                btnSave.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadAssignedFieldsListForTemplate(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal PannelType As String, ByVal FieldType As String)
        Try
            If PannelType = "TemplateHeader" Then
                ICTemplateFieldListController.GetAllTemplateFieldByTemplateIDForRadGrid(FileUploadTemplateCode, PageNumber, PageSize, Me.gvAssignedTemplateFieldForSingleTemplate, DoDataBind, FieldType.ToString)
                gvAssignedTemplateFieldForSingleTemplate.Columns(1).Visible = False
                If gvAssignedTemplateFieldForSingleTemplate.Items.Count > 0 Then
                    tblAssignedFieldsForSingleTemplate.Style.Remove("display")
                    lblAssignedSingleTemplateFieldsFields.Visible = True
                    lblRNFAssignedTemplateFields.Visible = False
                    If rbFieldFormate.SelectedValue.ToString = "0" Then
                        gvAssignedTemplateFieldForSingleTemplate.Columns(1).Visible = True
                    End If
                    If rbTemplateType.SelectedValue.ToString = "Dual" Then
                        lblAssignedSingleTemplateFieldsFields.Text = "Header Assigned Fields"
                    Else
                        lblAssignedSingleTemplateFieldsFields.Text = "Assigned Template Fields"
                    End If
                    gvAssignedTemplateFieldForSingleTemplate.Columns(8).Visible = CBool(htRights("Delete"))
                    If CBool(htRights("Update")) = True Then
                        tblSetFieldListOrder.Style.Remove("display")
                    End If
                Else
                    lblAssignedSingleTemplateFieldsFields.Visible = True
                    lblRNFAssignedTemplateFields.Visible = True
                    tblAssignedFieldsForSingleTemplate.Style.Add("display", "none")
                    btnSetOrder.Visible = False
                End If
            ElseIf PannelType = "Detail" Then

                ICTemplateFieldListController.GetAllTemplateFieldByTemplateIDForRadGrid(FileUploadTemplateCode, PageNumber, PageSize, Me.gvAssignedDetailFields, DoDataBind, FieldType.ToString)
                If gvAssignedDetailFields.Items.Count > 0 Then
                    tblAssignedDetailFields.Style.Remove("display")
                    lblAssignedDetailField.Visible = True
                    lblRNFAssignedDetailFields.Visible = False
                    gvAssignedDetailFields.Columns(1).Visible = False
                    If rbTemplateType.SelectedValue.ToString = "Dual" Then
                        lblAssignedDetailField.Text = "Assigned Detail Fields"
                    Else
                        lblAssignedDetailField.Text = "Assigned Detail Fields"
                    End If
                    gvAssignedDetailFields.Columns(8).Visible = CBool(htRights("Delete"))
                    If CBool(htRights("Update")) = True Then
                        tblDetailSetOrder.Style.Remove("display")
                    End If
                Else
                    lblAssignedDetailField.Visible = True
                    tblAssignedDetailFields.Style.Add("display", "none")
                    lblRNFAssignedDetailFields.Visible = True
                    If rbTemplateType.SelectedValue.ToString = "Dual" Then
                        lblAssignedDetailField.Text = "Assigned Detail Fields"
                    Else
                        lblAssignedDetailField.Text = "Assigned Detail Fields"
                    End If
                End If
                'gvAssignedDetailFields.Columns(1).Visible = False
                'If gvAssignedDetailFields.Items.Count > 0 Then

                '    If rbFieldFormate.SelectedValue.ToString = "0" Then
                '        gvAssignedDetailFields.Columns(1).Visible = True
                '    End If
                'Else
                '    pnlAssignedDetailFields.Style.Add("display", "none")
                'End If

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadDetailFieldListForDualTemplateType(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean)
        Try
            ICTemplateFieldListController.GetFieldListForRadGridByFieldType(Me.gvDetailFields, PageNumber, PageSize, DoDataBind, "FixFormat", True)
            If gvDetailFields.Items.Count > 0 Then
                tblDetail.Style.Remove("display")
                lblDetailFields.Visible = True
                If rbTemplateType.SelectedValue.ToString = "Dual" Then
                    lblDetailFields.Text = "Detail"
                Else
                    lblDetailFields.Text = "Detail"
                End If
            Else
                tblDetail.Style.Add("display", "none")
                lblDetailFields.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Private Sub Clear()
        txtFileUploadTemplateName.Text = ""
        txtSheetName.Text = ""
        rbFieldFormate.ClearSelection()
        RbFormat.ClearSelection()
        tblXLS.Style.Add("display", "none")
        tblTXT.Style.Add("display", "none")
        lblSheetOrFileFormat.Style.Add("display", "none")
        txtSheetName.Text = ""
        txtdelimiter.Text = ""
        lblReqSheetNameOrFixLength.Visible = False
        rimDelemiter.Enabled = False
        rimSheet.Enabled = False
        rfvFieldFormat.Enabled = False


    End Sub

    Dim RagExp_Valid As Boolean
    Private Function CheckGVDetailFieldsSelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvDetailFieldsRow As GridDataItem In gvDetailFields.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvDetailFieldsRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        'For Each validator As BaseValidator In Page.Validators
        '    If validator.Enabled = True And validator.IsValid = False Then
        '        Dim clientID As String
        '        clientID = validator.ClientID

        '    End If
        'Next

        If Page.IsValid Then
            Try

                If rbTemplateType.SelectedValue.ToString = "Dual" Then
                    If CheckHeaderDetailAndDelimiterIDentifier() = True Then
                        UIUtilities.ShowDialog(Me, "Error", "Duplicate header, detail and delimiter character is not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    If CheckGVDetailFieldsSelected() = False Then
                        UIUtilities.ShowDialog(Me, "Error", "Please select one field from detail grid.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If

                End If


                Dim objICTemplate As New ICTemplate
                Dim objICTemplateFrom As New ICTemplate
                Dim objICTemplateFieldList As New ICTemplateFields
                Dim SavedTemplateID As Integer = 0
                objICTemplate.es.Connection.CommandTimeout = 3600
                objICTemplateFrom.es.Connection.CommandTimeout = 3600
                objICTemplateFieldList.es.Connection.CommandTimeout = 3600

                If Not FileUploadTemplateCode.ToString = "0" Then
                    objICTemplateFrom.LoadByPrimaryKey(FileUploadTemplateCode)
                End If

                objICTemplate.TemplateName = txtFileUploadTemplateName.Text.ToString()
                objICTemplate.TemplateFormat = RbFormat.Text.ToString()
                objICTemplate.FileUploadTemplateCode = txtFileUploadTemplateCode.Text.ToString()

                If RbFormat.Text.ToString() = ".txt" Then
                    If rbFieldFormate.SelectedIndex = 0 Then
                        objICTemplate.IsFixLength = True
                    Else
                        objICTemplate.IsFixLength = False
                    End If
                    If rbFieldFormate.SelectedIndex = 1 Then
                        objICTemplate.FieldDelimiter = txtdelimiter.Text
                    End If
                End If
                If rbTemplateType.SelectedValue.ToString = "Single" Then
                    objICTemplate.TemplateType = "Single"
                ElseIf rbTemplateType.SelectedValue.ToString = "Dual" Then
                    objICTemplate.TemplateType = "Dual"
                    objICTemplate.HeaderIdentifier = txtHeaderIdentifier.Text.ToString
                    objICTemplate.DetailIdentifier = txtDetailIdentifier.Text.ToString
                End If

                If RbFormat.Text.ToString() = ".xls" Then
                    objICTemplate.SheetName = txtSheetName.Text.ToString()
                End If
                If RbFormat.Text.ToString() = ".xlsx" Then
                    objICTemplate.SheetName = txtSheetName.Text.ToString()
                End If
                If RbFormat.Text.ToString() = ".csv" Then
                    objICTemplate.SheetName = txtSheetName.Text.ToString()
                End If

                objICTemplate.IsActive = True
                If FileUploadTemplateCode = "0" Then
                    objICTemplate.CreateBy = Me.UserId
                    objICTemplate.CreateDate = Date.Now
                    objICTemplate.Creater = Me.UserId
                    objICTemplate.CreationDate = Date.Now
                    If CheckDuplicateTemplateNameAndCode(objICTemplate, False) = False Then
                        SavedTemplateID = ICFileUploadTemplateController.AddFileUploadTemplate(objICTemplate, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        AddFileUploadTemplateFields(SavedTemplateID.ToString, False)
                        UIUtilities.ShowDialog(Me, "File Upload Template", "File Upload Template saved successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL("SaveFileUploadTemplate", "&mid=" & Me.ModuleId & "&id=" & SavedTemplateID))


                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Duplicate file upload template is not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else
                    objICTemplate.TemplateID = FileUploadTemplateCode
                    objICTemplate.CreateBy = Me.UserId
                    objICTemplate.CreateDate = Date.Now
                    If CheckDuplicateTemplateNameAndCode(objICTemplate, True) = False Then
                        ICFileUploadTemplateController.AddFileUploadTemplate(objICTemplate, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        AddFileUploadTemplateFields(objICTemplate.TemplateID.ToString, True)
                        UpdateAPNFileuploadTemplateFields(objICTemplate, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        UIUtilities.ShowDialog(Me, "File Upload Template", "File Upload template updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Duplicate file upload template is not allowed.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                Clear()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Function GetLastFieldorder(ByVal templateid As Integer, ByVal FieldType As String) As Integer
        Dim objICTemplateFieldListColl As New ICTemplateFieldsCollection
        objICTemplateFieldListColl.es.Connection.CommandTimeout = 3600
        objICTemplateFieldListColl.Query.Select(objICTemplateFieldListColl.Query.FieldOrder)
        objICTemplateFieldListColl.Query.Where(objICTemplateFieldListColl.Query.TemplateID = templateid)
        If FieldType = "NonFlexi" Then
            objICTemplateFieldListColl.Query.Where(objICTemplateFieldListColl.Query.FieldType = "NonFlexi" Or objICTemplateFieldListColl.Query.FieldType = "DetailField")
        Else
            objICTemplateFieldListColl.Query.Where(objICTemplateFieldListColl.Query.FieldType = FieldType)
        End If
        objICTemplateFieldListColl.Query.es.Top = 1
        objICTemplateFieldListColl.Query.OrderBy(objICTemplateFieldListColl.Query.FieldOrder, EntitySpaces.DynamicQuery.esOrderByDirection.Descending)



        If objICTemplateFieldListColl.Query.Load Then
            Return objICTemplateFieldListColl(0).FieldOrder
        Else
            Return 0
        End If
    End Function
    Private Sub AddFileUploadTemplateFields(ByVal TemplateCode As String, ByVal IsUpDate As Boolean)
        Dim objICTemplate As New ICTemplate
        Dim objICTemplateField As ICTemplateFields
        Dim objICFieldsList As ICFieldsList
        Dim gvFieldListRow As GridDataItem
        Dim gvDeatilFieldListRow As GridDataItem
        Dim chkSelect, chkISPORequired, chkISDDRequired, chkISFTRequired, chkISIBFTRequired, chkISCQRequired, chkISCOTCRequired, chkISBPRequired As CheckBox
        Dim chkSelectDetail, chkISPORequiredDetail, chkISDDRequiredDetail, chkISFTRequiredDetail, chkISIBFTRequiredDetail, chkISCQRequiredDetail, chkISBPRequiredDetail, chkISCOTCRequiredDetail As CheckBox
        objICTemplate.es.Connection.CommandTimeout = 3600
        Dim txtFieldLength As TextBox
        objICTemplate.LoadByPrimaryKey(TemplateCode)

        If IsUpDate = True Then
            ICTemplateFieldListController.DeleteTemplateFieldForUpdate(TemplateCode, Me.UserId.ToString, Me.UserInfo.Username.ToString)
        End If

        For Each gvFieldListRow In gvFieldsList.Items

            chkSelect = New CheckBox
            chkSelect = DirectCast(gvFieldListRow.Cells(0).FindControl("chkSelect"), CheckBox)
            If chkSelect.Checked = True Then


                objICTemplateField = New ICTemplateFields
                objICFieldsList = New ICFieldsList


                objICTemplateField.es.Connection.CommandTimeout = 3600
                objICFieldsList.es.Connection.CommandTimeout = 3600


                objICFieldsList.LoadByPrimaryKey(gvFieldListRow.GetDataKeyValue("FieldID").ToString)


                chkISPORequired = New CheckBox
                chkISDDRequired = New CheckBox
                chkISFTRequired = New CheckBox
                chkISIBFTRequired = New CheckBox
                chkISCQRequired = New CheckBox
                chkISCOTCRequired = New CheckBox
                chkISBPRequired = New CheckBox
                chkISPORequired = DirectCast(gvFieldListRow.Cells(4).FindControl("chkRequiredPO"), CheckBox)
                chkISDDRequired = DirectCast(gvFieldListRow.Cells(5).FindControl("chkRequiredDD"), CheckBox)
                chkISFTRequired = DirectCast(gvFieldListRow.Cells(7).FindControl("chkRequiredFT"), CheckBox)
                chkISIBFTRequired = DirectCast(gvFieldListRow.Cells(6).FindControl("chkRequiredIBFT"), CheckBox)
                chkISCQRequired = DirectCast(gvFieldListRow.Cells(3).FindControl("chkRequiredCheque"), CheckBox)
                chkISCOTCRequired = DirectCast(gvFieldListRow.Cells(8).FindControl("chkRequiredCOTC"), CheckBox)
                chkISBPRequired = DirectCast(gvFieldListRow.Cells(9).FindControl("chkRequiredBillPayment"), CheckBox)



                objICTemplateField.FieldName = objICFieldsList.FieldName
                objICTemplateField.FieldType = objICFieldsList.FieldType
                objICTemplateField.FieldOrder = GetLastFieldorder(TemplateCode, "NonFlexi") + 1
                If objICTemplate.IsFixLength = True Then
                    txtFieldLength = New TextBox
                    txtFieldLength = DirectCast(gvFieldListRow.Cells(2).FindControl("txtFieldDBLength"), TextBox)
                    objICTemplateField.FixLength = CInt(txtFieldLength.Text)

                Else
                    objICTemplateField.FixLength = objICFieldsList.FieldDBLength

                End If
                'objICTemplateField.FixLength = objICFieldsList.FieldDBLength
                objICTemplateField.FieldID = objICFieldsList.FieldID
                objICTemplateField.TemplateID = TemplateCode
                objICTemplateField.CreateBy = Me.UserId
                objICTemplateField.CreateDate = Date.Now

                If chkSelect.Enabled = False Then
                    objICTemplateField.IsRequired = True
                    objICTemplateField.IsMustRequired = True
                Else
                    objICTemplateField.IsRequired = True
                    objICTemplateField.IsMustRequired = False
                End If

                If chkISCQRequired.Checked = True Then
                    If chkISCQRequired.Enabled = False Then
                        objICTemplateField.IsMustRequiredCheque = True
                        objICTemplateField.IsRequiredCheque = True
                    Else
                        objICTemplateField.IsMustRequiredCheque = False
                        objICTemplateField.IsRequiredCheque = True
                    End If

                ElseIf chkISCQRequired.Checked = False Then
                    objICTemplateField.IsMustRequiredCheque = False
                    objICTemplateField.IsRequiredCheque = False
                End If

                If chkISPORequired.Checked = True Then
                    If chkISPORequired.Enabled = False Then
                        objICTemplateField.IsMustRequiredPO = True
                        objICTemplateField.IsRequiredPO = True
                    Else
                        objICTemplateField.IsMustRequiredPO = False
                        objICTemplateField.IsRequiredPO = True
                    End If

                ElseIf chkISPORequired.Checked = False Then
                    objICTemplateField.IsMustRequiredPO = False
                    objICTemplateField.IsRequiredPO = False
                End If


                If chkISDDRequired.Checked = True Then
                    If chkISDDRequired.Enabled = False Then
                        objICTemplateField.IsMustRequiredDD = True
                        objICTemplateField.IsRequiredDD = True
                    Else
                        objICTemplateField.IsMustRequiredDD = False
                        objICTemplateField.IsRequiredDD = True
                    End If

                ElseIf chkISDDRequired.Checked = False Then
                    objICTemplateField.IsMustRequiredDD = False
                    objICTemplateField.IsRequiredDD = False
                End If


                If chkISIBFTRequired.Checked = True Then
                    If chkISIBFTRequired.Enabled = False Then
                        objICTemplateField.IsMustRequiredIBFT = True
                        objICTemplateField.IsRequiredIBFT = True
                    Else
                        objICTemplateField.IsMustRequiredIBFT = False
                        objICTemplateField.IsRequiredIBFT = True
                    End If

                ElseIf chkISIBFTRequired.Checked = False Then
                    objICTemplateField.IsMustRequiredIBFT = False
                    objICTemplateField.IsRequiredIBFT = False
                End If
                If chkISFTRequired.Checked = True Then
                    If chkISFTRequired.Enabled = False Then
                        objICTemplateField.IsMustRequiredFT = True
                        objICTemplateField.IsRequiredFT = True
                    Else
                        objICTemplateField.IsMustRequiredFT = False
                        objICTemplateField.IsRequiredFT = True
                    End If

                ElseIf chkISFTRequired.Checked = False Then
                    objICTemplateField.IsMustRequiredFT = False
                    objICTemplateField.IsRequiredFT = False
                End If
                If chkISCOTCRequired.Checked = True Then
                    If chkISCOTCRequired.Enabled = False Then
                        objICTemplateField.IsMustRequiredCOTC = True
                        objICTemplateField.IsRequiredCOTC = True
                    Else
                        objICTemplateField.IsMustRequiredCOTC = False
                        objICTemplateField.IsRequiredCOTC = True
                    End If

                ElseIf chkISFTRequired.Checked = False Then
                    objICTemplateField.IsMustRequiredCOTC = False
                    objICTemplateField.IsRequiredCOTC = False
                End If

                If chkISBPRequired.Checked = True Then
                    If chkISBPRequired.Enabled = False Then
                        objICTemplateField.IsMustRequiredBillPayment = True
                        objICTemplateField.IsRequiredBillPayment = True
                    Else
                        objICTemplateField.IsMustRequiredBillPayment = False
                        objICTemplateField.IsRequiredBillPayment = True
                    End If

                ElseIf chkISBPRequired.Checked = False Then
                    objICTemplateField.IsMustRequiredBillPayment = False
                    objICTemplateField.IsRequiredBillPayment = False
                End If






                If IsUpDate = False Then
                    ICTemplateFieldListController.AddTemplateField(objICTemplateField, IsUpDate, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                Else
                    ICTemplateFieldListController.AddTemplateField(objICTemplateField, IsUpDate, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                End If
            End If

        Next
        If rbTemplateType.SelectedValue.ToString = "Dual" Then
            For Each gvDeatilFieldListRow In gvDetailFields.Items

                chkSelectDetail = New CheckBox
                chkSelectDetail = DirectCast(gvDeatilFieldListRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelectDetail.Checked = True Then


                    objICTemplateField = New ICTemplateFields
                    objICFieldsList = New ICFieldsList


                    objICTemplateField.es.Connection.CommandTimeout = 3600
                    objICFieldsList.es.Connection.CommandTimeout = 3600


                    objICFieldsList.LoadByPrimaryKey(gvDeatilFieldListRow.GetDataKeyValue("FieldID").ToString)


                    chkISPORequiredDetail = New CheckBox
                    chkISDDRequiredDetail = New CheckBox
                    chkISFTRequiredDetail = New CheckBox
                    chkISIBFTRequiredDetail = New CheckBox
                    chkISCQRequiredDetail = New CheckBox
                    chkISCOTCRequiredDetail = New CheckBox
                    chkISBPRequiredDetail = New CheckBox

                    chkISPORequiredDetail = DirectCast(gvDeatilFieldListRow.Cells(3).FindControl("chkRequiredPO"), CheckBox)
                    chkISDDRequiredDetail = DirectCast(gvDeatilFieldListRow.Cells(4).FindControl("chkRequiredDD"), CheckBox)
                    chkISFTRequiredDetail = DirectCast(gvDeatilFieldListRow.Cells(6).FindControl("chkRequiredFT"), CheckBox)
                    chkISIBFTRequiredDetail = DirectCast(gvDeatilFieldListRow.Cells(5).FindControl("chkRequiredIBFT"), CheckBox)
                    chkISCQRequiredDetail = DirectCast(gvDeatilFieldListRow.Cells(2).FindControl("chkRequiredCheque"), CheckBox)
                    chkISCOTCRequiredDetail = DirectCast(gvDeatilFieldListRow.Cells(7).FindControl("chkRequiredCOTC"), CheckBox)
                    chkISBPRequiredDetail = DirectCast(gvDeatilFieldListRow.Cells(8).FindControl("chkRequiredBillPayment"), CheckBox)


                    objICTemplateField.FieldName = objICFieldsList.FieldName
                    objICTemplateField.FieldType = objICFieldsList.FieldType
                    objICTemplateField.FieldOrder = GetLastFieldorder(TemplateCode, "FixFormat") + 1
                    objICTemplateField.FixLength = objICFieldsList.FieldDBLength
                    objICTemplateField.FieldID = objICFieldsList.FieldID
                    objICTemplateField.TemplateID = TemplateCode
                    objICTemplateField.CreateBy = Me.UserId
                    objICTemplateField.CreateDate = Date.Now

                    If chkSelectDetail.Enabled = False Then
                        objICTemplateField.IsRequired = True
                        objICTemplateField.IsMustRequired = True
                    Else
                        objICTemplateField.IsRequired = True
                        objICTemplateField.IsMustRequired = False
                    End If

                    If chkISCQRequiredDetail.Checked = True Then
                        If chkISCQRequiredDetail.Enabled = False Then
                            objICTemplateField.IsMustRequiredCheque = True
                            objICTemplateField.IsRequiredCheque = True
                        Else
                            objICTemplateField.IsMustRequiredCheque = False
                            objICTemplateField.IsRequiredCheque = True
                        End If

                    ElseIf chkISCQRequiredDetail.Checked = False Then
                        objICTemplateField.IsMustRequiredCheque = False
                        objICTemplateField.IsRequiredCheque = False
                    End If

                    If chkISPORequiredDetail.Checked = True Then
                        If chkISPORequiredDetail.Enabled = False Then
                            objICTemplateField.IsMustRequiredPO = True
                            objICTemplateField.IsRequiredPO = True
                        Else
                            objICTemplateField.IsMustRequiredPO = False
                            objICTemplateField.IsRequiredPO = True
                        End If

                    ElseIf chkISPORequiredDetail.Checked = False Then
                        objICTemplateField.IsMustRequiredPO = False
                        objICTemplateField.IsRequiredPO = False
                    End If


                    If chkISDDRequiredDetail.Checked = True Then
                        If chkISDDRequiredDetail.Enabled = False Then
                            objICTemplateField.IsMustRequiredDD = True
                            objICTemplateField.IsRequiredDD = True
                        Else
                            objICTemplateField.IsMustRequiredDD = False
                            objICTemplateField.IsRequiredDD = True
                        End If

                    ElseIf chkISDDRequiredDetail.Checked = False Then
                        objICTemplateField.IsMustRequiredDD = False
                        objICTemplateField.IsRequiredDD = False
                    End If


                    If chkISIBFTRequiredDetail.Checked = True Then
                        If chkISIBFTRequiredDetail.Enabled = False Then
                            objICTemplateField.IsMustRequiredIBFT = True
                            objICTemplateField.IsRequiredIBFT = True
                        Else
                            objICTemplateField.IsMustRequiredIBFT = False
                            objICTemplateField.IsRequiredIBFT = True
                        End If

                    ElseIf chkISIBFTRequiredDetail.Checked = False Then
                        objICTemplateField.IsMustRequiredIBFT = False
                        objICTemplateField.IsRequiredIBFT = False
                    End If
                    If chkISFTRequiredDetail.Checked = True Then
                        If chkISFTRequiredDetail.Enabled = False Then
                            objICTemplateField.IsMustRequiredFT = True
                            objICTemplateField.IsRequiredFT = True
                        Else
                            objICTemplateField.IsMustRequiredFT = False
                            objICTemplateField.IsRequiredFT = True
                        End If

                    ElseIf chkISFTRequiredDetail.Checked = False Then
                        objICTemplateField.IsMustRequiredFT = False
                        objICTemplateField.IsRequiredFT = False
                    End If


                    If chkISCOTCRequiredDetail.Checked = True Then
                        If chkISCOTCRequiredDetail.Enabled = False Then
                            objICTemplateField.IsMustRequiredCOTC = True
                            objICTemplateField.IsRequiredCOTC = True
                        Else
                            objICTemplateField.IsMustRequiredCOTC = False
                            objICTemplateField.IsRequiredCOTC = True
                        End If

                    ElseIf chkISCOTCRequiredDetail.Checked = False Then
                        objICTemplateField.IsMustRequiredCOTC = False
                        objICTemplateField.IsRequiredCOTC = False
                    End If
                    If chkISBPRequiredDetail.Checked = True Then
                        If chkISBPRequiredDetail.Enabled = False Then
                            objICTemplateField.IsMustRequiredBillPayment = True
                            objICTemplateField.IsRequiredBillPayment = True
                        Else
                            objICTemplateField.IsMustRequiredBillPayment = False
                            objICTemplateField.IsRequiredBillPayment = True
                        End If

                    ElseIf chkISBPRequiredDetail.Checked = False Then
                        objICTemplateField.IsMustRequiredBillPayment = False
                        objICTemplateField.IsRequiredBillPayment = False
                    End If

                    If IsUpDate = False Then
                        ICTemplateFieldListController.AddTemplateField(objICTemplateField, IsUpDate, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    Else
                        ICTemplateFieldListController.AddTemplateField(objICTemplateField, IsUpDate, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    End If
                End If
            Next
        End If
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub
    Protected Sub rbFieldFormate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbFieldFormate.SelectedIndexChanged
        SetFieldFormat()
    End Sub
    Private Sub SetFieldFormat()
        txtdelimiter.Text = ""
        txtHeaderIdentifier.Text = ""
        txtDetailIdentifier.Text = ""
        rbTemplateType.ClearSelection()

        rimDelemiter.Enabled = False


        If rbFieldFormate.SelectedValue.ToString() = "0" Then
            tblTemplateType.Style.Add("display", "none")
            tblDelimitAndDualSetting.Style.Add("display", "none")
            rfvTemplateType.Enabled = False
            rimHeaderAndDetail.Enabled = False
            LoadFieldsList(Me.gvFieldsList.CurrentPageIndex + 1, Me.gvFieldsList.PageSize, True)
            tblFieldList.Style.Remove("display")
            'pnlFieldsList.Visible = True
            btnSave.Visible = True
            'pnlDetail.Visible = False
            tblDetail.Style.Add("display", "none")
            lblDetailFields.Visible = False
        End If
        If rbFieldFormate.SelectedValue.ToString() = "1" Then
            tblTemplateType.Style.Remove("display")
            rfvTemplateType.Enabled = True
            tblDelimitAndDualSetting.Style.Add("display", "none")
            rimHeaderAndDetail.Enabled = False
            tblFieldList.Style.Add("display", "none")
            lblTemplateFieldHeader.Visible = False
            btnSave.Visible = False
            tblDetail.Style.Add("display", "none")
            lblDetailFields.Visible = False
        End If
    End Sub
    Protected Sub RbFormat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RbFormat.SelectedIndexChanged
        SetFileFormat()
    End Sub
    Private Sub SetFileFormat()
        Try
            tblXLS.Style.Add("display", "none")
            tblTXT.Style.Add("display", "none")
            lblSheetOrFileFormat.Style.Remove("display")
            rbFieldFormate.ClearSelection()
            rbTemplateType.ClearSelection()
            pnlDelimeter.Style.Add("display", "none")
            txtSheetName.Text = ""
            txtdelimiter.Text = ""
            txtHeaderIdentifier.Text = ""
            txtDetailIdentifier.Text = ""
            rimSheet.Enabled = False
            rfvFieldFormat.Enabled = False
            If RbFormat.Text.ToString() = ".xls" Then
                lblSheetOrFileFormat.Text = "Sheet Name"
                lblReqSheetNameOrFixLength.Visible = True
                tblXLS.Style.Remove("display")
                rimSheet.Enabled = True
                rimSheet.GetSettingByBehaviorID("RagExpBehavior3").Validation.IsRequired = True
                tblTemplateType.Style.Add("display", "none")
                tblDelimitAndDualSetting.Style.Add("display", "none")
                rfvTemplateType.Enabled = False
                rimHeaderAndDetail.Enabled = False
                rbTemplateType.ClearSelection()
                txtdelimiter.Text = ""
                txtDetailIdentifier.Text = ""
                txtHeaderIdentifier.Text = ""
                rimDelemiter.Enabled = False
                LoadFieldsList(Me.gvFieldsList.CurrentPageIndex + 1, Me.gvFieldsList.PageSize, True)
                tblFieldList.Style.Remove("display")
                tblDetail.Style.Add("display", "none")
                lblDetailFields.Visible = False
            End If
            If RbFormat.Text.ToString() = ".xlsx" Then
                lblSheetOrFileFormat.Text = "Sheet Name"
                lblReqSheetNameOrFixLength.Visible = True
                tblXLS.Style.Remove("display")
                rimSheet.Enabled = True
                rimSheet.GetSettingByBehaviorID("RagExpBehavior3").Validation.IsRequired = True
                tblTemplateType.Style.Add("display", "none")
                tblDelimitAndDualSetting.Style.Add("display", "none")
                rbTemplateType.ClearSelection()
                txtdelimiter.Text = ""
                txtDetailIdentifier.Text = ""
                txtHeaderIdentifier.Text = ""
                rimDelemiter.Enabled = False
                rfvTemplateType.Enabled = False
                rimHeaderAndDetail.Enabled = False
                LoadFieldsList(Me.gvFieldsList.CurrentPageIndex + 1, Me.gvFieldsList.PageSize, True)
                tblFieldList.Style.Remove("display")
                tblDetail.Style.Add("display", "none")
                'pnlFieldsList.Visible = True
                'pnlDetail.Visible = False
                lblDetailFields.Visible = False

            End If
            If RbFormat.Text.ToString() = ".txt" Then
                lblSheetOrFileFormat.Text = "Field Format"
                tblTXT.Style.Remove("display")
                lblReqSheetNameOrFixLength.Visible = True
                rfvFieldFormat.Enabled = True
                rfvTemplateType.Enabled = False
                rimHeaderAndDetail.Enabled = False
                rimDelemiter.Enabled = False
                lblTemplateFieldHeader.Visible = False
                'pnlFieldsList.Visible = False
                'pnlDetail.Visible = False
                tblFieldList.Style.Add("display", "none")
                tblDetail.Style.Add("display", "none")
                btnSave.Visible = False
            End If
            If RbFormat.Text.ToString() = ".csv" Then
              
                lblSheetOrFileFormat.Text = "Sheet Name"
                lblReqSheetNameOrFixLength.Visible = True
                tblXLS.Style.Remove("display")
                rimSheet.Enabled = True
                rimSheet.GetSettingByBehaviorID("RagExpBehavior3").Validation.IsRequired = True
                tblTemplateType.Style.Add("display", "none")
                tblDelimitAndDualSetting.Style.Add("display", "none")
                rbTemplateType.ClearSelection()
                txtdelimiter.Text = ""
                txtDetailIdentifier.Text = ""
                txtHeaderIdentifier.Text = ""
                rimDelemiter.Enabled = False
                rfvTemplateType.Enabled = False
                rimHeaderAndDetail.Enabled = False
                LoadFieldsList(Me.gvFieldsList.CurrentPageIndex + 1, Me.gvFieldsList.PageSize, True)
                'pnlDetail.Visible = False
                tblDetail.Style.Add("display", "none")
                btnSave.Visible = True
                'pnlFieldsList.Visible = True
                tblFieldList.Style.Remove("display")
                lblDetailFields.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub rbTemplateType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbTemplateType.SelectedIndexChanged
        Try
           
            SetDelimitAndHeaderAndDetail()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetDelimitAndHeaderAndDetail()
        If rbTemplateType.SelectedValue = "Single" Then
            tblDelimitAndDualSetting.Style.Remove("display")
            pnlDelimeter.Style.Remove("display")
            pnlHeaderIdentifier.Style.Add("display", "none")
            pnlDetailIdentifier.Style.Add("display", "none")

            rimDelemiter.Enabled = True
            rimDelemiter.GetSettingByBehaviorID("RagExpBehavior4").Validation.IsRequired = True
            rimHeaderAndDetail.Enabled = False
            LoadFieldsList(Me.gvFieldsList.CurrentPageIndex + 1, Me.gvFieldsList.PageSize, True)
            'pnlFieldsList.Visible = True
            'pnlDetail.Style.Add("display", "none")
            tblDetail.Style.Add("display", "none")

            tblFieldList.Style.Remove("display")
            btnSave.Visible = True
        ElseIf rbTemplateType.SelectedValue = "Dual" Then
            tblDelimitAndDualSetting.Style.Remove("display")
            pnlDelimeter.Style.Remove("display")
            pnlHeaderIdentifier.Style.Remove("display")
            pnlDetailIdentifier.Style.Remove("display")
            rimHeaderAndDetail.Enabled = True
            rimDelemiter.Enabled = True
            rimDelemiter.GetSettingByBehaviorID("RagExpBehavior4").Validation.IsRequired = True
            rimHeaderAndDetail.GetSettingByBehaviorID("regHeader").Validation.IsRequired = True
            rimHeaderAndDetail.GetSettingByBehaviorID("reDetail").Validation.IsRequired = True
            LoadFieldsList(Me.gvFieldsList.CurrentPageIndex + 1, Me.gvFieldsList.PageSize, True)
            'pnlFieldsList.Visible = True
            tblFieldList.Style.Remove("display")
            LoadDetailFieldListForDualTemplateType(Me.gvDetailFields.CurrentPageIndex + 1, Me.gvDetailFields.PageSize, True)
            'pnlDetail.Visible = True
            tblDetail.Style.Remove("display")
            btnSave.Visible = True
        End If
    End Sub
    Private Function CheckDuplicateTemplateNameAndCode(ByVal objICTemplate As ICTemplate, ByVal IsUpdate As Boolean) As Boolean

        Dim objICTemplateFieldColl As New ICTemplateCollection
        Dim objICTemplateForUpdate As New ICTemplate
        Dim Result As Boolean = False


        objICTemplateFieldColl.es.Connection.CommandTimeout = 3600
        objICTemplateForUpdate.es.Connection.CommandTimeout = 3600



        If IsUpdate = False Then
            objICTemplateFieldColl.Query.Where(objICTemplateFieldColl.Query.TemplateName = objICTemplate.TemplateName Or objICTemplateFieldColl.Query.FileUploadTemplateCode = objICTemplate.FileUploadTemplateCode)
            objICTemplateFieldColl.Query.Load()
            If objICTemplateFieldColl.Query.Load Then
                Result = True
            End If
        ElseIf IsUpdate = True Then
            objICTemplateForUpdate.LoadByPrimaryKey(FileUploadTemplateCode)
            If Not objICTemplateForUpdate.TemplateName = objICTemplate.TemplateName Then
                objICTemplateFieldColl.Query.Where(objICTemplateFieldColl.Query.TemplateName = objICTemplate.TemplateName)
                objICTemplateFieldColl.Query.Load()
                If objICTemplateFieldColl.Query.Load Then
                    Result = True
                End If
            End If
            If Not objICTemplateForUpdate.FileUploadTemplateCode = objICTemplate.FileUploadTemplateCode Then
                objICTemplateFieldColl.Query.Where(objICTemplateFieldColl.Query.FileUploadTemplateCode = objICTemplate.FileUploadTemplateCode)
                objICTemplateFieldColl.Query.Load()
                If objICTemplateFieldColl.Query.Load Then
                    Result = True
                End If
            End If
        End If



        Return Result
    End Function


    Protected Sub gvAssignedDetailFields_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvAssignedDetailFields.ItemCommand
        Try
            Dim i As Int16 = 1
            If e.CommandName = "del" Then

                '    Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                Dim cat As New ICTemplateFields
                cat.es.Connection.CommandTimeout = 3600
                Dim order As Integer = 0
                'Dim acq As New FRCAcqAgentTemplateFieldCollection
                'acq.Query.Where(acq.Query.FieldID.Equals(Convert.ToInt64(e.CommandArgument.ToString())))
                'If acq.Query.Load Then
                '    UIUtilities.ShowDialog(Me, "Template Filed", "Cannot deleted : Template Filed have related data ", Intelligenes.FRC.Dialogmessagetype.Failure)
                'Else
                If cat.LoadByPrimaryKey(Convert.ToInt64(e.CommandArgument.ToString()), FileUploadTemplateCode) Then
                    order = cat.FieldOrder
                    cat.MarkAsDeleted()
                    cat.Save()
                    Dim objICTemplateFieldColl As New ICTemplateFieldsCollection
                    objICTemplateFieldColl.es.Connection.CommandTimeout = 3600

                    objICTemplateFieldColl.Query.Where(objICTemplateFieldColl.Query.TemplateID = FileUploadTemplateCode And objICTemplateFieldColl.Query.FieldOrder > order And objICTemplateFieldColl.Query.FieldType = "FixFormat")
                    objICTemplateFieldColl.Query.OrderBy(objICTemplateFieldColl.Query.FieldOrder.Ascending)
                    objICTemplateFieldColl.Query.Load()
                    For Each cat In objICTemplateFieldColl
                        MoveUp(Convert.ToInt64(cat.FieldID))
                    Next
                    ' fieldcoll.Save()
                    UIUtilities.ShowDialog(Me, "File Upload Template", "File Upload template field deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL("SaveFileUploadTemplate", "&mid=" & Me.ModuleId & "&id=" & FileUploadTemplateCode))
                    'If rbTemplateType.SelectedValue = "Dual" Then
                    '    LoadAssignedFieldsListForTemplate(Me.gvAssignedTemplateFieldForSingleTemplate.CurrentPageIndex + 1, Me.gvAssignedTemplateFieldForSingleTemplate.PageSize, True, "Detail", "FixFormat")
                    '    LoadDetailFieldListForDualTemplateType(Me.gvDetailFields.CurrentPageIndex + 1, Me.gvDetailFields.PageSize, True)
                    '    CheckSelectCheckBoxOnUpDateTemplate("Detail")
                    'Else
                    '    LoadAssignedFieldsListForTemplate(Me.gvAssignedTemplateFieldForSingleTemplate.CurrentPageIndex + 1, Me.gvAssignedTemplateFieldForSingleTemplate.PageSize, True, "TemplateHeader", "NonFlexi")
                    '    LoadFieldsList(Me.gvFieldsList.CurrentPageIndex + 1, Me.gvFieldsList.PageSize, True)
                    '    CheckSelectCheckBoxOnUpDateTemplate("Header")
                    'End If
                End If
                ' End If
           


            End If
            If e.CommandName = "Up" Then
                MoveUp(e.CommandArgument.ToString)
            End If
            If e.CommandName = "Down" Then
                MoveDown(FileUploadTemplateCode, e.CommandArgument.ToString)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub MoveUp(ByVal FieldID As String)

        ' CheckOrders(pcatid)

        Dim cat As New ICTemplateFields
        cat.es.Connection.CommandTimeout = 3600
        cat.LoadByPrimaryKey(FieldID, FileUploadTemplateCode)

        If Not cat.FieldOrder = 1 Then

            'Dim cat2 As New ICTemplateFields
            'cat2.es.Connection.CommandTimeout = 3600
            'cat2.Query.Where(cat2.Query.FieldOrder = cat.FieldOrder - 1 And cat2.Query.TemplateID = cat.TemplateID)

            'If cat2.Query.Load Then
            '    cat2.FieldOrder += 1
            '    cat2.Save()

            'End If

            cat.FieldOrder = cat.FieldOrder - 1

            cat.Save()

            'If rbTemplateType.SelectedValue = "Dual" Then
            '    LoadAssignedFieldsListForTemplate(Me.gvAssignedTemplateFieldForSingleTemplate.CurrentPageIndex + 1, Me.gvAssignedTemplateFieldForSingleTemplate.PageSize, True, "Detail", "FixFormat")
            '    LoadDetailFieldListForDualTemplateType(Me.gvDetailFields.CurrentPageIndex + 1, Me.gvDetailFields.PageSize, True)
            '    CheckSelectCheckBoxOnUpDateTemplate("Detail")
            'Else
            '    LoadAssignedFieldsListForTemplate(Me.gvAssignedTemplateFieldForSingleTemplate.CurrentPageIndex + 1, Me.gvAssignedTemplateFieldForSingleTemplate.PageSize, True, "TemplateHeader", "NonFlexi")
            '    LoadFieldsList(Me.gvFieldsList.CurrentPageIndex + 1, Me.gvFieldsList.PageSize, True)
            '    CheckSelectCheckBoxOnUpDateTemplate("Header")
            'End If
            '   Me.FillTreeview(Me.TreeView1)

        End If


    End Sub

    Private Sub MoveDown(ByVal templateid As String, ByVal FieldID As String)


        Dim fieldcoll As New ICTemplateFieldsCollection
        fieldcoll.es.Connection.CommandTimeout = 3600
        fieldcoll.Query.Where(fieldcoll.Query.TemplateID = templateid)

        fieldcoll.Query.Load()

        Dim cat As New ICTemplateFields
        cat.es.Connection.CommandTimeout = 3600
        cat.LoadByPrimaryKey(FieldID, templateid)




        If Not cat.FieldOrder = fieldcoll.Count Then

            Dim cat2 As New ICTemplateFields
            cat2.es.Connection.CommandTimeout = 3600

            cat2.Query.Where(cat2.Query.FieldOrder = cat.FieldOrder + 1 And cat.Query.TemplateID = templateid)


            If cat2.Query.Load Then


                cat2.FieldOrder -= 1


                cat2.Save()

            End If

            cat.FieldOrder = cat.FieldOrder + 1


            cat.Save()

            If rbTemplateType.SelectedValue = "Dual" Then
                LoadAssignedFieldsListForTemplate(Me.gvAssignedTemplateFieldForSingleTemplate.CurrentPageIndex + 1, Me.gvAssignedTemplateFieldForSingleTemplate.PageSize, True, "Detail", "FixFormat")
                LoadDetailFieldListForDualTemplateType(Me.gvDetailFields.CurrentPageIndex + 1, Me.gvDetailFields.PageSize, True)
                CheckSelectCheckBoxOnUpDateTemplate("Detail")
            Else
                LoadAssignedFieldsListForTemplate(Me.gvAssignedTemplateFieldForSingleTemplate.CurrentPageIndex + 1, Me.gvAssignedTemplateFieldForSingleTemplate.PageSize, True, "TemplateHeader", "NonFlexi")
                LoadFieldsList(Me.gvFieldsList.CurrentPageIndex + 1, Me.gvFieldsList.PageSize, True)
                CheckSelectCheckBoxOnUpDateTemplate("Header")
            End If

        End If

    End Sub

    Protected Sub gvAssignedTemplateFieldForSingleTemplate_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvAssignedTemplateFieldForSingleTemplate.ItemCommand
        Try
            Dim i As Int16 = 1
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    '    Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                    Dim cat As New ICTemplateFields
                    cat.es.Connection.CommandTimeout = 3600
                    Dim order As Integer = 0
                    'Dim acq As New FRCAcqAgentTemplateFieldCollection
                    'acq.Query.Where(acq.Query.FieldID.Equals(Convert.ToInt64(e.CommandArgument.ToString())))
                    'If acq.Query.Load Then
                    '    UIUtilities.ShowDialog(Me, "Template Filed", "Cannot deleted : Template Filed have related data ", Intelligenes.FRC.Dialogmessagetype.Failure)
                    'Else
                    If cat.LoadByPrimaryKey(Convert.ToInt64(e.CommandArgument.ToString()), FileUploadTemplateCode) Then
                        order = cat.FieldOrder
                        cat.MarkAsDeleted()
                        cat.Save()
                        Dim objICTemplateFieldColl As New ICTemplateFieldsCollection
                        objICTemplateFieldColl.es.Connection.CommandTimeout = 3600

                        objICTemplateFieldColl.Query.Where(objICTemplateFieldColl.Query.TemplateID = FileUploadTemplateCode And objICTemplateFieldColl.Query.FieldOrder > order And objICTemplateFieldColl.Query.FieldType = "NonFlexi")
                        objICTemplateFieldColl.Query.OrderBy(objICTemplateFieldColl.Query.FieldOrder.Ascending)
                        objICTemplateFieldColl.Query.Load()
                        For Each cat In objICTemplateFieldColl
                            MoveUp(Convert.ToInt64(cat.FieldID))
                        Next
                        UIUtilities.ShowDialog(Me, "File Upload Template", "File Upload template field deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL("SaveFileUploadTemplate", "&mid=" & Me.ModuleId & "&id=" & FileUploadTemplateCode))
                        ' fieldcoll.Save()

                        'If rbTemplateType.SelectedValue = "Dual" Then
                        '    LoadAssignedFieldsListForTemplate(Me.gvAssignedTemplateFieldForSingleTemplate.CurrentPageIndex + 1, Me.gvAssignedTemplateFieldForSingleTemplate.PageSize, True, "Detail", "FixFormat")
                        '    LoadDetailFieldListForDualTemplateType(Me.gvDetailFields.CurrentPageIndex + 1, Me.gvDetailFields.PageSize, True)
                        '    CheckSelectCheckBoxOnUpDateTemplate("Detail")
                        'Else
                        '    LoadAssignedFieldsListForTemplate(Me.gvAssignedTemplateFieldForSingleTemplate.CurrentPageIndex + 1, Me.gvAssignedTemplateFieldForSingleTemplate.PageSize, True, "TemplateHeader", "NonFlexi")
                        '    LoadFieldsList(Me.gvFieldsList.CurrentPageIndex + 1, Me.gvFieldsList.PageSize, True)
                        '    CheckSelectCheckBoxOnUpDateTemplate("Header")
                        'End If
                    End If
                    ' End If


                End If
            End If
            If e.CommandName = "Up" Then
                MoveUp(e.CommandArgument.ToString)
            End If
            If e.CommandName = "Down" Then
                MoveDown(FileUploadTemplateCode, e.CommandArgument.ToString)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub gvFieldsList_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvFieldsList.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvFieldsList.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub gvDetailFields_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvDetailFields.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvDetailFields.MasterTableView.ClientID & "','0');"
        End If
    End Sub
    Private Function CheckTextBoxesDuplicatValuesForDetailSection() As Boolean
        Dim Result As Boolean = False
        Dim txtBoxToValiDate As TextBox
        Dim txtBoxToCompare As TextBox
        Dim i As Integer = 0
        Dim MatchCount As Integer = 0

        For Each gvAssigendTempFieldListRow As GridDataItem In gvAssignedTemplateFieldForSingleTemplate.Items
            txtBoxToValiDate = New TextBox
            txtBoxToValiDate = DirectCast(gvAssigendTempFieldListRow.Cells(7).FindControl("txtFieldOrder"), TextBox)
            For Each gvAssignedFieldsToCompareRow As GridDataItem In gvAssignedTemplateFieldForSingleTemplate.Items
                txtBoxToCompare = New TextBox
                txtBoxToCompare = DirectCast(gvAssignedFieldsToCompareRow.Cells(7).FindControl("txtFieldOrder"), TextBox)
                If txtBoxToValiDate.Text = txtBoxToCompare.Text Then
                    MatchCount = MatchCount + 1
                End If
            Next
            If MatchCount > 1 Then
                Result = True
                Exit For
            End If
            MatchCount = 0
        Next

        Return Result
    End Function
    Private Function CheckTextBoxesDuplicatValuesForDetailSectionSecond() As Boolean
        Dim Result As Boolean = False
        Dim txtBoxToValiDate As TextBox
        Dim txtBoxToCompare As TextBox
        Dim i As Integer = 0
        Dim MatchCount As Integer = 0

        For Each gvAssigendDetailFieldRow As GridDataItem In gvAssignedDetailFields.Items
            txtBoxToValiDate = New TextBox
            txtBoxToValiDate = DirectCast(gvAssigendDetailFieldRow.Cells(7).FindControl("txtFieldOrderDetail"), TextBox)
            For Each gvAssignedFieldsToCompareRow As GridDataItem In gvAssignedDetailFields.Items
                txtBoxToCompare = New TextBox
                txtBoxToCompare = DirectCast(gvAssignedFieldsToCompareRow.Cells(7).FindControl("txtFieldOrderDetail"), TextBox)
                If txtBoxToValiDate.Text = txtBoxToCompare.Text Then
                    MatchCount = MatchCount + 1
                End If
            Next
            If MatchCount > 1 Then
                Result = True
                Exit For
            End If
            MatchCount = 0
        Next

        Return Result
    End Function
    Protected Sub btnSetOrder_Click(sender As Object, e As System.EventArgs) Handles btnSetOrder.Click
        If Page.IsValid Then
            Try

                Dim txtBoxToValiDate As TextBox
                Dim APNFUTemplatedID As Integer = 0
                Dim objICTemplateFieldsFrom As ICTemplateFields
                Dim RecordUpdatedCount As Integer = 0
                Dim RecordNotUpdatedCount As Integer = 0
                Dim objICTemplateField As New ICTemplateFields
                Dim objICTemplate As New ICTemplate
                objICTemplate.LoadByPrimaryKey(FileUploadTemplateCode)
                Dim StrFeildName As String = ""
                objICTemplateField.es.Connection.CommandTimeout = 3600
                If CheckTextBoxesDuplicatValuesForDetailSection() = True Then
                    UIUtilities.ShowDialog(Me, "Error", "Duplicate field order(s) are not allowed.", ICBO.IC.Dialogmessagetype.Warning)
                    Exit Sub
                End If
                For Each gvAssignedFiledsRow As GridDataItem In gvAssignedTemplateFieldForSingleTemplate.Items
                    txtBoxToValiDate = New TextBox
                    objICTemplateFieldsFrom = New ICTemplateFields
                    objICTemplateFieldsFrom.es.Connection.CommandTimeout = 3600
                    txtBoxToValiDate = DirectCast(gvAssignedFiledsRow.Cells(7).FindControl("txtFieldOrder"), TextBox)
                    APNFUTemplatedID = gvAssignedFiledsRow.GetDataKeyValue("FieldID")
                    If objICTemplateFieldsFrom.LoadByPrimaryKey(APNFUTemplatedID, FileUploadTemplateCode) Then
                        If Not objICTemplateFieldsFrom.FieldOrder = txtBoxToValiDate.Text.ToString Then
                            StrFeildName = ""
                            StrFeildName = gvAssignedFiledsRow.Item("FieldName").Text.ToString
                            ICTemplateFieldController.UpdateFieldOrderByFieldID(objICTemplateFieldsFrom.FieldOrder, txtBoxToValiDate.Text.ToString, FileUploadTemplateCode, APNFUTemplatedID, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            RecordUpdatedCount = RecordUpdatedCount + 1
                        Else
                            RecordNotUpdatedCount = RecordNotUpdatedCount + 1
                        End If
                    End If
                Next


                UpdateAPNFileuploadTemplateFields(objICTemplate, Me.UserId.ToString, Me.UserInfo.Username.ToString)


                If Not RecordUpdatedCount = 0 And Not RecordNotUpdatedCount = 0 Then
                    UIUtilities.ShowDialog(Me, "File Upload Template", "[ " & RecordUpdatedCount.ToString & " ] Field(s) order updated successfully.<br /> [ " & RecordNotUpdatedCount.ToString & " ] Field(s) order not updated due to following reasons:<br /> 1. Field Order is same as previous.", ICBO.IC.Dialogmessagetype.Success, NavigateURL("SaveFileUploadTemplate", "&mid=" & Me.ModuleId & "&id=" & FileUploadTemplateCode))

                    Exit Sub
                ElseIf Not RecordUpdatedCount = 0 And RecordNotUpdatedCount = 0 Then
                    UIUtilities.ShowDialog(Me, "File Upload Template", RecordUpdatedCount.ToString & " Field(s) order updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL("SaveFileUploadTemplate", "&mid=" & Me.ModuleId & "&id=" & FileUploadTemplateCode))

                    Exit Sub

                ElseIf Not RecordNotUpdatedCount = 0 And RecordUpdatedCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Account Payment Nature File Upload Template", "[ " & RecordNotUpdatedCount.ToString & " ] Field(s) order not updated due to following reasons: <br /> 1. Field Order is same as previous.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL("SaveFileUploadTemplate", "&mid=" & Me.ModuleId & "&id=" & FileUploadTemplateCode))

                    Exit Sub

                    'ElseIf RecordUpdatedCount = 0 And RecordNotUpdatedCount = 0 Then
                    '    LoadAssignedFieldsForAPNFUTemplate(objICAPNFUTemplate)
                    '    UIUtilities.ShowDialog(Me, "Account Payment Nature File Upload Template", "[ " & RecordNotUpdatedCount.ToString & " ] Field(s) order updated due to following reasons: <br /> 1. Field Order is same as previous.", ICBO.IC.Dialogmessagetype.Failure)
                    '    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnDetailSetOrder_Click(sender As Object, e As System.EventArgs) Handles btnDetailSetOrder.Click
        If Page.IsValid Then
            Try
                Dim txtBoxToValiDate As TextBox
                Dim APNFUTemplatedID As Integer = 0
                Dim objICTemplateFieldsFrom As ICTemplateFields
                Dim RecordUpdatedCount As Integer = 0
                Dim RecordNotUpdatedCount As Integer = 0
                Dim objICTemplateField As New ICTemplateFields
                Dim objICTemplate As New ICTemplate
                objICTemplate.LoadByPrimaryKey(FileUploadTemplateCode)
                Dim StrFeildName As String = ""
                objICTemplateField.es.Connection.CommandTimeout = 3600
                If CheckTextBoxesDuplicatValuesForDetailSectionSecond() = True Then
                    UIUtilities.ShowDialog(Me, "Error", "Duplicate field order(s) are not allowed.", ICBO.IC.Dialogmessagetype.Warning)
                    Exit Sub
                End If
                For Each gvAssignedFiledsRow As GridDataItem In gvAssignedDetailFields.Items
                    txtBoxToValiDate = New TextBox
                    objICTemplateFieldsFrom = New ICTemplateFields
                    objICTemplateFieldsFrom.es.Connection.CommandTimeout = 3600
                    txtBoxToValiDate = DirectCast(gvAssignedFiledsRow.Cells(7).FindControl("txtFieldOrderDetail"), TextBox)
                    APNFUTemplatedID = gvAssignedFiledsRow.GetDataKeyValue("FieldID")
                    If objICTemplateFieldsFrom.LoadByPrimaryKey(APNFUTemplatedID, FileUploadTemplateCode) Then
                        If Not objICTemplateFieldsFrom.FieldOrder = txtBoxToValiDate.Text.ToString Then
                            StrFeildName = ""
                            StrFeildName = gvAssignedFiledsRow.Item("FieldName").Text.ToString
                            ICTemplateFieldController.UpdateFieldOrderByFieldID(objICTemplateFieldsFrom.FieldOrder, txtBoxToValiDate.Text.ToString, FileUploadTemplateCode, APNFUTemplatedID, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            RecordUpdatedCount = RecordUpdatedCount + 1
                        Else
                            RecordNotUpdatedCount = RecordNotUpdatedCount + 1
                        End If
                    End If
                Next
                UpdateAPNFileuploadTemplateFields(objICTemplate, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                If Not RecordUpdatedCount = 0 And Not RecordNotUpdatedCount = 0 Then
                    UIUtilities.ShowDialog(Me, "File Upload Template", "[ " & RecordUpdatedCount.ToString & " ] Field(s) order updated successfully.<br /> [ " & RecordNotUpdatedCount.ToString & " ] Field(s) order not updated due to following reasons:<br /> 1. Field Order is same as previous.", ICBO.IC.Dialogmessagetype.Success, NavigateURL("SaveFileUploadTemplate", "&mid=" & Me.ModuleId & "&id=" & FileUploadTemplateCode))

                    Exit Sub
                ElseIf Not RecordUpdatedCount = 0 And RecordNotUpdatedCount = 0 Then
                    UIUtilities.ShowDialog(Me, "File Upload Template", RecordUpdatedCount.ToString & " Field(s) order updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL("SaveFileUploadTemplate", "&mid=" & Me.ModuleId & "&id=" & FileUploadTemplateCode))

                    Exit Sub

                ElseIf Not RecordNotUpdatedCount = 0 And RecordUpdatedCount = 0 Then
                    UIUtilities.ShowDialog(Me, "File Upload Template", "[ " & RecordNotUpdatedCount.ToString & " ] Field(s) order not updated due to following reasons: <br /> 1. Field Order is same as previous.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL("SaveFileUploadTemplate", "&mid=" & Me.ModuleId & "&id=" & FileUploadTemplateCode))

                    Exit Sub

                    'ElseIf RecordUpdatedCount = 0 And RecordNotUpdatedCount = 0 Then
                    '    LoadAssignedFieldsForAPNFUTemplate(objICAPNFUTemplate)
                    '    UIUtilities.ShowDialog(Me, "Account Payment Nature File Upload Template", "[ " & RecordNotUpdatedCount.ToString & " ] Field(s) order updated due to following reasons: <br /> 1. Field Order is same as previous.", ICBO.IC.Dialogmessagetype.Failure)
                    '    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Sub UpdateAPNFileuploadTemplateFields(ByVal Template As ICTemplate, ByVal UsersID As String, ByVal UsersName As String)
        For Each objICAPNFileTemplate As ICAccountPayNatureFileUploadTemplate In Template.ICAccountPayNatureFileUploadTemplateCollectionByTemplateID
            DeleteAPNFileUploadTemplateFields(objICAPNFileTemplate, UsersID, UsersName)
            AddAccountPaymentNatureFileUploadTemplateFields(objICAPNFileTemplate, False, objICAPNFileTemplate.UpToICAccountsPaymentNatureByAccountNumber.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode)
        Next
    End Sub
    Public Shared Sub DeleteAPNFileUploadTemplateFields(ByVal objICAPNFTemplate As ICAccountPayNatureFileUploadTemplate, ByVal UsersID As String, ByVal UsersName As String)
        Dim objICAPNFUTemplateFieldsColl As New ICAccountPaymentNatureFileUploadTemplatefieldsCollection
        Dim objICAPNFUTemplateField As ICAccountPaymentNatureFileUploadTemplatefields
        Dim ActionString As String = Nothing
        Dim FieldName As String = Nothing
        Dim FieldName2 As String = Nothing
        ActionString += "Account payment nature file upload template with account number [ " & objICAPNFTemplate.AccountNumber & " ], payment nature [ " & objICAPNFTemplate.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] "
        ActionString += ", file upload template [ " & objICAPNFTemplate.UpToICTemplateByTemplateID.TemplateName & " ] [ " & objICAPNFTemplate.UpToICTemplateByTemplateID.FileUploadTemplateCode & " ] fields [ "

        objICAPNFUTemplateFieldsColl.es.Connection.CommandTimeout = 3600


        objICAPNFUTemplateFieldsColl.Query.Where(objICAPNFUTemplateFieldsColl.Query.AccountNumber = objICAPNFTemplate.AccountNumber And objICAPNFUTemplateFieldsColl.Query.BranchCode = objICAPNFTemplate.BranchCode And objICAPNFUTemplateFieldsColl.Query.Currency = objICAPNFTemplate.Currency And objICAPNFUTemplateFieldsColl.Query.TemplateID = objICAPNFTemplate.TemplateID And objICAPNFUTemplateFieldsColl.Query.PaymentNatureCode = objICAPNFTemplate.PaymentNatureCode)

        If objICAPNFUTemplateFieldsColl.Query.Load() Then
            For Each objICAPNFUTemplateForDelete As ICAccountPaymentNatureFileUploadTemplatefields In objICAPNFUTemplateFieldsColl
                objICAPNFUTemplateField = New ICAccountPaymentNatureFileUploadTemplatefields
                objICAPNFUTemplateField.es.Connection.CommandTimeout = 3600
                objICAPNFUTemplateField.LoadByPrimaryKey(objICAPNFUTemplateForDelete.APNFUTemplateID)
                FieldName += objICAPNFUTemplateField.FieldName & " ; "
                objICAPNFUTemplateField.MarkAsDeleted()
                objICAPNFUTemplateField.Save()
            Next
            FieldName2 = FieldName.Remove(FieldName.Length - 2)
            ActionString += FieldName2
            ICUtilities.AddAuditTrail(ActionString & " ] deleted on Template updating.", "Account Payment Nature File Upload Template Field", objICAPNFTemplate.TemplateID.ToString + "" + objICAPNFTemplate.AccountNumber.ToString + "" + objICAPNFTemplate.BranchCode.ToString + "" + objICAPNFTemplate.Currency.ToString + "" + objICAPNFTemplate.PaymentNatureCode.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
        End If
    End Sub
    Private Sub AddAccountPaymentNatureFileUploadTemplateFields(ByVal objICAPNFUTemplate As ICAccountPayNatureFileUploadTemplate, ByVal IsUpdate As Boolean, ByVal objCompany As ICCompany)
        Dim objICTemplateFieldsColl As ICTemplateFieldsCollection
        Dim objICAPNFUTemplateFields As ICAccountPaymentNatureFileUploadTemplatefields
        Dim ActionString As String = Nothing
        Dim FieldName As String = Nothing
        Dim FieldName2 As String = Nothing
        ActionString += "Account payment nature file upload template with account number [ " & objICAPNFUTemplate.AccountNumber & " ], payment nature [ " & objICAPNFUTemplate.UpToICAccountsPaymentNatureByAccountNumber.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " ] "
        ActionString += ", file upload template [ " & objICAPNFUTemplate.UpToICTemplateByTemplateID.TemplateName & " ] [ " & objICAPNFUTemplate.UpToICTemplateByTemplateID.FileUploadTemplateCode & " ] added on Template updating with fields [ "
        Dim objICTemplate As New ICTemplate

        objICTemplate.es.Connection.CommandTimeout = 3600

        objICTemplate.LoadByPrimaryKey(objICAPNFUTemplate.TemplateID.ToString)

        If Not objICTemplate.TemplateType = "Dual" Then
            objICTemplateFieldsColl = New ICTemplateFieldsCollection
            objICTemplateFieldsColl = ICTemplateFieldController.GetTemplateFieldsByTemplateIDAndFieldType(objICTemplate.TemplateID.ToString, "NonFlexi")
            If objICTemplateFieldsColl.Count > 0 Then
                For Each objICTemplateField As ICTemplateFields In objICTemplateFieldsColl
                    objICAPNFUTemplateFields = New ICAccountPaymentNatureFileUploadTemplatefields
                    objICAPNFUTemplateFields.es.Connection.CommandTimeout = 3600
                    objICAPNFUTemplateFields.AccountNumber = objICAPNFUTemplate.AccountNumber
                    objICAPNFUTemplateFields.BranchCode = objICAPNFUTemplate.BranchCode
                    objICAPNFUTemplateFields.Currency = objICAPNFUTemplate.Currency
                    objICAPNFUTemplateFields.PaymentNatureCode = objICAPNFUTemplate.PaymentNatureCode
                    objICAPNFUTemplateFields.TemplateID = objICAPNFUTemplate.TemplateID
                    objICAPNFUTemplateFields.FieldID = objICTemplateField.FieldID
                    objICAPNFUTemplateFields.IsRequired = objICTemplateField.IsRequired
                    objICAPNFUTemplateFields.MustRequired = objICTemplateField.IsMustRequired
                    objICAPNFUTemplateFields.FieldOrder = objICTemplateField.FieldOrder
                    objICAPNFUTemplateFields.FieldType = objICTemplateField.FieldType
                    objICAPNFUTemplateFields.FixLength = CInt(objICTemplateField.FixLength)
                    objICAPNFUTemplateFields.FieldName = objICTemplateField.FieldName
                    If objCompany.IsAllowOpenPayment = True And objCompany.IsBeneRequired = True And objICTemplateField.FieldName = "BeneficiaryCode" Then
                        objICAPNFUTemplateFields.IsRequired = False
                        objICAPNFUTemplateFields.MustRequired = False
                        objICAPNFUTemplateFields.IsRequiredFT = False
                        objICAPNFUTemplateFields.IsRequiredPO = False
                        objICAPNFUTemplateFields.IsRequiredCQ = False
                        objICAPNFUTemplateFields.IsRequiredDD = False
                        objICAPNFUTemplateFields.IsRequiredIBFT = False
                        objICAPNFUTemplateFields.IsRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = False
                        objICAPNFUTemplateFields.IsMustRequiredFT = False
                        objICAPNFUTemplateFields.IsMustRequiredPO = False
                        objICAPNFUTemplateFields.IsMustRequiredCQ = False
                        objICAPNFUTemplateFields.IsMustRequiredDD = False
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = False
                        objICAPNFUTemplateFields.IsRequiredBillPayment = False
                    ElseIf objCompany.IsAllowOpenPayment = True And objCompany.IsBeneRequired = False And objICTemplateField.FieldName = "BeneficiaryCode" Then
                        objICAPNFUTemplateFields.IsRequired = False
                        objICAPNFUTemplateFields.MustRequired = False
                        objICAPNFUTemplateFields.IsRequiredFT = False
                        objICAPNFUTemplateFields.IsRequiredPO = False
                        objICAPNFUTemplateFields.IsRequiredCQ = False
                        objICAPNFUTemplateFields.IsRequiredDD = False
                        objICAPNFUTemplateFields.IsRequiredIBFT = False
                        objICAPNFUTemplateFields.IsRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = False
                        objICAPNFUTemplateFields.IsMustRequiredFT = False
                        objICAPNFUTemplateFields.IsMustRequiredPO = False
                        objICAPNFUTemplateFields.IsMustRequiredCQ = False
                        objICAPNFUTemplateFields.IsMustRequiredDD = False
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = False
                        objICAPNFUTemplateFields.IsRequiredBillPayment = False
                    ElseIf objCompany.IsAllowOpenPayment = False And objCompany.IsBeneRequired = False And objICTemplateField.FieldName = "BeneficiaryCode" Then
                        objICAPNFUTemplateFields.IsRequired = False
                        objICAPNFUTemplateFields.MustRequired = False
                        objICAPNFUTemplateFields.IsRequiredFT = False
                        objICAPNFUTemplateFields.IsRequiredPO = False
                        objICAPNFUTemplateFields.IsRequiredCQ = False
                        objICAPNFUTemplateFields.IsRequiredDD = False
                        objICAPNFUTemplateFields.IsRequiredIBFT = False
                        objICAPNFUTemplateFields.IsRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = False
                        objICAPNFUTemplateFields.IsMustRequiredFT = False
                        objICAPNFUTemplateFields.IsMustRequiredPO = False
                        objICAPNFUTemplateFields.IsMustRequiredCQ = False
                        objICAPNFUTemplateFields.IsMustRequiredDD = False
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = False
                        objICAPNFUTemplateFields.IsRequiredBillPayment = False
                    Else
                        objICAPNFUTemplateFields.IsRequired = objICTemplateField.IsRequired
                        objICAPNFUTemplateFields.MustRequired = objICTemplateField.IsMustRequired
                        objICAPNFUTemplateFields.IsRequiredFT = objICTemplateField.IsRequiredFT
                        objICAPNFUTemplateFields.IsRequiredPO = objICTemplateField.IsRequiredPO
                        objICAPNFUTemplateFields.IsRequiredCQ = objICTemplateField.IsRequiredCheque
                        objICAPNFUTemplateFields.IsRequiredDD = objICTemplateField.IsRequiredDD
                        objICAPNFUTemplateFields.IsRequiredIBFT = objICTemplateField.IsRequiredIBFT
                        objICAPNFUTemplateFields.IsRequiredCOTC = objICTemplateField.IsRequiredCOTC
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = objICTemplateField.IsMustRequiredIBFT
                        objICAPNFUTemplateFields.IsMustRequiredFT = objICTemplateField.IsMustRequiredFT
                        objICAPNFUTemplateFields.IsMustRequiredPO = objICTemplateField.IsMustRequiredPO
                        objICAPNFUTemplateFields.IsMustRequiredCQ = objICTemplateField.IsMustRequiredCheque
                        objICAPNFUTemplateFields.IsMustRequiredDD = objICTemplateField.IsMustRequiredDD
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = objICTemplateField.IsMustRequiredCOTC
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = objICTemplateField.IsMustRequiredBillPayment
                        objICAPNFUTemplateFields.IsRequiredBillPayment = objICTemplateField.IsRequiredBillPayment
                    End If

                    objICAPNFUTemplateFields.CreatedBy = Me.UserId
                    objICAPNFUTemplateFields.CreatedDate = Date.Now
                    FieldName += objICTemplateField.FieldName & " , "

                    If IsUpdate = False Then
                        ICAccountPaymentNatureFileUploadTemplateFieldsController.AddAPNFUploadTemplateFields(objICAPNFUTemplateFields, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    Else
                        ICAccountPaymentNatureFileUploadTemplateFieldsController.AddAPNFUploadTemplateFields(objICAPNFUTemplateFields, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    End If
                Next
                FieldName2 = FieldName.Remove(FieldName.Length - 2)
                ActionString += FieldName2
                ICUtilities.AddAuditTrail(ActionString & " ]", "Account Payment Nature File Upload Template Field", objICAPNFUTemplate.TemplateID.ToString + "" + objICAPNFUTemplate.AccountNumber.ToString + "" + objICAPNFUTemplate.BranchCode.ToString + "" + objICAPNFUTemplate.Currency.ToString + "" + objICAPNFUTemplate.PaymentNatureCode.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")
            End If



        Else

            objICTemplateFieldsColl = New ICTemplateFieldsCollection
            objICTemplateFieldsColl = ICTemplateFieldController.GetTemplateFieldsByTemplateIDAndFieldType(objICTemplate.TemplateID.ToString, "NonFlexi")
            If objICTemplateFieldsColl.Count > 0 Then
                For Each objICTemplateField As ICTemplateFields In objICTemplateFieldsColl
                    objICAPNFUTemplateFields = New ICAccountPaymentNatureFileUploadTemplatefields
                    objICAPNFUTemplateFields.es.Connection.CommandTimeout = 3600
                    objICAPNFUTemplateFields.AccountNumber = objICAPNFUTemplate.AccountNumber
                    objICAPNFUTemplateFields.BranchCode = objICAPNFUTemplate.BranchCode
                    objICAPNFUTemplateFields.Currency = objICAPNFUTemplate.Currency
                    objICAPNFUTemplateFields.PaymentNatureCode = objICAPNFUTemplate.PaymentNatureCode
                    objICAPNFUTemplateFields.TemplateID = objICAPNFUTemplate.TemplateID
                    objICAPNFUTemplateFields.FieldID = objICTemplateField.FieldID
                    objICAPNFUTemplateFields.IsRequired = objICTemplateField.IsRequired
                    objICAPNFUTemplateFields.MustRequired = objICTemplateField.IsMustRequired
                    objICAPNFUTemplateFields.FieldOrder = objICTemplateField.FieldOrder
                    objICAPNFUTemplateFields.FieldType = objICTemplateField.FieldType
                    objICAPNFUTemplateFields.FixLength = CInt(objICTemplateField.FixLength)
                    objICAPNFUTemplateFields.FieldName = objICTemplateField.FieldName
                    If objCompany.IsAllowOpenPayment = True And objCompany.IsBeneRequired = True And objICTemplateField.FieldName = "BeneficiaryCode" Then
                        objICAPNFUTemplateFields.IsRequired = False
                        objICAPNFUTemplateFields.MustRequired = False
                        objICAPNFUTemplateFields.IsRequiredFT = False
                        objICAPNFUTemplateFields.IsRequiredPO = False
                        objICAPNFUTemplateFields.IsRequiredCQ = False
                        objICAPNFUTemplateFields.IsRequiredDD = False
                        objICAPNFUTemplateFields.IsRequiredIBFT = False
                        objICAPNFUTemplateFields.IsRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = False
                        objICAPNFUTemplateFields.IsMustRequiredFT = False
                        objICAPNFUTemplateFields.IsMustRequiredPO = False
                        objICAPNFUTemplateFields.IsMustRequiredCQ = False
                        objICAPNFUTemplateFields.IsMustRequiredDD = False
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = False
                        objICAPNFUTemplateFields.IsRequiredBillPayment = False
                    ElseIf objCompany.IsAllowOpenPayment = True And objCompany.IsBeneRequired = False And objICTemplateField.FieldName = "BeneficiaryCode" Then
                        objICAPNFUTemplateFields.IsRequired = False
                        objICAPNFUTemplateFields.MustRequired = False
                        objICAPNFUTemplateFields.IsRequiredFT = False
                        objICAPNFUTemplateFields.IsRequiredPO = False
                        objICAPNFUTemplateFields.IsRequiredCQ = False
                        objICAPNFUTemplateFields.IsRequiredDD = False
                        objICAPNFUTemplateFields.IsRequiredIBFT = False
                        objICAPNFUTemplateFields.IsRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = False
                        objICAPNFUTemplateFields.IsMustRequiredFT = False
                        objICAPNFUTemplateFields.IsMustRequiredPO = False
                        objICAPNFUTemplateFields.IsMustRequiredCQ = False
                        objICAPNFUTemplateFields.IsMustRequiredDD = False
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = False
                        objICAPNFUTemplateFields.IsRequiredBillPayment = False
                    ElseIf objCompany.IsAllowOpenPayment = False And objCompany.IsBeneRequired = False And objICTemplateField.FieldName = "BeneficiaryCode" Then
                        objICAPNFUTemplateFields.IsRequired = False
                        objICAPNFUTemplateFields.MustRequired = False
                        objICAPNFUTemplateFields.IsRequiredFT = False
                        objICAPNFUTemplateFields.IsRequiredPO = False
                        objICAPNFUTemplateFields.IsRequiredCQ = False
                        objICAPNFUTemplateFields.IsRequiredDD = False
                        objICAPNFUTemplateFields.IsRequiredIBFT = False
                        objICAPNFUTemplateFields.IsRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = False
                        objICAPNFUTemplateFields.IsMustRequiredFT = False
                        objICAPNFUTemplateFields.IsMustRequiredPO = False
                        objICAPNFUTemplateFields.IsMustRequiredCQ = False
                        objICAPNFUTemplateFields.IsMustRequiredDD = False
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = False
                        objICAPNFUTemplateFields.IsRequiredBillPayment = False
                    Else
                        objICAPNFUTemplateFields.IsRequired = objICTemplateField.IsRequired
                        objICAPNFUTemplateFields.MustRequired = objICTemplateField.IsMustRequired
                        objICAPNFUTemplateFields.IsRequiredFT = objICTemplateField.IsRequiredFT
                        objICAPNFUTemplateFields.IsRequiredPO = objICTemplateField.IsRequiredPO
                        objICAPNFUTemplateFields.IsRequiredCQ = objICTemplateField.IsRequiredCheque
                        objICAPNFUTemplateFields.IsRequiredDD = objICTemplateField.IsRequiredDD
                        objICAPNFUTemplateFields.IsRequiredIBFT = objICTemplateField.IsRequiredIBFT
                        objICAPNFUTemplateFields.IsRequiredCOTC = objICTemplateField.IsRequiredCOTC
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = objICTemplateField.IsMustRequiredIBFT
                        objICAPNFUTemplateFields.IsMustRequiredFT = objICTemplateField.IsMustRequiredFT
                        objICAPNFUTemplateFields.IsMustRequiredPO = objICTemplateField.IsMustRequiredPO
                        objICAPNFUTemplateFields.IsMustRequiredCQ = objICTemplateField.IsMustRequiredCheque
                        objICAPNFUTemplateFields.IsMustRequiredDD = objICTemplateField.IsMustRequiredDD
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = objICTemplateField.IsMustRequiredCOTC
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = objICTemplateField.IsMustRequiredBillPayment
                        objICAPNFUTemplateFields.IsRequiredBillPayment = objICTemplateField.IsRequiredBillPayment
                    End If
                    objICAPNFUTemplateFields.CreatedBy = Me.UserId
                    objICAPNFUTemplateFields.CreatedDate = Date.Now
                    FieldName += objICTemplateField.FieldName & " , "
                    If IsUpdate = False Then
                        ICAccountPaymentNatureFileUploadTemplateFieldsController.AddAPNFUploadTemplateFields(objICAPNFUTemplateFields, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    Else
                        ICAccountPaymentNatureFileUploadTemplateFieldsController.AddAPNFUploadTemplateFields(objICAPNFUTemplateFields, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    End If


                Next
            End If


            objICTemplateFieldsColl = New ICTemplateFieldsCollection
            objICTemplateFieldsColl = ICTemplateFieldController.GetTemplateFieldsByTemplateIDAndFieldType(objICTemplate.TemplateID.ToString, "FixFormat")
            If objICTemplateFieldsColl.Count > 0 Then
                For Each objICTemplateField As ICTemplateFields In objICTemplateFieldsColl
                    objICAPNFUTemplateFields = New ICAccountPaymentNatureFileUploadTemplatefields
                    objICAPNFUTemplateFields.es.Connection.CommandTimeout = 3600
                    objICAPNFUTemplateFields.AccountNumber = objICAPNFUTemplate.AccountNumber
                    objICAPNFUTemplateFields.BranchCode = objICAPNFUTemplate.BranchCode
                    objICAPNFUTemplateFields.Currency = objICAPNFUTemplate.Currency
                    objICAPNFUTemplateFields.PaymentNatureCode = objICAPNFUTemplate.PaymentNatureCode
                    objICAPNFUTemplateFields.TemplateID = objICAPNFUTemplate.TemplateID
                    objICAPNFUTemplateFields.FieldID = objICTemplateField.FieldID
                    objICAPNFUTemplateFields.IsRequired = objICTemplateField.IsRequired
                    objICAPNFUTemplateFields.MustRequired = objICTemplateField.IsMustRequired
                    objICAPNFUTemplateFields.FieldOrder = objICTemplateField.FieldOrder
                    objICAPNFUTemplateFields.FieldType = objICTemplateField.FieldType
                    objICAPNFUTemplateFields.FixLength = CInt(objICTemplateField.FixLength)
                    objICAPNFUTemplateFields.FieldName = objICTemplateField.FieldName
                    If objCompany.IsAllowOpenPayment = True And objCompany.IsBeneRequired = True And objICTemplateField.FieldName = "BeneficiaryCode" Then
                        objICAPNFUTemplateFields.IsRequired = False
                        objICAPNFUTemplateFields.MustRequired = False
                        objICAPNFUTemplateFields.IsRequiredFT = False
                        objICAPNFUTemplateFields.IsRequiredPO = False
                        objICAPNFUTemplateFields.IsRequiredCQ = False
                        objICAPNFUTemplateFields.IsRequiredDD = False
                        objICAPNFUTemplateFields.IsRequiredIBFT = False
                        objICAPNFUTemplateFields.IsRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = False
                        objICAPNFUTemplateFields.IsMustRequiredFT = False
                        objICAPNFUTemplateFields.IsMustRequiredPO = False
                        objICAPNFUTemplateFields.IsMustRequiredCQ = False
                        objICAPNFUTemplateFields.IsMustRequiredDD = False
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = False
                        objICAPNFUTemplateFields.IsRequiredBillPayment = False
                    ElseIf objCompany.IsAllowOpenPayment = True And objCompany.IsBeneRequired = False And objICTemplateField.FieldName = "BeneficiaryCode" Then
                        objICAPNFUTemplateFields.IsRequired = False
                        objICAPNFUTemplateFields.MustRequired = False
                        objICAPNFUTemplateFields.IsRequiredFT = False
                        objICAPNFUTemplateFields.IsRequiredPO = False
                        objICAPNFUTemplateFields.IsRequiredCQ = False
                        objICAPNFUTemplateFields.IsRequiredDD = False
                        objICAPNFUTemplateFields.IsRequiredIBFT = False
                        objICAPNFUTemplateFields.IsRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = False
                        objICAPNFUTemplateFields.IsMustRequiredFT = False
                        objICAPNFUTemplateFields.IsMustRequiredPO = False
                        objICAPNFUTemplateFields.IsMustRequiredCQ = False
                        objICAPNFUTemplateFields.IsMustRequiredDD = False
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = False
                        objICAPNFUTemplateFields.IsRequiredBillPayment = False
                    ElseIf objCompany.IsAllowOpenPayment = False And objCompany.IsBeneRequired = False And objICTemplateField.FieldName = "BeneficiaryCode" Then
                        objICAPNFUTemplateFields.IsRequired = False
                        objICAPNFUTemplateFields.MustRequired = False
                        objICAPNFUTemplateFields.IsRequiredFT = False
                        objICAPNFUTemplateFields.IsRequiredPO = False
                        objICAPNFUTemplateFields.IsRequiredCQ = False
                        objICAPNFUTemplateFields.IsRequiredDD = False
                        objICAPNFUTemplateFields.IsRequiredIBFT = False
                        objICAPNFUTemplateFields.IsRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = False
                        objICAPNFUTemplateFields.IsMustRequiredFT = False
                        objICAPNFUTemplateFields.IsMustRequiredPO = False
                        objICAPNFUTemplateFields.IsMustRequiredCQ = False
                        objICAPNFUTemplateFields.IsMustRequiredDD = False
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = False
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = False
                        objICAPNFUTemplateFields.IsRequiredBillPayment = False
                    Else
                        objICAPNFUTemplateFields.IsRequired = objICTemplateField.IsRequired
                        objICAPNFUTemplateFields.MustRequired = objICTemplateField.IsMustRequired
                        objICAPNFUTemplateFields.IsRequiredFT = objICTemplateField.IsRequiredFT
                        objICAPNFUTemplateFields.IsRequiredPO = objICTemplateField.IsRequiredPO
                        objICAPNFUTemplateFields.IsRequiredCQ = objICTemplateField.IsRequiredCheque
                        objICAPNFUTemplateFields.IsRequiredDD = objICTemplateField.IsRequiredDD
                        objICAPNFUTemplateFields.IsRequiredIBFT = objICTemplateField.IsRequiredIBFT
                        objICAPNFUTemplateFields.IsRequiredCOTC = objICTemplateField.IsRequiredCOTC
                        objICAPNFUTemplateFields.IsMustRequiredIBFT = objICTemplateField.IsMustRequiredIBFT
                        objICAPNFUTemplateFields.IsMustRequiredFT = objICTemplateField.IsMustRequiredFT
                        objICAPNFUTemplateFields.IsMustRequiredPO = objICTemplateField.IsMustRequiredPO
                        objICAPNFUTemplateFields.IsMustRequiredCQ = objICTemplateField.IsMustRequiredCheque
                        objICAPNFUTemplateFields.IsMustRequiredDD = objICTemplateField.IsMustRequiredDD
                        objICAPNFUTemplateFields.IsMustRequiredCOTC = objICTemplateField.IsMustRequiredCOTC
                        objICAPNFUTemplateFields.IsMustRequiredBillPayment = objICTemplateField.IsMustRequiredBillPayment
                        objICAPNFUTemplateFields.IsRequiredBillPayment = objICTemplateField.IsRequiredBillPayment
                    End If
                    objICAPNFUTemplateFields.CreatedBy = Me.UserId
                    objICAPNFUTemplateFields.CreatedDate = Date.Now
                    FieldName += objICTemplateField.FieldName & " , "
                    If IsUpdate = False Then
                        ICAccountPaymentNatureFileUploadTemplateFieldsController.AddAPNFUploadTemplateFields(objICAPNFUTemplateFields, False, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    Else
                        ICAccountPaymentNatureFileUploadTemplateFieldsController.AddAPNFUploadTemplateFields(objICAPNFUTemplateFields, True, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    End If
                Next
                FieldName2 = FieldName.Remove(FieldName.Length - 2)
                ActionString += FieldName2
                ICUtilities.AddAuditTrail(ActionString & "]", "Account Payment Nature File Upload Template Field", objICAPNFUTemplate.TemplateID.ToString + "" + objICAPNFUTemplate.AccountNumber.ToString + "" + objICAPNFUTemplate.BranchCode.ToString + "" + objICAPNFUTemplate.Currency.ToString + "" + objICAPNFUTemplate.PaymentNatureCode.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "ADD")

            End If

        End If
    End Sub


End Class
