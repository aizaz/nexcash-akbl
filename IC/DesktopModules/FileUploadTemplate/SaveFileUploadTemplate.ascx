﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveFileUploadTemplate.ascx.vb"
    Inherits="DesktopModules_FileUploadTemplate_SaveFileUploadTemplate" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to remove Record(s)?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    /*function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;

        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }    */
</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z -_]*$" ErrorMessage="Invalid Name" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFileUploadTemplateName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="regTemplateCode" Validation-IsRequired="true"
        EmptyMessage="" ValidationExpression="^[a-zA-Z0-9]+$" ErrorMessage="Invalid Template Code">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFileUploadTemplateCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
</telerik:RadInputManager>
<telerik:RadInputManager ID="rimSheet" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior3" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Invalid Sheet Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtSheetName" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<telerik:RadInputManager ID="rimDelemiter" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior4" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Invalid Delimiter Character">
        <TargetControls>
            <telerik:TargetInput ControlID="txtdelimiter" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<telerik:RadInputManager ID="rimHeaderAndDetail" runat="server" Enabled="false">
    <telerik:TextBoxSetting BehaviorID="regHeader" Validation-IsRequired="false" EmptyMessage=""
        ErrorMessage="Invalid Header Identifier">
        <TargetControls>
            <telerik:TargetInput ControlID="txtHeaderIdentifier" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reDetail" Validation-IsRequired="false" EmptyMessage=""
        ErrorMessage="Invalid Detail Identifier">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDetailIdentifier" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblFileUploadTemplateName" runat="server" Text="Name" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqFileUploadTempName" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblFileUploadTemplateName0" runat="server" Text="File Upload Template Code"
                CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqFileUploadCode" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtFileUploadTemplateName" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtFileUploadTemplateCode" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblFileUploadTemplateDescription" runat="server" Text="Format" CssClass="lbl">
            </asp:Label>
            <asp:Label ID="lblReqFileFormat" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblSheetOrFileFormat" runat="server" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqSheetNameOrFixLength" runat="server" Text="*" ForeColor="Red"
                Visible="False"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RadioButtonList ID="RbFormat" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                <asp:ListItem Value=".txt">.txt</asp:ListItem>
                <asp:ListItem Value=".csv">.csv</asp:ListItem>
                <asp:ListItem Value=".xls">.xls</asp:ListItem>
                <asp:ListItem Value=".xlsx">.xlsx</asp:ListItem>
            </asp:RadioButtonList>
            <asp:RequiredFieldValidator ID="rfvFileFormat" runat="server" ControlToValidate="RbFormat"
                ErrorMessage="Select File Format" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <table id="tblXLS" runat="server" style="display: none" width="100%">
                <tr align="left" style="width: 100%" valign="top">
                    <td align="left" style="width: 100%" valign="top">
                        <asp:TextBox ID="txtSheetName" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <table id="tblTXT" runat="server" style="display: none; width= 100%">
                <tr align="left" style="width: 100%" valign="top">
                    <td align="left" style="width: 100%" valign="top">
                        <asp:RadioButtonList ID="rbFieldFormate" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                            <asp:ListItem Value="0">Fixed Length</asp:ListItem>
                            <asp:ListItem Value="1">Delimit Character</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvFieldFormat" runat="server" ControlToValidate="rbFieldFormate"
                            Display="Dynamic" Enabled="false" ErrorMessage="Select field format" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <table id="tblTemplateType" runat="server" style="display: none" width="100%">
                <%--style="display:none"--%>
                <tr>
                    <td align="left" valign="top" width="100%">
                        <asp:Label ID="lblTemplateType" runat="server" Text="Template Type" CssClass="lbl"></asp:Label><br />
                        <asp:RadioButtonList ID="rbTemplateType" runat="server" RepeatDirection="Horizontal"
                            AutoPostBack="True">
                            <asp:ListItem>Single</asp:ListItem>
                            <asp:ListItem>Dual</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:RequiredFieldValidator ID="rfvTemplateType" runat="server" ControlToValidate="rbTemplateType"
                            ErrorMessage="Select template type" Display="Dynamic" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <table id="tblDelimitAndDualSetting" runat="server" width="100%" style="display: none">
                <tr>
                    <td align="left" valign="top" width="100%">
                        <asp:Panel ID="pnlDelimeter" runat="server" Width="100%">
                            <asp:Label ID="lblDelimitCharacter" runat="server" CssClass="lbl" Text="Delimit Character"></asp:Label>
                            <asp:Label ID="lblReqDelimitCharacter" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            <br />
                            <asp:TextBox ID="txtdelimiter" runat="server" CssClass="txtbox" MaxLength="1"></asp:TextBox>
                        </asp:Panel>
                        <br />
                        <asp:Panel ID="pnlHeaderIdentifier" runat="server" Width="100%">
                            <asp:Label ID="lblHeaderIdentifier" runat="server" CssClass="lbl" Text="Header Identifier"></asp:Label>
                            <asp:Label ID="lblReqHeaderIdentifier" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            <br />
                            <asp:TextBox ID="txtHeaderIdentifier" runat="server" CssClass="txtbox" MaxLength="2"></asp:TextBox>
                        </asp:Panel>
                        <br />
                        <asp:Panel ID="pnlDetailIdentifier" runat="server" Width="100%">
                            <asp:Label ID="lblDetailIdentifier" runat="server" CssClass="lbl" Text="Detail Identifier"></asp:Label>
                            <asp:Label ID="lblReqDetailIdentifier" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            <br />
                            <asp:TextBox ID="txtDetailIdentifier" runat="server" CssClass="txtbox" MaxLength="2"></asp:TextBox>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%" colspan="4">
            <%--<asp:UpdatePanel ID="upFileUpload" runat="server" UpdateMode="Always" > 
                <ContentTemplate>--%>
            <table width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" colspan="4">
                        <asp:Label ID="lblAssignedSingleTemplateFieldsFields" runat="server" CssClass="headingblue"
                            Text="Assigned Template Fields"></asp:Label><br />
                        <asp:Label ID="lblRNFAssignedTemplateFields" runat="server" CssClass="headingblue"
                            Font-Bold="False" Text="No Record Found" Visible="False"></asp:Label><br />
                        <table id="tblAssignedFieldsForSingleTemplate" runat="server" style="width: 100%;
                            display: none">
                            <tr>
                                <td align="left" valign="top" colspan="4">
                                    <telerik:RadGrid ID="gvAssignedTemplateFieldForSingleTemplate" runat="server" AllowPaging="false"
                                        AllowSorting="false" AutoGenerateColumns="False" CellSpacing="0" PageSize="201"
                                        ShowFooter="True">
                                        <ClientSettings>
                                            <Scrolling UseStaticHeaders="true" AllowScroll="true" />
                                        </ClientSettings>
                                        <AlternatingItemStyle CssClass="rgAltRow" />
                                        <MasterTableView DataKeyNames="FieldID" TableLayout="Fixed">
                                            <CommandItemSettings ExportToPdfText="Export to PDF" />
                                            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                            </RowIndicatorColumn>
                                            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                            </ExpandCollapseColumn>
                                            <Columns>
                                                <%-- <telerik:GridTemplateColumn HeaderText="Required">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select" TextAlign="Right" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" "  Checked='<%# Bind("IsRequired") %>'/>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>--%>
                                                <telerik:GridBoundColumn DataField="FieldName" HeaderText="Name" SortExpression="FieldName">
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </telerik:GridBoundColumn>
                                                <telerik:GridTemplateColumn HeaderText="Length">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtFieldDBLength" runat="server" onkeypress="return numeralsOnly(event)"
                                                            Text='<%# Bind("FixLength") %>'></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Required Cheque">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRequiredCheque" runat="server" Checked='<%# Bind("IsRequiredCheque") %>'
                                                            Enabled="false" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Required PO">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRequiredPO" runat="server" Checked='<%# Bind("IsRequiredPO") %>'
                                                            Enabled="false" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Required DD">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRequiredDD" runat="server" Checked='<%# Bind("IsRequiredDD") %>'
                                                            Enabled="false" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Required IBFT">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRequiredIBFT" runat="server" Checked='<%# Bind("IsRequiredIBFT") %>'
                                                            Enabled="false" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Required FT">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRequiredFT" runat="server" Checked='<%# Bind("IsRequiredFT") %>'
                                                            Enabled="false" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Required COTC">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRequiredCOTC" runat="server" Checked='<%# Bind("IsRequiredCOTC") %>'
                                                            Enabled="false" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Required Bill Payment">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkRequiredBillPayment" runat="server" Checked='<%# Bind("IsRequiredBillPayment") %>'
                                                            Enabled="false" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Field Order">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtFieldOrder" runat="server" Enabled="true" MaxLength="2" Width="50"
                                                            Text='<%# Bind("FieldOrder") %>'></asp:TextBox><br />
                                                        <asp:RangeValidator ID="rvFieldMaxOrder" ControlToValidate="txtFieldOrder" MinimumValue='<%# Bind("MinOrder") %>'
                                                            MaximumValue='<%# Bind("MaxOrder") %>' Type="Integer" Enabled="true" ErrorMessage="Field order must be greater than zero and less than or equal to maximum size as displayed in grid."
                                                            runat="server" ValidationGroup="SetOrder" /><br />
                                                        <asp:RequiredFieldValidator ID="rfvFieldDBLength" runat="server" InitialValue=""
                                                            ValidationGroup="SetOrder" Display="Dynamic" ControlToValidate="txtFieldOrder"
                                                            Enabled="true" ErrorMessage="Please enter order in numbers only." SetFocusOnError="true"></asp:RequiredFieldValidator><br />
                                                        <asp:RegularExpressionValidator ID="revPassqord" runat="server" ControlToValidate="txtFieldOrder"
                                                            Display="Dynamic" ErrorMessage="Only numbers are allowed." ValidationExpression="^\d+$"
                                                            ValidationGroup="SetOrder"></asp:RegularExpressionValidator>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </telerik:GridTemplateColumn>
                                            
                                                <telerik:GridTemplateColumn HeaderText="Delete">
                                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("FieldID") %>'
                                                            CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                            ToolTip="Delete" Visible='<%# Bind("IsEnabled") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                        </MasterTableView>
                                        <ItemStyle CssClass="rgRow" />
                                    </telerik:RadGrid>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <table id="tblSetFieldListOrder" runat="server" style="width: 100%; display: none">
                            <tr>
                                <td align="center" valign="top" colspan="2">
                                    <asp:Button ID="btnSetOrder" runat="server" Text="Set Order" CssClass="btn" ValidationGroup="SetOrder" />
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:Label ID="lblAssignedDetailField" runat="server" CssClass="headingblue" Text="Assigned Detail Fields"></asp:Label><br />
                        <asp:Label ID="lblRNFAssignedDetailFields" runat="server" CssClass="headingblue"
                            Font-Bold="False" Text="No Record Found" Visible="False"></asp:Label><br />
                        <table id="tblAssignedDetailFields" runat="server" style="width:100%;display:none">
                        <tr>
                        <td align="left" valign="top" colspan="4">
                        <telerik:RadGrid ID="gvAssignedDetailFields" runat="server" AllowPaging="false" AllowSorting="false"
                                AutoGenerateColumns="False" CellSpacing="0" PageSize="168" ShowFooter="True">
                                <ClientSettings>
                                    <Scrolling UseStaticHeaders="true" AllowScroll="true" />
                                </ClientSettings>
                                <AlternatingItemStyle CssClass="rgAltRow" />
                                <MasterTableView DataKeyNames="FieldID" TableLayout="Fixed">
                                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <%-- <telerik:GridTemplateColumn HeaderText="Required">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select" TextAlign="Right" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" "  Checked='<%# Bind("IsRequired") %>'/>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>--%>
                                        <telerik:GridBoundColumn DataField="FieldName" HeaderText="Name" SortExpression="FieldName">
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="Length">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtFieldDBLength" runat="server" onkeypress="return numeralsOnly(event)"
                                                    Text='<%# Bind("FixLength") %>'></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Required Cheque">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredCheque" runat="server" Checked='<%# Bind("IsRequiredCheque") %>'
                                                    Enabled="false" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Required PO">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredPO" runat="server" Checked='<%# Bind("IsRequiredPO") %>'
                                                    Enabled="false" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Required DD">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredDD" runat="server" Checked='<%# Bind("IsRequiredDD") %>'
                                                    Enabled="false" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Required IBFT">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredIBFT" runat="server" Checked='<%# Bind("IsRequiredIBFT") %>'
                                                    Enabled="false" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Required FT">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredFT" runat="server" Checked='<%# Bind("IsRequiredFT") %>'
                                                    Enabled="false" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Required COTC">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredCOTC" runat="server" Checked='<%# Bind("IsRequiredCOTC") %>'
                                                    Enabled="false" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        </telerik:GridTemplateColumn>
                                         <telerik:GridTemplateColumn HeaderText="Required Bill Payment">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredBillPayment" runat="server" Checked='<%# Bind("IsRequiredBillPayment") %>'
                                                    Enabled="false" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Field Order">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtFieldOrderDetail" runat="server" Enabled="true" MaxLength="2"
                                                    Width="50" Text='<%# Bind("FieldOrder") %>'></asp:TextBox><br />
                                                <asp:RangeValidator ID="rvFieldMaxOrder" ControlToValidate="txtFieldOrderDetail"
                                                    MinimumValue='<%# Bind("MinOrder") %>' MaximumValue='<%# Bind("MaxOrder") %>'
                                                    Type="Integer" Enabled="true" ErrorMessage="Field order must be greater than zero and less than or equal to maximum size as displayed in grid."
                                                    runat="server" ValidationGroup="SetOrderDetail" /><br />
                                                <asp:RequiredFieldValidator ID="rfvFieldDBLength" runat="server" InitialValue=""
                                                    ValidationGroup="SetOrderDetail" Display="Dynamic" ControlToValidate="txtFieldOrderDetail"
                                                    Enabled="true" ErrorMessage="Please enter order in numbers only." SetFocusOnError="true"></asp:RequiredFieldValidator><br />
                                                <asp:RegularExpressionValidator ID="revPassqord" runat="server" ControlToValidate="txtFieldOrderDetail"
                                                    Display="Dynamic" ErrorMessage="Only numbers are allowed." ValidationExpression="^\d+$"
                                                    ValidationGroup="SetOrderDetail"></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        </telerik:GridTemplateColumn>
                                        <%-- <telerik:GridTemplateColumn HeaderText="Move Up">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnmoveup" runat="server" CommandName="Up" CommandArgument='<%#Eval("FieldID") %>'
                                                    ImageUrl="~/images/action_up.gif" />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Move Down">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnmoveDown" runat="server" CommandName="Down" CommandArgument='<%#Eval("FieldID") %>'
                                                    ImageUrl="~/images/action_down.gif" />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>--%>
                                        <telerik:GridTemplateColumn HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("FieldID") %>'
                                                    CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                    ToolTip="Delete" Visible='<%# Bind("IsEnabled") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                </MasterTableView>
                                <ItemStyle CssClass="rgRow" />
                            </telerik:RadGrid>
                        
                        </td>
                        
                        </tr>
                        
                        
                        </table>
                        
                        <br />
                        <table id="tblDetailSetOrder" runat="server" style="width: 100%; display: none">
                            <tr>
                                <td align="center" valign="top" colspan="2">
                                    <asp:Button ID="btnDetailSetOrder" runat="server" Text="Set Order" CssClass="btn"
                                        ValidationGroup="SetOrderDetail" />
                                </td>
                            </tr>
                        </table>
                        <asp:Label ID="lblTemplateFieldHeader" runat="server" CssClass="headingblue" Text="Fields List"></asp:Label><br />
                        <table id="tblFieldList" runat="server" style="width:100%;display:none">
                        <tr>
                        <td align="left" valign="top" colspan="4">
                        <telerik:RadGrid ID="gvFieldsList" runat="server" AllowPaging="false" AllowSorting="false"
                                AutoGenerateColumns="False" CellSpacing="0" PageSize="201" ShowFooter="True">
                                <ClientSettings>
                                    <Scrolling UseStaticHeaders="true" AllowScroll="true" />
                                </ClientSettings>
                                <AlternatingItemStyle CssClass="rgAltRow" />
                                <MasterTableView DataKeyNames="FieldID" TableLayout="Fixed">
                                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <telerik:GridTemplateColumn HeaderText="Required">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select" TextAlign="Right" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " Checked='<%# Bind("MustRequired") %>'
                                                    Enabled='<%# Bind("IsEnabled") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="FieldName" HeaderText="Name" SortExpression="FieldName">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="Fix Length">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtFieldDBLength" runat="server" onkeypress="return numeralsOnly(event)"
                                                    Text='<%# Bind("FieldDBLength") %>'></asp:TextBox>
                                                <asp:RangeValidator runat="server" ID="rVFieldDBLEngth" ControlToValidate="txtFieldDBLength"
                                                    Type="Integer" Display="Dynamic" ErrorMessage="Length is greater than Fixed."
                                                    ForeColor="Red" MinimumValue="1" MaximumValue='<%# Eval("FieldDBLength") %>'
                                                    Enabled="true"></asp:RangeValidator>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Required Cheque">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredCheque" runat="server" Checked='<%# Bind("IsRequiredCheque") %>'
                                                    Enabled='<%# Bind("ChequeEnabled") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Required PO">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredPO" runat="server" Checked='<%# Bind("IsRequiredPO") %>'
                                                    Enabled='<%# Bind("POEnabled") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Required DD">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredDD" runat="server" Checked='<%# Bind("IsRequiredDD") %>'
                                                    Enabled='<%# Bind("DDEnabled") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Required IBFT">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredIBFT" runat="server" Checked='<%# Bind("IsRequiredIBFT") %>'
                                                    Enabled='<%# Bind("IBFTEnabled") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Required FT">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredFT" runat="server" Checked='<%# Bind("IsRequiredFT") %>'
                                                    Enabled='<%# Bind("FTEnabled") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Required COTC">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredCOTC" runat="server" Checked='<%# Bind("IsRequiredCOTC")%>'
                                                    Enabled='<%# Bind("COTCEnabled") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Required Bill Payment">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredBillPayment" runat="server" Checked='<%# Bind("IsRequiredBillPayment")%>'
                                                    Enabled='<%# Bind("BillPaymentEnabled") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                    <EditFormSettings>
                                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                        </EditColumn>
                                    </EditFormSettings>
                                </MasterTableView>
                                <ItemStyle CssClass="rgRow" />
                                <FilterMenu EnableImageSprites="False">
                                </FilterMenu>
                            </telerik:RadGrid>
                        
                        
                        </td>
                        
                        </tr>
                        
                        </table>
                        <br />
                        <asp:Label ID="lblDetailFields" runat="server" CssClass="headingblue" Text="Detail Field List"></asp:Label><br />
                        <table id="tblDetail" runat="server" style="widh:100%; display:none">
                        <tr>
                        <td align="left" valign="top" colspan="4">
                        <telerik:RadGrid ID="gvDetailFields" runat="server" AllowPaging="false" Width="100%"
                                AllowSorting="false" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                                PageSize="168">
                                <ClientSettings>
                                    <Scrolling UseStaticHeaders="true" AllowScroll="true" />
                                </ClientSettings>
                                <AlternatingItemStyle CssClass="rgAltRow" />
                                <MasterTableView DataKeyNames="FieldID" TableLayout="Fixed">
                                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <telerik:GridTemplateColumn HeaderText="Required">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select" TextAlign="Right" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " Checked='<%# Bind("MustRequired") %>'
                                                    Enabled='<%# Bind("IsEnabled") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="FieldName" HeaderText="Name" SortExpression="FieldName">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="Required Cheque">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredCheque" runat="server" Checked='<%# Bind("IsRequiredCheque") %>'
                                                    Enabled='<%# Bind("ChequeEnabled") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Required PO">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredPO" runat="server" Checked='<%# Bind("IsRequiredPO") %>'
                                                    Enabled='<%# Bind("POEnabled") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Required DD">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredDD" runat="server" Checked='<%# Bind("IsRequiredDD") %>'
                                                    Enabled='<%# Bind("DDEnabled") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Required IBFT">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredIBFT" runat="server" Checked='<%# Bind("IsRequiredIBFT") %>'
                                                    Enabled='<%# Bind("IBFTEnabled") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Required FT">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredFT" runat="server" Checked='<%# Bind("IsRequiredFT") %>'
                                                    Enabled='<%# Bind("FTEnabled") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                         <telerik:GridTemplateColumn HeaderText="Required COTC">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredCOTC" runat="server" Checked='<%# Bind("IsRequiredCOTC")%>'
                                                    Enabled='<%# Bind("COTCEnabled") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Required Bill Payment">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkRequiredBillPayment" runat="server" Checked='<%# Bind("IsRequiredBillPayment")%>'
                                                    Enabled='<%# Bind("BillPaymentEnabled") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                </MasterTableView>
                                <ItemStyle CssClass="rgRow" />
                            </telerik:RadGrid>
                        </td>
                        
                        </tr>
                        
                        </table>
                        
                        <%-- <telerik:GridTemplateColumn HeaderText="Required">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select" TextAlign="Right" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" "  Checked='<%# Bind("IsRequired") %>'/>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>--%>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;&nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="72px" />
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px"
                CausesValidation="False" />
            &nbsp;
        </td>
    </tr>
</table>
