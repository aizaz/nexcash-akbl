﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewFileUploadTemplate.ascx.vb" Inherits="DesktopModules_FileUploadTemplate_ViewFileUploadTemplate" %>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
    function conBukDelete() {
        if (window.confirm("Are you sure you wish to remove Record(s)?") == true) {
            return true;
        }
        else {
            return false;
        }
    }  
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;

        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    //    }
</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnSaveFileUploadTemplate" runat="server" Text="Add File Upload Template" CssClass="btn" />
                        <asp:HiddenField ID="hfFUTemplateID" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <asp:Label ID="lblFUTemplateListHeader" runat="server" Text="File Upload Template List" CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <%--<asp:GridView ID="gvFileUploadTemplate" runat="server" 
                            AutoGenerateColumns="False" CssClass="Grid"
                            Width="100%" AllowPaging="True" EnableModelValidation="True" 
                            AllowSorting="True">
                            <AlternatingRowStyle CssClass="GridAltItem" />
                            <Columns>
                         
                                <asp:BoundField DataField="TemplateName" HeaderText="Name" SortExpression="TemplateName">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                 <asp:BoundField DataField="TemplateFormat" HeaderText="Format" SortExpression="TemplateFormat">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                 <asp:BoundField DataField="SheetName" HeaderText="Sheet Name" SortExpression="SheetName">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:CheckBoxField DataField="IsFixLength" HeaderText="FixLength">
                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                    </asp:CheckBoxField>                                 
                     <asp:BoundField DataField="FieldDelimiter" HeaderText="Field Delimiter" SortExpression="FieldDelimiter">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveFileUploadTemplate", "&mid=" & Me.ModuleId & "&id="& Eval("TemplateID"))%>'
                                            ToolTip="Edit">Edit</asp:HyperLink>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                </asp:TemplateField>                                
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("TemplateID") %>'
                                            CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                            ToolTip="Delete" />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="GridHeader" />
                            <PagerStyle CssClass="GridPager" />
                            <RowStyle CssClass="GridItem" />
                        </asp:GridView>--%>
                        <asp:Label ID="lblRNF" runat="server" Text="Record Not Found." CssClass="headingblue"
                            Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                    <telerik:RadGrid ID="gvFileUploadTemplate" runat="server" AllowPaging="True"
                            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                            PageSize="100">
                             <ClientSettings>
                            <Scrolling UseStaticHeaders="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView NoMasterRecordsText="" DataKeyNames="TemplateID" TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Approve" DataField="IsApproved">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="FileUploadTemplateCode" HeaderText="Code" SortExpression="FileUploadTemplateCode">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TemplateName" HeaderText="Name" SortExpression="TemplateName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="TemplateFormat" HeaderText="Format" SortExpression="TemplateFormat">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="SheetName" HeaderText="Sheet Name"
                                        SortExpression="SheetName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="FieldDelimiter" HeaderText="Field Delimiter"
                                        SortExpression="FieldDelimiter">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Fix Length" DataField="IsFixLength">
                                    </telerik:GridCheckBoxColumn>
                                    
                                    <telerik:GridTemplateColumn HeaderText="Edit">
                                        <ItemTemplate>
                                        <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveFileUploadTemplate", "&mid=" & Me.ModuleId & "&id="& Eval("TemplateID"))%>'
                                            ToolTip="Edit">Edit</asp:HyperLink>
                                    </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                  <telerik:GridTemplateColumn HeaderText="Delete">
                                       <ItemTemplate>
                                        <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("TemplateID") %>'
                                            CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                            ToolTip="Delete" />
                                    </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnBulkDeleteTemplates" runat="server" OnClientClick="javascript: return conBukDelete();"
                            Text="Delete File Upload Templates" CssClass="btn" />
                    </td>
                </tr>
               </table>
</td>
</tr></table>