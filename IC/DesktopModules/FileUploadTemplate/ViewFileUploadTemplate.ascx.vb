﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_FileUploadTemplate_ViewFileUploadTemplate
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable


    Protected Sub DesktopModules_Bank_ViewBank_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnSaveFileUploadTemplate.Visible = CBool(htRights("Add"))
                LoadgvFileUploadTemplate(0, Me.gvFileUploadTemplate.PageSize, True)

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client Bulk Upload Template Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            ' str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function

    Private Sub LoadgvFileUploadTemplate(ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean)
        Try

            ICFileUploadTemplateController.GetFileUploadTemplateForRadGrid(PageNumber, PageSize, Me.gvFileUploadTemplate, DoDataBind)

            If gvFileUploadTemplate.Items.Count > 0 Then
                gvFileUploadTemplate.Visible = True
                lblRNF.Visible = False
                btnBulkDeleteTemplates.Visible = CBool(htRights("Delete"))
                gvFileUploadTemplate.Columns(7).Visible = CBool(htRights("Delete"))
            Else
                gvFileUploadTemplate.Visible = False
                btnBulkDeleteTemplates.Visible = False
                lblRNF.Visible = True

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub


    Protected Sub btnSaveBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveFileUploadTemplate.Click
        Response.Redirect(NavigateURL("SaveFileUploadTemplate", "&mid=" & Me.ModuleId & "&id=0"), False)
    End Sub

 

    Protected Sub gvFileUploadTemplate_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvFileUploadTemplate.ItemCommand
        Try
            Dim objICTemplate As New ICTemplate
            objICTemplate.es.Connection.CommandTimeout = 3600
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    If ICTemplateFieldListController.CheckFileStatusAgainstTemplate(e.CommandArgument.ToString) = False Then
                        If objICTemplate.LoadByPrimaryKey(e.CommandArgument.ToString) Then
                            ICTemplateFieldListController.DeleteTemplateFieldForUpdate(objICTemplate.TemplateID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            ICFileUploadTemplateController.DeleteTemplate(objICTemplate.TemplateID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            UIUtilities.ShowDialog(Me, "Delete File Upload Template", "File upload template deleted successfully", ICBO.IC.Dialogmessagetype.Success)
                            LoadgvFileUploadTemplate(Me.gvFileUploadTemplate.CurrentPageIndex + 1, Me.gvFileUploadTemplate.PageSize, True)
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Error", "File upload template can not deleted due to following reason: <br /> 1. Files exists in pending read status", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        LoadgvFileUploadTemplate(Me.gvFileUploadTemplate.CurrentPageIndex + 1, Me.gvFileUploadTemplate.PageSize, False)
                        Exit Sub
                    End If

                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Error ", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                LoadgvFileUploadTemplate(Me.gvFileUploadTemplate.CurrentPageIndex + 1, Me.gvFileUploadTemplate.PageSize, False)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvFileUploadTemplate_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvFileUploadTemplate.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvFileUploadTemplate.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvFileUploadTemplate_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvFileUploadTemplate.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvFileUploadTemplate.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Private Function CheckGVFileUploadTemplateSelected() As Boolean
        Try
            Dim Result As Boolean = False
            Dim chkSelect As CheckBox
            For Each gvFileUploadTemplateRow As GridDataItem In gvFileUploadTemplate.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(gvFileUploadTemplateRow.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Return Result
                    Exit For
                End If
            Next
        Catch ex As Exception

            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnBulkDeleteTemplates_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBulkDeleteTemplates.Click
        If Page.IsValid Then
            Try
                Dim chkBox As CheckBox
                Dim objICFileUplodTemplate As ICTemplate
                Dim PassToDeleteCount As Integer = 0
                Dim FailToDeleteCount As Integer = 0
                Dim FileUploadTemplateID As String

                If CheckGVFileUploadTemplateSelected() = False Then
                    UIUtilities.ShowDialog(Me, "Error", "Please select at least one file upload template", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each gvFUTemplateRow As GridDataItem In gvFileUploadTemplate.Items
                    chkBox = New CheckBox
                    chkBox = DirectCast(gvFUTemplateRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkBox.Checked = True Then
                        objICFileUplodTemplate = New ICTemplate
                        objICFileUplodTemplate.es.Connection.CommandTimeout = 3600
                        FileUploadTemplateID = gvFUTemplateRow.GetDataKeyValue("TemplateID")
                        objICFileUplodTemplate.LoadByPrimaryKey(FileUploadTemplateID)
                        If ICTemplateFieldListController.CheckFileStatusAgainstTemplate(FileUploadTemplateID) = False Then
                            Try
                                ICTemplateFieldListController.DeleteTemplateFieldForUpdate(FileUploadTemplateID, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                ICFileUploadTemplateController.DeleteTemplate(FileUploadTemplateID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                PassToDeleteCount = PassToDeleteCount + 1
                            Catch ex As Exception
                                FailToDeleteCount = FailToDeleteCount + 1
                                Continue For
                            End Try
                        Else
                            FailToDeleteCount = FailToDeleteCount + 1
                        End If
                    End If
                Next
                If Not PassToDeleteCount = 0 And Not FailToDeleteCount = 0 Then
                    LoadgvFileUploadTemplate(Me.gvFileUploadTemplate.CurrentPageIndex + 1, Me.gvFileUploadTemplate.PageSize, True)
                    UIUtilities.ShowDialog(Me, "Delete File Upload Template", "[ " & PassToDeleteCount.ToString & " ] File upload template(s) deleted successfuly.<br /> [ " & FailToDeleteCount.ToString & " ] File upload template(s) can not be deleted due to following reason: <br /> 1. File exist with pending read status<br /> 2. Delete associated records", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                ElseIf Not PassToDeleteCount = 0 And FailToDeleteCount = 0 Then
                    LoadgvFileUploadTemplate(Me.gvFileUploadTemplate.CurrentPageIndex + 1, Me.gvFileUploadTemplate.PageSize, True)
                    UIUtilities.ShowDialog(Me, "Delete File Upload Template", PassToDeleteCount.ToString & " File upload template(s) deleted successfuly.", ICBO.IC.Dialogmessagetype.Success)

                    Exit Sub
                ElseIf Not FailToDeleteCount = 0 And PassToDeleteCount = 0 Then
                    LoadgvFileUploadTemplate(Me.gvFileUploadTemplate.CurrentPageIndex + 1, Me.gvFileUploadTemplate.PageSize, True)
                    UIUtilities.ShowDialog(Me, "Delete File Upload Template", "[ " & FailToDeleteCount.ToString & " ] File upload template(s) can not be deleted due to following reasons: <br /> 1. File exist with pending read status<br /> 2. Delete associated records", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub gvFileUploadTemplate_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvFileUploadTemplate.NeedDataSource
        Try
            LoadgvFileUploadTemplate(Me.gvFileUploadTemplate.CurrentPageIndex + 1, Me.gvFileUploadTemplate.PageSize, False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
