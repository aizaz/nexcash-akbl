﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Partial Class DesktopModules_RePrintReIssuanceQueue_RePrintReIssuanceQueue
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Dim dtDa As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Page.IsPostBack = False Then
                LoadgvAllowRePrintReIssue()
                LoadgvApproveRePrintReIssue()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadgvAllowRePrintReIssue()
        Try
            Dim connectionString As String = ""
            Dim fileLocation As String = Server.MapPath("~/Excel Sheets For Grids/RePrintingQueue.xlsx")
            connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = "Sheet1$"
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()
            gvAllowRePrintReIssue.DataSource = dtExcelRecords
            gvAllowRePrintReIssue.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadgvApproveRePrintReIssue()
        'Try
        '    Dim connectionString As String = ""
        '    Dim fileLocation As String = Server.MapPath("~/Excel Sheets For Grids/RePrintingQueue.xlsx")
        '    connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
        '    Dim con As New OleDbConnection(connectionString)
        '    Dim cmd As New OleDbCommand()
        '    cmd.CommandType = System.Data.CommandType.Text
        '    cmd.Connection = con
        '    Dim dAdapter As New OleDbDataAdapter(cmd)
        '    Dim dtExcelRecords As New DataTable()
        '    con.Open()
        '    Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
        '    Dim getExcelSheetName As String = "Sheet1$"
        '    cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
        '    dAdapter.SelectCommand = cmd
        '    dAdapter.Fill(dtExcelRecords)
        '    con.Close()
        '    gvApproveRePrintReIssue.DataSource = dtExcelRecords
        '    gvApproveRePrintReIssue.DataBind()
        'Catch ex As Exception
        '    ProcessModuleLoadException(Me, ex, False)
        '    UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        'End Try
    End Sub
End Class


