﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RePrintReIssuanceQueue.ascx.vb"
    Inherits="DesktopModules_RePrintReIssuanceQueue_RePrintReIssuanceQueue" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">

    function conApproveRePrinting() {
        if (window.confirm("Are you sure you wish to approve re printing?") == true) {
            var btnAllowRePrint = document.getElementById('<%=btnAllowRePrint.ClientID%>')
            btnAllowRePrint.value = "Processing...";
            btnAllowRePrint.disabled = true;

            var a = btnAllowRePrint.name;
            __doPostBack(a, '');
            return true;
        }
        else {
            return false;
        }
    }

    function conCancelRePrinting() {
        if (window.confirm("Are you sure you wish to cancel re printing?") == true) {
            var btnAllowRePrint0 = document.getElementById('<%=btnAllowRePrint0.ClientID%>')
            btnAllowRePrint0.value = "Processing...";
            btnAllowRePrint0.disabled = true;

            var a = btnAllowRePrint0.name;
            __doPostBack(a, '');
            return true;
        }
        else {
            return false;
        }
    }

    </script>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:TextBoxSetting BehaviorID="reTnxCount" Validation-IsRequired="false" EmptyMessage="Transaction Count"
        ErrorMessage="Transaction Count">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTnxCount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reTotalAmount" Validation-IsRequired="false"
        EmptyMessage="Total Amount" ErrorMessage="Total Amount">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTotalAmount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reCurrency" Validation-IsRequired="false" EmptyMessage="Currency"
        ErrorMessage="Currency">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCurrency" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rePaymentMode" Validation-IsRequired="false"
        EmptyMessage="Payment Mode" ErrorMessage="Payment Mode">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPaymentMode" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reAvailableBalance" Validation-IsRequired="false"
        EmptyMessage="Available Balance" ErrorMessage="Available Balance">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAvailableBalance" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reInstructionNo" Validation-IsRequired="false"
        EmptyMessage="Enter Instruction No." ErrorMessage="Enter Instruction No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstructionNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reInstrumentNo" Validation-IsRequired="false"
        EmptyMessage="Enter Instrument No." ErrorMessage="Enter Instrument No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstrumentNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reReferenceNo" Validation-IsRequired="false"
        EmptyMessage="Enter Reference No." ErrorMessage="Enter Reference No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reBaneName" Validation-IsRequired="false" EmptyMessage="Enter Baneficiary Name"
        ErrorMessage="Enter Baneficiary Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBeneName" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reAmount" Validation-IsRequired="false" EmptyMessage="Enter Amount"
        ErrorMessage="Enter Amount">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAmount" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Approve Instruments Re-Printing Queue"
                            CssClass="headingblue"></asp:Label>
                        <br />
                        Note:&nbsp; Fields marked as
                        <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        &nbsp;are required.
                    </td>
                </tr>
                 <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <table align="left" valign="top" style="width: 100%">
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%">
                                    <table align="left" valign="top" style="width: 100%">
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 100%" colspan="4">
                                                <asp:RadioButtonList ID="rbtnlstDateSelection" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem> Creation Date</asp:ListItem>
                                                    <asp:ListItem> Value Date</asp:ListItem>
                                                    <asp:ListItem> Last Print Date</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 25%">
                                                <asp:Label ID="lblDateFrom" runat="server" Text="Date From" CssClass="lbl"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="width: 25%">
                                                <telerik:RadDatePicker ID="raddpCreationFromDate" runat="server" CssClass="txtbox">
                                                    <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                    </Calendar>
                                                    <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                        LabelWidth="40%" type="text" value="">
                                                    </DateInput>
                                                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                </telerik:RadDatePicker>
                                            </td>
                                            <td align="left" valign="top" style="width: 25%">
                                                <asp:Label ID="lblCreationDateTo" runat="server" Text="Date To" CssClass="lbl"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="width: 25%">
                                                <telerik:RadDatePicker ID="raddpCreationToDate" runat="server" CssClass="txtbox">
                                                    <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                    </Calendar>
                                                    <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                        LabelWidth="40%" type="text" value="">
                                                    </DateInput>
                                                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 20%">
                                                <asp:Label ID="lblAccountPaymentNature0" runat="server" Text="Company" CssClass="lbl"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="dropdown">
                                                    <asp:ListItem Value="0">All</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 20%">
                                                <asp:Label ID="lblAccountPaymentNature" runat="server" Text="Account Payment Nature"
                                                    CssClass="lbl"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="ddlAcccountPaymentNature" runat="server" CssClass="dropdown">
                                                    <asp:ListItem Value="0">All</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 20%">
                                                <asp:Label ID="lblBatch" runat="server" Text="Batch" CssClass="lbl"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="ddlBatch" runat="server" CssClass="dropdown">
                                                    <asp:ListItem Value="0">All</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 20%">
                                                <asp:Label ID="lblProductType" runat="server" Text="Product Type" CssClass="lbl"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="ddlProductType" runat="server" CssClass="dropdown">
                                                    <asp:ListItem Value="0">All</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 20%">
                                                <asp:Label ID="lblRemarks" runat="server" Text="Remarks" CssClass="lbl"></asp:Label>
                                                <asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="width: 80%" colspan="3">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="txtbox" Height="69px" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr align="center" valign="top" style="width: 100%">
                                            <td align="center" valign="top" style="width: 100%" colspan="4">
                        <asp:Button ID="btnAllowRePrint" runat="server" Text="Approve Re-Printing" CssClass="btn" 
                                                    Width="172px" OnClientClick ="javascript: return conApproveRePrinting();" />
                        &nbsp;
                        <asp:Button ID="btnAllowRePrint0" runat="server" Text="Cancel Re-Printing" CssClass="btn" OnClientClick ="javascript: return conCancelRePrinting();" />
                        &nbsp;
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" />
                                            </td>
                                        </tr>                                        
                                    </table>
                                </td>
                                <td align="left" valign="top" style="width: 50%">
                                    <table align="left" valign="top" style="width: 100%">
                                        
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 50%">
                                                            <asp:Label ID="lblInstructionNo" runat="server" Text="Instruction No." CssClass="lbl"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="width: 50%">
                                                            <asp:Label ID="lblInstrumentNo" runat="server" Text="Instrument No." CssClass="lbl"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 50%">
                                                            <asp:TextBox ID="txtInstructionNo" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox>
                                            </td>
                                            <td align="left" valign="top" style="width: 50%">
                                                            <asp:TextBox ID="txtInstrumentNo" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 50%">
                                                            <asp:Label ID="lblReferenceNo" runat="server" Text="Reference No." CssClass="lbl"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="width: 50%">
                                                            <asp:TextBox ID="txtReferenceNo" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 50%">
                                                            <asp:Label ID="lblAmount" runat="server" Text="Amount" CssClass="lbl"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="width: 50%">
                                                &nbsp;</td>
                                        </tr>
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 50%">
                                                            <asp:TextBox ID="txtAmount" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox><br />
                                                            <asp:DropDownList ID="ddlAcccountPaymentNature0" runat="server" CssClass="dropdown">
                                                                <asp:ListItem>=</asp:ListItem>
                                                                <asp:ListItem>&gt;</asp:ListItem>
                                                                <asp:ListItem>&lt;</asp:ListItem>
                                                                <asp:ListItem>&gt;=</asp:ListItem>
                                                                <asp:ListItem>&lt;=</asp:ListItem>
                                                                <asp:ListItem>&lt;&gt;</asp:ListItem>
                                                            </asp:DropDownList>
                                            </td>
                                            <td align="left" valign="top" style="width: 50%">
                                                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn" CausesValidation="False" />
                                            </td>
                                        </tr>
                                        </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Label ID="lblNRF" runat="server" Text="No Record Found." CssClass="headingblue"
                            Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <telerik:RadGrid ID="gvAllowRePrintReIssue" runat="server" AllowPaging="True" Width="100%"
                            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                            PageSize="10">
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView>
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Checked="false" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Checked="false" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="InstructionNo" HeaderText="Instruction No." SortExpression="InstructionNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ReferenceNo" HeaderText="Reference No." SortExpression="ReferenceNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BatchNo" HeaderText="Batch No." SortExpression="BatchNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Dated" HeaderText="Creation Date" SortExpression="Dated"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneName" HeaderText="Bene Name" SortExpression="Bane Name">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Amount" HeaderText="Amount" SortExpression="Amount"
                                        HtmlEncode="false" DataFormatString="{0:N0}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PaymentMode" HeaderText="Payment Mode" SortExpression="PaymentMode">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InstrumentNo" HeaderText="Instrument No." SortExpression="InstrumentNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ValueDate" HeaderText="Value Date" SortExpression="ValueDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="LastPrintDate" HeaderText="Last Print Date" SortExpression="LastPrintDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn DataField="View Details">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlDetails" runat="server" ImageUrl="~/images/view.gif" ToolTip="Instruction Details">Detail</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                        </telerik:RadGrid>
                    </td>
                </tr>
                </table>
        </td>
    </tr>
   <%-- <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table width="100%" id="tblApproval" runat="server">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Label ID="Label1" runat="server" Text="Approve Instruments Re-Printing Queue"
                            CssClass="headingblue"></asp:Label>
                        <br />
                        Note:&nbsp; Fields marked as
                        <asp:Label ID="Label10" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        &nbsp;are required.
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Label ID="Label6" runat="server" Text="No Record Found." CssClass="headingblue"
                            Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <telerik:RadGrid ID="gvApproveRePrintReIssue" runat="server" AllowPaging="True" Width="100%"
                            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                            PageSize="10">
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView>
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Checked="true" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Checked="true" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="InstructionNo" HeaderText="Instruction No." SortExpression="InstructionNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ReferenceNo" HeaderText="Reference No." SortExpression="ReferenceNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BatchNo" HeaderText="Batch No." SortExpression="BatchNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Dated" HeaderText="Creation Date" SortExpression="Dated"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneName" HeaderText="Bene Name" SortExpression="Bane Name">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Amount" HeaderText="Amount" SortExpression="Amount"
                                        HtmlEncode="false" DataFormatString="{0:N0}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PaymentMode" HeaderText="Payment Mode" SortExpression="PaymentMode">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InstrumentNo" HeaderText="Instrument No." SortExpression="InstrumentNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ValueDate" HeaderText="Value Date" SortExpression="ValueDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="LastPrintDate" HeaderText="Last Print Date" SortExpression="LastPrintDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn DataField="View Details">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlDetails" runat="server" ImageUrl="~/images/view.gif" ToolTip="Instruction Details">Detail</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                        </telerik:RadGrid>
                    </td>
                </tr>
                </table>
        </td>
    </tr>--%>
</table>
