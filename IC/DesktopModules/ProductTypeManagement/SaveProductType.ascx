﻿<%@ Control Language="VB" AutoEventWireup="true" CodeFile="SaveProductType.ascx.vb"
    Inherits="DesktopModules_ProductTypeManagement_SaveProductType" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .style1
    {
        width: 25%;
    }
    .btn
    {
    }
    </style>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }

</script>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    
    function delcon() {
        if (window.confirm("This last record is not deleted, if you want to delete this record you must add another account.") == true) {
            return true;
        }
        else {
            return false;
        }
    }    
</script>
<telerik:RadInputManager ID="RadInputManager1" runat="server"  >
  
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior2" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9]{1,10}$" ErrorMessage="Invalid Product Type Code" 
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtProductTypeCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
     <telerik:TextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="true"
        EmptyMessage="" ErrorMessage="Invalid Product Type Name"  ClearValueOnError="true" >
        <TargetControls>
            <telerik:TargetInput ControlID="txtProductTypeName" />
        </TargetControls>
           </telerik:TextBoxSetting>

    <telerik:RegExpTextBoxSetting BehaviorID="reReversalTransactionType" Validation-IsRequired="true"
        ErrorMessage="Invalid Reversal Transaction Type" 
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReversalTransactionType" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reInstrumentType" Validation-IsRequired="true"
        ErrorMessage="Invalid Instrument Type" 
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstrumentType" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
     <telerik:RegExpTextBoxSetting BehaviorID="reVoucherType" Validation-IsRequired="true"
        ErrorMessage="Invalid Voucher Type" 
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtVoucherType" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>



</telerik:RadInputManager>
<asp:HiddenField ID="hfPreferenceOrder" runat="server" />
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 33%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 17%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblProductTypeCode" runat="server" Text="Product Type Code" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblProductTypeCodeReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblProductTypeName" runat="server" Text="Product Type Name" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblProductTypeNameReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:TextBox ID="txtProductTypeCode" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtProductTypeName" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox>           
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblDisbModes" runat="server" Text="Select Disbursement Mode" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblDisbModesReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIsSundryAllow" runat="server" Text="Is Sundry Allow" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:DropDownList ID="ddlDisbModes" runat="server" CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem>Cheque</asp:ListItem>
                <asp:ListItem Value="DD">Demand Draft</asp:ListItem>
                <asp:ListItem Value="PO">Pay Order</asp:ListItem>
                <asp:ListItem Value="Direct Credit">Fund Transfer</asp:ListItem>
                <asp:ListItem Value="Other Credit">Inter Bank Fund Transfer</asp:ListItem>
                <asp:ListItem>COTC</asp:ListItem>
                <asp:ListItem>Bill Payment</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkSundryAllow" runat="server" Text=" " CssClass="chkBox" AutoPostBack="True" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:RequiredFieldValidator ID="rfvddlCity0" runat="server" ControlToValidate="ddlDisbModes"
                Display="Dynamic" ErrorMessage="Please select Disbursement Mode" InitialValue="0"
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
   <%--    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            
            <table id="tblDefaultTranTypes" runat="server" style="width:100%">
                <tr align="left" style="width: 100%" valign="top">
                    <td align="left" class="style1" valign="top">
                        <asp:Label ID="lblOriginatingTranType" runat="server" CssClass="lbl" Text="Originating Transaction Type"></asp:Label>
                        <asp:Label ID="lblReqOriginatingTranType" runat="server" ForeColor="Red" Text="*"></asp:Label>
                    </td>
                    <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                    <td align="left" style="width: 25%" valign="top">
                        <asp:Label ID="lblRespondingTranType" runat="server" CssClass="lbl" Text="Responding Transaction Type"></asp:Label>
                        <asp:Label ID="lblReqRespondingTranType" runat="server" ForeColor="Red" Text="*"></asp:Label>
                    </td>
                    <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                </tr>
                <tr align="left" style="width: 100%" valign="top">
                    <td align="left" class="style1" valign="top">
                        <asp:TextBox ID="txtOriginatingTranType" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                    </td>
                    <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                    <td align="left" style="width: 25%" valign="top">
                        <asp:TextBox ID="txtRespondingTranType" runat="server" CssClass="txtbox" MaxLength="50"></asp:TextBox>
                    </td>
                    <td align="left" style="width: 25%" valign="top">&nbsp;</td>
                </tr>
                </table>
            
        </td>
       
    </tr>--%>
    
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblAccount" runat="server" Text="Select Payable / Sundry Account"
                CssClass="lbl"></asp:Label>
            <asp:Label ID="lblAccountReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblBank" runat="server" Text="Select Bank" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblBankReq" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:DropDownList ID="ddlAccount" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlBank" runat="server" CssClass="dropdown" AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:RequiredFieldValidator ID="rfvddlAccount" runat="server" ControlToValidate="ddlAccount"
                Display="Dynamic" ErrorMessage="Please Select Payable Account" InitialValue="0"
                SetFocusOnError="True" ValidationGroup="AddAccount"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlBank" runat="server" ControlToValidate="ddlBank"
                Display="Dynamic" ErrorMessage="Please select Bank" InitialValue="0" 
                SetFocusOnError="True" ValidationGroup="AddAccount"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblIsActive" runat="server" Text="Is Active" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIsApproved" runat="server" Text="Is Approved" CssClass="lbl" Visible="False"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:CheckBox ID="chkActive" runat="server" Text=" " CssClass="chkBox" Checked="True" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkApproved" runat="server" Text=" " Visible="False" CssClass="chkBox" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnAddAccount" runat="server" Text="Add Account" CssClass="btn" 
                Width="82px" ValidationGroup="AddAccount" />
            <br />
            <br />
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" colspan="4">
            <telerik:RadGrid ID="gvAccount" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                CellSpacing="0" ShowFooter="True">
                <AlternatingItemStyle CssClass="rgAltRow" />
                <MasterTableView DataKeyNames="DDPayableAccountID,PreferenceOrder,Count">
                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn DataField="BankName" HeaderText="Bank Name" SortExpression="BankName"
                            HeaderStyle-HorizontalAlign="Center">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PayableAccountNumber" HeaderText="AccountNumber"
                            SortExpression="PayableAccountNumber" HeaderStyle-HorizontalAlign="Center">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn HeaderText="Order" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:TextBox ID="txtBankOrder" runat="server" Text='<%#Eval("PreferenceOrder") %>' MaxLength = "2" Width="50"></asp:TextBox>
                                <br /> 
                                <asp:RegularExpressionValidator ID="revPassqord" runat="server" ControlToValidate="txtBankOrder"
                                    Display="Dynamic" ErrorMessage="Only numbers are allowed." ValidationExpression="^\d+$" ValidationGroup="SetAccount"></asp:RegularExpressionValidator><br />
                              
                              <asp:RangeValidator ID="rvFieldMaxOrder" ControlToValidate="txtBankOrder" MinimumValue="1" ValidationGroup="SetAccount"
                                    MaximumValue='<%#Eval("Count") %>' Type="Integer" Enabled="true" ErrorMessage="Field order must be greater than 0 and must be less than or equal to maximum preference order"
                                    runat="server" /><br /> 
                                   <%-- <asp:CompareValidator ID="cmpOrder" runat="server" ControlToValidate="txtBankOrder" ValueToCompare="99" Operator="LessThanEqual"  
                                    ErrorMessage="Field order must be greater than 0 and must be less than or equal to 99" SetFocusOnError="true" Type="Integer"></asp:CompareValidator><br /> --%>

                                <asp:RequiredFieldValidator ID="rfvPreferenceOrder" runat="server" InitialValue="" 
                                    Display="Dynamic" ControlToValidate="txtBankOrder" 
                                    Enabled="true" ErrorMessage="Please enter preference order in numbers only."
                                    SetFocusOnError="true" ValidationGroup="SetAccount"></asp:RequiredFieldValidator>



                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Delete" HeaderStyle-HorizontalAlign ="Center">
                            <ItemTemplate>
                                <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("DDPayableAccountID") %>'
                                    CommandName="del" ImageUrl="~/images/delete.gif" ToolTip="Delete" CausesValidation ="false" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                    <PagerStyle AlwaysVisible="True" />
                </MasterTableView>
                <ItemStyle CssClass="rgRow" />
                <PagerStyle AlwaysVisible="True" />
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" colspan="4">
            <br />
            <asp:Button ID="btnSetBankOrder" runat="server" Text="Set Bank Order" CssClass="btn" 
                Width="101px" ValidationGroup="SetAccount" />
            <br />
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnApproved" runat="server" Text="Approved" CssClass="btn" Visible="False"
                Width="71px" CausesValidation="False" Height="26px" />
            &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" 
                Width="71px" Height="26px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                CssClass="btnCancel" Width="75px" CausesValidation="False" Height="26px"
               />
            &nbsp;
        </td>
    </tr>
</table>
