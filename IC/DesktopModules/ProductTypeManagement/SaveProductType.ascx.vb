﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_ProductTypeManagement_SaveProductType
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ProductTypeCode As String
    Private Action As String
    Private htRights As Hashtable


    Protected Sub DesktopModules_ProductTypeManagement_SaveProductType_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Not ViewState("ProductTypeCode") Is Nothing Then
                ProductTypeCode = ViewState("ProductTypeCode").ToString()
            End If
            If Not Request.QueryString("At") Is Nothing Then
                Action = Request.QueryString("At").ToString()
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing


                ProductTypeCode = Request.QueryString("id").ToString()
                ViewState("ProductTypeCode") = ProductTypeCode.ToString()
                If ProductTypeCode.ToString() = "0" Then
                    Clear()
                    lblIsApproved.Visible = False
                    chkApproved.Visible = False
                    btnApproved.Visible = False
                    lblAccountReq.Visible = False
                    lblAccount.Visible = False
                    ddlAccount.Visible = False
                    lblIsSundryAllow.Visible = False
                    chkSundryAllow.Visible = False
                    lblBank.Visible = False
                    lblBankReq.Visible = False
                    ddlBank.Visible = False
                    btnAddAccount.Visible = False
                    btnSetBankOrder.Visible = False
                    gvAccount.Visible = False
                    lblPageHeader.Text = "Add Product Type"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                    '  btnAddAccount.Visible = CBool(htRights("Add"))
                Else
                    lblIsApproved.Visible = True
                    chkApproved.Visible = True
                    btnApproved.Visible = True
                    lblAccountReq.Visible = False
                    lblAccount.Visible = False
                    ddlAccount.Visible = False
                    rfvddlAccount.Visible = False
                    lblIsSundryAllow.Visible = False
                    chkSundryAllow.Visible = False
                    lblBank.Visible = False
                    lblBankReq.Visible = False
                    ddlBank.Visible = False
                    rfvddlBank.Visible = False
                    btnAddAccount.Visible = False
                    btnSetBankOrder.Visible = False
                    gvAccount.Visible = False
                    lblPageHeader.Text = "Edit Product Type"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    'btnAddAccount.Visible = CBool(htRights("Update"))
                    btnApproved.Visible = CBool(htRights("Approve"))
                    'rfvddlAccount.Enabled = False
                    'rfvddlBank.Enabled = False

                    txtProductTypeCode.ReadOnly = True

                    LoadddlAccount()
                    LoadddlBank()
                    Dim ICProductType As New ICProductType
                    ICProductType.es.Connection.CommandTimeout = 3600
                    If ICProductType.LoadByPrimaryKey(ProductTypeCode) Then
                        txtProductTypeCode.Text = ICProductType.ProductTypeCode.ToString()
                        txtProductTypeName.Text = ICProductType.ProductTypeName.ToString()
                        ddlDisbModes.SelectedValue = ICProductType.DisbursementMode
                        If ddlDisbModes.SelectedValue.ToString() = "DD" Then
                            lblAccount.Visible = True
                            ddlAccount.Visible = True
                            lblBank.Visible = True
                            ddlBank.Visible = True
                            '      LoadAccountsgv(True)
                            LoadddlBank()
                            lblBankReq.Visible = True
                            '   btnAddAccount.Visible = True
                            btnAddAccount.Visible = CBool(htRights("Update"))
                            btnSetBankOrder.Visible = True
                            LoadAccountsgv(True)
                            gvAccount.Visible = True
                            lblAccount.Text = "Select Payable Account"
                        ElseIf ddlDisbModes.SelectedValue.ToString() = "COTC" Then
                            If ICProductType.IsFunded = True Then
                                chkSundryAllow.Checked = ICProductType.IsFunded
                                chkSundryAllow.Visible = True
                                lblIsSundryAllow.Visible = True
                                lblAccountReq.Visible = True
                                lblAccount.Visible = True
                                lblAccount.Text = "Select Sundry Account"
                                ddlAccount.Visible = True
                                If ICProductType.FundedAccountType = "GL" Then
                                    ddlAccount.SelectedValue = ICProductType.FundedAccountType & "-" & ICProductType.FundedAccountNumber & "-" & ICProductType.FundedAccountBranchCode & "-" & ICProductType.FundedAccountCurrency & "-" & "-" & "-" & "-" & "-" & "-"
                                Else
                                    ddlAccount.SelectedValue = ICProductType.FundedAccountType & "-" & ICProductType.FundedAccountNumber & "-" & ICProductType.FundedAccountBranchCode & "-" & ICProductType.FundedAccountCurrency & "-" & "-" & "-" & "-" & "-" & "-"
                                End If
                            Else
                                chkSundryAllow.Visible = True
                                lblIsSundryAllow.Visible = True

                            End If


                        ElseIf ddlDisbModes.SelectedValue.ToString() = "PO" Then



                            lblAccount.Text = "Select Payable Account"
                            lblAccount.Visible = True
                            ddlAccount.Visible = True
                            If ICProductType.PayableAccountType = "GL" Then
                                ddlAccount.SelectedValue = ICProductType.PayableAccountType & "-" & ICProductType.PayableAccountNumber & "-" & ICProductType.PayableAccountBranchCode & "-" & ICProductType.PayableAccountCurrency & "-" & "-" & "-" & "-" & "-" & "-"
                            Else
                                ddlAccount.SelectedValue = ICProductType.PayableAccountType & "-" & ICProductType.PayableAccountNumber & "-" & ICProductType.PayableAccountBranchCode & "-" & ICProductType.PayableAccountCurrency & "-" & "-" & "-" & "-" & "-" & "-"
                            End If

                        ElseIf ddlDisbModes.SelectedValue.ToString() = "Direct Credit" Or ddlDisbModes.SelectedValue.ToString() = "Other Credit" Or ddlDisbModes.SelectedValue.ToString() = "Bill Payment" Then

                            If ICProductType.IsFunded = True Then
                                chkSundryAllow.Checked = ICProductType.IsFunded
                                chkSundryAllow.Visible = True
                                lblIsSundryAllow.Visible = True
                                lblAccountReq.Visible = True
                                lblAccount.Visible = True
                                lblAccount.Text = "Select Sundry Account"
                                ddlAccount.Visible = True
                                If ICProductType.FundedAccountType = "GL" Then
                                    ddlAccount.SelectedValue = ICProductType.FundedAccountType & "-" & ICProductType.FundedAccountNumber & "-" & ICProductType.FundedAccountBranchCode & "-" & ICProductType.FundedAccountCurrency & "-" & "-" & "-" & "-" & "-" & "-"
                                Else
                                    ddlAccount.SelectedValue = ICProductType.FundedAccountType & "-" & ICProductType.FundedAccountNumber & "-" & ICProductType.FundedAccountBranchCode & "-" & ICProductType.FundedAccountCurrency & "-" & "-" & "-" & "-" & "-" & "-"
                                End If
                            Else
                                chkSundryAllow.Visible = True
                                lblIsSundryAllow.Visible = True

                            End If


                        Else
                            lblAccount.Visible = False
                            ddlAccount.Visible = False
                            ddlAccount.Enabled = False
                            'rfvddlAccount.Visible = False
                            'rfvddlBank.Visible = False
                            lblAccountReq.Visible = False
                            lblBank.Visible = False
                            lblBankReq.Visible = False
                            ddlBank.Visible = False
                            btnAddAccount.Visible = False
                            btnSetBankOrder.Visible = False
                            gvAccount.Visible = False

                        End If

                        If ICProductType.IsApproved = True Then
                            chkApproved.Enabled = False
                            btnApproved.Visible = False
                        End If

                        chkActive.Checked = ICProductType.IsActive
                        chkApproved.Checked = ICProductType.IsApproved
                    End If
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Product Type Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        txtProductTypeCode.Text = ""
        txtProductTypeName.Text = ""
        ddlDisbModes.ClearSelection()
        chkApproved.Checked = False
        chkSundryAllow.Visible = False
        lblIsSundryAllow.Visible = False
        chkActive.Checked = True
        lblAccount.Visible = False
        lblAccountReq.Visible = False
        lblBank.Visible = False
        lblBankReq.Visible = False
        ddlAccount.Visible = False
        ddlBank.Visible = False
        btnAddAccount.Visible = False
        btnSetBankOrder.Visible = False
        gvAccount.Visible = False
        rfvddlAccount.Visible = False
        rfvddlBank.Visible = False
        ViewState("ProductTypeCode") = "0"
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Page.Validate()

        'For Each validator As BaseValidator In Page.Validators
        '    If validator.Enabled = True And validator.IsValid = False Then
        '        Dim clientID As String
        '        clientID = validator.ClientID

        '    End If
        'Next

        If Page.IsValid Then
            Try
                Dim ICPType As New ICProductType
                ICPType.es.Connection.CommandTimeout = 3600
                ICPType.ProductTypeCode = txtProductTypeCode.Text.ToString()
                ICPType.ProductTypeName = txtProductTypeName.Text.ToString()
                ICPType.DisbursementMode = ddlDisbModes.SelectedValue.ToString()
                ICPType.IsSundaryAllow = chkSundryAllow.Checked
                ICPType.IsActive = chkActive.Checked


                If ddlDisbModes.SelectedValue.ToString() = "Direct Credit" Or ddlDisbModes.SelectedValue.ToString() = "Other Credit" Or ddlDisbModes.SelectedValue.ToString = "Bill Payment" Then
                    If chkSundryAllow.Checked = True And ddlAccount.SelectedValue.ToString <> "0" Then
                        If ddlAccount.SelectedValue.Split("-")(0).ToString() = "GL" Then
                            ICPType.FundedAccountNumber = ddlAccount.SelectedValue.Split("-")(1).ToString()
                            ICPType.FundedAccountBranchCode = ddlAccount.SelectedValue.Split("-")(2).ToString()
                            ICPType.FundedAccountCurrency = ddlAccount.SelectedValue.Split("-")(3).ToString()
                            ICPType.FundedAccountType = ddlAccount.SelectedValue.Split("-")(0).ToString()
                            ICPType.FundedAccountSeqNo = ddlAccount.SelectedValue.Split("-")(4).ToString()
                            ICPType.FundedAccountClientNo = ddlAccount.SelectedValue.Split("-")(5).ToString()
                            ICPType.FundedAccountProfitCentre = ddlAccount.SelectedValue.Split("-")(6).ToString()
                            ICPType.IsFunded = chkSundryAllow.Checked
                            ICPType.PayableAccountNumber = Nothing
                            ICPType.PayableAccountBranchCode = Nothing
                            ICPType.PayableAccountCurrency = Nothing
                            rfvddlBank.Enabled = False
                        Else
                            ICPType.FundedAccountNumber = ddlAccount.SelectedValue.Split("-")(1).ToString()
                            ICPType.FundedAccountBranchCode = ddlAccount.SelectedValue.Split("-")(2).ToString()
                            ICPType.FundedAccountCurrency = ddlAccount.SelectedValue.Split("-")(3).ToString()
                            ICPType.FundedAccountType = ddlAccount.SelectedValue.Split("-")(0).ToString()
                            ICPType.FundedAccountSeqNo = Nothing
                            ICPType.FundedAccountClientNo = Nothing
                            ICPType.FundedAccountProfitCentre = Nothing
                            ICPType.IsFunded = chkSundryAllow.Checked
                            ICPType.PayableAccountNumber = Nothing
                            ICPType.PayableAccountBranchCode = Nothing
                            ICPType.PayableAccountCurrency = Nothing
                            rfvddlBank.Enabled = False
                        End If
                    ElseIf chkSundryAllow.Checked = True And ddlAccount.SelectedValue.ToString = "0" Then
                        UIUtilities.ShowDialog(Me, "Save Product Type", "Please select account.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    Else
                        ICPType.FundedAccountNumber = Nothing
                        ICPType.FundedAccountBranchCode = Nothing
                        ICPType.FundedAccountCurrency = Nothing

                        ICPType.PayableAccountNumber = Nothing
                        ICPType.PayableAccountBranchCode = Nothing
                        ICPType.PayableAccountCurrency = Nothing
                        ICPType.IsFunded = chkSundryAllow.Checked
                        rfvddlBank.Enabled = False
                    End If
                ElseIf ddlDisbModes.SelectedValue.ToString() = "COTC" Then

                    ICPType.FundedAccountNumber = Nothing
                    ICPType.FundedAccountBranchCode = Nothing
                    ICPType.FundedAccountCurrency = Nothing
                    ICPType.FundedAccountType = Nothing
                    ICPType.FundedAccountSeqNo = Nothing
                    ICPType.FundedAccountClientNo = Nothing
                    ICPType.FundedAccountProfitCentre = Nothing


                    If chkSundryAllow.Checked = True And ddlAccount.SelectedValue.ToString = "0" Then
                        UIUtilities.ShowDialog(Me, "Save Product Type", "Please select account.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    ElseIf chkSundryAllow.Checked = True And ddlAccount.SelectedValue.ToString <> "0" Then
                        If ddlAccount.SelectedValue.Split("-")(0).ToString() = "GL" Then
                            ICPType.FundedAccountNumber = ddlAccount.SelectedValue.Split("-")(1).ToString()
                            ICPType.FundedAccountBranchCode = ddlAccount.SelectedValue.Split("-")(2).ToString()
                            ICPType.FundedAccountCurrency = ddlAccount.SelectedValue.Split("-")(3).ToString()
                            ICPType.FundedAccountType = ddlAccount.SelectedValue.Split("-")(0).ToString()
                            ICPType.FundedAccountSeqNo = ddlAccount.SelectedValue.Split("-")(4).ToString()
                            ICPType.FundedAccountClientNo = ddlAccount.SelectedValue.Split("-")(5).ToString()
                            ICPType.FundedAccountProfitCentre = ddlAccount.SelectedValue.Split("-")(6).ToString()
                            ICPType.IsFunded = chkSundryAllow.Checked
                            ICPType.PayableAccountNumber = Nothing
                            ICPType.PayableAccountBranchCode = Nothing
                            ICPType.PayableAccountCurrency = Nothing
                            rfvddlBank.Enabled = False
                        Else
                            ICPType.FundedAccountNumber = ddlAccount.SelectedValue.Split("-")(1).ToString()
                            ICPType.FundedAccountBranchCode = ddlAccount.SelectedValue.Split("-")(2).ToString()
                            ICPType.FundedAccountCurrency = ddlAccount.SelectedValue.Split("-")(3).ToString()
                            ICPType.FundedAccountType = ddlAccount.SelectedValue.Split("-")(0).ToString()
                            ICPType.FundedAccountSeqNo = Nothing
                            ICPType.FundedAccountClientNo = Nothing
                            ICPType.FundedAccountProfitCentre = Nothing
                            ICPType.IsFunded = chkSundryAllow.Checked
                            ICPType.PayableAccountNumber = Nothing
                            ICPType.PayableAccountBranchCode = Nothing
                            ICPType.PayableAccountCurrency = Nothing
                            rfvddlBank.Enabled = False
                        End If
                    End If

                ElseIf ddlDisbModes.SelectedValue.ToString() = "DD" Then

                    If ddlAccount.SelectedValue = "0" Or ddlBank.SelectedValue = "0" Then
                        ICPType.PayableAccountNumber = Nothing
                        ICPType.PayableAccountBranchCode = Nothing
                        ICPType.PayableAccountCurrency = Nothing
                        ICPType.IsFunded = True
                        ICPType.NonPrincipalBankCode = Nothing
                        ICPType.FundedAccountNumber = Nothing
                        ICPType.FundedAccountBranchCode = Nothing
                        ICPType.FundedAccountCurrency = Nothing
                    Else
                        If ddlAccount.SelectedValue.Split("-")(0).ToString() = "GL" Then
                            ICPType.PayableAccountType = ddlAccount.SelectedValue.Split("-")(0).ToString()
                            ICPType.PayableAccountNumber = ddlAccount.SelectedValue.Split("-")(1).ToString()
                            ICPType.PayableAccountBranchCode = ddlAccount.SelectedValue.Split("-")(2).ToString()
                            ICPType.PayableAccountCurrency = ddlAccount.SelectedValue.Split("-")(3).ToString()
                            ICPType.PayableAccountSeqNo = ddlAccount.SelectedValue.Split("-")(4).ToString()
                            ICPType.PayableAccountClientNo = ddlAccount.SelectedValue.Split("-")(5).ToString()
                            ICPType.PayableAccountProfitCentre = ddlAccount.SelectedValue.Split("-")(6).ToString()
                            ICPType.IsFunded = True
                            ICPType.NonPrincipalBankCode = ddlBank.SelectedValue.ToString()
                            ICPType.FundedAccountNumber = Nothing
                            ICPType.FundedAccountBranchCode = Nothing
                            ICPType.FundedAccountCurrency = Nothing
                        Else
                            ICPType.PayableAccountType = ddlAccount.SelectedValue.Split("-")(0).ToString()
                            ICPType.PayableAccountNumber = ddlAccount.SelectedValue.Split("-")(1).ToString()
                            ICPType.PayableAccountBranchCode = ddlAccount.SelectedValue.Split("-")(2).ToString()
                            ICPType.PayableAccountCurrency = ddlAccount.SelectedValue.Split("-")(3).ToString()
                            ICPType.PayableAccountSeqNo = Nothing
                            ICPType.PayableAccountClientNo = Nothing
                            ICPType.PayableAccountProfitCentre = Nothing
                            ICPType.IsFunded = True
                            ICPType.NonPrincipalBankCode = ddlBank.SelectedValue.ToString()
                            ICPType.FundedAccountNumber = Nothing
                            ICPType.FundedAccountBranchCode = Nothing
                            ICPType.FundedAccountCurrency = Nothing
                        End If

                    End If
                ElseIf ddlDisbModes.SelectedValue.ToString() = "PO" Then
                    ''POCancellationTranType is used as cancellation originating tran type
                    ''ReversalRespondingTranType is used as cancellation responding tran type
                    If ddlDisbModes.SelectedValue.ToString() = "PO" And ddlAccount.SelectedValue.ToString() <> "0" Then

                        If ddlAccount.SelectedValue.Split("-")(0).ToString() = "GL" Then
                            ICPType.PayableAccountType = ddlAccount.SelectedValue.Split("-")(0).ToString()
                            ICPType.PayableAccountNumber = ddlAccount.SelectedValue.Split("-")(1).ToString()
                            ICPType.PayableAccountBranchCode = ddlAccount.SelectedValue.Split("-")(2).ToString()
                            ICPType.PayableAccountCurrency = ddlAccount.SelectedValue.Split("-")(3).ToString()
                            ICPType.PayableAccountSeqNo = ddlAccount.SelectedValue.Split("-")(4).ToString()
                            ICPType.PayableAccountClientNo = ddlAccount.SelectedValue.Split("-")(5).ToString()
                            ICPType.PayableAccountProfitCentre = ddlAccount.SelectedValue.Split("-")(6).ToString()
                            ICPType.IsFunded = True
                            ICPType.FundedAccountNumber = Nothing
                            ICPType.FundedAccountBranchCode = Nothing
                            ICPType.FundedAccountCurrency = Nothing
                            rfvddlBank.Enabled = False
                        Else
                            ICPType.PayableAccountType = ddlAccount.SelectedValue.Split("-")(0).ToString()
                            ICPType.PayableAccountNumber = ddlAccount.SelectedValue.Split("-")(1).ToString()
                            ICPType.PayableAccountBranchCode = ddlAccount.SelectedValue.Split("-")(2).ToString()
                            ICPType.PayableAccountCurrency = ddlAccount.SelectedValue.Split("-")(3).ToString()
                            ICPType.PayableAccountSeqNo = Nothing
                            ICPType.PayableAccountClientNo = Nothing
                            ICPType.PayableAccountProfitCentre = Nothing
                            ICPType.IsFunded = True
                            ICPType.FundedAccountNumber = Nothing
                            ICPType.FundedAccountBranchCode = Nothing
                            ICPType.FundedAccountCurrency = Nothing
                            rfvddlBank.Enabled = False
                        End If


                    ElseIf ddlDisbModes.SelectedValue.ToString() = "PO" And ddlAccount.SelectedValue.ToString() = "0" Then
                        UIUtilities.ShowDialog(Me, "Save Product Type", "Please select account.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If


                ElseIf ddlDisbModes.SelectedValue.ToString() = "Cheque" Then



                    ICPType.PayableAccountNumber = Nothing
                    ICPType.PayableAccountBranchCode = Nothing
                    ICPType.PayableAccountCurrency = Nothing

                    ICPType.FundedAccountNumber = Nothing
                    ICPType.FundedAccountBranchCode = Nothing
                    ICPType.FundedAccountCurrency = Nothing

                    ICPType.IsFunded = False
                End If

                If ICPType.DisbursementMode = "DD" Then

                    If gvAccount.Items.Count = 0 Then
                        UIUtilities.ShowDialog(Me, "Product Type", "Atleast one record must be saved in Account.", ICBO.IC.Dialogmessagetype.Warning)
                        Exit Sub
                    End If
                    If CheckTextBoxesDuplicatValues() = True Then
                        UIUtilities.ShowDialog(Me, "Error", "Bank order can not be same.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                If ProductTypeCode = "0" Then
                    ICPType.CreateBy = Me.UserId
                    ICPType.CreateDate = Date.Now
                    ICPType.Creater = Me.UserId
                    ICPType.CreationDate = Date.Now
                    If CheckDuplicateCodeName(ICPType) = False Then
                        ICProductTypeController.AddProductType(ICPType, False, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        UIUtilities.ShowDialog(Me, "Save Product Type", "Product Type added successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Else
                        UIUtilities.ShowDialog(Me, "Save Product Type", "Can not add duplicate Product Type.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If

                Else
                    If CheckDuplicateCodeNameOnUpDate(ICPType) = True Then
                        UIUtilities.ShowDialog(Me, "Save Product Type", "Can not add duplicate Product Type.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    ICPType.CreateBy = Me.UserId
                    ICPType.CreateDate = Date.Now
                    ICProductTypeController.AddProductType(ICPType, True, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    If Action = "Add" Then
                        UIUtilities.ShowDialog(Me, "Save Product Type", "Product Type added successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Else
                        UIUtilities.ShowDialog(Me, "Save Product Type", "Product Type updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    End If
                End If
                Clear()

                SetVisibility()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Private Function CheckDuplicateCodeName(ByVal cProductType As ICProductType) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim ICProdctType As New ICProductType
            Dim collProductType As New ICProductTypeCollection

            ICProdctType.es.Connection.CommandTimeout = 3600
            collProductType.es.Connection.CommandTimeout = 3600

            collProductType.Query.Where(collProductType.Query.ProductTypeCode.ToLower.Trim = cProductType.ProductTypeCode.ToLower.Trim)
            If collProductType.Query.Load() Then
                rslt = True
            End If

            collProductType = New ICProductTypeCollection
            collProductType.Query.Where(collProductType.Query.ProductTypeName.ToLower.Trim = cProductType.ProductTypeName.ToLower.Trim)
            If collProductType.Query.Load() Then
                rslt = True
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
        End Try
    End Function
    Private Function CheckIsInstructionsExistAgainstProductTypeCodeForUpDate(ByVal ProductTypeCodeForUpDate As String) As Boolean
        Dim objICInstructionColl As New ICInstructionCollection
        Dim ICProdctType As New ICProductType
        ICProdctType.es.Connection.CommandTimeout = 3600
        ICProdctType.LoadByPrimaryKey(ProductTypeCode)
        If Not ProductTypeCodeForUpDate.ToLower.Trim = ICProdctType.ProductTypeCode.ToLower.Trim Then
            objICInstructionColl.Query.Where(objICInstructionColl.Query.ProductTypeCode = ICProdctType.ProductTypeCode)
            If objICInstructionColl.Query.Load Then
                Return True
                Exit Function
            Else
                Return False
                Exit Function
            End If
        Else
            Return False
        End If



    End Function
    Private Function CheckDuplicateCodeNameOnUpDate(ByVal cProductType As ICProductType) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim ICProdctType As New ICProductType
            Dim collProductType As New ICProductTypeCollection

            ICProdctType.es.Connection.CommandTimeout = 3600
            collProductType.es.Connection.CommandTimeout = 3600

            ICProdctType.LoadByPrimaryKey(ProductTypeCode)

            If Not cProductType.ProductTypeName.ToLower.Trim = ICProdctType.ProductTypeName.ToLower.Trim Then
                collProductType = New ICProductTypeCollection
                collProductType.Query.Where(collProductType.Query.ProductTypeName.ToLower.Trim = cProductType.ProductTypeName.ToLower.Trim)
                collProductType.Query.Where(collProductType.Query.ProductTypeCode <> cProductType.ProductTypeCode)
                If collProductType.Query.Load() Then
                    rslt = True
                    Return rslt
                    Exit Function
                End If
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
        End Try
    End Function
    Private Sub LoadddlDisbModes()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlDisbModes.Items.Clear()
            ddlDisbModes.Items.Add(lit)
            ddlDisbModes.AppendDataBoundItems = True
            ddlDisbModes.DataSource = ICProductTypeController.GetAllProductTypesActiveAndApprove()
            ddlDisbModes.DataTextField = "ProductTypeName"
            ddlDisbModes.DataValueField = "ProductTypeCode"
            ddlDisbModes.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlAccount()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlAccount.Items.Clear()
            ddlAccount.Items.Add(lit)
            ddlAccount.AppendDataBoundItems = True
            ddlAccount.DataSource = ICProductTypeController.GetAllActiveApprovedBankAccountNew
            ddlAccount.DataTextField = "AccountTitle"
            ddlAccount.DataValueField = "AccountNumber"
            ddlAccount.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

        'Try
        '    Dim lit As New ListItem
        '    Dim objICBankAccount As New ICBankAccounts
        '    Dim collICBankAccount As New ICBankAccountsCollection

        '    lit.Value = "0"
        '    lit.Text = "-- Please Select --"
        '    ddlAccount.Items.Clear()
        '    ddlAccount.Items.Add(lit)
        '    collICBankAccount = ICProductTypeController.GetAllActiveApprovedBankAccountNew
        '    For Each objICBankAccount In collICBankAccount
        '        lit = New ListItem
        '        lit.Value = objICBankAccount.AccountNumber & "|" & objICBankAccount.BranchCode & "|" & objICBankAccount.Currency
        '        lit.Text = objICBankAccount.AccountNumber & " - " & objICBankAccount.AccountTitle
        '        ddlAccount.Items.Add(lit)
        '    Next
        'Catch ex As Exception
        '    ProcessModuleLoadException(Me, ex, False)
        '    UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        'End Try
    End Sub
    Private Sub LoadddlBank()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlBank.Items.Clear()
            ddlBank.Items.Add(lit)
            ddlBank.AppendDataBoundItems = True
            ddlBank.DataSource = ICProductTypeController.GetAllActiveApprovedNonPrincipalDDEnabledAndDDPreferredBank(ProductTypeCode.ToString())
            ddlBank.DataTextField = "BankName"
            ddlBank.DataValueField = "BankCode"
            ddlBank.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            If ddlDisbModes.SelectedValue = "DD" Then
                If Action = "Add" And gvAccount.Items.Count <> 0 Then
                    ' Delete DD Accounts
                    ' Delete Product Type
                    ICDDPayableAccountsController.DeleteAllDDPayableAccountsByProductTypeCode(ProductTypeCode.ToString(), ddlDisbModes.SelectedValue.ToString(), "Cancel", Me.UserId.ToString, Me.UserInfo.Username.ToString())
                    ICProductTypeController.DeleteProductType(ProductTypeCode.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString())
                End If
            End If
            Response.Redirect(NavigateURL(), False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Protected Sub btnApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproved.Click

        Dim cProductType As New ICProductType
        Dim dt As DataTable
        cProductType.es.Connection.CommandTimeout = 3600
        Page.Validate()

        If Page.IsValid Then
            Try
                cProductType.LoadByPrimaryKey(ProductTypeCode.ToString())
                If Me.UserInfo.IsSuperUser = False Then
                    If cProductType.CreateBy = Me.UserId Then
                        UIUtilities.ShowDialog(Me, "Approve Product Type", "Product Type  must be approve by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure)
                        chkApproved.Checked = False
                        Exit Sub
                    End If
                End If

                If chkApproved.Checked = True Then
                    If ddlDisbModes.SelectedValue.ToString = "DD" Then

                        If ICProductTypeController.IsDDPreferedBankIsAddedForDDProductTypeToApprove(txtProductTypeCode.Text.ToString) = False Then
                            UIUtilities.ShowDialog(Me, "Approve Product Type", "Please add dd preferred bank.", ICBO.IC.Dialogmessagetype.Warning)
                            Exit Sub
                        Else
                            ICProductTypeController.ApproveProductType(ProductTypeCode.ToString(), chkApproved.Checked, Me.UserId, Me.UserInfo.Username)
                            UIUtilities.ShowDialog(Me, "Approve Product Type", "Product Type approve.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                            Exit Sub
                        End If
                    End If

                    If ddlDisbModes.SelectedValue.ToString() = "Direct Credit" Or ddlDisbModes.SelectedValue.ToString() = "Other Credit" Or ddlDisbModes.SelectedValue.ToString = "Bill Payment" Then
                        If chkSundryAllow.Checked = True And ddlAccount.SelectedValue.ToString = "0" Then
                            UIUtilities.ShowDialog(Me, "Approve Product Type", "Please select account.", ICBO.IC.Dialogmessagetype.Failure)
                            Exit Sub
                        End If
                    End If

                    If ddlDisbModes.SelectedValue.ToString() = "PO" And ddlAccount.SelectedValue.ToString() = "0" Then
                        UIUtilities.ShowDialog(Me, "Approve Product Type", "Please select account.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If

                    ICProductTypeController.ApproveProductType(ProductTypeCode.ToString(), chkApproved.Checked, Me.UserId, Me.UserInfo.Username)
                    UIUtilities.ShowDialog(Me, "Approve Product Type", "Product Type approve.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Approve Product Type", "Please checked the Approved Check Box.", ICBO.IC.Dialogmessagetype.Warning)
                End If

                UIUtilities.ShowDialog(Me, "Approve Product Type", "Please checked the Approved Check Box.", ICBO.IC.Dialogmessagetype.Warning)


            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Protected Sub ddlDisbModes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDisbModes.SelectedIndexChanged
        SetVisibility()
    End Sub
    Private Sub SetVisibility()
        If ddlDisbModes.SelectedValue.ToString() = "Cheque" Then
            lblIsSundryAllow.Visible = False
            chkSundryAllow.Visible = False
            chkSundryAllow.Enabled = False
            lblAccount.Visible = False
            ddlAccount.Visible = False
            ddlAccount.Enabled = False
            rfvddlAccount.Visible = False
            rfvddlBank.Visible = False
            lblAccountReq.Visible = False
            lblBank.Visible = False
            lblBankReq.Visible = False
            ddlBank.Visible = False
            btnAddAccount.Visible = False
            btnSetBankOrder.Visible = False
            gvAccount.Visible = False

        ElseIf ddlDisbModes.SelectedValue.ToString() = "DD" Then
            LoadddlAccount()
            lblAccount.Visible = True
            ddlAccount.Visible = True
            ddlAccount.Enabled = True
            lblAccount.Text = "Select Payable Account"
            rfvddlAccount.Visible = True
            rfvddlBank.Visible = True
            lblAccountReq.Visible = True
            rfvddlAccount.ErrorMessage = "Select Payable Account"
            lblBank.Visible = True
            lblBankReq.Visible = True
            ddlBank.Visible = True
            lblIsSundryAllow.Visible = False
            chkSundryAllow.Visible = False
            chkSundryAllow.Checked = False
            ' btnAddAccount.Visible = True
            btnAddAccount.Visible = CBool(htRights("Add"))
            LoadddlBank()

        ElseIf ddlDisbModes.SelectedValue.ToString() = "PO" Then
            LoadddlAccount()
            lblAccount.Visible = True
            ddlAccount.Visible = True
            ddlAccount.Enabled = True
            rfvddlAccount.Visible = True
            lblAccountReq.Visible = True
            lblAccount.Text = "Select Payable Account"
            rfvddlAccount.ErrorMessage = "Select Payable Account"

            chkSundryAllow.Visible = False
            lblIsSundryAllow.Visible = False
            lblBank.Visible = False
            lblBankReq.Visible = False
            ddlBank.Visible = False
            btnAddAccount.Visible = False
            btnSetBankOrder.Visible = False
            gvAccount.Visible = False
            rfvddlBank.Visible = False

        ElseIf ddlDisbModes.SelectedValue.ToString() = "Direct Credit" Then
            lblAccount.Visible = False
            ddlAccount.Visible = False
            ddlAccount.Enabled = False
            lblAccountReq.Visible = False
            rfvddlAccount.Visible = False
            chkSundryAllow.Visible = True
            lblIsSundryAllow.Visible = True
            chkSundryAllow.Checked = False
            chkSundryAllow.Enabled = True

            lblBank.Visible = False
            lblBankReq.Visible = False
            ddlBank.Visible = False
            btnAddAccount.Visible = False
            btnSetBankOrder.Visible = False
            gvAccount.Visible = False

        ElseIf ddlDisbModes.SelectedValue.ToString = "COTC" Then
            lblAccount.Visible = False
            ddlAccount.Visible = False
            ddlAccount.Enabled = False
            lblAccountReq.Visible = False
            rfvddlAccount.Visible = False
            chkSundryAllow.Visible = True
            lblIsSundryAllow.Visible = True
            chkSundryAllow.Checked = False
            chkSundryAllow.Enabled = True

            lblBank.Visible = False
            lblBankReq.Visible = False
            ddlBank.Visible = False
            btnAddAccount.Visible = False
            btnSetBankOrder.Visible = False
            gvAccount.Visible = False

        ElseIf ddlDisbModes.SelectedValue.ToString() = "Other Credit" Or ddlDisbModes.SelectedValue.ToString = "Bill Payment" Then
            lblAccount.Visible = False
            ddlAccount.Visible = False
            ddlAccount.Enabled = False
            lblAccountReq.Visible = False
            rfvddlAccount.Visible = False
            rfvddlBank.Visible = False
            chkSundryAllow.Visible = True
            lblIsSundryAllow.Visible = True
            chkSundryAllow.Checked = False
            chkSundryAllow.Enabled = True

            lblBank.Visible = False
            lblBankReq.Visible = False
            ddlBank.Visible = False
            btnAddAccount.Visible = False
            btnSetBankOrder.Visible = False
            gvAccount.Visible = False

        Else
            chkSundryAllow.Visible = False
            lblIsSundryAllow.Visible = False
            lblAccount.Visible = False
            ddlAccount.Visible = False
            ddlAccount.Enabled = False
            rfvddlAccount.Visible = False
            lblAccountReq.Visible = False

            lblBank.Visible = False
            lblBankReq.Visible = False
            ddlBank.Visible = False
            btnAddAccount.Visible = False
            btnSetBankOrder.Visible = False
            gvAccount.Visible = False

        End If
    End Sub

    Protected Sub chkSundryAllow_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSundryAllow.CheckedChanged

        If chkSundryAllow.Checked = True And ddlDisbModes.SelectedValue.ToString() = "Direct Credit" Then
            LoadddlAccount()
            lblAccount.Visible = True
            ddlAccount.Visible = True
            ddlAccount.Enabled = True
            rfvddlAccount.Visible = True
            lblAccountReq.Visible = True
            lblAccount.Text = "Select Sundry Account"
            rfvddlAccount.ErrorMessage = "Select Sundry Account"
        ElseIf chkSundryAllow.Checked = True And ddlDisbModes.SelectedValue.ToString() = "COTC" Then
            LoadddlAccount()
            lblAccount.Visible = True
            ddlAccount.Visible = True
            ddlAccount.Enabled = True
            rfvddlAccount.Visible = True
            lblAccountReq.Visible = True
            lblAccount.Text = "Select Sundry Account"
            rfvddlAccount.ErrorMessage = "Select Sundry Account"

        ElseIf (chkSundryAllow.Checked = True And ddlDisbModes.SelectedValue.ToString() = "Other Credit") Or (chkSundryAllow.Checked = True And ddlDisbModes.SelectedValue.ToString() = "Bill Payment") Then
            LoadddlAccount()
            lblAccount.Visible = True
            ddlAccount.Visible = True
            ddlAccount.Enabled = True
            rfvddlAccount.Visible = True
            lblAccountReq.Visible = True
            lblAccount.Text = "Select Sundry Account"
            rfvddlAccount.ErrorMessage = "Select Sundry Account"

        Else
            lblAccount.Visible = False
            ddlAccount.Visible = False
            ddlAccount.Enabled = False
            rfvddlAccount.Visible = False
            lblAccountReq.Visible = False
        End If
    End Sub

    Protected Sub gvAccount_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvAccount.ItemCommand
        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    If gvAccount.Items.Count = 1 Then
                        Exit Sub



                    Else
                        Dim DDPayableAccounts As New ICDDPayableAccounts
                        DDPayableAccounts.es.Connection.CommandTimeout = 3600

                        DDPayableAccounts.LoadByPrimaryKey(e.CommandArgument.ToString())
                        ICDDPayableAccountsController.DeleteDDPayableAccounts(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        UIUtilities.ShowDialog(Me, "Delete DDPayable Account", "DDPayable Account deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                        LoadAccountsgv(True)
                        LoadddlBank()
                        'End If
                    End If
                End If
            End If

        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Deleted Product Type", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadAccountsgv(ByVal IsBind As Boolean)
        Try
            ICDDPayableAccountsController.GetAccountsgv(gvAccount.PageSize, gvAccount, IsBind, ProductTypeCode.ToString())

            If gvAccount.Items.Count > 0 Then
                gvAccount.Visible = True
                btnSetBankOrder.Visible = True

            Else
                gvAccount.Visible = False
                btnSetBankOrder.Visible = False

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Protected Sub btnAddAccount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddAccount.Click
        Page.Validate()

        If Page.IsValid Then
            Try
                Dim DDPayableAccounts As New ICDDPayableAccounts

                If ProductTypeCode.ToString() = "0" Then
                    Dim ICPType As New ICProductType
                    ICPType.es.Connection.CommandTimeout = 3600
                    ICPType.ProductTypeCode = txtProductTypeCode.Text.ToString()
                    ICPType.ProductTypeName = txtProductTypeName.Text.ToString()
                    ICPType.DisbursementMode = ddlDisbModes.SelectedValue.ToString()
                    ICPType.IsActive = chkActive.Checked
                    ICPType.IsFunded = True
                    ICPType.FundedAccountNumber = Nothing
                    ICPType.CreateBy = Me.UserId
                    ICPType.CreateDate = Date.Now

                    If txtProductTypeCode.Text.ToString = "" Then
                        UIUtilities.ShowDialog(Me, "Save Product Type", "Please enter Product Type Code.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf txtProductTypeName.Text.ToString = "" Then
                        UIUtilities.ShowDialog(Me, "Save Product Type", "Please enter Product Type Name.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                    DDPayableAccounts.ProductTypeCode = ProductTypeCode.ToString()

                    DDPayableAccounts.ProductTypeCode = txtProductTypeCode.Text.ToString()
                    DDPayableAccounts.BankCode = ddlBank.SelectedValue.ToString()
                    If ddlAccount.SelectedValue.Split("-")(0).ToString() = "GL" Then
                        DDPayableAccounts.PayableAccountType = ddlAccount.SelectedValue.Split("-")(0).ToString()
                        DDPayableAccounts.PayableAccountNumber = ddlAccount.SelectedValue.Split("-")(1).ToString()
                        DDPayableAccounts.PayableAccountBranchCode = ddlAccount.SelectedValue.Split("-")(2).ToString()
                        DDPayableAccounts.PayableAccountCurrency = ddlAccount.SelectedValue.Split("-")(3).ToString()
                        DDPayableAccounts.PayableAccountSeqNo = ddlAccount.SelectedValue.Split("-")(4).ToString()
                        DDPayableAccounts.PayableAccountClientNo = ddlAccount.SelectedValue.Split("-")(5).ToString()
                        DDPayableAccounts.PayableAccountProfitCentre = ddlAccount.SelectedValue.Split("-")(6).ToString()
                    Else
                        DDPayableAccounts.PayableAccountType = ddlAccount.SelectedValue.Split("-")(0).ToString()
                        DDPayableAccounts.PayableAccountNumber = ddlAccount.SelectedValue.Split("-")(1).ToString()
                        DDPayableAccounts.PayableAccountBranchCode = ddlAccount.SelectedValue.Split("-")(2).ToString()
                        DDPayableAccounts.PayableAccountCurrency = ddlAccount.SelectedValue.Split("-")(3).ToString()
                        DDPayableAccounts.PayableAccountSeqNo = Nothing
                        DDPayableAccounts.PayableAccountClientNo = Nothing
                        DDPayableAccounts.PayableAccountProfitCentre = Nothing
                    End If
                    DDPayableAccounts.PreferenceOrder = ICDDPayableAccountsController.GetDDPayableAccountPreferenceOrder(ProductTypeCode.ToString())

                    If ddlDisbModes.SelectedValue.ToString() = "DD" Then
                        ICPType.NonPrincipalBankCode = ddlBank.SelectedValue.ToString()
                    End If

                    If CheckDuplicateCodeName(ICPType) = False Then
                        ProductTypeCode = ICProductTypeController.AddProductType(ICPType, False, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        ViewState("ProductTypeCode") = ProductTypeCode.ToString()

                    Else
                        UIUtilities.ShowDialog(Me, "Save Product Type", "Can not add duplicate Product Type.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If

                    If CheckDuplicateDDPayableAccountAndBank(DDPayableAccounts) = False Then
                        ICDDPayableAccountsController.AddDDPayableAccounts(DDPayableAccounts, Me.UserId, Me.UserInfo.Username)
                        LoadAccountsgv(True)
                        LoadddlAccount()
                        LoadddlBank()
                    Else
                        UIUtilities.ShowDialog(Me, "Save DDPayable Accounts", "Can not add duplicate DDPayable Accounts.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If

                Else
                    DDPayableAccounts.ProductTypeCode = ProductTypeCode.ToString()
                    DDPayableAccounts.BankCode = ddlBank.SelectedValue.ToString()
                    If ddlAccount.SelectedValue.Split("-")(0).ToString() = "GL" Then
                        DDPayableAccounts.PayableAccountType = ddlAccount.SelectedValue.Split("-")(0).ToString()
                        DDPayableAccounts.PayableAccountNumber = ddlAccount.SelectedValue.Split("-")(1).ToString()
                        DDPayableAccounts.PayableAccountBranchCode = ddlAccount.SelectedValue.Split("-")(2).ToString()
                        DDPayableAccounts.PayableAccountCurrency = ddlAccount.SelectedValue.Split("-")(3).ToString()
                        DDPayableAccounts.PayableAccountSeqNo = ddlAccount.SelectedValue.Split("-")(4).ToString()
                        DDPayableAccounts.PayableAccountClientNo = ddlAccount.SelectedValue.Split("-")(5).ToString()
                        DDPayableAccounts.PayableAccountProfitCentre = ddlAccount.SelectedValue.Split("-")(6).ToString()
                    Else
                        DDPayableAccounts.PayableAccountType = ddlAccount.SelectedValue.Split("-")(0).ToString()
                        DDPayableAccounts.PayableAccountNumber = ddlAccount.SelectedValue.Split("-")(1).ToString()
                        DDPayableAccounts.PayableAccountBranchCode = ddlAccount.SelectedValue.Split("-")(2).ToString()
                        DDPayableAccounts.PayableAccountCurrency = ddlAccount.SelectedValue.Split("-")(3).ToString()
                        DDPayableAccounts.PayableAccountSeqNo = Nothing
                        DDPayableAccounts.PayableAccountClientNo = Nothing
                        DDPayableAccounts.PayableAccountProfitCentre = Nothing
                    End If
                    DDPayableAccounts.PreferenceOrder = ICDDPayableAccountsController.GetDDPayableAccountPreferenceOrder(ProductTypeCode.ToString())

                    If CheckDuplicateDDPayableAccountAndBank(DDPayableAccounts) = False Then


                        ICDDPayableAccountsController.AddDDPayableAccounts(DDPayableAccounts, Me.UserId, Me.UserInfo.Username)
                        UIUtilities.ShowDialog(Me, "Save DDPayable Accounts", "Payable Accounts added successfully.", ICBO.IC.Dialogmessagetype.Success)
                        LoadAccountsgv(True)
                        LoadddlAccount()
                        LoadddlBank()
                    Else
                        UIUtilities.ShowDialog(Me, "Save DDPayable Accounts", "Can not add duplicate DDPayable Accounts.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If

                    'Clear()
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub gvAccount_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAccount.NeedDataSource
        LoadAccountsgv(False)
    End Sub

    Private Function CheckDuplicateDDPayableAccountAndBank(ByVal DDPayableAccounts As ICDDPayableAccounts) As Boolean
        Try
            Dim collDDPayable As New ICDDPayableAccountsCollection
            ' Dim objDDPayable As New ICDDPayableAccounts
            Dim Result As Boolean = False

            collDDPayable.Query.Where(collDDPayable.Query.PayableAccountNumber = DDPayableAccounts.PayableAccountNumber And collDDPayable.Query.PayableAccountBranchCode = DDPayableAccounts.PayableAccountBranchCode And collDDPayable.Query.PayableAccountCurrency = DDPayableAccounts.PayableAccountCurrency)
            collDDPayable.Query.Where(collDDPayable.Query.ProductTypeCode = DDPayableAccounts.ProductTypeCode And collDDPayable.Query.BankCode = DDPayableAccounts.BankCode)
            If collDDPayable.Query.Load Then
                Result = True
            End If

            'collDDPayable.Query.Where(collDDPayable.Query.BankCode = DDPayableAccounts.BankCode.ToString())
            'For Each dr As DataRow In gvAccount.Items
            '    If collDDPayable.Query.Load Then
            '        Result = True
            '    End If
            'Next

            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function


    Protected Sub btnSetBankOrder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSetBankOrder.Click
        Page.Validate()

        If Page.IsValid Then
            Try
                Dim objICDDPayableAccounts As ICDDPayableAccounts
                Dim DDPAbleAccountsID As String
                Dim FieldOrder As Integer = 0
                Dim PassToChangeOrder As Integer = 0
                Dim FailToChnageOrder As Integer = 0
                Dim txtFieldOrder As TextBox
                If gvAccount.Items.Count > 0 Then
                    For Each gvAccountRow As GridDataItem In gvAccount.Items
                        objICDDPayableAccounts = New ICDDPayableAccounts
                        objICDDPayableAccounts.es.Connection.CommandTimeout = 3600
                        DDPAbleAccountsID = Nothing
                        FieldOrder = 0
                        If objICDDPayableAccounts.LoadByPrimaryKey(gvAccountRow.GetDataKeyValue("DDPayableAccountID")) Then
                            txtFieldOrder = New TextBox
                            txtFieldOrder = DirectCast(gvAccountRow.Cells(2).FindControl("txtBankOrder"), TextBox)
                            FieldOrder = CInt(txtFieldOrder.Text)
                            If Not objICDDPayableAccounts.PreferenceOrder = FieldOrder Then
                                objICDDPayableAccounts.PreferenceOrder = CInt(FieldOrder)
                                objICDDPayableAccounts.Save()
                                PassToChangeOrder = PassToChangeOrder + 1
                            Else
                                FailToChnageOrder = FailToChnageOrder + 1
                                Continue For
                            End If
                        End If
                    Next



                    If CheckTextBoxesDuplicatValues() = True Then
                        UIUtilities.ShowDialog(Me, "Error", "Bank order can not be same.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If

                    LoadAccountsgv(True)



                Else
                    UIUtilities.ShowDialog(Me, "Error", "Please select at least one record to set order", ICBO.IC.Dialogmessagetype.Failure)
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub


    Private Function CheckTextBoxesDuplicatValues() As Boolean
        Dim Result As Boolean = False
        Dim txtBoxToValiDate As TextBox
        Dim txtBoxToCompare As TextBox
        Dim i As Integer = 0
        Dim MatchCount As Integer = 0

        For Each gvAccountRow As GridDataItem In gvAccount.Items
            txtBoxToValiDate = New TextBox
            txtBoxToValiDate = DirectCast(gvAccountRow.Cells(2).FindControl("txtBankOrder"), TextBox)
            For Each gvAccountToCompareRow As GridDataItem In gvAccount.Items
                txtBoxToCompare = New TextBox
                txtBoxToCompare = DirectCast(gvAccountToCompareRow.Cells(2).FindControl("txtBankOrder"), TextBox)
                If txtBoxToValiDate.Text = txtBoxToCompare.Text Then
                    MatchCount = MatchCount + 1
                End If
            Next
            If MatchCount > 1 Then
                Result = True
                Exit For
            End If
            MatchCount = 0
        Next

        Return Result
    End Function

    Protected Sub ddlAccount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAccount.SelectedIndexChanged
        Try
            If ddlDisbModes.SelectedValue.ToString() = "DD" Then
                LoadAccountsgv(True)
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlBank_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBank.SelectedIndexChanged
        Try
            LoadAccountsgv(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAccount_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAccount.ItemDataBound
        Dim btnDelete As ImageButton
        Dim objDDPayableAcc As New ICDDPayableAccounts


        If TypeOf e.Item Is GridDataItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Dim value As Integer = CInt(item.GetDataKeyValue("Count"))

            If value = 1 Then
                btnDelete = New ImageButton
                btnDelete = DirectCast(e.Item.Cells(3).FindControl("IbtnDelete"), ImageButton)
                btnDelete.OnClientClick = Nothing
                btnDelete.Attributes("onclick") = "javascript: return delcon();"
            Else
                btnDelete = New ImageButton
                btnDelete = DirectCast(e.Item.Cells(3).FindControl("IbtnDelete"), ImageButton)
                btnDelete.OnClientClick = Nothing
                btnDelete.Attributes("onclick") = "javascript: return con();"
            End If
        End If

    End Sub
End Class
