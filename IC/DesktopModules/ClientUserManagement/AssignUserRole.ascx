﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AssignUserRole.ascx.vb"
    Inherits="DesktopModules_ClientUserManagement_AssignUserRole" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    
    <telerik:TextBoxSetting BehaviorID="REUserName" Validation-IsRequired="false"
        EmptyMessage="Enter Role Type">
        <TargetControls>
            <telerik:TargetInput ControlID="txtUserName" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="false"
        EmptyMessage="Enter Role Type">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDisplayName" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<style type="text/css">
    .style1
    {
        height: 22px;
    }
    .style3
    {
        width: 100%;
    }
</style>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
    function ValidateChkList(source, arguments) {

        arguments.IsValid = IsCheckBoxChecked() ? true : false;



    }



    

</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        //      alert(grid)
        //        if ((grid.rows.length -1) != 10 ) {
        //        if ((grid.rows.length) >= 12) {
        //            //            alert('More than 10 rows')
        //            for (i = 0; i < grid.rows.length - 1; i++) {
        //                cell = grid.rows[i].cells[CellNo];
        //                for (j = 0; j < cell.childNodes.length - 1; j++) {
        //                    if (cell.childNodes[j].type == "checkbox") {
        //                        cell.childNodes[j].checked = document.getElementById(idOfControllingCheckbox).checked;
        //                    }
        //                }
        //            }
        //        }
        //        else {
        //                     alert(grid.rows.length)
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    //    }
</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="center" valign="top">
                        <table width="100%">
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" colspan="2">
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Assign Role To Client Users" CssClass="headingblue"></asp:Label>
                                    <br />
                                    Note:&nbsp; Fields marked as
                                    <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                    &nbsp;are required.
                                </td>
                                <td align="left" valign="top" colspan="2">
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" colspan="2">
                                </td>
                                <td align="left" valign="top" colspan="2">
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblUserName" runat="server" Text="User ID" CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Label ID="lblDisplayName" runat="server" Text="User Name" CssClass="lbl"></asp:Label>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:TextBox ID="txtUserName" runat="server" CssClass="txtbox" MaxLength="15"
                                        Enabled="False"></asp:TextBox>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:TextBox ID="txtDisplayName" runat="server" CssClass="txtbox" 
                                        MaxLength="15" Enabled="False"></asp:TextBox>
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" colspan="4">
                                    <table class="style3" id="tblPaymentNature" runat="server">
                                        <tr>
                                            <td style="width: 25%">
                                                <asp:Label ID="lblUserRole" runat="server" Text="Select Role" CssClass="lbl"></asp:Label>
                                                <asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="ddlRole" runat="server" CssClass="dropdown">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 25%">
                                                <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="ddlRole"
                                                    CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Role" SetFocusOnError="True"
                                                    ToolTip="Required Field" InitialValue="0" ValidationGroup="A"></asp:RequiredFieldValidator>
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="center" valign="top" colspan="4">
                                    &nbsp;<asp:Button ID="btnSave" 
                                        runat="server" Text="Save" CssClass="btn" ValidationGroup="A" />
                                    &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False" />
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <asp:Label ID="lblNRF" runat="server" Text="No Record Found" Visible="False" Font-Bold="False"
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <telerik:RadGrid ID="gvUserRoles" runat="server" AllowPaging="true" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                            <ClientSettings>
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView DataKeyNames="UserID,RoleID" NoMasterRecordsText="" TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAllUserRoles" runat="server" CssClass="chkBox" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="RoleName" HeaderText="Role Name" SortExpression="RoleName">
                                    </telerik:GridBoundColumn>
                             <telerik:GridCheckBoxColumn HeaderText="Approve" DataField="IsApproved" AllowSorting="false">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridTemplateColumn HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("RoleID") %>'
                                                CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete Role" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnApproveRoles" runat="server" Text="Approve Roles" 
                            CssClass="btn" CausesValidation="False" />
                        &nbsp;<asp:Button ID="btnDeleteRoles" runat="server" Text="Delete Roles" 
                            CssClass="btn" CausesValidation="False" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
