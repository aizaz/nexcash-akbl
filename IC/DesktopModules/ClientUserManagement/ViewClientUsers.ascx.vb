﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_ClientUserManagement_ViewClientUsers
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
#Region "Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnAddUser.Visible = CBool(htRights("Add"))
                LoadddlGroup()
                LoadddlCompanyByGroupCode()
                LoadddlOfficeByCompanyCode()
                LoadgvFixUsers(0, Me.gvClientUsers.PageSize, True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client Users Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
#Region "Button Events"
    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        If Page.IsValid Then
            Try
                LoadgvFixUsers(Me.gvClientUsers.CurrentPageIndex + 1, Me.gvClientUsers.PageSize, True)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Protected Sub btnSaveUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddUser.Click
        Page.Validate()

        If Page.IsValid Then

            Response.Redirect(NavigateURL("SaveClientUser", "&mid=" & Me.ModuleId & "&id=0"), False)

        End If

    End Sub

#End Region
#Region "Grid View Events"
    Private Sub LoadgvFixUsers(ByVal PageIndex As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean)
        Try
            Dim UserName, DisplayName, Email, OfficeCode, UserType, CompanyCode, GroupCode As String

            If Not txtUserName.Text.ToString() = "" And Not txtUserName.Text = "Enter User Name" Then
                UserName = txtUserName.Text.ToString()
            Else
                UserName = ""
            End If
            If Not txtDisplayName.Text.ToString() = "" And Not txtDisplayName.Text = "Enter Display Name" Then
                DisplayName = txtDisplayName.Text.ToString()
            Else
                DisplayName = ""

            End If
            If Not txtEmailAddress.Text.ToString() = "" And Not txtEmailAddress.Text = "Enter Email Address" Then
                Email = txtEmailAddress.Text.ToString()
            Else
                Email = ""
            End If
            If ddlGroup.SelectedValue.ToString <> "0" Then
                GroupCode = ddlGroup.SelectedValue.ToString()
            Else
                'UIUtilities.ShowDialog(Me, "Error", "Please select office/branch", ICBO.IC.Dialogmessagetype.Failure)
                'Exit Sub
                GroupCode = "0"
            End If
            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString()
            Else
                'UIUtilities.ShowDialog(Me, "Error", "Please select office/branch", ICBO.IC.Dialogmessagetype.Failure)
                'Exit Sub
                CompanyCode = "0"
            End If
            If ddlOffice.SelectedValue.ToString <> "0" Then
                OfficeCode = ddlOffice.SelectedValue.ToString()
            Else
                'UIUtilities.ShowDialog(Me, "Error", "Please select office/branch", ICBO.IC.Dialogmessagetype.Failure)
                'Exit Sub
                OfficeCode = "0"
            End If
            UserType = "Client User"

            ICUserController.GetUserByOfficeCode(OfficeCode, UserType, UserName, DisplayName, Email, PageIndex, PageSize, Me.gvClientUsers, DoDataBind, GroupCode, CompanyCode)
            If gvClientUsers.Items.Count > 0 Then
                lblError.Visible = False
                gvClientUsers.Visible = True
                btnDeleteUsers.Visible = CBool(htRights("Delete"))
                btnApproveUsers.Visible = CBool(htRights("Approve"))
                gvClientUsers.Columns(9).Visible = CBool(htRights("Delete"))
                gvClientUsers.Columns(10).Visible = CBool(htRights("Tag Role"))
            Else
                lblError.Visible = True
                gvClientUsers.Visible = False
                btnDeleteUsers.Visible = False
                btnApproveUsers.Visible = False
            End If

           
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub

    Protected Sub gvClientUsers_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvClientUsers.ItemCreated
       Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvClientUsers.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvClientUsers_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvClientUsers.NeedDataSource
        Try
            LoadgvFixUsers(Me.gvClientUsers.CurrentPageIndex + 1, Me.gvClientUsers.PageSize, False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub gvFixUsers_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvClientUsers.PageIndexChanged
        Try
            gvClientUsers.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub gvFixUsers_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvClientUsers.ItemCommand
        Dim userCtrl As New RoleController
        Dim strChk As String()
        Dim IDRoles As Boolean = False

        Try


            If e.CommandName = "del" Then
                If Page.IsValid Then
                    Dim objICUser As New ICUser
                    objICUser.es.Connection.CommandTimeout = 3600
                    Dim iUser As New UserInfo
                    objICUser.LoadByPrimaryKey(e.CommandArgument.ToString())

                    If objICUser.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                        If objICUser.IsApproved Then
                            UIUtilities.ShowDialog(Me, "Delete User", "User is approved and cannot be deleted.", ICBO.IC.Dialogmessagetype.Failure)
                            LoadgvFixUsers(Me.gvClientUsers.CurrentPageIndex + 1, Me.gvClientUsers.PageSize, False)
                        Else
                            iUser = UserController.GetUserById(Me.PortalId, e.CommandArgument.ToString())
                            ICUserController.DeleteICUser(e.CommandArgument.ToString(), Me.UserId.ToString, Me.UserInfo.Username.ToString, objICUser.UserType.ToString)
                            UserController.RemoveUser(iUser)
                            'iUser = UserController.GetUserById(Me.PortalId, e.CommandArgument.ToString())
                            'ICUserController.DeleteICUser(e.CommandArgument.ToString())
                            'UserController.RemoveUser(iUser)
                            UIUtilities.ShowDialog(Me, "Delete User ", "User deleted successfully.", ICBO.IC.Dialogmessagetype.Success)

                            LoadgvFixUsers(Me.gvClientUsers.CurrentPageIndex + 1, Me.gvClientUsers.PageSize, True)
                        End If
                    End If
                End If
            End If
            If e.CommandName = "Unlock" Then
                Dim user As New UserInfo
                user = UserController.GetUserById(Me.PortalId, e.CommandArgument.ToString())
                UserController.UnLockUser(user)
                DataCache.RemoveCache("MembershipUser_" & user.Username.ToString())
                UIUtilities.ShowDialog(Me, "Save User", "User Unlocked Successfully.", ICBO.IC.Dialogmessagetype.Success)
                LoadgvFixUsers(Me.gvClientUsers.CurrentPageIndex + 1, Me.gvClientUsers.PageSize, True)
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "User : ", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region
#Region "Drop Down events"

    Protected Sub ddlOffice_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOffice.SelectedIndexChanged
        Try

            LoadgvFixUsers(Me.gvClientUsers.CurrentPageIndex + 1, Me.gvClientUsers.PageSize, True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
#Region "Other Functions/Routines"
   
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            ' str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompanyByGroupCode()
            LoadddlOfficeByCompanyCode()
            LoadgvFixUsers(Me.gvClientUsers.CurrentPageIndex + 1, Me.gvClientUsers.PageSize, True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadddlOfficeByCompanyCode()
            LoadgvFixUsers(Me.gvClientUsers.CurrentPageIndex + 1, Me.gvClientUsers.PageSize, True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCompanyByGroupCode()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlOfficeByCompanyCode()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlOffice.Items.Clear()
            ddlOffice.Items.Add(lit)
            ddlOffice.AppendDataBoundItems = True
            ddlOffice.DataSource = ICOfficeController.GetOfficeByCompanyCodeActiveAndApproveForUserCreation(ddlCompany.SelectedValue.ToString)
            ddlOffice.DataTextField = "OfficeName"
            ddlOffice.DataValueField = "OfficeID"
            ddlOffice.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
    Protected Sub btnDeleteUsers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteUsers.Click
        Dim rowGVBankUsers As GridDataItem
        Dim chkSelect As CheckBox
        Dim UserID As String = ""
        Dim PassToDeleteCount As Integer = 0
        Dim FailToDeleteCount As Integer = 0
        Dim userCtrl As New RoleController
        Dim arrLst As New ArrayList
        Dim iUser As UserInfo



        If CheckgvBankUserSelected() = False Then
            UIUtilities.ShowDialog(Me, "Delete Client User", "Please select atleast one user.", ICBO.IC.Dialogmessagetype.Failure)
            Exit Sub
        End If
        Try
            For Each rowGVClientUsers In gvClientUsers.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVClientUsers.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    UserID = rowGVClientUsers.GetDataKeyValue("UserID").ToString
                    Dim objICClientUser As New ICUser
                    objICClientUser.es.Connection.CommandTimeout = 3600
                    If objICClientUser.LoadByPrimaryKey(UserID.ToString()) Then
                        If objICClientUser.IsApproved = True Then
                            FailToDeleteCount = FailToDeleteCount + 1
                        Else
                            Try
                                iUser = New UserInfo
                                iUser = UserController.GetUserById(Me.PortalId, UserID.ToString)
                                ICUserController.DeleteICUser(objICClientUser.UserID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, objICClientUser.UserType.ToString)
                                UserController.RemoveUser(iUser)
                                PassToDeleteCount = PassToDeleteCount + 1
                            Catch ex As Exception
                                'If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                FailToDeleteCount = FailToDeleteCount + 1
                                Continue For
                                'End If
                            End Try
                        End If
                    End If
                End If

            Next

            If Not PassToDeleteCount = 0 And Not FailToDeleteCount = 0 Then
                LoadgvFixUsers(Me.gvClientUsers.CurrentPageIndex + 1, Me.gvClientUsers.PageSize, True)
                UIUtilities.ShowDialog(Me, "Delete Client User", "[ " & PassToDeleteCount.ToString & " ] Client user(s) deleted successfuly.<br /> [ " & FailToDeleteCount.ToString & " ] Client user(s) can not be deleted due to following reason(s): <br /> 1. Client user is approved <br /> 2. Please delete associated record(s) first", ICBO.IC.Dialogmessagetype.Failure)

                Exit Sub
            ElseIf Not PassToDeleteCount = 0 And FailToDeleteCount = 0 Then
                LoadgvFixUsers(Me.gvClientUsers.CurrentPageIndex + 1, Me.gvClientUsers.PageSize, True)
                UIUtilities.ShowDialog(Me, "Delete Client User", PassToDeleteCount.ToString & " Client user(s) deleted successfuly.", ICBO.IC.Dialogmessagetype.Success)

                Exit Sub
            ElseIf Not FailToDeleteCount = 0 And PassToDeleteCount = 0 Then
                LoadgvFixUsers(Me.gvClientUsers.CurrentPageIndex + 1, Me.gvClientUsers.PageSize, True)
                UIUtilities.ShowDialog(Me, "Delete Client User", "[ " & FailToDeleteCount.ToString & " ] Client user(s) can not be deleted due to following reasons: <br />1. Client user is approved<br /> 2. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)

                Exit Sub
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Function CheckgvBankUserSelected() As Boolean
        Try
            Dim rowGVICUsers As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICRoles In gvClientUsers.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnApproveUsers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveUsers.Click
        If Page.IsValid Then
            Try
                Dim rowGVClientUser As GridDataItem
                Dim chkSelect As CheckBox
                Dim UserID As String = ""
                Dim PassToApproveCount As Integer = 0
                Dim FailToApproveCount As Integer = 0

                If CheckgvBankUserSelected() = False Then
                    UIUtilities.ShowDialog(Me, "Save Client User", "Please select atleast one user.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVClientUser In gvClientUsers.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVClientUser.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        UserID = rowGVClientUser.GetDataKeyValue("UserID").ToString
                        Dim objICClientUser As New ICUser
                        objICClientUser.es.Connection.CommandTimeout = 3600
                        objICClientUser.LoadByPrimaryKey(UserID)
                        If Me.UserInfo.IsSuperUser = False Then
                            If objICClientUser.IsApproved = True Or objICClientUser.CreateBy = Me.UserId Then
                                FailToApproveCount = FailToApproveCount + 1
                                Continue For
                            Else
                                If objICClientUser.IsActive = True Then

                                    Try

                                        ICUserController.ApproveUser(objICClientUser.UserID, Me.UserId.ToString, True, Me.UserInfo.Username.ToString, objICClientUser.UserType.ToString)
                                        AuthorizeUser(objICClientUser.UserID.ToString)
                                        PassToApproveCount = PassToApproveCount + 1
                                    Catch ex As Exception

                                        FailToApproveCount = FailToApproveCount + 1
                                        Continue For

                                    End Try
                                Else
                                    FailToApproveCount = FailToApproveCount + 1
                                    Continue For

                                End If

                            End If

                        Else
                            If objICClientUser.IsApproved = True Then
                                FailToApproveCount = FailToApproveCount + 1
                            Else

                                If objICClientUser.IsActive = True Then
                                    Try
                                        ICUserController.ApproveUser(objICClientUser.UserID, Me.UserId.ToString, True, Me.UserInfo.Username.ToString, objICClientUser.UserType.ToString)
                                        AuthorizeUser(objICClientUser.UserID.ToString)
                                        PassToApproveCount = PassToApproveCount + 1
                                    Catch ex As Exception

                                        FailToApproveCount = FailToApproveCount + 1
                                        Continue For

                                    End Try

                                Else
                                    FailToApproveCount = FailToApproveCount + 1
                                    Continue For

                                End If

                            End If
                        End If

                    End If
                Next

                If Not PassToApproveCount = 0 And Not FailToApproveCount = 0 Then
                    LoadgvFixUsers(Me.gvClientUsers.CurrentPageIndex + 1, Me.gvClientUsers.PageSize, True)
                    UIUtilities.ShowDialog(Me, "Save Client User", "[ " & PassToApproveCount.ToString & " ] Client user(s) approved successfully.<br /> [ " & FailToApproveCount.ToString & " ] Client user(s) can not be approved due to following reasons: <br /> 1. User is already approved<br /> 2. User must be approved by other than maker<br />3. User must be active", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not PassToApproveCount = 0 And FailToApproveCount = 0 Then
                    LoadgvFixUsers(Me.gvClientUsers.CurrentPageIndex + 1, Me.gvClientUsers.PageSize, True)
                    UIUtilities.ShowDialog(Me, "Save Client User", PassToApproveCount.ToString & " Client user(s) approved successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Exit Sub
                ElseIf Not FailToApproveCount = 0 And PassToApproveCount = 0 Then
                    LoadgvFixUsers(Me.gvClientUsers.CurrentPageIndex + 1, Me.gvClientUsers.PageSize, True)
                    UIUtilities.ShowDialog(Me, "Save Client User", "[ " & FailToApproveCount.ToString & " ] Client user(s) can not be approved due to following reasons: <br /> 1. User is already approved<br /> 2. User must be approved by other than maker<br />3. User must be active", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Sub AuthorizeUser(ByVal UserCode As String)
        Dim objICUser As New ICUser()
        objICUser.es.Connection.CommandTimeout = 3600
        Try
            If objICUser.LoadByPrimaryKey(UserCode.ToString()) Then
                Dim user As New UserInfo
                Dim member As New UserMembership
                user = UserController.GetUserById(Me.PortalId, UserCode)
                If objICUser.IsApproved = True And objICUser.IsActive = True Then
                    member.Approved = True
                    user.Membership = member
                    UserController.UpdateUser(Me.PortalId, user)
                Else
                    member.Approved = False
                    user.Membership = member
                    UserController.UpdateUser(Me.PortalId, user)
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvClientUsers_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvClientUsers.ItemDataBound
        Dim i As String = ""
        Try
            Dim chkProcessAll As CheckBox


            Dim userid As String = Nothing
            i = e.Item.RowIndex
            If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
                chkProcessAll = New CheckBox
                chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAllUsers"), CheckBox)
                chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvClientUsers.MasterTableView.ClientID & "','0');"
            End If
            If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then

                Dim user As New UserInfo
                Dim IbtnUnlock As New ImageButton
                IbtnUnlock = DirectCast(e.Item.Cells(11).FindControl("IbtnUnlock"), ImageButton)
                user = UserController.GetUserById(Me.PortalId, IbtnUnlock.CommandArgument.ToString())
                If Not user Is Nothing Then
                    If user.Membership.LockedOut = True Then
                        IbtnUnlock.Visible = user.Membership.LockedOut
                    End If
                End If


            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub gvClientUsers_ExportCellFormatting(sender As Object, e As ExportCellFormattingEventArgs) Handles gvClientUsers.ExportCellFormatting

    End Sub
End Class