﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_ClientUserManagement_AssignUserRole
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private UserCode As String
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Dim dtDa As DataTable
    Private RolID As Integer = 0
    Private RoleType As String = ""
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            UserCode = Request.QueryString("id").ToString()
            RoleType = "Company Role"
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                Dim objICUser As New ICUser
                Dim objUserInfo As New UserInfo
                btnSave.Visible = CBool(htRights("Tag Role"))
               
                If objICUser.LoadByPrimaryKey(UserCode) Then
                    tblPaymentNature.Style.Remove("Display")
                    Dim objICBank As New ICBank
                    objUserInfo = UserController.GetUserById(Me.PortalId, UserCode)
                    txtUserName.Text = objUserInfo.Username
                    txtDisplayName.Text = objUserInfo.DisplayName
                    txtUserName.ReadOnly = True
                    txtDisplayName.ReadOnly = True
                    LoadddlRole(RoleType)
                    SetgvUserRoles(UserCode, 0, gvUserRoles.PageSize, Me.gvUserRoles, True)

                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Client Users Management")
            ViewState("htRights") = htRights
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlRole(ByVal RoleType As String)
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlRole.Items.Clear()
            ddlRole.Items.Add(lit)
            ddlRole.AppendDataBoundItems = True

            ddlRole.DataSource = ICRoleController.GetAllICRolesByRoleTypeActiveAndApprove(RoleType.ToString)
            ddlRole.DataTextField = "RoleName"
            ddlRole.DataValueField = "RoleID"
            ddlRole.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then


            Try
                If ICUserRolesController.CheckRoleAlReadyAssignToUser(UserCode.ToString, ddlRole.SelectedValue.ToString()) = True Then
                    UIUtilities.ShowDialog(Me, "User Roles", "Role already assign.", ICBO.IC.Dialogmessagetype.Warning)
                    LoadddlRole(RoleType)
                    SetgvUserRoles(UserCode, Me.gvUserRoles.CurrentPageIndex + 1, gvUserRoles.PageSize, Me.gvUserRoles, False)
                    Exit Sub
                Else
                    Dim objICUserRoles As New ICUserRoles
                    Dim objICUser As New ICUser
                    Dim i As Integer = 0
                    Dim objUserInfo As New UserInfo
                    Dim objRoleInfo As New RoleInfo
                    Dim roleCtrl As New RoleController

                    objICUserRoles.es.Connection.CommandTimeout = 3600
                    objICUser.es.Connection.CommandTimeout = 3600

                    objICUser.LoadByPrimaryKey(UserCode.ToString)

                    If CheckIsAdminOrSuperUser() = True Then
                        objRoleInfo = roleCtrl.GetRoleByName(Me.PortalId, ddlRole.SelectedItem.ToString())
                        objICUserRoles.RoleID = ddlRole.SelectedValue.ToString()
                        objICUserRoles.UserID = UserCode
                        objICUserRoles.AssignedBy = Me.UserId
                        objICUserRoles.AssignedOn = Date.Now
                        objICUserRoles.Creater = Me.UserId
                        objICUserRoles.CreationDate = Date.Now

                        ICUserRolesController.AddICUserRoles(objICUserRoles, False, Me.UserId.ToString, Me.UserInfo.Username.ToString, objICUserRoles.UpToICUserByUserID.UserType.ToString, objICUserRoles.UpToICRoleByRoleID.RoleType.ToString)
                        UIUtilities.ShowDialog(Me, "User Roles", "Assign role to user successfully.", ICBO.IC.Dialogmessagetype.Success)
                        LoadddlRole(RoleType)
                        SetgvUserRoles(UserCode, Me.gvUserRoles.CurrentPageIndex + 1, gvUserRoles.PageSize, Me.gvUserRoles, True)
                    Else
                        objRoleInfo = roleCtrl.GetRoleByName(Me.PortalId, ddlRole.SelectedItem.ToString())
                        objICUserRoles.RoleID = ddlRole.SelectedValue.ToString()
                        objICUserRoles.UserID = UserCode
                        objICUserRoles.AssignedBy = Me.UserId
                        objICUserRoles.AssignedOn = Date.Now
                        objICUserRoles.Creater = Me.UserId
                        objICUserRoles.CreationDate = Date.Now
                        ICUserRolesController.AddICUserRoles(objICUserRoles, False, Me.UserId.ToString, Me.UserInfo.Username.ToString, objICUserRoles.UpToICUserByUserID.UserType.ToString, objICUserRoles.UpToICRoleByRoleID.RoleType.ToString)
                        UIUtilities.ShowDialog(Me, "User Roles", "Assign role to user successfully.", ICBO.IC.Dialogmessagetype.Success)
                        LoadddlRole(RoleType)
                        SetgvUserRoles(UserCode, Me.gvUserRoles.CurrentPageIndex + 1, gvUserRoles.PageSize, Me.gvUserRoles, True)
                        'End If

                        'LoadddlRole(RoleType)
                        'SetgvUserRoles(UserCode, Me.gvUserRoles.CurrentPageIndex + 1, gvUserRoles.PageSize, Me.gvUserRoles, True)


                    End If
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Private Sub SetgvUserRoles(ByVal UserCode As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
        Try
            ICUserRolesController.GetAssignedUserRolesForRadGrid(UserCode, pagenumber, pagesize, rg, DoDataBind)


            If gvUserRoles.Items.Count > 0 Then
                gvUserRoles.Visible = True
                lblNRF.Visible = False
                gvUserRoles.Columns(3).Visible = CBool(htRights("Delete"))
                btnDeleteRoles.Visible = CBool(htRights("Delete"))
                btnApproveRoles.Visible = CBool(htRights("Approve"))

            Else
                gvUserRoles.Visible = False
                btnDeleteRoles.Visible = False
                btnApproveRoles.Visible = False
                lblNRF.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvUserRoles_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvUserRoles.ItemCommand
        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    Dim objUserInfo As New UserInfo
                    Dim objRoleInfo As New RoleInfo
                    Dim objRoleController As New RoleController
                    Dim objICUserRoles As New ICUserRoles

                    objICUserRoles.es.Connection.CommandTimeout = 3600
                    If objICUserRoles.LoadByPrimaryKey(UserCode, e.CommandArgument.ToString) Then
                        'If objICUserRoles.IsApproved = True Then
                        '    UIUtilities.ShowDialog(Me, "Client User Roles", "Client user role is approved and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
                        '    LoadddlRole(RoleType)
                        '    SetgvUserRoles(UserCode, Me.gvUserRoles.CurrentPageIndex + 1, gvUserRoles.PageSize, Me.gvUserRoles, True)
                        '    Exit Sub
                        'Else
                        objUserInfo = UserController.GetUserById(Me.PortalId, UserCode)
                        objRoleInfo = objRoleController.GetRoleByName(Me.PortalId, objICUserRoles.UpToICRoleByRoleID.RoleName.ToString)
                        RoleController.DeleteUserRole(objUserInfo, objRoleInfo, Me.PortalSettings, False)
                        ICUserRolesController.DeleteUserRoles(objRoleInfo.RoleID, UserCode, Me.UserId.ToString, Me.UserInfo.Username.ToString, objICUserRoles.UpToICUserByUserID.UserType.ToString, objICUserRoles.UpToICRoleByRoleID.RoleType.ToString)
                        ICUserRolesPaymentNatureController.DeleteTaggedAPNByRoleIDAndUserID(UserCode, e.CommandArgument.ToString, "Client", Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        UIUtilities.ShowDialog(Me, "Client User Roles", "Remove client role to user successfully.", ICBO.IC.Dialogmessagetype.Success)
                        LoadddlRole(RoleType)
                        SetgvUserRoles(UserCode, Me.gvUserRoles.CurrentPageIndex + 1, gvUserRoles.PageSize, Me.gvUserRoles, True)
                        'End If
                    End If


                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub gvUserRoles_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvUserRoles.NeedDataSource
        Try
            SetgvUserRoles(UserCode, Me.gvUserRoles.CurrentPageIndex + 1, gvUserRoles.PageSize, Me.gvUserRoles, False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnApproveRoles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveRoles.Click


        Try
                Dim rowGVAssignedRoles As GridDataItem
                Dim chkSelect As CheckBox
                Dim RoleID As String = ""
                Dim UsersID As String = ""
                Dim PassToApproveCount As Integer = 0
                Dim FailToApproveCount As Integer = 0
                Dim objICUser As ICUser
                Dim EmailCheckCount As Integer = 0
                If CheckgvAssignedUserRolesForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Client User Roles", "Please select atleast one role.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVAssignedRoles In gvUserRoles.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVAssignedRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        RoleID = rowGVAssignedRoles.GetDataKeyValue("RoleID").ToString
                        UsersID = rowGVAssignedRoles.GetDataKeyValue("UserID").ToString
                        Dim objICUserRole As New ICUserRoles
                        objICUserRole.es.Connection.CommandTimeout = 3600
                        objICUserRole.LoadByPrimaryKey(UserCode, RoleID)
                        If Me.UserInfo.IsSuperUser = True Then
                            If objICUserRole.IsApproved = True Then
                                FailToApproveCount = FailToApproveCount + 1
                            Else
                                If ICRightsController.GetRightsAssignedToRoleByRoleID(RoleID) = False Then
                                    FailToApproveCount = FailToApproveCount + 1
                                    Continue For
                                End If
                                Try

                                    ICUserRolesController.ApproveICUserRoles(objICUserRole.RoleID, UsersID, chkSelect.Checked, Me.UserId.ToString, Me.UserInfo.Username.ToString, objICUserRole.UpToICUserByUserID.UserType.ToString, objICUserRole.UpToICRoleByRoleID.RoleType.ToString)
                                    ICUserRolesController.AssignRoleToUser(RoleID.ToString(), Me.PortalId, UsersID)

                                    If EmailCheckCount = 0 Then
                                        objICUser = New ICUser
                                        objICUser.LoadByPrimaryKey(UsersID)
                                        Dim user As New UserInfo
                                        Dim objFromICUser As New ICUser
                                        user = UserController.GetUserById(Me.PortalId, UsersID)
                                        objFromICUser.LoadByPrimaryKey(UserCode)
                                        Dim password As String = ""
                                        Dim password2 As String = ""
                                        password2 = DotNetNuke.Entities.Users.UserController.GetPassword(user, password)
                                        'dotnetnuke.entities.users.usercontroller.getpassword(byref userinfo, string.empty)
                                        If Not objICUser.IsNotificationSent = True Or objICUser.IsNotificationSent Is Nothing Then

                                            EmailUtilities.UserCreated(objICUser, password2)
                                            SMSUtilities.UserCreated(objICUser, password2)
                                            objICUser.IsNotificationSent = True
                                            Dim Action As String = ""


                                            Action = "Company Office User " & objICUser.UserName & " Updated. "

                                            Action += "Notification sent from[" & objFromICUser.IsNotificationSent & "] to [" & objICUser.IsNotificationSent & "] ; "

                                            objICUser.Save()
                                            ICUtilities.AddAuditTrail(Action, "Client User", objICUser.UserID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "UPDATE")
                                        End If
                                    End If
                                    EmailCheckCount = 1


                                    PassToApproveCount = PassToApproveCount + 1
                                Catch ex As Exception

                                    FailToApproveCount = FailToApproveCount + 1
                                    Continue For

                                End Try
                            End If

                        Else
                            If objICUserRole.IsApproved = True Or objICUserRole.AssignedBy = Me.UserId Then
                                FailToApproveCount = FailToApproveCount + 1
                            Else
                                If ICRightsController.GetRightsAssignedToRoleByRoleID(RoleID) = False Then
                                    FailToApproveCount = FailToApproveCount + 1
                                    Continue For
                                End If
                                Try
                                    ICUserRolesController.ApproveICUserRoles(objICUserRole.RoleID, UsersID, chkSelect.Checked, Me.UserId.ToString, Me.UserInfo.Username.ToString, objICUserRole.UpToICUserByUserID.UserType.ToString, objICUserRole.UpToICRoleByRoleID.RoleType.ToString)
                                    ICUserRolesController.AssignRoleToUser(RoleID.ToString(), Me.PortalId, UsersID)


                                    If EmailCheckCount = 0 Then
                                        objICUser = New ICUser
                                        objICUser.LoadByPrimaryKey(UsersID)
                                        Dim user As New UserInfo
                                        Dim objFromICUser As New ICUser
                                        user = UserController.GetUserById(Me.PortalId, UsersID)
                                        objFromICUser.LoadByPrimaryKey(UserCode)
                                        Dim password As String = ""
                                        Dim password2 As String = ""
                                        password2 = DotNetNuke.Entities.Users.UserController.GetPassword(user, password)
                                        'dotnetnuke.entities.users.usercontroller.getpassword(byref userinfo, string.empty)
                                        If Not objICUser.IsNotificationSent = True Or objICUser.IsNotificationSent Is Nothing Then

                                            EmailUtilities.UserCreated(objICUser, password2)
                                            SMSUtilities.UserCreated(objICUser, password2)
                                            objICUser.IsNotificationSent = True
                                            Dim Action As String = ""


                                            Action = "Company Office User " & objICUser.UserName & " Updated. "

                                            Action += "Notification sent from[" & objFromICUser.IsNotificationSent & "] to [" & objICUser.IsNotificationSent & "] ; "

                                            objICUser.Save()
                                            ICUtilities.AddAuditTrail(Action, "Client User", objICUser.UserID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "UPDATE")
                                        End If
                                    End If
                                    EmailCheckCount = 1




                                    PassToApproveCount = PassToApproveCount + 1
                                Catch ex As Exception

                                    FailToApproveCount = FailToApproveCount + 1
                                    Continue For

                                End Try
                            End If
                        End If

                    End If
                Next

                If Not PassToApproveCount = 0 And Not FailToApproveCount = 0 Then
                    SetgvUserRoles(UsersID, Me.gvUserRoles.CurrentPageIndex + 1, Me.gvUserRoles.PageSize, Me.gvUserRoles, True)
                    'ICRoleController.GetAllICRolesForRadGrid("User Role", Me.gvICRoles.CurrentPageIndex + 1, Me.gvICRoles.PageSize, Me.gvICRoles, True)
                    UIUtilities.ShowDialog(Me, "Client User Roles", "[ " & PassToApproveCount.ToString & " ] Client role(s) for user approved successfully.<br /> [ " & FailToApproveCount.ToString & " ] Role(s) for user can not be approved due to following reasons: <br /> 1. Role for user is already approved<br /> 2. Role for user must be approved by other than maker<br />3. No rights are assigned to role", ICBO.IC.Dialogmessagetype.Success)

                    Exit Sub
                ElseIf Not PassToApproveCount = 0 And FailToApproveCount = 0 Then
                    SetgvUserRoles(UsersID, Me.gvUserRoles.CurrentPageIndex + 1, Me.gvUserRoles.PageSize, Me.gvUserRoles, True)
                    'ICRoleController.GetAllICRolesForRadGrid("Bank Role", Me.gvICRoles.CurrentPageIndex + 1, Me.gvICRoles.PageSize, Me.gvICRoles, True)
                    UIUtilities.ShowDialog(Me, "Client User Roles", PassToApproveCount.ToString & " Client role(s) for user approved successfully.", ICBO.IC.Dialogmessagetype.Success)

                    Exit Sub
                ElseIf Not FailToApproveCount = 0 And PassToApproveCount = 0 Then
                    SetgvUserRoles(UsersID, Me.gvUserRoles.CurrentPageIndex + 1, Me.gvUserRoles.PageSize, Me.gvUserRoles, True)
                    'ICRoleController.GetAllICRolesForRadGrid("Bank Role", Me.gvICRoles.CurrentPageIndex + 1, Me.gvICRoles.PageSize, Me.gvICRoles, True)
                    UIUtilities.ShowDialog(Me, "Client User Roles", "[ " & FailToApproveCount.ToString & " ] Client role(s) for user can not be approved due to following reasons: <br /> 1. Role for user is already approved<br /> 2. Role for user must be approved by other than maker<br />3. No rights are assigned to role", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
            End Try


    End Sub
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            ' str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function
    Private Function CheckgvAssignedUserRolesForProcessAll() As Boolean
        Try
            Dim rowGVAssignedRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVAssignedRoles In gvUserRoles.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVAssignedRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnDeleteRoles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteRoles.Click

        Try
                'Dim rowGVAssignedRoles As GridDataItem
                Dim objRoleInfo As RoleInfo
                Dim objRoleController As RoleController
                Dim objUserInfo As UserInfo
                Dim objICUserRole As ICUserRoles
                Dim chkSelect As CheckBox
                Dim RoleID As String = ""
                Dim UserID As String = ""
                Dim PassToDeleteCount As Integer = 0
                Dim FailToDeleteCount As Integer = 0
                Dim userCtrl As New RoleController
                Dim arrLst As New ArrayList



                If CheckgvAssignedUserRolesForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Client User Roles", "Please select atleast one role.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                For Each rowGVAssignedRoles In gvUserRoles.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowGVAssignedRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        RoleID = rowGVAssignedRoles.GetDataKeyValue("RoleID").ToString
                        UserID = rowGVAssignedRoles.GetDataKeyValue("UserID").ToString
                        objICUserRole = New ICUserRoles
                        objICUserRole.es.Connection.CommandTimeout = 3600
                        If objICUserRole.LoadByPrimaryKey(UserID, RoleID.ToString()) Then
                            'If objICUserRole.IsApproved = True Then
                            '    FailToDeleteCount = FailToDeleteCount + 1
                            'Else
                            Try
                                objRoleInfo = New RoleInfo
                                objRoleController = New RoleController
                                objUserInfo = New UserInfo
                                objRoleInfo = objRoleController.GetRoleByName(Me.PortalId, objICUserRole.UpToICRoleByRoleID.RoleName.ToString)
                                objUserInfo = UserController.GetUserById(Me.PortalId, UserID)
                                ICUserRolesController.DeleteUserRoles(RoleID, UserCode, Me.UserId.ToString, Me.UserInfo.Username.ToString, objICUserRole.UpToICUserByUserID.UserType.ToString, objICUserRole.UpToICRoleByRoleID.RoleType.ToString)
                                RoleController.DeleteUserRole(objUserInfo, objRoleInfo, Me.PortalSettings, False)
                                ICUserRolesPaymentNatureController.DeleteTaggedAPNByRoleIDAndUserID(UserID, RoleID, "Client", Me.UserId.ToString, Me.UserInfo.Username.ToString)
                                PassToDeleteCount = PassToDeleteCount + 1
                            Catch ex As Exception

                                FailToDeleteCount = FailToDeleteCount + 1
                                Continue For

                            End Try
                            'End If
                        End If
                    End If


                Next

                If Not PassToDeleteCount = 0 And Not FailToDeleteCount = 0 Then
                    SetgvUserRoles(UserCode, Me.gvUserRoles.CurrentPageIndex + 1, Me.gvUserRoles.PageSize, Me.gvUserRoles, True)
                    'ICRoleController.GetAllICRolesForRadGrid("Bank Role", Me.gvICRoles.CurrentPageIndex + 1, Me.gvICRoles.PageSize, Me.gvICRoles, True)
                    UIUtilities.ShowDialog(Me, "Client User Roles", "[ " & PassToDeleteCount.ToString & " ] Client role(s) for user deleted successfuly.<br /> [ " & FailToDeleteCount.ToString & " ] Role(s) can not be deleted due to following reasons: <br /> 1. Role for user is approved", ICBO.IC.Dialogmessagetype.Failure)

                    Exit Sub
                ElseIf Not PassToDeleteCount = 0 And FailToDeleteCount = 0 Then
                    SetgvUserRoles(UserCode, Me.gvUserRoles.CurrentPageIndex + 1, Me.gvUserRoles.PageSize, Me.gvUserRoles, True)
                    'ICRoleController.GetAllICRolesForRadGrid("Bank Role", Me.gvICRoles.CurrentPageIndex + 1, Me.gvICRoles.PageSize, Me.gvICRoles, True)
                    UIUtilities.ShowDialog(Me, "Client User Roles", PassToDeleteCount.ToString & " Client role(s) for user deleted successfuly.", ICBO.IC.Dialogmessagetype.Success)

                    Exit Sub
                ElseIf Not FailToDeleteCount = 0 And PassToDeleteCount = 0 Then
                    SetgvUserRoles(UserCode, Me.gvUserRoles.CurrentPageIndex + 1, Me.gvUserRoles.PageSize, Me.gvUserRoles, True)
                    'ICRoleController.GetAllICRolesForRadGrid("Bank Role", Me.gvICRoles.CurrentPageIndex + 1, Me.gvICRoles.PageSize, Me.gvICRoles, True)
                    UIUtilities.ShowDialog(Me, "Client User Roles", "[ " & FailToDeleteCount.ToString & " ] Client role(s) for user can not be deleted due to following reasons: <br /> 1. Role for user is approved", ICBO.IC.Dialogmessagetype.Failure)

                    Exit Sub
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub
    Protected Sub gvUserRoles_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvUserRoles.ItemCreated
         Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvUserRoles.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvUserRoles_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvUserRoles.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAllUserRoles"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvUserRoles.MasterTableView.ClientID & "','0');"
        End If
    End Sub

End Class