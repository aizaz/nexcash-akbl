﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewClientUsers.ascx.vb" Inherits="DesktopModules_ClientUserManagement_ViewClientUsers" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        //      alert(grid)
        //        if ((grid.rows.length -1) != 10 ) {
        //        if ((grid.rows.length) >= 12) {
        //            //            alert('More than 10 rows')
        //            for (i = 0; i < grid.rows.length - 1; i++) {
        //                cell = grid.rows[i].cells[CellNo];
        //                for (j = 0; j < cell.childNodes.length - 1; j++) {
        //                    if (cell.childNodes[j].type == "checkbox") {
        //                        cell.childNodes[j].checked = document.getElementById(idOfControllingCheckbox).checked;
        //                    }
        //                }
        //            }
        //        }
        //        else {
        //                     alert(grid.rows.length)
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }
    //    }
</script>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:RegExpTextBoxSetting BehaviorID="REUserName" Validation-IsRequired="false"
        ValidationExpression="\b(\w*)\b(.)*\b(-)*\b(_)*\b" ErrorMessage="Invalid Name"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtUserName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="REDisplayName" Validation-IsRequired="false"
        ValidationExpression="\b(\w*)\b(.)*\b(-)*\b(_)*\b" ErrorMessage="Invalid Display Name"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDisplayName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="REEmailAddress" Validation-IsRequired="false"
        ValidationExpression="^([a-zA-Z0-9_\-\.]+)@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$"
        ErrorMessage="Invalid Email" EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtEmailAddress" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
</telerik:RadInputManager>
<asp:Panel ID="pnlUsers" runat="server" Width="100%" ScrollBars="None" DefaultButton="btnRefresh">
    <table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 100%">
                <table class="style1" width="100%">
                    <tr>
                        <td align="left" valign="top">
                            <asp:Label ID="lblUserListHeader" runat="server" CssClass="headingblue" 
                                Text="Client Users List"></asp:Label>
                            <br />
                        <%--    Note:&nbsp; Fields marked as
                            <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="*"></asp:Label>
                            &nbsp;are required to be selected.--%>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <asp:Button ID="btnAddUser" runat="server" Text="Add User" CssClass="btn" TabIndex="1"
                                CausesValidation="False" />
                            <asp:HiddenField ID="hfBankCode" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <table width="100%">
                            <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True"
                CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True"
                CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
                                <tr align="left" valign="top" style="width: 100%">
                                    <td align="left" valign="top" style="width: 25%">
                                        <asp:Label ID="lblOfficeCode" runat="server" CssClass="lbl" 
                                            Text="Select Client Office"></asp:Label>
                                    </td>
                                    <td align="left" valign="top" style="width: 25%">
                                        &nbsp;
                                    </td>
                                    <td align="left" valign="top" style="width: 25%">
                                        <asp:Label ID="lblUserName" runat="server" CssClass="lbl" Text="User Name"></asp:Label>
                                    </td>
                                    <td align="left" valign="top" style="width: 25%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr align="left" valign="top" style="width: 100%">
                                    <td align="left" valign="top" style="width: 25%">
                                        <asp:DropDownList ID="ddlOffice" runat="server" AutoPostBack="True" 
                                            CssClass="dropdown">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" valign="top" style="width: 25%">
                                        &nbsp;
                                    </td>
                                    <td align="left" valign="top" style="width: 25%">
                                        <asp:TextBox ID="txtUserName" runat="server" CssClass="txtbox" MaxLength="25"></asp:TextBox>
                                    </td>
                                    <td align="left" valign="top" style="width: 25%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr align="left" valign="top" style="width: 100%">
                                    <td align="left" valign="top" style="width: 25%">
                                        <asp:Label ID="lblDisplayName" runat="server" CssClass="lbl" 
                                            Text="Display Name"></asp:Label>
                                    </td>
                                    <td align="center" valign="top" style="width: 25%">
                                        &nbsp;
                                    </td>
                                    <td align="left" valign="top" style="width: 25%">
                                        <asp:Label ID="lblEmail" runat="server" CssClass="lbl" Text="Email"></asp:Label>
                                    </td>
                                    <td align="left" valign="top" style="width: 25%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr align="left" valign="top" style="width: 100%">
                                    <td align="left" valign="top" style="width: 25%">
                                        <asp:TextBox ID="txtDisplayName" runat="server" CssClass="txtbox" 
                                            MaxLength="100"></asp:TextBox>
                                    </td>
                                    <td align="center" valign="top" style="width: 25%">
                                        &nbsp;
                                    </td>
                                    <td align="left" valign="top" style="width: 25%">
                                        <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="txtbox" 
                                            MaxLength="100"></asp:TextBox>
                                    </td>
                                    <td align="left" valign="top" style="width: 25%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr align="left" valign="top" style="width: 100%">
                                    <td align="left" valign="top" style="width: 25%">
                                        &nbsp;</td>
                                    <td align="center" valign="top" style="width: 25%">
                                        &nbsp;
                                    </td>
                                    <td align="left" valign="top" style="width: 25%">
                                        &nbsp;</td>
                                    <td align="left" valign="top" style="width: 25%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr align="left" valign="top" style="width: 100%">
                                    <td align="center" valign="top" colspan="4">
                                        <asp:Button ID="btnRefresh" runat="server" CssClass="btn" Text="Search" 
                                            Width="75px" />
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr align="left" valign="top" style="width: 100%">
                                    <td align="left" valign="top" style="width: 25%">
                                        &nbsp;
                                    </td>
                                    <td align="center" valign="top" colspan="2">
                                        &nbsp;
                                    </td>
                                    <td align="left" valign="top" style="width: 25%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr align="left" valign="top" style="width: 100%">
                                    <td align="left" valign="top" colspan="4">
                                        <asp:Label ID="lblError" runat="server" CssClass="headingblue" 
                                            Font-Bold="False" Text="No Record Found" Visible="False"></asp:Label>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <telerik:RadGrid ID="gvClientUsers" runat="server" AllowPaging="True" AllowSorting="True"
                                AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                                <ClientSettings>
                                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                </ClientSettings>
                                <AlternatingItemStyle CssClass="rgAltRow" />
                                <MasterTableView NoMasterRecordsText="" DataKeyNames="UserID" TableLayout="Fixed">
                                      <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                    <Columns>
                                        <telerik:GridTemplateColumn HeaderText="Approve" DataField="IsApproved">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAllUsers" runat="server" CssClass="chkBox" Text=" Select"
                                                    TextAlign="Right" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="UserName" HeaderText="User Name" SortExpression="UserName">
                                           <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" Wrap="true"/>
                                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="DisplayName" HeaderText="Display Name" SortExpression="DisplayName">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" Wrap="true"/>
                                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" Wrap="true"/>
                                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" />
                                        
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="PhoneNo1" HeaderText="Phone No." SortExpression="PhoneNo1">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" Wrap="true"/>
                                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="UserType" HeaderText="User Type" SortExpression="UserType">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridCheckBoxColumn HeaderText="Active" DataField="IsActive">
                                         <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                        </telerik:GridCheckBoxColumn>
                                        <telerik:GridCheckBoxColumn HeaderText="Approve" DataField="IsApproved">
                                         <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                        </telerik:GridCheckBoxColumn>
                                        <telerik:GridTemplateColumn HeaderText="Edit">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveClientUser", "&mid=" & Me.ModuleId & "&id="& Eval("UserID") )%>'
                                                    ToolTip="Edit">Edit</asp:HyperLink>
                                            </ItemTemplate>
                                             <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("UserID") %>'
                                                    CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                    ToolTip="Delete" />
                                            </ItemTemplate>
                                             <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Assign Role">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hlAssignRole" runat="server" ImageUrl="~/images/lock.gif" NavigateUrl='<%# NavigateURL("AssignRole", "&mid=" & Me.ModuleId & "&id="& Eval("UserID")) %>'
                                                    ToolTip="Assign Role">Assign Role</asp:HyperLink>
                                            </ItemTemplate>
                                             <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="IbtnUnlock" runat="server" CommandArgument='<%#Eval("UserID") %>'
                                                    CommandName="Unlock" ImageUrl="~/images/unlock.PNG" ToolTip="Unlock" 
                                                    Visible="false" />
                                            </ItemTemplate>
                                             <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="5%" />
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                 <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                            </telerik:RadGrid>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <asp:Button ID="btnApproveUsers" runat="server" Text="Approve Users" CssClass="btn" />
                            &nbsp;<asp:Button ID="btnDeleteUsers" runat="server" Text="Delete Users" CssClass="btn" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Panel>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   