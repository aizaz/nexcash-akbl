﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports EntitySpaces.Core
Partial Class DesktopModules_ReIssuanceQueue_ReIssuanceQueue
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrICAssignUserRolsID As New ArrayList
    Private ArrICAssignedOfficeID As New ArrayList
    Private ArrICAssignedStatus As New ArrayList
    Private ArrICAssignedPaymentModes As New ArrayList
    Private htRights As Hashtable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()
            If Page.IsPostBack = False Then
                RefreshPage()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub RefreshPage()
        Try

            btnAllowReIssuance.Attributes.Item("onclick") = "if (Page_ClientValidate('Remarks')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnAllowReIssuance, Nothing).ToString() & " } "
            btnCancel.Attributes.Item("onclick") = "if (Page_ClientValidate('Remarks')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnCancel, Nothing).ToString() & " } "
            DesignDts()
            rbtnlstDateSelection.SelectedValue = "Creation Date"

            ViewState("htRights") = Nothing
            GetPageAccessRights()
            SetArraListAssignedPaymentModes()
            'SetArraListAssignedOfficeIDs()
            raddpCreationToDate.SelectedDate = Date.Now
            raddpCreationToDate.MaxDate = Date.Now

            LoadddlCompany()
            LoadddlAccountPaymentNatureByUserID()
            LoadddlProductTypeByAccountPaymentNature()
            ClearAllTextBoxes()
            FillDesignedDtByAPNatureByAssignedRole()
            Dim dtPageLoad As New DataTable
            dtPageLoad = DirectCast(ViewState("AccountNumber"), DataTable)


            If dtPageLoad.Rows.Count > 0 Then
                LoadddlBatcNumber()
                LoadgvAccountPaymentNatureProductType(True)
            Else
                UIUtilities.ShowDialog(Me, "Error", "You do not have access to any transactional data. Please contact Al Baraka Administrator.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Approve Allow Re-Issuance Queue")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Approve Allow Re-Issuance Queue") = True Then
                    ArrICAssignUserRolsID.Add(RoleID.ToString)
                End If
            Next
        End If
    End Sub
    Private Sub SetArraListAssignedOfficeIDs()
        Dim dt As New DataTable
        Dim objICUser As New ICUser
        ArrICAssignedOfficeID.Clear()
        dt = ICUserRolesPaymentNatureController.GetAllTaggedLocationsWithUserByUSerIDAndRoleID(Me.UserId.ToString, ArrICAssignUserRolsID)
        If dt.Rows.Count > 0 Then
            objICUser.LoadByPrimaryKey(Me.UserId)
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("OfficeID").ToString
                ArrICAssignedOfficeID.Add(RoleID.ToString)
            Next
            
        End If
        If Not ArrICAssignedOfficeID.Contains(objICUser.OfficeCode.ToString) Then
            ArrICAssignedOfficeID.Add(objICUser.OfficeCode)
        End If
    End Sub
    Private Sub SetArraListAssignedStatus()
        ArrICAssignedStatus.Clear()
        ArrICAssignedStatus.Add(26)
    End Sub
    Private Sub DesignDts()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim dtOfficeID As New DataTable
        dtAccountNumber.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("BranchCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("Currency", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("PaymentNatureCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("OfficeID", GetType(System.String)))
        ViewState("AccountNumber") = dtAccountNumber
    End Sub
    Private Sub FillDesignedDtByAPNatureByAssignedRole()
         Dim dtAssignedAPNatureOfficeID As New DataTable
        dtAssignedAPNatureOfficeID.Rows.Clear()
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserSecond(Me.UserId.ToString, ArrICAssignUserRolsID)
        dtAssignedAPNatureOfficeID.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAssignedAPNatureOfficeID.Rows
            dr("DropDown") = "False"
        Next
        ViewState("AccountNumber") = Nothing
        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID
      
    End Sub
    Private Sub SetArraListAssignedPaymentModes()
        ArrICAssignedPaymentModes.Clear()
        If CBool(htRights("Cheque Instructions")) = True Then
            ArrICAssignedPaymentModes.Add("Cheque")
        End If
        If CBool(htRights("DD Instructions")) = True Then
            ArrICAssignedPaymentModes.Add("DD")
        End If
        If CBool(htRights("PO Instructions")) = True Then
            ArrICAssignedPaymentModes.Add("PO")
        End If
    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)



            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, "", ArrICAssignUserRolsID)
            ddlCompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlCompany.Items.Clear()
                    ddlCompany.Items.Add(lit)
                    lit.Selected = True
                    ddlCompany.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
                ddlCompany.DataSource = dt
                ddlCompany.DataTextField = "CompanyName"
                ddlCompany.DataValueField = "CompanyCode"
                ddlCompany.DataBind()
                ddlCompany.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccountPaymentNatureByUserID()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlAcccountPaymentNature.Items.Clear()
            ddlAcccountPaymentNature.Items.Add(lit)
            lit.Selected = True
            ddlAcccountPaymentNature.AppendDataBoundItems = True
            ddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForDropDown(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
            ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
            ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
            ddlAcccountPaymentNature.DataBind()
          
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlBatcNumber()
        Try
           
            Dim dtAccountNumber As New DataTable

            SetArraListAssignedPaymentModes()
            dtAccountNumber = ViewState("AccountNumber")

            SetArraListAssignedStatus()
            Dim lit As New ListItem
            lit.Text = "-- All --"
            lit.Value = "0"
            ddlBatch.Items.Clear()
            ddlBatch.Items.Add(lit)
            lit.Selected = True
            ddlBatch.AppendDataBoundItems = True
            ddlBatch.DataSource = ICInstructionController.GetAllTaggedBatchNumbersByCompanyCodeWithStatusForPrinting(ArrICAssignedPaymentModes, dtAccountNumber, ddlCompany.SelectedValue.ToString, ArrICAssignedStatus, Me.UserId.ToString)
            ddlBatch.DataTextField = "FileBatchNoName"
            ddlBatch.DataValueField = "FileBatchNo"
            ddlBatch.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    
    Private Sub LoadddlProductTypeByAccountPaymentNature()
        Try
            Dim lit As New ListItem
            Dim objICAPNature As New ICAccountsPaymentNature
            objICAPNature.es.Connection.CommandTimeout = 3600
            ddlProductType.Items.Clear()
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlProductType.Items.Add(lit)
            ddlProductType.AppendDataBoundItems = True
            If ddlAcccountPaymentNature.SelectedValue.ToString <> "0" Then
                If objICAPNature.LoadByPrimaryKey(ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)) Then
                    ddlProductType.DataSource = ICAccountPaymentNatureProductTypeController.GetAllProductTypesByAccountAndPaymentNature(objICAPNature)
                    ddlProductType.DataTextField = "ProductTypeName"
                    ddlProductType.DataValueField = "ProductTypeCode"
                    ddlProductType.DataBind()
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadgvAccountPaymentNatureProductType(ByVal DoDataBind As Boolean)
        Try
            
            Dim DateType, AccountNumber, BranchCode, Currency, PaymentNatureCode, CompanyCode, ProductTypeCode, InstrumentNo, InstructionNo, BatchCode, ReferenceNo As String
            Dim FromDate, ToDate As String
            Dim Amount As Double = 0
            Dim dtAccountNumber As New DataTable
            dtAccountNumber = ViewState("AccountNumber")
            DateType = ""
            AccountNumber = ""
            BranchCode = ""
            Currency = ""
            Currency = ""
            PaymentNatureCode = ""
            CompanyCode = "0"
            ProductTypeCode = ""
            InstructionNo = ""
            InstrumentNo = ""
            BatchCode = ""
            ReferenceNo = ""
            FromDate = Nothing
            ToDate = Nothing
            If txtAmount.Text <> "" And txtAmount.Text <> "Enter Amount" Then
                Amount = CDbl(txtAmount.Text)
            End If

            If rbtnlstDateSelection.SelectedValue = "Creation Date" Then
                DateType = "Creation Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Value Date" Then
                DateType = "Value Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Last Print Date" Then
                DateType = "Last Print Date"
            End If

            If txtReferenceNo.Text.ToString <> "" And txtReferenceNo.Text.ToString <> "Enter Reference No." Then
                ReferenceNo = txtReferenceNo.Text.ToString
            End If

            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            End If
            If ddlProductType.SelectedValue.ToString <> "0" Then
                ProductTypeCode = ddlProductType.SelectedValue.ToString
            End If
            If txtInstrumentNo.Text.ToString <> "" And txtInstrumentNo.Text.ToString <> "Enter Instrument No." Then
                InstrumentNo = txtInstrumentNo.Text.ToString
            End If
            If txtInstructionNo.Text.ToString <> "" And txtInstructionNo.Text.ToString <> "Enter Instruction No." Then
                InstructionNo = txtInstructionNo.Text.ToString
            End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                FromDate = raddpCreationFromDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If Not raddpCreationToDate.SelectedDate Is Nothing Then
                ToDate = raddpCreationToDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If ddlBatch.SelectedValue <> "0" Then
                BatchCode = ddlBatch.SelectedValue.ToString
            End If
            SetArraListAssignedStatus()
            SetArraListAssignedOfficeIDs()
            SetArraListAssignedPaymentModes()
            ICInstructionController.GetAllInstructionsForRadGridForPrintingWithUserLocation(DateType, FromDate, ToDate, dtAccountNumber, BatchCode, ProductTypeCode, InstructionNo, InstrumentNo, ReferenceNo, Me.gvAllowRePrintReIssue.CurrentPageIndex + 1, Me.gvAllowRePrintReIssue.PageSize, Me.gvAllowRePrintReIssue, DoDataBind, ArrICAssignedStatus, Me.UserId.ToString, Amount, ddlAmountOp.SelectedValue.ToString, ArrICAssignedPaymentModes, CompanyCode)
            If gvAllowRePrintReIssue.Items.Count > 0 Then
                gvAllowRePrintReIssue.Visible = True
                lblNRF.Visible = False
                btnAllowReIssuance.Visible = CBool(htRights("Approve Re-Issuance"))
                btnCancel.Visible = CBool(htRights("Cancel"))
            Else
                lblNRF.Visible = True
                gvAllowRePrintReIssue.Visible = False
                btnAllowReIssuance.Visible = False
                btnCancel.Visible = False

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    
    'Private Sub SetJavaScript()
    '    Dim imgBtn As ImageButton
    '    'If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Then
    '    Dim AppPath As String = DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
    '    For Each gvAuthorizationRow As GridDataItem In gvAllowRePrintReIssue.Items
    '        imgBtn = New ImageButton
    '        imgBtn = DirectCast(gvAuthorizationRow.Cells(11).FindControl("ibButton"), ImageButton)
    '        imgBtn.OnClientClick = "showurldialog('Instruction Details','" & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & imgBtn.CommandArgument.ToString() & "' ,true,700,650);return false;"
    '    Next
    '    'End If
    'End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            'SetArraListRoleID()
            LoadddlBatcNumber()
            LoadddlAccountPaymentNatureByUserID()
            LoadddlProductTypeByAccountPaymentNature()

            LoadgvAccountPaymentNatureProductType(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlAcccountPaymentNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcccountPaymentNature.SelectedIndexChanged
          Try
            If ddlAcccountPaymentNature.SelectedValue.ToString = "0" Then
                SetArraListRoleID()
                FillDesignedDtByAPNatureByAssignedRole()
                LoadddlBatcNumber()
                LoadddlProductTypeByAccountPaymentNature()

               
                LoadgvAccountPaymentNatureProductType(True)
            Else
FillDesignedDtByAPNatureByAssignedRole()
                SetViewStateDtOnAPNatureIndexChnage()
                LoadddlBatcNumber()
                LoadddlProductTypeByAccountPaymentNature()


                LoadgvAccountPaymentNatureProductType(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetViewStateDtOnAPNatureIndexChnage()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim AccountNumber, BranchCode, Currency, PaymentNatureCode As String
        Dim ArrayListOfficeID As New ArrayList
        Dim drAccountNo As DataRow
        AccountNumber = Nothing
        BranchCode = Nothing
        Currency = Nothing
        PaymentNatureCode = Nothing
        dtAccountNumber = ViewState("AccountNumber")
        ViewState("AccountNumber") = Nothing
        AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0)
        BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1)
        Currency = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2)
        PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)
        For Each dr As DataRow In dtAccountNumber.Rows
            If dr("AccountNumber").ToString.Split("-")(0) = AccountNumber And dr("AccountNumber").ToString.Split("-")(1) = BranchCode And dr("AccountNumber").ToString.Split("-")(2) = Currency And dr("PaymentNature").ToString = PaymentNatureCode Then
                ArrayListOfficeID.Add(dr("OfficeID").ToString)
            End If
        Next
        dtAccountNumber.Rows.Clear()
        If ArrayListOfficeID.Count > 0 Then
            For Each OfficeID In ArrayListOfficeID
                drAccountNo = dtAccountNumber.NewRow()
                drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
                drAccountNo("PaymentNature") = PaymentNatureCode
                drAccountNo("OfficeID") = OfficeID
                dtAccountNumber.Rows.Add(drAccountNo)
            Next
        Else
            drAccountNo = dtAccountNumber.NewRow()
            drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
            drAccountNo("PaymentNature") = PaymentNatureCode
            drAccountNo("OfficeID") = "0"
            dtAccountNumber.Rows.Add(drAccountNo)
        End If
        'dtAccountNumber.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAccountNumber.Rows
            dr("DropDown") = "True"
        Next
        ViewState("AccountNumber") = dtAccountNumber



    End Sub
    Protected Sub ddlBatch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBatch.SelectedIndexChanged
        Try
            SetArraListRoleID()
            LoadddlProductTypeByAccountPaymentNature()

            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged
        Try
            SetArraListRoleID()



            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ClearAllTextBoxes()
        txtRemarks.Text = ""
        txtInstructionNo.Text = ""
        txtInstrumentNo.Text = ""
        txtReferenceNo.Text = ""
        txtAmount.Text = ""
        ddlAmountOp.SelectedValue = "="
    End Sub
    Private Function CheckgvInstructionAuthentication() As Boolean
        Try

            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVInstruction In gvAllowRePrintReIssue.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVInstruction.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub gvAllowRePrintReIssue_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAllowRePrintReIssue.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAllowRePrintReIssue.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAllowRePrintReIssue_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAllowRePrintReIssue.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvAllowRePrintReIssue.MasterTableView.ClientID & "','0');"
        End If
        Dim imgBtn As LinkButton
        Dim AppPath As String = "" ' DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            imgBtn = New LinkButton
            imgBtn = DirectCast(item.Cells(1).FindControl("lbInstructionID"), LinkButton)
            imgBtn.OnClientClick = "showurldialog('Instruction Details',' " & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & item.GetDataKeyValue("InstructionID").ToString().EncryptString() & "' ,true,700,650);return false;"
            If CBool(htRights("View Instruction Details")) = True Then
                imgBtn.Enabled = True

            End If
            If item.GetDataKeyValue("IsAmendmentComplete") = True Then

                gvAllowRePrintReIssue.AlternatingItemStyle.CssClass = "GridItemComplete"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItemComplete"


            Else
                gvAllowRePrintReIssue.AlternatingItemStyle.CssClass = "GridItem"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItem"


            End If
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click



        Try
                LoadgvAccountPaymentNatureProductType(True)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try


    End Sub

    Protected Sub gvAllowRePrintReIssue_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAllowRePrintReIssue.NeedDataSource
        Try
            LoadgvAccountPaymentNatureProductType(False)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckgvClientRoleCheckedForProcessAll() As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvAllowRePrintReIssue.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnAllowReIssuance_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAllowReIssuance.Click
        If Page.IsValid Then


            Try
                Dim chkSelect As CheckBox
                Dim ArryListInstructionId As New ArrayList
                Dim objICInstruction As ICInstruction
                Dim objReIssuedInstruction As ICInstruction
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim StrAuditTrailAction As String
                Dim drSummary As DataRow
                Dim dtVerifiedInstructionPODD As New DataTable
                Dim dtVerifiedInstruction As New DataTable
                ArryListInstructionId.Clear()
                Dim ArrayListstatusForPODD As New ArrayList
                Dim ArrayListstatusForNonPODD As New ArrayList
                Dim POOrDDInstructionCount As Integer = 0
                Dim ChequeInstructionCount As Integer = 0
                Dim CustomMessage As String = "There is an issue in re-issuance approval. Please contact to Al Baraka administrator."
                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Allow Re-Issuance Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                Dim dt As New DataTable
                dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Message", GetType(System.String)))
                For Each gvVerificationRow As GridDataItem In gvAllowRePrintReIssue.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                    End If
                Next
                If ArryListInstructionId.Count > 0 Then
                    For Each InstructionID In ArryListInstructionId
                        objICInstruction = New ICInstruction
                        objReIssuedInstruction = New ICInstruction
                        objICInstruction.es.Connection.CommandTimeout = 3600
                        objICInstruction.LoadByPrimaryKey(InstructionID)
                        drSummary = dt.NewRow
                        If objICInstruction.ReissuanceAlloweBy <> Me.UserId Then
                            Try
                                ICInstructionController.CopyInstructionOnApprovalOfReIssuance(InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, txtRemarks.Text.ToString, "REISSUANCE")
                                'objReIssuedInstruction.Query.Where(objReIssuedInstruction.ReIssuanceID = objICInstruction.InstructionID.ToString)
                                'objReIssuedInstruction.Query.Load()

                                drSummary("InstructionID") = objICInstruction.InstructionID.ToString
                                drSummary("Message") = "Instruction(s) allowed re-issuance  successfully"
                                drSummary("Status") = "5"
                                dt.Rows.Add(drSummary)
                                PassToCancelCount = PassToCancelCount + 1
                                Continue For
                            Catch ex As Exception
                                objICInstruction.SkipReason = "Instruction(s) Skipped: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped, allow re-issuance fails due to " & ex.Message.ToString & ". Action was taken by [ " & Me.UserId & " ] [ " & Me.UserInfo.Username & " ]"
                                ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, CustomMessage)
                                drSummary("InstructionID") = objICInstruction.InstructionID.ToString
                                'drSummary("Message") = "Instruction(s) Skipped: Allow re-issuance fails due to " & ex.Message.ToString
                                drSummary("Message") = "Instruction(s) Skipped"
                                drSummary("Status") = objICInstruction.Status.ToString
                                dt.Rows.Add(drSummary)
                                'ICUtilities.AddAuditTrail("Instruction Skip: Allow re-issuance fails on allow reissuance queue  for instruction with ID [ " & InstructionID & " ] due to " & ex.Message.ToString, "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "SKIP")
                                FailToCancelCount = FailToCancelCount + 1
                                Continue For
                            End Try
                        Else
                            objICInstruction.SkipReason = "Instruction(s) Skipped: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped, allow re-issuance fails because re-issuance checker must be other than maker. Action was taken by [ " & Me.UserId & " ] [ " & Me.UserInfo.Username & " ]"
                            ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, CustomMessage)
                            drSummary("InstructionID") = objICInstruction.InstructionID.ToString
                            'drSummary("Message") = "Instruction(s) Skipped: Allow re-issuance fails due to " & ex.Message.ToString
                            drSummary("Message") = "Instruction(s) Skipped"
                            drSummary("Status") = objICInstruction.Status.ToString
                            dt.Rows.Add(drSummary)
                            'ICUtilities.AddAuditTrail("Instruction Skip: Allow re-issuance fails on allow reissuance queue  for instruction with ID [ " & InstructionID & " ] due to " & ex.Message.ToString, "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For
                        End If

                    Next
                End If
                RefreshPage()
                If dt.Rows.Count > 0 Then
                    gvShowSummary.DataSource = Nothing
                    gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                    gvShowSummary.DataBind()
                    pnlShowSummary.Style.Remove("display")
                    Dim scr As String
                    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                Else
                    gvShowSummary.DataSource = Nothing
                    pnlShowSummary.Style.Add("display", "none")
                End If
                ArrayListstatusForPODD.Clear()
                ArrayListstatusForNonPODD.Clear()
                ArrayListstatusForPODD.Add(16)
                ArrayListstatusForNonPODD.Add(8)
                dtVerifiedInstructionPODD = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocationForReIssuance(ArryListInstructionId, ArrayListstatusForPODD, True, "Printing")
                dtVerifiedInstruction = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocationForReIssuance(ArryListInstructionId, ArrayListstatusForNonPODD, False, "APNLocation", "Cheque")
                If dtVerifiedInstructionPODD.Rows.Count > 0 Then
                    EmailUtilities.ReissuanceApproved(dtVerifiedInstructionPODD, ArryListInstructionId, ArrayListstatusForPODD, "Printing")
                    SMSUtilities.ReissuanceApproved(dtVerifiedInstructionPODD, ArryListInstructionId, ArrayListstatusForPODD, "Printing")
                End If
                If dtVerifiedInstruction.Rows.Count > 0 Then
                    EmailUtilities.ReissuanceApproved(dtVerifiedInstruction, ArryListInstructionId, ArrayListstatusForNonPODD, "APNatureAndLocation")
                    SMSUtilities.ReissuanceApproved(dtVerifiedInstruction, ArryListInstructionId, ArrayListstatusForNonPODD, "APNatureAndLocation")
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

  
    Protected Sub gvAllowRePrintReIssue_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvAllowRePrintReIssue.PageIndexChanged
        Try
            gvAllowRePrintReIssue.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub raddpCreationFromDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationFromDate.SelectedDateChanged
        Try

            'If raddpCreationFromDate.SelectedDate Is Nothing Then
            '    UIUtilities.ShowDialog(Me, "Error", "Please select creation from date.", ICBO.IC.Dialogmessagetype.Failure)
            '    Exit Sub
            'End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Last Print Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Last print from date should be less than last print to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub raddpCreationToDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationToDate.SelectedDateChanged
        Try

            'If raddpCreationFromDate.SelectedDate Is Nothing Then
            '    UIUtilities.ShowDialog(Me, "Error", "Please select creation from date.", ICBO.IC.Dialogmessagetype.Failure)
            '    Exit Sub
            'End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Last Print Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Last print from date should be less than last print to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub rbtnlstDateSelection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnlstDateSelection.SelectedIndexChanged
        Try
            If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now.AddYears(2)
            Else
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
            End If

            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Last Print Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Last print from date should be less than last print to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Page.Validate()

        If Page.IsValid Then

            Try
                Dim chkSelect As CheckBox
                Dim ArryListInstructionId As New ArrayList
                Dim objICInstruction As ICInstruction
                Dim objReIssuedInstruction As ICInstruction
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim StrAuditTrailAction As String
                Dim drSummary As DataRow
                Dim dtVerifiedInstructionPODD As New DataTable
                Dim dtVerifiedInstruction As New DataTable
                ArryListInstructionId.Clear()
                Dim ArrayListstatusForPODD As New ArrayList
                Dim ArrayListstatusForNonPODD As New ArrayList
                Dim POOrDDInstructionCount As Integer = 0
                Dim ChequeInstructionCount As Integer = 0
                Dim StrRemarks As String = Nothing
                Dim CustomMessage As String = "There is an issue in re-issuance approval cancellation. Please contact to Al Baraka administrator."
                If CheckgvClientRoleCheckedForProcessAll() = False Then
                    UIUtilities.ShowDialog(Me, "Approve Allow Re-Issuance Queue", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If

                Dim dt As New DataTable
                dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Message", GetType(System.String)))
                For Each gvVerificationRow As GridDataItem In gvAllowRePrintReIssue.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))
                    End If
                Next
                If ArryListInstructionId.Count > 0 Then
                    If txtRemarks.Text <> "" Then
                        StrRemarks = txtRemarks.Text
                    End If
                    For Each InstructionID In ArryListInstructionId
                        objICInstruction = New ICInstruction
                        objReIssuedInstruction = New ICInstruction
                        objICInstruction.es.Connection.CommandTimeout = 3600
                        objICInstruction.LoadByPrimaryKey(InstructionID)
                        drSummary = dt.NewRow
                        Try
                            StrAuditTrailAction = Nothing
                            StrAuditTrailAction += "Instruction with ID [ " & objICInstruction.InstructionID & " ] re-issuance cancelled. Action was taken by [ " & Me.UserId & " ] [ " & Me.UserInfo.Username & " ]"
                            objICInstruction.AllowedReIssuanceCancelledBy = Me.UserId
                            objICInstruction.AllowedReIssuanceCancelledDate = Date.Now
                            objICInstruction.IsAllowedReIssuanceCancelled = True
                            ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAuditTrailAction, "UPDATE")
                            ICInstructionController.UpdateInstructionStatus(InstructionID, objICInstruction.Status.ToString, objICInstruction.LastStatus.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "Remarks", StrRemarks)
                            objReIssuedInstruction.LoadByPrimaryKey(InstructionID)
                            drSummary("InstructionID") = objReIssuedInstruction.InstructionID.ToString
                            drSummary("Message") = "Instruction(s) allowed re-issuance cancelled  successfully"
                            drSummary("Status") = objReIssuedInstruction.Status.ToString
                            dt.Rows.Add(drSummary)
                            PassToCancelCount = PassToCancelCount + 1
                            Continue For
                        Catch ex As Exception
                            objICInstruction.SkipReason = "Instruction(s) Skipped: Instruction with ID [ " & objICInstruction.InstructionID & " ] skipped, allow re-issuance cancellation fails due to " & ex.Message.ToString & ". Action was taken by [ " & Me.UserId & " ] [ " & Me.UserInfo.Username & " ]"
                            ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, CustomMessage)
                            drSummary("InstructionID") = objICInstruction.InstructionID.ToString
                            'drSummary("Message") = "Instruction(s) Skipped: Allow re-issuance fails due to " & ex.Message.ToString
                            drSummary("Message") = "Instruction(s) Skipped"
                            drSummary("Status") = objICInstruction.Status.ToString
                            dt.Rows.Add(drSummary)
                            'ICUtilities.AddAuditTrail("Instruction Skip: Allow re-issuance fails on allow reissuance queue  for instruction with ID [ " & InstructionID & " ] due to " & ex.Message.ToString, "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For
                        End Try
                    Next
                End If
                RefreshPage()
                If dt.Rows.Count > 0 Then
                    gvShowSummary.DataSource = Nothing
                    gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                    gvShowSummary.DataBind()
                    pnlShowSummary.Style.Remove("display")
                    Dim scr As String
                    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                Else
                    gvShowSummary.DataSource = Nothing
                    pnlShowSummary.Style.Add("display", "none")
                End If
                ArrayListstatusForNonPODD.Clear()
                ArrayListstatusForNonPODD.Add(21)
                If ArryListInstructionId.Count > 0 Then
                    dtVerifiedInstruction = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocationForPrinting(ArryListInstructionId, ArrayListstatusForNonPODD, False)
                    If dtVerifiedInstruction.Rows.Count > 0 Then
                        EmailUtilities.ReissuanceRejectedbyApprover(dtVerifiedInstruction, ArrayListstatusForNonPODD, ArryListInstructionId, "Printing")
                        SMSUtilities.TxnforReissuancecancelledbyapproverbeforeapproval(dtVerifiedInstruction, ArryListInstructionId, ArrayListstatusForNonPODD, "Printing")
                    End If
                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try

        End If
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click

    End Sub
End Class


