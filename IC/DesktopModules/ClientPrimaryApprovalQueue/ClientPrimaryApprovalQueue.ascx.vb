﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Imports EntitySpaces.Core

Partial Class DesktopModules_ClientPrimaryApprovalQueue_ClientPrimaryApprovalQueue
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrICAssignUserRolsID As New ArrayList
    Private ArrICAssignedOfficeID As New ArrayList
    Private ArrICAssignedStatus As New ArrayList
    Private ArrICAssignedPaymentModes As New ArrayList
    Private htRights As Hashtable

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        'Register AJAX components if available
        If DotNetNuke.Framework.AJAX.IsInstalled Then
            'Register the script manager
            DotNetNuke.Framework.AJAX.RegisterScriptManager()

            'Wrap the pnlAjaxUpdate within an update panel with progress control
            'DotNetNuke.Framework.AJAX.WrapUpdatePanelControl(Panel1, True)




            'Register the btnPostbackTimeUpdate button as a postback control
            ' DotNetNuke.Framework.AJAX.RegisterPostBackControl(Me.btnsubmit2)


        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()
            lblError.Visible = False
            lblMessageForQueue.Style.Add("display", "none")
            If Page.IsPostBack = False Then
                pnl2FACode.Style.Add("display", "none")
                lblMessageForQueue.Text = ICUtilities.GetSettingValue("ErrorMessage")
                txt2FACode.Text = ""
                btnApprove.Attributes.Item("onclick") = "if (Page_ClientValidate('Verify')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";document.getElementById('<%=lblMessageForQueue.ClientID%>').style.display = 'block';this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnApprove, Nothing).ToString() & " } "
                btnSendForAmend.Attributes.Item("onclick") = "if (Page_ClientValidate('VerifyAmend')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnSendForAmend, Nothing).ToString() & " } "
                'btnCancelInstructions.Attributes.Item("onclick") = "if (Page_ClientValidate('Verify')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnCancelInstructions, Nothing).ToString() & " } "
                'btnClose2FA.OnClientClick = "CloseDialog2FA('" & NavigateURL() & "');return false;"
                DesignDts()
                ViewState("htRights") = Nothing
                GetPageAccessRights()

                'SetArraListAssignedOfficeIDs()
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationFromDate.MaxDate = Date.Now
                rbtnlstDateSelection.SelectedValue = "Creation Date"
                RefreshPage()
             
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub RefreshPage()
        Dim dtPageLoad As New DataTable
        LoadddlCompany()
        LoadddlAccountNumbers()
        LoadddlAccountPaymentNatureByUserID()
        LoadddlProductTypeByAccountPaymentNature()
        ClearAllTextBoxes()
        FillDesignedDtByAPNatureByAssignedRole()
        dtPageLoad = DirectCast(ViewState("AccountNumber"), DataTable)
        If dtPageLoad.Rows.Count > 0 Then
            LoadddlBatcNumber()
            LoadgvAccountPaymentNatureProductType(True)
        Else
            UIUtilities.ShowDialog(Me, "Error", "You do not have access to any transactional data. Please contact Al Baraka Administrator.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
            Exit Sub
        End If
    End Sub
    Private Sub DesignDts()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim dtOfficeID As New DataTable
        dtAccountNumber.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("BranchCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("Currency", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("PaymentNatureCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("OfficeID", GetType(System.String)))
        ViewState("AccountNumber") = dtAccountNumber
    End Sub
    Private Sub FillDesignedDtByAPNatureByAssignedRole()


        Dim dtAssignedAPNatureOfficeID As New DataTable
        Dim PaymentNatureList As New ArrayList


        dtAssignedAPNatureOfficeID.Rows.Clear()
        PaymentNatureList = GetApprovalRulesPaymentNatureForUser(Me.UserInfo.UserID.ToString)
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserSecondForApprovalRule(Me.UserId.ToString, ArrICAssignUserRolsID, PaymentNatureList)
        dtAssignedAPNatureOfficeID.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAssignedAPNatureOfficeID.Rows
            dr("DropDown") = "False"
        Next
        ViewState("AccountNumber") = Nothing
        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID

        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID
     
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Approval Queue")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Approval Queue") = True Then
                    If Not ArrICAssignUserRolsID.Contains(RoleID.ToString) Then
                        ArrICAssignUserRolsID.Add(RoleID.ToString)
                    End If
                End If
            Next
        End If
    End Sub
    Private Sub SetArraListAssignedOfficeIDs()
        Dim dt As New DataTable
        Dim objICUser As New ICUser
        objICUser.es.Connection.CommandTimeout = 3600
        ArrICAssignedOfficeID.Clear()
        objICUser.LoadByPrimaryKey(Me.UserId)
        dt = ICUserRolesPaymentNatureController.GetAllTaggedLocationsWithUserByUSerIDAndRoleID(Me.UserId.ToString, ArrICAssignUserRolsID)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("OfficeID").ToString
                ArrICAssignedOfficeID.Add(RoleID.ToString)
            Next
        End If
        ArrICAssignedOfficeID.Add(objICUser.OfficeCode)
    End Sub
    Private Sub SetArraListAssignedStatus()
        Dim dt As New DataTable
        Dim NoApprovalRight As Boolean = False
        ArrICAssignedStatus.Clear()
        If CBool(htRights("Primary Approval")) = True Then
            ArrICAssignedStatus.Add(8)
            NoApprovalRight = True
        End If
        If CBool(htRights("Secondary Approval")) = True Then
            ArrICAssignedStatus.Add(8)
            NoApprovalRight = True
        End If
        If NoApprovalRight = False Then
            ArrICAssignedStatus.Add(8)

        End If

    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim PaymentNatureList As New ArrayList
            Dim dt As New DataTable
            Dim lit As New ListItem
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            PaymentNatureList = GetApprovalRulesPaymentNatureForUser(Me.UserInfo.UserID.ToString)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserIDForApprovalRule(Me.UserId.ToString, "", ArrICAssignUserRolsID, PaymentNatureList)
            ddlCompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlCompany.Items.Clear()
                    ddlCompany.Items.Add(lit)
                    lit.Selected = True
                    ddlCompany.Enabled = False
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
                ddlCompany.DataSource = dt
                ddlCompany.DataTextField = "CompanyName"
                ddlCompany.DataValueField = "CompanyCode"
                ddlCompany.DataBind()
                ddlCompany.Enabled = True
            Else

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlAccountPaymentNatureByUserID()
        Try
            Dim lit As New ListItem
            Dim PaymentNatureList As New ArrayList
            PaymentNatureList = GetApprovalRulesPaymentNatureForUser(Me.UserInfo.UserID.ToString)
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlAcccountPaymentNature.Items.Clear()
            ddlAcccountPaymentNature.Items.Add(lit)
            lit.Selected = True
            ddlAcccountPaymentNature.AppendDataBoundItems = True
            ddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForDropDownForApprovalRule(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID, PaymentNatureList)
            ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
            ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
            ddlAcccountPaymentNature.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlAccountNumbers()
        Try
            Dim lit As New ListItem
            Dim PaymentNatureList As New ArrayList
            PaymentNatureList = GetApprovalRulesPaymentNatureForUser(Me.UserInfo.UserID.ToString)
            lit.Text = "-- Please Select --"
            lit.Value = "0"
            ddlAccount.Items.Clear()
            ddlAccount.Items.Add(lit)
            lit.Selected = True
            ddlAccount.AppendDataBoundItems = True
            ddlAccount.DataSource = ICInstructionController.GetAccountNumbersTaggedWithUserSecondForApprovalRule(Me.UserId.ToString, ArrICAssignUserRolsID, ddlCompany.SelectedValue.ToString, PaymentNatureList)
            ddlAccount.DataTextField = "MainAccountNo"
            ddlAccount.DataValueField = "AccountNumber"
            ddlAccount.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlBatcNumber()
        Try
            Dim dtAccountNumber As New DataTable
           
            dtAccountNumber = ViewState("AccountNumber")

            Dim dtPaymentNature As New DataTable
            Dim ApprovalRuleIDList As New ArrayList
            Dim ApprovalRuleColl As New ICApprovalRuleCollection
            Dim MinAmount As Double = 0
            Dim MaxAmount As Double = 0

            'dtPaymentNature = ICInstructionApprovalRuleController.GetTaggedPaymentNatureWithUser(ArrICAssignUserRolsID, UserId)
            ApprovalRuleIDList = GetApprovalRulesForUserVIAPaymentNature(Me.UserInfo.UserID, dtAccountNumber)
            

            ApprovalRuleColl = ICApprovalRuleManagementController.GetApprovalRuleCollectionByApprovalRuleID(ApprovalRuleIDList)
            SetArraListAssignedStatus()
            Dim lit As New ListItem
            lit.Text = "-- All --"
            lit.Value = "0"
            ddlBatch.Items.Clear()
            ddlBatch.Items.Add(lit)
            lit.Selected = True
            ddlBatch.AppendDataBoundItems = True
            ddlBatch.DataSource = GetAllTaggedBatchNumbersByCompanyCodeWithStatusForApprovalQueue(ArrICAssignedPaymentModes, dtAccountNumber, ddlCompany.SelectedValue.ToString, ArrICAssignedStatus, Me.UserId.ToString, ApprovalRuleColl)
            ddlBatch.DataTextField = "FileBatchNoName"
            ddlBatch.DataValueField = "FileBatchNo"
            ddlBatch.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Public Shared Function GetAllTaggedBatchNumbersByCompanyCodeWithStatusForApprovalQueue(ByVal AssignedPaymentModes As ArrayList, ByVal dtAccountNo As DataTable, ByVal CompanyCode As String, ByVal ArrayListStatus As ArrayList, ByVal UserIDs As String, ByVal AppRuleCollection As ICApprovalRuleCollection) As DataTable
        Dim StrQuery As String = Nothing
        Dim WhereClause As String = Nothing
        WhereClause = " Where "
        Dim dt As New DataTable
        Dim params As New EntitySpaces.Interfaces.esParameters

        StrQuery = "select distinct(IC_Instruction.FileBatchNo), "
        StrQuery += "case ic_instruction.FileBatchNo when 'SI' "
        StrQuery += " then 'SI' else ic_instruction.FileBatchNo + '-' + IC_Files.OriginalFileName end as FileBatchNoName from IC_Instruction  "
        StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID  Where IC_Instruction.InstructionID IN "
        StrQuery += "(Select ins2.InstructionID from IC_Instruction as ins2 "
        If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
            WhereClause += " And ins2.PaymentMode in ( '"
            For i = 0 To AssignedPaymentModes.Count - 1
                WhereClause += AssignedPaymentModes(i) & "',"
            Next
            WhereClause = WhereClause.Remove(WhereClause.Length - 1)
            WhereClause += ")"
        End If
        WhereClause += " And "
        WhereClause += "("
        For Each objICApprovalRule As ICApprovalRule In AppRuleCollection
            WhereClause += "(ins2.Amount BETWEEN  " & objICApprovalRule.FromAmount & " AND " & objICApprovalRule.ToAmount & ") OR"
        Next

        WhereClause = WhereClause.Remove(WhereClause.Length - 3, 3)
        WhereClause += ")"
        If ArrayListStatus.Count > 0 Then
            WhereClause += " And ins2.Status in ( "
            For i = 0 To ArrayListStatus.Count - 1
                WhereClause += ArrayListStatus(i) & ","
            Next
            WhereClause = WhereClause.Remove(WhereClause.Length - 1)
            WhereClause += ")"
        End If
        If CompanyCode.ToString <> "0" Then
            WhereClause += " And ins2.CompanyCode=@CompanyCode"
            params.Add("CompanyCode", CompanyCode)
        End If
        If dtAccountNo.Rows.Count > 0 Then
            WhereClause += " And ( "
            WhereClause += " ( "
            WhereClause += " ins2.ClientAccountNo +" & "'-'" & "+ ins2.ClientAccountBranchCode +" & "'-'" & "+ ins2.ClientAccountCurrency +" & "'-'" & "+ ins2.PaymentNatureCode +" & "'-'" & "+ Convert(Varchar,ins2.CreatedOfficeCode) IN ("
            For Each dr As DataRow In dtAccountNo.Rows
                WhereClause += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "-" & dr("OfficeID").ToString & "',"
            Next
            WhereClause = WhereClause.Remove(WhereClause.Length - 1, 1)
            WhereClause += ")"
            WhereClause += " )"
            If dtAccountNo.Rows(0)(3).ToString = False Then
                WhereClause += " OR (IC_Instruction.CreateBy=@UserIDs)"
                'params.Add("UserIDs", UserIDs)
            End If

        Else
            WhereClause += " OR (IC_Instruction.CreateBy=@UserIDs)"
            'params.Add("UserIDs", UserIDs)
            'ArrAND.Add(qryObjICInstruction.CreateBy = UserIDs)
        End If

        If WhereClause.Contains(" Where  And") Then
            WhereClause = WhereClause.Replace(" Where  And", " Where ")
        End If
        StrQuery += WhereClause
        StrQuery += " )) AND IC_Instruction.InstructionID NOT IN (select IC_InstructionApprovalLog.InstructionID from IC_InstructionApprovalLog where UserID=@UserIDs AND IsAprrovalVoid=0)"
        params.Add("UserIDs", UserIDs)

        Dim util As New esUtility
        dt = util.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)
        Return dt
    End Function
    Private Sub LoadddlProductTypeByAccountPaymentNature()
        Try
            Dim lit As New ListItem
            Dim objICAPNature As New ICAccountsPaymentNature
            objICAPNature.es.Connection.CommandTimeout = 3600
            ddlProductType.Items.Clear()
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlProductType.Items.Add(lit)
            ddlProductType.AppendDataBoundItems = True
            If ddlAcccountPaymentNature.SelectedValue.ToString <> "0" Then
                If objICAPNature.LoadByPrimaryKey(ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2), ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)) Then
                    ddlProductType.DataSource = ICAccountPaymentNatureProductTypeController.GetAllProductTypesByAccountAndPaymentNature(objICAPNature)
                    ddlProductType.DataTextField = "ProductTypeName"
                    ddlProductType.DataValueField = "ProductTypeCode"
                    ddlProductType.DataBind()
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function GetApprovalRulesForUserVIAPaymentNature(ByVal UserID As String, ByVal PaymentNatureCode As DataTable) As ArrayList
        Dim qryObjICApprovalRule As New ICApprovalRuleQuery("qryObjICApprovalRule")
        Dim qryObjICApprovalRuleCond As New ICApprovalRuleConditionsQuery("qryObjICApprovalRuleCond")
        Dim qryObjICApprovalRuleCondUsers As New ICApprovalRuleConditionUsersQuery("qryObjICApprovalRuleCondUsers")
        Dim qryObjICApprovalGroupUsers As New ICApprovalGroupUsersQuery("qryObjICApprovalGroupUsers")
        Dim ApprovalRuleIDList As New ArrayList
        Dim PaymentNatureCodeList As New ArrayList
        PaymentNatureCodeList.Add("-1")

        For Each dr As DataRow In PaymentNatureCode.Rows
            If dr.Table.Columns.Contains("PaymentNatureCode") Then
                PaymentNatureCodeList.Add(dr("PaymentNatureCode"))
            ElseIf dr.Table.Columns.Contains("PaymentNature") Then
                PaymentNatureCodeList.Add(dr("PaymentNature"))
            End If

        Next

        qryObjICApprovalRule.Select(qryObjICApprovalRule.ApprovalRuleID)
        qryObjICApprovalRule.InnerJoin(qryObjICApprovalRuleCond).On(qryObjICApprovalRule.ApprovalRuleID = qryObjICApprovalRuleCond.ApprovalRuleID)
        qryObjICApprovalRule.InnerJoin(qryObjICApprovalRuleCondUsers).On(qryObjICApprovalRuleCond.ApprovalRuleConditionID = qryObjICApprovalRuleCondUsers.ApprovaRuleCondID)
        qryObjICApprovalRule.InnerJoin(qryObjICApprovalGroupUsers).On(qryObjICApprovalRuleCond.ApprovalGroupID = qryObjICApprovalGroupUsers.ApprovalGroupID)
        qryObjICApprovalRule.Where(qryObjICApprovalRule.PaymentNatureCode.In(PaymentNatureCodeList) And qryObjICApprovalRule.IsActive = True)
        qryObjICApprovalRule.Where(qryObjICApprovalRuleCondUsers.UserID = UserID)
        qryObjICApprovalRule.es.Distinct = True
        For Each dr2 As DataRow In qryObjICApprovalRule.LoadDataTable.Rows
            ApprovalRuleIDList.Add(dr2("ApprovalRuleID"))
        Next


        Return ApprovalRuleIDList

    End Function
    
    Private Function GetApprovalRulesPaymentNatureForUser(ByVal UserID As String) As ArrayList
        Dim qryObjICApprovalRule As New ICApprovalRuleQuery("qryObjICApprovalRule")
        Dim qryObjICApprovalRuleCond As New ICApprovalRuleConditionsQuery("qryObjICApprovalRuleCond")
        Dim qryObjICApprovalRuleCondUsers As New ICApprovalRuleConditionUsersQuery("qryObjICApprovalRuleCondUsers")
        Dim qryObjICApprovalGroupUsers As New ICApprovalGroupUsersQuery("qryObjICApprovalGroupUsers")
        Dim PaymentNatureCode As New ArrayList
        PaymentNatureCode.Add("-1")

        qryObjICApprovalRule.Select(qryObjICApprovalRule.PaymentNatureCode)
        qryObjICApprovalRule.InnerJoin(qryObjICApprovalRuleCond).On(qryObjICApprovalRule.ApprovalRuleID = qryObjICApprovalRuleCond.ApprovalRuleID)
        qryObjICApprovalRule.InnerJoin(qryObjICApprovalRuleCondUsers).On(qryObjICApprovalRuleCond.ApprovalRuleConditionID = qryObjICApprovalRuleCondUsers.ApprovaRuleCondID)
        qryObjICApprovalRule.InnerJoin(qryObjICApprovalGroupUsers).On(qryObjICApprovalRuleCond.ApprovalGroupID = qryObjICApprovalGroupUsers.ApprovalGroupID)
        qryObjICApprovalRule.Where(qryObjICApprovalRule.IsActive = True)
        qryObjICApprovalRule.Where(qryObjICApprovalRuleCondUsers.UserID = UserID)
        qryObjICApprovalRule.es.Distinct = True
        For Each dr2 As DataRow In qryObjICApprovalRule.LoadDataTable.Rows
            PaymentNatureCode.Add(dr2("PaymentNatureCode"))
        Next


        Return PaymentNatureCode

    End Function
    Private Sub LoadgvAccountPaymentNatureProductType(ByVal DoDataBind As Boolean)
        Try
            Dim DateType, AccountNumber, BranchCode, Currency, PaymentNatureCode, CompanyCode, ProductTypeCode, InstrumentNo, InstructionNo, BatchCode, ReferenceNo As String
            Dim FromDate, ToDate As String
            Dim dtAccountNumber As New DataTable
            Dim dtPaymentNature As New DataTable
            Dim ApprovalRuleIDList As New ArrayList
            Dim collObjICApprovalRule As New ICApprovalRuleCollection

            Dim MinAmount As Double = 0
            Dim MaxAmount As Double = 0
            dtAccountNumber = ViewState("AccountNumber")
            'dtPaymentNature = ICInstructionApprovalRuleController.GetTaggedPaymentNatureWithUser(ArrICAssignUserRolsID, UserId)
            ApprovalRuleIDList = GetApprovalRulesForUserVIAPaymentNature(Me.UserInfo.UserID, dtAccountNumber)
            'If ApprovalRuleIDList.Count = 0 Then
            '    UIUtilities.ShowDialog(Me, "Error", "You do not have access to any approval criteria. Please contact Al Baraka Administrator.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL(41))
            '    Exit Sub
            'End If
            collObjICApprovalRule = ICApprovalRuleManagementController.GetApprovalRuleCollectionByApprovalRuleID(ApprovalRuleIDList)

            'MinAmount = ICInstructionApprovalRuleController.GetApprovalRulesMinOrMaxAmount(ApprovalRuleIDList, "Min")
            'MaxAmount = ICInstructionApprovalRuleController.GetApprovalRulesMinOrMaxAmount(ApprovalRuleIDList, "Max")


            DateType = ""
            AccountNumber = ""
            BranchCode = ""
            Currency = ""
            Currency = ""
            PaymentNatureCode = ""
            CompanyCode = "0"
            ProductTypeCode = ""
            InstructionNo = ""
            InstrumentNo = ""
            BatchCode = ""
            ReferenceNo = ""
            FromDate = Nothing
            ToDate = Nothing
            If rbtnlstDateSelection.SelectedValue = "Creation Date" Then
                DateType = "Creation Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Value Date" Then
                DateType = "Value Date"
            ElseIf rbtnlstDateSelection.SelectedValue = "Verfication Date" Then
                DateType = "Verfication Date"
            End If
            If ddlAcccountPaymentNature.SelectedValue.ToString <> "0" Then
                AccountNumber = ddlAcccountPaymentNature.SelectedValue.Split("-")(0).ToString
                BranchCode = ddlAcccountPaymentNature.SelectedValue.Split("-")(1).ToString
                Currency = ddlAcccountPaymentNature.SelectedValue.Split("-")(2).ToString
                PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.Split("-")(3).ToString
            End If
            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            End If
            If ddlProductType.SelectedValue.ToString <> "0" Then
                ProductTypeCode = ddlProductType.SelectedValue.ToString
            End If
            If txtInstrumentNo.Text.ToString <> "" And txtInstrumentNo.Text.ToString <> "Enter Instrument No." Then
                InstrumentNo = txtInstrumentNo.Text.ToString
            End If
            If txtInstructionNo.Text.ToString <> "" And txtInstructionNo.Text.ToString <> "Enter Instruction No." Then
                InstructionNo = txtInstructionNo.Text.ToString
            End If
            If txtReferenceNo.Text.ToString <> "" And txtReferenceNo.Text.ToString <> "Enter Reference No." Then
                ReferenceNo = txtReferenceNo.Text.ToString
            End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing Then
                FromDate = raddpCreationFromDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If Not raddpCreationToDate.SelectedDate Is Nothing Then
                ToDate = raddpCreationToDate.SelectedDate.Value.ToString("dd-MMM-yyy")
            End If
            If ddlBatch.SelectedValue <> "0" Then
                BatchCode = ddlBatch.SelectedValue.ToString
            End If
            SetArraListAssignedStatus()
            SetArraListAssignedOfficeIDs()

            GetAllInstructionsForClientApprovalQueue(DateType, FromDate, ToDate, dtAccountNumber, BatchCode, ProductTypeCode, InstructionNo, InstrumentNo, ReferenceNo, Me.gvAuthorization.CurrentPageIndex + 1, Me.gvAuthorization.PageSize, Me.gvAuthorization, DoDataBind, ArrICAssignedStatus, Me.UserId.ToString, 0, "", ArrICAssignedPaymentModes, CompanyCode, Me.UserInfo.UserID, collObjICApprovalRule)
            ClearSummaryAndAccountBalance()
            If gvAuthorization.Items.Count > 0 Then
                gvAuthorization.Visible = True
                lblNRF.Visible = False
                gvAuthorization.Columns(0).Visible = CBool(htRights("Approve"))
                'gvAuthorization.Columns(1).Visible = CBool(htRights("Secondary Approval"))
                gvAuthorization.Columns(1).Visible = CBool(htRights("Cancel"))
                btnApprove.Visible = False
                btnCancelInstructions.Visible = False
                btnSendForAmend.Visible = False
                If CBool(htRights("Approve")) = True Then
                    btnApprove.Visible = True
                End If
                'If CBool(htRights("Secondary Approval")) = True Then
                '    btnApprove.Visible = True
                'End If
                If CBool(htRights("Cancel")) = True Then
                    btnCancelInstructions.Visible = True
                End If
                If CBool(htRights("Send For Amendment")) = True Then
                    btnSendForAmend.Visible = True
                End If

            Else
                btnApprove.Visible = False
                btnCancelInstructions.Visible = False
                btnSendForAmend.Visible = False
                lblNRF.Visible = True
                gvAuthorization.Visible = False
            End If
            pnl2FACode.Style.Add("display", "none")
            rim2FA.Enabled = False


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Public Shared Sub GetAllInstructionsForClientApprovalQueue(ByVal DateType As String, ByVal FromDate As String, ByVal ToDate As String, ByVal AccountNumber As DataTable, ByVal BatchCode As String, ByVal ProductTypeCode As String, ByVal InstructionNo As String, ByVal InstrumentNo As String, ByVal ReferenceNo As String, ByVal CurrentPage As Integer, ByVal PageSize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal InstructionStatus As ArrayList, ByVal UsersID As String, ByVal Amount As Double, ByVal AmountOperator As String, ByVal AssignedPaymentModes As ArrayList, ByVal CompanyCode As String, ByVal UserID As Integer, ByVal ApprovalRuleCollection As ICApprovalRuleCollection)
        Dim dt As DataTable
        Dim params As New EntitySpaces.Interfaces.esParameters

        Dim StrQuery As String = Nothing
        Dim WhereClasue As String = Nothing
        WhereClasue = " Where "
        StrQuery = "Select IC_Instruction.InstructionID,convert(date,IC_Instruction.CreateDate) as CreateDate,convert(date,ValueDate) as ValueDate,Amount,IC_UBPSCompany.CompanyName,IC_Instruction.UBPReferenceField,"
        StrQuery += "isnull(BeneficiaryName,'-') as BeneficiaryName,isnull(BeneficiaryAddress,'-') as BeneficiaryAddress,PaymentMode,ISNULL(InstrumentNo,'-') as InstrumentNo,"
        StrQuery += "ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,ISNULL(IC_Office.OfficeCode + '-' + IC_Office.OfficeName,'-') as OfficeName,"
        StrQuery += "ISNULL(ReferenceNo,'-') as ReferenceNo,IC_Bank.BankName,isnull(PreviousPrintLocationCode,'-') as PreviousPrintLocationCode,"
        StrQuery += "ISNULL(PreviousPrintLocationName,'-') as PreviousPrintLocationName,CONVERT(date,VerificationDate) as VerificationDate,"
        StrQuery += "CONVERT(date,ApprovalDate) as ApprovalDate,CONVERT(date,StaleDate) as staledate,CONVERT(date,CancellationDate) as CancellationDate,"
        StrQuery += "isnull(BeneficiaryAccountNo,'-') as BeneficiaryAccountNo,isnull(BeneAccountBranchCode,'-') as BeneAccountBranchCode,"
        StrQuery += "isnull(BeneAccountCurrency,'-') as BeneAccountCurrency,CONVERT(date,LastPrintDate) as LastPrintDate,"
        StrQuery += "CONVERT(date,RevalidationDate) as revalidationdate,IC_Instruction.ProductTypeCode,IC_Instruction.CompanyCode,IC_Instruction.status,IC_Company.CompanyName,"
        StrQuery += "IC_ProductType.ProductTypeName,"
        StrQuery += "case isnull(convert(varchar,IsAmendmentComplete),CONVERT(varchar,'false')) "
        StrQuery += "when 'false' then "
        StrQuery += "CONVERT(varchar,'false') "
        StrQuery += "else "
        StrQuery += "IsAmendmentComplete "
        StrQuery += " end "
        StrQuery += "as IsAmendmentComplete, "
        StrQuery += "case acquisitionmode "
        StrQuery += "when 'Online Form' then "
        StrQuery += "ic_instruction.FileBatchno else "
        StrQuery += "IC_Instruction.FileBatchNo + '-' + IC_Files.OriginalFileName "
        StrQuery += "end as FileBatchNo,ISNULL(CONVERT(varchar,IC_Instruction.ReIssuanceID),'-') as ReissuanceID "
        StrQuery += "from IC_Instruction "
        StrQuery += "left join IC_UBPSCompany on IC_Instruction.UBPCompanyID=IC_UBPSCompany.UBPSCompanyID "
        StrQuery += "left join IC_Office on IC_Instruction.PrintLocationCode=IC_Office.OfficeID "
        StrQuery += "left join IC_Bank on IC_Instruction.BeneficiaryBankCode=IC_Bank.BankCode "
        StrQuery += "left join IC_Company on IC_Instruction.CompanyCode=IC_Company.CompanyCode "
        StrQuery += "left join IC_ProductType on IC_Instruction.ProductTypeCode=IC_ProductType.ProductTypeCode "
        StrQuery += "left join IC_Files on IC_Instruction.FileID=IC_Files.FileID "
        StrQuery += "Left JOIN IC_AccountsPaymentNatureProductType on IC_Instruction.ClientAccountNo=IC_AccountsPaymentNatureProductType.AccountNumber AND IC_Instruction.ClientAccountBranchCode=IC_AccountsPaymentNatureProductType.BranchCode AND IC_Instruction.ClientAccountCurrency=IC_AccountsPaymentNatureProductType.Currency AND IC_Instruction.PaymentNatureCode=IC_AccountsPaymentNatureProductType.PaymentNatureCode AND IC_Instruction.ProductTypeCode=IC_AccountsPaymentNatureProductType.ProductTypeCode "
        'StrQuery += "Left JOIN IC_InstructionApprovalLog on IC_Instruction.InstructionID=IC_InstructionApprovalLog.InstructionID "
        WhereClasue += " AND IC_AccountsPaymentNatureProductType.IsApproved=1 AND IC_Instruction.InstructionID not in (select InstructionID from IC_InstructionApprovalLog where UserID=" & UserID & " AND IsAprrovalVoid=0)"
        WhereClasue += " AND  "
        WhereClasue += "("
        For Each objICApprovalRule As ICApprovalRule In ApprovalRuleCollection
            WhereClasue += "(IC_Instruction.Amount Between " & objICApprovalRule.FromAmount & " AND " & objICApprovalRule.ToAmount & " ) OR"
        Next

        WhereClasue = WhereClasue.Remove(WhereClasue.Length - 3, 3)
        WhereClasue += ")"
        If InstrumentNo.ToString = "" And InstructionNo.ToString = "" And ReferenceNo.ToString = "" And Amount = 0 Then
            If DateType.ToString = "Creation Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.CreateDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.CreateDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Value Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.ValueDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.ValueDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Approval Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.ApprovalDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Stale Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.StaleDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.StaleDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Cancellation Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.CancellationDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Last Print Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.LastPrintDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Revalidation Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.RevalidationDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            ElseIf DateType.ToString = "Verfication Date" Then
                If Not FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) between CONVERT(date,@FromDate) and CONVERT(date,@ToDate)"
                    params.Add("FromDate", FromDate)
                    params.Add("ToDate", ToDate)
                ElseIf FromDate Is Nothing And Not ToDate Is Nothing Then
                    WhereClasue += " AND convert(date,IC_Instruction.VerificationDate) <= CONVERT(date,@ToDate)"
                    params.Add("ToDate", ToDate)
                End If
            End If
            If ProductTypeCode.ToString <> "" Then
                WhereClasue += " AND IC_Instruction.ProductTypeCode=@ProductTypeCode"
                params.Add("ProductTypeCode", ProductTypeCode)
            End If
            If BatchCode.ToString <> "" Then
                WhereClasue += " AND IC_Instruction.FileBatchNo=@BatchCode"
                params.Add("BatchCode", BatchCode)
            End If
            If CompanyCode.ToString <> "0" Then
                WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                params.Add("CompanyCode", CompanyCode)
            End If
        Else
            If InstructionNo.ToString <> "" Then
                WhereClasue += " AND IC_Instruction.InstructionID=@InstructionNo"
                params.Add("InstructionNo", InstructionNo)
            End If
            If InstrumentNo.ToString <> "" Then
                WhereClasue += " AND InstrumentNo=@InstrumentNo"
                params.Add("InstrumentNo", InstrumentNo)
            End If
            If ReferenceNo.ToString <> "" Then
                WhereClasue += " AND ReferenceNo=@ReferenceNo"
                params.Add("ReferenceNo", ReferenceNo)
            End If
            If CompanyCode.ToString <> "0" Then
                WhereClasue += " AND IC_Instruction.CompanyCode=@CompanyCode"
                params.Add("CompanyCode", CompanyCode)
            End If
            If Not Amount = 0 Then
                If AmountOperator.ToString = "=" Then
                    WhereClasue += " AND Amount = @Amount"
                    params.Add("Amount", Amount)
                ElseIf AmountOperator.ToString = "<" Then
                    WhereClasue += " AND Amount < @Amount"
                    params.Add("Amount", Amount)
                ElseIf AmountOperator.ToString = ">" Then
                    WhereClasue += " AND Amount > @Amount"
                    params.Add("Amount", Amount)
                ElseIf AmountOperator.ToString = "<=" Then
                    WhereClasue += " AND Amount <= @Amount"
                    params.Add("Amount", Amount)
                ElseIf AmountOperator.ToString = ">=" Then
                    WhereClasue += " AND Amount >= @Amount"
                    params.Add("Amount", Amount)
                ElseIf AmountOperator.ToString = "<>" Then
                    WhereClasue += " AND Amount <> @Amount"
                    params.Add("Amount", Amount)
                End If
            End If
        End If
        If InstructionStatus.Count > 0 Then
            WhereClasue += " AND IC_Instruction.Status IN ("
            For i = 0 To InstructionStatus.Count - 1
                WhereClasue += InstructionStatus(i) & ","
            Next
            WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
            WhereClasue += ")"
        End If
        WhereClasue += " AND ( "
        Dim x As Integer = 0
        If AccountNumber.Rows.Count > 0 Then
            WhereClasue += " ( "
            WhereClasue += "ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + IC_Instruction.PaymentNatureCode + '" & "-" & "' + Convert(Varchar,CreatedOfficeCode) IN ("
            For Each dr As DataRow In AccountNumber.Rows
                'If x = 100 Then
                '    x = 0
                '    WhereClasue += ")"
                '    WhereClasue += " ( "
                '    WhereClasue += "ClientAccountNo +'" & "-" & "'+ ClientAccountBranchCode + '" & "-" & "'+ ClientAccountCurrency + '" & "-" & "' + PaymentNatureCode + '" & "-" & "' + Convert(Varchar,CreatedOfficeCode) IN ("
                'End If
                WhereClasue += "'" & dr("AccountNumber").ToString.Split("-")(0) & "-" & dr("AccountNumber").ToString.Split("-")(1) & "-" & dr("AccountNumber").ToString.Split("-")(2) & "-" & dr("PaymentNature").ToString & "-" & dr("OfficeID").ToString & "',"
                x = x + 1
            Next
            WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
            WhereClasue += ")"
            WhereClasue += " )"
            If AccountNumber(0)(3) = False Then
                WhereClasue += " OR (IC_Instruction.CreateBy=@UsersID)"
                params.Add("UsersID", UsersID)
            End If
        Else
            WhereClasue += " OR (IC_Instruction.CreateBy=@UsersID)"
            params.Add("UsersID", UsersID)
        End If
        WhereClasue += " ) "

        If AssignedPaymentModes.Count > 0 And Not AssignedPaymentModes Is Nothing Then
            WhereClasue += "AND PaymentMode IN ("
            For i = 0 To AssignedPaymentModes.Count - 1
                WhereClasue += "'" & AssignedPaymentModes(i) & "',"
            Next
            WhereClasue = WhereClasue.Remove(WhereClasue.Length - 1, 1)
            WhereClasue += ")"
        End If
        If WhereClasue.Contains(" Where  AND ") = True Then
            WhereClasue = WhereClasue.Replace(" Where  AND ", "Where ")
        End If
        StrQuery += WhereClasue
        StrQuery += " Order By IC_Instruction.InstructionID Desc"
        Dim utill As New esUtility()
        dt = New DataTable
        dt = utill.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)
        If Not CurrentPage = 0 Then

            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        Else
            rg.DataSource = dt
            If DoDataBind = True Then
                rg.DataBind()
            End If
        End If

    End Sub
    Private Sub ClearSummaryAndAccountBalance()
        txtTnxCount.Text = ""
        txtTotalAmount.Text = ""
        txtAvailableBalance.Text = ""
        LoadddlAccountNumbers()
    End Sub
    Private Sub SetArraListAssignedPaymentModes()
        ArrICAssignedPaymentModes.Clear()
        'If CBool(htRights("Cheque Instructions")) = True Then
        '    ArrICAssignedPaymentModes.Add("Cheque")
        'End If
        'If CBool(htRights("DD Instructions")) = True Then
        '    ArrICAssignedPaymentModes.Add("DD")
        'End If
        'If CBool(htRights("PO Instructions")) = True Then
        '    ArrICAssignedPaymentModes.Add("PO")
        'End If
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Try
                LoadgvAccountPaymentNatureProductType(True)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            'SetArraListRoleID()
            LoadddlBatcNumber()
            LoadddlAccountNumbers()
            LoadddlAccountPaymentNatureByUserID()
            LoadddlProductTypeByAccountPaymentNature()

            LoadgvAccountPaymentNatureProductType(True)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlAcccountPaymentNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcccountPaymentNature.SelectedIndexChanged
        Try
            If ddlAcccountPaymentNature.SelectedValue.ToString = "0" Then
                SetArraListRoleID()

                LoadddlAccountNumbers()
                LoadddlProductTypeByAccountPaymentNature()

                FillDesignedDtByAPNatureByAssignedRole()
                LoadddlBatcNumber()
                LoadgvAccountPaymentNatureProductType(True)
            Else
FillDesignedDtByAPNatureByAssignedRole()
                LoadddlAccountNumbers()
                LoadddlProductTypeByAccountPaymentNature()

                SetViewStateDtOnAPNatureIndexChnage()
                LoadddlBatcNumber()
                LoadgvAccountPaymentNatureProductType(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetViewStateDtOnAPNatureIndexChnage()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim AccountNumber, BranchCode, Currency, PaymentNatureCode As String
        Dim ArrayListOfficeID As New ArrayList
        Dim drAccountNo As DataRow
        AccountNumber = Nothing
        BranchCode = Nothing
        Currency = Nothing
        PaymentNatureCode = Nothing
        dtAccountNumber = ViewState("AccountNumber")
        ViewState("AccountNumber") = Nothing
        AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0)
        BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1)
        Currency = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2)
        PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)
        For Each dr As DataRow In dtAccountNumber.Rows
            If dr("AccountNumber").ToString.Split("-")(0) = AccountNumber And dr("AccountNumber").ToString.Split("-")(1) = BranchCode And dr("AccountNumber").ToString.Split("-")(2) = Currency And dr("PaymentNature").ToString = PaymentNatureCode Then
                ArrayListOfficeID.Add(dr("OfficeID").ToString)
            End If
        Next
        dtAccountNumber.Rows.Clear()
        If ArrayListOfficeID.Count > 0 Then
            For Each OfficeID In ArrayListOfficeID
                drAccountNo = dtAccountNumber.NewRow()
                drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
                drAccountNo("PaymentNature") = PaymentNatureCode
                drAccountNo("OfficeID") = OfficeID
                dtAccountNumber.Rows.Add(drAccountNo)
            Next
        Else
            drAccountNo = dtAccountNumber.NewRow()
            drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
            drAccountNo("PaymentNature") = PaymentNatureCode
            drAccountNo("OfficeID") = "0"
            dtAccountNumber.Rows.Add(drAccountNo)
        End If
        'dtAccountNumber.Columns.Add(New DataColumn("DropDown", GetType(System.String)))
        For Each dr As DataRow In dtAccountNumber.Rows
            dr("DropDown") = "True"
        Next
        ViewState("AccountNumber") = dtAccountNumber


        'Dim dtAccountNumber As New DataTable
        'Dim dtPaymentNature As New DataTable
        'Dim AccountNumber, BranchCode, Currency, PaymentNatureCode As String
        'Dim ArrayListOfficeID As New ArrayList
        'Dim drAccountNo As DataRow
        'AccountNumber = Nothing
        'BranchCode = Nothing
        'Currency = Nothing
        'PaymentNatureCode = Nothing
        'dtAccountNumber = ViewState("AccountNumber")
        'AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0)
        'BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1)
        'Currency = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2)
        'PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)
        'For Each dr As DataRow In dtAccountNumber.Rows
        '    If dr("AccountNumber").ToString.Split("-")(0) = AccountNumber And dr("AccountNumber").ToString.Split("-")(1) = BranchCode And dr("AccountNumber").ToString.Split("-")(2) = Currency And dr("PaymentNature").ToString = PaymentNatureCode Then
        '        ArrayListOfficeID.Add(dr("OfficeID").ToString)
        '    End If
        'Next
        'dtAccountNumber.Rows.Clear()
        'If ArrayListOfficeID.Count > 0 Then
        '    For Each OfficeID In ArrayListOfficeID
        '        drAccountNo = dtAccountNumber.NewRow()
        '        drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
        '        drAccountNo("PaymentNature") = PaymentNatureCode
        '        drAccountNo("OfficeID") = OfficeID
        '        dtAccountNumber.Rows.Add(drAccountNo)
        '    Next
        'Else
        '    drAccountNo = dtAccountNumber.NewRow()
        '    drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
        '    drAccountNo("PaymentNature") = PaymentNatureCode
        '    drAccountNo("OfficeID") = "0"
        '    dtAccountNumber.Rows.Add(drAccountNo)
        'End If

        'ViewState("AccountNumber") = dtAccountNumber



    End Sub
    Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged
        Try
            SetArraListRoleID()



            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub



    Protected Sub gvAuthorization_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvAuthorization.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    'Private Sub SetJavaScript()
    '    Dim imgBtn As ImageButton
    '    'If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Then
    '    Dim AppPath As String = DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
    '    For Each gvAuthorizationRow As GridDataItem In gvAuthorization.Items
    '        imgBtn = New ImageButton
    '        imgBtn = DirectCast(gvAuthorizationRow.Cells(15).FindControl("ibButton"), ImageButton)
    '        imgBtn.OnClientClick = "showurldialog('Instruction Details','" & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & imgBtn.CommandArgument.ToString() & "' ,true,700,650);return false;"
    '    Next
    '    'End If
    'End Sub

    Protected Sub gvAuthorization_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAuthorization.ItemDataBound

        Dim chkProcessPrimary As CheckBox
        Dim chkProcessSecondary As CheckBox
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            If CBool(htRights("Approve")) = True And CBool(htRights("Cancel")) = True Then
                Dim chkProcessAllPrimary As New CheckBox
                'Dim chkProcessAllSecondary As New CheckBox
                Dim chkProcessAllCancel As New CheckBox

                chkProcessAllPrimary = DirectCast(e.Item.Cells(0).FindControl("chkSelectAllPrimary"), CheckBox)
                'chkProcessAllSecondary = DirectCast(e.Item.Cells(1).FindControl("chkSelectAllSecondary"), CheckBox)
                chkProcessAllCancel = DirectCast(e.Item.Cells(1).FindControl("chkSelectAllCancel"), CheckBox)

                chkProcessAllPrimary.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAllPrimary.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','0');"
                'chkProcessAllSecondary.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAllSecondary.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','1');"
                chkProcessAllCancel.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAllCancel.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','1');"

            ElseIf CBool(htRights("Approve")) = True And CBool(htRights("Cancel")) = False Then
                Dim chkProcessAllPrimary As New CheckBox
                chkProcessAllPrimary = DirectCast(e.Item.Cells(0).FindControl("chkSelectAllPrimary"), CheckBox)
                chkProcessAllPrimary.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAllPrimary.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','0');"
           
            ElseIf CBool(htRights("Cancel")) = True And CBool(htRights("Approve")) = False Then

                Dim chkProcessAllCancel As New CheckBox
                chkProcessAllCancel = DirectCast(e.Item.Cells(1).FindControl("chkSelectAllCancel"), CheckBox)
                chkProcessAllCancel.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAllCancel.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','0');"
                'ElseIf CBool(htRights("Cancel")) = True And CBool(htRights("Primary Approval")) = True And CBool(htRights("Secondary Approval")) = False Then

                '    Dim chkProcessAllCancel As New CheckBox
                '    Dim chkProcessAllPrimary As New CheckBox
                '    chkProcessAllPrimary = DirectCast(e.Item.Cells(0).FindControl("chkSelectAllPrimary"), CheckBox)
                '    chkProcessAllPrimary.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAllPrimary.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','0');"

                '    chkProcessAllCancel = DirectCast(e.Item.Cells(2).FindControl("chkSelectAllCancel"), CheckBox)
                '    chkProcessAllCancel.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAllCancel.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','1');"
                'ElseIf CBool(htRights("Cancel")) = True And CBool(htRights("Primary Approval")) = False And CBool(htRights("Secondary Approval")) = True Then

                '    Dim chkProcessAllCancel As New CheckBox
                '    Dim chkProcessAllSecondary As New CheckBox
                '    chkProcessAllSecondary = DirectCast(e.Item.Cells(0).FindControl("chkSelectAllSecondary"), CheckBox)
                '    chkProcessAllSecondary.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAllSecondary.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','0');"

                '    chkProcessAllCancel = DirectCast(e.Item.Cells(2).FindControl("chkSelectAllCancel"), CheckBox)
                '    chkProcessAllCancel.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAllCancel.ClientID & "','" & gvAuthorization.MasterTableView.ClientID & "','1');"

            End If

        End If



        'If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
        '    Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        '    chkProcessPrimary = New CheckBox
        '    chkProcessSecondary = New CheckBox
        '    chkProcessPrimary = DirectCast(e.Item.Cells(0).FindControl("chkSelectPrimary"), CheckBox)
        '    'chkProcessSecondary = DirectCast(e.Item.Cells(1).FindControl("chkSelectSecondary"), CheckBox)


        '    If CBool(htRights("Primary Approval")) = True And CBool(htRights("Secondary Approval")) = True Then

        '        If item.GetDataKeyValue("Status").ToString = "8" Then
        '            chkProcessPrimary.Enabled = True
        '            chkProcessPrimary.Checked = False
        '            chkProcessSecondary.Enabled = False
        '            chkProcessSecondary.Checked = False
        '        ElseIf item.GetDataKeyValue("Status").ToString = "10" Then
        '            chkProcessPrimary.Enabled = False
        '            chkProcessPrimary.Checked = True
        '            chkProcessSecondary.Enabled = True
        '            chkProcessSecondary.Checked = False
        '        End If
        '    End If
        'End If

        Dim AppPath As String = "" 'DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(0).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(1).ToString() & "/" & DotNetNuke.Entities.Tabs.TabController.CurrentPage.FullUrl.Split("/")(2).ToString()
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim imgBtn As LinkButton
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            imgBtn = New LinkButton
            imgBtn = DirectCast(item.Cells(2).FindControl("lbInstructionID"), LinkButton)
            imgBtn.OnClientClick = "showurldialog('Instruction Details',' " & AppPath & "/ShowInstructionDetails.aspx?InstructionID=" & item.GetDataKeyValue("InstructionID").ToString().EncryptString() & "' ,true,700,650);return false;"
            If CBool(htRights("View Instruction Details")) = True Then
                imgBtn.Enabled = True

            End If
            If item.GetDataKeyValue("IsAmendmentComplete") = True Then

                gvAuthorization.AlternatingItemStyle.CssClass = "GridItemComplete"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItemComplete"


            Else
                gvAuthorization.AlternatingItemStyle.CssClass = "GridItem"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItem"


            End If
        End If
    End Sub
    Protected Sub gvAuthorization_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAuthorization.NeedDataSource
        Try
            'ClearAllTextBoxes()
            'SetArraListRoleID()
            LoadgvAccountPaymentNatureProductType(False)
            'SetJavaScript()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlBatch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBatch.SelectedIndexChanged
        Try
            SetArraListRoleID()
            LoadddlAccountNumbers()
            LoadddlProductTypeByAccountPaymentNature()

            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub ClearAllTextBoxes()
        txtRemarks.Text = ""
        txtInstructionNo.Text = ""
        txtInstrumentNo.Text = ""
        txtReferenceNo.Text = ""
        pnl2FACode.Style.Add("display", "none")
        txt2FACode.Text = Nothing
    End Sub
    Private Function CheckgvInstructionAuthentication() As Boolean
        Try

            Dim chkSelectAllPrimary As CheckBox
            Dim chkSelectAllSecondary As CheckBox
            Dim chkSelectCancel As CheckBox
            Dim Result As Boolean = False
            For Each rowGVInstruction In gvAuthorization.Items
                chkSelectAllPrimary = New CheckBox
                chkSelectAllSecondary = New CheckBox
                chkSelectAllPrimary = DirectCast(rowGVInstruction.Cells(0).FindControl("chkSelectPrimary"), CheckBox)
                chkSelectCancel = DirectCast(rowGVInstruction.Cells(1).FindControl("chkSelectCancel"), CheckBox)
                If ((chkSelectAllPrimary.Checked = True And chkSelectAllPrimary.Enabled = True)) Or (chkSelectCancel.Checked = True And chkSelectCancel.Visible = True And chkSelectCancel.Enabled = True) Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSummary.Click

        Try

                Dim InstructionCount As Double = 0
                Dim TotalAmount As Double = 0
                Dim TotalAmountInInstruction As Double

                Dim chkSelectPrimary As CheckBox
                Dim chkSelectSecondary As CheckBox
                Dim chkSelectCancel As CheckBox
                If CheckgvInstructionAuthentication() = False Then
                    UIUtilities.ShowDialog(Me, "Instruction Verification", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    txtTnxCount.Text = ""
                    txtTotalAmount.Text = ""
                    Exit Sub
                End If
                txtTnxCount.Text = ""
                txtTotalAmount.Text = ""

                For Each gvInsctructionAuthenticationRow As GridDataItem In gvAuthorization.Items
                    chkSelectPrimary = New CheckBox
                    chkSelectSecondary = New CheckBox
                    chkSelectCancel = New CheckBox
                    chkSelectPrimary = DirectCast(gvInsctructionAuthenticationRow.Cells(0).FindControl("chkSelectPrimary"), CheckBox)
                    chkSelectCancel = DirectCast(gvInsctructionAuthenticationRow.Cells(1).FindControl("chkSelectCancel"), CheckBox)
                    If (chkSelectPrimary.Checked = True And chkSelectPrimary.Enabled = True) Or (chkSelectCancel.Checked = True And chkSelectCancel.Enabled = True) Then
                        TotalAmountInInstruction = 0


                        InstructionCount = InstructionCount + 1
                        TotalAmountInInstruction = CDbl(gvInsctructionAuthenticationRow.Item("Amount").Text)
                        TotalAmount = TotalAmount + TotalAmountInInstruction





                    End If
                Next

                If Not TotalAmount = 0 Then
                    txtTotalAmount.Text = CDbl(TotalAmount).ToString("N2")
                End If
                If Not InstructionCount = 0 Then
                    txtTnxCount.Text = CInt(InstructionCount)
                End If

                InstructionCount = 0
                TotalAmount = 0

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try

    End Sub
    Private Function CheckgvClientRoleCheckedForProcessAll(ByVal ForApproval As Boolean) As Boolean
        Try
            Dim rowGVICClientRoles As GridDataItem
            Dim chkSelectPrimaryApproval As CheckBox
            Dim chkSelectSecondaryApproval As CheckBox
            Dim chkSelectCancel As CheckBox
            Dim Result As Boolean = False
            For Each rowGVICClientRoles In gvAuthorization.Items
                chkSelectPrimaryApproval = New CheckBox
                chkSelectSecondaryApproval = New CheckBox
                chkSelectCancel = New CheckBox
                chkSelectPrimaryApproval = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelectPrimary"), CheckBox)
                'chkSelectSecondaryApproval = DirectCast(rowGVICClientRoles.Cells(1).FindControl("chkSelectSecondary"), CheckBox)
                chkSelectCancel = DirectCast(rowGVICClientRoles.Cells(1).FindControl("chkSelectCancel"), CheckBox)
                If ForApproval = True Then
                    If (chkSelectPrimaryApproval.Checked = True And chkSelectPrimaryApproval.Enabled = True) Then
                        Result = True
                        Exit For
                    End If
                Else
                    If chkSelectCancel.Checked = True Then
                        Result = True
                        Exit For
                    End If
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Dim objUser As New ICUser
        Dim objICInstruction As ICInstruction
        If Page.IsValid Then
            Try
                If CheckgvClientRoleCheckedForProcessAll(True) = False Then
                    UIUtilities.ShowDialog(Me, "Instruction Verification", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If objUser.LoadByPrimaryKey(Me.UserId) Then
                    Dim dt As New DataTable
                    dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                    dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                    dt.Columns.Add(New DataColumn("Message", GetType(System.String)))
                    If objUser.Is2FARequiredOnApproval = True Then
                        If IC2FAController.Generate2FACodeAndSend(Me.UserId, "Approval") Then
                            btnOK2FA.Text = "OK"
                            txt2FACode.Text = Nothing
                            txt2FACode.Enabled = True
                            rim2FA.Enabled = True
                            btnOK2FA.Visible = True
                            pnl2FACode.Style.Remove("display")
                            btnApprove.Visible = False
                            btnSendForAmend.Visible = False
                            btnCancelInstructions.Visible = False
                        End If
                    Else
                        Dim chkSelectPrimaryApproval As CheckBox
                        Dim chkSelectSecondaryApproval As CheckBox
                        Dim ArrayListInstructionID As New ArrayList
                        Dim StrInstCount As String = Nothing
                        Dim StrArr As String() = Nothing

                        For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                            chkSelectPrimaryApproval = New CheckBox
                            chkSelectSecondaryApproval = New CheckBox
                            chkSelectPrimaryApproval = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelectPrimary"), CheckBox)
                            'chkSelectSecondaryApproval = DirectCast(gvVerificationRow.Cells(1).FindControl("chkSelectSecondary"), CheckBox)
                            If (chkSelectPrimaryApproval.Checked = True And chkSelectPrimaryApproval.Enabled = True) Then
                                objICInstruction = New ICInstruction
                                objICInstruction.LoadByPrimaryKey(gvVerificationRow.GetDataKeyValue("InstructionID"))
                                If objICInstruction.Status.ToString = "8" And objICInstruction.UpToICCompanyByCompanyCode.SkipPaymentQueue = True And (objICInstruction.PaymentMode = "PO" Or objICInstruction.PaymentMode = "DD" Or objICInstruction.PaymentMode = "Direct Credit" Or objICInstruction.PaymentMode = "Other Credit" Or objICInstruction.PaymentMode = "COTC" Or objICInstruction.PaymentMode = "Bill Payment") Then
                                    ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "52", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE")
                                    ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
                                ElseIf objICInstruction.Status.ToString = "8" Then
                                    ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
                                ElseIf objICInstruction.Status.ToString = "10" And objICInstruction.UpToICCompanyByCompanyCode.SkipPaymentQueue = False Then
                                    ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
                                End If
                            End If
                        Next

                        If ArrayListInstructionID.Count > 0 Then
                            Dim Remarks As String = ""
                            If txtRemarks.Text <> "" And txtRemarks.Text <> "Enter Remarks" Then
                                Remarks = txtRemarks.Text.ToString
                            End If
                            StrInstCount = ICInstructionProcessController.PerformClientPrimaryOrSecondaryApprovalOnInstruction(ArrayListInstructionID, Me.UserId.ToString, Me.UserInfo.Username.ToString, Remarks, dt)
                        Else
                            UIUtilities.ShowDialog(Me, "Error", "Invalid selected instruction(s) status", Dialogmessagetype.Failure, NavigateURL(41))
                            Exit Sub
                        End If
                        RefreshPage()
                        If dt.Rows.Count > 0 Then
                            gvShowSummary.DataSource = Nothing
                            gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                            gvShowSummary.DataBind()
                            pnlShowSummary.Style.Remove("display")
                            Dim scr As String
                            scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                            Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                        Else
                            gvShowSummary.DataSource = Nothing
                            pnlShowSummary.Style.Add("display", "none")
                        End If

                    End If
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    
    Protected Sub btnCancelInstructions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelInstructions.Click
        'Page.Validate()

        'If Page.IsValid Then
        Try
            Dim chkProcessAllCancel As CheckBox
            Dim FailToCancelCount As Integer = 0
            Dim PassToCancelCount As Integer
            Dim StrAuditTrailAction As String
            Dim objICInstruction As ICInstruction
            Dim ArrayListInstID As New ArrayList
            Dim dr As DataRow
            If CheckgvClientRoleCheckedForProcessAll(False) = False Then
                UIUtilities.ShowDialog(Me, "Instruction Verification", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            Dim dt As New DataTable
            dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
            dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
            dt.Columns.Add(New DataColumn("Message", GetType(System.String)))
            For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                chkProcessAllCancel = New CheckBox
                chkProcessAllCancel = DirectCast(gvVerificationRow.Cells(1).FindControl("chkSelectCancel"), CheckBox)
                If chkProcessAllCancel.Checked = True Then
                    dr = dt.NewRow
                    objICInstruction = New ICInstruction
                    Try

                        objICInstruction.es.Connection.CommandTimeout = 3600
                        objICInstruction.LoadByPrimaryKey(gvVerificationRow.GetDataKeyValue("InstructionID"))
                        ArrayListInstID.Add(objICInstruction.InstructionID)
                        StrAuditTrailAction = Nothing
                        StrAuditTrailAction += "Instruction with ID [ " & objICInstruction.InstructionID & " ]  updated."
                        StrAuditTrailAction += "cancelled by user [ " & Me.UserInfo.Username & " ]"
                        StrAuditTrailAction += "by user [ " & Me.UserInfo.UserID & " ] [ " & Me.UserInfo.Username & " ]"
                        Dim Remarks As String = ""
                        If txtRemarks.Text <> "" And txtRemarks.Text <> "Enter Remarks" Then
                            Remarks = txtRemarks.Text.ToString
                        End If
                        objICInstruction.CancellationDate = Date.Now
                        ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString, StrAuditTrailAction, "UPDATE")
                        ICInstructionController.UpdateInstructionStatus(gvVerificationRow.GetDataKeyValue("InstructionID"), gvVerificationRow.GetDataKeyValue("Status"), "5", Me.UserId.ToString, Me.UserInfo.Username.ToString, "Remarks", "CANCEL", Remarks)

                        If objICInstruction.PaymentMode = "Cheque" And objICInstruction.InstrumentNo IsNot Nothing And objICInstruction.InstrumentNo <> "" And (objICInstruction.PrintLocationCode IsNot Nothing And objICInstruction.PrintLocationCode.ToString <> "") Then

                            ICInstrumentController.MarkInstrumentNumberIsCancelledByInstructionIDAndInstrumentNumberAndOfficeCode(objICInstruction.InstrumentNo.ToString, objICInstruction.PrintLocationCode.ToString, objICInstruction.InstructionID.ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)

                        End If

                        dr("InstructionID") = objICInstruction.InstructionID.ToString
                        dr("Message") = "Instruction(s) cancelled successfully"
                        dr("Status") = "5"
                        dt.Rows.Add(dr)
                        PassToCancelCount = PassToCancelCount + 1
                        Continue For
                    Catch ex As Exception
                        'ICUtilities.AddAuditTrail("Instruction Skip:Cancel instruction fails on client primary approval queue due to " & ex.Message.ToString, "Instruction", objICInstruction.InstructionID.ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "SKIP")
                        objICInstruction.SkipReason = "Instruction Skip:Cancel instruction with ID [ " & objICInstruction.InstructionID & " ] fails on client primary approval queue due to " & ex.Message.ToString & ". Action was taken by [ " & Me.UserId & " ] [ " & Me.UserInfo.Username & " ]."
                        ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        dr("InstructionID") = objICInstruction.InstructionID.ToString
                        'dr("Message") = "Instruction not cancelled due to " & ex.Message.ToString
                        dr("Message") = "Instruction(s) skipped"
                        dr("Status") = objICInstruction.Status.ToString
                        dt.Rows.Add(dr)
                        FailToCancelCount = FailToCancelCount + 1
                        Continue For
                    End Try
                End If
            Next



            RefreshPage()
            If dt.Rows.Count > 0 Then
                gvShowSummary.DataSource = Nothing
                gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                gvShowSummary.DataBind()
                pnlShowSummary.Style.Remove("display")
                Dim scr As String
                scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
            Else
                gvShowSummary.DataSource = Nothing
                pnlShowSummary.Style.Add("display", "none")
            End If


            If Not PassToCancelCount = 0 Then
                Dim dtInstructionInfo As New DataTable
                Dim ArrayListStatus As New ArrayList
                ArrayListStatus.Clear()
                ArrayListStatus.Add(5)
                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArrayListInstID, ArrayListStatus, False)
                EmailUtilities.Cancelledtransactions(dtInstructionInfo, Me.UserInfo.Username.ToString, "APNatureAndLocation")
                SMSUtilities.Cancelledtransactions(Me.UserInfo.Username.ToString, dtInstructionInfo, ArrayListStatus, ArrayListInstID, "APNatureAndLocation")
            End If



        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
        'End If
    End Sub

    Protected Sub btnSendForAmend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendForAmend.Click
        If Page.IsValid Then
            Try
                Dim chkSelectPrimaryApproval As CheckBox
                Dim chkSelectSecondaryApproval As CheckBox
                Dim ArryListInstructionId As New ArrayList

                Dim FailToCancelCount As Integer = 0
                Dim PassToCancelCount As Integer
                Dim ArrayListStatus As New ArrayList
                Dim dtInstructionInfo As New DataTable
                Dim objICInstruction As ICInstruction
                Dim dr As DataRow
                ArryListInstructionId.Clear()
                If CheckgvClientRoleCheckedForProcessAll(True) = False Then
                    UIUtilities.ShowDialog(Me, "Instruction Verification", "Please select atleast one instruction.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                Dim dt As New DataTable
                dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                dt.Columns.Add(New DataColumn("Message", GetType(System.String)))
                For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                    chkSelectPrimaryApproval = New CheckBox
                    chkSelectSecondaryApproval = New CheckBox
                    chkSelectPrimaryApproval = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelectPrimary"), CheckBox)
                    'chkSelectSecondaryApproval = DirectCast(gvVerificationRow.Cells(1).FindControl("chkSelectSecondary"), CheckBox)
                    If (chkSelectPrimaryApproval.Checked = True And chkSelectPrimaryApproval.Enabled = True) Then
                        dr = dt.NewRow
                        Try
                            ArryListInstructionId.Add(gvVerificationRow.GetDataKeyValue("InstructionID"))

                            ICInstructionController.UpdateInstructionStatus(gvVerificationRow.GetDataKeyValue("InstructionID"), gvVerificationRow.GetDataKeyValue("Status"), "6", Me.UserId.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE", txtRemarks.Text.ToString)
                            ICInstructionApprovalRuleController.MarkAllApprovalsVoidByInstructionID(gvVerificationRow.GetDataKeyValue("InstructionID").ToString, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString)
                            dr("InstructionID") = gvVerificationRow.GetDataKeyValue("InstructionID").ToString
                            dr("Message") = "Instruction(s) sent for amendment successfully"
                            dr("Status") = "6"
                            dt.Rows.Add(dr)
                            PassToCancelCount = PassToCancelCount + 1
                        Catch ex As Exception
                            'ICUtilities.AddAuditTrail("Instruction Skip:Send for amendment for instruction with ID [ " & gvVerificationRow.GetDataKeyValue("InstructionID") & " ] fails on client primary approval queue due to " & ex.Message.ToString, "Instruction", gvVerificationRow.GetDataKeyValue("InstructionID").ToString, Me.UserId.ToString, Me.UserInfo.Username.ToString, "SKIP")
                            objICInstruction = New ICInstruction
                            objICInstruction.LoadByPrimaryKey(gvVerificationRow.GetDataKeyValue("InstructionID").ToString)
                            objICInstruction.SkipReason = "Instruction Skip:Send for amendment for instruction with ID [ " & gvVerificationRow.GetDataKeyValue("InstructionID") & " ] fails on client primary approval queue due to " & ex.Message.ToString & ". Action was taken by [ " & Me.UserId & " ] [ " & Me.UserInfo.Username & " ]."
                            ICInstructionController.UpdateInstructionSkipReason(objICInstruction, Me.UserId.ToString, Me.UserInfo.Username.ToString)
                            dr("InstructionID") = gvVerificationRow.GetDataKeyValue("InstructionID").ToString
                            dr("Message") = "Instruction(s) skipped"
                            dr("Status") = gvVerificationRow.GetDataKeyValue("Status").ToString
                            dt.Rows.Add(dr)
                            FailToCancelCount = FailToCancelCount + 1
                            Continue For
                        End Try
                    End If
                Next


                RefreshPage()
                If dt.Rows.Count > 0 Then
                    gvShowSummary.DataSource = Nothing
                    gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                    gvShowSummary.DataBind()
                    pnlShowSummary.Style.Remove("display")
                    Dim scr As String
                    scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                Else
                    gvShowSummary.DataSource = Nothing
                    pnlShowSummary.Style.Add("display", "none")
                End If


                ArrayListStatus.Clear()
                ArrayListStatus.Add(6)
                dtInstructionInfo = ICInstructionController.GetAllVerifiedAndUnverifiedInstructionsInVerificationQueueVIACreatedLocation(ArryListInstructionId, ArrayListStatus, False)
                EmailUtilities.TxnsendforAmendment(dtInstructionInfo, Me.UserInfo.Username.ToString)
                SMSUtilities.TxnsendforAmendmentByAprover(Me.UserInfo.Username.ToString, dtInstructionInfo, ArrayListStatus, ArryListInstructionId)


            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
#Region "2FA Code"
    Protected Sub btnOK2FA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK2FA.Click
        If Page.IsValid Then
            Try
                Dim objICInstruction As ICInstruction
                If btnOK2FA.Text = "OK" Then
                    Dim dt As New DataTable
                    dt.Columns.Add(New DataColumn("InstructionID", GetType(System.String)))
                    dt.Columns.Add(New DataColumn("Status", GetType(System.String)))
                    dt.Columns.Add(New DataColumn("Message", GetType(System.String)))
                    If IC2FAController.Check2FAExistAndValid(Me.UserId, txt2FACode.Text.ToString().Trim(), "Approval") Then
                        If ICBO.IC.IC2FAController.Update2FACodeStatusAfterUse(Me.UserId, "Approval") Then
                            Dim chkSelectPrimaryApproval As CheckBox
                            Dim chkSelectSecondaryApproval As CheckBox
                            Dim ArrayListInstructionID As New ArrayList
                            Dim StrInstCount As String = Nothing
                            Dim StrArr As String() = Nothing
                            pnl2FACode.Style.Add("display", "none")
                            For Each gvVerificationRow As GridDataItem In gvAuthorization.Items
                                chkSelectPrimaryApproval = New CheckBox
                                chkSelectSecondaryApproval = New CheckBox
                                chkSelectPrimaryApproval = DirectCast(gvVerificationRow.Cells(0).FindControl("chkSelectPrimary"), CheckBox)
                                'chkSelectSecondaryApproval = DirectCast(gvVerificationRow.Cells(1).FindControl("chkSelectSecondary"), CheckBox)
                                If (chkSelectPrimaryApproval.Checked = True And chkSelectPrimaryApproval.Enabled = True) Then
                                    objICInstruction = New ICInstruction
                                    objICInstruction.LoadByPrimaryKey(gvVerificationRow.GetDataKeyValue("InstructionID"))
                                    If objICInstruction.Status.ToString = "8" And objICInstruction.UpToICCompanyByCompanyCode.SkipPaymentQueue = True And (objICInstruction.PaymentMode = "PO" Or objICInstruction.PaymentMode = "DD" Or objICInstruction.PaymentMode = "Direct Credit" Or objICInstruction.PaymentMode = "Other Credit" Or objICInstruction.PaymentMode = "COTC" Or objICInstruction.PaymentMode = "Bill Payment") Then
                                        ICInstructionController.UpdateInstructionStatus(objICInstruction.InstructionID.ToString, objICInstruction.Status.ToString, "52", Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString, "Remarks", "UPDATE")
                                        ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
                                    ElseIf objICInstruction.Status.ToString = "8" Then
                                        ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
                                    ElseIf objICInstruction.Status.ToString = "10" And objICInstruction.UpToICCompanyByCompanyCode.SkipPaymentQueue = False Then
                                        ArrayListInstructionID.Add(objICInstruction.InstructionID.ToString)
                                    End If
                                End If
                            Next
                            Dim Remarks As String = ""
                            If txtRemarks.Text <> "" And txtRemarks.Text <> "Enter Remarks" Then
                                Remarks = txtRemarks.Text.ToString
                            End If
                            If ArrayListInstructionID.Count > 0 Then
                                StrInstCount = ICInstructionProcessController.PerformClientPrimaryOrSecondaryApprovalOnInstruction(ArrayListInstructionID, Me.UserId.ToString, Me.UserInfo.Username.ToString, txtRemarks.Text.ToString, dt)
                            Else
                                UIUtilities.ShowDialog(Me, "Error", "Invalid selected instruction(s) status", Dialogmessagetype.Failure, NavigateURL(41))
                                Exit Sub
                            End If
                            StrArr = StrInstCount.Split(",")
                            'If StrArr(1).ToString = "0" And StrArr(0).ToString <> "0" Then
                            '    ShowError("[ " & StrArr(0).ToString & " ] Instruction(s) approved successfully.")
                            'ElseIf StrArr(1).ToString <> "0" And StrArr(0).ToString = "0" Then
                            '    ShowError("[ " & StrArr(1).ToString & " ] Instruction(s) not approved successfully.")
                            'ElseIf StrArr(1).ToString <> "0" And StrArr(0).ToString <> "0" Then
                            '    ShowError("[ " & StrArr(0).ToString & " ] Instruction(s) approved successfully <br/ > [ " & StrArr(1).ToString & " ] Instruction(s) not approved successfully.")
                            'End If
                            btnOK2FA.Visible = False
                            RefreshPage()
                            If dt.Rows.Count > 0 Then
                                gvShowSummary.DataSource = Nothing
                                gvShowSummary.DataSource = ICInstructionController.GetFinalDTForSummary(dt)
                                gvShowSummary.DataBind()
                                pnlShowSummary.Style.Remove("display")
                                Dim scr As String
                                scr = "window.onload = function () {showsumdialog('Transaction Summary',true,700,650);};"
                                Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "scr", scr, True)
                            Else
                                gvShowSummary.DataSource = Nothing
                                pnlShowSummary.Style.Add("display", "none")
                            End If
                        End If
                    Else
                        If IC2FAController.CheckAndUpdateLoginAttemts(Me.UserId, "Approval") = False Then
                            btnOK2FA.Text = "Regenerate Code"
                            txt2FACode.Text = Nothing
                            txt2FACode.Enabled = False
                            rim2FA.Enabled = False
                            btnApprove.Visible = False
                            btnSendForAmend.Visible = False
                            btnCancelInstructions.Visible = False
                            ShowError("Retries Exhausted. Please regenerate code.")
                        Else
                            btnApprove.Visible = False
                            btnSendForAmend.Visible = False
                            btnCancelInstructions.Visible = False
                            txt2FACode.Enabled = True
                            txt2FACode.Text = Nothing
                            ShowError("Invalid Code. Please retry.")
                            rim2FA.Enabled = True
                        End If
                    End If
                ElseIf btnOK2FA.Text = "Regenerate Code" Then
                    If IC2FAController.Generate2FACodeAndSend(Me.UserId, "Approval") Then
                        btnOK2FA.Text = "OK"
                        txt2FACode.Text = Nothing
                        txt2FACode.Enabled = True
                        rim2FA.Enabled = True
                        btnApprove.Visible = False
                        btnSendForAmend.Visible = False
                        btnCancelInstructions.Visible = False
                    End If
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                ShowError(ex.Message.ToString())
            End Try
        End If
    End Sub
    Private Sub ShowError(ByVal Msg As String)
        lblError.Text = Msg.ToString()
        lblError.Visible = True
    End Sub
#End Region

    Protected Sub ddlAccount_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAccount.SelectedIndexChanged

        Try
            If ddlAccount.SelectedValue.ToString <> "0" Then
                txtAvailableBalance.Text = ""
                txtAvailableBalance.Text = CDbl(CBUtilities.BalanceInquiry(ddlAccount.SelectedValue.Split("-")(0).ToString, ICBO.CBUtilities.AccountType.RB, "Client Account Balance", ddlAccount.SelectedValue.Split("-")(0).ToString, ddlAccount.SelectedValue.Split("-")(1).ToString)).ToString("N2")
            Else
                txtAvailableBalance.Text = ""
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Protected Sub raddpCreationToDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationToDate.SelectedDateChanged
        Try
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Verfication Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Verfication from date should be less than verfication to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub raddpCreationFromDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles raddpCreationFromDate.SelectedDateChanged
        Try

            'If raddpCreationFromDate.SelectedDate Is Nothing Then
            '    UIUtilities.ShowDialog(Me, "Error", "Please select creation from date.", ICBO.IC.Dialogmessagetype.Failure)
            '    Exit Sub
            'End If
            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Verfication Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Verfication from date should be less than verfication to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAuthorization_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvAuthorization.PageIndexChanged
        Try
            gvAuthorization.CurrentPageIndex = e.NewPageIndex
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub rbtnlstDateSelection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnlstDateSelection.SelectedIndexChanged
        Try
            If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now.AddYears(2)
            Else
                raddpCreationToDate.SelectedDate = Date.Now
                raddpCreationToDate.MaxDate = Date.Now
            End If

            If Not raddpCreationFromDate.SelectedDate Is Nothing And Not raddpCreationToDate.SelectedDate Is Nothing Then
                If raddpCreationFromDate.SelectedDate > raddpCreationToDate.SelectedDate Then
                    If rbtnlstDateSelection.SelectedValue.ToString = "Value Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Value from date should be less than value to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    ElseIf rbtnlstDateSelection.SelectedValue.ToString = "Verfication Date" Then
                        UIUtilities.ShowDialog(Me, "Error", "Verfication from date should be less than verfication to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub

                    Else
                        UIUtilities.ShowDialog(Me, "Error", "Creation from date should be less than creation to date.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
            End If
            LoadgvAccountPaymentNatureProductType(True)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnClose2FA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose2FA.Click
        Page.Validate()

        If Page.IsValid Then
            Try
                RefreshPage()

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub


End Class

