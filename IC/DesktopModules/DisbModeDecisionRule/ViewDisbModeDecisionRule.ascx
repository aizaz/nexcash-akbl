﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewDisbModeDecisionRule.ascx.vb" Inherits="DesktopModules_DisbModeDecisionRule_ViewDisbModeDecisionRule" %>

    <script type="text/javascript">
        function con() {
            if (window.confirm("Are you sure you wish to remove this Record?") == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function delcon(item) {
            if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
                return true;
            }
            else {
                return false;
            }
        }    
</script>
<body style="font-weight: 700">
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="left" valign="top">
            <asp:Label ID="lblPageHeader" runat="server" Text="Disbursement Mode Rule Management" 
                            CssClass="headingblue" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnAddDisbRule" runat="server" Text="Add Disbursement Rule" 
                            CssClass="btn" CausesValidation="False" />
                        <asp:HiddenField ID="hfBankCode" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                    <table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAgent0" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label11" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True" 
                CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvAgent0" runat="server" ControlToValidate="ddlGroup"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Group" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAgent" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label9" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblDisbMode" runat="server" Text="Select Disbursement Mode" 
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="Label10" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" 
                CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlDisbMode" runat="server" AutoPostBack="True" 
                CssClass="dropdown">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem>Cheque</asp:ListItem>
                <asp:ListItem>COTC</asp:ListItem>
                <asp:ListItem Value="DD">Demand Draft</asp:ListItem>
                <asp:ListItem>Direct Credit</asp:ListItem>
                <asp:ListItem>Other Credit</asp:ListItem>
                <asp:ListItem Value="PO">Pay Order</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    </table> 
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <asp:Label ID="lblListHeader" runat="server" Text="Disbursement Rules List" 
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <asp:GridView ID="gvDisbRule" runat="server" AutoGenerateColumns="False" CssClass="Grid"
                            Width="100%" AllowPaging="True" EnableModelValidation="True" 
                            AllowSorting="True">
                            <AlternatingRowStyle CssClass="GridAltItem" />
                            <Columns>
                                <%--<asp:BoundField DataField="DisbRuleName" HeaderText="Rule Name" SortExpression="DisbRuleName">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>--%>
                                <asp:BoundField DataField="FieldName" HeaderText="Field Name" SortExpression="FieldName">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                 <asp:BoundField DataField="FieldCondition" HeaderText="Operator" SortExpression="FieldCondition">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                 <asp:BoundField DataField="FieldValue" HeaderText="Field Value" SortExpression="FieldValue">
                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:BoundField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveDisbRule", "&mid=" & Me.ModuleId & "&id="& Eval("DisbRuleID"))%>'
                                            ToolTip="Edit">Edit</asp:HyperLink>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                </asp:TemplateField>                                
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("DisbRuleID") %>'
                                            CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                            ToolTip="Delete" />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                    <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="16px" />
                                </asp:TemplateField>                               
                            </Columns>
                            <HeaderStyle CssClass="GridHeader" />
                            <PagerStyle CssClass="GridPager" />
                            <RowStyle CssClass="GridItem" />
                        </asp:GridView>
                    </td>
                </tr>
               </table>
</td>
</tr></table></body> 