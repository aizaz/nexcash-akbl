﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Partial Class DesktopModules_DisbModeDecisionRule_ViewDisbModeDecisionRule
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrRights As ArrayList
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Dim dtDa As DataTable
    Protected Sub DesktopModules_FixUsers_ViewFixRolesUsers_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If CheckIsAdminOrSuperUser() = False Then
                ManagePageAccessRights()
            End If
            If Page.IsPostBack = False Then
                LoadddlGroup()
                LoadddlCompany()
                ViewState("SortExp") = Nothing
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ManagePageAccessRights()
        Try
            ArrRights = ICBankRoleRightsController.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Disbursement Rule Management")
            If ArrRights.Count > 0 Then
                btnAddDisbRule.Visible = ArrRights(0)
            Else
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            ' str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return False
        End Try
    End Function
    Private Sub LoadddlCompany()
        Try
            ddlCompany.Items.Clear()
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True

            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"

            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlGroup()
        Try

            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True

            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"

            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetgvDisbRule(ByVal AgentID As String, ByVal DisbType As String)
        Try
            Dim objICDisbRuleColl As New ICDisbursementRuleCollection
            objICDisbRuleColl.es.Connection.CommandTimeout = 3600
            objICDisbRuleColl = ICDisbursementRuleController.GetDisbursementRulesByCompanyCodeAndDisbType(ddlCompany.SelectedValue.ToString(), DisbType.ToString())
            dtDa = objICDisbRuleColl.Query.LoadDataTable()
            ViewState("DataTable") = dtDa
            gvDisbRule.DataSource = dtDa
            gvDisbRule.DataBind()
            If CheckIsAdminOrSuperUser() = False Then
                If ArrRights.Count > 0 Then
                    gvDisbRule.Columns(4).Visible = ArrRights(2)
                Else
                    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL())
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Protected Sub btnAddDisbRule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDisbRule.Click
        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveDisbRule", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If
    End Sub

    Protected Sub gvDisbRule_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvDisbRule.PageIndexChanged
        If (Not ViewState("sortDirection") Is Nothing And Not ViewState("SortExp") Is Nothing) Then
            Dim sortExpression As String = CStr(ViewState("SortExp")).ToString()
            If GridViewSortDirection() = SortDirection.Ascending Then
                SortGridView(sortExpression, ASCENDING)
            Else
                SortGridView(sortExpression, DESCENDING)
            End If
        Else
            SetgvDisbRule(ddlCompany.SelectedValue.ToString(), ddlDisbMode.SelectedValue.ToString())
        End If
    End Sub

    Protected Sub gvDisbRule_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDisbRule.RowCommand
        Try
            If e.CommandName = "del" Then
                Dim ActionStr As String = ""
                Dim objICDisbRule As New ICDisbursementRule
                objICDisbRule.es.Connection.CommandTimeout = 3600
                If objICDisbRule.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                    ICDisbursementRuleController.DeleteDisbursementRules(e.CommandArgument.ToString())
                    ActionStr = "Disbursement Rule Deleted :"
                    ActionStr += " Company [Code: " & ddlCompany.SelectedValue.ToString() & " ; Name: " & ddlCompany.SelectedItem.Text.ToString & "] ;"
                    ActionStr += " Disbursement Type  [" & objICDisbRule.DisbType & "] ;"
                    ActionStr += " Field Name [" & objICDisbRule.FieldName & "] ;"
                    If objICDisbRule.FieldValue Is Nothing Or objICDisbRule.FieldValue = "" Then
                        ActionStr += " Condition / Operator [" & objICDisbRule.FieldCondition & "]"
                    Else
                        ActionStr += " Condition / Operator [" & objICDisbRule.FieldCondition & "] ;"
                        ActionStr += " Value [" & objICDisbRule.FieldValue & "]"
                    End If
                    'ICUtilities.AddAuditTrail(ActionStr.ToString(), "Disbursement Rule Management", objICDisbRule.DisbRuleID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    UIUtilities.ShowDialog(Me, "Disbursement Rule Management", "Disbursement Rule deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                    SetgvDisbRule(ddlCompany.SelectedValue.ToString(), ddlDisbMode.SelectedValue.ToString())
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Error", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlAgent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        ddlDisbMode.SelectedValue = "0"
        SetgvDisbRule(ddlCompany.SelectedValue.ToString(), ddlDisbMode.SelectedValue.ToString())
        ViewState("sortDirection") = Nothing
        ViewState("SortExp") = Nothing
    End Sub

    Protected Sub ddlDisbMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDisbMode.SelectedIndexChanged
        SetgvDisbRule(ddlCompany.SelectedValue.ToString(), ddlDisbMode.SelectedValue.ToString())
        ViewState("sortDirection") = Nothing
        ViewState("SortExp") = Nothing
    End Sub

    Protected Sub gvDisbRule_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDisbRule.PageIndexChanging
        gvDisbRule.PageIndex = e.NewPageIndex
        If (Not ViewState("sortDirection") Is Nothing And Not ViewState("SortExp") Is Nothing) Then
            Dim sortExpression As String = CStr(ViewState("SortExp")).ToString()
            If GridViewSortDirection() = SortDirection.Ascending Then
                SortGridView(sortExpression, ASCENDING)
            Else
                SortGridView(sortExpression, DESCENDING)
            End If
        Else
            SetgvDisbRule(ddlCompany.SelectedValue.ToString(), ddlDisbMode.SelectedValue.ToString())
        End If




    End Sub
    Public Property GridViewSortDirection() As SortDirection
        Get
            If ViewState("sortDirection") Is Nothing Then
                ViewState("sortDirection") = SortDirection.Ascending
            ElseIf ViewState("sortDirection") = SortDirection.Ascending Then
                ViewState("sortDirection") = SortDirection.Descending
            ElseIf ViewState("sortDirection") = SortDirection.Descending Then
                ViewState("sortDirection") = SortDirection.Ascending
            End If

            Return DirectCast(ViewState("sortDirection"), SortDirection)
        End Get
        Set(ByVal value As SortDirection)
            ViewState("sortDirection") = value
        End Set
    End Property


    Private Sub SortGridView(ByVal sortExpression As String, ByVal direction As String)
        '  You can cache the DataTable for improving performance
        Try
            Dim dt As DataTable = ViewState("DataTable")
            Dim sortedDt As New DataTable
            dt.DefaultView.Sort = sortExpression + direction

            gvDisbRule.DataSource = dt.DefaultView
            gvDisbRule.DataBind()


            ViewState("dtGrid") = dt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub

    Protected Sub gvDisbRule_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvDisbRule.Sorting
        ViewState("SortExp") = e.SortExpression
        Dim sortExpression As String = CStr(ViewState("SortExp")).ToString()
        If GridViewSortDirection() = SortDirection.Ascending Then
            SortGridView(sortExpression, ASCENDING)
        Else
            SortGridView(sortExpression, DESCENDING)
        End If
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
End Class
