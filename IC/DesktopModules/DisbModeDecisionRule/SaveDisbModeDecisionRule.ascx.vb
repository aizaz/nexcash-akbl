﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Partial Class DesktopModules_DisbModeDecisionRule_SaveDisbModeDecisionRule
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private DisbRuleID As String
    Private ArrRights As ArrayList

    Protected Sub DesktopModules_DisbModeDecisionRule_SaveUser_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DisbRuleID = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                LoadddlGroup()
                LoadddlCompany()
                If DisbRuleID.ToString() = "0" Then
                    Clear()
                    lblPageHeader.Text = "Add Disbursement Rule"
                    btnSave.Text = "Save"
                Else
                    lblPageHeader.Text = "Edit Disbursement Rule"
                    btnSave.Text = "Update"
                    Dim objICDisbRule As New ICDisbursementRule
                    Dim objICGroup As New ICGroup
                    Dim objICCompany As New ICCompany

                    objICDisbRule.es.Connection.CommandTimeout = 3600
                    objICDisbRule.LoadByPrimaryKey(DisbRuleID)



                    objICCompany.LoadByPrimaryKey(objICDisbRule.CompanyCode)
                    objICGroup.LoadByPrimaryKey(objICCompany.GroupCode)
                    LoadddlGroup()
                    ddlGroup.SelectedValue = objICGroup.GroupCode.ToString()
                    LoadddlCompany()
                    ddlCompany.SelectedValue = objICDisbRule.CompanyCode
                    ddlDisbMode.SelectedValue = objICDisbRule.DisbType
                    ddlFieldName.SelectedValue = objICDisbRule.FieldName
                    ddlOperator.SelectedValue = objICDisbRule.FieldCondition
                    txtValue.Text = objICDisbRule.FieldValue
                End If
                SetValueVisibility()
            End If
            If CheckIsAdminOrSuperUser() = False Then
                ManagePageAccessRights()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ManagePageAccessRights()
        Try
            ArrRights = ICBankRoleRightsController.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Disbursement Rule Management")
            If ArrRights.Count > 0 Then
                If DisbRuleID.ToString() = "0" Then
                    btnSave.Visible = ArrRights(0)
                Else
                    btnSave.Visible = ArrRights(1)
                End If
            Else
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
            ' chkApproved.Enabled = ArrRights(3)
            ' btnApproved.Visible = ArrRights(3)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return Result
        End Try
    End Function
    Private Sub Clear()
        LoadddlGroup()
        LoadddlCompany()
        ddlDisbMode.SelectedValue = "0"
        ddlFieldName.SelectedValue = "0"
        ddlOperator.SelectedValue = "0"
        txtValue.Text = ""
        lblGuidLine.Text = ""
        lblGuidLine.Visible = False
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim ActionStr As String = ""
        Dim objICDisbRule As New ICDisbursementRule
        objICDisbRule.es.Connection.CommandTimeout = 3600
        If Page.IsValid Then
            Try
                If DisbRuleID.ToString() <> "0" Then
                    objICDisbRule.LoadByPrimaryKey(DisbRuleID)
                End If
                objICDisbRule.CompanyCode = ddlCompany.SelectedValue
                objICDisbRule.DisbType = ddlDisbMode.SelectedValue
                objICDisbRule.FieldName = ddlFieldName.SelectedValue
                objICDisbRule.FieldCondition = ddlOperator.SelectedValue
                objICDisbRule.FieldValue = txtValue.Text
                If DisbRuleID.ToString() <> "0" Then
                    ActionStr = "Disbursement Rule Updated :"
                Else
                    ActionStr = "Disbursement Rule Added :"
                End If

                ActionStr += " Company [Code: " & ddlCompany.SelectedValue.ToString() & " ; Name: " & ddlCompany.SelectedItem.Text.ToString & "] ;"
                ActionStr += " Disbursement Type  [" & ddlDisbMode.SelectedValue.ToString() & "] ;"
                ActionStr += " Field Name [" & ddlFieldName.SelectedValue.ToString() & "] ;"
                If txtValue.Text.ToString() = "" Then
                    ActionStr += " Condition / Operator [" & ddlOperator.SelectedValue.ToString() & "]"
                Else
                    ActionStr += " Condition / Operator [" & ddlOperator.SelectedValue.ToString() & "] ;"
                    ActionStr += " Value [" & txtValue.Text.ToString() & "]"
                End If
                If DisbRuleID.ToString() = "0" Then
                    If ICDisbursementRuleController.CheckDuplicate(objICDisbRule) = False Then
                        ICDisbursementRuleController.AddDisbursementRules(objICDisbRule, False)
                        'ICUtilities.AddAuditTrail(ActionStr.ToString(), "Disbursement Rule Management", objICDisbRule.DisbRuleID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        UIUtilities.ShowDialog(Me, "Save Disbursement Rule", "Disbursement Rule added successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Else
                        UIUtilities.ShowDialog(Me, "Save Disbursement Rule", "Can not add duplicate Disbursement Rule.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else
                    ICDisbursementRuleController.AddDisbursementRules(objICDisbRule, True)
                    'ICUtilities.AddAuditTrail(ActionStr.ToString(), "Disbursement Rule Management", objICDisbRule.DisbRuleID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    UIUtilities.ShowDialog(Me, "Save Disbursement Rule", "Disbursement Rule updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                End If
                Clear()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
            end if
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub
    Private Sub LoadddlCompany()
        Try

            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True

            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"

            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlGroup()
        Try

            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True

            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"

            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetValueVisibility()
        If ddlOperator.SelectedValue.ToString = "IsNull" Then
            lblValue.Visible = False
            lblValueIsRequired.Visible = False
            txtValue.Visible = False
            rimValue.Enabled = False
            Exit Sub
        End If
        If ddlOperator.SelectedValue.ToString = "IsNotNull" Then
            lblValue.Visible = False
            lblValueIsRequired.Visible = False
            txtValue.Visible = False
            rimValue.Enabled = False
            Exit Sub
        End If
        rimValue.Enabled = True
        lblValue.Visible = True
        lblValueIsRequired.Visible = True
        txtValue.Visible = True
    End Sub
    Protected Sub ddlOperator_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOperator.SelectedIndexChanged
        SetValueVisibility()
        OperatorHelpText(ddlOperator.SelectedValue.ToString())
    End Sub
    Private Sub OperatorHelpText(ByVal Op As String)
        Try
            Select Case Op
                Case "0"
                    lblGuidLine.Text = ""
                    lblGuidLine.Visible = False
                Case "Contains"
                    lblGuidLine.Text = "Comma(,) separated string or numeric values required."
                    lblGuidLine.Visible = True
                Case "Equal"
                    lblGuidLine.Text = "String or numeric value required."
                    lblGuidLine.Visible = True
                Case "GreaterThan"
                    lblGuidLine.Text = "String or numeric value required."
                    lblGuidLine.Visible = True
                Case "GreaterThanOrEqual"
                    lblGuidLine.Text = "String or numeric value required."
                    lblGuidLine.Visible = True
                Case "In"
                    lblGuidLine.Text = "Comma(,) separated string or numeric values required."
                    lblGuidLine.Visible = True
                Case "IsNotNull"
                    lblGuidLine.Text = "Value not required."
                    lblGuidLine.Visible = True
                Case "IsNull"
                    lblGuidLine.Text = "Value not required."
                    lblGuidLine.Visible = True
                Case "LessThan"
                    lblGuidLine.Text = "String or numeric value required."
                    lblGuidLine.Visible = True
                Case "LessThanOrEqual"
                    lblGuidLine.Text = "String or numeric value required."
                    lblGuidLine.Visible = True
                Case "Like"
                    lblGuidLine.Text = "String or numeric value required."
                    lblGuidLine.Visible = True
                Case "NotEqual"
                    lblGuidLine.Text = "String or numeric value required."
                    lblGuidLine.Visible = True
                Case "NotIn"
                    lblGuidLine.Text = "Comma(,) separated string or numeric values required."
                    lblGuidLine.Visible = True
                Case "NotLike"
                    lblGuidLine.Text = "String or numeric value required."
                    lblGuidLine.Visible = True
            End Select

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
