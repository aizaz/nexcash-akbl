﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveDisbModeDecisionRule.ascx.vb"
    Inherits="DesktopModules_DisbModeDecisionRule_SaveDisbModeDecisionRule" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadInputManager ID="rimValue" runat="server" Enabled="true" >
    <telerik:TextBoxSetting BehaviorID="REValue" Validation-IsRequired="true" EmptyMessage="Enter Value" ErrorMessage="Invalid Value">
        <TargetControls>
            <telerik:TargetInput ControlID="txtValue" />
        </TargetControls>
    </telerik:TextBoxSetting>    
</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" Text="Add Disbursement Rule" CssClass="headingblue"></asp:Label>
            <br />
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" colspan="2">
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblreqAgent0" runat="server" Text="*" ForeColor="Red" 
                Visible="True"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" Visible="True" 
                AutoPostBack="True" CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvAgent0" runat="server" ControlToValidate="ddlGroup"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Group" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblreqAgent" runat="server" Text="*" ForeColor="Red" Visible="True"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAgentBranch" runat="server" Text="Disbursement Mode" CssClass="lbl"
                Visible="True"></asp:Label>
            <asp:Label ID="lblreqAgentBranch" runat="server" Text="*" ForeColor="Red" Visible="True"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" Visible="True" 
                AutoPostBack="True" CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlDisbMode" runat="server" Visible="True" 
                CssClass="dropdown">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem>Cheque</asp:ListItem>
                <asp:ListItem>COTC</asp:ListItem>
                <asp:ListItem Value="DD">Demand Draft</asp:ListItem>
                <asp:ListItem>Direct Credit</asp:ListItem>
                <asp:ListItem>Other Credit</asp:ListItem>
                <asp:ListItem Value="PO">Pay Order</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvAgent" runat="server" ControlToValidate="ddlCompany"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Company" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvAgentBranch" runat="server" ControlToValidate="ddlDisbMode"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Disbursement Mode"
                SetFocusOnError="True" ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblSubAgent" runat="server" Text="Field Name" CssClass="lbl" Visible="True"></asp:Label>
            <asp:Label ID="lblreqSubAgent" runat="server" Text="*" ForeColor="Red" Visible="True"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblSubAgentBranch" runat="server" Text="Operator" CssClass="lbl" Visible="True"></asp:Label>
            <asp:Label ID="lblreqSubAgentBranch" runat="server" Text="*" ForeColor="Red" Visible="True"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlFieldName" runat="server" Visible="True" 
                CssClass="dropdown">
                <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem Selected="False" Value="RIN">RIN</asp:ListItem>
                <asp:ListItem Selected="False" Value="RemitterCode">RemitterCode</asp:ListItem>
                <asp:ListItem Selected="False" Value="RemitterName">RemitterName</asp:ListItem>
                <asp:ListItem Selected="False" Value="RemitterCell">RemitterCell</asp:ListItem>
                <asp:ListItem Selected="False" Value="RemitterEmail">RemitterEmail</asp:ListItem>
                <asp:ListItem Selected="False" Value="RemitterCity">RemitterCity</asp:ListItem>
                <asp:ListItem Selected="False" Value="RemitterProvince">RemitterProvince</asp:ListItem>
                <asp:ListItem Selected="False" Value="RemittType">RemittType</asp:ListItem>
                <asp:ListItem Selected="False" Value="Amount">Amount</asp:ListItem>
                <asp:ListItem Selected="False" Value="AmountPKR">AmountPKR</asp:ListItem>
                <asp:ListItem Selected="False" Value="BeneIDNO">BeneIDNO</asp:ListItem>
                <asp:ListItem Selected="False" Value="BeneName">BeneName</asp:ListItem>
                <asp:ListItem Selected="False" Value="BeneCellNo">BeneCellNo</asp:ListItem>
                <asp:ListItem Selected="False" Value="BeneCity">BeneCity</asp:ListItem>
                <asp:ListItem Selected="False" Value="BeneEmail">BeneEmail</asp:ListItem>
                <asp:ListItem Selected="False" Value="BankCode">BankCode</asp:ListItem>
                <asp:ListItem Selected="False" Value="BankName">BankName</asp:ListItem>
                <asp:ListItem Selected="False" Value="BankAccountNumber">BankAccountNumber</asp:ListItem>
                <asp:ListItem Selected="False" Value="BankType">BankType</asp:ListItem>
                <asp:ListItem Selected="False" Value="DisbTYpe">DisbTYpe</asp:ListItem>
                <asp:ListItem Selected="False" Value="Description">Description</asp:ListItem>
                <asp:ListItem Selected="False" Value="TransferType">TransferType</asp:ListItem>
                <asp:ListItem Selected="False" Value="OFACUID_Bene">OFACUID_Bene</asp:ListItem>
                <asp:ListItem Selected="False" Value="Remarks">Remarks</asp:ListItem>
                <asp:ListItem Selected="False" Value="ExchangeRate">ExchangeRate</asp:ListItem>
                <asp:ListItem Selected="False" Value="RemittTime">RemittTime</asp:ListItem>
                <asp:ListItem Selected="False" Value="RemittRefNo">RemittRefNo</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlOperator" runat="server" Visible="True" 
                AutoPostBack="True" CssClass="dropdown">
                <asp:ListItem Selected="True" Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem Selected="False" Value="Contains">Contains</asp:ListItem>
                <asp:ListItem Selected="False" Value="Equal">Equal</asp:ListItem>
                <asp:ListItem Selected="False" Value="GreaterThan">GreaterThan</asp:ListItem>
                <asp:ListItem Selected="False" Value="GreaterThanOrEqual">GreaterThanOrEqual</asp:ListItem>
                <asp:ListItem Selected="False" Value="In">In</asp:ListItem>
                <asp:ListItem Selected="False" Value="IsNotNull">IsNotNull</asp:ListItem>
                <asp:ListItem Selected="False" Value="IsNull">IsNull</asp:ListItem>
                <asp:ListItem Selected="False" Value="LessThan">LessThan</asp:ListItem>
                <asp:ListItem Selected="False" Value="LessThanOrEqual">LessThanOrEqual</asp:ListItem>
                <asp:ListItem Selected="False" Value="Like">Like</asp:ListItem>
                <asp:ListItem Selected="False" Value="NotEqual">NotEqual</asp:ListItem>
                <asp:ListItem Selected="False" Value="NotIn">NotIn</asp:ListItem>
                <asp:ListItem Selected="False" Value="NotLike">NotLike</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvSubAgent" runat="server" ControlToValidate="ddlFieldName"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Field Name"
                SetFocusOnError="True" ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvSubAgentBranch" runat="server" ControlToValidate="ddlOperator"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Operator" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblValue" runat="server" Text="Value" CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblValueIsRequired" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtValue" runat="server" CssClass="txtbox" MaxLength="200"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblGuidLine" runat="server"
                Font-Size="Small" ForeColor="#999999" Visible="False"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" 
                style="margin-left: 0px" Width="71px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" 
                CausesValidation="False"  />
        </td>
    </tr>
</table>
