﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BankRoleAccountPaymentNatureTagging.ascx.vb"
    Inherits="DesktopModules_BankRoleAccountPaymentNature_BankRoleAccountPaymentNatureTagging" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:RegExpTextBoxSetting BehaviorID="REUserName" Validation-IsRequired="true"
        ValidationExpression="\b(\w*)\b(.)*\b(-)*\b(_)*\b" ErrorMessage="Invalid Name"
        EmptyMessage="Enter User Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtUserName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior7" Validation-IsRequired="true"
        ValidationExpression="\b(\w*)\b(.)*\b(-)*\b(_)*\b" ErrorMessage="Invalid Display  Name"
        EmptyMessage="Enter Display Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDisplayName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="false"
        EmptyMessage="Enter Role Type">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRoleType" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<style type="text/css">
    .style1
    {
        height: 22px;
    }
    .style2
    {
        height: 10px;
    }
    .style3
    {
        width: 100%;
    }
</style>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
    function ValidateChkList(source, arguments) {

        arguments.IsValid = IsCheckBoxChecked() ? true : false;



    }



    function IsCheckBoxChecked() {

        var isChecked = false;

        var list = document.getElementById('<%= chklstPaymentNature.ClientID %>');

        if (list != null) {

            for (var i = 0; i < list.rows.length; i++) {
                for (var j = 0; j < list.rows[i].cells.length; j++) {
                    var listControl = list.rows[i].cells[j].childNodes[0];
                    if (listControl.checked) {
                        isChecked = true;
                    }
                }
            }

        }

        return isChecked;


    }

</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="center" valign="top">
                        <table width="100%">
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" colspan="2">
                                    <asp:Label ID="lblPageHeader" runat="server" Text="Assign Accounts &amp; Payment Natures To Bank Roles"
                                        CssClass="headingblue"></asp:Label>
                                    <br />
                                    Note:&nbsp; Fields marked as
                                    <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                    &nbsp;are required.
                                </td>
                                <td align="left" valign="top" colspan="2">
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" colspan="2">
                                </td>
                                <td align="left" valign="top" colspan="2">
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" colspan="4">
                                    <table class="style3" id="tblPaymentNature" runat="server">
                                        <tr>
                                            <td style="width: 25%">
                                                <asp:Label ID="lblUserRole" runat="server" Text="Select Role" CssClass="lbl"></asp:Label>
                                                <asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="ddlRole" runat="server" CssClass="dropdown" AutoPostBack="True">
                                                    <asp:ListItem Selected="True">Bank Role</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 25%">
                                                <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="ddlRole"
                                                    CssClass="lblEror" Display="Dynamic" ErrorMessage="Please Select Role" SetFocusOnError="True"
                                                    ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 25%">
                                                <asp:Label ID="lblGroup" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
                                                <asp:Label ID="Label8" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 25%">
                                                <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
                                                <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlGroup" runat="server" CssClass="dropdown" AutoPostBack="True">
                                                    <asp:ListItem>-- Select Group --</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" AutoPostBack="True">
                                                    <asp:ListItem>-- Select Company</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvddlGroup" runat="server" ControlToValidate="ddlGroup"
                                                    Display="Dynamic" ErrorMessage="Please select Group" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" ControlToValidate="ddlCompany"
                                                    Display="Dynamic" ErrorMessage="Please select Company" InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblPaymentNature" runat="server" Text="Select Account, Payment Nature"
                                                    CssClass="lbl"></asp:Label>
                                                <asp:Label ID="lblMustReqPayNature" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CustomValidator ID="cusvalidPaymentNature" ClientValidationFunction="ValidateChkList"
                                                    runat="server" Display="Dynamic" ErrorMessage="Please select Account, Payment Nature"
                                                    SetFocusOnError="True"></asp:CustomValidator>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <asp:CheckBoxList ID="chklstPaymentNature" runat="server" CssClass="chkList" Font-Size="10">
                                                </asp:CheckBoxList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" />
                                    &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False" />
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top" style="width: 25%">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Label ID="lblUserListHeader" runat="server" Text="Custom User List" CssClass="headingblue"
                            Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" class="style2">
                        <asp:Label ID="Label5" runat="server" Text="No Record Found" Visible="False" Font-Bold="False"
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <telerik:RadGrid ID="gvUserRoles" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="1">
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView>
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Approve" DataField="IsApproved">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAllAccountPaymentNature" runat="server" CssClass="chkBox"
                                                Text=" Select" TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="Company" HeaderText="Company" SortExpression="Company">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Account" HeaderText="Account" SortExpression="Account">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="PaymentNature" HeaderText="Payment Nature" SortExpression="PaymentNature">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Approve" DataField="IsApproved">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridTemplateColumn>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandName="del" ImageUrl="~/images/delete.gif"
                                                OnClientClick="javascript: return con();" ToolTip="Delete" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnApproveAccountPaymentNature" runat="server" 
                            Text="Approve Accounts Payment Natures" CssClass="btn" />
                        &nbsp;<asp:Button ID="btnDeleteAccountsPaymentNatures" runat="server" Text="Delete Accounts Payment Natures"
                            CssClass="btn" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
