﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data.OleDb
Imports System.Data

Partial Class DesktopModules_BankRoleAccountPaymentNature_BankRoleAccountPaymentNatureTagging
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private UserCode As String
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Dim dtDa As DataTable
    Private RolID As Integer = 0
    Private RoleType As String = ""
    Private _gvPaymentNature As Object
    Private _lblErrorPaymentNature As Object


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'UserCode = Request.QueryString("id").ToString()
            'Dim objICUser As New ICUser
            'Dim objICOffice As New ICOffice
            'objICUser.es.Connection.CommandTimeout = 3600
            'objICUser.LoadByPrimaryKey(UserCode)
            'objICOffice = objICUser.UpToICOfficeByOfficeCode()
            'If Not objICOffice.CompanyCode Is Nothing Then
            '    RoleType = "Company Role"
            'ElseIf Not objICOffice.BankCode Is Nothing Then
            '    RoleType = "Bank Role"
            'End If
            If Page.IsPostBack = False Then



                Dim cUser As New UserInfo



                'If objICUser.LoadByPrimaryKey(UserCode) Then
                tblPaymentNature.Style.Remove("Display")
                rfvddlGroup.Enabled = True
                rfvddlCompany.Enabled = True
                cusvalidPaymentNature.Enabled = True
                Dim objICGroup As New ICGroup
                Dim objICCompany As New ICCompany
                Dim objICPayNature As New ICPaymentNature

                Dim objICBank As New ICBank
                'cUser = UserController.GetUserById(Me.PortalId, UserCode)
                'objICOffice = objICUser.UpToICOfficeByOfficeCode()
                'If Not objICOffice.CompanyCode Is Nothing Then
                'If Not objICOffice.CompanyCode.ToString() = "" Then

                LoadddlRole("Bank Role")

                'objICCompany.LoadByPrimaryKey(objICOffice.CompanyCode.ToString())
                'objICGroup.LoadByPrimaryKey(objICCompany.GroupCode.ToString())
                LoadddlGroup()
                'ddlGroup.SelectedValue = objICGroup.GroupCode.ToString()
                LoadddlCompany()
                'ddlCompany.SelectedValue = objICCompany.CompanyCode.ToString()
                'ddlGroup.Enabled = False
                'ddlCompany.Enabled = False
                'rfvddlGroup.Enabled = False
                'rfvddlCompany.Enabled = False
                'lblGroup.Text = "Select Group"
                'lblCompany.Text = "Select Company"
                'End If
                lblPaymentNature.Visible = True
                lblMustReqPayNature.Visible = True
                cusvalidPaymentNature.Enabled = True
                'LoadChkListPaymentNatureByCompanyCode()
                LoadgvFromExcel()
                'ElseIf Not objICOffice.BankCode Is Nothing Then
                '    'If Not objICOffice.BankCode.ToString() = "" Then

                '    LoadddlRole(RoleType)
                '    LoadddlGroup()
                '    LoadddlCompany()
                '    rfvddlGroup.Enabled = False
                '    rfvddlCompany.Enabled = False
                '    lblGroup.Text = "Select Group"
                '    lblCompany.Text = "Select Company"
                '    'End If
                '    lblPaymentNature.Visible = True
                '    lblMustReqPayNature.Visible = True
                '    cusvalidPaymentNature.Enabled = True
                '    LoadChkListPaymentNatureByCompanyCode()
                '    SetgvUserRoles()
                '    'objICBank.LoadByPrimaryKey(objICOffice.BankCode.ToString())
                '    'LoadddlBanks()
                '    'ddlGroup.SelectedValue = objICBank.BankCode.ToString()
                '    'LoadddlBanksBranches()
                '    'ddlCompany.SelectedValue = objICOffice.OfficeCode.ToString()
                '    'ddlGroup.Enabled = False
                '    'ddlCompany.Enabled = False
                '    'lblGroup.Text = "Select Bank"
                '    'lblCompany.Text = "Select Branch"
                '    'End If

                '    'lblGroup.Visible = False
                '    'ddlGroup.Visible = False
                '    'ddlGroup.ClearSelection()
                '    'lblCompany.Visible = False
                '    'ddlCompany.Visible = False
                '    'ddlCompany.ClearSelection()
                '    'rfvddlGroup.Enabled = False
                '    'rfvddlCompany.Enabled = False
                '    'Label8.Visible = False
                '    'Label4.Visible = False



                'End If



                'Else
                '    tblPaymentNature.Style.Add("Display", "none")
                '    rfvddlGroup.Enabled = False
                '    rfvddlCompany.Enabled = False
                '    cusvalidPaymentNature.Enabled = False
                '    ddlGroup.ClearSelection()
                '    ddlCompany.ClearSelection()
                '    chklstPaymentNature.ClearSelection()
                'End If


            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlRole(ByVal RoleType As String)
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlRole.Items.Clear()
            ddlRole.Items.Add(lit)
            ddlRole.AppendDataBoundItems = True

            ddlRole.DataSource = ICRoleController.GetAllICRolesByRoleType(RoleType)
            ddlRole.DataTextField = "RoleName"
            ddlRole.DataValueField = "RoleID"
            ddlRole.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadChkListPaymentNatureByCompanyCode()
        Try
            chklstPaymentNature.Items.Add(New ListItem("1087879090" & ";" & "Vendor Payments"))
            chklstPaymentNature.Items.Add(New ListItem("1087879567" & ";" & "Employee Salaries"))
            chklstPaymentNature.Items.Add(New ListItem("108787903896" & ";" & "Management Payment"))
            '        'Dim lit As New ListItem
            '        'lit.Value = "0"
            '        'lit.Text = "-- Please Select --"
            '        'ddlPaymentNature.Items.Clear()
            '        'ddlPaymentNature.Items.Add(lit)
            '        'ddlPaymentNature.AppendDataBoundItems = True
            '        Dim icPayNature As New ICPaymentNature
            '        Dim icAccountPaynatureProductTypeColl As New ICAccountsPaymentNatureProductTypeCollection
            '        Dim icAccountPaymentNatureProductType As New ICAccountsPaymentNatureProductType
            '        Dim dt As New DataTable
            '        Dim icAcount As New ICAccounts
            '        Dim strAccountNmbr As String
            '        Dim strBranchCode As String
            '        Dim strCurrencyCode As String

            '        dt = ICAccountPaymentNatureProductTypeController.GetAllAccountPaymentNatureProductType(ddlCompany.SelectedValue.ToString())
            '        If dt.Rows.Count > 0 Then
            '            For Each dr As DataRow In dt.Rows
            '                strAccountNmbr = dr.Item("AccountNumber")
            '                strBranchCode = dr.Item("BranchCode")
            '                strCurrencyCode = dr.Item("Currency")
            '                icAcount.LoadByPrimaryKey(strAccountNmbr, strBranchCode, strCurrencyCode)
            '                icPayNature.LoadByPrimaryKey(dr.Item("PaymentNatureCode"))
            '                chklstPaymentNature.Items.Add(New ListItem(icAcount.AccountTitle.ToString() & " ; " & icPayNature.PaymentNatureName))
            '            Next
            '        End If



            '        'Dim icacountColl As New ICAccountsCollection

            '        ''icacountColl = ICAccountsController.GetAllAccountsByCompanyCode(ddlCompany.SelectedValue.ToString())



            '        'For Each icPayNature In ICPaymentNatureController.GetAllPaymentNatureByCompanyCode(ddlCompany.SelectedValue.ToString())

            '        '    chklstPaymentNature.Items.Add(New ListItem(icPayNature.PaymentNatureName))


            '        'Next


            '        'chklstPaymentNature.DataSource = ICPaymentNatureController.GetAllPaymentNatureByCompanyCode(ddlCompany.SelectedValue.ToString())
            '        'chklstPaymentNature.DataTextField = "PaymentNatureName"
            '        'chklstPaymentNature.DataValueField = "PaymentNatureCode"
            '        'chklstPaymentNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try

                If CheckRoleAlReadyAssign(ddlRole.SelectedItem.Text.ToString()) = True Then
                    UIUtilities.ShowDialog(Me, "User Roles", "Role already assign.", ICBO.IC.Dialogmessagetype.Warning)
                    LoadddlRole(RoleType)
                    LoadgvFromExcel()
                Else
                    Dim objICUserRoles As New ICUserRoles
                    Dim objICUser As New ICUser
                    Dim j As Integer = 0
                    Dim cUser As New UserInfo
                    Dim cRole As New RoleInfo
                    Dim roleCtrl As New RoleController
                    'ICUserRolesController.AssignRoleToUser(ddlRole.SelectedValue.ToString(), Me.PortalId, UserCode)

                    cRole = roleCtrl.GetRoleByName(Me.PortalId, ddlRole.SelectedItem.ToString())

                    objICUserRoles.es.Connection.CommandTimeout = 3600
                    objICUser.es.Connection.CommandTimeout = 3600
                    'objICUserRoles.LoadByPrimaryKey(ddlRole.SelectedValue.ToString())
                    objICUser.LoadByPrimaryKey(UserCode.ToString())
                    'objICUserRoles.UserRolesID = cRole.RoleID
                    objICUserRoles.RoleID = ddlRole.SelectedValue.ToString()
                    'objICUserRoles.RoleName = ddlRole.SelectedItem.ToString()
                    'objICUserRoles.CreateBy = Me.UserId
                    'objICUserRoles.CreateDate = Now
                    objICUserRoles.ApprovedBy = Me.UserId



                    'j = ICUserRolesController.AddICUserRoles(objICUserRoles, objICUser, False)
                    If objICUser.UserType.ToString() = "Office User" Then


                        If Not j = 0 Then
                            Dim lit As New ListItem
                            For Each lit In chklstPaymentNature.Items
                                If lit.Selected = True Then
                                    Dim objICUSerRolPayNature As New ICUserRolesAccountAndPaymentNature
                                    'objICUSerRolPayNature.RolesID = j
                                    objICUSerRolPayNature.RolesID = objICUserRoles.RoleID
                                    objICUSerRolPayNature.UserID = UserCode
                                    objICUSerRolPayNature.PaymentNatureCode = lit.Value.ToString()
                                    objICUSerRolPayNature.CreateBy = Me.UserId
                                    'ICUserRolesPaymentNatureController.AddUserRolesPaymentNature(objICUSerRolPayNature, False)
                                End If
                            Next
                        End If
                    End If



                    'ICUtilities.AddAuditTrail("UserRoles : " & txtUserName.Text.ToString() & " Assign " & ddlRole.SelectedItem.Text.ToString() & " Role.", "UserRole", UserCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    UIUtilities.ShowDialog(Me, "User Roles", "Assign role to user successfully.", ICBO.IC.Dialogmessagetype.Success)
                    LoadddlRole(RoleType)
                    LoadgvFromExcel()
                    chklstPaymentNature.ClearSelection()
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub
    Private Function CheckRoleAlReadyAssign(ByVal RoleName As String) As Boolean
        Dim Result As Boolean = False
        Dim gvRow As Telerik.Web.UI.GridItem

        For Each gvRow In gvUserRoles.Items
            If gvRow.Cells(0).Text.ToString() = RoleName.ToString() Then
                Result = True
            End If
        Next
        Return Result
    End Function
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlBanks()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICBankController.GetAllApprovedAndActiveBanks()
            ddlGroup.DataTextField = "BankName"
            ddlGroup.DataValueField = "BankCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlBanksBranches()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICOfficeController.GetBranchByBankCodeActiveAndApprove(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "OfficeName"
            ddlCompany.DataValueField = "OfficeCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadgvFromExcel()
        Try
            Dim connectionString As String = ""
            Dim fileLocation As String = Server.MapPath("~/Excel Sheets For Grids/BankRoleAccountPaymentNatureTagging.xlsx")

            'Check whether file extension is xls or xslx

            'If fileExtension = ".xls" Then
            '    connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
            'ElseIf fileExtension = ".xlsx" Then
            connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & fileLocation & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
            'End If

            'Create OleDB Connection and OleDb Command

            Dim con As New OleDbConnection(connectionString)
            Dim cmd As New OleDbCommand()
            cmd.CommandType = System.Data.CommandType.Text
            cmd.Connection = con
            Dim dAdapter As New OleDbDataAdapter(cmd)
            Dim dtExcelRecords As New DataTable()
            con.Open()
            Dim dtExcelSheetName As DataTable = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim getExcelSheetName As String = "Sheet1$"
            cmd.CommandText = "SELECT * FROM [" & getExcelSheetName & "]"
            dAdapter.SelectCommand = cmd
            dAdapter.Fill(dtExcelRecords)
            con.Close()
            gvUserRoles.DataSource = dtExcelRecords
            gvUserRoles.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    'Private Sub SetgvUserRoles()
    '    Try
    '        Dim iUserRole As New UserRoleInfo
    '        Dim userCtrl As New RoleController
    '        Dim str As String()
    '        Dim i As Integer
    '        Dim dt As New DataTable
    '        Dim dr As DataRow
    '        dt.Columns.Add(New DataColumn("RoleId", GetType(System.String)))
    '        dt.Columns.Add(New DataColumn("RoleName", GetType(System.String)))
    '        dt.Columns.Add(New DataColumn("UserID", GetType(System.String)))
    '        dt.Columns.Add(New DataColumn("CompanyName", GetType(System.String)))
    '        dt.Columns.Add(New DataColumn("Account", GetType(System.String)))
    '        dt.Columns.Add(New DataColumn("PaymentNature", GetType(System.String)))
    '        str = userCtrl.GetPortalRolesByUser(UserCode, Me.PortalId)
    '        For i = 0 To str.Length - 1
    '            'Remove Registered Users
    '            If Not str(i).ToString() = "Registered Users" And Not str(i) = "Subscribers" Then
    '                Dim objICUserRolesPayNatureColl As New ICUserRolesAndPaymentNatureCollection
    '                Dim objICUserRolesPayNature As New ICUserRolesAndPaymentNature
    '                Dim dt2 As New DataTable
    '                Dim objICPatnature As New ICPaymentNature
    '                dr = dt.NewRow()
    '                dr("RoleID") = i
    '                RolID = i
    '                dt2 = ICUserRolesPaymentNatureController.GetPaymentNatureByRoleIDAndUserID(UserCode, i)
    '                'If dt2.Rows.Count > 0 Then
    '                '    For Each dr2 As DataRow In dt2.Rows
    '                '        objICPatnature.LoadByPrimaryKey(dr2.Item("UserRolesAndPaymentNatureID"))
    '                '    Next
    '                'End If

    '                dr("RoleName") = str(i)
    '                dr("UserID") = UserCode
    '                'dr("CompanyName") = ddlCompany.SelectedItem.ToString()

    '                dr("PaymentNature") = "Vendor Payments"


    '                dr("Account") = "1087879090"

    '                dt.Rows.Add(dr)
    '            End If
    '        Next
    '        'If dt.Rows.Count = 0 Then
    '        '    Label5.Visible = True
    '        'Else
    '        '    Label5.Visible = False
    '        'End If
    '        dtDa = dt
    '        ViewState("DataTable") = dtDa
    '        gvUserRoles.DataSource = dtDa
    '        gvUserRoles.DataBind()
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Sub
    Protected Sub ddlRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRole.SelectedIndexChanged
        If ddlRole.SelectedValue.ToString() = "0" Then
            LoadgvFromExcel()
            ViewState("sortDirection") = Nothing
            ViewState("SortExp") = Nothing
        Else
            Dim objICRole As New ICRole
            objICRole.LoadByPrimaryKey(ddlRole.SelectedValue.ToString())
        End If
    End Sub

    Protected Sub gvUserRoles_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvUserRoles.PageIndexChanged
        'If (Not ViewState("sortDirection") Is Nothing And Not ViewState("SortExp") Is Nothing) Then
        '    Dim sortExpression As String = CStr(ViewState("SortExp")).ToString()
        '    If GridViewSortDirection() = SortDirection.Ascending Then
        '        SortGridView(sortExpression, ASCENDING)
        '    Else
        '        SortGridView(sortExpression, DESCENDING)
        '    End If
        'Else
        '    LoadgvFromExcel()
        'End If
    End Sub

    Protected Sub gvUserRoles_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvUserRoles.ItemCommand
        'Try
        '    If e.CommandName = "del" Then
        '        Dim cUser As New UserInfo
        '        Dim cRole As New RoleInfo
        '        Dim roleCtrl As New RoleController

        '        cUser = UserController.GetUserById(Me.PortalId, UserCode)
        '        cRole = roleCtrl.GetRoleByName(Me.PortalId, e.CommandArgument.ToString())
        '        ICUserRolesPaymentNatureController.DeleteUserRolesPaymentNature(cRole.RoleID, UserCode)
        '        ICUserRolesController.DeleteUserRoles(cRole.RoleID, UserCode)
        '        RoleController.DeleteUserRole(cUser, cRole, Me.PortalSettings, True)



        '        'ICUtilities.AddAuditTrail("UserRoles : " & txtUserName.Text.ToString() & " Remove " & ddlRole.SelectedItem.Text.ToString() & " Role.", "UserRole", UserCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
        '        UIUtilities.ShowDialog(Me, "User Roles", "Remove role to user successfully.", ICBO.IC.Dialogmessagetype.Success)
        '        lblErrorPaymentNature.Visible = False
        '        LoadddlRole(RoleType)
        '        LoadgvFromExcel()
        '    ElseIf e.CommandName = "ShowPaymentNature" Then
        '        Dim cmdArg1 As String = Nothing
        '        Dim cmdArg2 As String = Nothing
        '        Dim cmdArg3 As String = Nothing

        '        Dim cRole As New RoleInfo
        '        Dim roleCtrl As New RoleController
        '        Dim s As String()
        '        s = e.CommandArgument.ToString().Split(";")
        '        cmdArg1 = s.GetValue(0)
        '        cmdArg2 = s.GetValue(1)
        '        cmdArg3 = s.GetValue(2)
        '        cRole = roleCtrl.GetRoleByName(Me.PortalId, cmdArg3)


        '        gvPaymentNature.DataSource = ICUserRolesPaymentNatureController.GetPaymentNatureByRoleIDAndUserID(cmdArg1, cRole.RoleID)
        '        gvPaymentNature.DataBind()

        '        If gvPaymentNature.Items.Count > 0 Then
        '            gvPaymentNature.Visible = True
        '            lblPaymentNatureList.Visible = True
        '            lblPaymentNatureList.Text = "Payment Nature List of  " & cmdArg3.ToString() & ""
        '            lblErrorPaymentNature.Visible = False
        '        Else
        '            lblErrorPaymentNature.Visible = True
        '            gvPaymentNature.Visible = False
        '            gvPaymentNature.DataSource = Nothing
        '            lblPaymentNatureList.Visible = False
        '            lblPaymentNatureList.Text = ""
        '        End If
        '    End If
        'Catch ex As Exception
        '    ProcessModuleLoadException(Me, ex, False)
        '    UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        'End Try
    End Sub
    Public Property GridViewSortDirection() As SortDirection
        Get
            If ViewState("sortDirection") Is Nothing Then
                ViewState("sortDirection") = SortDirection.Descending
            ElseIf ViewState("sortDirection") = SortDirection.Ascending Then
                ViewState("sortDirection") = SortDirection.Descending
            ElseIf ViewState("sortDirection") = SortDirection.Descending Then
                ViewState("sortDirection") = SortDirection.Ascending
            End If

            Return DirectCast(ViewState("sortDirection"), SortDirection)
        End Get
        Set(ByVal value As SortDirection)
            ViewState("sortDirection") = value
        End Set
    End Property


    Private Sub SortGridView(ByVal sortExpression As String, ByVal direction As String)
        '  You can cache the DataTable for improving performance
        Try
            Dim dt As DataTable = ViewState("DataTable")
            Dim sortedDt As New DataTable
            dt.DefaultView.Sort = sortExpression + direction

            gvUserRoles.DataSource = dt.DefaultView
            gvUserRoles.DataBind()


            ViewState("dtGrid") = dt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try


    End Sub

    Protected Sub gvUserRoles_SortCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridSortCommandEventArgs) Handles gvUserRoles.SortCommand

        'ViewState("SortExp") = e.SortExpression
        'Dim sortExpression As String = CStr(ViewState("SortExp")).ToString()
        'If GridViewSortDirection() = SortDirection.Ascending Then
        '    SortGridView(sortExpression, ASCENDING)
        'Else
        '    SortGridView(sortExpression, DESCENDING)
        'End If
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            LoadChkListPaymentNatureByCompanyCode()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            LoadddlCompany()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvUserRoles_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvUserRoles.ItemDataBound
        Try
            If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Then
                Dim objICUser As New ICUser
                objICUser.es.Connection.CommandTimeout = 3600
                objICUser.LoadByPrimaryKey(UserCode)
                Dim btn As New ImageButton
                btn = DirectCast(e.Item.Cells(1).FindControl("IbtnShowPaymentNature"), ImageButton)
                If objICUser.UserType = "Office User" Then

                    btn.Enabled = True
                Else
                    btn.Visible = False
                End If

            End If
        Catch ex As Exception

        End Try
    End Sub


End Class
