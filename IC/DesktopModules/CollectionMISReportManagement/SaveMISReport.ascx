﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveMISReport.ascx.vb" Inherits="DesktopModules_CollectionMISReportManagement_SaveMISReport" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .RadGrid_Default {
        font: 12px/16px "segoe ui",arial,sans-serif;
    }

    .RadGrid_Default {
        border: 1px solid #828282;
        background: #fff;
        color: #333;
    }

        .RadGrid_Default .rgMasterTable {
            font: 12px/16px "segoe ui",arial,sans-serif;
        }

    .RadGrid .rgMasterTable {
        border-collapse: separate;
        border-spacing: 0;
    }

    .RadGrid_Default .rgHeader {
        color: #333;
    }

    .RadGrid_Default .rgHeader {
        border: 0;
        border-bottom: 1px solid #828282;
        background: #eaeaea 0 -2300px repeat-x url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
    }

    .RadGrid .rgHeader {
        padding-top: 5px;
        padding-bottom: 4px;
        text-align: left;
        font-weight: normal;
    }

    .RadGrid .rgHeader {
        padding-left: 7px;
        padding-right: 7px;
    }

    .RadGrid .rgHeader {
        cursor: default;
    }

    .RadGrid_Default .rgHeader a {
        color: #333;
    }

    .RadGrid .rgHeader a {
        text-decoration: none;
    }

    .RadGrid_Default .rgFooter {
        background: #eee;
    }

    .btn {
    }
</style>
<script language="javascript" type="text/javascript">
    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }

</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
            }
        }
    }
</script>

<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }
    
</script>

<telerik:RadInputManager ID="RadInputManager1" runat="server">

    <telerik:TextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="true"
        EmptyMessage="" ErrorMessage="Invalid Report Name">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReportName" />
        </TargetControls>
    </telerik:TextBoxSetting>

</telerik:RadInputManager>

<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblReportName" runat="server" Text="Report Name"
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqReportName" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblReportFile" runat="server" Text="Report File"
                CssClass="lbl"></asp:Label>
            <asp:Label
                ID="lblReqReportFile" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtReportName" runat="server" CssClass="txtbox"
                MaxLength="200"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%"></td>
        <td align="left" valign="top" style="width: 25%">
            <asp:FileUpload ID="fupReportFile" runat="server" />

            <asp:HyperLink ID="hlDownloadReportFile" runat="server"
                ImageUrl="~/images/file.gif"
                ToolTip="Download File"></asp:HyperLink>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvReportFile" runat="server"
                ControlToValidate="fupReportFile" CssClass="lblEror" Display="Dynamic"
                ErrorMessage="Please select Report File" SetFocusOnError="True"
                ToolTip="Required Field"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>

    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblUserType" runat="server" Text="Select User Type"
                CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%"></td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlUserType" runat="server"
                CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem>Bank User</asp:ListItem>
                <asp:ListItem>Client User</asp:ListItem>
            </asp:DropDownList>
            <br />
        </td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%"></td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
        <td align="left" valign="top" style="width: 25%"></td>
        <td align="left" valign="top" style="width: 25%">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <table id="tblGroup" runat="server" style="width: 100%; display: none">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblGroup" runat="server" Text="Select Group"
                            CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblCompany" runat="server" Text="Select Company"
                            CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlGroup" runat="server"
                            CssClass="dropdown" AutoPostBack="True">
                            <asp:ListItem>-- Please Select --</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlCompany" runat="server"
                            CssClass="dropdown" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>


    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <table id="tblUsers" runat="server" style="width: 100%; display: none">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblUsers" runat="server" Text="Select Users"
                            CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%"></td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Panel ID="pnlUsers" runat="server" ScrollBars="Vertical" Height="250px">
                            <asp:CheckBoxList ID="ChkBoxListforUsers" runat="server">
                            </asp:CheckBoxList>
                        </asp:Panel>
                    </td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                    <td align="left" valign="top" style="width: 25%">&nbsp;</td>
                </tr>
            </table>


        </td>
    </tr>

    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">&nbsp;<asp:Button ID="btnSave" runat="server" Text="Save"
            CssClass="btn" Width="71px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False" />
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <telerik:RadGrid ID="gvAssignedUsersToReport" runat="server" AllowPaging="True" AllowSorting="True"
                AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                <ClientSettings>

                    <Scrolling UseStaticHeaders="true" AllowScroll="true" />
                </ClientSettings>
                <AlternatingItemStyle CssClass="rgAltRow" />
                <MasterTableView TableLayout="Fixed">
                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridTemplateColumn HeaderText="Approve" DataField="IsApproved">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" Select"
                                    TextAlign="Right" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn DataField="ReportUserID" HeaderText="ReportUserID" SortExpression="ReportUserID">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="UserID" HeaderText="UserID" SortExpression="UserID">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="UserName" HeaderText="User Name" SortExpression="UserName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="DisplayName" HeaderText="Display Name" SortExpression="DisplayName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Email" HeaderText="Email" SortExpression="Email">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="UserType" HeaderText="User Type" SortExpression="UserType">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Description" HeaderText="Description" SortExpression="Description">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn HeaderText="Approve" DataField="IsApproved" AllowSorting="false">
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridTemplateColumn>
                            <HeaderTemplate>
                                <asp:Label ID="lblDelete" runat="server" Text="Delete"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:ImageButton ID="IbtnDeleteUser" runat="server" CommandArgument='<%#Eval("ReportUserID") %>'
                                    CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                    ToolTip="Delete" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
                <ItemStyle CssClass="rgRow" />
                <PagerStyle AlwaysVisible="True" />
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">&nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnApprove" runat="server" Text="Approve Report Users"
                CssClass="btn" Width="138px" CausesValidation="False" />
            &nbsp;<asp:Button ID="btnDelete" runat="server" Text="Delete Report Users"
                CssClass="btn" Width="138px" CausesValidation="False" OnClientClick="javascript: return con();" />
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">&nbsp;</td>
    </tr>
</table>
