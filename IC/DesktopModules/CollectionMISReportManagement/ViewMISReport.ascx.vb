﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports Telerik.Web.UI

Partial Class DesktopModules_CollectionMISReportManagement_ViewMISReport
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
   
#Region "Page Load"
    Protected Sub DesktopModules_MISReportManagement_ViewMISReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnAddReport.Visible = CBool(htRights("Add"))
                LoadgvMISReportList(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Collection MIS Report Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(41))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Button Events"

    Protected Sub btnAddReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddReport.Click
        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveReport", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If
    End Sub

    Protected Sub btnDeleteReports_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteReports.Click
        Try
            Dim rowgvMISReportList As GridDataItem
            Dim chkSelect As CheckBox
            Dim ReportID As String = ""
            Dim PassCount As Integer = 0
            Dim FailCount As Integer = 0

            If CheckgvMISReportListForProcessAll() = True Then
                For Each rowgvMISReportList In gvMISReportList.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowgvMISReportList.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        ReportID = rowgvMISReportList("ReportID").Text.ToString()
                        Dim objReport As New ICCollectionMISReport
                        objReport.es.Connection.CommandTimeout = 3600
                        Try
                            ICCollectionMISReportController.DeleteReport(ReportID.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                            PassCount = PassCount + 1
                        Catch ex As Exception
                            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                FailCount = FailCount + 1
                            End If
                        End Try
                    End If

                Next

                If PassCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Delete Collection MIS Reports", "[" & FailCount.ToString() & "] MIS Reports can not be deleted due to following reasons : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    Exit Sub
                End If
                If FailCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Delete Collection MIS Reports", "[" & PassCount.ToString() & "] MIS Reports deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    Exit Sub
                End If
                UIUtilities.ShowDialog(Me, "Delete Collection MIS Reports", "[" & PassCount.ToString() & "] MIS Reports deleted successfully. [" & FailCount.ToString() & "] MIS Reports can not be deleted due to following reasons : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
            Else
                UIUtilities.ShowDialog(Me, "Delete Collection MIS Reports", "Please select atleast one(1) MIS Report.", ICBO.IC.Dialogmessagetype.Warning)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Function CheckgvMISReportListForProcessAll() As Boolean
        Try
            Dim rowgvMISReportList As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvMISReportList In gvMISReportList.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvMISReportList.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

#End Region


#Region "GridView"

    Private Sub LoadgvMISReportList(ByVal IsBind As Boolean)
        Try
            ICCollectionMISReportController.GetgvMISReportList(gvMISReportList.CurrentPageIndex + 1, gvMISReportList.PageSize, gvMISReportList, IsBind)

            If gvMISReportList.Items.Count > 0 Then
                gvMISReportList.Visible = True
                gvMISReportList.Columns(4).Visible = CBool(htRights("Delete"))
                btnDeleteReports.Visible = CBool(htRights("Delete"))
                lblRNF.Visible = False

            Else

                gvMISReportList.Visible = False
                btnDeleteReports.Visible = False
                lblRNF.Visible = True

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Protected Sub gvMISReportList_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvMISReportList.NeedDataSource
        LoadgvMISReportList(False)
    End Sub

    Protected Sub gvMISReportList_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvMISReportList.ItemCommand
        Try
            If e.CommandName = "del" Then
                Dim objReport As New ICCollectionMISReport

                objReport.LoadByPrimaryKey(e.CommandArgument.ToString())
                ICCollectionMISReportController.DeleteReport(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                UIUtilities.ShowDialog(Me, "Delete Collection MIS Report", "MIS Report deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                LoadgvMISReportList(True)
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Deleted Collection MIS Report", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvMISReportList_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvMISReportList.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvMISReportList.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvMISReportList_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvMISReportList.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelect"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvMISReportList.MasterTableView.ClientID & "','0');"
        End If
    End Sub

#End Region

End Class
