﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_PrincipalBankAccountManagement_ViewAccounts
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private AccNo As String
    Private BranchCode As String
    Private Currency As String
    Private htRights As Hashtable

#Region "Page Load"
    Protected Sub DesktopModules_AccountsManagement_ViewAccounts_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
        Try

            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing
                LoadBankAccounts(True)
                btnSavePrincipalBankAccount.Visible = CBool(htRights("Add"))
               
                'End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Bank Accounts Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

#Region "Button Events"

    Protected Sub btnSavePrincipalBankAccount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSavePrincipalBankAccount.Click
        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveAccount", "&mid=" & Me.ModuleId & "&PrincipalBankAccountID=0"), False)
        End If
    End Sub

    Protected Sub btnApproveAccounts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveAccounts.Click
        If Page.IsValid Then
            Try
                Dim rowgvBankAccounts As GridDataItem
                Dim chkSelect As CheckBox
                Dim PrincipalBankAccountID As String = ""
                Dim BranchCode As String = ""
                Dim Currency As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0
                Dim objBankAccounts As New ICBankAccounts

                If CheckgvBankAccountsForProcessAll() = True Then
                    For Each rowgvBankAccounts In gvBankAccounts.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvBankAccounts.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            PrincipalBankAccountID = Nothing
                            PrincipalBankAccountID = rowgvBankAccounts.GetDataKeyValue("PrincipalBankAccountID").ToString
                            objBankAccounts.es.Connection.CommandTimeout = 3600
                            If objBankAccounts.LoadByPrimaryKey(PrincipalBankAccountID) Then
                                If objBankAccounts.IsApproved Then
                                    FailCount = FailCount + 1
                                Else
                                    If Me.UserInfo.IsSuperUser = False Then
                                        If objBankAccounts.CreatedBy = Me.UserId Then
                                            FailCount = FailCount + 1
                                        Else
                                            ICBankAccountController.ApproveBankAccounts(PrincipalBankAccountID.ToString(), chkSelect.Checked, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                            PassCount = PassCount + 1
                                        End If
                                    Else
                                        ICBankAccountController.ApproveBankAccounts(PrincipalBankAccountID.ToString(), chkSelect.Checked, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                        PassCount = PassCount + 1
                                    End If
                                End If
                            End If
                        End If
                    Next

                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Approve Banks Accounts", "[" & FailCount.ToString() & "] Banks Accounts can not be approve due to following reasons : <br /> 1. Bank Account is approve.<br /> 2. Bank Accounts must be approve by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Approve Banks Accounts", "[" & PassCount.ToString() & "] Banks Accounts approve successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Approve Banks Accounts", "[" & PassCount.ToString() & "] Banks Accounts approve successfully. [" & FailCount.ToString() & "] Banks Accounts can not be approve due to following reasons : <br /> 1. Bank Account is approve.<br /> 2. Bank Accounts must be approve by the user other than the maker.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Approve Banks Accounts", "Please select atleast one(1) Bank Account.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Private Function CheckgvBankAccountsForProcessAll() As Boolean
        Try
            Dim rowgvBankAccounts As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvBankAccounts In gvBankAccounts.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvBankAccounts.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnDeleteAccounts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteAccounts.Click
        If Page.IsValid Then
            Try
                Dim rowgvBankAccounts As GridDataItem
                Dim chkSelect As CheckBox
                Dim PrincipalBankAccountID As String = ""
                Dim BranchCode As String = ""
                Dim Currency As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0

                If CheckgvBankAccountsForProcessAll() = True Then
                    For Each rowgvBankAccounts In gvBankAccounts.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvBankAccounts.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            PrincipalBankAccountID = Nothing





                            PrincipalBankAccountID = rowgvBankAccounts.GetDataKeyValue("PrincipalBankAccountID").ToString
                            Dim objBankAccounts As New ICBankAccounts
                            objBankAccounts.es.Connection.CommandTimeout = 3600
                            If objBankAccounts.LoadByPrimaryKey(PrincipalBankAccountID) Then
                                If objBankAccounts.IsApproved Then
                                    FailCount = FailCount + 1
                                Else
                                    Try
                                        ICBankAccountController.DeleteBankAccounts(PrincipalBankAccountID, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                        PassCount = PassCount + 1
                                    Catch ex As Exception
                                        If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                            FailCount = FailCount + 1
                                        End If
                                    End Try
                                End If
                            End If
                        End If

                    Next

                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Banks Accounts", "[" & FailCount.ToString() & "] Banks Accounts  can not be deleted due to following reasons : <br /> 1. Bank Account is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Banks Accounts", "[" & PassCount.ToString() & "] Banks Accounts deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Delete Banks Accounts", "[" & PassCount.ToString() & "] Banks Accounts deleted successfully. [" & FailCount.ToString() & "] Banks Accounts can not be deleted due to following reasons : <br /> 1. Bank Account is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Delete Banks Accounts", "Please select atleast one(1) Bank Account.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
#End Region

#Region "GridView Events"

    Private Sub LoadBankAccounts(ByVal IsBind As Boolean)
        Try
            ICBankAccountController.GetBankAccountsgv(gvBankAccounts.CurrentPageIndex + 1, gvBankAccounts.PageSize, gvBankAccounts, IsBind)
            If gvBankAccounts.Items.Count > 0 Then
                lblPrincipalBankAccountsListHeader.Visible = True
                lblBankAccountRNF.Visible = False
                gvBankAccounts.Visible = True
                btnDeleteAccounts.Visible = CBool(htRights("Delete"))
                btnApproveAccounts.Visible = CBool(htRights("Approve"))
                gvBankAccounts.Columns(10).Visible = CBool(htRights("Delete"))
            Else
                lblPrincipalBankAccountsListHeader.Visible = True
                gvBankAccounts.Visible = False
                btnApproveAccounts.Visible = False
                btnDeleteAccounts.Visible = False
                lblBankAccountRNF.Visible = True
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Protected Sub gvBankAccounts_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvBankAccounts.NeedDataSource
        Try
            LoadBankAccounts(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvBankAccounts_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvBankAccounts.PageIndexChanged
        gvBankAccounts.CurrentPageIndex = e.NewPageIndex
    End Sub

    Protected Sub gvBankAccounts_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvBankAccounts.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvBankAccounts.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvBankAccounts_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvBankAccounts.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvBankAccounts.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub gvBankAccounts_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvBankAccounts.ItemCommand
        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    Dim objBankAccount As New ICBankAccounts
                    Dim cmdArg1 As String = Nothing
                    Dim cmdArg2 As String = Nothing
                    Dim cmdArg3 As String = Nothing
                    Dim s As String()

                    objBankAccount.es.Connection.CommandTimeout = 3600

                    If objBankAccount.LoadByPrimaryKey(e.CommandArgument.ToString) Then
                        If objBankAccount.IsApproved Then
                            UIUtilities.ShowDialog(Me, "Warning", "Bank Account is approved and can not be deleted.", Dialogmessagetype.Failure)
                        Else
                            ICBankAccountController.DeleteBankAccounts(objBankAccount.PrincipalBankAccountID.ToString, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                            UIUtilities.ShowDialog(Me, "Deleted Bank Account", "Bank Account deleted successfully.", Dialogmessagetype.Success)
                            LoadBankAccounts(True)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Deleted Bank Account", "Cannot delete record. Please delete associated record(s) first.", Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

End Class
