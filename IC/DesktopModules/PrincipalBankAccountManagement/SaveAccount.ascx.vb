﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals

Partial Class DesktopModules_PrincipalBankAccountManagement_SaveAccount
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private PrincipalBankAccountID As String
  
    Private htRights As Hashtable

#Region "Page Load"
    Protected Sub DesktopModules_AccountsManagement_SaveAccount_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            PrincipalBankAccountID = Request.QueryString("PrincipalBankAccountID").ToString()
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                If PrincipalBankAccountID.ToString() = "0" Then
                    ViewState("htRights") = Nothing
                    GetPageAccessRights()

                    Clear()
                    lblIsApproved.Visible = False
                    chkApproved.Visible = False
                    btnApproved.Visible = False

                    btnCancel.Visible = True

                    lblPageHeader.Text = "Add Bank Account"
                    lblPageHeader.Visible = True
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    ViewState("htRights") = Nothing
                    GetPageAccessRights()
                    lblIsApproved.Visible = CBool(htRights("Approve"))
                    chkApproved.Visible = CBool(htRights("Approve"))
                    btnApproved.Visible = CBool(htRights("Approve"))
                    lblPageHeader.Visible = True
                    lblPageHeader.Text = "Edit Bank Account"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    txtAccountNumber.ReadOnly = True

                    Dim objICBankAccounts As New ICBankAccounts()
                    objICBankAccounts.es.Connection.CommandTimeout = 3600
                    objICBankAccounts.LoadByPrimaryKey(PrincipalBankAccountID)
                    If objICBankAccounts.LoadByPrimaryKey(PrincipalBankAccountID) Then
                        txtAccountName.Text = objICBankAccounts.AccountName.ToString()
                        txtAccountTitle.Text = objICBankAccounts.AccountTitle.ToString()
                        txtAccountNumber.Text = objICBankAccounts.AccountNumber.ToString()
                        txtBranchCode.Text = objICBankAccounts.BranchCode.ToString()
                        txtCurrency.Text = objICBankAccounts.Currency.ToString()
                        ddlCBAccountType.Enabled = True
                        chkActive.Checked = objICBankAccounts.IsActive
                        If objICBankAccounts.AccountType.ToString = "Payable Account" Then
                            ddlAccountType.SelectedValue = "Payable Account"

                        ElseIf objICBankAccounts.AccountType.ToString = "Stale Account" Then
                            ddlAccountType.SelectedValue = "Stale Account"

                        ElseIf objICBankAccounts.AccountType.ToString = "Normal Clearing Account" Then
                            ddlAccountType.SelectedValue = "Normal Clearing Account"

                        ElseIf objICBankAccounts.AccountType.ToString = "Intercity Clearing Account" Then
                            ddlAccountType.SelectedValue = "Intercity Clearing Account"

                        ElseIf objICBankAccounts.AccountType.ToString = "Same Day Clearing Account" Then
                            ddlAccountType.SelectedValue = "Same Day Clearing Account"

                        ElseIf objICBankAccounts.AccountType.ToString = "Branch Cash Till Account" Then
                            ddlAccountType.SelectedValue = "Branch Cash Till Account"
                        ElseIf objICBankAccounts.AccountType.ToString = "Global Charges Account" Then
                            ddlAccountType.SelectedValue = "Global Charges Account"
                        ElseIf objICBankAccounts.AccountType.ToString = "Contra GL Account" Then
                            ddlAccountType.SelectedValue = "Contra GL Account"
                            ddlCBAccountType.Enabled = False
                        End If


                        If Not objICBankAccounts.CBAccountType Is Nothing Then
                            If objICBankAccounts.CBAccountType = "GL" Then
                                ddlCBAccountType.SelectedValue = "GL"

                                txtAccountTitle.ReadOnly = False
                                txtCurrency.ReadOnly = True
                                txtBranchCode.ReadOnly = True

                            Else
                                ddlCBAccountType.SelectedValue = "RB"

                            End If
                        End If

                        If objICBankAccounts.IsApproved = True Then
                            chkApproved.Enabled = False
                            btnApproved.Visible = False
                            chkApproved.Checked = True
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Bank Accounts Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()

        ddlAccountType.ClearSelection()
        txtAccountTitle.Text = ""
        txtAccountNumber.Text = ""
        txtAccountName.Text = ""
        txtBranchCode.Text = ""
        txtCurrency.Text = ""
        chkActive.Checked = True
        ddlCBAccountType.ClearSelection()
        ddlCBAccountType.Enabled = True
        RadInputManager2.GetSettingByBehaviorID("Number").EmptyMessage = ""
    End Sub

#End Region

#Region "Buttons Events"

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim objICBankAccount As New ICBankAccounts()
                Dim BankIMD As String = ""
                Dim AccountStatus As String = ""
                objICBankAccount.es.Connection.CommandTimeout = 3600
                objICBankAccount.AccountTitle = txtAccountTitle.Text.ToString()
                objICBankAccount.AccountNumber = txtAccountNumber.Text.ToString()
                objICBankAccount.AccountName = txtAccountName.Text.ToString()
                objICBankAccount.AccountType = ddlAccountType.SelectedValue.ToString()
                objICBankAccount.CBAccountType = ddlCBAccountType.SelectedValue.ToString
                objICBankAccount.BranchCode = txtBranchCode.Text.ToString()
                objICBankAccount.Currency = txtCurrency.Text.ToString()
                If chkActive.Checked = True Then
                    objICBankAccount.IsActive = True
                Else
                    objICBankAccount.IsActive = False
                End If
                objICBankAccount.PrincipalBankCode = ICBankAccountController.GetPrincipleBankCodeandIMD().BankCode

                If ddlAccountType.SelectedValue.ToString.ToString() = "Stale Account" Then
                    ICBankAccountController.SetAllStaleAccountsAsFalse()
                    objICBankAccount.IsStaleAccount = True
                Else
                    objICBankAccount.IsStaleAccount = False
                End If
                If ddlCBAccountType.SelectedValue.ToString = "RB" Then

                ElseIf ddlCBAccountType.SelectedValue.ToString = "GL" Then
                    If txtAccountNumber.Text.Length <> 8 Then
                        UIUtilities.ShowDialog(Me, "Save Bank Account", "Invalid GL Account Number length", Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If


                If PrincipalBankAccountID = "0" Then
                    '  objICBankAccount.AccountTitle = txtAccountTitle.Text.ToString()
                    'objICBankAccount.BranchCode = txtBranchCode.Text.ToString()
                    'objICBankAccount.Currency = txtCurrency.Text.ToString()
                    objICBankAccount.CreatedBy = Me.UserId
                    objICBankAccount.CreatedDate = Date.Now
                    objICBankAccount.Creater = Me.UserId
                    objICBankAccount.CreationDate = Date.Now
                    If CheckDuplicate(objICBankAccount) = False Then
                        'If ddlCBAccountType.SelectedValue = "GL" Then
                        '    If CheckDuplicateAccNoBranchcodeCurrencyClientNoSeqNoProfitCentre(objICBankAccount) = True Then
                        '        UIUtilities.ShowDialog(Me, "Save Bank Account", "Can not add duplicate GL Account.", Dialogmessagetype.Failure)
                        '        Exit Sub
                        '    End If
                        'End If
                        ICBankAccountController.AddBankAccount(objICBankAccount, False, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        UIUtilities.ShowDialog(Me, "Save Bank Account", "Bank Account added successfully.", Dialogmessagetype.Success)
                        If ddlCBAccountType.SelectedValue = "GL" Then
                            ClearAllGLFields()
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Save Bank Account", "Can not add duplicate Bank Account.", Dialogmessagetype.Failure)
                        Exit Sub
                    End If


                Else
                    objICBankAccount.PrincipalBankAccountID = PrincipalBankAccountID
                    'If ddlCBAccountType.SelectedValue = "GL" Then
                    '    If CheckDuplicateAccNoBranchcodeCurrencyClientNoSeqNoProfitCentreOnEdit(objICBankAccount) = True Then
                    '        UIUtilities.ShowDialog(Me, "Save Bank Account", "Can not add duplicate GL Account.", Dialogmessagetype.Failure)
                    '        Exit Sub
                    '    End If
                    'End If
                    objICBankAccount.CreatedBy = Me.UserId
                    objICBankAccount.CreatedDate = Date.Now
                    'objICBankAccount.AccountNumber = txtAccountNumber.Text.ToString()
                    'objICBankAccount.AccountTitle = txtAccountTitle.Text.ToString()
                    'objICBankAccount.BranchCode = txtBranchCode.Text.ToString()
                    'objICBankAccount.Currency = txtCurrency.Text.ToString()
                    ICBankAccountController.AddBankAccount(objICBankAccount, True, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    UIUtilities.ShowDialog(Me, "Save Bank Account", "Bank Account updated successfully.", Dialogmessagetype.Success, NavigateURL())

                End If
                Clear()


            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Private Sub ClearAllGLFields()
      
        txtAccountTitle.ReadOnly = True
        txtCurrency.ReadOnly = True
        txtBranchCode.ReadOnly = True
        RadInputManager2.GetSettingByBehaviorID("Title").EmptyMessage = ""
        RadInputManager2.GetSettingByBehaviorID("Code").EmptyMessage = ""
        RadInputManager2.GetSettingByBehaviorID("Currency").EmptyMessage = ""
        RadInputManager2.GetSettingByBehaviorID("Number").EmptyMessage = ""
    End Sub

    Private Function CheckDuplicate(ByVal BankAccount As ICBankAccounts) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim collBankAccounts As New ICBankAccountsCollection

            'If ddlCBAccountType.SelectedValue = "RB" Then

            collBankAccounts = New ICBankAccountsCollection
            collBankAccounts.Query.Where(collBankAccounts.Query.AccountNumber.ToLower.Trim = BankAccount.AccountNumber.ToLower.Trim And collBankAccounts.Query.BranchCode.ToLower.Trim = BankAccount.BranchCode.ToLower.Trim)
            If collBankAccounts.Query.Load() Then
                rslt = True
            End If

            Return rslt
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
        End Try
    End Function

    Private Function CheckDuplicateAccNoBranchcodeCurrencyClientNoSeqNoProfitCentre(ByVal BankAccount As ICBankAccounts) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim collBankAccounts As New ICBankAccountsCollection

            collBankAccounts = New ICBankAccountsCollection

            collBankAccounts.Query.Where(collBankAccounts.Query.AccountNumber.ToLower.Trim = BankAccount.AccountNumber.ToLower.Trim)
            collBankAccounts.Query.Where(collBankAccounts.Query.BranchCode.ToLower.Trim = BankAccount.BranchCode.ToLower.Trim)
            collBankAccounts.Query.Where(collBankAccounts.Query.Currency.ToLower.Trim = BankAccount.Currency.ToLower.Trim)
            collBankAccounts.Query.Where(collBankAccounts.Query.ClientNo.ToLower.Trim = BankAccount.ClientNo.ToLower.Trim)
            collBankAccounts.Query.Where(collBankAccounts.Query.SeqNo = BankAccount.SeqNo)
            collBankAccounts.Query.Where(collBankAccounts.Query.ProfitCentre.ToLower.Trim = BankAccount.ProfitCentre.ToLower.Trim)

            If collBankAccounts.Query.Load() Then
                rslt = True
            End If

            Return rslt

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
        End Try
    End Function

    Private Function CheckDuplicateAccNoBranchcodeCurrencyClientNoSeqNoProfitCentreOnEdit(ByVal BankAccount As ICBankAccounts) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim collBankAccounts As New ICBankAccountsCollection

            collBankAccounts = New ICBankAccountsCollection

            collBankAccounts.Query.Where(collBankAccounts.Query.AccountNumber.ToLower.Trim <> BankAccount.AccountNumber.ToLower.Trim)
            collBankAccounts.Query.Where(collBankAccounts.Query.BranchCode.ToLower.Trim <> BankAccount.BranchCode.ToLower.Trim)
            collBankAccounts.Query.Where(collBankAccounts.Query.Currency.ToLower.Trim <> BankAccount.Currency.ToLower.Trim)

            collBankAccounts.Query.Where(collBankAccounts.Query.ClientNo.ToLower.Trim = BankAccount.ClientNo.ToLower.Trim)
            collBankAccounts.Query.Where(collBankAccounts.Query.SeqNo = BankAccount.SeqNo)
            collBankAccounts.Query.Where(collBankAccounts.Query.ProfitCentre.ToLower.Trim = BankAccount.ProfitCentre.ToLower.Trim)

            If collBankAccounts.Query.Load() Then
                rslt = True
            End If

            Return rslt

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproved.Click
        If Page.IsValid Then
            Try
                Dim objICBankAccount As New ICBankAccounts

                objICBankAccount.es.Connection.CommandTimeout = 3600

                objICBankAccount.LoadByPrimaryKey(PrincipalBankAccountID)
                If objICBankAccount.IsActive = False Or objICBankAccount.IsActive Is Nothing Then
                    UIUtilities.ShowDialog(Me, "Save Bank Accounts", "Bank Account must be active before approve", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                If Me.UserInfo.IsSuperUser = False Then
                    If objICBankAccount.CreatedBy = Me.UserId Then
                        UIUtilities.ShowDialog(Me, "Save Bank Accounts", "Bank Accounts must be approve by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If

                If chkApproved.Checked Then
                    ICBankAccountController.ApproveBankAccounts(PrincipalBankAccountID, chkApproved.Checked, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    UIUtilities.ShowDialog(Me, "Save Bank Accounts", "Bank Accounts approve.", Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Save Bank Accounts", "Please checked the Approved Check Box.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

#End Region


    Protected Sub txtAccountNumber_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAccountNumber.TextChanged
        Try
            Dim AccountStatus As String = ""
            Dim BankIMD As String = ""
            Dim AccountQry() As String
            Dim objICBankAccount As New ICBankAccounts()
            Dim BBAccNo As String = Nothing
            Dim RestraintsDetails As String = ""
            If txtAccountNumber.Text <> "" Then
                If CBUtilities.IsNormalAccountNoOrIBAN(txtAccountNumber.Text.ToString) = True Then
                    If CBUtilities.ValidateIBAN(txtAccountNumber.Text.ToString) = True Then
                        If ddlCBAccountType.SelectedValue.ToString = "RB" Then
                            BBAccNo = CBUtilities.GetBasicBankAccountNoFromIBAN(txtAccountNumber.Text.ToString)
                            BankIMD = ICBankAccountController.GetPrincipleBankCodeandIMD().BankIMD
                            Try
                                AccountStatus = CBUtilities.AccountStatus(BBAccNo, ICBO.CBUtilities.AccountType.RB, txtAccountNumber.Text.ToString(), txtAccountNumber.Text.ToString(), "Bank Account Status", "Add", txtBranchCode.Text)
                                AccountQry = CBUtilities.TitleFetch(BBAccNo, ICBO.CBUtilities.AccountType.RB, "Bank Account Title", txtAccountNumber.Text.ToString()).ToString.Split(";")
                                If AccountStatus = "Active" Then
                                    txtAccountTitle.Text = AccountQry(0).ToString()
                                    txtBranchCode.Text = AccountQry(1).ToString()
                                    txtCurrency.Text = AccountQry(2).ToString()
                                Else
                                    If AccountStatus.Contains("-") = True Then
                                        RestraintsDetails = AccountStatus.Split("-")(1)
                                        UIUtilities.ShowDialog(Me, "Warning", AccountStatus.ToString & "<br />" & RestraintsDetails, ICBO.IC.Dialogmessagetype.Warning)
                                    Else
                                        UIUtilities.ShowDialog(Me, "Warning", AccountStatus.ToString, ICBO.IC.Dialogmessagetype.Warning)
                                    End If
                                    txtAccountTitle.Text = AccountQry(0).ToString()
                                    txtBranchCode.Text = AccountQry(1).ToString()
                                    txtCurrency.Text = AccountQry(2).ToString()
                                End If
                            Catch ex As Exception
                                txtAccountTitle.Text = ""
                                txtBranchCode.Text = ""
                                txtCurrency.Text = ""
                                UIUtilities.ShowDialog(Me, "Save Bank Account", ex.Message.ToString(), Dialogmessagetype.Failure)
                                Exit Sub
                            End Try
                        End If

                    Else
                        txtAccountTitle.Text = ""
                        txtBranchCode.Text = ""
                        txtCurrency.Text = ""
                        UIUtilities.ShowDialog(Me, "Error", "Invalid bank account no.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                Else

                    If ddlCBAccountType.SelectedValue = "RB" Then

                        If txtAccountNumber.Text.ToString() <> "" Then
                            BankIMD = ICBankAccountController.GetPrincipleBankCodeandIMD().BankIMD
                            Try
                                AccountStatus = CBUtilities.AccountStatus(txtAccountNumber.Text.ToString(), ICBO.CBUtilities.AccountType.RB, "Bank Account Status", txtAccountNumber.Text.ToString(), "Status", "Add", txtBranchCode.Text)

                                AccountQry = CBUtilities.TitleFetch(txtAccountNumber.Text.ToString(), ICBO.CBUtilities.AccountType.RB, "Bank Account Title", txtAccountNumber.Text.ToString()).ToString.Split(";")
                                If AccountStatus = "Active" Then
                                    txtAccountTitle.Text = AccountQry(0).ToString()
                                    txtBranchCode.Text = AccountQry(1).ToString()
                                    txtCurrency.Text = AccountQry(2).ToString()
                                Else
                                    If AccountStatus.Contains("-") = True Then
                                        RestraintsDetails = AccountStatus.Split("-")(1)
                                        UIUtilities.ShowDialog(Me, "Warning", AccountStatus.ToString & "<br />" & RestraintsDetails, ICBO.IC.Dialogmessagetype.Warning)
                                    Else
                                        UIUtilities.ShowDialog(Me, "Warning", AccountStatus.ToString, ICBO.IC.Dialogmessagetype.Warning)
                                    End If
                                    txtAccountTitle.Text = AccountQry(0).ToString()
                                    txtBranchCode.Text = AccountQry(1).ToString()
                                    txtCurrency.Text = AccountQry(2).ToString()
                                End If
                            Catch ex As Exception
                                txtAccountTitle.Text = ""
                                txtBranchCode.Text = ""
                                txtCurrency.Text = ""
                                UIUtilities.ShowDialog(Me, "Save Bank Account", ex.Message.ToString(), Dialogmessagetype.Failure)
                                Exit Sub
                            End Try
                        Else
                            txtAccountNumber.Text = ""
                            txtAccountTitle.Text = ""
                            txtBranchCode.Text = ""
                            txtCurrency.Text = ""
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Protected Sub ddlAccountType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAccountType.SelectedIndexChanged
        Try
            ddlCBAccountType.Enabled = True
            If ddlAccountType.SelectedValue.ToString() = "Stale Account" Then
                If ICBankAccountController.IsStaleAccountPresent() = True Then
                    ddlAccountType.ClearSelection()
                    UIUtilities.ShowDialog(Me, "Save Bank Account", "Stale Account alreadt exist.", Dialogmessagetype.Failure)
                End If
                'ElseIf ddlAccountType.SelectedValue.ToString() = "Contra GL Account" Then
                '    ddlCBAccountType.SelectedValue = "GL"
                '    ddlCBAccountType.Enabled = False
                '    SetGLAccountTypeValues()
            ElseIf ddlAccountType.SelectedValue.ToString() = "0" Then
                ddlAccountType.ClearSelection()
                ddlCBAccountType.ClearSelection()
                SetGLAccountTypeValues()
            Else


                SetGLAccountTypeValues()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCBAccountType_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlCBAccountType.SelectedIndexChanged
        Try
            SetGLAccountTypeValues()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetGLAccountTypeValues()
        If ddlCBAccountType.SelectedValue = "GL" Then
          
            txtAccountTitle.ReadOnly = False
            txtCurrency.ReadOnly = False
            txtBranchCode.ReadOnly = False

            RadInputManager2.GetSettingByBehaviorID("Number").EmptyMessage = ""
            RadInputManager2.GetSettingByBehaviorID("Title").EmptyMessage = ""
            RadInputManager2.GetSettingByBehaviorID("Code").EmptyMessage = ""
            RadInputManager2.GetSettingByBehaviorID("Currency").EmptyMessage = ""

        Else
           
            txtAccountTitle.ReadOnly = True
            txtCurrency.ReadOnly = True
            txtBranchCode.ReadOnly = True

            RadInputManager2.GetSettingByBehaviorID("Title").EmptyMessage = ""
            RadInputManager2.GetSettingByBehaviorID("Code").EmptyMessage = ""
            RadInputManager2.GetSettingByBehaviorID("Currency").EmptyMessage = ""
            RadInputManager2.GetSettingByBehaviorID("Number").EmptyMessage = ""
        End If
    End Sub
End Class
