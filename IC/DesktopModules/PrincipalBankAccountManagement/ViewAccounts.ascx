﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewAccounts.ascx.vb"
    Inherits="DesktopModules_PrincipalBankAccountManagement_ViewAccounts" %>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
            }
        }
    }
</script>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }    
</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnSavePrincipalBankAccount" runat="server" Text="Add Principal Bank Account"
                            CssClass="btn" />
                        &nbsp;
                        <asp:HiddenField ID="hfAccountNumber" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <asp:Label ID="lblPrincipalBankAccountsListHeader" runat="server" Text="Principal Bank's Accounts List"
                            CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <telerik:RadGrid ID="gvBankAccounts" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100">
                            <ClientSettings>
                                <Scrolling UseStaticHeaders="true" AllowScroll="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView DataKeyNames="PrincipalBankAccountID" TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Approve" DataField="IsApproved">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select" TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="6%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="6%" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="AccountName" HeaderText="Account Name" SortExpression="AccountName">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="AccountType" HeaderText="Account Type" SortExpression="AccountType">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="6%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="6%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="Account Number" SortExpression="AccountNumber">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="15%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="AccountTitle" HeaderText="Account Title" SortExpression="AccountTitle">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="10%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BranchCode" HeaderText="Branch Code" SortExpression="BranchCode">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="6%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="6%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Currency" HeaderText="Currency" SortExpression="Currency">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="6%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="6%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Active" DataField="IsActive">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="6%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="6%" />
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Approve" DataField="IsApproved">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Label ID="Edit" runat="server" Text="Edit"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveAccount", "&mid=" & Me.ModuleId & "&PrincipalBankAccountID="& Eval("PrincipalBankAccountID"))%>'
                                                ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Label ID="Delete" runat="server" Text="Delete"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("PrincipalBankAccountID") %>'
                                                CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="4%" />
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnApproveAccounts" runat="server" Text="Approve Accounts" CssClass="btn" />
                        &nbsp;<asp:Button ID="btnDeleteAccounts" runat="server" Text="Delete Accounts" OnClientClick="javascript: return con();"
                            CssClass="btn" />
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Label ID="lblBankAccountRNF" runat="server" Text="Record Not Found." CssClass="headingblue"
                            Visible="false"></asp:Label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
