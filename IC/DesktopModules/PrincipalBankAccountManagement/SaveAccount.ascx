﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveAccount.ascx.vb"
    Inherits="DesktopModules_PrincipalBankAccountManagement_SaveAccount" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .style1
    {
        width: 604px;
    }
</style>
<telerik:RadInputManager ID="RadInputManager2" runat="server">
    <telerik:RegExpTextBoxSetting BehaviorID="Title" Validation-IsRequired="true" ErrorMessage="Invalid Account Title"
         ValidationExpression="^[a-zA-Z0-9 -_/()]{1,300}$">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountTitle" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="Name" Validation-IsRequired="true" ErrorMessage="Invalid Account Name"
        EmptyMessage="" ValidationExpression="^[a-zA-Z0-9 -_/()]{1,150}$">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
   
    <telerik:RegExpTextBoxSetting BehaviorID="Number" Validation-IsRequired="true" ErrorMessage="Invalid Account No."
       ValidationExpression="^[a-zA-Z0-9]{1,50}$">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAccountNumber" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="Currency" Validation-IsRequired="true"
        ErrorMessage="Invalid Currency"  ValidationExpression="^[a-zA-Z0-9]{3,3}$">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCurrency" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="Code" Validation-IsRequired="true" ValidationExpression="^[a-zA-Z0-9]{4,4}$"
        ErrorMessage="Invalid Branch Code ">
        <TargetControls>
            <telerik:TargetInput ControlID="txtBranchCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>

    
       <telerik:TextBoxSetting BehaviorID="radClientNo" Validation-IsRequired="true"
        ErrorMessage="Invalid Client Number" EmptyMessage ="" >
        <TargetControls>
            <telerik:TargetInput ControlID="txtClientNo" />
        </TargetControls>
    </telerik:TextBoxSetting>

      <telerik:RegExpTextBoxSetting BehaviorID="radSeqNo" Validation-IsRequired="true" ValidationExpression="^[0-9]{1,9}$"
        ErrorMessage="Invalid Sequence Number" EmptyMessage ="" >
        <TargetControls>
            <telerik:TargetInput ControlID="txtSeqNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>

      <telerik:TextBoxSetting BehaviorID="radProfitCentre" Validation-IsRequired="true"
        ErrorMessage="Invalid Profit Centre" EmptyMessage ="" >
        <TargetControls>
            <telerik:TargetInput ControlID="txtProfitCentre" />
        </TargetControls>
    </telerik:TextBoxSetting>


</telerik:RadInputManager>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
        <td align="left" valign="top" colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" class="style1">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" class="style1">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCBAccountType" runat="server" 
                Text="Core Banking Account Type" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblReqCBAccountType" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" class="style1">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCBAccountType" runat="server" AutoPostBack="True" 
                CssClass="dropdown">
                <asp:ListItem Value="0">--Please Select--</asp:ListItem>
                <asp:ListItem>GL</asp:ListItem>
                <asp:ListItem>RB</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" class="style1">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvCBAccountType" runat="server" ControlToValidate="ddlCBAccountType"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Core Banking Account Type"
                SetFocusOnError="True" ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" class="style1">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAccountType" runat="server" Text="Account Type" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label7" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblAccountName" runat="server" Text="Account Name" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label9" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlAccountType" runat="server" AutoPostBack="True" 
                CssClass="dropdown" TabIndex="1">
                <asp:ListItem Value="0">--Please Select--</asp:ListItem>
                <asp:ListItem>Payable Account</asp:ListItem>
                <asp:ListItem>Stale Account</asp:ListItem>
                <asp:ListItem>Normal Clearing Account</asp:ListItem>
                <asp:ListItem>Intercity Clearing Account</asp:ListItem>
                <asp:ListItem>Same Day Clearing Account</asp:ListItem>
                <asp:ListItem >Branch Cash Till Account</asp:ListItem>
                <asp:ListItem>Global Charges Account</asp:ListItem>
                <%--<asp:ListItem>Charges Account</asp:ListItem>--%>
            </asp:DropDownList>
            <br />
            <asp:RequiredFieldValidator ID="rfvAccountType" runat="server" ControlToValidate="ddlAccountType"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Account Type"
                SetFocusOnError="True" ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:TextBox ID="txtAccountName" runat="server" CssClass="txtbox" MaxLength="150"
                TabIndex="2"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblAccountNumber" runat="server" Text="Account Number" CssClass="lbl"></asp:Label>
            <asp:Label ID="Label8" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblAccountTitle" runat="server" Text="Account Title" CssClass="lbl"></asp:Label>
            <asp:Label ID="ReqAccountTitle" runat="server" Text="*" ForeColor="Red"></asp:Label>
            <br />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtAccountNumber" runat="server" CssClass="txtbox" MaxLength="50"
                AutoPostBack="True" TabIndex="3"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" class="style1">
            <asp:TextBox ID="txtAccountTitle" runat="server" CssClass="txtbox" MaxLength="300"
                TabIndex="4" ReadOnly="True"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" class="style1">
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCurrency" runat="server" Text="Currency" CssClass="lbl"></asp:Label>
            <asp:Label ID="ReqCurrency" runat="server" ForeColor="Red" Text="*"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblBranchCode" runat="server" Text="Branch Code" CssClass="lbl"></asp:Label>
            <asp:Label ID="ReqBranchCode" runat="server" ForeColor="Red" Text="*"></asp:Label>
            <br />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:TextBox ID="txtCurrency" runat="server" CssClass="txtbox" MaxLength="3" 
                TabIndex="5" ReadOnly="True"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" class="style1">
            <asp:TextBox ID="txtBranchCode" runat="server" CssClass="txtbox" MaxLength="4" 
                TabIndex="6" ReadOnly="True"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" class="style1">
        </td>
        <td align="left" valign="top" style="width: 25%">
        </td>
    </tr>


    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIsActive" runat="server" Text="Is Active" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:Label ID="lblIsApproved" runat="server" Text="Is Approved" CssClass="lbl" Visible="False"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkActive" runat="server" Text=" " CssClass="chkBox" Checked="True" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" class="style1">
            <asp:CheckBox ID="chkApproved" runat="server" Text=" " Visible="False" CssClass="chkBox" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnApproved" runat="server" Text="Approved" CssClass="btn" Visible="False"
                Width="71px" />
            &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" Width="71px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" CausesValidation="False"
                />
            &nbsp;
        </td>
    </tr>
</table>
