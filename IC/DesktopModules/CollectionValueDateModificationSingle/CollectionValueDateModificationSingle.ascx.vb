﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_CollectionValueDateModificationSingle_CollectionValueDateModificationSingle
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable

#Region "Page Load"
    Protected Sub DesktopModules_CompanyOfficeManagement_ViewCompanyOffice_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                btnSearch.Visible = CBool(htRights("Search"))
                tblMain.Style.Remove("display")
                tblInstrumentDetails.Style.Add("display", "none")
                txtNumber.Text = ""
                LoadddlBank()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Value Date Modification Single")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Grid View Events"

#End Region

#Region "Drop Down"

    Private Sub LoadddlBank()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlBank.Items.Clear()
            ddlBank.Items.Add(lit)
            ddlBank.AppendDataBoundItems = True
            ddlBank.DataSource = ICBankController.GetBank()
            ddlBank.DataTextField = "BankName"
            ddlBank.DataValueField = "BankCode"
            ddlBank.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region

#Region "Button Events"
#End Region

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Try
            Dim dt As New DataTable

            dt = ICCollectionsController.GetCollectionsForValueDateByBankCodeAndInstrumentNo(ddlBank.SelectedValue.ToString, txtNumber.Text.ToString)
            If dt.Rows.Count > 0 Then
                If dt.Rows(0)("CollectionStatusName") = "Cleared" Then
                    UIUtilities.ShowDialog(Me, "Value Date Modification Single", "Instrument already cleared", Dialogmessagetype.Failure)
                    Exit Sub
                Else
                    tblMain.Style.Add("display", "none")
                    tblInstrumentDetails.Style.Remove("display")
                    gvInstrumentDetails.DataSource = Nothing
                    txtValueDate.MinDate = Date.Now.AddDays(1)
                    txtValueDate.SelectedDate = Date.Now.AddDays(1)
                    gvInstrumentDetails.DataSource = dt
                    gvInstrumentDetails.DataBind()
                    btnModify.Visible = CBool(htRights("Modify"))
                End If
            Else
                UIUtilities.ShowDialog(Me, "Value Date Modification Single", "No Record found.", Dialogmessagetype.Failure)
                Exit Sub
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnBack2_Click(sender As Object, e As EventArgs) Handles btnBack2.Click
        Try
            Response.Redirect(NavigateURL())
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Try
            Response.Redirect(NavigateURL())
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnModify_Click(sender As Object, e As EventArgs) Handles btnModify.Click
        Try
            If ICHolidayManagementController.IsHoliday(txtValueDate.SelectedDate.ToString) = True Then
                UIUtilities.ShowDialog(Me, "Value Date Modification Single", "Selected date is holiday", Dialogmessagetype.Failure)
                Exit Sub
            End If
            If ICWorkingDaysController.IsWorkingDay(CDate(txtValueDate.SelectedDate).ToString("dddd")) = False Then
                UIUtilities.ShowDialog(Me, "Value Date Modification Single", "Selected date is non working day", Dialogmessagetype.Failure)
                Exit Sub
            End If
            If ICCollectionsController.ModifyValueDateByBankCodeAndInstrumentNo(gvInstrumentDetails.Items(0).GetDataKeyValue("BankCode").ToString, gvInstrumentDetails.Items(0).Item("InstrumentNo").Text.ToString, txtValueDate.SelectedDate, Me.UserInfo.UserID.ToString, Me.UserInfo.Username.ToString) = True Then
                UIUtilities.ShowDialog(Me, "Value Date Modification Single", "Value Date modified succcessfully", Dialogmessagetype.Success, NavigateURL())
            Else
                UIUtilities.ShowDialog(Me, "Value Date Modification Single", "Value Date not modified succcessfully", Dialogmessagetype.Failure, NavigateURL())
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
