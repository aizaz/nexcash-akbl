﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveCity.ascx.vb" Inherits="DesktopModules_Country_SaveCity" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
               <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior5" EmptyMessage="type.."
            Type="Number">
                    </telerik:NumericTextBoxSetting>                           
    <%--    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior6" Validation-IsRequired="true"
            ValidationExpression="^[ a-zA-Z][ a-zA-Z\\s]+$" ErrorMessage="Invalid Name" EmptyMessage="Enter City Name" >
            <TargetControls>
                <telerik:TargetInput ControlID="txtCityName" />
            </TargetControls>
        </telerik:RegExpTextBoxSetting>        --%>
          <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior2" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z ][a-zA-Z\\s ]+$" ErrorMessage="Invalid City Name"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCityName" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>

    <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior6" EmptyMessage="type.."
            Type="Number">
                    </telerik:NumericTextBoxSetting>                           
    <%--    <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior6" Validation-IsRequired="true"
            ValidationExpression="^[ a-zA-Z][ a-zA-Z\\s]+$" ErrorMessage="Invalid Name" EmptyMessage="Enter City Name" >
            <TargetControls>
                <telerik:TargetInput ControlID="txtCityName" />
            </TargetControls>
        </telerik:RegExpTextBoxSetting>        --%>
          <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior6" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9]{1,20}$" ErrorMessage="Invalid City Code"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDisplayCode" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>

    </telerik:RadInputManager>
<table width="100%">
<tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" Text="Add City" CssClass="headingblue"></asp:Label>
            &nbsp;
        </td>
        <td align="left" valign="top" colspan="2">
            
        </td>
    </tr>
    <tr align="left" valign="top" style="width:100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label6" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width:25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width:25%">
            &nbsp;</td>
    </tr>
    
    <tr align="left" valign="top" style="width:100%">
        <td align="left" valign="top" style="width:25%">
            <asp:Label ID="lblCountry" runat="server" Text="Country" CssClass="lbl"></asp:Label><asp:Label ID="Label2" runat="server" Text="*" ForeColor="Red"></asp:Label></td>
        <td align="left" valign="top" style="width:25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width:25%">
            <asp:Label ID="lblProvince" runat="server" Text="Province" CssClass="lbl"></asp:Label><asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label></td>
        <td align="left" valign="top" style="width:25%">
            &nbsp;</td>
    </tr>
    
    <tr align="left" valign="top" style="width:100%">
        <td align="left" valign="top" style="width:25%">
            <asp:DropDownList ID="ddlCountry" runat="server" AutoPostBack="True" 
                CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width:25%">
            &nbsp;</td>    
        <td align="left" valign="top" style="width:25%">
            <asp:DropDownList ID="ddlProvince" runat="server" AutoPostBack="True" 
                CssClass="dropdown">
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width:25%">
            &nbsp;</td>
    </tr>
    
    <tr align="left" valign="top" style="width:100%">
        <td align="left" valign="top" style="width:25%">
            <asp:RequiredFieldValidator ID="rfvCountry" runat="server" 
                ControlToValidate="ddlCountry" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Country" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width:25%">
            &nbsp;</td>    
        <td align="left" valign="top" style="width:25%">
            <asp:RequiredFieldValidator ID="rfvProvince" runat="server" 
                ControlToValidate="ddlProvince" CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select Province" SetFocusOnError="True" 
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
        </td>
        <td align="left" valign="top" style="width:25%">
            &nbsp;</td>
    </tr>
    
    <tr align="left" valign="top" style="width:100%">
        <td align="left" valign="top" style="width:25%">
            <asp:Label ID="lblDisplayCode" runat="server" Text="City Code" CssClass="lbl"></asp:Label><asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            </td>
        <td align="left" valign="top" style="width:25%">
            &nbsp;</td>    
        <td align="left" valign="top" style="width:25%">
            <asp:Label ID="lblCityName" runat="server" Text="City Name" 
                CssClass="lbl"></asp:Label><asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label></td>
        <td align="left" valign="top" style="width:25%">
            &nbsp;</td>
    </tr>
    
    <tr align="left" valign="top" style="width:100%">
        <td align="left" valign="top" style="width:25%">
            <asp:TextBox ID="txtDisplayCode" runat="server" MaxLength="10" CssClass="txtbox"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width:25%">
            &nbsp;</td>    
        <td align="left" valign="top" style="width:25%">
            <asp:TextBox ID="txtCityName" runat="server"   MaxLength="150" 
                CssClass="txtbox"></asp:TextBox>
        </td>
        <td align="left" valign="top" style="width:25%">
            &nbsp;</td>
    </tr>
    
    <tr align="left" valign="top" style="width:100%">
        <td align="left" valign="top" style="width:25%">
            <asp:Label ID="lblIsActive" runat="server" Text="Is Active" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width:25%">
            &nbsp;</td>    
        <td align="left" valign="top" style="width:25%">
            <asp:Label ID="lblIsApproved" runat="server" Text="Is Approved" CssClass="lbl" 
                Visible="False"></asp:Label>
            </td>
        <td align="left" valign="top" style="width:25%">
            &nbsp;</td>
    </tr>
    
    <tr align="left" valign="top" style="width:100%">
        <td align="left" valign="top" style="width:25%">
            <asp:CheckBox ID="chkActive" runat="server" Text=" " CssClass="chkBox" 
                Checked="True" />
        </td>
        <td align="left" valign="top" style="width:25%">
            &nbsp;</td>    
        <td align="left" valign="top" style="width:25%">
            <asp:CheckBox ID="chkApproved" runat="server" Text=" " Visible="False" 
                CssClass="chkBox" />
        </td>
        <td align="left" valign="top" style="width:25%">
            &nbsp;</td>
    </tr>
    
    <tr align="left" valign="top" style="width:100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnApproved" runat="server" Text="Approved" CssClass="btn" 
                Visible="False" Width="71px" />
        &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" 
                Width="70px" />
        &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" 
                CausesValidation="False"  />
        </td>
    </tr>
</table>