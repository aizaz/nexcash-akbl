﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Partial Class DesktopModules_Country_SaveCountry
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private CountryCode As String
    Private htRights As Hashtable


#Region "Page Load"

    'display code added by Jawwad 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            CountryCode = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing



                If CountryCode.ToString() = "0" Then
                    Clear()
                    lblIsApproved.Visible = False
                    chkApproved.Visible = False
                    btnApproved.Visible = False
                    btnSave.Visible = True
                    btnCancel.Visible = True
                    lblPageHeader.Text = "Add Country"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    lblIsApproved.Visible = CBool(htRights("Approve"))
                    chkApproved.Visible = CBool(htRights("Approve"))
                    btnApproved.Visible = CBool(htRights("Approve"))
                    btnCancel.Visible = True
                    lblPageHeader.Text = "Edit Country"
                    btnSave.Text = "Update"
                    btnSave.Visible = CBool(htRights("Update"))
                    Dim cCountry As New ICCountry
                    cCountry.es.Connection.CommandTimeout = 3600
                    cCountry.LoadByPrimaryKey(CountryCode)
                    If cCountry.LoadByPrimaryKey(CountryCode) Then

                        If cCountry.DisplayCode Is Nothing Then
                            txtDisplayCode.Text = ""
                        Else
                            txtDisplayCode.Text = cCountry.DisplayCode
                        End If
                        txtCountryName.Text = cCountry.CountryName.ToString()
                        chkActive.Checked = cCountry.IsActive
                        chkApproved.Checked = cCountry.IsApproved
                        If cCountry.IsApproved = True Then
                            chkApproved.Enabled = False
                            btnApproved.Visible = False
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Location Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        txtCountryName.Text = ""
        txtDisplayCode.Text = ""
        chkActive.Checked = True
    End Sub

#End Region

#Region "Button"

    'display code added by Jawwad 
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim cCountry As New ICCountry
                cCountry.es.Connection.CommandTimeout = 3600
                If CountryCode.ToString() <> "0" Then
                    cCountry.LoadByPrimaryKey(CountryCode)
                End If
                cCountry.DisplayCode = txtDisplayCode.Text
                cCountry.CountryName = txtCountryName.Text.ToString()
                cCountry.IsActive = chkActive.Checked

                If CountryCode = "0" Then
                    cCountry.CreateBy = Me.UserId
                    cCountry.CreateDate = Date.Now
                    cCountry.Creater = Me.UserId
                    cCountry.CreationDate = Date.Now
                    If CheckDuplicate(cCountry, False) = False Then
                        If CheckDuplicateDisplayCode(cCountry, False) = False Then
                            ICCountryController.AddCountry(cCountry, False, Me.UserId, Me.UserInfo.Username)
                            UIUtilities.ShowDialog(Me, "Save Country", "Country added successfully.", ICBO.IC.Dialogmessagetype.Success)
                            Clear()
                        Else
                            UIUtilities.ShowDialog(Me, "Save Country", "Can not add duplicate Country Code.", ICBO.IC.Dialogmessagetype.Failure)
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Save Country", "Can not add duplicate country.", ICBO.IC.Dialogmessagetype.Failure)
                    End If
                Else
                    cCountry.CreateBy = Me.UserId
                    cCountry.CreateDate = Date.Now
                    cCountry.CountryCode = CountryCode
                    If CheckDuplicate(cCountry, True) = False Then
                        If CheckDuplicateDisplayCode(cCountry, True) = False Then
                            ICCountryController.AddCountry(cCountry, True, Me.UserId, Me.UserInfo.Username)
                            UIUtilities.ShowDialog(Me, "Save Country", "Country updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Else
                            UIUtilities.ShowDialog(Me, "Save Country", "Can not update duplicate Country Code.", ICBO.IC.Dialogmessagetype.Failure)
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Save Country", "Can not update duplicate Country.", ICBO.IC.Dialogmessagetype.Failure)
                    End If



                End If

            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Private Function CheckDuplicate(ByVal cCountry As ICCountry, ByVal IsUpdatee As Boolean) As Boolean
        Dim rslt As Boolean = False
        Dim icCountry As New ICCountry
        icCountry.es.Connection.CommandTimeout = 3600
        Dim collCountry As New ICCountryCollection
        collCountry.es.Connection.CommandTimeout = 3600

        Try
            If IsUpdatee = False Then
                collCountry.Query.Where(collCountry.Query.CountryName.Like(cCountry.CountryName))
            Else
                collCountry.Query.Where(collCountry.Query.CountryName.Like(cCountry.CountryName) And collCountry.Query.CountryCode <> cCountry.CountryCode)
            End If
            If collCountry.Query.Load() Then
                rslt = True
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    'Check Duplicate code for display code added in db by Jawwad 
    Private Function CheckDuplicateDisplayCode(ByVal cCountry As ICCountry, ByVal IsUpdate As Boolean) As Boolean
        Dim rslt As Boolean = False
        Dim icCountry As New ICCountry
        icCountry.es.Connection.CommandTimeout = 3600
        Dim collCountry As New ICCountryCollection
        collCountry.es.Connection.CommandTimeout = 3600
        Try
            If IsUpdate = False Then
                collCountry.Query.Where(collCountry.Query.DisplayCode.Like(cCountry.DisplayCode))
            Else
                collCountry.Query.Where(collCountry.Query.DisplayCode.Like(cCountry.DisplayCode) And collCountry.Query.CountryCode <> cCountry.CountryCode)
            End If
            If collCountry.Query.Load() Then
                rslt = True
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    Protected Sub btnApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproved.Click
        Dim cCountry As New ICCountry
        cCountry.es.Connection.CommandTimeout = 3600
        If Page.IsValid Then
            Try
                cCountry.LoadByPrimaryKey(CountryCode.ToString())
                If Me.UserInfo.IsSuperUser = False Then
                    If cCountry.CreateBy = Me.UserId Then
                        UIUtilities.ShowDialog(Me, "Approve Country", "Country must be approve by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                If chkApproved.Checked Then
                    ICCountryController.ApproveCountry(CountryCode.ToString(), chkApproved.Checked, Me.UserId, Me.UserInfo.Username)
                    UIUtilities.ShowDialog(Me, "Approve Country", "Country approve.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Approve Country", "Please checked the Approved Check Box.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

#End Region

   
End Class
