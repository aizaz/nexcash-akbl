﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI

Partial Class DesktopModules_Country_SaveProvince
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ProvinceCode As String
    Private htRights As Hashtable

#Region "Page Load"

    'display code added by Jawwad 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            ProvinceCode = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing


                LoadddlCountry()
                If ProvinceCode.ToString() = "0" Then
                    Clear()
                    lblIsApproved.Visible = False
                    chkApproved.Visible = False
                    btnApproved.Visible = False
                    btnSave.Visible = True
                    btnCancel.Visible = True
                    lblPageHeader.Text = "Add Province"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    lblIsApproved.Visible = CBool(htRights("Approve"))
                    chkApproved.Visible = CBool(htRights("Approve"))
                    btnApproved.Visible = CBool(htRights("Approve"))
                    btnCancel.Visible = True
                    lblPageHeader.Text = "Edit Province"
                    btnSave.Text = "Update"
                    btnApproved.Visible = CBool(htRights("Approve"))
                    Dim cProvince As New ICProvince
                    cProvince.es.Connection.CommandTimeout = 3600
                    cProvince.LoadByPrimaryKey(ProvinceCode)

                    If cProvince.LoadByPrimaryKey(ProvinceCode) Then
                        If cProvince.DisplayCode Is Nothing Then
                            txtDisplayCode.Text = ""
                        Else
                            txtDisplayCode.Text = cProvince.DisplayCode

                        End If
                        txtProvinceName.Text = cProvince.ProvinceName.ToString()
                        ddlCountry.SelectedValue = cProvince.CountryCode.ToString()
                        chkActive.Checked = cProvince.IsActive
                        chkApproved.Checked = cProvince.IsApproved
                        If cProvince.IsApproved = True Then
                            chkApproved.Enabled = False
                            btnApproved.Visible = False
                        End If
                    End If
                End If
            End If
           
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Location Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        txtProvinceName.Text = ""
        txtDisplayCode.Text = ""
        ddlCountry.ClearSelection()
        chkActive.Checked = True
    End Sub

#End Region

#Region "Drop Down"

    Private Sub LoadddlCountry()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCountry.Items.Clear()
            ddlCountry.Items.Add(lit)
            ddlCountry.AppendDataBoundItems = True
            ddlCountry.DataSource = ICCountryController.GetCountryddl()
            ddlCountry.DataTextField = "CountryName"
            ddlCountry.DataValueField = "CountryCode"
            ddlCountry.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Button"

    'display code added by Jawwad 
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim cProvince As New ICProvince
                cProvince.es.Connection.CommandTimeout = 3600
                If ProvinceCode.ToString() <> "0" Then
                    cProvince.LoadByPrimaryKey(ProvinceCode)
                End If
                cProvince.DisplayCode = txtDisplayCode.Text
                cProvince.CountryCode = ddlCountry.SelectedValue.ToString()
                cProvince.ProvinceName = txtProvinceName.Text.ToString()
                cProvince.IsActive = chkActive.Checked
                If ProvinceCode = "0" Then
                    cProvince.CreateBy = Me.UserId
                    cProvince.CreateDate = Date.Now
                    cProvince.Creater = Me.UserId
                    cProvince.CreationDate = Date.Now
                    If CheckDuplicate(cProvince, False) = False Then
                        If CheckDuplicateDisplayCode(cProvince, False) = False Then
                            ICProvinceController.AddProvince(cProvince, False, Me.UserId, Me.UserInfo.Username)
                            UIUtilities.ShowDialog(Me, "Save Province", "Province added successfully.", ICBO.IC.Dialogmessagetype.Success)
                            Clear()
                        Else
                            UIUtilities.ShowDialog(Me, "Save Province", "Can not add duplicate Province Code.", ICBO.IC.Dialogmessagetype.Failure)
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Save Province", "Can not add duplicate Province.", ICBO.IC.Dialogmessagetype.Failure)
                    End If
                Else
                    cProvince.CreateBy = Me.UserId
                    cProvince.CreateDate = Date.Now
                    cProvince.ProvinceCode = ProvinceCode
                    If CheckDuplicate(cProvince, True) = False Then
                        If CheckDuplicateDisplayCode(cProvince, True) = False Then
                            ICProvinceController.AddProvince(cProvince, True, Me.UserId, Me.UserInfo.Username)
                            UIUtilities.ShowDialog(Me, "Save Province", "Province updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Else
                            UIUtilities.ShowDialog(Me, "Save Province", "Can not update duplicate Province Code.", ICBO.IC.Dialogmessagetype.Failure)
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Save Province", "Can not update duplicate Province.", ICBO.IC.Dialogmessagetype.Failure)
                    End If
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Private Function CheckDuplicate(ByVal cProvince As ICProvince, ByVal IsUpdatee As Boolean) As Boolean
        Dim rslt As Boolean = False
        Dim ICProvince As New ICProvince
        ICProvince.es.Connection.CommandTimeout = 3600
        Dim collProvince As New ICProvinceCollection
        collProvince.es.Connection.CommandTimeout = 3600

        Try
            If IsUpdatee = False Then
                collProvince.Query.Where(collProvince.Query.ProvinceName.Like(cProvince.ProvinceName))
            Else
                collProvince.Query.Where(collProvince.Query.ProvinceName.Like(cProvince.ProvinceName) And collProvince.Query.ProvinceCode <> cProvince.ProvinceCode)
            End If

            If collProvince.Query.Load() Then
                rslt = True
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    'Check Duplicate code for display code added in db by Jawwad 
    Private Function CheckDuplicateDisplayCode(ByVal cProvince As ICProvince, ByVal IsUpdate As Boolean) As Boolean
        Dim rslt As Boolean = False
        Dim ICProvince As New ICProvince
        ICProvince.es.Connection.CommandTimeout = 3600
        Dim collProvince As New ICProvinceCollection
        collProvince.es.Connection.CommandTimeout = 3600
        Try
            If IsUpdate = False Then
                collProvince.Query.Where(collProvince.Query.DisplayCode.Like(cProvince.DisplayCode))
            Else
                collProvince.Query.Where(collProvince.Query.DisplayCode.Like(cProvince.DisplayCode) And collProvince.Query.ProvinceCode <> cProvince.ProvinceCode)
            End If
            If collProvince.Query.Load() Then
                rslt = True
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    Protected Sub btnApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproved.Click
        Dim cProvince As New ICProvince
        cProvince.es.Connection.CommandTimeout = 3600
        If Page.IsValid Then
            Try
                cProvince.LoadByPrimaryKey(ProvinceCode.ToString())
                If Me.UserInfo.IsSuperUser = False Then
                    If cProvince.CreateBy = Me.UserId Then
                        UIUtilities.ShowDialog(Me, "Save Country", "Province must be approve by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                If chkApproved.Checked Then
                    ICProvinceController.ApproveProvince(ProvinceCode.ToString(), chkApproved.Checked, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    UIUtilities.ShowDialog(Me, "Save Province", "Province Approve.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Save Province", "Please checked the Approved Check Box.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Private Function ValidateCountry() As Boolean
        If ddlCountry.SelectedValue.ToString() = "0" Then
            Return False
        Else
            Return True
        End If
    End Function

#End Region

End Class
