﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewCountry.ascx.vb"
    Inherits="DesktopModules_Country_ViewCountry" %>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }    
</script>
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
            }
        }
    }
</script>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnSaveCountry" runat="server" Text="Add Country" CssClass="btn"
                            Width="100px" />
                        &nbsp;<asp:Button ID="btnSaveProvince" runat="server" Text="Add Province" CssClass="btn"
                            Width="100px" />
                        &nbsp;<asp:Button ID="btnSaveCity" runat="server" Text="Add City" CssClass="btn"
                            Width="108px" />
                        <asp:HiddenField ID="hfCountryCode" runat="server" />
                        <asp:HiddenField ID="hfProvinceCode" runat="server" />
                        <asp:HiddenField ID="hfCityCode" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Label ID="lblCountryListHeader" runat="server" Text="Country List" CssClass="headingblue"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <telerik:RadGrid ID="gvCountry" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100" CssClass="RadGrid">
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <ClientSettings>
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                            <MasterTableView TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Approve" DataField="IsApproved">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select" TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="CountryCode" HeaderText="ID" SortExpression="CountryCode">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CountryName" HeaderText="Country Name" SortExpression="CountryName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisplayCode" HeaderText="Country Code" SortExpression="DisplayCode">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Active" DataField="IsActive">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Approve" DataField="IsApproved">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Label ID="Edit" runat="server" Text="Edit"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveCountry", "&mid=" & Me.ModuleId & "&id="& Eval("CountryCode"))%>'
                                                ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Label ID="ShowProvince" runat="server" Text="View"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnShowProvince" runat="server" CommandArgument='<%#Eval("CountryCode") %>'
                                                CommandName="ShowProvince" ImageUrl="~/images/view.gif" ToolTip="View Province" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Label ID="Delete" runat="server" Text="Delete"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("CountryCode") %>'
                                                CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnApproveCountries" runat="server" Text="Approve Countries" CssClass="btn" />
                        &nbsp;<asp:Button ID="btnDeleteCountries" runat="server" Text="Delete Countries"
                            OnClientClick="javascript: return con();" CssClass="btn" />
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Label ID="lblProvinceListHeader" runat="server" Text="Province List" CssClass="headingblue"></asp:Label>
                        <br />
                        <asp:Label ID="lblProvinceRNF" runat="server" Text="Record Not Found." CssClass="headingblue"
                            Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <telerik:RadGrid ID="gvProvince" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100">
                             <ClientSettings>
                            <Scrolling UseStaticHeaders="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn >
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select" TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="ProvinceCode" HeaderText="ID" SortExpression="ProvinceCode">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProvinceName" HeaderText="Province Name" SortExpression="ProvinceName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisplayCode" HeaderText="Province Code" SortExpression="Display Code">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Active" DataField="IsActive">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Approve" DataField="IsApproved">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridTemplateColumn>
                                     <HeaderTemplate>
                                            <asp:Label ID="Edit" runat="server" Text="Edit"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveProvince", "&mid=" & Me.ModuleId & "&id="& Eval("ProvinceCode"))%>'
                                                ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn>
                                     <HeaderTemplate>
                                            <asp:Label ID="View" runat="server" Text="View"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnShowProvince" runat="server" CommandArgument='<%#Eval("ProvinceCode") %>'
                                                CommandName="ShowCities" ImageUrl="~/images/view.gif" ToolTip="View Cities" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn>
                                     <HeaderTemplate>
                                            <asp:Label ID="Delete" runat="server" Text="Delete"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("ProvinceCode") %>'
                                                CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnApproveProvinces" runat="server" Text="Approve Provinces" CssClass="btn" />
                        &nbsp;<asp:Button ID="btnDeleteProvinces" runat="server" Text="Delete Provinces"
                            CssClass="btn" />
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Label ID="lblCityListHeader" runat="server" Text="City List" CssClass="headingblue"></asp:Label>
                        <br />
                        <asp:Label ID="lblCityRNF" runat="server" Text="Record Not Found." CssClass="headingblue"
                            Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <telerik:RadGrid ID="gvCity" runat="server" AllowPaging="True" AllowSorting="True"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100">
                             <ClientSettings>
                            <Scrolling UseStaticHeaders="true" />
                            </ClientSettings>
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Approve" DataField="IsApproved">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Text=" Select" TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="CityCode" HeaderText="ID" SortExpression="CityCode">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CityName" HeaderText="City Name" SortExpression="CityName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisplayCode" HeaderText="City Code" SortExpression="DisplayCode">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Active" DataField="IsActive">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridCheckBoxColumn HeaderText="Approve" DataField="IsApproved">
                                    </telerik:GridCheckBoxColumn>
                                    <telerik:GridTemplateColumn>
                                     <HeaderTemplate>
                                            <asp:Label ID="Edit" runat="server" Text="Edit"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveCity", "&mid=" & Me.ModuleId & "&id="& Eval("CityCode"))%>'
                                                ToolTip="Edit">Edit</asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn>
                                     <HeaderTemplate>
                                            <asp:Label ID="Delete" runat="server" Text="Delete"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("CityCode") %>'
                                                CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                                ToolTip="Delete" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <asp:Button ID="btnApproveCities" runat="server" Text="Approve Cities" CssClass="btn" />
                        &nbsp;<asp:Button ID="btnDeleteCities" runat="server" Text="Delete Cities" CssClass="btn" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
