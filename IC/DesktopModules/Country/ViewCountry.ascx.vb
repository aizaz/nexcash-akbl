﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_Country_ViewCountry
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private Const ASCENDING As String = " ASC"
    Private Const DESCENDING As String = " DESC"
    Dim dtDa, dtProvince, dtCity As DataTable
    Private htRights As Hashtable

#Region "Page Load"

    Protected Sub DesktopModules_Country_ViewCountry_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If



            If Page.IsPostBack = False Then

                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing
                btnSaveCountry.Visible = CBool(htRights("Add"))
                btnSaveProvince.Visible = CBool(htRights("Add"))
                btnSaveCity.Visible = CBool(htRights("Add"))



                gvProvince.Visible = False
                gvCity.Visible = False

                btnDeleteCountries.Visible = False
                btnDeleteProvinces.Visible = False
                btnDeleteCities.Visible = False


                btnApproveCountries.Visible = False
                btnApproveProvinces.Visible = False
                btnApproveCities.Visible = False

                LoadCountries(True)
                ViewState("SortExp") = Nothing
                ViewState("SortExpProvince") = Nothing
                ViewState("SortExpCity") = Nothing
            End If
            CheckHeaderLabels()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Location Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub CheckHeaderLabels()
        If gvProvince.Items.Count > 0 Then
            lblProvinceListHeader.Visible = True
        Else
            lblProvinceListHeader.Visible = False
        End If
        If gvCity.Items.Count > 0 Then

            lblCityListHeader.Visible = True
        Else
            lblCityListHeader.Visible = False
        End If

    End Sub


#End Region

#Region "Drop Down"

    Private Sub LoadCountries(ByVal IsBind As Boolean)
        Try
            ICCountryController.GetCountrygv(gvCountry.CurrentPageIndex + 1, gvCountry.PageSize, gvCountry, IsBind)

            If gvCountry.Items.Count > 0 Then
                gvCountry.Visible = True
                btnDeleteCountries.Visible = CBool(htRights("Delete"))
                btnApproveCountries.Visible = CBool(htRights("Approve"))
                gvCountry.Columns(7).Visible = CBool(htRights("Delete"))

            Else
                gvCountry.Visible = False
                btnApproveCountries.Visible = False
                btnDeleteCountries.Visible = False
                lblProvinceRNF.Visible = False
                lblCityRNF.Visible = False

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadProvinces(ByVal CountryCode As String, ByVal IsBind As Boolean)
        Try
            ICProvinceController.GetProvincegv(CountryCode.ToString(), Me.gvProvince.CurrentPageIndex + 1, Me.gvProvince.PageSize, Me.gvProvince, IsBind)

            If gvProvince.Items.Count > 0 Then
                lblProvinceListHeader.Visible = True
                gvProvince.Visible = True
             
                lblProvinceRNF.Visible = False
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing

                btnDeleteProvinces.Visible = CBool(htRights("Delete"))
                btnApproveProvinces.Visible = CBool(htRights("Approve"))
                gvProvince.Columns(7).Visible = CBool(htRights("Delete"))
            Else
                lblProvinceListHeader.Visible = True
                gvProvince.Visible = False
                btnApproveProvinces.Visible = False
                btnDeleteProvinces.Visible = False
                lblProvinceRNF.Visible = True
                lblCityRNF.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub LoadCity(ByVal ProvinceCode As String, ByVal IsBind As Boolean)
        Try
            ICCityController.GetCitygv(ProvinceCode.ToString(), gvCity.CurrentPageIndex + 1, gvCity.PageSize, gvCity, IsBind)
            If gvCity.Items.Count > 0 Then
                lblCityListHeader.Visible = True
                lblCityRNF.Visible = False
                gvCity.Visible = True
                btnApproveCities.Visible = True
                btnDeleteCities.Visible = True
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing
                btnDeleteCities.Visible = CBool(htRights("Delete"))
                btnApproveCities.Visible = CBool(htRights("Approve"))
                gvCity.Columns(6).Visible = CBool(htRights("Delete"))
            Else
                lblCityListHeader.Visible = True
                gvCity.Visible = False
                btnApproveCities.Visible = False
                btnDeleteCities.Visible = False
                lblCityRNF.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

#End Region

      Protected Sub btnSaveCountry_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveCountry.Click
        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveCountry", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If

    End Sub

    Protected Sub btnSaveProvince_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveProvince.Click
        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveProvince", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If

    End Sub

    Protected Sub btnSaveCity_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveCity.Click
        If Page.IsValid Then
            Response.Redirect(NavigateURL("SaveCity", "&mid=" & Me.ModuleId & "&id=0"), False)
        End If

    End Sub

    Protected Sub gvCity_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvCity.ItemCommand
        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    Dim frcCity As New ICCity
                    frcCity.es.Connection.CommandTimeout = 3600
                    frcCity.LoadByPrimaryKey(e.CommandArgument.ToString())
                    If frcCity.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                        If frcCity.IsApproved Then
                            UIUtilities.ShowDialog(Me, "Warning", "City is approved and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
                            LoadCity(hfProvinceCode.Value.ToString(), True)
                        Else
                            ICCityController.DeleteCity(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                            UIUtilities.ShowDialog(Me, "Delete City", "City deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                            LoadCity(hfProvinceCode.Value.ToString(), True)
                        End If
                    End If
                End If

            End If

        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Delete City", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvProvince_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvProvince.ItemCommand
        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    Dim frcProvince As New ICProvince
                    frcProvince.es.Connection.CommandTimeout = 3600
                    frcProvince.LoadByPrimaryKey(e.CommandArgument.ToString())
                    If frcProvince.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                        If frcProvince.IsApproved Then
                            UIUtilities.ShowDialog(Me, "Warning", frcProvince.ProvinceName.ToString() & " is approved and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
                        Else
                            ICProvinceController.DeleteProvince(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                            UIUtilities.ShowDialog(Me, "Delete Province", "Province deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                            LoadProvinces(hfCountryCode.Value.ToString(), True)
                        End If
                    End If
                End If
            ElseIf e.CommandName = "ShowCities" Then
                    hfProvinceCode.Value = e.CommandArgument.ToString()
                Dim frcProvince As New ICProvince
                frcProvince.es.Connection.CommandTimeout = 3600
                frcProvince.LoadByPrimaryKey(e.CommandArgument.ToString())
                If frcProvince.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                    lblCityListHeader.Text = "City List of " & frcProvince.ProvinceName.ToString()
                Else
                    lblCityListHeader.Text = "City List"
                End If
                LoadCity(hfProvinceCode.Value.ToString(), True)
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Delete Province", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvCountry_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvCountry.ItemCommand
        Try

            If e.CommandName = "del" Then
                If Page.IsValid Then
                    Dim icCountry As New ICCountry
                    icCountry.es.Connection.CommandTimeout = 3600
                    If icCountry.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                        If icCountry.IsApproved Then
                            UIUtilities.ShowDialog(Me, "Warning", "Country is approved and can not be deleted.", ICBO.IC.Dialogmessagetype.Failure)
                        Else
                            ICCountryController.DeleteCountry(e.CommandArgument.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                            UIUtilities.ShowDialog(Me, "Delete Country", "Country deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                            LoadCountries(True)
                            lblProvinceRNF.Visible = False
                        End If
                    End If
                End If
                ElseIf e.CommandName = "ShowProvince" Then
                    hfCountryCode.Value = e.CommandArgument.ToString()
                Dim frcCountry As New ICCountry
                frcCountry.es.Connection.CommandTimeout = 3600
                frcCountry.LoadByPrimaryKey(e.CommandArgument.ToString())
                If frcCountry.LoadByPrimaryKey(e.CommandArgument.ToString()) Then
                    lblProvinceListHeader.Text = "Province List of " & frcCountry.CountryName.ToString()
                Else
                    lblProvinceListHeader.Text = "Province List"
                End If
                LoadProvinces(hfCountryCode.Value.ToString(), True)
                LoadCity(0, True)
                lblCityListHeader.Visible = False
                lblCityRNF.Visible = False
            End If

        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Delete Country", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnApproveCountries_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveCountries.Click
        If Page.IsValid Then
            Try
                Dim rowgvCountry As GridDataItem
                Dim chkSelect As CheckBox
                Dim CountryCode As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0
                Dim icCountry As New ICCountry

                If CheckgvCountryForProcessAll() = True Then
                    For Each rowgvCountry In gvCountry.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvCountry.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            CountryCode = rowgvCountry("CountryCode").Text.ToString()
                            icCountry.es.Connection.CommandTimeout = 3600
                            If icCountry.LoadByPrimaryKey(CountryCode.ToString()) Then
                                If icCountry.IsApproved Then
                                    FailCount = FailCount + 1
                                Else
                                    If Me.UserInfo.IsSuperUser = False Then
                                        If icCountry.CreateBy = Me.UserId Then
                                            FailCount = FailCount + 1
                                        Else
                                            ICCountryController.ApproveCountry(CountryCode.ToString(), chkSelect.Checked, Me.UserId, Me.UserInfo.Username)
                                            PassCount = PassCount + 1
                                        End If
                                    Else
                                        ICCountryController.ApproveCountry(CountryCode.ToString(), chkSelect.Checked, Me.UserId, Me.UserInfo.Username)
                                        PassCount = PassCount + 1
                                    End If
                                End If
                            End If
                        End If

                    Next
                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Approve Countries", "[" & FailCount.ToString() & "] Countries can not be approve due to following reasons : <br /> 1. Country is approve.<br /> 2. Country must be approve by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Approve Countries", "[" & PassCount.ToString() & "] Countries approve successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Approve Countries", "[" & PassCount.ToString() & "] Countries approve successfully. [" & FailCount.ToString() & "] Countries can not be approve due to following reasons : <br /> 1. Country is approve.<br /> 2. Country must be approve by the user other than the maker.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Approve Countries", "Please select atleast one(1) Country.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Private Function CheckgvCountryForProcessAll() As Boolean
        Try
            Dim rowgvCountry As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvCountry In gvCountry.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvCountry.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub btnDeleteCountries_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteCountries.Click
        If Page.IsValid Then
            Try
                Dim rowgvCountry As GridDataItem
                Dim chkSelect As CheckBox
                Dim CountryCode As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0

                If CheckgvCountryForProcessAll() = True Then
                    For Each rowgvCountry In gvCountry.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvCountry.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            CountryCode = rowgvCountry("CountryCode").Text.ToString()
                            Dim icCountry As New ICCountry
                            icCountry.es.Connection.CommandTimeout = 3600
                            If icCountry.LoadByPrimaryKey(CountryCode.ToString()) Then
                                If icCountry.IsApproved Then
                                    FailCount = FailCount + 1
                                Else
                                    Try
                                        ICCountryController.DeleteCountry(CountryCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                        PassCount = PassCount + 1
                                    Catch ex As Exception
                                        If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                            FailCount = FailCount + 1
                                        End If
                                    End Try
                                End If
                            End If
                        End If

                    Next

                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Countries", "[" & FailCount.ToString() & "] Countries can not be deleted due to following reasones : <br /> 1. Country is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Countries", "[" & PassCount.ToString() & "] Countries deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Delete Countries", "[" & PassCount.ToString() & "] Countries deleted successfully. [" & FailCount.ToString() & "] Countries can not be deleted due to following reasons : <br /> 1. Country is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Delete Countries", "Please select atleast one(1) Country.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If


    End Sub

    Protected Sub btnDeleteProvinces_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteProvinces.Click
        If Page.IsValid Then
            Try
                Dim rowgvProvince As GridDataItem
                Dim chkSelect As CheckBox
                Dim ProvinceCode As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0

                If CheckgvProvinceForProcessAll() = True Then
                    For Each rowgvProvince In gvProvince.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvProvince.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            ProvinceCode = rowgvProvince("ProvinceCode").Text.ToString()
                            Dim icProvince As New ICProvince
                            icProvince.es.Connection.CommandTimeout = 3600
                            If icProvince.LoadByPrimaryKey(ProvinceCode.ToString()) Then
                                If icProvince.IsApproved Then
                                    FailCount = FailCount + 1
                                Else
                                    Try
                                        ICProvinceController.DeleteProvince(ProvinceCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())

                                        PassCount = PassCount + 1
                                    Catch ex As Exception
                                        If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                            FailCount = FailCount + 1
                                        End If
                                    End Try
                                End If
                            End If
                        End If

                    Next

                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Provinces", "[" & FailCount.ToString() & "] Provinces can not be deleted due to following reasons : <br /> 1. Province is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Provinces", "[" & PassCount.ToString() & "] Provinces deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Delete Provinces", "[" & PassCount.ToString() & "] Provinces deleted successfully. [" & FailCount.ToString() & "] Provinces can not be deleted due to following reasons : <br /> 1. Province is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Delete Province", "Please select atleast one(1) Province.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Private Function CheckgvProvinceForProcessAll() As Boolean
        Try
            Dim rowgvProvince As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvProvince In gvProvince.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvProvince.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnApproveProvinces_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveProvinces.Click
        If Page.IsValid Then
            Try
                Dim rowgvProvince As GridDataItem
                Dim chkSelect As CheckBox
                Dim ProvinceCode As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0
                Dim icProvince As New ICProvince

                If CheckgvProvinceForProcessAll() = True Then
                    For Each rowgvProvince In gvProvince.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvProvince.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            ProvinceCode = rowgvProvince("ProvinceCode").Text.ToString()
                            icProvince.es.Connection.CommandTimeout = 3600
                            If icProvince.LoadByPrimaryKey(ProvinceCode.ToString()) Then
                                If icProvince.IsApproved Then
                                    FailCount = FailCount + 1
                                Else
                                    If Me.UserInfo.IsSuperUser = False Then
                                        If icProvince.CreateBy = Me.UserId Then
                                            FailCount = FailCount + 1
                                        Else
                                            ICProvinceController.ApproveProvince(ProvinceCode.ToString(), chkSelect.Checked, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                            PassCount = PassCount + 1
                                        End If
                                    Else
                                        ICProvinceController.ApproveProvince(ProvinceCode.ToString(), chkSelect.Checked, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                        PassCount = PassCount + 1
                                    End If

                                End If
                            End If
                        End If

                    Next

                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Approve Provinces", "[" & FailCount.ToString() & "]  Provinces can not be approved due to following reasons : <br /> 1.  Province is approved.<br /> 2.  Provinces must be approved by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Approve Provinces", "[" & PassCount.ToString() & "] Provinces approved successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Approve Provinces", "[" & PassCount.ToString() & "] Provinces approved successfully. [" & FailCount.ToString() & "] Provinces can not be approved due to following reasons : <br /> 1.  Province is approved.<br /> 2.  Provinces must be approved by the user other than the maker.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())

                Else
                    UIUtilities.ShowDialog(Me, "Approve Provinces", "Please select atleast one(1) Province.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnDeleteCities_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteCities.Click
        If Page.IsValid Then
            Try
                Dim rowgvCity As GridDataItem
                Dim chkSelect As CheckBox
                Dim CityCode As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0
                Dim icCity As New ICCity

                If CheckgvCityForProcessAll() = True Then
                    For Each rowgvCity In gvCity.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvCity.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            CityCode = rowgvCity("CityCode").Text.ToString()
                            icCity.es.Connection.CommandTimeout = 3600
                            If icCity.LoadByPrimaryKey(CityCode.ToString()) Then
                                If icCity.IsApproved Then
                                    FailCount = FailCount + 1
                                Else
                                    Try
                                        ICCityController.DeleteCity(CityCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                        PassCount = PassCount + 1
                                    Catch ex As Exception
                                        If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                            FailCount = FailCount + 1
                                        End If
                                    End Try
                                End If
                            End If
                        End If

                    Next

                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Cities", "[" & FailCount.ToString() & "] Cities can not be deleted due to following reasons : <br /> 1. City is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Delete Cities", "[" & PassCount.ToString() & "] Cities deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Delete Cities", "[" & PassCount.ToString() & "] Cities deleted successfully. [" & FailCount.ToString() & "] Cities can not be deleted due to following reasons : <br /> 1. City is approved.<br /> 2. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())

                Else
                    UIUtilities.ShowDialog(Me, "Delete Cities", "Please select atleast one(1) City.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Private Function CheckgvCityForProcessAll() As Boolean
        Try
            Dim rowgvCity As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvCity In gvCity.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvCity.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnApproveCities_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveCities.Click
        If Page.IsValid Then
            Try
                Dim rowgvCity As GridDataItem
                Dim chkSelect As CheckBox
                Dim CityCode As String = ""
                Dim PassCount As Integer = 0
                Dim FailCount As Integer = 0
                Dim icCity As New ICCity

                If CheckgvCityForProcessAll() = True Then
                    For Each rowgvCity In gvCity.Items
                        chkSelect = New CheckBox
                        chkSelect = DirectCast(rowgvCity.Cells(0).FindControl("chkSelect"), CheckBox)
                        If chkSelect.Checked = True Then
                            CityCode = rowgvCity("CityCode").Text.ToString()
                            icCity.es.Connection.CommandTimeout = 3600
                            If icCity.LoadByPrimaryKey(CityCode.ToString()) Then
                                If icCity.IsApproved Then
                                    FailCount = FailCount + 1
                                Else
                                    If Me.UserInfo.IsSuperUser = False Then
                                        If icCity.CreateBy = Me.UserId Then
                                            FailCount = FailCount + 1
                                        Else
                                            ICCityController.ApproveCity(CityCode.ToString(), chkSelect.Checked, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                            PassCount = PassCount + 1
                                        End If
                                    Else
                                        ICCityController.ApproveCity(CityCode.ToString(), chkSelect.Checked, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                                        PassCount = PassCount + 1
                                    End If

                                End If
                            End If
                        End If

                    Next

                    If PassCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Approve Cities", "[" & FailCount.ToString() & "]  Cities can not be approved due to following reasons : <br /> 1.  Cities is approved.<br /> 2.  Cities must be approved by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                        Exit Sub
                    End If
                    If FailCount = 0 Then
                        UIUtilities.ShowDialog(Me, "Approve Cities", "[" & PassCount.ToString() & "] Cities approved successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Exit Sub
                    End If
                    UIUtilities.ShowDialog(Me, "Approve Cities", "[" & PassCount.ToString() & "] Cities approved successfully. [" & FailCount.ToString() & "] Cities can not be approved due to following reasons : <br /> 1.  City is approved.<br /> 2.  Cities must be approved by the user other than the maker.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())

                Else
                    UIUtilities.ShowDialog(Me, "Approve Cities", "Please select atleast one(1) City.", ICBO.IC.Dialogmessagetype.Warning)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Protected Sub gvCountry_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvCountry.NeedDataSource
        LoadCountries(False)
    End Sub

    Protected Sub gvCountry_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvCountry.PageIndexChanged
        gvCountry.CurrentPageIndex = e.NewPageIndex
    End Sub

    Protected Sub gvProvince_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvProvince.NeedDataSource
        LoadProvinces(hfCountryCode.Value.ToString(), False)
    End Sub

    Protected Sub gvProvince_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvProvince.PageIndexChanged
        gvProvince.CurrentPageIndex = e.NewPageIndex
    End Sub

    Protected Sub gvCity_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvCity.NeedDataSource
        LoadCity(hfProvinceCode.Value.ToString(), False)
    End Sub

    Protected Sub gvCity_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvCity.PageIndexChanged
        gvCity.CurrentPageIndex = e.NewPageIndex
    End Sub

    Protected Sub gvCountry_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvCountry.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
               For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvCountry.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvProvince_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvProvince.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvProvince.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvCity_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvCity.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
               For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvCity.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvCountry_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvCountry.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvCountry.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub gvProvince_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvProvince.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvProvince.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub gvCity_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvCity.ItemDataBound
        Dim chkProcessAll As CheckBox

        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvCity.MasterTableView.ClientID & "','0');"
        End If
    End Sub
End Class

