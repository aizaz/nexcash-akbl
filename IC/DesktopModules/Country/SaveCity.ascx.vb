﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals

Partial Class DesktopModules_Country_SaveCity
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private CityCode As String
    Private htRights As Hashtable

#Region "Page Load"

    'display code added by Jawwad 
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            CityCode = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing


                LoadddlCountry()
                LoadddlProvince()
                If CityCode.ToString() = "0" Then
                    Clear()
                    lblIsApproved.Visible = False
                    chkApproved.Visible = False
                    btnApproved.Visible = False
                    btnSave.Visible = True
                    btnCancel.Visible = True
                    lblPageHeader.Text = "Add City"
                    btnSave.Text = "Save"
                    btnSave.Visible = CBool(htRights("Add"))
                Else
                    lblIsApproved.Visible = CBool(htRights("Approve"))
                    chkApproved.Visible = CBool(htRights("Approve"))

                    btnCancel.Visible = True
                    lblPageHeader.Text = "Edit City"
                    btnSave.Text = "Update"
                    btnApproved.Visible = CBool(htRights("Approve"))
                    btnSave.Visible = CBool(htRights("Update"))
                    Dim cCity As New ICCity
                    cCity.es.Connection.CommandTimeout = 3600
                    cCity.LoadByPrimaryKey(CityCode)
                    If cCity.LoadByPrimaryKey(CityCode) Then
                        If cCity.DisplayCode Is Nothing Then
                            txtDisplayCode.Text = ""
                        Else
                            txtDisplayCode.Text = cCity.DisplayCode
                        End If
                        txtCityName.Text = cCity.CityName.ToString()
                        Dim icprovince As New ICProvince
                        icprovince.es.Connection.CommandTimeout = 3600
                        icprovince.LoadByPrimaryKey(cCity.ProvinceCode.ToString())
                        If icprovince.LoadByPrimaryKey(cCity.ProvinceCode.ToString()) Then
                            ddlCountry.SelectedValue = icprovince.CountryCode.ToString()
                            LoadddlProvince()
                            ddlProvince.SelectedValue = cCity.ProvinceCode.ToString()
                        End If
                        chkActive.Checked = cCity.IsActive
                        chkApproved.Checked = cCity.IsApproved
                        If cCity.IsApproved = True Then
                            chkApproved.Enabled = False
                            btnApproved.Visible = False
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Location Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        txtCityName.Text = ""
        txtDisplayCode.Text = ""
        ddlCountry.SelectedValue = "0"
        ddlProvince.SelectedValue = "0"
        chkActive.Checked = True

        If ddlCountry.SelectedValue = "0" Then
            LoadddlProvince()
        End If

    End Sub

#End Region

#Region "Drop Down"

    Private Sub LoadddlCountry()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCountry.Items.Clear()
            ddlCountry.Items.Add(lit)
            ddlCountry.AppendDataBoundItems = True
            ddlCountry.DataSource = ICCountryController.GetCountryddl()
            ddlCountry.DataTextField = "CountryName"
            ddlCountry.DataValueField = "CountryCode"
            ddlCountry.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlProvince()
        Try
            ddlProvince.Items.Clear()
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlProvince.Items.Add(lit)
            ddlProvince.AppendDataBoundItems = True
            ddlProvince.DataSource = ICProvinceController.GetProvinceddl(ddlCountry.SelectedValue.ToString())
            ddlProvince.DataTextField = "ProvinceName"
            ddlProvince.DataValueField = "ProvinceCode"
            ddlProvince.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        LoadddlProvince()
    End Sub

#End Region

#Region "Button"

    'display code added by Jawwad 
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim cCity As New ICCity
                cCity.es.Connection.CommandTimeout = 3600
                If CityCode.ToString() <> "0" Then
                    cCity.LoadByPrimaryKey(CityCode)

                End If
                cCity.DisplayCode = txtDisplayCode.Text
                cCity.ProvinceCode = ddlProvince.SelectedValue.ToString()
                cCity.CityName = txtCityName.Text.ToString()
                cCity.IsActive = chkActive.Checked
                If CityCode = "0" Then
                    cCity.CreateBy = Me.UserId
                    cCity.CreateDate = Date.Now
                    cCity.Creater = Me.UserId
                    cCity.CreationDate = Date.Now
                    If CheckDuplicate(cCity, False) = False Then
                        If CheckDuplicateDisplayCode(cCity, False) = False Then
                            ICCityController.AddCity(cCity, False, Me.UserId, Me.UserInfo.Username)
                            UIUtilities.ShowDialog(Me, "Save City", "City added successfully.", ICBO.IC.Dialogmessagetype.Success)
                            Clear()
                        Else
                            UIUtilities.ShowDialog(Me, "Save City", "Can not add duplicate City Code.", ICBO.IC.Dialogmessagetype.Failure)
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Save City", "Can not add duplicate City.", ICBO.IC.Dialogmessagetype.Failure)
                    End If
                Else

                    cCity.CreateBy = Me.UserId
                    cCity.CreateDate = Date.Now
                    cCity.CityCode = CityCode
                    If CheckDuplicate(cCity, True) = False Then
                        If CheckDuplicateDisplayCode(cCity, True) = False Then
                            ICCityController.AddCity(cCity, True, Me.UserId, Me.UserInfo.Username)
                            UIUtilities.ShowDialog(Me, "Save City", "City updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                        Else
                            UIUtilities.ShowDialog(Me, "Save City", "Can not update duplicate City Code.", ICBO.IC.Dialogmessagetype.Failure)
                        End If
                    Else
                        UIUtilities.ShowDialog(Me, "Save City", "Can not update duplicate City.", ICBO.IC.Dialogmessagetype.Failure)
                    End If

                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If

    End Sub

    Private Function CheckDuplicate(ByVal cCity As ICCity, ByVal IsUpdatee As Boolean) As Boolean
        Dim rslt As Boolean = False
        Dim ICCity As New ICCity
        ICCity.es.Connection.CommandTimeout = 3600
        Dim collCity As New ICCityCollection
        collCity.es.Connection.CommandTimeout = 3600
        Try
            If IsUpdatee = False Then
                collCity.LoadAll()
                collCity.Query.Where(collCity.Query.CityName.Like(cCity.CityName))
            Else
                collCity.Query.Where(collCity.Query.CityName.Like(cCity.CityName) And collCity.Query.CityCode <> cCity.CityCode)
            End If

            If collCity.Query.Load() Then
                rslt = True
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    'Check Duplicate code for display code added in db by Jawwad 
    Private Function CheckDuplicateDisplayCode(ByVal cCity As ICCity, ByVal IsUpdate As Boolean) As Boolean
        Dim rslt As Boolean = False
        Dim ICCity As New ICCity
        ICCity.es.Connection.CommandTimeout = 3600
        Dim collCity As New ICCityCollection
        collCity.es.Connection.CommandTimeout = 3600
        Try
            If IsUpdate = False Then
                collCity.Query.Where(collCity.Query.DisplayCode.Like(cCity.DisplayCode))
            Else
                collCity.Query.Where(collCity.Query.DisplayCode.Like(cCity.DisplayCode) And collCity.Query.CityCode <> cCity.CityCode)
            End If
            If collCity.Query.Load() Then
                rslt = True
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    Protected Sub btnApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproved.Click
        Dim cCity As New IC.ICCity
        cCity.es.Connection.CommandTimeout = 3600
        If Page.IsValid Then
            Try
                cCity.LoadByPrimaryKey(CityCode.ToString())
                If Me.UserInfo.IsSuperUser = False Then
                    If cCity.CreateBy = Me.UserId Then
                        UIUtilities.ShowDialog(Me, "Save City", "City must be approve by the user other than the maker.", ICBO.IC.Dialogmessagetype.Failure)
                        Exit Sub
                    End If
                End If
                If chkApproved.Checked Then
                    ICCityController.ApproveCity(CityCode.ToString(), chkApproved.Checked, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    UIUtilities.ShowDialog(Me, "Approve City", "City approve.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                Else
                    UIUtilities.ShowDialog(Me, "Approve City", "Please checked the Approved Check Box.", ICBO.IC.Dialogmessagetype.Warning)

                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

#End Region

End Class
