﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaveCountry.ascx.vb"
    Inherits="DesktopModules_Country_SaveCountry" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style type="text/css">
    .style1
    {
        width: 25%;
        height: 26px;
    }
    .btn
    {}
</style>
<telerik:RadInputManager ID="RadInputManager1" runat="server">
               <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior2" EmptyMessage="type.."
            Type="Number">
                    </telerik:NumericTextBoxSetting>       
         <%--  <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="true"
            ValidationExpression="^[a-zA-Z ][a-zA-Z\\s ]+$" ErrorMessage="Invalid Name" EmptyMessage="Enter Country Name">
            <TargetControls>
                <telerik:TargetInput ControlID="txtCountryName" />
            </TargetControls>
        </telerik:RegExpTextBoxSetting>     --%>   

         <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior2" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z ][a-zA-Z\\s ]+$" ErrorMessage="Invalid Country Name"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCountryName" />
        </TargetControls>
        </telerik:RegExpTextBoxSetting> 

                   <telerik:NumericTextBoxSetting BehaviorID="NumericBehavior3" EmptyMessage="type.."
            Type="Number">
                    </telerik:NumericTextBoxSetting>       
         <%--  <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior1" Validation-IsRequired="true"
            ValidationExpression="^[a-zA-Z ][a-zA-Z\\s ]+$" ErrorMessage="Invalid Name" EmptyMessage="Enter Country Name">
            <TargetControls>
                <telerik:TargetInput ControlID="txtCountryName" />
            </TargetControls>
        </telerik:RegExpTextBoxSetting>     --%>   

         <telerik:RegExpTextBoxSetting BehaviorID="RagExpBehavior3" Validation-IsRequired="true"
        ValidationExpression="^[a-zA-Z0-9]{1,20}$" ErrorMessage="Invalid Country Code"
        EmptyMessage="">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDisplayCode" />
        </TargetControls>
        </telerik:RegExpTextBoxSetting>

    </telerik:RadInputManager>
<table width="100%">
<tr align="left" valign="top" style="width: 100%">
        <td align="left"  valign="top" colspan="2">
            <asp:Label ID="lblPageHeader" runat="server" Text="Add Country" CssClass="headingblue"></asp:Label>
            &nbsp;
        </td>
        <td align="left" valign="top" colspan="2">           
         </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblDisplayCode" runat="server" Text="Country Code" CssClass="lbl"></asp:Label><asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCountryName" runat="server" Text="Country Name" CssClass="lbl"></asp:Label><asp:Label ID="Label2" runat="server" Text="*"  ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" class="style1">
            <asp:TextBox ID="txtDisplayCode" runat="server" CssClass="txtbox" MaxLength="10"></asp:TextBox>
        </td>
        <td align="left" valign="top" class="style1">
        </td>
        <td align="left" valign="top" class="style1">
            <asp:TextBox ID="txtCountryName" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox>
        </td>
        <td align="left" valign="top" class="style1">
            &nbsp;
        </td>
    </tr>    
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIsActive" runat="server" Text="Is Active" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblIsApproved" runat="server" Text="Is Approved" CssClass="lbl" 
                Visible="False"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CheckBox ID="chkActive" runat="server" Text=" " CssClass="chkBox" 
                Checked="True" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
            <asp:CheckBox ID="chkApproved" runat="server" Text=" " Visible="False" 
                CssClass="txtbox" />
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;</td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnApproved" runat="server" Text="Approved" CssClass="btn" 
                Visible="False" Width="71px" />
        &nbsp;<asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn" 
                Width="70px" />
        &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" Width="75px" 
                CausesValidation="False"  />
            &nbsp;
        </td>
    </tr>
</table>
