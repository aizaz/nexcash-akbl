﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals

Partial Class DesktopModules_Area_SaveArea
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private AreaCode As String
    Private ArrRights As ArrayList
#Region "Page Load"
    Protected Sub DesktopModules_Area_SaveArea_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            AreaCode = Request.QueryString("id").ToString()
            If Page.IsPostBack = False Then
                LoadddlCountry()
                LoadddlProvince()
                LoadddlCity()
                If AreaCode.ToString() = "0" Then
                    Clear()
                    lblIsApproved.Visible = False
                    chkApproved.Visible = False
                    btnApproved.Visible = False
                    lblPageHeader.Text = "Add Area"
                    btnSave.Text = "Save"
                Else
                    lblIsApproved.Visible = True
                    chkApproved.Visible = True
                    btnApproved.Visible = True
                    lblPageHeader.Text = "Edit Area"
                    btnSave.Text = "Update"
                    txtAreaCode.ReadOnly = True
                    Dim ICArea As New ICArea
                    ICArea.es.Connection.CommandTimeout = 3600
                    ICArea.LoadByPrimaryKey(AreaCode)
                    If ICArea.LoadByPrimaryKey(AreaCode) Then
                        txtAreaCode.Text = ICArea.AreaCode.ToString()
                        txtAreaName.Text = ICArea.AreaName.ToString()


                        Dim objICCity As New ICCity
                        Dim objICProvince As New ICProvince
                        Dim objICCountry As New ICCountry

                        objICCity.es.Connection.CommandTimeout = 3600
                        objICProvince.es.Connection.CommandTimeout = 3600
                        objICCountry.es.Connection.CommandTimeout = 3600


                        objICCity = ICArea.UpToICCityByCityCode()
                        objICProvince = objICCity.UpToICProvinceByProvinceCode()
                        objICCountry = objICProvince.UpToICCountryByCountryCode()
                        LoadddlCountry()
                        ddlCountry.SelectedValue = objICCountry.CountryCode.ToString()
                        LoadddlProvince()
                        ddlProvince.SelectedValue = objICProvince.ProvinceCode.ToString()
                        LoadddlCity()
                        ddlCity.SelectedValue = objICCity.CityCode.ToString()
                        chkActive.Checked = ICArea.IsActive
                        chkApproved.Checked = ICArea.IsApproved
                    End If
                End If
                'btnSave.Visible = ArrRights(1)
            End If
            If CheckIsAdminOrSuperUser() = False Then
                ManagePageAccessRights()
            End If
            If AreaCode.ToString <> "0" Then
                Dim icArea As New ICArea
                icArea.es.Connection.CommandTimeout = 3600
                icArea.LoadByPrimaryKey(AreaCode)
                If Me.UserInfo.IsSuperUser = True Then
                    btnApproved.Visible = True
                    chkApproved.Enabled = True
                ElseIf icArea.IsApproved = True Or icArea.CreateBy = Me.UserId Then
                    btnApproved.Visible = False
                    chkApproved.Enabled = False
                End If

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
#End Region
#Region "Button Events"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                Dim ICArea As New ICArea
                ICArea.es.Connection.CommandTimeout = 3600
                ICArea.AreaCode = txtAreaCode.Text.ToString()
                ICArea.AreaName = txtAreaName.Text.ToString()
                ICArea.CityCode = ddlCity.SelectedValue.ToString()
                ICArea.IsActive = chkActive.Checked
                If AreaCode = "0" Then
                    ICArea.CreateBy = Me.UserId
                    ICArea.CreateDate = Date.Now
                    If CheckDuplicate(ICArea) = False Then
                        ICAreaController.AddArea(ICArea, False)
                        ICUtilities.AddAuditTrail("Area " & ICArea.AreaName.ToString() & " Added", "Area", ICArea.AreaCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString(), "")
                        UIUtilities.ShowDialog(Me, "Save Area", "Area added successfully.", ICBO.IC.Dialogmessagetype.Success)
                    Else
                        UIUtilities.ShowDialog(Me, "SaveArea", "Can not add duplicate Area.", ICBO.IC.Dialogmessagetype.Failure)
                    End If
                Else

                    ICAreaController.AddArea(ICArea, True)
                    ICUtilities.AddAuditTrail("Area  " & ICArea.AreaName.ToString() & " Updated", "Area", ICArea.AreaCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString(), "")
                    UIUtilities.ShowDialog(Me, "Save Area", "Area updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                End If
                Clear()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub
    Protected Sub btnApproved_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproved.Click
        Dim ICArea As New ICArea

        ICArea.es.Connection.CommandTimeout = 3600

        If Page.IsValid Then
            Try
                ICArea.LoadByPrimaryKey(AreaCode.ToString())
                ICAreaController.ApproveArea(AreaCode.ToString(), chkApproved.Checked, Me.UserId.ToString())
                If chkApproved.Checked Then
                    ICUtilities.AddAuditTrail("Area : " & ICArea.AreaName.ToString() & " Approved", "Area", ICArea.AreaCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString(), "")
                    UIUtilities.ShowDialog(Me, "Save Area", "Area approved.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())

                Else
                    ICUtilities.AddAuditTrail("Area: " & ICArea.AreaName.ToString() & " Not Approved", "Area", ICArea.AreaCode.ToString(), Me.UserId.ToString(), Me.UserInfo.Username.ToString(), "")
                    UIUtilities.ShowDialog(Me, "Save Area", "Area not approved.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
#End Region
#Region "Drop Down Events"
    Private Sub LoadddlCity()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCity.Items.Clear()
            ddlCity.Items.Add(lit)
            ddlCity.AppendDataBoundItems = True
            'ddlCity.DataSource = ICCityController.GetCity(ddlProvince.SelectedValue.ToString())
            ddlCity.DataTextField = "CityName"
            ddlCity.DataValueField = "CityCode"
            ddlCity.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlProvince()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlProvince.Items.Clear()
            ddlProvince.Items.Add(lit)
            ddlProvince.AppendDataBoundItems = True
            'ddlProvince.DataSource = ICProvinceController.GetProvince(ddlCountry.SelectedValue.ToString())
            ddlProvince.DataTextField = "ProvinceName"
            ddlProvince.DataValueField = "ProvinceCode"
            ddlProvince.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub LoadddlCountry()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCountry.Items.Clear()
            ddlCountry.Items.Add(lit)
            ddlCountry.AppendDataBoundItems = True
            'ddlCountry.DataSource = ICCountryController.GetCountry()
            ddlCountry.DataTextField = "CountryName"
            ddlCountry.DataValueField = "CountryCode"
            ddlCountry.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        LoadddlProvince()
        LoadddlCity()
    End Sub

    Protected Sub ddlProvince_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProvince.SelectedIndexChanged
        LoadddlCity()
    End Sub
#End Region
#Region "Other Functions/Routines"
    Private Sub ManagePageAccessRights()
        Try
            ArrRights = ICBankRoleRightsController.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Location Management")
            If ArrRights.Count > 0 Then
                If AreaCode.ToString() = "0" Then
                    btnSave.Visible = ArrRights(0)
                Else
                    btnSave.Visible = ArrRights(1)
                    chkApproved.Enabled = ArrRights(3)
                    btnApproved.Visible = ArrRights(3)
                End If
            Else
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckIsAdminOrSuperUser() As Boolean
        Dim userCtrl As New RoleController
        Dim Result As Boolean = False
        Dim str As String()
        Dim iUserRole As New UserRoleInfo
        Dim i As Integer
        Try
            Result = False
            str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
            For i = 0 To str.Length - 1
                If Trim(str(i).ToString()) = "FRC Administrator" Then
                    Result = True
                End If
                If Trim(str(i).ToString()) = "Administrators" Then
                    Result = True
                End If
            Next
            If Result = False Then
                If Me.UserInfo.IsSuperUser = True Then
                    Result = True
                End If
            End If
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            Return Result
        End Try
    End Function
    Private Sub Clear()
        txtAreaCode.Text = ""
        txtAreaName.Text = ""
        ddlCountry.ClearSelection()
        ddlProvince.ClearSelection()
        ddlCity.ClearSelection()
        chkActive.Checked = True
    End Sub

    Private Function CheckDuplicate(ByVal cArea As ICArea) As Boolean
        Try
            Dim rslt As Boolean = False
            Dim ICArea As New ICArea
            Dim collArea As New ICAreaCollection

            ICArea.es.Connection.CommandTimeout = 3600
            collArea.es.Connection.CommandTimeout = 3600


            If ICArea.LoadByPrimaryKey(cArea.AreaCode) Then
                rslt = True
            End If
            collArea.LoadAll()
            collArea.Query.Where(collArea.Query.AreaName.Like(cArea.AreaName))
            If collArea.Query.Load() Then
                rslt = True
            End If
            Return rslt
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
#End Region
    '#dnn_ctr_Login_tdPassword{display:none;}
End Class
