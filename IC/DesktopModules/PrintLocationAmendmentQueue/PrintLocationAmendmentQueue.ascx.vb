﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports Telerik.Web.UI
Partial Class DesktopModules_PrintLocationAmendmentQueue_PrintLocationAmendmentQueue
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private ArrICAssignUserRolsID As New ArrayList
    Private ArrICAssignedOfficeID As New ArrayList
    Private ArrICAssignedStatus As New ArrayList
    Private ArrICAssignedPaymentModes As New ArrayList
    Private ArrICAssignedStatusForApproval As New ArrayList
    Private ArrICAssignedCompanies As New ArrayList
    Private ArrICAssignedANos As New ArrayList
    Private ArrICAssignedBranchCodes As New ArrayList
    Private ArrICAssignedCurrency As New ArrayList
    Private ArrICAssignedPayNatures As New ArrayList
    Private htRights As Hashtable
    Protected Sub DesktopModules_PrintLocationAmendmentQueue_PrintLocationAmendmentQueue_Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            SetArraListRoleID()

            If Page.IsPostBack = False Then
                btnAmendPrintLocations.Attributes.Item("onclick") = "if (Page_ClientValidate('PrintLocation')) { this.value=" & Chr(34) & "Processing..." & Chr(34) & ";this.disabled=true;" & Me.Page.ClientScript.GetPostBackEventReference(Me.btnAmendPrintLocations, Nothing).ToString() & " } "

                DesignDts()
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                LoadddlCompany()

                LoadddlProductTypeByCompanyCode()
                'pnlPrintLocations.Visible = False
                tblPrintLocations.Style.Add("display", "none")
                gvAmendPrintLocation.Visible = False
                FillDesignedDtByAPNatureByAssignedRole()
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub DesignDts()
        Dim dtAccountNumber As New DataTable
        Dim dtPaymentNature As New DataTable
        Dim dtOfficeID As New DataTable
        dtAccountNumber.Columns.Add(New DataColumn("AccountNumber", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("BranchCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("Currency", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("PaymentNatureCode", GetType(System.String)))
        dtAccountNumber.Columns.Add(New DataColumn("OfficeID", GetType(System.String)))
        ViewState("AccountNo") = dtAccountNumber
    End Sub
    Private Sub FillDesignedDtByAPNatureByAssignedRole()
        Dim dtAccountNumber As DataTable
        Dim dtAssignedAPNatureOfficeID As New DataTable

        dtAccountNumber = New DataTable
        dtAccountNumber = ViewState("AccountNo")
        dtAccountNumber.Rows.Clear()
        dtAssignedAPNatureOfficeID.Rows.Clear()
        dtAssignedAPNatureOfficeID = ICInstructionController.GetAccountPaymentNatureTaggedWithUserSecond(Me.UserId.ToString, ArrICAssignUserRolsID)
        ViewState("AccountNumber") = dtAssignedAPNatureOfficeID
        'ViewState("PaymentNature") = dtPaymentNature
        'ViewState("OfficeID") = dtOfficeCode
    End Sub
    
    Private Sub LoadddlProductTypeByCompanyCode()
        Try
            Dim lit As New ListItem
            Dim objICAPNature As New ICAccountsPaymentNature
            objICAPNature.es.Connection.CommandTimeout = 3600
            ddlProductType.Items.Clear()
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlProductType.Items.Add(lit)
            ddlProductType.AppendDataBoundItems = True
            ddlProductType.DataSource = ICAccountPaymentNatureProductTypeController.GetAllProductTypesByAccountAndPaymentNatureForPrintLocationAmendmentQueue(ddlCompany.SelectedValue.ToString)
            ddlProductType.DataTextField = "ProductTypeName"
            ddlProductType.DataValueField = "ProductTypeCode"
            ddlProductType.DataBind()


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Print Location Amendment Queue")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetArraListRoleID()
        Dim dt As New DataTable
        ArrICAssignUserRolsID.Clear()
        dt = ICUserRolesController.GetAssignedUserRolesinDTByUserID(Me.UserId.ToString)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("RoleID").ToString
                If ICUserRolesController.IsRightIsAssignedToRole(RoleID, "Print Location Amendment Queue") = True Then
                    ArrICAssignUserRolsID.Add(RoleID.ToString)
                End If
            Next
        End If
    End Sub
    Private Sub SetArraListAssignedOfficeIDs()
        Dim dt As New DataTable
        Dim objICUser As New ICUser
        objICUser.LoadByPrimaryKey(Me.UserId)
        ArrICAssignedOfficeID.Clear()
        dt = ICUserRolesPaymentNatureController.GetAllTaggedLocationsWithUserByUSerIDAndRoleID(Me.UserId.ToString, ArrICAssignUserRolsID)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim RoleID As String = Nothing
                RoleID = dr("OfficeID").ToString
                If Not ArrICAssignedOfficeID.Contains(RoleID.ToString) Then
                    ArrICAssignedOfficeID.Add(RoleID.ToString)
                End If
            Next
        End If
        If Not ArrICAssignedOfficeID.Contains(objICUser.OfficeCode.ToString) Then
            ArrICAssignedOfficeID.Add(objICUser.OfficeCode)
        End If
    End Sub
    Private Sub SetArraListAssignedStatus()
        Dim dt As New DataTable
        ArrICAssignedStatus.Clear()
        ArrICAssignedStatus.Add(23)
        ArrICAssignedStatus.Add(12)
        ArrICAssignedStatus.Add(16)
        ArrICAssignedStatus.Add(18)
    End Sub
    Private Sub SetArraListAssignedStatusForApproval()
        Dim dt As New DataTable
        ArrICAssignedStatusForApproval.Clear()

    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim objICUser As New ICUser
            Dim dt As New DataTable
            Dim lit As New ListItem
            ArrICAssignedCompanies.Clear()
            lit.Value = Nothing
            lit.Text = Nothing
            objICUser.es.Connection.CommandTimeout = 3600
            objICUser.LoadByPrimaryKey(Me.UserId)
            dt = ICUserRolesPaymentNatureController.GetAllTaggedCompanysByUserID(Me.UserId.ToString, "", ArrICAssignUserRolsID)
            ddlCompany.Enabled = True
            If dt.Rows.Count = 1 Then
                For Each dr As DataRow In dt.Rows
                    lit.Value = dr("CompanyCode")
                    lit.Text = dr("CompanyName")
                    ddlCompany.Items.Clear()
                    ddlCompany.Items.Add(lit)
                    lit.Selected = True
                    ddlCompany.Enabled = False
                    ArrICAssignedCompanies.Add(lit.Value)
                Next
            ElseIf dt.Rows.Count > 1 Then

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
                ddlCompany.DataSource = dt
                ddlCompany.DataTextField = "CompanyName"
                ddlCompany.DataValueField = "CompanyCode"
                ddlCompany.DataBind()
                ddlCompany.Enabled = True
                For Each dr As DataRow In dt.Rows
                    ArrICAssignedCompanies.Add(lit.Value)
                Next
            Else

                lit.Value = "0"
                lit.Text = "-- All --"
                ddlCompany.Items.Clear()
                ddlCompany.Items.Add(lit)
                ddlCompany.AppendDataBoundItems = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    'Private Sub LoadddlAccountPaymentNatureByUserID()
    '    Try
    '        Dim lit As New ListItem
    '        lit.Value = "0"
    '        lit.Text = "-- All --"
    '        ddlAcccountPaymentNature.Items.Clear()
    '        ddlAcccountPaymentNature.Items.Add(lit)
    '        lit.Selected = True
    '        ddlAcccountPaymentNature.AppendDataBoundItems = True
    '        ddlAcccountPaymentNature.DataSource = ICInstructionController.GetAccountPaymentNatureTaggedWithUserForDropDown(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
    '        ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
    '        ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
    '        ddlAcccountPaymentNature.DataBind()
    '        'Dim lit As New ListItem
    '        'Dim dt As New DataTable
    '        'ArrICAssignedANos.Clear()
    '        'ArrICAssignedBranchCodes.Clear()
    '        'ArrICAssignedCurrency.Clear()
    '        'ArrICAssignedPayNatures.Clear()
    '        'lit.Value = "0"
    '        'lit.Text = "-- All --"
    '        'ddlAcccountPaymentNature.Items.Clear()
    '        'ddlAcccountPaymentNature.Items.Add(lit)
    '        'lit.Selected = True
    '        'ddlAcccountPaymentNature.AppendDataBoundItems = True
    '        'dt = ICInstructionController.GetAccountPaymentNatureTaggedWithUser(Me.UserId.ToString, ddlCompany.SelectedValue.ToString, ArrICAssignUserRolsID)
    '        'ddlAcccountPaymentNature.DataSource = dt
    '        'ddlAcccountPaymentNature.DataTextField = "AccountAndPaymentNature"
    '        'ddlAcccountPaymentNature.DataValueField = "AccountPaymentNature"
    '        'ddlAcccountPaymentNature.DataBind()
    '        'For Each dr As DataRow In dt.Rows
    '        '    ArrICAssignedANos.Add(dr("AccountPaymentNature").ToString.Split("-")(0))
    '        '    ArrICAssignedBranchCodes.Add(dr("AccountPaymentNature").ToString.Split("-")(1))
    '        '    ArrICAssignedCurrency.Add(dr("AccountPaymentNature").ToString.Split("-")(2))
    '        '    ArrICAssignedPayNatures.Add(dr("AccountPaymentNature").ToString.Split("-")(3))
    '        'Next
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try

    'End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Page.Validate()

        If Page.IsValid Then
            Try
                If (txtInstructionNo.Text.ToString = "" Or txtInstructionNo.Text.ToString = "Enter Instruction No.") And (txtInstrumentNo.Text.ToString = "" Or txtInstrumentNo.Text.ToString = "Enter Instrument No.") Then
                    UIUtilities.ShowDialog(Me, "Error", "Please enter instrument no or instruction no.", ICBO.IC.Dialogmessagetype.Failure)
                    Exit Sub
                Else
                    LoadgvAmendPrintLocation(True)
                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
       LoadddlProductTypeByCompanyCode
        HideGvAndPrintLocationPannel()
        txtInstructionNo.Text = ""
        txtInstrumentNo.Text = ""
    End Sub

    'Protected Sub ddlAcccountPaymentNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAcccountPaymentNature.SelectedIndexChanged
    '    Try
    '        If ddlAcccountPaymentNature.SelectedValue.ToString = "0" Then
    '            SetArraListRoleID()
    '            LoadddlProductTypeByAccountPaymentNature()
    '            FillDesignedDtByAPNatureByAssignedRole()
    '            HideGvAndPrintLocationPannel()
    '            txtInstructionNo.Text = ""
    '            txtInstrumentNo.Text = ""
    '        Else
    '            LoadddlProductTypeByAccountPaymentNature()
    '            SetViewStateDtOnAPNatureIndexChnage()
    '        HideGvAndPrintLocationPannel
    '            txtInstructionNo.Text = ""
    '            txtInstrumentNo.Text = ""


    '        End If
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    '    'LoadddlProductTypeByAccountPaymentNature()
    '    'pnlPrintLocations.Visible = False
    '    'lblAmendPrintLocation.Visible = False
    '    'lblNRFAmendLocation.Visible = False
    '    'txtInstructionNo.Text = ""
    '    'txtInstrumentNo.Text = ""
    'End Sub
    'Private Sub SetViewStateDtOnAPNatureIndexChnage()
    '    Dim dtAccountNumber As New DataTable
    '    Dim dtPaymentNature As New DataTable
    '    Dim AccountNumber, BranchCode, Currency, PaymentNatureCode As String
    '    Dim ArrayListOfficeID As New ArrayList
    '    Dim drAccountNo As DataRow
    '    AccountNumber = Nothing
    '    BranchCode = Nothing
    '    Currency = Nothing
    '    PaymentNatureCode = Nothing
    '    dtAccountNumber = ViewState("AccountNumber")
    '    AccountNumber = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(0)
    '    BranchCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(1)
    '    Currency = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(2)
    '    PaymentNatureCode = ddlAcccountPaymentNature.SelectedValue.ToString.Split("-")(3)
    '    For Each dr As DataRow In dtAccountNumber.Rows
    '        If dr("AccountNumber").ToString.Split("-")(0) = AccountNumber And dr("AccountNumber").ToString.Split("-")(1) = BranchCode And dr("AccountNumber").ToString.Split("-")(2) = Currency And dr("PaymentNature").ToString = PaymentNatureCode Then
    '            ArrayListOfficeID.Add(dr("OfficeID").ToString)
    '        End If
    '    Next
    '    dtAccountNumber.Rows.Clear()
    '    If ArrayListOfficeID.Count > 0 Then
    '        For Each OfficeID In ArrayListOfficeID
    '            drAccountNo = dtAccountNumber.NewRow()
    '            drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
    '            drAccountNo("PaymentNature") = PaymentNatureCode
    '            drAccountNo("OfficeID") = OfficeID
    '            dtAccountNumber.Rows.Add(drAccountNo)
    '        Next
    '    Else
    '        drAccountNo = dtAccountNumber.NewRow()
    '        drAccountNo("AccountNumber") = AccountNumber & "-" & BranchCode & "-" & Currency
    '        drAccountNo("PaymentNature") = PaymentNatureCode
    '        drAccountNo("OfficeID") = "0"
    '        dtAccountNumber.Rows.Add(drAccountNo)
    '    End If

    '    ViewState("AccountNumber") = dtAccountNumber



    'End Sub
    Private Sub SetArraListAssignedPaymentModes()
        ArrICAssignedPaymentModes.Clear()
        If CBool(htRights("Cheque Instructions")) = True Then
            ArrICAssignedPaymentModes.Add("Cheque")
        End If
        If CBool(htRights("DD Instructions")) = True Then
            ArrICAssignedPaymentModes.Add("DD")
        End If
        If CBool(htRights("PO Instructions")) = True Then
            ArrICAssignedPaymentModes.Add("PO")
        End If
    End Sub
    Private Sub LoadgvAmendPrintLocation(ByVal DoDataBind As Boolean)
        Try
            Dim DateType, AccountNumber, BranchCode, Currency, PaymentNatureCode, CompanyCode, ProductTypeCode, InstrumentNo, InstructionNo, BatchCode, ReferenceNo As String
            Dim FromDate, ToDate As String
            Dim Amount As Double = 0
        
            DateType = ""
            AccountNumber = ""
            BranchCode = ""
            Currency = ""
            Currency = ""
            PaymentNatureCode = ""
            CompanyCode = ""
            ProductTypeCode = ""
            InstructionNo = ""
            InstrumentNo = ""
            BatchCode = ""
            ReferenceNo = ""
            FromDate = Nothing
            ToDate = Nothing
            If ddlCompany.SelectedValue.ToString <> "0" Then
                CompanyCode = ddlCompany.SelectedValue.ToString
            End If
            If ddlProductType.SelectedValue.ToString <> "0" Then
                ProductTypeCode = ddlProductType.SelectedValue.ToString
            End If
            If txtInstrumentNo.Text.ToString <> "" And txtInstrumentNo.Text.ToString <> "Enter Instrument No." Then
                InstrumentNo = txtInstrumentNo.Text.ToString
            End If
            If txtInstructionNo.Text.ToString <> "" And txtInstructionNo.Text.ToString <> "Enter Instruction No." Then
                InstructionNo = txtInstructionNo.Text.ToString
            End If

            SetArraListAssignedStatus()
            SetArraListAssignedOfficeIDs()
            SetArraListAssignedPaymentModes()
            ICInstructionController.GetInstructionsForAmendmentForRadGrid(AccountNumber, BranchCode, Currency, PaymentNatureCode, ProductTypeCode, InstructionNo, InstrumentNo, Me.gvAmendPrintLocation.CurrentPageIndex + 1, Me.gvAmendPrintLocation.PageSize, Me.gvAmendPrintLocation, DoDataBind, ArrICAssignedStatus, ArrICAssignedOfficeID, Me.UserId.ToString, ArrICAssignedPaymentModes, CompanyCode)
            If gvAmendPrintLocation.Items.Count > 0 Then
                lblAmendPrintLocation.Visible = True
                gvAmendPrintLocation.Visible = True
                btnAmendPrintLocations.Visible = CBool(htRights("Amend Print Location"))
                lblNRFAmendLocation.Visible = False
                SetPannelPrintLocationByInstructionID(gvAmendPrintLocation.Items(0).GetDataKeyValue("InstructionID").ToString)
                'pnlPrintLocations.Visible = True
                tblPrintLocations.Style.Remove("display")
            Else
                lblAmendPrintLocation.Visible = True
                ddlPrintLocations.ClearSelection()
                SetPannelPrintLocationByInstructionID("")
                LoadddlBankOrGroupByLocationType(ddlLocationType.SelectedValue.ToString, 0)
                LoadddlPrintLocationsBankOrGroupByLocationType(ddlBankOrClient.SelectedValue.ToString)
                gvAmendPrintLocation.Visible = False
                btnAmendPrintLocations.Visible = False
                lblNRFAmendLocation.Visible = True
                'pnlPrintLocations.Visible = False
                tblPrintLocations.Style.Add("display", "none")
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetPannelPrintLocationByInstructionID(ByVal InstructionID As String)
        Dim objICInstruction As New ICInstruction
        objICInstruction.es.Connection.CommandTimeout = 3600

        If InstructionID.ToString <> "" Then
            If objICInstruction.LoadByPrimaryKey(InstructionID.ToString) Then
                If objICInstruction.PaymentMode.ToLower.ToString = "cheque" Then
                    ddlLocationType.ClearSelection()
                    ddlLocationType.Enabled = True
                    LoadddlBankOrGroupByLocationType(ddlLocationType.SelectedValue.ToString, objICInstruction.GroupCode)
                    LoadddlPrintLocationsBankOrGroupByLocationType(ddlBankOrClient.SelectedValue.ToString)
                ElseIf objICInstruction.PaymentMode.ToLower.ToString = "dd" Or objICInstruction.PaymentMode.ToLower.ToString = "po" Then
                    ddlLocationType.SelectedValue = "Bank Locations"
                    ddlLocationType.Enabled = False
                    LoadddlBankOrGroupByLocationType(ddlLocationType.SelectedValue.ToString, objICInstruction.GroupCode)
                    LoadddlPrintLocationsBankOrGroupByLocationType(ddlBankOrClient.SelectedValue.ToString)
                End If
            End If
        End If
    End Sub
    Private Sub LoadddlBankOrGroupByLocationType(ByVal LocationType As String, ByVal GroupCode As String)
        Dim lit As New ListItem
        lit.Value = "0"
        lit.Text = "-- All --"
        ddlBankOrClient.Items.Clear()
        ddlBankOrClient.Items.Add(lit)
        ddlBankOrClient.AppendDataBoundItems = True
        If ddlLocationType.SelectedValue.ToString = "Bank Locations" Then
            ddlBankOrClient.DataSource = ICBankController.GetAllApprovedAndActiveBanks
            ddlBankOrClient.DataTextField = "BankName"
            ddlBankOrClient.DataValueField = "BankCode"
            ddlBankOrClient.DataBind()
        ElseIf ddlLocationType.SelectedValue.ToString = "Client Locations" Then
            ddlBankOrClient.DataSource = ICGroupController.GetAllActiveAndApproveGroupsByGroupCode(GroupCode)
            ddlBankOrClient.DataTextField = "GroupName"
            ddlBankOrClient.DataValueField = "GroupCode"
            ddlBankOrClient.DataBind()
        End If
    End Sub
    Private Sub LoadddlPrintLocationsBankOrGroupByLocationType(ByVal BankORGroupCode As String)
        Dim lit As New ListItem
        lit.Value = "0"
        lit.Text = "-- All --"
        ddlPrintLocations.Items.Clear()
        ddlPrintLocations.Items.Add(lit)
        ddlPrintLocations.AppendDataBoundItems = True
        If ddlLocationType.SelectedValue.ToString = "Bank Locations" Then
            ddlPrintLocations.DataSource = ICOfficeController.GetBranchByBankCodeActiveAndApprovePrintitngLocationToDisplayBankCode(BankORGroupCode, ddlProductType.SelectedValue.ToString, gvAmendPrintLocation.Items(0).GetDataKeyValue("ClientAccountNo").ToString, gvAmendPrintLocation.Items(0).GetDataKeyValue("ClientAccountBranchCode").ToString, gvAmendPrintLocation.Items(0).GetDataKeyValue("ClientAccountCurrency").ToString, gvAmendPrintLocation.Items(0).GetDataKeyValue("PaymentNatureCode").ToString)
            ddlPrintLocations.DataTextField = "OfficeName"
            ddlPrintLocations.DataValueField = "OfficeID"
            ddlPrintLocations.DataBind()
        ElseIf ddlLocationType.SelectedValue.ToString = "Client Locations" Then
            ddlPrintLocations.DataSource = ICOfficeController.GetOfficeByGroupCodeActiveAndApprovePrintingLocatoionWithCodeAndName(BankORGroupCode, ddlProductType.SelectedValue.ToString, gvAmendPrintLocation.Items(0).GetDataKeyValue("ClientAccountNo").ToString, gvAmendPrintLocation.Items(0).GetDataKeyValue("ClientAccountBranchCode").ToString, gvAmendPrintLocation.Items(0).GetDataKeyValue("ClientAccountCurrency").ToString, gvAmendPrintLocation.Items(0).GetDataKeyValue("PaymentNatureCode").ToString)
            ddlPrintLocations.DataTextField = "OfficeName"
            ddlPrintLocations.DataValueField = "OfficeID"
            ddlPrintLocations.DataBind()
        End If
    End Sub
    'Protected Sub ddlLocationType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocationType.SelectedIndexChanged
    '    Try
    '        LoadddlBankOrGroupByLocationType(ddlBankOrClient)
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Protected Sub ddlBankOrClient_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBankOrClient.SelectedIndexChanged
        Try
            LoadddlPrintLocationsBankOrGroupByLocationType(ddlBankOrClient.SelectedValue.ToString)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlLocationType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLocationType.SelectedIndexChanged
        Try
            If ddlLocationType.SelectedValue.ToString = "Bank Locations" Then
                lblSelectBank.Text = "Select Bank"
                rfvddlBank.Text = "Please select Bank"
                LoadddlBankOrGroupByLocationType(ddlLocationType.SelectedValue.ToString, "")
                LoadddlPrintLocationsBankOrGroupByLocationType(ddlBankOrClient.SelectedValue.ToString)
            ElseIf ddlLocationType.SelectedValue.ToString = "Client Locations" Then
                lblSelectBank.Text = "Select Group"
                rfvddlBank.Text = "Please select Group"
                LoadddlBankOrGroupByLocationType(ddlLocationType.SelectedValue.ToString, gvAmendPrintLocation.Items(0).GetDataKeyValue("GroupCode").ToString)
                LoadddlPrintLocationsBankOrGroupByLocationType(ddlBankOrClient.SelectedValue.ToString)
            Else
                lblSelectBank.Text = "Select Bank"
                rfvddlBank.Text = "Please select Bank"
                LoadddlBankOrGroupByLocationType(ddlLocationType.SelectedValue.ToString, "")
                LoadddlPrintLocationsBankOrGroupByLocationType(ddlBankOrClient.SelectedValue.ToString)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub btnAmendPrintLocations_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAmendPrintLocations.Click
        Page.Validate()

        If Page.IsValid Then
            Try


                Dim objICInstruction As ICInstruction
                Dim PassToCancelCount As Integer = 0
                Dim FailToCancelCount As Integer = 0
                Dim StrAuditTrailAction As String
                Dim objICUser As New ICUser
                Dim objICOffice As New ICOffice
                Dim objICOfficeFrom As New ICOffice
                objICUser.es.Connection.CommandTimeout = 3600
                objICOffice.es.Connection.CommandTimeout = 3600

                If gvAmendPrintLocation.Items.Count > 0 Then
                    For Each gvAmendPrintLocationRow As GridDataItem In gvAmendPrintLocation.Items
                        objICInstruction = New ICInstruction
                        objICInstruction.es.Connection.CommandTimeout = 3600
                        objICInstruction.LoadByPrimaryKey(gvAmendPrintLocationRow.GetDataKeyValue("InstructionID").ToString)
                        Try
                            objICInstruction.PreviousPrintLocationCode = objICInstruction.PrintLocationCode
                            objICInstruction.PreviousPrintLocationName = objICInstruction.PrintLocationName
                            objICOfficeFrom.LoadByPrimaryKey(objICInstruction.PrintLocationCode)
                            objICInstruction.PrintLocationCode = Nothing
                            objICInstruction.PrintLocationName = Nothing
                            objICOffice.LoadByPrimaryKey(ddlPrintLocations.SelectedValue.ToString)
                            objICInstruction.AmendedPrintLocationCode = ddlPrintLocations.SelectedValue.ToString
                            objICInstruction.AmendedPrintLocationName = objICOffice.OfficeName
                            objICInstruction.PrintLocationAmendedBy = Me.UserId
                            objICInstruction.PrintLocationAmendmentDate = Date.Now
                            objICInstruction.IsPrintLocationAmended = True


                            ''Check To and Fro Print Location

                            objICUser.LoadByPrimaryKey(Me.UserId)
                            If Me.UserInfo.IsSuperUser = False Then
                                If objICUser.UserType = "Client User" Then

                                    If Not objICOfficeFrom.OffceType = "Company Office" Or Not objICOffice.OffceType = "Company Office" Then
                                        UIUtilities.ShowDialog(Me, "Error", "You are not authrized to amend these locations.", ICBO.IC.Dialogmessagetype.Failure)
                                        Exit Sub
                                    End If
                                End If
                            End If




                            StrAuditTrailAction = Nothing
                            StrAuditTrailAction += "Print location from [ " & objICOfficeFrom.OfficeCode & " ][ " & objICOfficeFrom.OfficeName & " ] to [ " & objICOffice.OfficeCode & " ][ " & objICOffice.OfficeName & " ] for Instruction with ID [ " & objICInstruction.InstructionID & " ] updated."
                            StrAuditTrailAction += "Action was taken by user [ " & Me.UserInfo.UserID & " ] [ " & Me.UserInfo.Username & " ]"
                            ICInstructionController.UpDateInstruction(objICInstruction, Me.UserId, Me.UserInfo.Username, StrAuditTrailAction, "UPDATE")
                            PassToCancelCount = PassToCancelCount + 1
                        Catch ex As Exception
                            FailToCancelCount = FailToCancelCount + 1
                        End Try
                    Next
                Else
                    LoadddlCompany()
                    LoadddlProductTypeByCompanyCode()
                    txtInstructionNo.Text = ""
                    txtInstructionNo.Text = ""
                    HideGvAndPrintLocationPannel()
                    UIUtilities.ShowDialog(Me, "Amendment Print Location Queue", "Please select at least one instruction.", ICBO.IC.Dialogmessagetype.Success)
                End If
                If FailToCancelCount = 0 And PassToCancelCount <> 0 Then
                    LoadddlCompany()
                    LoadddlProductTypeByCompanyCode()
                    txtInstructionNo.Text = ""
                    txtInstructionNo.Text = ""
                    HideGvAndPrintLocationPannel()
                    UIUtilities.ShowDialog(Me, "Print Location Amendment Queue", "[ " & PassToCancelCount & " ] Instruction(s) print location amended successfully", ICBO.IC.Dialogmessagetype.Success)

                ElseIf FailToCancelCount <> 0 And PassToCancelCount = 0 Then
                    LoadddlCompany()
                    LoadddlProductTypeByCompanyCode()
                    txtInstructionNo.Text = ""
                    txtInstructionNo.Text = ""
                    HideGvAndPrintLocationPannel()
                    UIUtilities.ShowDialog(Me, "Amendment Print Location Queue", "[ " & FailToCancelCount & " ] Instruction(s) print location not amended cancelled successfully", ICBO.IC.Dialogmessagetype.Failure)

                ElseIf FailToCancelCount <> 0 And PassToCancelCount <> 0 Then
                    LoadddlCompany()
                    LoadddlProductTypeByCompanyCode()
                    txtInstructionNo.Text = ""
                    txtInstructionNo.Text = ""
                    HideGvAndPrintLocationPannel()
                    UIUtilities.ShowDialog(Me, "Amendment Print Location Queue", "[ " & PassToCancelCount & " ] Instruction(s) print location amended successfully <br/ > [ " & FailToCancelCount & " ] Instruction(s) print location not amended successfully", ICBO.IC.Dialogmessagetype.Failure)

                End If
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub
    Private Sub HideGvAndPrintLocationPannel()
        'pnlPrintLocations.Visible = False
        tblPrintLocations.Style.Add("display", "none")
        gvAmendPrintLocation.DataSource = Nothing
        gvAmendPrintLocation.Visible = False
        lblAmendPrintLocation.Visible = False
        lblNRFAmendLocation.Visible = False
    End Sub
    'Private Function CheckgvClientRoleCheckedForProcessAll() As Boolean
    '    Try
    '        Dim rowGVICClientRoles As GridDataItem
    '        Dim chkSelect As CheckBox
    '        Dim Result As Boolean = False
    '        For Each rowGVICClientRoles In gvPenidngApprovalChangePrintLocation.Items
    '            chkSelect = New CheckBox
    '            chkSelect = DirectCast(rowGVICClientRoles.Cells(0).FindControl("chkSelect"), CheckBox)
    '            If chkSelect.Checked = True Then
    '                Result = True
    '                Exit For
    '            End If
    '        Next
    '        Return Result
    '    Catch ex As Exception
    '        ProcessModuleLoadException(Me, ex, False)
    '        UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
    '    End Try
    'End Function

    Protected Sub ddlProductType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductType.SelectedIndexChanged
        HideGvAndPrintLocationPannel()
        txtInstructionNo.Text = ""
        txtInstrumentNo.Text = ""
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect(NavigateURL(41))
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvAmendPrintLocation_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAmendPrintLocation.ItemDataBound
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Or e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)

            If item.GetDataKeyValue("IsAmendmentComplete") = True Then

                gvAmendPrintLocation.AlternatingItemStyle.CssClass = "GridItemComplete"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItemComplete"


            Else
                gvAmendPrintLocation.AlternatingItemStyle.CssClass = "GridItem"
                'gvAmendment.ItemStyle.BackColor = Color.Red
                e.Item.CssClass = "GridItem"


            End If

        End If
    End Sub
End Class

