﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PrintLocationAmendmentQueue.ascx.vb"
    Inherits="DesktopModules_PrintLocationAmendmentQueue_PrintLocationAmendmentQueue" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>



<script type="text/javascript" >


    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;

        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }


</script>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:TextBoxSetting BehaviorID="reTnxCount" Validation-IsRequired="false" EmptyMessage="Transaction Count"
        ErrorMessage="Transaction Count">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTnxCount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reTotalAmount" Validation-IsRequired="false"
        EmptyMessage="Total Amount" ErrorMessage="Total Amount">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTotalAmount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reCurrency" Validation-IsRequired="false" EmptyMessage="Currency"
        ErrorMessage="Currency">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCurrency" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rePaymentMode" Validation-IsRequired="false"
        EmptyMessage="Payment Mode" ErrorMessage="Payment Mode">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPaymentMode" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reAvailableBalance" Validation-IsRequired="false"
        EmptyMessage="Available Balance" ErrorMessage="Available Balance">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAvailableBalance" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reInstructionNo" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Enter Instruction No." ValidationExpression="[0-9]{1,8}">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstructionNo"  />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reInstrumentNo" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Enter Instrument No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstrumentNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reReferenceNo" Validation-IsRequired="false"
        EmptyMessage="Enter Reference No." ErrorMessage="Enter Reference No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceNo" />
        </TargetControls>  
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reRemarks" Validation-IsRequired="true" EmptyMessage="Enter Remarks"
        ErrorMessage="Enter Remarks">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRemarks" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%" colspan="4">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Print Location Amendment Queue"
                            CssClass="headingblue"></asp:Label>
                        <br />
                        Note:&nbsp; Fields marked as
                        <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        &nbsp;are required.
                    </td>
                </tr>
                <%--<tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%" colspan="4">
                        <table align="left" valign="top" style="width: 100%">
                            <tr align="left" valign="top" style="width: 100%">
                                <td align="left" valign="top" style="width: 50%">
                                    <table align="left" valign="top" style="width: 100%">
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 100%" colspan="4">
                                                &nbsp;</td>
                                        </tr>
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 25%">
                                                &nbsp;</td>
                                            <td align="left" valign="top" style="width: 25%">
                                                <telerik:RadDatePicker ID="raddpCreationFromDate" runat="server" CssClass="txtbox">
                                                    <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                    </Calendar>
                                                    <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                        LabelWidth="40%" type="text" value="">
                                                    </DateInput>
                                                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                </telerik:RadDatePicker>
                                            </td>
                                            <td align="left" valign="top" style="width: 25%">
                                                <asp:Label ID="lblCreationDateTo" runat="server" Text="Date To" CssClass="lbl"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="width: 25%">
                                                <telerik:RadDatePicker ID="raddpCreationToDate" runat="server" CssClass="txtbox">
                                                    <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                    </Calendar>
                                                    <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                        LabelWidth="40%" type="text" value="">
                                                    </DateInput>
                                                    <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 20%" >
                                                &nbsp;</td>
                                            <td align="left" valign="top" style="width: 80%" colspan="3">
                                                &nbsp;</td>
                                        </tr>
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 20%" >
                                                &nbsp;</td>
                                            <td align="left" valign="top" style="width: 80%" colspan="3">
                                                &nbsp;</td>
                                        </tr>
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 20%" >
                                                &nbsp;</td>
                                            <td align="left" valign="top" style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="ddlBatch" runat="server" CssClass="dropdown">
                                                    <asp:ListItem Value="0">All</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 20%" >
                                                &nbsp;</td>
                                            <td align="left" valign="top" style="width: 80%" colspan="3">
                                                &nbsp;</td>
                                        </tr>
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 20%" >
                                                &nbsp;</td>
                                            <td align="left" valign="top" style="width: 80%" colspan="3">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="txtbox" Height="69px" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr align="center" valign="top" style="width: 100%">
                                            <td align="center" valign="top" style="width: 100%" colspan="4">
                                                &nbsp;</td>
                                        </tr>
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 100%" colspan="4">
                                                &nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="left" valign="top" style="width: 50%">
                                    <table align="left" valign="top" style="width: 100%">
                                        <tr align="center"  valign="top" style="width: 100%">
                                            <td align="center"  valign="top" style="width: 100%" colspan="2">
                                    <asp:Label ID="lblTransactionSummary" runat="server" Text="Transaction Summary" CssClass="headingblue"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 50%">
                                    <asp:Label ID="lblTnxCount" runat="server" Text="Transaction Count" CssClass="lbl"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="width: 50%">
                                    <asp:Label ID="lblTotalAmount" runat="server" Text="Total Amount" CssClass="lbl"></asp:Label>
                                            </td>                                            
                                        </tr>
                                         <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 50%">
                                    <asp:TextBox ID="txtTnxCount" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox>
                                            </td>
                                            <td align="left" valign="top" style="width: 50%">
                                    <asp:TextBox ID="txtTotalAmount" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox>
                                            </td>                                            
                                        </tr>
                                         <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 50%">
                                    <asp:Label ID="lblCurrency" runat="server" Text="Currency" CssClass="lbl"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="width: 50%">
                                    <asp:Label ID="lblPaymentMode" runat="server" Text="Payment Mode" CssClass="lbl"></asp:Label>
                                            </td>                                            
                                        </tr>
                                         <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 50%">
                                    <asp:TextBox ID="txtCurrency" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox>
                                            </td>
                                            <td align="left" valign="top" style="width: 50%">
                                    <asp:TextBox ID="txtPaymentMode" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox>
                                            </td>                                            
                                        </tr>
                                         <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 50%">
                                    <asp:Label ID="lblAccount" runat="server" Text="Select Account" CssClass="lbl"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="width: 50%">
                                    <asp:Label ID="lblAvailableBalance" runat="server" Text="Available Balance" CssClass="lbl"></asp:Label>
                                            </td>                                            
                                        </tr>
                                         <tr align="left" valign="top" style="width: 100%">
                                            <td align="left" valign="top" style="width: 50%">
                                    <asp:DropDownList ID="ddlAccount" runat="server" CssClass="dropdown">
                                        <asp:ListItem Value="0">-- Select --</asp:ListItem>
                                    </asp:DropDownList>
                                            </td>
                                            <td align="left" valign="top" style="width: 50%">
                                    <asp:TextBox ID="txtAvailableBalance" runat="server" CssClass="txtbox" MaxLength="150"></asp:TextBox>
                                    <asp:LinkButton ID="lnkbtnGetBalance" runat="server">Check Balance</asp:LinkButton>
                                            </td>                                            
                                        </tr>
                                         <tr align="center"  valign="top" style="width: 100%">
                                            <td align="center"  valign="top" style="width: 100%" colspan="2">
                                    <asp:Button ID="btnSummary" runat="server" Text="Summary" CssClass="btn" CausesValidation="False" />
                                            </td>                                                                                      
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>--%>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblCompany" runat="server" Text="Company" CssClass="lbl"></asp:Label>
                                        <asp:Label ID="lblReqPrintLocation0" runat="server" 
                            ForeColor="Red" Text="*"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblProductType" runat="server" Text="Product Type" CssClass="lbl"></asp:Label>
                                        <asp:Label ID="lblReqPrintLocation1" runat="server" 
                            ForeColor="Red" Text="*"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown" 
                            AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:DropDownList ID="ddlProductType" runat="server" CssClass="dropdown" 
                            AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                                        <asp:RequiredFieldValidator ID="rfvddlCompany" runat="server" 
                                            ControlToValidate="ddlCompany" Display="Dynamic" Enabled="true" 
                                            ErrorMessage="Please select Company" InitialValue="0" 
                                            SetFocusOnError="True" ValidationGroup="Search"></asp:RequiredFieldValidator>
                                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                                        <asp:RequiredFieldValidator ID="rfvddlProductType" runat="server" 
                                            ControlToValidate="ddlProductType" Display="Dynamic" Enabled="true" 
                                            ErrorMessage="Please select Product Type" InitialValue="0" 
                                            SetFocusOnError="True" ValidationGroup="Search"></asp:RequiredFieldValidator>
                                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblInstructionNo" runat="server" Text="Instruction No." CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblInstrumentNo" runat="server" Text="Instrument No." CssClass="lbl"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtInstructionNo" runat="server" CssClass="txtbox" MaxLength="8"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        <asp:TextBox ID="txtInstrumentNo" runat="server" CssClass="txtbox" MaxLength="18"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" colspan="4">
                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn" 
                            ValidationGroup="Search" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                            CssClass="btnCancel" CausesValidation="False" Width="75px" />
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" colspan="4">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top"  style="width: 100%">
                    <td align="left" valign="top" colspan="4">
                      <%--  <asp:Panel ID="pnlPrintLocations" runat="server" ScrollBars="Vertical"
                           >--%>
                            <table id="tblPrintLocations" runat="server" style="width: 100%; display:none">
                                <tr style="width: 100%">
                                    <td align="left" valign="top" style="width: 50%">
                                        <asp:Label ID="lblLocationType" runat="server" Text="Location Type" CssClass="lbl"></asp:Label>
                                        <asp:Label ID="lblReqLocationType" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                    </td>
                                    <td align="left" valign="top" style="width: 50%">
                                        <asp:Label ID="lblSelectBank" runat="server" Text="Select Bank" CssClass="lbl"></asp:Label>
                                        <asp:Label ID="lblReqBank" runat="server" Text="*" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr style="width: 100%">
                                    <td align="left" valign="top" style="width: 50%">
                                        <asp:DropDownList ID="ddlLocationType" runat="server" CssClass="dropdown" 
                                            AutoPostBack="True">
                                            <asp:ListItem Value="0">-- All --</asp:ListItem>
                                            <asp:ListItem>Bank Locations</asp:ListItem>
                                            <asp:ListItem>Client Locations</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" valign="top" style="width: 50%">
                                        <asp:DropDownList ID="ddlBankOrClient" runat="server" CssClass="dropdown" 
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr style="width: 100%">
                                    <td align="left" valign="top" style="width: 50%">
                                        <asp:RequiredFieldValidator ID="rfvddlPrintLocationType" runat="server" 
                                            ControlToValidate="ddlLocationType" Display="Dynamic" Enabled="true" 
                                            ErrorMessage="Please select Location Type" InitialValue="0" 
                                            SetFocusOnError="True" ValidationGroup="PrintLocation"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" valign="top" style="width: 50%">
                                        <asp:RequiredFieldValidator ID="rfvddlBank" runat="server" 
                                            ControlToValidate="ddlBankOrClient" Display="Dynamic" Enabled="true" 
                                            ErrorMessage="Please select Bank" InitialValue="0" SetFocusOnError="True" 
                                            ValidationGroup="PrintLocation"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr style="width: 100%">
                                    <td align="left" style="width: 50%" valign="top">
                                        <asp:Label ID="lblPrintLocations" runat="server" CssClass="lbl" 
                                            Text="Select Print Location"></asp:Label>
                                        <asp:Label ID="lblReqPrintLocation" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </td>
                                    <td align="left" style="width: 50%" valign="top">
                                        &nbsp;</td>
                                </tr>
                                <tr style="width: 100%">

                                    <td align="left" valign="top" style="width: 50%">
                                        <asp:DropDownList ID="ddlPrintLocations" runat="server" 
                                            CssClass="dropdown">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" valign="top" style="width: 50%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr style="width: 100%">
                                    <td align="left" style="width: 50%" valign="top">
                                        <asp:RequiredFieldValidator ID="rfvddlPrintLocation" runat="server" 
                                            ControlToValidate="ddlPrintLocations" Display="Dynamic" Enabled="true" 
                                            ErrorMessage="Please select Print Location" InitialValue="0" 
                                            SetFocusOnError="True" ValidationGroup="PrintLocation"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" style="width: 50%" valign="top">
                                        &nbsp;</td>
                                </tr>
                                <tr style="width: 100%">
                                    <td align="center" valign="top" colspan="2">
                                        <asp:Button ID="btnAmendPrintLocations" runat="server" CssClass="btn" CausesValidation="true"
                                            Text="Amend Print Location" ValidationGroup="PrintLocation" />
                                    </td>
                                </tr>
                            </table>
                        <%--</asp:Panel>--%>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 25%">
                        <asp:Label ID="lblAmendPrintLocation" runat="server" Text="Amend Print Locations"
                            CssClass="headingblue" Visible="False"></asp:Label>
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" style="width: 25%">
                        &nbsp;
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%" colspan="4">
                        <asp:Label ID="lblNRFAmendLocation" runat="server" Text="No Record Found." CssClass="headingblue"
                            Visible="False"></asp:Label>
                        <br />
                        <telerik:RadGrid ID="gvAmendPrintLocation" runat="server" AllowPaging="True" 
                            Width="100%" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                            PageSize="1">
                            <AlternatingItemStyle CssClass="rgAltRow" />
                            <MasterTableView DataKeyNames="InstructionID,GroupCode,IsAmendmentComplete,ClientAccountNo,ClientAccountBranchCode,ClientAccountCurrency,PaymentNatureCode">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <%--      <telerik:GridTemplateColumn>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Checked="true" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox" Checked="true" Text=" " />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>--%>
                                    <telerik:GridBoundColumn DataField="InstructionID" HeaderText="Instruction No." SortExpression="InstructionID">
                                    </telerik:GridBoundColumn>
                                      <telerik:GridBoundColumn DataField="FileBatchNo" HeaderText="Batch No." SortExpression="FileBatchNo">
                                       
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="CompanyName" HeaderText="Client Name" SortExpression="CompanyName">
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridBoundColumn DataField="Dated" HeaderText="Creation Date" SortExpression="Dated"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}">
                                    </telerik:GridBoundColumn>--%>
                                    <telerik:GridBoundColumn DataField="PaymentMode" HeaderText="Payment Nature" SortExpression="PaymentMode">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ClientAccountNo" HeaderText="Account No." SortExpression="ClientAccountNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ClientAccountBranchCode" HeaderText="Branch Code" SortExpression="ClientAccountBranchCode">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ClientAccountCurrency" HeaderText="Currency" SortExpression="ClientAccountCurrency">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProductTypeCode" HeaderText="Product Type" SortExpression="ProductTypeCode">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InstrumentNo" HeaderText="Instrument No." SortExpression="InstrumentNo">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Amount" HeaderText="Amount" SortExpression="Amount"
                                        HtmlEncode="false" DataFormatString="{0:N0}">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="OfficeName" HeaderText="Print Location" SortExpression="OfficeName">
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                        </telerik:RadGrid>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%" colspan="4">
                        
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%" colspan="4">
                        &nbsp;</td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="center" valign="top" style="width: 100%" colspan="4">
                        &nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
