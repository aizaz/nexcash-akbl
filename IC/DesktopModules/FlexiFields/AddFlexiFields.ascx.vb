﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Partial Class DesktopModules_FlexiFields_AddFlexiFields
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private FieldID As String = ""
    Private htRights As Hashtable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not ViewState("htRights") Is Nothing Then
            htRights = DirectCast(ViewState("htRights"), Hashtable)
        End If
        FieldID = Request.QueryString("Id").ToString()
        If Not Page.IsPostBack Then
            ViewState("htRights") = Nothing
            ManagePageAccessRights()
            LoadddlGroup()
            LoadddlCompany()
            If FieldID.ToString() = "0" Then
                lblPageHeader.Text = "Add Flexi Field"
                btnFieldSave.Text = "Save"
                btnFieldSave.Visible = CBool(htRights("Add"))
            Else
                lblPageHeader.Text = "Update Flexi Field"
                btnFieldSave.Text = "Update"
                btnFieldSave.Visible = CBool(htRights("Update"))
                Dim cFlexiField As New ICFlexiFields
                cFlexiField.es.Connection.CommandTimeout = 3600
                If cFlexiField.LoadByPrimaryKey(FieldID) Then
                    txtFieldTitle.Text = cFlexiField.FieldTitle
                    listFieldType.SelectedValue = cFlexiField.FieldType
                    SetFieldType()
                    txtdefaultvalue.Text = cFlexiField.DefaultValue
                    txthelptext.Text = cFlexiField.HelpText
                    If listFieldType.SelectedItem.Text.ToLower = "dropdownlist" Or listFieldType.SelectedItem.Text.ToLower = "ordertype" Then
                        Dim listOffields As String = String.Empty
                        pnlDropDownList.Visible = True
                        listOffields = cFlexiField.ListOfValues
                        Dim list As String()
                        list = listOffields.Split(";")
                        LbDropDonw.DataSource = list
                        LbDropDonw.DataBind()
                    ElseIf listFieldType.SelectedItem.Text.ToLower = "textbox" Then
                        txtLenght.Text = cFlexiField.FieldLength
                        Me.lbldefaultvalue.Visible = True
                        Me.txtdefaultvalue.Visible = True
                        Me.lblGuidLine.Visible = True
                    ElseIf listFieldType.SelectedItem.Text.ToLower = "multiline textbox" Then
                        Me.lbldefaultvalue.Visible = True
                        Me.txtdefaultvalue.Visible = True
                        Me.lblGuidLine.Visible = True
                    Else
                        pnlDropDownList.Visible = False
                        listFieldType.Enabled = True
                    End If
                    Dim objCompany As New ICCompany
                    If objCompany.LoadByPrimaryKey(cFlexiField.CompanyCode) Then
                        ddlGroup.SelectedValue = objCompany.GroupCode.ToString()
                        LoadddlCompany()
                        ddlCompany.SelectedValue = objCompany.CompanyCode.ToString()

                    End If
                    txtLenght.Text = cFlexiField.FieldLength
                    ChkIsRequired.Checked = cFlexiField.IsRequired
                    ChkIsEnable.Checked = cFlexiField.IsEnabled
                    chkActive.Checked = cFlexiField.IsActive
                    If cFlexiField.ValExp = "" Then
                        ListValidationExpression.SelectedValue = "-1"
                    ElseIf cFlexiField.ValExp = "\d+" Then
                        ListValidationExpression.SelectedValue = "\d+"
                    Else
                        ListValidationExpression.SelectedValue = "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    End If
                End If
            End If
        End If
        'If CheckIsAdminOrSuperUser() = False Then
        '    ManagePageAccessRights()
        'End If
    End Sub
   Private Sub ManagePageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Flexi Fields Management")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckIsAdminOrSuperUser() As Boolean
        'Dim userCtrl As New RoleController
        'Dim Result As Boolean = False
        'Dim str As String()
        'Dim iUserRole As New UserRoleInfo
        'Dim i As Integer
        'Try
        '    Result = False
        '    str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
        '    For i = 0 To str.Length - 1
        '        If Trim(str(i).ToString()) = "FRC Administrator" Then
        '            Result = True
        '        End If
        '        If Trim(str(i).ToString()) = "Administrators" Then
        '            Result = True
        '        End If
        '    Next
        '    If Result = False Then
        '        If Me.UserInfo.IsSuperUser = True Then
        '            Result = True
        '        End If
        '    End If
        '    Return Result
        'Catch ex As Exception
        '    ProcessModuleLoadException(Me, ex, False)
        '    UIUtilities.ShowDialog(Me, "Error", ex.Message, Intelligenes.FRC.Dialogmessagetype.Failure)
        '    Return Result
        'End Try
    End Function
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Select --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Protected Sub listFieldType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles listFieldType.SelectedIndexChanged
        Try
            SetFieldType()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub SetFieldType()
        Try
            Me.lbldefaultvalue.Visible = False
            Me.txtdefaultvalue.Visible = False
            Me.lblGuidLine.Visible = False
            If listFieldType.SelectedItem.Text.ToLower = "dropdownlist" Then
                pnlDropDownList.Visible = True
                ListValidationExpression.SelectedIndex = 0
            Else
                pnlDropDownList.Visible = False
                ListValidationExpression.SelectedIndex = 0
            End If
            If listFieldType.SelectedItem.Text.ToLower = "textbox" Then
                pnlValidation.Visible = True
                ListValidationExpression.SelectedIndex = 0
                Me.lbldefaultvalue.Visible = True
                Me.txtdefaultvalue.Visible = True
                Me.lblGuidLine.Visible = True
            Else
                pnlValidation.Visible = False
                ListValidationExpression.SelectedIndex = 0
            End If
            If listFieldType.SelectedItem.Text.ToLower = "multiline textbox" Then
                Me.lbldefaultvalue.Visible = True
                Me.txtdefaultvalue.Visible = True
                Me.lblGuidLine.Visible = True
            End If
            If ListValidationExpression.SelectedIndex = 0 Then
                lblCustomValidation.Visible = False
                txtCustomValidation.Visible = False
                lblCustomValidationIR.Visible = False
            Else
                lblCustomValidation.Visible = True
                txtCustomValidation.Visible = True
                lblCustomValidationIR.Visible = True
            End If
            If listFieldType.SelectedItem.Text.ToLower = "radiobutton" Or listFieldType.SelectedItem.Text.ToLower = "checkbox" Then
                PnlIsREQ.Visible = False
            Else
                PnlIsREQ.Visible = True
            End If
            If LbDropDonw.Items.Count > 0 Then
                ' RequiredFieldValidatorValue0.Enabled = False
            Else
                '  RequiredFieldValidatorValue0.Enabled = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub ListValidationExpression_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListValidationExpression.SelectedIndexChanged
        Try
            If ListValidationExpression.SelectedItem.Text.ToLower = "custom" Then
                txtCustomValidation.Visible = True
                lblCustomValidation.Visible = True
                lblCustomValidationIR.Visible = True
                RequiredFieldValidator14.Enabled = True
                txtCustomValidation.Visible = True
                lblCustomValidation.Visible = True
            Else
                txtCustomValidation.Visible = False
                lblCustomValidation.Visible = False
                lblCustomValidationIR.Visible = False
                RequiredFieldValidator14.Enabled = False
                txtCustomValidation.Visible = False
                lblCustomValidation.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Public Sub ClearFormField()
        txtFieldTitle.Text = ""
        listFieldType.SelectedIndex = 0
        Me.txtdefaultvalue.Text = ""
        Me.txthelptext.Text = ""
        ChkIsRequired.Checked = False
        ChkIsEnable.Checked = False
        chkActive.Checked = True
        Me.lbldefaultvalue.Visible = False
        Me.txtdefaultvalue.Visible = False
        Me.lblGuidLine.Visible = False
    End Sub
    Public Function IsDropDownList(ByVal FieldId As Integer) As Boolean
        Try
            Dim obj As New ICFlexiFields
            obj.es.Connection.CommandTimeout = 3600
            obj.LoadByPrimaryKey(FieldId)
            If obj.FieldType = "RadioButtonList" Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function
    Protected Sub BtnAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnAdd.Click
        Try
            LbDropDonw.Items.Add(txtDisplayText.Text & "|" & TxtValue.Text)
            txtDisplayText.Text = ""
            TxtValue.Text = ""
            txtDisplayText.Focus()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Protected Sub btbClear_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btbClear.Click
        If LbDropDonw.Items.Count > 0 Then
            If ChkRemoveAll.Checked = False Then
                If LbDropDonw.SelectedItem Is Nothing Then
                    UIUtilities.ShowDialog(Me, "Flexi Fields", "Please select item to remove.", IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
                LbDropDonw.Items.Remove(LbDropDonw.SelectedItem)
            Else
                LbDropDonw.Items.Clear()
                ChkRemoveAll.Checked = False
            End If
        Else
            UIUtilities.ShowDialog(Me, "Flexi Fields", "No item to remove.", IC.Dialogmessagetype.Failure)
            Exit Sub
        End If
        
    End Sub
    Protected Sub btnFieldSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFieldSave.Click
        Try
            Dim cFlexiField As New ICFlexiFields
            Dim Action As String = ""
            Dim IsReq, IsEnab As String
            cFlexiField.es.Connection.CommandTimeout = 3600
            If FieldID <> "0" Then
                cFlexiField.LoadByPrimaryKey(FieldID)
            End If
            cFlexiField.FieldTitle = txtFieldTitle.Text
            cFlexiField.FieldType = listFieldType.SelectedValue
            cFlexiField.DefaultValue = Me.txtdefaultvalue.Text
            cFlexiField.HelpText = Me.txthelptext.Text
            If listFieldType.SelectedItem.Text.ToLower = "dropdownlist" Then
                Dim str As New StringBuilder
                For i As Integer = 0 To LbDropDonw.Items.Count - 1
                    If i = LbDropDonw.Items.Count - 1 Then
                        str.Append(LbDropDonw.Items(i).Value)
                    Else
                        str.Append(LbDropDonw.Items(i).Value & ";")
                    End If
                Next
                cFlexiField.ListOfValues = str.ToString()
            ElseIf listFieldType.SelectedItem.Text.ToLower = "textbox" Then
                cFlexiField.FieldLength = txtLenght.Text
            Else
                cFlexiField.ListOfValues = ""
            End If
            If ListValidationExpression.SelectedItem.Text.ToLower = "numeric" Or ListValidationExpression.SelectedItem.Text.ToLower = "email" Then
                cFlexiField.ValExp = ListValidationExpression.SelectedValue
            ElseIf ListValidationExpression.SelectedItem.Text.ToLower = "custom" Then
                cFlexiField.ValExp = txtCustomValidation.Text
            Else
                cFlexiField.ValExp = ""
            End If
            cFlexiField.FieldLength = txtLenght.Text
            cFlexiField.CompanyCode = ddlCompany.SelectedValue.ToString()
            cFlexiField.IsRequired = ChkIsRequired.Checked
            cFlexiField.IsEnabled = ChkIsEnable.Checked
            cFlexiField.IsActive = chkActive.Checked
            If ChkIsRequired.Checked = True Then
                IsReq = "True"
            Else
                IsReq = "False"
            End If
            If ChkIsEnable.Checked = True Then
                IsEnab = "True"
            Else
                IsEnab = "False"
            End If
            If FieldID = "0" Then
                cFlexiField.CreatedBy = Me.UserId
                cFlexiField.CreatedOn = Date.Now
                cFlexiField.FieldOrder = ICFlexiFieldsController.GetFlexiFieldOrderByCompanyCode(ddlCompany.SelectedValue.ToString())
                If ICFlexiFieldsController.CheckDuplicate(cFlexiField) = False Then
                    Action = "Flexi Field [Title : " & cFlexiField.FieldTitle.ToString() & " ; Type : " & cFlexiField.FieldType.ToString() & " ; Length : " & cFlexiField.FieldLength.ToString() & " ; IsRequired : " & IsReq.ToString() & " ; IsEnabled : " & IsEnab.ToString() & "] of Company [Code : " & ddlCompany.SelectedValue.ToString() & " ; Name : " & ddlCompany.SelectedItem.Text.ToString() & "] Added."
                    ICFlexiFieldsController.AddFlexiField(cFlexiField, False, Action.ToString(), Me.UserId, Me.UserInfo.Username)
                    UIUtilities.ShowDialog(Me, "Flexi Fields", "Flexi Field added successfully.", IC.Dialogmessagetype.Success, NavigateURL("SaveFlexiField", "&mid=" & Me.ModuleId & "&id=0"))
                Else
                    UIUtilities.ShowDialog(Me, "Flexi Fields", "Can not add duplicate Flexi Field or Field exist as Template Field.", IC.Dialogmessagetype.Failure)
                    Exit Sub
                End If
            Else
                Action = "Flexi Field [Title : " & cFlexiField.FieldTitle.ToString() & " ; Type : " & cFlexiField.FieldType.ToString() & " ; Length : " & cFlexiField.FieldLength.ToString() & " ; IsRequired : " & IsReq.ToString() & " ; IsEnabled : " & IsEnab.ToString() & "] of Company [Code : " & ddlCompany.SelectedValue.ToString() & " ; Name : " & ddlCompany.SelectedItem.Text.ToString() & "] Updated."
                ICFlexiFieldsController.AddFlexiField(cFlexiField, True, Action.ToString(), Me.UserId, Me.UserInfo.Username)
                UIUtilities.ShowDialog(Me, "Flexi Fields", "Flexi Field updated successfully.", IC.Dialogmessagetype.Success, NavigateURL())
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try
    End Sub
  
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        LoadddlCompany()
    End Sub
End Class
