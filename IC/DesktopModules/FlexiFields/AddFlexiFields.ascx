﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddFlexiFields.ascx.vb"
    Inherits="DesktopModules_FlexiFields_AddFlexiFields" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:TextBoxSetting BehaviorID="rvFielfTitle" Validation-IsRequired="false" EmptyMessage="Enter Field Title">
        <TargetControls>
            <telerik:TargetInput ControlID="txtFieldTitle" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rvHelpText" Validation-IsRequired="false" EmptyMessage="Enter Help Text">
        <TargetControls>
            <telerik:TargetInput ControlID="txthelptext" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rvdefaultvalue" Validation-IsRequired="false"
        EmptyMessage="Enter Drfault Value">
        <TargetControls>
            <telerik:TargetInput ControlID="txtdefaultvalue" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rvDisplayText" Validation-IsRequired="false"
        EmptyMessage="Enter Display Text">
        <TargetControls>
            <telerik:TargetInput ControlID="txtDisplayText" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rvValue" Validation-IsRequired="false" EmptyMessage="Enter value">
        <TargetControls>
            <telerik:TargetInput ControlID="TxtValue" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rvCustomValidation" Validation-IsRequired="false"
        EmptyMessage="Enter Custom Validation">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCustomValidation" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rvLenght" Validation-IsRequired="false" EmptyMessage="Enter Field Length">
        <TargetControls>
            <telerik:TargetInput ControlID="txtLenght" />
        </TargetControls>
    </telerik:TextBoxSetting>
</telerik:RadInputManager>
<table width="100%">
    <tr valign="top" align="left" style="width: 100%">
        <td valign="top" align="left" style="width: 100%">
            <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
        </td>
    </tr>
    <tr valign="top" align="left" style="width: 100%">
        <td valign="top" align="left" style="width: 100%">
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label3" runat="server" Text="*" ForeColor="Red"></asp:Label>&nbsp;are
            required.
        </td>
    </tr>
    <tr valign="top" align="left" style="width: 100%">
        <td valign="top" align="left" style="width: 100%">
            <table width="100%">
                <tr>
                    <td valign="middle" align="left" width="25%">
                        <asp:Label ID="lblGroup" runat="server" Text="Group" CssClass="lbl"></asp:Label>
                        <asp:Label ID="Label10" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td valign="middle" align="left" width="25%">
                        &nbsp;
                    </td>
                    <td valign="top" align="left" width="25%">
                        <asp:Label ID="lblCompany" runat="server" Text="Company" CssClass="lbl"></asp:Label>
                        <asp:Label ID="Label19" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td width="25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="left" width="25%">
                          <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True" CssClass="dropdown">
                                        <asp:ListItem Value="0">-- Select --</asp:ListItem>
                                        <asp:ListItem>FFC</asp:ListItem>
                                        <asp:ListItem>Engro</asp:ListItem>
                                    </asp:DropDownList>
                    </td>
                    <td valign="middle" align="left" width="25%">
                        &nbsp;
                    </td>
                    <td valign="top" align="left" width="25%">
                         <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdown">
                                        <asp:ListItem Value="0">-- Select --</asp:ListItem>
                                        <asp:ListItem>Engro Foods</asp:ListItem>
                                        <asp:ListItem>Engro Corp</asp:ListItem>
                                    </asp:DropDownList>
                    </td>
                    <td width="25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="left" width="25%">
                        <asp:RequiredFieldValidator ID="rfvGroup" runat="server" ControlToValidate="ddlGroup"
                            CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Group"
                            SetFocusOnError="True" ToolTip="Required Field" InitialValue="0" 
                            ValidationGroup="1"></asp:RequiredFieldValidator>
                    </td>
                    <td valign="middle" align="left" width="25%">
                        &nbsp;
                    </td>
                    <td valign="top" align="left" width="25%">
                        <asp:RequiredFieldValidator ID="rfvCompany" runat="server" ControlToValidate="ddlCompany"
                            CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Company"
                            SetFocusOnError="True" ToolTip="Required Field" InitialValue="0" 
                            ValidationGroup="1"></asp:RequiredFieldValidator>
                    </td>
                    <td width="25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td valign="middle" align="left" width="25%">
                        <asp:Label ID="Label7" runat="server" CssClass="lbl" Text="Field Title"></asp:Label>
                        <asp:Label ID="Label11" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td valign="middle" align="left" width="25%">
                        &nbsp;
                    </td>
                    <td valign="top" align="left" width="25%">
                        <asp:Label ID="Label4" runat="server" CssClass="lbl" Text="Field Type"></asp:Label>
                        <asp:Label ID="Label13" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td width="25%">
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" width="25%">
                        <asp:TextBox ID="txtFieldTitle" runat="server" CssClass="txtbox" MaxLength="50" ValidationGroup="1"></asp:TextBox>
                    </td>
                    <td align="left" valign="top" width="25%">
                        &nbsp;
                    </td>
                    <td valign="top" width="25%">
                        <asp:DropDownList ID="listFieldType" runat="server" AutoPostBack="True" CssClass="dropdown"
                            OnSelectedIndexChanged="listFieldType_SelectedIndexChanged" ValidationGroup="1">
                            <asp:ListItem Value="-1">--Select--</asp:ListItem>
                            <asp:ListItem>TextBox</asp:ListItem>
                            <asp:ListItem>MultiLine TextBox</asp:ListItem>
                            <asp:ListItem>DropDownList</asp:ListItem>
                            <asp:ListItem>CheckBox</asp:ListItem>
                            <asp:ListItem>RadioButton</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left" width="25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" width="25%">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtFieldTitle"
                            Display="Dynamic" ErrorMessage="Please enter Field Title" SetFocusOnError="True"
                            ValidationGroup="1" CssClass="txmaingrey"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" valign="top" width="25%">
                        &nbsp;
                    </td>
                    <td align="left" valign="top" width="25%">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" __designer:wfdid="w268"
                            ControlToValidate="listFieldType" CssClass="txmaingrey" Display="Dynamic" ErrorMessage="Please select Field Type"
                            InitialValue="-1" ValidationGroup="1"></asp:RequiredFieldValidator>
                    </td>
                    <td align="left" width="25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" width="25%">
                        <asp:Label ID="lblhelptext" runat="server" CssClass="lbl" Text="Help Text"></asp:Label>
                        <asp:Label ID="Label12" runat="server" Text="*" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="left" width="25%">
                        &nbsp;
                    </td>
                    <td align="left" width="25%" valign="top">
                                        <asp:Label ID="lblvalidationExpression0" runat="server" CssClass="lbl" 
                                            Text="Field Length"></asp:Label>
                                        <asp:Label ID="Label18" runat="server" ForeColor="Red" Text="*"></asp:Label>
                    </td>
                    <td align="left" width="25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" width="25%">
                        <asp:TextBox ID="txthelptext" runat="server" CssClass="txtbox" MaxLength="200" ValidationGroup="1"></asp:TextBox>
                    </td>
                    <td align="left" width="25%">
                        &nbsp;
                    </td>
                    <td align="left" width="25%" valign="top">
                                        <asp:TextBox ID="txtLenght" runat="server" CssClass="txtbox" MaxLength="5" 
                                            ValidationGroup="1"></asp:TextBox>
                    </td>
                    <td align="left" width="25%">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" width="25%">
                        &nbsp;</td>
                    <td align="left" width="25%">
                        &nbsp;</td>
                    <td align="left" width="25%" valign="top">
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtLenght"
                                            ErrorMessage="Please enter Numeric value" 
                            ValidationExpression="\d+" ValidationGroup="1"></asp:RegularExpressionValidator>
                    </td>
                    <td align="left" width="25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" width="25%">
                                        <asp:Label ID="Label20" runat="server" CssClass="lbl" 
                            Text="Is Active"></asp:Label>
                                    </td>
                    <td align="left" width="25%">
                        &nbsp;</td>
                    <td align="left" width="25%" valign="top">
                                        &nbsp;</td>
                    <td align="left" width="25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" width="25%">
            <asp:CheckBox ID="chkActive" runat="server" Text=" " CssClass="chkBox" Checked="True" />
                    </td>
                    <td align="left" width="25%">
                        &nbsp;</td>
                    <td align="left" width="25%" valign="top">
                                        &nbsp;</td>
                    <td align="left" width="25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" width="25%">
                        &nbsp;</td>
                    <td align="left" width="25%">
                        &nbsp;</td>
                    <td align="left" width="25%" valign="top">
                                        &nbsp;</td>
                    <td align="left" width="25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" width="25%">
                        <asp:Label ID="lbldefaultvalue" runat="server" CssClass="lbl" Text="Default Value"
                            Visible="False"></asp:Label>
                    </td>
                    <td align="left" width="25%">
                        &nbsp;</td>
                    <td align="left" width="25%" valign="top">
                        &nbsp;</td>
                    <td align="left" width="25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" width="25%">
                        <asp:TextBox ID="txtdefaultvalue" runat="server" CssClass="txtbox" MaxLength="50"
                            ValidationGroup="1" Visible="False"></asp:TextBox>
                    </td>
                    <td align="left" width="25%">
                        &nbsp;</td>
                    <td align="left" width="25%" valign="top">
                        &nbsp;</td>
                    <td align="left" width="25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" width="25%">
            <asp:Label ID="lblGuidLine" runat="server"
                Font-Size="Small" ForeColor="#999999" Visible="False">Note: If validation is set then set default value according to validation.</asp:Label>
                    </td>
                    <td align="left" width="25%">
                        &nbsp;</td>
                    <td align="left" width="25%" valign="top">
                        &nbsp;</td>
                    <td align="left" width="25%">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="left" width="25%">
                        &nbsp;
                    </td>
                    <td align="left" width="25%">
                        &nbsp;
                    </td>
                    <td align="left" width="25%" valign="top">
                        &nbsp;
                    </td>
                    <td align="left" width="25%">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr valign="top" align="left" style="width: 100%">
        <td valign="top" align="left" style="width: 100%">
            <table width="100%">
                <tr>
                    <td align="left" colspan="2">
                        <asp:Panel ID="pnlDropDownList" runat="server" Visible="false">
                            <table style="width: 100%">
                                <tr>
                                    <td align="left" valign="top" width="25%">
                                        <asp:Label ID="Label8" runat="server" CssClass="lbl" Text="Display Text"></asp:Label>
                                        <asp:Label ID="Label14" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </td>
                                    <td align="left" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                    <td width="25%" align="left" valign="top">
                                        <asp:Label ID="Label9" runat="server" CssClass="lbl" Text="Value"></asp:Label>
                                        <asp:Label ID="Label15" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </td>
                                    <td width="25%" align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" width="25%">
                                        <asp:TextBox ID="txtDisplayText" runat="server" CssClass="txtbox" MaxLength="50"
                                            ValidationGroup="2"></asp:TextBox>
                                    </td>
                                    <td align="left" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                    <td width="25%" align="left" valign="top">
                                        <asp:TextBox ID="TxtValue" runat="server" CssClass="txtbox" MaxLength="50" ValidationGroup="2"></asp:TextBox>
                                    </td>
                                    <td width="25%" align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" dir="ltr" valign="top" width="25%">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorDisplayTxt" runat="server"
                                            ControlToValidate="txtDisplayText" CssClass="txmaingrey" Display="Dynamic" ErrorMessage="Please enter display text"
                                            SetFocusOnError="True" ValidationGroup="2"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" dir="ltr" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                    <td width="25%" align="left" valign="top">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorValue" runat="server" ControlToValidate="txtvalue"
                                            CssClass="txmaingrey" Display="Dynamic" ErrorMessage="Please enter value" SetFocusOnError="True"
                                            ValidationGroup="2"></asp:RequiredFieldValidator>
                                    </td>
                                    <td width="25%" align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                    <td align="left" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                    <td width="25%" align="left" valign="top">
                                        &nbsp;
                                    </td>
                                    <td width="25%" align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" width="25%">
                                        <asp:ImageButton ID="BtnAdd" runat="server" CssClass="button" ImageUrl="~/images/add.gif"
                                            Text="Add" ValidationGroup="2" />
                                    </td>
                                    <td align="left" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                    <td width="25%" align="left" valign="top">
                                        &nbsp;
                                    </td>
                                    <td width="25%" align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td width="25%" align="left" valign="top">
                                        <asp:ListBox ID="LbDropDonw" runat="server" Width="200px"></asp:ListBox>
                                    </td>
                                    <td width="25%" align="left" valign="top">
                                        &nbsp;
                                    </td>
                                    <td width="25%" align="left" valign="top">
                                        &nbsp;
                                    </td>
                                    <td width="25%" align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" width="25%">
                                        <asp:CheckBox ID="ChkRemoveAll" runat="server" Text=" Remove All" />
                                    </td>
                                    <td align="left" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                    <td align="left" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                    <td align="left" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td width="25%" align="left" valign="top">
                                        <asp:ImageButton ID="btbClear" runat="server" CssClass="button" ImageUrl="~/images/delete.gif"
                                            Text="Clear" Width="16px" />
                                    </td>
                                    <td width="25%" align="left" valign="top">
                                        &nbsp;
                                    </td>
                                    <td width="25%" align="left" valign="top">
                                        &nbsp;
                                    </td>
                                    <td width="25%" align="left" valign="top">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:Panel ID="pnlValidation" runat="server" Visible="false">
                            <table width="100%">
                                <tr>
                                    <td align="left" valign="top" width="25%">
                                        <asp:Label ID="lblvalidationExpression" runat="server" CssClass="lbl" Text="Validation Expression"></asp:Label>
                                    </td>
                                    <td align="left" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="lblCustomValidation" runat="server" CssClass="lbl" Text="Custom Validation"
                                            Visible="False"></asp:Label>
                                        <asp:Label ID="lblCustomValidationIR" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </td>
                                    <td align="left" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" width="25%">
                                        <asp:DropDownList ID="ListValidationExpression" runat="server" __designer:wfdid="w265"
                                            AutoPostBack="True" CssClass="frinputbx" OnSelectedIndexChanged="ListValidationExpression_SelectedIndexChanged"
                                            Width="200px">
                                            <asp:ListItem Value="-1">--Select--</asp:ListItem>
                                            <asp:ListItem Value="\d+">Numeric</asp:ListItem>
                                            <asp:ListItem Value="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">Email</asp:ListItem>
                                            <asp:ListItem>Custom</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCustomValidation" runat="server" CssClass="txtbox" MaxLength="50"
                                            ValidationGroup="1" Visible="False"></asp:TextBox>
                                    </td>
                                    <td align="left" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                    <td align="left" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                    <td align="left">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtCustomValidation"
                                            CssClass="txmaingrey" Display="Dynamic" ErrorMessage="Please enter Custom Validation"
                                            SetFocusOnError="True" ValidationGroup="1" Enabled="False"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:Panel ID="PnlIsREQ" runat="server" Visible="false">
                            <table style="width: 100%">
                                <tr>
                                    <td align="left" valign="top" width="25%">
                                        <asp:Label ID="Label2" runat="server" CssClass="lbl" Text="Is Enabled"></asp:Label>
                                    </td>
                                    <td align="left" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                    <td align="left" valign="top" width="25%">
                                        <asp:Label ID="Label1" runat="server" CssClass="lbl" Text="Is Required"></asp:Label>
                                    </td>
                                    <td align="left" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top" width="25%">
                                        <asp:CheckBox ID="ChkIsEnable" runat="server" CssClass="txmaingrey" />
                                    </td>
                                    <td align="left" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                    <td align="left" valign="top" width="25%">
                                        <asp:CheckBox ID="ChkIsRequired" runat="server" CssClass="txmaingrey" />
                                    </td>
                                    <td align="left" valign="top" width="25%">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr valign="top" align="center" style="width: 100%">
        <td valign="top" align="center" style="width: 100%">
            <asp:Button ID="btnFieldSave" runat="server" CssClass="btn" Text="Save" Width="71px"
                ValidationGroup="1" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" CausesValidation="False" CssClass="btn"
                Text="Cancel" Width="71px" />
        </td>
    </tr>
</table>
