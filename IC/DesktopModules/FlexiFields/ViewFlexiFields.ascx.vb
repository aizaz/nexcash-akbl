﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data.OleDb
Imports System.Data
Imports Telerik.Web.UI
Partial Class DesktopModules_FlexiFields_ViewFlexiFields
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If
            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                ManagePageAccessRights()
                LoadddlGroup()
                LoadddlCompany()
                btnSaveFlexiField.Visible = CBool(htRights("Add"))
                LoadgvFlexiFields(True)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub ManagePageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Flexi Fields Management")
            ViewState("htRights") = htRights
            'If htRights("View") = False Then
            '    UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            'End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Function CheckIsAdminOrSuperUser() As Boolean
        'Dim userCtrl As New RoleController
        'Dim Result As Boolean = False
        'Dim str As String()
        'Dim iUserRole As New UserRoleInfo
        'Dim i As Integer
        'Try
        '    Result = False
        '    str = userCtrl.GetPortalRolesByUser(Me.UserId, Me.PortalId)
        '    ' str = userCtrl.GetRolesByUser(Me.UserId, Me.PortalId)
        '    For i = 0 To str.Length - 1
        '        If Trim(str(i).ToString()) = "FRC Administrator" Then
        '            Result = True
        '        End If
        '        If Trim(str(i).ToString()) = "Administrators" Then
        '            Result = True
        '        End If
        '    Next
        '    If Result = False Then
        '        If Me.UserInfo.IsSuperUser = True Then
        '            Result = True
        '        End If
        '    End If
        '    Return Result
        'Catch ex As Exception
        '    ProcessModuleLoadException(Me, ex, False)
        '    UIUtilities.ShowDialog(Me, "Error", ex.Message, Intelligenes.FRC.Dialogmessagetype.Failure)
        '    Return False
        'End Try
    End Function
    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString())
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Private Sub LoadgvFlexiFields(ByVal IsBind As Boolean)
        Try
            ICFlexiFieldsController.SetgvFlexiFields(ddlGroup.SelectedValue.ToString(), ddlCompany.SelectedValue.ToString(), gvFlexiFields.CurrentPageIndex + 1, gvFlexiFields.PageSize, gvFlexiFields, IsBind)
            If gvFlexiFields.Items.Count > 0 Then
                lblRNF.Visible = False
                gvFlexiFields.Visible = True
                btnBulkDeleteFlexiFields.Visible = CBool(htRights("Delete"))
                gvFlexiFields.Columns(10).Visible = CBool(htRights("Delete"))
            Else
                lblRNF.Visible = True
                gvFlexiFields.Visible = False
                btnBulkDeleteFlexiFields.Visible = False
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub
    Protected Sub btnSaveBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveFlexiField.Click
        Response.Redirect(NavigateURL("SaveFlexiField", "&mid=" & Me.ModuleId & "&id=0"), False)
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        LoadddlCompany()
        LoadgvFlexiFields(True)
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        LoadgvFlexiFields(True)
    End Sub

    Protected Sub gvFlexiFields_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvFlexiFields.ItemCommand
        Try
            If e.CommandName = "del" Then
                Dim FieldID As String = ""
                FieldID = e.CommandArgument.ToString()
                DeleteFlexiField(FieldID.ToString())
                UIUtilities.ShowDialog(Me, "Flexi Fields", "Flexi Field deleted successfully.", IC.Dialogmessagetype.Success, NavigateURL())
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Flexi Fields", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
    Private Sub DeleteFlexiField(ByVal FieldID As String)
        Dim objFlexiField As New ICFlexiFields
        Dim objCompany As New ICCompany
        Dim ActionText As String = ""
        Dim IsReq, IsEnab As String
        objFlexiField.LoadByPrimaryKey(FieldID.ToString())
        objCompany.LoadByPrimaryKey(objFlexiField.CompanyCode.ToString())
        If objFlexiField.IsRequired = True Then
            IsReq = "True"
        Else
            IsReq = "False"
        End If
        If objFlexiField.IsEnabled = True Then
            IsEnab = "True"
        Else
            IsEnab = "False"
        End If
        ActionText = "Flexi Field [Title : " & objFlexiField.FieldTitle.ToString() & " ; Type : " & objFlexiField.FieldType.ToString() & " ; Length : " & objFlexiField.FieldLength.ToString() & " ; IsRequired : " & IsReq.ToString() & " ; IsEnabled : " & IsEnab.ToString() & "] of Company [Code : " & objCompany.CompanyCode.ToString() & " ; Name : " & objCompany.CompanyName.ToString() & "] Deleted."
        ICFlexiFieldsController.DeleteFlexiField(objFlexiField, ActionText.ToString(), Me.UserId, Me.UserInfo.Username)

    End Sub
    Protected Sub gvFlexiFields_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvFlexiFields.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvFlexiFields.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvFlexiFields_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvFlexiFields.ItemDataBound
        Dim chkProcessAll As CheckBox
        If e.Item.ItemType = Telerik.Web.UI.GridItemType.Header Then
            chkProcessAll = New CheckBox
            chkProcessAll = DirectCast(e.Item.Cells(0).FindControl("chkSelectAll"), CheckBox)
            chkProcessAll.Attributes("onclick") = "checkAllCheckboxes('" & chkProcessAll.ClientID & "','" & gvFlexiFields.MasterTableView.ClientID & "','0');"
        End If
    End Sub

    Protected Sub gvFlexiFields_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvFlexiFields.NeedDataSource
        Try
            LoadgvFlexiFields(False)
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvFlexiFields_PageIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridPageChangedEventArgs) Handles gvFlexiFields.PageIndexChanged
        gvFlexiFields.CurrentPageIndex = e.NewPageIndex
    End Sub
    Private Function CheckgvFlexiFieldsForProcessAll() As Boolean
        Try
            Dim rowgvFlexiFields As GridDataItem
            Dim chkSelect As CheckBox
            Dim Result As Boolean = False
            For Each rowgvFlexiFields In gvFlexiFields.Items
                chkSelect = New CheckBox
                chkSelect = DirectCast(rowgvFlexiFields.Cells(0).FindControl("chkSelect"), CheckBox)
                If chkSelect.Checked = True Then
                    Result = True
                    Exit For
                End If
            Next
            Return Result
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Function

    Protected Sub btnBulkDeleteFlexiFields_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBulkDeleteFlexiFields.Click
        Try
            Dim rowgvFlexiField As GridDataItem
            Dim chkSelect As CheckBox
            Dim FieldID As String
            Dim PassCount As Integer = 0
            Dim FailCount As Integer = 0

            If CheckgvFlexiFieldsForProcessAll() = True Then
                For Each rowgvFlexiField In gvFlexiFields.Items
                    chkSelect = New CheckBox
                    chkSelect = DirectCast(rowgvFlexiField.Cells(0).FindControl("chkSelect"), CheckBox)
                    If chkSelect.Checked = True Then
                        FieldID = rowgvFlexiField.GetDataKeyValue("FieldID").ToString()
                        Try
                            DeleteFlexiField(FieldID.ToString())
                            PassCount = PassCount + 1
                        Catch ex As Exception
                            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                                FailCount = FailCount + 1
                            End If
                        End Try
                    End If
                Next
                If PassCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Flexi Fields", "[" & FailCount.ToString() & "] Flexi Fields can not be deleted due to following reasons : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Failure, NavigateURL())
                    Exit Sub
                End If
                If FailCount = 0 Then
                    UIUtilities.ShowDialog(Me, "Flexi Fields", "[" & PassCount.ToString() & "] Flexi Fields deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    Exit Sub
                End If
                UIUtilities.ShowDialog(Me, "Flexi Fields", "[" & PassCount.ToString() & "] Flexi Fields deleted successfully. [" & FailCount.ToString() & "] Flexi Fields can not be deleted due to following reasons : <br /> 1. Associated records are present.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
            Else
                UIUtilities.ShowDialog(Me, "Flexi Fields", "Please select atleast one(1) Flexi Field.", ICBO.IC.Dialogmessagetype.Warning)
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub
End Class
