﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Security.Permissions
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports Telerik.Web.UI

Partial Class DesktopModules_User_Limits_ViewUserLimits
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private AccntNumber As String
    Private BranchCode As String
    Private Currency As String
    Private PayNatureCode As String
    Private UserType As String
    Private TemplateID As String
    Private htRights As Hashtable

#Region "Page Load"

    Protected Sub DesktopModules_User_Limits_ViewUserLimits_Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            TemplateID = Request.QueryString("FileUploadTemplateCode").ToString()
            AccntNumber = Request.QueryString("AccountNumber").ToString()
            BranchCode = Request.QueryString("BranchCode").ToString()
            Currency = Request.QueryString("Currency").ToString()
            PayNatureCode = Request.QueryString("PaymentNatureCode").ToString()
            UserType = Request.QueryString("UserType").ToString()


            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing


                If AccntNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" And PayNatureCode.ToString = "0" And TemplateID.ToString = "0" Then
                    Clear()
                    lblPageHeader.Text = "Add Email Settings"
                    savebtn.Text = "Save"
                    savebtn.Visible = CBool(htRights("Add"))
                    lblAssignedUsers.Visible = False
                    lblUsers.Visible = False
                    lblUsersRequired.Visible = False
                    pnlUsers.Visible = False

                Else

                    lblPageHeader.Text = "Edit Email Settings"
                    savebtn.Text = "Update"
                    savebtn.Visible = CBool(htRights("Update"))
                    lblAssignedUsers.Visible = False
                    lblUsers.Visible = False
                    lblUsersRequired.Visible = False
                    pnlUsers.Visible = False


                    ddlUserType.Enabled = False
                    ddlGroup.Enabled = False
                    ddlCompany.Enabled = False
                    ddlAccPNature.Enabled = False
                    ddlFileUploadTemplate.Enabled = False


                    Dim ObjICEmail As New ICEmailSettings
                    Dim objICACCPaymentNature As New ICAccountsPaymentNature
                    Dim objICAccounts As New ICAccounts
                    Dim objICCompany As New ICCompany
                    Dim objICUser As New ICUser
                    Dim objICGroup As New ICGroup
                    Dim emailSettingID As Integer = 0
                    objICCompany.es.Connection.CommandTimeout = 3600
                    objICAccounts.es.Connection.CommandTimeout = 3600
                    ObjICEmail.es.Connection.CommandTimeout = 3600
                    objICUser.es.Connection.CommandTimeout = 3600
                    objICGroup.es.Connection.CommandTimeout = 3600


                    emailSettingID = ICEmailProcessingController.GetICEmailSettingSingleObjectForUpdate(AccntNumber, BranchCode, Currency, PayNatureCode, TemplateID, UserType)
                    ObjICEmail.LoadByPrimaryKey(emailSettingID)
                    objICACCPaymentNature = ObjICEmail.UpToICAccountsPaymentNatureByAccountNumber
                    objICAccounts = objICACCPaymentNature.UpToICAccountsByAccountNumber
                    objICCompany = objICAccounts.UpToICCompanyByCompanyCode
                    objICGroup = objICCompany.UpToICGroupByGroupCode
                    objICUser = ObjICEmail.UpToICUserByUserID

                    ddlUserType.SelectedValue = objICUser.UserType
                    LoadddlGroup()
                    ddlGroup.SelectedValue = objICGroup.GroupCode
                    LoadddlCompany()
                    ddlCompany.SelectedValue = objICCompany.CompanyCode
                    LoadddlAccountPaymentNature()
                    ddlAccPNature.SelectedValue = AccntNumber & "-" & BranchCode & "-" & Currency & "-" & PayNatureCode
                    LoadddlFileUploadTemplate()
                    ddlFileUploadTemplate.SelectedValue = ObjICEmail.FileUploadTemplateCode


                    If ddlUserType.SelectedItem.Text = "Bank User" Then
                        LoadChkBoxListforUsers()
                        LoadgvAssignedUsers(True)
                    ElseIf ddlUserType.SelectedItem.Text = "Client User" Then
                        LoadChkBoxListforUsers()
                        LoadgvAssignedUsers(True)
                    End If


                End If

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub


    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Email Processing Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        ddlUserType.ClearSelection()
        LoadddlGroup()
        LoadddlCompany()
        LoadddlAccountPaymentNature()
        LoadddlFileUploadTemplate()

        pnlUsers.Visible = False
        lblUsers.Visible = False
        lblUsersRequired.Visible = False
        lblAssignedUsers.Visible = False
        gvAssignedUsers.Visible = False

    End Sub

#End Region


#Region "Button"

    Protected Sub savebtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles savebtn.Click
        If Page.IsValid Then
            Try
                Dim objICEmail As New ICEmailSettings
                Dim lit As New ListItem
                objICEmail.es.Connection.CommandTimeout = 3600

                objICEmail.AccountNumber = ddlAccPNature.SelectedValue.Split("-")(0).ToString()
                objICEmail.BranchCode = ddlAccPNature.SelectedValue.Split("-")(1).ToString()
                objICEmail.Currency = ddlAccPNature.SelectedValue.Split("-")(2).ToString()
                objICEmail.PaymentNatureCode = ddlAccPNature.SelectedValue.Split("-")(3).ToString()
                objICEmail.GroupCode = ddlGroup.SelectedValue.ToString()
                '  objICEmail.UserID = chkListUsers.SelectedValue.ToString
                objICEmail.FileUploadTemplateCode = ddlFileUploadTemplate.SelectedValue.ToString()

                If AccntNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" And PayNatureCode.ToString = "0" And TemplateID.ToString = "0" Then
                    objICEmail.CreatedBy = Me.UserId
                    objICEmail.CreatedDate = Date.Now
                    objICEmail.Creater = Me.UserId
                    objICEmail.CreationDate = Date.Now
                    For Each lit In chkListUsers.Items
                        If lit.Selected = True Then
                            objICEmail.UserID = lit.Value
                            ICEmailProcessingController.AddEmailSettings(objICEmail, False, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        End If
                    Next

                    UIUtilities.ShowDialog(Me, "Save Email Processing", "Email Processing added successfully.", ICBO.IC.Dialogmessagetype.Success)
                Else
                    objICEmail.CreatedBy = Me.UserId
                    objICEmail.CreatedDate = Date.Now
                    For Each lit In chkListUsers.Items
                        If lit.Selected = True Then
                            objICEmail.UserID = lit.Value
                            ICEmailProcessingController.AddEmailSettings(objICEmail, False, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                        End If
                    Next
                    UIUtilities.ShowDialog(Me, "Save Email Processing", "Email Processing updated successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())

                End If
                Clear()
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect(NavigateURL(), False)
    End Sub

#End Region

#Region "Drop down"

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            If ddlUserType.SelectedValue = "0" Then
                lit.Value = "0"
                lit.Text = "-- Please Select --"
                ddlGroup.Items.Clear()
                ddlGroup.Items.Add(lit)
            Else
                lit.Value = "0"
                lit.Text = "-- Please Select --"
                ddlGroup.Items.Clear()
                ddlGroup.Items.Add(lit)
                ddlGroup.AppendDataBoundItems = True
                ddlGroup.DataSource = ICGroupController.GetAllActiveAndApproveGroups()
                ddlGroup.DataTextField = "GroupName"
                ddlGroup.DataValueField = "GroupCode"
                ddlGroup.DataBind()
            End If


        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetAllActiveCompaniesByGroupCode(ddlGroup.SelectedValue.ToString)
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try

            If ddlGroup.SelectedValue = "0" Then
                lblUsers.Visible = False
                lblUsersRequired.Visible = False
                pnlUsers.Visible = False
                rfvUserChkBoxList.Enabled = False

            ElseIf ddlUserType.SelectedValue.ToString() = "0" Then
                Clear()
                'rfvUserChkBoxList.Enabled = False

            ElseIf ddlUserType.SelectedValue.ToString = "Bank User" Then
                LoadddlCompany()
                LoadddlAccountPaymentNature()
                LoadddlFileUploadTemplate()

            ElseIf ddlUserType.SelectedValue.ToString = "Client User" Then
                LoadChkBoxListforUsers()
                LoadddlCompany()
                LoadddlAccountPaymentNature()
                LoadddlFileUploadTemplate()

            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub


    Protected Sub ddlUserType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlUserType.SelectedIndexChanged
        Try
            If ddlUserType.SelectedValue = "Bank User" Then
                LoadChkBoxListforUsers()
                LoadddlGroup()


                LoadddlCompany()
                LoadddlAccountPaymentNature()
                LoadddlFileUploadTemplate()

            ElseIf ddlUserType.SelectedValue = "Client User" Then
                lblUsers.Visible = False
                lblUsersRequired.Visible = False
                pnlUsers.Visible = False
                LoadddlGroup()
                LoadChkBoxListforUsers()

                LoadddlCompany()
                LoadddlAccountPaymentNature()
                LoadddlFileUploadTemplate()

            ElseIf ddlUserType.SelectedValue = "0" Then
                Clear()
                rfvUserChkBoxList.Enabled = False
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Private Sub LoadChkBoxListforUsers()
        Try
            If ddlUserType.SelectedValue = "Client User" Then
                chkListUsers.DataSource = ICUserController.GetAllUsersByUserGroupCode(ddlGroup.SelectedValue.ToString(), "Client User", "EmailSettings", AccntNumber, BranchCode, Currency, PayNatureCode, TemplateID)
                chkListUsers.DataTextField = "UserName"
                chkListUsers.DataValueField = "UserID"
                chkListUsers.DataBind()
                If chkListUsers.Items.Count > 0 Then
                    lblUsers.Visible = True
                    lblUsersRequired.Visible = True
                    pnlUsers.Visible = True

                    If AccntNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" And PayNatureCode.ToString = "0" And TemplateID.ToString = "0" Then
                        rfvUserChkBoxList.Enabled = True
                        lblUsersRequired.Visible = True
                    Else
                        rfvUserChkBoxList.Enabled = False
                        lblUsersRequired.Visible = False
                    End If
                End If

            ElseIf ddlUserType.SelectedValue = "Bank User" Then
                chkListUsers.DataSource = ICUserController.GetAllActiveAndApproveUsersOfPrincipalBank(AccntNumber, BranchCode, Currency, PayNatureCode, TemplateID)
                chkListUsers.DataTextField = "UserName"
                chkListUsers.DataValueField = "UserID"
                chkListUsers.DataBind()
                If chkListUsers.Items.Count > 0 Then
                    lblUsers.Visible = True
                    lblUsersRequired.Visible = True
                    pnlUsers.Visible = True

                    If AccntNumber.ToString() = "0" And BranchCode.ToString() = "0" And Currency.ToString() = "0" And PayNatureCode.ToString = "0" And TemplateID.ToString = "0" Then
                        rfvUserChkBoxList.Enabled = True
                        lblUsersRequired.Visible = True
                    Else
                        rfvUserChkBoxList.Enabled = False
                        lblUsersRequired.Visible = False
                    End If

                End If
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlAccPNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAccPNature.SelectedIndexChanged
        Try
            LoadddlFileUploadTemplate()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try

            If ddlCompany.SelectedValue.ToString() = "0" Then
                LoadddlAccountPaymentNature()
                LoadddlFileUploadTemplate()
            Else
                LoadddlAccountPaymentNature()
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlAccountPaymentNature()
        Try

            Dim lit As New ListItem
            Dim dr As DataRow

            Dim dtICAccountPayNature As New DataTable

            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlAccPNature.Items.Clear()
            ddlAccPNature.Items.Add(lit)

            dtICAccountPayNature = ICAccountPaymentNatureController.GetAccountPaymentNatureByCompanyCode(ddlCompany.SelectedValue.ToString())

            For Each dr In dtICAccountPayNature.Rows
                lit = New ListItem

                lit.Value = dr("AccountPaymentNature").ToString
                lit.Text = dr("AccountAndPaymentNature").ToString


                ddlAccPNature.Items.Add(lit)
            Next

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlFileUploadTemplate()
        Try
            Dim lit As New ListItem
            Dim dr As DataRow
            Dim dtICFileUploadTemp As New DataTable

            lit.Value = "0"
            lit.Text = "-- Please Select --"
            ddlFileUploadTemplate.Items.Clear()
            ddlFileUploadTemplate.Items.Add(lit)
            If ddlAccPNature.SelectedValue.ToString <> "0" Then

                dtICFileUploadTemp = ICAccountPaymentNatureController.GetFileUploadTemplateByAccountPaymentNatureForSaveEmailSettings(ddlAccPNature.SelectedValue.ToString(), AccntNumber, BranchCode, Currency, PayNatureCode, TemplateID, ddlUserType.SelectedValue.ToString())

                For Each dr In dtICFileUploadTemp.Rows
                    lit = New ListItem

                    lit.Value = dr("TemplateID").ToString
                    lit.Text = dr("TemplateName").ToString

                    ddlFileUploadTemplate.Items.Add(lit)
                Next
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Private Sub LoadgvAssignedUsers(ByVal IsBind As Boolean)
        Try
            Dim objICAPNature As New ICAccountsPaymentNature
            objICAPNature.es.Connection.CommandTimeout = 3600


            objICAPNature.LoadByPrimaryKey(ddlAccPNature.SelectedValue.Split("-")(0).ToString, ddlAccPNature.SelectedValue.Split("-")(1).ToString, ddlAccPNature.SelectedValue.Split("-")(2).ToString, ddlAccPNature.SelectedValue.Split("-")(3).ToString)

            ICEmailProcessingController.GetgvAssignedUsers(ddlUserType.SelectedValue.ToString, gvAssignedUsers, IsBind, objICAPNature, ddlGroup.SelectedValue.ToString, ddlFileUploadTemplate.SelectedValue.ToString)
            If gvAssignedUsers.Items.Count > 0 Then
                lblAssignedUsers.Visible = True
                gvAssignedUsers.Visible = True
                gvAssignedUsers.Columns(1).Visible = CBool(htRights("Delete"))
                'SetCustomizedJavaScriptFunction()
            Else
                lblAssignedUsers.Visible = False
                gvAssignedUsers.Visible = False
                gvAssignedUsers.Columns(1).Visible = CBool(htRights("Delete"))

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

#End Region

#Region "Grid View"

    Protected Sub gvAssignedUsers_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvAssignedUsers.ItemCommand
        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    'If gvAssignedUsers.Items.Count = 1 Then
                    'Dim s As String() = Nothing
                    'Dim ID As String() = Nothing
                    's = ddlAccPNature.SelectedValue.ToString().Split("-")
                    'ID = e.CommandArgument.ToString().Split(";")
                    'Dim objEmailSetting As New ICEmailSettings
                    '  Dim objICEmailColl As New ICEmailSettingsCollection
                    'Dim dt As DataTable

                    'dt = ICEmailProcessingController.GetEmailSettings(s(0).ToString(), s(1).ToString(), s(2).ToString(), s(3).ToString(), ddlFileUploadTemplate.SelectedValue.ToString, ddlUserType.SelectedValue.ToString, ID(0).ToString())

                    'If dt.Rows.Count > 0 Then
                    '    For Each dr As DataRow In dt.Rows
                    '        objEmailSetting = New ICEmailSettings
                    '        If objEmailSetting.LoadByPrimaryKey(dr(0).ToString) Then
                    ICEmailProcessingController.DeleteEmailSettings(e.CommandArgument.ToString(), Me.UserId.ToString, Me.UserInfo.Username.ToString)
                    '  LoadgvAssignedUsers(True)

                    LoadChkBoxListforUsers()



                    'End If
                    '        Next
                    If gvAssignedUsers.Items.Count = 1 Then
                        UIUtilities.ShowDialog(Me, "Delete Email Processing", "Email Setting deleted successfully.", ICBO.IC.Dialogmessagetype.Success, NavigateURL())
                    Else
                        UIUtilities.ShowDialog(Me, "Delete Email Processing", "Email Setting User deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                        LoadgvAssignedUsers(True)
                    End If

                    '  End If



                    'Else
                    'Dim objAssignedUsers As New ICEmailSettings
                    'objAssignedUsers.es.Connection.CommandTimeout = 3600
                    'objAssignedUsers.LoadByPrimaryKey(ID(1).ToString())
                    'ICEmailProcessingController.DeleteEmailSettings(objAssignedUsers, Me.UserId.ToString(), Me.UserInfo.Username.ToString())
                    'UIUtilities.ShowDialog(Me, "Delete Email Processing", "Assigned User deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                    'LoadgvAssignedUsers(True)
                    'LoadChkBoxListforUsers()
                    'End If

                End If
            End If

        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Deleted Email Processing", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

  
    Protected Sub gvAssignedUsers_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvAssignedUsers.NeedDataSource
        LoadgvAssignedUsers(False)
    End Sub

#End Region
    Private Sub SetCustomizedJavaScriptFunction()
        Dim btnDelete As ImageButton
        For Each gvAssignedRows As GridDataItem In gvAssignedUsers.Items
            If CInt(gvAssignedRows.GetDataKeyValue("Count")) = 1 Then
                btnDelete = New ImageButton
                btnDelete = DirectCast(gvAssignedRows.Cells(1).FindControl("IbtnDelete"), ImageButton)
                'btnDelete.OnClientClick = ""
                btnDelete.Attributes("onclick") = "javascript: return delcon();"
            End If
        Next
    End Sub
    Protected Sub gvAssignedUsers_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvAssignedUsers.ItemDataBound

        Dim btnDelete As ImageButton
        Dim objAssignedUser As New ICEmailSettings


        If TypeOf e.Item Is GridDataItem Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Dim value As Integer = CInt(item.GetDataKeyValue("Count"))
           
            If value = 1 Then
                btnDelete = New ImageButton
                btnDelete = DirectCast(e.Item.Cells(1).FindControl("IbtnDelete"), ImageButton)
                btnDelete.OnClientClick = Nothing
                btnDelete.Attributes("onclick") = "javascript: return delcon();"
            Else
                btnDelete = New ImageButton
                btnDelete = DirectCast(e.Item.Cells(1).FindControl("IbtnDelete"), ImageButton)
                btnDelete.OnClientClick = Nothing
                btnDelete.Attributes("onclick") = "javascript: return con();"
            End If
        End If


        'If e.Item.ItemType = Telerik.Web.UI.GridDataItem Then
        '    'For Each gvAssignedRows As GridDataItem In gvAssignedUsers.Items
        '    If CInt(e.Item.Cells(3).Text) = 1 Then

        '    End If
        '    'Next
        'End If
    End Sub


End Class
