﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ViewEmailProcessingManagement.ascx.vb"
    Inherits="DesktopModules_EmailProcessingManagement_ViewEmailProcessingManagement" %>
<style type="text/css">
    .btn
    {
        margin-left: 0px;
    }
    .RadGrid_Default
    {
        font: 12px/16px "segoe ui" ,arial,sans-serif;
    }
    .RadGrid_Default
    {
        border: 1px solid #828282;
        background: #fff;
        color: #333;
    }
    .RadGrid_Default .rgMasterTable
    {
        font: 12px/16px "segoe ui" ,arial,sans-serif;
    }
    .RadGrid .rgMasterTable
    {
        border-collapse: separate;
        border-spacing: 0;
    }
    .RadGrid_Default .rgHeader
    {
        color: #333;
    }
    .RadGrid_Default .rgHeader
    {
        border: 0;
        border-bottom: 1px solid #828282;
        background: #eaeaea 0 -2300px repeat-x url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
    }
    .RadGrid .rgHeader
    {
        padding-top: 5px;
        padding-bottom: 4px;
        text-align: left;
        font-weight: normal;
    }
    .RadGrid .rgHeader
    {
        padding-left: 7px;
        padding-right: 7px;
    }
    .RadGrid .rgHeader
    {
        cursor: default;
    }
    .RadGrid_Default .rgHeader a
    {
        color: #333;
    }
    .RadGrid .rgHeader a
    {
        text-decoration: none;
    }
    .RadGrid_Default .rgFooter
    {
        background: #eee;
    }
</style>
<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon(item) {
        if (window.confirm("Are you sure you wish to remove this " + item + "?") == true) {
            return true;
        }
        else {
            return false;
        }
    }    
</script>
<!--
<script type="text/javascript">
    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }
            }
        }
    }
</script>
 
            -->
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2" style="width: 50%">
            <asp:Label ID="lblPageHeader" runat="server" Text="Email Setting List" CssClass="headingblue"></asp:Label>
            <br />
        </td>
        <td align="left" valign="top" colspan="2" style="width: 50%">
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2" style="width: 50%">
            &nbsp;
        </td>
        <td align="left" valign="top" colspan="2" style="width: 50%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Button ID="btnAddEmailSettings" runat="server" CausesValidation="False" CssClass="btn"
                Text="Add Email Setting" />
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="Label2" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True" CssClass="dropdown">
                <asp:ListItem Value="0">-- All --</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" CssClass="dropdown">
                <asp:ListItem Value="0">-- All --</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblSelectAccountPNature" runat="server" Text="Select Account Payment Nature"
                CssClass="lbl"></asp:Label>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                <asp:Label ID="lblFileUploadTemplateCode" runat="server" Text="Select File Upload Template"
                    CssClass="lbl"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlAccPNature" runat="server" CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem Value="0">-- All --</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlFileUploadTemplate" runat="server" AutoPostBack="True" CssClass="dropdown">
                <asp:ListItem Value="0">-- All --</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;<td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;<td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <telerik:RadGrid ID="gvEmailSettingTaggedUsers" runat="server" AllowPaging="True"
                AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
                PageSize="100">
                 <ClientSettings>
                            <Scrolling UseStaticHeaders="true" />
                            </ClientSettings>
                <AlternatingItemStyle CssClass="rgAltRow" />
                <MasterTableView NoMasterRecordsText="" DataKeyNames ="UserType" TableLayout="Fixed">
                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn DataField="GroupName" HeaderText="GroupName" SortExpression="GroupName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AccountNumber" HeaderText="Account Number" SortExpression="AccountNumber">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PaymentNatureCode" HeaderText="Payment Nature Code"
                            SortExpression="PaymentNatureCode">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PaymentNatureName" HeaderText="Payment Nature Name"
                            SortExpression="PaymentNatureName">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="TemplateName" HeaderText="Template Name" SortExpression="TemplateName">
                        </telerik:GridBoundColumn>                     
                        <telerik:GridBoundColumn DataField="UserType" HeaderText="User Type" SortExpression="UserType">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn>
                            <HeaderTemplate>
                                <asp:Label ID="Edit" runat="server" Text="Edit"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HyperLink ID="hlEdit" runat="server" ImageUrl="~/images/edit.gif" NavigateUrl='<%#NavigateURL("SaveEmailSettings", "mid=" & Me.ModuleId & "&AccountNumber=" & Eval("AccountNumber") & "&BranchCode=" & Eval("BranchCode") & "&Currency=" & Eval("Currency") & "&PaymentNatureCode=" & Eval("PaymentNatureCode") & "&FileUploadTemplateCode=" & Eval("FileUploadTemplateCode") & "&UserType=" & Eval("UserType"))%>'
                                    ToolTip="Edit">Edit</asp:HyperLink>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn>
                            <HeaderTemplate>
                                <asp:Label ID="Delete" runat="server" Text="Delete"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("AccountNumber").ToString + ";" +Eval("BranchCode").ToString + ";" +Eval("Currency").ToString  + ";" +Eval("PaymentNatureCode").ToString  + ";" +Eval("FileUploadTemplateCode").ToString + ";" +Eval("UserType").ToString%>'
                                    CommandName="del" ImageUrl="~/images/delete.gif" OnClientClick="javascript: return con();"
                                    ToolTip="Delete" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                    <PagerStyle AlwaysVisible="True" />
                </MasterTableView>
                <ItemStyle CssClass="rgRow" />
                <PagerStyle AlwaysVisible="True" />
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            <asp:Label ID="lblRNF" runat="server" Text="Record Not Found." CssClass="headingblue"
                Visible="false"></asp:Label>
        </td>
    </tr>
</table>
