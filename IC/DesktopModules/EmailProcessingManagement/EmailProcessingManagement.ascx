﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EmailProcessingManagement.ascx.vb"
    Inherits="DesktopModules_User_Limits_ViewUserLimits" %>
<style type="text/css">
    .btn
    {
        margin-left: 0px;
    }
    .RadGrid_Default
    {
        font: 12px/16px "segoe ui" ,arial,sans-serif;
    }
    .RadGrid_Default
    {
        border: 1px solid #828282;
        background: #fff;
        color: #333;
    }
    .RadGrid_Default .rgMasterTable
    {
        font: 12px/16px "segoe ui" ,arial,sans-serif;
    }
    .RadGrid .rgMasterTable
    {
        border-collapse: separate;
        border-spacing: 0;
    }
    .RadGrid_Default .rgHeader
    {
        color: #333;
    }
    .RadGrid_Default .rgHeader
    {
        border: 0;
        border-bottom: 1px solid #828282;
        background: #eaeaea 0 -2300px repeat-x url('mvwres://Telerik.Web.UI, Version=2012.2.607.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
    }
    .RadGrid .rgHeader
    {
        padding-top: 5px;
        padding-bottom: 4px;
        text-align: left;
        font-weight: normal;
    }
    .RadGrid .rgHeader
    {
        padding-left: 7px;
        padding-right: 7px;
    }
    .RadGrid .rgHeader
    {
        cursor: default;
    }
    .RadGrid_Default .rgHeader a
    {
        color: #333;
    }
    .RadGrid .rgHeader a
    {
        text-decoration: none;
    }
    .RadGrid_Default .rgFooter
    {
        background: #eee;
    }
</style>
<script>
    function ValidateChkList(source, arguments) {

        arguments.IsValid = IsCheckBoxChecked() ? true : false;



    }



    function IsCheckBoxChecked() {

        var isChecked = false;

        var list = document.getElementById('<%= chkListUsers.ClientID %>');

        if (list != null) {

            for (var i = 0; i < list.rows.length; i++) {
                for (var j = 0; j < list.rows[i].cells.length; j++) {
                    var listControl = list.rows[i].cells[j].childNodes[0];
                    if (listControl.checked) {
                        isChecked = true;
                    }
                }
            }

        }

        return isChecked;


    }
</script>

<script type="text/javascript">
    function con() {
        if (window.confirm("Are you sure you wish to remove this Record?") == true) {
            return true;
        }
        else {
            return false;
        }
    }

    function delcon() {
        if (window.confirm("This is the last Assigned user of Email Settings , if you delete that user the whole Email setting will be deleted.") == true) {
            return true;
        }
        else {
            return false;
        }
    }    
</script>
<script language="javascript" type="text/javascript">
    function AdjustWidth(ddl) {
        var maxWidth = 0;
        var Counter = 0;
        for (var i = 0; i < ddl.length; i++) {
            if (ddl.options[i].text.length > maxWidth) {
                Counter = Counter + 1;
                maxWidth = ddl.options[i].text.length;
            }
        }
        if (Counter = 1) { ddl.style.width = "250px"; }

        else {
            alert(Counter);
            ddl.style.width = maxWidth + "px";
        }
        //-->
    }
</script>
<style type="text/css">
.ctrDropDown{
    width:250px;
   
}
.ctrDropDownClick{
    
    width:600px;
 
}
.plainDropDown{
    width:250px;
   
}
</style>
<table width="100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2" style="width: 50%">
            <asp:Label ID="lblPageHeader" runat="server" Text="Edit Email Settings" CssClass="headingblue"></asp:Label>
            <br />
            Note:&nbsp; Fields marked as
            <asp:Label ID="Label1" runat="server" Text="*" ForeColor="Red"></asp:Label>
            &nbsp;are required.
        </td>
        <td align="left" valign="top" colspan="2" style="width: 50%">
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="2" style="width: 50%">
            &nbsp;
        </td>
        <td align="left" valign="top" colspan="2" style="width: 50%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="Label4" runat="server" Text="Select User Type" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblUsrTypeRequired" runat="server" Text="*" ForeColor="Red"></asp:Label>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlUserType" runat="server" AutoPostBack="True" CssClass="dropdown">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                <asp:ListItem>Bank User</asp:ListItem>
                <asp:ListItem>Client User</asp:ListItem>
            </asp:DropDownList>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvUserType" runat="server" ControlToValidate="ddlUserType"
                CssClass="lblEror" Display="Dynamic" 
                ErrorMessage="Please select User Type" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="Label2" runat="server" Text="Select Group" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblGroupRequired" runat="server" Text="*" ForeColor="Red"></asp:Label>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
        </td>
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblCompany" runat="server" Text="Select Company" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblCompanyRequired" runat="server" Text="*" ForeColor="Red"></asp:Label>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:DropDownList ID="ddlGroup" runat="server" AutoPostBack="True" CssClass="dropdown">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
            </asp:DropDownList>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" CssClass="dropdown">
                    <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvGroup" runat="server" ControlToValidate="ddlGroup"
                CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Group" SetFocusOnError="True"
                ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                <asp:RequiredFieldValidator ID="rfvCompany" runat="server" ControlToValidate="ddlCompany"
                    CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select Company" SetFocusOnError="True"
                    ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblSelectAccountPNature" runat="server" Text="Select Account Payment Nature"
                CssClass="lbl"></asp:Label>
            <asp:Label ID="lblAccPNatureRequired" runat="server" Text="*" ForeColor="Red"></asp:Label>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                <asp:Label ID="lblFileUploadTemplateCode" runat="server" Text="Select File Upload Template"
                    CssClass="lbl"></asp:Label>
                <asp:Label ID="lblFileUploadTemplateRequired" runat="server" Text="*" 
                    ForeColor="Red"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <%--<asp:DropDownList ID="ddlAccPNature" runat="server" CssClass="dropdown" AutoPostBack="True">
                <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
            </asp:DropDownList>--%> <div style= "width:250px; overflow:hidden;">
            <asp:DropDownList ID="ddlAccPNature" runat="server" class="ctrDropDown"  onblur="this.className='ctrDropDown';" onmousedown="this.className='ctrDropDownClick';" onchange="this.className='ctrDropDown';" AutoPostBack="true">
            </asp:DropDownList>
            </div></td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                <asp:DropDownList ID="ddlFileUploadTemplate" runat="server" AutoPostBack="True" CssClass="dropdown">
                    <asp:ListItem Value="0">-- Please Select --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:RequiredFieldValidator ID="rfvddlAccPNature" runat="server" ControlToValidate="ddlAccPNature"
                Display="Dynamic" ErrorMessage="Please select Account Payment Nature" InitialValue="0"
                SetFocusOnError="True"></asp:RequiredFieldValidator>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                <asp:RequiredFieldValidator ID="rfvFileUploadTemp" runat="server" ControlToValidate="ddlFileUploadTemplate"
                    CssClass="lblEror" Display="Dynamic" ErrorMessage="Please select File Upload Template"
                    SetFocusOnError="True" ToolTip="Required Field" InitialValue="0"></asp:RequiredFieldValidator>
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:Label ID="lblUsers" runat="server" Text="Select Users" CssClass="lbl"></asp:Label>
            <asp:Label ID="lblUsersRequired" runat="server" Text="*" ForeColor="Red"></asp:Label>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
            <td align="left" valign="top" style="width: 25%">
                &nbsp;
            </td>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" colspan="4">
            <asp:Panel ID="pnlUsers" runat="server" ScrollBars="Vertical" Width="328px" Height="200">
                <asp:CheckBoxList ID="chkListUsers" runat="server" CssClass="dropdown">
                </asp:CheckBoxList>
            </asp:Panel>
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            <asp:CustomValidator ID="rfvUserChkBoxList" ClientValidationFunction="ValidateChkList"
                runat="server" Display="Dynamic" ErrorMessage="Please select at least one User"
                SetFocusOnError="True" Enabled="False"></asp:CustomValidator>
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
        <td align="left" valign="top" style="width: 25%">
            &nbsp;
        </td>
    </tr>
    <tr align="left" valign="top" style="width: 100%">
        <td align="center" valign="top" colspan="4">
            &nbsp;<asp:Button ID="savebtn" runat="server" Text="Save" CssClass="btn" 
                Width="74px" />
            &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnCancel" 
                Width="75px" CausesValidation="False" />
    </tr>
</table>
<table width="100%">
    <tr align="center" valign="top" style="width: 100%">
        <td align="left" valign="top">
            &nbsp;
        </td>
    </tr>
    <tr align="center" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <asp:Label ID="lblAssignedUsers" runat="server" Text="Assigned User(s) List" 
                CssClass="lbl"></asp:Label>
        </td>
    </tr>
    <tr align="center" valign="top" style="width: 100%">
        <td align="left" valign="top">
            <telerik:RadGrid ID="gvAssignedUsers" runat="server" AllowPaging="True" AllowSorting="True"
                AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="100">
                 <ClientSettings>
                            <Scrolling UseStaticHeaders="true" />
                            </ClientSettings>
                <AlternatingItemStyle CssClass="rgAltRow" />
                <MasterTableView DataKeyNames ="Count" TableLayout="Fixed">
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn DataField="UserName" HeaderText="Name" SortExpression="UserName">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-Width="30px">
                            <HeaderTemplate>
                                <asp:Label ID="lblDelete" runat="server" Text="Delete"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:ImageButton ID="IbtnDelete" runat="server" CommandArgument='<%#Eval("EmailIDSettingsID").ToString %>'
                                    CommandName="del" ImageUrl="~/images/delete.gif" 
                                    ToolTip="Delete" />
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
                <ItemStyle CssClass="rgRow" />
            </telerik:RadGrid>
        </td>
    </tr>
    <tr align="center" valign="top" style="width: 100%">
        <td align="center" valign="top">
            &nbsp;
        </td>
    </tr>
</table>
