﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Security.Permissions
Imports DotNetNuke.Entities.Portals
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports Telerik.Web.UI

Partial Class DesktopModules_EmailProcessingManagement_ViewEmailProcessingManagement
    Inherits DotNetNuke.Entities.Modules.PortalModuleBase
    Private htRights As Hashtable

#Region "Page Load"

    Protected Sub DesktopModules_EmailProcessingManagement_ViewEmailProcessingManagement_Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not ViewState("htRights") Is Nothing Then
                htRights = DirectCast(ViewState("htRights"), Hashtable)
            End If

            If Page.IsPostBack = False Then
                ViewState("htRights") = Nothing
                GetPageAccessRights()
                ViewState("SortExp") = Nothing
                btnAddEmailSettings.Visible = CBool(htRights("Add"))
                LoadddlGroup()
                LoadgvEmailSettingTaggedUsers(True)

            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString, ICBO.IC.Dialogmessagetype.Failure)
        End Try

    End Sub

    Private Sub GetPageAccessRights()
        Try
            htRights = ICUtilities.ValidateRightsOnRole(Me.PortalId.ToString(), Me.UserId.ToString(), "Email Processing Management")
            ViewState("htRights") = htRights
            If htRights("View") = False Then
                UIUtilities.ShowDialog(Me, "Warning", "Unauthorized Access.", ICBO.IC.Dialogmessagetype.Warning, NavigateURL(40))
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub Clear()
        ddlGroup.ClearSelection()
        ddlCompany.ClearSelection()
        ddlAccPNature.ClearSelection()
        ddlFileUploadTemplate.ClearSelection()

    End Sub

#End Region

#Region "Drop Down"

    Private Sub LoadddlGroup()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlGroup.Items.Clear()
            ddlGroup.Items.Add(lit)
            ddlGroup.AppendDataBoundItems = True
            ddlGroup.DataSource = ICGroupController.GetAllActiveAndApproveGroups()
            ddlGroup.DataTextField = "GroupName"
            ddlGroup.DataValueField = "GroupCode"
            ddlGroup.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlCompany()
        Try
            Dim lit As New ListItem
            lit.Value = "0"
            lit.Text = "-- All --"
            ddlCompany.Items.Clear()
            ddlCompany.Items.Add(lit)
            ddlCompany.AppendDataBoundItems = True
            ddlCompany.DataSource = ICCompanyController.GetCompanyActiveAndApproveByGroupCode(ddlGroup.SelectedValue.ToString)
            ddlCompany.DataTextField = "CompanyName"
            ddlCompany.DataValueField = "CompanyCode"
            ddlCompany.DataBind()

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlAccountPaymentNature()
        Try
            Dim lit As New ListItem
            Dim dtICAccountPayNature As New DataTable

            lit.Value = "0"
            lit.Text = "-- All --"
            ddlAccPNature.Items.Clear()
            ddlAccPNature.Items.Add(lit)
            lit.Selected = True
            ddlAccPNature.AppendDataBoundItems = True
            ddlAccPNature.DataSource = ICAccountPaymentNatureController.GetAccountPaymentNatureByCompanyCode(ddlCompany.SelectedValue.ToString)
            ddlAccPNature.DataTextField = "AccountAndPaymentNature"
            ddlAccPNature.DataValueField = "AccountPaymentNature"
            ddlAccPNature.DataBind()
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Private Sub LoadddlFileUploadTemplate()
        Try
            Dim lit As New ListItem
            Dim dr As DataRow
            Dim dtICFileUploadTemp As New DataTable

            lit.Value = "0"
            lit.Text = "-- All --"
            ddlFileUploadTemplate.Items.Clear()
            ddlFileUploadTemplate.Items.Add(lit)

            dtICFileUploadTemp = ICAccountPaymentNatureController.GetFileUploadTemplateByAccountPaymentNatureForViewEmailSettings(ddlAccPNature.SelectedValue.ToString())

            If dtICFileUploadTemp.Rows.Count = 0 Then
                Exit Sub
            Else
                For Each dr In dtICFileUploadTemp.Rows
                    lit = New ListItem

                    lit.Value = dr("TemplateID").ToString
                    lit.Text = dr("TemplateName").ToString

                    ddlFileUploadTemplate.Items.Add(lit)
                Next

            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            If ddlGroup.SelectedValue = "0" Then
                LoadgvEmailSettingTaggedUsers(True)
                LoadddlCompany()
                LoadddlAccountPaymentNature()
                LoadddlFileUploadTemplate()
                LoadgvEmailSettingTaggedUsers(True)
            Else
                LoadddlCompany()
                LoadddlAccountPaymentNature()
                LoadddlFileUploadTemplate()
                LoadgvEmailSettingTaggedUsers(True)
               
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            If ddlCompany.SelectedValue = "0" Then
                LoadddlAccountPaymentNature()
                LoadddlFileUploadTemplate()
                LoadgvEmailSettingTaggedUsers(True)

            Else
                LoadddlAccountPaymentNature()
                LoadddlFileUploadTemplate()
                LoadgvEmailSettingTaggedUsers(True)
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub


    Protected Sub ddlAccPNature_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAccPNature.SelectedIndexChanged
        Try
            If ddlAccPNature.SelectedValue = "0" Then
                LoadgvEmailSettingTaggedUsers(True)
            Else
                LoadddlFileUploadTemplate()
                LoadgvEmailSettingTaggedUsers(True)
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub ddlFileUploadTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFileUploadTemplate.SelectedIndexChanged
        Try
            If ddlFileUploadTemplate.SelectedValue = "0" Then
                LoadgvEmailSettingTaggedUsers(True)
            Else
                LoadgvEmailSettingTaggedUsers(True)
            End If

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

#Region "Button"

    Protected Sub btnAddEmailSettings_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddEmailSettings.Click
        Page.Validate()
        If Page.IsValid Then
            Try
                Response.Redirect(NavigateURL("SaveEmailSettings", "&mid=" & Me.ModuleId & "&AccountNumber=0" & "&BranchCode=0" & "&Currency=0" & "&PaymentNatureCode=0" & "&FileUploadTemplateCode=0" & "&UserType=0"), False)
            Catch ex As Exception
                ProcessModuleLoadException(Me, ex, False)
                UIUtilities.ShowDialog(Me, "Error", ex.Message.ToString(), ICBO.IC.Dialogmessagetype.Failure)
            End Try
        End If
    End Sub

#End Region

#Region "Grid View"

    Private Sub LoadgvEmailSettingTaggedUsers(ByVal IsBind As Boolean)
        Try
            ICEmailProcessingController.GetEmailSettingTaggedUsersgv(gvEmailSettingTaggedUsers.CurrentPageIndex + 1, gvEmailSettingTaggedUsers.PageSize, gvEmailSettingTaggedUsers, IsBind, ddlGroup.SelectedValue.ToString, ddlCompany.SelectedValue.ToString, ddlAccPNature.SelectedValue.ToString, ddlFileUploadTemplate.SelectedValue.ToString)

            If gvEmailSettingTaggedUsers.Items.Count > 0 Then
                gvEmailSettingTaggedUsers.Visible = True
                lblRNF.Visible = False
                gvEmailSettingTaggedUsers.Columns(8).Visible = CBool(htRights("Delete"))
            Else

                gvEmailSettingTaggedUsers.Visible = False
                lblRNF.Visible = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvEmailSettingTaggedUsers_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvEmailSettingTaggedUsers.NeedDataSource
        Try
            LoadgvEmailSettingTaggedUsers(False)

        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvEmailSettingTaggedUsers_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles gvEmailSettingTaggedUsers.ItemCommand
        Try
            If e.CommandName = "del" Then
                If Page.IsValid Then
                    Dim dt As New DataTable
                    Dim s As String() = Nothing
                    s = e.CommandArgument.ToString().Split(";")
                    dt = ICEmailProcessingController.GetEmailSettings(s(0).ToString, s(1).ToString, s(2).ToString, s(3).ToString, s(4).ToString, s(5).ToString)
                    If dt.Rows.Count > 0 Then
                        For Each dr As DataRow In dt.Rows
                            ICEmailProcessingController.DeleteEmailSettings(dr(0).ToString(), Me.UserId.ToString, Me.UserInfo.Username.ToString)
                        Next
                    End If

                    s = Nothing
                    UIUtilities.ShowDialog(Me, "Delete Email Processing", "Email Processing deleted successfully.", ICBO.IC.Dialogmessagetype.Success)
                    LoadgvEmailSettingTaggedUsers(True)
                End If
            End If
        Catch ex As Exception
            If ex.Message.ToString().Contains("The DELETE statement conflicted with the REFERENCE constraint") = True Then
                UIUtilities.ShowDialog(Me, "Deleted Email Processing", "Cannot delete record. Please delete associated record(s) first.", ICBO.IC.Dialogmessagetype.Failure)
                Exit Sub
            End If
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

    Protected Sub gvEmailSettingTaggedUsers_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles gvEmailSettingTaggedUsers.ItemCreated
        Try
            If TypeOf (e.Item) Is GridPagerItem Then
                Dim myPageSizeCombo As RadComboBox = e.Item.FindControl("PageSizeComboBox")
                myPageSizeCombo.Items.Clear()
                Dim arrPageSizes() As String = {"10", "25", "50", "100", "500"}
                For x As Integer = 0 To UBound(arrPageSizes)
                    Dim myRadComboBoxItem As New RadComboBoxItem(arrPageSizes(x))
                    myPageSizeCombo.Items.Add(myRadComboBoxItem)
                    myRadComboBoxItem.Attributes.Add("ownerTableViewId", gvEmailSettingTaggedUsers.MasterTableView.ClientID)
                Next
                myPageSizeCombo.FindItemByText(e.Item.OwnerTableView.PageSize.ToString()).Selected = True
            End If
        Catch ex As Exception
            ProcessModuleLoadException(Me, ex, False)
            UIUtilities.ShowDialog(Me, "Error", ex.Message, ICBO.IC.Dialogmessagetype.Failure)
        End Try
    End Sub

#End Region

End Class
