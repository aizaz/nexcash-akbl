﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ReIssuanceQueue.ascx.vb" Inherits="DesktopModules_ReIssuanceQueue_ReIssuanceQueue" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadInputManager ID="RadInputManager3" runat="server">
    <telerik:TextBoxSetting BehaviorID="reTnxCount" Validation-IsRequired="false" EmptyMessage=""
        ErrorMessage="Transaction Count">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTnxCount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reTotalAmount" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Total Amount">
        <TargetControls>
            <telerik:TargetInput ControlID="txtTotalAmount" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reCurrency" Validation-IsRequired="false" EmptyMessage=""
        ErrorMessage="Currency">
        <TargetControls>
            <telerik:TargetInput ControlID="txtCurrency" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="rePaymentMode" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Payment Mode">
        <TargetControls>
            <telerik:TargetInput ControlID="txtPaymentMode" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reAvailableBalance" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Available Balance">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAvailableBalance" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reInstructionNo" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Enter Instruction No." ValidationExpression="[0-9]{0,8}">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstructionNo" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reInstrumentNo" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Enter Instrument No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtInstrumentNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reReferenceNo" Validation-IsRequired="false"
        EmptyMessage="" ErrorMessage="Enter Reference No.">
        <TargetControls>
            <telerik:TargetInput ControlID="txtReferenceNo" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:TextBoxSetting BehaviorID="reBaneName" Validation-IsRequired="false" EmptyMessage=""
        ErrorMessage="Invalid Remarks" Validation-ValidationGroup="Remarks">
        <TargetControls>
            <telerik:TargetInput ControlID="txtRemarks" />
        </TargetControls>
    </telerik:TextBoxSetting>
    <telerik:RegExpTextBoxSetting BehaviorID="reAmount" Validation-IsRequired="false" EmptyMessage=""
        ErrorMessage="Enter Amount" ValidationExpression="[0-9]{0,8}">
        <TargetControls>
            <telerik:TargetInput ControlID="txtAmount" />
        </TargetControls>
    </telerik:RegExpTextBoxSetting>
</telerik:RadInputManager>

<script type="text/javascript">

    function conAllowReIssuance() {
        if (window.confirm("Are you sure you wish to allow re issuance instruction(s)?") == true) {
            var btnAllowReIssuance = document.getElementById('<%=btnAllowReIssuance.ClientID%>')
            btnAllowReIssuance.value = "Processing...";
            btnAllowReIssuance.disabled = true;

            var a = btnAllowReIssuance.name;
            __doPostBack(a, '');
            return true;
        }
        else {
            return false;
        }
    }

    function checkAllCheckboxes(idOfControllingCheckbox, gridViewClientID, CellNo) {
        var grid = document.getElementById(gridViewClientID);
        var cell;
        for (i = 0; i < grid.rows.length; i++) {
            cell = grid.rows[i].cells[CellNo];

            for (j = 0; j < cell.childNodes.length; j++) {

                if (cell.childNodes[j].childNodes.length > 0) {
                    //                        alert(cell.childNodes[j].childNodes[0].type)
                    if (cell.childNodes[j].childNodes[0].type == "checkbox") {
                        cell.childNodes[j].childNodes[0].checked = document.getElementById(idOfControllingCheckbox).checked;
                    }
                }




            }
        }

    }

    $(document).ready(function () {

        // Dialog
        $('#urldialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#loading').hide();



            },
            resizable: false
        });
        // Summary Dialog
        $('#sumdialog').dialog({
            autoOpen: false,
            width: 600,
            height: 600,
            open: function (type, data) {
                $(this).parent().appendTo("form");
            },
            close: function (event, ui) {
                $('#sumdialog').hide();
                //                window.location.reload();
                //                window.location.reload();sa
            },
            resizable: false
        });

    });
    function showsumdialog(title, modal, width, height) {
        //  alert(modal);
        $('#sumdialog').dialog("option", "title", title);
        $('#sumdialog').dialog("option", "modal", modal);
        $('#sumdialog').dialog("option", "width", width);
        $('#sumdialog').dialog("option", "height", height);
        $('#sumdialog').dialog('open');

        return false;

    };
    function CloseSummaryDialogBox() {
        $('#sumdialog').dialog('close');
    }
    function showurldialog(title, url, modal, width, height) {
        //  alert(modal);
        $('#urldialog').dialog("option", "title", title);
        $('#urldialog').dialog("option", "modal", modal);
        $('#urldialog').dialog("option", "width", width);
        $('#urldialog').dialog("option", "height", height);
        $('#urldialog').dialog('open');
        $('#dialogiframe').hide();
        $('#loading').show();
        $('#dialogiframe').attr("src", url);
        $('#dialogiframe').load(function () {
            $('#loading').hide();
            $('#dialogiframe').show();
        });
        return false;

    };

    function CloseIframe() {
        $('#urldialog').dialog('close');
    }



    function textboxMultilineMaxNumber(txt, maxLen) {
        try {
            if (txt.value.length > (maxLen - 1)) return false;
        } catch (e) {
        }
    }
</script>

<%--<asp:Label ID="lblscript" runat="server" Text=""></asp:Label>--%>
<script language="javascript" type="text/javascript">
    function AdjustWidth(ddl) {
        var maxWidth = 0;
        var Counter = 0;
        for (var i = 0; i < ddl.length; i++) {
            if (ddl.options[i].text.length > maxWidth) {
                Counter = Counter + 1;
                maxWidth = ddl.options[i].text.length;
            }
        }
        if (Counter = 1) { ddl.style.width = "250px"; }

        else {
            alert(Counter);
            ddl.style.width = maxWidth + "px";
        }
        //-->
    }
</script>
<style type="text/css">
    .ctrDropDown
    {
        width: 290px;
        font-size: 11px;
        height: 19px;
    }
    .ctrDropDownClick
    {
        width: 430px;
        font-size: 11px;
        height: 19px;
    }
    .plainDropDown
    {
        width: 290px;
        font-size: 11px;
        height: 19px;
    }
</style>

<table align="left" cellpadding="0" cellspacing="0" style="width: 100%">
    <tr align="left" valign="top" style="width: 100%">
        <td align="left" valign="top" style="width: 100%">
            <table class="style1" width="100%">
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Allow Instruments Re-Issuance Queue"
                            CssClass="lblHeadingForQueue"></asp:Label>
                        <br />
                       <%-- Note:&nbsp; Fields marked as
                        <asp:Label ID="Label4" runat="server" Text="*" ForeColor="Red"></asp:Label>
                        &nbsp;are required.--%>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                    <asp:Panel ID="pnlHeader" runat="server" DefaultButton="btnSearch">
                            <table align="left" valign="top" style="width: 100%">
                                <tr align="left" valign="top" style="width: 100%">
                                    <td align="left" valign="top" style="width: 43%">
                                        <table align="left" style="width: 100%" cellpadding="0" cellspacing="0">
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    &nbsp;
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:RadioButtonList ID="rbtnlstDateSelection" runat="server" RepeatDirection="Horizontal"
                                                        Font-Size="10px" AutoPostBack="True">
                                                        <asp:ListItem Value="Creation Date"> Creation Date</asp:ListItem>
                                                        <asp:ListItem Value="Value Date"> Value Date</asp:ListItem>
                                                          <asp:ListItem Value="Last Print Date">Last Print Date</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblDateFrom" runat="server" Text="Date From" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <telerik:RadDatePicker ID="raddpCreationFromDate" runat="server" Width="180px" AutoPostBack="true">
                                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                        </Calendar>
                                                        <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                            Font-Size="10.5px" LabelWidth="40%" type="text" value="">
                                                        </DateInput>
                                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                    </telerik:RadDatePicker>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%; height: 25%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblDateTo" runat="server" Text="Date To" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <telerik:RadDatePicker ID="raddpCreationToDate" runat="server" Width="180px" AutoPostBack="true">
                                                        <Calendar UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x">
                                                        </Calendar>
                                                        <DateInput DateFormat="dd-MMM-yyyy" DisplayDateFormat="dd-MMM-yyyy" DisplayText=""
                                                            LabelWidth="40%" type="text" value="" Font-Size="10.5px">
                                                        </DateInput>
                                                        <DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                    </telerik:RadDatePicker>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 45%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 25%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblCompany" runat="server" Text="Company" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:DropDownList ID="ddlCompany" runat="server" CssClass="dropdownForQueue" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblAccountPaymentNature" runat="server" Text="Account Payment Nature"
                                                        CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <%--<asp:DropDownList ID="ddlAcccountPaymentNature" runat="server" CssClass="dropdownForQueue"
                                                        AutoPostBack="True">
                                                    </asp:DropDownList>--%>
                                                    <div style="width: 290px; overflow: hidden;">                                                      
                                                        <asp:DropDownList ID="ddlAcccountPaymentNature" runat="server" class="ctrDropDown"
                                                            onblur="this.className='ctrDropDown';" onmousedown="this.className='ctrDropDownClick';"
                                                            onchange="this.className='ctrDropDown';" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </div>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblBatch" runat="server" Text="Batch" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:DropDownList ID="ddlBatch" runat="server" CssClass="dropdownForQueue" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblProductType" runat="server" Text="Product Type" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:DropDownList ID="ddlProductType" runat="server" CssClass="dropdownForQueue"
                                                        AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    <asp:Label ID="lblRemarks" runat="server" Text="Remarks" CssClass="lblForDropDown"></asp:Label>
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:TextBox ID="txtRemarks" runat="server" Width="288px" CssClass="txtboxForQueue"
                                                        TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 40%; height: 1px">
                                                </td>
                                                <td align="left" valign="top" style="width: 30%; height: 1px">
                                                </td>
                                            </tr>
                                            <tr align="left" valign="top" style="width: 100%">
                                                <td align="left" valign="top" style="width: 30%">
                                                    &nbsp;
                                                </td>
                                                <td align="left" valign="top" style="width: 40%">
                                                    <asp:Button ID="btnAllowReIssuance" runat="server" CssClass="btn2" 
                                                        Text="Allow Re-Issuance" ValidationGroup="Remarks" OnClientClick ="javascript: return conAllowReIssuance();" />
                                                </td>
                                                <td align="left" valign="top" style="width: 100%">
                                                <%-- <asp:Button ID="btnCancelInstructions" runat="server" Text="Cancel Instructions"
                                                    CssClass="btnCancel2" ValidationGroup="Remarks" />--%>
                                                   <%-- <asp:Button ID="btnPrintSingle" runat="server" Text="Print Single" CssClass="btn2"
                                                        ValidationGroup="Remarks" />
                                                    <asp:Button ID="btnPrintBulk" runat="server" Text="Print Mutiple" CssClass="btn2"
                                                        ValidationGroup="Remarks" />
                                                    <asp:Button ID="btnCancelPrint" runat="server" Text="Cancel Print" CssClass="btnCancel2"
                                                        ValidationGroup="Remarks" />
                                                    &nbsp;&nbsp;--%>
                                                </td>
                                                <td align="left" valign="top" style="width: 30%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="right" valign="top" style="width: 57%">
                                        <table align="left" style="width: 100%">
                                            <tr>
                                                <td align="right" valign="top" style="width: 30%">
                                                </td>
                                                <td align="left" valign="top" style="width: 70%">
                                                    <table style="width: 100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="left" valign="top" style="width: 100%">
                                                                <fieldset class="fieldsetForIE7RightTopTD" style="height: 100px">
                                                                    <legend class="legendForIE7RightTopTD">Search</legend>
                                                                    <table align="left" style="width: 100%">
                                                                        <tr align="left" valign="top" style="width: 100%">
                                                                            <td align="left" valign="top" style="width: 25%">
                                                                                <asp:Label ID="lblInstructionNo" runat="server" Text="Instruction No." CssClass="lblForFieldSetLabels"></asp:Label>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 25%">
                                                                                <asp:Label ID="lblInstrumentNo" runat="server" Text="Instrument No." CssClass="lblForFieldSetLabels"></asp:Label>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 25%">
                                                                                <asp:Label ID="Label2" runat="server" Text="Reference No." CssClass="lblForFieldSetLabels"></asp:Label>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 25%">
                                                                                &nbsp;
                                                                            </td>
                                                                        </tr>
                                                                        <tr align="left" valign="top" style="width: 100%">
                                                                            <td align="left" valign="top" style="width: 29%">
                                                                                <asp:TextBox ID="txtInstructionNo" runat="server" Width="120px" Font-Size="10px"
                                                                                    MaxLength="8"></asp:TextBox>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 29%">
                                                                                <asp:TextBox ID="txtInstrumentNo" runat="server" Width="120px" Font-Size="10px" MaxLength="18"></asp:TextBox>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 29%">
                                                                                <asp:TextBox ID="txtReferenceNo" runat="server" Width="120px" Font-Size="10px" MaxLength="18"></asp:TextBox>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 13%">
                                                                                &nbsp;<asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn2" CausesValidation="False" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr align="left" valign="top" style="width: 100%">
                                                                            <td align="left" valign="top" style="width: 50%">
                                                                                <asp:DropDownList ID="ddlAmountOp" runat="server" CssClass="dropdownForQueueForAmountOperator">
                                                                                    <asp:ListItem>=</asp:ListItem>
                                                                                    <asp:ListItem>&gt;</asp:ListItem>
                                                                                    <asp:ListItem>&lt;</asp:ListItem>
                                                                                    <asp:ListItem>&gt;=</asp:ListItem>
                                                                                    <asp:ListItem>&lt;=</asp:ListItem>
                                                                                    <asp:ListItem>&lt;&gt;</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td align="left" valign="top" style="width: 50%">
                                                                                <asp:TextBox ID="txtAmount" runat="server" Width="120px" Font-Size="10px" MaxLength="8"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <asp:Label ID="lblNRF" runat="server" Text="No Record Found." CssClass="headingblue2"
                            Visible="false"></asp:Label>
                    </td>
                </tr>
                <tr align="left" valign="top" style="width: 100%">
                    <td align="left" valign="top" style="width: 100%">
                        <telerik:RadGrid ID="gvAllowRePrintReIssue" runat="server" AllowPaging="True" Width="100%"
                            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" CssClass="RadGrid"
                            PageSize="100">
                            <ClientSettings>
                            
                            <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                            </ClientSettings>
                             <MasterTableView DataKeyNames="InstructionID,IsAmendmentComplete" NoMasterRecordsText="" TableLayout="Fixed">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderStyle-Width="6%">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" CssClass="chkBox" Checked="false" Text=" Select"
                                                TextAlign="Right" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkBox2" Checked="false" Text="" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                     <telerik:GridTemplateColumn HeaderText="Instruction No." HeaderStyle-Width="8%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbInstructionID" runat="server" Text='<%#Eval("InstructionID") %>'
                                                ToolTip="Instruction No." ForeColor="Blue" Enabled="false"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                       
                                    </telerik:GridTemplateColumn>
                                      <telerik:GridBoundColumn DataField="FileBatchNo" HeaderText="Batch No" SortExpression="FileBatchNo">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%" />
                                    </telerik:GridBoundColumn>
                                   <telerik:GridBoundColumn DataField="CreateDate" HeaderText="Creation Date" SortExpression="CreateDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}" HeaderStyle-Width="8%">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%"/>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%"/>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ValueDate" HeaderText="Value Date" SortExpression="ValueDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}" HeaderStyle-Width="8%">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%"/>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%"/>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="BeneficiaryName" HeaderText="Bene Name" SortExpression="BeneficiaryName" >
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="13%" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="13%"/>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Amount" HeaderText="Amount" SortExpression="Amount"
                                        HtmlEncode="false" DataFormatString="{0:N2}">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="7%"/>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ProductTypeCode" HeaderText="Payment Mode" SortExpression="ProductTypeCode">
                                       
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="10%"/>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="10%"/>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="InstrumentNo" HeaderText="Instrument No." SortExpression="InstrumentNo">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" Width="8%"/>
                                    </telerik:GridBoundColumn>
                                   
                                    <telerik:GridBoundColumn DataField="LastPrintDate" HeaderText="Last Print Date" SortExpression="LastPrintDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}" HeaderStyle-Width="8%">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%"/>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%"/>
                                    </telerik:GridBoundColumn>
                                 
                                </Columns>
                            <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                           <HeaderStyle CssClass="GridHeader" />
                            <ItemStyle CssClass="GridItem" />
                            <AlternatingItemStyle CssClass="GridItem" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                </tr>
                </table>
        </td>
    </tr>
    
</table>
<div id="sumdialog" title="" style="text-align: center">
    <asp:Panel ID="pnlShowSummary" runat="server" Style="display: none; width: 100%"
        ScrollBars="Vertical" Height="600px">
        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnCancel" OnClientClick="CloseSummaryDialogBox();return false;" /><br />
        <telerik:RadGrid ID="gvShowSummary" runat="server" AllowPaging="false" Width="600px" CssClass="RadGrid"
            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True"
            PageSize="100">
        
            <AlternatingItemStyle CssClass="rgAltRow" />
            <MasterTableView TableLayout="Fixed">
                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                </RowIndicatorColumn>
                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="InstructionID" HeaderText="Instruction No." SortExpression="InstructionID">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                    <%-- <telerik:GridBoundColumn DataField="ReferenceNo" HeaderText="Reference No." SortExpression="ReferenceNo">
                                    </telerik:GridBoundColumn>--%>
                    <telerik:GridBoundColumn DataField="Message" HeaderText="Message" SortExpression="Message">
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
                <PagerStyle AlwaysVisible="True" />
            </MasterTableView>
            <ItemStyle CssClass="rgRow" />
            <PagerStyle AlwaysVisible="True" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
    </asp:Panel>
</div>
<div id="urldialog" title="">
    <iframe id='dialogiframe' frameborder="0" allowtransparency="true" src="" style="border-style: none;
        width: 100%; height: 100%; overflow-x: hidden; background-color: transparent;">
    </iframe>
     <div id='loading' style="display: none; margin: 0px auto; text-align: center; width: 100%;
        height: 100%; position: relative">
        <img src='../../../../../images/progressbar.gif' style="left: 45%; top: 50%; position: absolute;" />
    </div>
</div>
