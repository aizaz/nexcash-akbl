Dear [UserName], 

Following transactions are rejected by approver and cannot by re-printed. [For PO/DD txn] {Funds have been reversed to company account.} Details are

[Client Name] - [Account No.] - [Product ]Type - [No. of transactions]

To log on, follow the link [AppLink]

Sincerely,
Al Baraka
