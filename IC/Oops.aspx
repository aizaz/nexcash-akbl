﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Oops.aspx.vb" Inherits="ErrorPages_Oops" %>

<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>
<%@ Register TagPrefix="dnn" TagName="JQUERY" Src="~/Admin/Skins/jQuery.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<link href="../Portals/_default/Skins/IC/OopsStyle.css" rel="stylesheet" />
<dnn:JQUERY ID="dnnjQuery" runat="server" jQueryUI="true" DnnjQueryPlugins="true" />

<style>
    html body {
    
    margin: 0;
    padding: 0;
    }
    .dnnFormItemNew {
        margin-left: 85%;
        margin-top: 10px;
        width: 389px;
        font-family: "Trebuchet MS";
        font-size: 16px;
        color: #000;
        margin-right: 50%;
        text-align:center;
    }

    .dnnFormItem {
        float: left;
        margin-top: 10px;
        width: 389px;
        font-family: "Trebuchet MS";
        font-size: 16px;
        color: #000;
    }
</style>

<!-- Header Start -->
<div id="header">
    <div id="header-wrap">
        <div id="logo">
            <img src="/Portals/_default/Skins/IC/image/albrakalogo.png" width="177" height="53" alt="" />
        </div>
    </div>
    <div id="red-bg">
        <div id="red-bg-wrap">
            <div id="pri-nav">
                <ul>
                    <a id="linkregister" href="/Default.aspx" runat="server">
                        <li>Home</li>
                    </a>
                   <%-- <a>
                        <li></li>
                    </a>--%>

                </ul>
            </div>
        </div>
    </div>
</div>

<!-- Header end -->




<!-- Middle Start -->
<div id="main-middle">
    <div id="main-middle-wrap">

        <div class="middle">

            <div class="inside-middle">
                <div class="inside-right">
                    <div id="Div3" class="dnnFormItem" runat="server">
                        <asp:panel runat="server" align="middle" valign="center">
<div id="Div1" class="dnnFormItemNew" runat="server" >
   
    <img src="/images/Caution.png" height="100" />
     <div id="Div2" class="dnnFormItem" runat="server" >
                                 <div class="heading">
            <h1>Oops! Something went wrong</h1>
                                      
                                         </div>
        
                        </div>
     <div id="Div6" style="margin-left:10px;margin-top:100px" runat="server">
                        <asp:panel runat="server" align="bottom">
                              <div id="Div7" runat="server" style="margin-left:10px;margin-top:100px">
                             <asp:hyperlink id="Hyperlink1" runat="server" text="Return to site" navigateurl="/Default.aspx" />
     </div>
                             </asp:panel>

                    </div>
           
</div>
                       
     
</asp:panel>
                    </div>

                </div>

            </div>

        </div>
    </div>

    <%--<div id="Div4" class="dnnFormItem" runat="server">
        <asp:panel runat="server" align="bottom">
                              <div id="Div5" runat="server" style="margin-left:10px;margin-top:100px">
                             <asp:hyperlink id="btnReturn" runat="server" text="Return to site" navigateurl="/Default.aspx" />
     </div>
                             </asp:panel>

    </div>--%>
</div>



<!-- Footer Start -->

<%--<div class="footer">
    <div class="footer-wrap">
        <p>
            <dnn:COPYRIGHT ID="COPYRIGHT1" runat="server" />
        </p>

        <div class="footerlinks">
            <ul>
                <li><a href="#">Sitemap><a href="#">Sitemap</a> | </li>
                <li><a href="#">Privacy Policy</a> | </li>
                <li><a href="#">Disclaimer</a></li>
            </ul>
        </div>
    </div>
</div>--%>

<!-- Footer End -->

