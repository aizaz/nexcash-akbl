﻿Imports ICBO
Imports ICBO.IC
Imports System.IO
Imports System.Xml
Partial Class DownloadFile
    'Inherits System.Web.UI.Page
    Inherits DotNetNuke.Framework.CDefault

    Private FileId As String
    Protected Sub DownloadFile_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim isValid As Boolean = True
        Dim objUserInfo As UserInfo = UserController.GetCurrentUserInfo

        If Session("AuthToken") Is Nothing Or Request.QueryString("fileid") Is Nothing Or objUserInfo.UserID = -1 Then
            isValid = False
        End If

        If isValid Then
            isValid = ICUtilities.AuthenticateRequest(Request, Session("AuthToken"))
        End If

        If Not isValid Then
            Response.Redirect(NavigateURL("Login"))
            'Exit Sub
        End If


        FileId = Request.QueryString("fileid").ToString

        If FileId.IsValidBase64String() = False Then
            Exit Sub
        End If

        FileId = FileId.DecryptString()

        If Page.IsPostBack = False Then

            Dim FileType As Boolean = True
            If Not Request.QueryString("FileType") Is Nothing Then
                If Request.QueryString("FileType").ToString = "1" Then
                    FileType = True
                Else
                    FileType = False
                End If
            Else
                FileType = True
            End If

            ICUtilities.GetFileFromDB(FileId, FileType)

            ' GetFileFromDB(Request.QueryString("fileid"), FileType)




        End If



    End Sub
    Public Shared Sub GetFileFromDB(ByVal fileid As Integer, ByVal IsFile As Boolean)
        If IsFile = True Then
            Dim att As New ICFiles
            If att.LoadByPrimaryKey(fileid) Then
                If att.FileName.Split(".")(1).Trim.ToString().ToLower() = "trdx" Then
                    HttpContext.Current.Response.Clear()
                    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & att.FileName)
                    HttpContext.Current.Response.ContentType = "text/xml"
                    HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8
                    Dim doc As New XmlDocument
                    Dim xml As String = System.Text.Encoding.UTF8.GetString(att.FileData)
                    Dim i As Integer = 0
                    i = xml.IndexOf("<")
                    If i > 0 Then
                        xml = xml.Remove(0, i)
                    End If
                    doc.LoadXml(xml)
                    doc.Save(HttpContext.Current.Response.OutputStream)
                    HttpContext.Current.Response.Flush()
                    HttpContext.Current.Response.End()
                Else
                    HttpContext.Current.Response.Clear()
                    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & att.OriginalFileName.Replace(" ", ""))
                    HttpContext.Current.Response.ContentType = att.FileMIMEType
                    HttpContext.Current.Response.BinaryWrite(att.FileData)
                    HttpContext.Current.Response.Flush()
                    HttpContext.Current.Response.End()
                End If
            End If
        Else
            Dim objICMISReport As New ICMISReports
            If objICMISReport.LoadByPrimaryKey(fileid) Then
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" & objICMISReport.ReportName & ".trdx")
                HttpContext.Current.Response.ContentType = "text/xml"
                HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8
                Dim doc As New XmlDocument
                Dim xml As String = System.Text.Encoding.UTF8.GetString(objICMISReport.ReportFileData)
                Dim i As Integer = 0
                i = xml.IndexOf("<")
                If i > 0 Then
                    xml = xml.Remove(0, i)
                End If
                doc.LoadXml(xml)
                doc.Save(HttpContext.Current.Response.OutputStream)
                HttpContext.Current.Response.Flush()
                HttpContext.Current.Response.End()
            End If
        End If
    End Sub
End Class
