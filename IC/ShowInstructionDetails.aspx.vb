﻿Imports ICBO
Imports ICBO.IC

Partial Class ShowInstructionDetails
    'Inherits System.Web.UI.Page
    Inherits DotNetNuke.Framework.CDefault
    Private InstructionID As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim isValid As Boolean = True

        Dim objUserInfo As UserInfo = UserController.GetCurrentUserInfo

        If Session("AuthToken") Is Nothing Or Request.QueryString("InstructionID") Is Nothing Or objUserInfo.UserID = -1 Then
            'Response.Redirect(NavigateURL("Login"))
            isValid = False
        End If

        If isValid Then
            isValid = ICUtilities.AuthenticateRequest(Request, Session("AuthToken"))
        End If

        isValid = ICUserRolesController.HasInstructionDetailRightsByUserId(objUserInfo.UserID)

        If Not isValid Then
            tblDetails.Visible = False
            Exit Sub
        End If

        InstructionID = Request.QueryString("InstructionID").ToString

        If InstructionID.IsValidBase64String() = False Then
            Exit Sub
        End If

        If isValidNumber(InstructionID) = True Then
            Exit Sub
        End If

        InstructionID = InstructionID.DecryptString()


        'System.Threading.Thread.Sleep(2000)

        Try
            Dim objICInstruction As New ICInstruction
            Dim objICFiles As New ICFiles
            Dim objICTemplate As New ICTemplate



            objICInstruction.LoadByPrimaryKey(InstructionID)
            dvViewInsDetails.DataSource = ICInstructionController.GetInstructionDetailsByInstructionID(InstructionID)
            dvViewInsDetails.DataBind()


            gvApproverDetails.DataSource = ICInstructionController.GetInstructionApproverDetailsByInstructionID(InstructionID)
            gvApproverDetails.DataBind()

            If gvApproverDetails.Items.Count > 0 Then
                gvApproverDetails.Visible = True
                lblAppNRF.Visible = False
            Else
                gvApproverDetails.Visible = False
                lblAppNRF.Visible = True
            End If

            If Not objICInstruction.FileID Is Nothing Then


                If objICFiles.LoadByPrimaryKey(objICInstruction.FileID) Then
                    If Not objICFiles.FileUploadTemplateID Is Nothing Then
                        objICTemplate.LoadByPrimaryKey(objICFiles.FileUploadTemplateID)
                        If objICTemplate.TemplateType = "Dual" Then
                            gvDetails.DataSource = ICInstructionController.GetInstructionDetails(InstructionID)
                            gvDetails.DataBind()

                            If gvDetails.Items.Count > 0 Then
                                pnlDetails.Visible = True
                                lblDetails.Visible = True
                            End If

                        Else
                            gvHeaderDetail.DataSource = ICInstructionController.GetInstructionDetailsForHeaderTemplate(InstructionID)
                            gvHeaderDetail.DataBind()
                            If gvHeaderDetail.Items.Count > 0 Then
                                pnlHeaderDetail.Visible = True
                                lblHeaderDetails.Visible = True
                            End If

                        End If




                    End If
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    Public Shared Function isValidNumber(number As String) As Boolean

        Try
            Convert.ToInt64(number)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Page.ClientScript.RegisterClientScriptBlock(Page.GetType, Page.ClientID, "window.onload = function () {parent.CloseIframe();    }", True)
    End Sub
End Class
