﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowSiignatures.aspx.vb"
    Inherits="ShowSiignatures" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="Portals/_default/Skins/IC/css/styles.css" rel="stylesheet" type="text/css" />
  <%--  <link href="Portals/_default/Skins/IC/jqueryui/jquery-ui-1.8.11.custom.css" rel="stylesheet"
        type="text/css" />
    <script src="Portals/_default/Skins/IC/jqueryui/jquery-ui-1.8.11.custom.min.js"
        type="text/javascript"></script>--%>
</head>
<body>
    <form id="form1" runat="server">
   <%--<asp:ScriptManager ID="smng" runat="server" >
   <Scripts >
   </Scripts>
   </asp:ScriptManager>--%>
    <table id="tblDetails" runat="server" width="100%">
        <tr align="left" valign="top" style="width: 100%">
            <td align="center" valign="top" colspan="2">
                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnCancel" CausesValidation="False" />
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 50%">
                <asp:Label ID="lblPageHeader" runat="server" CssClass="headingblue"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 50%">
                &nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 100%" colspan="2">
                <asp:Label ID="lblRNF" runat="server" Font-Bold="False" Text="No Record Found" Visible="False"
                    CssClass="headingblue"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" colspan="2">
                
                        <telerik:RadGrid ID="gvAppprovers" runat="server" AllowPaging="false" Width="100%"
                            AllowSorting="false" AutoGenerateColumns="False" CellSpacing="0" ShowFooter="false"
                            PageSize="50">
                            <alternatingitemstyle cssclass="rgAltRow" />
                            <MasterTableView  DataKeyNames="UserID">
                    <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                    <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    </ExpandCollapseColumn>
                    <Columns>                        
                        <telerik:GridBoundColumn DataField="UserName" HeaderText="User Name" SortExpression="UserName">
                        </telerik:GridBoundColumn>                       
                        <telerik:GridBoundColumn DataField="DisplayName" HeaderText="Display Name"
                            SortExpression="DisplayName">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn HeaderText="Signatures">
                                        <ItemTemplate>
                                            <asp:Image ID="imgSignature" runat="server" Width="100" Height="60" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                    <PagerStyle AlwaysVisible="True" />
                </mastertableview>
                            <itemstyle cssclass="rgRow" />
                            <pagerstyle alwaysvisible="True" />
                            <filtermenu enableimagesprites="False">
                </filtermenu>
                        </telerik:RadGrid>
               
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
