﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowCollectionsDetails.aspx.vb"
    Inherits="ShowInstructionDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="Portals/_default/Skins/IC/css/styles.css" rel="stylesheet" type="text/css" />
    <%--<link href="Portals/_default/Skins/IC/jqueryui/jquery-ui-1.8.11.custom.css" rel="stylesheet"
        type="text/css" />
    <script src="Portals/_default/Skins/IC/jqueryui/jquery-ui-1.8.11.custom.min.js"
        type="text/javascript"></script>--%>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="smng" runat="server">
        <Scripts>
        </Scripts>
    </asp:ScriptManager>
    <table id="tblDetails" runat="server" width="100%">
        <tr align="left" valign="top" style="width: 100%">
            <td align="center" valign="top" colspan="2">
                <asp:Button ID="btnCancel" runat="server" Text="Close" CssClass="btnCancel2" 
                    CausesValidation="False" />
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 50%">
                <asp:Label ID="lblViewInstructionDetails" runat="server" Text="Collection Details"
                    CssClass="lblHeadingForQueue"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 50%">
                &nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 100%" colspan="2">
                <asp:Label ID="lblRNF" runat="server" Font-Bold="False" Text="No Record Found" Visible="False"
                    CssClass="lblHeadingForQueue"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" colspan="2">
                <telerik:RadGrid ID="gvCollDetails" runat="server" AllowPaging="True" AllowSorting="false" AutoGenerateColumns="false" CellSpacing="0" CssClass="RadGrid" PageSize="100" ShowFooter="True">
                    <ClientSettings>
                        <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    </ClientSettings>
                    <AlternatingItemStyle CssClass="rgAltRow" />
                    <MasterTableView TableLayout="Fixed">
                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                        </ExpandCollapseColumn>
                        <Columns>
                             <telerik:GridBoundColumn DataField="CollectionDate" HeaderText="Collection Date" SortExpression="CollectionDate"
                                        HtmlEncode="false" DataFormatString="{0:dd-MMM-yyyy}" >
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top"  />
                                    </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="OfficeName" HeaderText="Collection Location" SortExpression="OfficeName">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                             <telerik:GridBoundColumn DataField="CollectionAmount" HeaderText="Amount" SortExpression="CollectionAmount"
                                        HtmlEncode="false" DataFormatString="{0:N2}">
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"  />
                                    </telerik:GridBoundColumn>
                             <telerik:GridBoundColumn DataField="CollectionMode" HeaderText="Collection Mode" SortExpression="CollectionMode">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </telerik:GridBoundColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                        </EditFormSettings>
                        <PagerStyle AlwaysVisible="True" />
                    </MasterTableView>
                    <ItemStyle CssClass="rgRow" />
                    <PagerStyle AlwaysVisible="True" />
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                </telerik:RadGrid>
            </td>
        </tr>
         <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" colspan="2">
                
            </td>
        </tr>

        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" colspan="2">
               
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
