﻿Imports ICBO
Imports ICBO.IC
Partial Class ShowInstructionDetails
    Inherits System.Web.UI.Page
    Private Ref1 As String
    Private Ref2 As String
    Private Ref3 As String
    Private Ref4 As String
    Private Ref5 As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("Ref1").ToString.Split("-")(0) <> "" Then
            Ref1 = Request.QueryString("Ref1").ToString.Split("-")(0)
        Else
            Ref1 = Nothing
        End If
        If Request.QueryString("Ref1").ToString.Split("-")(1) <> "" Then
            Ref2 = Request.QueryString("Ref1").ToString.Split("-")(1)
        Else
            Ref2 = Nothing
        End If
        If Request.QueryString("Ref1").ToString.Split("-")(2) <> "" Then
            Ref3 = Request.QueryString("Ref1").ToString.Split("-")(2)
        Else
            Ref3 = Nothing
        End If
        If Request.QueryString("Ref1").ToString.Split("-")(3) <> "" Then
            Ref4 = Request.QueryString("Ref1").ToString.Split("-")(3)
        Else
            Ref4 = Nothing
        End If
        If Request.QueryString("Ref1").ToString.Split("-")(4) <> "" Then
            Ref5 = Request.QueryString("Ref1").ToString.Split("-")(4)
        Else
            Ref5 = Nothing
        End If
        'System.Threading.Thread.Sleep(2000)

        Try
            Dim dt As New DataTable
            dt = ICCollectionsController.GetCollDetailsByReferenceFields(Ref1, Ref2, Ref3, Ref4, Ref5)
            If dt.Rows.Count > 0 Then
                gvCollDetails.DataSource = dt
                gvCollDetails.DataBind()
                gvCollDetails.Visible = True
                lblRNF.Visible = False
            Else
                gvCollDetails.DataSource = Nothing
                gvCollDetails.Visible = False
                gvCollDetails.DataBind()
                lblRNF.Visible = True
            End If

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Page.ClientScript.RegisterClientScriptBlock(Page.GetType, Page.ClientID, "window.onload = function () {parent.CloseIframe();    }", True)
    End Sub
End Class
