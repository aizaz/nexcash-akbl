﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowInstructionDetails.aspx.vb"
    Inherits="ShowInstructionDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="Portals/_default/Skins/IC/css/styles.css" rel="stylesheet" type="text/css" />
    <%--<link href="Portals/_default/Skins/IC/jqueryui/jquery-ui-1.8.11.custom.css" rel="stylesheet"
        type="text/css" />
    <script src="Portals/_default/Skins/IC/jqueryui/jquery-ui-1.8.11.custom.min.js"
        type="text/javascript"></script>--%>
</head>
<body>
    <form id="form1" runat="server">
   <%-- <asp:ScriptManager ID="smng" runat="server">
        <Scripts>
        </Scripts>
    </asp:ScriptManager>--%>
    <table id="tblDetails" runat="server" width="100%">
        <tr align="left" valign="top" style="width: 100%">
            <td align="center" valign="top" colspan="2">
                <asp:Button ID="btnCancel" runat="server" Text="Close" CssClass="btnCancel2" 
                    CausesValidation="False" />
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 50%">
                <asp:Label ID="lblViewInstructionDetails" runat="server" Text="Instruction Details"
                    CssClass="lblHeadingForQueue"></asp:Label>
            </td>
            <td align="left" valign="top" style="width: 50%">
                &nbsp;
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" style="width: 100%" colspan="2">
                <asp:Label ID="lblRNF" runat="server" Font-Bold="False" Text="No Record Found" Visible="False"
                    CssClass="lblHeadingForQueue"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" colspan="2">
                <asp:DetailsView ID="dvViewInsDetails" runat="server" AutoGenerateColumns="False"
                    CssClass="Grid" Width="100%" AllowPaging="True" EnableModelValidation="True"
                    AutoGenerateRows="False">
                    <AlternatingRowStyle CssClass="GridAltItem" />
                    <FieldHeaderStyle Width="180px" HorizontalAlign="Left" VerticalAlign="Top" />
                    <Fields>
                        <asp:BoundField DataField="InstructionID" HeaderText="Instruction No" SortExpression="InstructionID">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ClientName" HeaderText="Client Name" SortExpression="ClientName">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Amount" HeaderText="Amount" SortExpression="Amount" DataFormatString="{0:N2}"
                            HtmlEncode="False">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="AmountInWords" HeaderText="Amount in Words" SortExpression="AmountInWords">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BeneficiaryName" HeaderText="Beneficiary Name" SortExpression="BeneficiaryName">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BeneficiaryEmail" HeaderText="Beneficiary Email" SortExpression="BeneficiaryEmail">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BeneficiaryMobile" HeaderText="Beneficiary Cell No" SortExpression="BeneficiaryMobile">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BeneficiaryAccountNo" HeaderText="Beneficiary Account No"
                            SortExpression="BeneficiaryAccountNo">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BeneficaryAccountType" HeaderText="Beneficiary Account Type"
                            SortExpression="BeneficaryAccountType">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BeneficiaryAddress" HeaderText="Beneficiary Address" SortExpression="BeneficiaryAddress">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BeneficiaryCountry" HeaderText="Beneficiary Country" SortExpression="Beneficiary Country">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BeneficiaryProvince" HeaderText="Beneficiary Province"
                            SortExpression="BeneficiaryProvince">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BeneficiaryCity" HeaderText="Beneficiary City" SortExpression="BeneficiaryCity">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PrintLocationName" HeaderText="Print Location" SortExpression="PrintLocationName">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DrawnOnBranchCode" HeaderText="DD Drawn Branch" SortExpression="DrawnOnBranchCode">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BeneficiaryCNIC" HeaderText="Beneficiary CNIC No" SortExpression="BeneficiaryCNIC">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ClientAccountNo" HeaderText="Client Account No" SortExpression="ClientAccountNo">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ClientCountry" HeaderText="Client Country" SortExpression="ClientCountry">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ClientProvince" HeaderText="Client Province" SortExpression="ClientProvince">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ClientCity" HeaderText="Client City" SortExpression="ClientCity">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PaymentMode" HeaderText="Payment Mode" SortExpression="PaymentMode">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ProductTypeName" HeaderText="Product Type" SortExpression="ProductTypeName">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BeneficiaryBankCode" HeaderText="Beneficiary Bank Code"
                            SortExpression="BeneficiaryBankCode">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BankName" HeaderText="Beneficiary Bank Name" SortExpression="BankName">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BeneficiaryAccountTitle" HeaderText="Bank Account Title"
                            SortExpression="BeneficiaryAccountTitle">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Remarks" HeaderText="Remarks" SortExpression="Remarks">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CreateDate" HeaderText="Creation Date" SortExpression="CreateDate"
                            DataFormatString="{0:dd-MMM-yyyy}" HtmlEncode="False">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ValueDate" HeaderText="Value Date" SortExpression="ValueDate"
                            DataFormatString="{0:dd-MMM-yyyy}" HtmlEncode="False">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                         <asp:BoundField DataField="PrintDate" HeaderText="Print Date" SortExpression="PrintDate"
                            DataFormatString="{0:dd-MMM-yyyy}" HtmlEncode="False">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="LastStatus" HeaderText="Last Status" SortExpression="Last Status">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FileID" HeaderText="File ID" SortExpression="FileID">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="AcqMode" HeaderText="Acquisition Mode" SortExpression="AcqMode">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                         <asp:BoundField DataField="CompanyName" HeaderText="UBP Company" SortExpression="CompanyName">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="UBPReferenceField" HeaderText="UBP Reference No." SortExpression="UBPReferenceField">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BeneficiaryIDType" HeaderText="Beneficiary ID Type" SortExpression="BeneficiaryIDType">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="BeneficiaryIDNo" HeaderText="Beneficiary ID No" SortExpression="BeneficiaryIDNo">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        
                        <asp:BoundField DataField="SkipReason" HeaderText="Skip Reason" SortExpression="SkipReason">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CreatedBy" HeaderText="Created By" SortExpression="CreatedBy">
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:BoundField>
                    </Fields>
                    <HeaderStyle CssClass="GridHeader" />
                            <RowStyle CssClass="GridItem" />
                            <AlternatingRowStyle CssClass="GridItem" />
                    
                    <PagerStyle CssClass="GridPager" />
                  
                </asp:DetailsView>
            </td>
        </tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" colspan="2">
                <asp:Label ID="lblHeaderDetails0" runat="server" Text="Approver Details" CssClass="lblHeadingForQueue"></asp:Label>
                </td></tr>
        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" colspan="2">

                <asp:Label ID="lblAppNRF" runat="server" Text="No Approver Found" CssClass="lblHeadingForQueue" Visible="False"></asp:Label>

            </td></tr>
         <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" colspan="2">

                        <telerik:RadGrid ID="gvApproverDetails" runat="server" AllowPaging="false" AllowSorting="false"
                            AutoGenerateColumns="False" CellSpacing="0" ShowFooter="True" PageSize="5" CssClass="RadGrid">
                           <AlternatingItemStyle CssClass="rgAltRow" />
                           <ClientSettings>
                           <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                           </ClientSettings>
                            <MasterTableView TableLayout="Auto">
                                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="UserName" HeaderText="User Name" SortExpression="UserName">
                                         <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="DisplayName" HeaderText="Display Name" SortExpression="DisplayName">
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="ApprovalDateTime" HeaderText="Approval Date" SortExpression="ApprovalDateTime">
                                     <ItemStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                        <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top"/>
                                    </telerik:GridBoundColumn>
                                </Columns>
                                 <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                                <PagerStyle AlwaysVisible="True" />
                            </MasterTableView>
                            <ItemStyle CssClass="rgRow" />
                            <PagerStyle AlwaysVisible="True" />
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>

            </td></tr>
         <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" colspan="2">
                <asp:Label ID="lblHeaderDetails" runat="server" Text="Details" CssClass="lblHeadingForQueue" Visible="false"></asp:Label>
                <br />
                <asp:Panel ID="pnlHeaderDetail" runat="server" BorderWidth="1px" Height="200px" ScrollBars="Vertical"
                    Visible="false" Width="640px">
                    <telerik:RadGrid ID="gvHeaderDetail" runat="server" AllowPaging="false" AllowSorting="false"
                        AutoGenerateColumns="false" CellSpacing="0" PageSize="4" ShowFooter="True" Visible="true">
                        <AlternatingItemStyle CssClass="rgAltRow" HorizontalAlign="Left" VerticalAlign="Top" />
                        <MasterTableView>
                            <CommandItemSettings ExportToPdfText="Export to PDF" />
                            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridBoundColumn DataField="FF_Amt_1" HeaderText="FF_Amt_1" SortExpression="FF_Amt_1"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_2" HeaderText="FF_Amt_2" SortExpression="FF_Amt_2"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_3" HeaderText="FF_Amt_3" SortExpression="FF_Amt_3"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_4" HeaderText="FF_Amt_4" SortExpression="FF_Amt_4"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_5" HeaderText="FF_Amt_5" SortExpression="FF_Amt_5"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_6" HeaderText="FF_Amt_6" SortExpression="FF_Amt_6"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_7" HeaderText="FF_Amt_7" SortExpression="FF_Amt_7"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_8" HeaderText="FF_Amt_8" SortExpression="FF_Amt_8"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_9" HeaderText="FF_Amt_9" SortExpression="FF_Amt_9"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_10" HeaderText="FF_Amt_10" SortExpression="FF_Amt_10"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_11" HeaderText="FF_Amt_11" SortExpression="FF_Amt_11"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_12" HeaderText="FF_Amt_12" SortExpression="FF_Amt_12"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_13" HeaderText="FF_Amt_13" SortExpression="FF_Amt_13"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_14" HeaderText="FF_Amt_14" SortExpression="FF_Amt_14"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_15" HeaderText="FF_Amt_15" SortExpression="FF_Amt_15"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_16" HeaderText="FF_Amt_16" SortExpression="FF_Amt_16"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_17" HeaderText="FF_Amt_17" SortExpression="FF_Amt_17"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_18" HeaderText="FF_Amt_18" SortExpression="FF_Amt_18"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_19" HeaderText="FF_Amt_19" SortExpression="FF_Amt_19"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_20" HeaderText="FF_Amt_10" SortExpression="FF_Amt_20"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_21" HeaderText="FF_Amt_21" SortExpression="FF_Amt_21"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_22" HeaderText="FF_Amt_22" SortExpression="FF_Amt_22"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_23" HeaderText="FF_Amt_23" SortExpression="FF_Amt_23"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_24" HeaderText="FF_Amt_24" SortExpression="FF_Amt_24"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_25" HeaderText="FF_Amt_25" SortExpression="FF_Amt_25"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_26" HeaderText="FF_Amt_26" SortExpression="FF_Amt_26"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_27" HeaderText="FF_Amt_27" SortExpression="FF_Amt_27"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_28" HeaderText="FF_Amt_28" SortExpression="FF_Amt_28"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_29" HeaderText="FF_Amt_29" SortExpression="FF_Amt_29"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_30" HeaderText="FF_Amt_30" SortExpression="FF_Amt_30"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_31" HeaderText="FF_Amt_31" SortExpression="FF_Amt_31"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_32" HeaderText="FF_Amt_32" SortExpression="FF_Amt_32"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_33" HeaderText="FF_Amt_33" SortExpression="FF_Amt_33"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_34" HeaderText="FF_Amt_34" SortExpression="FF_Amt_34"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_35" HeaderText="FF_Amt_35" SortExpression="FF_Amt_35"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_36" HeaderText="FF_Amt_36" SortExpression="FF_Amt_36"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_37" HeaderText="FF_Amt_37" SortExpression="FF_Amt_37"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_38" HeaderText="FF_Amt_38" SortExpression="FF_Amt_38"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_39" HeaderText="FF_Amt_39" SortExpression="FF_Amt_39"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_40" HeaderText="FF_Amt_40" SortExpression="FF_Amt_40"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_41" HeaderText="FF_Amt_41" SortExpression="FF_Amt_41"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_42" HeaderText="FF_Amt_42" SortExpression="FF_Amt_42"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_43" HeaderText="FF_Amt_43" SortExpression="FF_Amt_43"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_44" HeaderText="FF_Amt_44" SortExpression="FF_Amt_44"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_45" HeaderText="FF_Amt_45" SortExpression="FF_Amt_45"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_46" HeaderText="FF_Amt_46" SortExpression="FF_Amt_46"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_47" HeaderText="FF_Amt_47" SortExpression="FF_Amt_47"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_48" HeaderText="FF_Amt_48" SortExpression="FF_Amt_48"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_49" HeaderText="FF_Amt_49" SortExpression="FF_Amt_49"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_50" HeaderText="FF_Amt_50" SortExpression="FF_Amt_50"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_1" HeaderText="FF_txt_1" SortExpression="FF_txt_1">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_2" HeaderText="FF_txt_2" SortExpression="FF_txt_2">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_3" HeaderText="FF_txt_3" SortExpression="FF_txt_3">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_4" HeaderText="FF_txt_4" SortExpression="FF_txt_4">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_5" HeaderText="FF_txt_5" SortExpression="FF_txt_5">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_6" HeaderText="FF_txt_6" SortExpression="FF_txt_6">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_7" HeaderText="FF_txt_7" SortExpression="FF_txt_7">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_8" HeaderText="FF_txt_8" SortExpression="FF_txt_8">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_9" HeaderText="FF_txt_9" SortExpression="FF_txt_9">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_10" HeaderText="FF_txt_10" SortExpression="FF_txt_10">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_11" HeaderText="FF_txt_11" SortExpression="FF_txt_10">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_12" HeaderText="FF_txt_12" SortExpression="FF_txt_12">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_13" HeaderText="FF_txt_13" SortExpression="FF_txt_13">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_14" HeaderText="FF_txt_14" SortExpression="FF_txt_14">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_15" HeaderText="FF_txt_15" SortExpression="FF_txt_15">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_16" HeaderText="FF_txt_16" SortExpression="FF_txt_16">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_17" HeaderText="FF_txt_17" SortExpression="FF_txt_17">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_18" HeaderText="FF_txt_18" SortExpression="FF_txt_18">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_19" HeaderText="FF_txt_19" SortExpression="FF_txt_19">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_20" HeaderText="FF_txt_20" SortExpression="FF_txt_20">
                                </telerik:GridBoundColumn>
                               <telerik:GridBoundColumn DataField="FF_txt_21" HeaderText="FF_txt_21" SortExpression="FF_txt_21">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_22" HeaderText="FF_txt_22" SortExpression="FF_txt_22">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_23" HeaderText="FF_txt_23" SortExpression="FF_txt_23">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_24" HeaderText="FF_txt_24" SortExpression="FF_txt_24">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_25" HeaderText="FF_txt_25" SortExpression="FF_txt_25">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_26" HeaderText="FF_txt_26" SortExpression="FF_txt_26">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_27" HeaderText="FF_txt_27" SortExpression="FF_txt_27">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_28" HeaderText="FF_txt_28" SortExpression="FF_txt_28">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_29" HeaderText="FF_txt_29" SortExpression="FF_txt_29">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_30" HeaderText="FF_txt_30" SortExpression="FF_txt_30">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_31" HeaderText="FF_txt_31" SortExpression="FF_txt_31">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_32" HeaderText="FF_txt_32" SortExpression="FF_txt_32">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_33" HeaderText="FF_txt_33" SortExpression="FF_txt_33">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_34" HeaderText="FF_txt_34" SortExpression="FF_txt_34">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_35" HeaderText="FF_txt_35" SortExpression="FF_txt_35">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_36" HeaderText="FF_txt_36" SortExpression="FF_txt_36">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_37" HeaderText="FF_txt_7" SortExpression="FF_txt_37">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_38" HeaderText="FF_txt_38" SortExpression="FF_txt_38">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_39" HeaderText="FF_txt_39" SortExpression="FF_txt_39">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_40" HeaderText="FF_txt_40" SortExpression="FF_txt_40">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_41" HeaderText="FF_txt_41" SortExpression="FF_txt_41">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_42" HeaderText="FF_txt_42" SortExpression="FF_txt_42">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_43" HeaderText="FF_txt_43" SortExpression="FF_txt_43">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_44" HeaderText="FF_txt_44" SortExpression="FF_txt_44">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_45" HeaderText="FF_txt_45" SortExpression="FF_txt_45">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_46" HeaderText="FF_txt_46" SortExpression="FF_txt_46">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_47" HeaderText="FF_txt_47" SortExpression="FF_txt_47">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_48" HeaderText="FF_txt_48" SortExpression="FF_txt_48">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_49" HeaderText="FF_txt_49" SortExpression="FF_txt_49">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_50" HeaderText="FF_txt_50" SortExpression="FF_txt_50">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_51" HeaderText="FF_txt_51" SortExpression="FF_txt_51">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_52" HeaderText="FF_txt_52" SortExpression="FF_txt_52">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_53" HeaderText="FF_txt_53" SortExpression="FF_txt_53">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_54" HeaderText="FF_txt_54" SortExpression="FF_txt_54">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_55" HeaderText="FF_txt_55" SortExpression="FF_txt_55">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_56" HeaderText="FF_txt_56" SortExpression="FF_txt_56">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_57" HeaderText="FF_txt_57" SortExpression="FF_txt_57">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_58" HeaderText="FF_txt_58" SortExpression="FF_txt_58">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_59" HeaderText="FF_txt_59" SortExpression="FF_txt_59">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_60" HeaderText="FF_txt_60" SortExpression="FF_txt_60">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_61" HeaderText="FF_txt_61" SortExpression="FF_txt_61">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_62" HeaderText="FF_txt_62" SortExpression="FF_txt_62">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_63" HeaderText="FF_txt_63" SortExpression="FF_txt_63">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_64" HeaderText="FF_txt_64" SortExpression="FF_txt_64">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_65" HeaderText="FF_txt_65" SortExpression="FF_txt_65">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_66" HeaderText="FF_txt_66" SortExpression="FF_txt_66">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_67" HeaderText="FF_txt_67" SortExpression="FF_txt_67">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_68" HeaderText="FF_txt_68" SortExpression="FF_txt_68">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_69" HeaderText="FF_txt_69" SortExpression="FF_txt_69">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_70" HeaderText="FF_txt_70" SortExpression="FF_txt_70">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_71" HeaderText="FF_txt_71" SortExpression="FF_txt_71">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_72" HeaderText="FF_txt_72" SortExpression="FF_txt_72">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_73" HeaderText="FF_txt_73" SortExpression="FF_txt_73">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_74" HeaderText="FF_txt_74" SortExpression="FF_txt_74">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_75" HeaderText="FF_txt_75" SortExpression="FF_txt_75">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_76" HeaderText="FF_txt_76" SortExpression="FF_txt_76">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_77" HeaderText="FF_txt_77" SortExpression="FF_txt_77">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_78" HeaderText="FF_txt_78" SortExpression="FF_txt_78">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_79" HeaderText="FF_txt_79" SortExpression="FF_txt_79">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_80" HeaderText="FF_txt_80" SortExpression="FF_txt_80">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_81" HeaderText="FF_txt_81" SortExpression="FF_txt_81">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_82" HeaderText="FF_txt_82" SortExpression="FF_txt_82">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_83" HeaderText="FF_txt_83" SortExpression="FF_txt_83">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_84" HeaderText="FF_txt_84" SortExpression="FF_txt_84">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_85" HeaderText="FF_txt_85" SortExpression="FF_txt_85">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_86" HeaderText="FF_txt_86" SortExpression="FF_txt_86">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_87" HeaderText="FF_txt_87" SortExpression="FF_txt_87">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_88" HeaderText="FF_txt_88" SortExpression="FF_txt_88">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_89" HeaderText="FF_txt_89" SortExpression="FF_txt_89">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_90" HeaderText="FF_txt_90" SortExpression="FF_txt_90">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_91" HeaderText="FF_txt_91" SortExpression="FF_txt_91">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_92" HeaderText="FF_txt_92" SortExpression="FF_txt_92">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_93" HeaderText="FF_txt_93" SortExpression="FF_txt_93">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_94" HeaderText="FF_txt_94" SortExpression="FF_txt_94">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_95" HeaderText="FF_txt_95" SortExpression="FF_txt_95">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_96" HeaderText="FF_txt_96" SortExpression="FF_txt_96">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_97" HeaderText="FF_txt_97" SortExpression="FF_txt_97">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_98" HeaderText="FF_txt_98" SortExpression="FF_txt_98">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_99" HeaderText="FF_txt_99" SortExpression="FF_txt_99">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_100" HeaderText="FF_txt_100" SortExpression="FF_txt_100">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                          <HeaderStyle CssClass="GridHeader"/>
                            <ItemStyle CssClass="GridItem" />
                            <AlternatingItemStyle CssClass="GridItem" />
                    </telerik:RadGrid>
                </asp:Panel>
            </td>
        </tr>

        <tr align="left" valign="top" style="width: 100%">
            <td align="left" valign="top" colspan="2">
                <asp:Label ID="lblDetails" runat="server" Text="Details" CssClass="lblHeadingForQueue" Visible="false"></asp:Label>
                <br />
                <asp:Panel ID="pnlDetails" runat="server" BorderWidth="1px" Height="200px" ScrollBars="Vertical"
                    Visible="false" Width="640px">
                    <telerik:RadGrid ID="gvDetails" runat="server" AllowPaging="false" AllowSorting="false"
                        AutoGenerateColumns="false" CellSpacing="0" PageSize="4" ShowFooter="True" Visible="true">
                        <AlternatingItemStyle CssClass="rgAltRow" HorizontalAlign="Left" VerticalAlign="Top" />
                        <MasterTableView>
                            <CommandItemSettings ExportToPdfText="Export to PDF" />
                            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                            </ExpandCollapseColumn>
                            <Columns>
                              <telerik:GridBoundColumn DataField="FF_Amt_1" HeaderText="FF_Amt_1" SortExpression="FF_Amt_1"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_2" HeaderText="FF_Amt_2" SortExpression="FF_Amt_2"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_3" HeaderText="FF_Amt_3" SortExpression="FF_Amt_3"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_4" HeaderText="FF_Amt_4" SortExpression="FF_Amt_4"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_5" HeaderText="FF_Amt_5" SortExpression="FF_Amt_5"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_6" HeaderText="FF_Amt_6" SortExpression="FF_Amt_6"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_7" HeaderText="FF_Amt_7" SortExpression="FF_Amt_7"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_8" HeaderText="FF_Amt_8" SortExpression="FF_Amt_8"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_9" HeaderText="FF_Amt_9" SortExpression="FF_Amt_9"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_10" HeaderText="FF_Amt_10" SortExpression="FF_Amt_10"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_11" HeaderText="FF_Amt_11" SortExpression="FF_Amt_11"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_12" HeaderText="FF_Amt_12" SortExpression="FF_Amt_12"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_13" HeaderText="FF_Amt_13" SortExpression="FF_Amt_13"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_14" HeaderText="FF_Amt_14" SortExpression="FF_Amt_14"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_15" HeaderText="FF_Amt_15" SortExpression="FF_Amt_15"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_16" HeaderText="FF_Amt_16" SortExpression="FF_Amt_16"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_17" HeaderText="FF_Amt_17" SortExpression="FF_Amt_17"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_18" HeaderText="FF_Amt_18" SortExpression="FF_Amt_18"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_19" HeaderText="FF_Amt_19" SortExpression="FF_Amt_19"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_20" HeaderText="FF_Amt_10" SortExpression="FF_Amt_20"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_21" HeaderText="FF_Amt_21" SortExpression="FF_Amt_21"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_22" HeaderText="FF_Amt_22" SortExpression="FF_Amt_22"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_23" HeaderText="FF_Amt_23" SortExpression="FF_Amt_23"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_24" HeaderText="FF_Amt_24" SortExpression="FF_Amt_24"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_25" HeaderText="FF_Amt_25" SortExpression="FF_Amt_25"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_26" HeaderText="FF_Amt_26" SortExpression="FF_Amt_26"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_27" HeaderText="FF_Amt_27" SortExpression="FF_Amt_27"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_28" HeaderText="FF_Amt_28" SortExpression="FF_Amt_28"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_29" HeaderText="FF_Amt_29" SortExpression="FF_Amt_29"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_30" HeaderText="FF_Amt_30" SortExpression="FF_Amt_30"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_31" HeaderText="FF_Amt_31" SortExpression="FF_Amt_31"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_32" HeaderText="FF_Amt_32" SortExpression="FF_Amt_32"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_33" HeaderText="FF_Amt_33" SortExpression="FF_Amt_33"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_34" HeaderText="FF_Amt_34" SortExpression="FF_Amt_34"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_35" HeaderText="FF_Amt_35" SortExpression="FF_Amt_35"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_36" HeaderText="FF_Amt_36" SortExpression="FF_Amt_36"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_37" HeaderText="FF_Amt_37" SortExpression="FF_Amt_37"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_38" HeaderText="FF_Amt_38" SortExpression="FF_Amt_38"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_39" HeaderText="FF_Amt_39" SortExpression="FF_Amt_39"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_40" HeaderText="FF_Amt_40" SortExpression="FF_Amt_40"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_41" HeaderText="FF_Amt_41" SortExpression="FF_Amt_41"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_42" HeaderText="FF_Amt_42" SortExpression="FF_Amt_42"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_43" HeaderText="FF_Amt_43" SortExpression="FF_Amt_43"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_44" HeaderText="FF_Amt_44" SortExpression="FF_Amt_44"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_45" HeaderText="FF_Amt_45" SortExpression="FF_Amt_45"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_Amt_46" HeaderText="FF_Amt_46" SortExpression="FF_Amt_46"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_47" HeaderText="FF_Amt_47" SortExpression="FF_Amt_47"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_48" HeaderText="FF_Amt_48" SortExpression="FF_Amt_48"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_49" HeaderText="FF_Amt_49" SortExpression="FF_Amt_49"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="FF_Amt_50" HeaderText="FF_Amt_50" SortExpression="FF_Amt_50"
                                    HtmlEncode="false" DataFormatString="{0:N0}">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_1" HeaderText="FF_txt_1" SortExpression="FF_txt_1">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_2" HeaderText="FF_txt_2" SortExpression="FF_txt_2">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_3" HeaderText="FF_txt_3" SortExpression="FF_txt_3">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_4" HeaderText="FF_txt_4" SortExpression="FF_txt_4">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_5" HeaderText="FF_txt_5" SortExpression="FF_txt_5">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_6" HeaderText="FF_txt_6" SortExpression="FF_txt_6">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_7" HeaderText="FF_txt_7" SortExpression="FF_txt_7">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_8" HeaderText="FF_txt_8" SortExpression="FF_txt_8">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_9" HeaderText="FF_txt_9" SortExpression="FF_txt_9">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_10" HeaderText="FF_txt_10" SortExpression="FF_txt_10">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_11" HeaderText="FF_txt_11" SortExpression="FF_txt_10">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_12" HeaderText="FF_txt_12" SortExpression="FF_txt_12">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_13" HeaderText="FF_txt_13" SortExpression="FF_txt_13">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_14" HeaderText="FF_txt_14" SortExpression="FF_txt_14">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_15" HeaderText="FF_txt_15" SortExpression="FF_txt_15">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_16" HeaderText="FF_txt_16" SortExpression="FF_txt_16">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_17" HeaderText="FF_txt_17" SortExpression="FF_txt_17">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_18" HeaderText="FF_txt_18" SortExpression="FF_txt_18">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_19" HeaderText="FF_txt_19" SortExpression="FF_txt_19">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_20" HeaderText="FF_txt_20" SortExpression="FF_txt_20">
                                </telerik:GridBoundColumn>
                               <telerik:GridBoundColumn DataField="FF_txt_21" HeaderText="FF_txt_21" SortExpression="FF_txt_21">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_22" HeaderText="FF_txt_22" SortExpression="FF_txt_22">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_23" HeaderText="FF_txt_23" SortExpression="FF_txt_23">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_24" HeaderText="FF_txt_24" SortExpression="FF_txt_24">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_25" HeaderText="FF_txt_25" SortExpression="FF_txt_25">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_26" HeaderText="FF_txt_26" SortExpression="FF_txt_26">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_27" HeaderText="FF_txt_27" SortExpression="FF_txt_27">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_28" HeaderText="FF_txt_28" SortExpression="FF_txt_28">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_29" HeaderText="FF_txt_29" SortExpression="FF_txt_29">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_30" HeaderText="FF_txt_30" SortExpression="FF_txt_30">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_31" HeaderText="FF_txt_31" SortExpression="FF_txt_31">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_32" HeaderText="FF_txt_32" SortExpression="FF_txt_32">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_33" HeaderText="FF_txt_33" SortExpression="FF_txt_33">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_34" HeaderText="FF_txt_34" SortExpression="FF_txt_34">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_35" HeaderText="FF_txt_35" SortExpression="FF_txt_35">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_36" HeaderText="FF_txt_36" SortExpression="FF_txt_36">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_37" HeaderText="FF_txt_7" SortExpression="FF_txt_37">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_38" HeaderText="FF_txt_38" SortExpression="FF_txt_38">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_39" HeaderText="FF_txt_39" SortExpression="FF_txt_39">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_40" HeaderText="FF_txt_40" SortExpression="FF_txt_40">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_41" HeaderText="FF_txt_41" SortExpression="FF_txt_41">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_42" HeaderText="FF_txt_42" SortExpression="FF_txt_42">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_43" HeaderText="FF_txt_43" SortExpression="FF_txt_43">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_44" HeaderText="FF_txt_44" SortExpression="FF_txt_44">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_45" HeaderText="FF_txt_45" SortExpression="FF_txt_45">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_46" HeaderText="FF_txt_46" SortExpression="FF_txt_46">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_47" HeaderText="FF_txt_47" SortExpression="FF_txt_47">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_48" HeaderText="FF_txt_48" SortExpression="FF_txt_48">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_49" HeaderText="FF_txt_49" SortExpression="FF_txt_49">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_50" HeaderText="FF_txt_50" SortExpression="FF_txt_50">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_51" HeaderText="FF_txt_51" SortExpression="FF_txt_51">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_52" HeaderText="FF_txt_52" SortExpression="FF_txt_52">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_53" HeaderText="FF_txt_53" SortExpression="FF_txt_53">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_54" HeaderText="FF_txt_54" SortExpression="FF_txt_54">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_55" HeaderText="FF_txt_55" SortExpression="FF_txt_55">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_56" HeaderText="FF_txt_56" SortExpression="FF_txt_56">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_57" HeaderText="FF_txt_57" SortExpression="FF_txt_57">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_58" HeaderText="FF_txt_58" SortExpression="FF_txt_58">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_59" HeaderText="FF_txt_59" SortExpression="FF_txt_59">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_60" HeaderText="FF_txt_60" SortExpression="FF_txt_60">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_61" HeaderText="FF_txt_61" SortExpression="FF_txt_61">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_62" HeaderText="FF_txt_62" SortExpression="FF_txt_62">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_63" HeaderText="FF_txt_63" SortExpression="FF_txt_63">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_64" HeaderText="FF_txt_64" SortExpression="FF_txt_64">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_65" HeaderText="FF_txt_65" SortExpression="FF_txt_65">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_66" HeaderText="FF_txt_66" SortExpression="FF_txt_66">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_67" HeaderText="FF_txt_67" SortExpression="FF_txt_67">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_68" HeaderText="FF_txt_68" SortExpression="FF_txt_68">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_69" HeaderText="FF_txt_69" SortExpression="FF_txt_69">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_70" HeaderText="FF_txt_70" SortExpression="FF_txt_70">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_71" HeaderText="FF_txt_71" SortExpression="FF_txt_71">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_72" HeaderText="FF_txt_72" SortExpression="FF_txt_72">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_73" HeaderText="FF_txt_73" SortExpression="FF_txt_73">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_74" HeaderText="FF_txt_74" SortExpression="FF_txt_74">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_75" HeaderText="FF_txt_75" SortExpression="FF_txt_75">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_76" HeaderText="FF_txt_76" SortExpression="FF_txt_76">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_77" HeaderText="FF_txt_77" SortExpression="FF_txt_77">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_78" HeaderText="FF_txt_78" SortExpression="FF_txt_78">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_79" HeaderText="FF_txt_79" SortExpression="FF_txt_79">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_80" HeaderText="FF_txt_80" SortExpression="FF_txt_80">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_81" HeaderText="FF_txt_81" SortExpression="FF_txt_81">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_82" HeaderText="FF_txt_82" SortExpression="FF_txt_82">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_83" HeaderText="FF_txt_83" SortExpression="FF_txt_83">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_84" HeaderText="FF_txt_84" SortExpression="FF_txt_84">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_85" HeaderText="FF_txt_85" SortExpression="FF_txt_85">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_86" HeaderText="FF_txt_86" SortExpression="FF_txt_86">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_87" HeaderText="FF_txt_87" SortExpression="FF_txt_87">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_88" HeaderText="FF_txt_88" SortExpression="FF_txt_88">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_89" HeaderText="FF_txt_89" SortExpression="FF_txt_89">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_90" HeaderText="FF_txt_90" SortExpression="FF_txt_90">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_91" HeaderText="FF_txt_91" SortExpression="FF_txt_91">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_92" HeaderText="FF_txt_92" SortExpression="FF_txt_92">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_93" HeaderText="FF_txt_93" SortExpression="FF_txt_93">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_94" HeaderText="FF_txt_94" SortExpression="FF_txt_94">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_95" HeaderText="FF_txt_95" SortExpression="FF_txt_95">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_96" HeaderText="FF_txt_96" SortExpression="FF_txt_96">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_97" HeaderText="FF_txt_97" SortExpression="FF_txt_97">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_98" HeaderText="FF_txt_98" SortExpression="FF_txt_98">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_99" HeaderText="FF_txt_99" SortExpression="FF_txt_99">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FF_txt_100" HeaderText="FF_txt_100" SortExpression="FF_txt_100">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_Amount" HeaderText="Detail_Amount" SortExpression="Detail_Amount">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_BeneAccountNo" HeaderText="Detail_BeneAccountNo" SortExpression="Detail_BeneAccountNo">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_BeneAddress" HeaderText="Detail_BeneAddress" SortExpression="Detail_BeneAddress">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_BeneBankName" HeaderText="Detail_BeneBankName" SortExpression="Detail_BeneBankName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_BeneBranchAddress" HeaderText="Detail_BeneBranchAddress" SortExpression="Detail_BeneBranchAddress">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_BeneBranchCode" HeaderText="Detail_BeneBranchCode" SortExpression="Detail_BeneBranchCode">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_BeneBranchName" HeaderText="Detail_BeneBranchName" SortExpression="Detail_BeneBranchName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_BeneCNIC" HeaderText="Detail_BeneCNIC" SortExpression="Detail_BeneCNIC">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_BeneCountry" HeaderText="Detail_BeneCountry" SortExpression="Detail_BeneCountry">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_BeneProvince" HeaderText="Detail_BeneProvince" SortExpression="Detail_BeneProvince">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_BeneCity" HeaderText="Detail_BeneCity" SortExpression="Detail_BeneCity">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_BeneEmail" HeaderText="Detail_BeneEmail" SortExpression="Detail_BeneEmail">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_BeneMobile" HeaderText="Detail_BeneMobile" SortExpression="Detail_BeneMobile">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_BenePhone" HeaderText="Detail_BenePhone" SortExpression="Detail_BenePhone">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_InvoiceNo" HeaderText="Detail_InvoiceNo" SortExpression="Detail_InvoiceNo">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_IS_NO" HeaderText="Detail_IS_NO" SortExpression="Detail_IS_NO">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_TXN_CODE" HeaderText="Detail_TXN_CODE" SortExpression="Detail_TXN_CODE">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_DetailAmt" HeaderText="Detail_DetailAmt" SortExpression="Detail_DetailAmt">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_1" HeaderText="Detail_FF_Text_1" SortExpression="Detail_FF_Text_1">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_2" HeaderText="Detail_FF_Text_2" SortExpression="Detail_FF_Text_2">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_3" HeaderText="Detail_FF_Text_3" SortExpression="Detail_FF_Text_3">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_4" HeaderText="Detail_FF_Text_4" SortExpression="Detail_FF_Text_4">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_5" HeaderText="Detail_FF_Text_5" SortExpression="Detail_FF_Text_5">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_6" HeaderText="Detail_FF_Text_6" SortExpression="Detail_FF_Text_6">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_7" HeaderText="Detail_FF_Text_7" SortExpression="Detail_FF_Text_7">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_8" HeaderText="Detail_FF_Text_8" SortExpression="Detail_FF_Text_8">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_9" HeaderText="Detail_FF_Text_9" SortExpression="Detail_FF_Text_9">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_10" HeaderText="Detail_FF_Text_10" SortExpression="Detail_FF_Text_10">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_11" HeaderText="Detail_FF_Text_11" SortExpression="Detail_FF_Text_11">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_12" HeaderText="Detail_FF_Text_12" SortExpression="Detail_FF_Text_12">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_13" HeaderText="Detail_FF_Text_13" SortExpression="Detail_FF_Text_13">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_14" HeaderText="Detail_FF_Text_14" SortExpression="Detail_FF_Text_14">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_15" HeaderText="Detail_FF_Text_15" SortExpression="Detail_FF_Text_15">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_16" HeaderText="Detail_FF_Text_16" SortExpression="Detail_FF_Text_16">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_17" HeaderText="Detail_FF_Text_17" SortExpression="Detail_FF_Text_17">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_18" HeaderText="Detail_FF_Text_18" SortExpression="Detail_FF_Text_18">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_19" HeaderText="Detail_FF_Text_19" SortExpression="Detail_FF_Text_19">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_20" HeaderText="Detail_FF_Text_20" SortExpression="Detail_FF_Text_20">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="Detail_FF_Text_21" HeaderText="Detail_FF_Text_21" SortExpression="Detail_FF_Text_21">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_22" HeaderText="Detail_FF_Text_22" SortExpression="Detail_FF_Text_22">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_23" HeaderText="Detail_FF_Text_23" SortExpression="Detail_FF_Text_23">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_24" HeaderText="Detail_FF_Text_24" SortExpression="Detail_FF_Text_24">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_25" HeaderText="Detail_FF_Text_25" SortExpression="Detail_FF_Text_25">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_26" HeaderText="Detail_FF_Text_26" SortExpression="Detail_FF_Text_26">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_27" HeaderText="Detail_FF_Text_27" SortExpression="Detail_FF_Text_27">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_28" HeaderText="Detail_FF_Text_28" SortExpression="Detail_FF_Text_28">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_29" HeaderText="Detail_FF_Text_29" SortExpression="Detail_FF_Text_29">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_30" HeaderText="Detail_FF_Text_30" SortExpression="Detail_FF_Text_30">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="Detail_FF_Text_31" HeaderText="Detail_FF_Text_31" SortExpression="Detail_FF_Text_31">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_32" HeaderText="Detail_FF_Text_32" SortExpression="Detail_FF_Text_32">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_33" HeaderText="Detail_FF_Text_33" SortExpression="Detail_FF_Text_33">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_34" HeaderText="Detail_FF_Text_34" SortExpression="Detail_FF_Text_34">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_35" HeaderText="Detail_FF_Text_35" SortExpression="Detail_FF_Text_35">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_36" HeaderText="Detail_FF_Text_36" SortExpression="Detail_FF_Text_36">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_37" HeaderText="Detail_FF_Text_37" SortExpression="Detail_FF_Text_37">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_38" HeaderText="Detail_FF_Text_38" SortExpression="Detail_FF_Text_38">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_39" HeaderText="Detail_FF_Text_39" SortExpression="Detail_FF_Text_39">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_40" HeaderText="Detail_FF_Text_40" SortExpression="Detail_FF_Text_40">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="Detail_FF_Text_41" HeaderText="Detail_FF_Text_41" SortExpression="Detail_FF_Text_41">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_42" HeaderText="Detail_FF_Text_42" SortExpression="Detail_FF_Text_42">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_43" HeaderText="Detail_FF_Text_43" SortExpression="Detail_FF_Text_43">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_44" HeaderText="Detail_FF_Text_44" SortExpression="Detail_FF_Text_44">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_45" HeaderText="Detail_FF_Text_45" SortExpression="Detail_FF_Text_45">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_46" HeaderText="Detail_FF_Text_46" SortExpression="Detail_FF_Text_46">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_47" HeaderText="Detail_FF_Text_47" SortExpression="Detail_FF_Text_47">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_48" HeaderText="Detail_FF_Text_48" SortExpression="Detail_FF_Text_48">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_49" HeaderText="Detail_FF_Text_49" SortExpression="Detail_FF_Text_49">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_50" HeaderText="Detail_FF_Text_50" SortExpression="Detail_FF_Text_50">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="Detail_FF_Text_51" HeaderText="Detail_FF_Text_51" SortExpression="Detail_FF_Text_51">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_52" HeaderText="Detail_FF_Text_52" SortExpression="Detail_FF_Text_52">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_53" HeaderText="Detail_FF_Text_53" SortExpression="Detail_FF_Text_53">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_54" HeaderText="Detail_FF_Text_54" SortExpression="Detail_FF_Text_54">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_55" HeaderText="Detail_FF_Text_55" SortExpression="Detail_FF_Text_55">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_56" HeaderText="Detail_FF_Text_56" SortExpression="Detail_FF_Text_56">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_57" HeaderText="Detail_FF_Text_57" SortExpression="Detail_FF_Text_57">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_58" HeaderText="Detail_FF_Text_58" SortExpression="Detail_FF_Text_58">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_59" HeaderText="Detail_FF_Text_59" SortExpression="Detail_FF_Text_59">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_60" HeaderText="Detail_FF_Text_60" SortExpression="Detail_FF_Text_60">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="Detail_FF_Text_61" HeaderText="Detail_FF_Text_61" SortExpression="Detail_FF_Text_61">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_62" HeaderText="Detail_FF_Text_62" SortExpression="Detail_FF_Text_62">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_63" HeaderText="Detail_FF_Text_63" SortExpression="Detail_FF_Text_63">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_64" HeaderText="Detail_FF_Text_64" SortExpression="Detail_FF_Text_64">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_65" HeaderText="Detail_FF_Text_65" SortExpression="Detail_FF_Text_65">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_66" HeaderText="Detail_FF_Text_66" SortExpression="Detail_FF_Text_66">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_67" HeaderText="Detail_FF_Text_67" SortExpression="Detail_FF_Text_67">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_68" HeaderText="Detail_FF_Text_68" SortExpression="Detail_FF_Text_68">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_69" HeaderText="Detail_FF_Text_69" SortExpression="Detail_FF_Text_69">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_70" HeaderText="Detail_FF_Text_70" SortExpression="Detail_FF_Text_70">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="Detail_FF_Text_71" HeaderText="Detail_FF_Text_71" SortExpression="Detail_FF_Text_71">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_72" HeaderText="Detail_FF_Text_72" SortExpression="Detail_FF_Text_72">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_73" HeaderText="Detail_FF_Text_73" SortExpression="Detail_FF_Text_73">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_74" HeaderText="Detail_FF_Text_74" SortExpression="Detail_FF_Text_74">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_75" HeaderText="Detail_FF_Text_75" SortExpression="Detail_FF_Text_75">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_76" HeaderText="Detail_FF_Text_76" SortExpression="Detail_FF_Text_76">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_77" HeaderText="Detail_FF_Text_77" SortExpression="Detail_FF_Text_77">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_78" HeaderText="Detail_FF_Text_78" SortExpression="Detail_FF_Text_78">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_79" HeaderText="Detail_FF_Text_79" SortExpression="Detail_FF_Text_79">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_80" HeaderText="Detail_FF_Text_80" SortExpression="Detail_FF_Text_80">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="Detail_FF_Text_81" HeaderText="Detail_FF_Text_81" SortExpression="Detail_FF_Text_81">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_82" HeaderText="Detail_FF_Text_82" SortExpression="Detail_FF_Text_82">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_83" HeaderText="Detail_FF_Text_83" SortExpression="Detail_FF_Text_83">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_84" HeaderText="Detail_FF_Text_84" SortExpression="Detail_FF_Text_84">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_85" HeaderText="Detail_FF_Text_85" SortExpression="Detail_FF_Text_85">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_86" HeaderText="Detail_FF_Text_86" SortExpression="Detail_FF_Text_86">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_87" HeaderText="Detail_FF_Text_87" SortExpression="Detail_FF_Text_87">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_88" HeaderText="Detail_FF_Text_88" SortExpression="Detail_FF_Text_88">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_89" HeaderText="Detail_FF_Text_89" SortExpression="Detail_FF_Text_89">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_90" HeaderText="Detail_FF_Text_90" SortExpression="Detail_FF_Text_90">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="Detail_FF_Text_91" HeaderText="Detail_FF_Text_91" SortExpression="Detail_FF_Text_91">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_92" HeaderText="Detail_FF_Text_92" SortExpression="Detail_FF_Text_92">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_93" HeaderText="Detail_FF_Text_93" SortExpression="Detail_FF_Text_93">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_94" HeaderText="Detail_FF_Text_94" SortExpression="Detail_FF_Text_94">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_95" HeaderText="Detail_FF_Text_95" SortExpression="Detail_FF_Text_95">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_96" HeaderText="Detail_FF_Text_96" SortExpression="Detail_FF_Text_96">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_97" HeaderText="Detail_FF_Text_97" SortExpression="Detail_FF_Text_97">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_98" HeaderText="Detail_FF_Text_8" SortExpression="Detail_FF_Text_98">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_99" HeaderText="Detail_FF_Text_9" SortExpression="Detail_FF_Text_99">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Text_100" HeaderText="Detail_FF_Text_100" SortExpression="Detail_FF_Text_100">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_1" HeaderText="Detail_FF_Amt_1" SortExpression="Detail_FF_Amt_1">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_2" HeaderText="Detail_FF_Amt_2" SortExpression="Detail_FF_Amt_2">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_3" HeaderText="Detail_FF_Amt_3" SortExpression="Detail_FF_Amt_3">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_4" HeaderText="Detail_FF_Amt_4" SortExpression="Detail_FF_Amt_4">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_5" HeaderText="Detail_FF_Amt_5" SortExpression="Detail_FF_Amt_5">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_6" HeaderText="Detail_FF_Amt_6" SortExpression="Detail_FF_Amt_6">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_7" HeaderText="Detail_FF_Amt_7" SortExpression="Detail_FF_Amt_7">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_8" HeaderText="Detail_FF_Amt_8" SortExpression="Detail_FF_Amt_8">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_9" HeaderText="Detail_FF_Amt_9" SortExpression="Detail_FF_Amt_9">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_10" HeaderText="Detail_FF_Amt_10" SortExpression="Detail_FF_Amt_10">
                                </telerik:GridBoundColumn>
                                  <telerik:GridBoundColumn DataField="Detail_FF_Amt_11" HeaderText="Detail_FF_Amt_11" SortExpression="Detail_FF_Amt_11">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_12" HeaderText="Detail_FF_Amt_12" SortExpression="Detail_FF_Amt_12">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_13" HeaderText="Detail_FF_Amt_13" SortExpression="Detail_FF_Amt_13">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_14" HeaderText="Detail_FF_Amt_14" SortExpression="Detail_FF_Amt_14">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_15" HeaderText="Detail_FF_Amt_15" SortExpression="Detail_FF_Amt_15">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_16" HeaderText="Detail_FF_Amt_16" SortExpression="Detail_FF_Amt_16">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_17" HeaderText="Detail_FF_Amt_17" SortExpression="Detail_FF_Amt_17">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_18" HeaderText="Detail_FF_Amt_18" SortExpression="Detail_FF_Amt_18">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_19" HeaderText="Detail_FF_Amt_19" SortExpression="Detail_FF_Amt_19">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_20" HeaderText="Detail_FF_Amt_20" SortExpression="Detail_FF_Amt_20">
                                </telerik:GridBoundColumn>
                                  <telerik:GridBoundColumn DataField="Detail_FF_Amt_21" HeaderText="Detail_FF_Amt_21" SortExpression="Detail_FF_Amt_21">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_22" HeaderText="Detail_FF_Amt_22" SortExpression="Detail_FF_Amt_22">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_23" HeaderText="Detail_FF_Amt_23" SortExpression="Detail_FF_Amt_23">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_24" HeaderText="Detail_FF_Amt_24" SortExpression="Detail_FF_Amt_24">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_25" HeaderText="Detail_FF_Amt_25" SortExpression="Detail_FF_Amt_25">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_26" HeaderText="Detail_FF_Amt_26" SortExpression="Detail_FF_Amt_26">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_27" HeaderText="Detail_FF_Amt_27" SortExpression="Detail_FF_Amt_27">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_28" HeaderText="Detail_FF_Amt_28" SortExpression="Detail_FF_Amt_28">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_29" HeaderText="Detail_FF_Amt_29" SortExpression="Detail_FF_Amt_29">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_30" HeaderText="Detail_FF_Amt_30" SortExpression="Detail_FF_Amt_30">
                                </telerik:GridBoundColumn>
                                  <telerik:GridBoundColumn DataField="Detail_FF_Amt_31" HeaderText="Detail_FF_Amt_31" SortExpression="Detail_FF_Amt_31">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_32" HeaderText="Detail_FF_Amt_32" SortExpression="Detail_FF_Amt_32">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_33" HeaderText="Detail_FF_Amt_33" SortExpression="Detail_FF_Amt_33">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_34" HeaderText="Detail_FF_Amt_34" SortExpression="Detail_FF_Amt_34">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_35" HeaderText="Detail_FF_Amt_35" SortExpression="Detail_FF_Amt_35">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_36" HeaderText="Detail_FF_Amt_36" SortExpression="Detail_FF_Amt_36">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_37" HeaderText="Detail_FF_Amt_37" SortExpression="Detail_FF_Amt_37">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_38" HeaderText="Detail_FF_Amt_38" SortExpression="Detail_FF_Amt_38">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_39" HeaderText="Detail_FF_Amt_39" SortExpression="Detail_FF_Amt_39">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_40" HeaderText="Detail_FF_Amt_40" SortExpression="Detail_FF_Amt_40">
                                </telerik:GridBoundColumn>
                                 <telerik:GridBoundColumn DataField="Detail_FF_Amt_41" HeaderText="Detail_FF_Amt_41" SortExpression="Detail_FF_Amt_41">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_42" HeaderText="Detail_FF_Amt_42" SortExpression="Detail_FF_Amt_42">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_43" HeaderText="Detail_FF_Amt_43" SortExpression="Detail_FF_Amt_43">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_44" HeaderText="Detail_FF_Amt_44" SortExpression="Detail_FF_Amt_44">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_45" HeaderText="Detail_FF_Amt_45" SortExpression="Detail_FF_Amt_45">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_46" HeaderText="Detail_FF_Amt_6" SortExpression="Detail_FF_Amt_6">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_47" HeaderText="Detail_FF_Amt_47" SortExpression="Detail_FF_Amt_47">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_48" HeaderText="Detail_FF_Amt_48" SortExpression="Detail_FF_Amt_48">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_49" HeaderText="Detail_FF_Amt_49" SortExpression="Detail_FF_Amt_49">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Detail_FF_Amt_50" HeaderText="Detail_FF_Amt_50" SortExpression="Detail_FF_Amt_50">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                          <HeaderStyle CssClass="GridHeader"/>
                            <ItemStyle CssClass="GridItem" />
                            <AlternatingItemStyle CssClass="GridItem" />
                    </telerik:RadGrid>
                </asp:Panel>
            </td>
        </tr>
       
    </table>
    </form>
</body>
</html>
