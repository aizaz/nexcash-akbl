﻿
Partial Class ErrorPages_Oops
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        Try

            If Session("AuthToken") Is Nothing Then
                Response.Redirect(NavigateURL("Login"))
            End If

        Catch ex As Exception
            Response.Redirect(NavigateURL("Login"))
        End Try

    End Sub
End Class
