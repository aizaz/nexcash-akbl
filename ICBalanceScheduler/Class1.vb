﻿Public Class BalanceScheduler
    Inherits DotNetNuke.Services.Scheduling.SchedulerClient

 
    Public Sub New(ByVal objSceduleHistoryItem As DotNetNuke.Services.Scheduling.ScheduleHistoryItem)
        MyBase.New()
        Me.ScheduleHistoryItem = objSceduleHistoryItem
    End Sub
    Public Overrides Sub DoWork()

        Try
            Dim CurrentTime As DateTime = Date.Now.Hour & ":" & Date.Now.Minute
            Dim ValidateTime As DateTime = Convert.ToDateTime("7:00 PM").Hour & ":" & Convert.ToDateTime("7:00 PM").Minute


            Me.Progressing()



            'call functions here


            If CurrentTime >= ValidateTime Then
                ICBO.IC.ICTaggedClientUserForAccountController.SendDailyAccountBalanceToTaggedClientUser()
            End If


            If CurrentTime >= Convert.ToDateTime("9:30 AM").Hour & ":" & Convert.ToDateTime("9:30 AM").Minute Then
                ICBO.IC.ICPdcController.AutoSendEmailForPDCClearingUser()
                ICBO.IC.ICPdcController.UpdatePDCNotificationSentDate()

            End If


            Me.ScheduleHistoryItem.AddLogNote("IC_BalanceScheduler Service Start. Success.")

            Me.ScheduleHistoryItem.Succeeded = True




        Catch ex As Exception

            Me.ScheduleHistoryItem.Succeeded = False
            Me.ScheduleHistoryItem.AddLogNote("IC_BalanceScheduler Service Start. Failed. " + ex.ToString())

            Me.Errored(ex)
        End Try
    End Sub
End Class
