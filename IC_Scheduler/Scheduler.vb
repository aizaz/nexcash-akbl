﻿Public Class Scheduler
    Inherits DotNetNuke.Services.Scheduling.SchedulerClient

    Public Sub New(ByVal objSceduleHistoryItem As DotNetNuke.Services.Scheduling.ScheduleHistoryItem)
        MyBase.New()
        Me.ScheduleHistoryItem = objSceduleHistoryItem
    End Sub


    Public Overrides Sub DoWork()

        Try

            Me.Progressing()



            'call functions here


            ICBO.ICPackageManagementController.ApplyScheduledCharges()

            ICBO.ICPackageManagementController.GeneratePackageInvoices()







            Me.ScheduleHistoryItem.AddLogNote("IC_Scheduler Service Start. Success.")

            Me.ScheduleHistoryItem.Succeeded = True



        Catch ex As Exception


            Me.ScheduleHistoryItem.Succeeded = False
            Me.ScheduleHistoryItem.AddLogNote("IC_Scheduler Service Start. Failed. " + ex.ToString())

            Me.Errored(ex)




        End Try








    End Sub


End Class
