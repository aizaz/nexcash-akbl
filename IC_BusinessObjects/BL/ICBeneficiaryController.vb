﻿Namespace IC
    Public Class ICBeneficiaryController
        Public Shared Function AddBeneficiaryForInstruction(ByVal objICBeneficiary As ICBeneficiary, ByVal UsersID As String, ByVal UsersName As String) As Integer
            Dim objICBeneficiaryForSave As New ICBeneficiary
            Dim objICBeneficiaryExisting As New ICBeneficiary
            Dim objICBeneficiaryExistingColl As New ICBeneficiaryCollection
            Dim BenefeciryID As Integer = 0
            Dim ActionStr As String = ""
            objICBeneficiaryForSave.es.Connection.CommandTimeout = 3600
            objICBeneficiaryExistingColl.es.Connection.CommandTimeout = 3600
            If objICBeneficiary.BeneCellNumber <> "" And Not objICBeneficiary.BeneCellNumber Is Nothing Then
                objICBeneficiaryExistingColl = GetExistingBeneficiaryByMobNumber(objICBeneficiary.BeneCellNumber.ToString)
                If objICBeneficiaryExistingColl.Count > 0 Then
                    objICBeneficiaryExisting = objICBeneficiaryExistingColl(0)
                    objICBeneficiaryForSave.LoadByPrimaryKey(objICBeneficiaryExisting.BeneID)
                End If
            End If

            objICBeneficiaryForSave.BeneName = objICBeneficiary.BeneName
            objICBeneficiaryForSave.BeneAddress1 = objICBeneficiary.BeneAddress1
            objICBeneficiaryForSave.BeneIDNO = objICBeneficiary.BeneIDNO
            objICBeneficiaryForSave.BeneCellNumber = objICBeneficiary.BeneCellNumber
            objICBeneficiaryForSave.BeneCity = objICBeneficiary.BeneCity
            objICBeneficiaryForSave.BeneProvince = objICBeneficiary.BeneProvince
          
            objICBeneficiaryForSave.BeneAccountNumber = objICBeneficiary.BeneAccountNumber
            objICBeneficiaryForSave.BankName = objICBeneficiary.BankName
            objICBeneficiaryForSave.CreateBy = objICBeneficiary.CreateBy
            objICBeneficiaryForSave.CreateDate = objICBeneficiary.CreateDate
            objICBeneficiaryForSave.IsActive = objICBeneficiary.IsActive
            objICBeneficiaryForSave.Save()
            BenefeciryID = objICBeneficiaryForSave.BeneID

            ICUtilities.AddAuditTrail("Beneficiary  [ " & objICBeneficiaryForSave.BeneName & " ] added.", "Beneficiary", objICBeneficiaryForSave.BeneID.ToString, UsersID.ToString, UsersName.ToString, "ADD")

            Return BenefeciryID
        End Function
        Public Shared Function GetExistingBeneficiaryByMobNumber(ByVal MobNo As String) As ICBeneficiaryCollection
            Dim objICBeneficiaryColl As New ICBeneficiaryCollection
            objICBeneficiaryColl.es.Connection.CommandTimeout = 3600

            objICBeneficiaryColl.Query.Where(objICBeneficiaryColl.Query.BeneCellNumber = MobNo.ToString)
            objICBeneficiaryColl.Query.OrderBy(objICBeneficiaryColl.Query.BeneID, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            objICBeneficiaryColl.Query.Load()
            Return objICBeneficiaryColl
        End Function
    End Class

End Namespace

