﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Imports EntitySpaces.Core
Imports System

Public Class ICPackageManagementController

    Dim dt As New DataTable

    Public Shared Sub AddPackage(ByVal ICPackage As ICBO.IC.ICPackage, ByVal IsUpdate As Boolean, ByVal UserID As Integer, ByVal username As String, ByVal globalcharges As ArrayList, ByVal packageslabs As DataTable)



        Dim objPackageMangement As New IC.ICPackage



        objPackageMangement.es.Connection.CommandTimeout = 3600

        If IsUpdate = False Then
            objPackageMangement.CreatedBy = ICPackage.CreatedBy
            objPackageMangement.CreatedOn = ICPackage.CreatedOn

        Else
            objPackageMangement.LoadByPrimaryKey(ICPackage.PackageID)
            objPackageMangement.CreatedBy = ICPackage.CreatedBy
            objPackageMangement.CreatedOn = ICPackage.CreatedOn

        End If


        objPackageMangement.Title = ICPackage.Title
        objPackageMangement.DisbursementMode = ICPackage.DisbursementMode
        objPackageMangement.PackageType = ICPackage.PackageType

        If ICPackage.PackageType = "Fixed" Then


            objPackageMangement.PackageAmount = ICPackage.PackageAmount



        ElseIf ICPackage.PackageType = "Percentage of Instruction Amount" Then



            objPackageMangement.PackagePercentage = ICPackage.PackagePercentage

        ElseIf ICPackage.PackageType = "Percentage of Volume" Then

            objPackageMangement.PackagePercentage = ICPackage.PackagePercentage


        End If



        objPackageMangement.ChargeFrequency = ICPackage.ChargeFrequency
        objPackageMangement.IsActive = ICPackage.IsActive
        objPackageMangement.CreatedBy = objPackageMangement.CreatedBy
        objPackageMangement.CreatedOn = objPackageMangement.CreatedOn

        objPackageMangement.Save()


        If IsUpdate = True Then


            If objPackageMangement.ICPackagesGlobalChargesCollectionByPackageID.LoadAll Then


                objPackageMangement.ICPackagesGlobalChargesCollectionByPackageID.MarkAllAsDeleted()

                objPackageMangement.ICPackagesGlobalChargesCollectionByPackageID.Save()





            End If





        End If

        Dim count As Integer

        For count = 0 To globalcharges.Count - 1


            Dim Pgc As New IC.ICPackagesGlobalCharges

            Pgc.PackageID = objPackageMangement.PackageID
            Pgc.GlobalChargeID = Convert.ToInt32(globalcharges.Item(count))


            Pgc.Save()







        Next



        If ICPackage.PackageType = "Percentage of Volume" Then

            If IsUpdate = True Then


                If ICPackage.ICPackageSlabsCollectionByPackageID.LoadAll Then

                    ICPackage.ICPackageSlabsCollectionByPackageID.MarkAllAsDeleted()
                    ICPackage.ICPackageSlabsCollectionByPackageID.Save()




                End If


            End If


            If Not packageslabs Is Nothing Then


                For Each row As DataRow In packageslabs.Rows


                    Dim slab As New IC.ICPackageSlabs


                    slab.Min = row.Item("minvalue")
                    slab.Max = row.Item("maxvalue")
                    slab.Percentage = row.Item("percentage")
                    slab.PackageID = objPackageMangement.PackageID

                    slab.Save()









                Next



            End If


        End If




        ICPackage.PackageID = objPackageMangement.PackageID






        If (IsUpdate = False) Then
            ICUtilities.AddAuditTrail(" Added ", "Package", objPackageMangement.PackageID.ToString(), UserID.ToString(), username.ToString(), "ADD")
        Else
            ICUtilities.AddAuditTrail(" Updated ", "Package", objPackageMangement.PackageID.ToString(), UserID.ToString(), username.ToString(), "UPDATE")
        End If


    End Sub


    Public Shared Sub GetAllPackages(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)


        Dim qryObjICPackages As New IC.ICPackageQuery("qryICPackages")
        Dim packagecoll As New IC.ICPackageCollection



        qryObjICPackages.Select(qryObjICPackages.Title, qryObjICPackages.PackageID, qryObjICPackages.PackageType, qryObjICPackages.DisbursementMode, qryObjICPackages.IsActive)


        qryObjICPackages.OrderBy(qryObjICPackages.Title, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)





        If Not pagenumber = 0 Then

            qryObjICPackages.es.PageNumber = pagenumber
            qryObjICPackages.es.PageSize = pagesize

            packagecoll.Load(qryObjICPackages)

            rg.DataSource = packagecoll
            If DoDataBind Then

                rg.DataBind()
            End If
        Else


            qryObjICPackages.es.PageNumber = 1
            qryObjICPackages.es.PageSize = pagesize


            packagecoll.Load(qryObjICPackages)

            rg.DataSource = packagecoll

            If DoDataBind Then


                rg.DataBind()

            End If
        End If
    End Sub

    Public Shared Sub DeletePackagesByPackageID(ByVal PackageID As String, ByVal userid As String, ByVal username As String)
        Dim objICPackages As New IC.ICPackage
        Dim strPackageID, StrPackage As String
        objICPackages.es.Connection.CommandTimeout = 3600

        If objICPackages.LoadByPrimaryKey(PackageID) Then
            strPackageID = objICPackages.PackageID.ToString
            StrPackage = objICPackages.CreatedOn.ToString


            If objICPackages.ICPackagesGlobalChargesCollectionByPackageID.LoadAll Then

                objICPackages.ICPackagesGlobalChargesCollectionByPackageID.MarkAllAsDeleted()
                objICPackages.ICPackagesGlobalChargesCollectionByPackageID.Save()






            End If



            If objICPackages.ICPackageSlabsCollectionByPackageID.LoadAll Then

                objICPackages.ICPackageSlabsCollectionByPackageID.MarkAllAsDeleted()
                objICPackages.ICPackageSlabsCollectionByPackageID.Save()






            End If


            objICPackages.MarkAsDeleted()
            objICPackages.Save()
            ICUtilities.AddAuditTrail("Package(s): " & StrPackage & " deleted.", "Package", PackageID.ToString(), userid.ToString, username.ToString, "DELETE")
        End If
    End Sub

    Public Shared Function GetAllActivePackageByID(ByVal DisbursementMode As String) As IC.ICPackageCollection
        Dim ICPackageColl As New IC.ICPackageCollection

        ICPackageColl.es.Connection.CommandTimeout = 3600

        ICPackageColl.Query.Where(ICPackageColl.Query.DisbursementMode = DisbursementMode And ICPackageColl.Query.IsActive = True)
        ICPackageColl.Query.OrderBy(ICPackageColl.Query.Title.Ascending)
        ICPackageColl.Query.Load()
        Return ICPackageColl
    End Function

    Public Shared Function GetPackage(ByVal packageid As Integer) As IC.ICPackage



        Dim package As New IC.ICPackage

        If package.LoadByPrimaryKey(packageid) Then

            Return package

        Else

            Return Nothing



        End If


    End Function

    Public Shared Function GetPackageCharges(ByVal packageid As Integer) As IC.ICPackagesGlobalChargesCollection



        Dim packages As New IC.ICPackagesGlobalChargesCollection

        packages.Query.Where(packages.Query.PackageID = packageid)





        If packages.Query.Load Then

            Return packages

        Else

            Return Nothing



        End If


    End Function


    Public Shared Function GetPackageSlabs(ByVal packageid As Integer) As IC.ICPackageSlabsCollection




        Dim packages As New IC.ICPackageSlabsCollection

        packages.Query.Where(packages.Query.PackageID = packageid)





        If packages.Query.Load Then

            Return packages

        Else

            Return Nothing



        End If


    End Function


    Public Shared Sub ApplyCharges(ByVal InstructionId As Integer, ByVal InstructionStatus As Integer)




        Dim isinstructioncompleted As Boolean = False


        Select Case InstructionStatus


            Case 15


                isinstructioncompleted = True



            Case 21


                isinstructioncompleted = True

            Case 48

                isinstructioncompleted = True

        End Select


        If isinstructioncompleted = False Then


            Exit Sub




        End If




        Dim instruction As New IC.ICInstruction

        instruction.LoadByPrimaryKey(InstructionId)




       




        Dim acpnpt As New IC.ICAccountsPaymentNatureProductType


        If acpnpt.LoadByPrimaryKey(instruction.ClientAccountNo, instruction.ClientAccountBranchCode, instruction.ClientAccountCurrency, instruction.PaymentNatureCode, instruction.ProductTypeCode) Then


            If acpnpt.PackageId.HasValue = False Then


                'no package tagged, exit


                Exit Sub

            Else

                Dim package As New IC.ICPackage

                package.LoadByPrimaryKey(acpnpt.PackageId)


                If Not package.PackageType = "Percentage of Instruction Amount" Then


                    Exit Sub

                Else

                    Dim chargepercent As Integer = package.PackagePercentage

                    Dim instructionamount As Decimal = instruction.Amount


                    Dim chargeamount As Decimal = instructionamount * (chargepercent / 100)



                    Dim packagecharge As New IC.ICPackageCharges

                    packagecharge.AccountNumber = instruction.ClientAccountNo
                    packagecharge.Currency = instruction.ClientAccountCurrency
                    packagecharge.BranchCode = instruction.ClientAccountBranchCode
                    packagecharge.PaymentNatureCode = instruction.PaymentNatureCode
                    packagecharge.ProductTypeCode = instruction.ProductTypeCode
                    packagecharge.PacakgeId = package.PackageID
                    packagecharge.ChargeType = "PackageCharges"
                    packagecharge.ChargeAmount = chargeamount
                    packagecharge.InstructionID = InstructionId
                    packagecharge.ChargeDate = Now



                    packagecharge.Save()




                    If package.ICPackagesGlobalChargesCollectionByPackageID.LoadAll Then


                        For Each pgc As IC.ICPackagesGlobalCharges In package.ICPackagesGlobalChargesCollectionByPackageID


                            Dim gchargeamount As Decimal

                            If pgc.UpToICGlobalChargesByGlobalChargeID.ChargeType = "Fixed" Then


                                gchargeamount = pgc.UpToICGlobalChargesByGlobalChargeID.ChargeAmount




                            Else


                                gchargeamount = chargeamount * (pgc.UpToICGlobalChargesByGlobalChargeID.ChargePercentage / 100)




                            End If




                            packagecharge = New IC.ICPackageCharges

                            packagecharge.AccountNumber = instruction.ClientAccountNo
                            packagecharge.Currency = instruction.ClientAccountCurrency
                            packagecharge.BranchCode = instruction.ClientAccountBranchCode
                            packagecharge.PaymentNatureCode = instruction.PaymentNatureCode
                            packagecharge.ProductTypeCode = instruction.ProductTypeCode
                            packagecharge.PacakgeId = package.PackageID
                            packagecharge.ChargeType = "GlobalCharges"
                            packagecharge.GlobalChargeId = pgc.GlobalChargeID
                            packagecharge.ChargeAmount = gchargeamount
                            packagecharge.InstructionID = InstructionId
                            packagecharge.ChargeDate = Now



                            packagecharge.Save()



                        Next




                    End If








                End If






            End If


        End If










    End Sub

    Public Shared Sub ApplyScheduledCharges()


        Dim packagecoll As New IC.ICPackageCollection

        packagecoll.Query.Where(packagecoll.Query.IsActive = True, packagecoll.Query.PackageType.NotIn("Percentage of Instruction Amount"))


        If packagecoll.Query.Load Then


            For Each package As IC.ICPackage In packagecoll

                Dim applycharges As Boolean = False
                Dim chargefromdate, chargetodate As Date





                If package.LastScheduleRunDate.HasValue = False Then


                    package.LastScheduleRunDate = Now.Date


                    packagecoll.Save()


                    applycharges = False




                Else


                    chargefromdate = package.LastScheduleRunDate.Value.Date




                    Select Case package.ChargeFrequency


                        Case "Weekly"

                            chargetodate = chargefromdate.AddDays(7)



                        Case "Monthly"

                            chargetodate = chargefromdate.AddMonths(1)



                        Case "Quarterly"



                            chargetodate = chargefromdate.AddMonths(3)









                    End Select


                    If chargetodate.Date <= Now.Date Then


                        applycharges = True

                    Else

                        applycharges = False




                    End If



                End If


                If applycharges = True Then


                    ApplyPackageCharges(package.PackageID, chargefromdate, chargetodate)






                End If



            Next





        End If







    End Sub

    Private Shared Sub ApplyPackageCharges(ByVal packageid As Integer, ByVal chargefrom As Date, ByVal chargeto As Date)


        Dim package As New IC.ICPackage

        package.LoadByPrimaryKey(packageid)


        If package.ICAccountsPaymentNatureProductTypeCollectionByPackageId.LoadAll Then


            For Each acpn As IC.ICAccountsPaymentNatureProductType In package.ICAccountsPaymentNatureProductTypeCollectionByPackageId



                Dim instructioncoll As New IC.ICInstructionCollection

                instructioncoll.Query.Select(instructioncoll.Query.InstructionID.Count, instructioncoll.Query.Amount.Sum)


                instructioncoll.Query.Where(instructioncoll.Query.Status.In(15, 21, 48), instructioncoll.Query.ClientAccountNo = acpn.AccountNumber, instructioncoll.Query.ClientAccountBranchCode = acpn.BranchCode, instructioncoll.Query.ClientAccountCurrency = acpn.Currency, instructioncoll.Query.PaymentNatureCode = acpn.PaymentNatureCode, instructioncoll.Query.ProductTypeCode = acpn.ProductTypeCode, instructioncoll.Query.CreateDate.Date.Between(chargefrom.Date, chargeto.Date))


                If instructioncoll.Query.Load Then



                    If Not instructioncoll(0).InstructionID = 0 Then




                        Dim chargeamount As Decimal


                        If package.PackageType = "Fixed" Then



                            chargeamount = instructioncoll(0).InstructionID * package.PackageAmount


                        Else


                            Dim pslabs As New IC.ICPackageSlabsCollection

                            pslabs.Query.Where(pslabs.Query.PackageID = package.PackageID, pslabs.Query.Min <= instructioncoll(0).InstructionID, pslabs.Query.Max >= instructioncoll(0).InstructionID)


                            If pslabs.Query.Load Then



                                chargeamount = instructioncoll(0).Amount * (pslabs(0).Percentage / 100)






                            End If





                        End If



                        Dim packagecharge As New IC.ICPackageCharges

                        packagecharge.AccountNumber = acpn.AccountNumber
                        packagecharge.Currency = acpn.Currency
                        packagecharge.BranchCode = acpn.BranchCode
                        packagecharge.PaymentNatureCode = acpn.PaymentNatureCode
                        packagecharge.ProductTypeCode = acpn.ProductTypeCode
                        packagecharge.PacakgeId = package.PackageID
                        packagecharge.ChargeType = "PackageCharges"
                        packagecharge.ChargeAmount = chargeamount
                        packagecharge.ChargeDate = Now
                        packagecharge.ChargeFromDate = chargefrom
                        packagecharge.ChargeToDate = chargeto




                        packagecharge.Save()




                        If package.ICPackagesGlobalChargesCollectionByPackageID.LoadAll Then


                            For Each pgc As IC.ICPackagesGlobalCharges In package.ICPackagesGlobalChargesCollectionByPackageID


                                Dim gchargeamount As Decimal

                                If pgc.UpToICGlobalChargesByGlobalChargeID.ChargeType = "Fixed" Then


                                    gchargeamount = pgc.UpToICGlobalChargesByGlobalChargeID.ChargeAmount




                                Else


                                    gchargeamount = chargeamount * (pgc.UpToICGlobalChargesByGlobalChargeID.ChargePercentage / 100)




                                End If




                                packagecharge = New IC.ICPackageCharges

                                packagecharge.AccountNumber = acpn.AccountNumber
                                packagecharge.Currency = acpn.Currency
                                packagecharge.BranchCode = acpn.BranchCode
                                packagecharge.PaymentNatureCode = acpn.PaymentNatureCode
                                packagecharge.ProductTypeCode = acpn.ProductTypeCode
                                packagecharge.PacakgeId = package.PackageID
                                packagecharge.ChargeType = "GlobalCharges"
                                packagecharge.GlobalChargeId = pgc.GlobalChargeID
                                packagecharge.ChargeAmount = gchargeamount
                                packagecharge.ChargeDate = Now
                                packagecharge.ChargeFromDate = chargefrom
                                packagecharge.ChargeToDate = chargeto



                                packagecharge.Save()



                            Next




                        End If



                    End If


                End If













            Next








        End If



        package.LastScheduleRunDate = Now

        package.Save()





    End Sub


    Public Shared Sub GeneratePackageInvoices()



        Dim companycoll As New IC.ICCompanyCollection


        companycoll.Query.Where(companycoll.Query.IsActive = True, companycoll.Query.IsApprove = True, companycoll.Query.PackageInvoiceBillingDate.DatePart("day") = Now.Day)


        If companycoll.Query.Load Then


            For Each company As IC.ICCompany In companycoll


                'If company.PackageInvoiceBillingDate.HasValue = False Then


                '    company.PackageInvoiceBillingDate = New Date(Now.Year, Now.Month, 1)

                '    companycoll.Save()




                'End If


                GeneratePackageInvoice(company)




            Next



        End If




    End Sub


    Private Shared Sub GeneratePackageInvoice(ByVal company As IC.ICCompany)


        Dim acpnq As New IC.ICAccountsPaymentNatureProductTypeQuery("acpnq")

        Dim acq As New IC.ICAccountsQuery("acq")

        acpnq.InnerJoin(acq).On(acpnq.AccountNumber = acq.AccountNumber And acpnq.BranchCode = acq.BranchCode And acpnq.Currency = acq.Currency)

        acpnq.Where(acq.CompanyCode = company.CompanyCode, acpnq.PackageId.IsNotNull)

        Dim acpncoll As New IC.ICAccountsPaymentNatureProductTypeCollection


        If acpncoll.Load(acpnq) Then



            If IsInvoiceGenerated(company.CompanyCode) = False Then


                'create new invoice

                Dim pinv As New IC.ICPackageInvoice

                pinv.InvoiceDate = Now
                pinv.InvoiceDateFrom = Now.AddMonths(-1)
                pinv.InvoiceDateTo = Now
                pinv.CompanyCode = company.CompanyCode
                pinv.IsPaid = False


                pinv.Save()

                Dim amount As Decimal



                Dim pcq As New IC.ICPackageChargesQuery("pcq")

                Dim accq As New IC.ICAccountsQuery("accq")

                pcq.InnerJoin(accq).On(pcq.AccountNumber = accq.AccountNumber And pcq.BranchCode = accq.BranchCode And pcq.Currency = accq.Currency)

                pcq.Where(accq.CompanyCode = company.CompanyCode, pcq.InvoiceId.IsNull)



                Dim chargecoll As New IC.ICPackageChargesCollection


                If chargecoll.Load(pcq) Then

                    For Each charge As IC.ICPackageCharges In chargecoll



                        charge.InvoiceId = pinv.InvoiceId


                        amount += charge.ChargeAmount






                    Next

                    chargecoll.Save()




                End If



                pinv.InvoiceAmount = amount

                pinv.Save()




            End If




        End If





    End Sub


    Private Shared Function IsInvoiceGenerated(ByVal companycode As Integer) As Boolean


        Dim invoicedatefrom, invoicedateto As Date

        invoicedatefrom = Now.AddMonths(-1)
        invoicedateto = Now



        Dim invoicecoll As New IC.ICPackageInvoiceCollection


        invoicecoll.Query.Where(invoicecoll.Query.CompanyCode = companycode, invoicecoll.Query.InvoiceDateFrom.Date = invoicedatefrom.Date, invoicecoll.Query.InvoiceDateTo.Date = invoicedateto.Date)



        If invoicecoll.Query.Load = False Then

            'invoice not present - generate


            Return False


        Else


            Return True









        End If










    End Function

End Class
