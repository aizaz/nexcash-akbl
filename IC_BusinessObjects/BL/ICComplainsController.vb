﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC


    Public Class ICComplainsController


        'Public Shared Sub AddComplain(ByRef cComplain As IC.ICComplains)

        '    Dim FRCComplain As New IC.ICComplains

        '    FRCComplain.es.Connection.CommandTimeout = 3600


        '    'FRCComplain.AddNew()
        '    FRCComplain.CreateDate = Date.Now
        '    FRCComplain.Subject = cComplain.Subject
        '    FRCComplain.ComplainMessage = cComplain.ComplainMessage
        '    FRCComplain.Sender = cComplain.Sender
        '    FRCComplain.Receiver = cComplain.Receiver
        '    FRCComplain.Status = cComplain.Status
        '    FRCComplain.ParentID = cComplain.ParentID
        '    FRCComplain.Name = cComplain.Name
        '    FRCComplain.PhoneNo = cComplain.PhoneNo
        '    FRCComplain.Email = cComplain.Email

        '    FRCComplain.Save()

        '    cComplain.ComplainID = FRCComplain.ComplainID
        'End Sub

        Public Shared Sub AddComplain(ByRef cComplain As IC.ICComplains, ByRef chkComplain As IC.ICComplains, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)

            Dim FRCComplain As New IC.ICComplains
            Dim FRCchkComplain As New IC.ICComplains

            FRCComplain.es.Connection.CommandTimeout = 3600

            If (isUpdate = False) Then
                'FRCComplain.AddNew()
                FRCComplain.CreateDate = Date.Now

            Else
                FRCchkComplain.LoadByPrimaryKey(chkComplain.ComplainID)
                FRCComplain.CreateDate = Date.Now
                'cComplain.ParentID = FRCchkComplain.ComplainID
            End If
            FRCComplain.Subject = cComplain.Subject
            FRCComplain.ComplainMessage = cComplain.ComplainMessage
            FRCComplain.Sender = cComplain.Sender
            FRCComplain.Receiver = cComplain.Receiver
            FRCComplain.Status = cComplain.Status
            FRCComplain.ParentID = cComplain.ParentID
            FRCComplain.Name = cComplain.Name
            FRCComplain.PhoneNo = cComplain.PhoneNo
            FRCComplain.Email = cComplain.Email

            FRCComplain.Save()

            'cComplain.ComplainID = FRCComplain.ComplainID

            If (isUpdate = False) Then
                'CurrentAt = " ID : [  " & FRCComplain.ChequeId & " ]  Cheque Number: [ " & FRCComplain.ChequeNo & "] Account Number: [" & FRCComplain.AccountNumber & "] Branch Code: [" & FRCComplain.BranchCode & "] Currency: [" & FRCComplain.Currency & "]"
                ICUtilities.AddAuditTrail("Complain with subject[" & FRCComplain.Subject.ToString() & "] Added ", "Complain", FRCComplain.ComplainID.ToString(), UserID, UserName, "ADDED")
            Else

                '    CurrentAt = "Cheque : Current Values [ID: " & FRCComplain.ChequeId & "; Cheque No:  " & FRCComplain.ChequeNo & "; Account Number: " & FRCComplain.AccountNumber & "; Branch Code: " & FRCComplain.BranchCode & "; Currency: " & FRCComplain.Currency & " ]"
                '    PrevAt = "<br />Cheque : Previous Values [ID: " & FRCComplain.ChequeId & "; Cheque No:  " & prevFRCComplain.ChequeNo & "; Account Number: " & prevFRCComplain.AccountNumber & "; Branch Code: " & prevFRCComplain.BranchCode & "; Currency: " & prevFRCComplain.Currency & " ]"
                ICUtilities.AddAuditTrail("Reply on complain with parentid[" & FRCchkComplain.ComplainID.ToString() & "] and subject[" & FRCchkComplain.Subject.ToString() & "] Replied ", "Complain", FRCComplain.ComplainID.ToString(), UserID.ToString(), UserName.ToString(), "REPLIED")

            End If
        End Sub

        'Public Shared Sub UpdateComplainStatus(ByVal ComplainID As String, ByVal Status As String)
        '    Dim FRCComplain As New IC.ICComplains


        '    FRCComplain.es.Connection.CommandTimeout = 3600

        '    FRCComplain.LoadByPrimaryKey(ComplainID)
        '    FRCComplain.Status = Status.ToString()
        '    FRCComplain.Save()
        'End Sub

        Public Shared Sub UpdateComplainStatus(ByVal ComplainID As String, ByVal Status As String, ByVal UserID As String, ByVal UserName As String)
            Dim FRCComplain As New IC.ICComplains
            Dim FRCchkComplain As New IC.ICComplains

            FRCComplain.es.Connection.CommandTimeout = 3600

            FRCComplain.LoadByPrimaryKey(ComplainID)
            FRCComplain.Status = Status.ToString()
            FRCComplain.Save()

            ICUtilities.AddAuditTrail("Complain with complainid[" & FRCComplain.ComplainID.ToString() & "]  status updated as [" & Status.ToString() & "] ", "Complain", FRCComplain.ComplainID.ToString(), UserID.ToString(), UserName.ToString(), "UPDATED")

        End Sub


        'Public Shared Function GetAllCopmlainsBySender(ByVal SenderID As String) As ICComplainsCollection
        '    Dim FRCComplain As New ICComplainsCollection

        '    FRCComplain.es.Connection.CommandTimeout = 3600

        '    FRCComplain.Query.Where(FRCComplain.Query.Sender.Equal(SenderID) And FRCComplain.Query.ParentID.IsNull())
        '    'FRCComplain.Query.OrderBy(FRCComplain.Query.CreateDate.Ascending)
        '    FRCComplain.Query.OrderBy(FRCComplain.Query.ComplainID.Descending)
        '    FRCComplain.Query.Load()
        '    Return FRCComplain
        'End Function

        ' commited this for writing new query according to telerik grid view so page sorting can be done using need data source
        'Public Shared Function GetAllCopmlainsBySender(ByVal SenderID As String) As ICComplainsCollection
        '    Dim FRCComplain As New ICComplainsCollection

        '    FRCComplain.es.Connection.CommandTimeout = 3600

        '    FRCComplain.Query.Where(FRCComplain.Query.Sender = SenderID And FRCComplain.Query.ParentID.IsNull())
        '    'FRCComplain.Query.OrderBy(FRCComplain.Query.CreateDate.Ascending)
        '    FRCComplain.Query.OrderBy(FRCComplain.Query.ComplainID.Descending)
        '    FRCComplain.Query.Load()
        '    Return FRCComplain
        'End Function


        ' writing new query according to telerik grid view so page sorting can be done using need data source
        Public Shared Sub GetAllCopmlainsBySender(ByVal SenderID As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
            Dim FRCComplain As New ICComplainsCollection


            FRCComplain.es.Connection.CommandTimeout = 3600


            FRCComplain.Query.Where(FRCComplain.Query.Sender = SenderID And FRCComplain.Query.ParentID.IsNull())
            'FRCComplain.Query.OrderBy(FRCComplain.Query.CreateDate.Ascending)
            FRCComplain.Query.OrderBy(FRCComplain.Query.ComplainID.Descending)
            FRCComplain.Query.Load()
            rg.DataSource = FRCComplain

            If DoDataBind Then
                rg.DataBind()
            End If
        End Sub

        'Public Shared Function GetAllCopmlainsBySenderAndParentID(ByVal SenderID As String, ByVal ParentID As String) As ICComplainsCollection
        '    Dim FRCComplain As New ICComplainsCollection


        '    FRCComplain.es.Connection.CommandTimeout = 3600

        '    FRCComplain.Query.Where(FRCComplain.Query.Sender.Equal(SenderID) And FRCComplain.Query.ParentID.Equal(ParentID))
        '    FRCComplain.Query.OrderBy(FRCComplain.Query.CreateDate.Ascending)
        '    FRCComplain.Query.Load()
        '    Return FRCComplain
        'End Function


        ' commited this for writing new query according to telerik grid view so page sorting can be done using need data source
        'Public Shared Function GetAllCopmlainsBySenderAndParentID(ByVal SenderID As String, ByVal ParentID As String) As ICComplainsCollection
        '    Dim FRCComplain As New ICComplainsCollection


        '    FRCComplain.es.Connection.CommandTimeout = 3600

        '    FRCComplain.Query.Where(FRCComplain.Query.Sender = SenderID And FRCComplain.Query.ParentID = ParentID)
        '    FRCComplain.Query.OrderBy(FRCComplain.Query.CreateDate.Descending)
        '    FRCComplain.Query.Load()
        '    Return FRCComplain
        'End Function

        ' writing new query according to telerik grid view so page sorting can be done using need data source
        Public Shared Sub GetAllCopmlainsBySenderAndParentID(ByVal SenderID As String, ByVal ParentID As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
            Dim FRCComplain As New ICComplainsCollection


            FRCComplain.es.Connection.CommandTimeout = 3600

            FRCComplain.Query.Where(FRCComplain.Query.Sender = SenderID And FRCComplain.Query.ParentID = ParentID)
            FRCComplain.Query.OrderBy(FRCComplain.Query.CreateDate.Descending)
            FRCComplain.Query.Load()
            rg.DataSource = FRCComplain

            If DoDataBind Then
                rg.DataBind()
            End If
        End Sub


        'Public Shared Function GetStatusOfCopmlainsByComplainID(ByVal ComplainID As String) As String
        '    Dim FRCComplain As New IC.ICComplains


        '    FRCComplain.es.Connection.CommandTimeout = 3600

        '    FRCComplain.Query.Where(FRCComplain.Query.ComplainID.Equal(ComplainID))
        '    FRCComplain.Query.Load()
        '    Return FRCComplain.Status.Trim.ToString()
        'End Function

        Public Shared Function GetStatusOfCopmlainsByComplainID(ByVal ComplainID As String) As String
            Dim FRCComplain As New IC.ICComplains
            Try
                FRCComplain.es.Connection.CommandTimeout = 3600

                FRCComplain.Query.Where(FRCComplain.Query.ComplainID = ComplainID)
                If FRCComplain.Query.Load() Then
                    Return FRCComplain.Status.Trim.ToString()
                Else
                    Return ""
                End If

            Catch ex As Exception
                ICUtilities.UpdateLog("Error:" & ex.ToString)
                Return ""
            End Try
        End Function
        Public Shared Sub GetAllCopmlainsForAdmin(ByVal Status As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

            Dim qryComplain As New ICComplainsQuery("cmp")
            Dim qryUser As New ICUserQuery("usr")


            'qryComplain.es.Connection.CommandTimeout = 3600
            'qryUser.es.Connection.CommandTimeout = 3600

            qryComplain.Select(qryComplain.ComplainID, qryComplain.Subject, qryComplain.ComplainMessage, qryComplain.Sender, qryComplain.Receiver, qryComplain.Status, qryComplain.CreateDate, qryComplain.ParentID, (qryUser.UserName + "-" + qryUser.DisplayName).As("SenderName"))
            qryComplain.InnerJoin(qryUser).On(qryComplain.Sender = qryUser.UserID)
            qryComplain.Where(qryComplain.ParentID.IsNull())
            If Status.ToString() <> "0" Then
                qryComplain.Where(qryComplain.Status = Status.ToString())
            End If
            'qryComplain.OrderBy(qryComplain.CreateDate.Ascending)
            qryComplain.OrderBy(qryComplain.ComplainID.Descending)
            qryComplain.Load()
            rg.DataSource = qryComplain.LoadDataTable

            If DoDataBind Then
                rg.DataBind()
            End If



        End Sub
        ''Before Farhan modified 25052015
        'Public Shared Sub GetAllCopmlainsForAdmin(ByVal Status As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
        '    Dim qryComplain As New ICComplainsQuery("cmp")
        '    Dim qryUser As New ICUserQuery("usr")


        '    'qryComplain.es.Connection.CommandTimeout = 3600
        '    'qryUser.es.Connection.CommandTimeout = 3600

        '    qryComplain.Select(qryComplain.ComplainID, qryComplain.Subject, qryComplain.ComplainMessage, qryComplain.Sender, qryComplain.Receiver, qryComplain.Status, qryComplain.CreateDate, qryComplain.ParentID, qryUser.UserName.As("SenderName"))
        '    qryComplain.InnerJoin(qryUser).On(qryComplain.Sender = qryUser.UserID)
        '    qryComplain.Where(qryComplain.ParentID.IsNull())
        '    If Status.ToString() <> "0" Then
        '        qryComplain.Where(qryComplain.Status = Status.ToString())
        '    End If
        '    'qryComplain.OrderBy(qryComplain.CreateDate.Ascending)
        '    qryComplain.OrderBy(qryComplain.ComplainID.Descending)
        '    qryComplain.Load()
        '    rg.DataSource = qryComplain.LoadDataTable

        '    If DoDataBind Then
        '        rg.DataBind()
        '    End If



        'End Sub

        'Public Shared Function GetAllCopmlainsParentID(ByVal ParentID As String) As ICComplainsCollection
        '    Dim FRCComplain As New ICComplainsCollection

        '    FRCComplain.es.Connection.CommandTimeout = 3600

        '    FRCComplain.Query.Where(FRCComplain.Query.ParentID.Equal(ParentID))
        '    FRCComplain.Query.OrderBy(FRCComplain.Query.CreateDate.Ascending)
        '    FRCComplain.Query.Load()
        '    Return FRCComplain
        'End Function


        ' commited this for writing new query according to telerik grid view so page sorting can be done using need data source
        'Public Shared Function GetAllCopmlainsParentID(ByVal ParentID As String) As ICComplainsCollection
        '    Dim FRCComplain As New ICComplainsCollection

        '    FRCComplain.es.Connection.CommandTimeout = 3600

        '    FRCComplain.Query.Where(FRCComplain.Query.ParentID = ParentID)
        '    FRCComplain.Query.OrderBy(FRCComplain.Query.CreateDate.Descending)
        '    FRCComplain.Query.Load()
        '    Return FRCComplain
        'End Function


        ' writing new query according to telerik grid view so page sorting can be done using need data source
        Public Shared Sub GetAllCopmlainsParentID(ByVal ParentID As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
            Dim FRCComplain As New ICComplainsCollection


            FRCComplain.es.Connection.CommandTimeout = 3600

            FRCComplain.Query.Where(FRCComplain.Query.ParentID = ParentID)
            FRCComplain.Query.OrderBy(FRCComplain.Query.CreateDate.Descending)
            FRCComplain.Query.Load()
            rg.DataSource = FRCComplain


            If DoDataBind Then
                rg.DataBind()
            End If
        End Sub



        ' Aizaz Ahmed Work 28-Feb-2012

        Public Shared Function GetCopmlainDescriptionByComplainID(ByVal ComplainID As String) As String
            Dim FRCComplain As New IC.ICComplains


            FRCComplain.es.Connection.CommandTimeout = 3600

            If FRCComplain.LoadByPrimaryKey(ComplainID) Then
                Return FRCComplain.ComplainMessage
            Else
                Return ""
            End If
        End Function



    End Class



End Namespace


