﻿Namespace IC
    Public Class ICAccountsPaymentNatureProductTypeTemplatesController
        'Public Shared Function GetAccountsPaymentNatureProductTypeTemplatesByTemplateIDAndAccountsPaymentNatureProductTypeCode(ByVal TemplateID As String, ByVal AccountsPaymentNatureProductTypeCode As String) As ICAccountsPaymentNatureProductTypeTemplatesCollection
        '    Dim objAPNPTTemplateFieldListColl As New IC.ICAccountsPaymentNatureProductTypeTemplatesCollection

        '    objAPNPTTemplateFieldListColl.es.Connection.CommandTimeout = 3600
        '    objAPNPTTemplateFieldListColl.Query.Where(objAPNPTTemplateFieldListColl.Query.TemplateID = TemplateID And objAPNPTTemplateFieldListColl.Query.AccountsPaymentNatureProductTypeCode = AccountsPaymentNatureProductTypeCode)
        '    objAPNPTTemplateFieldListColl.Query.OrderBy(objAPNPTTemplateFieldListColl.Query.FieldOrder.Ascending)
        '    objAPNPTTemplateFieldListColl.Query.Load()
        '    Return objAPNPTTemplateFieldListColl
        'End Function
        Public Shared Sub AddAPNPTTemplate(ByVal objICAPNPTypeTemplate As ICAccountsPaymentNatureProductTypeTemplates, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICAPNPTTemplateFoSave As New ICAccountsPaymentNatureProductTypeTemplates
            objICAPNPTTemplateFoSave.es.Connection.CommandTimeout = 3600

            objICAPNPTTemplateFoSave.AccountNumber = objICAPNPTypeTemplate.AccountNumber
            objICAPNPTTemplateFoSave.BranchCode = objICAPNPTypeTemplate.BranchCode
            objICAPNPTTemplateFoSave.Currency = objICAPNPTypeTemplate.Currency
            objICAPNPTTemplateFoSave.PaymentNatureCode = objICAPNPTypeTemplate.PaymentNatureCode
            objICAPNPTTemplateFoSave.ProductCode = objICAPNPTypeTemplate.ProductCode
            objICAPNPTTemplateFoSave.TemplateID = objICAPNPTypeTemplate.TemplateID
            objICAPNPTTemplateFoSave.CreatedBy = objICAPNPTypeTemplate.CreatedBy
            objICAPNPTTemplateFoSave.CreatedDate = objICAPNPTypeTemplate.CreatedDate
            objICAPNPTTemplateFoSave.Creater = objICAPNPTypeTemplate.Creater
            objICAPNPTTemplateFoSave.CreationDate = objICAPNPTypeTemplate.CreationDate
            objICAPNPTTemplateFoSave.Save()
            ICUtilities.AddAuditTrail("File upload template  : " & objICAPNPTTemplateFoSave.UpToICTemplateByTemplateID.TemplateName & " with Code " & objICAPNPTTemplateFoSave.UpToICTemplateByTemplateID.FileUploadTemplateCode & " of type " & objICAPNPTTemplateFoSave.UpToICTemplateByTemplateID.TemplateType & " and of format " & objICAPNPTTemplateFoSave.UpToICTemplateByTemplateID.TemplateFormat & " against : account number :" & objICAPNPTTemplateFoSave.AccountNumber & ", Branch Code: " & objICAPNPTTemplateFoSave.BranchCode & ", Currency " & objICAPNPTTemplateFoSave.Currency & ", Payment nature code " & objICAPNPTTemplateFoSave.PaymentNatureCode & ", Product type code " & objICAPNPTTemplateFoSave.ProductCode & " added.", "Account Payment Nature Product Type Template", objICAPNPTTemplateFoSave.APNPTTemplateID.ToString, UsersID.ToString, UsersName.ToString, "ADD")
        End Sub
        Public Shared Sub DeleteAllAPNPTypeFileUploadTemplates(ByVal objICAPNPType As ICAccountsPaymentNatureProductType, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICAPNPTypeTemplatesColl As New ICAccountsPaymentNatureProductTypeTemplatesCollection
            Dim objAPNPTypeTemplate As ICAccountsPaymentNatureProductTypeTemplates
            objICAPNPTypeTemplatesColl.es.Connection.CommandTimeout = 3600



            objICAPNPTypeTemplatesColl.Query.Where(objICAPNPTypeTemplatesColl.Query.AccountNumber = objICAPNPType.AccountNumber And objICAPNPTypeTemplatesColl.Query.BranchCode = objICAPNPType.BranchCode And objICAPNPTypeTemplatesColl.Query.Currency = objICAPNPType.Currency And objICAPNPTypeTemplatesColl.Query.PaymentNatureCode = objICAPNPType.PaymentNatureCode And objICAPNPTypeTemplatesColl.Query.ProductCode = objICAPNPType.ProductTypeCode)


            If objICAPNPTypeTemplatesColl.Query.Load() Then
                For Each objICAPNPTTemplateForDelete As ICAccountsPaymentNatureProductTypeTemplates In objICAPNPTypeTemplatesColl
                    objAPNPTypeTemplate = New ICAccountsPaymentNatureProductTypeTemplates
                    objAPNPTypeTemplate.es.Connection.CommandTimeout = 3600
                    objAPNPTypeTemplate.LoadByPrimaryKey(objICAPNPTTemplateForDelete.APNPTTemplateID)
                    objAPNPTypeTemplate.MarkAsDeleted()
                    objAPNPTypeTemplate.Save()
                    ICUtilities.AddAuditTrail("File upload template  : " & objICAPNPTTemplateForDelete.UpToICTemplateByTemplateID.TemplateName & " with Code " & objICAPNPTTemplateForDelete.UpToICTemplateByTemplateID.FileUploadTemplateCode & " of type " & objICAPNPTTemplateForDelete.UpToICTemplateByTemplateID.TemplateType & " and of format " & objICAPNPTTemplateForDelete.UpToICTemplateByTemplateID.TemplateFormat & " deleted. Against : account number :" & objICAPNPTTemplateForDelete.AccountNumber & ", Branch Code: " & objICAPNPTTemplateForDelete.BranchCode & ", Currency " & objICAPNPTTemplateForDelete.Currency & ", Payment nature code " & objICAPNPTTemplateForDelete.PaymentNatureCode & ", Product type code " & objICAPNPTTemplateForDelete.ProductCode & " .", "Account Payment Nature Product Type Template", objICAPNPTTemplateForDelete.APNPTTemplateID.ToString, UsersID.ToString, UsersName.ToString, "DELETE")

                Next
            End If
        End Sub
        Public Shared Function GetAllAPNPTypeTemplatesByAPNPType(ByVal objICAPNPType As ICAccountsPaymentNatureProductType) As ICAccountsPaymentNatureProductTypeTemplatesCollection
            Dim objICAPNPTypeTemplatesColl As New ICAccountsPaymentNatureProductTypeTemplatesCollection
            objICAPNPTypeTemplatesColl.es.Connection.CommandTimeout = 3600
            objICAPNPTypeTemplatesColl.Query.Select(objICAPNPTypeTemplatesColl.Query.TemplateID)
            objICAPNPTypeTemplatesColl.Query.Where(objICAPNPTypeTemplatesColl.Query.AccountNumber = objICAPNPType.AccountNumber And objICAPNPTypeTemplatesColl.Query.BranchCode = objICAPNPType.BranchCode And objICAPNPTypeTemplatesColl.Query.Currency = objICAPNPType.Currency And objICAPNPTypeTemplatesColl.Query.PaymentNatureCode = objICAPNPType.PaymentNatureCode And objICAPNPTypeTemplatesColl.Query.ProductCode = objICAPNPType.ProductTypeCode)
            objICAPNPTypeTemplatesColl.Query.OrderBy(objICAPNPTypeTemplatesColl.Query.TemplateID, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            objICAPNPTypeTemplatesColl.Query.Load()
            Return objICAPNPTypeTemplatesColl
        End Function
        'Public Shared Sub DeleteAPNPTTemplateByTemplateIDAndAccountsPaymentNatureProductTypeCode(ByVal TemplatesID As String, ByVal AccountsPaymentNatureProductTypeCode As String)
        '    Dim collobjICAPNPTTemplate As New ICAccountsPaymentNatureProductTypeTemplatesCollection
        '    Dim objICAPNPTTemplate As New ICAccountsPaymentNatureProductTypeTemplates
        '    collobjICAPNPTTemplate.es.Connection.CommandTimeout = 3600
        '    objICAPNPTTemplate.es.Connection.CommandTimeout = 3600

        '    collobjICAPNPTTemplate.Query.Where(collobjICAPNPTTemplate.Query.TemplateID = TemplatesID And collobjICAPNPTTemplate.Query.AccountsPaymentNatureProductTypeCode = AccountsPaymentNatureProductTypeCode)

        '    If collobjICAPNPTTemplate.LoadAll() Then
        '        For Each objICAPNPTTemplate In collobjICAPNPTTemplate
        '            DeleteAPNPTTemplate(objICAPNPTTemplate.AccountsPaymentNatureProductTypeTemplatesID.ToString())
        '        Next
        '    End If
        'End Sub
        'Public Shared Function GetAPNPTTemplatesByAccountsPaymentNatureProductTypeCode(ByVal AccountsPaymentNatureProductTypeCode As String) As DataTable
        '    Dim qryAPNPTTemplate As New ICAccountsPaymentNatureProductTypeTemplatesQuery("qryAPNPTTemplate")
        '    Dim qryTemplate As New ICTemplateQuery("qryTemplate")
        '    qryAPNPTTemplate.es.Distinct = True

        '    qryAPNPTTemplate.Select(qryAPNPTTemplate.TemplateID, qryTemplate.TemplateName)
        '    qryAPNPTTemplate.InnerJoin(qryTemplate).On(qryAPNPTTemplate.TemplateID = qryTemplate.TemplateID)
        '    qryAPNPTTemplate.Where(qryAPNPTTemplate.AccountsPaymentNatureProductTypeCode = AccountsPaymentNatureProductTypeCode)



        '    Return qryAPNPTTemplate.LoadDataTable()
        'End Function
    End Class
End Namespace

