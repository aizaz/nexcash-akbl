﻿Imports Telerik.Web.UI
Namespace IC
    Public Class ICNotificationManagementController

        Public Shared Sub AddICNotificationForUser(ByVal objICNotificationMAnagement As ICNotificationManagement, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICNotficationManagementForSave As New ICNotificationManagement
            objICNotficationManagementForSave.es.Connection.CommandTimeout = 3600


            objICNotficationManagementForSave.EventID = objICNotificationMAnagement.EventID
            objICNotficationManagementForSave.UserID = objICNotificationMAnagement.UserID
            objICNotficationManagementForSave.CreatedBy = objICNotificationMAnagement.CreatedBy
            objICNotficationManagementForSave.CreatedDate = objICNotificationMAnagement.CreatedDate
            objICNotficationManagementForSave.Creater = objICNotificationMAnagement.Creater
            objICNotficationManagementForSave.CreationDate = objICNotificationMAnagement.CreationDate
            objICNotficationManagementForSave.IsSMSAllow = objICNotificationMAnagement.IsSMSAllow
            objICNotficationManagementForSave.IsEmailAllow = objICNotificationMAnagement.IsEmailAllow
            objICNotficationManagementForSave.Save()
            ICUtilities.AddAuditTrail("Notification management setting for event :" & objICNotficationManagementForSave.UpToICEventsByEventID.EventName & " for user " & objICNotficationManagementForSave.UpToICUserByUserID.UserName & " added.", "Notification Management", objICNotficationManagementForSave.NotificationID.ToString, UsersID.ToString, UsersName.ToString, "ADD")
        End Sub

        Public Shared Sub DeleteNotificationSettingForUSer(ByVal objICNotificationManagement As ICNotificationManagement, ByVal USersID As String, ByVal UsersName As String)
            Dim objICNotificationForDelete As New ICNotificationManagement
            objICNotificationForDelete.es.Connection.CommandTimeout = 3600
            If objICNotificationForDelete.LoadByPrimaryKey(objICNotificationManagement.NotificationID) Then
                objICNotificationForDelete.MarkAsDeleted()
                objICNotificationForDelete.Save()
                ICUtilities.AddAuditTrail("Notification management setting for event :" & objICNotificationManagement.UpToICEventsByEventID.EventName & " for user " & objICNotificationManagement.UpToICUserByUserID.UserName & " deleted.", "Notification Management", objICNotificationManagement.NotificationID.ToString, USersID.ToString, UsersName.ToString, "DELETE")
            End If
        End Sub
        Public Shared Sub ApproveNotificationSettingForUSer(ByVal IsApproved As Boolean, ByVal NotificationID As String, ByVal USersID As String, ByVal UsersName As String)
            Dim objICNotificationManagement As New ICNotificationManagement
            objICNotificationManagement.es.Connection.CommandTimeout = 3600
            If objICNotificationManagement.LoadByPrimaryKey(NotificationID.ToString) Then
                objICNotificationManagement.IsApproved = IsApproved
                objICNotificationManagement.ApprovedBy = USersID.ToString
                objICNotificationManagement.ApprovedOn = Date.Now
                objICNotificationManagement.Save()
                ICUtilities.AddAuditTrail("Notification management setting for event :" & objICNotificationManagement.UpToICEventsByEventID.EventName & " for user " & objICNotificationManagement.UpToICUserByUserID.UserName & " approved.", "Notification Management", objICNotificationManagement.NotificationID.ToString, USersID.ToString, UsersName.ToString, "APPROVE")
            End If
        End Sub
        Public Shared Function GetAllNotificationAssignedToUser(ByVal USerID As String) As ICNotificationManagementCollection
            Dim objICNotificationListColl As New ICNotificationManagementCollection
            Dim dt As New DataTable
            objICNotificationListColl.es.Connection.CommandTimeout = 3600

            objICNotificationListColl.Query.Where(objICNotificationListColl.Query.UserID = USerID.ToString)
            objICNotificationListColl.Query.Load()
            Return objICNotificationListColl
        End Function
        Public Shared Sub GetAllEventsListAssignedToUserForRadGrid(ByVal UserID As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDatabind As Boolean, ByVal rg As RadGrid)
            Dim qryobjICNotification As New ICNotificationManagementQuery("qryobjICNotification")
            Dim qryObjICEventList As New ICEventsQuery("qryObjICEventList")
            Dim dt As New DataTable

            qryobjICNotification.Select(qryObjICEventList.EventName, qryobjICNotification.EventID, qryobjICNotification.NotificationID)
            qryobjICNotification.Select(qryobjICNotification.IsApproved.Case.When(qryobjICNotification.IsApproved = True).Then("True").Else("False").End.Cast(EntitySpaces.DynamicQuery.esCastType.String).As("IsApproved"))
            qryobjICNotification.Select(qryobjICNotification.IsSMSAllow.Case.When(qryobjICNotification.IsSMSAllow = True).Then("Yes").Else("No").End.Cast(EntitySpaces.DynamicQuery.esCastType.String).As("VIASMS"))
            qryobjICNotification.Select(qryobjICNotification.IsEmailAllow.Case.When(qryobjICNotification.IsEmailAllow = True).Then("Yes").Else("No").End.Cast(EntitySpaces.DynamicQuery.esCastType.String).As("VIAEmail"))
            qryobjICNotification.InnerJoin(qryObjICEventList).On(qryobjICNotification.EventID = qryObjICEventList.EventID)
            qryobjICNotification.Where(qryobjICNotification.UserID = UserID)


            dt = qryobjICNotification.LoadDataTable

            If Not PageNumber = 0 Then
                qryobjICNotification.es.PageNumber = PageNumber
                qryobjICNotification.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDatabind = True Then
                    rg.DataBind()
                End If
            Else
                qryobjICNotification.es.PageNumber = 1
                qryobjICNotification.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDatabind = True Then
                    rg.DataBind()
                End If
            End If


        End Sub
        
        Public Shared Function GetUsersForSMSorEMail(ByVal EventID As String, ByVal IsEmail As Boolean, ByVal GroupCode As String) As DataTable

            Dim qryUserClient As New ICUserQuery("qryUserClient")
            Dim qryEventEmailsClient As New ICEventsQuery("qryEventEmailsClient")
            Dim qryNotificationClient As New ICNotificationManagementQuery("qryNotificationClient")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")


            Dim qryUserBank As New ICUserQuery("qryUserBank")
            Dim qryEventEmailsBank As New ICEventsQuery("qryEventEmailsBank")
            Dim qryNotificationBank As New ICNotificationManagementQuery("qryNotificationBank")
           




            Dim dtClientUsers As New DataTable
            Dim dtBankUsers As New DataTable
            Dim dt As New DataTable
            Dim drclient, drBank As DataRow

            dt.Columns.Add(New DataColumn("UserID", GetType(System.String)))
            dt.Columns.Add(New DataColumn("UserName", GetType(System.String)))
            dt.Columns.Add(New DataColumn("DisplayName", GetType(System.String)))
            dt.Columns.Add(New DataColumn("Email", GetType(System.String)))
            dt.Columns.Add(New DataColumn("CellNo", GetType(System.String)))
            dt.Columns.Add(New DataColumn("UserType", GetType(System.String)))


            qryNotificationClient.Select(qryNotificationClient.UserID, qryUserClient.UserName, qryUserClient.DisplayName, qryUserClient.Email, qryUserClient.CellNo, qryUserClient.UserType)

            qryNotificationClient.InnerJoin(qryEventEmailsClient).On(qryNotificationClient.EventID = qryEventEmailsClient.EventID)
            qryNotificationClient.InnerJoin(qryUserClient).On(qryNotificationClient.UserID = qryUserClient.UserID)
            qryNotificationClient.InnerJoin(qryObjICOffice).On(qryUserClient.OfficeCode = qryObjICOffice.OfficeID)
            qryNotificationClient.InnerJoin(qryObjICCompany).On(qryObjICOffice.CompanyCode = qryObjICCompany.CompanyCode)
            qryNotificationClient.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
            qryNotificationClient.Where(qryEventEmailsClient.EventID = EventID And qryUserClient.UserType = "Client User" And qryObjICGroup.GroupCode = GroupCode)
            qryNotificationClient.Where(qryNotificationClient.IsApproved = True)


            If IsEmail = True Then
                qryNotificationClient.Where(qryNotificationClient.IsEmailAllow = True)
            Else
                qryNotificationClient.Where(qryNotificationClient.IsSMSAllow = True)
            End If

            dtClientUsers = qryNotificationClient.LoadDataTable()


            qryNotificationBank.Select(qryNotificationBank.UserID, qryUserBank.UserName, qryUserBank.DisplayName, qryUserBank.Email, qryUserBank.CellNo, qryUserBank.UserType)

            qryNotificationBank.InnerJoin(qryEventEmailsBank).On(qryNotificationBank.EventID = qryEventEmailsBank.EventID)
            qryNotificationBank.InnerJoin(qryUserBank).On(qryNotificationBank.UserID = qryUserBank.UserID)

            qryNotificationBank.Where(qryEventEmailsBank.EventID = EventID And qryUserBank.UserType = "Bank User")
            qryNotificationBank.Where(qryNotificationBank.IsApproved = True)


            If IsEmail = True Then
                qryNotificationBank.Where(qryNotificationBank.IsEmailAllow = True)
            Else
                qryNotificationBank.Where(qryNotificationBank.IsSMSAllow = True)
            End If

            dtBankUsers = qryNotificationBank.LoadDataTable()


            If dtClientUsers.Rows.Count > 0 Then
                For Each dr As DataRow In dtClientUsers.Rows
                    drclient = dt.NewRow
                    drclient("UserID") = dr("UserID")
                    drclient("UserName") = dr("UserName")
                    drclient("DisplayName") = dr("DisplayName")
                    drclient("Email") = dr("Email")
                    drclient("CellNo") = dr("CellNo")
                    drclient("UserType") = dr("UserType")
                    dt.Rows.Add(drclient)
                Next
            End If
            If dtBankUsers.Rows.Count > 0 Then
                For Each dr As DataRow In dtBankUsers.Rows
                    drBank = dt.NewRow
                    drBank("UserID") = dr("UserID")
                    drBank("UserName") = dr("UserName")
                    drBank("DisplayName") = dr("DisplayName")
                    drBank("Email") = dr("Email")
                    drBank("CellNo") = dr("CellNo")
                    drBank("UserType") = dr("UserType")
                    dt.Rows.Add(drBank)
                Next
            End If


            Return dt
        End Function
        Public Shared Function IsUserTaggedWithEvent(ByVal UsersID As String, ByVal EventID As String, ByVal VIASMS As Boolean) As Boolean
            Dim objICNotificationColl As New ICNotificationManagementCollection
            Dim Result As Boolean = False
            objICNotificationColl.Query.Where(objICNotificationColl.Query.EventID = EventID And objICNotificationColl.Query.UserID = UsersID And objICNotificationColl.Query.IsApproved = True)
            If VIASMS = True Then
                objICNotificationColl.Query.Where(objICNotificationColl.Query.IsSMSAllow = True)
            ElseIf VIASMS = False Then
                objICNotificationColl.Query.Where(objICNotificationColl.Query.IsEmailAllow = True)
            End If
            objICNotificationColl.Query.Load()
            If objICNotificationColl.Query.Load Then
                Result = True
            End If
            Return Result
        End Function
        Public Shared Function GetUsersForSMSorEMail2(ByVal EventID As String, ByVal IsEmail As Boolean, ByVal APNLocation As String, ByVal TaggingType As String) As DataTable
            Dim dt As New DataTable
            Dim qryObjICURAPNLocation As New ICUserRolesAccountPaymentNatureAndLocationsQuery("qryObjICURAPNLocation")
            Dim qryObjICNotificationManagement As New ICNotificationManagementQuery("qryObjICNotificationManagement")
            Dim qryObjICUser As New ICUserQuery("qryObjICUser")
            qryObjICURAPNLocation.Select(qryObjICURAPNLocation.UserID, qryObjICUser.UserName, qryObjICUser.DisplayName, qryObjICUser.CellNo, qryObjICUser.Email, qryObjICUser.UserType)
            qryObjICURAPNLocation.InnerJoin(qryObjICNotificationManagement).On(qryObjICURAPNLocation.UserID = qryObjICNotificationManagement.UserID)
            qryObjICURAPNLocation.InnerJoin(qryObjICUser).On(qryObjICNotificationManagement.UserID = qryObjICUser.UserID)
            If TaggingType = "APNatureAndLocation" Then
                qryObjICURAPNLocation.Where(qryObjICURAPNLocation.AccountNumber = APNLocation.Split("-")(0), qryObjICURAPNLocation.BranchCode = APNLocation.Split("-")(1), qryObjICURAPNLocation.Currency = APNLocation.Split("-")(2), qryObjICURAPNLocation.PaymentNatureCode = APNLocation.Split("-")(3), qryObjICURAPNLocation.OfficeID = APNLocation.Split("-")(4))
            ElseIf TaggingType = "Printing" Then
                qryObjICURAPNLocation.Where(qryObjICURAPNLocation.AccountNumber = APNLocation.Split("-")(0), qryObjICURAPNLocation.BranchCode = APNLocation.Split("-")(1), qryObjICURAPNLocation.Currency = APNLocation.Split("-")(2), qryObjICURAPNLocation.PaymentNatureCode = APNLocation.Split("-")(3))
                qryObjICURAPNLocation.Where(qryObjICUser.OfficeCode = APNLocation.Split("-")(4))
            ElseIf TaggingType = "APNature" Then
                qryObjICURAPNLocation.Where(qryObjICURAPNLocation.AccountNumber = APNLocation.Split("-")(0), qryObjICURAPNLocation.BranchCode = APNLocation.Split("-")(1), qryObjICURAPNLocation.Currency = APNLocation.Split("-")(2), qryObjICURAPNLocation.PaymentNatureCode = APNLocation.Split("-")(3))
            End If
            qryObjICURAPNLocation.Where(qryObjICURAPNLocation.IsApprove = True)
            qryObjICURAPNLocation.Where(qryObjICNotificationManagement.IsApproved = True And qryObjICNotificationManagement.EventID = EventID)
            If IsEmail = True Then
                qryObjICURAPNLocation.Where(qryObjICNotificationManagement.IsEmailAllow = True)
            ElseIf IsEmail = False Then
                qryObjICURAPNLocation.Where(qryObjICNotificationManagement.IsSMSAllow = True)
            End If
            qryObjICURAPNLocation.es.Distinct = True
            qryObjICURAPNLocation.Load()
            dt = qryObjICURAPNLocation.LoadDataTable
            Return dt
        End Function
        ''24-03-2014 Jawad
        
        'Created this method for getting user tagged for clearing for sending email and sms(utlities) of Pdc Notification
        Public Shared Function GetAllUserTaggedForClearing(ByVal EventID As String, ByVal IsEmail As Boolean) As DataTable

            Dim qryObjICNotificationManagement As New ICNotificationManagementQuery("qryObjICNotificationManagement")
            Dim qryObjICUser As New ICUserQuery("qryObjICUser")
            Dim qryNotificationClient As New ICNotificationManagementQuery("qryNotificationClient")
            Dim qryEventEmailsClient As New ICEventsQuery("qryEventEmailsClient")
            Dim dt As New DataTable
            Dim dtClientUsers As New DataTable


            qryObjICNotificationManagement.Select(qryObjICUser.UserID, qryObjICUser.UserName, qryObjICUser.DisplayName, qryObjICUser.Email, qryObjICUser.CellNo)

            qryObjICNotificationManagement.InnerJoin(qryObjICUser).On(qryObjICNotificationManagement.UserID = qryObjICUser.UserID)
            'qryNotificationClient.InnerJoin(qryEventEmailsClient).On(qryNotificationClient.EventID = qryEventEmailsClient.EventID)
            qryObjICNotificationManagement.Where(qryObjICNotificationManagement.IsApproved = True And qryObjICUser.IsApproved = True And qryObjICUser.IsActive = True)

            If IsEmail = True Then
                qryObjICNotificationManagement.Where(qryObjICNotificationManagement.IsEmailAllow = True)
            Else
                qryObjICNotificationManagement.Where(qryObjICNotificationManagement.IsSMSAllow = True)
            End If

            dt = qryObjICNotificationManagement.LoadDataTable()

            Return dt

        End Function
        

        'Created this method for getting user tagged with accounts for sending email and sms(utlilities) of Accounts Management Module
        Public Shared Function GetAllTaggedClientUserForAccountBalance(ByVal EventID As String, ByVal IsEmail As Boolean, ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String) As DataTable

            Dim qryObjICNotificationManagement As New ICNotificationManagementQuery("qryObjICNotificationManagement")
            Dim qryObjICUser As New ICUserQuery("qryObjICUser")
            Dim qryObjICAccountsQuery As New ICAccountsQuery("qryObjICAccountsQuery")
            Dim qryObjICTaggedClientUserForAccountQuery As New ICTaggedClientUserForAccountQuery("qryObjICTaggedClientUserForAccountQuery")
            Dim qryNotificationClient As New ICNotificationManagementQuery("qryNotificationClient")
            Dim qryEventEmailsClient As New ICEventsQuery("qryEventEmailsClient")
            Dim dt As New DataTable
            Dim dtClientUsers As New DataTable


            qryObjICNotificationManagement.Select(qryObjICUser.UserID, qryObjICUser.UserName, qryObjICUser.DisplayName, qryObjICUser.Email, qryObjICUser.CellNo)

            qryObjICNotificationManagement.InnerJoin(qryObjICTaggedClientUserForAccountQuery).On(qryObjICNotificationManagement.UserID = qryObjICTaggedClientUserForAccountQuery.UserID)
            qryObjICNotificationManagement.InnerJoin(qryObjICUser).On(qryObjICTaggedClientUserForAccountQuery.UserID = qryObjICUser.UserID)
            qryObjICNotificationManagement.InnerJoin(qryObjICAccountsQuery).On(qryObjICAccountsQuery.AccountNumber = qryObjICTaggedClientUserForAccountQuery.AccountNumber And qryObjICAccountsQuery.BranchCode = qryObjICTaggedClientUserForAccountQuery.BranchCode And qryObjICAccountsQuery.Currency = qryObjICTaggedClientUserForAccountQuery.Currency)
            'qryNotificationClient.InnerJoin(qryEventEmailsClient).On(qryNotificationClient.EventID = qryEventEmailsClient.EventID)
            qryObjICNotificationManagement.Where(qryObjICAccountsQuery.AccountNumber = AccountNumber And qryObjICAccountsQuery.BranchCode = BranchCode And qryObjICAccountsQuery.Currency = Currency)
            qryObjICNotificationManagement.Where(qryObjICAccountsQuery.DailyBalanceAllow = True And qryObjICNotificationManagement.EventID = EventID)


            If IsEmail = True Then
                qryObjICNotificationManagement.Where(qryObjICNotificationManagement.IsEmailAllow = True)
            Else
                qryObjICNotificationManagement.Where(qryObjICNotificationManagement.IsSMSAllow = True)
            End If

            dt = qryObjICNotificationManagement.LoadDataTable()

            Return dt

        End Function

        'Created this method for getting user who logged complain for sending email and sms(utlilities) of Complain Management Module
        Public Shared Function GetAllUserComplain(ByVal EventID As String, ByVal IsEmail As Boolean) As DataTable

            Dim qryObjICNotificationManagement As New ICNotificationManagementQuery("qryObjICNotificationManagement")
            Dim qryObjICUser As New ICUserQuery("qryObjICUser")
            Dim qryObjICAccountsQuery As New ICAccountsQuery("qryObjICAccountsQuery")
            Dim qryObjICComplainsQuery As New ICComplainsQuery("qryObjICComplainsQuery")
            Dim qryNotificationClient As New ICNotificationManagementQuery("qryNotificationClient")
            Dim qryEventEmailsClient As New ICEventsQuery("qryEventEmailsClient")
            Dim dt As New DataTable
            Dim dtClientUsers As New DataTable


            qryObjICNotificationManagement.Select(qryObjICUser.UserID, qryObjICUser.UserName, qryObjICUser.DisplayName, qryObjICUser.Email, qryObjICUser.CellNo)

            qryObjICNotificationManagement.InnerJoin(qryObjICUser).On(qryObjICNotificationManagement.UserID = qryObjICUser.UserID)

            qryObjICNotificationManagement.Where(qryObjICNotificationManagement.IsApproved = True And qryObjICUser.IsApproved = True And qryObjICUser.IsActive = True)
            qryObjICNotificationManagement.Where(qryObjICNotificationManagement.EventID = EventID)
            qryObjICNotificationManagement.Where(qryObjICNotificationManagement.IsEmailAllow = True)

            dt = qryObjICNotificationManagement.LoadDataTable()

            Return dt

        End Function



        
    End Class
End Namespace
