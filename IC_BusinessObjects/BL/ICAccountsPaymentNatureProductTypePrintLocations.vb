﻿Namespace IC
    Public Class ICAccountsPaymentNatureProductTypePrintLocationsController
        Public Shared Sub AddAPNPTypePrintLocations(ByVal objICAPNPTpePrintLocation As ICAccountPaymentNatureProductTypeAndPrintLocations, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICAPNPTPrintLocation As New ICAccountPaymentNatureProductTypeAndPrintLocations
            objICAPNPTPrintLocation.es.Connection.CommandTimeout = 3600
            objICAPNPTPrintLocation.AccountNumber = objICAPNPTpePrintLocation.AccountNumber
            objICAPNPTPrintLocation.BranchCode = objICAPNPTpePrintLocation.BranchCode
            objICAPNPTPrintLocation.Currency = objICAPNPTpePrintLocation.Currency
            objICAPNPTPrintLocation.PaymentNatureCode = objICAPNPTpePrintLocation.PaymentNatureCode
            objICAPNPTPrintLocation.ProductTypeCode = objICAPNPTpePrintLocation.ProductTypeCode
            objICAPNPTPrintLocation.OfficeID = objICAPNPTpePrintLocation.OfficeID
            objICAPNPTPrintLocation.CreatedBy = objICAPNPTpePrintLocation.CreatedBy
            objICAPNPTPrintLocation.CreatedOn = objICAPNPTpePrintLocation.CreatedOn
            objICAPNPTPrintLocation.Creater = objICAPNPTpePrintLocation.Creater
            objICAPNPTPrintLocation.CreationDate = objICAPNPTpePrintLocation.CreationDate
            objICAPNPTPrintLocation.Save()
            ICUtilities.AddAuditTrail("Print locations  : " & objICAPNPTPrintLocation.UpToICOfficeByOfficeID.OfficeName & " with Code " & objICAPNPTPrintLocation.UpToICOfficeByOfficeID.OfficeCode & " against : account number :" & objICAPNPTPrintLocation.AccountNumber & ", Branch Code: " & objICAPNPTPrintLocation.BranchCode & ", Currency " & objICAPNPTPrintLocation.Currency & ", Payment nature code " & objICAPNPTPrintLocation.PaymentNatureCode & ", Product type code " & objICAPNPTPrintLocation.ProductTypeCode & " added.", "Account Payment Nature Product Type Print Location", objICAPNPTPrintLocation.Apnptid.ToString, UsersID.ToString, UsersName.ToString, "ADD")
        End Sub
        Public Shared Function GetAllAPNPTypePrintLocationsByAPNPType(ByVal objICAPNPType As ICAccountsPaymentNatureProductType) As ICAccountPaymentNatureProductTypeAndPrintLocationsCollection
            Dim objICAPNPTypePrintLocationColl As New ICAccountPaymentNatureProductTypeAndPrintLocationsCollection
            objICAPNPTypePrintLocationColl.es.Connection.CommandTimeout = 3600
            objICAPNPTypePrintLocationColl.Query.Select(objICAPNPTypePrintLocationColl.Query.OfficeID)
            objICAPNPTypePrintLocationColl.Query.Where(objICAPNPTypePrintLocationColl.Query.AccountNumber = objICAPNPType.AccountNumber And objICAPNPTypePrintLocationColl.Query.BranchCode = objICAPNPType.BranchCode And objICAPNPTypePrintLocationColl.Query.Currency = objICAPNPType.Currency And objICAPNPTypePrintLocationColl.Query.PaymentNatureCode = objICAPNPType.PaymentNatureCode And objICAPNPTypePrintLocationColl.Query.ProductTypeCode = objICAPNPType.ProductTypeCode)
            objICAPNPTypePrintLocationColl.Query.OrderBy(objICAPNPTypePrintLocationColl.Query.OfficeID, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            objICAPNPTypePrintLocationColl.Query.Load()
            Return objICAPNPTypePrintLocationColl
        End Function
        Public Shared Sub DeleteAllAPNPTypePrintLocationsByAPNPType(ByVal objICAPNPType As ICAccountsPaymentNatureProductType, ByVal UsersID As String, ByVal UsersName As String)
            Dim objICAPNPTypePrintLocationColl As New ICAccountPaymentNatureProductTypeAndPrintLocationsCollection
            Dim objAPNPTypePrintLocation As ICAccountPaymentNatureProductTypeAndPrintLocations
            objICAPNPTypePrintLocationColl.es.Connection.CommandTimeout = 3600
            objICAPNPTypePrintLocationColl.Query.Where(objICAPNPTypePrintLocationColl.Query.AccountNumber = objICAPNPType.AccountNumber And objICAPNPTypePrintLocationColl.Query.BranchCode = objICAPNPType.BranchCode And objICAPNPTypePrintLocationColl.Query.Currency = objICAPNPType.Currency And objICAPNPTypePrintLocationColl.Query.PaymentNatureCode = objICAPNPType.PaymentNatureCode And objICAPNPTypePrintLocationColl.Query.ProductTypeCode = objICAPNPType.ProductTypeCode)
            If objICAPNPTypePrintLocationColl.Query.Load() Then
                For Each objICAPNPTPLocation As ICAccountPaymentNatureProductTypeAndPrintLocations In objICAPNPTypePrintLocationColl
                    objAPNPTypePrintLocation = New ICAccountPaymentNatureProductTypeAndPrintLocations
                    objAPNPTypePrintLocation.es.Connection.CommandTimeout = 3600
                    objAPNPTypePrintLocation.LoadByPrimaryKey(objICAPNPTPLocation.Apnptid)
                    objAPNPTypePrintLocation.MarkAsDeleted()
                    objAPNPTypePrintLocation.Save()
                    ICUtilities.AddAuditTrail("Print locations  : " & objICAPNPTPLocation.UpToICOfficeByOfficeID.OfficeName & " with Code " & objICAPNPTPLocation.UpToICOfficeByOfficeID.OfficeCode & " deleted. Against : account number :" & objICAPNPTPLocation.AccountNumber & ", Branch Code: " & objICAPNPTPLocation.BranchCode & ", Currency " & objICAPNPTPLocation.Currency & ", Payment nature code " & objICAPNPTPLocation.PaymentNatureCode & ", Product type code " & objICAPNPTPLocation.ProductTypeCode & " .", "Account Payment Nature Product Type Print Location", objICAPNPTPLocation.Apnptid.ToString, UsersID.ToString, UsersName.ToString, "DELETE")
                Next
            End If
        End Sub
        Public Shared Function GetAllAPNPTypePrintLocationsByAPNature(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String) As DataTable
            Dim qryObjICAPNPTypePrintLocation As New ICAccountPaymentNatureProductTypeAndPrintLocationsQuery("qryObjICAPNPTypePrintLocation")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim dt As New DataTable
            qryObjICAPNPTypePrintLocation.Select(qryObjICOffice.OfficeID, qryObjICOffice.OfficeName)
            qryObjICAPNPTypePrintLocation.InnerJoin(qryObjICOffice).On(qryObjICAPNPTypePrintLocation.OfficeID = qryObjICOffice.OfficeID)
            qryObjICAPNPTypePrintLocation.Where(qryObjICAPNPTypePrintLocation.AccountNumber = AccountNumber.ToString And qryObjICAPNPTypePrintLocation.BranchCode = BranchCode.ToString)
            qryObjICAPNPTypePrintLocation.Where(qryObjICAPNPTypePrintLocation.Currency = Currency.ToString And qryObjICAPNPTypePrintLocation.PaymentNatureCode = PaymentNatureCode.ToString)

            dt = qryObjICAPNPTypePrintLocation.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAllTaggedLocationsWithAPNPType(ByVal ProductType As String, ByVal AccountNo As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String) As DataTable
            Dim dt As New DataTable
            Dim qryObjAPNPTPrintLocations As New ICAccountPaymentNatureProductTypeAndPrintLocationsQuery("qryObjAPNPTPrintLocations")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            qryObjAPNPTPrintLocations.Select(qryObjAPNPTPrintLocations.OfficeID, (qryObjICOffice.OfficeCode.Coalesce("'-'") + " - " + qryObjICOffice.OfficeName).As("OfficeName"))
            qryObjAPNPTPrintLocations.InnerJoin(qryObjICOffice).On(qryObjAPNPTPrintLocations.OfficeID = qryObjICOffice.OfficeID)
            qryObjAPNPTPrintLocations.Where(qryObjAPNPTPrintLocations.AccountNumber = AccountNo And qryObjAPNPTPrintLocations.BranchCode = BranchCode And qryObjAPNPTPrintLocations.Currency = Currency And qryObjAPNPTPrintLocations.PaymentNatureCode = PaymentNatureCode And qryObjAPNPTPrintLocations.ProductTypeCode = ProductType)
            qryObjAPNPTPrintLocations.es.Distinct = True
            dt = qryObjAPNPTPrintLocations.LoadDataTable
            Return dt
        End Function
    End Class
End Namespace
