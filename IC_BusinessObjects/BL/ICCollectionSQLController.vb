﻿Imports ICBO
Imports ICBO.IC
Imports Telerik.Web.UI
Imports System
Imports System.Data.SqlClient
Namespace IC
    Public Class ICCollectionSQLController
        Public Shared Function GetDate(ByVal Dates As String) As DateTime
            Try
                Dim format() As String = {"dd/MM/yyyy", "d/M/yyyy", "dd-MM-yyyy", "ddMMMyyyy", "dd-MM-yy", "dd/MM/yy", "MMyy", "ddMMyy"}
                Dim ValueDate As Date = Date.ParseExact(Dates, format, System.Globalization.DateTimeFormatInfo.InvariantInfo, Globalization.DateTimeStyles.None)

                Return ValueDate

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub GetSQLCommandFromDataRowByCommandType(ByVal objICInvoiceDBTemp As ICCollectionInvoiceDataTemplate, ByRef ReqSQLStatement As String, ByVal drVerified As DataRow, ByVal UserID As String, ByVal FileID As String, ByVal CommandType As String, ByVal MasterOrDetail As String)
            Dim objICCollectionInvocieDBHeaderFields As ICCollectionInvoiceDataTemplateFieldsCollection
            Dim objICFiles As New ICFiles
            Dim objICCollectionInvoiceDBField As ICCollectionInvoiceDataTemplateFields
            Dim FieldID, FieldName As String
            Dim RefDate As DateTime = Nothing
            Dim CoumnCount As Integer = 0
            Dim WhereClause As String = " WHERE "
            ReqSQLStatement = ""
            FieldID = ""
            objICFiles.LoadByPrimaryKey(FileID)
            If CommandType = "Insert" Then
                If MasterOrDetail = "Detail" Then
                    objICCollectionInvocieDBHeaderFields = New ICCollectionInvoiceDataTemplateFieldsCollection
                    objICCollectionInvocieDBHeaderFields = ICCollectionInvoiceDataTemplateFieldsListController.GetTemplateFieldsByCollectionInvoiceTemplateOfDualFile(objICInvoiceDBTemp.CollectionInvoiceTemplateID.ToString, "Detail")
                    ReqSQLStatement = "INSERT INTO IC_INVOICEDB_" & objICInvoiceDBTemp.TemplateName & "_Detail ("
                    For Each Field As ICCollectionInvoiceDataTemplateFields In objICCollectionInvocieDBHeaderFields
                        ReqSQLStatement += Field.FieldName & ","
                    Next
                    ReqSQLStatement += "FileID,TemplateID,"
                    ReqSQLStatement += ReqSQLStatement.Remove(ReqSQLStatement.Length - 1, 1) & ") Values "
                    ReqSQLStatement += "("
                    For Each dcVer2 As DataColumn In drVerified.Table.Columns
                        If dcVer2.ColumnName.ToString().Contains("-") Then
                            colName = dcVer2.ColumnName.ToString().Split("-")
                            FieldName = colName(0).ToString()
                            FieldID = colName(1).ToString()
                            objICCollectionInvoiceDBField = New ICCollectionInvoiceDataTemplateFields
                            objICCollectionInvoiceDBField.LoadByPrimaryKey(FieldID)
                            If objICCollectionInvoiceDBField.FieldDBType = "String" Then
                                If drVerified(CoumnCount).ToString().Trim <> "" Then

                                    ReqSQLStatement += "'" & drVerified(CoumnCount).ToString().Trim & "',"
                                Else
                                    ReqSQLStatement += "NULL,"
                                End If
                            ElseIf objICCollectionInvoiceDBField.FieldDBType = "Date" Then
                                'ReqSQLStatement += "NULL,"
                                If drVerified(CoumnCount).ToString().Trim <> "" Then
                                    ReqSQLStatement += "CONVERT(DATE,'" & GetDate(drVerified(CoumnCount).ToString().Trim).ToString("yyyy-MM-dd") & "'),"
                                Else
                                    ReqSQLStatement += "NULL,"
                                End If
                                'If drVerified(CoumnCount).ToString().Trim <> "" Then
                                '    ReqSQLStatement += "'" & GetDate(drVerified(CoumnCount).ToString().Trim) & "',"
                                'Else
                                '    ReqSQLStatement += "NULL,"
                                'End If
                            ElseIf objICCollectionInvoiceDBField.FieldDBType = "Decimal" Then
                                If drVerified(CoumnCount).ToString().Trim <> "" Then
                                    ReqSQLStatement += "" & CDbl(drVerified(CoumnCount).ToString().Trim) & ","
                                Else
                                    ReqSQLStatement += "NULL,"
                                End If
                            ElseIf objICCollectionInvoiceDBField.FieldDBType = "Integer" Then
                                If drVerified(CoumnCount).ToString().Trim <> "" Then
                                    ReqSQLStatement += "" & CInt(drVerified(CoumnCount).ToString().Trim) & ","
                                Else
                                    ReqSQLStatement += "NULL,"
                                End If

                            End If
                        End If
                        ReqSQLStatement += "'" & FileID & "','" & objICFiles.FileUploadTemplateID & "',"
                        ReqSQLStatement += ReqSQLStatement.Remove(ReqSQLStatement.Length - 1, 1) & "),"
                        CoumnCount = CoumnCount + 1
                    Next
                    ReqSQLStatement = ReqSQLStatement.Remove(ReqSQLStatement.Length - 1, 1)
                ElseIf MasterOrDetail = "Master" Then
                    objICCollectionInvocieDBHeaderFields = New ICCollectionInvoiceDataTemplateFieldsCollection
                    objICCollectionInvocieDBHeaderFields = ICCollectionInvoiceDataTemplateFieldsListController.GetTemplateFieldsByCollectionInvoiceTemplateOfDualFile(objICInvoiceDBTemp.CollectionInvoiceTemplateID.ToString, "Header")
                    ReqSQLStatement = "INSERT INTO IC_INVOICEDB_" & objICInvoiceDBTemp.TemplateName & "("
                    For Each Field As ICCollectionInvoiceDataTemplateFields In objICCollectionInvocieDBHeaderFields
                        ReqSQLStatement += Field.FieldName & ","
                    Next
                    ReqSQLStatement += "CreatedDate,CreatedBy,Status,LastStatus,FileID,TemplateID" & ") Values "
                    ReqSQLStatement += "("
                    For Each dcVer As DataColumn In drVerified.Table.Columns
                        If dcVer.ColumnName.ToString().Contains("-") Then
                            colName = dcVer.ColumnName.ToString().Split("-")
                            FieldName = colName(0).ToString()
                            FieldID = colName(1).ToString()
                            objICCollectionInvoiceDBField = New ICCollectionInvoiceDataTemplateFields
                            objICCollectionInvoiceDBField.LoadByPrimaryKey(FieldID)
                            If objICCollectionInvoiceDBField.FieldDBType = "String" Then
                                If drVerified(CoumnCount).ToString().Trim <> "" Then
                                    ReqSQLStatement += "'" & drVerified(CoumnCount).ToString().Trim & "',"
                                Else
                                    ReqSQLStatement += "NULL,"
                                End If
                            ElseIf objICCollectionInvoiceDBField.FieldDBType = "Date" Then
                                'ReqSQLStatement += "NULL,"
                                If drVerified(CoumnCount).ToString().Trim <> "" Then
                                    ReqSQLStatement += "CONVERT(DATE,'" & GetDate(drVerified(CoumnCount).ToString().Trim).ToString("yyyy-MM-dd") & "'),"
                                Else
                                    ReqSQLStatement += "NULL,"
                                End If
                                'If drVerified(CoumnCount).ToString().Trim <> "" Then

                                '    ReqSQLStatement += "'" & GetDate(drVerified(CoumnCount).ToString().Trim) & "',"
                                'Else
                                '    ReqSQLStatement += "NULL,"
                                'End If
                            ElseIf objICCollectionInvoiceDBField.FieldDBType = "Decimal" Then
                                If drVerified(CoumnCount).ToString().Trim <> "" Then
                                    ReqSQLStatement += "" & CDbl(drVerified(CoumnCount).ToString().Trim) & ","
                                Else
                                    ReqSQLStatement += "NULL,"
                                End If
                            ElseIf objICCollectionInvoiceDBField.FieldDBType = "Integer" Then
                                If drVerified(CoumnCount).ToString().Trim <> "" Then
                                    ReqSQLStatement += "" & CInt(drVerified(CoumnCount).ToString().Trim) & ","
                                Else
                                    ReqSQLStatement += "NULL,"
                                End If

                            End If
                        End If

                        CoumnCount = CoumnCount + 1
                    Next
                    ReqSQLStatement += "CONVERT(DATE,'" & Date.Now.ToString("yyyy-MM-dd") & "')," & UserID & ",'Pending','Pending','" & FileID & "','" & objICFiles.FileUploadTemplateID & "'),"
                    'ReqSQLStatement += "'" & Date.Now & "'," & UserID & ",'Pending','Pending','" & FileID & "','" & objICFiles.FileUploadTemplateID & "'),"
                    ReqSQLStatement = ReqSQLStatement.Remove(ReqSQLStatement.Length - 1, 1)
                End If
            ElseIf CommandType = "Update" Then
                If MasterOrDetail = "Detail" Then
                    ReqSQLStatement = "UPDATE IC_INVOICEDB_" & objICInvoiceDBTemp.TemplateName & "_Detail SET "
                    For Each dcVer As DataColumn In drVerified.Table.Columns
                        If dcVer.ColumnName.ToString().Contains("-") Then
                            colName = dcVer.ColumnName.ToString().Split("-")
                            FieldName = colName(0).ToString()
                            FieldID = colName(1).ToString()
                            objICCollectionInvoiceDBField = New ICCollectionInvoiceDataTemplateFields
                            objICCollectionInvoiceDBField.LoadByPrimaryKey(FieldID)
                            If objICCollectionInvoiceDBField.FieldType <> "NonFlexi" And objICCollectionInvoiceDBField.IsReference = False Then
                                If objICCollectionInvoiceDBField.FieldDBType = "String" Then
                                    If drVerified(CoumnCount).ToString <> "" Then
                                        ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "='" & drVerified(CoumnCount).ToString & "',"
                                    Else
                                        ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=NULL,"
                                    End If

                                ElseIf objICCollectionInvoiceDBField.FieldDBType = "Date" Then
                                    'ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=NULL,"
                                    'If drVerified(CoumnCount).ToString <> "" Then

                                    '    ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "='" & GetDate(drVerified(CoumnCount).ToString().Trim) & "',"
                                    'Else
                                    '    ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=NULL,"
                                    'End If
                                    If drVerified(CoumnCount).ToString <> "" Then

                                        ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=CONVERT(DATE,'" & GetDate(drVerified(CoumnCount).ToString().Trim).ToString("yyyy-MM-dd") & "'),"
                                    Else
                                        ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=NULL,"
                                    End If
                                ElseIf objICCollectionInvoiceDBField.FieldDBType = "Decimal" Then
                                    If drVerified(CoumnCount).ToString <> "" Then
                                        ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=" & CDbl(drVerified(CoumnCount).ToString) & ","
                                    Else
                                        ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=NULL,"
                                    End If

                                ElseIf objICCollectionInvoiceDBField.FieldDBType = "Integer" Then
                                    If drVerified(CoumnCount).ToString <> "" Then
                                        ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=" & CInt(drVerified(CoumnCount).ToString) & ","
                                    Else
                                        ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=NULL,"
                                    End If

                                End If
                            ElseIf objICCollectionInvoiceDBField.FieldType <> "NonFlexi" And objICCollectionInvoiceDBField.IsReference = True Then
                                If objICCollectionInvoiceDBField.FieldDBType = "String" Then
                                    WhereClause += objICCollectionInvoiceDBField.FieldName & "='" & drVerified(CoumnCount).ToString & "' AND "
                                ElseIf objICCollectionInvoiceDBField.FieldDBType = "Date" Then
                                    WhereClause += objICCollectionInvoiceDBField.FieldName & "='" & drVerified(CoumnCount).ToString & "' AND "
                                ElseIf objICCollectionInvoiceDBField.FieldDBType = "Decimal" Then
                                    WhereClause += objICCollectionInvoiceDBField.FieldName & "=" & CDbl(drVerified(CoumnCount).ToString) & " AND "
                                ElseIf objICCollectionInvoiceDBField.FieldDBType = "Integer" Then
                                    WhereClause += objICCollectionInvoiceDBField.FieldName & "=" & CInt(drVerified(CoumnCount).ToString) & " AND "
                                End If
                            End If
                        End If
                    Next
                    ReqSQLStatement += "FileID='" & FileID & "',TemplateID='" & objICFiles.FileUploadTemplateID & "'" & WhereClause.Remove(WhereClause.Length - 5, 5).ToString
                    CoumnCount = CoumnCount + 1
                ElseIf MasterOrDetail = "Master" Then
                    ReqSQLStatement = "UPDATE IC_INVOICEDB_" & objICInvoiceDBTemp.TemplateName & " SET "
                    For Each dcVer As DataColumn In drVerified.Table.Columns
                        If dcVer.ColumnName.ToString().Contains("-") Then
                            colName = dcVer.ColumnName.ToString().Split("-")
                            FieldName = colName(0).ToString()
                            FieldID = colName(1).ToString()
                            objICCollectionInvoiceDBField = New ICCollectionInvoiceDataTemplateFields
                            objICCollectionInvoiceDBField.LoadByPrimaryKey(FieldID)
                            If objICCollectionInvoiceDBField.FieldType = "NonFlexi" And objICCollectionInvoiceDBField.IsReference = False Then
                                If objICCollectionInvoiceDBField.FieldDBType = "String" Then
                                    If drVerified(CoumnCount).ToString <> "" Then
                                        ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "='" & drVerified(CoumnCount).ToString & "',"
                                    Else
                                        ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=NULL,"
                                    End If

                                ElseIf objICCollectionInvoiceDBField.FieldDBType = "Date" Then
                                    'ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=NULL,"
                                    'If drVerified(CoumnCount).ToString <> "" Then

                                    '    ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "='" & GetDate(drVerified(CoumnCount).ToString().Trim) & "',"
                                    'Else
                                    '    ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=NULL,"
                                    'End If
                                    If drVerified(CoumnCount).ToString <> "" Then

                                        ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=CONVERT(DATE,'" & GetDate(drVerified(CoumnCount).ToString().Trim).ToString("yyyy-MM-dd") & "'),"
                                    Else
                                        ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=NULL,"
                                    End If
                                ElseIf objICCollectionInvoiceDBField.FieldDBType = "Decimal" Then
                                    If drVerified(CoumnCount).ToString <> "" Then
                                        ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=" & CDbl(drVerified(CoumnCount).ToString) & ","
                                    Else
                                        ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=NULL,"
                                    End If

                                ElseIf objICCollectionInvoiceDBField.FieldDBType = "Integer" Then
                                    If drVerified(CoumnCount).ToString <> "" Then
                                        ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=" & CInt(drVerified(CoumnCount).ToString) & ","
                                    Else
                                        ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=NULL,"
                                    End If

                                End If
                            ElseIf objICCollectionInvoiceDBField.FieldType = "NonFlexi" And objICCollectionInvoiceDBField.IsReference = True Then
                                If objICCollectionInvoiceDBField.FieldDBType = "String" Then
                                    WhereClause += objICCollectionInvoiceDBField.FieldName & "='" & drVerified(CoumnCount).ToString & "' AND "
                                ElseIf objICCollectionInvoiceDBField.FieldDBType = "Date" Then
                                    WhereClause += objICCollectionInvoiceDBField.FieldName & "='" & drVerified(CoumnCount).ToString & "' AND "
                                ElseIf objICCollectionInvoiceDBField.FieldDBType = "Decimal" Then
                                    WhereClause += objICCollectionInvoiceDBField.FieldName & "=" & CDbl(drVerified(CoumnCount).ToString) & " AND "
                                ElseIf objICCollectionInvoiceDBField.FieldDBType = "Integer" Then
                                    WhereClause += objICCollectionInvoiceDBField.FieldName & "=" & CInt(drVerified(CoumnCount).ToString) & " AND "
                                End If
                            End If
                        End If
                        CoumnCount = CoumnCount + 1
                    Next

                    ReqSQLStatement += "CreatedDate=CONVERT(DATE,'" & Date.Now.ToString("yyyy-MM-dd") & "'),CreatedBy=" & UserID & ",Status='Pending',LastStatus='Pending',FileID='" & FileID & "',TemplateID='" & objICFiles.FileUploadTemplateID & "'" & WhereClause.Remove(WhereClause.Length - 5, 5).ToString
                End If
            ElseIf CommandType = "Delete" Then
                If MasterOrDetail = "Detail" Then
                    objICCollectionInvocieDBHeaderFields = New ICCollectionInvoiceDataTemplateFieldsCollection
                    objICCollectionInvocieDBHeaderFields = ICCollectionInvoiceDataTemplateFieldsListController.GetTemplateFieldsByCollectionInvoiceTemplateOfDualFile(objICInvoiceDBTemp.CollectionInvoiceTemplateID.ToString, "Detail")
                    ReqSQLStatement = "DELETE FROM IC_INVOICEDB_" & objICInvoiceDBTemp.TemplateName & "_Detail WHERE "
                    For Each Field As ICCollectionInvoiceDataTemplateFields In objICCollectionInvocieDBHeaderFields
                        If Field.IsReference = True Then
                            If Field.FieldDBType = "String" Then
                                WhereClause += Field.FieldName & "='" & drVerified(CoumnCount).ToString & "' AND "
                            ElseIf Field.FieldDBType = "Date" Then
                                WhereClause += Field.FieldName & "='" & drVerified(CoumnCount).ToString & "' AND "
                            ElseIf Field.FieldDBType = "Decimal" Then
                                WhereClause += Field.FieldName & "=" & CDbl(drVerified(CoumnCount).ToString) & " AND "
                            ElseIf Field.FieldDBType = "Integer" Then
                                WhereClause += Field.FieldName & "=" & CInt(drVerified(CoumnCount).ToString) & " AND "
                            End If
                        End If
                        CoumnCount = CoumnCount + 1
                    Next
                    ReqSQLStatement += WhereClause.Remove(WhereClause.Length - 5, 5)
                ElseIf MasterOrDetail = "Master" Then
                    objICCollectionInvocieDBHeaderFields = New ICCollectionInvoiceDataTemplateFieldsCollection
                    objICCollectionInvocieDBHeaderFields = ICCollectionInvoiceDataTemplateFieldsListController.GetTemplateFieldsByCollectionInvoiceTemplateOfDualFile(objICInvoiceDBTemp.CollectionInvoiceTemplateID.ToString, "Header")
                    ReqSQLStatement = "DELETE FROM IC_INVOICEDB_" & objICInvoiceDBTemp.TemplateName & " WHERE "
                    For Each Field As ICCollectionInvoiceDataTemplateFields In objICCollectionInvocieDBHeaderFields
                        If Field.IsReference = True Then
                            If Field.FieldDBType = "String" Then
                                WhereClause += Field.FieldName & "='" & drVerified(CoumnCount).ToString & "' AND "
                            ElseIf Field.FieldDBType = "Decimal" Then
                                WhereClause += Field.FieldName & "=" & CDbl(drVerified(CoumnCount).ToString) & " AND "
                            ElseIf Field.FieldDBType = "Date" Then
                                WhereClause += Field.FieldName & "='" & drVerified(CoumnCount).ToString & "' AND "
                            ElseIf Field.FieldDBType = "Integer" Then
                                WhereClause += Field.FieldName & "=" & CInt(drVerified(CoumnCount).ToString) & " AND "
                            End If
                        End If
                        CoumnCount = CoumnCount + 1
                    Next
                    ReqSQLStatement += WhereClause.Remove(WhereClause.Length - 5, 5)
                End If
            ElseIf CommandType = "Select" Then
                If MasterOrDetail = "Detail" Then
                    objICCollectionInvocieDBHeaderFields = New ICCollectionInvoiceDataTemplateFieldsCollection
                    objICCollectionInvocieDBHeaderFields = ICCollectionInvoiceDataTemplateFieldsListController.GetTemplateFieldsByCollectionInvoiceTemplateOfDualFile(objICInvoiceDBTemp.CollectionInvoiceTemplateID.ToString, "Detail")
                    ReqSQLStatement = "Select * FROM IC_INVOICEDB_" & objICInvoiceDBTemp.TemplateName & "_Detail "
                    For Each Field As ICCollectionInvoiceDataTemplateFields In objICCollectionInvocieDBHeaderFields
                        If Field.IsReference = True Then
                            If Field.FieldDBType = "String" Then
                                WhereClause += Field.FieldName & "='" & drVerified(CoumnCount).ToString & "' AND "
                            ElseIf Field.FieldDBType = "Decimal" Then
                                WhereClause += Field.FieldName & "=" & CDbl(drVerified(CoumnCount).ToString) & " AND "
                            ElseIf Field.FieldDBType = "Date" Then

                                WhereClause += Field.FieldName & "='" & drVerified(CoumnCount).ToString.Trim & "' AND "
                            ElseIf Field.FieldDBType = "Integer" Then
                                WhereClause += Field.FieldName & "=" & CInt(drVerified(CoumnCount).ToString) & " AND "
                            End If
                        End If
                        CoumnCount = CoumnCount + 1
                    Next
                    ReqSQLStatement += WhereClause.Remove(WhereClause.Length - 5, 5)
                ElseIf MasterOrDetail = "Master" Then
                    objICCollectionInvocieDBHeaderFields = New ICCollectionInvoiceDataTemplateFieldsCollection
                    objICCollectionInvocieDBHeaderFields = ICCollectionInvoiceDataTemplateFieldsListController.GetTemplateFieldsByCollectionInvoiceTemplateOfDualFile(objICInvoiceDBTemp.CollectionInvoiceTemplateID.ToString, "Header")
                    ReqSQLStatement = "SELECT * FROM IC_INVOICEDB_" & objICInvoiceDBTemp.TemplateName & " "
                    For Each Field As ICCollectionInvoiceDataTemplateFields In objICCollectionInvocieDBHeaderFields
                        If Field.IsReference = True Then
                            If Field.FieldDBType = "String" Then
                                WhereClause += Field.FieldName & "='" & drVerified(CoumnCount).ToString & "' AND "
                            ElseIf Field.FieldDBType = "Decimal" Then
                                WhereClause += Field.FieldName & "=" & CDbl(drVerified(CoumnCount).ToString) & " AND "
                            ElseIf Field.FieldDBType = "Date" Then

                                WhereClause += Field.FieldName & "='" & drVerified(CoumnCount).ToString.Trim & "' AND "
                            ElseIf Field.FieldDBType = "Integer" Then
                                WhereClause += Field.FieldName & "=" & CInt(drVerified(CoumnCount).ToString) & " AND "
                            End If
                        End If
                        CoumnCount = CoumnCount + 1
                    Next
                    ReqSQLStatement += WhereClause.Remove(WhereClause.Length - 5, 5)
                End If
            End If

        End Sub
        'Public Shared Sub GetSQLCommandFromDataRowByCommandType(ByVal objICInvoiceDBTemp As ICCollectionInvoiceDataTemplate, ByRef ReqSQLStatement As String, ByVal drVerified As DataRow, ByVal UserID As String, ByVal FileID As String, ByVal CommandType As String, ByVal MasterOrDetail As String)
        '    Dim objICCollectionInvocieDBHeaderFields As ICCollectionInvoiceDataTemplateFieldsCollection
        '    Dim objICFiles As New ICFiles
        '    Dim objICCollectionInvoiceDBField As ICCollectionInvoiceDataTemplateFields
        '    Dim FieldID, FieldName As String
        '    Dim CoumnCount As Integer = 0
        '    Dim WhereClause As String = " WHERE "
        '    ReqSQLStatement = ""
        '    FieldID = ""
        '    objICFiles.LoadByPrimaryKey(FileID)
        '    If CommandType = "Insert" Then
        '        If MasterOrDetail = "Detail" Then
        '            objICCollectionInvocieDBHeaderFields = New ICCollectionInvoiceDataTemplateFieldsCollection
        '            objICCollectionInvocieDBHeaderFields = ICCollectionInvoiceDataTemplateFieldsListController.GetTemplateFieldsByCollectionInvoiceTemplateOfDualFile(objICInvoiceDBTemp.CollectionInvoiceTemplateID.ToString, "Detail")
        '            ReqSQLStatement = "INSERT INTO IC_INVOICEDB_" & objICInvoiceDBTemp.TemplateName & "_Detail ("
        '            For Each Field As ICCollectionInvoiceDataTemplateFields In objICCollectionInvocieDBHeaderFields
        '                ReqSQLStatement += Field.FieldName & ","
        '            Next
        '            ReqSQLStatement += "FileID,TemplateID,"
        '            ReqSQLStatement += ReqSQLStatement.Remove(ReqSQLStatement.Length - 1, 1) & ") Values "
        '            ReqSQLStatement += "("
        '            For Each dcVer2 As DataColumn In drVerified.Table.Columns
        '                If dcVer2.ColumnName.ToString().Contains("-") Then
        '                    colName = dcVer2.ColumnName.ToString().Split("-")
        '                    FieldName = colName(0).ToString()
        '                    FieldID = colName(1).ToString()
        '                    objICCollectionInvoiceDBField = New ICCollectionInvoiceDataTemplateFields
        '                    objICCollectionInvoiceDBField.LoadByPrimaryKey(FieldID)
        '                    If objICCollectionInvoiceDBField.FieldDBType = "String" Then
        '                        ReqSQLStatement += "'" & drVerified(CoumnCount).ToString().Trim & "',"
        '                    ElseIf objICCollectionInvoiceDBField.FieldDBType = "Date" Then
        '                        ReqSQLStatement += "'" & drVerified(CoumnCount).ToString().Trim & "',"
        '                    ElseIf objICCollectionInvoiceDBField.FieldDBType = "Decimal" Then
        '                        ReqSQLStatement += "" & CDbl(drVerified(CoumnCount).ToString().Trim) & ","
        '                    ElseIf objICCollectionInvoiceDBField.FieldDBType = "Integer" Then
        '                        ReqSQLStatement += "" & CInt(drVerified(CoumnCount).ToString().Trim) & ","
        '                    End If
        '                End If
        '                ReqSQLStatement += "'" & FileID & "','" & objICFiles.FileUploadTemplateID & "',"
        '                ReqSQLStatement += ReqSQLStatement.Remove(ReqSQLStatement.Length - 1, 1) & "),"
        '                CoumnCount = CoumnCount + 1
        '            Next
        '            ReqSQLStatement = ReqSQLStatement.Remove(ReqSQLStatement.Length - 1, 1)
        '        ElseIf MasterOrDetail = "Master" Then
        '            objICCollectionInvocieDBHeaderFields = New ICCollectionInvoiceDataTemplateFieldsCollection
        '            objICCollectionInvocieDBHeaderFields = ICCollectionInvoiceDataTemplateFieldsListController.GetTemplateFieldsByCollectionInvoiceTemplateOfDualFile(objICInvoiceDBTemp.CollectionInvoiceTemplateID.ToString, "Header")
        '            ReqSQLStatement = "INSERT INTO IC_INVOICEDB_" & objICInvoiceDBTemp.TemplateName & "("
        '            For Each Field As ICCollectionInvoiceDataTemplateFields In objICCollectionInvocieDBHeaderFields
        '                ReqSQLStatement += Field.FieldName & ","
        '            Next
        '            ReqSQLStatement += "CreatedDate,CreatedBy,Status,LastStatus,FileID,TemplateID" & ") Values "
        '            ReqSQLStatement += "("
        '            For Each dcVer As DataColumn In drVerified.Table.Columns
        '                If dcVer.ColumnName.ToString().Contains("-") Then
        '                    colName = dcVer.ColumnName.ToString().Split("-")
        '                    FieldName = colName(0).ToString()
        '                    FieldID = colName(1).ToString()
        '                    objICCollectionInvoiceDBField = New ICCollectionInvoiceDataTemplateFields
        '                    objICCollectionInvoiceDBField.LoadByPrimaryKey(FieldID)
        '                    If objICCollectionInvoiceDBField.FieldDBType = "String" Then
        '                        ReqSQLStatement += "'" & drVerified(CoumnCount).ToString & "',"
        '                    ElseIf objICCollectionInvoiceDBField.FieldDBType = "Date" Then
        '                        ReqSQLStatement += "'" & drVerified(CoumnCount).ToString().Trim & "',"
        '                    ElseIf objICCollectionInvoiceDBField.FieldDBType = "Decimal" Then
        '                        ReqSQLStatement += "" & CDbl(drVerified(CoumnCount).ToString) & ","
        '                    ElseIf objICCollectionInvoiceDBField.FieldDBType = "Integer" Then
        '                        ReqSQLStatement += "" & CInt(drVerified(CoumnCount).ToString) & ","
        '                    End If
        '                End If

        '                CoumnCount = CoumnCount + 1
        '            Next
        '            ReqSQLStatement += "'" & Date.Now & "'," & UserID & ",'Pending','Pending','" & FileID & "','" & objICFiles.FileUploadTemplateID & "'),"
        '            ReqSQLStatement = ReqSQLStatement.Remove(ReqSQLStatement.Length - 1, 1)
        '        End If
        '    ElseIf CommandType = "Update" Then
        '        If MasterOrDetail = "Detail" Then
        '            ReqSQLStatement = "UPDATE IC_INVOICEDB_" & objICInvoiceDBTemp.TemplateName & "_Detail SET "
        '            For Each dcVer As DataColumn In drVerified.Table.Columns
        '                If dcVer.ColumnName.ToString().Contains("-") Then
        '                    colName = dcVer.ColumnName.ToString().Split("-")
        '                    FieldName = colName(0).ToString()
        '                    FieldID = colName(1).ToString()
        '                    objICCollectionInvoiceDBField = New ICCollectionInvoiceDataTemplateFields
        '                    objICCollectionInvoiceDBField.LoadByPrimaryKey(FieldID)
        '                    If objICCollectionInvoiceDBField.FieldType <> "NonFlexi" And objICCollectionInvoiceDBField.IsReference = False Then
        '                        If objICCollectionInvoiceDBField.FieldDBType = "String" Then
        '                            ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "='" & drVerified(CoumnCount).ToString & "',"
        '                        ElseIf objICCollectionInvoiceDBField.FieldDBType = "Date" Then
        '                            ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "='" & drVerified(CoumnCount).ToString & "',"
        '                        ElseIf objICCollectionInvoiceDBField.FieldDBType = "Decimal" Then
        '                            ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=" & CDbl(drVerified(CoumnCount).ToString) & ","
        '                        ElseIf objICCollectionInvoiceDBField.FieldDBType = "Integer" Then
        '                            ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=" & CInt(drVerified(CoumnCount).ToString) & ","
        '                        End If
        '                    ElseIf objICCollectionInvoiceDBField.FieldType <> "NonFlexi" And objICCollectionInvoiceDBField.IsReference = True Then
        '                        If objICCollectionInvoiceDBField.FieldDBType = "String" Then
        '                            WhereClause += objICCollectionInvoiceDBField.FieldName & "='" & drVerified(CoumnCount).ToString & " AND "
        '                        ElseIf objICCollectionInvoiceDBField.FieldDBType = "Date" Then
        '                            WhereClause += objICCollectionInvoiceDBField.FieldName & "='" & drVerified(CoumnCount).ToString & "' AND "
        '                        ElseIf objICCollectionInvoiceDBField.FieldDBType = "Decimal" Then
        '                            WhereClause += objICCollectionInvoiceDBField.FieldName & "=" & CDbl(drVerified(CoumnCount).ToString) & " AND "
        '                        ElseIf objICCollectionInvoiceDBField.FieldDBType = "Integer" Then
        '                            WhereClause += objICCollectionInvoiceDBField.FieldName & "=" & CInt(drVerified(CoumnCount).ToString) & " AND "
        '                        End If
        '                    End If
        '                End If
        '            Next
        '            ReqSQLStatement += "FileID='" & FileID & "',TemplateID='" & objICFiles.FileUploadTemplateID & "'" & WhereClause.Remove(WhereClause.Length - 5, 5).ToString
        '            CoumnCount = CoumnCount + 1
        '        ElseIf MasterOrDetail = "Master" Then
        '            ReqSQLStatement = "UPDATE IC_INVOICEDB_" & objICInvoiceDBTemp.TemplateName & " SET "
        '            For Each dcVer As DataColumn In drVerified.Table.Columns
        '                If dcVer.ColumnName.ToString().Contains("-") Then
        '                    colName = dcVer.ColumnName.ToString().Split("-")
        '                    FieldName = colName(0).ToString()
        '                    FieldID = colName(1).ToString()
        '                    objICCollectionInvoiceDBField = New ICCollectionInvoiceDataTemplateFields
        '                    objICCollectionInvoiceDBField.LoadByPrimaryKey(FieldID)
        '                    If objICCollectionInvoiceDBField.FieldType = "NonFlexi" And objICCollectionInvoiceDBField.IsReference = False Then
        '                        If objICCollectionInvoiceDBField.FieldDBType = "String" Then
        '                            ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "='" & drVerified(CoumnCount).ToString & "',"
        '                        ElseIf objICCollectionInvoiceDBField.FieldDBType = "Date" Then
        '                            ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "='" & drVerified(CoumnCount).ToString & "',"
        '                        ElseIf objICCollectionInvoiceDBField.FieldDBType = "Decimal" Then
        '                            ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=" & CDbl(drVerified(CoumnCount).ToString) & ","
        '                        ElseIf objICCollectionInvoiceDBField.FieldDBType = "Integer" Then
        '                            ReqSQLStatement += objICCollectionInvoiceDBField.FieldName & "=" & CInt(drVerified(CoumnCount).ToString) & ","
        '                        End If
        '                    ElseIf objICCollectionInvoiceDBField.FieldType = "NonFlexi" And objICCollectionInvoiceDBField.IsReference = True Then
        '                        If objICCollectionInvoiceDBField.FieldDBType = "String" Then
        '                            WhereClause += objICCollectionInvoiceDBField.FieldName & "='" & drVerified(CoumnCount).ToString & " AND "
        '                        ElseIf objICCollectionInvoiceDBField.FieldDBType = "Date" Then
        '                            WhereClause += objICCollectionInvoiceDBField.FieldName & "='" & drVerified(CoumnCount).ToString & "' AND "
        '                        ElseIf objICCollectionInvoiceDBField.FieldDBType = "Decimal" Then
        '                            WhereClause += objICCollectionInvoiceDBField.FieldName & "=" & CDbl(drVerified(CoumnCount).ToString) & " AND "
        '                        ElseIf objICCollectionInvoiceDBField.FieldDBType = "Integer" Then
        '                            WhereClause += objICCollectionInvoiceDBField.FieldName & "=" & CInt(drVerified(CoumnCount).ToString) & " AND "
        '                        End If
        '                    End If
        '                End If
        '                CoumnCount = CoumnCount + 1
        '            Next
        '            ReqSQLStatement += "CreatedDate='" & Date.Now & "',CreatedBy=" & UserID & ",Status='Pending',LastStatus='Pending',FileID='" & FileID & "',TemplateID='" & objICFiles.FileUploadTemplateID & "'" & WhereClause.Remove(WhereClause.Length - 5, 5).ToString
        '        End If
        '    ElseIf CommandType = "Delete" Then
        '        If MasterOrDetail = "Detail" Then
        '            objICCollectionInvocieDBHeaderFields = New ICCollectionInvoiceDataTemplateFieldsCollection
        '            objICCollectionInvocieDBHeaderFields = ICCollectionInvoiceDataTemplateFieldsListController.GetTemplateFieldsByCollectionInvoiceTemplateOfDualFile(objICInvoiceDBTemp.CollectionInvoiceTemplateID.ToString, "Detail")
        '            ReqSQLStatement = "DELETE FROM IC_INVOICEDB_" & objICInvoiceDBTemp.TemplateName & "_Detail WHERE "
        '            For Each Field As ICCollectionInvoiceDataTemplateFields In objICCollectionInvocieDBHeaderFields
        '                If Field.IsReference = True Then
        '                    If Field.FieldDBType = "String" Then
        '                        WhereClause += Field.FieldName & "='" & drVerified(CoumnCount).ToString & " AND "
        '                    ElseIf Field.FieldDBType = "Date" Then
        '                        WhereClause += Field.FieldName & "='" & drVerified(CoumnCount).ToString & "' AND "
        '                    ElseIf Field.FieldDBType = "Decimal" Then
        '                        WhereClause += Field.FieldName & "=" & CDbl(drVerified(CoumnCount).ToString) & " AND "
        '                    ElseIf Field.FieldDBType = "Integer" Then
        '                        WhereClause += Field.FieldName & "=" & CInt(drVerified(CoumnCount).ToString) & " AND "
        '                    End If
        '                End If
        '                CoumnCount = CoumnCount + 1
        '            Next
        '            ReqSQLStatement += WhereClause.Remove(WhereClause.Length - 5, 5)
        '        ElseIf MasterOrDetail = "Master" Then
        '            objICCollectionInvocieDBHeaderFields = New ICCollectionInvoiceDataTemplateFieldsCollection
        '            objICCollectionInvocieDBHeaderFields = ICCollectionInvoiceDataTemplateFieldsListController.GetTemplateFieldsByCollectionInvoiceTemplateOfDualFile(objICInvoiceDBTemp.CollectionInvoiceTemplateID.ToString, "Header")
        '            ReqSQLStatement = "DELETE FROM IC_INVOICEDB_" & objICInvoiceDBTemp.TemplateName & " WHERE "
        '            For Each Field As ICCollectionInvoiceDataTemplateFields In objICCollectionInvocieDBHeaderFields
        '                If Field.IsReference = True Then
        '                    If Field.FieldDBType = "String" Then
        '                        WhereClause += Field.FieldName & "='" & drVerified(CoumnCount).ToString & " AND "
        '                    ElseIf Field.FieldDBType = "Decimal" Then
        '                        WhereClause += Field.FieldName & "=" & CDbl(drVerified(CoumnCount).ToString) & " AND "
        '                    ElseIf Field.FieldDBType = "Date" Then
        '                        WhereClause += Field.FieldName & "='" & drVerified(CoumnCount).ToString & "' AND "
        '                    ElseIf Field.FieldDBType = "Integer" Then
        '                        WhereClause += Field.FieldName & "=" & CInt(drVerified(CoumnCount).ToString) & " AND "
        '                    End If
        '                End If
        '                CoumnCount = CoumnCount + 1
        '            Next
        '            ReqSQLStatement += WhereClause.Remove(WhereClause.Length - 5, 5)
        '        End If
        '    ElseIf CommandType = "Select" Then
        '        If MasterOrDetail = "Detail" Then
        '            objICCollectionInvocieDBHeaderFields = New ICCollectionInvoiceDataTemplateFieldsCollection
        '            objICCollectionInvocieDBHeaderFields = ICCollectionInvoiceDataTemplateFieldsListController.GetTemplateFieldsByCollectionInvoiceTemplateOfDualFile(objICInvoiceDBTemp.CollectionInvoiceTemplateID.ToString, "Detail")
        '            ReqSQLStatement = "Select * FROM IC_INVOICEDB_" & objICInvoiceDBTemp.TemplateName & "_Detail WHERE "
        '            For Each Field As ICCollectionInvoiceDataTemplateFields In objICCollectionInvocieDBHeaderFields
        '                If Field.IsReference = True Then
        '                    If Field.FieldDBType = "String" Then
        '                        WhereClause += Field.FieldName & "='" & drVerified(CoumnCount).ToString & " AND "
        '                    ElseIf Field.FieldDBType = "Decimal" Then
        '                        WhereClause += Field.FieldName & "=" & CDbl(drVerified(CoumnCount).ToString) & " AND "
        '                    ElseIf Field.FieldDBType = "Date" Then
        '                        WhereClause += Field.FieldName & "='" & drVerified(CoumnCount).ToString & "' AND "
        '                    ElseIf Field.FieldDBType = "Integer" Then
        '                        WhereClause += Field.FieldName & "=" & CInt(drVerified(CoumnCount).ToString) & " AND "
        '                    End If
        '                End If
        '                CoumnCount = CoumnCount + 1
        '            Next
        '            ReqSQLStatement += WhereClause.Remove(WhereClause.Length - 5, 5)
        '        ElseIf MasterOrDetail = "Master" Then
        '            objICCollectionInvocieDBHeaderFields = New ICCollectionInvoiceDataTemplateFieldsCollection
        '            objICCollectionInvocieDBHeaderFields = ICCollectionInvoiceDataTemplateFieldsListController.GetTemplateFieldsByCollectionInvoiceTemplateOfDualFile(objICInvoiceDBTemp.CollectionInvoiceTemplateID.ToString, "Header")
        '            ReqSQLStatement = "SELECT * FROM IC_INVOICEDB_" & objICInvoiceDBTemp.TemplateName & " WHERE "
        '            For Each Field As ICCollectionInvoiceDataTemplateFields In objICCollectionInvocieDBHeaderFields
        '                If Field.IsReference = True Then
        '                    If Field.FieldDBType = "String" Then
        '                        WhereClause += Field.FieldName & "='" & drVerified(CoumnCount).ToString & " AND "
        '                    ElseIf Field.FieldDBType = "Decimal" Then
        '                        WhereClause += Field.FieldName & "=" & CDbl(drVerified(CoumnCount).ToString) & " AND "
        '                    ElseIf Field.FieldDBType = "Date" Then
        '                        WhereClause += Field.FieldName & "='" & drVerified(CoumnCount).ToString & "' AND "
        '                    ElseIf Field.FieldDBType = "Integer" Then
        '                        WhereClause += Field.FieldName & "=" & CInt(drVerified(CoumnCount).ToString) & " AND "
        '                    End If
        '                End If
        '                CoumnCount = CoumnCount + 1
        '            Next
        '            ReqSQLStatement += WhereClause.Remove(WhereClause.Length - 5, 5)
        '        End If
        '    End If

        'End Sub
        Public Shared Sub GetSQLCommandFromReferenceFieldsByCommandType(ByVal InvoiceTemplateID As String, ByRef ReqSQLStatement As String, ByVal UserID As String, ByVal FileID As String, ByVal CommandType As String, ByVal MasterOrDetail As String, ByVal Ref1 As String, ByVal Ref2 As String, ByVal Ref3 As String, ByVal Ref4 As String, ByVal Ref5 As String, ByVal Status As String, ByVal LastStatus As String)
            Dim objICCollectionInvocieDBHeaderFields As ICCollectionInvoiceDataTemplateFieldsCollection
            Dim objICFiles As New ICFiles
            Dim CoumnCount As Integer = 0
            Dim WhereClause As String = " WHERE "
            ReqSQLStatement = ""
            If FileID IsNot Nothing Then
                objICFiles.LoadByPrimaryKey(FileID)
            End If
            objICCollectionInvocieDBHeaderFields = New ICCollectionInvoiceDataTemplateFieldsCollection
            objICCollectionInvocieDBHeaderFields = ICCollectionInvoiceDataTemplateFieldsListController.GetReferenceFieldsCollectionInvoiceTemplateOfDualFile(InvoiceTemplateID)
            If objICCollectionInvocieDBHeaderFields(0).UpToICCollectionInvoiceDataTemplateByCollectionInvoiceTemplateID.TemplateType = "Dual" Then
                MasterOrDetail = "Detail"
            Else
                MasterOrDetail = "Master"
            End If
            If CommandType = "Insert" Then
                If MasterOrDetail = "Detail" Then
                    ReqSQLStatement = "INSERT INTO IC_INVOICEDB_" & objICCollectionInvocieDBHeaderFields(0).UpToICCollectionInvoiceDataTemplateByCollectionInvoiceTemplateID.TemplateName & "_Detail ("
                    For Each Field As ICCollectionInvoiceDataTemplateFields In objICCollectionInvocieDBHeaderFields
                        ReqSQLStatement += Field.FieldName & ","
                    Next
                    ReqSQLStatement += "FileID,TemplateID,"
                    ReqSQLStatement += ReqSQLStatement.Remove(ReqSQLStatement.Length - 1, 1) & ") Values "
                    ReqSQLStatement += "("
                    For i = 0 To objICCollectionInvocieDBHeaderFields.Count - 1
                        Select Case i.ToString
                            Case "0"
                                If Ref1 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        ReqSQLStatement += "'" & Ref1.ToString().Trim & "',"
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                        ReqSQLStatement += "" & CDbl(Ref1.ToString().Trim) & ","
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Date" Then
                                        ReqSQLStatement += "'" & Ref1.ToString().Trim & "',"
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        ReqSQLStatement += "" & CInt(Ref1.ToString().Trim) & ","
                                    End If
                                End If
                            Case "1"
                                If Ref2 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        ReqSQLStatement += "'" & Ref2.ToString().Trim & "',"
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                        ReqSQLStatement += "" & CDbl(Ref2.ToString().Trim) & ","
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Date" Then
                                        ReqSQLStatement += "'" & Ref2.ToString().Trim & "',"
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        ReqSQLStatement += "" & CInt(Ref2.ToString().Trim) & ","
                                    End If
                                End If
                            Case "2"
                                If Ref3 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        ReqSQLStatement += "'" & Ref3.ToString().Trim & "',"
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                        ReqSQLStatement += "" & CDbl(Ref3.ToString().Trim) & ","
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Date" Then
                                        ReqSQLStatement += "'" & Ref3.ToString().Trim & "',"
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        ReqSQLStatement += "" & CInt(Ref3.ToString().Trim) & ","
                                    End If
                                End If
                            Case "3"
                                If Ref4 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        ReqSQLStatement += "'" & Ref4.ToString().Trim & "',"
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                        ReqSQLStatement += "" & CDbl(Ref4.ToString().Trim) & ","
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Date" Then
                                        ReqSQLStatement += "'" & Ref4.ToString().Trim & "',"
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        ReqSQLStatement += "" & CInt(Ref4.ToString().Trim) & ","
                                    End If
                                End If
                            Case "4"
                                If Ref5 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        ReqSQLStatement += "'" & Ref5.ToString().Trim & "',"
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then

                                        ReqSQLStatement += "" & CDbl(Ref5.ToString().Trim) & ","
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Date" Then
                                        ReqSQLStatement += "'" & Ref5.ToString().Trim & "',"
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        ReqSQLStatement += "" & CInt(Ref5.ToString().Trim) & ","
                                    End If
                                End If
                        End Select
                    Next
                    ReqSQLStatement += "'" & FileID & "','" & InvoiceTemplateID & "',"
                    ReqSQLStatement += ReqSQLStatement.Remove(ReqSQLStatement.Length - 1, 1) & ")"
                ElseIf MasterOrDetail = "Master" Then
                    ReqSQLStatement = "INSERT INTO IC_INVOICEDB_" & objICCollectionInvocieDBHeaderFields(0).UpToICCollectionInvoiceDataTemplateByCollectionInvoiceTemplateID.TemplateName & " ("
                    For Each Field As ICCollectionInvoiceDataTemplateFields In objICCollectionInvocieDBHeaderFields
                        ReqSQLStatement += Field.FieldName & ","
                    Next
                    ReqSQLStatement += "CreatedDate,CreatedBy,Status,LastStatus,FileID,TemplateID" & ") Values "
                    ReqSQLStatement += "("
                    For i = 0 To objICCollectionInvocieDBHeaderFields.Count - 1
                        Select Case i.ToString
                            Case "0"
                                If Ref1 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        ReqSQLStatement += "'" & Ref1.ToString().Trim & "',"
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                        ReqSQLStatement += "" & CDbl(Ref1.ToString().Trim) & ","
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        ReqSQLStatement += "" & CInt(Ref1.ToString().Trim) & ","
                                    End If
                                End If
                            Case "1"
                                If Ref2 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        ReqSQLStatement += "'" & Ref2.ToString().Trim & "',"
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                        ReqSQLStatement += "" & CDbl(Ref2.ToString().Trim) & ","
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        ReqSQLStatement += "" & CInt(Ref2.ToString().Trim) & ","
                                    End If
                                End If
                            Case "2"
                                If Ref3 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        ReqSQLStatement += "'" & Ref3.ToString().Trim & "',"
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                        ReqSQLStatement += "" & CDbl(Ref3.ToString().Trim) & ","
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        ReqSQLStatement += "" & CInt(Ref3.ToString().Trim) & ","
                                    End If
                                End If
                            Case "3"
                                If Ref4 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        ReqSQLStatement += "'" & Ref4.ToString().Trim & "',"
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                        ReqSQLStatement += "" & CDbl(Ref4.ToString().Trim) & ","
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        ReqSQLStatement += "" & CInt(Ref4.ToString().Trim) & ","
                                    End If
                                End If
                            Case "4"
                                If Ref5 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        ReqSQLStatement += "'" & Ref5.ToString().Trim & "',"
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                        ReqSQLStatement += "" & CDbl(Ref5.ToString().Trim) & ","
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        ReqSQLStatement += "" & CInt(Ref5.ToString().Trim) & ","
                                    End If
                                End If
                        End Select
                    Next
                    ReqSQLStatement += "'" & Date.Now & "'," & UserID & ",'" & Status & "','" & LastStatus & "',"
                    If FileID IsNot Nothing Then
                        ReqSQLStatement += "'" & FileID & "',"
                    Else
                        ReqSQLStatement += "NULL,"
                    End If
                    If InvoiceTemplateID IsNot Nothing Then
                        If FileID IsNot Nothing Then
                            ReqSQLStatement += "'" & InvoiceTemplateID & "')"
                        Else
                            ReqSQLStatement += "NULL)"
                        End If
                    End If
                End If
            ElseIf CommandType = "Delete" Then
                If MasterOrDetail = "Detail" Then
                    ReqSQLStatement = "DELETE FROM IC_INVOICEDB_" & objICCollectionInvocieDBHeaderFields(0).UpToICCollectionInvoiceDataTemplateByCollectionInvoiceTemplateID.TemplateName & "_Detail"
                Else
                    ReqSQLStatement = "DELETE FROM IC_INVOICEDB_" & objICCollectionInvocieDBHeaderFields(0).UpToICCollectionInvoiceDataTemplateByCollectionInvoiceTemplateID.TemplateName & ""
                End If
                For i = 0 To objICCollectionInvocieDBHeaderFields.Count - 1
                    Select Case i.ToString
                        Case "0"
                            If Ref1 IsNot Nothing Then
                                If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref1.ToString().Trim & "' AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref1.ToString().Trim) & " AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref1.ToString().Trim) & " AND "
                                End If
                            End If
                        Case "1"
                            If Ref2 IsNot Nothing Then
                                If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref2.ToString().Trim & "' AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref2.ToString().Trim) & " AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref2.ToString().Trim) & " AND "
                                End If
                            End If
                        Case "2"
                            If Ref3 IsNot Nothing Then
                                If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref3.ToString().Trim & "' AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref3.ToString().Trim) & " AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref3.ToString().Trim) & " AND "
                                End If
                            End If
                        Case "3"
                            If Ref4 IsNot Nothing Then
                                If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref4.ToString().Trim & "' AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref4.ToString().Trim) & " AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref4.ToString().Trim) & " AND "
                                End If
                            End If
                        Case "4"
                            If Ref5 IsNot Nothing Then
                                If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref5.ToString().Trim & "' AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref5.ToString().Trim) & " AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref5.ToString().Trim) & " AND "
                                End If
                            End If
                    End Select
                Next
                ReqSQLStatement += WhereClause.Remove(WhereClause.Length - 5, 5)

            ElseIf CommandType = "Select" Then
                If MasterOrDetail = "Detail" Then
                    ReqSQLStatement = "SELECT * FROM IC_INVOICEDB_" & objICCollectionInvocieDBHeaderFields(0).UpToICCollectionInvoiceDataTemplateByCollectionInvoiceTemplateID.TemplateName & "_Detail"
                Else
                    ReqSQLStatement = "SELECT * FROM IC_INVOICEDB_" & objICCollectionInvocieDBHeaderFields(0).UpToICCollectionInvoiceDataTemplateByCollectionInvoiceTemplateID.TemplateName & ""
                End If
                For i = 0 To objICCollectionInvocieDBHeaderFields.Count - 1
                    Select Case i.ToString
                        Case "0"
                            If Ref1 IsNot Nothing Then
                                If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref1.ToString().Trim & "' AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref1.ToString().Trim) & " AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref1.ToString().Trim) & " AND "
                                End If
                            End If
                        Case "1"
                            If Ref2 IsNot Nothing Then
                                If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref2.ToString().Trim & "' AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref2.ToString().Trim) & " AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref2.ToString().Trim) & " AND "
                                End If
                            End If
                        Case "2"
                            If Ref3 IsNot Nothing Then
                                If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref3.ToString().Trim & "' AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref3.ToString().Trim) & " AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref3.ToString().Trim) & " AND "
                                End If
                            End If
                        Case "3"
                            If Ref4 IsNot Nothing Then
                                If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref4.ToString().Trim & "' AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref4.ToString().Trim) & " AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref4.ToString().Trim) & " AND "
                                End If
                            End If
                        Case "4"
                            If Ref5 IsNot Nothing Then
                                If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref5.ToString().Trim & "' AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref5.ToString().Trim) & " AND "
                                ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                    WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref5.ToString().Trim) & " AND "
                                End If
                            End If
                    End Select
                Next
                ReqSQLStatement += WhereClause.Remove(WhereClause.Length - 5, 5)
            ElseIf CommandType = "Update" Then
                If MasterOrDetail = "Detail" Then
                    ReqSQLStatement = "UPDATE IC_INVOICEDB_" & objICCollectionInvocieDBHeaderFields(0).UpToICCollectionInvoiceDataTemplateByCollectionInvoiceTemplateID.TemplateName & "_Detail SET "
                    If FileID IsNot Nothing Then
                        ReqSQLStatement += "FileID='" & FileID & "',"
                    Else
                        ReqSQLStatement += "FileID=NULL,"
                    End If
                    If InvoiceTemplateID IsNot Nothing Then
                        ReqSQLStatement += "TemplateID='" & InvoiceTemplateID & "',"
                    Else
                        ReqSQLStatement += "TemplateID=NULL"
                    End If
                    For i = 0 To objICCollectionInvocieDBHeaderFields.Count - 1
                        Select Case i.ToString
                            Case "0"
                                If Ref1 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref1.ToString().Trim & "' AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref1.ToString().Trim) & " AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref1.ToString().Trim) & " AND "
                                    End If
                                End If
                            Case "1"
                                If Ref2 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref2.ToString().Trim & "' AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref2.ToString().Trim) & " AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref2.ToString().Trim) & " AND "
                                    End If
                                End If
                            Case "2"
                                If Ref3 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref3.ToString().Trim & "' AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref3.ToString().Trim) & " AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref3.ToString().Trim) & " AND "
                                    End If
                                End If
                            Case "3"
                                If Ref4 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref4.ToString().Trim & "' AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref4.ToString().Trim) & " AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref4.ToString().Trim) & " AND "
                                    End If
                                End If
                            Case "4"
                                If Ref5 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref5.ToString().Trim & "' AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref5.ToString().Trim) & " AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref5.ToString().Trim) & " AND "
                                    End If
                                End If
                        End Select
                    Next
                    ReqSQLStatement += WhereClause.Remove(WhereClause.Length - 5, 5)
                ElseIf MasterOrDetail = "Master" Then
                    ReqSQLStatement = "UPDATE IC_INVOICEDB_" & objICCollectionInvocieDBHeaderFields(0).UpToICCollectionInvoiceDataTemplateByCollectionInvoiceTemplateID.TemplateName & ""
                    ReqSQLStatement += " SET CreatedDate='" & Date.Now & "',"
                    ReqSQLStatement += "Status='" & Status & "',"
                    ReqSQLStatement += "LastStatus='" & LastStatus & "',"
                    If FileID IsNot Nothing And FileID <> "" Then
                        ReqSQLStatement += "FileID='" & FileID & "',"
                    Else
                        ReqSQLStatement += "FileID=NULL,"
                    End If
                    If InvoiceTemplateID IsNot Nothing And InvoiceTemplateID <> "" Then
                        ReqSQLStatement += "TemplateID='" & InvoiceTemplateID & "',"
                    Else
                        ReqSQLStatement += "TemplateID=NULL,"
                    End If

                    ReqSQLStatement = ReqSQLStatement.Remove(ReqSQLStatement.Length - 1, 1) & " "
                    For i = 0 To objICCollectionInvocieDBHeaderFields.Count - 1
                        Select Case i.ToString
                            Case "0"
                                If Ref1 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref1.ToString().Trim & "' AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref1.ToString().Trim) & " AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref1.ToString().Trim) & " AND "
                                    End If
                                End If
                            Case "1"
                                If Ref2 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref2.ToString().Trim & "' AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref2.ToString().Trim) & " AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref2.ToString().Trim) & " AND "
                                    End If
                                End If
                            Case "2"
                                If Ref3 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref3.ToString().Trim & "' AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref3.ToString().Trim) & " AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref3.ToString().Trim) & " AND "
                                    End If
                                End If
                            Case "3"
                                If Ref4 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref4.ToString().Trim & "' AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref4.ToString().Trim) & " AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref4.ToString().Trim) & " AND "
                                    End If
                                End If
                            Case "4"
                                If Ref5 IsNot Nothing Then
                                    If objICCollectionInvocieDBHeaderFields(i).FieldDBType = "String" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "='" & Ref5.ToString().Trim & "' AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Decimal" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CDbl(Ref5.ToString().Trim) & " AND "
                                    ElseIf objICCollectionInvocieDBHeaderFields(i).FieldDBType = "Integer" Then
                                        WhereClause += objICCollectionInvocieDBHeaderFields(i).FieldName & "=" & CInt(Ref5.ToString().Trim) & " AND "
                                    End If
                                End If
                        End Select
                    Next
                    ReqSQLStatement += WhereClause.Remove(WhereClause.Length - 5, 5)
                End If
            End If

        End Sub
        Public Shared Function ExecuteSqlStatementForCreateTableVIATemplate(ByVal SQLStringToExecute As String) As Boolean
            Dim Result As Boolean = False
            Dim SQLConn As New SqlConnection
            Dim SQLCmd As SqlCommand
            Dim ConnectionString As String = System.Configuration.ConfigurationManager.ConnectionStrings("SiteSqlServer").ToString
            If SQLConn.State = ConnectionState.Open Then
                SQLConn.Close()
                SQLConn.Dispose()
            End If
            SQLConn.ConnectionString = ConnectionString
            SQLConn.Open()
            SQLCmd = New SqlCommand(SQLStringToExecute, SQLConn)
            Try
                SQLCmd.ExecuteNonQuery()
                SQLConn.Close()
                SQLConn.Dispose()
                SQLConn = New SqlConnection
                SQLConn.ConnectionString = ConnectionString
                SQLConn.Open()
                SQLConn.Close()
                SQLConn.Dispose()
                Result = True
            Catch ex As SqlException
                Result = False
                Throw ex
            End Try
            Return Result
        End Function
        Public Shared Function IsInvoiceRecordAlreadyExists(ByVal SQLCommand As String) As Boolean
            Dim Result As Boolean = False
            Dim SQLConn As New SqlConnection
            Dim SQLCmd As SqlCommand
            Dim da As SqlDataAdapter
            Dim dt As New DataTable
            Dim ConnectionString As String = System.Configuration.ConfigurationManager.ConnectionStrings("SiteSqlServer").ToString
            Try
                If SQLConn.State = ConnectionState.Open Then
                    SQLConn.Close()
                    SQLConn.Dispose()
                End If
                SQLConn = New SqlConnection
                SQLConn.ConnectionString = ConnectionString
                SQLCmd = New SqlCommand()
                SQLCmd.CommandText = SQLCommand
                SQLConn.Open()
                da = New SqlDataAdapter(SQLCmd.CommandText.ToString, SQLConn)
                da.Fill(dt)


                If dt.Rows.Count > 0 Then
                    Result = True
                Else
                    Result = False
                End If
                SQLConn.Close()
                SQLConn.Dispose()
                Return Result
            Catch ex As SqlException
                Result = False
                Throw ex
            End Try
            Return Result
        End Function
        Public Shared Function GetInvoiceRecordBySQLStatement(ByVal SQLCommand As String) As DataTable

            Dim SQLConn As New SqlConnection
            Dim SQLCmd As SqlCommand
            Dim da As SqlDataAdapter
            Dim dt As New DataTable
            Dim ConnectionString As String = System.Configuration.ConfigurationManager.ConnectionStrings("SiteSqlServer").ToString
            Try
                If SQLConn.State = ConnectionState.Open Then
                    SQLConn.Close()
                    SQLConn.Dispose()
                End If
                SQLConn = New SqlConnection
                SQLConn.ConnectionString = ConnectionString
                SQLCmd = New SqlCommand()
                SQLCmd.CommandText = SQLCommand
                SQLConn.Open()
                da = New SqlDataAdapter(SQLCmd.CommandText.ToString, SQLConn)
                da.Fill(dt)
                SQLConn.Close()
                SQLConn.Dispose()
                Return dt
            Catch ex As SqlException
                Throw ex
            End Try
        End Function
        'Public Shared Function GetSQLStringWithFieldsByTemplateID(ByVal CollectionInvoiceDataTemplateID As String, ByRef StrForAuditTrail As String, ByRef SqlStringDetail As String) As String

        '    Dim objICCollectionInvoiceDataTemplate As New ICCollectionInvoiceDataTemplate
        '    Dim objICCollectionInvoiceDataTemplateFieldColl As New ICCollectionInvoiceDataTemplateFieldsCollection
        '    objICCollectionInvoiceDataTemplate.LoadByPrimaryKey(CollectionInvoiceDataTemplateID)
        '    objICCollectionInvoiceDataTemplateFieldColl = ICCollectionInvoiceDataTemplateFieldsListController.GetTemplateFieldsTemplateID(CollectionInvoiceDataTemplateID)
        '    Dim SqlStringNonFlexi As String = Nothing
        '    SqlStringDetail = Nothing
        '    Dim i As Integer = 0
        '    Dim SqlPK As String = " PRIMARY KEY ("
        '    Dim SqlFK As String = Nothing



        '    SqlStringNonFlexi = "CREATE TABLE IC_InvoiceDB_" & objICCollectionInvoiceDataTemplate.TemplateName & "("
        '    SqlStringDetail += "CREATE TABLE IC_InvoiceDB_" & objICCollectionInvoiceDataTemplate.TemplateName & "_Detail("
        '    StrForAuditTrail = "Database [ IC_InvoiceDB_" & objICCollectionInvoiceDataTemplate.TemplateName & " ] for template [ " & objICCollectionInvoiceDataTemplate.TemplateName & " ] created with fields : </br>"
        '    StrForAuditTrail += "Follwoing fields are set as Primary Key: </br>"

        '    For Each objICPrimaryKeyField As ICCollectionInvoiceDataTemplateFields In objICCollectionInvoiceDataTemplateFieldColl
        '        If objICPrimaryKeyField.IsReference = True Then
        '            StrForAuditTrail += objICPrimaryKeyField.FieldName & " " & objICPrimaryKeyField.FieldType
        '        End If
        '    Next



        '    For Each objICCollectionInvoiceDataTemplateField As ICCollectionInvoiceDataTemplateFields In objICCollectionInvoiceDataTemplateFieldColl
        '        If objICCollectionInvoiceDataTemplateField.FieldType = "NonFlexi" Then
        '            SqlStringNonFlexi += objICCollectionInvoiceDataTemplateField.FieldName
        '            If objICCollectionInvoiceDataTemplateField.IsReference = False Then
        '                StrForAuditTrail += "[ " & objICCollectionInvoiceDataTemplateField.FieldName & " ]"
        '                StrForAuditTrail += "[ " & objICCollectionInvoiceDataTemplateField.FieldDBType & " ] ,"
        '            End If
        '            If objICCollectionInvoiceDataTemplateField.FieldDBType = "Decimal" Then
        '                If objICCollectionInvoiceDataTemplateField.IsReference = True Then
        '                    SqlStringDetail += objICCollectionInvoiceDataTemplateField.FieldName
        '                    SqlStringDetail += " Numeric(18,2) NOT NULL ,"
        '                    SqlStringNonFlexi += " Numeric(18,2) NOT NULL ,"
        '                    SqlPK += objICCollectionInvoiceDataTemplateField.FieldName & ","
        '                    SqlFK += objICCollectionInvoiceDataTemplateField.FieldName & ","
        '                Else
        '                    SqlStringNonFlexi += " Numeric(18,2) NULL ,"
        '                End If

        '            ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "Integer" Then
        '                If objICCollectionInvoiceDataTemplateField.IsReference = True Then
        '                    SqlStringDetail += objICCollectionInvoiceDataTemplateField.FieldName
        '                    SqlStringDetail += " Integer NOT NULL ,"
        '                    SqlStringNonFlexi += " Integer NOT NULL ,"
        '                    SqlPK += objICCollectionInvoiceDataTemplateField.FieldName & ","
        '                    SqlFK += objICCollectionInvoiceDataTemplateField.FieldName & ","
        '                Else
        '                    SqlStringNonFlexi += " Integer NULL ,"
        '                End If
        '            ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "String" Then
        '                If objICCollectionInvoiceDataTemplateField.IsReference = True Then
        '                    SqlStringDetail += objICCollectionInvoiceDataTemplateField.FieldName
        '                    SqlStringDetail += " Varchar(" & objICCollectionInvoiceDataTemplateField.FieldLength & ") NOT NULL ,"
        '                    SqlStringNonFlexi += " Varchar(" & objICCollectionInvoiceDataTemplateField.FieldLength & ") NOT NULL ,"
        '                    SqlPK += objICCollectionInvoiceDataTemplateField.FieldName & ","
        '                    SqlFK += objICCollectionInvoiceDataTemplateField.FieldName & ","
        '                Else
        '                    SqlStringNonFlexi += " Varchar(" & objICCollectionInvoiceDataTemplateField.FieldLength & ") NULL ,"
        '                End If
        '            ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "Date" Then
        '                If objICCollectionInvoiceDataTemplateField.IsReference = True Then
        '                    SqlStringDetail += objICCollectionInvoiceDataTemplateField.FieldName
        '                    SqlStringDetail += " DateTime NOT NULL ,"
        '                    SqlStringNonFlexi += " DateTime NOT NULL ,"
        '                    SqlPK += objICCollectionInvoiceDataTemplateField.FieldName & ","
        '                    SqlFK += objICCollectionInvoiceDataTemplateField.FieldName & ","
        '                Else
        '                    SqlStringNonFlexi += " DateTime NULL ,"
        '                End If
        '            End If
        '        Else
        '            SqlStringDetail += objICCollectionInvoiceDataTemplateField.FieldName
        '            If objICCollectionInvoiceDataTemplateField.IsReference = False Then
        '                StrForAuditTrail += "[ " & objICCollectionInvoiceDataTemplateField.FieldName & " ]"
        '                StrForAuditTrail += "[ " & objICCollectionInvoiceDataTemplateField.FieldDBType & " ] ,"
        '            End If
        '            If objICCollectionInvoiceDataTemplateField.FieldDBType = "Decimal" Then
        '                SqlStringDetail += " Numeric(18,2) NULL ,"
        '            ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "Integer" Then
        '                SqlStringDetail += " Integer NULL ,"
        '            ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "Date" Then
        '                SqlStringDetail += " DateTime NULL ,"
        '            ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "String" Then
        '                SqlStringDetail += " Varchar(" & objICCollectionInvoiceDataTemplateField.FieldLength & ") NULL ,"
        '            End If
        '        End If
        '    Next
        '    SqlStringNonFlexi += " CreatedDate DateTime NULL, CreatedBy int NULL,Status varchar(20),LastStatus varchar(20),FileID Varchar(20) NULL,TemplateID Varchar(20) NULL,"
        '    SqlPK = SqlPK.Remove(SqlPK.Length - 1, 1) & ")"
        '    SqlFK = SqlFK.Remove(SqlFK.Length - 1, 1)
        '    'SqlString = SqlString.Remove(SqlString.Length - 1, 1)
        '    SqlStringNonFlexi += SqlPK
        '    SqlStringNonFlexi += ")"
        '    SqlStringDetail += "FileID Varchar(20) NULL,TemplateID Varchar(20) NULL, " & SqlPK & ", CONSTRAINT fk_IC_InvoiceDB_" & objICCollectionInvoiceDataTemplate.TemplateName & " FOREIGN KEY("
        '    SqlStringDetail += SqlFK & ") REFERENCES IC_InvoiceDB_" & objICCollectionInvoiceDataTemplate.TemplateName & "(" & SqlFK & ")"
        '    SqlStringDetail += ")"
        '    StrForAuditTrail = StrForAuditTrail.Remove(StrForAuditTrail.Length - 1, 1)
        '    Return SqlStringNonFlexi

        'End Function

        Public Shared Function GetSQLStringWithFieldsByTemplateID(ByVal CollectionInvoiceDataTemplateID As String, ByRef StrForAuditTrail As String, ByRef SqlStringDetail As String) As String

            Dim objICCollectionInvoiceDataTemplate As New ICCollectionInvoiceDataTemplate
            Dim objICCollectionInvoiceDataTemplateFieldColl As New ICCollectionInvoiceDataTemplateFieldsCollection
            objICCollectionInvoiceDataTemplate.LoadByPrimaryKey(CollectionInvoiceDataTemplateID)
            objICCollectionInvoiceDataTemplateFieldColl = ICCollectionInvoiceDataTemplateFieldsListController.GetTemplateFieldsTemplateID(CollectionInvoiceDataTemplateID)
            Dim SqlStringNonFlexi As String = Nothing
            SqlStringDetail = Nothing
            Dim i As Integer = 0
            Dim SqlPK As String = " PRIMARY KEY ("
            Dim SqlFK As String = Nothing



            SqlStringNonFlexi = "CREATE TABLE IC_InvoiceDB_" & objICCollectionInvoiceDataTemplate.TemplateName & "("
            SqlStringDetail += "CREATE TABLE IC_InvoiceDB_" & objICCollectionInvoiceDataTemplate.TemplateName & "_Detail("
            StrForAuditTrail = "Database [ IC_InvoiceDB_" & objICCollectionInvoiceDataTemplate.TemplateName & " ] for template [ " & objICCollectionInvoiceDataTemplate.TemplateName & " ] created with fields : </br>"
            StrForAuditTrail += "Follwoing fields are set as Primary Key: </br>"

            For Each objICPrimaryKeyField As ICCollectionInvoiceDataTemplateFields In objICCollectionInvoiceDataTemplateFieldColl
                If objICPrimaryKeyField.IsReference = True Then
                    StrForAuditTrail += objICPrimaryKeyField.FieldName & " " & objICPrimaryKeyField.FieldType
                End If
            Next



            For Each objICCollectionInvoiceDataTemplateField As ICCollectionInvoiceDataTemplateFields In objICCollectionInvoiceDataTemplateFieldColl
                If objICCollectionInvoiceDataTemplateField.FieldType = "NonFlexi" Then
                    SqlStringNonFlexi += objICCollectionInvoiceDataTemplateField.FieldName
                    If objICCollectionInvoiceDataTemplateField.IsReference = False Then
                        StrForAuditTrail += "[ " & objICCollectionInvoiceDataTemplateField.FieldName & " ]"
                        StrForAuditTrail += "[ " & objICCollectionInvoiceDataTemplateField.FieldDBType & " ] ,"
                    End If
                    If objICCollectionInvoiceDataTemplateField.FieldDBType = "Decimal" Then
                        If objICCollectionInvoiceDataTemplateField.IsReference = True Then
                            SqlStringDetail += objICCollectionInvoiceDataTemplateField.FieldName
                            SqlStringDetail += " Numeric(18,2) NOT NULL ,"
                            SqlStringNonFlexi += " Numeric(18,2) NOT NULL ,"
                            SqlPK += objICCollectionInvoiceDataTemplateField.FieldName & ","
                            SqlFK += objICCollectionInvoiceDataTemplateField.FieldName & ","
                        Else
                            SqlStringNonFlexi += " Numeric(18,2) NULL ,"
                        End If

                    ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "Integer" Then
                        If objICCollectionInvoiceDataTemplateField.IsReference = True Then
                            SqlStringDetail += objICCollectionInvoiceDataTemplateField.FieldName
                            SqlStringDetail += " Integer NOT NULL ,"
                            SqlStringNonFlexi += " Integer NOT NULL ,"
                            SqlPK += objICCollectionInvoiceDataTemplateField.FieldName & ","
                            SqlFK += objICCollectionInvoiceDataTemplateField.FieldName & ","
                        Else
                            SqlStringNonFlexi += " Integer NULL ,"
                        End If
                    ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "String" Then
                        If objICCollectionInvoiceDataTemplateField.IsReference = True Then
                            SqlStringDetail += objICCollectionInvoiceDataTemplateField.FieldName
                            SqlStringDetail += " Varchar(" & objICCollectionInvoiceDataTemplateField.FieldLength & ") NOT NULL ,"
                            SqlStringNonFlexi += " Varchar(" & objICCollectionInvoiceDataTemplateField.FieldLength & ") NOT NULL ,"
                            SqlPK += objICCollectionInvoiceDataTemplateField.FieldName & ","
                            SqlFK += objICCollectionInvoiceDataTemplateField.FieldName & ","
                        Else
                            SqlStringNonFlexi += " Varchar(" & objICCollectionInvoiceDataTemplateField.FieldLength & ") NULL ,"
                        End If
                    ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "Date" Then
                        If objICCollectionInvoiceDataTemplateField.IsReference = True Then
                            SqlStringDetail += objICCollectionInvoiceDataTemplateField.FieldName
                            SqlStringDetail += " DateTime NOT NULL ,"
                            SqlStringNonFlexi += " DateTime NOT NULL ,"
                            SqlPK += objICCollectionInvoiceDataTemplateField.FieldName & ","
                            SqlFK += objICCollectionInvoiceDataTemplateField.FieldName & ","
                        Else
                            SqlStringNonFlexi += " DateTime NULL ,"
                        End If
                    End If
                Else
                    SqlStringDetail += objICCollectionInvoiceDataTemplateField.FieldName
                    If objICCollectionInvoiceDataTemplateField.IsReference = False Then
                        StrForAuditTrail += "[ " & objICCollectionInvoiceDataTemplateField.FieldName & " ]"
                        StrForAuditTrail += "[ " & objICCollectionInvoiceDataTemplateField.FieldDBType & " ] ,"
                    End If
                    If objICCollectionInvoiceDataTemplateField.FieldDBType = "Decimal" Then
                        SqlStringDetail += " Numeric(18,2) NULL ,"
                    ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "Integer" Then
                        SqlStringDetail += " Integer NULL ,"
                    ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "Date" Then
                        SqlStringDetail += " DateTime NULL ,"
                    ElseIf objICCollectionInvoiceDataTemplateField.FieldDBType = "String" Then
                        SqlStringDetail += " Varchar(" & objICCollectionInvoiceDataTemplateField.FieldLength & ") NULL ,"
                    End If
                End If
            Next
            SqlStringNonFlexi += " CreatedDate DateTime NULL, CreatedBy int NULL,Status varchar(50),LastStatus varchar(50),FileID Varchar(20) NULL,TemplateID Varchar(20) NULL,"
            SqlPK = SqlPK.Remove(SqlPK.Length - 1, 1) & ")"
            SqlFK = SqlFK.Remove(SqlFK.Length - 1, 1)

            'SqlString = SqlString.Remove(SqlString.Length - 1, 1)
            SqlStringNonFlexi += SqlPK
            SqlStringNonFlexi += ")"
            SqlStringDetail += "FileID Varchar(20) NULL,TemplateID Varchar(20) NULL, CONSTRAINT fk_IC_InvoiceDB_" & objICCollectionInvoiceDataTemplate.TemplateName & " FOREIGN KEY("
            SqlStringDetail += SqlFK & ") REFERENCES IC_InvoiceDB_" & objICCollectionInvoiceDataTemplate.TemplateName & "(" & SqlFK & ")"
            SqlStringDetail += ")"
            StrForAuditTrail = StrForAuditTrail.Remove(StrForAuditTrail.Length - 1, 1)
            Return SqlStringNonFlexi

        End Function

        Public Shared Function GetInvoiceRecordsByReferenceFields(ByVal objICCollectionInvoiceDBTemplate As ICCollectionInvoiceDataTemplate, ByVal ReferenceField1 As String, ByVal ReferenceField2 As String, ByVal ReferenceField3 As String, ByVal ReferenceField4 As String, ByVal ReferenceField5 As String) As DataTable

            Dim objICCollectionInvoiceDBTempField As New ICCollectionInvoiceDataTemplateFieldsCollection
            objICCollectionInvoiceDBTempField = ICCollectionInvoiceDataTemplateFieldsListController.GetReferenceFieldsCollectionInvoiceTemplateOfDualFile(objICCollectionInvoiceDBTemplate.CollectionInvoiceTemplateID.ToString)
            Dim SQLConn As New SqlConnection
            Dim FieldCount As Integer = 1
            Dim SQLCmd As SqlCommand
            Dim SelectCommand As String = "SELECT * FROM IC_InvoiceDB_" & objICCollectionInvoiceDBTemplate.TemplateName & " WHERE "
            For Each Field As ICCollectionInvoiceDataTemplateFields In objICCollectionInvoiceDBTempField
                If FieldCount = 1 Then
                    If ReferenceField1 IsNot Nothing Then
                        If Field.FieldDBType = "String" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField1 & "' AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Integer" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField1 & " AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Decimal" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField1 & " AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Date" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField1 & "' AND "
                            FieldCount = FieldCount + 1
                        End If
                    Else
                        FieldCount = FieldCount + 1
                    End If
                ElseIf FieldCount = 2 Then
                    If ReferenceField2 IsNot Nothing Then
                        If Field.FieldDBType = "String" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField2 & "' AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Integer" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField2 & " AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Decimal" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField2 & " AND "
                            FieldCount = FieldCount + 1
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Date" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField2 & "' AND "
                            FieldCount = FieldCount + 1
                        End If
                    Else
                        FieldCount = FieldCount + 1
                    End If
                ElseIf FieldCount = 3 Then
                    If ReferenceField3 IsNot Nothing Then
                        If Field.FieldDBType = "String" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField3 & "' AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Integer" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField3 & " AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Decimal" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField3 & " AND "
                            FieldCount = FieldCount + 1
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Date" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField3 & "' AND "
                            FieldCount = FieldCount + 1
                        End If
                    Else
                        FieldCount = FieldCount + 1
                    End If
                ElseIf FieldCount = 4 Then
                    If ReferenceField4 IsNot Nothing Then
                        If Field.FieldDBType = "String" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField4 & "' AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Integer" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField4 & " AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Decimal" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField4 & " AND "

                            FieldCount = FieldCount + 1
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Date" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField4 & "' AND "
                            FieldCount = FieldCount + 1
                        End If
                    Else
                        FieldCount = FieldCount + 1
                    End If
                ElseIf FieldCount = 5 Then
                    If ReferenceField5 IsNot Nothing Then
                        If Field.FieldDBType = "String" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField5 & "' AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Integer" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField5 & " AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Decimal" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField5 & " AND "
                            FieldCount = FieldCount + 1
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Date" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField5 & "' AND "
                            FieldCount = FieldCount + 1
                        End If
                    Else
                        FieldCount = FieldCount + 1
                    End If
                End If
            Next
            SelectCommand = SelectCommand.Remove(SelectCommand.Length - 5, 5)

            Dim dt As New DataTable
            dt = GetInvoiceRecordBySQLStatement(SelectCommand)
            Return dt
        End Function
        Public Shared Function UpdateInvoiceRecordsStatusByReferenceFields(ByVal TemplateID As String, ByVal ReferenceField1 As String, ByVal ReferenceField2 As String, ByVal ReferenceField3 As String, ByVal ReferenceField4 As String, ByVal ReferenceField5 As String, ByVal FromStatus As String, ByVal ToStatus As String, ByVal UserID As String, ByVal UserName As String) As Boolean

            Dim objICCollectionInvoiceDBTempField As New ICCollectionInvoiceDataTemplateFieldsCollection
            Dim objICCollectionInvoiceDBTemplate As New ICCollectionInvoiceDataTemplate
            Dim ActionString As String = ""
            Dim RelatedID As String = ReferenceField1 & ReferenceField2 & ReferenceField3 & ReferenceField4 & ReferenceField5
            objICCollectionInvoiceDBTemplate.LoadByPrimaryKey(TemplateID)
            objICCollectionInvoiceDBTempField = ICCollectionInvoiceDataTemplateFieldsListController.GetReferenceFieldsCollectionInvoiceTemplateOfDualFile(objICCollectionInvoiceDBTemplate.CollectionInvoiceTemplateID.ToString)

            Dim FieldCount As Integer = 1
            Dim Result As Boolean = False
            Dim SelectCommand As String = "Update IC_InvoiceDB_" & objICCollectionInvoiceDBTemplate.TemplateName & " SET Status='" & ToStatus & "',LastStatus='" & FromStatus & "' WHERE "
            For Each Field As ICCollectionInvoiceDataTemplateFields In objICCollectionInvoiceDBTempField
                If FieldCount = 1 Then
                    If ReferenceField1 IsNot Nothing Then
                        If Field.FieldDBType = "String" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField1 & "' AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Integer" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField1 & " AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Date" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField1 & "' AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Decimal" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField1 & " AND "
                            FieldCount = FieldCount + 1
                        End If
                    Else
                        FieldCount = FieldCount + 1
                    End If
                ElseIf FieldCount = 2 Then
                    If ReferenceField2 IsNot Nothing Then
                        If Field.FieldDBType = "String" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField2 & "' AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Integer" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField2 & " AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Decimal" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField2 & " AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Date" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField2 & "' AND "
                            FieldCount = FieldCount + 1
                        End If
                    Else
                        FieldCount = FieldCount + 1
                    End If
                ElseIf FieldCount = 3 Then
                    If ReferenceField3 IsNot Nothing Then
                        If Field.FieldDBType = "String" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField3 & "' AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Integer" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField3 & " AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Decimal" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField3 & " AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Date" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField3 & "' AND "
                            FieldCount = FieldCount + 1
                        End If
                    Else
                        FieldCount = FieldCount + 1
                    End If
                ElseIf FieldCount = 4 Then
                    If ReferenceField4 IsNot Nothing Then
                        If Field.FieldDBType = "String" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField4 & "' AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Integer" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField4 & " AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Decimal" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField4 & " AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Date" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField4 & "' AND "
                            FieldCount = FieldCount + 1
                        End If
                    Else
                        FieldCount = FieldCount + 1
                    End If
                ElseIf FieldCount = 5 Then
                    If ReferenceField5 IsNot Nothing Then
                        If Field.FieldDBType = "String" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField5 & "' AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Integer" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField5 & " AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Decimal" Then
                            SelectCommand += Field.FieldName & "=" & ReferenceField5 & " AND "
                            FieldCount = FieldCount + 1
                        ElseIf Field.FieldDBType = "Date" Then
                            SelectCommand += Field.FieldName & "='" & ReferenceField5 & "' AND "
                            FieldCount = FieldCount + 1
                        End If
                    Else
                        FieldCount = FieldCount + 1
                    End If
                End If
            Next
            SelectCommand = SelectCommand.Remove(SelectCommand.Length - 5, 5)
            Try
                If ExecuteSqlStatementForCreateTableVIATemplate(SelectCommand) = True Then
                    Result = True
                    ActionString = "Invoice status update from [ " & FromStatus & " ] to [ " & ToStatus & " ]. Action was taken by user [ " & UserID & " ] [ " & UserName & " ]."
                    ICUtilities.AddAuditTrail(ActionString, "Invoice", RelatedID, UserID, UserName, "UPDATE")
                Else
                    ActionString = "Invoice status update fail from [ " & FromStatus & " ] to [ " & ToStatus & " ]. Action was taken by user [ " & UserID & " ] [ " & UserName & " ]."
                    ICUtilities.AddAuditTrail(ActionString, "Invoice", RelatedID, UserID, UserName, "UPDATE")
                    Result = False
                End If
            Catch ex As Exception
                ActionString = "Invoice status update fail from [ " & FromStatus & " ] to [ " & ToStatus & " ] due to " & ex.Message.ToString & ". Action was taken by user [ " & UserID & " ] [ " & UserName & " ]."
                ICUtilities.AddAuditTrail(ActionString, "Invoice", RelatedID, UserID, UserName, "ERROR")
                Result = False
            End Try
            Return Result
        End Function
        Public Shared Function GetReqFieldDataByFieldIDAndTemplateName(ByVal Ref1 As String, ByVal Ref2 As String, ByVal Ref3 As String, ByVal Ref4 As String, ByVal Ref5 As String, ByVal FieldID As String) As String
            Dim objICCollectionInvoiceDBField As New ICCollectionInvoiceDataTemplateFields
            Dim objICCollectionInvoiceDBFieldColl As New ICCollectionInvoiceDataTemplateFieldsCollection
            Dim SQLCmd As String = Nothing
            Dim dt As New DataTable
            objICCollectionInvoiceDBField.LoadByPrimaryKey(FieldID)

            If objICCollectionInvoiceDBField.FieldType = "NonFlexi" Then
                SQLCmd = "SELECT " & objICCollectionInvoiceDBField.FieldName & " FROM IC_InvoiceDB_" & objICCollectionInvoiceDBField.UpToICCollectionInvoiceDataTemplateByCollectionInvoiceTemplateID.TemplateName & " WHERE "
            Else
                SQLCmd = "SELECT " & objICCollectionInvoiceDBField.FieldName & " FROM IC_InvoiceDB_" & objICCollectionInvoiceDBField.UpToICCollectionInvoiceDataTemplateByCollectionInvoiceTemplateID.TemplateName & "_Detail WHERE "
            End If
            objICCollectionInvoiceDBFieldColl = ICCollectionInvoiceDataTemplateFieldsListController.GetReferenceFieldsCollectionInvoiceTemplateOfDualFile(objICCollectionInvoiceDBField.CollectionInvoiceTemplateID.ToString)
            For i = 0 To objICCollectionInvoiceDBFieldColl.Count - 1
                Select Case i.ToString
                    Case "0"
                        If Ref1 IsNot Nothing Then
                            If objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Integer" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref1 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Decimal" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref1 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "String" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref1 & "' AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Date" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref1 & "' AND "
                            End If
                        End If
                    Case "1"
                        If Ref2 IsNot Nothing Then
                            If objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Integer" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref2 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Decimal" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref2 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "String" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref2 & "' AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Date" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref2 & "' AND "
                            End If
                        End If
                    Case "2"
                        If Ref3 IsNot Nothing Then
                            If objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Integer" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref3 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Decimal" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref3 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "String" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref3 & "' AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Date" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref3 & "' AND "
                            End If
                        End If
                    Case "3"
                        If Ref4 IsNot Nothing Then
                            If objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Integer" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref4 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Decimal" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref4 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "String" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref4 & "' AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Date" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref4 & "' AND "
                            End If
                        End If
                    Case "4"
                        If Ref5 IsNot Nothing Then
                            If objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Integer" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref5 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Decimal" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref5 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "String" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref5 & "' AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Date" Then
                                SQLCmd += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref5 & "' AND "
                            End If
                        End If
                End Select
            Next
            SQLCmd = SQLCmd.Remove(SQLCmd.Length - 5, 5)
            dt = GetInvoiceRecordBySQLStatement(SQLCmd)
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0)(0).ToString
            Else
                Return ""
            End If
        End Function
        Public Shared Function GetWhereClauseStatementWithTemplateIDIncluded(ByVal objICCollectionNature As ICCollectionNature, ByVal Ref1 As String, ByVal Ref2 As String, ByVal Ref3 As String, ByVal Ref4 As String, ByVal Ref5 As String) As String

            Dim WhereClause As String = " WHERE TemplateID='" & objICCollectionNature.InvoiceTemplateID & "' AND "


            Dim objICCollectionInvoiceDBFieldColl As New ICCollectionInvoiceDataTemplateFieldsCollection
            objICCollectionInvoiceDBFieldColl = ICCollectionInvoiceDataTemplateFieldsListController.GetReferenceFieldsCollectionInvoiceTemplateOfDualFile(objICCollectionNature.InvoiceTemplateID.ToString)
            For i = 0 To objICCollectionInvoiceDBFieldColl.Count - 1
                Select Case i.ToString
                    Case "0"
                        If Ref1 IsNot Nothing Then
                            If objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Integer" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref1 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Decimal" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref1 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "String" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref1 & "' AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Date" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref1 & "' AND "
                            End If
                        End If
                    Case "1"
                        If Ref2 IsNot Nothing Then
                            If objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Integer" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref2 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Decimal" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref2 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "String" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref2 & "' AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Date" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref2 & "' AND "
                            End If
                        End If
                    Case "2"
                        If Ref3 IsNot Nothing Then
                            If objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Integer" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref3 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Decimal" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref3 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "String" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref3 & "' AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Date" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref3 & "' AND "
                            End If
                        End If
                    Case "3"
                        If Ref4 IsNot Nothing Then
                            If objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Integer" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref4 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Decimal" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref4 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "String" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref4 & "' AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Date" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref4 & "' AND "
                            End If
                        End If
                    Case "4"
                        If Ref5 IsNot Nothing Then
                            If objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Integer" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref5 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Decimal" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "=" & Ref5 & " AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "String" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref5 & "' AND "
                            ElseIf objICCollectionInvoiceDBFieldColl(i).FieldDBType = "Date" Then
                                WhereClause += objICCollectionInvoiceDBFieldColl(i).FieldName & "='" & Ref5 & "' AND "
                            End If
                        End If
                End Select
            Next
            WhereClause = WhereClause.Remove(WhereClause.Length - 5, 5)
            Return WhereClause
        End Function
        Public Shared Function GetAmountAndDueDateDataByCollectionNature(ByVal objICCollectionNature As ICCollectionNature, ByVal Ref1 As String, ByVal Ref2 As String, ByVal Ref3 As String, ByVal Ref4 As String, ByVal Ref5 As String) As DataTable
            Try
                Dim SelectCommand As String = Nothing
                Dim WhereClause As String = Nothing
                Dim dt As New DataTable
                SelectCommand = "Select " & objICCollectionNature.UpToICCollectionInvoiceDataTemplateFieldsByDueDateFieldID.FieldName & " AS DueDate,"
                SelectCommand += "" & objICCollectionNature.UpToICCollectionInvoiceDataTemplateFieldsByAmountBeforeDueDateFieldID.FieldName & " AS AmountBeforeDueDate,"
                SelectCommand += "" & objICCollectionNature.UpToICCollectionInvoiceDataTemplateFieldsByAmountAfterDueDateFieldID.FieldName & " AS AmountAfterDueDate "
                SelectCommand += " FROM IC_InvoiceDB_" & objICCollectionNature.UpToICCollectionInvoiceDataTemplateFieldsByDueDateFieldID.UpToICCollectionInvoiceDataTemplateByCollectionInvoiceTemplateID.TemplateName
                WhereClause = GetWhereClauseStatementWithTemplateIDIncluded(objICCollectionNature, Ref1, Ref2, Ref3, Ref4, Ref5)
                SelectCommand += WhereClause
                dt = GetInvoiceRecordBySQLStatement(SelectCommand)
                Return dt
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetRecordsforDBPurged(ByVal objICCollectionNature As ICCollectionNature, ByVal StartDate As String, ByVal EndDate As String, ByVal FilteredDate As String) As DataTable
            Try
                Dim SelectCommand As String = Nothing
                Dim WhereClause As String = Nothing
                Dim dt As New DataTable

                SelectCommand = "SELECT * "
                SelectCommand += "FROM IC_InvoiceDB_" & objICCollectionNature.UpToICCollectionInvoiceDataTemplateByInvoiceTemplateID.TemplateName & " "
                'If FilteredDate <> "0" Then
                WhereClause += "WHERE (" & FilteredDate & " >= '" & StartDate & "') AND (" & FilteredDate & " <= '" & EndDate & "') "
                'End If
                WhereClause += "AND Status = 'Pending' "
                SelectCommand += WhereClause

                dt = GetInvoiceRecordBySQLStatement(SelectCommand)
                Return dt
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function DBPurged(ByVal objICCollectionNature As ICCollectionNature, ByVal StartDate As String, ByVal EndDate As String, ByVal FilteredDate As String) As Boolean
            Try
                Dim SelectCommand As String = Nothing
                Dim WhereClause As String = Nothing
                Dim dt As New DataTable
                Dim IsSuccessfullyPurged As Boolean = False

                SelectCommand = "DELETE "
                SelectCommand += "FROM IC_InvoiceDB_" & objICCollectionNature.UpToICCollectionInvoiceDataTemplateByInvoiceTemplateID.TemplateName & " "
                'If FilteredDate <> "0" Then
                WhereClause += "WHERE (" & FilteredDate & " >= '" & StartDate & "') AND (" & FilteredDate & " <= '" & EndDate & "') "
                'End If
                WhereClause += "AND Status = 'Pending' "
                SelectCommand += WhereClause

                dt = GetInvoiceRecordBySQLStatement(SelectCommand)

                If dt.Rows.Count = "0" Then
                    IsSuccessfullyPurged = True
                End If
                Return IsSuccessfullyPurged

            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace
