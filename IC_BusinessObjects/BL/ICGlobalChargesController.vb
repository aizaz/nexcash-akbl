﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Imports EntitySpaces.Core

Public Class ICGlobalChargesController
    Public Shared Sub AddGlobalCharges(ByVal ICGlobal As ICBO.IC.ICGlobalCharges, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String)
        Dim objGlobalCharges As New IC.ICGlobalCharges



        objGlobalCharges.es.Connection.CommandTimeout = 3600

        If IsUpdate = False Then
            objGlobalCharges.CreatedBy = ICGlobal.CreatedBy
            objGlobalCharges.CreatedOn = ICGlobal.CreatedOn

        Else
            objGlobalCharges.LoadByPrimaryKey(ICGlobal.GlobalChargeID)
            objGlobalCharges.CreatedBy = ICGlobal.CreatedBy
            objGlobalCharges.CreatedOn = ICGlobal.CreatedOn

        End If



        objGlobalCharges.Title = ICGlobal.Title
        objGlobalCharges.ChargeType = ICGlobal.ChargeType


        If objGlobalCharges.ChargeType = "Fixed" Then

            objGlobalCharges.ChargeAmount = ICGlobal.ChargeAmount

            objGlobalCharges.ChargePercentage = Nothing



        Else

            objGlobalCharges.ChargeAmount = Nothing



            objGlobalCharges.ChargePercentage = ICGlobal.ChargePercentage


        End If



        objGlobalCharges.ChargeAmount = ICGlobal.ChargeAmount
        objGlobalCharges.ChargePercentage = ICGlobal.ChargePercentage
        objGlobalCharges.Active = ICGlobal.Active
      
        objGlobalCharges.PrincipalBankAccountID = ICGlobal.PrincipalBankAccountID
        objGlobalCharges.Save()

      

        If (IsUpdate = False) Then
            ICUtilities.AddAuditTrail(" Added ", "GlobalCharges", objGlobalCharges.GlobalChargeID.ToString(), UsersID.ToString(), UsersName.ToString(), "ADD")
        Else
            ICUtilities.AddAuditTrail(" Updated ", "GlobalCharges", objGlobalCharges.GlobalChargeID.ToString(), UsersID.ToString(), UsersName.ToString(), "UPDATE")
        End If


    End Sub


    Public Shared Sub GetAllGlobalCharges(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
        Dim qryObjICGlobalCharges As New IC.ICGlobalChargesQuery("qryICGlobalCharges")
        ' Dim qryObjICGroup As New I("qryObjICGroup")
        Dim coll As New IC.ICGlobalChargesCollection

        qryObjICGlobalCharges.Select(qryObjICGlobalCharges.GlobalChargeID, qryObjICGlobalCharges.Title, qryObjICGlobalCharges.ChargeType, qryObjICGlobalCharges.ChargeAmount, qryObjICGlobalCharges.Active)
        'qryObjICGlobalCharges.InnerJoin(qryObjICGroup).On(qryObjICGlobalCharges.GroupCode = qryObjICGroup.GroupCode)
        qryObjICGlobalCharges.OrderBy(qryObjICGlobalCharges.Title, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)




        If Not pagenumber = 0 Then

            qryObjICGlobalCharges.es.PageNumber = pagenumber
            qryObjICGlobalCharges.es.PageSize = pagesize

            coll.Load(qryObjICGlobalCharges)

            rg.DataSource = coll
            If DoDataBind Then

                rg.DataBind()
            End If
        Else
            qryObjICGlobalCharges.es.PageNumber = 1
            qryObjICGlobalCharges.es.PageSize = pagesize

            coll.Load(qryObjICGlobalCharges)

            rg.DataSource = coll


            If DoDataBind Then


                rg.DataBind()

            End If
        End If
    End Sub

    Public Shared Sub DeleteGlobalChargesByGlobalID(ByVal GlobalID As String, ByVal userid As String, ByVal username As String)
        Dim objICGlobalCharges As New IC.ICGlobalCharges
        Dim strGlobalID, StrTitle As String
        objICGlobalCharges.es.Connection.CommandTimeout = 3600

        If objICGlobalCharges.LoadByPrimaryKey(GlobalID) Then
            strGlobalID = objICGlobalCharges.GlobalChargeID.ToString
            StrTitle = objICGlobalCharges.Title.ToString
            objICGlobalCharges.MarkAsDeleted()
            objICGlobalCharges.Save()
            ICUtilities.AddAuditTrail("GlobalCharges Title  : " & StrTitle & " deleted.", "GlobalCharges", GlobalID.ToString(), userid.ToString, username.ToString, "DELETE")
        End If
    End Sub

    Public Shared Function GetClearingAccounts(ByVal Accountype As String, ByVal UsersID As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

        Dim StrQuery As String = Nothing
        Dim WhereClause As String = Nothing
        Dim dt As New DataTable
        Dim params As New EntitySpaces.Interfaces.esParameters
        WhereClause = " Where "
        StrQuery += "select * from IC_BankAccounts"
        WhereClause += " where AccountType=@Accountype"
        params.Add("Accountype", Accountype)

        StrQuery += WhereClause

        Dim util As New esUtility
        dt = util.FillDataTable(EntitySpaces.DynamicQuery.esQueryType.Text, StrQuery, params)
        If Not pagenumber = 0 Then
            rg.DataSource = dt
            If DoDataBind Then
                rg.DataBind()
            End If
        Else

            rg.DataSource = dt
            If DoDataBind Then
                rg.DataBind()
            End If
        End If
    End Function

    Public Shared Function getallactiveclearingaccount() As IC.ICBankAccountsCollection

        Dim icGlobalColl As New IC.ICBankAccountsCollection
        icGlobalColl.es.Connection.CommandTimeout = 3600
        icGlobalColl.Query.Where(icGlobalColl.Query.IsActive = True)
        icGlobalColl.Query.OrderBy(icGlobalColl.Query.AccountTitle.Ascending)
        icGlobalColl.Query.Load()
        Return icGlobalColl
    End Function


    Public Shared Function GetallAccounttypeWithChargesTypeAccount() As DataTable

        Dim qryObjICBankAccount As New IC.ICBankAccountsQuery("qryObjICBankAccount")
        Dim dt As New DataTable
        'qryObjICBankAccount.Select(qryObjICBankAccount.CBAccountType.Case.When(qryObjICBankAccount.CBAccountType = "RB").Then(qryObjICBankAccount.AccountNumber + "-" + qryObjICBankAccount.BranchCode + "-" + qryObjICBankAccount.Currency + "-" + qryObjICBankAccount.CBAccountType).Else(qryObjICBankAccount.AccountNumber.ToString + "-" + qryObjICBankAccount.BranchCode.ToString + "-" + qryObjICBankAccount.Currency + "-" + qryObjICBankAccount.CBAccountType + "-" + qryObjICBankAccount.SeqNo.ToString + "-" + qryObjICBankAccount.ProfitCentre + "-" + qryObjICBankAccount.ClientNo.ToString).End().As("AccountDeta"))
        qryObjICBankAccount.Select((qryObjICBankAccount.PrincipalBankAccountID).As("PrincipalBankAccountID"))

        qryObjICBankAccount.Select((qryObjICBankAccount.AccountNumber + "-" + qryObjICBankAccount.AccountTitle).As("AccountTitleAndNumber"))

        'qryObjICBankAccount.Select((qryObjICBankAccount.AccountNumber + "-" + qryObjICBankAccount.BranchCode + "-" + qryObjICBankAccount.Currency + "-" + qryObjICBankAccount.CBAccountType).As("AccountDeta"))
        qryObjICBankAccount.Where(qryObjICBankAccount.IsActive = True And qryObjICBankAccount.AccountType = "Global Charges Account")
        dt = qryObjICBankAccount.LoadDataTable
        Return dt

    End Function

    Public Shared Function GetGlobalChargesForPackages() As IC.ICGlobalChargesCollection




        Dim gccoll As New IC.ICGlobalChargesCollection


        gccoll.Query.Where(gccoll.Query.Active = True)

        gccoll.Query.OrderBy(gccoll.Query.Title, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)

        If gccoll.Query.Load Then


            Return gccoll

        Else


            Return Nothing



        End If




    End Function

End Class
