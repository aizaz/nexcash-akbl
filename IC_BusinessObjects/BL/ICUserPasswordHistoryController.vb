﻿Imports System.Security.Cryptography
Imports System.Text

Namespace IC
    Public Class ICUserPasswordHistoryController
        Public Shared Sub AddICUserPasswordHistory(ByVal ICUserPasswordHistory As ICUserPasswordHistory)
            Dim objICUserPasswordHistory As New ICUserPasswordHistory
            objICUserPasswordHistory.es.Connection.CommandTimeout = 3600

            objICUserPasswordHistory.UserID = ICUserPasswordHistory.UserID
            objICUserPasswordHistory.Password = ICUserPasswordHistory.Password
            objICUserPasswordHistory.CreatedDate = ICUserPasswordHistory.CreatedDate
            objICUserPasswordHistory.CreatedBy = ICUserPasswordHistory.CreatedBy

            objICUserPasswordHistory.Save()

            ' Delete More than 2 previous passwords
            DeleteMoreThan2UserPasswordHistory(ICUserPasswordHistory.UserID)
        End Sub
        Public Shared Sub DeleteMoreThan2UserPasswordHistory(ByVal UserID As String)
            Dim dtPasswordHistory As New DataTable
            Dim dr As DataRow
            Dim i As Integer = 0
            Dim objUserPWDHistory As New ICUserPasswordHistory
            Dim qryUserPWDHistory As New ICUserPasswordHistoryQuery("qryUPH")
            qryUserPWDHistory.es2.Connection.CommandTimeout = 3600

            qryUserPWDHistory.Select(qryUserPWDHistory.Id)
            qryUserPWDHistory.Where(qryUserPWDHistory.UserID = UserID)
            qryUserPWDHistory.OrderBy(qryUserPWDHistory.Id.Descending)

            dtPasswordHistory = qryUserPWDHistory.LoadDataTable()

            If dtPasswordHistory.Rows.Count > 2 Then
                i = 0
                For Each dr In dtPasswordHistory.Rows
                    If i > 1 Then
                        objUserPWDHistory = New ICUserPasswordHistory
                        If objUserPWDHistory.LoadByPrimaryKey(dr("Id").ToString()) Then
                            objUserPWDHistory.MarkAsDeleted()
                            objUserPWDHistory.Save()
                        End If
                        i = i + 1
                    Else
                        i = i + 1
                    End If
                Next
            End If
        End Sub
        Public Shared Function CheckUserPasswordHistory(ByVal UserID As String, ByVal Password As String) As Boolean
            Dim collICUserPasswordHistory As New ICUserPasswordHistoryCollection
            collICUserPasswordHistory.es.Connection.CommandTimeout = 3600
            collICUserPasswordHistory.Query.Where(collICUserPasswordHistory.Query.UserID = UserID And collICUserPasswordHistory.Query.Password = Password)
            If collICUserPasswordHistory.Query.Load() Then
                Return False
            Else
                Return True
            End If
        End Function
        Public Shared Function GenerateMD5Hash(ByVal SourceText As String) As String
            'Create an encoding object to ensure the encoding standard for the source text
            'Retrieve a byte array based on the source text
            Dim ByteSourceText() As Byte = Encoding.Default.GetBytes(SourceText)
            'Instantiate an MD5 Provider object
            Dim Md5 As MD5 = Md5.Create
            'Compute the hash value from the source
            Dim ByteHash() As Byte = Md5.ComputeHash(ByteSourceText)
            Dim sbuilder As New StringBuilder()
            Dim i As Integer
            For i = 0 To ByteHash.Length - 1
                '  sbuilder.Append(ByteHash(i).ToString("x2"))
                sbuilder.Append(ByteHash(i).ToString("g"))
            Next
            Return sbuilder.ToString()
        End Function
        '' Random generate Password
        Public Shared Function Generate(minLength As Integer, _
                                  maxLength As Integer) _
      As String

            Dim PASSWORD_CHARS_LCASE As String = "abcdefgijkmnopqrstwxyz"
            Dim PASSWORD_CHARS_UCASE As String = "ABCDEFGHJKLMNPQRSTWXYZ"
            Dim PASSWORD_CHARS_NUMERIC As String = "23456789"
            Dim PASSWORD_CHARS_SPECIAL As String = "-_!@#$"

            ' Make sure that input parameters are valid.
            If (minLength <= 0 Or maxLength <= 0 Or minLength > maxLength) Then
                Generate = Nothing
            End If

            ' Create a local array containing supported password characters
            ' grouped by types. You can remove character groups from this
            ' array, but doing so will weaken the password strength.
            Dim charGroups As Char()() = New Char()() _
            { _
                PASSWORD_CHARS_LCASE.ToCharArray(), _
                PASSWORD_CHARS_UCASE.ToCharArray(), _
                PASSWORD_CHARS_NUMERIC.ToCharArray(), _
                PASSWORD_CHARS_SPECIAL.ToCharArray() _
            }

            ' Use this array to track the number of unused characters in each
            ' character group.
            Dim charsLeftInGroup As Integer() = New Integer(charGroups.Length - 1) {}

            ' Initially, all characters in each group are not used.
            Dim I As Integer
            For I = 0 To charsLeftInGroup.Length - 1
                charsLeftInGroup(I) = charGroups(I).Length
            Next

            ' Use this array to track (iterate through) unused character groups.
            Dim leftGroupsOrder As Integer() = New Integer(charGroups.Length - 1) {}

            ' Initially, all character groups are not used.
            For I = 0 To leftGroupsOrder.Length - 1
                leftGroupsOrder(I) = I
            Next

            ' Because we cannot use the default randomizer, which is based on the
            ' current time (it will produce the same "random" number within a
            ' second), we will use a random number generator to seed the
            ' randomizer.

            ' Use a 4-byte array to fill it with random bytes and convert it then
            ' to an integer value.
            Dim randomBytes As Byte() = New Byte(3) {}

            ' Generate 4 random bytes.
            Dim rng As RNGCryptoServiceProvider = New RNGCryptoServiceProvider()

            rng.GetBytes(randomBytes)

            ' Convert 4 bytes into a 32-bit integer value.
            Dim seed As Integer = BitConverter.ToInt32(randomBytes, 0)

            ' Now, this is real randomization.
            Dim random As Random = New Random(seed)

            ' This array will hold password characters.
            Dim password As Char() = Nothing

            ' Allocate appropriate memory for the password.
            If (minLength < maxLength) Then
                password = New Char(random.Next(minLength - 1, maxLength)) {}
            Else
                password = New Char(minLength - 1) {}
            End If

            ' Index of the next character to be added to password.
            Dim nextCharIdx As Integer

            ' Index of the next character group to be processed.
            Dim nextGroupIdx As Integer

            ' Index which will be used to track not processed character groups.
            Dim nextLeftGroupsOrderIdx As Integer

            ' Index of the last non-processed character in a group.
            Dim lastCharIdx As Integer

            ' Index of the last non-processed group.
            Dim lastLeftGroupsOrderIdx As Integer = leftGroupsOrder.Length - 1

            ' Generate password characters one at a time.
            For I = 0 To password.Length - 1

                ' If only one character group remained unprocessed, process it;
                ' otherwise, pick a random character group from the unprocessed
                ' group list. To allow a special character to appear in the
                ' first position, increment the second parameter of the Next
                ' function call by one, i.e. lastLeftGroupsOrderIdx + 1.
                If (lastLeftGroupsOrderIdx = 0) Then
                    nextLeftGroupsOrderIdx = 0
                Else
                    nextLeftGroupsOrderIdx = random.Next(0, lastLeftGroupsOrderIdx)
                End If

                ' Get the actual index of the character group, from which we will
                ' pick the next character.
                nextGroupIdx = leftGroupsOrder(nextLeftGroupsOrderIdx)

                ' Get the index of the last unprocessed characters in this group.
                lastCharIdx = charsLeftInGroup(nextGroupIdx) - 1

                ' If only one unprocessed character is left, pick it; otherwise,
                ' get a random character from the unused character list.
                If (lastCharIdx = 0) Then
                    nextCharIdx = 0
                Else
                    nextCharIdx = random.Next(0, lastCharIdx + 1)
                End If

                ' Add this character to the password.
                password(I) = charGroups(nextGroupIdx)(nextCharIdx)

                ' If we processed the last character in this group, start over.
                If (lastCharIdx = 0) Then
                    charsLeftInGroup(nextGroupIdx) = _
                                    charGroups(nextGroupIdx).Length
                    ' There are more unprocessed characters left.
                Else
                    ' Swap processed character with the last unprocessed character
                    ' so that we don't pick it until we process all characters in
                    ' this group.
                    If (lastCharIdx <> nextCharIdx) Then
                        Dim temp As Char = charGroups(nextGroupIdx)(lastCharIdx)
                        charGroups(nextGroupIdx)(lastCharIdx) = _
                                    charGroups(nextGroupIdx)(nextCharIdx)
                        charGroups(nextGroupIdx)(nextCharIdx) = temp
                    End If

                    ' Decrement the number of unprocessed characters in
                    ' this group.
                    charsLeftInGroup(nextGroupIdx) = _
                               charsLeftInGroup(nextGroupIdx) - 1
                End If

                ' If we processed the last group, start all over.
                If (lastLeftGroupsOrderIdx = 0) Then
                    lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1
                    ' There are more unprocessed groups left.
                Else
                    ' Swap processed group with the last unprocessed group
                    ' so that we don't pick it until we process all groups.
                    If (lastLeftGroupsOrderIdx <> nextLeftGroupsOrderIdx) Then
                        Dim temp As Integer = _
                                    leftGroupsOrder(lastLeftGroupsOrderIdx)
                        leftGroupsOrder(lastLeftGroupsOrderIdx) = _
                                    leftGroupsOrder(nextLeftGroupsOrderIdx)
                        leftGroupsOrder(nextLeftGroupsOrderIdx) = temp
                    End If

                    ' Decrement the number of unprocessed groups.
                    lastLeftGroupsOrderIdx = lastLeftGroupsOrderIdx - 1
                End If
            Next

            ' Convert password characters into a string and return the result.
            Generate = New String(password)
        End Function
    End Class
End Namespace

