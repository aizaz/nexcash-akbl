﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC
    Public Class ICBeneficiaryGroupManagementController

        Public Shared Sub AddBeneficiaryGroup(ByVal objBeneGroup As ICBeneGroup, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String)
            Dim objBeneficiaryGroup As New ICBeneGroup
            Dim prevobjBeneficiaryGroup As New ICBeneGroup
            Dim CurrentAt As String
            Dim PrevAt As String

            If (isUpdate = False) Then
                objBeneficiaryGroup.CreateBy = objBeneGroup.CreateBy
                objBeneficiaryGroup.CreateDate = objBeneGroup.CreateDate
                objBeneficiaryGroup.Creater = objBeneGroup.Creater
                objBeneficiaryGroup.CreationDate = objBeneGroup.CreationDate
            Else
                objBeneficiaryGroup.LoadByPrimaryKey(objBeneGroup.BeneGroupCode)
                prevobjBeneficiaryGroup.LoadByPrimaryKey(objBeneGroup.BeneGroupCode)
                objBeneficiaryGroup.CreateBy = objBeneGroup.CreateBy
                objBeneficiaryGroup.CreateDate = objBeneGroup.CreateDate
            End If
            objBeneficiaryGroup.BeneGroupName = objBeneGroup.BeneGroupName
            objBeneficiaryGroup.BeneTransactionLimit = objBeneGroup.BeneTransactionLimit
            objBeneficiaryGroup.PaymentNatureCode = objBeneGroup.PaymentNatureCode
            objBeneficiaryGroup.BeneGroupCode = objBeneGroup.BeneGroupCode
            objBeneficiaryGroup.CreateBy = objBeneGroup.CreateBy
            objBeneficiaryGroup.CreateDate = objBeneGroup.CreateDate
            objBeneficiaryGroup.IsActive = objBeneGroup.IsActive
            objBeneficiaryGroup.Creater = objBeneGroup.Creater
            objBeneficiaryGroup.CreationDate = objBeneGroup.CreationDate
            objBeneficiaryGroup.CompanyCode = objBeneGroup.CompanyCode

            objBeneficiaryGroup.Save()

            If (isUpdate = False) Then
                CurrentAt = "Beneficiary Group : [Name:  " & objBeneficiaryGroup.BeneGroupName.ToString() & " ; Code:  " & objBeneficiaryGroup.BeneGroupCode.ToString() & " ; IsActive:  " & objBeneficiaryGroup.IsActive & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Beneficiary Group Management", objBeneficiaryGroup.BeneGroupCode.ToString(), UserID.ToString(), UserName.ToString(), "ADD")
            Else
                CurrentAt = "Beneficiary Group : Current Values [Name:  " & objBeneficiaryGroup.BeneGroupName.ToString() & " ; Code:  " & objBeneficiaryGroup.BeneGroupCode.ToString() & " ; IsActive:  " & objBeneficiaryGroup.IsActive & "]"
                PrevAt = "<br />Beneficiary Group : Previous Values [Name:  " & prevobjBeneficiaryGroup.BeneGroupName.ToString() & " ; Code:  " & prevobjBeneficiaryGroup.BeneGroupCode.ToString() & " ; IsActive:  " & prevobjBeneficiaryGroup.IsActive & "] "
                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Beneficiary Group Management", objBeneficiaryGroup.BeneGroupCode.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")
            End If
        End Sub

        Public Shared Sub GetgvBeneGroup(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
            Dim collBeneGroup As New ICBeneGroupCollection
            If Not pagenumber = 0 Then
                collBeneGroup.Query.Select(collBeneGroup.Query.BeneGroupCode, collBeneGroup.Query.BeneGroupName, collBeneGroup.Query.BeneTransactionLimit, collBeneGroup.Query.IsActive)
                collBeneGroup.Query.OrderBy(collBeneGroup.Query.BeneGroupName.Ascending)
                collBeneGroup.Query.Load()
                rg.DataSource = collBeneGroup

                If DoDataBind Then
                    rg.DataBind()
                End If
            Else
                collBeneGroup.Query.es.PageNumber = 1
                collBeneGroup.Query.es.PageSize = pagesize
                collBeneGroup.Query.OrderBy(collBeneGroup.Query.BeneGroupName.Ascending)
                collBeneGroup.Query.Load()
                rg.DataSource = collBeneGroup

                If DoDataBind Then
                    rg.DataBind()
                End If

            End If
        End Sub

        Public Shared Sub DeleteBeneGroup(ByVal BeneGroupCode As String, ByVal UserID As String, ByVal UserName As String)
            Dim objBeneGroup As New ICBeneGroup
            Dim CurrentAt As String = ""

            If objBeneGroup.LoadByPrimaryKey(BeneGroupCode) Then
                CurrentAt = "Beneficiary Group : [Code:  " & objBeneGroup.BeneGroupCode.ToString() & " ; Name:  " & objBeneGroup.BeneGroupName.ToString() & " ; Transaction Limit:  " & objBeneGroup.BeneTransactionLimit.ToString() & "]"
                objBeneGroup.MarkAsDeleted()
                objBeneGroup.Save()
                ICUtilities.AddAuditTrail(CurrentAt & " Deleted ", "Beneficiary Group Management", BeneGroupCode.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")
            End If
        End Sub

        Public Shared Function GetAllActiveGroupsForBene() As DataTable
            Dim qryICGroup As New ICGroupQuery("qryICGroup")
            Dim qryICComapny As New ICCompanyQuery("qryICComapny")
            Dim dt As New DataTable
            qryICGroup.Select(qryICGroup.GroupCode, qryICGroup.GroupName)
            qryICGroup.InnerJoin(qryICComapny).On(qryICGroup.GroupCode = qryICComapny.GroupCode)
            qryICGroup.Where(qryICGroup.IsActive = True)
            qryICGroup.Where(qryICComapny.IsActive = True And qryICComapny.IsApprove = True And qryICComapny.IsBeneRequired = True)

            qryICGroup.OrderBy(qryICGroup.GroupName.Ascending)
            qryICGroup.es.Distinct = True
            dt = qryICGroup.LoadDataTable
            Return dt
        End Function

        Public Shared Function GetAllActiveCompaniesByGroupCodeForBene(ByVal GroupCode As String) As ICCompanyCollection
            Dim ICComapnyColl As New ICCompanyCollection
            ICComapnyColl.Query.Where(ICComapnyColl.Query.GroupCode = GroupCode And ICComapnyColl.Query.IsActive = True)
            ICComapnyColl.Query.Where(ICComapnyColl.Query.IsApprove = True And ICComapnyColl.Query.IsBeneRequired = True)
            ICComapnyColl.Query.OrderBy(ICComapnyColl.Query.CompanyName.Ascending)
            ICComapnyColl.Query.Load()
            Return ICComapnyColl
        End Function

        Public Shared Function GetAllActiveandApproveBeneGroupByPN(ByVal PaymentNatureCode As String) As ICBeneGroupCollection
            Dim collGroupBene As New ICBeneGroupCollection
            collGroupBene.Query.Where(collGroupBene.Query.PaymentNatureCode = PaymentNatureCode And collGroupBene.Query.IsActive = True)
            collGroupBene.Query.OrderBy(collGroupBene.Query.BeneGroupName.Ascending)
            collGroupBene.Query.Load()
            Return collGroupBene
        End Function

        Public Shared Function GetAllActiveandApproveBeneGroupByPNandCompanyCode(ByVal PaymentNatureCode As String, ByVal CompanyCode As String) As ICBeneGroupCollection
            Dim collGroupBene As New ICBeneGroupCollection
            collGroupBene.Query.Where(collGroupBene.Query.PaymentNatureCode = PaymentNatureCode And collGroupBene.Query.IsActive = True And collGroupBene.Query.CompanyCode = CompanyCode)
            collGroupBene.Query.OrderBy(collGroupBene.Query.BeneGroupName.Ascending)
            collGroupBene.Query.Load()
            Return collGroupBene
        End Function

        Public Shared Function GetBeneGroupCode(ByVal BeneGroupCode As String) As String
            Dim collICBeneGroup As New ICBeneGroupCollection
            collICBeneGroup.Query.Where(collICBeneGroup.Query.IsActive = True)
            collICBeneGroup.Query.Where(collICBeneGroup.Query.BeneGroupCode = BeneGroupCode)
            If collICBeneGroup.Query.Load() Then
                Return collICBeneGroup(0).BeneGroupCode.ToString()
            Else
                Return ""
            End If
        End Function

       Public Shared Function GetPaymentNatureByCompanyCode(ByVal CompanyCode As String) As DataTable
            Dim qryObjICAccountPNature As New ICAccountsPaymentNatureQuery("qryObjICAccountPNature")
            Dim qryObjICPaymentNature As New ICPaymentNatureQuery("qryObjICPaymentNature")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim dt As New DataTable

            qryObjICAccountPNature.es.Distinct = True
            qryObjICAccountPNature.Select((qryObjICPaymentNature.PaymentNatureName).As("PaymentNatureName"), (qryObjICAccountPNature.PaymentNatureCode).As("PaymentNatureCode"))
            qryObjICAccountPNature.InnerJoin(qryObjICAccounts).On(qryObjICAccountPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAccountPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAccountPNature.Currency = qryObjICAccounts.Currency)
            qryObjICAccountPNature.InnerJoin(qryObjICPaymentNature).On(qryObjICAccountPNature.PaymentNatureCode = qryObjICPaymentNature.PaymentNatureCode)
            qryObjICAccountPNature.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICAccountPNature.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
            qryObjICAccountPNature.Where(qryObjICPaymentNature.IsActive = True)
            qryObjICAccountPNature.Where(qryObjICCompany.IsActive = True And qryObjICCompany.IsApprove = True)
            qryObjICAccountPNature.Where(qryObjICAccounts.IsActive = True And qryObjICAccounts.IsApproved = True)
            qryObjICAccountPNature.Where(qryObjICAccountPNature.IsActive = True)
            dt = qryObjICAccountPNature.LoadDataTable
            Return dt

        End Function

        Public Shared Function GetCompanyByCompanyCodeForBene(ByVal CompanyCode As String) As String
            Dim ICComapnyColl As New ICCompanyCollection
            ICComapnyColl.Query.Where(ICComapnyColl.Query.CompanyCode = CompanyCode And ICComapnyColl.Query.IsActive = True)
            If ICComapnyColl.Query.Load() Then
                Return ICComapnyColl(0).CompanyName.ToString()
            Else
                Return ""
            End If
        End Function
        Public Shared Function GetAllActiveBeneGroup() As DataTable
            Dim collBeneGroup As New ICBeneGroupCollection
            Dim dt As New DataTable
            collBeneGroup.Query.Where(collBeneGroup.Query.IsActive = True)
            dt = collBeneGroup.Query.LoadDataTable()
            Return dt
        End Function
    End Class
End Namespace
