﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls

Namespace IC
    Public Class ICOfficeController
        Public Shared Function GetPrincipalBankBranchActiveAndApproveForAddUser() As DataTable

            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim dt As New DataTable

            qryObjICOffice.Select((qryObjICOffice.OfficeCode + "-" + qryObjICOffice.OfficeName).As("OfficeName"), qryObjICOffice.OfficeID, qryObjICOffice.OfficeCode)
            qryObjICOffice.Where(qryObjICOffice.OffceType = "Branch Office" And qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True And qryObjICBank.IsPrincipal = True)
            qryObjICOffice.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
            qryObjICOffice.OrderBy(qryObjICOffice.OfficeCode.Ascending, qryObjICOffice.OfficeName.Ascending)
            dt = qryObjICOffice.LoadDataTable


            Return dt
        End Function
        Public Shared Sub AddOffice(ByVal ICOFfice As ICOffice, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String, ByVal ActionStr As String)

            Dim objICOffce As New ICOffice
            Dim prevobjICOffce As New ICOffice

            Dim currentCity As New ICCity
            Dim previousCity As New ICCity

            Dim IsActiveText As String
            Dim IsApproveText As String
            Dim IsBranchText As String
            Dim IsPrintingLocationText As String
            'Dim CurrentAt As String
            'Dim PrevAt As String

            objICOffce.es.Connection.CommandTimeout = 3600
            prevobjICOffce.es.Connection.CommandTimeout = 3600

            If isUpdate = False Then

                objICOffce.CreatedBy = ICOFfice.CreatedBy
                objICOffce.CreateDate = ICOFfice.CreateDate
                objICOffce.Creater = ICOFfice.Creater
                objICOffce.CreationDate = ICOFfice.CreationDate
                currentCity.LoadByPrimaryKey(ICOFfice.CityID.ToString())
            Else
                objICOffce.LoadByPrimaryKey(ICOFfice.OfficeID.ToString())
                prevobjICOffce.LoadByPrimaryKey(ICOFfice.OfficeID.ToString())
                currentCity.LoadByPrimaryKey(objICOffce.CityID.ToString())
                previousCity.LoadByPrimaryKey(prevobjICOffce.CityID.ToString())
                objICOffce.CreatedBy = ICOFfice.CreatedBy
                objICOffce.CreateDate = ICOFfice.CreateDate
            End If

            objICOffce.CityID = ICOFfice.CityID.Value.ToString()
            If Not ICOFfice.OfficeCode Is Nothing Then
                objICOffce.OfficeCode = ICOFfice.OfficeCode.ToString()
            Else
                objICOffce.OfficeCode = Nothing
            End If
            If Not ICOFfice.BankCode Is Nothing Then
                objICOffce.BankCode = ICOFfice.BankCode
            Else
                objICOffce.BankCode = Nothing
            End If
            If Not ICOFfice.CompanyCode Is Nothing Then
                objICOffce.CompanyCode = ICOFfice.CompanyCode
            Else
                objICOffce.CompanyCode = Nothing
            End If
            objICOffce.Email = ICOFfice.Email.ToString()
            objICOffce.OfficeName = ICOFfice.OfficeName.ToString()
            objICOffce.Phone1 = ICOFfice.Phone1.ToString()
            objICOffce.Address = ICOFfice.Address.ToString()
            objICOffce.Description = ICOFfice.Description.ToString()
            objICOffce.OffceType = ICOFfice.OffceType.ToString()
            objICOffce.Phone2 = ICOFfice.Phone2.ToString()


            objICOffce.IsActive = ICOFfice.IsActive
            objICOffce.IsBranch = ICOFfice.IsBranch
            objICOffce.IsPrintingLocation = ICOFfice.IsPrintingLocation
            objICOffce.IsAllowedForTagging = ICOFfice.IsAllowedForTagging
            objICOffce.IsApprove = False

            If ICOFfice.OffceType = "Branch Office" Then

                If ICOFfice.ClearingNormalAccountNumber = "0" And ICOFfice.ClearingNormalAccountBranchCode = "0" And ICOFfice.ClearingNormalAccountCurrency = "0" Then
                    objICOffce.ClearingNormalAccountBranchCode = Nothing
                    objICOffce.ClearingNormalAccountCurrency = Nothing
                    objICOffce.ClearingNormalAccountNumber = Nothing
                    objICOffce.ClearingNormalAccountType = Nothing
                    objICOffce.ClearingNormalSeqNo = Nothing
                    objICOffce.ClearingNormalClientNo = Nothing
                    objICOffce.ClearingNormalProfitCenter = Nothing
                ElseIf ICOFfice.ClearingNormalAccountNumber = "" And ICOFfice.ClearingNormalAccountBranchCode = "" And ICOFfice.ClearingNormalAccountCurrency = "" Then
                    objICOffce.ClearingNormalAccountBranchCode = Nothing
                    objICOffce.ClearingNormalAccountCurrency = Nothing
                    objICOffce.ClearingNormalAccountNumber = Nothing
                    objICOffce.ClearingNormalAccountType = Nothing
                    objICOffce.ClearingNormalSeqNo = Nothing
                    objICOffce.ClearingNormalClientNo = Nothing
                    objICOffce.ClearingNormalProfitCenter = Nothing
                Else
                    objICOffce.ClearingNormalAccountNumber = ICOFfice.ClearingNormalAccountNumber
                    objICOffce.ClearingNormalAccountBranchCode = ICOFfice.ClearingNormalAccountBranchCode
                    objICOffce.ClearingNormalAccountCurrency = ICOFfice.ClearingNormalAccountCurrency
                    objICOffce.ClearingNormalAccountType = ICOFfice.ClearingNormalAccountType
                    objICOffce.ClearingNormalSeqNo = ICOFfice.ClearingNormalSeqNo
                    objICOffce.ClearingNormalClientNo = ICOFfice.ClearingNormalClientNo
                    objICOffce.ClearingNormalProfitCenter = ICOFfice.ClearingNormalProfitCenter
                End If

                If ICOFfice.InterCityAccountNumber = "0" And ICOFfice.InterCityAccountBranchCode = "0" And ICOFfice.InterCityAccountCurrency = "0" Then
                    objICOffce.InterCityAccountBranchCode = Nothing
                    objICOffce.InterCityAccountCurrency = Nothing
                    objICOffce.InterCityAccountNumber = Nothing
                    objICOffce.InterCityAccountType = Nothing
                    objICOffce.InterCityAccountSeqNo = Nothing
                    objICOffce.InterCityAccountClientNo = Nothing
                    objICOffce.InterCityAccountProfitCentre = Nothing
                ElseIf ICOFfice.InterCityAccountNumber = "" And ICOFfice.InterCityAccountBranchCode = "" And ICOFfice.InterCityAccountCurrency = "" Then
                    objICOffce.InterCityAccountBranchCode = Nothing
                    objICOffce.InterCityAccountCurrency = Nothing
                    objICOffce.InterCityAccountNumber = Nothing
                    objICOffce.InterCityAccountType = Nothing
                    objICOffce.InterCityAccountSeqNo = Nothing
                    objICOffce.InterCityAccountClientNo = Nothing
                    objICOffce.InterCityAccountProfitCentre = Nothing
                Else
                    objICOffce.InterCityAccountBranchCode = ICOFfice.InterCityAccountBranchCode
                    objICOffce.InterCityAccountCurrency = ICOFfice.InterCityAccountCurrency
                    objICOffce.InterCityAccountNumber = ICOFfice.InterCityAccountNumber
                    objICOffce.InterCityAccountType = ICOFfice.InterCityAccountType
                    objICOffce.InterCityAccountSeqNo = ICOFfice.InterCityAccountSeqNo
                    objICOffce.InterCityAccountClientNo = ICOFfice.InterCityAccountClientNo
                    objICOffce.InterCityAccountProfitCentre = ICOFfice.InterCityAccountProfitCentre
                End If

                If ICOFfice.SameDayAccountNumber = "0" And ICOFfice.SameDayAccountBranchCode = "0" And ICOFfice.SameDayAccountCurrency = "0" Then
                    objICOffce.SameDayAccountBranchCode = Nothing
                    objICOffce.SameDayAccountCurrency = Nothing
                    objICOffce.SameDayAccountNumber = Nothing
                    objICOffce.SameDayAccountType = Nothing
                    objICOffce.SameDayAccountSeqNo = Nothing
                    objICOffce.SameDayAccountClientNo = Nothing
                    objICOffce.SameDayAccountProfitCentre = Nothing
                ElseIf ICOFfice.SameDayAccountNumber = "" And ICOFfice.SameDayAccountBranchCode = "" And ICOFfice.SameDayAccountCurrency = "" Then
                    objICOffce.SameDayAccountBranchCode = Nothing
                    objICOffce.SameDayAccountCurrency = Nothing
                    objICOffce.SameDayAccountNumber = Nothing
                    objICOffce.SameDayAccountType = Nothing
                    objICOffce.SameDayAccountSeqNo = Nothing
                    objICOffce.SameDayAccountClientNo = Nothing
                    objICOffce.SameDayAccountProfitCentre = Nothing
                Else
                    objICOffce.SameDayAccountBranchCode = ICOFfice.SameDayAccountBranchCode
                    objICOffce.SameDayAccountCurrency = ICOFfice.SameDayAccountCurrency
                    objICOffce.SameDayAccountNumber = ICOFfice.SameDayAccountNumber
                    objICOffce.SameDayAccountType = ICOFfice.SameDayAccountType
                    objICOffce.SameDayAccountSeqNo = ICOFfice.SameDayAccountSeqNo
                    objICOffce.SameDayAccountClientNo = ICOFfice.SameDayAccountClientNo
                    objICOffce.SameDayAccountProfitCentre = ICOFfice.SameDayAccountProfitCentre
                End If

                If ICOFfice.BranchCashTillAccountNumber = "0" And ICOFfice.BranchCashTillAccountBranchCode = "0" And ICOFfice.BranchCashTillAccountCurrency = "0" Then
                    objICOffce.BranchCashTillAccountNumber = Nothing
                    objICOffce.BranchCashTillAccountBranchCode = Nothing
                    objICOffce.BranchCashTillAccountCurrency = Nothing
                    objICOffce.BranchCashTillAccountType = Nothing
                    objICOffce.BranchCashTillAccountSeqNo = Nothing
                    objICOffce.BranchCashTillAccountClientNo = Nothing
                    objICOffce.BranchCashTillAccountProfitCentre = Nothing
                ElseIf ICOFfice.BranchCashTillAccountNumber = "" And ICOFfice.BranchCashTillAccountBranchCode = "" And ICOFfice.BranchCashTillAccountCurrency = "" Then
                    objICOffce.BranchCashTillAccountNumber = Nothing
                    objICOffce.BranchCashTillAccountBranchCode = Nothing
                    objICOffce.BranchCashTillAccountCurrency = Nothing
                    objICOffce.BranchCashTillAccountType = Nothing
                    objICOffce.BranchCashTillAccountSeqNo = Nothing
                    objICOffce.BranchCashTillAccountClientNo = Nothing
                    objICOffce.BranchCashTillAccountProfitCentre = Nothing
                Else
                    objICOffce.BranchCashTillAccountNumber = ICOFfice.BranchCashTillAccountNumber
                    objICOffce.BranchCashTillAccountBranchCode = ICOFfice.BranchCashTillAccountBranchCode
                    objICOffce.BranchCashTillAccountCurrency = ICOFfice.BranchCashTillAccountCurrency
                    objICOffce.BranchCashTillAccountType = ICOFfice.BranchCashTillAccountType
                    objICOffce.BranchCashTillAccountSeqNo = ICOFfice.BranchCashTillAccountSeqNo
                    objICOffce.BranchCashTillAccountClientNo = ICOFfice.BranchCashTillAccountClientNo
                    objICOffce.BranchCashTillAccountProfitCentre = ICOFfice.BranchCashTillAccountProfitCentre
                End If



            End If


            If objICOffce.OffceType = "Company Office" Then
                objICOffce.IsApprove = True
            End If

            If ICOFfice.IsActive = True Then
                IsActiveText = True
            Else
                IsActiveText = False
            End If

            If ICOFfice.IsApprove = True Then
                IsApproveText = True
            Else
                IsApproveText = False
            End If

            If ICOFfice.IsBranch = True Then
                IsBranchText = True
            Else
                IsBranchText = False
            End If

            If ICOFfice.IsPrintingLocation = True Then
                IsPrintingLocationText = True
            Else
                IsPrintingLocationText = False
            End If


            objICOffce.Save()

            If objICOffce.OffceType = "Branch Office" Then
                If (isUpdate = False) Then
                    '   CurrentAt = "Bank Branch : [Code:  " & objICOffce.OfficeCode.ToString() & " ; Name:  " & objICOffce.OfficeName.ToString() & " ; Phone1:  " & objICOffce.Phone1.ToString() & " ; Phone2:  " & objICOffce.Phone2.ToString() & " ; Address:  " & objICOffce.Address.ToString() & " ; Email:  " & objICOffce.Email.ToString() & " ; Description:  " & objICOffce.Description.ToString() & " ; City:  " & currentCity.CityName.ToString() & " ; IsActive:  " & objICOffce.IsActive.ToString() & " ; IsApproved:  " & objICOffce.IsApprove.ToString() & " ; IsBranch:  " & objICOffce.IsBranch.ToString() & " ; Is Printing Location:  " & objICOffce.IsPrintingLocation.ToString() & " ; ClearingNormalAccountNumber: " & objICOffce.ClearingNormalAccountNumber & " ; ClearingNormalAccountBranchCode: " & objICOffce.ClearingNormalAccountBranchCode & " ; ClearingNormalAccountCurrency: " & objICOffce.ClearingNormalAccountCurrency & " ; InterCityAccountNumber: " & objICOffce.InterCityAccountNumber & " ; InterCityAccountBranchCode: " & objICOffce.InterCityAccountBranchCode & " ; InterCityAccountCurrency: " & objICOffce.InterCityAccountCurrency & " ; SameDayAccountNumber: " & objICOffce.SameDayAccountNumber & " ; SameDayAccountBranchCode: " & objICOffce.SameDayAccountBranchCode & " ; SameDayAccountCurrency: " & objICOffce.SameDayAccountCurrency & "]"
                    ICUtilities.AddAuditTrail(ActionStr & " Added ", "Bank Branch", objICOffce.OfficeID.ToString(), UserID.ToString(), UserName.ToString(), "ADD")

                Else

                    'CurrentAt = "Bank Branch : Current Values [Code:  " & objICOffce.OfficeCode.ToString() & " ; Name:  " & objICOffce.OfficeName.ToString() & " ; Phone1:  " & objICOffce.Phone1.ToString() & " ; Phone2:  " & objICOffce.Phone2.ToString() & " ; Address:  " & objICOffce.Address.ToString() & " ; Email:  " & objICOffce.Email.ToString() & " ; Description:  " & objICOffce.Description.ToString() & " ; City:  " & currentCity.CityName.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApproveText.ToString() & " ; IsBranch:  " & IsBranchText.ToString() & " ; Is Printing Location:  " & IsPrintingLocationText.ToString() & " ; ClearingNormalAccountNumber: " & objICOffce.ClearingNormalAccountNumber & " ; ClearingNormalAccountBranchCode: " & objICOffce.ClearingNormalAccountBranchCode & " ; ClearingNormalAccountCurrency: " & objICOffce.ClearingNormalAccountCurrency & " ; InterCityAccountNumber: " & objICOffce.InterCityAccountNumber & " ; InterCityAccountBranchCode: " & objICOffce.InterCityAccountBranchCode & " ; InterCityAccountCurrency: " & objICOffce.InterCityAccountCurrency & " ; SameDayAccountNumber: " & objICOffce.SameDayAccountNumber & " ; SameDayAccountBranchCode: " & objICOffce.SameDayAccountBranchCode & " ; SameDayAccountCurrency: " & objICOffce.SameDayAccountCurrency & "]"
                    'PrevAt = "<br />Bank Branch : Previous Values [Code: " & prevobjICOffce.OfficeCode.ToString() & " ; Name: " & prevobjICOffce.OfficeName.ToString() & " ; Phone1:  " & prevobjICOffce.Phone1.ToString() & " ; Phone2:  " & prevobjICOffce.Phone2.ToString() & " ; Address:  " & prevobjICOffce.Address.ToString() & " ; Email:  " & prevobjICOffce.Email.ToString() & " ; Description:  " & prevobjICOffce.Description.ToString() & " ; City:  " & previousCity.CityName.ToString() & " ; IsActive:  " & prevobjICOffce.IsActive.ToString() & " ; IsApproved:  " & prevobjICOffce.IsApprove.ToString() & " ; IsBranch:  " & prevobjICOffce.IsBranch.ToString() & " ; Is Printing Location:  " & prevobjICOffce.IsPrintingLocation.ToString() & " ; ClearingNormalAccountNumber: " & prevobjICOffce.ClearingNormalAccountNumber & " ; ClearingNormalAccountBranchCode: " & prevobjICOffce.ClearingNormalAccountBranchCode & " ; ClearingNormalAccountCurrency: " & prevobjICOffce.ClearingNormalAccountCurrency & " ; InterCityAccountNumber: " & prevobjICOffce.InterCityAccountNumber & " ; InterCityAccountBranchCode: " & prevobjICOffce.InterCityAccountBranchCode & " ; InterCityAccountCurrency: " & prevobjICOffce.InterCityAccountCurrency & " ; SameDayAccountNumber: " & prevobjICOffce.SameDayAccountNumber & " ; SameDayAccountBranchCode: " & prevobjICOffce.SameDayAccountBranchCode & " ; SameDayAccountCurrency: " & prevobjICOffce.SameDayAccountCurrency & "] "
                    ICUtilities.AddAuditTrail(ActionStr & " Updated ", "Bank Branch", objICOffce.OfficeID.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

                End If


            ElseIf objICOffce.OffceType = "Company Office" Then
                If (isUpdate = False) Then
                    ' ICUtilities.AddAuditTrail("Company Office" & objICOffce.OfficeName & " Added ", "Company Office", objICOffce.OfficeID.ToString(), UserID.ToString(), UserName.ToString())
                    ICUtilities.AddAuditTrail("Company Office [ " & objICOffce.OfficeName & " ], with Office Code [ " & objICOffce.OfficeCode & " ] , Phone No 1 [ " & objICOffce.Phone1 & " ], Phone 2 [ " & objICOffce.Phone2 & " ], Email [ " & objICOffce.Email & " ], Address [ " & objICOffce.Address & " ], City [ " & objICOffce.UpToICCityByCityID.CityName & " ], Province [ " & objICOffce.UpToICCityByCityID.UpToICProvinceByProvinceCode.ProvinceName & " ], Country [ " & objICOffce.UpToICCityByCityID.UpToICProvinceByProvinceCode.UpToICCountryByCountryCode.CountryName & " ], printing location [ " & objICOffce.IsPrintingLocation & " ] , Description [ " & objICOffce.Description & " ], Is Active [ " & objICOffce.IsActive & " ] Added ", "Company Office", objICOffce.OfficeID.ToString(), UserID.ToString(), UserName.ToString(), "ADD")

                Else
                    ICUtilities.AddAuditTrail(ActionStr, "Company Office", objICOffce.OfficeID.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

                End If



            End If

        End Sub
        'Public Shared Sub AddOffice(ByVal ICOFfice As ICOffice, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String, ByVal ActionStr As String)

        '    Dim objICOffce As New ICOffice
        '    Dim prevobjICOffce As New ICOffice

        '    Dim currentCity As New ICCity
        '    Dim previousCity As New ICCity

        '    Dim IsActiveText As String
        '    Dim IsApproveText As String
        '    Dim IsBranchText As String
        '    Dim IsPrintingLocationText As String
        '    'Dim CurrentAt As String
        '    'Dim PrevAt As String

        '    objICOffce.es.Connection.CommandTimeout = 3600
        '    prevobjICOffce.es.Connection.CommandTimeout = 3600

        '    If isUpdate = False Then

        '        objICOffce.CreatedBy = ICOFfice.CreatedBy
        '        objICOffce.CreateDate = ICOFfice.CreateDate
        '        objICOffce.Creater = ICOFfice.Creater
        '        objICOffce.CreationDate = ICOFfice.CreationDate
        '        currentCity.LoadByPrimaryKey(ICOFfice.CityID.ToString())
        '    Else
        '        objICOffce.LoadByPrimaryKey(ICOFfice.OfficeID.ToString())
        '        prevobjICOffce.LoadByPrimaryKey(ICOFfice.OfficeID.ToString())
        '        currentCity.LoadByPrimaryKey(objICOffce.CityID.ToString())
        '        previousCity.LoadByPrimaryKey(prevobjICOffce.CityID.ToString())
        '        objICOffce.CreatedBy = ICOFfice.CreatedBy
        '        objICOffce.CreateDate = ICOFfice.CreateDate
        '    End If

        '    objICOffce.CityID = ICOFfice.CityID.Value.ToString()
        '    If Not ICOFfice.OfficeCode Is Nothing Then
        '        objICOffce.OfficeCode = ICOFfice.OfficeCode.ToString()
        '    Else
        '        objICOffce.OfficeCode = Nothing
        '    End If
        '    If Not ICOFfice.BankCode Is Nothing Then
        '        objICOffce.BankCode = ICOFfice.BankCode
        '    Else
        '        objICOffce.BankCode = Nothing
        '    End If
        '    If Not ICOFfice.CompanyCode Is Nothing Then
        '        objICOffce.CompanyCode = ICOFfice.CompanyCode
        '    Else
        '        objICOffce.CompanyCode = Nothing
        '    End If
        '    objICOffce.Email = ICOFfice.Email.ToString()
        '    objICOffce.OfficeName = ICOFfice.OfficeName.ToString()
        '    objICOffce.Phone1 = ICOFfice.Phone1.ToString()
        '    objICOffce.Address = ICOFfice.Address.ToString()
        '    objICOffce.Description = ICOFfice.Description.ToString()
        '    objICOffce.OffceType = ICOFfice.OffceType.ToString()
        '    objICOffce.Phone2 = ICOFfice.Phone2.ToString()


        '    objICOffce.IsActive = ICOFfice.IsActive
        '    objICOffce.IsBranch = ICOFfice.IsBranch
        '    objICOffce.IsPrintingLocation = ICOFfice.IsPrintingLocation
        '    objICOffce.IsApprove = False

        '    If ICOFfice.OffceType = "Branch Office" Then

        '        If ICOFfice.ClearingNormalAccountNumber = "0" And ICOFfice.ClearingNormalAccountBranchCode = "0" And ICOFfice.ClearingNormalAccountCurrency = "0" Then
        '            objICOffce.ClearingNormalAccountBranchCode = Nothing
        '            objICOffce.ClearingNormalAccountCurrency = Nothing
        '            objICOffce.ClearingNormalAccountNumber = Nothing
        '            objICOffce.ClearingNormalAccountType = Nothing
        '            objICOffce.ClearingNormalSeqNo = Nothing
        '            objICOffce.ClearingNormalClientNo = Nothing
        '            objICOffce.ClearingNormalProfitCenter = Nothing
        '        ElseIf ICOFfice.ClearingNormalAccountNumber = "" And ICOFfice.ClearingNormalAccountBranchCode = "" And ICOFfice.ClearingNormalAccountCurrency = "" Then
        '            objICOffce.ClearingNormalAccountBranchCode = Nothing
        '            objICOffce.ClearingNormalAccountCurrency = Nothing
        '            objICOffce.ClearingNormalAccountNumber = Nothing
        '            objICOffce.ClearingNormalAccountType = Nothing
        '            objICOffce.ClearingNormalSeqNo = Nothing
        '            objICOffce.ClearingNormalClientNo = Nothing
        '            objICOffce.ClearingNormalProfitCenter = Nothing
        '        Else
        '            objICOffce.ClearingNormalAccountNumber = ICOFfice.ClearingNormalAccountNumber
        '            objICOffce.ClearingNormalAccountBranchCode = ICOFfice.ClearingNormalAccountBranchCode
        '            objICOffce.ClearingNormalAccountCurrency = ICOFfice.ClearingNormalAccountCurrency
        '            objICOffce.ClearingNormalAccountType = ICOFfice.ClearingNormalAccountType
        '            objICOffce.ClearingNormalSeqNo = ICOFfice.ClearingNormalSeqNo
        '            objICOffce.ClearingNormalClientNo = ICOFfice.ClearingNormalClientNo
        '            objICOffce.ClearingNormalProfitCenter = ICOFfice.ClearingNormalProfitCenter
        '        End If

        '        If ICOFfice.InterCityAccountNumber = "0" And ICOFfice.InterCityAccountBranchCode = "0" And ICOFfice.InterCityAccountCurrency = "0" Then
        '            objICOffce.InterCityAccountBranchCode = Nothing
        '            objICOffce.InterCityAccountCurrency = Nothing
        '            objICOffce.InterCityAccountNumber = Nothing
        '            objICOffce.InterCityAccountType = Nothing
        '            objICOffce.InterCityAccountSeqNo = Nothing
        '            objICOffce.InterCityAccountClientNo = Nothing
        '            objICOffce.InterCityAccountProfitCentre = Nothing
        '        ElseIf ICOFfice.InterCityAccountNumber = "" And ICOFfice.InterCityAccountBranchCode = "" And ICOFfice.InterCityAccountCurrency = "" Then
        '            objICOffce.InterCityAccountBranchCode = Nothing
        '            objICOffce.InterCityAccountCurrency = Nothing
        '            objICOffce.InterCityAccountNumber = Nothing
        '            objICOffce.InterCityAccountType = Nothing
        '            objICOffce.InterCityAccountSeqNo = Nothing
        '            objICOffce.InterCityAccountClientNo = Nothing
        '            objICOffce.InterCityAccountProfitCentre = Nothing
        '        Else
        '            objICOffce.InterCityAccountBranchCode = ICOFfice.InterCityAccountBranchCode
        '            objICOffce.InterCityAccountCurrency = ICOFfice.InterCityAccountCurrency
        '            objICOffce.InterCityAccountNumber = ICOFfice.InterCityAccountNumber
        '            objICOffce.InterCityAccountType = ICOFfice.InterCityAccountType
        '            objICOffce.InterCityAccountSeqNo = ICOFfice.InterCityAccountSeqNo
        '            objICOffce.InterCityAccountClientNo = ICOFfice.InterCityAccountClientNo
        '            objICOffce.InterCityAccountProfitCentre = ICOFfice.InterCityAccountProfitCentre
        '        End If

        '        If ICOFfice.SameDayAccountNumber = "0" And ICOFfice.SameDayAccountBranchCode = "0" And ICOFfice.SameDayAccountCurrency = "0" Then
        '            objICOffce.SameDayAccountBranchCode = Nothing
        '            objICOffce.SameDayAccountCurrency = Nothing
        '            objICOffce.SameDayAccountNumber = Nothing
        '            objICOffce.SameDayAccountType = Nothing
        '            objICOffce.SameDayAccountSeqNo = Nothing
        '            objICOffce.SameDayAccountClientNo = Nothing
        '            objICOffce.SameDayAccountProfitCentre = Nothing
        '        ElseIf ICOFfice.SameDayAccountNumber = "" And ICOFfice.SameDayAccountBranchCode = "" And ICOFfice.SameDayAccountCurrency = "" Then
        '            objICOffce.SameDayAccountBranchCode = Nothing
        '            objICOffce.SameDayAccountCurrency = Nothing
        '            objICOffce.SameDayAccountNumber = Nothing
        '            objICOffce.SameDayAccountType = Nothing
        '            objICOffce.SameDayAccountSeqNo = Nothing
        '            objICOffce.SameDayAccountClientNo = Nothing
        '            objICOffce.SameDayAccountProfitCentre = Nothing
        '        Else
        '            objICOffce.SameDayAccountBranchCode = ICOFfice.SameDayAccountBranchCode
        '            objICOffce.SameDayAccountCurrency = ICOFfice.SameDayAccountCurrency
        '            objICOffce.SameDayAccountNumber = ICOFfice.SameDayAccountNumber
        '            objICOffce.SameDayAccountType = ICOFfice.SameDayAccountType
        '            objICOffce.SameDayAccountSeqNo = ICOFfice.SameDayAccountSeqNo
        '            objICOffce.SameDayAccountClientNo = ICOFfice.SameDayAccountClientNo
        '            objICOffce.SameDayAccountProfitCentre = ICOFfice.SameDayAccountProfitCentre
        '        End If

        '        If ICOFfice.BranchCashTillAccountNumber = "0" And ICOFfice.BranchCashTillAccountBranchCode = "0" And ICOFfice.BranchCashTillAccountCurrency = "0" Then
        '            objICOffce.BranchCashTillAccountNumber = Nothing
        '            objICOffce.BranchCashTillAccountBranchCode = Nothing
        '            objICOffce.BranchCashTillAccountCurrency = Nothing
        '            objICOffce.BranchCashTillAccountType = Nothing
        '            objICOffce.BranchCashTillAccountSeqNo = Nothing
        '            objICOffce.BranchCashTillAccountClientNo = Nothing
        '            objICOffce.BranchCashTillAccountProfitCentre = Nothing
        '        ElseIf ICOFfice.BranchCashTillAccountNumber = "" And ICOFfice.BranchCashTillAccountBranchCode = "" And ICOFfice.BranchCashTillAccountCurrency = "" Then
        '            objICOffce.BranchCashTillAccountNumber = Nothing
        '            objICOffce.BranchCashTillAccountBranchCode = Nothing
        '            objICOffce.BranchCashTillAccountCurrency = Nothing
        '            objICOffce.BranchCashTillAccountType = Nothing
        '            objICOffce.BranchCashTillAccountSeqNo = Nothing
        '            objICOffce.BranchCashTillAccountClientNo = Nothing
        '            objICOffce.BranchCashTillAccountProfitCentre = Nothing
        '        Else
        '            objICOffce.BranchCashTillAccountNumber = ICOFfice.BranchCashTillAccountNumber
        '            objICOffce.BranchCashTillAccountBranchCode = ICOFfice.BranchCashTillAccountBranchCode
        '            objICOffce.BranchCashTillAccountCurrency = ICOFfice.BranchCashTillAccountCurrency
        '            objICOffce.BranchCashTillAccountType = ICOFfice.BranchCashTillAccountType
        '            objICOffce.BranchCashTillAccountSeqNo = ICOFfice.BranchCashTillAccountSeqNo
        '            objICOffce.BranchCashTillAccountClientNo = ICOFfice.BranchCashTillAccountClientNo
        '            objICOffce.BranchCashTillAccountProfitCentre = ICOFfice.BranchCashTillAccountProfitCentre
        '        End If



        '    End If


        '    If objICOffce.OffceType = "Company Office" Then
        '        objICOffce.IsApprove = True
        '    End If

        '    If ICOFfice.IsActive = True Then
        '        IsActiveText = True
        '    Else
        '        IsActiveText = False
        '    End If

        '    If ICOFfice.IsApprove = True Then
        '        IsApproveText = True
        '    Else
        '        IsApproveText = False
        '    End If

        '    If ICOFfice.IsBranch = True Then
        '        IsBranchText = True
        '    Else
        '        IsBranchText = False
        '    End If

        '    If ICOFfice.IsPrintingLocation = True Then
        '        IsPrintingLocationText = True
        '    Else
        '        IsPrintingLocationText = False
        '    End If


        '    objICOffce.Save()

        '    If objICOffce.OffceType = "Branch Office" Then
        '        If (isUpdate = False) Then
        '            '   CurrentAt = "Bank Branch : [Code:  " & objICOffce.OfficeCode.ToString() & " ; Name:  " & objICOffce.OfficeName.ToString() & " ; Phone1:  " & objICOffce.Phone1.ToString() & " ; Phone2:  " & objICOffce.Phone2.ToString() & " ; Address:  " & objICOffce.Address.ToString() & " ; Email:  " & objICOffce.Email.ToString() & " ; Description:  " & objICOffce.Description.ToString() & " ; City:  " & currentCity.CityName.ToString() & " ; IsActive:  " & objICOffce.IsActive.ToString() & " ; IsApproved:  " & objICOffce.IsApprove.ToString() & " ; IsBranch:  " & objICOffce.IsBranch.ToString() & " ; Is Printing Location:  " & objICOffce.IsPrintingLocation.ToString() & " ; ClearingNormalAccountNumber: " & objICOffce.ClearingNormalAccountNumber & " ; ClearingNormalAccountBranchCode: " & objICOffce.ClearingNormalAccountBranchCode & " ; ClearingNormalAccountCurrency: " & objICOffce.ClearingNormalAccountCurrency & " ; InterCityAccountNumber: " & objICOffce.InterCityAccountNumber & " ; InterCityAccountBranchCode: " & objICOffce.InterCityAccountBranchCode & " ; InterCityAccountCurrency: " & objICOffce.InterCityAccountCurrency & " ; SameDayAccountNumber: " & objICOffce.SameDayAccountNumber & " ; SameDayAccountBranchCode: " & objICOffce.SameDayAccountBranchCode & " ; SameDayAccountCurrency: " & objICOffce.SameDayAccountCurrency & "]"
        '            ICUtilities.AddAuditTrail(ActionStr & " Added ", "Bank Branch", objICOffce.OfficeID.ToString(), UserID.ToString(), UserName.ToString(), "ADD")

        '        Else

        '            'CurrentAt = "Bank Branch : Current Values [Code:  " & objICOffce.OfficeCode.ToString() & " ; Name:  " & objICOffce.OfficeName.ToString() & " ; Phone1:  " & objICOffce.Phone1.ToString() & " ; Phone2:  " & objICOffce.Phone2.ToString() & " ; Address:  " & objICOffce.Address.ToString() & " ; Email:  " & objICOffce.Email.ToString() & " ; Description:  " & objICOffce.Description.ToString() & " ; City:  " & currentCity.CityName.ToString() & " ; IsActive:  " & IsActiveText.ToString() & " ; IsApproved:  " & IsApproveText.ToString() & " ; IsBranch:  " & IsBranchText.ToString() & " ; Is Printing Location:  " & IsPrintingLocationText.ToString() & " ; ClearingNormalAccountNumber: " & objICOffce.ClearingNormalAccountNumber & " ; ClearingNormalAccountBranchCode: " & objICOffce.ClearingNormalAccountBranchCode & " ; ClearingNormalAccountCurrency: " & objICOffce.ClearingNormalAccountCurrency & " ; InterCityAccountNumber: " & objICOffce.InterCityAccountNumber & " ; InterCityAccountBranchCode: " & objICOffce.InterCityAccountBranchCode & " ; InterCityAccountCurrency: " & objICOffce.InterCityAccountCurrency & " ; SameDayAccountNumber: " & objICOffce.SameDayAccountNumber & " ; SameDayAccountBranchCode: " & objICOffce.SameDayAccountBranchCode & " ; SameDayAccountCurrency: " & objICOffce.SameDayAccountCurrency & "]"
        '            'PrevAt = "<br />Bank Branch : Previous Values [Code: " & prevobjICOffce.OfficeCode.ToString() & " ; Name: " & prevobjICOffce.OfficeName.ToString() & " ; Phone1:  " & prevobjICOffce.Phone1.ToString() & " ; Phone2:  " & prevobjICOffce.Phone2.ToString() & " ; Address:  " & prevobjICOffce.Address.ToString() & " ; Email:  " & prevobjICOffce.Email.ToString() & " ; Description:  " & prevobjICOffce.Description.ToString() & " ; City:  " & previousCity.CityName.ToString() & " ; IsActive:  " & prevobjICOffce.IsActive.ToString() & " ; IsApproved:  " & prevobjICOffce.IsApprove.ToString() & " ; IsBranch:  " & prevobjICOffce.IsBranch.ToString() & " ; Is Printing Location:  " & prevobjICOffce.IsPrintingLocation.ToString() & " ; ClearingNormalAccountNumber: " & prevobjICOffce.ClearingNormalAccountNumber & " ; ClearingNormalAccountBranchCode: " & prevobjICOffce.ClearingNormalAccountBranchCode & " ; ClearingNormalAccountCurrency: " & prevobjICOffce.ClearingNormalAccountCurrency & " ; InterCityAccountNumber: " & prevobjICOffce.InterCityAccountNumber & " ; InterCityAccountBranchCode: " & prevobjICOffce.InterCityAccountBranchCode & " ; InterCityAccountCurrency: " & prevobjICOffce.InterCityAccountCurrency & " ; SameDayAccountNumber: " & prevobjICOffce.SameDayAccountNumber & " ; SameDayAccountBranchCode: " & prevobjICOffce.SameDayAccountBranchCode & " ; SameDayAccountCurrency: " & prevobjICOffce.SameDayAccountCurrency & "] "
        '            ICUtilities.AddAuditTrail(ActionStr & " Updated ", "Bank Branch", objICOffce.OfficeID.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

        '        End If


        '    ElseIf objICOffce.OffceType = "Company Office" Then
        '        If (isUpdate = False) Then
        '            ' ICUtilities.AddAuditTrail("Company Office" & objICOffce.OfficeName & " Added ", "Company Office", objICOffce.OfficeID.ToString(), UserID.ToString(), UserName.ToString())
        '            ICUtilities.AddAuditTrail("Company Office [ " & objICOffce.OfficeName & " ], with Office Code [ " & objICOffce.OfficeCode & " ] , Phone No 1 [ " & objICOffce.Phone1 & " ], Phone 2 [ " & objICOffce.Phone2 & " ], Email [ " & objICOffce.Email & " ], Address [ " & objICOffce.Address & " ], City [ " & objICOffce.UpToICCityByCityID.CityName & " ], Province [ " & objICOffce.UpToICCityByCityID.UpToICProvinceByProvinceCode.ProvinceName & " ], Country [ " & objICOffce.UpToICCityByCityID.UpToICProvinceByProvinceCode.UpToICCountryByCountryCode.CountryName & " ], printing location [ " & objICOffce.IsPrintingLocation & " ] , Description [ " & objICOffce.Description & " ], Is Active [ " & objICOffce.IsActive & " ] Added ", "Company Office", objICOffce.OfficeID.ToString(), UserID.ToString(), UserName.ToString(), "ADD")

        '        Else
        '            ICUtilities.AddAuditTrail(ActionStr, "Company Office", objICOffce.OfficeID.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")

        '        End If



        '    End If

        'End Sub

        'Public Shared Sub GetBankBranchgv(ByVal BankCode As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

        '    Dim qryOffice As New ICOfficeQuery("office")
        '    Dim qryBank As New ICBankQuery("bank")
        '    Dim dtBankBranches As New DataTable



        '    Dim collOffice As New ICOfficeCollection

        '    If Not pagenumber = 0 Then
        '        qryOffice.Select(qryOffice.OfficeID, qryOffice.OfficeCode, qryOffice.OfficeName, qryBank.BankName.As("ParentName"))
        '        qryOffice.Select(qryOffice.IsActive, qryOffice.IsApprove)
        '        If BankCode.ToString() <> "0" Then
        '            qryOffice.Where(qryOffice.BankCode = BankCode.ToString())
        '        End If
        '        qryOffice.InnerJoin(qryBank).On(qryOffice.BankCode = qryBank.BankCode)
        '        qryOffice.OrderBy(qryOffice.BankCode.Ascending, qryOffice.OfficeID.Ascending)
        '        dtBankBranches = qryOffice.LoadDataTable()
        '        rg.DataSource = dtBankBranches

        '        If DoDataBind Then
        '            rg.DataBind()
        '        End If

        '    Else
        '        qryOffice.es.PageNumber = pagenumber
        '        qryOffice.es.PageSize = pagesize

        '        qryOffice.Select(qryOffice.OfficeID, qryOffice.OfficeCode, qryOffice.OfficeName, qryBank.BankName.As("ParentName"))
        '        qryOffice.Select(qryOffice.IsActive, qryOffice.IsApprove)
        '        If BankCode.ToString() <> "0" Then
        '            qryOffice.Where(qryOffice.BankCode = BankCode.ToString())
        '        End If
        '        qryOffice.InnerJoin(qryBank).On(qryOffice.BankCode = qryBank.BankCode)
        '        qryOffice.OrderBy(qryOffice.BankCode.Ascending, qryOffice.OfficeID.Ascending)
        '        dtBankBranches = qryOffice.LoadDataTable()
        '        rg.DataSource = dtBankBranches


        '        If DoDataBind Then
        '            rg.DataBind()
        '        End If

        '    End If

        'End Sub
        'Public Shared Sub GetBankBranchgv(ByVal BankCode As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

        '    Dim qryOffice As New ICOfficeQuery("office")
        '    Dim qryBank As New ICBankQuery("bank")
        '    Dim dtBankBranches As New DataTable



        '    Dim collOffice As New ICOfficeCollection

        '    If Not pagenumber = 0 Then
        '        qryOffice.Select(qryOffice.OfficeID, qryOffice.OfficeCode, qryOffice.OfficeName, qryBank.BankName.As("ParentName"))
        '        qryOffice.Select(qryOffice.IsActive, qryOffice.IsApprove)
        '        If BankCode.ToString() <> "0" Then
        '            qryOffice.Where(qryOffice.BankCode = BankCode.ToString())
        '        End If
        '        qryOffice.InnerJoin(qryBank).On(qryOffice.BankCode = qryBank.BankCode)
        '        qryOffice.OrderBy(qryOffice.BankCode.Ascending, qryOffice.OfficeID.Ascending)
        '        dtBankBranches = qryOffice.LoadDataTable()
        '        rg.DataSource = dtBankBranches

        '        If DoDataBind Then
        '            rg.DataBind()
        '        End If

        '    Else
        '        qryOffice.es.PageNumber = pagenumber
        '        qryOffice.es.PageSize = pagesize

        '        qryOffice.Select(qryOffice.OfficeID, qryOffice.OfficeCode, qryOffice.OfficeName, qryBank.BankName.As("ParentName"))
        '        qryOffice.Select(qryOffice.IsActive, qryOffice.IsApprove)
        '        If BankCode.ToString() <> "0" Then
        '            qryOffice.Where(qryOffice.BankCode = BankCode.ToString())
        '        End If
        '        qryOffice.InnerJoin(qryBank).On(qryOffice.BankCode = qryBank.BankCode)
        '        qryOffice.OrderBy(qryOffice.BankCode.Ascending, qryOffice.OfficeID.Ascending)
        '        dtBankBranches = qryOffice.LoadDataTable()
        '        rg.DataSource = dtBankBranches


        '        If DoDataBind Then
        '            rg.DataBind()
        '        End If

        '    End If

        'End Sub
        Public Shared Sub GetBankBranchgv(ByVal BankCode As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

            Dim qryOffice As New ICOfficeQuery("office")
            Dim qryBank As New ICBankQuery("bank")
            Dim dtBankBranches As New DataTable



            Dim collOffice As New ICOfficeCollection

            If Not pagenumber = 0 Then
                qryOffice.Select(qryOffice.OfficeID, qryOffice.OfficeCode, qryOffice.OfficeName, qryBank.BankName.As("ParentName"))
                qryOffice.Select(qryOffice.IsActive, qryOffice.IsApprove)
                If BankCode.ToString() <> "0" Then
                    qryOffice.Where(qryOffice.BankCode = BankCode.ToString())
                End If
                qryOffice.InnerJoin(qryBank).On(qryOffice.BankCode = qryBank.BankCode)
                qryOffice.OrderBy(qryOffice.OfficeCode.Ascending)
                dtBankBranches = qryOffice.LoadDataTable()
                rg.DataSource = dtBankBranches

                If DoDataBind Then
                    rg.DataBind()
                End If

            Else
                qryOffice.es.PageNumber = pagenumber
                qryOffice.es.PageSize = pagesize

                qryOffice.Select(qryOffice.OfficeID, qryOffice.OfficeCode, qryOffice.OfficeName, qryBank.BankName.As("ParentName"))
                qryOffice.Select(qryOffice.IsActive, qryOffice.IsApprove)
                If BankCode.ToString() <> "0" Then
                    qryOffice.Where(qryOffice.BankCode = BankCode.ToString())
                End If
                qryOffice.InnerJoin(qryBank).On(qryOffice.BankCode = qryBank.BankCode)
                qryOffice.OrderBy(qryOffice.OfficeCode.Ascending)
                dtBankBranches = qryOffice.LoadDataTable()
                rg.DataSource = dtBankBranches


                If DoDataBind Then
                    rg.DataBind()
                End If

            End If

        End Sub

        'Public Shared Sub GetBankBranchgv(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)

        '    Dim qryOffice As New ICOfficeQuery("office")
        '    Dim qryBank As New ICBankQuery("bank")
        '    Dim dtBankBranches As New DataTable



        '    Dim collOffice As New ICOfficeCollection

        '    If Not pagenumber = 0 Then
        '        qryOffice.Select(qryOffice.OfficeID, qryOffice.OfficeCode, qryOffice.OfficeName, qryBank.BankName.As("ParentName"))
        '        qryOffice.Select(qryOffice.IsActive, qryOffice.IsApprove)
        '        qryOffice.InnerJoin(qryBank).On(qryOffice.BankCode = qryBank.BankCode)
        '        qryOffice.OrderBy(qryOffice.OfficeID.Ascending)
        '        dtBankBranches = qryOffice.LoadDataTable()
        '        rg.DataSource = dtBankBranches

        '        If DoDataBind Then
        '            rg.DataBind()
        '        End If

        '    Else
        '        qryOffice.es.PageNumber = pagenumber
        '        qryOffice.es.PageSize = pagesize

        '        qryOffice.Select(qryOffice.OfficeID, qryOffice.OfficeCode, qryOffice.OfficeName, qryBank.BankName.As("ParentName"))
        '        qryOffice.Select(qryOffice.IsActive, qryOffice.IsApprove)
        '        qryOffice.InnerJoin(qryBank).On(qryOffice.BankCode = qryBank.BankCode)
        '        qryOffice.OrderBy(qryOffice.OfficeID.Ascending)
        '        dtBankBranches = qryOffice.LoadDataTable()
        '        rg.DataSource = dtBankBranches


        '        If DoDataBind Then
        '            rg.DataBind()
        '        End If

        '    End If

        'End Sub


        'Dim qryBankCode As New ICOfficeQuery("bnkcode")
        'Dim qryBankName As New ICBankQuery("bnkname")

        'qryBankName.Select(qryBankName.BankCode, qryBankName.BankName)
        'qryBankName.InnerJoin(qryBankCode).On(qryBankCode.BankCode = qryBankName.BankCode)
        'qryBankName.OrderBy(qryBankName.BankName.Ascending)




        '  Public Shared Sub DeleteOffice(ByVal officeCode As String, ByVal UserID As String, ByVal UserName As String)
        'Public Shared Sub DeleteOffice(ByVal officeID As String, ByVal UserID As String, ByVal UserName As String)
        '    Dim objICOffice As New ICOffice
        '    'objICOffice.LoadByPrimaryKey(officeCode)
        '    objICOffice.LoadByPrimaryKey(officeID)
        '    objICOffice.MarkAsDeleted()
        '    objICOffice.Save()
        '    ICUtilities.AddAuditTrail("Bank Branch : " & objICOffice.OfficeName.ToString() & " Deleted", "Bank Branch", objICOffice.OfficeID.ToString(), UserID.ToString(), UserName.ToString())
        'End Sub

        Public Shared Sub DeleteOffice(ByVal officeID As String, ByVal UserID As String, ByVal UserName As String, ByVal OfficeType As String)

            Dim objICOffice As New ICOffice
            Dim currentCity As New ICCity
            Dim CurrentAtForBankBranch As String
            Dim CurrentAtForCompanyOfice As String


            Dim officeName As String = ""

            objICOffice.es.Connection.CommandTimeout = 3600

            objICOffice.LoadByPrimaryKey(officeID)
            currentCity.LoadByPrimaryKey(objICOffice.CityID.ToString())
            officeName = objICOffice.OfficeName.ToString()
            If OfficeType = "Bank Branch" Then
                CurrentAtForBankBranch = "Bank Branch : [Code:  " & objICOffice.OfficeCode.ToString() & " ; Name:  " & objICOffice.OfficeName.ToString() & " ; Phone1:  " & objICOffice.Phone1.ToString() & " ; Phone2:  " & objICOffice.Phone2.ToString() & " ; Address:  " & objICOffice.Address.ToString() & " ; Email:  " & objICOffice.Email.ToString() & " ; Description:  " & objICOffice.Description.ToString() & " ; City:  " & currentCity.CityName.ToString() & " ; IsActive:  " & objICOffice.IsActive.ToString() & " ; IsApproved:  " & objICOffice.IsApprove.ToString() & " ; IsBranch:  " & objICOffice.IsBranch.ToString() & " ; Is Printing Location:  " & objICOffice.IsPrintingLocation.ToString() & "]"
            ElseIf OfficeType = "Company Office" Then
                CurrentAtForCompanyOfice = "Company Office : [ Name:  " & objICOffice.OfficeName.ToString() & " ; Phone1:  " & objICOffice.Phone1.ToString() & " Address:  " & objICOffice.Address.ToString() & " ; Email:  " & objICOffice.Email.ToString() & " ; City:  " & currentCity.CityName.ToString() & "]"
            End If


            objICOffice.MarkAsDeleted()
            objICOffice.Save()
            '  ICUtilities.AddAuditTrail("Bank Branch : " & officeName.ToString() & " Deleted", "Bank Branch", officeID.ToString(), UserID.ToString(), UserName.ToString())
            '  ICUtilities.AddAuditTrail("Bank Branch : " & objICOffice.OfficeName.ToString() & " Deleted", "Bank Branch", objICOffice.OfficeID.ToString(), UserID.ToString(), UserName.ToString())
            If OfficeType.ToString = "Bank Branch" Then
                ICUtilities.AddAuditTrail(CurrentAtForBankBranch & " Deleted ", "Bank Branch", objICOffice.OfficeID.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")
            ElseIf OfficeType.ToString = "Company Office" Then
                ICUtilities.AddAuditTrail(CurrentAtForCompanyOfice & " Deleted ", "Company Office", objICOffice.OfficeID.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")
            End If


        End Sub

        ' Public Shared Sub ApproveOffice(ByVal officeCode As String, ByVal ApprovedBy As Integer, ByVal IsApproved As Boolean, ByVal UserName As String)
        Public Shared Sub ApproveOffice(ByVal officeID As String, ByVal IsApproved As Boolean, ByVal UserID As Integer, ByVal UserName As String)
            Dim objICOffice As New ICOffice
            Dim currentCity As New ICCity
            Dim CurrentAt As String

            objICOffice.es.Connection.CommandTimeout = 3600


            ' objICOffice.LoadByPrimaryKey(officeCode)
            objICOffice.LoadByPrimaryKey(officeID)
            currentCity.LoadByPrimaryKey(objICOffice.CityID.ToString())

            objICOffice.IsApprove = IsApproved
            objICOffice.Approvedby = UserID
            objICOffice.ApprovedOn = Date.Now
            objICOffice.Save()
            '  ICUtilities.AddAuditTrail("Bank Branch: " & objICOffice.OfficeName.ToString() & " Approved", "Bank Branch", objICOffice.OfficeCode.ToString(), ApprovedBy.ToString(), UserName.ToString())
            ' ICUtilities.AddAuditTrail("Bank Branch: " & objICOffice.OfficeName.ToString() & " Approved", "Bank Branch", objICOffice.OfficeID.ToString(), ApprovedBy.ToString(), UserName.ToString())

            CurrentAt = "Bank Branch : [Code:  " & objICOffice.OfficeCode.ToString() & " ; Name:  " & objICOffice.OfficeName.ToString() & " ; Phone1:  " & objICOffice.Phone1.ToString() & " ; Phone2:  " & objICOffice.Phone2.ToString() & " ; Address:  " & objICOffice.Address.ToString() & " ; Email:  " & objICOffice.Email.ToString() & " ; Description:  " & objICOffice.Description.ToString() & " ; City:  " & currentCity.CityName.ToString() & " ; IsActive:  " & objICOffice.IsActive.ToString() & " ; IsApproved:  " & objICOffice.IsApprove.ToString() & " ; IsBranch:  " & objICOffice.IsBranch.ToString() & " ; Is Printing Location:  " & objICOffice.IsPrintingLocation.ToString() & "]"
            ICUtilities.AddAuditTrail(CurrentAt & " Approved ", "Bank Branch", objICOffice.OfficeID.ToString(), UserID.ToString(), UserName.ToString(), "APPROVE")


        End Sub

        Public Shared Function GetAllOffice(ByVal OfficeType As String) As DataTable
            Dim qryBank As New ICBankQuery("bnk")
            Dim qryOffice As New ICOfficeQuery("ofc")
            Dim qryCompany As New ICCompanyQuery("cmp")


            'qryOffice.Select(qryOffice.OfficeCode, qryOffice.Description, qryOffice.CityID, qryOffice.IsPrintingLocation, qryOffice.Address, qryOffice.Phone1, qryOffice.Phone2, qryOffice.Email, qryOffice.IsApprove, qryOffice.CreateDate, qryOffice.Approvedby, qryOffice.ApprovedOn, qryOffice.CreatedBy,
            '          qryOffice.IsActive, qryOffice.OffceType, qryOffice.BankCode, qryOffice.CompanyCode, qryOffice.OfficeName)

            qryOffice.Select(qryOffice.OfficeID, qryOffice.OfficeCode, qryOffice.Description, qryOffice.CityID, qryOffice.IsPrintingLocation, qryOffice.Address, qryOffice.Phone1, qryOffice.Phone2, qryOffice.Email, qryOffice.IsApprove, qryOffice.CreateDate, qryOffice.Approvedby, qryOffice.ApprovedOn, qryOffice.CreatedBy,
                    qryOffice.IsActive, qryOffice.OffceType, qryOffice.BankCode, qryOffice.CompanyCode, qryOffice.OfficeName)


            If OfficeType.ToString() = "Branch Office" Then
                qryOffice.Select(qryBank.BankName.As("ParentName"))
                qryOffice.InnerJoin(qryBank).On(qryOffice.BankCode = qryBank.BankCode)
            ElseIf OfficeType.ToString() = "Company Office" Then
                qryOffice.Select(qryCompany.CompanyName.As("ParentName"))
                qryOffice.InnerJoin(qryCompany).On(qryOffice.CompanyCode = qryCompany.CompanyCode)
            End If
            qryOffice.OrderBy(qryOffice.OfficeCode.Descending)

            Return qryOffice.LoadDataTable()
        End Function


        Public Shared Function GetOfficeByOfficeTypeActiveAndApprove(ByVal OfficeType As String) As ICOfficeCollection
            Dim objICOfficecoll As New ICOfficeCollection
            objICOfficecoll.es.Connection.CommandTimeout = 3600

            objICOfficecoll.Query.Where(objICOfficecoll.Query.OffceType = OfficeType.ToString() And objICOfficecoll.Query.IsActive = True And objICOfficecoll.Query.IsApprove = True)
            objICOfficecoll.Query.OrderBy(objICOfficecoll.Query.OfficeName.Ascending)
            objICOfficecoll.Query.Load()



            Return objICOfficecoll
        End Function
        Public Shared Function GetBranchByBankCodeActiveAndApprove(ByVal BankCode As String) As ICOfficeCollection
            Dim objICOfficecoll As New ICOfficeCollection
            objICOfficecoll.es.Connection.CommandTimeout = 3600

            objICOfficecoll.Query.Where(objICOfficecoll.Query.BankCode = BankCode.ToString() And objICOfficecoll.Query.OffceType = "Branch Office" And objICOfficecoll.Query.IsActive = True And objICOfficecoll.Query.IsApprove = True)
            objICOfficecoll.Query.OrderBy(objICOfficecoll.Query.OfficeName.Ascending)
            objICOfficecoll.Query.Load()



            Return objICOfficecoll
        End Function
        Public Shared Function GetPrincipalBankBranchActiveAndApprovePrintingLocation() As DataTable

            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim dt As New DataTable


            qryObjICOffice.Where(qryObjICOffice.OffceType = "Branch Office" And qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True And qryObjICBank.IsPrincipal = True)
            qryObjICOffice.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
            qryObjICOffice.OrderBy(qryObjICOffice.OfficeID.Descending)
            dt = qryObjICOffice.LoadDataTable


            Return dt
        End Function
        Public Shared Function GetPrincipalBankBranchActiveAndApprovePrintingLocations() As DataTable

            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim dt As New DataTable

            qryObjICOffice.Select(qryObjICOffice.OfficeID, (qryObjICOffice.OfficeCode + "-" + qryObjICOffice.OfficeName).As("OfficeName"))
            qryObjICOffice.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
            qryObjICOffice.Where(qryObjICOffice.OffceType = "Branch Office" And qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True And qryObjICBank.IsPrincipal = True And qryObjICOffice.IsPrintingLocation = True)
            qryObjICOffice.Where(qryObjICBank.IsActive = True And qryObjICBank.IsApproved = True)

            qryObjICOffice.OrderBy(qryObjICOffice.OfficeID.Descending)
            dt = qryObjICOffice.LoadDataTable


            Return dt
        End Function
        Public Shared Function GetPrincipalBankBranchActiveAndApprove() As DataTable

            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim dt As New DataTable


            qryObjICOffice.Where(qryObjICOffice.OffceType = "Branch Office" And qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True And qryObjICBank.IsPrincipal = True)
            qryObjICOffice.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
            qryObjICOffice.OrderBy(qryObjICOffice.OfficeID.Descending)
            dt = qryObjICOffice.LoadDataTable


            Return dt
        End Function
        Public Shared Function GetPrincipalBankBranchActiveAndApproveWithCodeAndName() As DataTable

            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim dt As New DataTable

            qryObjICOffice.Select(qryObjICOffice.OfficeID, (qryObjICOffice.OfficeCode + "-" + qryObjICOffice.OfficeName).As("OfficeName"))
            qryObjICOffice.Where(qryObjICOffice.OffceType = "Branch Office" And qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True And qryObjICBank.IsPrincipal = True)
            qryObjICOffice.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
            qryObjICOffice.OrderBy(qryObjICOffice.OfficeID.Descending)
            dt = qryObjICOffice.LoadDataTable


            Return dt
        End Function
        Public Shared Function GetBranchByBankCodeActiveAndApprovePrintitngLocation(ByVal BankCode As String) As ICOfficeCollection
            Dim objICOfficecoll As New ICOfficeCollection
            objICOfficecoll.es.Connection.CommandTimeout = 3600

            objICOfficecoll.Query.Where(objICOfficecoll.Query.BankCode = BankCode.ToString() And objICOfficecoll.Query.OffceType = "Branch Office" And objICOfficecoll.Query.IsActive = True And objICOfficecoll.Query.IsApprove = True And objICOfficecoll.Query.IsPrintingLocation = True)
            objICOfficecoll.Query.OrderBy(objICOfficecoll.Query.OfficeName.Ascending)
            objICOfficecoll.Query.Load()



            Return objICOfficecoll
        End Function
        Public Shared Sub GetOfficeByCompanyCodeActiveAndApproveForRadGrid(ByVal OfficeType As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As RadGrid, ByVal CompanyCode As String, ByVal GroupCode As String)
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim dt As New DataTable

            qryObjICOffice.Select(qryObjICOffice.OfficeID, qryObjICOffice.OfficeName, qryObjICOffice.IsActive, qryObjICOffice.OfficeCode.Coalesce("'-'"))
            qryObjICOffice.LeftJoin(qryObjICCompany).On(qryObjICOffice.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICOffice.LeftJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
            If Not GroupCode.ToString = "" Then
                qryObjICOffice.Where(qryObjICGroup.GroupCode = GroupCode.ToString)
            End If
            If Not CompanyCode.ToString = "" Then
                qryObjICOffice.Where(qryObjICCompany.CompanyCode = CompanyCode.ToString)
            End If

            qryObjICOffice.Where(qryObjICOffice.OffceType = OfficeType)

            qryObjICOffice.OrderBy(qryObjICOffice.OfficeCode.Descending, qryObjICOffice.OfficeName.Descending, qryObjICOffice.OfficeID.Descending)

            dt = qryObjICOffice.LoadDataTable
            If Not PageNumber = 0 Then

                qryObjICOffice.es.PageNumber = PageNumber
                qryObjICOffice.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICOffice.es.PageNumber = 1
                qryObjICOffice.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If

            End If


        End Sub

        Public Shared Function GetOfficeByCompanyCodeActiveAndApprove(ByVal CompanyCode As String) As ICOfficeCollection
            Dim objICOfficecoll As New ICOfficeCollection
            objICOfficecoll.es.Connection.CommandTimeout = 3600

            objICOfficecoll.Query.Where(objICOfficecoll.Query.CompanyCode = CompanyCode.ToString() And objICOfficecoll.Query.OffceType = "Company Office" And objICOfficecoll.Query.IsActive = True And objICOfficecoll.Query.IsApprove = True)
            objICOfficecoll.Query.OrderBy(objICOfficecoll.Query.OfficeName.Ascending)
            objICOfficecoll.Query.Load()



            Return objICOfficecoll
        End Function
        Public Shared Function GetOfficeByCompanyCodeActiveAndApproveForUserCreation(ByVal CompanyCode As String) As ICOfficeCollection
            Dim objICOfficecoll As New ICOfficeCollection
            objICOfficecoll.es.Connection.CommandTimeout = 3600
            objICOfficecoll.Query.Select(objICOfficecoll.Query.OfficeID, (objICOfficecoll.Query.OfficeCode.Coalesce("''") + "-" + objICOfficecoll.Query.OfficeName).As("OfficeName"))
            objICOfficecoll.Query.Where(objICOfficecoll.Query.CompanyCode = CompanyCode.ToString() And objICOfficecoll.Query.OffceType = "Company Office" And objICOfficecoll.Query.IsActive = True And objICOfficecoll.Query.IsApprove = True)
            objICOfficecoll.Query.OrderBy(objICOfficecoll.Query.OfficeName.Ascending)
            objICOfficecoll.Query.Load()



            Return objICOfficecoll
        End Function
        Public Shared Function GetOfficeByCompanyCodeActiveAndApproveSecond(ByVal CompanyCode As String) As ICOfficeCollection
            Dim objICOfficecoll As New ICOfficeCollection
            objICOfficecoll.es.Connection.CommandTimeout = 3600
            objICOfficecoll.Query.Select(objICOfficecoll.Query.OfficeID, (objICOfficecoll.Query.OfficeCode.Coalesce("''") + "-" + objICOfficecoll.Query.OfficeName).As("OfficeName"))
            objICOfficecoll.Query.Where(objICOfficecoll.Query.CompanyCode = CompanyCode.ToString() And objICOfficecoll.Query.OffceType = "Company Office" And objICOfficecoll.Query.IsActive = True And objICOfficecoll.Query.IsApprove = True)
            objICOfficecoll.Query.OrderBy(objICOfficecoll.Query.OfficeName.Ascending)
            objICOfficecoll.Query.Load()



            Return objICOfficecoll
        End Function
        ' Aizaz Work [Dated: 01-Nov-2013]
        Public Shared Function GetAllOfficeWithCompanyCodeActiveAndApprove() As DataTable
            Dim qryICOfficecoll As New ICOfficeQuery
            qryICOfficecoll.es2.Connection.CommandTimeout = 3600
            qryICOfficecoll.Select(qryICOfficecoll.OfficeID, (qryICOfficecoll.OfficeCode.Coalesce("''") + "-" + qryICOfficecoll.OfficeName).As("OfficeName"), qryICOfficecoll.CompanyCode)
            qryICOfficecoll.Where(qryICOfficecoll.OffceType = "Company Office" And qryICOfficecoll.IsActive = True And qryICOfficecoll.IsApprove = True)
            qryICOfficecoll.OrderBy((qryICOfficecoll.OfficeCode.Coalesce("''") + "-" + qryICOfficecoll.OfficeName).Ascending)




            Return qryICOfficecoll.LoadDataTable()
        End Function
        Public Shared Function GetOfficeWithOfficeCodeByCompanyCodeActiveAndApprove(ByVal CompanyCode As String) As ICOfficeCollection
            Dim objICOfficecoll As New ICOfficeCollection
            objICOfficecoll.es.Connection.CommandTimeout = 3600

            objICOfficecoll.Query.Where(objICOfficecoll.Query.CompanyCode = CompanyCode.ToString() And objICOfficecoll.Query.OffceType = "Company Office" And objICOfficecoll.Query.IsActive = True And objICOfficecoll.Query.IsApprove = True)
            objICOfficecoll.Query.OrderBy(objICOfficecoll.Query.OfficeName.Ascending)
            objICOfficecoll.Query.Load()



            Return objICOfficecoll
        End Function
        Public Shared Function GetOfficeByCompanyCodeActiveAndApprovePrintingLocatoion(ByVal CompanyCode As String) As ICOfficeCollection
            Dim objICOfficecoll As New ICOfficeCollection
            objICOfficecoll.es.Connection.CommandTimeout = 3600

            objICOfficecoll.Query.Where(objICOfficecoll.Query.CompanyCode = CompanyCode.ToString() And objICOfficecoll.Query.OffceType = "Company Office" And objICOfficecoll.Query.IsActive = True And objICOfficecoll.Query.IsApprove = True And objICOfficecoll.Query.IsPrintingLocation = True)
            objICOfficecoll.Query.OrderBy(objICOfficecoll.Query.OfficeName.Ascending)
            objICOfficecoll.Query.Load()



            Return objICOfficecoll
        End Function
        Public Shared Function GetOfficeByCompanyCodeActiveAndApprovePrintingLocatoionSecond(ByVal CompanyCode As String) As ICOfficeCollection
            Dim objICOfficecoll As New ICOfficeCollection
            objICOfficecoll.es.Connection.CommandTimeout = 3600
            objICOfficecoll.Query.Select(objICOfficecoll.Query.OfficeID, (objICOfficecoll.Query.OfficeCode.Coalesce("''") + "-" + objICOfficecoll.Query.OfficeName).As("OfficeName"))
            objICOfficecoll.Query.Where(objICOfficecoll.Query.CompanyCode = CompanyCode.ToString() And objICOfficecoll.Query.OffceType = "Company Office" And objICOfficecoll.Query.IsActive = True And objICOfficecoll.Query.IsApprove = True And objICOfficecoll.Query.IsPrintingLocation = True)
            objICOfficecoll.Query.OrderBy(objICOfficecoll.Query.OfficeName.Ascending)
            objICOfficecoll.Query.Load()



            Return objICOfficecoll
        End Function
        Public Shared Function GetOfficeByGroupCodeActiveAndApprovePrintingLocatoion(ByVal GroupCode As String) As DataTable
            Dim qryobjICOffice As New ICOfficeQuery("qryobjICOffice")
            Dim qryobjICCompany As New ICCompanyQuery("qryobjICCompany")
            qryobjICOffice.Select(qryobjICOffice.OfficeID, qryobjICOffice.OfficeName)
            qryobjICOffice.InnerJoin(qryobjICCompany).On(qryobjICOffice.CompanyCode = qryobjICCompany.CompanyCode)
            qryobjICOffice.Where(qryobjICOffice.OffceType = "Company Office" And qryobjICOffice.IsActive = True And qryobjICOffice.IsApprove = True And qryobjICOffice.IsPrintingLocation = True)
            qryobjICOffice.Where(qryobjICCompany.GroupCode = GroupCode)
            qryobjICOffice.OrderBy(qryobjICOffice.OfficeName, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            qryobjICOffice.Load()
            Return qryobjICOffice.LoadDataTable
        End Function
        Public Shared Function GetOfficeByGroupCodeActiveAndApprovePrintingLocatoionWithCodeAndName(ByVal GroupCode As String, ByVal ProductTypeCode As String, ByVal AccountNo As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String) As DataTable
            Dim qryobjICAPNPrintLocation As New ICAccountPaymentNatureProductTypeAndPrintLocationsQuery("qryobjICAPNPrintLocation")
            Dim qryobjICOffice As New ICOfficeQuery("qryobjICOffice")
            Dim qryobjICCompany As New ICCompanyQuery("qryobjICCompany")
            qryobjICAPNPrintLocation.Select(qryobjICAPNPrintLocation.OfficeID, (qryobjICOffice.OfficeCode.Coalesce("''") + " - " + qryobjICOffice.OfficeName).As("OfficeName"))
            qryobjICAPNPrintLocation.InnerJoin(qryobjICOffice).On(qryobjICAPNPrintLocation.OfficeID = qryobjICOffice.OfficeID)
            qryobjICAPNPrintLocation.InnerJoin(qryobjICCompany).On(qryobjICOffice.CompanyCode = qryobjICCompany.CompanyCode)
            qryobjICAPNPrintLocation.Where(qryobjICOffice.OffceType = "Company Office" And qryobjICOffice.IsActive = True And qryobjICOffice.IsApprove = True And qryobjICOffice.IsPrintingLocation = True)
            qryobjICAPNPrintLocation.Where(qryobjICCompany.GroupCode = GroupCode And qryobjICAPNPrintLocation.ProductTypeCode = ProductTypeCode)
            qryobjICAPNPrintLocation.Where(qryobjICAPNPrintLocation.AccountNumber = AccountNo And qryobjICAPNPrintLocation.BranchCode = BranchCode And qryobjICAPNPrintLocation.Currency = Currency And qryobjICAPNPrintLocation.PaymentNatureCode = PaymentNatureCode)
            qryobjICAPNPrintLocation.es.Distinct = True
            qryobjICAPNPrintLocation.OrderBy(qryobjICOffice.OfficeName, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            qryobjICAPNPrintLocation.Load()
            Return qryobjICAPNPrintLocation.LoadDataTable
            'Dim qryobjICOffice As New ICOfficeQuery("qryobjICOffice")
            'Dim qryobjICCompany As New ICCompanyQuery("qryobjICCompany")
            'qryobjICOffice.Select(qryobjICOffice.OfficeID, (qryobjICOffice.OfficeCode.Coalesce("''") + " - " + qryobjICOffice.OfficeName).As("OfficeName"))
            'qryobjICOffice.InnerJoin(qryobjICCompany).On(qryobjICOffice.CompanyCode = qryobjICCompany.CompanyCode)
            'qryobjICOffice.Where(qryobjICOffice.OffceType = "Company Office" And qryobjICOffice.IsActive = True And qryobjICOffice.IsApprove = True And qryobjICOffice.IsPrintingLocation = True)
            'qryobjICOffice.Where(qryobjICCompany.GroupCode = GroupCode)
            'qryobjICOffice.OrderBy(qryobjICOffice.OfficeName, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            'qryobjICOffice.Load()
            'Return qryobjICOffice.LoadDataTable
        End Function
        Public Shared Function GetAllOfficeActiveAndApprovePrintingLocatoion() As ICOfficeCollection
            Dim objICOfficecoll As New ICOfficeCollection
            objICOfficecoll.es.Connection.CommandTimeout = 3600

            objICOfficecoll.Query.Where(objICOfficecoll.Query.IsActive = True And objICOfficecoll.Query.IsApprove = True And objICOfficecoll.Query.IsPrintingLocation = True)
            objICOfficecoll.Query.OrderBy(objICOfficecoll.Query.OfficeName.Ascending)
            objICOfficecoll.Query.Load()



            Return objICOfficecoll
        End Function
        Public Shared Function GetNormalClearingAccountsByAccountType() As ICBankAccountsCollection
            Dim collNormalAcc As New ICBankAccountsCollection
            collNormalAcc.es.Connection.CommandTimeout = 3600

            collNormalAcc.Query.Select(collNormalAcc.Query.AccountNumber, collNormalAcc.Query.BranchCode, collNormalAcc.Query.Currency)
            collNormalAcc.Query.Where(collNormalAcc.Query.AccountType = "Normal Clearing Account")
            collNormalAcc.Query.OrderBy(collNormalAcc.Query.AccountNumber.Ascending)
            collNormalAcc.Query.Load()

            Return collNormalAcc

        End Function
        Public Shared Function GetNormalClearingAccountsByAccountTypeNew() As ICBankAccountsCollection
            Dim collSameDayAcc As New ICBankAccountsCollection
            Dim dt As New DataTable
            collSameDayAcc.es.Connection.CommandTimeout = 3600

            collSameDayAcc.Query.Select((collSameDayAcc.Query.CBAccountType.Coalesce("'-'") + "-" + collSameDayAcc.Query.AccountNumber.Coalesce("'-'") + "-" + collSameDayAcc.Query.BranchCode.Coalesce("'-'") + "-" + collSameDayAcc.Query.Currency.Coalesce("'-'") + "-" + collSameDayAcc.Query.SeqNo.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'") + "-" + collSameDayAcc.Query.ClientNo.Coalesce("'-'") + "-" + collSameDayAcc.Query.ProfitCentre.Coalesce("'-'")).As("AccountNumber"))
            collSameDayAcc.Query.Select((collSameDayAcc.Query.AccountNumber.Coalesce("'-'") + "-" + collSameDayAcc.Query.AccountName.Coalesce("'-'")).As("AccountTitle"))
            collSameDayAcc.Query.Where(collSameDayAcc.Query.AccountType = "Normal Clearing Account")
            collSameDayAcc.Query.Where(collSameDayAcc.Query.IsActive = True And collSameDayAcc.Query.IsApproved = True)
            collSameDayAcc.Query.OrderBy(collSameDayAcc.Query.AccountNumber.Ascending)
            collSameDayAcc.Query.Load()
            dt = collSameDayAcc.Query.LoadDataTable
            Return collSameDayAcc
           

        End Function

        Public Shared Function GetIntercityClearingAccountsByAccountType() As ICBankAccountsCollection
            Dim collIntercityAcc As New ICBankAccountsCollection
            collIntercityAcc.es.Connection.CommandTimeout = 3600
            Dim dt As New DataTable
            collIntercityAcc.Query.Select(collIntercityAcc.Query.AccountNumber, collIntercityAcc.Query.BranchCode, collIntercityAcc.Query.Currency)
            collIntercityAcc.Query.Where(collIntercityAcc.Query.AccountType = "Intercity Clearing Account")
            collIntercityAcc.Query.OrderBy(collIntercityAcc.Query.AccountNumber.Ascending)
            collIntercityAcc.Query.Load()
            dt = collIntercityAcc.Query.LoadDataTable
            Return collIntercityAcc

        End Function
        Public Shared Function GetIntercityClearingAccountsByAccountTypeNew() As ICBankAccountsCollection

            Dim collSameDayAcc As New ICBankAccountsCollection
            collSameDayAcc.es.Connection.CommandTimeout = 3600
            Dim dt As New DataTable
            collSameDayAcc.Query.Select((collSameDayAcc.Query.CBAccountType.Coalesce("'-'") + "-" + collSameDayAcc.Query.AccountNumber.Coalesce("'-'") + "-" + collSameDayAcc.Query.BranchCode.Coalesce("'-'") + "-" + collSameDayAcc.Query.Currency.Coalesce("'-'") + "-" + collSameDayAcc.Query.SeqNo.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'") + "-" + collSameDayAcc.Query.ClientNo.Coalesce("'-'") + "-" + collSameDayAcc.Query.ProfitCentre.Coalesce("'-'")).As("AccountNumber"))
            collSameDayAcc.Query.Select((collSameDayAcc.Query.AccountNumber.Coalesce("'-'") + "-" + collSameDayAcc.Query.AccountName.Coalesce("'-'")).As("AccountTitle"))
            collSameDayAcc.Query.Where(collSameDayAcc.Query.AccountType = "Intercity Clearing Account")
            collSameDayAcc.Query.Where(collSameDayAcc.Query.IsActive = True And collSameDayAcc.Query.IsApproved = True)
            collSameDayAcc.Query.OrderBy(collSameDayAcc.Query.AccountNumber.Ascending)

            collSameDayAcc.Query.Load()
            dt = collSameDayAcc.Query.LoadDataTable
            Return collSameDayAcc

        End Function

        Public Shared Function GetSameDayClearingAccountsByAccountType() As ICBankAccountsCollection
            Dim collSameDayAcc As New ICBankAccountsCollection
            collSameDayAcc.es.Connection.CommandTimeout = 3600

            collSameDayAcc.Query.Select(collSameDayAcc.Query.AccountNumber, collSameDayAcc.Query.BranchCode, collSameDayAcc.Query.Currency)
            collSameDayAcc.Query.Where(collSameDayAcc.Query.AccountType = "Same Day Clearing Account")
            collSameDayAcc.Query.OrderBy(collSameDayAcc.Query.AccountNumber.Ascending)
            collSameDayAcc.Query.Load()

            Return collSameDayAcc

        End Function
        Public Shared Function GetSameDayClearingAccountsByAccountTypeNew() As ICBankAccountsCollection
            Dim collSameDayAcc As New ICBankAccountsCollection
            collSameDayAcc.es.Connection.CommandTimeout = 3600
            Dim dt As New DataTable
            collSameDayAcc.Query.Select((collSameDayAcc.Query.CBAccountType.Coalesce("'-'") + "-" + collSameDayAcc.Query.AccountNumber.Coalesce("'-'") + "-" + collSameDayAcc.Query.BranchCode.Coalesce("'-'") + "-" + collSameDayAcc.Query.Currency.Coalesce("'-'") + "-" + collSameDayAcc.Query.SeqNo.Cast(EntitySpaces.DynamicQuery.esCastType.String).Coalesce("'-'") + "-" + collSameDayAcc.Query.ClientNo.Coalesce("'-'") + "-" + collSameDayAcc.Query.ProfitCentre.Coalesce("'-'")).As("AccountNumber"))
            collSameDayAcc.Query.Select((collSameDayAcc.Query.AccountNumber.Coalesce("'-'") + "-" + collSameDayAcc.Query.AccountName.Coalesce("'-'")).As("AccountTitle"))
            collSameDayAcc.Query.Where(collSameDayAcc.Query.AccountType = "Same Day Clearing Account")
            collSameDayAcc.Query.Where(collSameDayAcc.Query.IsActive = True And collSameDayAcc.Query.IsApproved = True)
            collSameDayAcc.Query.OrderBy(collSameDayAcc.Query.AccountNumber.Ascending)
            collSameDayAcc.Query.Load()
            dt = collSameDayAcc.Query.LoadDataTable
            Return collSameDayAcc

        End Function
        Public Shared Function GetAllPrinciaplBankBranchesByCityCode(ByVal CityCode As String) As DataTable
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim dt As New DataTable
            qryObjICOffice.Select(qryObjICOffice.OfficeID, qryObjICOffice.OfficeName)
            qryObjICOffice.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
            qryObjICOffice.Where(qryObjICBank.IsActive = True And qryObjICOffice.IsApprove = True And qryObjICOffice.IsActive = True And qryObjICBank.IsPrincipal = True)
            qryObjICOffice.Where(qryObjICOffice.CityID = CityCode.ToString And qryObjICOffice.OffceType = "Branch Office")
            dt = qryObjICOffice.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAllNonPrinciaplDDPreferedBankBranchesByCityCode(ByVal CityCode As String) As DataTable
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim dt As New DataTable
            qryObjICOffice.Select(qryObjICOffice.OfficeID, qryObjICOffice.OfficeName)
            qryObjICOffice.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
            qryObjICOffice.Where(qryObjICBank.IsActive = True And qryObjICOffice.IsApprove = True And qryObjICOffice.IsActive = True And qryObjICBank.IsDDPreferred = True And qryObjICBank.IsPrincipal = False)
            qryObjICOffice.Where(qryObjICOffice.CityID = CityCode.ToString And qryObjICOffice.OffceType = "Branch Office")
            dt = qryObjICOffice.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAllNonPrinciaplBankCodeTaggedWithDDPayableAccounts(ByVal ProductTypeCode As String) As ICDDPayableAccountsCollection
            Dim ObjICDDPayableAccountsColl As New ICDDPayableAccountsCollection
            ObjICDDPayableAccountsColl.es.Connection.CommandTimeout = 3600
            ObjICDDPayableAccountsColl.Query.Where(ObjICDDPayableAccountsColl.Query.ProductTypeCode = ProductTypeCode)
            ObjICDDPayableAccountsColl.Query.OrderBy(ObjICDDPayableAccountsColl.Query.PreferenceOrder, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            If ObjICDDPayableAccountsColl.Query.Load Then
                Return ObjICDDPayableAccountsColl
            End If
        End Function
        'Public Shared Function GetAllNonPrinciaplBankBranchesByCityCodeAndBankCode(ByVal CityCode As String, ByVal BankCode As String) As ICOfficeCollection
        '    Dim objICOfficeColl As New ICOfficeCollection
        '    objICOfficeColl.es.Connection.CommandTimeout = 3600
        '    objICOfficeColl.Query.Where(objICOfficeColl.Query.BankCode = BankCode And objICOfficeColl.Query.CityID = CityCode)
        '    objICOfficeColl.Query.Where(objICOfficeColl.Query.OffceType = "Branch Office")
        '    objICOfficeColl.Query.OrderBy(objICOfficeColl.Query.OfficeID, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
        '    objICOfficeColl.Query.Load()
        '    Return objICOfficeColl
        'End Function
        Public Shared Function GetAllNonPrinciaplBankBranchesByCityCodeAndBankCode(ByVal CityCode As String, ByVal BankCode As String) As DataTable
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim dt As New DataTable

            qryObjICOffice.Select(qryObjICOffice.OfficeID, qryObjICOffice.OfficeName)
            qryObjICOffice.InnerJoin(qryObjICBank).on(qryObjICOffice.BankCode = qryObjICBank.BankCode)
            qryObjICOffice.Where(qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True And qryObjICOffice.OffceType = "Branch Office")
            qryObjICOffice.Where(qryObjICOffice.CityID = CityCode.ToString And qryObjICBank.BankCode = BankCode And qryObjICBank.IsDDPreferred = False And qryObjICBank.IsDDEnabled = True)
            qryObjICOffice.OrderBy(qryObjICOffice.OfficeID.Ascending)
            dt = qryObjICOffice.LoadDataTable
            Return dt
        End Function
        ' Aizaz Ahmed [Dated: 12-Feb-2013]
        'Public Shared Function IsPrintLocationExistByPrintLocationNameOrCode(ByVal PrintLocation As String) As Boolean
        '    Dim objICOffice As New ICOffice
        '    objICOffice.es.Connection.CommandTimeout = 3600
        '    objICOffice.Query.Where(objICOffice.Query.OfficeCode = PrintLocation And objICOffice.Query.IsActive = True And objICOffice.Query.IsPrintingLocation = True)
        '    If objICOffice.Query.Load() Then
        '        Return True
        '    Else
        '        objICOffice = New ICOffice
        '        objICOffice.Query.Where(objICOffice.Query.OfficeName.ToLower = PrintLocation.ToLower.ToString() And objICOffice.Query.IsActive = True And objICOffice.Query.IsPrintingLocation = True)
        '        If objICOffice.Query.Load() Then
        '            Return True
        '        Else
        '            Return False
        '        End If
        '    End If
        'End Function
        Public Shared Function IsPrintLocationExistByPrintLocationNameOrCode(ByVal PrintLocation As String, ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal ProdutcTypeCode As String, ByVal PaymentNatureCode As String) As Boolean
            
            Dim i As Integer = Nothing

            If Integer.TryParse(PrintLocation.ToString(), i) Then
                Dim qryObjAPNPTPrintLocations As New ICAccountPaymentNatureProductTypeAndPrintLocationsQuery("qryObjAPNPTPrintLocations")
                Dim qryObjAPNPType As New ICAccountsPaymentNatureProductTypeQuery("qryObjAPNPType")
                Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
                Dim dt As New DataTable
                qryObjAPNPTPrintLocations.InnerJoin(qryObjAPNPType).On(qryObjAPNPTPrintLocations.AccountNumber = qryObjAPNPType.AccountNumber And qryObjAPNPTPrintLocations.BranchCode = qryObjAPNPType.BranchCode And qryObjAPNPTPrintLocations.Currency = qryObjAPNPType.Currency And qryObjAPNPTPrintLocations.PaymentNatureCode = qryObjAPNPType.PaymentNatureCode And qryObjAPNPTPrintLocations.ProductTypeCode = qryObjAPNPType.ProductTypeCode)
                qryObjAPNPTPrintLocations.InnerJoin(qryObjICOffice).On(qryObjAPNPTPrintLocations.OfficeID = qryObjICOffice.OfficeID)
                qryObjAPNPTPrintLocations.Where(qryObjAPNPTPrintLocations.OfficeID = PrintLocation And qryObjAPNPTPrintLocations.AccountNumber = AccountNumber And qryObjAPNPTPrintLocations.BranchCode = BranchCode And qryObjAPNPTPrintLocations.Currency = Currency And qryObjAPNPTPrintLocations.PaymentNatureCode = PaymentNatureCode And qryObjAPNPTPrintLocations.ProductTypeCode = ProdutcTypeCode)
                qryObjAPNPTPrintLocations.Where(qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True And qryObjAPNPType.IsApproved = True)

                dt = qryObjAPNPTPrintLocations.LoadDataTable
                If dt.Rows.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            Else
                Dim qryObjAPNPTPrintLocations As New ICAccountPaymentNatureProductTypeAndPrintLocationsQuery("qryObjAPNPTPrintLocations")
                Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
                Dim dt As New DataTable
                qryObjAPNPTPrintLocations.Select(qryObjICOffice.OfficeName)
                qryObjAPNPTPrintLocations.InnerJoin(qryObjICOffice).On(qryObjAPNPTPrintLocations.OfficeID = qryObjICOffice.OfficeID)
                qryObjAPNPTPrintLocations.Where(qryObjAPNPTPrintLocations.AccountNumber = AccountNumber And qryObjAPNPTPrintLocations.BranchCode = BranchCode And qryObjAPNPTPrintLocations.Currency = Currency And qryObjAPNPTPrintLocations.PaymentNatureCode = PaymentNatureCode And qryObjAPNPTPrintLocations.ProductTypeCode = ProdutcTypeCode)
                qryObjAPNPTPrintLocations.Where(qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True)
                'qryObjAPNPTPrintLocations.Where(qryObjICOffice.OfficeName = PrintLocation.ToString)
                qryObjAPNPTPrintLocations.Where(qryObjICOffice.OfficeName.Trim.ToLower = PrintLocation.ToString.Trim.ToLower)
                'qryObjAPNPTPrintLocations.Where(qryObjICOffice.OfficeName = PrintLocation.ToString)
                dt = qryObjAPNPTPrintLocations.LoadDataTable
                If dt.Rows.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            End If
        End Function
        Public Shared Function GetPrintLocationByPrintLocationNameOrCode(ByVal PrintLocation As String) As ICOffice
            Dim objICOffice As New ICOffice
            objICOffice.es.Connection.CommandTimeout = 3600
            objICOffice.Query.Where(objICOffice.Query.OfficeCode = PrintLocation And objICOffice.Query.IsActive = True And objICOffice.Query.IsPrintingLocation = True)
            If objICOffice.Query.Load() Then
                Return objICOffice
            Else
                objICOffice = New ICOffice
                objICOffice.Query.Where(objICOffice.Query.OfficeName.ToLower = PrintLocation.ToLower.ToString() And objICOffice.Query.IsActive = True And objICOffice.Query.IsPrintingLocation = True)
                If objICOffice.Query.Load() Then
                    Return objICOffice
                Else
                    Return Nothing
                End If
            End If
        End Function
        'Public Shared Function GetBranchByBankCodeActiveAndApprovePrintitngLocationToDisplayBankCode(ByVal BankCode As String) As ICOfficeCollection
        '    Dim objICOfficecoll As New ICOfficeCollection
        '    objICOfficecoll.es.Connection.CommandTimeout = 3600
        '    objICOfficecoll.Query.Select(objICOfficecoll.Query.OfficeID, (objICOfficecoll.Query.OfficeCode + " - " + objICOfficecoll.Query.OfficeName).As("OfficeName"))
        '    objICOfficecoll.Query.Where(objICOfficecoll.Query.BankCode = BankCode.ToString() And objICOfficecoll.Query.OffceType = "Branch Office" And objICOfficecoll.Query.IsActive = True And objICOfficecoll.Query.IsApprove = True And objICOfficecoll.Query.IsPrintingLocation = True)
        '    objICOfficecoll.Query.OrderBy(objICOfficecoll.Query.OfficeName.Ascending)
        '    objICOfficecoll.Query.Load()



        '    Return objICOfficecoll
        'End Function
        Public Shared Function GetBranchByBankCodeActiveAndApprovePrintitngLocationToDisplayBankCode(ByVal BankCode As String, ByVal ProductTypeCode As String, ByVal AccountNo As String, ByVal BranhcCode As String, ByVal Currentcy As String, ByVal PaymentNatureCode As String) As DataTable
            Dim qryObjICAPNPrintLocations As New ICAccountPaymentNatureProductTypeAndPrintLocationsQuery("qryObjICAPNPrintLocations")
            Dim qryObjICAPNPType As New ICAccountsPaymentNatureProductTypeQuery("qryObjICAPNPType")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim dt As New DataTable
            qryObjICAPNPrintLocations.Select(qryObjICAPNPrintLocations.OfficeID, (qryObjICOffice.OfficeCode.Coalesce("''") + " - " + qryObjICOffice.OfficeName).As("OfficeName"))
            qryObjICAPNPrintLocations.InnerJoin(qryObjICAPNPType).On(qryObjICAPNPrintLocations.AccountNumber = qryObjICAPNPType.AccountNumber And qryObjICAPNPrintLocations.BranchCode = qryObjICAPNPType.BranchCode And qryObjICAPNPrintLocations.Currency = qryObjICAPNPType.Currency And qryObjICAPNPrintLocations.PaymentNatureCode = qryObjICAPNPType.PaymentNatureCode And qryObjICAPNPrintLocations.ProductTypeCode = qryObjICAPNPType.ProductTypeCode)
            qryObjICAPNPrintLocations.InnerJoin(qryObjICOffice).On(qryObjICAPNPrintLocations.OfficeID = qryObjICOffice.OfficeID)
            qryObjICAPNPrintLocations.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
            qryObjICAPNPrintLocations.Where(qryObjICAPNPrintLocations.ProductTypeCode = ProductTypeCode And qryObjICOffice.OffceType = "Branch Office" And qryObjICOffice.IsPrintingLocation = True)
            qryObjICAPNPrintLocations.Where(qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True And qryObjICBank.BankCode = BankCode.ToString)
            qryObjICAPNPrintLocations.Where(qryObjICAPNPrintLocations.AccountNumber = AccountNo And qryObjICAPNPrintLocations.BranchCode = BranhcCode And qryObjICAPNPrintLocations.Currency = Currentcy And qryObjICAPNPrintLocations.PaymentNatureCode = PaymentNatureCode)
            qryObjICAPNPrintLocations.es.Distinct = True
            dt = qryObjICAPNPrintLocations.LoadDataTable
            Return dt
        End Function
        'Public Shared Function GetPrincipalBankBranchForRoleAPNTagging() As DataTable
        '    Dim dt As New DataTable
        '    Dim qryObjICMainOff As New ICOfficeQuery("qryObjICMainOff")
        '    Dim qryObjICSubQueryOff As New ICOfficeQuery("qryObjICSubQueryOff")
        '    Dim qryObjICBank As New ICBankQuery("qryObjICBank")
        '    Dim qryObjICSubQueryBank As New ICBankQuery("qryObjICSubQueryBank")
        '    qryObjICMainOff.Select(qryObjICMainOff.OfficeID, qryObjICMainOff.OfficeName, qryObjICMainOff.IsBranch, qryObjICMainOff.IsPrintingLocation)
        '    qryObjICMainOff.InnerJoin(qryObjICBank).On(qryObjICMainOff.BankCode = qryObjICBank.BankCode)
        '    qryObjICMainOff.Where(qryObjICMainOff.OffceType = "Branch Office" And qryObjICMainOff.IsActive = True And qryObjICMainOff.IsApprove = True)
        '    qryObjICMainOff.Where(qryObjICBank.IsPrincipal = True)
        '    qryObjICMainOff.Where(qryObjICMainOff.OfficeID.NotIn(qryObjICSubQueryOff.[Select](qryObjICSubQueryOff.OfficeID).InnerJoin(qryObjICSubQueryBank).On(qryObjICSubQueryOff.BankCode = qryObjICSubQueryBank.BankCode).Where(qryObjICSubQueryBank.IsPrincipal = True And qryObjICSubQueryOff.OffceType = "Branch Office" And qryObjICSubQueryOff.IsBranch = True And (qryObjICSubQueryOff.IsPrintingLocation.IsNull Or qryObjICSubQueryOff.IsPrintingLocation = False))))
        '    qryObjICMainOff.OrderBy(qryObjICMainOff.OfficeName.Ascending)
        '    dt = qryObjICMainOff.LoadDataTable()
        '    Return dt
        'End Function
        Public Shared Function IsPrintLocationExistByPrintLocationNameOrCodeForBulkUpLoad(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String) As DataTable

            Dim qryObjAPNPTPrintLocations As New ICAccountPaymentNatureProductTypeAndPrintLocationsQuery("qryObjAPNPTPrintLocations")
            Dim qryObjAPNPType As New ICAccountsPaymentNatureProductTypeQuery("qryObjAPNPType")
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim dt As New DataTable
            qryObjAPNPTPrintLocations.Select(qryObjICOffice.OfficeName.Trim.ToLower, qryObjICOffice.OfficeID, qryObjAPNPTPrintLocations.ProductTypeCode.Trim.ToLower)
            qryObjAPNPTPrintLocations.InnerJoin(qryObjAPNPType).On(qryObjAPNPTPrintLocations.AccountNumber = qryObjAPNPType.AccountNumber And qryObjAPNPTPrintLocations.BranchCode = qryObjAPNPType.BranchCode And qryObjAPNPTPrintLocations.Currency = qryObjAPNPType.Currency And qryObjAPNPTPrintLocations.PaymentNatureCode = qryObjAPNPType.PaymentNatureCode And qryObjAPNPTPrintLocations.ProductTypeCode = qryObjAPNPType.ProductTypeCode)
            qryObjAPNPTPrintLocations.InnerJoin(qryObjICOffice).On(qryObjAPNPTPrintLocations.OfficeID = qryObjICOffice.OfficeID)
            qryObjAPNPTPrintLocations.Where(qryObjAPNPTPrintLocations.AccountNumber = AccountNumber And qryObjAPNPTPrintLocations.BranchCode = BranchCode And qryObjAPNPTPrintLocations.Currency = Currency And qryObjAPNPTPrintLocations.PaymentNatureCode = PaymentNatureCode)
            qryObjAPNPTPrintLocations.Where(qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True And qryObjAPNPType.IsApproved = True)
            qryObjAPNPTPrintLocations.es.Distinct = True
            'qryObjAPNPTPrintLocations.Where(qryObjICOffice.OfficeName = PrintLocation.ToString)
            'qryObjAPNPTPrintLocations.Where(qryObjICOffice.OfficeName = PrintLocation.ToString)
            dt = qryObjAPNPTPrintLocations.LoadDataTable
            Return dt

        End Function
        Public Shared Function GetPrincipalBankBranchForRoleAPNTagging() As DataTable
            Dim dt As New DataTable
            Dim qryObjICMainOff As New ICOfficeQuery("qryObjICMainOff")
            Dim qryObjICSubQueryOff As New ICOfficeQuery("qryObjICSubQueryOff")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim qryObjICSubQueryBank As New ICBankQuery("qryObjICSubQueryBank")
            qryObjICMainOff.Select(qryObjICMainOff.OfficeID, qryObjICMainOff.OfficeName, qryObjICMainOff.IsBranch, qryObjICMainOff.IsPrintingLocation)
            qryObjICMainOff.InnerJoin(qryObjICBank).On(qryObjICMainOff.BankCode = qryObjICBank.BankCode)
            qryObjICMainOff.Where(qryObjICMainOff.OffceType = "Branch Office" And qryObjICMainOff.IsActive = True And qryObjICMainOff.IsApprove = True)
            qryObjICMainOff.Where(qryObjICBank.IsPrincipal = True And qryObjICMainOff.IsAllowedForTagging = True)
            'qryObjICMainOff.Where(qryObjICMainOff.OfficeID.NotIn(qryObjICSubQueryOff.[Select](qryObjICSubQueryOff.OfficeID).InnerJoin(qryObjICSubQueryBank).On(qryObjICSubQueryOff.BankCode = qryObjICSubQueryBank.BankCode).Where(qryObjICSubQueryBank.IsPrincipal = True And qryObjICSubQueryOff.OffceType = "Branch Office" And qryObjICSubQueryOff.IsBranch = True And (qryObjICSubQueryOff.IsPrintingLocation.IsNull Or qryObjICSubQueryOff.IsPrintingLocation = False))))
            qryObjICMainOff.OrderBy(qryObjICMainOff.OfficeName.Ascending)
            dt = qryObjICMainOff.LoadDataTable()
            Return dt
        End Function
        'Public Shared Function IsPrintLocationExistByPrintLocationNameOrCodeForBulkUpLoad(ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String) As DataTable

        '    Dim qryObjAPNPTPrintLocations As New ICAccountPaymentNatureProductTypeAndPrintLocationsQuery("qryObjAPNPTPrintLocations")
        '    Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")

        '    Dim dt As New DataTable
        '    qryObjAPNPTPrintLocations.Select(qryObjICOffice.OfficeName.Trim.ToLower, qryObjICOffice.OfficeID)
        '    qryObjAPNPTPrintLocations.InnerJoin(qryObjICOffice).On(qryObjAPNPTPrintLocations.OfficeID = qryObjICOffice.OfficeID)
        '    qryObjAPNPTPrintLocations.Where(qryObjAPNPTPrintLocations.AccountNumber = AccountNumber And qryObjAPNPTPrintLocations.BranchCode = BranchCode And qryObjAPNPTPrintLocations.Currency = Currency And qryObjAPNPTPrintLocations.PaymentNatureCode = PaymentNatureCode)
        '    qryObjAPNPTPrintLocations.Where(qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True)
        '    'qryObjAPNPTPrintLocations.Where(qryObjICOffice.OfficeName = PrintLocation.ToString)
        '    'qryObjAPNPTPrintLocations.Where(qryObjICOffice.OfficeName = PrintLocation.ToString)
        '    dt = qryObjAPNPTPrintLocations.LoadDataTable
        '    Return dt

        'End Function
        Public Shared Function IsPrincipalBankBranchExistByCodeORNameAsDataTable(ByVal BranchCode As String) As DataTable
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim i As Integer
            If Integer.TryParse(BranchCode, i) = True Then
                qryObjICOffice.Select(qryObjICOffice.OfficeID, qryObjICOffice.OfficeName)
                qryObjICOffice.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
                qryObjICOffice.Where(qryObjICBank.IsActive = True And qryObjICBank.IsApproved = True And qryObjICBank.IsPrincipal = True)
                qryObjICOffice.Where(qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True And qryObjICOffice.OffceType = "Branch Office")
                qryObjICOffice.Where(qryObjICOffice.OfficeID = BranchCode)
                If qryObjICOffice.LoadDataTable.Rows.Count > 0 Then
                    Return qryObjICOffice.LoadDataTable
                Else
                    qryObjICOffice = New ICOfficeQuery("qryObjICOffice")
                    qryObjICBank = New ICBankQuery("qryObjICBank")
                    qryObjICOffice.Select(qryObjICOffice.OfficeID, qryObjICOffice.OfficeName)
                    qryObjICOffice.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
                    qryObjICOffice.Where(qryObjICBank.IsActive = True And qryObjICBank.IsApproved = True And qryObjICBank.IsPrincipal = True)
                    qryObjICOffice.Where(qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True And qryObjICOffice.OffceType = "Branch Office")
                    qryObjICOffice.Where(qryObjICOffice.OfficeCode = BranchCode)
                    Return qryObjICOffice.LoadDataTable
                End If
            Else
                qryObjICOffice = New ICOfficeQuery("qryObjICOffice")
                qryObjICBank = New ICBankQuery("qryObjICBank")
                qryObjICOffice.Select(qryObjICOffice.OfficeID, qryObjICOffice.OfficeName)
                qryObjICOffice.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
                qryObjICOffice.Where(qryObjICBank.IsActive = True And qryObjICBank.IsApproved = True And qryObjICBank.IsPrincipal = True)
                qryObjICOffice.Where(qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True And qryObjICOffice.OffceType = "Branch Office")
                qryObjICOffice.Where(qryObjICOffice.OfficeCode = BranchCode)
                If qryObjICOffice.LoadDataTable.Rows.Count > 0 Then
                    Return qryObjICOffice.LoadDataTable
                Else
                    qryObjICOffice = New ICOfficeQuery("qryObjICOffice")
                    qryObjICBank = New ICBankQuery("qryObjICBank")
                    qryObjICOffice.Select(qryObjICOffice.OfficeID, qryObjICOffice.OfficeName)
                    qryObjICOffice.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
                    qryObjICOffice.Where(qryObjICBank.IsActive = True And qryObjICBank.IsApproved = True And qryObjICBank.IsPrincipal = True)
                    qryObjICOffice.Where(qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True And qryObjICOffice.OffceType = "Branch Office")
                    qryObjICOffice.Where(qryObjICOffice.OfficeName = BranchCode)
                    Return qryObjICOffice.LoadDataTable
                End If

            End If



        End Function
        Public Shared Function IsPrincipalBankBranchExistByCodeORNameAsBoolean(ByVal BranchCode As String) As Boolean
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim Result As Boolean = False
            Dim i As Integer
            If Integer.TryParse(BranchCode, i) = True Then
                qryObjICOffice.Select(qryObjICOffice.OfficeID, qryObjICOffice.OfficeName)
                qryObjICOffice.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
                qryObjICOffice.Where(qryObjICBank.IsActive = True And qryObjICBank.IsApproved = True And qryObjICBank.IsPrincipal = True)
                qryObjICOffice.Where(qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True And qryObjICOffice.OffceType = "Branch Office")
                qryObjICOffice.Where(qryObjICOffice.OfficeID = BranchCode)
                If qryObjICOffice.LoadDataTable.Rows.Count > 0 Then
                    Result = True
                Else
                    qryObjICOffice = New ICOfficeQuery("qryObjICOffice")
                    qryObjICBank = New ICBankQuery("qryObjICBank")
                    qryObjICOffice.Select(qryObjICOffice.OfficeID, qryObjICOffice.OfficeName)
                    qryObjICOffice.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
                    qryObjICOffice.Where(qryObjICBank.IsActive = True And qryObjICBank.IsApproved = True And qryObjICBank.IsPrincipal = True)
                    qryObjICOffice.Where(qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True And qryObjICOffice.OffceType = "Branch Office")
                    qryObjICOffice.Where(qryObjICOffice.OfficeCode = BranchCode)
                    If qryObjICOffice.LoadDataTable.Rows.Count > 0 Then
                        Result = True
                    End If
                End If
            Else
                qryObjICOffice = New ICOfficeQuery("qryObjICOffice")
                qryObjICBank = New ICBankQuery("qryObjICBank")
                qryObjICOffice.Select(qryObjICOffice.OfficeID, qryObjICOffice.OfficeName)
                qryObjICOffice.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
                qryObjICOffice.Where(qryObjICBank.IsActive = True And qryObjICBank.IsApproved = True And qryObjICBank.IsPrincipal = True)
                qryObjICOffice.Where(qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True And qryObjICOffice.OffceType = "Branch Office")
                qryObjICOffice.Where(qryObjICOffice.OfficeCode = BranchCode)
                If qryObjICOffice.LoadDataTable.Rows.Count > 0 Then
                    Result = True
                Else
                    qryObjICOffice = New ICOfficeQuery("qryObjICOffice")
                    qryObjICBank = New ICBankQuery("qryObjICBank")
                    qryObjICOffice.Select(qryObjICOffice.OfficeID, qryObjICOffice.OfficeName)
                    qryObjICOffice.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
                    qryObjICOffice.Where(qryObjICBank.IsActive = True And qryObjICBank.IsApproved = True And qryObjICBank.IsPrincipal = True)
                    qryObjICOffice.Where(qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True And qryObjICOffice.OffceType = "Branch Office")
                    qryObjICOffice.Where(qryObjICOffice.OfficeName = BranchCode)
                    If qryObjICOffice.LoadDataTable.Rows.Count > 0 Then
                        Result = True
                    End If
                End If

            End If


            Return Result
        End Function
        Public Shared Function GetOfficeNameByOfficeCode(ByVal OfficeCode As String) As String
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICBank As New ICBankQuery("qryObjICBank")
            Dim OfficeName As String = ""

            qryObjICOffice.Where(qryObjICOffice.OffceType = "Branch Office" And qryObjICBank.IsPrincipal = True And qryObjICOffice.OfficeCode = OfficeCode)
            qryObjICOffice.Where(qryObjICOffice.IsActive = True And qryObjICOffice.IsApprove = True)
            qryObjICOffice.InnerJoin(qryObjICBank).On(qryObjICOffice.BankCode = qryObjICBank.BankCode)
            qryObjICOffice.OrderBy(qryObjICOffice.OfficeID.Descending)
            If qryObjICOffice.Load() Then
                OfficeName = qryObjICOffice.OfficeName
            End If
            Return OfficeName

        End Function
    End Class
End Namespace

