﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Namespace IC
    Public Class IC2FAController
        Public Shared Function Generate2FACodeAndSend(ByVal UserID As String, ByVal Event2FA As String) As Boolean
            Dim obj2FACode As New IC2FACodes
            Dim objUser As New ICUser


            Dim Code As String = ""
            If Check2FAExistAndValid(UserID, "", Event2FA.ToString()) = False Then
                Code = ICUtilities.CreateCode2FA(8).ToString()
                objUser.LoadByPrimaryKey(UserID.ToString())
                obj2FACode.UserID = UserID.ToString()
                obj2FACode.Code = Code.ToString()
                obj2FACode.Status = "Pending"
                obj2FACode.FailAttempts = 0
                obj2FACode.CreationDate = Date.Now
                obj2FACode.CodeFor = Event2FA.ToString()
                obj2FACode.Save()

                ICUtilities.AddAuditTrail("2FA Code for " & Event2FA.ToString() & " is generated for User [ID : " & objUser.UserID & "; Name : " & objUser.DisplayName & "; UserName : " & objUser.UserName & "] ", "2FA Code", obj2FACode.Id.ToString(), objUser.UserID.ToString(), objUser.UserName.ToString(), "GENERATE")
                If Event2FA.ToString() = "Approval" Then
                    If objUser.IsTwoFAVIAEmailAllowForApproval = True Then
                        If ICNotificationManagementController.IsUserTaggedWithEvent(objUser.UserID.ToString, "37", False) = True Then
                            ' Send Email to User
                            If objUser.Email IsNot Nothing Then
                                Send2FACode(Code.ToString(), objUser.UserName.ToString(), "Email", objUser.Email.ToString(), Event2FA.ToString())
                            End If
                        End If
                    End If
                    If objUser.IsTwoFAVIASMSAllowForApproval = True Then
                        If ICNotificationManagementController.IsUserTaggedWithEvent(objUser.UserID.ToString, "37", True) = True Then
                            ' Send SMS to User
                            If objUser.CellNo IsNot Nothing Then
                                Send2FACode(Code.ToString(), objUser.UserName.ToString(), "SMS", objUser.CellNo.ToString(), Event2FA.ToString())
                            End If
                        End If
                      
                    End If
                ElseIf Event2FA.ToString() = "Login" Then
                    If objUser.IsTwoFAVIAEmailAllowForLogin = True Then
                        If ICNotificationManagementController.IsUserTaggedWithEvent(objUser.UserID.ToString, "36", False) = True Then
                            ' Send Email to User
                            If objUser.Email IsNot Nothing Then
                                Send2FACode(Code.ToString(), objUser.UserName.ToString(), "Email", objUser.Email.ToString(), Event2FA.ToString())
                            End If
                        End If
                       
                    End If
                    If objUser.IsTwoFAVIASMSAllowForLogin = True Then
                        If ICNotificationManagementController.IsUserTaggedWithEvent(objUser.UserID.ToString, "36", True) = True Then
                            ' Send SMS to User
                            If objUser.CellNo IsNot Nothing Then
                                Send2FACode(Code.ToString(), objUser.UserName.ToString(), "SMS", objUser.CellNo.ToString(), Event2FA.ToString())
                            End If
                        End If
                    End If
                End If
                Return True
            Else
                Return True
            End If
        End Function
        Public Shared Function Check2FAExistAndValid(ByVal UserID As String, ByVal Code2FA As String, ByVal Event2FA As String) As Boolean
            Dim qry2FA As New IC2FACodesQuery("fa")
            Dim CreationDate As Date = Nothing
            Dim TimeOut As Integer = 0
            Dim ID2FA As String = ""
            Dim SettingValue As String = ""
            Dim dt2FA As New DataTable

            qry2FA.Select(qry2FA.Id, qry2FA.CreationDate)
            qry2FA.Where(qry2FA.UserID = UserID.ToString() And qry2FA.Status = "Pending" And qry2FA.CodeFor = Event2FA.ToString())
            If Code2FA.ToString() <> "" Then
                qry2FA.Where(qry2FA.Code = Code2FA.ToString())
            End If

            dt2FA = qry2FA.LoadDataTable()
            If dt2FA.Rows.Count > 0 Then
                ID2FA = dt2FA.Rows(0).Item("Id").ToString()
                CreationDate = CDate(dt2FA.Rows(0).Item("CreationDate").ToString())
                SettingValue = ICUtilities.GetSettingValue("2FATimeOut").ToString()
                If SettingValue.ToString() = "" Then
                    TimeOut = 0
                Else
                    TimeOut = CInt(SettingValue.ToString())
                End If
                If Date.Now > CreationDate.AddSeconds(TimeOut) Then
                    Dim obj2FA As New IC2FACodes
                    obj2FA.LoadByPrimaryKey(ID2FA.ToString())
                    obj2FA.Status = "Expired"
                    obj2FA.Save()
                    Return False
                Else
                    Return True
                End If
            Else
                Return False
            End If
        End Function
        Private Shared Sub Send2FACode(ByVal Code As String, ByVal UserName As String, ByVal SendVia As String, ByVal SendTo As String, ByVal Event2FA As String)
            Dim Subject As String = ""
            Dim MsgBody As String = ""

            If SendVia.ToString() = "Email" Then

                If Event2FA.ToString() = "Approval" Then
                    Subject = "2FA Code for Approval"
                    MsgBody = "<p style='font-family:Calibri; font-size:10pt'>Dear " & UserName.ToString() & ",<br /><br />Please use the Code " & Code.ToString() & " to approve transactions.<br /><br />Sincerely,<br />Al Baraka</p>"
                ElseIf Event2FA.ToString() = "Login" Then
                    Subject = "2FA Code for Login"
                    MsgBody = "<p style='font-family:Calibri; font-size:10pt'>Dear " & UserName.ToString() & ",<br /><br />Please use the Code  " & Code.ToString() & " to login.<br /><br />Sincerely,<br />Al Baraka</p>"
                End If
                Try
                    ICUtilities.SendEmail(SendTo.ToString(), Subject.ToString(), MsgBody.ToString())
                Catch ex As Exception
                    ICUtilities.AddAuditTrail("Fail to send Email due to" & ex.Message.ToString, "Email Notification", Nothing, Nothing, Nothing, "Email")
                    Exit Sub
                End Try
            ElseIf SendVia.ToString() = "SMS" Then
                If Event2FA.ToString() = "Approval" Then
                    MsgBody = "Dear " & UserName.ToString() & ", Please use the Code " & Code.ToString() & " to approve transactions. Sincerely, Al Baraka"
                ElseIf Event2FA.ToString() = "Login" Then
                    MsgBody = "Dear " & UserName.ToString() & ", Please use the Code  " & Code.ToString() & " to login. Sincerely, Al Baraka"
                End If
                Try
                    ICUtilities.SendSMS(SendTo.ToString(), MsgBody.ToString())
                Catch ex As Exception
                    ICUtilities.AddAuditTrail("Fail to send SMS due to" & ex.Message.ToString, "SMS Notification", Nothing, Nothing, Nothing, "SMS")
                    Exit Sub
                End Try
            End If
        End Sub
        Public Shared Function CheckAndUpdateLoginAttemts(ByVal UserID As String, ByVal Event2FA As String) As Boolean
            Dim qry2FA As New IC2FACodesQuery("fa")
            Dim ID2FA As String = ""
            Dim SettingValue As String = ""
            Dim FailAttempts As Integer = 0
            Dim MaxFailAttempts As Integer = 0
            Dim dt2FA As New DataTable

            qry2FA.Select(qry2FA.Id, qry2FA.FailAttempts)
            qry2FA.Where(qry2FA.UserID = UserID.ToString() And qry2FA.Status = "Pending" And qry2FA.CodeFor = Event2FA.ToString())

            dt2FA = qry2FA.LoadDataTable()
            If dt2FA.Rows.Count > 0 Then
                ID2FA = dt2FA.Rows(0).Item("Id").ToString()
                SettingValue = ICUtilities.GetSettingValue("2FAMaxFailAttempts").ToString()
                If SettingValue.ToString() = "" Then
                    MaxFailAttempts = 0
                Else
                    MaxFailAttempts = CInt(SettingValue.ToString())
                End If

                Dim obj2FA As New IC2FACodes

                obj2FA.LoadByPrimaryKey(ID2FA.ToString())
                obj2FA.FailAttempts = obj2FA.FailAttempts + 1
                obj2FA.Save()

                obj2FA = New IC2FACodes
                obj2FA.LoadByPrimaryKey(ID2FA.ToString())

                If obj2FA.FailAttempts = MaxFailAttempts Then
                    obj2FA.Status = "Retries Exhausted"
                    obj2FA.Save()
                    Return False
                Else
                    Return True
                End If
            Else
                Return False
            End If
        End Function
        Public Shared Function Update2FACodeStatusAfterUse(ByVal UserID As String, ByVal Event2FA As String) As Boolean
            Dim qry2FA As New IC2FACodesQuery("fa")
            Dim ID2FA As String = ""
            Dim SettingValue As String = ""
            Dim FailAttempts As Integer = 0
            Dim MaxFailAttempts As Integer = 0
            Dim dt2FA As New DataTable

            qry2FA.Select(qry2FA.Id, qry2FA.FailAttempts)
            qry2FA.Where(qry2FA.UserID = UserID.ToString() And qry2FA.Status = "Pending" And qry2FA.CodeFor = Event2FA.ToString())

            dt2FA = qry2FA.LoadDataTable()
            If dt2FA.Rows.Count > 0 Then
                Dim obj2FA As New IC2FACodes
                ID2FA = dt2FA.Rows(0).Item("Id").ToString()
                obj2FA.LoadByPrimaryKey(ID2FA.ToString())
                obj2FA.Status = "Used"
                obj2FA.Save()

                Return True
            Else
                Return False
            End If
        End Function
    End Class
End Namespace

