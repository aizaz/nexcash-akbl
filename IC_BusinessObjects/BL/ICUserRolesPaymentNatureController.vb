﻿Imports Telerik.Web.UI
Imports System.Web
Namespace IC
    Public Class ICUserRolesPaymentNatureController
        Public Shared Sub AddUserRolesPaymentNature(ByVal ICUserRolePayNature As ICUserRolesAccountAndPaymentNature, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String, ByVal TaggingType As String)

            Dim objICUserRoleAPNature As New ICUserRolesAccountAndPaymentNature
            objICUserRoleAPNature.es.Connection.CommandTimeout = 3600



            If IsUpdate = False Then

                objICUserRoleAPNature.RolesID = ICUserRolePayNature.RolesID
                objICUserRoleAPNature.UserID = ICUserRolePayNature.UserID
                objICUserRoleAPNature.PaymentNatureCode = ICUserRolePayNature.PaymentNatureCode
                objICUserRoleAPNature.CreateBy = ICUserRolePayNature.CreateBy
                objICUserRoleAPNature.CreateDate = ICUserRolePayNature.CreateDate
                objICUserRoleAPNature.Creater = ICUserRolePayNature.Creater
                objICUserRoleAPNature.CreationDate = ICUserRolePayNature.CreationDate
                objICUserRoleAPNature.IsApproved = False
                objICUserRoleAPNature.BranchCode = ICUserRolePayNature.BranchCode
                objICUserRoleAPNature.AccountNumber = ICUserRolePayNature.AccountNumber
                objICUserRoleAPNature.Currency = ICUserRolePayNature.Currency
                objICUserRoleAPNature.Save()
                'If TaggingType = "Client" Then
                '    ICUtilities.AddAuditTrail("Account " & objICUserRoleAPNature.AccountNumber & " payment nature " & objICUserRoleAPNature.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " of group " & objICUserRoleAPNature.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupName & " for role " & objICUserRoleAPNature.UpToICRoleByRolesID.RoleName & " for user " & objICUserRoleAPNature.UpToICUserByUserID.UserName & " added.", " User Role Account Payment Nature", objICUserRoleAPNature.AccountNumber.ToString + "" + objICUserRoleAPNature.BranchCode.ToString + "" + objICUserRoleAPNature.Currency.ToString + "" + objICUserRoleAPNature.PaymentNatureCode.ToString, UsersID, UsersName, "ADD")
                'Else
                '    ICUtilities.AddAuditTrail("Account " & objICUserRoleAPNature.AccountNumber & " payment nature " & objICUserRoleAPNature.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " of group " & objICUserRoleAPNature.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupName & " for role " & objICUserRoleAPNature.UpToICRoleByRolesID.RoleName & " for user " & objICUserRoleAPNature.UpToICUserByUserID.UserName & " added.", " User Role Account Payment Nature", objICUserRoleAPNature.AccountNumber.ToString + "" + objICUserRoleAPNature.BranchCode.ToString + "" + objICUserRoleAPNature.Currency.ToString + "" + objICUserRoleAPNature.PaymentNatureCode.ToString, UsersID, UsersName, "ADD")
                'End If
            End If



        End Sub
        Public Shared Sub DeleteUserRolesPaymentNature(ByVal objICUserRolesAPNature As ICUserRolesAccountAndPaymentNature, ByVal UsersID As String, ByVal UsersName As String, ByVal TaggingType As String)
            Dim objICUserRolePayNatureForDelete As New ICUserRolesAccountAndPaymentNature
            objICUserRolePayNatureForDelete.es.Connection.CommandTimeout = 3600
            If objICUserRolePayNatureForDelete.LoadByPrimaryKey(objICUserRolesAPNature.RolesID, objICUserRolesAPNature.PaymentNatureCode, objICUserRolesAPNature.UserID, objICUserRolesAPNature.AccountNumber, objICUserRolesAPNature.BranchCode, objICUserRolesAPNature.Currency) Then
                objICUserRolePayNatureForDelete.MarkAsDeleted()
                objICUserRolePayNatureForDelete.Save()
                'If TaggingType = "Client" Then
                '    ICUtilities.AddAuditTrail("Account " & objICUserRolesAPNature.AccountNumber & " payment nature " & objICUserRolesAPNature.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " of group " & objICUserRolesAPNature.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupName & " for role " & objICUserRolesAPNature.UpToICRoleByRolesID.RoleName & " for user " & objICUserRolesAPNature.UpToICUserByUserID.UserName & " deleted.", " User Role Account Payment Nature", objICUserRolesAPNature.AccountNumber.ToString + "" + objICUserRolesAPNature.BranchCode.ToString + "" + objICUserRolesAPNature.Currency.ToString + "" + objICUserRolesAPNature.PaymentNatureCode.ToString, UsersID, UsersName, "DELETE")
                'Else
                '    ICUtilities.AddAuditTrail("Account " & objICUserRolesAPNature.AccountNumber & " payment nature " & objICUserRolesAPNature.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " of group " & objICUserRolesAPNature.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupName & "for Role " & objICUserRolesAPNature.UpToICRoleByRolesID.RoleName & " for user " & objICUserRolesAPNature.UpToICUserByUserID.UserName & " deleted.", " User Role Account Payment Nature", objICUserRolesAPNature.AccountNumber.ToString + "" + objICUserRolesAPNature.BranchCode.ToString + "" + objICUserRolesAPNature.Currency.ToString + "" + objICUserRolesAPNature.PaymentNatureCode.ToString, UsersID, UsersName, "DELETE")
                'End If
            End If
        End Sub
        Public Shared Sub ApproveUserRolesAPNatures(ByVal objICUserRolesAPNature As ICUserRolesAccountAndPaymentNature, ByVal IsApproved As Boolean, ByVal UsersID As String, ByVal UsersName As String, ByVal TaggingType As String)
            Dim objICUserRolesAPNatureForApprove As New ICUserRolesAccountAndPaymentNature
            objICUserRolesAPNatureForApprove.es.Connection.CommandTimeout = 3600
            If objICUserRolesAPNatureForApprove.LoadByPrimaryKey(objICUserRolesAPNature.RolesID, objICUserRolesAPNature.PaymentNatureCode, objICUserRolesAPNature.UserID, objICUserRolesAPNature.AccountNumber, objICUserRolesAPNature.BranchCode, objICUserRolesAPNature.Currency) Then
                objICUserRolesAPNatureForApprove.IsApproved = IsApproved
                objICUserRolesAPNatureForApprove.ApprovedBy = UsersID.ToString
                objICUserRolesAPNatureForApprove.ApprovedOn = Date.Now
                objICUserRolesAPNatureForApprove.Save()
                'If TaggingType = "Client" Then
                '    ICUtilities.AddAuditTrail("Account " & objICUserRolesAPNatureForApprove.AccountNumber & " payment nature " & objICUserRolesAPNatureForApprove.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " of group " & objICUserRolesAPNatureForApprove.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupName & " for role " & objICUserRolesAPNatureForApprove.UpToICRoleByRolesID.RoleName & " for user " & objICUserRolesAPNatureForApprove.UpToICUserByUserID.UserName & " approved.", " User Role Account Payment Nature", objICUserRolesAPNatureForApprove.AccountNumber.ToString + "" + objICUserRolesAPNatureForApprove.BranchCode.ToString + "" + objICUserRolesAPNatureForApprove.Currency.ToString + "" + objICUserRolesAPNatureForApprove.PaymentNatureCode.ToString, UsersID, UsersName, "APPROVE")
                'Else
                '    ICUtilities.AddAuditTrail("Account " & objICUserRolesAPNatureForApprove.AccountNumber & " payment nature " & objICUserRolesAPNatureForApprove.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " of group " & objICUserRolesAPNatureForApprove.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupName & " for role " & objICUserRolesAPNatureForApprove.UpToICRoleByRolesID.RoleName & " for user " & objICUserRolesAPNatureForApprove.UpToICUserByUserID.UserName & " approved.", " User Role Account Payment Nature", objICUserRolesAPNatureForApprove.AccountNumber.ToString + "" + objICUserRolesAPNatureForApprove.BranchCode.ToString + "" + objICUserRolesAPNatureForApprove.Currency.ToString + "" + objICUserRolesAPNatureForApprove.PaymentNatureCode.ToString, UsersID, UsersName, "APPROVE")
                'End If

            End If
        End Sub
        'Public Shared Sub AddUserRolesPaymentNature(ByVal ICUserRolePayNature As ICUserRolesAccountAndPaymentNature, ByVal IsUpdate As Boolean, ByVal UsersID As String, ByVal UsersName As String, ByVal TaggingType As String)

        '    Dim objICUserRoleAPNature As New ICUserRolesAccountAndPaymentNature
        '    objICUserRoleAPNature.es.Connection.CommandTimeout = 3600



        '    If IsUpdate = False Then

        '        objICUserRoleAPNature.RolesID = ICUserRolePayNature.RolesID
        '        objICUserRoleAPNature.UserID = ICUserRolePayNature.UserID
        '        objICUserRoleAPNature.PaymentNatureCode = ICUserRolePayNature.PaymentNatureCode
        '        objICUserRoleAPNature.CreateBy = ICUserRolePayNature.CreateBy
        '        objICUserRoleAPNature.CreateDate = ICUserRolePayNature.CreateDate
        '        objICUserRoleAPNature.Creater = ICUserRolePayNature.Creater
        '        objICUserRoleAPNature.CreationDate = ICUserRolePayNature.CreationDate
        '        objICUserRoleAPNature.IsApproved = False
        '        objICUserRoleAPNature.BranchCode = ICUserRolePayNature.BranchCode
        '        objICUserRoleAPNature.AccountNumber = ICUserRolePayNature.AccountNumber
        '        objICUserRoleAPNature.Currency = ICUserRolePayNature.Currency
        '        objICUserRoleAPNature.Save()
        '        'If TaggingType = "Client" Then
        '        '    ICUtilities.AddAuditTrail("Account " & objICUserRoleAPNature.AccountNumber & " payment nature " & objICUserRoleAPNature.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " of group " & objICUserRoleAPNature.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupName & " for role " & objICUserRoleAPNature.UpToICRoleByRolesID.RoleName & " for user " & objICUserRoleAPNature.UpToICUserByUserID.UserName & " added.", " User Role Account Payment Nature", objICUserRoleAPNature.AccountNumber.ToString + "" + objICUserRoleAPNature.BranchCode.ToString + "" + objICUserRoleAPNature.Currency.ToString + "" + objICUserRoleAPNature.PaymentNatureCode.ToString, UsersID, UsersName, "ADD")
        '        'Else
        '        '    ICUtilities.AddAuditTrail("Account " & objICUserRoleAPNature.AccountNumber & " payment nature " & objICUserRoleAPNature.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " of group " & objICUserRoleAPNature.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupName & " for role " & objICUserRoleAPNature.UpToICRoleByRolesID.RoleName & " for user " & objICUserRoleAPNature.UpToICUserByUserID.UserName & " added.", " User Role Account Payment Nature", objICUserRoleAPNature.AccountNumber.ToString + "" + objICUserRoleAPNature.BranchCode.ToString + "" + objICUserRoleAPNature.Currency.ToString + "" + objICUserRoleAPNature.PaymentNatureCode.ToString, UsersID, UsersName, "ADD")
        '        'End If
        '    End If



        'End Sub
        'Public Shared Sub DeleteUserRolesPaymentNature(ByVal objICUserRolesAPNature As ICUserRolesAccountAndPaymentNature, ByVal UsersID As String, ByVal UsersName As String, ByVal TaggingType As String)
        '    Dim objICUserRolePayNatureForDelete As New ICUserRolesAccountAndPaymentNature
        '    objICUserRolePayNatureForDelete.es.Connection.CommandTimeout = 3600
        '    If objICUserRolePayNatureForDelete.LoadByPrimaryKey(objICUserRolesAPNature.RolesID, objICUserRolesAPNature.PaymentNatureCode, objICUserRolesAPNature.UserID, objICUserRolesAPNature.AccountNumber, objICUserRolesAPNature.BranchCode, objICUserRolesAPNature.Currency) Then
        '        objICUserRolePayNatureForDelete.MarkAsDeleted()
        '        objICUserRolePayNatureForDelete.Save()
        '        'If TaggingType = "Client" Then
        '        '    ICUtilities.AddAuditTrail("Account " & objICUserRolesAPNature.AccountNumber & " payment nature " & objICUserRolesAPNature.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " of group " & objICUserRolesAPNature.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupName & " for role " & objICUserRolesAPNature.UpToICRoleByRolesID.RoleName & " for user " & objICUserRolesAPNature.UpToICUserByUserID.UserName & " deleted.", " User Role Account Payment Nature", objICUserRolesAPNature.AccountNumber.ToString + "" + objICUserRolesAPNature.BranchCode.ToString + "" + objICUserRolesAPNature.Currency.ToString + "" + objICUserRolesAPNature.PaymentNatureCode.ToString, UsersID, UsersName, "DELETE")
        '        'Else
        '        '    ICUtilities.AddAuditTrail("Account " & objICUserRolesAPNature.AccountNumber & " payment nature " & objICUserRolesAPNature.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " of group " & objICUserRolesAPNature.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupName & "for Role " & objICUserRolesAPNature.UpToICRoleByRolesID.RoleName & " for user " & objICUserRolesAPNature.UpToICUserByUserID.UserName & " deleted.", " User Role Account Payment Nature", objICUserRolesAPNature.AccountNumber.ToString + "" + objICUserRolesAPNature.BranchCode.ToString + "" + objICUserRolesAPNature.Currency.ToString + "" + objICUserRolesAPNature.PaymentNatureCode.ToString, UsersID, UsersName, "DELETE")
        '        'End If
        '    End If
        'End Sub
        Public Shared Function GetAllUserRolesAccountPaymentNature(ByVal PaymentNatureCode As String, ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal RoleID As String, ByVal UsersId As String) As ICUserRolesAccountAndPaymentNatureCollection
            Dim objICUserRolesAPNatureColl As New ICUserRolesAccountAndPaymentNatureCollection
            objICUserRolesAPNatureColl.es.Connection.CommandTimeout = 3600
            objICUserRolesAPNatureColl.Query.Where(objICUserRolesAPNatureColl.Query.PaymentNatureCode = PaymentNatureCode.ToString And objICUserRolesAPNatureColl.Query.AccountNumber = AccountNumber.ToString)
            objICUserRolesAPNatureColl.Query.Where(objICUserRolesAPNatureColl.Query.BranchCode = BranchCode.ToString And objICUserRolesAPNatureColl.Query.Currency = Currency.ToString)
            If UsersId <> "0" Then
                objICUserRolesAPNatureColl.Query.Where(objICUserRolesAPNatureColl.Query.UserID = UsersId.ToString)
            End If
            If RoleID <> "0" Then
                objICUserRolesAPNatureColl.Query.Where(objICUserRolesAPNatureColl.Query.RolesID = RoleID.ToString)
            End If
            objICUserRolesAPNatureColl.Query.Load()
            Return objICUserRolesAPNatureColl
            
        End Function
        Public Shared Function GetAllUserRolesAccountPaymentNatureByRoleIDAndUserID(ByVal RoleID As String, ByVal UsersId As String) As ICUserRolesAccountAndPaymentNatureCollection
            Dim objICUserRolesAPNatureColl As New ICUserRolesAccountAndPaymentNatureCollection
            objICUserRolesAPNatureColl.es.Connection.CommandTimeout = 3600
            objICUserRolesAPNatureColl.LoadAll()
            If RoleID <> "0" Then
                objICUserRolesAPNatureColl.Query.Where(objICUserRolesAPNatureColl.Query.RolesID = RoleID.ToString)
            End If
            If UsersId <> "0" Then
                objICUserRolesAPNatureColl.Query.Where(objICUserRolesAPNatureColl.Query.UserID = UsersId.ToString)
            End If

            objICUserRolesAPNatureColl.Query.Load()
            Return objICUserRolesAPNatureColl
        End Function
        Public Shared Function GetAllUserRolesAccountPaymentNatureByRoleIDAndAPN(ByVal RoleID As String, ByVal UsersId As String, ByVal AccountNumber As String, ByVal BranchCode As String, ByVal Currency As String, ByVal PaymentNatureCode As String) As ICUserRolesAccountAndPaymentNatureCollection
            Dim objICUserRolesAPNatureColl As New ICUserRolesAccountAndPaymentNatureCollection
            objICUserRolesAPNatureColl.es.Connection.CommandTimeout = 3600
            objICUserRolesAPNatureColl.LoadAll()
            objICUserRolesAPNatureColl.Query.Where(objICUserRolesAPNatureColl.Query.AccountNumber = AccountNumber And objICUserRolesAPNatureColl.Query.BranchCode = BranchCode And objICUserRolesAPNatureColl.Query.Currency = Currency And objICUserRolesAPNatureColl.Query.PaymentNatureCode = PaymentNatureCode)
            If RoleID <> "0" Then
                objICUserRolesAPNatureColl.Query.Where(objICUserRolesAPNatureColl.Query.RolesID = RoleID.ToString)
            End If
            If UsersId <> "0" Then
                objICUserRolesAPNatureColl.Query.Where(objICUserRolesAPNatureColl.Query.UserID = UsersId.ToString)
            End If

            objICUserRolesAPNatureColl.Query.Load()
            Return objICUserRolesAPNatureColl
        End Function
        'Public Shared Sub ApproveUserRolesAPNatures(ByVal objICUserRolesAPNature As ICUserRolesAccountAndPaymentNature, ByVal IsApproved As Boolean, ByVal UsersID As String, ByVal UsersName As String, ByVal TaggingType As String)
        '    Dim objICUserRolesAPNatureForApprove As New ICUserRolesAccountAndPaymentNature
        '    objICUserRolesAPNatureForApprove.es.Connection.CommandTimeout = 3600
        '    If objICUserRolesAPNatureForApprove.LoadByPrimaryKey(objICUserRolesAPNature.RolesID, objICUserRolesAPNature.PaymentNatureCode, objICUserRolesAPNature.UserID, objICUserRolesAPNature.AccountNumber, objICUserRolesAPNature.BranchCode, objICUserRolesAPNature.Currency) Then
        '        objICUserRolesAPNatureForApprove.IsApproved = IsApproved
        '        objICUserRolesAPNatureForApprove.ApprovedBy = UsersID.ToString
        '        objICUserRolesAPNatureForApprove.ApprovedOn = Date.Now
        '        objICUserRolesAPNatureForApprove.Save()
        '        If TaggingType = "Client" Then
        '            ICUtilities.AddAuditTrail("Account " & objICUserRolesAPNatureForApprove.AccountNumber & " payment nature " & objICUserRolesAPNatureForApprove.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " of group " & objICUserRolesAPNatureForApprove.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupName & " for role " & objICUserRolesAPNatureForApprove.UpToICRoleByRolesID.RoleName & " for user " & objICUserRolesAPNatureForApprove.UpToICUserByUserID.UserName & " approved.", " User Role Account Payment Nature", objICUserRolesAPNatureForApprove.AccountNumber.ToString + "" + objICUserRolesAPNatureForApprove.BranchCode.ToString + "" + objICUserRolesAPNatureForApprove.Currency.ToString + "" + objICUserRolesAPNatureForApprove.PaymentNatureCode.ToString, UsersID, UsersName, "APPROVE")
        '        Else
        '            ICUtilities.AddAuditTrail("Account " & objICUserRolesAPNatureForApprove.AccountNumber & " payment nature " & objICUserRolesAPNatureForApprove.UpToICPaymentNatureByPaymentNatureCode.PaymentNatureName & " of group " & objICUserRolesAPNatureForApprove.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupName & " for role " & objICUserRolesAPNatureForApprove.UpToICRoleByRolesID.RoleName & " for user " & objICUserRolesAPNatureForApprove.UpToICUserByUserID.UserName & " approved.", " User Role Account Payment Nature", objICUserRolesAPNatureForApprove.AccountNumber.ToString + "" + objICUserRolesAPNatureForApprove.BranchCode.ToString + "" + objICUserRolesAPNatureForApprove.Currency.ToString + "" + objICUserRolesAPNatureForApprove.PaymentNatureCode.ToString, UsersID, UsersName, "APPROVE")
        '        End If

        '    End If
        'End Sub
        Public Shared Sub GetAccountsPaymentNatureByRoleIDAndUserID(ByVal UserID As String, ByVal RoleID As String, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As Radgrid)
            Dim qryObjICUserRolesAPN As New ICUserRolesAccountAndPaymentNatureQuery("objICUserRolesAPNQuery")
            Dim qryObjICAccountsPaymentNature As New ICAccountsPaymentNatureQuery("qryObjICAccountsPaymentNature")
            Dim qryObjICPaymentNature As New ICPaymentNatureQuery("qryObjICPaymentNature")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            Dim dt As New DataTable

            qryObjICUserRolesAPN.Select(qryObjICGroup.GroupName, qryObjICCompany.CompanyName, qryObjICUserRolesAPN.AccountNumber, qryObjICUserRolesAPN.BranchCode)
            qryObjICUserRolesAPN.Select(qryObjICUserRolesAPN.Currency, qryObjICUserRolesAPN.PaymentNatureCode, qryObjICUserRolesAPN.RolesID)
            If UserID <> "0" Then
                qryObjICUserRolesAPN.Select(qryObjICUserRolesAPN.UserID)
            End If
            qryObjICUserRolesAPN.Select(qryObjICPaymentNature.PaymentNatureName, qryObjICUserRolesAPN.IsApproved)
            qryObjICUserRolesAPN.InnerJoin(qryObjICAccountsPaymentNature).On(qryObjICUserRolesAPN.AccountNumber = qryObjICAccountsPaymentNature.AccountNumber And qryObjICUserRolesAPN.BranchCode = qryObjICAccountsPaymentNature.BranchCode And qryObjICUserRolesAPN.Currency = qryObjICAccountsPaymentNature.Currency And qryObjICUserRolesAPN.PaymentNatureCode = qryObjICAccountsPaymentNature.PaymentNatureCode)
            qryObjICUserRolesAPN.InnerJoin(qryObjICAccounts).On(qryObjICAccountsPaymentNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICAccountsPaymentNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICAccountsPaymentNature.Currency = qryObjICAccounts.Currency)
            qryObjICUserRolesAPN.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICUserRolesAPN.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
            qryObjICUserRolesAPN.InnerJoin(qryObjICPaymentNature).On(qryObjICAccountsPaymentNature.PaymentNatureCode = qryObjICPaymentNature.PaymentNatureCode)
            qryObjICUserRolesAPN.GroupBy(qryObjICGroup.GroupName, qryObjICCompany.CompanyName, qryObjICUserRolesAPN.AccountNumber, qryObjICUserRolesAPN.BranchCode)
            qryObjICUserRolesAPN.GroupBy(qryObjICUserRolesAPN.Currency, qryObjICUserRolesAPN.PaymentNatureCode, qryObjICUserRolesAPN.RolesID)
            If UserID <> "0" Then
                qryObjICUserRolesAPN.GroupBy(qryObjICUserRolesAPN.UserID)
            End If
            qryObjICUserRolesAPN.GroupBy(qryObjICPaymentNature.PaymentNatureName, qryObjICUserRolesAPN.IsApproved)
            qryObjICUserRolesAPN.OrderBy(qryObjICGroup.GroupName.Descending, qryObjICCompany.CompanyName.Descending, qryObjICUserRolesAPN.AccountNumber.Descending, qryObjICPaymentNature.PaymentNatureName.Descending)
            If UserID.ToString() <> "0" Then

                qryObjICUserRolesAPN.Where(qryObjICUserRolesAPN.UserID = UserID.ToString)

            End If
            If RoleID.ToString() <> "0" Then
                qryObjICUserRolesAPN.Where(qryObjICUserRolesAPN.RolesID = RoleID.ToString)
            End If
            dt = qryObjICUserRolesAPN.LoadDataTable()


            If Not PageNumber = 0 Then
                qryObjICUserRolesAPN.es.PageNumber = PageNumber
                qryObjICUserRolesAPN.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            Else
                qryObjICUserRolesAPN.es.PageNumber = 1
                qryObjICUserRolesAPN.es.PageSize = PageSize
                rg.DataSource = dt
                If DoDataBind = True Then
                    rg.DataBind()
                End If
            End If
        End Sub
        Public Shared Function GetAllUserRolesAccountPaymentNatureByUserID(ByVal UsersId As String) As ICUserRolesAccountAndPaymentNatureCollection
            Dim objICUserRolesAPNatureColl As New ICUserRolesAccountAndPaymentNatureCollection
            objICUserRolesAPNatureColl.es.Connection.CommandTimeout = 3600
            objICUserRolesAPNatureColl.Query.Where(objICUserRolesAPNatureColl.Query.UserID = UsersId.ToString And objICUserRolesAPNatureColl.Query.IsApproved = True)
            objICUserRolesAPNatureColl.Query.Load()
            Return objICUserRolesAPNatureColl
        End Function
        'Public Shared Function GetAllTaggedCompanysByUserID(ByVal UserID As String, ByVal GroupCode As String, ByVal RoleID As ArrayList) As DataTable
        '    Dim dt As New DataTable
        '    Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
        '    Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
        '    Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

        '    qryObjICUserRolesAPNature.Select(qryObjICCompany.CompanyCode, qryObjICCompany.CompanyName)
        '    qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAccounts.Currency)
        '    qryObjICUserRolesAPNature.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
        '    qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.UserID = UserID.ToString And qryObjICCompany.GroupCode = GroupCode.ToString)
        '    qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.RolesID.In(RoleID) And qryObjICCompany.IsActive = True And qryObjICCompany.IsApprove = True)
        '    qryObjICUserRolesAPNature.GroupBy(qryObjICCompany.CompanyCode, qryObjICCompany.CompanyName)
        '    qryObjICUserRolesAPNature.OrderBy(qryObjICCompany.CompanyCode, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
        '    dt = qryObjICUserRolesAPNature.LoadDataTable

        '    Return dt
        'End Function
        Public Shared Function GetAllTaggedCompanysByUserID(ByVal UserID As String, ByVal GroupCode As String, ByVal RoleID As ArrayList) As DataTable
            Dim dt As New DataTable
            Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

            qryObjICUserRolesAPNature.Select(qryObjICCompany.CompanyCode, qryObjICCompany.CompanyName)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAccounts.Currency)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.UserID = UserID.ToString)
            If GroupCode.ToString <> "" Then
                qryObjICUserRolesAPNature.Where(qryObjICCompany.GroupCode = GroupCode.ToString)
            End If
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.RolesID.In(RoleID) And qryObjICCompany.IsActive = True And qryObjICCompany.IsApprove = True)
            qryObjICUserRolesAPNature.Where(qryObjICAccounts.IsActive = True And qryObjICAccounts.IsApproved = True)
            qryObjICUserRolesAPNature.GroupBy(qryObjICCompany.CompanyCode, qryObjICCompany.CompanyName)
            qryObjICUserRolesAPNature.OrderBy(qryObjICCompany.CompanyCode, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            dt = qryObjICUserRolesAPNature.LoadDataTable

            Return dt
        End Function
        Public Shared Function GetAllTaggedCompanysByUserIDForApprovalRule(ByVal UserID As String, ByVal GroupCode As String, ByVal RoleID As ArrayList, ByVal PaymentNatureList As ArrayList) As DataTable
            Dim dt As New DataTable
            Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

            qryObjICUserRolesAPNature.Select(qryObjICCompany.CompanyCode, qryObjICCompany.CompanyName)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAccounts.Currency)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.UserID = UserID.ToString)
            If GroupCode.ToString <> "" Then
                qryObjICUserRolesAPNature.Where(qryObjICCompany.GroupCode = GroupCode.ToString)
            End If
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.RolesID.In(RoleID) And qryObjICCompany.IsActive = True And qryObjICCompany.IsApprove = True)
            qryObjICUserRolesAPNature.Where(qryObjICAccounts.IsActive = True And qryObjICAccounts.IsApproved = True)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.PaymentNatureCode.In(PaymentNatureList))
            qryObjICUserRolesAPNature.GroupBy(qryObjICCompany.CompanyCode, qryObjICCompany.CompanyName)
            qryObjICUserRolesAPNature.OrderBy(qryObjICCompany.CompanyCode, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            dt = qryObjICUserRolesAPNature.LoadDataTable

            Return dt
        End Function
        Public Shared Function GetAllTaggedGroupsByUserID(ByVal UserID As String, ByVal RoleID As ArrayList) As DataTable
            Dim dt As New DataTable
            Dim qryObjICUserRolesAPNature As New ICUserRolesAccountAndPaymentNatureQuery("qryObjICUserRolesAPNature")
            Dim qryObjICAccounts As New ICAccountsQuery("qryObjICAccounts")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")
            Dim qryObjICGroup As New ICGroupQuery("qryObjICGroup")
            qryObjICUserRolesAPNature.Select(qryObjICGroup.GroupCode, qryObjICGroup.GroupName)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICAccounts).On(qryObjICUserRolesAPNature.AccountNumber = qryObjICAccounts.AccountNumber And qryObjICUserRolesAPNature.BranchCode = qryObjICAccounts.BranchCode And qryObjICUserRolesAPNature.Currency = qryObjICAccounts.Currency)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICCompany).On(qryObjICAccounts.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICUserRolesAPNature.InnerJoin(qryObjICGroup).On(qryObjICCompany.GroupCode = qryObjICGroup.GroupCode)
            qryObjICUserRolesAPNature.GroupBy(qryObjICGroup.GroupCode, qryObjICGroup.GroupName)
            qryObjICUserRolesAPNature.Where(qryObjICAccounts.IsActive = True And qryObjICAccounts.IsApproved = True)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.UserID = UserID.ToString And qryObjICGroup.IsActive = True)
            qryObjICUserRolesAPNature.Where(qryObjICUserRolesAPNature.RolesID.In(RoleID))
            qryObjICUserRolesAPNature.OrderBy(qryObjICGroup.GroupCode, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            dt = qryObjICUserRolesAPNature.LoadDataTable

            Return dt
        End Function
        Public Shared Function GetAllTaggedLocationsWithUserByAPNatureUSerIDAndCompanyCode(ByVal objICAPNature As ICAccountsPaymentNature, ByVal UsersID As String) As DataTable
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICUserRolesAPNLocations As New ICUserRolesAccountPaymentNatureAndLocationsQuery("qryObjICUserRolesAPNLocations")
            Dim dt As New DataTable

            qryObjICUserRolesAPNLocations.Select(qryObjICOffice.OfficeID, qryObjICOffice.OfficeName)
            qryObjICUserRolesAPNLocations.InnerJoin(qryObjICOffice).On(qryObjICUserRolesAPNLocations.OfficeID = qryObjICOffice.OfficeID)
            qryObjICUserRolesAPNLocations.Where(qryObjICUserRolesAPNLocations.AccountNumber = objICAPNature.AccountNumber And qryObjICUserRolesAPNLocations.BranchCode = objICAPNature.BranchCode And qryObjICUserRolesAPNLocations.Currency = objICAPNature.Currency And qryObjICUserRolesAPNLocations.PaymentNatureCode = objICAPNature.PaymentNatureCode)
            qryObjICUserRolesAPNLocations.Where(qryObjICUserRolesAPNLocations.UserID = UsersID)
            qryObjICUserRolesAPNLocations.OrderBy(qryObjICOffice.OfficeID, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            dt = qryObjICUserRolesAPNLocations.LoadDataTable
            Return dt
        End Function
        Public Shared Function GetAllTaggedLocationsWithUserByUSerIDAndRoleID(ByVal UserID As String, ByVal RoleID As ArrayList) As DataTable
            Dim qryObjICOffice As New ICOfficeQuery("qryObjICOffice")
            Dim qryObjICUserRolesAPNLocations As New ICUserRolesAccountPaymentNatureAndLocationsQuery("qryObjICUserRolesAPNLocations")
            Dim dt As New DataTable

            qryObjICUserRolesAPNLocations.Select(qryObjICOffice.OfficeID, qryObjICOffice.OfficeName)
            qryObjICUserRolesAPNLocations.InnerJoin(qryObjICOffice).On(qryObjICUserRolesAPNLocations.OfficeID = qryObjICOffice.OfficeID)
            qryObjICUserRolesAPNLocations.Where(qryObjICUserRolesAPNLocations.UserID = UserID And qryObjICUserRolesAPNLocations.RoleID.In(RoleID))
            qryObjICUserRolesAPNLocations.Where(qryObjICUserRolesAPNLocations.IsApprove = True)
            qryObjICUserRolesAPNLocations.es.Distinct = True
            qryObjICUserRolesAPNLocations.OrderBy(qryObjICOffice.OfficeID, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)
            dt = qryObjICUserRolesAPNLocations.LoadDataTable
            Return dt
        End Function
        Public Shared Function DeleteTaggedAPNByRoleIDAndUserID(ByVal UserId As String, ByVal RoleID As String, ByVal TaggingType As String, ByVal ActionUSerID As String, ByVal ActionUserName As String) As Boolean
            Dim objICUserRolesAPNatureColl As New ICUserRolesAccountAndPaymentNatureCollection
            Dim objICUserRolesAPNatureCollForDelete As New ICUserRolesAccountAndPaymentNatureCollection
            Dim objICUserRolesAPNatureLocationsCollForDelete As New ICUserRolesAccountPaymentNatureAndLocationsCollection
            Dim objICUserRolesAPNatureLocationsColl As New ICUserRolesAccountPaymentNatureAndLocationsCollection
            Dim objICAuditTrailColl As New ICAuditTrailCollection
            Dim objICAuditTrail As ICAuditTrail
            'Dim TaggingType As String = Nothing
            Dim StrAuditTrailAction As String = Nothing
            'If ddlUserType.SelectedValue.ToString = "Client User" Then
            '    TaggingType = "Client"
            'Else
            '    TaggingType = "Bank"
            'End If
            objICUserRolesAPNatureColl.es.Connection.CommandTimeout = 3600
            objICUserRolesAPNatureLocationsColl.es.Connection.CommandTimeout = 36
            objICUserRolesAPNatureColl = ICUserRolesPaymentNatureController.GetAllUserRolesAccountPaymentNatureByRoleIDAndUserID(RoleID.ToString, UserId.ToString)
            If objICUserRolesAPNatureColl.Count > 0 Then
                For Each objICUserRolesAPNature As ICUserRolesAccountAndPaymentNature In objICUserRolesAPNatureColl
                    objICAuditTrail = New ICAuditTrail
                    StrAuditTrailAction = Nothing
                    StrAuditTrailAction = "Account [ " & objICUserRolesAPNature.AccountNumber & " ] payment nature [ " & objICUserRolesAPNature.PaymentNatureCode & "] "
                    StrAuditTrailAction += " of group [ " & objICUserRolesAPNature.UpToICAccountsByAccountNumber.UpToICCompanyByCompanyCode.UpToICGroupByGroupCode.GroupCode & " ] for role [ " & objICUserRolesAPNature.UpToICRoleByRolesID.RoleID & " ]"
                    StrAuditTrailAction += " for user [ " & objICUserRolesAPNature.UpToICUserByUserID.UserName & " ] deleted on removal of assigned role "
                    Dim ICount As Integer = 0
                    objICUserRolesAPNatureLocationsColl = New ICUserRolesAccountPaymentNatureAndLocationsCollection
                    objICUserRolesAPNatureLocationsColl = ICUserRolesAccountPaymentNatureAndLocationsController.GetAllUserRolesAccountPaymentNatureLocations(objICUserRolesAPNature.PaymentNatureCode, objICUserRolesAPNature.AccountNumber, objICUserRolesAPNature.BranchCode, objICUserRolesAPNature.Currency, objICUserRolesAPNature.RolesID, objICUserRolesAPNature.UserID)
                    If objICUserRolesAPNatureLocationsColl.Count > 0 Then
                        For Each objICUserRoleAPNatureLocation As ICUserRolesAccountPaymentNatureAndLocations In objICUserRolesAPNatureLocationsColl
                            If ICount = 0 Then
                                StrAuditTrailAction += "with location [ "
                                ICount = ICount + 1
                            End If
                            StrAuditTrailAction += objICUserRoleAPNatureLocation.OfficeID.ToString & ";"
                            objICUserRolesAPNatureLocationsCollForDelete.Add(objICUserRoleAPNatureLocation)
                            'ICUserRolesAccountPaymentNatureAndLocationsController.DeleteUserRolesAPNaturesLocations(objICUserRoleAPNatureLocation, Me.UserId.ToString, Me.UserInfo.Username.ToString, TaggingType)
                        Next
                        'objICUserRolesAPNatureLocationsColl.Save()
                    End If
                    If StrAuditTrailAction.Contains(";") = True Then
                        StrAuditTrailAction = StrAuditTrailAction.Remove(StrAuditTrailAction.Length - 1, 1)
                        StrAuditTrailAction += " ]."
                    Else
                        StrAuditTrailAction += "."
                    End If
                    objICAuditTrail.AuditAction = StrAuditTrailAction
                    objICAuditTrail.AuditType = "User Role Account Payment Nature"
                    objICAuditTrail.RelatedID = objICUserRolesAPNature.AccountNumber.ToString + "" + objICUserRolesAPNature.BranchCode.ToString + "" + objICUserRolesAPNature.Currency.ToString + "" + objICUserRolesAPNature.PaymentNatureCode.ToString
                    objICAuditTrail.AuditDate = Date.Now
                    objICAuditTrail.UserID = ActionUSerID
                    objICAuditTrail.Username = ActionUserName
                    objICAuditTrail.ActionType = "DELETE"
                    If Not HttpContext.Current Is Nothing Then
                        objICAuditTrail.IPAddress = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
                    End If
                    objICUserRolesAPNatureCollForDelete.Add(objICUserRolesAPNature)
                    'ICUserRolesPaymentNatureController.DeleteUserRolesPaymentNature(objICUserRolesAPNature, Me.UserId.ToString, Me.UserInfo.Username.ToString, TaggingType)
                    objICAuditTrailColl.Add(objICAuditTrail)
                Next

            End If
            If objICUserRolesAPNatureLocationsCollForDelete.Count > 0 Then
                objICUserRolesAPNatureLocationsCollForDelete.MarkAllAsDeleted()
                objICUserRolesAPNatureLocationsCollForDelete.Save()
            End If
            If objICUserRolesAPNatureCollForDelete.Count > 0 Then
                objICUserRolesAPNatureCollForDelete.MarkAllAsDeleted()
                objICUserRolesAPNatureCollForDelete.Save()
            End If
            If objICAuditTrailColl.Count > 0 Then
                objICAuditTrailColl.Save()
            End If
            Return True
        End Function
        ' Aizaz Work [Dated: 26-Oct-2014]
        Public Shared Function GetAllUserRolesAccountPaymentNatureByRoleIDForUserRoleAssignment(ByVal RoleID As String) As ICUserRolesAccountAndPaymentNatureCollection
            Dim objICUserRolesAPNatureColl As New ICUserRolesAccountAndPaymentNatureCollection
            objICUserRolesAPNatureColl.Query.es.Distinct = True
            objICUserRolesAPNatureColl.Query.Select(objICUserRolesAPNatureColl.Query.RolesID, objICUserRolesAPNatureColl.Query.PaymentNatureCode, objICUserRolesAPNatureColl.Query.AccountNumber, objICUserRolesAPNatureColl.Query.BranchCode, objICUserRolesAPNatureColl.Query.Currency, objICUserRolesAPNatureColl.Query.IsApproved)
            objICUserRolesAPNatureColl.Query.Where(objICUserRolesAPNatureColl.Query.RolesID = RoleID.ToString)
            objICUserRolesAPNatureColl.Query.OrderBy(objICUserRolesAPNatureColl.Query.AccountNumber.Ascending, objICUserRolesAPNatureColl.Query.BranchCode.Ascending, objICUserRolesAPNatureColl.Query.Currency.Ascending, objICUserRolesAPNatureColl.Query.PaymentNatureCode.Ascending)
            objICUserRolesAPNatureColl.Query.Load()
            Return objICUserRolesAPNatureColl
        End Function
    End Class
End Namespace

