﻿Imports Microsoft.VisualBasic
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Namespace IC
    Public Class ICBeneficiaryManagementController

        Public Shared Function AddBeneficiary(ByVal objBene As ICBene, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String) As Integer
            'Dim objBeneficiary As New ICBene
            'Dim prevobjBeneficiary As New ICBene
            'Dim CurrentAt As String
            'Dim PrevAt As String

            'If (isUpdate = False) Then
            '    objBeneficiary.CreateBy = objBene.CreateBy
            '    objBeneficiary.CreateDate = objBene.CreateDate
            '    objBeneficiary.Creater = objBene.Creater
            '    objBeneficiary.CreationDate = objBene.CreationDate
            'Else
            '    objBeneficiary.LoadByPrimaryKey(objBene.BeneID)
            '    prevobjBeneficiary.LoadByPrimaryKey(objBene.BeneID)
            '    objBeneficiary.CreateBy = objBene.CreateBy
            '    objBeneficiary.CreateDate = objBene.CreateDate
            'End If

            'objBeneficiary.CompanyCode = objBene.CompanyCode
            'objBeneficiary.PaymentNatureCode = objBene.PaymentNatureCode
            'objBeneficiary.BeneGroupCode = objBene.BeneGroupCode
            'objBeneficiary.BeneficiaryCode = objBene.BeneficiaryCode
            'objBeneficiary.BeneficiaryName = objBene.BeneficiaryName
            'objBeneficiary.BeneficiaryAccountNo = objBene.BeneficiaryAccountNo
            'objBeneficiary.BeneficiaryAccountTitle = objBene.BeneficiaryAccountTitle
            'objBeneficiary.BeneAccountBranchCode = objBene.BeneAccountBranchCode
            'objBeneficiary.BeneAccountCurrency = objBene.BeneAccountCurrency
            'objBeneficiary.BeneficiaryBankCode = objBene.BeneficiaryBankCode
            'objBeneficiary.BeneficiaryBankName = objBene.BeneficiaryBankName
            'objBeneficiary.BeneficiaryIBFTAccountNo = objBene.BeneficiaryIBFTAccountNo
            'objBeneficiary.BeneficiaryIBFTAccountTitle = objBene.BeneficiaryIBFTAccountTitle
            'objBeneficiary.BeneficiaryCNIC = objBene.BeneficiaryCNIC
            'objBeneficiary.BeneficiaryBankAddress = objBene.BeneficiaryBankAddress
            'objBeneficiary.BeneficiaryBranchCode = objBene.BeneficiaryBranchCode
            'objBeneficiary.BeneficiaryBranchName = objBene.BeneficiaryBranchName
            'objBeneficiary.BeneficiaryBranchAddress = objBene.BeneficiaryBranchAddress
            'objBeneficiary.BeneficiaryMobile = objBene.BeneficiaryMobile
            'objBeneficiary.BeneficiaryEmail = objBene.BeneficiaryEmail
            'objBeneficiary.BeneficiaryPhone = objBene.BeneficiaryPhone
            'objBeneficiary.BeneficiaryAddress = objBene.BeneficiaryAddress
            'objBeneficiary.BeneficiaryCountry = objBene.BeneficiaryCountry
            'objBeneficiary.BeneficiaryProvince = objBene.BeneficiaryProvince
            'objBeneficiary.BeneficiaryCity = objBene.BeneficiaryCity
            'objBeneficiary.CreateBy = objBene.CreateBy
            'objBeneficiary.CreateDate = objBene.CreateDate
            'objBeneficiary.IsActive = objBene.IsActive
            'objBeneficiary.IsApproved = objBene.IsApproved
            'objBeneficiary.ApprovedOn = objBene.ApprovedOn
            'objBeneficiary.ApprovedBy = objBene.ApprovedBy
            'objBeneficiary.Creater = objBene.Creater
            'objBeneficiary.CreationDate = objBene.CreationDate
            'objBeneficiary.ApprovalByforDeletion = objBene.ApprovalByforDeletion
            'objBeneficiary.ApprovalforDeletionStatus = objBene.ApprovalforDeletionStatus
            'objBeneficiary.BeneficiaryAddedVia = objBene.BeneficiaryAddedVia
            'objBeneficiary.BeneficiaryStatus = objBene.BeneficiaryStatus
            'objBeneficiary.BeneficiaryIDNo = objBene.BeneficiaryIDNo
            'objBeneficiary.BeneficiaryIDType = objBene.BeneficiaryIDType
            'objBeneficiary.UBPCompanyID = objBene.UBPCompanyID
            'objBeneficiary.UBPReferenceNo = objBene.UBPReferenceNo
            'objBeneficiary.Save()

            'If (isUpdate = False) Then
            '    CurrentAt = "Beneficiary : [Name:  " & objBeneficiary.BeneficiaryName & " ; Beneficiary Group Code:  " & objBeneficiary.BeneGroupCode.ToString() & " ; IsActive:  " & objBeneficiary.IsActive & " ; IsApproved:  " & objBeneficiary.IsApproved & "]"
            '    ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Beneficiary Management", objBeneficiary.BeneID.ToString(), UserID.ToString(), UserName.ToString(), "ADD")
            'Else
            '    CurrentAt = "Beneficiary : Current Values [Name:  " & objBeneficiary.BeneficiaryName.ToString() & " ; Beneficiary Group Code:  " & objBeneficiary.BeneGroupCode.ToString() & " ; IsActive:  " & objBeneficiary.IsActive & " ; IsApproved:  " & objBeneficiary.IsApproved & "]"
            '    PrevAt = "<br />Beneficiary : Previous Values [Name:  " & prevobjBeneficiary.BeneficiaryName.ToString() & " ; Beneficiary Group Code:  " & prevobjBeneficiary.BeneGroupCode.ToString() & " ; IsActive:  " & prevobjBeneficiary.IsActive & " ; IsApproved:  " & prevobjBeneficiary.IsApproved & "] "
            '    ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Beneficiary Management", objBeneficiary.BeneID.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")
            'End If

            'Return objBeneficiary.BeneID

        End Function



        Public Shared Function AddBeneficiaryfromform(ByVal objBene As ICBene, ByVal isUpdate As Boolean, ByVal UserID As String, ByVal UserName As String) As Integer
            Dim objBeneficiary As New ICBene
            Dim prevobjBeneficiary As New ICBene
            Dim CurrentAt As String
            Dim PrevAt As String

            If (isUpdate = False) Then
                objBeneficiary.CreateBy = objBene.CreateBy
                objBeneficiary.CreateDate = objBene.CreateDate
                objBeneficiary.Creater = objBene.Creater
                objBeneficiary.CreationDate = objBene.CreationDate
            Else
                objBeneficiary.LoadByPrimaryKey(objBene.BeneID)
                prevobjBeneficiary.LoadByPrimaryKey(objBene.BeneID)
                objBeneficiary.CreateBy = objBene.CreateBy
                objBeneficiary.CreateDate = objBene.CreateDate
            End If

            objBeneficiary.CompanyCode = objBene.CompanyCode
            objBeneficiary.PaymentNatureCode = objBene.PaymentNatureCode
            objBeneficiary.BeneGroupCode = objBene.BeneGroupCode
            objBeneficiary.BeneficiaryCode = objBene.BeneficiaryCode
            objBeneficiary.BeneficiaryName = objBene.BeneficiaryName
            objBeneficiary.BeneficiaryAccountNo = objBene.BeneficiaryAccountNo
            objBeneficiary.BeneficiaryAccountTitle = objBene.BeneficiaryAccountTitle
            objBeneficiary.BeneAccountBranchCode = objBene.BeneAccountBranchCode
            objBeneficiary.BeneAccountCurrency = objBene.BeneAccountCurrency
            objBeneficiary.BeneficiaryBankCode = objBene.BeneficiaryBankCode
            objBeneficiary.BeneficiaryBankName = objBene.BeneficiaryBankName
            objBeneficiary.BeneficiaryIBFTAccountNo = objBene.BeneficiaryIBFTAccountNo
            objBeneficiary.BeneficiaryIBFTAccountTitle = objBene.BeneficiaryIBFTAccountTitle
            objBeneficiary.BeneficiaryCNIC = objBene.BeneficiaryCNIC
            objBeneficiary.BeneficiaryBankAddress = objBene.BeneficiaryBankAddress
            objBeneficiary.BeneficiaryBranchCode = objBene.BeneficiaryBranchCode
            objBeneficiary.BeneficiaryBranchName = objBene.BeneficiaryBranchName
            objBeneficiary.BeneficiaryBranchAddress = objBene.BeneficiaryBranchAddress
            objBeneficiary.BeneficiaryMobile = objBene.BeneficiaryMobile
            objBeneficiary.BeneficiaryEmail = objBene.BeneficiaryEmail
            objBeneficiary.BeneficiaryPhone = objBene.BeneficiaryPhone
            objBeneficiary.BeneficiaryAddress = objBene.BeneficiaryAddress
            objBeneficiary.BeneficiaryCountry = objBene.BeneficiaryCountry
            objBeneficiary.BeneficiaryProvince = objBene.BeneficiaryProvince
            objBeneficiary.BeneficiaryCity = objBene.BeneficiaryCity
            objBeneficiary.CreateBy = objBene.CreateBy
            objBeneficiary.CreateDate = objBene.CreateDate
            objBeneficiary.IsActive = objBene.IsActive
            objBeneficiary.IsApproved = objBene.IsApproved
            objBeneficiary.ApprovedOn = objBene.ApprovedOn
            objBeneficiary.ApprovedBy = objBene.ApprovedBy
            objBeneficiary.Creater = objBene.Creater
            objBeneficiary.CreationDate = objBene.CreationDate
            objBeneficiary.ApprovalByforDeletion = objBene.ApprovalByforDeletion
            objBeneficiary.ApprovalforDeletionStatus = objBene.ApprovalforDeletionStatus
            objBeneficiary.BeneficiaryAddedVia = objBene.BeneficiaryAddedVia
            objBeneficiary.BeneficiaryStatus = objBene.BeneficiaryStatus
            objBeneficiary.BeneficiaryIDNo = objBene.BeneficiaryIDNo
            objBeneficiary.BeneficiaryIDType = objBene.BeneficiaryIDType
            objBeneficiary.UBPCompanyID = objBene.UBPCompanyID
            objBeneficiary.UBPReferenceNo = objBene.UBPReferenceNo
            objBeneficiary.Save()

            If (isUpdate = False) Then
                CurrentAt = "Beneficiary : [Name:  " & objBeneficiary.BeneficiaryName & " ; Beneficiary Group Code:  " & objBeneficiary.BeneGroupCode.ToString() & " ; IsActive:  " & objBeneficiary.IsActive & " ; IsApproved:  " & objBeneficiary.IsApproved & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Added ", "Beneficiary Management", objBeneficiary.BeneID.ToString(), UserID.ToString(), UserName.ToString(), "ADD")
            Else
                CurrentAt = "Beneficiary : Current Values [Name:  " & objBeneficiary.BeneficiaryName.ToString() & " ; Beneficiary Group Code:  " & objBeneficiary.BeneGroupCode.ToString() & " ; IsActive:  " & objBeneficiary.IsActive & " ; IsApproved:  " & objBeneficiary.IsApproved & "]"
                PrevAt = "<br />Beneficiary : Previous Values [Name:  " & prevobjBeneficiary.BeneficiaryName.ToString() & " ; Beneficiary Group Code:  " & prevobjBeneficiary.BeneGroupCode.ToString() & " ; IsActive:  " & prevobjBeneficiary.IsActive & " ; IsApproved:  " & prevobjBeneficiary.IsApproved & "] "
                ICUtilities.AddAuditTrail(CurrentAt & PrevAt & " Updated ", "Beneficiary Management", objBeneficiary.BeneID.ToString(), UserID.ToString(), UserName.ToString(), "UPDATE")
            End If

            Return objBeneficiary.BeneID

        End Function

        Public Shared Sub AddBeneProductType(ByVal objBene As ICBene, ByVal ProductTypeCode As String)
            'Dim objBeneProductType As New ICBeneProductType
            'objBeneProductType.BeneficiaryCode = objBene.BeneficiaryCode
            'objBeneProductType.ProductTypeCode = ProductTypeCode
            'objBeneProductType.BeneID = objBene.BeneID
            'objBeneProductType.CreatedBy = objBene.CreateBy
            'objBeneProductType.CreatedDate = objBene.CreateDate
            'objBeneProductType.Save()
        End Sub

        Public Shared Sub AddBeneProductTypefromform(ByVal objBene As ICBene, ByVal ProductTypeCode As String)
            Dim objBeneProductType As New ICBeneProductType
            objBeneProductType.BeneficiaryCode = objBene.BeneficiaryCode
            objBeneProductType.ProductTypeCode = ProductTypeCode
            objBeneProductType.BeneID = objBene.BeneID
            objBeneProductType.CreatedBy = objBene.CreateBy
            objBeneProductType.CreatedDate = objBene.CreateDate
            objBeneProductType.Save()
        End Sub

        Public Shared Sub DeleteBeneProductType(ByVal BeneID As String, ByVal UserID As String, ByVal UserName As String)
            Dim collBeneProductType As New ICBeneProductTypeCollection
            Dim objBeneProductType As ICBeneProductType
            Dim ActionString As String = Nothing
            collBeneProductType.Query.Where(collBeneProductType.Query.BeneID = BeneID)
            If collBeneProductType.Query.Load() Then
                ActionString = "Products for Beneficiary [" & collBeneProductType(0).UpToICBeneByBeneID.BeneficiaryCode & "][ " & collBeneProductType(0).UpToICBeneByBeneID.BeneficiaryName & " ]"

                For Each obj As ICBeneProductType In collBeneProductType
                    objBeneProductType = New ICBeneProductType
                    If objBeneProductType.LoadByPrimaryKey(obj.BenePTID) Then
                        ActionString += "[ " & objBeneProductType.ProductTypeCode & " ]"
                        objBeneProductType.MarkAsDeleted()
                        objBeneProductType.Save()
                    End If
                Next
                ActionString += " deleted. Action was taken by user [ " & UserID & " ][ " & UserName & " ]."
                ICUtilities.AddAuditTrail(ActionString, "Beneficiary Management", BeneID, UserID, UserName, "DELETE")
            End If
        End Sub

        Public Shared Function GetAlltaggedBeneProductTypebyBeneID(ByVal BeneID As String) As DataTable
            Dim collBeneProductType As New ICBeneProductTypeCollection
            collBeneProductType.Query.Where(collBeneProductType.Query.BeneID = BeneID)
            If collBeneProductType.Query.Load() Then
                Return collBeneProductType.Query.LoadDataTable()
            End If
        End Function

        Public Shared Sub GetgvBene(ByVal BeneCode As String, ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean, ByVal BeneName As String)
            Dim qryBene As New ICBeneQuery("qryBene")
            Dim qryPN As New ICPaymentNatureQuery("qryPN")
            Dim qryBeneGroup As New ICBeneGroupQuery("qryBeneGroup")

            If Not pagenumber = 0 Then
                qryBene.Select(qryBene.BeneID, qryBene.BeneficiaryCode, qryBene.BeneficiaryName, qryPN.PaymentNatureName, qryBene.BeneficiaryAddedVia)
                qryBene.Select(qryBene.IsActive, qryBene.IsApproved, qryBene.ApprovalforDeletionStatus, qryBene.BeneficiaryStatus, qryBeneGroup.BeneGroupName)
                qryBene.InnerJoin(qryPN).On(qryBene.PaymentNatureCode = qryPN.PaymentNatureCode)
                qryBene.InnerJoin(qryBeneGroup).On(qryBene.BeneGroupCode = qryBeneGroup.BeneGroupCode)
                'qryBene.Where(qryBene.ApprovalforDeletionStatus = False Or qryBene.ApprovalforDeletionStatus.IsNull)
                If BeneCode <> "0" Then
                    qryBene.Where(qryBene.BeneGroupCode = BeneCode)
                End If
                If BeneName <> "" Then
                    qryBene.Where(qryBene.BeneficiaryName.Cast(EntitySpaces.DynamicQuery.esCastType.String).ToLower.Like("%" & BeneName.ToLower & "%"))
                End If
                qryBene.OrderBy(qryBene.BeneID.Descending)
                rg.DataSource = qryBene.LoadDataTable()

                If DoDataBind Then
                    rg.DataBind()
                End If
            End If
        End Sub

        Public Shared Sub GetgvBeneDelete(ByVal pagenumber As Integer, ByVal pagesize As Integer, ByVal rg As RadGrid, ByVal DoDataBind As Boolean)
            Dim qryBene As New ICBeneQuery("qryBene")
            Dim qryPN As New ICPaymentNatureQuery("qryPN")
            If Not pagenumber = 0 Then
                qryBene.Select(qryBene.BeneID, qryBene.BeneficiaryCode, qryBene.BeneficiaryName, qryPN.PaymentNatureName, qryBene.IsActive, qryBene.IsApproved, qryBene.ApprovalforDeletionStatus)
                qryBene.Select(qryBene.BeneficiaryAddedVia, qryBene.BeneficiaryStatus)
                qryBene.InnerJoin(qryPN).On(qryBene.PaymentNatureCode = qryPN.PaymentNatureCode)
                qryBene.Where(qryBene.ApprovalforDeletionStatus = True)
                qryBene.OrderBy(qryBene.BeneficiaryName.Ascending)
                rg.DataSource = qryBene.LoadDataTable()

                If DoDataBind Then
                    rg.DataBind()
                End If
                'Else
                '    qryBene.Query.es.PageNumber = 1
                '    qryBene.Query.es.PageSize = pagesize
                '    qryBene.Query.OrderBy(qryBene.Query.BeneficiaryName.Ascending)
                '    qryBene.Query.Load()
                '    rg.DataSource = qryBene
                '    If DoDataBind Then
                '        rg.DataBind()
                '    End If
            End If
        End Sub

        Public Shared Sub DeleteBene(ByVal BeneID As String, ByVal UserID As String, ByVal UserName As String)
            Dim objBene As New ICBene
            Dim CurrentAt As String = ""
            DeleteBeneProductType(BeneID, UserID, UserName)
            If objBene.LoadByPrimaryKey(BeneID) Then
                CurrentAt = "Beneficiary : [Group Code:  " & objBene.BeneGroupCode.ToString() & " ; Name:  " & objBene.BeneficiaryName.ToString() & "]"
                objBene.MarkAsDeleted()
                objBene.Save()
                ICUtilities.AddAuditTrail(CurrentAt & " Deleted ", "Beneficiary Management", BeneID.ToString(), UserID.ToString(), UserName.ToString(), "DELETE")
            End If
        End Sub

        Public Shared Sub ApprovedBeneforDeleteApproval(ByVal BeneID As String, ByVal UserID As String, ByVal UserName As String)
            Dim objBene As New ICBene
            Dim CurrentAt As String = ""

            If objBene.LoadByPrimaryKey(BeneID) Then
                CurrentAt = "Beneficiary : [Name:  " & objBene.BeneficiaryName.ToString() & "]"
                objBene.ApprovalByforDeletion = UserID
                objBene.ApprovalforDeletionStatus = True
                objBene.Save()
                ICUtilities.AddAuditTrail(CurrentAt & " Delete Approval ", "Beneficiary Management", BeneID.ToString(), UserID.ToString(), UserName.ToString(), "DELETE APPROVAL")
            End If
        End Sub

        Public Shared Sub RejectBeneforDeleteApproval(ByVal BeneID As String, ByVal UserID As String, ByVal UserName As String)
            Dim objBene As New ICBene
            Dim CurrentAt As String = ""

            If objBene.LoadByPrimaryKey(BeneID) Then
                CurrentAt = "Beneficiary : [Name:  " & objBene.BeneficiaryName.ToString() & "]"
                objBene.ApprovalByforDeletion = Nothing
                objBene.ApprovalforDeletionStatus = False
                objBene.Save()
                ICUtilities.AddAuditTrail(CurrentAt & " Reject Delete Approval ", "Beneficiary Management", BeneID.ToString(), UserID.ToString(), UserName.ToString(), "REJECT DELETE APPROVAL")
            End If
        End Sub

        Public Shared Sub ApproveBene(ByVal BeneID As String, ByVal IsApproved As Boolean, ByVal UserID As String, ByVal UserName As String)
            Dim objBene As New ICBene
            Dim CurrentAt As String

            If objBene.LoadByPrimaryKey(BeneID) Then
                objBene.IsApproved = IsApproved
                objBene.ApprovedBy = UserID
                objBene.ApprovedOn = Date.Now
                objBene.BeneficiaryStatus = "Approved"
                objBene.Save()
                CurrentAt = "Beneficiary : [Group Code:  " & objBene.BeneGroupCode.ToString() & " ; Name:  " & objBene.BeneficiaryName.ToString() & "]"
                ICUtilities.AddAuditTrail(CurrentAt & " Approved ", "Beneficiary Management", BeneID.ToString(), UserID.ToString(), UserName.ToString(), "APPROVE")
            End If
        End Sub

        Public Shared Function GetBeneDetailsForExport() As DataTable
            Dim collBene As New ICBeneQuery("collBene")
            Dim objICUBPSComp As New ICUBPSCompanyQuery("objICUBPSComp")
            Dim objCompany As New ICCompanyQuery("objCompany")
            Dim objGroup As New ICGroupQuery("objGroup")
            Dim objCountry As New ICCountryQuery("objCountry")
            Dim objProvince As New ICProvinceQuery("objProvince")
            Dim objCity As New ICCityQuery("objCity")

            Dim dt As New DataTable
            collBene.Select(collBene.BeneID, collBene.BeneficiaryCode, objCompany.CompanyName, objGroup.GroupName, collBene.BeneficiaryName, collBene.BeneGroupCode, collBene.PaymentNatureCode, collBene.BeneficiaryAccountNo, collBene.BeneficiaryAccountTitle, collBene.BeneAccountBranchCode, collBene.BeneAccountCurrency, collBene.BeneficiaryBankCode, collBene.BeneficiaryBankName, collBene.BeneficiaryBankAddress)
            collBene.Select(collBene.BeneficiaryBranchCode, collBene.BeneficiaryBranchName, collBene.BeneficiaryBranchAddress, collBene.BeneficiaryMobile, collBene.BeneficiaryIDType, collBene.BeneficiaryIDNo, collBene.BeneficiaryEmail, collBene.BeneficiaryPhone, collBene.BeneficiaryAddress, objCountry.CountryName.As("Beneficiary Country"), objProvince.ProvinceName.As("Beneficiary Province"), objCity.CityName.As("Beneficiary City"), collBene.IsActive, collBene.IsApproved, collBene.ApprovalforDeletionStatus)
            collBene.Select(collBene.BeneficiaryAddedVia, collBene.BeneficiaryIBFTAccountNo, collBene.BeneficiaryStatus)
            collBene.Select(objICUBPSComp.CompanyName.As("UBPS Company Name"), collBene.UBPReferenceNo)
            collBene.LeftJoin(objICUBPSComp).On(collBene.UBPCompanyID = objICUBPSComp.CompanyCode)
            collBene.LeftJoin(objCompany).On(collBene.CompanyCode = objCompany.CompanyCode)
            collBene.LeftJoin(objGroup).On(objCompany.GroupCode = objGroup.GroupCode)
            collBene.LeftJoin(objCountry).On(collBene.BeneficiaryCountry = objCountry.CountryCode)
            collBene.LeftJoin(objProvince).on(collBene.BeneficiaryProvince = objProvince.ProvinceCode)
            collBene.LeftJoin(objCity).on(collBene.BeneficiaryCity = objCity.CityCode)
            collBene.OrderBy(collBene.BeneID.Ascending)
            dt = collBene.LoadDataTable
            Return dt
        End Function

        Public Shared Function GetAllActiveBene() As ICBeneCollection
            Dim collBene As New ICBeneCollection
            collBene.es.Connection.CommandTimeout = 3600
            collBene.Query.Where(collBene.Query.IsActive = True)
            collBene.Query.OrderBy(collBene.Query.BeneficiaryName.Ascending)
            collBene.Query.Load()
            Return collBene
        End Function

        Public Shared Function GetAllActiveandApprovedBeneByBeneGroupCode(ByVal BeneGroupCode As String) As ICBeneCollection
            Dim collBene As New ICBeneCollection
            collBene.Query.Where(collBene.Query.IsActive = True And collBene.Query.IsApproved = True)
            collBene.Query.Where(collBene.Query.BeneGroupCode = BeneGroupCode)
            collBene.Query.OrderBy(collBene.Query.BeneficiaryName.Ascending)
            collBene.Query.Load()
            Return collBene
        End Function
        Public Shared Function CheckDuplicateBeneForSingleInput(ByVal ICBene As ICBene, ByVal CompanyCode As String) As String
            Dim rslt As String = "OK"
            Dim collBene As ICBeneCollection

            If ICBene.BeneID Is Nothing Then
                collBene = New ICBeneCollection
                If Not ICBene.BeneficiaryCode Is Nothing Then
                    collBene.Query.Where(collBene.Query.BeneficiaryCode.ToLower.Trim = ICBene.BeneficiaryCode.ToLower.Trim And collBene.Query.IsApproved = True And collBene.Query.IsActive = True)
                    collBene.Query.Where(collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary Code"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryAccountNo <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryAccountNo.Trim = ICBene.BeneficiaryAccountNo.Trim And collBene.Query.IsApproved = True And collBene.Query.IsActive = True)
                    collBene.Query.Where(collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary FT Account Number"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryIBFTAccountNo <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryIBFTAccountNo.Trim = ICBene.BeneficiaryIBFTAccountNo.Trim And collBene.Query.IsApproved = True And collBene.Query.IsActive = True)
                    collBene.Query.Where(collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary IBFT Account Number"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryCNIC <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryCNIC.Trim = ICBene.BeneficiaryCNIC.Trim And collBene.Query.BeneficiaryCNIC.Trim <> "0" And collBene.Query.IsApproved = True And collBene.Query.IsActive = True)
                    collBene.Query.Where(collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary CNIC"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.UBPReferenceNo <> "" Then
                    collBene.Query.Where(collBene.Query.UBPReferenceNo.Trim = ICBene.UBPReferenceNo.Trim And collBene.Query.UBPCompanyID = ICBene.UBPCompanyID And collBene.Query.IsApproved = True And collBene.Query.IsActive = True)
                    collBene.Query.Where(collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "UBP Reference No"
                    End If
                End If
            Else
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryCode <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryCode.ToLower.Trim = ICBene.BeneficiaryCode.ToLower.Trim And collBene.Query.BeneID <> ICBene.BeneID And collBene.Query.IsApproved = True And collBene.Query.IsActive = True)
                    collBene.Query.Where(collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary Code"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryAccountNo <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryAccountNo.Trim = ICBene.BeneficiaryAccountNo.Trim And collBene.Query.BeneID <> ICBene.BeneID And collBene.Query.IsApproved = True And collBene.Query.IsActive = True)
                    collBene.Query.Where(collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary FT Account Number"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryIBFTAccountNo <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryIBFTAccountNo.Trim = ICBene.BeneficiaryIBFTAccountNo.Trim And collBene.Query.BeneID <> ICBene.BeneID And collBene.Query.IsApproved = True And collBene.Query.IsActive = True)
                    collBene.Query.Where(collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary IBFT Account Number"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryCNIC <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryCNIC.Trim = ICBene.BeneficiaryCNIC.Trim And collBene.Query.BeneID <> ICBene.BeneID And collBene.Query.BeneficiaryCNIC.Trim <> "0" And collBene.Query.IsApproved = True And collBene.Query.IsActive = True)
                    collBene.Query.Where(collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary CNIC"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.UBPReferenceNo <> "" Then
                    collBene.Query.Where(collBene.Query.UBPReferenceNo.Trim = ICBene.UBPReferenceNo.Trim And collBene.Query.BeneID <> ICBene.BeneID And collBene.Query.UBPCompanyID = ICBene.UBPCompanyID And collBene.Query.IsApproved = True And collBene.Query.IsActive = True)
                    collBene.Query.Where(collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "UBP Reference No"
                    End If
                End If
            End If
            Return rslt
        End Function
        Public Shared Function CheckDuplicateBene(ByVal ICBene As ICBene, ByVal CompanyCode As String) As String
            Dim rslt As String = "OK"
            Dim collBene As ICBeneCollection

            If ICBene.BeneID Is Nothing Then
                collBene = New ICBeneCollection
                If Not ICBene.BeneficiaryCode Is Nothing Then
                    collBene.Query.Where(collBene.Query.BeneficiaryCode.ToLower.Trim = ICBene.BeneficiaryCode.ToLower.Trim And collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary Code"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryAccountNo <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryAccountNo.Trim = ICBene.BeneficiaryAccountNo.Trim And collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary FT Account Number"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryIBFTAccountNo <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryIBFTAccountNo.Trim = ICBene.BeneficiaryIBFTAccountNo.Trim And collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary IBFT Account Number"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryCNIC <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryCNIC.Trim = ICBene.BeneficiaryCNIC.Trim And collBene.Query.BeneficiaryCNIC.Trim <> "0" And collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary CNIC"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.UBPReferenceNo <> "" Then
                    collBene.Query.Where(collBene.Query.UBPReferenceNo.Trim = ICBene.UBPReferenceNo.Trim And collBene.Query.UBPCompanyID = ICBene.UBPCompanyID And collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "UBP Reference No"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryIDNo <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryIDNo.Trim = ICBene.BeneficiaryIDNo.Trim And collBene.Query.BeneficiaryIDType = ICBene.BeneficiaryIDType And collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary ID No"
                    End If
                End If
            Else
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryCode <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryCode.ToLower.Trim = ICBene.BeneficiaryCode.ToLower.Trim And collBene.Query.BeneID <> ICBene.BeneID And collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary Code"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryAccountNo <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryAccountNo.Trim = ICBene.BeneficiaryAccountNo.Trim And collBene.Query.BeneID <> ICBene.BeneID And collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary FT Account Number"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryIBFTAccountNo <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryIBFTAccountNo.Trim = ICBene.BeneficiaryIBFTAccountNo.Trim And collBene.Query.BeneID <> ICBene.BeneID And collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary IBFT Account Number"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryCNIC <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryCNIC.Trim = ICBene.BeneficiaryCNIC.Trim And collBene.Query.BeneID <> ICBene.BeneID And collBene.Query.BeneficiaryCNIC.Trim <> "0" And collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary CNIC"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.UBPReferenceNo <> "" Then
                    collBene.Query.Where(collBene.Query.UBPReferenceNo.Trim = ICBene.UBPReferenceNo.Trim And collBene.Query.BeneID <> ICBene.BeneID And collBene.Query.UBPCompanyID = ICBene.UBPCompanyID And collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "UBP Reference No"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryIDNo <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryIDNo.Trim = ICBene.BeneficiaryIDNo.Trim And collBene.Query.BeneID <> ICBene.BeneID And collBene.Query.BeneficiaryIDType = ICBene.BeneficiaryIDType And collBene.Query.CompanyCode = CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary ID No"
                    End If
                End If
            End If
            Return rslt
        End Function
        Public Shared Function CheckDuplicateBeneForAdd(ByVal ICBene As ICBene) As String
            Dim rslt As String = "OK"
            Dim collBene As ICBeneCollection

            If ICBene.BeneID Is Nothing Then
                collBene = New ICBeneCollection
                If Not ICBene.BeneficiaryCode Is Nothing Then
                    collBene.Query.Where(collBene.Query.BeneficiaryCode.ToLower.Trim = ICBene.BeneficiaryCode.ToLower.Trim And collBene.Query.CompanyCode = ICBene.CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary Code"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryAccountNo <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryAccountNo.Trim = ICBene.BeneficiaryAccountNo.Trim And collBene.Query.CompanyCode = ICBene.CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary FT Account Number"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryIBFTAccountNo <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryIBFTAccountNo.Trim = ICBene.BeneficiaryIBFTAccountNo.Trim And collBene.Query.CompanyCode = ICBene.CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary IBFT Account Number"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryCNIC <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryCNIC.Trim = ICBene.BeneficiaryCNIC.Trim And collBene.Query.BeneficiaryCNIC.Trim <> "0" And collBene.Query.CompanyCode = ICBene.CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary CNIC"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.UBPReferenceNo <> "" Then
                    collBene.Query.Where(collBene.Query.UBPReferenceNo.Trim = ICBene.UBPReferenceNo.Trim And collBene.Query.UBPCompanyID = ICBene.UBPCompanyID And collBene.Query.CompanyCode = ICBene.CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "UBP Reference No"
                    End If
                End If
            Else
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryCode <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryCode.ToLower.Trim = ICBene.BeneficiaryCode.ToLower.Trim And collBene.Query.BeneID <> ICBene.BeneID And collBene.Query.CompanyCode = ICBene.CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary Code"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryAccountNo <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryAccountNo.Trim = ICBene.BeneficiaryAccountNo.Trim And collBene.Query.BeneID <> ICBene.BeneID And collBene.Query.CompanyCode = ICBene.CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary FT Account Number"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryIBFTAccountNo <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryIBFTAccountNo.Trim = ICBene.BeneficiaryIBFTAccountNo.Trim And collBene.Query.BeneID <> ICBene.BeneID And collBene.Query.CompanyCode = ICBene.CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary IBFT Account Number"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.BeneficiaryCNIC <> "" Then
                    collBene.Query.Where(collBene.Query.BeneficiaryCNIC.Trim = ICBene.BeneficiaryCNIC.Trim And collBene.Query.BeneID <> ICBene.BeneID And collBene.Query.BeneficiaryCNIC.Trim <> "0" And collBene.Query.CompanyCode = ICBene.CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "Beneficiary CNIC"
                    End If
                End If
                collBene = New ICBeneCollection
                If ICBene.UBPReferenceNo <> "" Then
                    collBene.Query.Where(collBene.Query.UBPReferenceNo.Trim = ICBene.UBPReferenceNo.Trim And collBene.Query.BeneID <> ICBene.BeneID And collBene.Query.UBPCompanyID = ICBene.UBPCompanyID And collBene.Query.CompanyCode = ICBene.CompanyCode)
                    If collBene.Query.Load() Then
                        rslt = "UBP Reference No"
                    End If
                End If
            End If
            Return rslt
        End Function
        Public Shared Function IsBeneExistByBeneCodeForBulkUpload() As DataTable
            Dim collBene As New ICBeneCollection
            'collBene.Query.Select(collBene.Query.BeneficiaryCode)
            collBene.Query.Where(collBene.Query.IsActive = True And collBene.Query.IsApproved = True)
            collBene.Query.OrderBy(collBene.Query.BeneficiaryCode.Ascending)
            Return collBene.Query.LoadDataTable()
        End Function

        Public Shared Function GetBeneCodeandBeneIDByBeneAccNoandCNIC(ByVal DisbursementMode As String, ByVal Value As String, ByVal CompanyCode As String) As String
            Dim collBene As New ICBeneCollection
            Dim BeneCode As String = Nothing
            If DisbursementMode = "COTC" Then
                collBene.Query.Where(collBene.Query.BeneficiaryIDNo = Value.Split("-")(0) And collBene.Query.BeneficiaryIDType = Value.Split("-")(1) And collBene.Query.IsActive = True And collBene.Query.IsApproved = True And collBene.Query.CompanyCode = CompanyCode)
            ElseIf DisbursementMode = "Other Credit" Then
                collBene.Query.Where(collBene.Query.BeneficiaryIBFTAccountNo = Value And collBene.Query.IsActive = True And collBene.Query.IsApproved = True And collBene.Query.CompanyCode = CompanyCode)
            ElseIf DisbursementMode = "Direct Credit" Then
                collBene.Query.Where(collBene.Query.BeneficiaryAccountNo = Value And collBene.Query.IsActive = True And collBene.Query.IsApproved = True And collBene.Query.CompanyCode = CompanyCode)
            ElseIf DisbursementMode = "Bill Payment" Then
                collBene.Query.Where(collBene.Query.UBPCompanyID = Value.Split("-")(0) And collBene.Query.UBPReferenceNo = Value.Split("-")(1) And collBene.Query.IsActive = True And collBene.Query.IsApproved = True And collBene.Query.CompanyCode = CompanyCode)
            ElseIf DisbursementMode = "Cheque" Or DisbursementMode = "PO" Or DisbursementMode = "DD" Then
                collBene.Query.Where(collBene.Query.BeneficiaryCode = Value And collBene.Query.IsActive = True And collBene.Query.IsApproved = True And collBene.Query.CompanyCode = CompanyCode)
            End If

            If collBene.Query.Load() Then
                BeneCode = collBene(0).BeneficiaryCode & "-" & collBene(0).BeneID
            End If
            Return BeneCode
        End Function

        Public Shared Function GetBeneIDbyBeneCode(ByVal BeneCode As String) As String
            Dim objBene As New ICBeneCollection
            Dim beneId As String = ""
            objBene.Query.Where(objBene.Query.BeneficiaryCode = BeneCode)
            If objBene.Query.Load() Then
                beneId = objBene(0).BeneID
            End If
            Return beneId
        End Function
        Public Shared Function GetBeneIDbyBeneFields(ByVal FieldName As String, ByVal FieldValue As String, ByVal CompanyCode As String, Optional BeneIDType As String = "") As String
            Dim objBene As New ICBene
            Dim beneId As String = ""
            If FieldName = "BeneficiaryCode" Then
                objBene.Query.Where(objBene.Query.BeneficiaryCode = FieldValue And objBene.Query.CompanyCode = CompanyCode)
                objBene.Query.Where(objBene.Query.IsActive = True And objBene.Query.IsApproved = True)
                If objBene.Query.Load() Then
                    beneId = objBene.BeneID
                End If
            ElseIf FieldName = "BeneficiaryIBFTAccountNo" Then
                objBene.Query.Where(objBene.Query.BeneficiaryIBFTAccountNo = FieldValue And objBene.Query.CompanyCode = CompanyCode)
                objBene.Query.Where(objBene.Query.IsActive = True And objBene.Query.IsApproved = True)
                If objBene.Query.Load() Then
                    beneId = objBene.BeneID
                End If
            ElseIf FieldName = "BeneficiaryAccountNo" Then
                objBene.Query.Where(objBene.Query.BeneficiaryAccountNo = FieldValue And objBene.Query.CompanyCode = CompanyCode)
                objBene.Query.Where(objBene.Query.IsActive = True And objBene.Query.IsApproved = True)
                If objBene.Query.Load() Then
                    beneId = objBene.BeneID
                End If
            ElseIf (FieldName = "BeneficiaryIdentificationNo" Or FieldName = "BeneficiaryIdentificationType") And (BeneIDType <> "" Or Not BeneIDType Is Nothing) Then
                objBene.Query.Where(objBene.Query.BeneficiaryIDNo = FieldValue And objBene.Query.CompanyCode = CompanyCode)
                objBene.Query.Where(objBene.Query.BeneficiaryIDType = BeneIDType)
                objBene.Query.Where(objBene.Query.IsActive = True And objBene.Query.IsApproved = True)
                If objBene.Query.Load() Then
                    beneId = objBene.BeneID
                End If
            ElseIf FieldName = "UBP Reference No" Then
                objBene.Query.Where(objBene.Query.UBPCompanyID = FieldValue.Split("-")(0) And objBene.Query.UBPReferenceNo = FieldValue.Split("-")(1))
                objBene.Query.Where(objBene.Query.IsActive = True And objBene.Query.IsApproved = True And objBene.Query.CompanyCode = CompanyCode)
                If objBene.Query.Load() Then
                    beneId = objBene.BeneID
                End If
            End If
            Return beneId
        End Function
        Public Shared Function GetAllActiveandApprovedBeneByBeneGroupCodeandTaggedProductType(ByVal BeneGroupCode As String, ByVal ProductTypeCode As String) As DataTable
            Dim qryBene As New ICBeneQuery("qryBene")
            Dim qryBeneProductType As New ICBeneProductTypeQuery("qryBeneProductType")
            qryBene.InnerJoin(qryBeneProductType).On(qryBene.BeneID = qryBeneProductType.BeneID)
            qryBene.Where(qryBene.IsActive = True And qryBene.IsApproved = True)
            qryBene.Where(qryBene.BeneGroupCode = BeneGroupCode)
            qryBene.Where(qryBeneProductType.ProductTypeCode = ProductTypeCode)
            qryBene.OrderBy(qryBene.BeneficiaryName.Ascending)
            Return qryBene.LoadDataTable()
        End Function
        Public Shared Function GetActiveandApproveBene() As DataTable
            Dim collBene As New ICBeneCollection
            collBene.Query.Where(collBene.Query.IsActive = True And collBene.Query.IsApproved = True)
            collBene.Query.OrderBy(collBene.Query.BeneficiaryCode.Ascending)
            Return collBene.Query.LoadDataTable()
        End Function
        Public Shared Function GetBeneTaggedWithGroupCompanyandProductType(ByVal GroupCode As String, ByVal CompanyCode As String) As DataTable
            Dim qryBene As New ICBeneQuery("qryBene")
            Dim qryComp As New ICCompanyQuery("qryComp")
            Dim qryGroup As New ICGroupQuery("qryGroup")
            qryBene.InnerJoin(qryComp).On(qryBene.CompanyCode = qryComp.CompanyCode)
            qryBene.InnerJoin(qryGroup).On(qryComp.GroupCode = qryGroup.GroupCode)
            qryBene.Where(qryGroup.GroupCode = GroupCode)
            qryBene.Where(qryBene.CompanyCode = CompanyCode)
            qryBene.Where(qryBene.IsActive = True And qryBene.IsApproved = True)
            qryBene.OrderBy(qryBene.BeneficiaryCode.Ascending)
            Return qryBene.LoadDataTable()
        End Function
        Public Shared Function GetBeneTaggedWithProductType() As DataTable
            Dim qryBene As New ICBeneQuery("qryBene")
            Dim qryBenePT As New ICBeneProductTypeQuery("qryBenePT")
            qryBenePT.InnerJoin(qryBene).On(qryBenePT.BeneID = qryBene.BeneID)
            qryBenePT.Where(qryBene.IsActive = True And qryBene.IsApproved = True)
            qryBenePT.OrderBy(qryBenePT.BenePTID.Ascending)
            Return qryBenePT.LoadDataTable()
        End Function
        Public Shared Function IsBeneActiveAndApproveByBeneCode(ByVal BeneCode As String) As Boolean
            Dim qryObjICBene As New ICBeneQuery("qryObjICBene")

            qryObjICBene.Where(qryObjICBene.IsActive = True And qryObjICBene.IsApproved = True And qryObjICBene.BeneficiaryCode = BeneCode)


            If qryObjICBene.LoadDataTable.Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Function
        Public Shared Function IsBeneTaggedByBeneCodeGroupAndCompanyCode(ByVal BeneCode As String, ByVal CompanyCode As String, ByVal GroupCode As String) As Boolean
            Dim qryObjICBene As New ICBeneQuery("qryObjICBene")
            Dim qryObjICCompany As New ICCompanyQuery("qryObjICCompany")

            qryObjICBene.InnerJoin(qryObjICCompany).On(qryObjICBene.CompanyCode = qryObjICCompany.CompanyCode)
            qryObjICBene.Where(qryObjICBene.IsActive = True And qryObjICBene.IsApproved = True And qryObjICBene.BeneficiaryCode = BeneCode)
            qryObjICBene.Where(qryObjICBene.CompanyCode = CompanyCode And qryObjICCompany.GroupCode = GroupCode)

            If qryObjICBene.LoadDataTable.Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Function
        Public Shared Function IsBeneTaggedWithProductByBeneCodeAndCompanyCode(ByVal BeneCode As String, ByVal CompanyCode As String, ByVal ProductCode As String) As Boolean
            Dim qryObjICBene As New ICBeneQuery("qryObjICBene")
            Dim qryObjICBeneProd As New ICBeneProductTypeQuery("qryObjICBeneProd")

            qryObjICBene.InnerJoin(qryObjICBeneProd).On(qryObjICBene.BeneficiaryCode = qryObjICBeneProd.BeneficiaryCode)
            qryObjICBene.Where(qryObjICBene.IsActive = True And qryObjICBene.IsApproved = True And qryObjICBene.BeneficiaryCode = BeneCode)
            qryObjICBene.Where(qryObjICBene.CompanyCode = CompanyCode And qryObjICBeneProd.ProductTypeCode = ProductCode)

            If qryObjICBene.LoadDataTable.Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function GetBeneByBeneCode(ByVal BeneCode As String) As ICBene

        End Function


    End Class
End Namespace

