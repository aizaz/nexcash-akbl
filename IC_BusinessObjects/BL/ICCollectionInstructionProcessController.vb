﻿Imports ICBO
Imports ICBO.IC
Imports DotNetNuke.Security.Roles
Imports DotNetNuke.Entities.Portals
Imports System
Imports System.IO
Imports System.Text
Imports System.Data
Imports System.Data.OleDb
Imports Telerik.Web.UI
Imports Microsoft.VisualBasic
Imports Microsoft.Exchange.WebServices.Data
Imports System.Net
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports FtpLib
Namespace IC
    Public Class ICCollectionInstructionProcessController
        Private Shared Function ValidateRemoteCertificate(ByVal sender As Object, ByVal certificate As X509Certificate, ByVal chain As X509Chain, ByVal policyerrors As SslPolicyErrors) As Boolean
            Return True
        End Function
        Public Shared Sub AutoProcessInvoiceDBFiles(ByVal AcqMode As String, ByVal FileType As String)
            Try
                Dim collFiles As New ICFilesCollection
                Dim objFile As New ICFiles

                collFiles = ICFilesController.GetPendingFilesForProcessingViaAcqModeAndFileType(AcqMode.ToString(), "Invoice DB File")
                If collFiles.Count > 0 Then
                    For Each objFile In collFiles
                        'ReadInstructionsInFile(objFile.FileID)
                        Try
                            ICFilesController.AsynChronusMethodForParseAndSaveCollectionInstructions(objFile.CreatedBy.ToString, objFile.FileID.ToString, AcqMode)
                        Catch ex As Exception
                            Dim objICFiles As New ICFiles
                            objICFiles.LoadByPrimaryKey(objFile.FileID.ToString)
                            objICFiles.Status = "Pending Read"
                            objICFiles.ProcessDate = Nothing
                            objICFiles.TotalAmount = Nothing
                            objICFiles.TransactionCount = Nothing
                            objICFiles.ProcessBy = Nothing
                            Dim ActionStr As String = Nothing
                            ActionStr = "Client Invoice DB File [ " & objICFiles.OriginalFileName & " ] of Client [ " & objICFiles.UpToICCompanyByCompanyCode.CompanyName & " ] status updated from"
                            ActionStr += " [ " & objICFiles.Status & " ] to [ Pending Read ] while parsing faild VIA auto process."
                            ICFilesController.UpdateClientFile(objICFiles, Nothing, Nothing, ActionStr, "Invoice DB File Upload")
                            'ICInstructionController.DeleteInstructionByFileIDInError(objFile.FileID.ToString, Nothing, Nothing)
                            Continue For
                        End Try
                    Next
                End If
            Catch ex As Exception
                ICUtilities.UpdateErrorLog(ex.Message, "EmailInvoiceDBFileProcess")
            End Try
        End Sub
        Public Shared Sub AutoReadCollectionEmailsAndFileUpload()

            Dim MIMEType As String = ""
            Dim FileName As String = ""
            Dim chkCorrectFile As Boolean = False
            Dim IsFileRead As Boolean = False
            Dim dtEmailSettings As New DataTable
            Dim drEmailSettings As DataRow
            Dim UserID, UserEmail, TemplateID, CollectionNatureCode, CompanyCode, CompanyName, GroupCode, GroupName, EmailUserName As String


            Dim Action, AuditType As String
            Dim objICFile As New ICFiles

            Dim objICFileUploadTemplate As New ICCollectionInvoiceDataTemplate
            Dim CheckResult As String = ""
            Dim FileLocation As String = ""
            Dim objICUser As ICUser
            Dim FailureReason As String = ""

            FileLocation = ICUtilities.GetSettingValue("PhysicalApplicationPath").ToString() & "UploadedFiles\"


            Action = ""
            AuditType = ""
            UserID = ""
            UserEmail = ""
            TemplateID = ""
            CollectionNatureCode = ""
            CompanyCode = ""
            CompanyName = ""
            GroupCode = ""
            GroupName = ""
            EmailUserName = ""
            Try
                Dim NoOfFiles As Integer = 0
                Dim UserName, Password, Server, Domain As String
                Dim Service As New ExchangeService(ExchangeVersion.Exchange2010_SP1)

                UserName = ICUtilities.GetCollectionSettingValue("UserName")
                Password = ICUtilities.GetCollectionSettingValue("Password")
                Server = ICUtilities.GetCollectionSettingValue("Server")
                Domain = ICUtilities.GetCollectionSettingValue("Domain")
                Service.Credentials = New NetworkCredential(UserName, Password, Domain)
                Service.Url = New Uri("https://" & Server & "/EWS/Exchange.asmx")
                ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf ValidateRemoteCertificate)
                Dim results As FindItemsResults(Of Item)
                results = Service.FindItems(WellKnownFolderName.Inbox, New ItemView(10))

                dtEmailSettings = ICCollectionNatureController.GetCollectionNatureEmailSettingsDataTable()
                If dtEmailSettings.Rows.Count > 0 Then
                    For Each Item As EmailMessage In results.Items
                        Item.Load()
                        For Each drEmailSettings In dtEmailSettings.Rows
                            chkCorrectFile = False
                            UserID = drEmailSettings("UserID").ToString()
                            If UserID <> "" Then
                                objICFile = New ICFiles
                                objICFileUploadTemplate = New ICCollectionInvoiceDataTemplate


                                UserEmail = drEmailSettings("Email").ToString()
                                TemplateID = drEmailSettings("TemplateID").ToString()
                                CollectionNatureCode = drEmailSettings("CollectionNatureCode").ToString()

                                CompanyCode = drEmailSettings("CompanyCode").ToString()
                                CompanyName = drEmailSettings("CompanyName").ToString()
                                GroupCode = drEmailSettings("GroupCode").ToString()
                                GroupName = drEmailSettings("GroupName").ToString()
                                EmailUserName = drEmailSettings("UserName").ToString()
                                If Item.Sender.Address.ToString.ToLower.Trim = UserEmail.ToString.ToLower.Trim Then
                                    chkCorrectFile = True
                                End If
                                If chkCorrectFile = True Then
                                    chkCorrectFile = False
                                    NoOfFiles = 0
                                    IsFileRead = False
                                    For Each atta As FileAttachment In Item.Attachments
                                        If atta.Name.ToString().Contains("-") Then
                                            If atta.Name.ToString().Split("-")(1).Trim.ToString().Contains(".") Then
                                                If TemplateID.ToString() = atta.Name.ToString().Split("-")(1).Trim.ToString().Split(".")(0).ToString().Trim Then
                                                    chkCorrectFile = True
                                                End If
                                            Else
                                                If TemplateID.ToString() = atta.Name.ToString().Split("-")(1).Trim.ToString() Then
                                                    chkCorrectFile = True
                                                End If

                                            End If
                                        Else
                                            ICUtilities.UpdateErrorLog("Unable to Upload Invoice DB File by User [ID: " & UserID & " ; Email: " & UserEmail & " ; UserName: " & EmailUserName & "] due to ['-' is missing in file name.]", "EmailInvoiceDBFileProcess")
                                        End If
                                        If chkCorrectFile = True Then
                                            chkCorrectFile = False
                                            'MIMEType = atta.ContentType
                                            MIMEType = System.IO.Path.GetExtension(atta.Name.ToString())
                                            'Check .txt
                                            If MIMEType = ".txt" Then
                                                chkCorrectFile = True
                                            End If
                                            'Check .xls
                                            If MIMEType = ".xls" Then
                                                chkCorrectFile = True
                                            End If
                                            'Check .xlsx
                                            If MIMEType = ".xlsx" Then
                                                chkCorrectFile = True
                                            End If
                                        End If
                                        If chkCorrectFile = True Then

                                            If objICFileUploadTemplate.LoadByPrimaryKey(TemplateID) Then
                                                objICUser = New ICUser
                                                objICUser.LoadByPrimaryKey(UserID)
                                                If objICFileUploadTemplate.TemplateFormat.ToString().ToLower.ToString = ".txt" Then
                                                    If atta.Name.ToString().Split(".")(1).ToString().ToLower.Contains("txt") Or atta.Name.ToString().Split(".")(1).ToString().ToLower.Contains("csv") Then
                                                        FileName = objICFileUploadTemplate.TemplateName.ToString().Replace(" ", "_") & "" & Date.Now.ToString("ddMMyyyyHHmmss") & atta.Name.ToString().Replace(" ", "_")
                                                        ' Save File on HDD
                                                        atta.Load(FileLocation & FileName)
                                                        CheckResult = ICFilesController.CheckCollectionFileBeforeUpload(TemplateID, FileLocation & FileName.ToString())

                                                        If CheckResult.ToString() = "OK" Then
                                                            Action = "Invoice DB File: File [Name: " & atta.Name.ToString() & "] of Group [Code: " & GroupCode & " ; Name: " & GroupName & "], Company [Code: " & CompanyCode & " ; Name: " & CompanyName & "] and Template Name: " & objICFileUploadTemplate.TemplateName & " ; Format : " & objICFileUploadTemplate.TemplateFormat.ToString() & "] is uploaded by User [ID: " & UserID & " ; Name: " & UserName & "]."
                                                            AuditType = "Invoice DB File Upload"
                                                            objICFile.FileID = ICUtilities.UploadFiletoDBViaPath(FileLocation & FileName, "Invoice DB File", "Invoice DB File", FileName, UserID, UserName, Action.ToString(), AuditType.ToString())
                                                            Dim cICFiles As New ICFiles

                                                            cICFiles.LoadByPrimaryKey(objICFile.FileID)
                                                            cICFiles.Status = "Pending Read"
                                                            cICFiles.AcqMode = "Email File Upload"
                                                            cICFiles.FileBatchNo = ICFilesController.GetFileBatchNo("Invoice DB File")
                                                            cICFiles.OriginalFileName = FileName
                                                            cICFiles.FileLocation = FileLocation & FileName.ToString()
                                                            cICFiles.CompanyCode = CompanyCode

                                                            cICFiles.PaymentNatureCode = CollectionNatureCode
                                                            cICFiles.FileUploadTemplateID = TemplateID
                                                            cICFiles.CreatedDate = Date.Now

                                                            Action = "Invoice DB File: File [Name: " & cICFiles.FileName & " ; ID: " & cICFiles.FileID & "] is updated as [Status: " & cICFiles.Status & " ; AcquisitionMode: " & cICFiles.AcqMode & " ; BatchNo: " & cICFiles.FileBatchNo & " ; OriginalFileName: " & cICFiles.OriginalFileName & " ; FileLocation: " & cICFiles.FileLocation & " ; CompanyCode: " & cICFiles.CompanyCode & " ; AccountNumber: " & cICFiles.AccountNumber & " ; BranchCode: " & cICFiles.BranchCode & " ; Currency: " & cICFiles.Currency & " ; Collection Nature: " & cICFiles.PaymentNatureCode & " ; TemplateID: " & cICFiles.FileUploadTemplateID & "] by User [ID: " & UserID & " ; Name: " & UserName & "]."
                                                            AuditType = "Invoice DB File Upload"
                                                            ICFilesController.UpdateClientFile(cICFiles, UserID, UserName, Action.ToString(), AuditType)
                                                        Else
                                                            ' Move file to garbage folder
                                                            Dim Mape As FolderView = New FolderView(Integer.MaxValue)
                                                            Mape.PropertySet = New PropertySet(BasePropertySet.IdOnly)
                                                            Mape.PropertySet.Add(FolderSchema.DisplayName)
                                                            Mape.Traversal = FolderTraversal.Shallow
                                                            Dim fFR As FindFoldersResults = Service.FindFolders(WellKnownFolderName.Inbox, Mape)
                                                            For Each Folder In fFR.Folders
                                                                If Folder.DisplayName.Trim.ToLower.ToString() = "ICGarbage".Trim.ToLower.ToString() Then
                                                                    Item.Move(Folder.Id)
                                                                End If
                                                            Next
                                                            Dim aFileInfo As IO.FileInfo
                                                            aFileInfo = New IO.FileInfo(FileLocation & FileName.ToString())
                                                            'Delete file from HDD.
                                                            If aFileInfo.Exists Then
                                                                aFileInfo.Delete()
                                                            End If
                                                           
                                                            ICUtilities.AddAuditTrail("Unable to Upload File by User [ID: " & UserID & " ; Email: " & UserEmail & " ; UserName: " & EmailUserName & "] due to [" & CheckResult.ToString() & "]", "Email File Not Process", Nothing, UserID, EmailUserName, "ADD")

                                                        End If
                                                    Else
                                                        ' Move file to garbage folder
                                                        Dim Mape As FolderView = New FolderView(Integer.MaxValue)
                                                        Mape.PropertySet = New PropertySet(BasePropertySet.IdOnly)
                                                        Mape.PropertySet.Add(FolderSchema.DisplayName)
                                                        Mape.Traversal = FolderTraversal.Shallow
                                                        Dim fFR As FindFoldersResults = Service.FindFolders(WellKnownFolderName.Inbox, Mape)
                                                        For Each Folder In fFR.Folders
                                                            If Folder.DisplayName.Trim.ToLower.ToString() = "ICGarbage".Trim.ToLower.ToString() Then
                                                                Item.Move(Folder.Id)
                                                            End If
                                                        Next
                                                        ICUtilities.AddAuditTrail("Unable to Upload File by User [ID: " & UserID & " ; Email: " & UserEmail & " ; UserName: " & EmailUserName & "] due to [File Must be in the " & objICFileUploadTemplate.TemplateFormat.ToString() & " or csv format]", "Email Invoice DB File Not Process", Nothing, UserID, EmailUserName, "ADD")
                                                        
                                                    End If
                                                End If
                                                If objICFileUploadTemplate.TemplateFormat.ToString().ToLower.ToString = ".xls" Then
                                                    If atta.Name.ToString().Split(".")(1).ToString().ToLower = "xls" Then
                                                        FileName = objICFileUploadTemplate.TemplateName.ToString().Replace(" ", "_") & "" & Date.Now.ToString("ddMMyyyyHHmmss") & atta.Name.ToString().Replace(" ", "_")
                                                        ' Save File on HDD
                                                        atta.Load(FileLocation & FileName)
                                                       CheckResult = ICFilesController.CheckCollectionFileBeforeUpload(TemplateID, FileLocation & FileName.ToString())

                                                        If CheckResult.ToString() = "OK" Then
                                                            Action = "Invoice DB File: File [Name: " & atta.Name.ToString() & "] of Group [Code: " & GroupCode & " ; Name: " & GroupName & "], Company [Code: " & CompanyCode & " ; Name: " & CompanyName & "] and Template Name: " & objICFileUploadTemplate.TemplateName & " ; Format : " & objICFileUploadTemplate.TemplateFormat.ToString() & "] is uploaded by User [ID: " & UserID & " ; Name: " & UserName & "]."
                                                            AuditType = "Invoice DB File Upload"
                                                            objICFile.FileID = ICUtilities.UploadFiletoDBViaPath(FileLocation & FileName, "Invoice DB File", "Invoice DB File", FileName, UserID, UserName, Action.ToString(), AuditType.ToString())
                                                            Dim cICFiles As New ICFiles

                                                            cICFiles.es.Connection.CommandTimeout = 3600

                                                            cICFiles.LoadByPrimaryKey(objICFile.FileID)
                                                            cICFiles.Status = "Pending Read"
                                                            cICFiles.AcqMode = "Email File Upload"
                                                            cICFiles.FileBatchNo = ICFilesController.GetFileBatchNo("Invoice DB File")
                                                            cICFiles.OriginalFileName = FileName
                                                            cICFiles.FileLocation = FileLocation & FileName.ToString()
                                                            cICFiles.CompanyCode = CompanyCode

                                                            'cICFiles.AccountNumber = AccountNumber
                                                            'cICFiles.BranchCode = BranchCode
                                                            'cICFiles.Currency = Currency
                                                            cICFiles.PaymentNatureCode = CollectionNatureCode
                                                            cICFiles.FileUploadTemplateID = TemplateID
                                                            cICFiles.CreatedDate = Date.Now

                                                            Action = "Invoice DB File: File [Name: " & cICFiles.FileName & " ; ID: " & cICFiles.FileID & "] is updated as [Status: " & cICFiles.Status & " ; AcquisitionMode: " & cICFiles.AcqMode & " ; BatchNo: " & cICFiles.FileBatchNo & " ; OriginalFileName: " & cICFiles.OriginalFileName & " ; FileLocation: " & cICFiles.FileLocation & " ; CompanyCode: " & cICFiles.CompanyCode & " ; AccountNumber: " & cICFiles.AccountNumber & " ; BranchCode: " & cICFiles.BranchCode & " ; Currency: " & cICFiles.Currency & " ; Collection Nature: " & cICFiles.PaymentNatureCode & " ; TemplateID: " & cICFiles.FileUploadTemplateID & "] by User [ID: " & UserID & " ; Name: " & UserName & "]."
                                                            AuditType = "Invoice DB File Upload"
                                                            ICFilesController.UpdateClientFile(cICFiles, UserID, UserName, Action.ToString(), AuditType)

                                                        Else
                                                            Dim aFileInfo As IO.FileInfo
                                                            aFileInfo = New IO.FileInfo(FileLocation & FileName.ToString())
                                                            'Delete file from HDD.
                                                            If aFileInfo.Exists Then
                                                                aFileInfo.Delete()
                                                            End If
                                                            ' Move file to garbage folder
                                                            Dim Mape As FolderView = New FolderView(Integer.MaxValue)
                                                            Mape.PropertySet = New PropertySet(BasePropertySet.IdOnly)
                                                            Mape.PropertySet.Add(FolderSchema.DisplayName)
                                                            Mape.Traversal = FolderTraversal.Shallow
                                                            Dim fFR As FindFoldersResults = Service.FindFolders(WellKnownFolderName.Inbox, Mape)
                                                            For Each Folder In fFR.Folders
                                                                If Folder.DisplayName.Trim.ToLower.ToString() = "ICGarbage".Trim.ToLower.ToString() Then
                                                                    Item.Move(Folder.Id)
                                                                End If
                                                            Next

                                                            ICUtilities.AddAuditTrail("Unable to Upload File by User [ID: " & UserID & " ; Email: " & UserEmail & " ; UserName: " & EmailUserName & "] due to [" & CheckResult.ToString() & "]", "Email Invoice DB File Not Process", Nothing, UserID, EmailUserName, "ERROR")
                                                        End If
                                                    Else
                                                        ' Move file to garbage folder
                                                        Dim Mape As FolderView = New FolderView(Integer.MaxValue)
                                                        Mape.PropertySet = New PropertySet(BasePropertySet.IdOnly)
                                                        Mape.PropertySet.Add(FolderSchema.DisplayName)
                                                        Mape.Traversal = FolderTraversal.Shallow
                                                        Dim fFR As FindFoldersResults = Service.FindFolders(WellKnownFolderName.Inbox, Mape)
                                                        For Each Folder In fFR.Folders
                                                            If Folder.DisplayName.Trim.ToLower.ToString() = "ICGarbage".Trim.ToLower.ToString() Then
                                                                Item.Move(Folder.Id)
                                                            End If
                                                        Next
                                                        ICUtilities.AddAuditTrail("Unable to Upload File by User [ID: " & UserID & " ; Email: " & UserEmail & " ; UserName: " & EmailUserName & "] due to [File Must be in the " & objICFileUploadTemplate.TemplateFormat.ToString() & " format]", "Email Invoice DB File Not Process", Nothing, UserID, EmailUserName, "ERROR")
                                                        
                                                    End If
                                                End If
                                                If objICFileUploadTemplate.TemplateFormat.ToString().ToLower.ToString = ".xlsx" Then
                                                    If atta.Name.ToString().Split(".")(1).ToString().ToLower = "xlsx" Then
                                                        FileName = objICFileUploadTemplate.TemplateName.ToString().Replace(" ", "_") & "" & Date.Now.ToString("ddMMyyyyHHmmss") & atta.Name.ToString().Replace(" ", "_")
                                                        ' Save File on HDD
                                                        atta.Load(FileLocation & FileName)
                                                        CheckResult = ICFilesController.CheckCollectionFileBeforeUpload(TemplateID, FileLocation & FileName.ToString())

                                                        If CheckResult.ToString() = "OK" Then
                                                            Action = "Invoice DB File: File [Name: " & atta.Name.ToString() & "] of Group [Code: " & GroupCode & " ; Name: " & GroupName & "], Company [Code: " & CompanyCode & " ; Name: " & CompanyName & "] and Template Name: " & objICFileUploadTemplate.TemplateName & " ; Format : " & objICFileUploadTemplate.TemplateFormat.ToString() & "] is uploaded by User [ID: " & UserID & " ; Name: " & UserName & "]."
                                                            AuditType = "Invoice DB File Upload"
                                                            objICFile.FileID = ICUtilities.UploadFiletoDBViaPath(FileLocation & FileName, "Invoice DB File", "Invoice DB File", FileName, UserID, UserName, Action.ToString(), AuditType.ToString())
                                                            Dim cICFiles As New ICFiles

                                                            cICFiles.es.Connection.CommandTimeout = 3600

                                                            cICFiles.LoadByPrimaryKey(objICFile.FileID)
                                                            cICFiles.Status = "Pending Read"
                                                            cICFiles.AcqMode = "Email File Upload"
                                                            cICFiles.FileBatchNo = ICFilesController.GetFileBatchNo("Invoice DB File")
                                                            cICFiles.OriginalFileName = FileName
                                                            cICFiles.FileLocation = FileLocation & FileName.ToString()
                                                            cICFiles.CompanyCode = CompanyCode

                                                            'cICFiles.AccountNumber = AccountNumber
                                                            'cICFiles.BranchCode = BranchCode
                                                            'cICFiles.Currency = Currency
                                                            cICFiles.PaymentNatureCode = CollectionNatureCode
                                                            cICFiles.FileUploadTemplateID = TemplateID
                                                            cICFiles.CreatedDate = Date.Now

                                                            Action = "Invoice DB File: File [Name: " & cICFiles.FileName & " ; ID: " & cICFiles.FileID & "] is updated as [Status: " & cICFiles.Status & " ; AcquisitionMode: " & cICFiles.AcqMode & " ; BatchNo: " & cICFiles.FileBatchNo & " ; OriginalFileName: " & cICFiles.OriginalFileName & " ; FileLocation: " & cICFiles.FileLocation & " ; CompanyCode: " & cICFiles.CompanyCode & " ; AccountNumber: " & cICFiles.AccountNumber & " ; BranchCode: " & cICFiles.BranchCode & " ; Currency: " & cICFiles.Currency & " ; Collection Nature: " & cICFiles.PaymentNatureCode & " ; TemplateID: " & cICFiles.FileUploadTemplateID & "] by User [ID: " & UserID & " ; Name: " & UserName & "]."
                                                            AuditType = "Invoice DB File Upload"
                                                            ICFilesController.UpdateClientFile(cICFiles, UserID, UserName, Action.ToString(), AuditType)
                                                        Else
                                                            Dim aFileInfo As IO.FileInfo
                                                            aFileInfo = New IO.FileInfo(FileLocation & FileName.ToString())
                                                            'Delete file from HDD.
                                                            If aFileInfo.Exists Then
                                                                aFileInfo.Delete()
                                                            End If
                                                            ' Move file to garbage folder
                                                            Dim Mape As FolderView = New FolderView(Integer.MaxValue)
                                                            Mape.PropertySet = New PropertySet(BasePropertySet.IdOnly)
                                                            Mape.PropertySet.Add(FolderSchema.DisplayName)
                                                            Mape.Traversal = FolderTraversal.Shallow
                                                            Dim fFR As FindFoldersResults = Service.FindFolders(WellKnownFolderName.Inbox, Mape)
                                                            For Each Folder In fFR.Folders
                                                                If Folder.DisplayName.Trim.ToLower.ToString() = "ICGarbage".Trim.ToLower.ToString() Then
                                                                    Item.Move(Folder.Id)
                                                                End If
                                                            Next

                                                            ICUtilities.AddAuditTrail("Unable to Upload File by User [ID: " & UserID & " ; Email: " & UserEmail & " ; UserName: " & EmailUserName & "] due to [" & CheckResult.ToString() & "]", "Email Invoice DB File Not Process", Nothing, UserID, EmailUserName, "ERROR")
                                                        End If
                                                    Else
                                                        ' Move file to garbage folder
                                                        Dim Mape As FolderView = New FolderView(Integer.MaxValue)
                                                        Mape.PropertySet = New PropertySet(BasePropertySet.IdOnly)
                                                        Mape.PropertySet.Add(FolderSchema.DisplayName)
                                                        Mape.Traversal = FolderTraversal.Shallow
                                                        Dim fFR As FindFoldersResults = Service.FindFolders(WellKnownFolderName.Inbox, Mape)
                                                        For Each Folder In fFR.Folders
                                                            If Folder.DisplayName.Trim.ToLower.ToString() = "ICGarbage".Trim.ToLower.ToString() Then
                                                                Item.Move(Folder.Id)
                                                            End If
                                                        Next
                                                        
                                                        ICUtilities.AddAuditTrail("Unable to Upload File by User [ID: " & UserID & " ; Email: " & UserEmail & " ; UserName: " & EmailUserName & "] due to [File Must be in the " & objICFileUploadTemplate.TemplateFormat.ToString() & " format]", "Email Invoice DB File Not Process", Nothing, UserID, EmailUserName, "ERROR")
                                                    End If
                                                End If
                                                If objICFileUploadTemplate.TemplateFormat.ToString().ToLower.ToString = ".csv" Then
                                                    If atta.Name.ToString().Split(".")(1).ToString().ToLower = "csv" Then
                                                        FileName = objICFileUploadTemplate.TemplateName.ToString().Replace(" ", "_") & "" & Date.Now.ToString("ddMMyyyyHHmmss") & atta.Name.ToString().Replace(" ", "_")
                                                        ' Save File on HDD
                                                        atta.Load(FileLocation & FileName)
                                                         CheckResult = ICFilesController.CheckCollectionFileBeforeUpload(TemplateID, FileLocation & FileName.ToString())

                                                        If CheckResult.ToString() = "OK" Then
                                                            Action = "Invoice DB File: File [Name: " & atta.Name.ToString() & "] of Group [Code: " & GroupCode & " ; Name: " & GroupName & "], Company [Code: " & CompanyCode & " ; Name: " & CompanyName & "] and Template Name: " & objICFileUploadTemplate.TemplateName & " ; Format : " & objICFileUploadTemplate.TemplateFormat.ToString() & "] is uploaded by User [ID: " & UserID & " ; Name: " & UserName & "]."
                                                            AuditType = "Invoice DB File Upload"
                                                            objICFile.FileID = ICUtilities.UploadFiletoDBViaPath(FileLocation & FileName, "Invoice DB File", "Invoice DB File", FileName, UserID, UserName, Action.ToString(), AuditType.ToString())
                                                            Dim cICFiles As New ICFiles

                                                            cICFiles.es.Connection.CommandTimeout = 3600

                                                            cICFiles.LoadByPrimaryKey(objICFile.FileID)
                                                            cICFiles.Status = "Pending Read"
                                                            cICFiles.AcqMode = "Email File Upload"
                                                            cICFiles.FileBatchNo = ICFilesController.GetFileBatchNo("Invoice DB File")
                                                            cICFiles.OriginalFileName = FileName
                                                            cICFiles.FileLocation = FileLocation & FileName.ToString()
                                                            cICFiles.CompanyCode = CompanyCode

                                                            'cICFiles.AccountNumber = AccountNumber
                                                            'cICFiles.BranchCode = BranchCode
                                                            'cICFiles.Currency = Currency
                                                            cICFiles.PaymentNatureCode = CollectionNatureCode
                                                            cICFiles.FileUploadTemplateID = TemplateID
                                                            cICFiles.CreatedDate = Date.Now

                                                            Action = "Invoice DB File: File [Name: " & cICFiles.FileName & " ; ID: " & cICFiles.FileID & "] is updated as [Status: " & cICFiles.Status & " ; AcquisitionMode: " & cICFiles.AcqMode & " ; BatchNo: " & cICFiles.FileBatchNo & " ; OriginalFileName: " & cICFiles.OriginalFileName & " ; FileLocation: " & cICFiles.FileLocation & " ; CompanyCode: " & cICFiles.CompanyCode & " ; AccountNumber: " & cICFiles.AccountNumber & " ; BranchCode: " & cICFiles.BranchCode & " ; Currency: " & cICFiles.Currency & " ; Collection Nature: " & cICFiles.PaymentNatureCode & " ; TemplateID: " & cICFiles.FileUploadTemplateID & "] by User [ID: " & UserID & " ; Name: " & UserName & "]."
                                                            AuditType = "Invoice DB File Upload"
                                                            ICFilesController.UpdateClientFile(cICFiles, UserID, UserName, Action.ToString(), AuditType)
                                                        Else
                                                            Dim aFileInfo As IO.FileInfo
                                                            aFileInfo = New IO.FileInfo(FileLocation & FileName.ToString())
                                                            'Delete file from HDD.
                                                            If aFileInfo.Exists Then
                                                                aFileInfo.Delete()
                                                            End If
                                                            ' Move file to garbage folder
                                                            Dim Mape As FolderView = New FolderView(Integer.MaxValue)
                                                            Mape.PropertySet = New PropertySet(BasePropertySet.IdOnly)
                                                            Mape.PropertySet.Add(FolderSchema.DisplayName)
                                                            Mape.Traversal = FolderTraversal.Shallow
                                                            Dim fFR As FindFoldersResults = Service.FindFolders(WellKnownFolderName.Inbox, Mape)
                                                            For Each Folder In fFR.Folders
                                                                If Folder.DisplayName.Trim.ToLower.ToString() = "ICGarbage".Trim.ToLower.ToString() Then
                                                                    Item.Move(Folder.Id)
                                                                End If
                                                            Next

                                                            ICUtilities.AddAuditTrail("Unable to Upload File by User [ID: " & UserID & " ; Email: " & UserEmail & " ; UserName: " & EmailUserName & "] due to [" & CheckResult.ToString() & "]", "Email Invoice DB File Not Process", Nothing, UserID, EmailUserName, "ERROR")
                                                        End If
                                                    Else
                                                        ' Move file to garbage folder
                                                        Dim Mape As FolderView = New FolderView(Integer.MaxValue)
                                                        Mape.PropertySet = New PropertySet(BasePropertySet.IdOnly)
                                                        Mape.PropertySet.Add(FolderSchema.DisplayName)
                                                        Mape.Traversal = FolderTraversal.Shallow
                                                        Dim fFR As FindFoldersResults = Service.FindFolders(WellKnownFolderName.Inbox, Mape)
                                                        For Each Folder In fFR.Folders
                                                            If Folder.DisplayName.Trim.ToLower.ToString() = "ICGarbage".Trim.ToLower.ToString() Then
                                                                Item.Move(Folder.Id)
                                                            End If
                                                        Next
                                                       
                                                        ICUtilities.AddAuditTrail("Unable to Upload File by User [ID: " & UserID & " ; Email: " & UserEmail & " ; UserName: " & EmailUserName & "] due to [File Must be in the " & objICFileUploadTemplate.TemplateFormat.ToString() & " format]", "Email Invoice DB File Not Process", Nothing, UserID, EmailUserName, "ERROR")
                                                    End If
                                                End If
                                            End If
                                        Else
                                           

                                            ICUtilities.AddAuditTrail("Unable to Upload File by User [ID: " & UserID & " ; Email: " & UserEmail & " ; UserName: " & EmailUserName & "] due to [Invalid file format]", "Email Invoice DB File Not Process", Nothing, UserID, EmailUserName, "ERROR")
                                            
                                        End If
                                    Next
                                    If IsFileRead = True Then
                                        Dim Mape As FolderView = New FolderView(Integer.MaxValue)
                                        Mape.PropertySet = New PropertySet(BasePropertySet.IdOnly)
                                        Mape.PropertySet.Add(FolderSchema.DisplayName)
                                        Mape.Traversal = FolderTraversal.Shallow

                                        Dim fFR As FindFoldersResults = Service.FindFolders(WellKnownFolderName.Inbox, Mape)
                                        For Each Folder In fFR.Folders
                                            If IsFileRead = True Then
                                                If Folder.DisplayName.Trim.ToLower.ToString() = "ICEmails".Trim.ToLower.ToString() Then
                                                    Item.Move(Folder.Id)
                                                End If
                                            Else
                                                If Folder.DisplayName.Trim.ToLower.ToString() = "ICGarbage".Trim.ToLower.ToString() Then
                                                    Item.Move(Folder.Id)
                                                End If
                                            End If

                                        Next
                                        If NoOfFiles > 0 Then
                                            ICUtilities.AddAuditTrail("Get " & NoOfFiles.ToString() & " files uploaded by User [ID: " & UserID & " ; Email: " & UserEmail & " ; UserName: " & EmailUserName & "]", "Invoice DB File Upload", Nothing, UserID, EmailUserName, "EMAIL / FTP FILES")
                                        End If
                                        Exit For
                                    End If
                                End If
                            End If
                        Next
                    Next

                End If
            Catch ex As Exception
                ICUtilities.UpdateErrorLog("Unable to Upload File by User [ID: " & UserID & " ; Email: " & UserEmail & " ; UserName: " & EmailUserName & "] due to [" & ex.Message & "]", "EmailInvoiceDBFileProcess")
            End Try
        End Sub
        Public Shared Sub AutoReadCollectionFTPAndFileUpload()

            Dim MIMEType As String = ""
            Dim FileName As String = ""
            Dim chkCorrectFile As Boolean = False
            Dim IsFileRead As Boolean = False
            Dim dtFTPSettings As New DataTable
            Dim drFTPSettings As DataRow
            Dim TemplateID, CollectionNatureCode, CompanyCode, CompanyName, GroupCode, GroupName, FTPUserName As String
            Dim FTPAddress, FTPPassword, FTPFolder, FTPLocationType As String

            Dim Action, AuditType As String
            Dim objICFile As New ICFiles
            Dim objICFileUploadTemplate As New ICCollectionInvoiceDataTemplate
            Dim CheckResult As String = ""
            Dim FileLocation As String = ""
            Dim objICCompany As ICCompany
            FileLocation = ICUtilities.GetSettingValue("PhysicalApplicationPath").ToString() & "UploadedFiles\"
            Action = ""
            AuditType = ""
            TemplateID = ""
            'AccountNumber = ""
            'BranchCode = ""
            'Currency = ""
            CollectionNatureCode = ""
            CompanyCode = ""
            CompanyName = ""
            GroupCode = ""
            GroupName = ""
            FTPUserName = ""
            FTPAddress = ""
            FTPPassword = ""
            FTPFolder = ""
            FTPLocationType = ""
            Try

                Dim ftp As FtpConnection

                Dim aFileInfo As FileInfo
                Dim fileInfo As FtpFileInfo
                Dim FileColl() As FtpFileInfo
                Dim NoOfFiles As Integer = 0

                dtFTPSettings = ICCollectionNatureController.GetCollectionNatureFTPSettingsDataTable()
                If dtFTPSettings.Rows.Count > 0 Then
                    For Each drFTPSettings In dtFTPSettings.Rows
                        chkCorrectFile = False

                        objICFile = New ICFiles
                        objICFileUploadTemplate = New ICCollectionInvoiceDataTemplate

                        TemplateID = drFTPSettings("TemplateID").ToString()
                        
                        PaymentNatureCode = drFTPSettings("CollectionNatureCode").ToString()

                        CompanyCode = drFTPSettings("CompanyCode").ToString()
                        CompanyName = drFTPSettings("CompanyName").ToString()
                        GroupCode = drFTPSettings("GroupCode").ToString()
                        GroupName = drFTPSettings("GroupName").ToString()
                        FTPUserName = drFTPSettings("FTPUserName").ToString()
                        FTPAddress = drFTPSettings("FTPHostAddress").ToString()
                        FTPPassword = drFTPSettings("FTPPassword").ToString()
                        FTPFolder = drFTPSettings("FTPFolderName").ToString()

                        ftp = New FtpConnection(FTPAddress, FTPUserID, FTPPassword)

                        ftp.Open()
                        ftp.Login()

                        If Not (FTPFolder = "") Then
                            ftp.SetCurrentDirectory(FTPFolder.ToString())
                        End If

                        FileColl = ftp.GetFiles()
                        NoOfFiles = 0
                        For Each fileInfo In FileColl
                            chkCorrectFile = False
                            If fileInfo.Name.ToString().Contains("-") Then
                                If fileInfo.Name.ToString().Split("-")(1).Trim.ToString().Contains(".") Then
                                    If TemplateID.ToString() = fileInfo.Name.ToString().Split("-")(1).Trim.ToString().Split(".")(0).ToString().Trim Then
                                        chkCorrectFile = True
                                    End If
                                Else
                                    If TemplateID.ToString() = fileInfo.Name.ToString().Split("-")(1).Trim.ToString() Then
                                        chkCorrectFile = True
                                    End If
                                End If

                            Else
                                
                                ICUtilities.UpdateErrorLog("Unable to Upload File by FTP User [ID: " & FTPUserName & " ; FTPLocation: " & FTPAddress & " \ " & FTPFolder & " ] due to ['-' is missing in file name.]", "InvoiceDBFTPFileProcess")
                            End If
                            If chkCorrectFile = True Then
                                chkCorrectFile = False
                                MIMEType = fileInfo.Extension.ToString().ToLower
                                'Check .txt
                                If MIMEType = ".txt" Then
                                    chkCorrectFile = True
                                End If
                                'Check .xls
                                If MIMEType = ".xls" Then
                                    chkCorrectFile = True
                                End If
                                'Check .xlsx
                                If MIMEType = ".xlsx" Then
                                    chkCorrectFile = True
                                End If
                                If chkCorrectFile = True Then
                                    If objICFileUploadTemplate.LoadByPrimaryKey(TemplateID) Then
                                        If objICFileUploadTemplate.TemplateFormat.ToString().ToLower.ToString = ".txt" Then
                                            If fileInfo.Name.ToString().Split(".")(1).ToString().ToLower.Contains("txt") Or fileInfo.Name.ToString().Split(".")(1).ToString().ToLower.Contains("csv") Then
                                                FileName = objICFileUploadTemplate.TemplateName.ToString().Replace(" ", "_") & "" & Date.Now.ToString("ddMMyyyyHHmmss") & fileInfo.Name.ToString().Replace(" ", "_")
                                                ' Save File on HDD
                                                ftp.GetFile(fileInfo.Name.ToString(), FileLocation & FileName, True)
                                                CheckResult = ICFilesController.CheckCollectionFileBeforeUpload(TemplateID.ToString(), FileLocation & FileName.ToString()).ToString()
                                                If CheckResult.ToString() = "OK" Then
                                                    Action = "Client Invoice DB File: File [Name: " & fileInfo.Name.ToString() & "] of Group [Code: " & GroupCode.ToString() & " ; Name: " & GroupName.ToString() & "], Company [Code: " & CompanyCode.ToString() & " ; Name: " & CompanyName.ToString() & "] and Template [Code: " & objICFileUploadTemplate.CollectionInvoiceTemplateID.ToString() & " ; Name: " & objICFileUploadTemplate.TemplateName.ToString() & " ; Format : " & objICFileUploadTemplate.TemplateFormat.ToString() & "] is uploaded via FTP Upload by User [ID: " & FTPUserName & " ; FTPLocation: " & FTPAddress & " \ " & FTPFolder & "]."
                                                    AuditType = "Invoice DB File Upload"
                                                    objICFile.FileID = ICUtilities.UploadFiletoDBViaPath(FileLocation & FileName, "Invoice DB File", "Invoice DB File", fileInfo.Name.ToString, FTPUserName, FTPUserName, Action.ToString(), AuditType.ToString())
                                                    Dim cICFiles As New ICFiles
                                                    cICFiles.LoadByPrimaryKey(objICFile.FileID)
                                                    cICFiles.Status = "Pending Read"
                                                    cICFiles.AcqMode = "FTP File Upload"
                                                    cICFiles.FileBatchNo = ICFilesController.GetFileBatchNo("Invoice DB File")
                                                    cICFiles.FileName = FileName
                                                    cICFiles.OriginalFileName = fileInfo.Name.ToString()
                                                    cICFiles.FileLocation = FileLocation & FileName.ToString()
                                                    cICFiles.CompanyCode = CompanyCode.ToString()
                                                    'cICFiles.AccountNumber = AccountNumber.ToString()
                                                    'cICFiles.BranchCode = BranchCode.ToString()
                                                    'cICFiles.Currency = Currency.ToString()
                                                    cICFiles.PaymentNatureCode = PaymentNatureCode.ToString()
                                                    cICFiles.FileUploadTemplateID = TemplateID.ToString()
                                                    cICFiles.CreatedDate = Now
                                                    NoOfFiles = NoOfFiles + 1
                                                    ftp.RemoveFile(fileInfo.Name)

                                                    Action = "Client Invoice DB File: File [Name: " & cICFiles.FileName & " ; ID: " & cICFiles.FileID & "] is updated as [Status: " & cICFiles.Status & " ; AcquisitionMode: " & cICFiles.AcqMode & " ; BatchNo: " & cICFiles.FileBatchNo & " ; OriginalFileName: " & cICFiles.OriginalFileName & " ; FileLocation: " & cICFiles.FileLocation & " ; CompanyCode: " & cICFiles.CompanyCode & " ; AccountNumber: " & cICFiles.AccountNumber & " ; BranchCode: " & cICFiles.BranchCode & " ; Currency: " & cICFiles.Currency & " ; PaymentNatureCode: " & cICFiles.PaymentNatureCode & " ; TemplateID: " & cICFiles.FileUploadTemplateID & "] by User Name: " & FTPUserName.ToString() & "]."
                                                    AuditType = "Invoice DB File Upload"
                                                    ICFilesController.UpdateClientFile(cICFiles, Nothing, FTPUserName, Action.ToString(), AuditType)
                                                    'EmailUtilities.FileuploadedbyuserThroughFTP(cICFiles.OriginalFileName.ToString, objICUser.DisplayName.ToString, APNLocation)
                                                    'SMSUtilities.FileuploadedbyuserThroughFTP(objICUser.DisplayName.ToString, APNLocation)
                                                Else
                                                    Dim objFileInfo As IO.FileInfo
                                                    objFileInfo = New IO.FileInfo(FileLocation & FileName.ToString())
                                                    'Delete file from HDD.
                                                    If objFileInfo.Exists Then
                                                        objFileInfo.Delete()
                                                    End If
                                                    ICUtilities.AddAuditTrail("Unable to Upload File by User [ID: " & FTPUserName & " ; FTPLocation: " & FTPAddress & " \ " & FTPFolder & " due to [" & CheckResult.ToString() & "]", "Invoice DB FTP File Not Process", Nothing, Nothing, FTPUserName, "ERROR")
                                                    ftp.RemoveFile(fileInfo.Name)
                                                End If
                                            Else
                                                ICUtilities.AddAuditTrail("Unable to Upload File by User [ID: " & FTPUserName & " ; FTPLocation: " & FTPAddress & " \ " & FTPFolder & " ; UserName: " & FTPUserName & "] due to [File Must be in the " & objICFileUploadTemplate.TemplateFormat.ToString() & " or csv format]", "Invoice DB FTP File Not Process", Nothing, Nothing, FTPUserName, "ERROR")
                                                ftp.RemoveFile(fileInfo.Name)
                                            End If
                                        End If
                                        If objICFileUploadTemplate.TemplateFormat.ToString().ToLower.ToString = ".xls" Then
                                            If fileInfo.Name.ToString().Split(".")(1).ToString().ToLower = "xls" Then
                                                FileName = objICFileUploadTemplate.TemplateName.ToString().Replace(" ", "_") & "" & Date.Now.ToString("ddMMyyyyHHmmss") & fileInfo.Name.ToString().Replace(" ", "_")
                                                ' Save File on HDD
                                                ftp.GetFile(fileInfo.Name.ToString(), FileLocation & FileName, True)
                                                CheckResult = ICFilesController.CheckCollectionFileBeforeUpload(TemplateID.ToString(), FileLocation & FileName.ToString()).ToString()
                                                If CheckResult.ToString() = "OK" Then
                                                    Action = "Client Invoice DB File: File [Name: " & fileInfo.Name.ToString() & "] of Group [Code: " & GroupCode.ToString() & " ; Name: " & GroupName.ToString() & "], Company [Code: " & CompanyCode.ToString() & " ; Name: " & CompanyName.ToString() & "] and Template [Code: " & objICFileUploadTemplate.CollectionInvoiceTemplateID.ToString() & " ; Name: " & objICFileUploadTemplate.TemplateName.ToString() & " ; Format : " & objICFileUploadTemplate.TemplateFormat.ToString() & "] is uploaded via FTP Upload by User [ID: " & FTPUserName & " ; FTPLocation: " & FTPAddress & " \ " & FTPFolder & "]."
                                                    AuditType = "Invoice DB File Upload"
                                                    objICFile.FileID = ICUtilities.UploadFiletoDBViaPath(FileLocation & FileName, "Invoice DB File", "Invoice DB File", fileInfo.Name.ToString, FTPUserName, FTPUserName, Action.ToString(), AuditType.ToString())
                                                    Dim cICFiles As New ICFiles
                                                    cICFiles.LoadByPrimaryKey(objICFile.FileID)
                                                    cICFiles.Status = "Pending Read"
                                                    cICFiles.AcqMode = "FTP File Upload"
                                                    cICFiles.FileBatchNo = ICFilesController.GetFileBatchNo("Invoice DB File")
                                                    cICFiles.FileName = FileName
                                                    cICFiles.OriginalFileName = fileInfo.Name.ToString()
                                                    cICFiles.FileLocation = FileLocation & FileName.ToString()
                                                    cICFiles.CompanyCode = CompanyCode.ToString()
                                                    'cICFiles.AccountNumber = AccountNumber.ToString()
                                                    'cICFiles.BranchCode = BranchCode.ToString()
                                                    'cICFiles.Currency = Currency.ToString()
                                                    cICFiles.PaymentNatureCode = PaymentNatureCode.ToString()
                                                    cICFiles.FileUploadTemplateID = TemplateID.ToString()
                                                    cICFiles.CreatedDate = Now
                                                    NoOfFiles = NoOfFiles + 1
                                                    ftp.RemoveFile(fileInfo.Name)

                                                    Action = "Client Invoice DB File: File [Name: " & cICFiles.FileName & " ; ID: " & cICFiles.FileID & "] is updated as [Status: " & cICFiles.Status & " ; AcquisitionMode: " & cICFiles.AcqMode & " ; BatchNo: " & cICFiles.FileBatchNo & " ; OriginalFileName: " & cICFiles.OriginalFileName & " ; FileLocation: " & cICFiles.FileLocation & " ; CompanyCode: " & cICFiles.CompanyCode & " ; AccountNumber: " & cICFiles.AccountNumber & " ; BranchCode: " & cICFiles.BranchCode & " ; Currency: " & cICFiles.Currency & " ; PaymentNatureCode: " & cICFiles.PaymentNatureCode & " ; TemplateID: " & cICFiles.FileUploadTemplateID & "] by User Name: " & FTPUserName.ToString() & "]."
                                                    AuditType = "Invoice DB File Upload"
                                                    ICFilesController.UpdateClientFile(cICFiles, Nothing, FTPUserName, Action.ToString(), AuditType)
                                                    'EmailUtilities.FileuploadedbyuserThroughFTP(cICFiles.OriginalFileName.ToString, objICUser.DisplayName.ToString, APNLocation)
                                                    'SMSUtilities.FileuploadedbyuserThroughFTP(objICUser.DisplayName.ToString, APNLocation)
                                                Else
                                                    Dim objFileInfo As IO.FileInfo
                                                    objFileInfo = New IO.FileInfo(FileLocation & FileName.ToString())
                                                    'Delete file from HDD.
                                                    If objFileInfo.Exists Then
                                                        objFileInfo.Delete()
                                                    End If
                                                    ICUtilities.AddAuditTrail("Unable to Upload File by User [ID: " & FTPUserName & " ; FTPLocation: " & FTPAddress & " \ " & FTPFolder & " due to [" & CheckResult.ToString() & "]", "Invoice DB FTP File Not Process", Nothing, Nothing, FTPUserName, "ERROR")
                                                    ftp.RemoveFile(fileInfo.Name)
                                                End If
                                            Else
                                                ICUtilities.AddAuditTrail("Unable to Upload File by User [ID: " & FTPUserName & " ; FTPLocation: " & FTPAddress & " \ " & FTPFolder & " ; UserName: " & FTPUserName & "] due to [File Must be in the " & objICFileUploadTemplate.TemplateFormat.ToString() & " format]", "Invoice DB FTP File Not Process", Nothing, Nothing, FTPUserName, "ERROR")
                                                ftp.RemoveFile(fileInfo.Name)
                                            End If
                                        End If
                                        If objICFileUploadTemplate.TemplateFormat.ToString().ToLower.ToString = ".xlsx" Then
                                            If fileInfo.Name.ToString().Split(".")(1).ToString().ToLower = "xlsx" Then
                                                FileName = objICFileUploadTemplate.TemplateName.ToString().Replace(" ", "_") & "" & Date.Now.ToString("ddMMyyyyHHmmss") & fileInfo.Name.ToString().Replace(" ", "_")
                                                ' Save File on HDD
                                                ftp.GetFile(fileInfo.Name.ToString(), FileLocation & FileName, True)
                                               CheckResult = ICFilesController.CheckCollectionFileBeforeUpload(TemplateID.ToString(), FileLocation & FileName.ToString()).ToString()
                                                If CheckResult.ToString() = "OK" Then
                                                    Action = "Client Invoice DB File: File [Name: " & fileInfo.Name.ToString() & "] of Group [Code: " & GroupCode.ToString() & " ; Name: " & GroupName.ToString() & "], Company [Code: " & CompanyCode.ToString() & " ; Name: " & CompanyName.ToString() & "] and Template [Code: " & objICFileUploadTemplate.CollectionInvoiceTemplateID.ToString() & " ; Name: " & objICFileUploadTemplate.TemplateName.ToString() & " ; Format : " & objICFileUploadTemplate.TemplateFormat.ToString() & "] is uploaded via FTP Upload by User [ID: " & FTPUserName & " ; FTPLocation: " & FTPAddress & " \ " & FTPFolder & "]."
                                                    AuditType = "Invoice DB File Upload"
                                                    objICFile.FileID = ICUtilities.UploadFiletoDBViaPath(FileLocation & FileName, "Invoice DB File", "Invoice DB File", fileInfo.Name.ToString, FTPUserName, FTPUserName, Action.ToString(), AuditType.ToString())
                                                    Dim cICFiles As New ICFiles
                                                    cICFiles.LoadByPrimaryKey(objICFile.FileID)
                                                    cICFiles.Status = "Pending Read"
                                                    cICFiles.AcqMode = "FTP File Upload"
                                                    cICFiles.FileBatchNo = ICFilesController.GetFileBatchNo("Invoice DB File")
                                                    cICFiles.FileName = FileName
                                                    cICFiles.OriginalFileName = fileInfo.Name.ToString()
                                                    cICFiles.FileLocation = FileLocation & FileName.ToString()
                                                    cICFiles.CompanyCode = CompanyCode.ToString()
                                                    'cICFiles.AccountNumber = AccountNumber.ToString()
                                                    'cICFiles.BranchCode = BranchCode.ToString()
                                                    'cICFiles.Currency = Currency.ToString()
                                                    cICFiles.PaymentNatureCode = PaymentNatureCode.ToString()
                                                    cICFiles.FileUploadTemplateID = TemplateID.ToString()
                                                    cICFiles.CreatedDate = Now
                                                    NoOfFiles = NoOfFiles + 1
                                                    ftp.RemoveFile(fileInfo.Name)

                                                    Action = "Client Invoice DB File: File [Name: " & cICFiles.FileName & " ; ID: " & cICFiles.FileID & "] is updated as [Status: " & cICFiles.Status & " ; AcquisitionMode: " & cICFiles.AcqMode & " ; BatchNo: " & cICFiles.FileBatchNo & " ; OriginalFileName: " & cICFiles.OriginalFileName & " ; FileLocation: " & cICFiles.FileLocation & " ; CompanyCode: " & cICFiles.CompanyCode & " ; AccountNumber: " & cICFiles.AccountNumber & " ; BranchCode: " & cICFiles.BranchCode & " ; Currency: " & cICFiles.Currency & " ; PaymentNatureCode: " & cICFiles.PaymentNatureCode & " ; TemplateID: " & cICFiles.FileUploadTemplateID & "] by User Name: " & FTPUserName.ToString() & "]."
                                                    AuditType = "Invoice DB File Upload"
                                                    ICFilesController.UpdateClientFile(cICFiles, Nothing, FTPUserName, Action.ToString(), AuditType)
                                                    'EmailUtilities.FileuploadedbyuserThroughFTP(cICFiles.OriginalFileName.ToString, objICUser.DisplayName.ToString, APNLocation)
                                                    'SMSUtilities.FileuploadedbyuserThroughFTP(objICUser.DisplayName.ToString, APNLocation)
                                                Else
                                                    Dim objFileInfo As IO.FileInfo
                                                    objFileInfo = New IO.FileInfo(FileLocation & FileName.ToString())
                                                    'Delete file from HDD.
                                                    If objFileInfo.Exists Then
                                                        objFileInfo.Delete()
                                                    End If
                                                    ICUtilities.AddAuditTrail("Unable to Upload File by User [ID: " & FTPUserName & " ; FTPLocation: " & FTPAddress & " \ " & FTPFolder & " due to [" & CheckResult.ToString() & "]", "Invoice DB FTP File Not Process", Nothing, Nothing, FTPUserName, "ERROR")
                                                    ftp.RemoveFile(fileInfo.Name)
                                                End If
                                            Else
                                                ICUtilities.AddAuditTrail("Unable to Upload File by User [ID: " & FTPUserName & " ; FTPLocation: " & FTPAddress & " \ " & FTPFolder & "  due to [File Must be in the " & objICFileUploadTemplate.TemplateFormat.ToString() & " format]", "Invoice DB FTP File Not Process", Nothing, Nothing, FTPUserName, "ERROR")
                                                ftp.RemoveFile(fileInfo.Name)
                                            End If
                                        End If
                                        If objICFileUploadTemplate.TemplateFormat.ToString().ToLower.ToString = ".csv" Then
                                            If fileInfo.Name.ToString().Split(".")(1).ToString().ToLower = "csv" Then
                                                FileName = objICFileUploadTemplate.TemplateName.ToString().Replace(" ", "_") & "" & Date.Now.ToString("ddMMyyyyHHmmss") & fileInfo.Name.ToString().Replace(" ", "_")
                                                ' Save File on HDD
                                                ftp.GetFile(fileInfo.Name.ToString(), FileLocation & FileName, True)
                                                CheckResult = ICFilesController.CheckCollectionFileBeforeUpload(TemplateID.ToString(), FileLocation & FileName.ToString()).ToString()
                                                If CheckResult.ToString() = "OK" Then
                                                    Action = "Client Invoice DB File: File [Name: " & fileInfo.Name.ToString() & "] of Group [Code: " & GroupCode.ToString() & " ; Name: " & GroupName.ToString() & "], Company [Code: " & CompanyCode.ToString() & " ; Name: " & CompanyName.ToString() & "] and Template [Code: " & objICFileUploadTemplate.CollectionInvoiceTemplateID.ToString() & " ; Name: " & objICFileUploadTemplate.TemplateName.ToString() & " ; Format : " & objICFileUploadTemplate.TemplateFormat.ToString() & "] is uploaded via FTP Upload by User [ID: " & FTPUserName & " ; FTPLocation: " & FTPAddress & " \ " & FTPFolder & "]."
                                                    AuditType = "Invoice DB File Upload"
                                                    objICFile.FileID = ICUtilities.UploadFiletoDBViaPath(FileLocation & FileName, "Invoice DB File", "Invoice DB File", fileInfo.Name.ToString, FTPUserName, FTPUserName, Action.ToString(), AuditType.ToString())
                                                    Dim cICFiles As New ICFiles
                                                    cICFiles.LoadByPrimaryKey(objICFile.FileID)
                                                    cICFiles.Status = "Pending Read"
                                                    cICFiles.AcqMode = "FTP File Upload"
                                                    cICFiles.FileBatchNo = ICFilesController.GetFileBatchNo("Invoice DB File")
                                                    cICFiles.FileName = FileName
                                                    cICFiles.OriginalFileName = fileInfo.Name.ToString()
                                                    cICFiles.FileLocation = FileLocation & FileName.ToString()
                                                    cICFiles.CompanyCode = CompanyCode.ToString()
                                                    'cICFiles.AccountNumber = AccountNumber.ToString()
                                                    'cICFiles.BranchCode = BranchCode.ToString()
                                                    'cICFiles.Currency = Currency.ToString()
                                                    cICFiles.PaymentNatureCode = PaymentNatureCode.ToString()
                                                    cICFiles.FileUploadTemplateID = TemplateID.ToString()
                                                    cICFiles.CreatedDate = Now
                                                    NoOfFiles = NoOfFiles + 1
                                                    ftp.RemoveFile(fileInfo.Name)

                                                    Action = "Client Invoice DB File: File [Name: " & cICFiles.FileName & " ; ID: " & cICFiles.FileID & "] is updated as [Status: " & cICFiles.Status & " ; AcquisitionMode: " & cICFiles.AcqMode & " ; BatchNo: " & cICFiles.FileBatchNo & " ; OriginalFileName: " & cICFiles.OriginalFileName & " ; FileLocation: " & cICFiles.FileLocation & " ; CompanyCode: " & cICFiles.CompanyCode & " ; AccountNumber: " & cICFiles.AccountNumber & " ; BranchCode: " & cICFiles.BranchCode & " ; Currency: " & cICFiles.Currency & " ; Collection Nature Code: " & cICFiles.PaymentNatureCode & " ; TemplateID: " & cICFiles.FileUploadTemplateID & "] by User Name: " & FTPUserName.ToString() & "]."
                                                    AuditType = "Invoice DB File Upload"
                                                    ICFilesController.UpdateClientFile(cICFiles, Nothing, FTPUserName, Action.ToString(), AuditType)
                                                   
                                                Else
                                                    Dim objFileInfo As IO.FileInfo
                                                    objFileInfo = New IO.FileInfo(FileLocation & FileName.ToString())
                                                    'Delete file from HDD.
                                                    If objFileInfo.Exists Then
                                                        objFileInfo.Delete()
                                                    End If
                                                    ICUtilities.AddAuditTrail("Unable to Upload File by User [ID: " & FTPUserName & " ; FTPLocation: " & FTPAddress & " \ " & FTPFolder & " due to [" & CheckResult.ToString() & "]", "Invoice DB FTP File Not Process", Nothing, Nothing, FTPUserName, "ERROR")
                                                    ftp.RemoveFile(fileInfo.Name)
                                                End If
                                            Else
                                                ICUtilities.AddAuditTrail("Unable to Upload File by User [ID: " & FTPUserName & " ; FTPLocation: " & FTPAddress & " \ " & FTPFolder & "  due to [File Must be in the " & objICFileUploadTemplate.TemplateFormat.ToString() & " format]", "Invoice DB FTP File Not Process", Nothing, Nothing, FTPUserName, "ERROR")
                                                ftp.RemoveFile(fileInfo.Name)
                                            End If
                                        End If
                                    End If
                                Else

                                    ICUtilities.AddAuditTrail("Unable to Upload File by User [ID: " & FTPUserName & " ; FTPLocation: " & FTPAddress & " \ " & FTPFolder & " due to [Invalid file format]", "FTP File Not Process", Nothing, Nothing, FTPUserName, "ERROR")
                                End If
                            End If
                        Next
                        ftp.Close()
                        If NoOfFiles > 0 Then
                            ICUtilities.AddAuditTrail("Get " & NoOfFiles.ToString() & " files uploaded by User [ID: " & FTPUserName & " ; FTPLocation: " & FTPAddress & " \ " & FTPFolder & " ]", "Invoice DB File Upload", Nothing, Nothing, FTPUserName, "Invoice DB FTP FILES")
                        End If

                    Next
                End If
            Catch ex As Exception
                ICUtilities.UpdateErrorLog("Unable to Upload File by User [ID: " & FTPUserName & " ; FTPLocation: " & FTPAddress & " \ " & FTPFolder & " due to [" & ex.Message & "]", "InvoiceDBFTPFileNotProcess")
            End Try
        End Sub
    End Class
End Namespace
