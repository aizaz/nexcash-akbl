﻿Public Class ICPackageInvoiceController



    Public Shared Sub GetPackageInvoices(ByVal CompanyCode As Integer, ByVal PageNumber As Integer, ByVal PageSize As Integer, ByVal DoDataBind As Boolean, ByVal rg As Telerik.Web.UI.RadGrid, ByVal IsPaid As Nullable(Of Integer), ByVal FromDate As Nullable(Of Date), ByVal ToDate As Nullable(Of Date))



        Dim pinvcoll As New IC.ICPackageInvoiceCollection

        pinvcoll.Query.Where(pinvcoll.Query.CompanyCode = CompanyCode)

        If IsPaid.HasValue Then

            pinvcoll.Query.Where(pinvcoll.Query.IsPaid = IsPaid)





        End If


        If FromDate.HasValue And ToDate.HasValue Then


            pinvcoll.Query.Where(pinvcoll.Query.InvoiceDate.Date.Between(FromDate.Value.Date, ToDate.Value.Date))





        End If






        If Not PageNumber = 0 Then

            pinvcoll.Query.es.PageNumber = PageNumber
            pinvcoll.Query.es.PageSize = PageSize
           
        Else
            pinvcoll.Query.es.PageNumber = 1
            pinvcoll.Query.es.PageSize = PageSize
          

        End If


        pinvcoll.Query.OrderBy(pinvcoll.Query.InvoiceDate, EntitySpaces.DynamicQuery.esOrderByDirection.Ascending)


        If pinvcoll.Query.Load Then

            rg.DataSource = pinvcoll
        Else

            rg.DataSource = New Object() {}




        End If



        If DoDataBind = True Then
            rg.DataBind()
        End If

    End Sub


    Public Shared Sub UpdatePackageInvoice(ByVal packageinvoice As IC.ICPackageInvoice, ByVal userid As Integer, ByVal username As Integer)





        Dim pinv As New IC.ICPackageInvoice


        pinv.LoadByPrimaryKey(packageinvoice.InvoiceId)


        pinv.ChargeCreatedBy = packageinvoice.ChargeCreatedBy
        pinv.ChargeCreatedOn = packageinvoice.ChargeCreatedOn
        pinv.IsApproved = packageinvoice.IsApproved
        pinv.ChargeTransactionStatus = packageinvoice.ChargeTransactionStatus





        pinv.ClientAccountNumber = packageinvoice.ClientAccountNumber
        pinv.ClientAccountBranchCode = packageinvoice.ClientAccountBranchCode
        pinv.ClientAccountCurrency = packageinvoice.ClientAccountCurrency


        pinv.BankAccountId = packageinvoice.BankAccountId

        pinv.DiscountAmount = packageinvoice.DiscountAmount



        pinv.PaidAmount = packageinvoice.PaidAmount

        pinv.Remarks = packageinvoice.Remarks

        pinv.Save()


        Dim action As String = "Invoice charge created by user [ " & userid.ToString & " ] [ " & username.ToString & " ]"


        ICUtilities.AddAuditTrail(action, "Package Invoice", pinv.InvoiceId, userid, username, "UPDATE")






    End Sub


    Public Shared Function ApprovePackageInvoice(ByVal packageinvoice As IC.ICPackageInvoice, ByVal userid As Integer, ByVal username As Integer) As Boolean





        Dim pinv As New IC.ICPackageInvoice


        pinv.LoadByPrimaryKey(packageinvoice.InvoiceId)


        pinv.ChargeApprovedBy = packageinvoice.ChargeApprovedBy
        pinv.ChargeApprovedOn = packageinvoice.ChargeApprovedOn
        pinv.IsApproved = packageinvoice.IsApproved
        pinv.BankAccountId = packageinvoice.BankAccountId



        pinv.ChargeTransactionStatus = packageinvoice.ChargeTransactionStatus



        pinv.Save()


        Dim action As String = "Invoice charge approved by user [ " & userid.ToString & " ] [ " & username.ToString & " ]"


        ICUtilities.AddAuditTrail(action, "Package Invoice", pinv.InvoiceId, userid, username, "UPDATE")



        'create funds transfer transaction


        Dim bankaccount As New IC.ICBankAccounts

        bankaccount.LoadByPrimaryKey(pinv.BankAccountId)

        pinv.ChargeTransactionStatus = "Pending CB"

        pinv.Save()


        Dim bankaccounttype As CBUtilities.AccountType

        If bankaccount.CBAccountType = "RB" Then

            bankaccounttype = CBUtilities.AccountType.RB

        Else

            bankaccounttype = CBUtilities.AccountType.GL



        End If


        Dim trstatus As CBUtilities.TransactionStatus

        'trstatus = CBUtilities.OpenEndedFundsTransfer(pinv.ClientAccountNumber, pinv.ClientAccountBranchCode, pinv.ClientAccountCurrency, "", "", "", CBUtilities.AccountType.RB, bankaccount.AccountNumber, bankaccount.BranchCode, bankaccount.Currency, bankaccount.ClientNo, bankaccount.SeqNo, bankaccount.ProfitCentre, bankaccounttype, pinv.PaidAmount, "Package Invoice", pinv.InvoiceId)
        trstatus = CBUtilities.OpenEndedFundsTransfer(pinv.ClientAccountNumber, pinv.ClientAccountBranchCode, pinv.ClientAccountCurrency, "", "", "", CBUtilities.AccountType.RB, bankaccount.AccountNumber, bankaccount.BranchCode, bankaccount.Currency, "", "", "", bankaccounttype, pinv.PaidAmount, "Package Invoice", pinv.InvoiceId)


        Select Case trstatus


            Case CBUtilities.TransactionStatus.Successfull

                pinv.ChargeTransactionStatus = "Settled"
                pinv.PaymentDate = Now
                pinv.IsPaid = True
                pinv.Save()



                Dim traction As String = "Invoice charge transaction successful"

                ICUtilities.AddAuditTrail(traction, "Package Invoice", pinv.InvoiceId, userid, username, "UPDATE")




                Return True




            Case Else

                pinv.ChargeTransactionStatus = "Failed"
                pinv.ChargeTransactionErrorMessage = trstatus.ToString
                pinv.IsPaid = False


                pinv.Save()


                Dim traction As String = "Invoice charge transaction un-successful with error:" & trstatus.ToString

                ICUtilities.AddAuditTrail(traction, "Package Invoice", pinv.InvoiceId, userid, username, "UPDATE")

                Return False



        End Select














    End Function





End Class
