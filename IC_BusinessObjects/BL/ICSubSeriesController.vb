﻿Namespace IC
    Public Class ICSubSeriesController
        Public Shared Sub AddSubSeriesFromMasterSeries(ByVal cSubSeries As ICSubSeries, ByVal IsUpdate As Boolean)

            Dim objICSubSeries As New ICSubSeries
            objICSubSeries.es.Connection.CommandTimeout = 3600

            If IsUpdate = False Then
                objICSubSeries.CreateDate = Now
                objICSubSeries.CreatedBy = cSubSeries.CreatedBy
                objICSubSeries.Approvedby = cSubSeries.CreatedBy
                objICSubSeries.ApprovedOn = Now
                objICSubSeries.IsActive = cSubSeries.IsActive
                objICSubSeries.IsApprove = cSubSeries.IsApprove
            Else

                objICSubSeries.LoadByPrimaryKey(cSubSeries.SubSeriesID)

            End If
            objICSubSeries.MasterSeriesID = cSubSeries.MasterSeriesID
            objICSubSeries.OfficeCode = cSubSeries.OfficeCode
            objICSubSeries.Series = cSubSeries.Series
            objICSubSeries.Save()
        End Sub
        Public Shared Function GetSubSeriesByMasterSeriesIDForDelete(ByVal MasterSeriesID As String) As Boolean


            Dim rslt As Boolean = False

            Dim objICSubSeriesColl As New ICSubSeriesCollection
            objICSubSeriesColl.es.Connection.CommandTimeout = 3600



            objICSubSeriesColl.Query.Where(objICSubSeriesColl.Query.MasterSeriesID = MasterSeriesID And objICSubSeriesColl.Query.OfficeCode <> "0")

            objICSubSeriesColl.Query.Load()
            If objICSubSeriesColl.Query.Load() = True Then

                rslt = True
            Else
                rslt = False


            End If

            Return rslt



        End Function
        Public Shared Sub DeleteSubSeriesByMasterSeriesID(ByVal MasterSeriesID As String)
            Dim objICSubSeriesColl As New ICSubSeriesCollection
            objICSubSeriesColl.es.Connection.CommandTimeout = 3600
            objICSubSeriesColl.Query.Where(objICSubSeriesColl.Query.MasterSeriesID = MasterSeriesID)
            objICSubSeriesColl.Query.Load()
            If objICSubSeriesColl.Query.Load() Then
                For Each objICSubSeries As ICSubSeries In objICSubSeriesColl
                    Dim objICSubSeries2 As New ICSubSeries
                    objICSubSeries2.LoadByPrimaryKey(objICSubSeries.SubSeriesID)
                    objICSubSeries2.MarkAsDeleted()
                    objICSubSeries2.Save()
                Next


            End If



        End Sub
        Public Shared Function GetSubSeriesByMasterSeriesIDAndOfficeCode(ByVal MasterSeriesID As String, ByVal OfficeCode As String) As DataTable
            Dim objICSubSeriesColl As New ICSubSeriesCollection
            Dim dt As New DataTable
            objICSubSeriesColl.es.Connection.ToString()
            objICSubSeriesColl.Query.Where(objICSubSeriesColl.Query.MasterSeriesID = MasterSeriesID And objICSubSeriesColl.Query.OfficeCode = OfficeCode)
            objICSubSeriesColl.Query.OrderBy(objICSubSeriesColl.Query.SubSeriesID.Ascending)
            dt = objICSubSeriesColl.Query.LoadDataTable()
            Return dt





        End Function
        Public Shared Function GetSubSeriesByMasterSeriesIDAndOfficeCodeNull(ByVal MasterSeriesID As String) As DataTable
            Dim objICSubSeriesColl As New ICSubSeriesCollection
            Dim dt As New DataTable
            objICSubSeriesColl.es.Connection.ToString()
            objICSubSeriesColl.Query.Where(objICSubSeriesColl.Query.MasterSeriesID = MasterSeriesID And objICSubSeriesColl.Query.OfficeCode.IsNull())
            objICSubSeriesColl.Query.OrderBy(objICSubSeriesColl.Query.SubSeriesID.Ascending)
            dt = objICSubSeriesColl.Query.LoadDataTable()
            Return dt





        End Function
       
    End Class
End Namespace

