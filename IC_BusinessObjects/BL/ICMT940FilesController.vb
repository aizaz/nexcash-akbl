﻿Namespace IC
    Public Class ICMT940FilesController
        Public Shared Function AddMT940File(ByVal objICMT40File As ICMT940Files) As String
            Dim objICMT940FileSave As New ICMT940Files
            Dim ActionString As String = Nothing
            objICMT940FileSave.FileName = objICMT40File.FileName
            objICMT940FileSave.OriginalFileName = objICMT40File.OriginalFileName
            objICMT940FileSave.FileLocation = objICMT40File.FileLocation
            objICMT940FileSave.CreateDate = objICMT40File.CreateDate
            objICMT940FileSave.ExportedBy = objICMT40File.ExportedBy
            objICMT940FileSave.CompanyCode = objICMT40File.CompanyCode
            objICMT940FileSave.ClientAccountNo = objICMT40File.ClientAccountNo
            objICMT940FileSave.ClientAccountBranchCode = objICMT40File.ClientAccountBranchCode
            objICMT940FileSave.ClientAccountCurrency = objICMT40File.ClientAccountCurrency
            objICMT940FileSave.MT940FileData = objICMT40File.MT940FileData
            objICMT940FileSave.MT940FileMIMEType = objICMT40File.MT940FileMIMEType
            objICMT940FileSave.Save()
            ActionString = Nothing
            ActionString = "MT940 File for Client with Code [ " & objICMT940FileSave.CompanyCode & " ] , Account No [ " & objICMT940FileSave.ClientAccountNo & " ]"
            ActionString += " File Name [ " & objICMT940FileSave.OriginalFileName & " ] added"
            ICUtilities.AddAuditTrail(ActionString, "MT940", objICMT940FileSave.MT940FileID.ToString, Nothing, Nothing, "ADD")
            Return objICMT940FileSave.MT940FileID.ToString()
        End Function
        Public Shared Sub UpdateRTGSFile(ByVal objICMT40File As ICMT940Files, ByVal NoOfTransactions As Integer)
            Dim objICMT940FileSave As New ICMT940Files
            objICMT940FileSave.LoadByPrimaryKey(objICMT40File.MT940FileID)
            Dim ActionString As String = Nothing
            objICMT940FileSave.FileName = objICMT40File.FileName
            objICMT940FileSave.OriginalFileName = objICMT40File.OriginalFileName
            objICMT940FileSave.FileLocation = objICMT40File.FileLocation
            objICMT940FileSave.CreateDate = objICMT40File.CreateDate
            objICMT940FileSave.ExportedBy = objICMT40File.ExportedBy
            objICMT940FileSave.CompanyCode = objICMT40File.CompanyCode
            objICMT940FileSave.ClientAccountNo = objICMT40File.ClientAccountNo
            objICMT940FileSave.ClientAccountBranchCode = objICMT40File.ClientAccountBranchCode
            objICMT940FileSave.ClientAccountCurrency = objICMT40File.ClientAccountCurrency
            objICMT940FileSave.MT940FileData = objICMT40File.MT940FileData
            objICMT940FileSave.MT940FileMIMEType = objICMT40File.MT940FileMIMEType
            objICMT940FileSave.NoOfTxns = objICMT40File.NoOfTxns
            objICMT940FileSave.ExportPeriodToDate = objICMT40File.ExportPeriodToDate
            objICMT940FileSave.ExportPeriodFromDate = objICMT40File.ExportPeriodFromDate
            objICMT940FileSave.Save()
            ActionString = Nothing
            ActionString = "MT940 File for Client with Code [ " & objICMT940FileSave.CompanyCode & " ] , Account No [ " & objICMT940FileSave.ClientAccountNo & " ]"
            ActionString += " File Name [ " & objICMT940FileSave.OriginalFileName & " ] updated"
            ICUtilities.AddAuditTrail(ActionString, "MT940", objICMT940FileSave.MT940FileID.ToString, Nothing, Nothing, "UPDATE")
        End Sub
        Public Shared Function CreatMT940Files(ByVal msg As String, ByVal FileName As String) As String
            Dim apppath As String
            Dim path As String

            apppath = ICUtilities.GetSettingValue("PhysicalApplicationPath") & "MT940Files"

            Dim dinfo As New IO.DirectoryInfo(apppath)

            If dinfo.Exists = False Then
                dinfo.Create()
            End If

            path = apppath & "\" & FileName.ToString() & ".txt"

            Dim finfo As New IO.FileInfo(path)
            If finfo.Exists Then
                Dim writer As System.IO.StreamWriter
                writer = IO.File.AppendText(path)
                writer.Flush()
                writer.Write(msg.Replace(" ", "").TrimEnd(" "))
                writer.Flush()
                writer.Close()
            Else
                Dim writer As System.IO.StreamWriter
                writer = IO.File.CreateText(path)
                writer.Flush()
                writer.Write(msg.Replace(" ", "").TrimEnd(" "))
                writer.Flush()
                writer.Close()
            End If

            Return path
        End Function
        Public Shared Sub CreatDownloadedMT940Files(ByVal FilePath As String)
            'Dim apppath As String
            'Dim path As String

            'apppath = ICUtilities.GetSettingValue("PhysicalApplicationPath") & "MT940Files"

            'Dim dinfo As New IO.DirectoryInfo(FilePath)

            'If dinfo.Exists = False Then
            '    dinfo.Create()
            'End If

            'path = apppath & "\" & FileName.ToString() & ".txt"
            Dim reader As New System.IO.StreamReader(FilePath)
            Dim finfo As New IO.FileInfo(FilePath)
            If finfo.Exists Then
                Do While reader.Peek <> -1
                    Dim writer As System.IO.StreamWriter
                    writer = IO.File.AppendText(FilePath)
                    writer.WriteLine(reader.ToString)
                    writer.Flush()
                    writer.Close()
                Loop
            Else
                Do While reader.Peek <> -1
                    Dim writer As System.IO.StreamWriter
                    writer = IO.File.AppendText(FilePath)
                    writer.WriteLine(reader.ToString)
                    writer.Flush()
                    writer.Close()
                Loop
            End If

            'Return path
        End Sub
        Public Shared Function GetPageCount(ByVal dtMT940 As DataTable) As Integer
            Dim PageCount As Integer = 0
            Dim ResultPageCount As Double
            If dtMT940.Rows.Count = 1 Then
                PageCount = 1
            Else
                ResultPageCount = (dtMT940.Rows.Count / 6)
                If ResultPageCount.ToString.Contains(".") Then
                    If CInt(ResultPageCount.ToString.Split(".")(0).Substring(0, 2)) <> 1 Then
                        PageCount = CInt(ResultPageCount) + 1
                    Else
                        PageCount = CInt(ResultPageCount)
                    End If
                End If
            End If
            Return PageCount
        End Function

        'Public Shared Function GetSixRowsTableForMT940(ByVal dtMT940 As DataTable, ByVal FileCount As Integer, ByRef StartIndex As Integer, ByRef LastIndex As Integer) As DataTable
        '    Dim dtFinal As New DataTable
        '    Dim i As Integer = 0
        '    Dim j As Integer = 0
        '    If FileCount = 1 Then


        '        If dtMT940.Rows.Count = 1 Or dtMT940.Rows.Count <= 7 Then
        '            ''In case of only one row(No txns or only 6 txns including first row)
        '            dtFinal = dtMT940.Clone
        '            For Each dr As DataRow In dtMT940.Rows
        '                dtFinal.ImportRow(dtMT940.Rows(i))
        '                i = i + 1
        '            Next
        '        Else
        '            ''First loop if more than 6 txns
        '            dtFinal = dtMT940.Clone
        '            For j = StartIndex To (LastIndex = (StartIndex + 6))
        '                dtFinal.ImportRow(dtMT940.Rows(j))
        '            Next
        '            StartIndex = LastIndex + 1
        '            LastIndex = StartIndex + 5
        '        End If
        '    Else
        '        dtFinal = dtMT940.Clone
        '        If dtMT940.Rows.Count > LastIndex Then
        '            For j = StartIndex To LastIndex
        '                dtFinal.ImportRow(dtMT940.Rows(j))
        '            Next
        '            StartIndex = LastIndex + 1
        '            LastIndex = StartIndex + 5
        '        Else
        '            LastIndex = dtMT940.Rows.Count - 1
        '            For j = StartIndex To LastIndex
        '                dtFinal.ImportRow(dtMT940.Rows(j))
        '            Next
        '        End If
        '    End If
        '    Return dtFinal
        'End Function
        Public Shared Function GetSixRowsTableForMT940(ByVal dtMT940 As DataTable) As DataSet
            Dim dtFinal As New DataTable
            Dim FinalDS As New DataSet
            Dim IsFirstLoop As Boolean = True
            Dim i As Integer
            For Each dr As DataRow In dtMT940.Rows
                If i = 0 Then
                    dtFinal = New DataTable
                    dtFinal = dtMT940.Clone
                    dtFinal.ImportRow(dtMT940.Rows(i))
                Else
                    dtFinal.ImportRow(dtMT940.Rows(i))
                End If
                i = i + 1
                If IsFirstLoop = True Then
                    If i = 7 Then
                        FinalDS.Tables.Add(dtFinal)
                        i = 0
                        IsFirstLoop = False
                    End If
                Else
                    If i = 6 Then
                        FinalDS.Tables.Add(dtFinal)
                        i = 0
                    End If
                End If
            Next
            Return FinalDS
        End Function
        Public Shared Function ExportMT940FileForSOA(ByVal dtMT940 As DataTable, ByVal UsersID As String, ByVal UsersName As String, ByVal objICMT940 As ICMT940Files, ByVal AccountTitle As String, ByVal CurrentPageNo As String, ByVal TotalPageCount As String) As Integer
            Dim LineMsg As String = ""
            Dim Actionstring As String = Nothing
            Dim FileName As String = ""
            Dim FilePath As String = ""
            Dim MT940FileID As String = ""
            Dim NoOfTransaction As Integer = 0
            Dim LastRow As Integer = 0
            Dim PageCount As Integer = 0
            Dim MT940FileIDs As Integer = 0
            Dim dr As DataRow
            LastRow = dtMT940.Rows.Count - 1
            PageCount = GetPageCount(dtMT940)
            FileName = Date.Now.ToString("ddMMyyyy") & dtMT940.Rows(0)("CustomerSwiftCode") & dtMT940.Rows(0)(2) & dtMT940.Rows(0)(3) & dtMT940.Rows(0)(4) & "SWIFT"
            'Add RTGS File in DB
            objICMT940.FileName = objICMT940.CompanyCode & objICMT940.ClientAccountNo & ".txt"
            objICMT940.OriginalFileName = FileName & ".txt"
            MT940FileIDs = AddMT940File(objICMT940)
            objICMT940.MT940FileID = MT940FileIDs
            If dtMT940.Rows.Count = 1 Then
                'LineMsg








            End If








            dr = dtMT940(0)

            ''Owner Account title Info
            LineMsg = "{:20:"
            If AccountTitle.Length > 16 Then
                LineMsg += AccountTitle.Substring(0, 16) & System.Environment.NewLine
            Else
                LineMsg += AccountTitle & System.Environment.NewLine
            End If
            ''Account No
            LineMsg += ":25:"
            If objICMT940.ClientAccountNo.Length > 25 Then
                LineMsg += objICMT940.ClientAccountNo.Substring(0, 25) & System.Environment.NewLine
            Else
                LineMsg += objICMT940.ClientAccountNo & System.Environment.NewLine
            End If
            ''Transaction SequenceNo
            'LineMsg += ":28:" & dr("SequenceNo") & System.Environment.NewLine
            LineMsg += ":28:" & dr("SerialNo") & System.Environment.NewLine
            ''Opening Balance
            If dr("OpeningBalance") < 0 Then
                LineMsg += ":60F:D" & CDate(dr("Date")).ToString("yyMMdd") & "PKR"
                'LineMsg += ":60:D" & dr("Date").ToString & "PKR"
                If dr("OpeningBalance").ToString.Contains(".") Then
                    LineMsg += dr("OpeningBalance").ToString.Replace(".", ",") & System.Environment.NewLine
                Else
                    LineMsg += dr("OpeningBalance").ToString & "," & System.Environment.NewLine
                    'LineMsg += dr("OpeningBalance").ToString & System.Environment.NewLine
                End If
            Else
                'LineMsg += ":60:C" & dr("Date").ToString & "PKR"
                LineMsg += ":60F:C" & CDate(dr("Date")).ToString("yyMMdd") & "PKR"
                If dr("OpeningBalance").ToString.Contains(".") Then
                    LineMsg += dr("OpeningBalance").ToString.Replace(".", ",") & System.Environment.NewLine
                Else
                    LineMsg += dr("OpeningBalance").ToString & "," & System.Environment.NewLine
                    'LineMsg += dr("OpeningBalance").ToString & System.Environment.NewLine
                End If
            End If

            ''Write Header
            FilePath = CreatMT940Files(LineMsg, FileName.ToString())
            ''Write Transaction details Line
            For Each drMT940 As DataRow In dtMT940.Rows
                NoOfTransaction = NoOfTransaction + 1
                LineMsg = Nothing
                'LineMsg += ":61F:" & CDate(dr("Date")).ToString("yyMMdd")
                LineMsg += ":61:" & CDate(dr("Date")).ToString("yyMMdd")
                'LineMsg += ":61:" & dr("Date").ToString
                If dr("RunningBalance") < 0 Then
                    LineMsg += "D"
                    If dr("RunningBalance").ToString.Contains(".") Then
                        LineMsg += dr("RunningBalance").ToString.Replace(".", ",")
                    Else
                        LineMsg += dr("RunningBalance").ToString & ","
                        'LineMsg += dr("RunningBalance").ToString
                    End If
                ElseIf dr("RunningBalance") > 0 Then
                    LineMsg += "C"
                    If dr("RunningBalance").ToString.Contains(".") Then
                        LineMsg += dr("RunningBalance").ToString.Replace(".", ",")
                    Else
                        LineMsg += dr("RunningBalance").ToString & ","
                        'LineMsg += dr("RunningBalance").ToString
                    End If
                End If
                LineMsg += dr("TransactionType") & dr("TransferAccount") & System.Environment.NewLine
                LineMsg += ":86:" & dr("TransactionDetails")
                'If (NoOfTransaction - 1) = LastRow Then
                '    LineMsg += "}"
                'End If
                FilePath = CreatMT940Files(LineMsg, FileName.ToString())
            Next

            LineMsg = Nothing
            LineMsg = ":62F:"
            If dtMT940(LastRow)("ClosingBalance") > 0 Then
                'LineMsg += "D" & dtMT940(LastRow)("Date").ToString & "PKR"
                LineMsg += "D" & CDate(dtMT940(LastRow)("Date")).ToString("yyMMdd") & "PKR"
                If dtMT940(LastRow)("ClosingBalance").ToString.Contains(".") Then
                    LineMsg += dtMT940(LastRow)("ClosingBalance").ToString.Replace(".", ",")
                Else
                    LineMsg += dtMT940(LastRow)("ClosingBalance").ToString & ","
                    'LineMsg += dtMT940(LastRow)("ClosingBalance").ToString
                End If
            ElseIf dtMT940(LastRow)("ClosingBalance") < 0 Then
                LineMsg += "C" & CDate(dtMT940(LastRow)("Date")).ToString("yyMMdd") & "PKR"
                If dtMT940(LastRow)("ClosingBalance").ToString.Contains(".") Then
                    LineMsg += dtMT940(LastRow)("ClosingBalance").ToString.Replace(".", ",")
                Else
                    LineMsg += dtMT940(LastRow)("ClosingBalance").ToString & ","
                    'LineMsg += dtMT940(LastRow)("ClosingBalance").ToString
                End If
            End If
            LineMsg += System.Environment.NewLine & "}"
            FilePath = CreatMT940Files(LineMsg, FileName.ToString())


            ''Write data and MIME type of File
            Dim fileinfo As New IO.FileInfo(FilePath)
            Dim strfilename As String = System.IO.Path.GetFileName(FilePath)
            Dim imageBytes(fileinfo.Length) As Byte
            Dim reader As IO.FileStream = fileinfo.OpenRead
            reader.Read(imageBytes, 0, fileinfo.Length)
            reader.Close()
            reader.Dispose()
            objICMT940.NoOfTxns = NoOfTransaction
            objICMT940.FileLocation = FilePath

            objICMT940.MT940FileMIMEType = "text/plain"
            objICMT940.MT940FileData = imageBytes
            UpdateRTGSFile(objICMT940, NoOfTransaction)


            Actionstring = "Statement of Account for Client with Code [ " & objICMT940.CompanyCode & " ] of Account No [ " & objICMT940.ClientAccountNo & " ]"
            Actionstring += " exported in MT940. Action was taken by user   [ " & UsersID & " ] [ " & UsersName & " ]"
            ICUtilities.AddAuditTrail(Actionstring, "MT940", objICMT940.MT940FileID.ToString, UsersID.ToString, UsersName.ToString, "EXPORT")
            Return MT940FileIDs


        End Function
    End Class
End Namespace
